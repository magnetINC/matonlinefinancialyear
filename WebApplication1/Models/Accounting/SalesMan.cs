﻿using MatDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Accounting
{
    public class SalesMan
    {
        public int EmployeeId { get; set; }
        public string EmployeeName { get; set; }        
    }
}