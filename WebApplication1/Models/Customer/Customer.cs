﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Customer
{
    public class CustomerModel  //model of customer
    {
        public int Id { get; set; }
        public decimal StoreId { get; set; }
        public string Store { get; set; }
        public decimal RackId { get; set; }
        public string Rack { get; set; }
        public string Batch { get; set; }
        public DateTime MfD { get; set; }
        public DateTime ExpD { get; set; }
        public decimal Quantity { get; set; }
        public decimal Rate { get; set; }
        public decimal UnitId { get; set; }
        public decimal Unit { get; set; }
        public decimal Amount { get; set; }
    }

    public class CustomerCentreSearchModel
    {
        public string CustomerName { get; set; }
        public int StateId { get; set; }
        public string State { get; set; }
        public int CityId { get; set; }
        public string City { get; set; }
        public string Status { get; set; }
        public string OpenBalances { get; set; }
    }
}