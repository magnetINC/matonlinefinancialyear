﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Customer
{
    public class SalesQuotationModel  //model of sale quoation
    {
        //public string QuotationNo { get; set; }
        //public int Customer { get; set; }
        //public int SalesMan { get; set; }
        //public string TransDate { get; set; }
        //public int PricingLevel { get; set; }     
        //public int Currency { get; set; }
        //public string Naration { get; set; }
        //public decimal TotalAmount { get; set; }
        //public decimal TotalTax { get; set; }
        //public decimal GrandTotal { get; set; }
        //public bool IsApproved { get; set; }
        public SalesQuotationMasterInfo Master { get; set; }
        public List<SalesQuotationDetailsInfo> LineItems { get; set; }
    }


    //not in use again
    public class LineItemModel
    {
        public int SiNo { get; set; }
        public string Barcode { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
        public int Unit { get; set; }
        public int Batch { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
        public decimal Tax { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal StoreId { get; set; }
    }


}