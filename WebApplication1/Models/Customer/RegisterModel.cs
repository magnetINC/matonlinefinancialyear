﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Register
{
    public class SalesQuotationSearchParameters
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public decimal ledgerId { get; set; }
        public string qoutationNo { get; set; }
        public string condition { get; set; }
        public decimal gmUserId { get; set; }
    }

    public class SalesOrderSearchParameters
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public decimal ledgerId { get; set; }
        public string salesOrderNo { get; set; }
        public string condition { get; set; }
        public decimal gmUserId { get; set; }
    }

    public class DeliveryNoteSearchParameters
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public decimal ledgerId { get; set; }
        public string deliveryNoteNo { get; set; }
        public decimal userId { get; set; }
    }

    public class RejectionInSearchParameters
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public decimal ledgerId { get; set; }
        public string rejectionInNo { get; set; }
        public decimal voucherTypeId { get; set; }
    }
    public class SalesInvoiceSearchParameters
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public decimal ledgerId { get; set; }
        public string voucherNo { get; set; }
        public string salesMode { get; set; }
        public decimal voucherTypeId { get; set; }
    }
    public class PDCReceivableSearchParameters
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public string ledgerName { get; set; }
        public string formNo { get; set; }
    }
    public class ReceiptSearchParameters
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public decimal ledgerId { get; set; }
        public string formNo { get; set; }
    }
    public class CreditNoteSearchParameters
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public string formNo { get; set; }
    }
}