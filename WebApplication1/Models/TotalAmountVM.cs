﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class TotalAmountVM
    {
        public decimal totalAmount { get; set; }
        public string day { get; set; }
    }
}