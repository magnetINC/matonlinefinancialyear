﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.OtherReports
{
    public class ReportSearchViewModels
    {
    }

    public class StockJournalVM
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal VoucherType { get; set; }
        public string VoucherNo { get; set; }
        public string ProductCode { get; set; }
        public string Product { get; set; }
    }

    public class StockVarianceVM
    {
        public decimal ProductId { get; set; }
        public decimal StoreId { get; set; }
        public string ItemCode { get; set; }
    }

    public class InventoryMovementVM
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal VoucherType { get; set; }
        public string VoucherNo { get; set; }
        public decimal ProductId { get; set; }
        public string ProductCode { get; set; }
        public decimal StoreId { get; set; }
        public decimal ProductGroup { get; set; }
        public decimal BatchNoId { get; set; }
    }
    public class PhysicalStockVM
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ProductName { get; set; }
        public string VoucherNo { get; set; }
        public decimal VoucherTypeId { get; set; }
        public decimal ProductCode { get; set; }
        public decimal StoreId { get; set; }
        public string StrProductCode { get; set; }
    }

    public class InventoryStatistics
    {
        public decimal BrandId { get; set; }
        public decimal ModelNoId { get; set; }
        public decimal SizeId { get; set; }
        public decimal ProductGroupId { get; set; }
        public string Criteria { get; set; }
        public string BatchName { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class StockReportRow
    {
        public decimal TotalQtyBalance { get; set; }
        public decimal TotalAverageCost { get; set; }
        public decimal TotalStockValue { get; set; }
        public List<StockReportRowItems> StockReportRowItem { get; set; }
    }
    public class StockReportRowItems
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string StoreName { get; set; }
        public string InwardQty { get; set; }
        public string OutwardQty { get; set; }
        public string Rate { get; set; }
        public string AverageCost { get; set; }
        public string QtyBalance { get; set; }
        public string StockValue { get; set; }
        public string VoucherTypeName { get; set; }
        public string Date { get; set; }
        public string Batch { get; set; }
        public string ProductCode { get; set; }
        public string RefNo { get; set; }
    }
    public class StockReportSearch
    {
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ProductCode { get; set; }
        public decimal StoreId { get; set; }
        public string BatchNo { get; set; }
        public decimal ProductId { get; set; }
        public string RefNo { get; set; }
    }

    public class StockSummary
    {
        public string ProductId { get; set; }
        public string ProductName { get; set; }
        public string StoreId { get; set; }
        public string StoreName { get; set; }
        public string PurchaseRate { get; set; }
        public string SalesRate { get; set; }
        public string QtyBalance { get; set; }
        public string AvgCostValue { get; set; }
        public string StockValue { get; set; }
        public string ProductCode { get; set; }
    }

    public class StockSummarySearch
    {
        public decimal ProductId { get; set; }
        public decimal StoreId { get; set; }
        public string BatchNo { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string ProductCode { get; set; }
        public string RefNo { get; set; }
    }
}