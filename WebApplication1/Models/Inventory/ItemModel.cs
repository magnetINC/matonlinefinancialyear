﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Inventory
{
    public class NewStoreModel  //model to add items to stores when creating an "opening stock" item
    {
        public int Id { get; set; }
        public decimal StoreId { get; set; }
        public string Store { get; set; }
        public decimal RackId { get; set; }
        public string Rack { get; set; }
        public string Batch { get; set; }
        public DateTime MfD { get; set; }
        public DateTime ExpD { get; set; }
        public decimal Quantity { get; set; }
        public decimal Rate { get; set; }
        public decimal UnitId { get; set; }
        public decimal Unit { get; set; }
        public decimal Amount { get; set; }
    }
    public class NewBomModel    //model to populate new bom items
    {
        public decimal BomId { get; set; }
        public decimal RawMaterialId { get; set; }
        public decimal UnitId { get; set; }
        public decimal Quantity { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public DateTime ExtraDate { get; set; }
    }

    public class NewMultipleUnitModel
    {
        public decimal ConversionRate { get; set; }
        public decimal UnitId { get; set; }
        public string Quantities { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public DateTime ExtraDate { get; set; }
        public decimal ProductId { get; set; }
    }
    public class NewStockPostingModel
    {
        public decimal StockPostingId{ get; set; }
        public DateTime Date{ get; set; }
        public decimal VoucherTypeId{ get; set; }
        public string VoucherNo{ get; set; }
        public string InvoiceNo{ get; set; }
        public decimal ProductId{ get; set; }
        public decimal BatchId{ get; set; }
        public decimal UnitId{ get; set; }
        public decimal GodownId{ get; set; }
        public decimal RackId{ get; set; }
        public decimal AgainstVoucherTypeId{ get; set; }
        public string AgainstInvoiceNo{ get; set; }
        public string AgainstVoucherNo{ get; set; }
        public decimal InwardQty{ get; set; }
        public decimal OutwardQty{ get; set; }
        public decimal Rate{ get; set; }
        public decimal FinancialYearId{ get; set; }
        public DateTime ExtraDate{ get; set; }
        public string Extra1{ get; set; }
        public string Extra2{ get; set; }
        public int ProjectId{ get; set; }
        public int CategoryId{ get; set; }
    }
    public class ItemModel
    {
        public ProductSP ProductSp { get; set; }
        public ProductInfo ProductInfo { get; set; }
        public UnitConvertionSP UnitConvertion { get; set; }
        public UnitConvertionInfo UnitConvertionInfo { get; set; }
        public List<NewStoreModel> NewStores { get; set; }
        public List<NewBomModel> NewBoms { get; set; }
        public List<NewMultipleUnitModel> NewMultipleUnits { get; set; }
        public List<NewStockPostingModel> NewStockPostings { get; set; }
        public bool AutoBarcode { get; set; }
        public bool IsSaveBomCheck { get; set; }
        public bool IsSaveMultipleUnitCheck { get; set; }
        public bool IsOpeningStock { get; set; }
        public bool IsBatch { get; set; }
    }

    public class EditItemModel
    {
        public ProductSP ProductSp { get; set; }
        public ProductInfo ProductInfo { get; set; }
        public UnitConvertionSP UnitConvertion { get; set; }
        public UnitConvertionInfo UnitConvertionInfo { get; set; }
        public List<NewStoreModel> NewStores { get; set; }
        public List<NewBomModel> NewBoms { get; set; }
        public List<NewMultipleUnitModel> NewMultipleUnits { get; set; }
        public List<NewStockPostingModel> NewStockPostings { get; set; }
        public bool AutoBarcode { get; set; }
        public bool IsSaveBomCheck { get; set; }
        public bool IsSaveMultipleUnitCheck { get; set; }
        public bool IsOpeningStock { get; set; }
        public bool IsBatch { get; set; }
    }
    public class AddItem
    {
        int inBatchIdWithPartNoNA;  //identity for "batch with barcode"
        decimal decBatchId;
        public decimal SaveItem(ItemModel input)
        {
            ProductSP spProduct = new ProductSP();
            ProductInfo infoProduct = new ProductInfo();
            UnitConvertionSP spUnitConvertion = new UnitConvertionSP();
            UnitConvertionInfo infoUnitConvertion = new UnitConvertionInfo();

            infoProduct.ProductName = input.ProductInfo.ProductName;
            infoProduct.ProductCode = input.ProductInfo.ProductCode;
            infoProduct.PurchaseRate= input.ProductInfo.PurchaseRate;
            infoProduct.SalesRate = input.ProductInfo.SalesRate;
            infoProduct.Mrp = input.ProductInfo.Mrp;
            
            infoProduct.MaximumStock = input.ProductInfo.MaximumStock;
            infoProduct.MinimumStock = input.ProductInfo.MinimumStock;
            infoProduct.ReorderLevel = input.ProductInfo.ReorderLevel;
            infoProduct.Extra1 = input.ProductInfo.Extra1;
            infoProduct.Extra2 = input.ProductInfo.Extra2;
            infoProduct.ExtraDate = DateTime.Now;
            infoProduct.TaxId = input.ProductInfo.TaxId;
            infoProduct.UnitId = input.ProductInfo.UnitId;
            infoProduct.GroupId = input.ProductInfo.GroupId;
            infoProduct.Narration = input.ProductInfo.Narration;
            // code segment modified by ___precious 04/08/2016 (copied by alex from matfinancial desktop 19/06/2017)
            infoProduct.ProductType = input.ProductInfo.ProductType;
            infoProduct.SalesAccount = input.ProductInfo.SalesAccount;
            infoProduct.EffectiveDate = Convert.ToDateTime(input.ProductInfo.EffectiveDate);
            infoProduct.ExpenseAccount = input.ProductInfo.ExpenseAccount;

            infoUnitConvertion.UnitId = input.ProductInfo.UnitId;
            infoUnitConvertion.ConversionRate = 1;
            infoUnitConvertion.Quantities = string.Empty;
            infoUnitConvertion.Extra1 = string.Empty;
            infoUnitConvertion.Extra2 = string.Empty;
            infoUnitConvertion.ExtraDate = DateTime.Now;
            if (input.ProductInfo.TaxapplicableOn=="Rate")
            {
                infoProduct.TaxapplicableOn = "Rate";
            }
            else
            {
                infoProduct.TaxapplicableOn = input.ProductInfo.TaxapplicableOn;
            }
            if (input.ProductInfo.BrandId!=0)
            {
                infoProduct.BrandId = input.ProductInfo.BrandId;
            }
            else
            {
                infoProduct.BrandId = 1;
            }
            if (input.ProductInfo.SizeId!=0)
            {
                infoProduct.SizeId = input.ProductInfo.SizeId;
            }
            else
            {
                infoProduct.SizeId = 1;
            }
            if (input.ProductInfo.ModelNoId!=0)
            {
                infoProduct.ModelNoId = input.ProductInfo.ModelNoId;
            }
            else
            {
                infoProduct.ModelNoId = 1;
            }
            if (input.ProductInfo.GodownId!=0)
            {
                infoProduct.GodownId = input.ProductInfo.GodownId;
            }
            else
            {
                infoProduct.GodownId = 1;
            }
            if (input.ProductInfo.RackId!=0)
            {
                infoProduct.RackId = input.ProductInfo.RackId;
            }
            else
            {
                infoProduct.RackId = 1;
            }
            infoProduct.IsallowBatch = input.ProductInfo.IsallowBatch;
            infoProduct.IsBom = input.ProductInfo.IsBom;
            infoProduct.Isopeningstock = input.ProductInfo.Isopeningstock;
            if(infoProduct.Isopeningstock==true)
            {
                infoProduct.PurchaseRate = input.NewStores.FirstOrDefault().Rate;
                //use the first rate for purchase rate since all stores use same rate
            }
            infoProduct.Ismultipleunit = input.ProductInfo.Ismultipleunit;
           
            infoProduct.IsActive = input.ProductInfo.IsActive;
            infoProduct.IsshowRemember = input.ProductInfo.IsshowRemember;
            if(infoProduct.Isopeningstock)
            {
                if(!infoProduct.IsallowBatch)
                {
                    decimal productIdentity = spProduct.ProductAdd(infoProduct);
                    if (productIdentity > 0)
                    {
                        PostledgerItems(productIdentity, input.NewStores, input.ProductInfo.EffectiveDate);  /// a call to Update Ledger When an item is Created
                        infoUnitConvertion.ProductId = productIdentity;
                        spUnitConvertion.UnitConvertionAdd(infoUnitConvertion);
                        BatchWithBarCode(input, productIdentity);
                        if (input.IsSaveBomCheck)
                        {
                            int bm = BomTableFill(input.NewBoms, productIdentity);
                        }
                        if (input.IsSaveMultipleUnitCheck)
                        {
                            int unt = UnitConvertionTableFill(input.NewMultipleUnits, productIdentity);
                        }
                        if (input.IsOpeningStock)
                        {
                            decimal stk = StockPostingTableFill(input.NewStores, productIdentity, input);
                        }
                        return productIdentity;
                    }
                }
                if(infoProduct.IsallowBatch)
                {
                    decimal productIdentity = spProduct.ProductAdd(infoProduct);
                    if (productIdentity > 0)
                    {
                        PostledgerItems(productIdentity, input.NewStores, input.ProductInfo.EffectiveDate);  /// a call to Update Ledger When an item is Created
                        infoUnitConvertion.ProductId = productIdentity;
                        spUnitConvertion.UnitConvertionAdd(infoUnitConvertion);
                        if (input.IsSaveBomCheck)
                        {
                            int bm = BomTableFill(input.NewBoms, productIdentity);
                        }
                        if (input.IsSaveMultipleUnitCheck)
                        {
                            int unt = UnitConvertionTableFill(input.NewMultipleUnits, productIdentity);
                        }
                        BatchTableWithStockAndProductBatchFill(input.NewStores, productIdentity, input);
                        return productIdentity;
                    }
                }
            }
            else
            {
                decimal productIdentity = spProduct.ProductAdd(infoProduct);
                if (productIdentity > 0)
                {
                    PostledgerItems(productIdentity, input.NewStores, input.ProductInfo.EffectiveDate); /// a call to Update Ledger When an item is Created
                    infoUnitConvertion.ProductId = productIdentity;
                    if (spUnitConvertion.UnitConvertionAdd(infoUnitConvertion) > 0)
                    {
                        if (input.IsSaveBomCheck)
                        {
                            int bm = BomTableFill(input.NewBoms, productIdentity);
                        }
                        if (input.IsSaveMultipleUnitCheck)
                        {
                            int unt = UnitConvertionTableFill(input.NewMultipleUnits, productIdentity);
                        }
                        if( BatchWithBarCode(input, productIdentity)>0)
                        {
                            //return "SUCCESS";
                        }
                    }
                }
                return productIdentity;
            }

            return 0;
        }

        public int PostledgerItems(decimal productId,List<NewStoreModel> stores,DateTime ledgerDate)
        {
            try
            {
                string strfinancialId;
                FinancialYearSP spFinancialYear = new FinancialYearSP();
                FinancialYearInfo infoFinancialYear = new FinancialYearInfo();
                infoFinancialYear = spFinancialYear.FinancialYearViewForAccountLedger(1);
                strfinancialId = infoFinancialYear.FromDate.ToString("dd-MMM-yyyy");
                LedgerPostingInfo info = new LedgerPostingInfo();
                LedgerPostingSP sp = new LedgerPostingSP();
                foreach(var store in stores)
                {
                    decimal OpeningStock = Convert.ToDecimal(store.Amount);
                    info.Date = ledgerDate;
                    info.VoucherTypeId = 2;
                    info.VoucherNo = "55";
                    info.LedgerId = 55;
                    info.Debit = 0;
                    info.Credit = OpeningStock;
                    info.DetailsId = 0;
                    info.YearId = 1;
                    info.InvoiceNo = 55.ToString();
                    info.ChequeNo = "";
                    info.ChequeDate = DateTime.Now;
                    info.ExtraDate = DateTime.Now;
                    info.Extra1 = productId.ToString();
                    info.Extra2 = "";
                    return sp.LedgerPostingAdd(info);
                }
               
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 0;
        }

        /// <summary>
        /// Function to save batch
        /// </summary>
        public int BatchWithBarCode(ItemModel item,decimal productId)
        {
            try
            {
                BatchSP spBatch = new BatchSP();
                BatchInfo infoBatch = new BatchInfo();
                Int64 inBarcode = spBatch.AutomaticBarcodeGeneration();
                infoBatch.BatchNo = "NA";
                infoBatch.ExpiryDate = DateTime.Now;
                infoBatch.ManufacturingDate = DateTime.Now;
                infoBatch.partNo = item.ProductInfo.PartNo;
                infoBatch.ProductId = productId;                
                infoBatch.narration = string.Empty;
                infoBatch.ExtraDate = DateTime.Now;
                if (item.AutoBarcode == false && item.ProductInfo.barcode != "")  // precious
                {
                    infoBatch.barcode = item.ProductInfo.barcode;
                }
                else
                {
                    infoBatch.barcode = Convert.ToString(inBarcode);
                }
                infoBatch.Extra1 = string.Empty;
                infoBatch.Extra2 = string.Empty;
                return inBatchIdWithPartNoNA = spBatch.BatchAddWithBarCode(infoBatch);
            }
            catch (Exception ex)
            {
                return 0;   
            }
        }

        /// <summary>
        /// Function to add data to tbl_Bom
        /// </summary>
        public int BomTableFill(List<NewBomModel> boms ,decimal productId)
        {
            try
            {
                BomInfo infoBom = new BomInfo();
                BomSP spBom = new BomSP();
                
                foreach(var bom in boms)
                {
                    infoBom.RowmaterialId = bom.RawMaterialId;
                    infoBom.UnitId = bom.UnitId;
                    infoBom.Quantity = bom.Quantity;
                    infoBom.Extra1 = "";
                    infoBom.Extra2 = "";
                    infoBom.ExtraDate = DateTime.Now;
                    if(spBom.BomFromDatatable(infoBom, productId)<1)    //means a bom item fails to add
                    {
                        return 0;
                    }
                }
                return 1;           
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 0;
        }

        /// <summary>
        /// Function to add multiple unit details to tbl_UnitConversion table
        /// </summary>
        public int UnitConvertionTableFill(List<NewMultipleUnitModel> units, decimal productId)
        {
            try
            {
                UnitConvertionSP spUnitConvertion = new UnitConvertionSP();
                UnitConvertionInfo infoUnitConversion = new UnitConvertionInfo();
                foreach(var unit in units)
                {
                    if(unit.ConversionRate!=0)
                    {
                        infoUnitConversion.ConversionRate = unit.ConversionRate;
                        infoUnitConversion.UnitId = unit.UnitId;
                        infoUnitConversion.Quantities = unit.Quantities;
                        infoUnitConversion.Extra1 = unit.Extra1;
                        infoUnitConversion.Extra2 = unit.Extra2;
                        infoUnitConversion.ExtraDate = unit.ExtraDate;
                        infoUnitConversion.ProductId = productId;
                        return spUnitConvertion.UnitConvertionAdd(infoUnitConversion);
                    }
                }                
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 0;
        }

        /// <summary>
        /// Function to save data to StockPosting Table
        /// </summary>
        public decimal StockPostingTableFill(List<NewStoreModel> stores,decimal productId,ItemModel item)
        {
            try
            {
                StockPostingSP spStockPosting = new StockPostingSP();
                StockPostingInfo infoStockPosting;
                foreach(var store in stores)
                {
                    infoStockPosting = new StockPostingInfo
                    {
                        AgainstInvoiceNo = string.Empty,
                        AgainstVoucherNo = string.Empty,
                        Date = Convert.ToDateTime(item.ProductInfo.EffectiveDate),
                        AgainstVoucherTypeId = 0,
                        InvoiceNo = Convert.ToString(productId),
                        VoucherNo = Convert.ToString(productId),
                        ProductId=productId,
                        VoucherTypeId = 2,
                        UnitId = Convert.ToDecimal(store.UnitId),
                        InwardQty = Convert.ToDecimal(store.Quantity),
                        OutwardQty = 0,
                        Rate = Convert.ToDecimal(store.Rate),
                        FinancialYearId =MATFinancials.PublicVariables._decCurrentFinancialYearId,
                        Extra1 = string.Empty,
                        Extra2 = string.Empty,
                        ExtraDate = DateTime.Now,
                        RackId=store.RackId,
                        GodownId=store.StoreId,
                        ProjectId=1,
                        CategoryId=1
                    };                 
                    if (!item.IsBatch)
                    {
                        infoStockPosting.BatchId = inBatchIdWithPartNoNA;
                    }
                    if(spStockPosting.StockPostingAdd(infoStockPosting)<1)
                    {
                        return 0;
                    }
                }
                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Function to add data to StockTable With Batch
        /// </summary>
        public decimal BatchTableWithStockAndProductBatchFill(List<NewStoreModel> stores,decimal productId,ItemModel item)
        {
            try
            {
                BatchSP spBatch = new BatchSP();
                BatchInfo infoBatch;
                StockPostingSP spStockPosting = new StockPostingSP();
                StockPostingInfo infoStockPosting;
                foreach(var store in stores)
                {
                    infoStockPosting = new StockPostingInfo
                    {                      

                        AgainstInvoiceNo = string.Empty,
                        AgainstVoucherNo = string.Empty,
                        Date = Convert.ToDateTime(item.ProductInfo.EffectiveDate),
                        AgainstVoucherTypeId = 0,
                        InvoiceNo = Convert.ToString(productId),
                        VoucherNo = Convert.ToString(productId),
                        ProductId = productId,
                        VoucherTypeId = 2,
                        UnitId = Convert.ToDecimal(store.UnitId),
                        InwardQty = Convert.ToDecimal(store.Quantity),
                        OutwardQty = 0,
                        Rate = Convert.ToDecimal(store.Rate),
                        FinancialYearId =MATFinancials.PublicVariables._decCurrentFinancialYearId,
                        Extra1 = string.Empty,
                        Extra2 = string.Empty,
                        ExtraDate = DateTime.Now,
                        RackId = store.RackId,
                        GodownId = store.StoreId,
                        ProjectId = 1,
                        CategoryId = 1
                    };

                    infoBatch = new BatchInfo
                    {
                        ManufacturingDate=store.MfD,
                        ExpiryDate=store.ExpD,
                        BatchNo=store.Batch,
                        ProductId=productId,
                        Extra1=string.Empty,
                        Extra2=string.Empty,
                        ExtraDate=DateTime.Now,
                        barcode=Convert.ToString(spBatch.AutomaticBarcodeGeneration()),
                        
                    };
                    decBatchId = spBatch.BatchAddReturnIdentity(infoBatch);
                    if (store.RackId==0)
                    {
                        infoStockPosting.RackId = 1;
                    }
                    else
                    {
                        infoStockPosting.RackId = store.RackId;
                    }
                    if(store.StoreId==0)
                    {
                        infoStockPosting.GodownId = 1;
                    }
                    else
                    {
                        infoStockPosting.GodownId = store.StoreId;
                    }
                    if(!item.IsBatch)
                    {
                        infoStockPosting.BatchId = 0;
                    }
                    else
                    {
                        infoStockPosting.BatchId = decBatchId;
                    }
                    var chk = spStockPosting.StockPostingAdd(infoStockPosting);
                    if(chk<1)
                    {
                        return 0;
                    }
                }
                return 1;  
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "PC:18" + ex.Message;
                return 0;
            }
        }
    }
}