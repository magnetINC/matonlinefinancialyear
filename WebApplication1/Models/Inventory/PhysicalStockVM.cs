﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Inventory
{
    public class PhysicalStockVM
    {
        public PhysicalStockMasterInfo StockMaster { get; set; }
        public List<PhysicalStockDetailsInfo> StockDetails { get; set; }
        public decimal TotalAmount { get; set; }
    }
}