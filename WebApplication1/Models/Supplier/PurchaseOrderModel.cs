﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Supplier
{
    public class PurchaseOrderModel  //model of sale quoation
    {
        public int SiNo { get; set; }
        public string OrderNo { get; set; }
        public int Supplier { get; set; }
        public int Currency { get; set; }
        public string TransDate { get; set; }
        public string DueDate { get; set; }
        public string DueDays { get; set; }
       // public int SalesMan { get; set; }
        
        //public int PricingLevel { get; set; }     
        
        public string Narration { get; set; }
        public decimal TotalAmount { get; set; }
        //public decimal TotalTax { get; set; }
        //public decimal GrandTotal { get; set; }
        public List<LineItemModel> LineItems { get; set; }
    }



    public class LineItemModel
    {
        public int SiNo { get; set; }
        public string Barcode { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
        public int Unit { get; set; }
        //public int Batch { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
        public decimal TaxId { get; set; }
        public decimal TaxAmount { get; set; }
    }


}