﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
//using System.Web.Mvc;

namespace MatApi.Models.Supplier
{
    public class SupplierCentreSearchModel
    {
        public string SupplierName { get; set; }
        public int StateId { get; set; }
        public string State { get; set; }
        public int CityId { get; set; }
        public string City { get; set; }
        public string Status { get; set; }
    }
}