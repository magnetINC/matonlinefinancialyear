﻿using MatApi.Models;
using MatApi.Models.Customer;
using MatApi.Models.Register;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RegisterController : ApiController
    {
        decimal decQuotationMasterId = 0;
        TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();

        [HttpGet]
        public HttpResponseMessage CashOrPartyComboFill()
        {
            try
            {
                var cashOrParty = TransactionGeneralFillObj.CashOrPartyUnderSundryDrComboFill(true);

                return Request.CreateResponse(HttpStatusCode.OK, (object)cashOrParty);

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpGet]
        public DataTable SalesModeComboFill(int id)
        {
            decimal decVoucherTypeId = 0;
            DataTable dtbl = new DataTable();
            SalesDetailsSP spSalesDetails = new SalesDetailsSP();
            try
            {
                decVoucherTypeId = id;
                dtbl = spSalesDetails.voucherNoViewAllByVoucherTypeIdForSi(decVoucherTypeId);
                DataRow drow = dtbl.NewRow();
                drow["invoiceNo"] = "All";
                dtbl.Rows.InsertAt(drow, 0);

                return dtbl;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        [HttpGet]
        public DataTable voucherTypeComboFill()
        {
            var vType = new TransactionsGeneralFill().VoucherTypeComboFill("Rejection In", true);
            DataRow drow = vType.NewRow();
            drow["voucherTypeName"] = "-Select Type-";
            vType.Rows.InsertAt(drow, 0);
            return vType;
        }

        [HttpGet]
        public DataTable cashOrBankComboFill()
        {
            try
            {
                DataTable dtbl = new DataTable();
                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                dtbl = obj.AccountLedgerComboFill();

                // modify dtbl to get only cash and banks
                decimal[] ledgers = new decimal[] { 27, 28 };
                var query = (from d in dtbl.AsEnumerable()
                             where ledgers.Contains(d.Field<decimal>("accountGroupId"))
                             select d).ToList();
                dtbl = query.CopyToDataTable();

                return dtbl;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        [HttpGet]
        public DataTable AccountLedgerComboFill()
        {
            try
            {
                DataTable dtbl = new DataTable();
                PDCPayableMasterSP sppdcpayable = new PDCPayableMasterSP();
                dtbl = sppdcpayable.AccountLedgerComboFill(false);
                DataRow dr = dtbl.NewRow();
                dr["ledgerId"] = 0;
                dr["ledgerName"] = "All";
                dtbl.Rows.InsertAt(dr, 0);

                return dtbl;
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        [HttpPost]
        public List<SalesOrderConfirmationListVM> SalesQuotation(SalesQuotationSearchParameters searchParam)
        {
            try
            {
                string strCondition = string.Empty;
                string strInvoiceNo = string.Empty;
                decimal decLedgerId = searchParam.ledgerId;

                DataTable dtblSalesQuotationMasterRegister = new DataTable();
                SalesQuotationMasterSP SpSalesQuotationMaster = new SalesQuotationMasterSP();

                if (searchParam.ledgerId == 0)
                {
                    decLedgerId = -1;
                }

                string invoiceNo = "";
                invoiceNo = searchParam.qoutationNo;

                dtblSalesQuotationMasterRegister = SpSalesQuotationMaster.SalesQuotationRegisterSearch(invoiceNo, decLedgerId, searchParam.fromDate, searchParam.toDate, searchParam.condition);
                List<SalesOrderConfirmationListVM> respnse = new List<SalesOrderConfirmationListVM>();
                foreach(DataRow dr in dtblSalesQuotationMasterRegister.Rows)
                {
                    SalesQuotationMasterInfo quot = new SalesQuotationMasterSP().SalesQuotationMasterView(Convert.ToDecimal(dr[7].ToString()));
                    UserInfo user = new UserSP().UserView(quot.userId);
                    UserInfo gm = new UserSP().UserView(searchParam.gmUserId);

                    if(gm.StoreId==user.StoreId)
                    {
                        respnse.Add(new SalesOrderConfirmationListVM
                        {
                            approved = dr[9].ToString(),
                            date = dr[4].ToString(),
                            invoiceNo = dr[1].ToString(),
                            ledgerId = dr[2].ToString(),
                            ledgerName = dr[5].ToString(),
                            narration = dr[8].ToString(),
                            QuotationMasterId = dr[7].ToString(),
                            totalAmount = dr[6].ToString(),
                            userName = dr[10].ToString(),
                            voucherTypeName = dr[3].ToString()
                        });
                    }
                    
                }
                return respnse;
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        [HttpPost]
        public List<SalesOrderAuthorizationListVM> SalesOrder(SalesOrderSearchParameters searchParam)
        {
            try
            {
                string strCondition = string.Empty;
                string strInvoiceNo = string.Empty;
                decimal decLedgerId = searchParam.ledgerId;

                DataTable dtblSalesOrderRegister = new DataTable();
                SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
                string invoiceNo = "";

                if (searchParam.salesOrderNo == string.Empty)
                {
                    searchParam.salesOrderNo = "All";
                }
                if (searchParam.ledgerId == 0)
                {
                    decLedgerId = -1;
                }

                //DateTime fromDate = Convert.ToDateTime(searchParam.fromDate);
                //DateTime toDate = Convert.ToDateTime(searchParam.toDate);

                invoiceNo = searchParam.salesOrderNo;

                dtblSalesOrderRegister = spSalesOrderMaster.SalesOrderRegisterSearch(invoiceNo, decLedgerId, searchParam.fromDate, searchParam.toDate, searchParam.condition);
                List<SalesOrderAuthorizationListVM> respnse = new List<SalesOrderAuthorizationListVM>();
                foreach (DataRow dr in dtblSalesOrderRegister.Rows)
                {
                    SalesOrderMasterInfo quot = new SalesOrderMasterSP().SalesOrderMasterView(Convert.ToDecimal(dr[1].ToString()));
                    string query = string.Format("select userId from tbl_User where userName='{0}'", dr[11].ToString());
                    decimal userIdFromUsername = Convert.ToDecimal(new DBMatConnection().getSingleValue(query));
                    UserInfo user = new UserSP().UserView(userIdFromUsername);
                    UserInfo gm = new UserSP().UserView(searchParam.gmUserId);

                    if (gm.StoreId == user.StoreId)
                    {
                        respnse.Add(new SalesOrderAuthorizationListVM
                        {
                            AuthorizationStatus = dr[13].ToString(),
                            date = dr[5].ToString(),
                            invoiceNo = dr[2].ToString(),
                            dueDate = dr[7].ToString(),
                            ledgerName = dr[6].ToString(),
                            QuotationNo = dr[5].ToString(),
                            salesOrderMasterId = dr[1].ToString(),
                            totalAmount = dr[9].ToString(),
                            userName = dr[11].ToString(),
                            voucherTypeName = dr[3].ToString()
                        });
                    }

                }
                return respnse;
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        [HttpPost]
        public List<WaybillResonse> WaybillListing(WaybillParam searchParam)
        {
            List<WaybillResonse> response = new List<WaybillResonse>();
            DBMatConnection con = new DBMatConnection();
            string query = "";
            if(searchParam.condition=="All")
            {
                query = string.Format("select * from tbl_salesordermaster where extra2>='{0}' and extra2<='{1}' and AuthorizationStatus in ('Authorized','Processed') and vouchertypeid=10030 order by extradate desc",
                searchParam.fromDate, searchParam.toDate, searchParam.condition);
            }
            else
            {
                query = string.Format("select * from tbl_salesordermaster where extra2>='{0}' and extra2<='{1}' and AuthorizationStatus='{2}' and vouchertypeid=10030 order by extradate desc",
                searchParam.fromDate, searchParam.toDate, searchParam.condition);
            }
            
            DataTable result=con.customSelect(query);
            foreach(DataRow row in result.Rows)
            {
                UserInfo user = new UserSP().UserView(Convert.ToDecimal(row[13].ToString()));
                UserInfo gm = new UserSP().UserView(searchParam.UserId);
                if(user.StoreId==gm.StoreId)
                {
                    response.Add(new WaybillResonse
                    {
                        SalesOrderMasterId = Convert.ToDecimal(row[0].ToString()),
                        AgentId = Convert.ToDecimal(row[8].ToString()),
                        AgentName = new AccountLedgerSP().AccountLedgerView(Convert.ToDecimal(row[8].ToString())).LedgerName,
                        AuthorizationDate = Convert.ToDateTime(row[19].ToString()).ToShortDateString(),
                        OrderDate = Convert.ToDateTime(row[5].ToString()).ToShortDateString(),
                        Status = row[21].ToString(),
                        VoucherNo = row[1].ToString()
                    });
                }
                
            }
            return response;
        }

        [HttpGet]
        public decimal OrderConfirmationCount()
        {
            decimal userId = MATFinancials.PublicVariables._decCurrentUserId;
            var user = new UserSP().UserView(userId);
            string storeId = user.StoreId;
            decimal count = 0;

            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("select * from tbl_SalesQuotationMaster where approved ='Pending'");
            var orders = conn.customSelect(queryStr);

            foreach (DataRow o in orders.Rows)
            {
                decimal Id = Convert.ToDecimal(o[17]);
                var nUser = new UserSP().UserView(Id);
                string sId = nUser.StoreId;
                if (sId == storeId)
                {
                    count++;
                }
            }
            return count;
        }

        [HttpGet]
        public decimal OrderAuthorisationCount()
        {
            decimal userId = MATFinancials.PublicVariables._decCurrentUserId;
            var user = new UserSP().UserView(userId);
            string storeId = user.StoreId;
            decimal count = 0;

            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("select * from tbl_SalesOrderMaster where AuthorizationStatus ='Pending'");
            var orders = conn.customSelect(queryStr);

            foreach (DataRow o in orders.Rows)
            {
                decimal Id = Convert.ToDecimal(o[13]);
                var nUser = new UserSP().UserView(Id);
                string sId = nUser.StoreId;
                if (sId == storeId)
                {
                    count++;
                }
            }
            return count;
        }

        [HttpGet]
        public decimal ReleaseFormCount()
        {
            decimal userId = MATFinancials.PublicVariables._decCurrentUserId;
            var user = new UserSP().UserView(userId);
            string storeId = user.StoreId;
            decimal count = 0;

            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("select * from tbl_salesordermaster where AuthorizationStatus = 'Authorized' and vouchertypeid=10030");
            var orders = conn.customSelect(queryStr);

            foreach (DataRow o in orders.Rows)
            {
                decimal Id = Convert.ToDecimal(o[13]);
                var nUser = new UserSP().UserView(Id);
                string sId = nUser.StoreId;
                if (sId == storeId)
                {
                    count++;
                }
            }
            return count;
        }

        [HttpPost]
        public DataTable DeliveryNote(DeliveryNoteSearchParameters searchParam)
        {
            DataTable resp = new DataTable();
            DataTable dtblDeliveryNote = new DataTable();
            resp.Columns.Add("slNo", typeof(string));
            resp.Columns.Add("deliveryNoteMasterId", typeof(string));
            resp.Columns.Add("invoiceNo", typeof(string));
            resp.Columns.Add("voucherTypeName", typeof(string));
            resp.Columns.Add("Date", typeof(string));
            resp.Columns.Add("CashOrParty", typeof(string));
            resp.Columns.Add("Amount", typeof(string));
            resp.Columns.Add("narration", typeof(string));
            resp.Columns.Add("currencyName", typeof(string));
            resp.Columns.Add("userName", typeof(string));
            //resp.Columns.Add("authorizedBy", typeof(string));
            resp.Columns.Add("OrderNoOrQuotationNo", typeof(string));
            resp.Columns.Add("status", typeof(string));

            try
            {
                string strCondition = "";
                string strInvoiceNo = "";

                DeliveryNoteMasterSP spDeliveryNoteMaster = new DeliveryNoteMasterSP();

                if (searchParam.deliveryNoteNo == "")
                {
                    strInvoiceNo = "";
                }
                else
                {
                    strInvoiceNo = searchParam.deliveryNoteNo;
                }
                dtblDeliveryNote = spDeliveryNoteMaster.DeliveryNoteRegisterGridFillCorrespondingToInvoiceNoAndLedger(strInvoiceNo, 
                    searchParam.ledgerId, searchParam.fromDate, searchParam.toDate, MATFinancials.PublicVariables._inNoOfDecimalPlaces);

                foreach (DataRow dt in dtblDeliveryNote.Rows)
                {
                    var currentUser = new UserSP().UserView(searchParam.userId);
                    decimal itemsLocation = getDeliveryNoteItemStore(Convert.ToDecimal(dt.ItemArray[1]));

                    string query = "SELECT RoleId FROM tbl_CycleActionPriviledge WHERE CycleAction='Audit Control'";
                    var roleId = new DBMatConnection().getSingleValue(query);

                    //var orderUserId = new SalesOrderMasterSP().SalesOrderMasterView(Convert.ToDecimal(dt.ItemArray[10])).UserId;
                    //var confirmedBy = new UserSP().UserView(orderUserId);
                    var deliveryNoteUserId = new DeliveryNoteMasterSP().DeliveryNoteMasterView(Convert.ToDecimal(dt.ItemArray[1])).UserId;
                    resp.Rows.Add(new Object[] {
                                dt.ItemArray[0],
                                dt.ItemArray[1],
                                dt.ItemArray[2],
                                dt.ItemArray[3],
                                dt.ItemArray[4],
                                dt.ItemArray[5],
                                dt.ItemArray[6],
                                dt.ItemArray[7],
                                dt.ItemArray[8],
                                new UserSP().UserView(deliveryNoteUserId).FirstName +" "+new UserSP().UserView(deliveryNoteUserId).LastName,
                                //confirmedBy.FirstName+" "+confirmedBy.LastName,
                                dt.ItemArray[10],
                                dt.ItemArray[11]
                            });

                    //if (roleId != null)
                    //{
                    //    if (roleId == "1")
                    //    {
                    //        if (dt.ItemArray[11].ToString() == "Processed")
                    //        {
                    //            resp.Rows.Add(new Object[] {
                    //                dt.ItemArray[0],
                    //                dt.ItemArray[1],
                    //                dt.ItemArray[2],
                    //                dt.ItemArray[3],
                    //                dt.ItemArray[4],
                    //                dt.ItemArray[5],
                    //                dt.ItemArray[6],
                    //                dt.ItemArray[7],
                    //                dt.ItemArray[8],
                    //                new UserSP().UserView(deliveryNoteUserId).FirstName +" "+new UserSP().UserView(deliveryNoteUserId).LastName,
                    //                //confirmedBy.FirstName+" "+confirmedBy.LastName,
                    //                dt.ItemArray[10],
                    //                dt.ItemArray[11]
                    //            });
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (currentUser.StoreId == itemsLocation.ToString() && currentUser.RoleId.ToString() == roleId)
                    //        {
                    //            if (dt.ItemArray[11].ToString() == "Processed")
                    //            {
                    //                resp.Rows.Add(new Object[] {
                    //                dt.ItemArray[0],
                    //                dt.ItemArray[1],
                    //                dt.ItemArray[2],
                    //                dt.ItemArray[3],
                    //                dt.ItemArray[4],
                    //                dt.ItemArray[5],
                    //                dt.ItemArray[6],
                    //                dt.ItemArray[7],
                    //                dt.ItemArray[8],
                    //                new UserSP().UserView(deliveryNoteUserId).FirstName +" "+new UserSP().UserView(deliveryNoteUserId).LastName,
                    //                //confirmedBy.FirstName+" "+confirmedBy.LastName,
                    //                dt.ItemArray[10],
                    //                dt.ItemArray[11]
                    //            });
                    //            }
                    //        }
                    //    }

                    //}                    

                }                       

                //return dtblDeliveryNote;
            }
            catch (Exception ex)
            {
            }
            return resp;
        }

        [HttpPost]
        public DataTable DeliveryNoteForProcessing(DeliveryNoteSearchParameters searchParam)
        {
            DataTable resp = new DataTable();
            DataTable dtblDeliveryNote = new DataTable();
            resp.Columns.Add("slNo", typeof(string));
            resp.Columns.Add("deliveryNoteMasterId", typeof(string));
            resp.Columns.Add("invoiceNo", typeof(string));
            resp.Columns.Add("voucherTypeName", typeof(string));
            resp.Columns.Add("status", typeof(string));
            resp.Columns.Add("Date", typeof(string));
            resp.Columns.Add("CashOrParty", typeof(string));
            resp.Columns.Add("Amount", typeof(string));
            resp.Columns.Add("narration", typeof(string));
            resp.Columns.Add("currencyName", typeof(string));
            resp.Columns.Add("userName", typeof(string));
            resp.Columns.Add("OrderNoOrQuotationNo", typeof(string));

            try
            {
                string strCondition = "";
                string strInvoiceNo = "";

                DeliveryNoteMasterSP spDeliveryNoteMaster = new DeliveryNoteMasterSP();

                if (searchParam.deliveryNoteNo == "")
                {
                    strInvoiceNo = "";
                }
                else
                {
                    strInvoiceNo = searchParam.deliveryNoteNo;
                }
                dtblDeliveryNote = spDeliveryNoteMaster.DeliveryNoteRegisterGridFillCorrespondingToInvoiceNoAndLedger(strInvoiceNo,
                    searchParam.ledgerId, searchParam.fromDate, searchParam.toDate, MATFinancials.PublicVariables._inNoOfDecimalPlaces);

                foreach (DataRow dt in dtblDeliveryNote.Rows)
                {
                    var currentUser = new UserSP().UserView(searchParam.userId);
                    decimal itemsLocation = getDeliveryNoteItemStore(Convert.ToDecimal(dt.ItemArray[1]));
                    
                    string query = "SELECT RoleId FROM tbl_CycleActionPriviledge WHERE CycleAction='Warehouse Manager'";
                    var roleId = new DBMatConnection().getSingleValue(query);
                    if (roleId != null)
                    {
                        if(roleId=="1")
                        {
                            resp.Rows.Add(new Object[] {
                                        dt.ItemArray[0],
                                        dt.ItemArray[1],
                                        dt.ItemArray[2],
                                        dt.ItemArray[3],
                                        dt.ItemArray[4],
                                        dt.ItemArray[5],
                                        dt.ItemArray[6],
                                        dt.ItemArray[7],
                                        dt.ItemArray[8],
                                        dt.ItemArray[9],
                                        dt.ItemArray[10],
                                        dt.ItemArray[11]
                                    });
                        }
                        else
                        {
                            if (currentUser.StoreId == itemsLocation.ToString() && currentUser.RoleId.ToString() == roleId)
                            {
                                resp.Rows.Add(new Object[] {
                                dt.ItemArray[0],
                                dt.ItemArray[1],
                                dt.ItemArray[2],
                                dt.ItemArray[3],
                                dt.ItemArray[4],
                                dt.ItemArray[5],
                                dt.ItemArray[6],
                                dt.ItemArray[7],
                                dt.ItemArray[8],
                                dt.ItemArray[9],
                                dt.ItemArray[10],
                                dt.ItemArray[11]
                            });
                            }
                        }
                    }

                }

                //return dtblDeliveryNote;
            }
            catch (Exception ex)
            {
            }
            return resp;
        }

        private decimal getDeliveryNoteItemStore(decimal deliveryNoteMasterId)
        {
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("SELECT DISTINCT godownId FROM tbl_deliverynotedetails WHERE deliveryNoteMasterId={0}",deliveryNoteMasterId);
            return Convert.ToDecimal(con.getSingleValue(query));
        }

        private string getDeliveryNoteStatus(decimal deliveryNoteMasterId)
        {
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("SELECT status FROM tbl_deliverynotemaster WHERE deliveryNoteMasterId={0}", deliveryNoteMasterId);
            return con.getSingleValue(query);
        }

        [HttpPost]
        public DataTable RejectionIn(RejectionInSearchParameters searchParam)
        {
            RejectionInMasterSP SpRejectionInMaster = new RejectionInMasterSP();
            DataTable dtbl = new DataTable();
            try
            {
                decimal decLedgerId = searchParam.ledgerId;
                string strInvoiceNo = "";

                if (searchParam.rejectionInNo == "")
                {
                    strInvoiceNo = "";
                }
                else
                {
                    strInvoiceNo = searchParam.rejectionInNo;
                }

                if (searchParam.ledgerId == 0 || searchParam.ledgerId == -1)
                {
                    decLedgerId = -1;
                }

                return dtbl = SpRejectionInMaster.RejectionInRegisterFill(searchParam.fromDate, searchParam.toDate, decLedgerId,
                    strInvoiceNo, searchParam.voucherTypeId);
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        [HttpPost]
        public DataTable SalesInvoice(SalesInvoiceSearchParameters searchParam)
        {
            string strVoucherNo = "";
            SalesMasterSP spSalesmaster = new SalesMasterSP();
            DataTable dtblSalesInvoice = new DataTable();
            try
            {
                if (searchParam.voucherNo == "")
                {
                    strVoucherNo = "";
                }
                else
                {
                    strVoucherNo = searchParam.voucherNo;
                }
                dtblSalesInvoice = spSalesmaster.SalesInvoiceRegisterGridfill(searchParam.fromDate, searchParam.toDate, 
                    searchParam.voucherTypeId, searchParam.ledgerId, strVoucherNo, searchParam.salesMode);

                return dtblSalesInvoice;
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        [HttpPost]
        public DataTable PDCReceivable(PDCReceivableSearchParameters searchParam)
        {
            try
            {
                if (searchParam.ledgerName == "")
                {
                    searchParam.ledgerName = "ALL";
                }
                DataTable dtbl = new DataTable();
                PDCReceivableMasterSP spPdcreceivable = new PDCReceivableMasterSP();
                dtbl = spPdcreceivable.PDCReceivableRegisterSearch(searchParam.fromDate, searchParam.toDate,
                    searchParam.formNo, searchParam.ledgerName);
                return dtbl;
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        [HttpPost]
        public DataTable Receipt(ReceiptSearchParameters searchParam)
        {
            try
            {
                ReceiptMasterSP SpReceiptMaster = new ReceiptMasterSP();
                ReceiptMasterInfo InfoReceiptMaster = new ReceiptMasterInfo();
                DataTable dtblReceipt = new DataTable();

                dtblReceipt = SpReceiptMaster.ReceiptMasterSearch(searchParam.fromDate, searchParam.toDate, 
                    searchParam.ledgerId, searchParam.formNo);

                return dtblReceipt;
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        [HttpPost]
        public DataTable CreditNote(CreditNoteSearchParameters searchParam)
        {
            string strVoucherNo = "";
            CreditNoteMasterSP spCreditNoteMaster = new CreditNoteMasterSP();
            DataTable dtblCreditNote = new DataTable();
            try
            {
                if (searchParam.formNo == "")
                {
                    strVoucherNo = "";
                }
                else
                {
                    strVoucherNo = searchParam.formNo;
                }
                dtblCreditNote = spCreditNoteMaster.CreditNoteRegisterSearch(strVoucherNo, searchParam.fromDate.ToString(), searchParam.toDate.ToString()); 

                return dtblCreditNote;
            }
            catch (Exception ex)
            {

            }

            return null;
        }

    }

    public class WaybillParam
    {
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string condition { get; set; }
        public decimal UserId { get; set; }
    }

    public class WaybillResonse
    {
        public decimal SalesOrderMasterId { get; set; }
        public string VoucherNo { get; set; }
        public string OrderDate { get; set; }
        public string AuthorizationDate { get; set; }
        public decimal AgentId { get; set; }
        public string AgentName { get; set; }
        public string Status { get; set; }
        
    }
}
