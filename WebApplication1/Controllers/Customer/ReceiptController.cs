﻿using MatApi.Models.Customer;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MatApi.Controllers.Customer
{
    public class ReceiptController : ApiController
    {
        decimal decReceiptVoucherTypeId = 5;
        DataTable dtblPartyBalance = new DataTable();//to store party balance entries while clicking btn_Save in Receipt voucher
        string tableName = "ReceiptMaster";//to get the table name in voucher type selection

        public ReceiptController()
        {

        }

        [HttpGet]
        public HttpResponseMessage GetLookups()
        {
            dynamic response = new ExpandoObject();
            var bankOrCash = new TransactionsGeneralFill().CashOrBankComboFill();
            var currency = new TransactionsGeneralFill().CurrencyComboFill();
            var customers = new AccountLedgerSP().AccountLedgerViewCustomerOnly();

            response.BankOrCash = bankOrCash;
            response.Currency = currency;
            response.Customers = customers;
            response.VoucherNo = generateVoucherNo();
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public DataTable GetCustomerInvoices(decimal ledgerId,string voucherNo)
        {
            return new PartyBalanceSP().PartyBalanceComboViewByLedgerId(ledgerId, "Cr", decReceiptVoucherTypeId, voucherNo);
        }

        public int SavePayment(ReceiptVM input)
        {
            bool isSaved = false;
            
            ReceiptMasterInfo InfoReceiptMaster = new ReceiptMasterInfo();
            ReceiptMasterSP SpReceiptMaster = new ReceiptMasterSP();
            ReceiptDetailsInfo InfoReceiptDetails = new ReceiptDetailsInfo();
            ReceiptDetailsSP SpReceiptDetails = new ReceiptDetailsSP();
            PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
            PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
            LedgerPostingInfo ledgerPostingInfo = new LedgerPostingInfo();

            input.ReceiptMasterInfo.Extra1 = string.Empty;
            input.ReceiptMasterInfo.Extra2 = string.Empty;
            input.ReceiptMasterInfo.FinancialYearId = PublicVariables._decCurrentFinancialYearId;            
            input.ReceiptMasterInfo.SuffixPrefixId = 0;
            input.ReceiptMasterInfo.VoucherTypeId = decReceiptVoucherTypeId;
            decimal decReceiptMasterId = SpReceiptMaster.ReceiptMasterAdd(input.ReceiptMasterInfo);
            if (decReceiptMasterId != 0)
            {
                isSaved = true;
                //if (dgvReceiptVoucher.Rows[0].Cells["dgvtxtChequeNo"].Value != null && dgvReceiptVoucher.Rows[0].Cells["dgvtxtChequeNo"].Value.ToString() != string.Empty && dgvReceiptVoucher.Rows.Count == 2)
                //{
                //    chequeNo = dgvReceiptVoucher.Rows[0].Cells["dgvtxtChequeNo"].Value.ToString();
                //}
                isSaved=MasterLedgerPosting(decReceiptMasterId,input.ReceiptMasterInfo);
                //if (dgvReceiptVoucher.Rows[0].Cells["dgvCmbWithHoldingTax"].Value != null
                //    && dgvReceiptVoucher.Rows[0].Cells["dgvCmbWithHoldingTax"].Value.ToString().Trim() != string.Empty
                //    && txtWithHoldingTax.Text != string.Empty && Convert.ToDecimal(txtWithHoldingTax.Text) > 0)
                //{
                //    decimal taxId = Convert.ToDecimal(dgvReceiptVoucher.Rows[0].Cells["dgvCmbWithHoldingTax"].Value);
                //    decimal ledgerId = dtblWithholdingTax.AsEnumerable().Where(i => i.Field<decimal>("taxId") == taxId)
                //    .Select(i => i.Field<decimal>("ledgerId")).FirstOrDefault();
                //    postLedgerForWithholdingTax(ledgerId);
                //    postWithholdingTaxDetails(true, false, false);
                //}
            }
            for (int i=0;i<input.ReceiptDetailsInfo.Count;i++)
            {
                input.ReceiptDetailsInfo[i].Extra1 = string.Empty;
                input.ReceiptDetailsInfo[i].Extra2 = string.Empty;
                input.ReceiptDetailsInfo[i].ProjectId = 0;
                input.ReceiptDetailsInfo[i].CategoryId = 0;
                input.ReceiptDetailsInfo[i].ExchangeRateId=1;
                input.ReceiptDetailsInfo[i].ReceiptMasterId = decReceiptMasterId;
                if(input.ReceiptDetailsInfo[i].ChequeNo==null || input.ReceiptDetailsInfo[i].ChequeNo=="")
                {
                    input.ReceiptDetailsInfo[i].ChequeDate = DateTime.Now;
                    input.ReceiptDetailsInfo[i].ChequeNo = "";
                }
                 
                decimal decReceiptDetailsId = SpReceiptDetails.ReceiptDetailsAdd(input.ReceiptDetailsInfo[i]);
                if (decReceiptDetailsId != 0)
                {
                    isSaved = true;
                    //for (int inJ = 0; inJ < inTableRowCount; inJ++)
                    //{
                    //    if (dgvReceiptVoucher.Rows[inI].Cells["dgvcmbAccountLedger"].Value.ToString() == dtblPartyBalance.Rows[inJ]["LedgerId"].ToString())
                    //    {
                    //        PartyBalanceAddOrEdit(inJ);
                    //    }
                    //}
                    for(int k=0;k<input.PartyBalanceInfo.Count;k++)
                    {
                        if(input.ReceiptDetailsInfo[i].LedgerId==input.PartyBalanceInfo[k].LedgerId)
                        {
                            isSaved=PartyBalanceAddOrEdit(k,input.PartyBalanceInfo,input.ReceiptMasterInfo);
                        }
                    }
                    //inB++;

                    isSaved=DetailsLedgerPosting(i, decReceiptDetailsId,input.ReceiptDetailsInfo[i].ChequeDate,input.ReceiptDetailsInfo[i].ChequeNo,input.ReceiptMasterInfo,input.ReceiptDetailsInfo[i], input.PartyBalanceInfo);
                }
                else
                {
                    isSaved = false;
                }
            }
            if(isSaved) //if nothing failed, return the next voucher number
            {
                return Convert.ToInt32(generateVoucherNo());
            }

            return 0;
        }

        public bool MasterLedgerPosting(decimal decReceiptMasterId,ReceiptMasterInfo input)
        {
            bool isSave = false;

            try
            {
                LedgerPostingInfo InfoLedgerPosting = new LedgerPostingInfo();
                LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
                ExchangeRateSP SpExchangRate = new ExchangeRateSP();
                InfoLedgerPosting.Debit = 0;
                InfoLedgerPosting.Credit = 0;
                //InfoLedgerPosting.DetailsId = 0;
                InfoLedgerPosting.Date = input.Date;
                InfoLedgerPosting.VoucherNo = input.VoucherNo;
                InfoLedgerPosting.VoucherTypeId = decReceiptVoucherTypeId;
                InfoLedgerPosting.InvoiceNo = input.InvoiceNo;
                InfoLedgerPosting.DetailsId = decReceiptMasterId;
                InfoLedgerPosting.Extra1 = string.Empty;
                InfoLedgerPosting.Extra2 = string.Empty;
                InfoLedgerPosting.ChequeNo = "";
                InfoLedgerPosting.LedgerId = input.LedgerId;
                InfoLedgerPosting.ChequeDate = DateTime.Now;
                InfoLedgerPosting.YearId= PublicVariables._decCurrentFinancialYearId;
                if(SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting) >0)
                {
                    isSave = true;
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RV21:" + ex.Message;
            }
            return isSave;
        }

        public bool PartyBalanceAddOrEdit(int inJ, List<PartyBalanceInfo> input,ReceiptMasterInfo receiptMaster)
        {
            bool isSave = false;

            try
            {
                int inTableRowCount = dtblPartyBalance.Rows.Count;
                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                PartyBalanceSP spPartyBalance = new PartyBalanceSP();
                InfopartyBalance.Debit = 0;
                InfopartyBalance.CreditPeriod = 0;
                InfopartyBalance.Date = receiptMaster.Date;
                InfopartyBalance.Credit = input[inJ].Credit;
                InfopartyBalance.ExchangeRateId = 1/*input[inJ].ExchangeRateId*/;
                InfopartyBalance.Extra1 = string.Empty;
                InfopartyBalance.Extra2 = string.Empty;
                InfopartyBalance.ExtraDate = DateTime.Now;
                InfopartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                InfopartyBalance.LedgerId = input[inJ].LedgerId;
                InfopartyBalance.ReferenceType = input[inJ].ReferenceType;
                if (input[inJ].ReferenceType == "New" || input[inJ].ReferenceType == "OnAccount")
                {
                    InfopartyBalance.AgainstInvoiceNo = "0";//dtblPartyBalance.Rows[inJ]["AgainstInvoiceNo"].ToString();
                    InfopartyBalance.AgainstVoucherNo = "0";//dtblPartyBalance.Rows[inJ]["AgainstVoucherNo"].ToString();
                    InfopartyBalance.AgainstVoucherTypeId = 0;// Convert.ToDecimal(dtblPartyBalance.Rows[inJ]["AgainstVoucherTypeId"].ToString());//decPaymentVoucherTypeId;
                    InfopartyBalance.VoucherTypeId = decReceiptVoucherTypeId;
                    InfopartyBalance.InvoiceNo = input[inJ].InvoiceNo;

                    InfopartyBalance.VoucherNo = input[inJ].VoucherNo;

                }
                else
                {
                    InfopartyBalance.ExchangeRateId = 1;
                    InfopartyBalance.AgainstInvoiceNo = input[inJ].AgainstInvoiceNo;

                    InfopartyBalance.AgainstVoucherNo = input[inJ].AgainstVoucherNo;

                    InfopartyBalance.AgainstVoucherTypeId = decReceiptVoucherTypeId;
                    InfopartyBalance.VoucherTypeId = input[inJ].VoucherTypeId;
                    InfopartyBalance.VoucherNo = input[inJ].VoucherNo;
                    InfopartyBalance.InvoiceNo = input[inJ].InvoiceNo;
                }
                if (input[inJ].PartyBalanceId.ToString() == "0")
                {
                    if(spPartyBalance.PartyBalanceAdd(InfopartyBalance)>0)
                    {
                        isSave = true;
                    }
                }
                else
                {
                    InfopartyBalance.PartyBalanceId = input[inJ].PartyBalanceId;
                    if(spPartyBalance.PartyBalanceEdit(InfopartyBalance)>0)
                    {
                        isSave = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RV10:" + ex.Message;
            }
            return isSave;
        }

        public bool DetailsLedgerPosting(int inA, decimal decreceiptDetailsId,DateTime chequeDate,string chequeNo,ReceiptMasterInfo mast, ReceiptDetailsInfo det,List<PartyBalanceInfo> partyBalanceDetails)
        {
            bool isSaved = false;
            LedgerPostingInfo InfoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
            ExchangeRateSP SpExchangRate = new ExchangeRateSP();
            SettingsSP spSettings = new SettingsSP();
            decimal decOldExchange = 0;
            decimal decNewExchangeRate = 0;
            decimal decNewExchangeRateId = 0;
            decimal decOldExchangeId = 0;
           // decConvertRate = 0;
            try
            {
                //if (!dgvReceiptVoucher.Rows[inA].Cells["dgvtxtAmount"].ReadOnly)
                //{

                    //decimal d = Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbCurrency"].Value.ToString());
                    //InfoLedgerPosting.LedgerId = Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbAccountLedger"].Value.ToString());

                    //decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbCurrency"].Value.ToString()));
                    //decAmount = Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvtxtAmount"].Value.ToString());
                    //decConvertRate = decAmount * decSelectedCurrencyRate;

                    //InfoLedgerPosting.Date = input.Date;
                    //InfoLedgerPosting.Debit = 0;
                    //InfoLedgerPosting.Credit = input.Credit;
                    //InfoLedgerPosting.DetailsId = decreceiptDetailsId;
                    //InfoLedgerPosting.Extra1 = string.Empty;
                    //InfoLedgerPosting.Extra2 = string.Empty;
                    //InfoLedgerPosting.InvoiceNo = input.InvoiceNo;
                    //if (dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value != null && dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value.ToString() != string.Empty)
                    //{
                    //    InfoLedgerPosting.ChequeNo = dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value.ToString();
                    //    if (dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value != null && dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value.ToString() != string.Empty)
                    //    {
                    //        InfoLedgerPosting.ChequeDate = Convert.ToDateTime(dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value.ToString());
                    //    }
                    //    else
                    //        InfoLedgerPosting.ChequeDate = DateTime.Now;
                    //}
                    //else
                    //{
                    //    InfoLedgerPosting.ChequeNo = string.Empty;
                    //    InfoLedgerPosting.ChequeDate = DateTime.Now;
                    //}


                    //InfoLedgerPosting.VoucherNo = strVoucherNo;

                    //InfoLedgerPosting.VoucherTypeId = decReceiptVoucherTypeId;
                    //InfoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    //SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
                //}
                //else
                //{
                    InfoLedgerPosting.Date = mast.Date;
                    InfoLedgerPosting.Extra1 = string.Empty;
                    InfoLedgerPosting.Extra2 = string.Empty;
                    InfoLedgerPosting.InvoiceNo = mast.InvoiceNo;
                    InfoLedgerPosting.VoucherTypeId = decReceiptVoucherTypeId;
                    InfoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    InfoLedgerPosting.Debit = 0;
                    InfoLedgerPosting.LedgerId = det.LedgerId;
                    InfoLedgerPosting.VoucherNo = mast.VoucherNo;
                    InfoLedgerPosting.DetailsId = decreceiptDetailsId;
                    if(chequeDate!=null && (chequeNo!="" || chequeNo!=null))
                    {
                        InfoLedgerPosting.ChequeDate = chequeDate;
                        InfoLedgerPosting.ChequeNo = chequeNo;
                    }                    
                    else
                    {
                        InfoLedgerPosting.ChequeNo = "";
                        InfoLedgerPosting.ChequeDate = DateTime.Now;
                    }

                    //foreach (DataRow dr in dtblPartyBalance.Rows)
                    //{
                    //    if (InfoLedgerPosting.LedgerId == Convert.ToDecimal(dr["LedgerId"].ToString()))
                    //    {
                    //        decOldExchange = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                    //        decNewExchangeRateId = Convert.ToDecimal(dr["CurrencyId"].ToString());
                    //        decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchange);
                    //        decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                    //        //decConvertRate = decConvertRate + (decAmount * decSelectedCurrencyRate);      //old implementation takes continual sum of all rows rather than for each row 20161228
                    //        decConvertRate = (decAmount * decSelectedCurrencyRate);
                    //    }
                    //}
                    InfoLedgerPosting.Credit = partyBalanceDetails[inA].Credit;
                    if(SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting)>0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }

                    InfoLedgerPosting.LedgerId = 12;
                    InfoLedgerPosting.DetailsId = 0;
                    //foreach (DataRow dr in dtblPartyBalance.Rows)
                    for(int v=0;v< partyBalanceDetails.Count;v++)
                    {
                        //if (Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbAccountLedger"].Value.ToString()) == Convert.ToDecimal(dr["LedgerId"].ToString()))
                        if(partyBalanceDetails[v].ReferenceType=="Against")
                        {
                            if (partyBalanceDetails[inA].Credit>=0)
                            {

                                InfoLedgerPosting.Credit = partyBalanceDetails[inA].Credit;
                                InfoLedgerPosting.Debit = 0;
                            }
                            else
                            {
                                InfoLedgerPosting.Debit = -1 * partyBalanceDetails[inA].Credit;
                                InfoLedgerPosting.Credit = 0;
                            }
                            if(SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting)>0)
                            {
                                isSaved = true;
                            }
                                else
                            {
                                isSaved = false;
                            }
                            //if (dr["ReferenceType"].ToString() == "Against")
                            //{
                            //    decNewExchangeRateId = Convert.ToDecimal(dr["CurrencyId"].ToString());
                            //    decNewExchangeRate = SpExchangRate.GetExchangeRateByExchangeRateId(decNewExchangeRateId);
                            //    decOldExchangeId = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                            //    decOldExchange = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchangeId);
                            //    decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                            //    decimal decForexAmount = (decAmount * decNewExchangeRate) - (decAmount * decOldExchange);
                            //    if (decForexAmount >= 0)
                            //    {

                            //        InfoLedgerPosting.Credit = decForexAmount;
                            //        InfoLedgerPosting.Debit = 0;
                            //    }
                            //    else
                            //    {
                            //        InfoLedgerPosting.Debit = -1 * decForexAmount;
                            //        InfoLedgerPosting.Credit = 0;
                            //    }
                            //    SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
                            //}
                        }

                    }


               // }

            }
            catch (Exception ex)
            {
               // formMDI.infoError.ErrorString = "RV23:" + ex.Message;
            }
            return isSaved;
        }

        private string generateVoucherNo()
        {
            SalaryVoucherMasterSP spMaster = new SalaryVoucherMasterSP();
            ReceiptMasterSP SpReceiptMaster = new ReceiptMasterSP();
            TransactionsGeneralFill obj = new TransactionsGeneralFill();

            string strVoucherNo = "";
            if (strVoucherNo == string.Empty)
            {
                strVoucherNo = "0";
            }
            strVoucherNo = obj.VoucherNumberAutomaicGeneration(decReceiptVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
            if (Convert.ToDecimal(strVoucherNo) != SpReceiptMaster.ReceiptMasterGetMax(decReceiptVoucherTypeId) + 1)
            {
                strVoucherNo = SpReceiptMaster.ReceiptMasterGetMax(decReceiptVoucherTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decReceiptVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                if (SpReceiptMaster.ReceiptMasterGetMax(decReceiptVoucherTypeId) == 0)
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decReceiptVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                }
            }

            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decReceiptVoucherTypeId, DateTime.Now);
            string strPrefix = infoSuffixPrefix.Prefix;
            string strSuffix = infoSuffixPrefix.Suffix;
            string strInvoiceNo = strPrefix + strVoucherNo + strSuffix;
            return strInvoiceNo;
        }
    }
    
}
