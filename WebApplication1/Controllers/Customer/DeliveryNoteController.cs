﻿using MatApi.Models;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DeliveryNoteController : ApiController
    {
        string strVoucherNo = string.Empty;//To save the automatically generated voucher number
        decimal decDeliveryNoteVoucherTypeId = 17;
        decimal decDeliveryNoteSuffixPrefixId = 0;//To store the SuffixPrefix Id of the selected voucher type
        decimal decDeliveryNoteMasterId = 0;
        decimal decDelivryNoteIdToEdit = 0;//To take the deliveryNoteMasterId coming from frmDeliveryNoteRegister and frmDeliveryNoteReport


        public DeliveryNoteController()
        {

        }
                
        public decimal SaveOrEditFunction(CreateDeliveryNoteVM input)
        {
            try
            {
                // to take assign voucher number in case automatic voucher numbering is set to off urefe 20161206
                if (input.DeliveryNoteNo.Trim() != string.Empty)
                {
                    strVoucherNo = input.DeliveryNoteNo;
                }
                decimal decProductId = 0;
                decimal decBatchId = 0;
                decimal decCalcQty = 0;
                SettingsSP spSettings = new SettingsSP();
                string strStatus = spSettings.SettingsStatusCheck("NegativeStockStatus");
                bool isNegativeLedger = false;
                StockPostingSP spStockPosting = new StockPostingSP();
                foreach(var lineItem in input.LineItems)
                {
                    if (lineItem.ProductId>0)
                    {
                        decProductId = Convert.ToDecimal(lineItem.ProductId);

                        if (lineItem.BatchId>0)
                        {
                            decBatchId = Convert.ToDecimal(lineItem.BatchId);
                        }
                        decimal decCurrentStock = spStockPosting.StockCheckForProductSale(decProductId, decBatchId);
                        if (lineItem.Quantity>0)
                        {
                            decCalcQty = decCurrentStock - lineItem.Quantity;
                        }
                        if (decCalcQty < 0)
                        {
                            isNegativeLedger = true;
                            break;
                        }
                    }
                }
                if (isNegativeLedger)
                {
                    if (strStatus == "Warn")
                    {
                        //if (MessageBox.Show("Negative Stock balance exists,Do you want to Continue", "MAT Financials", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                        //{
                        //    SaveOrEdit();
                        //}
                        return SaveOrEdit(input);
                    }
                    else if (strStatus == "Block")
                    {
                        //MessageBox.Show("Cannot continue ,due to negative stock balance", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                        // Clear();
                    }
                    else
                    {
                        return SaveOrEdit(input);
                    }
                }
                else
                {
                    return SaveOrEdit(input);
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "DN46:" + ex.Message;
            }
            return 0;
        }

        public decimal SaveOrEdit(CreateDeliveryNoteVM input)
        {
            DeliveryNoteMasterSP spDeliveryNoteMaster = new DeliveryNoteMasterSP();
            string strDeliveryModeText = string.Empty;
            //input.DeliveryMode= strDeliveryModeText;
            return SaveFunction(input);
        }

        public decimal SaveFunction(CreateDeliveryNoteVM input)
        {
            bool isSaved = false;
            decimal returnValue = 0;
            try
            {
                DeliveryNoteMasterInfo infoDeliveryNoteMaster = new DeliveryNoteMasterInfo();
                DeliveryNoteDetailsInfo infoDeliveryNoteDetails = new DeliveryNoteDetailsInfo();
                StockPostingInfo infoStockPosting = new StockPostingInfo();
                StockPostingSP spStockPosting = new StockPostingSP();
                DeliveryNoteMasterSP spDeliveryNoteMaster = new DeliveryNoteMasterSP();
                DeliveryNoteDetailsSP spDeliveryNoteDetails = new DeliveryNoteDetailsSP();
                SalesOrderMasterInfo infoSalesOrderMaster = new SalesOrderMasterInfo();
                SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
                SalesQuotationMasterInfo infoSalesQuotationMaster = new SalesQuotationMasterInfo();
                SalesQuotationMasterSP spSalesQuotationMaster = new SalesQuotationMasterSP();

                //check if invoice number entered already exists
                string query = string.Format("SELECT voucherNo FROM tbl_DeliveryNoteMaster WHERE voucherNo='{0}' AND voucherTypeId=17", input.DeliveryNoteNo);
                if (new DBMatConnection().IsFormNumberExist(query))
                {
                    returnValue = -1;
                }
                else
                {
                    infoDeliveryNoteMaster.InvoiceNo = input.DeliveryNoteNo;
                    infoDeliveryNoteMaster.VoucherTypeId = decDeliveryNoteVoucherTypeId;
                    infoDeliveryNoteMaster.Date = /*Convert.ToDateTime(input.Date)*/DateTime.Now.Date;
                    infoDeliveryNoteMaster.LedgerId = Convert.ToDecimal(input.CustomerId);
                    infoDeliveryNoteMaster.VoucherNo = input.DeliveryNoteNo;
                    infoDeliveryNoteMaster.SuffixPrefixId = decDeliveryNoteSuffixPrefixId;

                    if (input.OrderNo != "")
                    {
                        if (input.DeliveryMode == "Against Order")
                        {
                            infoDeliveryNoteMaster.OrderMasterId = Convert.ToDecimal(input.SalesOrderMasterId);
                            infoSalesOrderMaster = spSalesOrderMaster.SalesOrderMasterView(infoDeliveryNoteMaster.OrderMasterId);
                        }
                        else if (input.DeliveryMode == "Against Quotation")
                        {
                            infoDeliveryNoteMaster.QuotationMasterId = Convert.ToDecimal(input.OrderNo);
                            infoSalesQuotationMaster = spSalesQuotationMaster.SalesQuotationMasterView(infoDeliveryNoteMaster.QuotationMasterId);
                        }
                        else if (input.DeliveryMode == "NA")
                        {
                            infoDeliveryNoteMaster.OrderMasterId = 0;
                            infoDeliveryNoteMaster.QuotationMasterId = 0;
                        }
                    }
                    else
                    {
                        infoDeliveryNoteMaster.OrderMasterId = 0;
                        infoDeliveryNoteMaster.QuotationMasterId = 0;
                    }
                    infoDeliveryNoteMaster.PricinglevelId = input.PricingLevelId;
                    if (input.SalesManId > 0)
                    {
                        infoDeliveryNoteMaster.EmployeeId = input.SalesManId;
                    }
                    else
                    {
                        infoDeliveryNoteMaster.EmployeeId = 0;
                    }
                    infoDeliveryNoteMaster.Narration = input.Narration;
                    infoDeliveryNoteMaster.TotalAmount = Convert.ToDecimal(input.TotalAmount);
                    infoDeliveryNoteMaster.UserId = input.UserId/*MATFinancials.PublicVariables._decCurrentUserId*/;
                    infoDeliveryNoteMaster.LrNo = input.LrNo;
                    infoDeliveryNoteMaster.TransportationCompany = input.TransportaionCompany;
                    infoDeliveryNoteMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoDeliveryNoteMaster.Extra1 = string.Empty;
                    infoDeliveryNoteMaster.Extra2 = string.Empty;
                    infoDeliveryNoteMaster.ExchangeRateId = input.CurrencyId;
                    decDeliveryNoteMasterId = Convert.ToDecimal(spDeliveryNoteMaster.DeliveryNoteMasterAdd(infoDeliveryNoteMaster));
                    if (decDeliveryNoteMasterId > 0)
                    {
                        isSaved = true;
                        //update status to processed
                        DBMatConnection db = new DBMatConnection();
                        string updateQuery = string.Format("UPDATE tbl_DeliveryNoteMaster SET status='Unprocessed' WHERE deliverynotemasterid={0}",decDeliveryNoteMasterId);
                        db.customUpdateQuery(updateQuery);

                        foreach (var lineItem in input.LineItems)
                        {
                            if (input.OrderNo != string.Empty)
                            {
                                if (input.DeliveryMode == "Against Order")
                                {
                                    infoDeliveryNoteDetails.OrderDetails1Id = lineItem.SalesOrderDetailsId;
                                }
                                else if (input.DeliveryMode == "Against Quotation")
                                {
                                    infoDeliveryNoteDetails.QuotationDetails1Id = lineItem.SalesOrderDetailsId;
                                }
                                else if (input.DeliveryMode == "NA")
                                {
                                    infoDeliveryNoteDetails.OrderDetails1Id = 0;
                                    infoDeliveryNoteDetails.QuotationDetails1Id = 0;
                                }
                            }
                            else
                            {
                                infoDeliveryNoteDetails.OrderDetails1Id = 0;
                                infoDeliveryNoteDetails.QuotationDetails1Id = 0;
                            }
                            infoDeliveryNoteDetails.ProductId = lineItem.ProductId;
                            infoDeliveryNoteDetails.Qty = lineItem.Quantity;
                            infoDeliveryNoteDetails.Rate = lineItem.Rate;
                            infoDeliveryNoteDetails.UnitId = lineItem.UnitId;
                            infoDeliveryNoteDetails.Amount = lineItem.Rate * lineItem.Quantity;
                            infoDeliveryNoteDetails.UnitConversionId = new UnitConvertionSP().UnitconversionIdViewByUnitIdAndProductId(lineItem.UnitId, lineItem.ProductId);
                            infoDeliveryNoteDetails.RackId = lineItem.RackId;
                            infoDeliveryNoteDetails.BatchId = new BatchSP().BatchIdViewByProductId(lineItem.ProductId);
                            infoDeliveryNoteDetails.GodownId = lineItem.StoreId;
                            infoDeliveryNoteDetails.SlNo = Convert.ToInt32(lineItem.SL);
                            infoDeliveryNoteDetails.Extra1 = string.Empty;
                            infoDeliveryNoteDetails.Extra2 = string.Empty;
                            infoDeliveryNoteDetails.ProjectId = 0;
                            infoDeliveryNoteDetails.CategoryId = 0;
                            infoDeliveryNoteDetails.itemDescription = lineItem.Description;
                            infoDeliveryNoteDetails.DeliveryNoteMasterId = decDeliveryNoteMasterId;
                            if (spDeliveryNoteDetails.DeliveryNoteDetailsAdd(infoDeliveryNoteDetails) > 0)
                            {
                                isSaved = true;
                            }
                            else
                            {
                                isSaved = false;
                            }

                            infoStockPosting.Date = Convert.ToDateTime(input.Date);
                            infoStockPosting.InvoiceNo = input.DeliveryNoteNo;
                            infoStockPosting.VoucherNo = strVoucherNo;
                            infoStockPosting.VoucherTypeId = decDeliveryNoteVoucherTypeId;
                            infoStockPosting.AgainstVoucherTypeId = 0;
                            infoStockPosting.AgainstVoucherNo = "NA";
                            infoStockPosting.AgainstInvoiceNo = "NA";
                            infoStockPosting.ProductId = infoDeliveryNoteDetails.ProductId;
                            infoStockPosting.BatchId = infoDeliveryNoteDetails.BatchId;
                            infoStockPosting.UnitId = infoDeliveryNoteDetails.UnitId;
                            infoStockPosting.GodownId = infoDeliveryNoteDetails.GodownId;
                            infoStockPosting.RackId = infoDeliveryNoteDetails.RackId;
                            infoStockPosting.OutwardQty = infoDeliveryNoteDetails.Qty;
                            infoStockPosting.Rate = infoDeliveryNoteDetails.Rate;
                            infoStockPosting.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                            infoStockPosting.Extra1 = string.Empty;
                            infoStockPosting.Extra2 = string.Empty;
                            if (spStockPosting.StockPostingAdd(infoStockPosting) > 0)
                            {
                                isSaved = true;
                            }
                            else
                            {
                                isSaved = false;
                            }
                        }
                    }
                    else
                    {
                        return 0;
                    }
                }
                
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "DN35:" + ex.Message;
            }
            if(isSaved)
            {
                return decDeliveryNoteMasterId;
            }
            else
            {
                return returnValue;
            }
        }

        [HttpGet]
        public HttpResponseMessage InvoiceLookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var customers = transactionGeneralFillObj.CashOrPartyUnderSundryDrComboFill();
            var salesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();
            var pricingLevels = transactionGeneralFillObj.PricingLevelViewAll();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            dynamic response = new ExpandoObject();
            response.Customers = customers;
            response.SalesMen = salesMen;
            response.PricingLevels = pricingLevels;
            response.Currencies = currencies;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage SearchProduct(string filter, string searchBy)
        {
            SalesDetailsSP spSalesDetails = new SalesDetailsSP();
            UnitSP spUnit = new UnitSP();
            GodownSP spGodown = new GodownSP();
            BatchSP spBatch = new BatchSP();
            TaxSP spTax = new TaxSP();
            RackSP spRack = new RackSP();
            dynamic response = new ExpandoObject();


            if (searchBy == "ProductCode")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByProductCodeForSI(28, filter);
                var units = spUnit.UnitViewAll();
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackViewAll();
                var batches = spBatch.BatchViewAll();
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Units = units;
                response.Stores = stores;
                response.Racks = racks;
                response.Batches = batches;
                response.Tax = taxes;
                response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productDetails.Rows[0].ItemArray[4].ToString()));
            }
            else if (searchBy == "ProductName")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByProductNameForSI(28, filter);
                var units = spUnit.UnitViewAll();
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackViewAll();
                var batches = spBatch.BatchViewAll();
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Units = units;
                response.Stores = stores;
                response.Racks = racks;
                response.Batches = batches;
                response.Tax = taxes;
                response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productDetails.Rows[0].ItemArray[4].ToString()));
            }
            else if (searchBy == "Barcode")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByBarcodeForSI(28, filter);
                var units = spUnit.UnitViewAll();
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackViewAll();
                var batches = spBatch.BatchViewAll();
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Units = units;
                response.Stores = stores;
                response.Racks = racks;
                response.Batches = batches;
                response.Tax = taxes;
                response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productDetails.Rows[0].ItemArray[4].ToString()));
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetSalesModeOrderNo(decimal ledgerId, decimal voucherTypeId, decimal salesInvoiceToEdit = 0)
        {
            SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
            DeliveryNoteMasterSP spDeliveryNoteMasterSp = new DeliveryNoteMasterSP();
            SalesQuotationMasterSP spSalesQuotationMasterSp = new SalesQuotationMasterSP();

            if (voucherTypeId == 16)
            {
                var salesOrders = spSalesOrderMaster.GetSalesOrderNoIncludePendingCorrespondingtoLedgerforSI(ledgerId, salesInvoiceToEdit, voucherTypeId);
                return Request.CreateResponse(HttpStatusCode.OK, salesOrders);
            }
            if (voucherTypeId == 17)
            {
                var deliveryNotes = spDeliveryNoteMasterSp.GetDeleveryNoteNoIncludePendingCorrespondingtoLedgerForSI(ledgerId, salesInvoiceToEdit, voucherTypeId);
                return Request.CreateResponse(HttpStatusCode.OK, deliveryNotes);
            }
            if (voucherTypeId == 15)
            {
                var salesQuotations = spSalesQuotationMasterSp.GetSalesQuotationIncludePendingCorrespondingtoLedgerForSI(ledgerId, salesInvoiceToEdit, voucherTypeId);
                return Request.CreateResponse(HttpStatusCode.OK, salesQuotations);
            }

            return Request.CreateResponse(HttpStatusCode.OK, "SALES_MODE_FAILURE");
        }

        [HttpGet]
        public HttpResponseMessage GetCurrencies(DateTime input)
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();
            var dt = transactionGeneralFillObj.CurrencyComboByDate(input);
            return Request.CreateResponse(HttpStatusCode.OK, dt);
        }

        [HttpGet]
        public HttpResponseMessage ItemUnits(decimal decProductId)
        {
            try
            {
                DataTable dtbl = new DataTable();
                UnitSP spUnit = new UnitSP();
                dtbl = spUnit.UnitViewAllByProductId(decProductId);
                return Request.CreateResponse(HttpStatusCode.OK, dtbl);
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, "GENERAL_FAILURE");
        }

        [HttpGet]
        public HttpResponseMessage ItemStores()
        {
            try
            {
                GodownSP spGodown = new GodownSP();
                DataTable dtblGodown = new DataTable();
                dtblGodown = spGodown.GodownViewAll();
                return Request.CreateResponse(HttpStatusCode.OK, dtblGodown);
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, "GENERAL_FAILURE");
        }

        [HttpGet]
        public HttpResponseMessage ItemStoreRacks(decimal storeId)
        {
            try
            {
                DataTable dtbl = new DataTable();
                RackSP spRack = new RackSP();
                dtbl = spRack.RackNamesCorrespondingToGodownId(storeId);
                return Request.CreateResponse(HttpStatusCode.OK, dtbl);
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, "GENERAL_FAILURE");
        }

        [HttpGet]
        public HttpResponseMessage ItemBatch(decimal decProductId)
        {
            try
            {
                DataTable dtbl = new DataTable();
                BatchSP spBatch = new BatchSP();
                dtbl = spBatch.BatchNamesCorrespondingToProduct(decProductId);
                return Request.CreateResponse(HttpStatusCode.OK, dtbl);
            }
            catch (Exception ex)
            {

            }
            return Request.CreateResponse(HttpStatusCode.OK, "GENERAL_FAILURE");
        }
        
        [HttpGet]
        public DataTable LineItemByDeliveryMode(string deliveryMode,decimal customerId,decimal currentUserId)
        {
            DataTable lineItem = null;
            DataTable resp = new DataTable();
            if (deliveryMode=="Against Order")
            {
                lineItem = new SalesOrderMasterSP().GetSalesOrderNoIncludePendingCorrespondingtoLedgerforDN(customerId, 10030, decDelivryNoteIdToEdit);

                
                resp.Columns.Add("salesOrderMasterId", typeof(decimal));
                resp.Columns.Add("invoiceNo", typeof(string));

                //restricting order numbers to only orders for the store of current logged in user
                foreach (DataRow dt in lineItem.Rows)
                {
                    var quotationDetails = new SalesOrderDetailsSP().SalesOrderDetailsViewByMasterId(Convert.ToDecimal(dt.ItemArray[0]));
                    string itemsStoreId = quotationDetails.Rows[0][21].ToString();   //store of the quotation items, first one is picked since
                                                                                     //it's same store across all items
                                                                                     //var quotationUser = new UserSP().UserView(quotationDetails.userId);
                    var currentUser = new UserSP().UserView(currentUserId);
                    string query = "SELECT RoleId FROM tbl_CycleActionPriviledge WHERE CycleAction='Stock Officer'";
                    var roleId = new DBMatConnection().getSingleValue(query);
                    if (roleId != null)
                    {
                        if(roleId=="1")
                        {
                            resp.Rows.Add(new Object[] {
                                            dt.ItemArray[0],
                                            dt.ItemArray[1]
                                            });
                        }
                        else
                        {
                            if (itemsStoreId == currentUser.StoreId && Convert.ToDecimal(roleId) == currentUser.RoleId)
                            {
                                resp.Rows.Add(new Object[] {
                                            dt.ItemArray[0],
                                            dt.ItemArray[1]
                                            });
                            }
                        }
                        
                    }
                    //resp.Rows.Add(new Object[] {
                    //            dt.ItemArray[0],
                    //            dt.ItemArray[1]
                    //            });
                }
            }
            else if(deliveryMode=="Against Quotation")
            {
                //filter this later
                lineItem= new SalesQuotationMasterSP().GetSalesQuotationNumberCorrespondingToLedgerForDN(customerId, 10031, decDelivryNoteIdToEdit);
            }         
                      

            return resp;
        }

        [HttpGet]
        public HttpResponseMessage PopulateLineItems(string deliveryMode,decimal orderNo)
        {
            dynamic response = new ExpandoObject();
            if (deliveryMode== "Against Order")
            {
                DataTable dtblMaster = new SalesOrderMasterSP().SalesOrderMasterViewBySalesOrderMasterId(orderNo);
                DataTable dtblDetails = new SalesOrderDetailsSP().SalesOrderDetailsViewBySalesOrderMasterIdWithRemaining(orderNo, 0);
                foreach (DataRow row in dtblDetails.Rows)
                {
                    row["extra2"] = getSalesOrderQuantityRequested(Convert.ToDecimal(row["salesOrderDetailsId"]));
                }
                response.Master = dtblMaster;
                response.Details = dtblDetails;
                response.Units = new UnitSP().UnitViewAll();
                response.Stores = new GodownSP().GodownViewAll();
                response.Racks = new RackSP().RackViewAll();
                response.Batches = new BatchSP().BatchViewAll();
                response.Taxes = new TaxSP().TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
            }
            else if (deliveryMode == "Against Quotation")
            {
                DataTable dtblMaster = new SalesQuotationMasterSP().SalesQuotationMasterViewByQuotationMasterId(orderNo);
                DataTable dtblQuotationDetails =  new SalesQuotationDetailsSP().SalesQuotationDetailsViewByquotationMasterIdWithRemainingByNotInCurrDN(orderNo, 0);
                response.Master = dtblMaster;
                response.Details = dtblQuotationDetails;
                response.Units = new UnitSP().UnitViewAll();
                response.Stores = new GodownSP().GodownViewAll();
                response.Racks = new RackSP().RackViewAll();
                response.Batches = new BatchSP().BatchViewAll();
                response.Taxes = new TaxSP().TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
            }
            
            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }

        private string getGodownIdForQuotationItem(decimal quotationDetailsId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("SELECT tbl_salesQuotationDetails.extra1,tbl_salesOrderDetails.quotationDetailsId,tbl_salesQuotationDetails.quotationDetailsId from tbl_salesQuotationDetails,tbl_salesOrderDetails where tbl_salesOrderDetails.quotationDetailsId=tbl_salesQuotationDetails.quotationDetailsId AND tbl_salesOrderDetails.salesOrderDetailsId= {0}"
                , quotationDetailsId);
            return conn.getSingleValue(queryStr);
        }
        private decimal getSalesOrderQuantityRequested(decimal salesOrderDetailsId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("SELECT qty from tbl_salesorderdetails WHERE salesorderdetailsid={0}", salesOrderDetailsId);
            return Convert.ToDecimal(conn.getSingleValue(queryStr));
        }
        private decimal getQuantityInStock(decimal productId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format(" SELECT  convert(decimal(18, 2), (ISNULL(SUM(s.inwardQty), 0) - ISNULL(SUM(s.outwardQty), 0))) AS Currentstock " +
                " FROM tbl_StockPosting s inner join tbl_product p on p.productId = s.productId " +
                " INNER JOIN tbl_Batch b on s.batchId = b.batchId " +
                " WHERE p.productId = '{0}' AND s.date <= '{1}' ", productId, MATFinancials.PublicVariables._dtToDate);
            return Convert.ToDecimal(conn.getSingleValue(queryStr));
        }

        [HttpGet]
        public HttpResponseMessage GetReleaseFormTransaction(decimal deliveryNoteMasterId)
        {
            dynamic response = new ExpandoObject();
            var master = new DeliveryNoteMasterSP().DeliveryNoteMasterView(deliveryNoteMasterId);
            var details = new DeliveryNoteDetailsSP().DeliveryNoteDetailsViewByDeliveryNoteMasterId(master.DeliveryNoteMasterId);
            response.Master = master;
            response.Details = details;
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public bool ProcessReleaseNote(decimal deliveryNoteMasterId)
        {
            DBMatConnection db = new DBMatConnection();
            string query = string.Format("UPDATE tbl_deliverynotemaster SET status='Processed' WHERE deliverynotemasterId={0}",deliveryNoteMasterId);
            return db.ExecuteNonQuery2(query);
        }

        [HttpPost]
        public List<WayBillListingResponse> GetDeliveryNotes(SearchDeliveryNote input)
        {
            List<WayBillListingResponse> response = new List<WayBillListingResponse>();
            var deliveryNotes = new DeliveryNoteMasterSP().DeliveryNoteMasterViewAll();
            foreach(DataRow row in deliveryNotes.Rows)
            {
                if(Convert.ToDateTime(row[5].ToString())>= input.FromDate && Convert.ToDateTime(row[5].ToString())<= input.ToDate
                    &&input.Status.ToLower()== row[21].ToString().ToLower())
                {
                    var userObj = new UserSP().UserView(Convert.ToDecimal(row[13].ToString()));
                    response.Add(new WayBillListingResponse
                    {
                        DeliverynoteMasterId = Convert.ToDecimal(row[0].ToString()),
                        Date = Convert.ToDateTime(row[5].ToString()).ToShortDateString(),
                        LedgerId = Convert.ToDecimal(row[6].ToString()),
                        LedgerName = new AccountLedgerSP().AccountLedgerView(Convert.ToDecimal(row[6].ToString())).LedgerName,
                        OrderNo = Convert.ToDecimal(row[2].ToString()),
                        Status = row[21].ToString(),
                        UserId = userObj.UserId,
                        User = userObj.FirstName + " " + userObj.LastName
                    });
                }
                
            }
            return response;
        }

        [HttpGet]
        public HttpResponseMessage GetDetailsForPrint(decimal salesOrderMasterId)
        {
            dynamic response = new ExpandoObject();
            var master = new SalesOrderMasterSP().SalesOrderMasterViewBySalesOrderMasterId(salesOrderMasterId);
            var details = new SalesOrderDetailsSP().SalesOrderDetailsViewByMasterId(salesOrderMasterId);
            var ledger = new AccountLedgerSP().AccountLedgerViewAll();

            response.Master = master;
            response.Details = details;
            response.ledger = ledger;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
    }

    public class SearchDeliveryNote
    {
        public string Status { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }
    public class WayBillListingResponse
    {
        public decimal DeliverynoteMasterId { get; set; }
        public decimal OrderNo { get; set; }
        public string Date { get; set; }
        public decimal LedgerId { get; set; }
        public string LedgerName { get; set; }
        public decimal UserId { get; set; }
        public string User { get; set; }
        public string Status { get; set; }
    }
}
