﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PosController : ApiController
    {
        public HttpResponseMessage GetLookups()
        {
            dynamic response = new ExpandoObject();

            var pricingLevels = new TransactionsGeneralFill().PricingLevelViewAll();
            var salesPoints= new CounterSP().CounterOnlyViewAll();
            var printers = new Object();
            var products = new ProductSP().ProductViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks= new RackSP().RackViewAll();
            var tax= new TaxSP().TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
            var bankLedger=new AccountLedgerSP().BankledgerComboFill();
            var customerOnly = new AccountLedgerSP().AccountLedgerViewCustomerOnly();
            var cash = new AccountLedgerSP().CashComboFill();

            response.PricingLevel = pricingLevels;
            response.SalesPoints = salesPoints;
            response.Printers = printers;
            response.Products = products;
            response.Stores = stores;
            response.Racks = racks;
            response.Tax = tax;
            response.BankLedger = bankLedger;
            response.CustomerOnly = customerOnly;
            response.Cash = cash;

            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }

        [HttpPost]
        public HttpResponseMessage SearchProduct(decimal productId)
        {
            dynamic response = new ExpandoObject();
            
            var product = new ProductSP().ProductView(productId);
            var batches=new BatchSP().BatchNoViewByProductId(productId);
            var units=new UnitSP().UnitViewAllByProductId(productId);

            response.Product = product;
            response.Batches = batches;
            response.Units = units;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
    }
}
