﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials;
using MatApi.Models.Register;
using System.Data;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PaymentsController : ApiController
    {
        public PaymentsController()
        {

        }

        [HttpGet]
        public DataTable GetSuppliers()
        {
            DataTable dtbl = new DataTable();
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            dtbl = obj.BankOrCashComboFill(false);
            return dtbl;
        }

        [HttpPost]
        public HttpResponseMessage Registers(PaymentRegisterVM input)
        {
            var resp = new PaymentMasterSP().PaymentMasterSearch(input.FromDate, input.ToDate, input.LedgerId, input.InvoiceNo);
            return Request.CreateResponse(HttpStatusCode.OK,(object)resp);
        }
    }
}
