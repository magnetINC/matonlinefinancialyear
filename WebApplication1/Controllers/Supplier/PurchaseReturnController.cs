﻿using MatApi.Models;
using MatApi.Models.Register;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PurchaseReturnController : ApiController
    {
        string strCashorParty = string.Empty;
        string strPurchaseAccount = string.Empty;
        string strProductCode = string.Empty;
        string strVoucherNo = string.Empty;
        string strTaxComboFill = string.Empty;
        string strInvoiceNo = string.Empty;
        string strReturnNo = string.Empty;
        bool isFromPurchaseAccountCombo = false;
        bool isFromCashOrPartyCombo = false;
        bool isFromPurchaseReturn = false;
        bool isDontExecuteCashorParty = false;
        bool isDontExecuteVoucherType = false;
        bool isAmountcalc = true;
        bool isAutomatic = false;
        bool isValueChanged = false;
        bool isInvoiceFil = false;
        bool isEditFill = false;
        decimal decPurchaseReturnVoucherTypeId = 14;
        decimal decPurchaseReturnSuffixPrefixId = 0;
        decimal decPurchaseReturnMasterId = 0;
        decimal decPurchaseReturnTypeId = 0;
        decimal decQty = 0;
        decimal decRate = 0;
        decimal decBatchId = 0;
        decimal decMasterId = -2;
        decimal decAgainstVoucherTypeId = 0;
        int inMaxCount = 0;
        int inNarrationCount = 0;
        PurchaseMasterInfo infoPurchaseMaster = new PurchaseMasterInfo();
        PurchaseReturnMasterInfo infoPurchaseReturnMaster = new PurchaseReturnMasterInfo();
        public PurchaseReturnController()
        {

        }
        [HttpGet]
        public HttpResponseMessage InvoiceLookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var products = new ProductSP().ProductViewAll();
            var customers = transactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill();
            var salesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();
            var pricingLevels = transactionGeneralFillObj.PricingLevelViewAll();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            var applyOn = new PurchaseDetailsSP().VoucherTypeComboFillForPurchaseInvoice();
            var units = new UnitSP().UnitViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks = new RackSP().RackViewAll();
            var batches = new BatchSP().BatchViewAll();
            var tax = new TaxSP().TaxViewAll();

            dynamic response = new ExpandoObject();
            response.Products = products;
            response.Customers = customers;
            response.SalesMen = salesMen;
            response.PricingLevels = pricingLevels;
            response.Currencies = currencies;
            response.ApplyOn = applyOn;
            response.Units = units;
            response.Stores = stores;
            response.Racks = racks;
            response.Batches = batches;
            response.Tax = tax;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage InvoiceNoComboFill(decimal decLedger, decimal decvoucherTypeId)
        {
            DataTable dtblInvoiceNo = new DataTable();
            PurchaseMasterSP SPPurchaseMaster = new PurchaseMasterSP();

            dtblInvoiceNo = SPPurchaseMaster.GetInvoiceNoCorrespondingtoLedger(decLedger, 0, decvoucherTypeId);
            return Request.CreateResponse(HttpStatusCode.OK, (object)dtblInvoiceNo);
        }

        [HttpGet]
        public HttpResponseMessage GetPurchaseDetails(decimal purchaseMasterId)
        {
            var purchaseDetails = new PurchaseDetailsSP().PurchaseDetailsViewByPurchaseMasterIdWithRemaining(purchaseMasterId, decPurchaseReturnMasterId, 14);
            return Request.CreateResponse(HttpStatusCode.OK, (object)purchaseDetails);
        }

        [HttpGet]
        public DataTable GetInvoiceNumbers(decimal supplierId,decimal voucherTypeId)
        {
            decimal decLedgerId = 0;
            decimal decVoucherId = 0;
            try
            {
                DataTable dtbl = new DataTable();
                decLedgerId = (supplierId== 0 ) ? -1 : supplierId;
                decVoucherId = (voucherTypeId == 0) ? -1 : voucherTypeId;
                dtbl = new PurchaseReturnMasterSP().GetInvoiceNoCorrespondingtoLedgerForPurchaseReturnReport(decLedgerId, decVoucherId);
                if (dtbl != null)
                {
                    DataRow dr = dtbl.NewRow();
                    dr["purchaseMasterId"] = 0;
                    dr["invoiceNo"] = "All";
                    dtbl.Rows.InsertAt(dr, 0);                    
                }
                return dtbl;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        [HttpPost]
        public HttpResponseMessage Registers(PurchaseReturnRegisterVM input)
        {
            var resp =new PurchaseReturnMasterSP().PurchaseReturnRegisterFill(input.FromDate, input.ToDate, input.LedgerId, input.InvoiceNo, input.AgainstInvoiceNo, input.VoucherType);
            return Request.CreateResponse(HttpStatusCode.OK,(object)resp);
        }

        [HttpPost]
        public string Save(CreatePurchaseReturnVM input)
        {
            PurchaseMasterSP SPPurchaseMaster = new PurchaseMasterSP();
            PurchaseReturnMasterSP SPPurchaseReturnMaster = new PurchaseReturnMasterSP();
            PurchaseReturnDetailsSP SPPurchaseReturnDetails = new PurchaseReturnDetailsSP();
            PurchaseReturnDetailsInfo infoPurchaseReturnDetails = new PurchaseReturnDetailsInfo();
            StockPostingInfo infoStockPosting = new StockPostingInfo();
            StockPostingSP spStockPosting = new StockPostingSP();
            UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
            AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
            AccountLedgerSP spAccountLedger = new AccountLedgerSP();
            PurchaseReturnBilltaxInfo infoPurchaseReturnBillTax = new PurchaseReturnBilltaxInfo();
            PurchaseReturnBilltaxSP spPurchaseReturnBillTax = new PurchaseReturnBilltaxSP();
            SettingsSP spSettings = new SettingsSP();
            UnitSP spUnit = new UnitSP();
            DataTable dtblPurchaseMasterViewById = new DataTable();
            string strAgainstVoucherNo = string.Empty;
            string strAgainstInvoiceNo = string.Empty;
            decimal decPurchaseReturnMasterIds = 0;
            decimal decPurchaseMasterId = 0;
            decimal decDiscount = 0;
            decimal decExchangeRate = 0;
            decimal decDis = 0;

            try
            {
               
                    infoPurchaseReturnMaster.VoucherNo = input.ReturnNo;
                    infoPurchaseReturnMaster.InvoiceNo = input.ReturnNo;
                if (decPurchaseReturnVoucherTypeId != 0)
                {
                    infoPurchaseReturnMaster.VoucherTypeId = decPurchaseReturnVoucherTypeId;
                }
                infoPurchaseReturnMaster.SuffixPrefixId = (decPurchaseReturnSuffixPrefixId != 0) ? decPurchaseReturnSuffixPrefixId : 0;
                infoPurchaseReturnMaster.LedgerId = input.SupplierId;
                //infoPurchaseReturnMaster.PurchaseAccount = Convert.ToDecimal(cmbPurchaseAccount.SelectedValue.ToString());    // -- old implementation selects purchase account from dropdown -- //
                //if (decPurchaseReturnVoucherTypeId != 0)
                    //infoPurchaseReturnMaster.PurchaseAccount = spLedgerPosting.ProductExpenseAccountId();      // -- new implementation uses the product default/selected purchase account -- //
                if (input.SupplierId != 0 )
                {
                    infoPurchaseReturnMaster.PurchaseMasterId = input.PurchaseMasterId;
                    decPurchaseMasterId = input.PurchaseMasterId;
                }
                else
                {
                    infoPurchaseReturnMaster.PurchaseMasterId = 0;
                }
                infoPurchaseReturnMaster.ExchangeRateId = input.CurrencyId;
                infoPurchaseReturnMaster.Narration = input.Narration;
                infoPurchaseReturnMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                infoPurchaseReturnMaster.LrNo = input.LrNo;
                infoPurchaseReturnMaster.TransportationCompany = input.TransportationCompany;
                infoPurchaseReturnMaster.Date = input.Date;
                infoPurchaseReturnMaster.TotalAmount = input.NetAmount;
                infoPurchaseReturnMaster.TotalTax = input.TaxAmount;
                infoPurchaseReturnMaster.Discount = input.DiscountAmount;
                infoPurchaseReturnMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoPurchaseReturnMaster.Extra1 = string.Empty;
                infoPurchaseReturnMaster.Extra2 = string.Empty;
                infoPurchaseReturnMaster.ExtraDate = DateTime.Now;
                infoPurchaseReturnMaster.GrandTotal = input.TotalAmount;

                decPurchaseReturnMasterIds = SPPurchaseReturnMaster.PurchaseReturnMasterAddWithReturnIdentity(infoPurchaseReturnMaster);

                infoLedgerPosting.Date = infoPurchaseReturnMaster.Date;
                infoLedgerPosting.VoucherTypeId = infoPurchaseReturnMaster.VoucherTypeId;
                infoLedgerPosting.VoucherNo = infoPurchaseReturnMaster.VoucherNo;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.InvoiceNo = infoPurchaseReturnMaster.InvoiceNo;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;

                infoLedgerPosting.LedgerId = infoPurchaseReturnMaster.PurchaseAccount;
                infoLedgerPosting.Debit = 0;

                infoLedgerPosting.Credit = input.NetAmount * spExchangeRate.ExchangeRateViewByExchangeRateId(input.CurrencyId)  ;
                infoLedgerPosting.ExtraDate = DateTime.Now;

                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                infoLedgerPosting.LedgerId = infoPurchaseReturnMaster.LedgerId;
                infoLedgerPosting.Debit = input.GrandTotal * spExchangeRate.ExchangeRateViewByExchangeRateId(input.CurrencyId);
                infoLedgerPosting.ExtraDate = DateTime.Now;
                infoLedgerPosting.Credit = 0;
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                foreach (var prt in input.LineItems)
                {
                    if (prt.TaxId != 0)
                    {
                        infoLedgerPosting.LedgerId = input.SupplierId;
                        infoLedgerPosting.Credit = prt.Amount;
                        infoLedgerPosting.Debit = 0;
                        infoLedgerPosting.ExtraDate = DateTime.Now;
                        spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                    }
                }

                if (input.DiscountAmount != 0)
                {
                    decDis = input.DiscountAmount;
                }
                if (decDis >= 0)
                {
                    infoLedgerPosting.Debit = 0;
                    infoLedgerPosting.Credit = decDis;
                    infoLedgerPosting.LedgerId = 9;
                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
                
                foreach (var pr in input.LineItems)
                {
                    if (pr.ProductId != 0)
                    {
                        infoPurchaseReturnDetails.ExtraDate = DateTime.Now;
                        infoPurchaseReturnDetails.Extra1 = string.Empty;
                        infoPurchaseReturnDetails.Extra2 = string.Empty;
                        infoPurchaseReturnDetails.PurchaseReturnMasterId =  decPurchaseReturnMasterIds;
                        infoPurchaseReturnDetails.ProductId = pr.ProductId;
                        infoPurchaseReturnDetails.Qty = pr.Quantity;
                        infoPurchaseReturnDetails.Rate = pr.Rate;
                        infoPurchaseReturnDetails.UnitId = pr.UnitId;
                        infoPurchaseReturnDetails.UnitConversionId = SPUnitConversion.UnitconversionIdViewByUnitIdAndProductId(infoPurchaseReturnDetails.UnitId, infoPurchaseReturnDetails.ProductId);
                        }
                        infoPurchaseReturnDetails.Discount = pr.Discount;
                        if (pr.TaxId != 0)
                        {
                            infoPurchaseReturnDetails.TaxId = pr.TaxId;
                            if (strTaxComboFill != string.Empty)
                            {
                            infoPurchaseReturnDetails.TaxAmount = pr.taxAmount;
                            }
                        }
                        else
                        {
                            infoPurchaseReturnDetails.TaxId = 0;
                        }
                        if (pr.BatchId != 0)
                        {
                            infoPurchaseReturnDetails.BatchId = pr.BatchId;
                        }
                        else
                        {
                            infoPurchaseReturnDetails.GodownId = 0;
                        }
                        if (pr.GodownId != 0)
                        {
                            infoPurchaseReturnDetails.GodownId = pr.GodownId;
                        }
                        else
                        {
                            infoPurchaseReturnDetails.RackId = 0;
                        }
                        if (pr.RackId != 0)
                        {
                            infoPurchaseReturnDetails.RackId = pr.RackId;
                        }
                        infoPurchaseReturnDetails.GrossAmount = pr.GrossAmount;
                        infoPurchaseReturnDetails.NetAmount = pr.NetAmount;
                        infoPurchaseReturnDetails.Amount = pr.Amount;
                        infoPurchaseReturnDetails.SlNo = pr.SL;
                        if (pr.Projectid != 0)
                        {
                            infoPurchaseReturnDetails.ProjectId = pr.Projectid;
                        }
                        else
                        {
                            infoPurchaseReturnDetails.ProjectId = 0;
                        }
                        if (pr.CategoryId != 0)
                        {
                            infoPurchaseReturnDetails.CategoryId = pr.CategoryId;
                        }
                        else
                        {
                            infoPurchaseReturnDetails.CategoryId = 0;
                        }
                        if (pr.Description != null && pr.Description != string.Empty)
                        {
                            infoPurchaseReturnDetails.itemDescription = pr.Description;
                        }
                    infoPurchaseReturnDetails.PurchaseDetailsId = pr.PurchaseDetailsId;
                        if (pr.PurchaseReturnDetailId != 0)
                        {
                            if (pr.PurchaseReturnDetailId == 0)
                            {
                                SPPurchaseReturnDetails.PurchaseReturnDetailsAddWithReturnIdentity(infoPurchaseReturnDetails);
                            }
                            else
                            {
                                infoPurchaseReturnDetails.PurchaseReturnDetailsId = pr.PurchaseReturnDetailId;
                                SPPurchaseReturnDetails.PurchaseReturnDetailsEdit(infoPurchaseReturnDetails);
                            }
                        }
                        else
                        {
                            SPPurchaseReturnDetails.PurchaseReturnDetailsAddWithReturnIdentity(infoPurchaseReturnDetails);
                        }
                        
                        infoPurchaseMaster = SPPurchaseMaster.PurchaseMasterView(infoPurchaseReturnMaster.PurchaseMasterId);
                        infoStockPosting.Date = infoPurchaseReturnMaster.Date;
                        infoStockPosting.ProductId = infoPurchaseReturnDetails.ProductId;
                        infoStockPosting.BatchId = infoPurchaseReturnDetails.BatchId;
                        infoStockPosting.UnitId = infoPurchaseReturnDetails.UnitId;
                        infoStockPosting.GodownId = infoPurchaseReturnDetails.GodownId;
                        infoStockPosting.RackId = infoPurchaseReturnDetails.RackId;
                        decimal decConversionId = SPUnitConversion.UnitConversionRateByUnitConversionId(infoPurchaseReturnDetails.UnitConversionId);
                        //infoStockPosting.OutwardQty = infoPurchaseReturnDetails.Qty / (decConversionId == 0 ? 1 : decConversionId);
                        infoStockPosting.OutwardQty = infoPurchaseReturnDetails.Qty;
                        infoStockPosting.InwardQty = 0;
                        infoStockPosting.Rate = infoPurchaseReturnDetails.Rate;
                        infoStockPosting.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                        infoStockPosting.Extra1 = string.Empty;
                        infoStockPosting.Extra2 = string.Empty;
                        if (infoPurchaseReturnDetails.PurchaseDetailsId != 0)
                        {
                            infoStockPosting.AgainstVoucherTypeId = decPurchaseReturnVoucherTypeId; // infoPurchaseMaster.VoucherTypeId;
                            infoStockPosting.AgainstVoucherNo = pr.VoucherNo; // infoPurchaseMaster.VoucherNo;
                            infoStockPosting.AgainstInvoiceNo = pr.InvoiceNo; // infoPurchaseMaster.InvoiceNo;
                            infoStockPosting.VoucherNo = infoPurchaseMaster.VoucherNo; // strVoucherNo;
                            infoStockPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo; // txtReturnNo.Text.Trim();
                            infoStockPosting.VoucherTypeId = pr.VoucherTypeId; // decPurchaseReturnVoucherTypeId;
                            decAgainstVoucherTypeId = infoStockPosting.VoucherTypeId;
                        }
                        else
                        {
                            infoStockPosting.AgainstVoucherTypeId = 0;
                            infoStockPosting.AgainstVoucherNo = "NA";
                            infoStockPosting.AgainstInvoiceNo = "NA";
                            infoStockPosting.VoucherNo = infoPurchaseReturnMaster.VoucherNo;
                            infoStockPosting.InvoiceNo = infoPurchaseReturnMaster.InvoiceNo;
                            infoStockPosting.VoucherTypeId = decPurchaseReturnVoucherTypeId;
                            decAgainstVoucherTypeId = 0;
                        }
                        spStockPosting.StockPostingAdd(infoStockPosting);
                    }
                infoAccountLedger = spAccountLedger.AccountLedgerView(infoPurchaseReturnMaster.LedgerId);
                if (infoAccountLedger.BillByBill == true)
                {
                    infoPartyBalance.Date = infoPurchaseReturnMaster.Date;
                    infoPartyBalance.LedgerId = infoPurchaseReturnMaster.LedgerId;
                    if (decAgainstVoucherTypeId != 0)
                    {
                        infoPartyBalance.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                        infoPartyBalance.VoucherNo = infoPurchaseMaster.VoucherNo;
                        infoPartyBalance.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                        infoPartyBalance.AgainstVoucherTypeId = infoPurchaseReturnMaster.VoucherTypeId;
                        infoPartyBalance.AgainstVoucherNo = infoPurchaseReturnMaster.VoucherNo;
                        infoPartyBalance.AgainstInvoiceNo = infoPurchaseReturnMaster.InvoiceNo;
                        infoPartyBalance.ReferenceType = "Against";
                    }
                    else
                    {
                        infoPartyBalance.VoucherTypeId = infoPurchaseReturnMaster.VoucherTypeId;
                        infoPartyBalance.VoucherNo = infoPurchaseReturnMaster.VoucherNo;
                        infoPartyBalance.InvoiceNo = infoPurchaseReturnMaster.InvoiceNo;
                        infoPartyBalance.AgainstVoucherTypeId = 0;
                        infoPartyBalance.AgainstVoucherNo = "NA";
                        infoPartyBalance.AgainstInvoiceNo = "NA";
                        infoPartyBalance.ReferenceType = "New";
                    }
                    infoPartyBalance.Debit = infoPurchaseReturnMaster.TotalAmount;
                    infoPartyBalance.Credit = 0;
                    infoPartyBalance.CreditPeriod = 0;
                    infoPartyBalance.ExchangeRateId = infoPurchaseReturnMaster.ExchangeRateId;
                    infoPartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoPartyBalance.Extra1 = string.Empty;
                    infoPartyBalance.Extra2 = string.Empty;
                    spPartyBalance.PartyBalanceAdd(infoPartyBalance);
                }

                // delete previous tax that was entered
                DBMatConnection conn = new DBMatConnection();
                string queryStr = string.Format("select prb.purchaseReturnBillTaxId from tbl_PurchaseReturnBilltax prb WHERE prb.purchaseReturnMasterId = {0}", decPurchaseReturnMasterId);
                string returnedValue = conn.getSingleValue(queryStr);
                if (returnedValue != null && returnedValue != string.Empty && returnedValue != string.Empty)
                {
                    spPurchaseReturnBillTax.PurchaseReturnBilltaxDelete(Convert.ToDecimal(returnedValue));
                }
                foreach (var prt in input.LineItems)
                {
                    if (prt.TaxId != 0)
                    {
                        infoPurchaseReturnBillTax.PurchaseReturnMasterId = decPurchaseReturnMasterIds; // decPurchaseReturnMasterIds;
                        infoPurchaseReturnBillTax.TaxId = prt.TaxId;
                        infoPurchaseReturnBillTax.TaxAmount = prt.taxAmount;
                        infoPurchaseReturnBillTax.Extra1 = string.Empty;
                        infoPurchaseReturnBillTax.Extra2 = string.Empty;
                        spPurchaseReturnBillTax.PurchaseReturnBilltaxAdd(infoPurchaseReturnBillTax);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return "Purchase Return Saved";
        }

        public string VoucherNumberGeneration()
        {
            string strPrefix = string.Empty;
            string strSuffix = string.Empty;
            string tableName = "PurchaseReturnMaster";
            string strReturnNo = string.Empty;
            TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
            PurchaseReturnMasterSP SPPurchaseReturnMaster = new PurchaseReturnMasterSP();
            try
            {
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }
                strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(decPurchaseReturnVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                if (Convert.ToDecimal(strVoucherNo) != SPPurchaseReturnMaster.PurchaseReturnMasterGetMaxPlusOne(decPurchaseReturnVoucherTypeId))
                {
                    strVoucherNo = SPPurchaseReturnMaster.PurchaseReturnMasterGetMax(decPurchaseReturnVoucherTypeId).ToString();
                    strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(decPurchaseReturnVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                    if (SPPurchaseReturnMaster.PurchaseReturnMasterGetMax(decPurchaseReturnVoucherTypeId) == "0")
                    {
                        strVoucherNo = "0";
                        strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(decPurchaseReturnVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                    }
                }
                SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decPurchaseReturnVoucherTypeId, DateTime.Now);
                strPrefix = infoSuffixPrefix.Prefix;
                strSuffix = infoSuffixPrefix.Suffix;
                decPurchaseReturnSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                strReturnNo = strPrefix + strVoucherNo + strSuffix;
            }
            catch (Exception ex)
            {
            }
            return strReturnNo;
        }
        [HttpGet]
        public string GetAutoFormNo()
        {
            return VoucherNumberGeneration();
        }
    }
}
