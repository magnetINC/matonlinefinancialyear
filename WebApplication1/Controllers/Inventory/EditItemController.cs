﻿using MatApi.Models;
using MatApi.Models.Inventory;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory
{
    public class EditItemController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage GetLookups(int id)
        {
            dynamic response = new ExpandoObject();
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var size = new SizeSP().SizeViewAll();
            var units = new UnitSP().UnitViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks = new RackSP().RackViewAll();
            var brand = new BrandSP().BrandViewAll();
            var allTaxes = new TaxSP().TaxViewAll();
            var productGroup = new ProductGroupSP().ProductGroupViewAll();
        
            var expAccount = new AccountLedgerSP().AccountLedgerViewAll();
            decimal[] ledgers = new decimal[] { 11, 37 };
            var query = (from d in expAccount.AsEnumerable()
                             where ledgers.Contains(d.Field<decimal>("accountGroupId"))
                             select d).ToList();
            expAccount = query.CopyToDataTable();

            var salesAccounts = new SalesMasterSP().SalesInvoiceSalesAccountModeComboFill();
            var bom = new BomSP().ProduBomForEdit(id);
            var products = new ProductSP().ProductViewAll();
            var stockPosting = new StockPostingSP().StockPostingViewAll();
            var batch = new BatchSP().BatchViewAll();
            var modelNos = new ModelNoSP().ModelNoViewAll();

            response.Sizes = size;
            response.Units = units;
            response.Stores = stores;
            response.Racks = racks;
            response.Brands = brand;
            response.Taxes = allTaxes;
            response.ProductGroup = productGroup;
            response.ModelNos = modelNos;
            response.SalesAccount = salesAccounts;
            response.ExpenseAccount = expAccount;
            response.Bom = bom;
            response.Products = products;
            response.Stocks = stockPosting;
            response.Batch = batch;
            response.ModelNos = modelNos;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetProductDetails(int id)
        {
            var ProductDetails = new ProductSP().ProductView(id);

            return Request.CreateResponse(HttpStatusCode.OK, (object)ProductDetails);
        }

        [HttpPost]
        public string UpdateProductDetails(ItemModel input)
        {
            try
            {
                ProductSP prodSP = new ProductSP();
                input.ProductInfo.ExtraDate = DateTime.Now;
                var res = prodSP.ProductEdit(input.ProductInfo);

                BomSP bomSP = new BomSP();
                List<BomInfo> bomInfo = new List<BomInfo>();

                if (res == true)
                {
                    foreach (var o in input.NewBoms)
                    {
                        bomInfo.Add
                            (new BomInfo
                            {
                                Extra1 = o.Extra1,
                                Extra2 = o.Extra2,
                                ExtraDate = DateTime.Now,
                                Quantity = o.Quantity,
                                RowmaterialId = o.RawMaterialId,
                                UnitId = o.UnitId,
                                ProductId = input.ProductInfo.ProductId,
                                BomId = o.BomId
                            });
                    }
                    var boms = bomSP.ProduBomForEdit(input.ProductInfo.ProductId);
                    for (int i = 0; i < boms.Rows.Count; i++)
                    {
                        var exist = bomInfo.Find(p => p.ProductId == Convert.ToDecimal(boms.Rows[i].ItemArray[1]));

                        if (exist == null)
                        {
                            bomSP.BomDelete(Convert.ToDecimal(boms.Rows[i].ItemArray[0]));
                        }
                    }
                    foreach (var o in bomInfo)
                    {

                        if (o.BomId > 0)
                        {
                            bomSP.BomEdit(o);
                        }
                        else if (o.BomId == 0)
                        {
                            o.ExtraDate = DateTime.Now;
                            bomSP.BomAdd(o);
                        }
                    }
                }
                else
                {
                    return "failed";
                }
            }
            catch (Exception ex)
            {

            }
            return "";
        }
        [HttpGet]
        public string DeleteItem(decimal id)
        {
            ProductSP ProductDetails = new ProductSP();
            var res = ProductDetails.ProductReferenceCheck(id);
            var str = "";
            if(res == false)
            {
                var delres = ProductDetails.ProductDelete(id);

                if(delres == true)
                {
                    str = "Item Deleted";
                }
                else
                {
                    str = "A problem occured";
                }
            }
            else
            {
                str = "Unable to delete. Item contains reference.";
            }

            return str;
        }
    }
}
