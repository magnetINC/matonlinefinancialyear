﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory.ItemComponent
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BatchController : ApiController
    {
        BatchSP batchSp;
        public BatchController()
        {
            batchSp = new BatchSP();
        }

        public List<BatchInfo> GetBatches()
        {
            var batchesDt = batchSp.BatchViewAll();
            List<BatchInfo> batches = new List<BatchInfo>();
            foreach (DataRow row in batchesDt.Rows)
            {
                batches.Add(new BatchInfo
                {
                    BatchId = Convert.ToDecimal(row["BatchId"]),
                    BatchNo = row["BatchNo"].ToString(),
                    ProductId = Convert.ToDecimal(row["ProductId"]),
                    ManufacturingDate = Convert.ToDateTime(row["ManufacturingDate"]),
                    ExpiryDate = Convert.ToDateTime(row["ExpiryDate"])
                });
            }
            return batches;
        }

        public BatchInfo GetBatch(decimal batchId)
        {
            var batchDt = batchSp.BatchView(batchId);
            BatchInfo batch = new BatchInfo
            {
                BatchId = Convert.ToDecimal(batchDt.BatchId),
                BatchNo = batchDt.BatchNo.ToString(),
                ProductId = Convert.ToDecimal(batchDt.ProductId),
                ManufacturingDate = Convert.ToDateTime(batchDt.ManufacturingDate),
                ExpiryDate = Convert.ToDateTime(batchDt.ExpiryDate)
            };
            return batch;
        }

        [HttpPost]
        public bool DeleteBatch(BatchInfo batch)
        {
            if (batchSp.BatchDelete(batch.BatchId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddBatch(BatchInfo batch)
        {
            batch.Extra1 = "";
            batch.Extra2 = "";
            batch.narration = "";
            batch.partNo = "";
            batch.barcode = "";
            batch.ExtraDate = DateTime.Now;
            if (batchSp.BatchAdd(batch) > 0)
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditBatch(BatchInfo batch)
        {
            batch.Extra1 = "";
            batch.Extra2 = "";
            batch.narration = "";
            batch.partNo = "";
            batch.barcode = "";
            batch.ExtraDate = DateTime.Now;
            return batchSp.BatchEdit(batch);
        }
    }
}
