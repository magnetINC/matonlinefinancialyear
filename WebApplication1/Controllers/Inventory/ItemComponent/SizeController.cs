﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory.ItemComponent
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SizeController : ApiController
    {
        SizeSP sizeSp;
        public SizeController()
        {
            sizeSp = new SizeSP();
        }

        public List<SizeInfo> GetSizes()
        {
            var sizesDt = sizeSp.SizeViewAll();
            List<SizeInfo> sizes = new List<SizeInfo>();
            foreach (DataRow row in sizesDt.Rows)
            {
                sizes.Add(new SizeInfo
                {
                    SizeId = Convert.ToDecimal(row["SizeId"]),
                    Size = row["Size"].ToString(),
                    Extra1 = row["Extra1"].ToString(),
                    Extra2 = row["Extra2"].ToString(),
                    Narration = row["Narration"].ToString()
                });
            }
            return sizes;
        }

        public SizeInfo GetSize(decimal sizeId)
        {
            var sizeDt = sizeSp.SizeViewing(sizeId);
            SizeInfo size = new SizeInfo
            {
                SizeId = Convert.ToDecimal(sizeDt.SizeId),
                Size = sizeDt.Size,
                Extra1 = sizeDt.Extra1,
                Extra2 = sizeDt.Extra1,
                Narration = sizeDt.Narration
            };
            return size;
        }

        [HttpPost]
        public bool DeleteSize(SizeInfo size)
        {
            if (new SizeSP().SizeDeleting(size.SizeId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddSize(SizeInfo size)
        {
            size.Extra1 = "";
            size.Extra2 = "";
            size.Narration = "";
            size.ExtraDate = DateTime.Now;
            if (sizeSp.SizeAdding(size) > 0)
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditSize(SizeInfo size)
        {
            size.Extra1 = "";
            size.Extra2 = "";
            size.Narration = "";
            size.ExtraDate = DateTime.Now;
            return sizeSp.SizeEditing(size);
        }
    }
}
