﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ItemGroupController : ApiController
    {
        ProductGroupSP productGroupSp;
        public ItemGroupController()
        {
            productGroupSp = new ProductGroupSP();
        }

        public List<ProductGroupInfo> GetProductGroups()
        {
            var productGroupsDt = productGroupSp.ProductGroupViewAll();
            //var productGroupsDt = productGroupSp.ProductGroupViewForComboFillForProductGroup();
            List<ProductGroupInfo> productGroups = new List<ProductGroupInfo>();
            foreach (DataRow row in productGroupsDt.Rows)
            {
                //if (Convert.ToDecimal(row["GroupId"])==1)
                //{
                //    continue;   //don't show the first group
                //}
                //else
                //{
                //    productGroups.Add(new ProductGroupInfo
                //    {
                //        GroupId = Convert.ToDecimal(row["GroupId"]),
                //        GroupName = row["GroupName"].ToString(),
                //        GroupUnder = Convert.ToDecimal(row["narration"].ToString())
                //    });
                //}
                productGroups.Add(new ProductGroupInfo
                {
                    GroupId = Convert.ToDecimal(row["GroupId"]),
                    GroupName = row["GroupName"].ToString(),
                    GroupUnder = Convert.ToDecimal(row["narration"].ToString()),
                    Extra1= row["extra1"].ToString()
                });

            }
            return productGroups;
        }

        private string findGroupName(decimal groupId)
        {
            var productGroupsDt = productGroupSp.ProductGroupViewAll();
            foreach (DataRow row in productGroupsDt.Rows)
            {
                if (Convert.ToDecimal(row["GroupId"]) == groupId)
                {
                    return row["GroupName"].ToString();
                }
            }
            return "";
        }

        public ProductGroupInfo GetProductGroup(decimal groupId)
        {
            var productGroupDt = productGroupSp.ProductGroupView(groupId);
            ProductGroupInfo productGroup = new ProductGroupInfo
            {
                GroupId = Convert.ToDecimal(productGroupDt.GroupId),
                GroupName = productGroupDt.GroupName,
                Extra1=productGroupDt.Extra1
            };
            return productGroup;
        }

        [HttpPost]
        public bool DeleteProductGroup(ProductGroupInfo productGroup)
        {
            if (productGroupSp.ProductGroupReferenceDelete(productGroup.GroupId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public ProductGroupInfo AddProductGroup(ProductGroupInfo productGroup)
        {
            productGroup.Extra1 = productGroup.Extra1;
            productGroup.Extra2 = "";
            productGroup.Narration = "";
            productGroup.ExtraDate = DateTime.Now;
            if (productGroupSp.ProductGroupAdd(productGroup) > 0)
            {
                return productGroup;
            }
            return null;
        }
        [HttpPost]
        public ProductGroupInfo EditProductGroup(ProductGroupInfo productGroup)
        {
            productGroup.Extra1 = "";
            productGroup.Extra2 = "";
            productGroup.Narration = "";
            productGroup.ExtraDate = DateTime.Now;
            if(productGroupSp.ProductGroupEdit(productGroup))
            {
                return productGroup;
            }
            return null;
        }
    }
}
