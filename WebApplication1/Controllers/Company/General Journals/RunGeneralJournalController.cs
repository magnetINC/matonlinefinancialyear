﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Company.General_Journals
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RunGeneralJournalController : ApiController
    {
        public RunGeneralJournalController()
        {
        }

        [HttpGet]
        public HttpResponseMessage GetDropdowns()
        {
            dynamic response = new ExpandoObject();
            response.AccountLedger = new AccountLedgerSP().AccountLedgerViewForJournalVoucher();
            response.Currencies = new TransactionsGeneralFill().CurrencyComboByDate(DateTime.Now);

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
    }
}
