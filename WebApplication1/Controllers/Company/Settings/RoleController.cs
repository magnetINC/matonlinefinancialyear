﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Company.Settings
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RoleController : ApiController
    {
        public RoleController()
        {

        }

        [HttpGet]
        public List<RoleInfo> GetRoles()
        {
            RoleSP spRole = new RoleSP();
            return spRole.RoleViewGridFill();
        }

        [HttpPost]
        public RoleInfo AddRole(RoleInfo input)
        {
            try
            {
                RoleSP spRole = new RoleSP();
                input.Extra1 = "";
                input.Extra2 = "";
                input.Narration = "";
                if (spRole.RoleCheckExistence(input.RoleId, input.Role) == false)
                {
                    if(spRole.RoleAdd(input)>0)
                    {
                        return input;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RL:3" + ex.Message;
            }
            return input;
        }

        [HttpPost]
        public RoleInfo EditRole(RoleInfo input)
        {
            try
            {
                input.Extra1 = "";
                input.Extra2 = "";
                input.Narration = "";
                RoleSP spRole = new RoleSP();
                if (spRole.RoleCheckExistence(input.RoleId, input.Role) == false)
                {
                    if (spRole.RoleEdit(input) > 0)
                    {
                        return input;
                    }
                    else
                    {
                        return null;
                    }
                }
                else
                {
                    return null;
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RL:3" + ex.Message;
            }
            return input;
        }

        [HttpPost]
        public string DeleteRole(RoleInfo input)
        {
            RoleSP spRole = new RoleSP();
            if (spRole.RoleReferenceDelete(input.RoleId) == -1)
            {
                return "ROLE_HAS_BEEN_REFERENCED";
            }
            else
            {
                return "SUCCESS";
            }
        }

        [HttpGet]
        public DataTable GetRolesForPriviledges()
        {
            return new RoleSP().RoleViewAll();
        }
    }
}
