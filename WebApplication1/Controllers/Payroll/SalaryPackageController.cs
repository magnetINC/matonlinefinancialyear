﻿using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SalaryPackageController : ApiController
    {
        SalaryPackageSP salaryPackageSp;
        PayHeadSP payHeadSp;
        public SalaryPackageController()
        {
            salaryPackageSp = new SalaryPackageSP();
            payHeadSp = new PayHeadSP();
        }

        public HttpResponseMessage GetSalaryPackages()
        {
            var payElementsDt = payHeadSp.PayHeadViewAll();
            var salaryPackagesDt = salaryPackageSp.SalaryPackageViewAll();
            SalaryPackageViewModel response = new SalaryPackageViewModel
            {
                SalaryPackage=salaryPackagesDt,
                PayElement=payElementsDt
            };
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        public HttpResponseMessage GetSalaryPackage(decimal salaryPackageId)
        {
            var salaryPackageDt = salaryPackageSp.SalaryPackageView(salaryPackageId);
            return Request.CreateResponse(HttpStatusCode.OK, salaryPackageDt);
        }

        [HttpPost]
        public bool DeleteSalaryPackage(SalaryPackageInfo salaryPackageInfo)
        {
            return salaryPackageSp.SalaryPackageDeleteAll(salaryPackageInfo.SalaryPackageId);
        }

        [HttpPost]
        public bool AddSalaryPackage(SalaryPackageInfo salaryPackageInfo)
        {
            //salaryPackageInfo.ExtraDate = DateTime.Now;
            //salaryPackageInfo.Extra1 = "";
            //salaryPackageInfo.Extra2 = "";
            //salaryPackageInfo.Narration = "";
            //return payHeadSp.PayHeadAdd(payElementInfo);
            return false;
        }
        [HttpPost]
        public bool EditPayElement(PayHeadInfo payElementInfo)
        {
            payElementInfo.ExtraDate = DateTime.Now;
            payElementInfo.Extra1 = "";
            payElementInfo.Extra2 = "";
            payElementInfo.Narration = "";
            return payHeadSp.PayHeadEdit(payElementInfo);
        }
    }
}
