﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATClassLibrary.Model
{
    public class RackViewAll
    {
		public dynamic rackId { get; set; }
		public string rackName { get; set; }
		public dynamic godownId { get; set; }
		public string narration { get; set; }
		public dynamic extraDate { get; set; }
		public string extra1 { get; set; }
		public string extra2 { get; set; }

	}
}
