﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATClassLibrary.Model
{
    public class ModelNoModel
    {
	 public dynamic	modelNoId { get; set; }
		public string modelNo { get; set; }
		public string narration { get; set; }
		public dynamic extraDate { get; set; }
		public string extra1 { get; set; }
		public string extra2 { get; set; }
	}
}
