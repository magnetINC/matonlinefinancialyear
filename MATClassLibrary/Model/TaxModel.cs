﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATClassLibrary.Model
{
    public  class TaxModel
    {

       public string taxName { get; set; }
        public string taxId { get; set; }

        public string typeOfVoucher { get; set; }
        public string voucherTypeId { get; set; }

    }
}
