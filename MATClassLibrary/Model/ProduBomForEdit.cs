﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATClassLibrary.Model
{
    public class ProduBomForEdit
    {
	  public dynamic bomId { get; set; }
		public dynamic productId { get; set; }
		public dynamic rowmaterialId { get; set; }
		public dynamic quantity { get; set; }
		public dynamic unitId { get; set; }
		public string extra1 { get; set; }
		public string extra2 { get; set; }
		public dynamic extraDate { get; set; }
	}
}
