﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATClassLibrary.Model
{
    public class BatchViewModel
    {
		public dynamic batchId { get; set; }
		public string batchNo { get; set; }
		public dynamic productId { get; set; }
		public dynamic manufacturingDate { get; set; }
		public dynamic expiryDate { get; set; }
		public string extra1 { get; set; }
		public string extra2 { get; set; }
		public dynamic extraDate { get; set; }
		public string narration { get; set; }
	}
}
