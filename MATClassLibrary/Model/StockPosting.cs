﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatOnline.Model
{
    public class StockPosting
    {

		public dynamic date { get; set; }
		public dynamic voucherTypeId { get; set; }
		public string voucherNo { get; set; }
		public string invoiceNo { get; set; }
		public dynamic productId { get; set; }
		public dynamic unitId { get; set; }
		public dynamic batchId { get; set; }
		public dynamic godownId { get; set; }
		public dynamic rackId { get; set; }
		public dynamic againstVoucherTypeId { get; set; }
		public string againstInvoiceNo { get; set; }
		public string againstVoucherNo { get; set; }
		public dynamic inwardQty { get; set; }
		public dynamic outwardQty { get; set; }
		public dynamic rate { get; set; }
		public dynamic financialYearId { get; set; }
		public dynamic extraDate { get; set; }
		public string extra1 { get; set; }
		public string extra2 { get; set; }
		public dynamic ProjectId { get; set; }
		public dynamic CategoryId { get; set; }
		public dynamic StockProductID { get; set; }
		public dynamic amount { get; set; }
		
	}
}