﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATClassLibrary.Model
{
   public  class ProductViewModel
    {
            public dynamic isallowBatch { get; set; }
        public dynamic isBom { get; set; }
        public dynamic ismultipleunit { get; set; }

        public string batchNo { get; set; }
        public dynamic productId { get; set; }
        public string productCode { get; set; }
        public string productName { get; set; }
        public dynamic groupId { get; set; }
        public dynamic brandId { get; set; }
        public dynamic unitId { get; set; }
        public dynamic sizeId { get; set; }
        public dynamic modelNoId { get; set; }
        public dynamic taxId { get; set; }
        public dynamic taxapplicableOn { get; set; }
        public dynamic purchaseRate { get; set; }
        public dynamic salesRate { get; set; }
        public dynamic mrp { get; set; }
        public dynamic minimumStock { get; set; }
        public dynamic maximumStock { get; set; }
        public dynamic reorderLevel { get; set; }
        public dynamic godownId { get; set; }
        public dynamic rackId { get; set; }
     
        public dynamic isopeningstock { get; set; }
        public string narration { get; set; }
        public dynamic isActive { get; set; }
        public dynamic isshowRemember { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public dynamic extraDate { get; set; }
        public string partNo { get; set; }
        public dynamic salesAccount { get; set; }
        public dynamic expenseAccount { get; set; }
        public dynamic effectiveDate { get; set; }
        public string productType { get; set; }
    }
}
