﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATClassLibrary.Model
{
    public class UnitViewAll
    {
        public dynamic unitId { get; set; }
        public string unitName { get; set; }
        public string narration { get; set; }
        public dynamic noOfDecimalplaces { get; set; }

    }
}
