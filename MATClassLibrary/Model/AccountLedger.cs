﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATClassLibrary.Model
{
    public class AccountLedger
    {
	    public dynamic ledgerId { get; set; }
		public object accountGroupId { get; set; }
		public string ledgerName { get; set; }
		public string openingBalance { get; set; }
		public string crOrDr { get; set; }
		public string narration { get; set; }
		public string mailingName { get; set; }
		public string address { get; set; }
		public string phone { get; set; }
		public string email { get; set; }
		public string creditPeriod { get; set; }
		public string creditLimit { get; set; }
		public dynamic pricinglevelId { get; set; }
		public string billByBill { get; set; }
		public string tin { get; set; }
		public string cst { get; set; }
		public string pan { get; set; }
		public dynamic routeId { get; set; }
		public string bankAccountNumber { get; set; }
		public string branchName { get; set; }
	 public	string  branchCode { get; set; }
		public dynamic extraDate { get; set; }
		public string extra1 { get; set; }
		public string extra2 { get; set; }
		public dynamic isActive { get; set; }

	}
}
