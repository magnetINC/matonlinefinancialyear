﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MATClassLibrary.Model
{
    public class SizeViewAll
    {

		public string extra2 { get; set; }
		public string extra1 { get; set; }
		public dynamic extraDate { get; set; }
		public string narration { get; set; }
		public string size { get; set; }
		public int sizeId { get; set; }
		
	}
}