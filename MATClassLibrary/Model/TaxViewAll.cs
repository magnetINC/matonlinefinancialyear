﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATClassLibrary.Model
{
    public class TaxViewAll
    {
	    public dynamic taxId { get; set; }
		public string taxName { get; set; }
		public string applicableOn { get; set; }

		public dynamic rate { get; set; }

		public string calculatingMode { get; set; }
		public string narration { get; set; }
		public dynamic isActive { get; set; }
		public dynamic extraDate { get; set; }

		public string extra1 { get; set; }
		public string extra2 { get; set; }
		public string type { get; set; }

	}
}
