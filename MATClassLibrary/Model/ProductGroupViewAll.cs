﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATClassLibrary.Model
{
    public  class ProductGroupViewAll
    {
		public dynamic groupId { get; set; }
		public string groupName { get; set; }
		public string groupUnder { get; set; }

		public string narration { get; set; }

		public string extra1 { get; set; }
		public string extra2 { get; set; }

		public dynamic extraDate { get; set; }
	}
}
