﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATClassLibrary.Model
{
    public class BrandViewAll
    {

        public dynamic brandId { get; set; }
        public string brandName { get; set; }
        public string narration { get; set; }
        public string manufacturer { get; set; }
       
    }
}
