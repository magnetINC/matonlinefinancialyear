﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.SalesModel
{
    public class GetSalesMode
    {
        public decimal productId { get; set; }
        public string barCode { get; set; }
        public decimal batchId { get; set; }
        public string batchNo { get; set; }
        public string unitName { get; set; }
        public decimal productCode { get; set; }
        public string productName { get; set; }
        public string qty { get; set; }
        public string rate { get; set; }
        public decimal currencyId { get; set; }
        public decimal unitConversionId { get; set; }
        public decimal unitId { get; set; }
        public string godownId { get; set; }
        public decimal rackId { get; set; }
        public decimal taxId { get; set; }
        public string voucherNo { get; set; }
        public string invoiceNo { get; set; }
        public decimal voucherTypeId { get; set; }
        public string conversionRate { get; set; }
        public decimal projectId { get; set; }
        public decimal categoryId { get; set; }
        public decimal amount { get; set; }
        public string narration { get; set; }
        public string itemDescription { get; set; }
    }
}