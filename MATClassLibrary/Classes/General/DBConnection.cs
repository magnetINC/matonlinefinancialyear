using System;
using System.Collections.Generic;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;
using System.IO;
using System.ServiceProcess;
using Microsoft.SqlServer.Management.Common;
using Microsoft.SqlServer.Management.Smo;
using System.Diagnostics;
using System.Configuration;

//<summary>    
//Summary description for DBConnection    
//</summary>    
namespace MATFinancials
{
    public class DBConnection
    {
        protected SqlConnection sqlcon;
        public string sqlConnectionString = ConfigurationManager.AppSettings.Get("MATLIB:ConnectionString");
        protected string serverName = (ConfigurationManager.AppSettings["MsSqlServer"] == null || ConfigurationManager.AppSettings["MsSqlServer"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["MsSqlServer"].ToString();
        protected string userId = (ConfigurationManager.AppSettings["MsSqlUserId"] == null || ConfigurationManager.AppSettings["MsSqlUserId"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["MsSqlUserId"].ToString();
        protected string password = (ConfigurationManager.AppSettings["MsSqlPassword"] == null || ConfigurationManager.AppSettings["MsSqlPassword"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["MsSqlPassword"].ToString();
        protected string ApplicationPath = (ConfigurationManager.AppSettings["ApplicationPath"] == null || ConfigurationManager.AppSettings["ApplicationPath"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["ApplicationPath"].ToString();
        //protected string ApplicationPath = (ConfigurationManager.AppSettings["magnetConnectionString"] == null || ConfigurationManager.AppSettings["magnetConnectionString"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["magnetConnectionString"].ToString();
        protected string isSqlServer = (ConfigurationManager.AppSettings["isServerConnection"] == null || ConfigurationManager.AppSettings["isServerConnection"].ToString() == string.Empty) ? null : ConfigurationManager.AppSettings["isServerConnection"].ToString();
        public DBConnection()
        {
            SqlConnection.ClearAllPools();
            string path = string.Empty;
            //sqlConnectionString = ConfigurationManager.AppSettings.Get("MATLIB:ConnectionString");
            if (PublicVariables._decCurrentCompanyId > 0)
            {
                path = ApplicationPath + "\\Data\\" + PublicVariables._decCurrentCompanyId + "\\DBMATAccounting.mdf";
                sqlcon = new SqlConnection(@"Data Source=.; AttachDbFilename=" + path + "; Integrated Security = true; Connect Timeout = 120; User Instance=true");
            }
            else if (PublicVariables._decCurrentCompanyId == 0)
            {
                path = ApplicationPath + "\\Data\\DBMATAccounting.mdf";
                //TODO: test for first login
                sqlcon = new SqlConnection();

                //test
                //  sqlcon.ConnectionString = @"Data Source=45.40.135.165;Initial Catalog=KascoKano_Test;user id=sa;Password=sa@12;MultipleActiveResultSets=True";

                // sqlcon.ConnectionString = @"Data Source=45.40.135.165;Initial Catalog=DBMATAccounting_NordicaFertilityCenter;user id=sa;Password=sa@12;MultipleActiveResultSets=True";

                // sqlcon.ConnectionString = @"Data Source=45.40.135.165;Initial Catalog=DBMATAccounting_Kascokano;user id=sa;Password=sa@12;MultipleActiveResultSets=True";

                //   sqlcon.ConnectionString = @"Data Source=45.40.135.165;Initial Catalog=DBMATAccounting_HisPro_Test;user id=sa;Password=sa@12;MultipleActiveResultSets=True";

                //sqlcon.ConnectionString = @"Data Source=.\SQLEXPRESS;Initial Catalog=DBMATAccounting_Kascokano;integrated security=True;MultipleActiveResultSets=True";
                //

                //sqlcon.ConnectionString = @"Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=DBMATAccounting_HisPro;integrated security=True;MultipleActiveResultSets=True";

                //  sqlcon.ConnectionString = @"Data Source=45.40.135.165;Initial Catalog=DBMATAccounting_HisPro_Test;user id=sa;Password=sa@12;MultipleActiveResultSets=True";

                /* dev | local connection string */
               //sqlcon.ConnectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=DBMATAccounting_HisPro_Test;integrated security=True;MultipleActiveResultSets=True";
               sqlcon.ConnectionString = "Data Source=(localdb)\\MSSQLLocalDB;Initial Catalog=DBMATAccounting_HisPro_TestKA;integrated security=True;MultipleActiveResultSets=True";

                /* test deployment connection string */
                //sqlcon.ConnectionString = "Data Source=161.97.160.238;initial catalog=DBMATAccounting_HisPro_Test;persist security info=True;user id=Matoline_user;password=43Fmu6g~;multipleactiveresultsets=True;application name=EntityFramework";

                /* atorj deployment connection string */
                //sqlcon.ConnectionString = "Data Source=VMI805339;initial catalog=DBMATAccounting_Atorj;persist security info=True;user id=Matoline_user;password=43Fmu6g~;multipleactiveresultsets=True;application name=EntityFramework";

                /* kasco deployment connection string */
                //sqlcon.ConnectionString = "Data Source=VMI805339;initial catalog=DBMATAccounting_Kascokano;persist security info=True;user id=Matoline_user;password=43Fmu6g~;multipleactiveresultsets=True;application name=EntityFramework";
                //sqlcon.ConnectionString = "Data Source=161.97.160.238;initial catalog=DBMATAccounting_Kascokano;persist security info=True;user id=Matoline_user;password=43Fmu6g~;multipleactiveresultsets=True;application name=EntityFramework";



                /* hisfarm deployment connection string */
                //sqlcon.ConnectionString = "Data Source=161.97.160.238;initial catalog=DBMATAccounting_HisPro;persist security info=True;user id=Matoline_userr;password=43Fmu6g~;multipleactiveresultsets=True;application name=EntityFramework";

                /* nordica-deployment connection string */
                //sqlcon.ConnectionString = "Data Source=VMI805339;initial catalog=DBMATAccounting_NordicaFertilityCenter;persist security info=True;user id=Matoline_user;password=43Fmu6g~;multipleactiveresultsets=True;application name=EntityFramework";



            }
            else
            {
                path = ApplicationPath + "\\Data\\COMP\\DBMATAccounting.mdf";
            }

            if (serverName != null)
            {
                if (isSqlServer != null)
                {
                    if (userId == null || password == null)
                    {
                        //1.sqlcon = new SqlConnection(this.MagnetConnectionString);
                        sqlcon = new SqlConnection(@"Data Source=" + serverName + ";AttachDbFilename=" + path + ";Integrated Security=True;Connect Timeout=120");
                    }
                    else if (Convert.ToBoolean(isSqlServer))
                    {
                        sqlcon = new SqlConnection(this.MagnetConnectionString);//Felix 20161021 For server connection
                    }
                    else
                    {
                        //2.sqlcon = new SqlConnection(this.MagnetConnectionString);
                        sqlcon = new SqlConnection(@"Data Source=" + serverName + ";AttachDbFilename=" + path + ";user id='" + userId + "';password='" + password + "'; Connect Timeout=120");
                        // attach dbfile here
                    }
                }
                else
                {
                    if (userId == null || password == null)
                    {
                        //3.sqlcon = new SqlConnection(this.MagnetConnectionString);   // goes through with this connection string
                        sqlcon = new SqlConnection(@"Data Source=" + serverName + ";AttachDbFilename=" + path + ";Integrated Security=True;Connect Timeout=120;User Instance=True");

                    }
                    else
                    {
                        //4.sqlcon = new SqlConnection(this.MagnetConnectionString);
                        sqlcon = new SqlConnection(@"Data Source=" + serverName + ";AttachDbFilename=" + path + "; Connect Timeout=120; User Instance=False");

                    }
                }
                try
                {
                    if (string.IsNullOrEmpty(sqlcon.ConnectionString)) sqlcon.ConnectionString = sqlConnectionString;
                    sqlcon.Open();
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.Message);
                }
            }
        }
        public string MagnetConnectionString
        {
            get
            {
                //return Convert.ToString(ConfigurationManager.AppSettings["magnetConnectionString"]);
                return ConfigurationManager.AppSettings.Get("MATLIB:ConnectionString"); 
            }
        }

        public string MagnetConnectionString2
        {
            get
            {
                return Convert.ToString(ConfigurationManager.AppSettings["magnetConnectionString2"]);
            }
        }
    }
}
