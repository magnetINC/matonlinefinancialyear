﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
namespace MATFinancials
{
    public class CheckUserPrivilege : DBConnection
    {
        /// <summary>
        /// Function to check the privilege
        /// </summary>
        /// <param name="userId"></param>
        /// <param name="formName"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        public static bool PrivilegeCheck(decimal userId, string formName, string action)
        {
            bool isPrivilege = false;
            PrivilegeSP spPrivilege = new PrivilegeSP();
            if (spPrivilege.PrivilegeCheck(userId, formName, action))
            {
                isPrivilege = true;
            }
            else
            {
                isPrivilege = false;
            }
            return isPrivilege;
        }
    }
}
