﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Configuration;
using System.IO;
namespace MATFinancials
{
    public static class GlobalObject
    {
        private static string _errorLogDirectory = string.Empty;
        private static string _successfulLogDirectory = string.Empty; 
        private static string _eventLogFileLocation = string.Empty; 
        private static string _applicationPath = ConfigurationManager.AppSettings["ApplicationPath"];
        private static string _validationUrl = ConfigurationManager.AppSettings["MagnetUrl"];

        public static string ApplicationPath { get { return _applicationPath; } }
        public static string ValidationUrl { get { return _validationUrl; } }

        public static string ErrorLogDirectory
        {

            get { return CreateDirectory(string.Format("{0}\\Log",ApplicationPath)); }
        }
        public static string SuccessfulLogDirectory
        {
            get { return ErrorLogDirectory; }
        }
        public static string CurrentErrorLogDirectory {
            get {
                return CreateDirectory(string.Format("{0}\\{1}", ErrorLogDirectory, DateTime.Now.ToString("yyyyMM"))) ;
            }
        }
        public static string CurrentEventLogFileLocation {
            get {
                return CreateFile(string.Format("{0}\\{1}_log.txt", CurrentErrorLogDirectory, DateTime.Now.ToString("yyyyMMdd"))) ;
            }
        }
        public static string CurrentEventLogFileLocationStartup
        {
            get
            {
                return CreateFile(string.Format("{0}\\{1}_Startup_log.txt", CurrentErrorLogDirectory, DateTime.Now.ToString("yyyyMMdd")));
            }
        }
        private static string CurrentEventLogFileLocationCommon
        {
            get
            {
                return CreateFile(string.Format("{0}\\{1}_event.txt", CurrentErrorLogDirectory, DateTime.Now.ToString("yyyyMMdd")));
            }
        }
        public static bool LogEvent(StreamWriter writer, LogType logType, string message, string title)
        {
            bool isLogged = false;
            try
            {
                writer.WriteLine(string.Format("{0} {1}\t {2} \t {3} \r", DateTime.Now.ToString("yyyyMMdd.hhmmss"), logType.ToString().Substring(0,1), !string.IsNullOrEmpty(title)? title + " : ":string.Empty, message ) );
                isLogged = true;
            }
            finally { }
            return isLogged;
        }
        public static bool WriteLine( LogType logType, string title, string message)
        {
            StreamWriter writer = new StreamWriter(Path.GetFullPath(CurrentEventLogFileLocationCommon), true);
            bool isLogged = false;
            try
            {
                writer.WriteLine(string.Format("{0} {1}\t {2} \t {3} \r", DateTime.Now.ToString("yyyyMMdd.hhmmss"), logType.ToString().Substring(0, 1), !string.IsNullOrEmpty(title) ? title + " : " : string.Empty, message));
                isLogged = true;
            }
            finally {
                writer.Flush();writer.Close();
                writer.Dispose();
            }
            return isLogged;
        }

        #region Private Static
        private static string CreateDirectory(string directory)
        {
            if (!Directory.Exists(directory))
                Directory.CreateDirectory(directory) ;
            return directory;
        }
        private static string CreateFile(string completePath)
        {
            if (!File.Exists(completePath))
                using (var fs = File.Create(completePath)) { }
            return completePath;
        }
        #endregion

        public enum LogType
        {
            Information = 0,
            Warning = 1,
            Error = 2,
            Success = 3
        }

    }

    
}

