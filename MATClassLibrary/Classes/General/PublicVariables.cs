﻿using iTextSharp.text;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.Linq;
using System.Text;
using static System.Data.Entity.Infrastructure.Design.Executor;
using System.Web;

namespace MATFinancials
{
    public static class PublicVariables
    {
        public static decimal _decCurrentUserId = 1;
        public static decimal _decCurrentCompanyId = 0;
        public static DateTime _dtCurrentDate=DateTime.Now;
        public static DateTime _dtFromDate=new DateTime(2018,01,01);//financial year starting    
        public static DateTime _dtToDate= DateTime.MaxValue;/*new DateTime(2030,12,31)*///financial year ending        
        public static int CurrentFinicialId { get; set; }
        public static decimal _decCurrentFinancialYearId {get;set;}
        public static bool isMessageAdd = true;
        public static bool isMessageEdit = true;
        public static bool isMessageDelete = true;
        public static bool isMessageClose = true;
        // public static decimal _decCurrencyId = 1;
        public static decimal _decCurrencyId = 48;
        public static int _inNoOfDecimalPlaces = 2;
        public static string MessageToShow = string.Empty;
        public static string MessageHeadear = string.Empty;
        public static decimal PartyBalance = 0;
        public static DateTime paymentDate = _dtCurrentDate;
        public static decimal productId = 0;

        public static string SessionName = "ApplicationSession";

        public static string DataConnectionString { get; set; }

        public static void SaveSession(DateTime StartDate, DateTime EndDate, int financialYearId , int UserId, string DataConnections)
        {
            DataConnectionString = DataConnections;
            var session = HttpContext.Current.Session;
            if (session == null)
            {
                try
                {
                    if (session == null || session[SessionName] == null)
                    {
                        _dtFromDate = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day);
                        _dtToDate = new DateTime(EndDate.Year, EndDate.Month, EndDate.Day);

                        _decCurrentFinancialYearId = financialYearId;
                        CurrentFinicialId = financialYearId;
                        _decCurrentUserId = UserId;
                        //HttpContext.Current.Session.Add(SessionName, sessionMode);
                        return;
                    }
                }
                catch (Exception ex)
                {
                    //HttpContext.Current.Session.Remove(SessionName);
                    //HttpContext.Current.Session.Add(SessionName, sessionMode);
                    return;
                }

                return;
            }
            else
            {
                _dtFromDate = new DateTime(StartDate.Year, StartDate.Month, StartDate.Day);
                _dtToDate = new DateTime(EndDate.Year, EndDate.Month, EndDate.Day);

                _decCurrentFinancialYearId = financialYearId;
                CurrentFinicialId = financialYearId;
                _decCurrentUserId = UserId;
            }
            return;
        }

        // defined by precious 20/07/2016 to set default colour for skin
        public static System.Drawing.Color myBackColour()
        {
            //return Color.DarkOliveGreen;
            //return Color.FromArgb(170, 184, 131);
            return System.Drawing.Color.White;//.Gray;// (170, 184, 131);
        }
        public static System.Drawing.Color myForeColour()
        {
            //return Color.WhiteSmoke;
            return System.Drawing.Color.Black;//.White;
        }
    }
    //class CheckUserPrivilege : DBConnection
    //{
    //    /// <summary>
    //    /// Function to check the privilege
    //    /// </summary>
    //    /// <param name="userId"></param>
    //    /// <param name="formName"></param>
    //    /// <param name="action"></param>
    //    /// <returns></returns>
    //    public static bool PrivilegeCheck(decimal userId, string formName, string action)
    //    {
    //        bool isPrivilege = false;
    //        PrivilegeSP spPrivilege = new PrivilegeSP();
    //        if (spPrivilege.PrivilegeCheck(userId, formName, action))
    //        {
    //            isPrivilege = true;
    //        }
    //        else
    //        {
    //            isPrivilege = false;
    //        }
    //        return isPrivilege;
    //    }
    //}
}
