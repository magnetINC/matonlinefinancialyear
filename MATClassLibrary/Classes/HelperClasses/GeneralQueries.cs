﻿using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATFinancials.Classes.HelperClasses
{
    public class GeneralQueries : DBConnection
    {
        SqlCommand comm;
        SqlParameter param;
        SqlDataAdapter da;
        DataSet ds;
        SqlTransaction transaction;
        DBMatConnection conn = new DBMatConnection();
        public GeneralQueries()
        {
            comm = new SqlCommand();
            param = new SqlParameter();
            da = new SqlDataAdapter();
            ds = new DataSet();
        }
        public DataTable SalaryPackageWithPayHead()
        {
            DataTable dt = new DataTable();
            ds = new DataSet();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                string CommStr = "select s.salaryPackageId, h.payHeadId, s.amount, h.payHeadName, h.ReceivingLedgerId from tbl_SalaryPackageDetails s inner join tbl_PayHead h on s.payHeadId = h.payHeadId";
                comm = new SqlCommand(CommStr, sqlcon);
                da = new SqlDataAdapter(comm);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ8:" + ex.Message;
            }
            finally
            {
                sqlcon.Close();
            }
            return dt;
        }
        public void BulkInsertSalaryPaymentDetails(DataTable dtSalaryVoucherDetails, DataTable dtLedgerPosting)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                //start a local transaction
                transaction = sqlcon.BeginTransaction();
                //assign transaction to command object for pending local transaction
                //comm.Transaction = transaction;
                //assign stored procedure and connection to command object 
                comm = new SqlCommand("SP_LedgerPostingSalaryVoucherDetails_Insert", sqlcon, transaction);
                param = new SqlParameter();
                comm.CommandType = CommandType.StoredProcedure;

                param = new SqlParameter();
                param = comm.Parameters.AddWithValue("@TVPSalaryVoucherDetails", dtSalaryVoucherDetails);
                param.SqlDbType = SqlDbType.Structured;
                param.TypeName = "dbo.[tblType_SalaryVoucherDetails]";

                param = new SqlParameter();
                param = comm.Parameters.AddWithValue("@TVP_LedgerPosting", dtLedgerPosting);
                param.TypeName = "dbo.[tblType_LedgerPosting]";
                param.SqlDbType = SqlDbType.Structured;
                // assign transaction to command object for pending local transaction
                comm.Transaction = transaction;
                comm.ExecuteNonQuery();
                // commit the transaction
                transaction.Commit();
            }
            catch (Exception ex)
            {
                //in case of exception, roll back the transaction
                try
                {
                    transaction.Rollback();
                }
                catch (Exception exe)
                {
                    //formMDI.infoError.ErrorString = "GQ7:" + ex.Message;
                }
            }
        }
        public DataSet EmployeeReceivingLedgerDetails()
        {
            ds = new DataSet();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                //string CommStr = "select s.salaryPackageId, h.payHeadId, s.amount, h.payHeadName, h.ReceivingLedgerId from tbl_SalaryPackageDetails s inner join tbl_PayHead h on s.payHeadId = h.payHeadId";
                comm = new SqlCommand("EmployeeReceivingLedgerDetails", sqlcon);
                comm.CommandType = CommandType.StoredProcedure;
                da = new SqlDataAdapter(comm);
                da.Fill(ds);
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ6:" + ex.Message;
            }
            finally
            {
                sqlcon.Close();
            }
            return ds;
        }
        public decimal retainedEarnings(DateTime toDate)
        {
             decimal retainedEarnings = 0;
            try
            {
                decimal ClosingStockForRollOver = 0;
                decimal dcOpeningStockForBalanceSheetRollOver = 0;
                string calculationMethod = string.Empty;

                SettingsInfo InfoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                //--------------- Selection Of Calculation Method According To Settings ------------------// 
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }

                FinancialStatementSP SpFinance = new FinancialStatementSP();

                DataSet dsetProfitAndLoss = SpFinance.ProfitAndLossAnalysisUpToaDateForBalansheet(PublicVariables._dtFromDate, toDate);
                DataSet DsetBalanceSheet = SpFinance.BalanceSheet(PublicVariables._dtFromDate, toDate);
                DataSet dsetProfitAndLossRollOver = SpFinance.ProfitAndLossAnalysisUpToaDateForBalansheetForRollOver(PublicVariables._dtFromDate, toDate);
                DataSet DsetBalanceSheetForRollOver = SpFinance.BalanceSheetForRollOver(PublicVariables._dtFromDate, toDate);
                DataTable dtbl = new DataTable();
                decimal decTotalProfitAndLoss = decimal.Parse(DsetBalanceSheet.Tables[2].Compute("Sum(Balance)", string.Empty).ToString());
                decimal decRetainedEarning = decimal.Parse(DsetBalanceSheetForRollOver.Tables[2].Compute("Sum(Balance)", string.Empty).ToString());

                decimal dcProfitRollOver = 0;
                for (int i = 0; i < dsetProfitAndLoss.Tables.Count; ++i)
                {
                    dtbl = dsetProfitAndLossRollOver.Tables[i];
                    decimal dcSum = 0;
                    if (i == 0 || (i % 2) == 0)
                    {
                        if (dtbl.Rows.Count > 0)
                        {
                            dcSum = decimal.Parse(dtbl.Compute("Sum(Debit)", string.Empty).ToString());
                            dcProfitRollOver = dcProfitRollOver - dcSum;
                        }
                    }
                    else
                    {
                        if (dtbl.Rows.Count > 0)
                        {
                            dcSum = decimal.Parse(dtbl.Compute("Sum(Credit)", string.Empty).ToString());
                            dcProfitRollOver = dcProfitRollOver + dcSum;
                        }
                    }
                }

                FinancialYearSP spFinancialYear = new FinancialYearSP();
                DataTable dtblF= new DataTable();
                dtblF = spFinancialYear.FinancialYearViewAll();

                DateTime finacialYear = PublicVariables._dtToDate;

                decimal decCurrentProfitLossRollover = 0;
                for (int i = 0; i < dtblF.Rows.Count; ++i) {
                    DataRow row = dtblF.Rows[i];
                    if(finacialYear.Year == Convert.ToDateTime(row["toDate"]).Year)
                    {
                        break;
                    }
                    DateTime thisFromDate = Convert.ToDateTime(row["fromDate"]);
                    DateTime thisToDate = Convert.ToDateTime(row["toDate"]);
                    decimal pl = calRetainedEarnings(thisFromDate, thisToDate, calculationMethod);
                    decCurrentProfitLossRollover += pl; /*dcProfitRollOver + ClosingStockForRollOver*/
                }

                //DateTime dateTry = PublicVariables._dtToDate;
                //PublicVariables._dtToDate = finacialYear;
                //dateTry = PublicVariables._dtToDate;

                ClosingStockForRollOver = SpFinance.StockValueGetOnDateForRollOver(DateTime.Parse(PublicVariables._dtToDate.ToString()), calculationMethod, false, false, true);
                dcOpeningStockForBalanceSheetRollOver = SpFinance.StockValueGetOnDateForRollOver(toDate, calculationMethod, true, true, true);
                ClosingStockForRollOver = Math.Round(ClosingStockForRollOver, 2) - dcOpeningStockForBalanceSheetRollOver;

                //retainedEarnings = decTotalProfitAndLoss + decCurrentProfitLossRollover + decRetainedEarning;
                retainedEarnings = decCurrentProfitLossRollover;
                //decimal profitLoss = profitOrLoss(toDate, calculationMethod);
                //retainedEarnings = profitLoss + decRetainedEarning;
            }
            catch (Exception ex)
            {
              Console.WriteLine( "GQ5:" + ex.Message);
            }
            return retainedEarnings;
        } 
         public decimal calRetainedEarnings(DateTime fromDate, DateTime toDate, string calculationMethod)
        {
            decimal profitOrLoss = 0;
            DateTime currentDate = DateTime.Now;
            try
            {
                decimal TotalOpeningStockAndExpenses = 0;
                decimal TotalRevenue = 0;
                decimal OperatingExpenses = 0;
                decimal GrossProfitOrLoss = 0;
                decimal dcOpeningStock = 0;
                decimal currentStockValue = 0;
                DataSet dsetProfitAndLoss = new DataSet();
                DataTable dtbl = new DataTable();
                DataTable dtblFinancial = new DataTable();
                FinancialStatementSP SpFinance = new FinancialStatementSP();
                GeneralQueries methodForStockValue = new GeneralQueries();
                DBMatConnection conn = new DBMatConnection();

                currentDate = currentDate.AddYears(-1);

                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", toDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", fromDate.ToString());
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : fromDate;
                DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : fromDate.AddDays(-1);
                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    lastFinYearEnd = fromDate.AddDays(-1);
                }
                decimal OpeningStock = SpFinance.StockValueGetOnDate(toDate, calculationMethod, true, true);
                decimal dcOpeningStockForRollOver = SpFinance.StockValueGetOnDateForRollOver(DateTime.Parse(toDate.ToString()), calculationMethod, false, false, true);
                OpeningStock = OpeningStock + dcOpeningStockForRollOver;
                decimal ClosingStock = methodForStockValue.currentStockValue(fromDate, toDate, false, 0, false, currentDate);
                ClosingStock = ClosingStock - OpeningStock;
                dsetProfitAndLoss = SpFinance.ProfitAndLossAnalysis(fromDate, toDate);

                dcOpeningStock = SpFinance.StockValueGetOnDate(fromDate, toDate, calculationMethod, true, false);
                dcOpeningStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, false, 0, false, currentDate);
                dcOpeningStock = dcOpeningStock + dcOpeningStockForRollOver;
                currentStockValue = methodForStockValue.currentStockValue(lastFinYearStart, toDate, false, 0, false, currentDate); //this should work instaed   //

                decimal dcProfit = 0;
                for (int i = 0; i < dsetProfitAndLoss.Tables.Count; ++i)
                {
                    dtbl = dsetProfitAndLoss.Tables[i];
                    decimal dcSum = 0;
                    if (i == 0 || (i % 2) == 0)
                    {
                        if (dtbl.Rows.Count > 0)
                        {
                            dcSum = decimal.Parse(dtbl.Compute("Sum(Debit)", string.Empty).ToString());
                            dcProfit = dcProfit - dcSum;
                        }
                    }
                    else
                    {
                        if (dtbl.Rows.Count > 0)
                        {
                            dcSum = decimal.Parse(dtbl.Compute("Sum(Credit)", string.Empty).ToString());
                            dcProfit = dcProfit + dcSum;
                        }
                    }
                }
                TotalOpeningStockAndExpenses += dcOpeningStock;
                /// ---Purchases - Debit
                dtblFinancial = new DataTable();
                dtblFinancial = dsetProfitAndLoss.Tables[0];
                decimal dcPurchaseAccount = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = decimal.Parse(rw["Debit"].ToString().ToString());
                        dcPurchaseAccount += dcBalance;
                    }
                }
                TotalOpeningStockAndExpenses += dcPurchaseAccount;

                //---Sales Account  -Credit
                dtblFinancial = new DataTable();
                dtblFinancial = dsetProfitAndLoss.Tables[1];
                decimal dcSalesAccount = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = decimal.Parse(rw["Credit"].ToString().ToString());
                        dcSalesAccount += dcBalance;
                        TotalRevenue += dcBalance;
                    }
                }
                //----Direct Income 
                dtblFinancial = new DataTable();
                dtblFinancial = dsetProfitAndLoss.Tables[3];
                decimal dcDirectIncoome = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = Convert.ToDecimal(rw["Credit"].ToString());
                        dcDirectIncoome += dcBalance;
                        TotalRevenue += dcBalance;
                    }
                }
                decimal PlusDirectExpenses = TotalOpeningStockAndExpenses;

                ///------Indirect Expense 
                dtblFinancial = new DataTable();
                dtblFinancial = dsetProfitAndLoss.Tables[4];
                decimal dcIndirectExpense = 0;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = Convert.ToDecimal(rw["Debit"].ToString());
                        dcIndirectExpense += dcBalance;
                    }
                    OperatingExpenses += dcIndirectExpense;
                }
                //---Direct Expense
                PlusDirectExpenses = TotalOpeningStockAndExpenses;
                dtblFinancial = new DataTable();
                dtblFinancial = dsetProfitAndLoss.Tables[2];
                decimal dcDirectExpense = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = Convert.ToDecimal(rw["Debit"].ToString());
                        dcDirectExpense += dcBalance;
                        PlusDirectExpenses += dcBalance;
                    }
                }
                ///---Other Income 
                dtblFinancial = new DataTable();
                dtblFinancial = dsetProfitAndLoss.Tables[5];
                decimal dcIndirectIncome = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = Convert.ToDecimal(rw["Credit"].ToString());
                        dcIndirectIncome += dcBalance;
                    }
                }
                GrossProfitOrLoss = TotalRevenue - (PlusDirectExpenses - currentStockValue);
                profitOrLoss = (dcIndirectIncome + GrossProfitOrLoss) - OperatingExpenses;
            }
            catch (Exception ex)
            {
                Console.WriteLine( "GQ3:" + ex.Message);
            }
            //if (isForRetainedEarning)
            //{
                //toDate = toDate.AddYears(+1);
                //PublicVariables._dtFromDate = PublicVariables._dtFromDate.AddYears(+1);
                //PublicVariables._dtToDate = PublicVariables._dtToDate.AddYears(+1);
                //currentDate = currentDate.AddYears(+1);
                //isForRetainedEarning = false;
            //}
            return profitOrLoss;
        }
              public decimal profitOrLoss(DateTime toDate, string calculationMethod, bool isForRetainedEarning, int rollBack = 0)
        {
            decimal profitOrLoss = 0;
            DateTime currentDate = DateTime.Now;
            try
            {
                decimal TotalOpeningStockAndExpenses = 0;
                decimal TotalRevenue = 0;
                decimal OperatingExpenses = 0;
                decimal GrossProfitOrLoss = 0;
                decimal dcOpeningStock = 0;
                decimal currentStockValue = 0;
                DataSet dsetProfitAndLoss = new DataSet();
                DataTable dtbl = new DataTable();
                DataTable dtblFinancial = new DataTable();
                FinancialStatementSP SpFinance = new FinancialStatementSP();
                GeneralQueries methodForStockValue = new GeneralQueries();
                DBMatConnection conn = new DBMatConnection();
                if (isForRetainedEarning)
                {
                    //DateTime currFinalcialYearFrom = DateTime.Now;
                    //DateTime currFinalcialYearTo = DateTime.Now;
                    //if (rollBack == 1)
                    //{
                        //currFinalcialYearFrom = PublicVariables._dtFromDate;
                        //currFinalcialYearFrom = PublicVariables._dtToDate;
                    //}    
                    //toDate = PublicVariables._dtToDate.AddYears(-rollBack); // toDate.AddYears(-1);
                    PublicVariables._dtFromDate = PublicVariables._dtFromDate.AddYears(-rollBack);
                    PublicVariables._dtToDate = PublicVariables._dtToDate.AddYears(-rollBack);
                    currentDate = currentDate.AddYears(-1);
                }
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate = '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate = '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    lastFinYearEnd = Convert.ToDateTime(PublicVariables._dtFromDate).AddDays(-1);
                } 
                //lastFinYearEnd = new DateTime(2019, 12,31 );
                //PublicVariables._dtFromDate = new DateTime(2019, 1,1);
                //PublicVariables._dtToDate = new DateTime(2019, 12,31 );
                //toDate = new DateTime(2019, 12,31 );
                decimal OpeningStock = SpFinance.StockValueGetOnDate(toDate, calculationMethod, true, true);
                decimal dcOpeningStockForRollOver = SpFinance.StockValueGetOnDateForRollOver(DateTime.Parse(PublicVariables._dtToDate.ToString()), calculationMethod, false, false, true);
                OpeningStock = OpeningStock + dcOpeningStockForRollOver;
                decimal ClosingStock = methodForStockValue.currentStockValue(PublicVariables._dtFromDate, toDate, false, 0, false, currentDate);
                ClosingStock = ClosingStock - OpeningStock;
                dsetProfitAndLoss = SpFinance.ProfitAndLossAnalysis(PublicVariables._dtFromDate, toDate);

                dcOpeningStock = SpFinance.StockValueGetOnDate(PublicVariables._dtFromDate, toDate, calculationMethod, true, false);
                dcOpeningStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, false, 0, false, currentDate);
                dcOpeningStock = dcOpeningStock + dcOpeningStockForRollOver;
                currentStockValue = methodForStockValue.currentStockValue(lastFinYearStart, toDate, false, 0, false, currentDate); //this should work instaed   //

                decimal dcProfit = 0;
                for (int i = 0; i < dsetProfitAndLoss.Tables.Count; ++i)
                {
                    dtbl = dsetProfitAndLoss.Tables[i];
                    decimal dcSum = 0;
                    if (i == 0 || (i % 2) == 0)
                    {
                        if (dtbl.Rows.Count > 0)
                        {
                            dcSum = decimal.Parse(dtbl.Compute("Sum(Debit)", string.Empty).ToString());
                            dcProfit = dcProfit - dcSum;
                        }
                    }
                    else
                    {
                        if (dtbl.Rows.Count > 0)
                        {
                            dcSum = decimal.Parse(dtbl.Compute("Sum(Credit)", string.Empty).ToString());
                            dcProfit = dcProfit + dcSum;
                        }
                    }
                }
                TotalOpeningStockAndExpenses += dcOpeningStock;
                /// ---Purchases - Debit
                dtblFinancial = new DataTable();
                dtblFinancial = dsetProfitAndLoss.Tables[0];
                decimal dcPurchaseAccount = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = decimal.Parse(rw["Debit"].ToString().ToString());
                        dcPurchaseAccount += dcBalance;
                    }
                }
                TotalOpeningStockAndExpenses += dcPurchaseAccount;

                //---Sales Account  -Credit
                dtblFinancial = new DataTable();
                dtblFinancial = dsetProfitAndLoss.Tables[1];
                decimal dcSalesAccount = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = decimal.Parse(rw["Credit"].ToString().ToString());
                        dcSalesAccount += dcBalance;
                        TotalRevenue += dcBalance;
                    }
                }
                //----Direct Income 
                dtblFinancial = new DataTable();
                dtblFinancial = dsetProfitAndLoss.Tables[3];
                decimal dcDirectIncoome = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = Convert.ToDecimal(rw["Credit"].ToString());
                        dcDirectIncoome += dcBalance;
                        TotalRevenue += dcBalance;
                    }
                }
                decimal PlusDirectExpenses = TotalOpeningStockAndExpenses;

                ///------Indirect Expense 
                dtblFinancial = new DataTable();
                dtblFinancial = dsetProfitAndLoss.Tables[4];
                decimal dcIndirectExpense = 0;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = Convert.ToDecimal(rw["Debit"].ToString());
                        dcIndirectExpense += dcBalance;
                    }
                    OperatingExpenses += dcIndirectExpense;
                }
                //---Direct Expense
                PlusDirectExpenses = TotalOpeningStockAndExpenses;
                dtblFinancial = new DataTable();
                dtblFinancial = dsetProfitAndLoss.Tables[2];
                decimal dcDirectExpense = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = Convert.ToDecimal(rw["Debit"].ToString());
                        dcDirectExpense += dcBalance;
                        PlusDirectExpenses += dcBalance;
                    }
                }
                ///---Other Income 
                dtblFinancial = new DataTable();
                dtblFinancial = dsetProfitAndLoss.Tables[5];
                decimal dcIndirectIncome = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = Convert.ToDecimal(rw["Credit"].ToString());
                        dcIndirectIncome += dcBalance;
                    }
                }
                GrossProfitOrLoss = TotalRevenue - (PlusDirectExpenses - currentStockValue);
                profitOrLoss = (dcIndirectIncome + GrossProfitOrLoss) - OperatingExpenses;
            }
            catch (Exception ex)
            {
               // formMDI.infoError.ErrorString = "GQ3:" + ex.Message;
            }
            if (isForRetainedEarning)
            {
                toDate = toDate.AddYears(+1);
                PublicVariables._dtFromDate = PublicVariables._dtFromDate.AddYears(+1);
                PublicVariables._dtToDate = PublicVariables._dtToDate.AddYears(+1);
                currentDate = currentDate.AddYears(+1);
                isForRetainedEarning = false;
            }
            return profitOrLoss;
        }


        //public decimal profitOrLoss(DateTime toDate, string calculationMethod, bool isForRetainedEarning)
        //{
        //    decimal profitOrLoss = 0;
        //    DateTime currentDate = DateTime.Now;
        //    try
        //    {
        //        decimal TotalOpeningStockAndExpenses = 0;
        //        decimal TotalRevenue = 0;
        //        decimal OperatingExpenses = 0;
        //        decimal GrossProfitOrLoss = 0;
        //        decimal dcOpeningStock = 0;
        //        decimal currentStockValue = 0;
        //        DataSet dsetProfitAndLoss = new DataSet();
        //        DataTable dtbl = new DataTable();
        //        DataTable dtblFinancial = new DataTable();
        //        FinancialStatementSP SpFinance = new FinancialStatementSP();
        //        GeneralQueries methodForStockValue = new GeneralQueries();
        //        DBMatConnection conn = new DBMatConnection();
        //        if (isForRetainedEarning)
        //        {
        //            toDate = PublicVariables._dtToDate.AddYears(-1); // toDate.AddYears(-1);
        //            PublicVariables._dtFromDate = PublicVariables._dtFromDate.AddYears(-1);
        //            PublicVariables._dtToDate = PublicVariables._dtToDate.AddYears(-1);
        //            currentDate = currentDate.AddYears(-1);
        //        }
        //        string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
        //        string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
        //        DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
        //        DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
        //        if (conn.getSingleValue(queryStr) != string.Empty)
        //        {
        //            lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
        //        }
        //        else
        //        {
        //            lastFinYearEnd = Convert.ToDateTime(PublicVariables._dtFromDate).AddDays(-1);
        //        }
        //        decimal OpeningStock = SpFinance.StockValueGetOnDate(toDate, calculationMethod, true, true);
        //        decimal dcOpeningStockForRollOver = SpFinance.StockValueGetOnDateForRollOver(DateTime.Parse(PublicVariables._dtToDate.ToString()), calculationMethod, false, false, true);
        //        OpeningStock = OpeningStock + dcOpeningStockForRollOver;
        //        decimal ClosingStock = methodForStockValue.currentStockValue(PublicVariables._dtFromDate, toDate, false, 0, false, currentDate);
        //        ClosingStock = ClosingStock - OpeningStock;
        //        dsetProfitAndLoss = SpFinance.ProfitAndLossAnalysis(PublicVariables._dtFromDate, toDate);

        //        dcOpeningStock = SpFinance.StockValueGetOnDate(PublicVariables._dtFromDate, toDate, calculationMethod, true, false);
        //        dcOpeningStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, false, 0, false, currentDate);
        //        dcOpeningStock = dcOpeningStock + dcOpeningStockForRollOver;
        //        currentStockValue = methodForStockValue.currentStockValue(lastFinYearStart, toDate, false, 0, false, currentDate); //this should work instaed   //

        //        decimal dcProfit = 0;
        //        for (int i = 0; i < dsetProfitAndLoss.Tables.Count; ++i)
        //        {
        //            dtbl = dsetProfitAndLoss.Tables[i];
        //            decimal dcSum = 0;
        //            if (i == 0 || (i % 2) == 0)
        //            {
        //                if (dtbl.Rows.Count > 0)
        //                {
        //                    dcSum = decimal.Parse(dtbl.Compute("Sum(Debit)", string.Empty).ToString());
        //                    dcProfit = dcProfit - dcSum;
        //                }
        //            }
        //            else
        //            {
        //                if (dtbl.Rows.Count > 0)
        //                {
        //                    dcSum = decimal.Parse(dtbl.Compute("Sum(Credit)", string.Empty).ToString());
        //                    dcProfit = dcProfit + dcSum;
        //                }
        //            }
        //        }
        //        TotalOpeningStockAndExpenses += dcOpeningStock;
        //        /// ---Purchases - Debit
        //        dtblFinancial = new DataTable();
        //        dtblFinancial = dsetProfitAndLoss.Tables[0];
        //        decimal dcPurchaseAccount = 0m;
        //        if (dtblFinancial.Rows.Count > 0)
        //        {
        //            foreach (DataRow rw in dtblFinancial.Rows)
        //            {
        //                decimal dcBalance = decimal.Parse(rw["Debit"].ToString().ToString());
        //                dcPurchaseAccount += dcBalance;
        //            }
        //        }
        //        TotalOpeningStockAndExpenses += dcPurchaseAccount;

        //        //---Sales Account  -Credit
        //        dtblFinancial = new DataTable();
        //        dtblFinancial = dsetProfitAndLoss.Tables[1];
        //        decimal dcSalesAccount = 0m;
        //        if (dtblFinancial.Rows.Count > 0)
        //        {
        //            foreach (DataRow rw in dtblFinancial.Rows)
        //            {
        //                decimal dcBalance = decimal.Parse(rw["Credit"].ToString().ToString());
        //                dcSalesAccount += dcBalance;
        //                TotalRevenue += dcBalance;
        //            }
        //        }
        //        //----Direct Income 
        //        dtblFinancial = new DataTable();
        //        dtblFinancial = dsetProfitAndLoss.Tables[3];
        //        decimal dcDirectIncoome = 0m;
        //        if (dtblFinancial.Rows.Count > 0)
        //        {
        //            foreach (DataRow rw in dtblFinancial.Rows)
        //            {
        //                decimal dcBalance = Convert.ToDecimal(rw["Credit"].ToString());
        //                dcDirectIncoome += dcBalance;
        //                TotalRevenue += dcBalance;
        //            }
        //        }
        //        decimal PlusDirectExpenses = TotalOpeningStockAndExpenses;

        //        ///------Indirect Expense 
        //        dtblFinancial = new DataTable();
        //        dtblFinancial = dsetProfitAndLoss.Tables[4];
        //        decimal dcIndirectExpense = 0;
        //        if (dtblFinancial.Rows.Count > 0)
        //        {
        //            foreach (DataRow rw in dtblFinancial.Rows)
        //            {
        //                decimal dcBalance = Convert.ToDecimal(rw["Debit"].ToString());
        //                dcIndirectExpense += dcBalance;
        //            }
        //            OperatingExpenses += dcIndirectExpense;
        //        }
        //        //---Direct Expense
        //        PlusDirectExpenses = TotalOpeningStockAndExpenses;
        //        dtblFinancial = new DataTable();
        //        dtblFinancial = dsetProfitAndLoss.Tables[2];
        //        decimal dcDirectExpense = 0m;
        //        if (dtblFinancial.Rows.Count > 0)
        //        {
        //            foreach (DataRow rw in dtblFinancial.Rows)
        //            {
        //                decimal dcBalance = Convert.ToDecimal(rw["Debit"].ToString());
        //                dcDirectExpense += dcBalance;
        //                PlusDirectExpenses += dcBalance;
        //            }
        //        }
        //        ///---Other Income 
        //        dtblFinancial = new DataTable();
        //        dtblFinancial = dsetProfitAndLoss.Tables[5];
        //        decimal dcIndirectIncome = 0m;
        //        if (dtblFinancial.Rows.Count > 0)
        //        {
        //            foreach (DataRow rw in dtblFinancial.Rows)
        //            {
        //                decimal dcBalance = Convert.ToDecimal(rw["Credit"].ToString());
        //                dcIndirectIncome += dcBalance;
        //            }
        //        }
        //        GrossProfitOrLoss = TotalRevenue - (PlusDirectExpenses - currentStockValue);
        //        profitOrLoss = (dcIndirectIncome + GrossProfitOrLoss) - OperatingExpenses;
        //    }
        //    catch (Exception ex)
        //    {
        //        //formMDI.infoError.ErrorString = "GQ3:" + ex.Message;
        //    }
        //    if (isForRetainedEarning)
        //    {
        //        toDate = toDate.AddYears(+1);
        //        PublicVariables._dtFromDate = PublicVariables._dtFromDate.AddYears(+1);
        //        PublicVariables._dtToDate = PublicVariables._dtToDate.AddYears(+1);
        //        currentDate = currentDate.AddYears(+1);
        //        isForRetainedEarning = false;
        //    }
        //    return profitOrLoss;
        //}
        public decimal currentStockValue(DateTime FromDate, DateTime ToDate, bool isForCostOfSales, decimal expenseAccount, bool isForOpening, DateTime yearEndDATE)
        {
            decimal returnedStockValue = 0;
            decimal returnValue = 0;
            try
            {
                DBMatConnection conn = new DBMatConnection();
                StockPostingSP spstock = new StockPostingSP();
                DataTable dtbl = new DataTable();
                dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", FromDate, ToDate, "", "");

                if (isForCostOfSales && dtbl.Rows.Count > 0)
                {
                    if (isForOpening)
                    {
                        if (dtbl.AsEnumerable().Where(m => m.Field<DateTime>("date") <= yearEndDATE).Any())
                        {
                            dtbl = dtbl.AsEnumerable().Where(m => m.Field<DateTime>("date") <= yearEndDATE).Select(n => n).CopyToDataTable();
                        }
                    }
                    if (dtbl.AsEnumerable().Where(k => k.Field<decimal>("expenseAccount") == expenseAccount).Any())
                    {
                        dtbl = dtbl.AsEnumerable().Where(k => k.Field<decimal>("expenseAccount") == expenseAccount).Select(t => t).CopyToDataTable();
                    }
                    else
                    {
                        dtbl.Rows.Clear();
                    }
                }

                decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                decimal prevProductId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                string productName = "", storeName = "", productCode = "";
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal totalAssetValue = 0, qtyBalance = 0;
                decimal value1 = 0;
                int i = 0;
                bool isAgainstVoucher = false;

                foreach (DataRow dr in dtbl.Rows)
                {
                    currentProductId = Convert.ToDecimal(dr["productId"]);
                    currentGodownID = Convert.ToDecimal(dr["godownId"]);
                    productCode = dr["productCode"].ToString();
                    string voucherType = "", refNo = "";

                    decimal inwardQty = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == prevProductId
                                         select p.Field<decimal>("inwardQty")).Sum();
                    decimal outwardQty = (from p in dtbl.AsEnumerable()
                                          where p.Field<decimal>("productId") == prevProductId
                                          select p.Field<decimal>("outwardQty")).Sum();

                    inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                    outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                    decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                    string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                    rate2 = Convert.ToDecimal(dr["rate"]);
                    if (previousRunningAverage != 0 && currentProductId == prevProductId)
                    {
                        purchaseRate = previousRunningAverage;
                    }
                    if (currentProductId == prevProductId)
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        prevProductId = Convert.ToDecimal(dr["productId"]);
                        productCode = dr["productCode"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    else
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    //======================== 2017-02-27 =============================== //
                    refNo = dr["refNo"].ToString();
                    if (againstVoucherType != "NA")
                    {
                        refNo = dr["againstVoucherNo"].ToString();
                        isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                        againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                        //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                        string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                        string typeOfVoucher = conn.getSingleValue(queryString);
                        if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                        if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                    }
                    voucherType = dr["typeOfVoucher"].ToString();
                    //---------  COMMENT END ----------- //
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if (outwardQty2 > 0)
                    {
                        if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = purchaseRate;
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                        {
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue -= outwardQty2 * rate2;
                        }

                        else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Sales Order")
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                    }
                    else if (voucherType == "Sales Return" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Rejection In" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;

                        }
                        else
                        {
                            computedAverageRate = previousRunningAverage;//(stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                        // voucherType = "Sales Return";
                    }
                    else if (voucherType == "Delivery Note")
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                    }
                    else
                    {
                        // hndles other transactions such as purchase, opening balance, etc
                        qtyBal += inwardQty2 - outwardQty2;
                        stockValue += (inwardQty2 * rate2);
                    }
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        if (voucherType == "Sales Invoice")
                        {
                            stockValue = qtyBal * purchaseRate;
                        }
                        else
                        {
                            stockValue = qtyBal * rate2;
                        }
                        //totalAssetValue += Math.Round(value1, 2);
                        string intermediateValue = value1.ToString("N2");
                        totalAssetValue += Convert.ToDecimal(intermediateValue); // value1;
                        //i++;
                        returnValue = totalAssetValue;
                    }
                    // -----------------added 2017-02-21 -------------------- //
                    if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        if (voucherType == "Sales Invoice")
                        {
                            computedAverageRate = purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = rate2;
                        }
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        stockValue = qtyBal * computedAverageRate;
                    }
                    // ------------------------------------------------------ //

                    previousRunningAverage = computedAverageRate;

                    value1 = stockValue;
                    prevProductId = currentProductId;
                    prevGodownID = currentGodownID;

                    if (i == dtbl.Rows.Count - 1)
                    {
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                    {
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    i++;
                }
                returnedStockValue = Convert.ToDecimal(totalAssetValue + stockValue);
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ9:" + ex.Message;
            }
            return returnedStockValue;
        }
        public Tuple<decimal, decimal, decimal, decimal> currentStockValue(DateTime FromDate, DateTime ToDate, bool isForOpening, string currentProductCode, decimal store)
        {
            decimal returnedStockValue = 0, stockQuantity = 0, inwardQuantity = 0, outwardQuantity = 0, incomingwardQty = 0, outGoingwardQty = 0;
            try
            {
                decimal returnValue = 0;
                try
                {
                    DBMatConnection conn = new DBMatConnection();
                    StockPostingSP spstock = new StockPostingSP();
                    DataTable dtbl = new DataTable();
                    dtbl = spstock.StockReportDetailsGridFill(0, store, "All", FromDate, ToDate, currentProductCode, "");

                    decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                    decimal prevProductId = 0;
                    decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                    string productName = "", storeName = "", productCode = "";
                    decimal qtyBal = 0, stockValue = 0;
                    decimal computedAverageRate = 0, previousRunningAverage = 0;
                    decimal totalAssetValue = 0, qtyBalance = 0;
                    decimal value1 = 0;
                    int i = 0;
                    if (dtbl != null && dtbl.Rows.Count > 0)
                    {
                        incomingwardQty = (from j in dtbl.AsEnumerable()
                                           select j.Field<decimal>("inwardQty")).Sum();
                        outGoingwardQty = (from j in dtbl.AsEnumerable()
                                           select j.Field<decimal>("outwardQty")).Sum();
                        stockQuantity = incomingwardQty - outGoingwardQty;
                        inwardQuantity = incomingwardQty;
                        outwardQuantity = outGoingwardQty;
                    }

                    bool isAgainstVoucher = false;

                    foreach (DataRow dr in dtbl.Rows)
                    {
                        currentProductId = Convert.ToDecimal(dr["productId"]);
                        currentGodownID = Convert.ToDecimal(dr["godownId"]);
                        productCode = dr["productCode"].ToString();
                        string voucherType = "", refNo = "";

                        decimal inwardQty = (from p in dtbl.AsEnumerable()
                                             where p.Field<decimal>("productId") == prevProductId
                                             select p.Field<decimal>("inwardQty")).Sum();
                        decimal outwardQty = (from p in dtbl.AsEnumerable()
                                              where p.Field<decimal>("productId") == prevProductId
                                              select p.Field<decimal>("outwardQty")).Sum();

                        inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                        outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                        decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                        string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                        rate2 = Convert.ToDecimal(dr["rate"]);
                        if (previousRunningAverage != 0 && currentProductId == prevProductId)
                        {
                            purchaseRate = previousRunningAverage;
                        }
                        if (currentProductId == prevProductId)
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            prevProductId = Convert.ToDecimal(dr["productId"]);
                            productCode = dr["productCode"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        else
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        //======================== 2017-02-27 =============================== //
                        refNo = dr["refNo"].ToString();
                        if (againstVoucherType != "NA")
                        {
                            refNo = dr["againstVoucherNo"].ToString();
                            isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                            againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                            //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                            string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                            string typeOfVoucher = conn.getSingleValue(queryString);
                            if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                            if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                        }
                        voucherType = dr["typeOfVoucher"].ToString();
                        //---------  COMMENT END ----------- //
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if (outwardQty2 > 0)
                        {
                            if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = purchaseRate;
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                            {
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue -= outwardQty2 * rate2;
                            }

                            else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Sales Order")
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else
                            {
                                if (qtyBal != 0)
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                        }
                        else if (voucherType == "Sales Return" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Rejection In" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;

                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage;//(stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            // voucherType = "Sales Return";
                        }
                        else if (voucherType == "Delivery Note")
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                        }
                        else
                        {
                            // hndles other transactions such as purchase, opening balance, etc
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += (inwardQty2 * rate2);
                        }
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            if (voucherType == "Sales Invoice")
                            {
                                stockValue = qtyBal * purchaseRate;
                            }
                            else
                            {
                                stockValue = qtyBal * rate2;
                            }
                            //totalAssetValue += Math.Round(value1, 2);
                            string intermediateValue = value1.ToString("N2");
                            totalAssetValue += Convert.ToDecimal(intermediateValue); // value1;
                            //i++;
                            returnValue = totalAssetValue;
                        }
                        // -----------------added 2017-02-21 -------------------- //
                        if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            if (voucherType == "Sales Invoice")
                            {
                                computedAverageRate = purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = rate2;
                            }
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            stockValue = qtyBal * computedAverageRate;
                        }
                        // ------------------------------------------------------ //

                        previousRunningAverage = computedAverageRate;

                        value1 = stockValue;
                        prevProductId = currentProductId;
                        prevGodownID = currentGodownID;

                        if (i == dtbl.Rows.Count - 1)
                        {
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                        {
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        i++;
                    }
                    returnedStockValue = Convert.ToDecimal(totalAssetValue + stockValue);
                }
                catch (Exception ex)
                {
                    //formMDI.infoError.ErrorString = "GQ9:" + ex.Message;
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ10:" + ex.Message;
            }
            var turple = new Tuple<decimal, decimal, decimal, decimal>(returnedStockValue, stockQuantity, inwardQuantity, outwardQuantity);
            return turple;
            //return returnedStockValue;
        }




        public Tuple<decimal, decimal, decimal, decimal> currentStockValue12350(DateTime FromDate, DateTime ToDate, bool isForOpening, string currentProductCode, decimal store, DataTable dtblLast)
        {
            decimal returnedStockValue = 0, stockQuantity = 0, inwardQuantity = 0, outwardQuantity = 0, incomingwardQty = 0, outGoingwardQty = 0;
            try
            {
                decimal returnValue = 0;
                try
                {
                    DBMatConnection conn = new DBMatConnection();
                    StockPostingSP spstock = new StockPostingSP();
                    DataTable dtbl = new DataTable();
                    dtbl = dtblLast;
                        
                        //spstock.StockReportDetailsGridFill(0, store, "All", FromDate, ToDate, currentProductCode, "");

                    decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                    decimal prevProductId = 0;
                    decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                    string productName = "", storeName = "", productCode = "";
                    decimal qtyBal = 0, stockValue = 0;
                    decimal computedAverageRate = 0, previousRunningAverage = 0;
                    decimal totalAssetValue = 0, qtyBalance = 0;
                    decimal value1 = 0;
                    int i = 0;
                    if (dtbl != null && dtbl.Rows.Count > 0)
                    {
                        incomingwardQty = (from j in dtbl.AsEnumerable()
                                           select j.Field<decimal>("inwardQty")).Sum();
                        outGoingwardQty = (from j in dtbl.AsEnumerable()
                                           select j.Field<decimal>("outwardQty")).Sum();

                        if (isForOpening)
                        {
                            stockQuantity = incomingwardQty;
                            inwardQuantity = incomingwardQty;
                            outwardQuantity = 0;
                        }
                        else
                        {

                        }

                        stockQuantity = incomingwardQty - outGoingwardQty;
                        inwardQuantity = incomingwardQty;
                        outwardQuantity = outGoingwardQty;
                    }

                    bool isAgainstVoucher = false;

                    foreach (DataRow dr in dtbl.Rows)
                    {
                        currentProductId = Convert.ToDecimal(dr["productId"]);
                        currentGodownID = Convert.ToDecimal(dr["godownId"]);
                        productCode = dr["productCode"].ToString();
                        string voucherType = "", refNo = "";

                        decimal inwardQty = (from p in dtbl.AsEnumerable()
                                             where p.Field<decimal>("productId") == prevProductId
                                             select p.Field<decimal>("inwardQty")).Sum();
                        decimal outwardQty = (from p in dtbl.AsEnumerable()
                                              where p.Field<decimal>("productId") == prevProductId
                                              select p.Field<decimal>("outwardQty")).Sum();

                        inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                        outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                        decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                        string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                        rate2 = Convert.ToDecimal(dr["rate"]);
                        if (previousRunningAverage != 0 && currentProductId == prevProductId)
                        {
                            purchaseRate = previousRunningAverage;
                        }
                        if (currentProductId == prevProductId)
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            prevProductId = Convert.ToDecimal(dr["productId"]);
                            productCode = dr["productCode"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        else
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        //======================== 2017-02-27 =============================== //
                        refNo = dr["refNo"].ToString();
                        if (againstVoucherType != "NA")
                        {
                            refNo = dr["againstVoucherNo"].ToString();
                            isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                            againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                            //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                            string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                            string typeOfVoucher = conn.getSingleValue(queryString);
                            if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                            if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                        }
                        voucherType = dr["typeOfVoucher"].ToString();
                        //---------  COMMENT END ----------- //
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if (outwardQty2 > 0)
                        {
                            if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = purchaseRate;
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                            {
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue -= outwardQty2 * rate2;
                            }

                            else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Sales Order")
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else
                            {
                                if (qtyBal != 0)
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                        }
                        else if (voucherType == "Sales Return" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Rejection In" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;

                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage;//(stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            // voucherType = "Sales Return";
                        }
                        else if (voucherType == "Delivery Note")
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                        }
                        else
                        {
                            // hndles other transactions such as purchase, opening balance, etc
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += (inwardQty2 * rate2);
                        }
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            if (voucherType == "Sales Invoice")
                            {
                                stockValue = qtyBal * purchaseRate;
                            }
                            else
                            {
                                stockValue = qtyBal * rate2;
                            }
                            //totalAssetValue += Math.Round(value1, 2);
                            string intermediateValue = value1.ToString("N2");
                            totalAssetValue += Convert.ToDecimal(intermediateValue); // value1;
                            //i++;
                            returnValue = totalAssetValue;
                        }
                        // -----------------added 2017-02-21 -------------------- //
                        if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            if (voucherType == "Sales Invoice")
                            {
                                computedAverageRate = purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = rate2;
                            }
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            stockValue = qtyBal * computedAverageRate;
                        }
                        // ------------------------------------------------------ //

                        previousRunningAverage = computedAverageRate;

                        value1 = stockValue;
                        prevProductId = currentProductId;
                        prevGodownID = currentGodownID;

                        if (i == dtbl.Rows.Count - 1)
                        {
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                        {
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        i++;
                    }
                    returnedStockValue = Convert.ToDecimal(totalAssetValue + stockValue);
                }
                catch (Exception ex)
                {
                    //formMDI.infoError.ErrorString = "GQ9:" + ex.Message;
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ10:" + ex.Message;
            }
            var turple = new Tuple<decimal, decimal, decimal, decimal>(returnedStockValue, stockQuantity, inwardQuantity, outwardQuantity);
            return turple;
            //return returnedStockValue;
        }




        public Tuple<decimal, decimal, decimal, decimal> currentStockValue1235(DateTime FromDate, DateTime ToDate, bool isForOpening, string currentProductCode, decimal store)
        {
            decimal returnedStockValue = 0, stockQuantity = 0, inwardQuantity = 0, outwardQuantity = 0, incomingwardQty = 0, outGoingwardQty = 0;
            try
            {
                decimal returnValue = 0;
                try
                {
                    DBMatConnection conn = new DBMatConnection();
                    StockPostingSP spstock = new StockPostingSP();
                    DataTable dtbl = new DataTable();
                    dtbl = spstock.StockReportDetailsGridFill(0, store, "All", FromDate, ToDate, currentProductCode, "");

                    decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                    decimal prevProductId = 0;
                    decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                    string productName = "", storeName = "", productCode = "";
                    decimal qtyBal = 0, stockValue = 0;
                    decimal computedAverageRate = 0, previousRunningAverage = 0;
                    decimal totalAssetValue = 0, qtyBalance = 0;
                    decimal value1 = 0;
                    int i = 0;
                    if (dtbl != null && dtbl.Rows.Count > 0)
                    {
                        incomingwardQty = (from j in dtbl.AsEnumerable()
                                           select j.Field<decimal>("inwardQty")).Sum();
                        outGoingwardQty = (from j in dtbl.AsEnumerable()
                                           select j.Field<decimal>("outwardQty")).Sum();

                        if(isForOpening)
                        {
                            stockQuantity = incomingwardQty;
                            inwardQuantity = incomingwardQty;
                            outwardQuantity = 0;
                        }
                        else
                        {
                           
                        }

                        stockQuantity = incomingwardQty - outGoingwardQty;
                        inwardQuantity = incomingwardQty;
                        outwardQuantity = outGoingwardQty;
                    }

                    bool isAgainstVoucher = false;

                    foreach (DataRow dr in dtbl.Rows)
                    {
                        currentProductId = Convert.ToDecimal(dr["productId"]);
                        currentGodownID = Convert.ToDecimal(dr["godownId"]);
                        productCode = dr["productCode"].ToString();
                        string voucherType = "", refNo = "";

                        decimal inwardQty = (from p in dtbl.AsEnumerable()
                                             where p.Field<decimal>("productId") == prevProductId
                                             select p.Field<decimal>("inwardQty")).Sum();
                        decimal outwardQty = (from p in dtbl.AsEnumerable()
                                              where p.Field<decimal>("productId") == prevProductId
                                              select p.Field<decimal>("outwardQty")).Sum();

                        inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                        outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                        decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                        string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                        rate2 = Convert.ToDecimal(dr["rate"]);
                        if (previousRunningAverage != 0 && currentProductId == prevProductId)
                        {
                            purchaseRate = previousRunningAverage;
                        }
                        if (currentProductId == prevProductId)
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            prevProductId = Convert.ToDecimal(dr["productId"]);
                            productCode = dr["productCode"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        else
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        //======================== 2017-02-27 =============================== //
                        refNo = dr["refNo"].ToString();
                        if (againstVoucherType != "NA")
                        {
                            refNo = dr["againstVoucherNo"].ToString();
                            isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                            againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                            //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                            string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                            string typeOfVoucher = conn.getSingleValue(queryString);
                            if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                            if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                        }
                        voucherType = dr["typeOfVoucher"].ToString();
                        //---------  COMMENT END ----------- //
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if (outwardQty2 > 0)
                        {
                            if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = purchaseRate;
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                            {
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue -= outwardQty2 * rate2;
                            }

                            else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Sales Order")
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else
                            {
                                if (qtyBal != 0)
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                        }
                        else if (voucherType == "Sales Return" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Rejection In" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;

                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage;//(stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            // voucherType = "Sales Return";
                        }
                        else if (voucherType == "Delivery Note")
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                        }
                        else
                        {
                            // hndles other transactions such as purchase, opening balance, etc
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += (inwardQty2 * rate2);
                        }
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            if (voucherType == "Sales Invoice")
                            {
                                stockValue = qtyBal * purchaseRate;
                            }
                            else
                            {
                                stockValue = qtyBal * rate2;
                            }
                            //totalAssetValue += Math.Round(value1, 2);
                            string intermediateValue = value1.ToString("N2");
                            totalAssetValue += Convert.ToDecimal(intermediateValue); // value1;
                            //i++;
                            returnValue = totalAssetValue;
                        }
                        // -----------------added 2017-02-21 -------------------- //
                        if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            if (voucherType == "Sales Invoice")
                            {
                                computedAverageRate = purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = rate2;
                            }
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            stockValue = qtyBal * computedAverageRate;
                        }
                        // ------------------------------------------------------ //

                        previousRunningAverage = computedAverageRate;

                        value1 = stockValue;
                        prevProductId = currentProductId;
                        prevGodownID = currentGodownID;

                        if (i == dtbl.Rows.Count - 1)
                        {
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                        {
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        i++;
                    }
                    returnedStockValue = Convert.ToDecimal(totalAssetValue + stockValue);
                }
                catch (Exception ex)
                {
                    //formMDI.infoError.ErrorString = "GQ9:" + ex.Message;
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ10:" + ex.Message;
            }
            var turple = new Tuple<decimal, decimal, decimal, decimal>(returnedStockValue, stockQuantity, inwardQuantity, outwardQuantity);
            return turple;
            //return returnedStockValue;
        }





        public decimal currentStockValue99(DateTime FromDate, DateTime ToDate, bool isForCostOfSales, decimal expenseAccount, bool isForOpening, DateTime yearEndDATE)
        {
            decimal returnedStockValue = 0;
            decimal returnValue = 0;
            try
            {
                DBMatConnection conn = new DBMatConnection();
                StockPostingSP spstock = new StockPostingSP();
                DataTable dtbl = new DataTable();
                dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", FromDate, ToDate, "", "");

                if (isForCostOfSales && dtbl.Rows.Count > 0)
                {
                    if (isForOpening)
                    {
                        if (dtbl.AsEnumerable().Where(m => m.Field<DateTime>("date") <= yearEndDATE).Any())
                        {
                            dtbl = dtbl.AsEnumerable().Where(m => m.Field<DateTime>("date") <= yearEndDATE).Select(n => n).CopyToDataTable();
                        }
                    }
                    if (dtbl.AsEnumerable().Where(k => k.Field<decimal>("expenseAccount") == expenseAccount).Any())
                    {
                        dtbl = dtbl.AsEnumerable().Where(k => k.Field<decimal>("expenseAccount") == expenseAccount).Select(t => t).CopyToDataTable();
                    }
                    else
                    {
                        dtbl.Rows.Clear();
                    }
                }

                decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                decimal prevProductId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                string productName = "", storeName = "", productCode = "";
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal totalAssetValue = 0, qtyBalance = 0;
                decimal value1 = 0;
                int i = 0;
                bool isAgainstVoucher = false;

                foreach (DataRow dr in dtbl.Rows)
                {
                    currentProductId = Convert.ToDecimal(dr["productId"]);
                    currentGodownID = Convert.ToDecimal(dr["godownId"]);
                    productCode = dr["productCode"].ToString();
                    string voucherType = "", refNo = "";

                    decimal inwardQty = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == prevProductId
                                         select p.Field<decimal>("inwardQty")).Sum();
                    decimal outwardQty = (from p in dtbl.AsEnumerable()
                                          where p.Field<decimal>("productId") == prevProductId
                                          select p.Field<decimal>("outwardQty")).Sum();

                    inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                    outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                    decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                    string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                    rate2 = Convert.ToDecimal(dr["rate"]);
                    if (previousRunningAverage != 0 && currentProductId == prevProductId)
                    {
                        purchaseRate = previousRunningAverage;
                    }
                    if (currentProductId == prevProductId)
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        prevProductId = Convert.ToDecimal(dr["productId"]);
                        productCode = dr["productCode"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    else
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    //======================== 2017-02-27 =============================== //
                    refNo = dr["refNo"].ToString();
                    if (againstVoucherType != "NA")
                    {
                        refNo = dr["againstVoucherNo"].ToString();
                        isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                        againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                        //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                        string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                        string typeOfVoucher = conn.getSingleValue(queryString);
                        if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                        if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                    }
                    voucherType = dr["typeOfVoucher"].ToString();
                    //---------  COMMENT END ----------- //
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if (outwardQty2 > 0)
                    {
                        if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = purchaseRate;
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                        {
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue -= outwardQty2 * rate2;
                        }

                        else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Sales Order")
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                    }
                    else if (voucherType == "Sales Return" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Rejection In" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;

                        }
                        else
                        {
                            computedAverageRate = previousRunningAverage;//(stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                        // voucherType = "Sales Return";
                    }
                    else if (voucherType == "Delivery Note")
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                    }
                    else
                    {
                        // hndles other transactions such as purchase, opening balance, etc
                        qtyBal += inwardQty2 - outwardQty2;
                        stockValue += (inwardQty2 * rate2);
                    }
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        if (voucherType == "Sales Invoice")
                        {
                            stockValue = qtyBal * purchaseRate;
                        }
                        else
                        {
                            stockValue = qtyBal * rate2;
                        }
                        //totalAssetValue += Math.Round(value1, 2);
                        string intermediateValue = value1.ToString("N2");
                        totalAssetValue += Convert.ToDecimal(intermediateValue); // value1;
                        //i++;
                        returnValue = totalAssetValue;
                    }
                    // -----------------added 2017-02-21 -------------------- //
                    if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        if (voucherType == "Sales Invoice")
                        {
                            computedAverageRate = purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = rate2;
                        }
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        stockValue = qtyBal * computedAverageRate;
                    }
                    // ------------------------------------------------------ //

                    previousRunningAverage = computedAverageRate;

                    value1 = stockValue;
                    prevProductId = currentProductId;
                    prevGodownID = currentGodownID;

                    if (i == dtbl.Rows.Count - 1)
                    {
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                    {
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    i++;
                }
                returnedStockValue = Convert.ToDecimal(totalAssetValue + stockValue);
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ9:" + ex.Message;
            }
            return returnedStockValue;
        }

        public Tuple<decimal, decimal, decimal, decimal> currentStockValue89(DateTime FromDate, DateTime ToDate, bool isForOpening, string currentProductCode, decimal store)
        {
            decimal returnedStockValue = 0, stockQuantity = 0, inwardQuantity = 0, outwardQuantity = 0, incomingwardQty = 0, outGoingwardQty = 0;
            try
            {
                decimal returnValue = 0;
                try
                {
                    DBMatConnection conn = new DBMatConnection();
                    StockPostingSP spstock = new StockPostingSP();
                    DataTable dtbl = new DataTable();
                    dtbl = spstock.StockReportDetailsGridFill(0, store, "All", FromDate, ToDate, currentProductCode, "");

                    decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                    decimal prevProductId = 0;
                    decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                    string productName = "", storeName = "", productCode = "";
                    decimal qtyBal = 0, stockValue = 0;
                    decimal computedAverageRate = 0, previousRunningAverage = 0;
                    decimal totalAssetValue = 0, qtyBalance = 0;
                    decimal value1 = 0;
                    int i = 0;
                    if (dtbl != null && dtbl.Rows.Count > 0)
                    {
                        incomingwardQty = (from j in dtbl.AsEnumerable()
                                           select j.Field<decimal>("inwardQty")).Sum();
                        outGoingwardQty = (from j in dtbl.AsEnumerable()
                                           select j.Field<decimal>("outwardQty")).Sum();
                        stockQuantity = incomingwardQty - outGoingwardQty;
                        inwardQuantity = incomingwardQty;
                        outwardQuantity = outGoingwardQty;
                    }

                    bool isAgainstVoucher = false;

                    foreach (DataRow dr in dtbl.Rows)
                    {
                        currentProductId = Convert.ToDecimal(dr["productId"]);
                        currentGodownID = Convert.ToDecimal(dr["godownId"]);
                        productCode = dr["productCode"].ToString();
                        string voucherType = "", refNo = "";

                        decimal inwardQty = (from p in dtbl.AsEnumerable()
                                             where p.Field<decimal>("productId") == prevProductId
                                             select p.Field<decimal>("inwardQty")).Sum();
                        decimal outwardQty = (from p in dtbl.AsEnumerable()
                                              where p.Field<decimal>("productId") == prevProductId
                                              select p.Field<decimal>("outwardQty")).Sum();

                        inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                        outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                        decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                        string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                        rate2 = Convert.ToDecimal(dr["rate"]);
                        if (previousRunningAverage != 0 && currentProductId == prevProductId)
                        {
                            purchaseRate = previousRunningAverage;
                        }
                        if (currentProductId == prevProductId)
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            prevProductId = Convert.ToDecimal(dr["productId"]);
                            productCode = dr["productCode"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        else
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        //======================== 2017-02-27 =============================== //
                        refNo = dr["refNo"].ToString();
                        if (againstVoucherType != "NA")
                        {
                            refNo = dr["againstVoucherNo"].ToString();
                            isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                            againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                            //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                            string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                            string typeOfVoucher = conn.getSingleValue(queryString);
                            if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                            if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                        }
                        voucherType = dr["typeOfVoucher"].ToString();
                        //---------  COMMENT END ----------- //
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if (outwardQty2 > 0)
                        {
                            if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = purchaseRate;
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                            {
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue -= outwardQty2 * rate2;
                            }

                            else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Sales Order")
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else
                            {
                                if (qtyBal != 0)
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                        }
                        else if (voucherType == "Sales Return" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Rejection In" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;

                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage;//(stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            // voucherType = "Sales Return";
                        }
                        else if (voucherType == "Delivery Note")
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                        }
                        else
                        {
                            // hndles other transactions such as purchase, opening balance, etc
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += (inwardQty2 * rate2);
                        }
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            if (voucherType == "Sales Invoice")
                            {
                                stockValue = qtyBal * purchaseRate;
                            }
                            else
                            {
                                stockValue = qtyBal * rate2;
                            }
                            //totalAssetValue += Math.Round(value1, 2);
                            string intermediateValue = value1.ToString("N2");
                            totalAssetValue += Convert.ToDecimal(intermediateValue); // value1;
                            //i++;
                            returnValue = totalAssetValue;
                        }
                        // -----------------added 2017-02-21 -------------------- //
                        if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            if (voucherType == "Sales Invoice")
                            {
                                computedAverageRate = purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = rate2;
                            }
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            stockValue = qtyBal * computedAverageRate;
                        }
                        // ------------------------------------------------------ //

                        previousRunningAverage = computedAverageRate;

                        value1 = stockValue;
                        prevProductId = currentProductId;
                        prevGodownID = currentGodownID;

                        if (i == dtbl.Rows.Count - 1)
                        {
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                        {
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        i++;
                    }
                    returnedStockValue = Convert.ToDecimal(totalAssetValue + stockValue);
                }
                catch (Exception ex)
                {
                    //formMDI.infoError.ErrorString = "GQ9:" + ex.Message;
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ10:" + ex.Message;
            }
            var turple = new Tuple<decimal, decimal, decimal, decimal>(returnedStockValue, stockQuantity, inwardQuantity, outwardQuantity);
            return turple;
            //return returnedStockValue;
        }


        public Tuple<decimal, decimal, decimal, decimal> currentStockValue5(DateTime FromDate, DateTime ToDate, bool isForOpening, string currentProductCode, decimal store)
        {
            decimal returnedStockValue = 0, stockQuantity = 0, inwardQuantity = 0, outwardQuantity = 0, incomingwardQty = 0, outGoingwardQty = 0;
            try
            {
                decimal returnValue = 0;
                try
                {
                    DBMatConnection conn = new DBMatConnection();
                    StockPostingSP spstock = new StockPostingSP();
                    DataTable dtbl = new DataTable();
                    dtbl = spstock.StockReportDetailsGridFill(0, store, "All", FromDate, ToDate, currentProductCode, "");

                    decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                    decimal prevProductId = 0;
                    decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                    string productName = "", storeName = "", productCode = "";
                    decimal qtyBal = 0, stockValue = 0;
                    decimal computedAverageRate = 0, previousRunningAverage = 0;
                    decimal totalAssetValue = 0, qtyBalance = 0;
                    decimal value1 = 0;
                    int i = 0;
                    if (dtbl != null && dtbl.Rows.Count > 0)
                    {
                        incomingwardQty = (from j in dtbl.AsEnumerable()
                                           select j.Field<decimal>("inwardQty")).Sum();
                        outGoingwardQty = (from j in dtbl.AsEnumerable()
                                           select j.Field<decimal>("outwardQty")).Sum();
                        stockQuantity = incomingwardQty - outGoingwardQty;
                        inwardQuantity = incomingwardQty;
                        outwardQuantity = outGoingwardQty;
                    }

                    bool isAgainstVoucher = false;

                    foreach (DataRow dr in dtbl.Rows)
                    {
                        currentProductId = Convert.ToDecimal(dr["productId"]);
                        currentGodownID = Convert.ToDecimal(dr["godownId"]);
                        productCode = dr["productCode"].ToString();
                        string voucherType = "", refNo = "";

                        decimal inwardQty = (from p in dtbl.AsEnumerable()
                                             where p.Field<decimal>("productId") == prevProductId
                                             select p.Field<decimal>("inwardQty")).Sum();
                        decimal outwardQty = (from p in dtbl.AsEnumerable()
                                              where p.Field<decimal>("productId") == prevProductId
                                              select p.Field<decimal>("outwardQty")).Sum();

                        inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                        outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                        decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                        string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                        rate2 = Convert.ToDecimal(dr["rate"]);
                        if (previousRunningAverage != 0 && currentProductId == prevProductId)
                        {
                            purchaseRate = previousRunningAverage;
                        }
                        if (currentProductId == prevProductId)
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            prevProductId = Convert.ToDecimal(dr["productId"]);
                            productCode = dr["productCode"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        else
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        //======================== 2017-02-27 =============================== //
                        refNo = dr["refNo"].ToString();
                        if (againstVoucherType != "NA")
                        {
                            refNo = dr["againstVoucherNo"].ToString();
                            isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                            againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                            //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                            string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                            string typeOfVoucher = conn.getSingleValue(queryString);
                            if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                            if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                        }
                        voucherType = dr["typeOfVoucher"].ToString();
                        //---------  COMMENT END ----------- //
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if (outwardQty2 > 0)
                        {
                            if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = purchaseRate;
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                            {
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue -= outwardQty2 * rate2;
                            }

                            else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Sales Order")
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else
                            {
                                if (qtyBal != 0)
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                        }
                        else if (voucherType == "Sales Return" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Rejection In" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;

                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage;//(stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            // voucherType = "Sales Return";
                        }
                        else if (voucherType == "Delivery Note")
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                        }
                        else
                        {
                            // hndles other transactions such as purchase, opening balance, etc
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += (inwardQty2 * rate2);
                        }
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            if (voucherType == "Sales Invoice")
                            {
                                stockValue = qtyBal * purchaseRate;
                            }
                            else
                            {
                                stockValue = qtyBal * rate2;
                            }
                            //totalAssetValue += Math.Round(value1, 2);
                            string intermediateValue = value1.ToString("N2");
                            totalAssetValue += Convert.ToDecimal(intermediateValue); // value1;
                            //i++;
                            returnValue = totalAssetValue;
                        }
                        // -----------------added 2017-02-21 -------------------- //
                        if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            if (voucherType == "Sales Invoice")
                            {
                                computedAverageRate = purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = rate2;
                            }
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            stockValue = qtyBal * computedAverageRate;
                        }
                        // ------------------------------------------------------ //

                        previousRunningAverage = computedAverageRate;

                        value1 = stockValue;
                        prevProductId = currentProductId;
                        prevGodownID = currentGodownID;

                        if (i == dtbl.Rows.Count - 1)
                        {
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                        {
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        i++;
                    }
                    returnedStockValue = Convert.ToDecimal(totalAssetValue + stockValue);
                }
                catch (Exception ex)
                {
                    //formMDI.infoError.ErrorString = "GQ9:" + ex.Message;
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ10:" + ex.Message;
            }
            var turple = new Tuple<decimal, decimal, decimal, decimal>(returnedStockValue, stockQuantity, inwardQuantity, outwardQuantity);
            return turple;
            //return returnedStockValue;
        }
        public Tuple<decimal, decimal, decimal, decimal> currentStockValue1(DateTime FromDate, DateTime ToDate, bool isForOpening, string currentProductCode, decimal store)
        {
            decimal returnedStockValue = 0, stockQuantity = 0, inwardQuantity = 0, outwardQuantity = 0, incomingwardQty = 0, outGoingwardQty = 0;
            try
            {
                decimal returnValue = 0;
                try
                {
                    DBMatConnection conn = new DBMatConnection();
                    StockPostingSP spstock = new StockPostingSP();
                    DataTable dtbl = new DataTable();
                    dtbl = spstock.StockReportDetailsGridFill(0, store, "All", FromDate, ToDate, currentProductCode, "");

                    decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                    decimal prevProductId = 0;
                    decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                    string productName = "", storeName = "", productCode = "";
                    decimal qtyBal = 0, stockValue = 0;
                    decimal computedAverageRate = 0, previousRunningAverage = 0;
                    decimal totalAssetValue = 0, qtyBalance = 0;
                    decimal value1 = 0;
                    int i = 0;
                    if (dtbl != null && dtbl.Rows.Count > 0)
                    {
                        incomingwardQty = (from j in dtbl.AsEnumerable()
                                           select j.Field<decimal>("inwardQty")).Sum();
                        outGoingwardQty = (from j in dtbl.AsEnumerable()
                                           select j.Field<decimal>("outwardQty")).Sum();
                        stockQuantity = incomingwardQty - outGoingwardQty;
                        inwardQuantity = incomingwardQty;
                        outwardQuantity = outGoingwardQty;
                    }

                    bool isAgainstVoucher = false;

                    foreach (DataRow dr in dtbl.Rows)
                    {
                        currentProductId = Convert.ToDecimal(dr["productId"]);
                        currentGodownID = Convert.ToDecimal(dr["godownId"]);
                        productCode = dr["productCode"].ToString();
                        string voucherType = "", refNo = "";

                        decimal inwardQty = (from p in dtbl.AsEnumerable()
                                             where p.Field<decimal>("productId") == prevProductId
                                             select p.Field<decimal>("inwardQty")).Sum();
                        decimal outwardQty = (from p in dtbl.AsEnumerable()
                                              where p.Field<decimal>("productId") == prevProductId
                                              select p.Field<decimal>("outwardQty")).Sum();

                        inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                        outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                        decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                        string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                        rate2 = Convert.ToDecimal(dr["rate"]);
                        if (previousRunningAverage != 0 && currentProductId == prevProductId)
                        {
                            purchaseRate = previousRunningAverage;
                        }
                        if (currentProductId == prevProductId)
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            prevProductId = Convert.ToDecimal(dr["productId"]);
                            productCode = dr["productCode"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        else
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        //======================== 2017-02-27 =============================== //
                        refNo = dr["refNo"].ToString();
                        if (againstVoucherType != "NA")
                        {
                            refNo = dr["againstVoucherNo"].ToString();
                            isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                            againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                            //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                            string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                            string typeOfVoucher = conn.getSingleValue(queryString);
                            if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                            if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                        }
                        voucherType = dr["typeOfVoucher"].ToString();
                        //---------  COMMENT END ----------- //
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if (outwardQty2 > 0)
                        {
                            if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = purchaseRate;
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                            {
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue -= outwardQty2 * rate2;
                            }

                            else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Sales Order")
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else
                            {
                                if (qtyBal != 0)
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                        }
                        else if (voucherType == "Sales Return" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Rejection In" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;

                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage;//(stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            // voucherType = "Sales Return";
                        }
                        else if (voucherType == "Delivery Note")
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                        }
                        else
                        {
                            // hndles other transactions such as purchase, opening balance, etc
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += (inwardQty2 * rate2);
                        }
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            if (voucherType == "Sales Invoice")
                            {
                                stockValue = qtyBal * purchaseRate;
                            }
                            else
                            {
                                stockValue = qtyBal * rate2;
                            }
                            //totalAssetValue += Math.Round(value1, 2);
                            string intermediateValue = value1.ToString("N2");
                            totalAssetValue += Convert.ToDecimal(intermediateValue); // value1;
                            //i++;
                            returnValue = totalAssetValue;
                        }
                        // -----------------added 2017-02-21 -------------------- //
                        if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            if (voucherType == "Sales Invoice")
                            {
                                computedAverageRate = purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = rate2;
                            }
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            stockValue = qtyBal * computedAverageRate;
                        }
                        // ------------------------------------------------------ //

                        previousRunningAverage = computedAverageRate;

                        value1 = stockValue;
                        prevProductId = currentProductId;
                        prevGodownID = currentGodownID;

                        if (i == dtbl.Rows.Count - 1)
                        {
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                        {
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        i++;
                    }
                    returnedStockValue = Convert.ToDecimal(totalAssetValue + stockValue);
                }
                catch (Exception ex)
                {
                    //formMDI.infoError.ErrorString = "GQ9:" + ex.Message;
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ10:" + ex.Message;
            }
            var turple = new Tuple<decimal, decimal, decimal, decimal>(returnedStockValue, stockQuantity, inwardQuantity, outwardQuantity);
            return turple;
            //return returnedStockValue;
        }
        public decimal lastBankReconciliation(decimal ledgerId, DateTime fromDate, DateTime toDate)
        {
            decimal lastReconciledAmount = 0;
            try
            {
                BankReconciliationSP spBankReconciliation = new BankReconciliationSP();
                DataTable dtblBankReconciled = new DataTable();
                DataTable dtblBankUnreconciled = new DataTable();
                decimal totalWithdrawal = 0;
                decimal totalDeposit = 0;
                decimal bankBalance = 0;
                dtblBankReconciled = spBankReconciliation.BankReconciliationFillReconcile(ledgerId, fromDate, toDate);
                dtblBankUnreconciled = spBankReconciliation.BankReconciliationUnrecocile(ledgerId, fromDate, toDate, true);
                if (dtblBankReconciled.Rows.Count > 0)
                {
                    var deposits = (from i in dtblBankReconciled.AsEnumerable()
                                    select i.Field<decimal>("debit")).Sum();
                    deposits += (from i in dtblBankUnreconciled.AsEnumerable()
                                 select i.Field<decimal>("debit")).Sum();
                    var withDrawals = (from i in dtblBankReconciled.AsEnumerable()
                                       select i.Field<decimal>("credit")).Sum();
                    withDrawals += (from i in dtblBankUnreconciled.AsEnumerable()
                                    select i.Field<decimal>("credit")).Sum();
                    bankBalance += (deposits - withDrawals);
                }
                if (dtblBankUnreconciled.Rows.Count > 0)
                {
                    var zeroValue = new List<decimal> { 0 };
                    var unpresentedCheques = dtblBankUnreconciled.AsEnumerable().Where(l => !zeroValue.Contains(l.Field<decimal>("debit"))).CopyToDataTable();
                    var uncreditedCheques = dtblBankUnreconciled.AsEnumerable().Where(l => !zeroValue.Contains(l.Field<decimal>("credit"))).CopyToDataTable();
                    totalWithdrawal = (from j in uncreditedCheques.AsEnumerable()
                                       select j.Field<decimal>("credit")).Sum();
                    bankBalance += totalWithdrawal;
                    totalDeposit = (from j in unpresentedCheques.AsEnumerable()
                                    select j.Field<decimal>("debit")).Sum();
                }
                bankBalance = bankBalance - totalDeposit;
                lastReconciledAmount = bankBalance + previousBankReconciliations(ledgerId, PublicVariables._dtFromDate, fromDate.AddDays(-1)); 
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ4:" + ex.Message;
            }
            return lastReconciledAmount;
        }
        public decimal previousBankReconciliations(decimal ledgerId, DateTime fromDate, DateTime toDate)
        {
            decimal previousBankReconciliations = 0;
            try
            {
                BankReconciliationSP spBankReconciliation = new BankReconciliationSP();
                DataTable dtblBankReconciled = new DataTable();
                DataTable dtblBankUnreconciled = new DataTable();
                decimal totalWithdrawal = 0;
                decimal totalDeposit = 0;
                decimal bankBalance = 0;
                dtblBankReconciled = spBankReconciliation.BankReconciliationFillReconcile(ledgerId, fromDate, toDate);
                dtblBankUnreconciled = spBankReconciliation.BankReconciliationUnrecocile(ledgerId, fromDate, toDate, true);
                if (dtblBankReconciled.Rows.Count > 0)
                {
                    var deposits = (from i in dtblBankReconciled.AsEnumerable()
                                    select i.Field<decimal>("debit")).Sum();
                    deposits += (from i in dtblBankUnreconciled.AsEnumerable()
                                 select i.Field<decimal>("debit")).Sum();
                    var withDrawals = (from i in dtblBankReconciled.AsEnumerable()
                                       select i.Field<decimal>("credit")).Sum();
                    withDrawals += (from i in dtblBankUnreconciled.AsEnumerable()
                                    select i.Field<decimal>("credit")).Sum();
                    bankBalance += (deposits - withDrawals);
                }
                if (dtblBankUnreconciled.Rows.Count > 0)
                {
                    var zeroValue = new List<decimal> { 0 };
                    var unpresentedCheques = dtblBankUnreconciled.AsEnumerable().Where(l => !zeroValue.Contains(l.Field<decimal>("debit"))).CopyToDataTable();
                    var uncreditedCheques = dtblBankUnreconciled.AsEnumerable().Where(l => !zeroValue.Contains(l.Field<decimal>("credit"))).CopyToDataTable();
                    totalWithdrawal = (from j in uncreditedCheques.AsEnumerable()
                                       select j.Field<decimal>("credit")).Sum();
                    bankBalance += totalWithdrawal;
                    totalDeposit = (from j in unpresentedCheques.AsEnumerable()
                                    select j.Field<decimal>("debit")).Sum();
                }
                bankBalance = bankBalance - totalDeposit;
                previousBankReconciliations = bankBalance;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ4:" + ex.Message;
            }
            return previousBankReconciliations;
        }

    }
}