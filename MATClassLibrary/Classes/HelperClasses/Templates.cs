﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using iTextSharp.text;
using iTextSharp.text.pdf;
using iTextSharp.text.html.simpleparser;
using System.IO;
using System.Data;
using System.Web.UI;
using System.Windows.Forms;
using System.Net.Mail;
using System.Net;

namespace MATFinancials.Classes.HelperClasses
{
    public class Templates
    {
        bool isSend = false, isCheck = false;
        //frmLoading frmLodingObj = new frmLoading();
        public Templates()
        {
            DataSet ds = new DataSet();
        }

        public void generateInvoicePDF(DataSet dsPDFDetails, decimal partyId, string invoiceType, ref invoiceEmailDetails pinvoiceEmailDetails)
        {
            string totalAmount = "";
            string taxBillOnAmount = "";
            string billDiscount = "";
            string appliedAmount = "";
            string grandTotal = "";
            string invoiceNumber = "";
            string voucherType = "";
            string amountInWords = "";
            string ledgerName = "", partyType = "";
            string partyAddress = "";
            DataTable dtCompanyDetails = new DataTable();
            DataTable dtMaster = new DataTable();
            DataTable dtDetails = new DataTable();
            dtCompanyDetails = dsPDFDetails.Tables[0];
            dtMaster = dsPDFDetails.Tables[1];
            dtDetails = dsPDFDetails.Tables[2];

            // Customize details for sales invoice
            if (invoiceType == "sales")
            {
                // remove unwanted columns from sales details and master table
                dtDetails.Columns.Remove("Column1");
                dtDetails.Columns.Remove("AppliedAmount");
                dtDetails.Columns.Remove("Batch");
                dtDetails.Columns.Remove("discount");
                dtMaster.Columns.Remove("CAddress");

                totalAmount = Convert.ToDecimal(dtMaster.Rows[0]["totalAmount"]).ToString("N2");
                amountInWords = new NumToText().AmountWords(Convert.ToDecimal(totalAmount), PublicVariables._decCurrencyId);
                taxBillOnAmount = Convert.ToDecimal(dtMaster.Rows[0]["taxAmtOnBill"]).ToString("N2");
                billDiscount = Convert.ToDecimal(dtMaster.Rows[0]["billDiscount"]).ToString("N2");
                appliedAmount = Convert.ToDecimal(dtMaster.Rows[0]["taxAmtOnBill"]).ToString("N2");
                grandTotal = Convert.ToDecimal(dtMaster.Rows[0]["grandTotal"]).ToString("N2");
                invoiceNumber = dtMaster.Rows[0]["SalesInvoiceNo"].ToString();
                ledgerName = dtMaster.Rows[0]["ledgerName"].ToString();
                voucherType = "SALES INVOICE";
                partyType = "Customer";
            }
            if (invoiceType == "purchase")
            {
                totalAmount = Convert.ToDecimal(dtMaster.Rows[0]["totalAmount"]).ToString("N2");
                amountInWords = new NumToText().AmountWords(Convert.ToDecimal(totalAmount), PublicVariables._decCurrencyId);
                taxBillOnAmount = Convert.ToDecimal(dtMaster.Rows[0]["taxAmount"]).ToString("N2");
                billDiscount = Convert.ToDecimal(dtMaster.Rows[0]["Discount"]).ToString("N2");
                //appliedAmount = Convert.ToDecimal(dtMaster.Rows[0]["taxAmtOnBill"]).ToString("N2");
                grandTotal = Convert.ToDecimal(dtMaster.Rows[0]["grandTotal"]).ToString("N2");
                invoiceNumber = dtMaster.Rows[0]["voucherNo"].ToString();
                ledgerName = dtMaster.Rows[0]["ledgerName"].ToString();
                voucherType = "PURCHASE INVOICE";
                partyType = "Supplier";
            }
            if (invoiceType == "sales quotation")
            {
                totalAmount = Convert.ToDecimal(dtMaster.Rows[0]["totalAmount"]).ToString("N2");
                taxBillOnAmount = Convert.ToDecimal(dtMaster.Rows[0]["taxAmount"]).ToString("N2");
                totalAmount = (Convert.ToDecimal(totalAmount) - Convert.ToDecimal(taxBillOnAmount)).ToString();
                grandTotal = Convert.ToDecimal(dtMaster.Rows[0]["totalAmount"]).ToString("N2");
                amountInWords = new NumToText().AmountWords(Convert.ToDecimal(grandTotal), PublicVariables._decCurrencyId);
                invoiceNumber = dtMaster.Rows[0]["invoiceNo"].ToString();
                ledgerName = dtMaster.Rows[0]["ledgerName"].ToString();
                voucherType = "SALES QUOTATION";
                partyType = "Customer";
            }
            if (invoiceType == "sales order")
            {   
                // remove unwanted columns from sales details and master table
                dtDetails.Columns.Remove("PNarration");
                dtDetails.Columns.Remove("batchNo");

                totalAmount = Convert.ToDecimal(dtMaster.Rows[0]["totalAmount"]).ToString("N2");
                taxBillOnAmount = Convert.ToDecimal(dtMaster.Rows[0]["taxAmount"]).ToString("N2");
                totalAmount = (Convert.ToDecimal(totalAmount) - Convert.ToDecimal(taxBillOnAmount)).ToString();
                grandTotal = Convert.ToDecimal(dtMaster.Rows[0]["totalAmount"]).ToString("N2");
                amountInWords = new NumToText().AmountWords(Convert.ToDecimal(grandTotal), PublicVariables._decCurrencyId);
                invoiceNumber = dtMaster.Rows[0]["invoiceNo"].ToString();
                ledgerName = dtMaster.Rows[0]["ledgerName"].ToString();
                voucherType = "SALES ORDER";
                partyType = "Customer";
            }

            Document pdfDoc = new Document();
            using (StringWriter sw = new StringWriter())
            {
                using (HtmlTextWriter hw = new HtmlTextWriter(sw))
                {
                    try
                    {
                        StringBuilder sb = new StringBuilder();

                        // generate invoice header
                        sb.Append("<table width='100%' cellspacing='0' cellpadding='2'>");
                        sb.Append("<tr><td align='center' style='background-color: #18B5F0' colspan = '2'><b>" + voucherType + "</b></td></tr>");
                        sb.Append("<tr><td colspan = '2'></td></tr>");
                        sb.Append("<tr><td><b> ");
                        sb.Append(dtCompanyDetails.Rows[0]["companyName"]);
                        sb.Append("</b></td><td align = 'right'><b>Invoice No: </b>");
                        sb.Append(invoiceNumber);
                        sb.Append(" </td></tr>");
                        sb.Append("<tr><td><b> ");
                        sb.Append(dtCompanyDetails.Rows[0]["address"]);
                        sb.Append("</b></td><td align = 'right'><b>Invoice Date: </b>");
                        sb.Append(DateTime.Now);
                        sb.Append(" </td></tr>");
                        sb.Append("<tr><td>Customer: ");
                        sb.Append(ledgerName);
                        sb.Append("</td><td align = 'right'><b> </b>");
                        sb.Append(" ");
                        sb.Append(" </td></tr>");
                        sb.Append("<tr><td> ");
                        sb.Append("");
                        sb.Append("</td><td align = 'right'><b> </b></td></tr>");

                        //sb.Append("<tr><td colspan = '2'><b> ");
                        //sb.Append(dtCompanyDetails.Rows[0]["companyName"]);
                        //sb.Append("</b></td></tr>");
                        //sb.Append("<tr><td colspan = '2'><b>  ");
                        //sb.Append(dtCompanyDetails.Rows[0]["address"]);
                        //sb.Append("</b></td></tr>");
                        sb.Append("</table>");
                        sb.Append("<br />");

                        // generate invoice items in grid
                        sb.Append("<table border = '1'>");
                        sb.Append("<tr>");
                        foreach (DataColumn column in dtDetails.Columns)
                        {
                            sb.Append("<th >"); // ("<th style = 'background-color: #D20B0C;color:#ffffff'>");
                            sb.Append(column.ColumnName);
                            sb.Append("</th>");
                        }
                        sb.Append("</tr>");
                        foreach (DataRow row in dtDetails.Rows)
                        {
                            sb.Append("<tr>");
                            foreach (DataColumn column in dtDetails.Columns)
                            {
                                sb.Append("<td>");
                                sb.Append(row[column]);
                                sb.Append("</td>");
                            }
                            sb.Append("</tr>");
                        }
                        sb.Append("</table>");
                        sb.Append("<br />");
                        sb.Append("<br />");

                        //Footer showing transactions summary
                        sb.Append("<table width='100%' cellspacing='0' cellpadding='2'>");

                        sb.Append("<tr> ");
                        sb.Append("<td><b>Amount in words: </b>" + amountInWords + "</td>");
                        sb.Append("<td ><b> </ b > </ td >");
                        sb.Append("<td align = 'right'><b>Tax amount on bill: </b></td>");
                        sb.Append("<td align = 'right'> " + taxBillOnAmount + "</ td >");
                        sb.Append("</ tr > ");
                        sb.Append("<tr>");
                        sb.Append("<td><b> </b></td>");
                        sb.Append("<td> </td>");
                        sb.Append("<td align = 'right'><b>Total Amount: </b></td>");
                        sb.Append("<td align = 'right'>" + totalAmount + " </td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td><b> </b></td>");
                        sb.Append("<td> </td>");
                        sb.Append("<td align = 'right'><b>Bill discount: </b></td>");
                        sb.Append("<td align = 'right'>" + billDiscount + " </td>");
                        sb.Append("</tr>");
                        sb.Append("<tr>");
                        sb.Append("<td><b> </b></td>");
                        sb.Append("<td > </ td >");
                        sb.Append("<td align = 'right'><b>Applied Amount: </b></td>");
                        sb.Append("<td align = 'right'>" + appliedAmount + "</ td >");
                        sb.Append("</ tr > ");
                        sb.Append("<tr>");
                        sb.Append("<td><b> </b></td>");
                        sb.Append("<td > </td>");
                        sb.Append("<td align = 'right'> <b>Balance due: </b></td>");
                        sb.Append("<td align = 'right'>" + grandTotal + " </td>");
                        sb.Append("</tr>");
                        sb.Append("</table>");
                        sb.Append("<br />");

                        //Export HTML String as PDF.
                        DirectoryInfo dir1 = new DirectoryInfo(Application.StartupPath + "\\Generated Invoices");
                        if (!Directory.Exists(Application.StartupPath + "\\Generated Invoices"))
                        {
                            dir1.Create();
                        }
                        if (File.Exists(Application.StartupPath + "\\Generated Invoices\\Invoice_" + invoiceNumber + ".pdf"))
                        {
                            File.Delete(Application.StartupPath + "\\Generated Invoices\\Invoice_" + invoiceNumber + ".pdf");
                        }
                        StringReader sr = new StringReader(sb.ToString());
                        pdfDoc = new Document(PageSize.A3, 10f, 10f, 10f, 0f);

                        //BaseFont bf = BaseFont.CreateFont(BaseFont.TIMES_ROMAN, BaseFont.CP1252, BaseFont.NOT_EMBEDDED);
                        //iTextSharp.text.Font font = new iTextSharp.text.Font(bf, 10, iTextSharp.text.Font.NORMAL);
                        //Paragraph p1 = new Paragraph(new Chunk("Some text content here \n", font));
                        //pdfDoc.Add(font);

                        HTMLWorker htmlparser = new HTMLWorker(pdfDoc);
                        PdfWriter writer = PdfWriter.GetInstance(pdfDoc, new FileStream(Application.StartupPath + "\\Generated Invoices\\Invoice_" + invoiceNumber + ".pdf", FileMode.Create));
                        pdfDoc.Open();
                        htmlparser.Parse(sr);
                        pdfDoc.Close();

                        // if the invoice was generated successfully, send it via email
                        if (File.Exists(Application.StartupPath + "\\Generated Invoices\\Invoice_" + invoiceNumber + ".pdf"))
                        {
                            string attachment = Application.StartupPath + "\\Generated Invoices\\Invoice_" + invoiceNumber + ".pdf";
                            sendInvoiceToEmail(partyId, invoiceNumber, grandTotal, dtCompanyDetails.Rows[0]["companyName"].ToString(), attachment, ref pinvoiceEmailDetails, partyType);
                        }
                    }
                    catch (Exception ex)
                    {
                        if (ex.Message.Contains("The process cannot access the file") && ex.Message.Contains("because it is being used by another process."))
                        {
                            MessageBox.Show("Close the PDF file and try again", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            MessageBox.Show("T1:" + ex.Message, "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    finally
                    {
                        try
                        {
                            pdfDoc.Close();
                        }
                        catch (Exception ex)
                        {

                        }
                    }
                }
            }
        }

        private void sendInvoiceToEmail(decimal partyId, string invoiceNo, string invoiceAmt, string companyName, string attachedFile, ref invoiceEmailDetails pInvoiceEmailDetails, string partyType)
        {
            try
            {
                string mailTo = "", MailFrom = "", MailPassword = "", smtpServer = "", message = "", mailTitle = "";
                Int32 portNumber = 0;
                bool enableSSL = true;
                DAL.DBMatConnection db = new DAL.DBMatConnection();
                frmSendMail validateEmail = new frmSendMail();
                db.AddParameter("@partyId", partyId);
                DataSet ds = db.getDataSet("invoiceMailDetails");
                if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
                {
                    mailTo = Convert.ToString(ds.Tables[0].Rows[0]["recipientsEmail"].ToString());
                    MailFrom = Convert.ToString(ds.Tables[0].Rows[0]["emailID"].ToString());
                    MailPassword = Convert.ToString(ds.Tables[0].Rows[0]["logonPassword"].ToString());
                    smtpServer = ds.Tables[0].Rows[0]["smtp"].ToString();
                    portNumber = Convert.ToInt32(ds.Tables[0].Rows[0]["portNumber"]);
                }
                else
                {
                    Cursor.Current = Cursors.Default;
                    isSend = true;
                    MessageBox.Show("Please Ensure to Create an Email Address Before Attempting to Send a Mail..", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (validateEmail.ValidateEmail(mailTo) && validateEmail.ValidateEmail(MailFrom))
                {
                    //frmLodingObj.ShowFromSendMail(); //TODO: use properly
                    MailAddress senderAddress = new MailAddress(MailFrom);
                    MailAddress receiverAddress = new MailAddress(mailTo);
                    string decryptedPassword = new MATFinancials.Classes.Security().base64Decode(MailPassword);
                    System.Net.Mail.MailMessage mailMsg = new System.Net.Mail.MailMessage(senderAddress, receiverAddress);

                    string mailBody = string.Format("Dear " + partyType +":{0}{1}Your invoice-" + invoiceNo + " for "+ invoiceAmt + " is attached for your action.{2}" +
                    "Thank you for your business, we appreciate.{3}{4}Yours Sincerely{5}" + companyName + "{6}"
                    ,Environment.NewLine, Environment.NewLine, Environment.NewLine, Environment.NewLine, Environment.NewLine, Environment.NewLine, Environment.NewLine);
                    mailTitle = "Invoice " + invoiceNo + " from " + companyName;
                    message = mailBody;
                    //message += "Dear Customer: <br/> ";
                    //message += "Your invoice-" + invoiceNo + " for " + invoiceAmt + " is attached for your action. <br/>";
                    //message += "Thank you for your business, we appreciate. <br/> Yours Sincerely <br/>";
                    //message += companyName + "<br/>";
                    mailMsg.Subject = mailTitle;
                    mailMsg.IsBodyHtml = true;
                    mailMsg.Body = message;

                    //return credentions to sales invoice for onward passage to send mail form
                    invoiceEmailDetails invoiceEmailDetails = new invoiceEmailDetails();
                    invoiceEmailDetails.MailFrom = MailFrom;
                    invoiceEmailDetails.MailPassword = MailPassword;
                    invoiceEmailDetails.mailTitle = mailTitle;
                    invoiceEmailDetails.mailTo = mailTo;
                    invoiceEmailDetails.message = message;
                    invoiceEmailDetails.smtpServer = smtpServer;
                    invoiceEmailDetails.attachment = attachedFile;

                    pInvoiceEmailDetails = invoiceEmailDetails;
                    // Changed 20170331 *** do not send the email automatically, pop up send mail window *** so skip the below processes, it works though
                    /*
                    System.Net.Mail.Attachment attachment = new System.Net.Mail.Attachment(attachedFile);
                    mailMsg.Attachments.Add(attachment);
                    var client = new SmtpClient()
                    {
                        Host = smtpServer,
                        Port = portNumber,
                        UseDefaultCredentials = false,
                        Credentials = new NetworkCredential(MailFrom, decryptedPassword),
                        EnableSsl = enableSSL,
                        DeliveryMethod = SmtpDeliveryMethod.Network,
                        Timeout = 20000
                    };
                    client.Send(mailMsg);
                    isSend = true;
                    isCheck = true;
                    MessageBox.Show("Mail sent successfully ", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                */
                }

            }
            catch (Exception ex)
            {
                Cursor.Current = Cursors.Default;
                isSend = false;
                MessageBox.Show("Mail sending failed", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
    }
}
