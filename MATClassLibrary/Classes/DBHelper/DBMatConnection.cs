﻿using System;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.Configuration;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace MATFinancials.DAL
{

    public class DBMatConnection: DBConnection
    {
        private SqlConnection Conn;
        private SqlCommand Cmd;
        private SqlDataAdapter Da;
        private DataSet Ds;
        public string ConStr;
        public string m_Error;
        private int rowAffected = 0;
        public String QueryText
        {
            get;
            set;
        }
        #region Constructor
        public DBMatConnection()
        {
            try
            {
                Conn = sqlcon; // Simply reuse connection from the DBConnection class.
               
                Cmd = new SqlCommand();
                Da = new SqlDataAdapter();
                Ds = new DataSet();

                Cmd.Connection = Conn;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.Constructor()");
            }
        }
        public DBMatConnection(string connStr)
        {
            try
            {
                Conn = new SqlConnection(connStr);

                Cmd = new SqlCommand();
                Da = new SqlDataAdapter();
                Ds = new DataSet();

                Cmd.Connection = Conn;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.Constructor()");
            }
        }
        ~DBMatConnection()
        {
           // Conn.Dispose();
            Cmd.Dispose();
            Da.Dispose();
            Ds.Dispose();
        }
        #endregion
        #region Open & Close Connection
        public void OpenConnection()
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }

                //if (Conn.State == ConnectionState.Closed)
                //    Conn.Open();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.OpenConnection()");
            }
        }
        public void CloseConnection()
        {
            try
            {
                if (sqlcon.State == ConnectionState.Open)
                {
                    sqlcon.Close();
                }

                //if (Conn.State == ConnectionState.Open)
                //    Conn.Close();

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.CloseConnection()");
            }
        }
        #endregion
        #region Set & Get Connection
        public SqlConnection Connection
        {
            get { return Conn; }
            set { Conn = value; }
        }
        #endregion
        #region ExecuteQuery
        public DataSet ExecuteQuery(string Query)
        {
            try
            {
                Cmd.CommandText = Query;
                Cmd.Connection = Conn;
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                da.Fill(Ds);
                return Ds;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.ExecuteNonQuery()");
            }

        }

        public DataTable CustomQueryToDataTable(string query)
        {
            DataTable dt = new DataTable();
            var ds = new DataSet();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                string CommStr = query;
              var  comm = new SqlCommand(CommStr, sqlcon);
             var   da = new SqlDataAdapter(comm);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "GQ8:" + ex.Message;
            }
            finally
            {
                sqlcon.Close();
            }
            return dt;
        }
        #endregion
        #region ExecuteNonQuery
        public bool ExecuteNonQuery(string spName)
        {
            bool isQueryExecuted = false;
            try
            {
                Cmd.CommandText = spName;
                // Parth Add New  Code for increase time in second default time 30 second
                Cmd.CommandTimeout = 60;
                // Parth End new code
                Cmd.CommandType = CommandType.StoredProcedure;
                OpenConnection();
                rowAffected = Cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.ExecuteNonQuery()");
            }
            finally
            {
                CloseConnection();
            }
            if (rowAffected > 0)
            {
                isQueryExecuted = true;
            }
            return isQueryExecuted;
        }
        public bool ExecuteNonQuery2(string queryStr )
        {
            bool isQueryExecuted = false;
            try
            {
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = queryStr;
                OpenConnection();
                rowAffected = Cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {

            }
            finally
            {
                CloseConnection();
            }
            if (rowAffected > 0)
            {
                isQueryExecuted = true;
            }
            return isQueryExecuted;
        }
        public bool IsFormNumberExist(string queryStr)
        {
            try
            {
                Cmd.CommandType = CommandType.Text;
                Cmd.CommandText = queryStr;
                OpenConnection();
                DataTable dt = new DataTable();
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                da.Fill(dt);
                if(dt.Rows.Count>0)
                {
                    return true;
                }
            }
            catch (Exception ex)
            {

            }
            return false;
        }

        public int ExecuteNonQuery()
        {
            try
            {
                Cmd.CommandText = QueryText;
                // Parth Add New  Code for increase time in second default time 30 second
                Cmd.CommandTimeout = 60;
                // Parth End new code
                Cmd.CommandType = CommandType.StoredProcedure;
                OpenConnection();
                return Cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.ExecuteNonQuery()");
            }
            finally
            {
                CloseConnection();
            }
        }
        #endregion
        #region getDataSet
        public DataSet getDataSet(string spName)
        {
            try
            {
                Cmd.CommandText = spName;
                Cmd.CommandType = CommandType.StoredProcedure;
                Da.SelectCommand = Cmd;
                Da.Fill(Ds);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.getDataTable()");
            }
            return Ds;
        }
        public DataSet GetDataSet(string queryStr)
        {
            try
            {
                Cmd.CommandText = queryStr; // = new SqlCommand(queryStr, sqlcon);
                Cmd.CommandType = CommandType.Text;
                Da.SelectCommand = Cmd; // = new SqlDataAdapter(Cmd);
                Da.Fill(Ds);
            }
            catch (Exception ex)
            {

            }
            return Ds;
        }
        public DataSet getDataSet()
        {
            try
            {
                Cmd.CommandText = QueryText;
                Cmd.CommandType = CommandType.StoredProcedure;
                Da.SelectCommand = Cmd;
                Da.Fill(Ds);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.getDataTable()");
            }
            return Ds;
        }
        #endregion
        #region getDatasetwithpar
        public DataSet getDatasetwithpar(string spName, string parameter, string value)
        {
            try
            {
                Cmd.CommandText = spName;
                Cmd.CommandType = CommandType.StoredProcedure;
                AddParameter(parameter, value);
                Da.SelectCommand = Cmd;
                Da.Fill(Ds);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.getDataTable()");
            }

            return Ds;
        }
        public DataSet getDataSetwithparameter(string spName, string Parameter, Guid value)
        {
            try
            {
                Conn = new SqlConnection(ConStr);

                Cmd = new SqlCommand();
                Da = new SqlDataAdapter();
                Ds = new DataSet();

                Cmd.Connection = Conn;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.Constructor()");
            }



            try
            {

                Cmd.CommandText = spName;
                Cmd.CommandType = CommandType.StoredProcedure;
                AddParameter(Parameter, value);
                Da.SelectCommand = Cmd;
                Da.Fill(Ds);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.getDataTable()");
            }

            return Ds;
        }
        #endregion
        #region
        public string getSingleValue(string QueryStr)
        {
            string returnValue = "";
            Ds = new DataSet();
            try
            {
                Cmd.CommandText = QueryStr;
                Cmd.Connection = Conn;
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                da.Fill(Ds);
                if(Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    returnValue = Ds.Tables[0].Rows[0][0].ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.ExecuteNonQuery()");
            }
            return returnValue;
        }
        public string getSingleValue2(string QueryStr)
        {
            string returnValue = "";
            Ds = new DataSet();
            try
            {
                Cmd.CommandText = QueryStr;
                Cmd.Connection = Conn;
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                da.Fill(Ds);
                if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
                {
                    returnValue = Ds.Tables[0].Rows[0][1].ToString();
                }
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.ExecuteNonQuery()");
            }
            return returnValue;
        }
        //public string getSingleValue2(string QueryStr)
        //{
        //    string returnValue = "";
        //    Ds = new DataSet();
        //    try
        //    {
        //        Cmd.CommandText = QueryStr;
        //        Cmd.Connection = Conn;
        //        SqlDataAdapter da = new SqlDataAdapter(Cmd);
        //        da.Fill(Ds);
        //        if (Ds != null && Ds.Tables.Count > 0 && Ds.Tables[0].Rows.Count > 0)
        //        {
        //            returnValue = Ds.Tables[0].Rows[0][1].ToString();
        //        }
        //    }
        //    catch (Exception ex)
        //    {
        //        throw new Exception(ex.Message + " : dbOperation.ExecuteNonQuery()");
        //    }
        //    return returnValue;
        //}⁠⁠⁠⁠
        public DataSet customQuery(string QueryStr)
        {
            Ds = new DataSet();
            try
            {
                Cmd.CommandText = QueryStr;
                Cmd.Connection = Conn;
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                da.Fill(Ds);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.ExecuteNonQuery()");
            }
            return Ds;
        }

        public DataTable customSelect(string QueryStr)
        {
            DataTable dt = new DataTable();
            try
            {
                Cmd.CommandText = QueryStr;
                Cmd.Connection = Conn;
                SqlDataAdapter da = new SqlDataAdapter(Cmd);
                da.Fill(dt);
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.ExecuteNonQuery()");
            }
            return dt;
        }

        public decimal customUpdateQuery(string QueryStr)
        {
            decimal affected = 0;
            try
            {
                Cmd.CommandText = QueryStr;
                Cmd.Connection = Conn;
                Conn.Open();
                affected=Cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.ExecuteNonQuery()");
            }
            finally
            {
                Conn.Close();
            }
            return affected;
        }
        #endregion
        #region AddParameter
        public void AddParameter(string ParameterName, long Value)
        {
            try
            {
                Cmd.Parameters.Add(ParameterName, SqlDbType.BigInt).Value = Value;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddParameter() : string");
            }
        }
        public void AddQuery(string QueryText)
        {
            try
            {
                //Cmd.CommandText = QueryText;
                //Cmd.CommandType = CommandType.StoredProcedure;
                this.QueryText = QueryText;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddQuery() : string");
            }
        }
        public void AddParameter(string ParameterName, double Value)
        {
            try
            {
                Cmd.Parameters.Add(ParameterName, SqlDbType.Float).Value = Value;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddParameter() : string");
            }
        }
        public void AddParameter(string ParameterName, string Value)
        {
            try
            {
                Cmd.Parameters.Add(ParameterName, SqlDbType.VarChar).Value = Value;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddParameter() : string");
            }
        }
        public void AddParameter(string ParameterName, Guid Value)
        {
            try
            {
                Cmd.Parameters.Add(ParameterName, SqlDbType.UniqueIdentifier).Value = Value;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddParameter() : Guid");
            }
        }
        public void AddParameter(string ParameterName, int Value)
        {
            try
            {
                Cmd.Parameters.Add(ParameterName, SqlDbType.Int).Value = Value;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddParameter() : int");
            }
        }
        public void AddParameter(string ParameterName, bool Value)
        {
            try
            {
                Cmd.Parameters.Add(ParameterName, SqlDbType.Bit).Value = Value;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddParameter() : bool");
            }
        }
        public void AddParameter(string ParameterName, DateTime Value)
        {
            try
            {
                Cmd.Parameters.Add(ParameterName, SqlDbType.DateTime).Value = Value;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddParameter() : DateTime");
            }
        }

        public void AddParameter(string ParameterName, DataTable Value)
        {
            try
            {
                Cmd.Parameters.Add(ParameterName, SqlDbType.Structured).Value = Value;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddParameter() : Datatable");
            }
        }
        public void AddParameter(string ParameterName, decimal Value)
        {
            try
            {
                Cmd.Parameters.Add(ParameterName, SqlDbType.Decimal).Value = Value;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddParameter() : decimal");
            }
        }
        public void ChangeToOutputParameter(string ParameterName)
        {
            try
            {
                Cmd.Parameters[ParameterName].Direction = ParameterDirection.Output;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddReturnParameter()");
            }
        }
        public void ChangeToOutputParameter(string ParameterName, SqlDbType dbType)
        {
            try
            {
                Cmd.Parameters[ParameterName].Direction = ParameterDirection.Output;
                Cmd.Parameters[ParameterName].SqlDbType = dbType;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddReturnParameter()");
            }
        }
        public void AddReturnParameter(string ParameterName, SqlDbType dbType)
        {
            try
            {
                Cmd.Parameters.Add(ParameterName, dbType).Direction = ParameterDirection.ReturnValue;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.AddReturnParameter()");
            }
        }
        public object GetParameterReturnValue(string ParameterName)
        {
            try
            {
                return Cmd.Parameters[ParameterName].Value;
            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message + " : dbOperation.GetParameterReturnValue()");
            }
        }
        #endregion
    }
}

