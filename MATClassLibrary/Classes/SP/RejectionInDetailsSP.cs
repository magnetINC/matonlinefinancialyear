
 
using System;    
using System.Collections.Generic;    
using System.Text;    
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
    
//<summary>    
//Summary description for RejectionInDetailsSP    
//</summary>    
namespace MATFinancials    
{
    public class RejectionInDetailsSP : DBConnection
    {
        #region Function
        /// <summary>
        /// Function to insert values to RejectionInDetails Table
        /// </summary>
        /// <param name="rejectionindetailsinfo"></param>
        public void RejectionInDetailsAdd(RejectionInDetailsInfo rejectionindetailsinfo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RejectionInDetailsAdd", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@rejectionInMasterId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.RejectionInMasterId;
                sprmparam = sccmd.Parameters.Add("@deliveryNoteDetailsId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.DeliveryNoteDetailsId;
                sprmparam = sccmd.Parameters.Add("@productId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.ProductId;
                sprmparam = sccmd.Parameters.Add("@qty", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.Qty;
                sprmparam = sccmd.Parameters.Add("@rate", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.Rate;
                sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.UnitId;
                sprmparam = sccmd.Parameters.Add("@unitConversionId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.UnitConversionId;
                sprmparam = sccmd.Parameters.Add("@batchId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.BatchId;
                sprmparam = sccmd.Parameters.Add("@godownId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.GodownId;
                sprmparam = sccmd.Parameters.Add("@rackId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.RackId;
                sprmparam = sccmd.Parameters.Add("@amount", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.Amount;
                sprmparam = sccmd.Parameters.Add("@slNo", SqlDbType.Int);
                sprmparam.Value = rejectionindetailsinfo.SlNo;
                
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = rejectionindetailsinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = rejectionindetailsinfo.Extra2;
                sprmparam = sccmd.Parameters.Add("@ProjectId", SqlDbType.Int);
                sprmparam.Value = rejectionindetailsinfo.ProjectId;
                sprmparam = sccmd.Parameters.Add("@CategoryId", SqlDbType.Int);
                sprmparam.Value = rejectionindetailsinfo.CategoryId;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to Update values in RejectionInDetails Table
        /// </summary>
        /// <param name="rejectionindetailsinfo"></param>
        public void RejectionInDetailsEdit(RejectionInDetailsInfo rejectionindetailsinfo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RejectionInDetailsEdit", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@rejectionInDetailsId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.RejectionInDetailsId;
                sprmparam = sccmd.Parameters.Add("@rejectionInMasterId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.RejectionInMasterId;
                sprmparam = sccmd.Parameters.Add("@deliveryNoteDetailsId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.DeliveryNoteDetailsId;
                sprmparam = sccmd.Parameters.Add("@productId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.ProductId;
                sprmparam = sccmd.Parameters.Add("@qty", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.Qty;
                sprmparam = sccmd.Parameters.Add("@rate", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.Rate;
                sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.UnitId;
                sprmparam = sccmd.Parameters.Add("@unitConversionId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.UnitConversionId;
                sprmparam = sccmd.Parameters.Add("@batchId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.BatchId;
                sprmparam = sccmd.Parameters.Add("@godownId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.GodownId;
                sprmparam = sccmd.Parameters.Add("@rackId", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.RackId;
                sprmparam = sccmd.Parameters.Add("@amount", SqlDbType.Decimal);
                sprmparam.Value = rejectionindetailsinfo.Amount;
                sprmparam = sccmd.Parameters.Add("@slNo", SqlDbType.Int);
                sprmparam.Value = rejectionindetailsinfo.SlNo;
                sprmparam = sccmd.Parameters.Add("@extraDate", SqlDbType.DateTime);
                sprmparam.Value = rejectionindetailsinfo.ExtraDate;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = rejectionindetailsinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = rejectionindetailsinfo.Extra2;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
        }
         
       /// <summary>
        /// Function to delete from RejectionInDetails table based on the parameter
       /// </summary>
       /// <param name="decRejectionInMasterId"></param>
        public void DeleteRejectionInDetailsByRejectionInMasterId(decimal decRejectionInMasterId)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand cmd = new SqlCommand("RejectionInDetailsDeleteByRejectionInMasterId", sqlcon);
                cmd.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter();
                param = cmd.Parameters.Add("@rejectionInMasterId", SqlDbType.Decimal);
                param.Value = decRejectionInMasterId;
                cmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
        }
       /// <summary>
        /// Function to get particular values from RejectionInDetails table based on the parameter
       /// </summary>
       /// <param name="decRejectionInMasterId"></param>
       /// <returns></returns>
        public DataTable RejectionInDetailsViewByRejectionInMasterId(decimal decRejectionInMasterId)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlDataAdapter sqlda = new SqlDataAdapter("RejectionInDetailsViewByRejectionInMasterId", sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter param = new SqlParameter();
                param = sqlda.SelectCommand.Parameters.Add("@rejectionInMasterId", SqlDbType.Decimal);
                param.Value = decRejectionInMasterId;
                sqlda.Fill(dtbl);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return dtbl;
        }
        #endregion
    }
}
