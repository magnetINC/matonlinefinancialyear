﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATClassLibrary.Classes.SP
{
    public class DatePickerModel
    {
        public int FiFromDay { get; set; }
        public int FiFromMonth { get; set; }
        public int FiFromYear { get; set; }

        public int FiToDay { get; set; }
        public int FiToMonth { get; set; }
        public int FiToYear { get; set; }

    }

    public class PickerModel
    {
        public List<DatePickerModel> DatePickers { get; set; }
        public bool IsChecked { get; set; }
        public int SelectedIndex { get; set; }


    }
}

