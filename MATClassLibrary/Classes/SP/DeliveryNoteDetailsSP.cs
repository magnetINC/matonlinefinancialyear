 
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
using MatApi.Models.SalesModel;
//<summary>    
//Summary description for DeliveryNoteDetailsSP    
//</summary>    
namespace MATFinancials
{
    public class DeliveryNoteDetailsSP : DBConnection
    {
        #region Function
        /// <summary>
        /// Function to insert values to DeliveryNoteDetails Table
        /// </summary>
        /// <param name="deliverynotedetailsinfo"></param>
        public decimal DeliveryNoteDetailsAdd(DeliveryNoteDetailsInfo deliverynotedetailsinfo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("DeliveryNoteDetailsAdd", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@deliveryNoteMasterId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.DeliveryNoteMasterId;
                sprmparam = sccmd.Parameters.Add("@orderDetails1Id", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.OrderDetails1Id;
                sprmparam = sccmd.Parameters.Add("@productId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.ProductId;
                sprmparam = sccmd.Parameters.Add("@qty", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.Qty;
                sprmparam = sccmd.Parameters.Add("@rate", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.Rate;
                sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.UnitId;
                sprmparam = sccmd.Parameters.Add("@unitConversionId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.UnitConversionId;
                sprmparam = sccmd.Parameters.Add("@amount", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.Amount;
                sprmparam = sccmd.Parameters.Add("@quotationDetails1Id", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.QuotationDetails1Id;
                sprmparam = sccmd.Parameters.Add("@batchId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.BatchId;
                sprmparam = sccmd.Parameters.Add("@godownId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.GodownId;
                sprmparam = sccmd.Parameters.Add("@rackId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.RackId;
                sprmparam = sccmd.Parameters.Add("@slNo", SqlDbType.Int);
                sprmparam.Value = deliverynotedetailsinfo.SlNo;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = deliverynotedetailsinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = deliverynotedetailsinfo.Extra2;
                sprmparam = sccmd.Parameters.Add("@ProjectId", SqlDbType.Int);
                sprmparam.Value = deliverynotedetailsinfo.ProjectId;
                sprmparam = sccmd.Parameters.Add("@CategoryId", SqlDbType.Int);
                sprmparam.Value = deliverynotedetailsinfo.CategoryId;
                sprmparam = sccmd.Parameters.Add("@itemDescription", SqlDbType.VarChar);
                sprmparam.Value = deliverynotedetailsinfo.itemDescription;
                return sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return 0;
        }
        /// <summary>
        /// Function to Update values in DeliveryNoteDetails Table
        /// </summary>
        /// <param name="deliverynotedetailsinfo"></param>
        public void DeliveryNoteDetailsEdit(DeliveryNoteDetailsInfo deliverynotedetailsinfo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("DeliveryNoteDetailsEdit", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@deliveryNoteDetailsId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.DeliveryNoteDetails1Id;
                sprmparam = sccmd.Parameters.Add("@deliveryNoteMasterId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.DeliveryNoteMasterId;
                sprmparam = sccmd.Parameters.Add("@orderDetails1Id", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.OrderDetails1Id;
                sprmparam = sccmd.Parameters.Add("@productId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.ProductId;
                sprmparam = sccmd.Parameters.Add("@qty", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.Qty;
                sprmparam = sccmd.Parameters.Add("@rate", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.Rate;
                sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.UnitId;
                sprmparam = sccmd.Parameters.Add("@unitConversionId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.UnitConversionId;
                sprmparam = sccmd.Parameters.Add("@amount", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.Amount;
                sprmparam = sccmd.Parameters.Add("@quotationDetails1Id", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.QuotationDetails1Id;
                sprmparam = sccmd.Parameters.Add("@batchId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.BatchId;
                sprmparam = sccmd.Parameters.Add("@godownId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.GodownId;
                sprmparam = sccmd.Parameters.Add("@rackId", SqlDbType.Decimal);
                sprmparam.Value = deliverynotedetailsinfo.RackId;
                sprmparam = sccmd.Parameters.Add("@slNo", SqlDbType.Int);
                sprmparam.Value = deliverynotedetailsinfo.SlNo;
                sprmparam = sccmd.Parameters.Add("@ProjectId", SqlDbType.Int);
                sprmparam.Value = deliverynotedetailsinfo.ProjectId;
                sprmparam = sccmd.Parameters.Add("@CategoryId", SqlDbType.Int);
                sprmparam.Value = deliverynotedetailsinfo.CategoryId;
                sprmparam = sccmd.Parameters.Add("@itemDescription", SqlDbType.VarChar);
                sprmparam.Value = deliverynotedetailsinfo.itemDescription;
                sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
        }
        /// <summary>
        /// Function to get all the values from DeliveryNoteDetails Table
        /// </summary>
        /// <returns></returns>
        public DataTable DeliveryNoteDetailsViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("DeliveryNoteDetailsViewAll", sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get particular values from DeliveryNoteDetails table based on the parameter
        /// </summary>
        /// <param name="deliveryNoteDetails1Id"></param>
        /// <returns></returns>
        public DeliveryNoteDetailsInfo DeliveryNoteDetailsView(decimal deliveryNoteDetails1Id)
        {
            DeliveryNoteDetailsInfo deliverynotedetailsinfo = new DeliveryNoteDetailsInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("DeliveryNoteDetailsView", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@deliveryNoteDetails1Id", SqlDbType.Decimal);
                sprmparam.Value = deliveryNoteDetails1Id;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    deliverynotedetailsinfo.DeliveryNoteDetails1Id = decimal.Parse(sdrreader[0].ToString());
                    deliverynotedetailsinfo.DeliveryNoteMasterId = decimal.Parse(sdrreader[1].ToString());
                    deliverynotedetailsinfo.OrderDetails1Id = decimal.Parse(sdrreader[2].ToString());
                    deliverynotedetailsinfo.ProductId = decimal.Parse(sdrreader[3].ToString());
                    deliverynotedetailsinfo.Qty = decimal.Parse(sdrreader[4].ToString());
                    deliverynotedetailsinfo.Rate = decimal.Parse(sdrreader[5].ToString());
                    deliverynotedetailsinfo.UnitId = decimal.Parse(sdrreader[6].ToString());
                    deliverynotedetailsinfo.UnitConversionId = decimal.Parse(sdrreader[7].ToString());
                    deliverynotedetailsinfo.Amount = decimal.Parse(sdrreader[8].ToString());
                    deliverynotedetailsinfo.QuotationDetails1Id = decimal.Parse(sdrreader[9].ToString());
                    deliverynotedetailsinfo.BatchId = decimal.Parse(sdrreader[10].ToString());
                    deliverynotedetailsinfo.GodownId = decimal.Parse(sdrreader[11].ToString());
                    deliverynotedetailsinfo.RackId = decimal.Parse(sdrreader[12].ToString());
                    deliverynotedetailsinfo.SlNo = int.Parse(sdrreader[13].ToString());
                    deliverynotedetailsinfo.ExtraDate = DateTime.Parse(sdrreader[14].ToString());
                    deliverynotedetailsinfo.Extra1 = sdrreader[15].ToString();
                    deliverynotedetailsinfo.Extra2 = sdrreader[16].ToString();
                    deliverynotedetailsinfo.ProjectId = int.Parse(sdrreader[17].ToString());
                    deliverynotedetailsinfo.CategoryId = int.Parse(sdrreader[18].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sdrreader.Close();
                sqlcon.Close();
            }
            return deliverynotedetailsinfo;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="DeliveryNoteDetailsId"></param>
        /// <returns></returns>
        public decimal DeliveryNoteDetailsDelete(decimal DeliveryNoteDetailsId)
        {
            decimal decResult = 0;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("DeliveryNoteDetailsDelete", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@deliveryNoteDetailsId", SqlDbType.Decimal);
                sprmparam.Value = DeliveryNoteDetailsId;
                int ineffectedRow = Convert.ToInt32(sccmd.ExecuteNonQuery().ToString());
                if (ineffectedRow > 0)
                {
                    decResult = 1;
                }
                else
                {
                    decResult = 0;
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return decResult;
        }
        /// <summary>
        /// Function to  get the next id for DeliveryNoteDetails table
        /// </summary>
        /// <returns></returns>
        public int DeliveryNoteDetailsGetMax()
        {
            int max = 0;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("DeliveryNoteDetailsMax", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                max = int.Parse(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }

            finally
            {
                sqlcon.Close();
            }
            return max;
        }
        /// <summary>
        /// Function to get values of pending based on parameters
        /// </summary>
        /// <param name="decDeliveryNoteMasterId"></param>
        /// <param name="decRejectionInMasterId"></param>
        /// <returns></returns>
        public DataTable DeliveryNoteDetailsViewByDeliveryNoteMasterIdWithPending(decimal decDeliveryNoteMasterId, decimal decRejectionInMasterId)
        {

            DataTable dtbl = new DataTable();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                //DeliveryNoteDetailsViewByDeliveryNoteMasterIdWithPending
                SqlDataAdapter sqlda = new SqlDataAdapter("DeliveryNoteDetailsViewByDeliveryNoteMasterIdWithPending", sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter sqlparam = new SqlParameter();
                sqlparam = sqlda.SelectCommand.Parameters.Add("@deliveryNoteMasterId", SqlDbType.Decimal);
                sqlparam.Value = decDeliveryNoteMasterId;
                sqlparam = sqlda.SelectCommand.Parameters.Add("@rejectionInMasterId", SqlDbType.Decimal);
                sqlparam.Value = decRejectionInMasterId;
                sqlda.Fill(dtbl);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get values of Sales based on DeliveryNote with parameters passed
        /// </summary>
        /// <param name="decDeliveryNoteMasterId"></param>
        /// <param name="SIMasterId"></param>
        /// <param name="voucherTypeId"></param>
        /// <returns></returns>
        public DataTable SalesInvoiceGridfillAgainestDeliveryNoteUsingDeliveryNoteDetails(decimal decDeliveryNoteMasterId, decimal SIMasterId, decimal voucherTypeId)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlDataAdapter sqldataadapter = new SqlDataAdapter("SalesInvoiceGridfillAgainestDeliveryNoteUsingDeliveryNoteDetails", sqlcon);
                sqldataadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter sqlparameter = new SqlParameter();
                sqlparameter = sqldataadapter.SelectCommand.Parameters.Add("@deliveryNoteMasterId", SqlDbType.Decimal);
                sqlparameter.Value = decDeliveryNoteMasterId;
                sqlparameter = sqldataadapter.SelectCommand.Parameters.Add("@salesMasterId", SqlDbType.Decimal);
                sqlparameter.Value = SIMasterId;
                sqlparameter = sqldataadapter.SelectCommand.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                sqlparameter.Value = voucherTypeId;
                sqldataadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.Message);
                MessageBox.Show("DNOrder" + ex.Message, "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            finally
            {
                sqlcon.Close();
            }
            return dtbl;
        }

        //public List<GetSalesMode> SalesInvoiceGridfillAgainestDeliveryNoteUsingDeliveryNoteDetails(decimal decDeliveryNoteMasterId, decimal SIMasterId, decimal voucherTypeId)
        //{
        //    SqlCommand cmd = new SqlCommand("SalesInvoiceGridfillAgainestDeliveryNoteUsingDeliveryNoteDetails", sqlcon);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    SqlParameter sqlparameter = new SqlParameter();
        //    sqlparameter = cmd.Parameters.Add("@deliveryNoteMasterId", SqlDbType.Decimal);
        //    sqlparameter.Value = decDeliveryNoteMasterId;
        //    sqlparameter = cmd.Parameters.Add("@salesMasterId", SqlDbType.Decimal);
        //    sqlparameter.Value = SIMasterId;
        //    sqlparameter = cmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
        //    sqlparameter.Value = voucherTypeId;
        //    if (sqlcon.State == ConnectionState.Closed)
        //    {
        //        sqlcon.Open();
        //    }
        //    var listsales = new List<GetSalesMode>();
        //    var rdr = cmd.ExecuteReader();

        //    if (rdr.HasRows)
        //    {
        //        while (rdr.Read())
        //        {
        //            var salesModel = new GetSalesMode();

        //            salesModel.productId = Convert.ToDecimal(rdr["productId"].ToString());
        //            salesModel.narration = rdr["narration"].ToString();
        //            try
        //            {
        //                salesModel.productCode = Convert.ToDecimal(rdr["productCode"].ToString());
        //            }
        //            catch (Exception ex)
        //            {

        //            }
        //            salesModel.productName = rdr["productName"].ToString();
        //            salesModel.barCode = rdr["barCode"].ToString();
        //            salesModel.batchId = Convert.ToDecimal(rdr["batchId"].ToString());
        //            salesModel.batchNo = rdr["batchNo"].ToString();
        //            salesModel.unitName = rdr["unitName"].ToString();
        //            salesModel.projectId = Convert.ToDecimal(rdr["projectId"].ToString());
        //            salesModel.qty = rdr["qty"].ToString();
        //            salesModel.rate = rdr["rate"].ToString();
        //            salesModel.conversionRate = rdr["conversionRate"].ToString();
        //            salesModel.unitConversionId = Convert.ToDecimal(rdr["unitConversionId"].ToString());
        //            salesModel.unitId = Convert.ToDecimal(rdr["unitId"].ToString());
        //            salesModel.godownId = rdr["godownId"].ToString();
        //            salesModel.rackId = Convert.ToDecimal(rdr["rackId"].ToString());
        //            salesModel.taxId = Convert.ToDecimal(rdr["taxId"].ToString());
        //            salesModel.voucherNo = rdr["voucherNo"].ToString();
        //            salesModel.voucherTypeId = Convert.ToDecimal(rdr["voucherTypeId"].ToString());
        //            salesModel.invoiceNo = rdr["invoiceNo"].ToString();
        //            salesModel.itemDescription = rdr["itemDescription"].ToString();
        //            salesModel.categoryId = Convert.ToDecimal(rdr["categoryId"].ToString());
        //            salesModel.amount = Convert.ToDecimal(rdr["amount"].ToString());
        //            salesModel.currencyId = Convert.ToDecimal(rdr["currencyId"].ToString());

        //            listsales.Add(salesModel);
        //        }

        //    }
        //    return listsales;
        //}
        //public List<GetSalesMode> SalesInvoiceGridfillAgainestDeliveryNoteUsingDeliveryNoteDetails1(decimal decDeliveryNoteMasterId, decimal SIMasterId, decimal voucherTypeId)
        //{
        //    SqlCommand cmd = new SqlCommand("SalesInvoiceGridfillAgainestDeliveryNoteUsingDeliveryNoteDetails", sqlcon);
        //    cmd.CommandType = CommandType.StoredProcedure;
        //    SqlParameter sqlparameter = new SqlParameter();
        //    sqlparameter = cmd.Parameters.Add("@deliveryNoteMasterId", SqlDbType.Decimal);
        //    sqlparameter.Value = decDeliveryNoteMasterId;
        //    sqlparameter = cmd.Parameters.Add("@salesMasterId", SqlDbType.Decimal);
        //    sqlparameter.Value = SIMasterId;
        //    sqlparameter = cmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
        //    sqlparameter.Value = voucherTypeId;
        //    if (sqlcon.State == ConnectionState.Closed)
        //    {
        //        sqlcon.Open();
        //    }
        //    var listsales = new List<GetSalesMode>();
        //    var rdr = cmd.ExecuteReader();

        //    if (rdr.HasRows)
        //    {
        //        while (rdr.Read())
        //        {
        //            var salesModel = new GetSalesMode();

        //            salesModel.productId = Convert.ToDecimal(rdr["productId"].ToString());
        //            salesModel.narration = rdr["narration"].ToString();
        //            try
        //            {
        //                salesModel.productCode = Convert.ToDecimal(rdr["productCode"].ToString());
        //            }
        //            catch (Exception ex)
        //            {

        //            }
        //            salesModel.productName = rdr["productName"].ToString();
        //            salesModel.barCode = rdr["barCode"].ToString();
        //            salesModel.batchId = Convert.ToDecimal(rdr["batchId"].ToString());
        //            salesModel.batchNo = rdr["batchNo"].ToString();
        //            salesModel.unitName = rdr["unitName"].ToString();
        //            salesModel.projectId = Convert.ToDecimal(rdr["projectId"].ToString());
        //            salesModel.qty = rdr["qty"].ToString();
        //            salesModel.rate = rdr["rate"].ToString();
        //            salesModel.conversionRate = rdr["conversionRate"].ToString();
        //            salesModel.unitConversionId = Convert.ToDecimal(rdr["unitConversionId"].ToString());
        //            salesModel.unitId = Convert.ToDecimal(rdr["unitId"].ToString());
        //            salesModel.godownId = rdr["godownId"].ToString();
        //            salesModel.rackId = Convert.ToDecimal(rdr["rackId"].ToString());
        //            salesModel.taxId = Convert.ToDecimal(rdr["taxId"].ToString());
        //            salesModel.voucherNo = rdr["voucherNo"].ToString();
        //            salesModel.voucherTypeId = Convert.ToDecimal(rdr["voucherTypeId"].ToString());
        //            salesModel.invoiceNo = rdr["invoiceNo"].ToString();
        //            salesModel.itemDescription = rdr["itemDescription"].ToString();
        //            salesModel.categoryId = Convert.ToDecimal(rdr["categoryId"].ToString());
        //            salesModel.amount = Convert.ToDecimal(rdr["amount"].ToString());
        //            salesModel.currencyId = Convert.ToDecimal(rdr["currencyId"].ToString());

        //            listsales.Add(salesModel);
        //        }

        //    }
        //    return listsales;
        //}
        /// <summary>
        /// Function to view DeliveryNote Details based on parameter
        /// </summary>
        /// <param name="decDeliveryNoteMasterId"></param>
        /// <returns></returns>
        public DataTable DeliveryNoteDetailsViewByDeliveryNoteMasterId(decimal decDeliveryNoteMasterId)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlDataAdapter sqlda = new SqlDataAdapter("DeliveryNoteDetailsViewByDeliveryNoteMasterId", sqlcon);
                sqlda.SelectCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter sqlParameter = new SqlParameter();
                sqlParameter = sqlda.SelectCommand.Parameters.Add("@deliveryNoteMasterId", SqlDbType.Decimal);
                sqlParameter.Value = decDeliveryNoteMasterId;
                sqlda.Fill(dtbl);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get Quantity details based on parameters
        /// </summary>
        /// <param name="decDeliveryNoteId"></param>
        /// <param name="decProductId"></param>
        /// <returns></returns>
        public DeliveryNoteDetailsInfo QuantityEditingAfterCheckingSalesAndRejectionInForDeliveryNote(decimal decDeliveryNoteId, decimal decProductId)
        {
            DeliveryNoteDetailsInfo infoDeliveryNoteDetails = new DeliveryNoteDetailsInfo();
            SqlDataReader sdrReader = null;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("QuantityEditingAfterCheckingSalesAndRejectionInForDeliveryNote", sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlcmd.Parameters.Add("@deliveryNoteMasterId", SqlDbType.Decimal);
                sprmparam.Value = decDeliveryNoteId;
                sprmparam = sqlcmd.Parameters.Add("@productId", SqlDbType.Decimal);
                sprmparam.Value = decProductId;
                sdrReader = sqlcmd.ExecuteReader();
                while (sdrReader.Read())
                {
                    infoDeliveryNoteDetails.Qty = Convert.ToDecimal(sdrReader["Qty"].ToString());
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sdrReader.Close();
                sqlcon.Close();
            }
            return infoDeliveryNoteDetails;
        }
        #endregion
    }
}
