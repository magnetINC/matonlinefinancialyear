﻿using MATFinancials.Classes.Info;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATFinancials.Classes.SP
{
    public class CategorySP : DBConnection
    {
        SqlCommand comm;
        SqlParameter param;
        DataTable dt;
        DataSet ds;
        SqlDataAdapter da;
        int RowsAffected = 0;
        bool IsTransactionSuccessfull = false;
        public CategorySP()
        {
           
        }
        public bool CategoryAdd(CategoryInfo CategoryInfo)
        {
            try
            {
                if(sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                comm = new SqlCommand("SP_Category_Insert", sqlcon);
                comm.CommandType = CommandType.StoredProcedure;
                //param = new SqlParameter();
                comm.Parameters.AddWithValue("@CategoryName", CategoryInfo.CategoryName);
                comm.Parameters.AddWithValue("@CreatedBy", CategoryInfo.CreatedBy);
                comm.Parameters.AddWithValue("@CreatedOn", CategoryInfo.CreatedOn);
                comm.Parameters.AddWithValue("@ModifiedBy", CategoryInfo.ModifiedBy);
                comm.Parameters.AddWithValue("@ModifiedOn", CategoryInfo.ModifiedOn);
                RowsAffected = comm.ExecuteNonQuery();
                if(RowsAffected < 0)
                {
                    IsTransactionSuccessfull = true;
                }
            }
            catch(Exception ex) { }
            finally
            {
                sqlcon.Close();
            }
            return IsTransactionSuccessfull;
        }
        public bool CategoryUpdate(CategoryInfo CategoryInfo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                comm = new SqlCommand("SP_Category_Update", sqlcon);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("CategoryId", CategoryInfo.CategoryId);
                comm.Parameters.AddWithValue("CategoryName", CategoryInfo.CategoryName);
                comm.Parameters.AddWithValue("@CreatedBy", CategoryInfo.CreatedBy);
                comm.Parameters.AddWithValue("@CreatedOn", CategoryInfo.CreatedOn);
                comm.Parameters.AddWithValue("@ModifiedBy", CategoryInfo.ModifiedBy);
                comm.Parameters.AddWithValue("@ModifiedOn", CategoryInfo.ModifiedOn);
                RowsAffected = comm.ExecuteNonQuery();
                if (RowsAffected > 0)
                {
                    IsTransactionSuccessfull = true;
                }
            }
            catch (Exception ex) { }
            finally
            {
                sqlcon.Close();
            }
            return IsTransactionSuccessfull;
        }
        public bool CategoryDelete(CategoryInfo CategoryInfo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                comm = new SqlCommand("SP_Category_Delete", sqlcon);
                comm.CommandType = CommandType.StoredProcedure;
                comm.Parameters.AddWithValue("CategoryId", CategoryInfo.CategoryId);
                RowsAffected = comm.ExecuteNonQuery();
                if (RowsAffected > 0)
                {
                    IsTransactionSuccessfull = true;
                }
            }
            catch (Exception ex) { }
            finally
            {
                sqlcon.Close();
            }
            return IsTransactionSuccessfull;
        }
        public DataTable CategoryView(CategoryInfo CategoryInfo)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                da = new SqlDataAdapter("SP_Category_View", sqlcon);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.Fill(dt);
            }
            catch (Exception ex) { }
            finally
            {
                sqlcon.Close();
            }
            return dt;
        }

        public DataTable CategoryViewAll()
        {
            try
            {
                dt = new DataTable(); da = new SqlDataAdapter();
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                da = new SqlDataAdapter("SP_Category_ViewAll", sqlcon);
                da.SelectCommand.CommandType = CommandType.StoredProcedure;
                da.Fill(dt);
            }
            catch (Exception ex) { }
            finally
            {
                sqlcon.Close();
            }
            return dt;
        }

    }
}
