﻿
 
using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;
namespace MATFinancials
{
    public class RoleSP : DBConnection
    {
        #region Function
        /// <summary>
        /// Function to insert values to Role Table
        /// </summary>
        /// <param name="infoRole"></param>
        /// <returns></returns>
        public decimal RoleAdd(RoleInfo infoRole)
        {
            decimal decRoleIdentity = 0;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RoleAdd", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@role", SqlDbType.VarChar);
                sprmparam.Value = infoRole.Role;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = infoRole.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = infoRole.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = infoRole.Extra2;
                decRoleIdentity = Convert.ToDecimal(sccmd.ExecuteScalar());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return decRoleIdentity;
        }
        /// <summary>
        /// Function to Update values in Role Table
        /// </summary>
        /// <param name="infoRole"></param>
        public int RoleEdit(RoleInfo infoRole)
        {
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RoleEdit", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@roleId", SqlDbType.Decimal);
                sprmparam.Value = infoRole.RoleId;
                sprmparam = sccmd.Parameters.Add("@role", SqlDbType.VarChar);
                sprmparam.Value = infoRole.Role;
                sprmparam = sccmd.Parameters.Add("@narration", SqlDbType.VarChar);
                sprmparam.Value = infoRole.Narration;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = infoRole.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = infoRole.Extra2;
                return sccmd.ExecuteNonQuery();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return 0;
        }
        /// <summary>
        /// Function to get all the values from Role Table
        /// </summary>
        /// <returns></returns>
        public DataTable RoleViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("RoleViewAll", sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get particular values from Role table based on the parameter
        /// </summary>
        /// <param name="roleId"></param>
        /// <returns></returns>
        public RoleInfo RoleView(decimal roleId, string con)
        {
            RoleInfo infoRole = new RoleInfo();
            SqlDataReader sdrreader = null;
            try
            {
                //DefaultDatabaseConnection = con;
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RoleView", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@roleId", SqlDbType.Decimal);
                sprmparam.Value = roleId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    infoRole.RoleId = decimal.Parse(sdrreader[0].ToString());
                    infoRole.Role = sdrreader[1].ToString();
                    infoRole.Narration = sdrreader[2].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sdrreader.Close();
                sqlcon.Close();
            }
            return infoRole;
        }
        /// <summary>
        /// Function to get all role details
        /// </summary>
        /// <returns></returns>
        public List<RoleInfo> RoleViewGridFill()
        {
            DataTable dtbl = new DataTable();
            List<RoleInfo> resp = new List<RoleInfo>();
            //dtbl.Columns.Add("SlNo", typeof(int));
            //dtbl.Columns["SlNo"].AutoIncrement = true;
            //dtbl.Columns["SlNo"].AutoIncrementSeed = 1;
            //dtbl.Columns["SlNo"].AutoIncrementStep = 1;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("RoleViewAll", sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
                for(int i=0;i<dtbl.Rows.Count; i++)
                {
                    resp.Add(new RoleInfo {
                        RoleId = Convert.ToDecimal(dtbl.Rows[i].ItemArray[0]),
                        Role = Convert.ToString(dtbl.Rows[i].ItemArray[1])
                    });
                }
            }
            catch (Exception ex)
            {
                //MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return resp;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter
        /// </summary>
        /// <param name="RoleId"></param>
        /// <returns></returns>
        public decimal RoleReferenceDelete(decimal RoleId)
        {
            decimal decRole = 0;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("RoleReferenceDelete", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@roleId", SqlDbType.Decimal);
                sprmparam.Value = RoleId;
                decRole = Convert.ToDecimal(sccmd.ExecuteNonQuery().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return decRole;
        }
        /// <summary>
        /// Function to check the existance of Role
        /// </summary>
        /// <param name="decRoleId"></param>
        /// <param name="strRole"></param>
        /// <returns></returns>
        public bool RoleCheckExistence(decimal decRoleId, string strRole)
        {
            bool isEdit = false;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sqlcmd = new SqlCommand("RoleCheckExistence", sqlcon);
                sqlcmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sqlcmd.Parameters.Add("@roleId", SqlDbType.Decimal);
                sprmparam.Value = decRoleId;
                sprmparam = sqlcmd.Parameters.Add("@role", SqlDbType.VarChar);
                sprmparam.Value = strRole;
                object obj = sqlcmd.ExecuteScalar();
                if (obj != null)
                {
                    if (int.Parse(obj.ToString()) == 1)
                    {
                        isEdit = true;
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return isEdit;
        }
        #endregion
    }
}
