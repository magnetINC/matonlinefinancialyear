

using System;
using System.Collections.Generic;
using System.Text;
using System.Windows.Forms;
using System.Data;
using System.Data.SqlClient;

//<summary>    
//Summary description for PaymentDetailsSP    
//</summary>    
namespace MATFinancials
{
    public class PaymentDetailsSP : DBConnection
    {
        #region Function
        /// <summary>
        /// Function to insert values to PaymentDetails Table
        /// </summary>
        /// <param name="paymentdetailsinfo"></param>
        /// <returns></returns>
        public decimal PaymentDetailsAdd(PaymentDetailsInfo paymentdetailsinfo)
        {
            decimal decPaymentDetailsId = 0;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("PaymentDetailsAdd", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@paymentMasterId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.PaymentMasterId;
                sprmparam = sccmd.Parameters.Add("@ledgerId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.LedgerId;
                sprmparam = sccmd.Parameters.Add("@amount", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.Amount;
                sprmparam = sccmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.ExchangeRateId;
                sprmparam = sccmd.Parameters.Add("@chequeNo", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.ChequeNo;
                sprmparam = sccmd.Parameters.Add("@chequeDate", SqlDbType.DateTime);
                sprmparam.Value = paymentdetailsinfo.ChequeDate;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.Extra2;
                sprmparam = sccmd.Parameters.Add("@ProjectId", SqlDbType.Int);
                sprmparam.Value = paymentdetailsinfo.ProjectId;
                sprmparam = sccmd.Parameters.Add("@CategoryId", SqlDbType.Int);
                sprmparam.Value = paymentdetailsinfo.CategoryId;
                sprmparam = sccmd.Parameters.Add("@memo", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.Memo;
                sprmparam = sccmd.Parameters.Add("@invoiceNo", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.InvoiceNo;
                sprmparam = sccmd.Parameters.Add("@withholdingTaxId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.withholdingTaxId;
                sprmparam = sccmd.Parameters.Add("@withholdingTaxAmount", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.withholdingTaxAmount;
                sprmparam = sccmd.Parameters.Add("@grossAmount", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.grossAmount;
                decPaymentDetailsId = Convert.ToDecimal(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return decPaymentDetailsId;
        }
        /// <summary>
        /// Function to insert values to PaymentDetails Table
        /// </summary>
        /// <param name="paymentdetailsinfo"></param>
        /// <returns></returns>
        public decimal PendingPaymentDetailsAdd(PaymentDetailsInfo paymentdetailsinfo)
        {
            decimal decPaymentDetailsId = 0;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("PendingPaymentDetailsAdd", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@pendingPaymentMasterId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.PaymentMasterId;
                sprmparam = sccmd.Parameters.Add("@ledgerId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.LedgerId;
                sprmparam = sccmd.Parameters.Add("@amount", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.Amount;
                sprmparam = sccmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.ExchangeRateId;
                sprmparam = sccmd.Parameters.Add("@chequeNo", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.ChequeNo;
                sprmparam = sccmd.Parameters.Add("@doneBy", SqlDbType.DateTime);
                sprmparam.Value = paymentdetailsinfo.DoneBy;
                sprmparam = sccmd.Parameters.Add("@chequeDate", SqlDbType.DateTime);
                sprmparam.Value = paymentdetailsinfo.ChequeDate;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.Extra2;
                sprmparam = sccmd.Parameters.Add("@ProjectId", SqlDbType.Int);
                sprmparam.Value = paymentdetailsinfo.ProjectId;
                sprmparam = sccmd.Parameters.Add("@CategoryId", SqlDbType.Int);
                sprmparam.Value = paymentdetailsinfo.CategoryId;
                sprmparam = sccmd.Parameters.Add("@memo", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.Memo;
                sprmparam = sccmd.Parameters.Add("@withholdingTaxId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.withholdingTaxId;
                sprmparam = sccmd.Parameters.Add("@withholdingTaxAmount", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.withholdingTaxAmount;
                sprmparam = sccmd.Parameters.Add("@grossAmount", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.grossAmount;
                decPaymentDetailsId = Convert.ToDecimal(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return decPaymentDetailsId;
        }
        /// <summary>
        /// Returns all rows of PendingPaymentDetails
        /// </summary>
        /// <returns></returns>
        public DataTable PendingPaymentDetailsViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("PendingPaymentDetailsViewAll", sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return dtbl;
        }
        public DataTable PendingPaymentDetailsViewBySalesMasterId(decimal pendingPaymentMasterId)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlDataAdapter sqldataadapter = new SqlDataAdapter("PendingPaymentDetailsViewByPendingPaymentMasterId", sqlcon);
                sqldataadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                SqlParameter sqlparameter = new SqlParameter();
                sqlparameter = sqldataadapter.SelectCommand.Parameters.Add("@pendingPaymentMasterId", SqlDbType.VarChar);
                sqlparameter.Value = pendingPaymentMasterId;
                sqldataadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                Messages.ErrorMessage(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to  get the next id for PaymentDetails Table
        /// </summary>
        /// <returns></returns>
        public string GetCurrencyNameByExchangeRate(string exchangeRateId)
        {
            string currencyName = "";
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("CurrencyNameByExchangeRate", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = exchangeRateId;
                currencyName = sccmd.ExecuteScalar().ToString();
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return currencyName;
        }
        /// <summary>
        /// Function to Update values in PaymentDetails Table
        /// </summary>
        /// <param name="paymentdetailsinfo"></param>
        /// <returns></returns>
        public decimal PaymentDetailsEdit(PaymentDetailsInfo paymentdetailsinfo)
        {
            decimal decPaymentDetailsId = 0;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("PaymentDetailsEdit", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@paymentDetailsId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.PaymentDetailsId;
                sprmparam = sccmd.Parameters.Add("@paymentMasterId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.PaymentMasterId;
                sprmparam = sccmd.Parameters.Add("@ledgerId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.LedgerId;
                sprmparam = sccmd.Parameters.Add("@amount", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.Amount;
                sprmparam = sccmd.Parameters.Add("@exchangeRateId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.ExchangeRateId;
                sprmparam = sccmd.Parameters.Add("@chequeNo", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.ChequeNo;
                sprmparam = sccmd.Parameters.Add("@chequeDate", SqlDbType.DateTime);
                sprmparam.Value = paymentdetailsinfo.ChequeDate;
                sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.Extra1;
                sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.Extra2;
                sprmparam = sccmd.Parameters.Add("@ProjectId", SqlDbType.Int);
                sprmparam.Value = paymentdetailsinfo.ProjectId;
                sprmparam = sccmd.Parameters.Add("@CategoryId", SqlDbType.Int);
                sprmparam.Value = paymentdetailsinfo.CategoryId;
                sprmparam = sccmd.Parameters.Add("@memo", SqlDbType.VarChar);
                sprmparam.Value = paymentdetailsinfo.Memo;
                sprmparam = sccmd.Parameters.Add("@withholdingTaxId", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.withholdingTaxId;
                sprmparam = sccmd.Parameters.Add("@withholdingTaxAmount", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.withholdingTaxAmount;
                sprmparam = sccmd.Parameters.Add("@grossAmount", SqlDbType.Decimal);
                sprmparam.Value = paymentdetailsinfo.grossAmount;
                decPaymentDetailsId = Convert.ToDecimal(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return decPaymentDetailsId;
        }
        /// <summary>
        /// Function to get all the values from PaymentDetails Table
        /// </summary>
        /// <returns></returns>
        public DataTable PaymentDetailsViewAll()
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlDataAdapter sdaadapter = new SqlDataAdapter("PaymentDetailsViewAll", sqlcon);
                sdaadapter.SelectCommand.CommandType = CommandType.StoredProcedure;
                sdaadapter.Fill(dtbl);
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return dtbl;
        }
        /// <summary>
        /// Function to get particular values from PaymentDetails Table based on the parameter
        /// </summary>
        /// <param name="paymentDetailsId"></param>
        /// <returns></returns>
        public PaymentDetailsInfo PaymentDetailsView(decimal paymentDetailsId)
        {
            PaymentDetailsInfo paymentdetailsinfo = new PaymentDetailsInfo();
            SqlDataReader sdrreader = null;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("PaymentDetailsView", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@paymentDetailsId", SqlDbType.Decimal);
                sprmparam.Value = paymentDetailsId;
                sdrreader = sccmd.ExecuteReader();
                while (sdrreader.Read())
                {
                    paymentdetailsinfo.PaymentDetailsId = decimal.Parse(sdrreader[0].ToString());
                    paymentdetailsinfo.PaymentMasterId = decimal.Parse(sdrreader[1].ToString());
                    paymentdetailsinfo.LedgerId = decimal.Parse(sdrreader[2].ToString());
                    paymentdetailsinfo.Amount = decimal.Parse(sdrreader[3].ToString());
                    paymentdetailsinfo.ExchangeRateId = decimal.Parse(sdrreader["exchangeRateId"].ToString());
                    paymentdetailsinfo.ChequeNo = sdrreader[4].ToString();
                    paymentdetailsinfo.ChequeDate = DateTime.Parse(sdrreader[5].ToString());
                    paymentdetailsinfo.ExtraDate = DateTime.Parse(sdrreader[6].ToString());
                    paymentdetailsinfo.Extra1 = sdrreader[7].ToString();
                    paymentdetailsinfo.Extra2 = sdrreader[8].ToString();
                    paymentdetailsinfo.ProjectId = Convert.ToInt32(sdrreader[9]);
                    paymentdetailsinfo.CategoryId = Convert.ToInt32(sdrreader[10]);
                    paymentdetailsinfo.Memo = sdrreader[11].ToString();
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sdrreader.Close();
                sqlcon.Close();
            }
            return paymentdetailsinfo;
        }
        /// <summary>
        /// Function to delete particular details based on the parameter From Table PaymentDetails
        /// </summary>
        /// <param name="PaymentDetailsId"></param>
        //public void PaymentDetailsDelete(decimal PaymentDetailsId)
        //{
        //    try
        //    {
        //        if (sqlcon.State == ConnectionState.Closed)
        //        {
        //            sqlcon.Open();
        //        }
        //        SqlCommand sccmd = new SqlCommand("PaymentDetailsDelete", sqlcon);
        //        sccmd.CommandType = CommandType.StoredProcedure;
        //        SqlParameter sprmparam = new SqlParameter();
        //        sprmparam = sccmd.Parameters.Add("@paymentDetailsId", SqlDbType.Decimal);
        //        sprmparam.Value = PaymentDetailsId;
        //        sccmd.ExecuteNonQuery();
        //    }
        //    catch (Exception ex)
        //    {
        //        MessageBox.Show(ex.ToString());
        //    }
        //    finally
        //    {
        //        sqlcon.Close();
        //    }
        //}
        /// <summary>
        /// Function to  get the next id for PaymentDetails Table
        /// </summary>
        /// <returns></returns>
        public int PaymentDetailsGetMax()
        {
            int max = 0;
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("PaymentDetailsMax", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                max = int.Parse(sccmd.ExecuteScalar().ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {
                sqlcon.Close();
            }
            return max;
        }
        /// <summary>
        /// Function to Payment Details View 
        /// </summary>
        /// <param name="paymentMastertId"></param>
        /// <returns></returns>
        public DataTable PaymentDetailsViewByMasterId(decimal paymentMastertId)
        {
            DataTable dtbl = new DataTable();
            try
            {
                if (sqlcon.State == ConnectionState.Closed)
                {
                    sqlcon.Open();
                }
                SqlCommand sccmd = new SqlCommand("PaymentDetailsViewByMasterId", sqlcon);
                sccmd.CommandType = CommandType.StoredProcedure;
                SqlParameter sprmparam = new SqlParameter();
                sprmparam = sccmd.Parameters.Add("@paymentMasterId", SqlDbType.Decimal);
                sprmparam.Value = paymentMastertId;
                SqlDataAdapter sqlda = new SqlDataAdapter();
                sqlda.SelectCommand = sccmd;
                sqlda.Fill(dtbl);

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
            finally
            {

                sqlcon.Close();
            }
            return dtbl;
        }

        public class PaymentDetailsDelete
        {
            private decimal masterid;

            public PaymentDetailsDelete(decimal masterid)
            {
                this.masterid = masterid;
            }
        }
        #endregion
    }
}
