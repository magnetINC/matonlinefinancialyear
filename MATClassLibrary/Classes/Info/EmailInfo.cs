﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MATFinancials.Classes.Info
{
    class EmailInfo
    {
        public int emailConfigurationId { get; set; }
        public string EmailId { get; set; }
        public string LogonPassword { get; set; }
        public string IsDefault { get; set; }
        public string smtp { get; set; }
        public string portNumber { get; set; }
        public bool enableSSL { get; set; }
    }
}
