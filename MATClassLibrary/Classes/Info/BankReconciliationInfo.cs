
using System;
using System.Collections.Generic;
using System.Text;
//<summary>    
//Summary description for BankReconciliationInfo    
//</summary>    
namespace MATFinancials
{
    public class BankReconciliationInfo
    {
        private decimal _reconcileId;
        private decimal _ledgerPostingId;
        private DateTime _statementDate;
        private DateTime _extraDate;
        private string _extra1;
        private string _extra2;
        private DateTime _ReconStartDate;
        private DateTime _ReconEndDate;


        public decimal ReconcileId
        {
            get { return _reconcileId; }
            set { _reconcileId = value; }
        }
        public decimal LedgerPostingId
        {
            get { return _ledgerPostingId; }
            set { _ledgerPostingId = value; }
        }
        public DateTime StatementDate
        {
            get { return _statementDate; }
            set { _statementDate = value; }
        }
        public DateTime ExtraDate
        {
            get { return _extraDate; }
            set { _extraDate = value; }
        }
        public string Extra1
        {
            get { return _extra1; }
            set { _extra1 = value; }
        }
        public string Extra2
        {
            get { return _extra2; }
            set { _extra2 = value; }
        }
        public  DateTime ReconStartDate
        {
            get { return _ReconStartDate; }
            set { _ReconStartDate = value; }
        }
        public DateTime ReconEndDate
        {
            get { return _ReconEndDate; }
            set { _ReconEndDate = value; }
        }
    }
}
