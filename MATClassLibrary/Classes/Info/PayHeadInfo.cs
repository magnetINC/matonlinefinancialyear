


using System;
using System.Collections.Generic;
using System.Text;
//<summary>    
//Summary description for PayHeadInfo    
//</summary>    
namespace MATFinancials
{
    //used by Niyas
    //Date:12-03-2013
    public class PayHeadInfo
    {


        public decimal PayHeadId
        {
            get;
            set;
        }
        public string PayHeadName
        {
            get;
            set;
        }
        public string Type
        {
            get;
            set;
        }
        public string Narration
        {
            get;
            set;
        }
        public DateTime ExtraDate
        {
            get;
            set;
        }
        public string Extra1
        {
            get;
            set;
        }
        public string Extra2
        {
            get;
            set;
        }
        public int ReceivingLedgerId { get; set; }
    }
}
