﻿using MATFinancials.Classes.Info;
using MATFinancials.Classes.SP;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace MATFinancials.Other
{
    public partial class frmEmailConfiguration : Form
    {
        public frmEmailConfiguration()
        {
            InitializeComponent();
        }

        public int IDtoDelete { get; set; }
        private void btnClose_Click(object sender, EventArgs e)
        {
            try
            {
                if (Messages.CloseConfirmation())
                {
                    this.Close();
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "EC01" + ex.Message;
                throw;
            }
        }

        private void btnClear_Click(object sender, EventArgs e)
        {
            txtEmailId.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtConfirmPassword.Text = string.Empty;
            txtPassword.Enabled = true;
            txtConfirmPassword.Enabled = true;
            txtSmtp.Text = string.Empty;
            txtPortNumber.Text = string.Empty;
            chkEnableSSL.Checked = true;
            chkIdDefault.Checked = true;
            btnSave.Text = "Save";
            cmbMailProvider.SelectedIndex = -1;
        }

        private void btnSave_Click(object sender, EventArgs e)
        {
            if(txtEmailId.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Email ID is Required");
                txtEmailId.Focus();
                return;
            }
            if(txtPassword.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Password is Required.");
                txtPassword.Focus();
                return;
            }
            if(txtConfirmPassword.Text.Trim() == string.Empty)
            {
                MessageBox.Show("Confirm Password Field is Required.");
                txtConfirmPassword.Focus();
                return;
            }
            if(txtPassword.Text != txtConfirmPassword.Text)
            {
                MessageBox.Show("Password and Confirm Password Fields Do not Match");
                txtPassword.Text = string.Empty;
                txtConfirmPassword.Text = string.Empty;
                txtPassword.Focus();
                return;
            }
            if (cmbMailProvider.SelectedIndex == -1)
            {
                MessageBox.Show("Select email service provider");
                txtSmtp.Focus();
                return;
            }
            else if (cmbMailProvider.SelectedIndex == 0)
            {
                txtSmtp.Text = "smtp.gmail.com";
                txtPortNumber.Text = "587";
                chkEnableSSL.Checked = true;
            }
            else if (cmbMailProvider.SelectedIndex == 1)
            {
                txtSmtp.Text = "smtp.mail.yahoo.com";
                txtPortNumber.Text = "465";
                chkEnableSSL.Checked = true;
            }
            else if (cmbMailProvider.SelectedIndex == 2)
            {
                txtSmtp.Text = "smtp.live.com";
                txtPortNumber.Text = "587";
                chkEnableSSL.Checked = true;
            }
            //if (txtPortNumber.Text.Trim() == string.Empty)
            //{
            //    MessageBox.Show("Enter Port Number");
            //    txtPortNumber.Focus();
            //    return;
            //}
            SaveFunction();
        }

        public void SaveFunction()
        {
            if (btnSave.Text == "Update")
            {
                UpdateEmailConfig();
            }
            else
            {
                EmailSP spEmail = new EmailSP();
                EmailInfo infoEmail = new EmailInfo();
                string encryptedComingPassword = new MATFinancials.Classes.Security().base64Encode(txtPassword.Text);

                infoEmail.EmailId = txtEmailId.Text;
                infoEmail.LogonPassword = encryptedComingPassword;
                infoEmail.IsDefault = "Yes";
                infoEmail.smtp = txtSmtp.Text.Trim();
                infoEmail.portNumber = txtPortNumber.Text.Trim();
                infoEmail.enableSSL = chkEnableSSL.Checked;
                spEmail.EmailConfigAdd(infoEmail);
                MessageBox.Show("Email ID Configuration Was Successful");
                txtConfirmPassword.Text = string.Empty;
                txtEmailId.Text = string.Empty;
                txtPassword.Text = string.Empty;
                txtSmtp.Text = string.Empty;
                txtPortNumber.Text = string.Empty;
                chkEnableSSL.Checked = true;
                chkIdDefault.Checked = true;
                cmbMailProvider.SelectedIndex = -1;
            }
            GridFill();
        }

        public void UpdateEmailConfig()
        {
            try
            {
                EmailSP spEmail = new EmailSP();
                EmailInfo infoEmail = new EmailInfo();
                string encryptedComingPassword = new MATFinancials.Classes.Security().base64Encode(txtPassword.Text.Trim());
                infoEmail.EmailId = txtEmailId.Text;
                if (chkIdDefault.Checked == true)
                {
                    infoEmail.IsDefault = "Yes";
                }
                else
                {
                    infoEmail.IsDefault = "No";
                }
                infoEmail.emailConfigurationId = IDtoDelete;
                infoEmail.LogonPassword = encryptedComingPassword;
                infoEmail.smtp = txtSmtp.Text;
                infoEmail.portNumber = txtPortNumber.Text;
                infoEmail.enableSSL = chkEnableSSL.Checked;
                spEmail.EmailConfigUpdate(infoEmail);
                MessageBox.Show("Update was Successful");
            }
            catch(Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }

        private void frmEmailConfiguration_Load(object sender, EventArgs e)
        {
            chkEnableSSL.Checked = true;
            cmbMailProvider.SelectedIndex = -1;
            GridFill();
        }

        private void button1_Click(object sender, EventArgs e)
        {
            if(emailConfigPanel.Visible == false)
            {
                emailConfigPanel.Visible = true;
                emailConfigPanel.Enabled = true;
            }
        }

        private void btnEdit_Click(object sender, EventArgs e)
        {
            EditFunction();
        }

        public void EditFunction()
        {


        }
        public void GridFill()
        {
            EmailSP spEmail = new EmailSP();
            DataTable dtbl = new DataTable();
            dtbl = spEmail.EmailConfigViewAll();
            dgvEmailConfiguration.DataSource = dtbl; 
        }

        private void dgvEmailConfiguration_CellContentDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if(e.RowIndex != -1)
                {
                    if(dgvEmailConfiguration.CurrentRow.Index == e.RowIndex)
                    {
                        IDtoDelete = Convert.ToInt32(dgvEmailConfiguration.Rows[e.RowIndex].Cells["SNO"].Value.ToString());
                        FillControls();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }

        /// <summary>
        /// Fills controls for UserEdit
        /// </summary>
        public  void FillControls()
        {
            // Code to implement here 
            EmailInfo infoEmail = new EmailInfo();
            EmailSP spEmail = new EmailSP();
            infoEmail = spEmail.EmailConfigView(IDtoDelete);
            string EncryptedPassword = new Classes.Security().base64Decode(infoEmail.LogonPassword);

            txtEmailId.Text = infoEmail.EmailId;
            txtPassword.Text = EncryptedPassword;
            txtConfirmPassword.Text = EncryptedPassword;
            if(infoEmail.IsDefault == "Yes")
            {
                chkIdDefault.Checked = true;
            }
            else
            {
                chkIdDefault.Checked = false;
            }
            if (infoEmail.smtp.Contains("gmail")) { cmbMailProvider.SelectedIndex = 0; }
            if (infoEmail.smtp.Contains("yahoo")) { cmbMailProvider.SelectedIndex = 1; }
            if (infoEmail.smtp.Contains("live")) { cmbMailProvider.SelectedIndex = 2; }
            txtSmtp.Text = infoEmail.smtp;
            txtPortNumber.Text = infoEmail.portNumber;
            chkEnableSSL.Checked = infoEmail.enableSSL;
            txtPassword.Enabled = true;
            txtConfirmPassword.Enabled = true;
            btnSave.Text = "Update";
        }

        public void DeleteFunction()
        {
            try
            {
                EmailSP spEmail = new EmailSP();
                spEmail.DeleteEmail(IDtoDelete);
                MessageBox.Show("Email Was Successfully Deleted");
                
                Clear();

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
            
        }

        /// <summary>
        /// Function to reset controls
        /// </summary>
        public void Clear()
        {
            txtConfirmPassword.Text = string.Empty;
            txtEmailId.Text = string.Empty;
            txtPassword.Text = string.Empty;
            txtSmtp.Text = string.Empty;
            txtPortNumber.Text = string.Empty;
            chkEnableSSL.Checked = true;
            chkIdDefault.Checked = true;
            btnSave.Text = "Save";
            GridFill();
        }
        /// <summary>
        /// Function to Carry out Delete
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void btnDelete_Click(object sender, EventArgs e)
        {
            DeleteFunction();
        }

        private void dgvEmailConfiguration_CellDoubleClick(object sender, DataGridViewCellEventArgs e)
        {
            try
            {
                if (e.RowIndex != -1)
                {
                    if (dgvEmailConfiguration.CurrentRow.Index == e.RowIndex)
                    {
                        IDtoDelete = Convert.ToInt32(dgvEmailConfiguration.Rows[e.RowIndex].Cells["SNO"].Value.ToString());
                        FillControls();
                    }
                }
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }
        }
    }
}
