﻿
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.ComponentModel;
using System.Data;

namespace MATFinancialsI
{
    public class OverAllClass : INotifyPropertyChanged
    {
        private String myClass;

        public String Class
        {
            get { return myClass; }
            set {
                myClass = value;
                RaisePropertyChangeEvent("Class");
            }
        }

        private double dblValue;

        public double Value
        {
            get { return dblValue; }
            set {
                dblValue = value;
                RaisePropertyChangeEvent("Value");
            }
        }

        public static List<OverAllClass> ConstructTest(string str, DataTable dt)
        {
            List<OverAllClass> OverAllData = new List<OverAllClass>();
            if (str == "Income")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["Nature"].ToString() == "Income")
                    {
                        OverAllData.Add(new OverAllClass() { Class = dt.Rows[i][1].ToString(), Value = Convert.ToDouble(dt.Rows[i][2]) });
                    }

                }
            }
            else if (str == "Expence")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["Nature"].ToString() == "Expenses")
                    {
                        OverAllData.Add(new OverAllClass() { Class = dt.Rows[i][1].ToString(), Value = Convert.ToDouble(dt.Rows[i][2]) });
                    }

                }
            }
            else if (str == "Asset")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["Nature"].ToString() == "Assets")
                    {
                        OverAllData.Add(new OverAllClass() { Class = dt.Rows[i][1].ToString(), Value = Convert.ToDouble(dt.Rows[i][2]) });
                    }
                   
                }
               
            }
            else if (str == "Liability")
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    if (dt.Rows[i]["Nature"].ToString() == "Liabilities")
                    {
                        OverAllData.Add(new OverAllClass() { Class = dt.Rows[i][1].ToString(), Value = Convert.ToDouble(dt.Rows[i][2]) });
                    }

                }
            }
            return OverAllData;
        }
        #region INotifyPropertyChanged Members

        public event PropertyChangedEventHandler PropertyChanged;

        private void RaisePropertyChangeEvent(String propertyName)
        {
            if (PropertyChanged != null)
                this.PropertyChanged(this, new PropertyChangedEventArgs(propertyName));
        }

        #endregion
    }
}
