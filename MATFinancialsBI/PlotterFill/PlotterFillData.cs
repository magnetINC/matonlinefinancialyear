﻿
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MATFinancials.MATFinancialsIChart.Common;

namespace MATFinancialsI.PlotterFill
{
    public class PlotterFillData : RingArray<QueryFill>
    {
        private const int TOTAL_POINTS = 1000;

        public PlotterFillData()
            : base(TOTAL_POINTS)
        {

        }
    }

    public class QueryFill
    {
        public DateTime Date { get; set; }

        public double Value { get; set; }

        public string PopUp { get; set; }

        public QueryFill(double value, DateTime date, string popUp)
        {
            this.Date = date;
            this.Value = value;
            this.PopUp = popUp;
        }
    }
}
