﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Payroll.Controllers
{
    public class LeaveManagementController : Controller
    {
        // GET: Payroll/LeaveManagement
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult LeaveType()
        {
            return View();
        }

        public ActionResult LeaveApplication()
        {
            return View();
        }

        public ActionResult LeaveApproval()
        {
            return View();
        }

        public ActionResult LeaveApprovalStageOne()
        {
            return View();
        }

        public ActionResult LeaveApprovalStageTwo()
        {
            return View();
        }

        public ActionResult LeaveApprovalStageThree()
        {
            return View();
        }

        public ActionResult LeaveApprovalHr()
        {
            return View();
        }
    }
}