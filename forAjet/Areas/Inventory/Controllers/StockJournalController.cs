﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Inventory.Controllers
{
    public class StockJournalController : Controller
    {
        // GET: Inventory/ItemCreation
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult StockJournalConfirmation()
        {
            return View();
        }
        public ActionResult StockJournalRegister()
        {
            return View();
        }
        public ActionResult TransferIn()
        {
            return View();
        }
        public ActionResult TransferOut()
        {
            return View();
        }
        public ActionResult StockTransferEdit(int id)
        {
            ViewBag.journalId = id;
            return View();
        }

    }
}