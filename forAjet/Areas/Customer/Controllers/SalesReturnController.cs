﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Customer.Controllers
{
    public class SalesReturnController : Controller
    {
        // GET: Customer/SalesReturn
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SalesReturnListing()
        {
            return View();
        }
        public ActionResult SalesReturnRegister()
        {
            return View();
        }
    }
}