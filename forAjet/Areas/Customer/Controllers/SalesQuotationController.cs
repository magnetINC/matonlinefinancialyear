﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Customer.Controllers
{
    public class SalesQuotationController : Controller
    {
        // GET: Customer/SalesQuotation
        public ActionResult Index()
        {
            return View();
        }
        public ActionResult SalesQuotationRegister()
        {
            return View();
        }

        public ActionResult SalesOrderRegisterByStatus()
        {
            return View();
        }
        public ActionResult SalesQuotationRegisterDetails(int id)
        {
            ViewBag.SalesQuotationMasterId = id;
            return View();
        }
    }
}