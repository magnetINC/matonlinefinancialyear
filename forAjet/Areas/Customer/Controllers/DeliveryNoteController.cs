﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;
using System.IO;

namespace MatOnline.Areas.Customer.Controllers
{
    public class DeliveryNoteController : Controller
    {
        // GET: Customer/DeliveryNote
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult DeliveryNoteRegister()
        {
            return View();
        }

        public ActionResult GenerateWayBill(decimal rid)    //rid-release note id
        {
            ViewBag.ReleaseNoteId = rid;
            return View();
        }

        public ActionResult ProcessReleaseForm()
        {
            return View();
        }

        public ActionResult ProcessReleaseFormDetails(decimal rid)
        {
            ViewBag.ReleaseNoteId = rid;
            return View();
        }

        public void Report()
        {

            string cont = "";
            cont += "<html>";
            cont += "<head>";
            //cont += "<script>";
            //cont += System.IO.File.ReadAllText(Server.MapPath("~/Assets/mat/js/bootstrap.min.js"));
            //cont += System.IO.File.ReadAllText(Server.MapPath("~/Assets/mat/js/jquery-2.1.1.min.js"));
            //cont += "</script>";
            //cont += "<style>";
            //cont += System.IO.File.ReadAllText(Server.MapPath("~/Assets/mat/css/bootstrap.min.css"));
            //cont += "</style>";
            cont += "</head>";
            cont += "<body>";
            cont += System.IO.File.ReadAllText(Server.MapPath("~/Areas/Customer/Views/DeliveryNote/Report.cshtml"));
            cont += "</body>";
            cont += "</html>";

            string HTMLContent = cont;
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + "PDFfile.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(GetPDF(HTMLContent));
            Response.End();
            
        }

        public byte[] GetPDF(string pHTML)
        {
            byte[] bPDF = null;

            MemoryStream ms = new MemoryStream();
            TextReader txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class  
            Document doc = new Document(PageSize.A4, 25, 25, 25, 25);

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
            PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 3: we create a worker parse the document  
            HTMLWorker htmlWorker = new HTMLWorker(doc);
            
            // 4: we open document and start the worker on the document  
            doc.Open();
            htmlWorker.StartDocument();


            // 5: parse the html into the document  
            htmlWorker.Parse(txtReader);

            // 6: close the document and the worker  
            htmlWorker.EndDocument();
            htmlWorker.Close();
            doc.Close();

            bPDF = ms.ToArray();

            return bPDF;
        }
    }
}