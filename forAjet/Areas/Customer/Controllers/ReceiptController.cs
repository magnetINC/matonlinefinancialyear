﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Customer.Controllers
{
    public class ReceiptController : Controller
    {
        // GET: Customer/Receipt
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Receipt()
        {
            return View();
        }
        public ActionResult EditReceipt(int id)
        {
            ViewBag.ReceiptId = id;
            return View();
        }
    }
}