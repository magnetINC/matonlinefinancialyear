﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Customer.Controllers
{
    public class SalesOrderController : Controller
    {
        // GET: Customer/SalesOrder
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult OrderConfirmationListing()
        {
            return View();
        }

        //public ActionResult OrderAuthorizationListing()
        //{
        //    return View();
        //}

        //public ActionResult ReleaseFormListing()
        //{
        //    return View();
        //}

        public ActionResult ReleaseFormDetails()
        {
            return View();
        }

        public ActionResult WaybillGenerationListing()
        {
            return View();
        }

        public ActionResult GenerateWaybill()
        {
            return View();
        }

        public ActionResult SalesOrderRegister()
        {
            return View();
        }
        public ActionResult SalesOrderConfirmationRegister()
        {
            return View();
        }
        public ActionResult SalesOrderRegisterDetails(int id)
        {
            ViewBag.salesOderMasterId = id;
            return View();
        }

        public ActionResult OrderToConfirmation()
        {
            return View();
        }

        public ActionResult OrderAuthorizationListing()
        {
            return View();
        }

        public ActionResult ReleaseFormListing()
        {
            return View();
        }
    }

    public class OrderConfirmationDetailsVM
    {
        public int MyProperty { get; set; }
    }
}