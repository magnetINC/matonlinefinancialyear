﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Company.Controllers.Masters
{
    public class BudgetController : Controller
    {
        // GET: Company/Budget
        public ActionResult BudgetSettings()
        {
            return View();
        }
        public ActionResult BudgetVariance()
        {
            return View();
        }
    }
}