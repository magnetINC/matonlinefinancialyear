﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClosedXML;
using ClosedXML.Excel;
using MatApi.Models.Reports.OtherReports;
using MATFinancials.DAL;
using MatApi.Models;
using MATFinancials.Classes.HelperClasses;
using System.Globalization;

namespace MatOnline.Areas.Company.Controllers.Masters.Account
{
    public class AccountLedgerController : Controller
    {
        // GET: Company/AccountLedger
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Create()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Edit()
        {
            return View();
        }

        [HttpPost]
        public ActionResult Details()
        {
            ViewBag.LedgerId = Request["ledgerId"];
            return View();
        }

        //this is used to capture "GET" made to the details method, so user is redirected back to home page of ledgers
        //since we don't want ledger id passed as get parameter
        [HttpGet]
        public ActionResult Details(int? id)
        {
            return RedirectToAction("Index", "AccountLedger", new { area = "Company" });
        }
        public FileResult Export()
        {
            DataTable excelData = new DataTable("Grid");

            excelData.Columns.AddRange(new DataColumn[] {
                new DataColumn("Account Name"),
                new DataColumn("Account Type"),
                new DataColumn("Description"),
                new DataColumn("Account No."),
                new DataColumn("Company Name"),
                new DataColumn("E-mail"),
                new DataColumn("Cr/Dr"),
                new DataColumn("Balance"),
            });

            AccountLedgerSP spAccountLedger = new AccountLedgerSP();
            GeneralQueries queryForRetainedEarning = new GeneralQueries();
            DataTable dtbl = new DataTable();
            dtbl = spAccountLedger.AccountListViewAll(0, MATFinancials.PublicVariables._dtFromDate, MATFinancials.PublicVariables._dtToDate);
            // ----------------- calculate and replace retained earning in the datatable ---------------- //
            decimal retainedEarning = queryForRetainedEarning.retainedEarnings(MATFinancials.PublicVariables._dtToDate);
            //string retainedEarnings = retainedEarning > 0 ? retainedEarning.ToString("N2") : "(" + (retainedEarning * -1).ToString("N2") + ")";
            if (dtbl.AsEnumerable().Where(m => m.Field<string>("ledgerName").ToLower() == "retained earning").Any())
            {
                foreach (DataRow row in dtbl.Rows)
                {
                    if (row["ledgerName"].ToString().ToLower() == "retained earning")
                    {
                        row["Balance"] = retainedEarning;
                        row["CD"] = Convert.ToDecimal(retainedEarning) > 0 ? "Cr" : "Dr";
                    }
                }
            }

            for (var p = 0; p < dtbl.Rows.Count; p++)
            {
                try
                {
                    excelData.Rows.Add(
                    dtbl.Rows[p]["ledgerName"].ToString(),
                    dtbl.Rows[p]["nature"].ToString(),
                    dtbl.Rows[p]["accountNumber"].ToString(),
                    dtbl.Rows[p]["description"].ToString(),
                    dtbl.Rows[p]["mailingName"].ToString(),
                    dtbl.Rows[p]["Email"].ToString(),
                    dtbl.Rows[p]["CD"].ToString(),
                    Math.Round(Convert.ToDecimal(dtbl.Rows[p]["Balance"]), 2).ToString("C", new CultureInfo("HA-LATN-NG"))
                    );
                }
                catch (Exception e)
                {

                }
            }


            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(excelData);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Account List.xlsx");
                }
            }
        }
    }
}