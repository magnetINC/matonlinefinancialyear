﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Company.Controllers.Settings
{
    public class RolePriviledgeSettingsController : Controller
    {
        // GET: Company/RolePriviledgeSettings
        public ActionResult Index()
        {
            Authorize.IsAuthorize("frmRolePriviledgeSettings","View",Convert.ToDecimal(User.Identity.Name));
            return View();
        }
    }
}