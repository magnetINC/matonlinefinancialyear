﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Supplier.Controllers
{
    public class PurchaseOrderController : Controller
    {
        // GET: Supplier/PurchaseOrder
        public ActionResult Index()
        {
            return View();
        }
        
        public ActionResult Register()
        {
            return View();
        }
        public ActionResult PurchaseOrderListing()
        {
            return View();
        }
        public ActionResult PurchaseOrderEdit(int id)
        {
            ViewBag.purchaseOrderId = id;
            return View();
        }
    }
}