﻿
function showLoader() {
    $('#page-content').loader('show');
}

function hideLoader() {
    $('#page-content').loader('hide');
}

function countDown(container,startSeconds)
{
    var start = startSeconds+1;
    var x = setInterval(function () {
        --start;
        $("#"+container).html(start);
        if(start==0)
        {
            clearInterval(x);
            $("#successModal").modal("toggle");
        }
        return start;
    }, 1000);

    
}
var Utilities =
    {
        ErrorNotification: function (msg)
        {
            var autoClose = true;
            var content = '<div class="alert alert-danger media fade in">\
                                <div class="media-left">\
									<span class="icon-wrap icon-wrap-xs icon-circle alert-icon">\
										<i class="fa fa-times fa-lg"></i>\
									</span>\
								</div>\
								<div class="media-body">\
									<h4 class="alert-title">Oops!</h4>\
									<p class="alert-message">' + msg + '</p>\
								</div>\
            				</div>';
            //var content = '<div class="alert alert-danger" role="alert">\
            //                  <h4 class="alert-heading" style="padding:0; margin:0;">Well done!</h4>\
            //                  <span style="word-wrap:break-word;">Aww yeah, you successfully read this important alert messagejdfkdfgkshdgklhsdjgkhasldbg;asdjg;asdig asdgaisdgnasdkgn asdginasdg asdginasdghasidgh asdgiahshdhglkasld ga8sdgha;skdgha sdghasdgkjashduashetw89ehsg asdg88hawep8tphgsad iuasd fjgasdufglasjf ahb8asdgfjasdgofiua <span>\
            //               </div>';

            $.niftyNoty({
                type: "btn-danger",
                container: 'floating',
                html: content,
                timer: autoClose ? 10000 : 0
            });
        },
        SuccessNotification: function (msg) {
            var autoClose = true;
            var content = '<div class="alert alert-success media fade in">\
								<div class="media-left">\
									<span class="icon-wrap icon-wrap-xs icon-circle alert-icon">\
										<i class="fa fa-check fa-lg"></i>\
									</span>\
								</div>\
								<div class="media-body">\
                                    <h4 class="alert-title"></h4>\
									<p class="alert-message">'+ msg + '</p>\
								</div>\
							</div>';

            $.niftyNoty({
                type: "btn-success",
                container: 'floating',
                html: content,
                timer: autoClose ? 3000 : 0
            });
        },
        Loader:
            {
                Show:function()
                {
                    $('#page-content').loader('show');
                },
                Hide:function()
                {
                    $('#page-content').loader('hide');
                }
            },
        CountDown:function()
        {
            var start = startSeconds + 1;
            var x = setInterval(function () {
                --start;
                $("#" + container).html(start);
                if (start == 0) {
                    clearInterval(x);
                    $("#successModal").modal("toggle");
                }
                return start;
            }, 1000);
        },
        Alert:function(msg)
        {
            alert(msg);
        },
        Confirm: function (msg)
        {
            return confirm(msg);
        },
        IsValueEmptyOrNull:function(val)
        {
            if(val!="" && val!=null)
            {
                return true;
            }
            return false;
        },
        MonthString: function (num) {
            if (num == "01" || num == "1")
                return "Jan";
            if (num == "02" || num == "2")
                return "Feb";
            if (num == "03" || num == "3")
                return "Mar";
            if (num == "04" || num == "4")
                return "Apr";
            if (num == "05" || num == "5")
                return "May";
            if (num == "06" || num == "6")
                return "Jun";
            if (num == "07" || num == "7")
                return "Jul";
            if (num == "08" || num == "8")
                return "Aug";
            if (num == "09" || num == "9")
                return "Sep";
            if (num == "10")
                return "Oct";
            if (num == "11")
                return "Nov";
            if (num == "12")
                return "Dec";
        },
        FormatJsonDate: function (jsonDate) {
            var datePt = jsonDate.split("T");
            var dt = datePt[0].split("-");
            return dt[2] + "-" + this.MonthString(dt[1]) + "-" + dt[0];
        },
        PopulateDropDownFromArray: function (dataSource, valueIndex, displayIndex)
        {
            var objectOutput = [];
            var output = "";
            var htmlOutput = "";
            $.each(dataSource, function (count, record) {
                var array = $.map(record, function (value, index) {
                    return [value];
                });
                objectOutput.push(array);
            });
            //console.log(objectOutput);
            for (i = 0; i < objectOutput.length; i++) {
                output += '<option value="' + objectOutput[i][valueIndex] + '">' + objectOutput[i][displayIndex] + '</option>';
            }
            return output;
        },
        ShowStockCardDialog:function(productId)
        {
            var products = [];
            //if (productId < 1)
            //{
                
            //}
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/ProductCreation/GetProducts",
                type: "Get",
                contentType: "application/json",
                success: function (data) {
                    // $("#stockCardProducts").html('<option value="0">--Select Product--</option>' + Utilities.PopulateDropDownFromArray(data, 1, 3));
                    $("#stockCardProducts").kendoDropDownList({
                        width:"100",
                        filter: "contains",
                        dataTextField: "ProductName",
                        dataValueField: "ProductId",
                        optionLabel: "Select item...",
                        dataSource: data,
                        change: function (e) {
                            var selectedItem = this.dataItem(e.item);
                            Utilities.ShowStockCardDialog(selectedItem.ProductId);
                        }
                    });

                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.Loader.Hide();
                }
            });

            $("#itemNameSpan").html("NA");
            $("#itemCodeSpan").html("NA");
            $("#itemDescriptionSpan").html("NA");
            $("#purchaseOrderSpan").html("NA");

            $("#stockCardTbody").html('\
                                            <tr>\
                                                <td>NA</td>\
                                                <td>NA</td>\
                                                <td>NA</td>\
                                                <td>NA</td>\
                                            </tr>\
                                        ');

            $("#stockCardModal").modal("show");
            if (productId > 0)
            {
                Utilities.Loader.Show();
                $.ajax({
                url: API_BASE_URL + "/ProductCreation/ProductStockCard?productId="+productId,
                type: "Get",
                contentType: "application/json",
                success:function(data)
                {
                    $("#itemNameSpan").html(data.ProductName);
                    $("#itemCodeSpan").html(data.ProductCode);
                    $("#itemDescriptionSpan").html(data.ProductDescription);
                    $("#purchaseOrderSpan").html(data.PurchaseOrder);
                    var locations = data.StockCardLocations;
                    var locationsHtml = "";
                    var totalQtyOnHand = 0;
                    var totalSalesOrder = 0;
                    for (i = 0; i < locations.length; i++)
                    {
                        console.log("loc", locations[i]);
                        totalQtyOnHand = totalQtyOnHand + parseFloat(locations[i].QuantityOnHand);
                        totalSalesOrder = totalSalesOrder + parseFloat(locations[i].SalesOrderQuantity);
                        locationsHtml += '<tr>\
                                                <td>' + locations[i].StoreName + '</td>\
                                                <td>' + parseFloat(locations[i].SalesOrderQuantity) + '</td>\
                                                <td>' + parseFloat(locations[i].QuantityOnHand) + '</td>\
                                                <td>' + (parseFloat(locations[i].QuantityOnHand) - parseFloat(locations[i].SalesOrderQuantity)) + '</td>\
                                            </tr>\
                                        ';
                    }
                    locationsHtml += '\
                                            <tr>\
                                                <td><b>All Stores</b></td>\
                                                <td><b>' + totalSalesOrder + '</b></td>\
                                                <td><b>' + totalQtyOnHand + '</b></td>\
                                                <td><b>' + (totalQtyOnHand - totalSalesOrder) + '</b></td>\
                                            </tr>\
                                        ';
                    $("#stockCardTbody").html(locationsHtml);
                    console.log(data);
                    Utilities.Loader.Hide();
                },
                error:function(err)
                {
                    Utilities.Loader.Hide();
                }
            });
            }
            $('#stockCardModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
        },
        FormatCurrency:function(amount)
        {
            var val= amount.toLocaleString('en-US', {
                style: 'currency',
                currency: 'NGN',
            });
            return val.replace("NGN","");
        },
        FormatQuantity: function (qty) {
            var val = qty.toLocaleString('en-US');
            return val;
        },
        LogOut:function()
        {
            localStorage.removeItem("MAT_USER_INFO");   //clear user data in localstorage
            window.location = "/Security/Login/Logout";     //and redirect to logout controller to do backend logout
        },
        YyMmDdDate:function()
        {
            var year = new Date().getFullYear();
            var month = (parseInt(new Date().getMonth() + 1) < 10) ? ("0" + (parseInt(new Date().getMonth() + 1))) : (parseInt(new Date().getMonth() + 1));
            var day = new Date().getDate() < 10 ? ("0" + new Date().getDate()) : new Date().getDate();
            return year + "-" + month + "-" + day;
        },
        ApplyRolePriviledge:function()
        {
            //PriviledgeSettings.ApplyUserPriviledge();
        },
        SetDate:function(oldDate,interval)
        {
            var yourDate = new Date(oldDate);
            var newdate = "";
            if (interval > 0)
            {
               newdate= new Date(yourDate.setDate(yourDate.getDate() +interval));
            }
            else if (interval < 0)
            {
                newdate = new Date(yourDate.setDate(yourDate.getDate() - (interval*-1)));
            }
            return newdate.getFullYear() + "-" + (newdate.getMonth() + 1) + "-" + newdate.getDate();
        }
    };
