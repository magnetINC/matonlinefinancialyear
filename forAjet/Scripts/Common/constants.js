﻿$(function () {
});

var API_BASE_URL = "http://localhost:30170/api";
//var API_BASE_URL = "http://45.40.135.165:8082/api";

var k={
    "ProductInfo":
        {
            "ProductName":"test object3",
            "ProductCode":"140090",
            "PurchaseRate":"2000",
            "SalesRate":"4000",
            "Mrp":"NULL",
            "MaximumStock":"60",
            "MinimumStock":"20",
            "ReorderLevel":"10",
            "TaxId":"10011",
            "UnitId":"30006",
            "GroupId":"10047",
            "ProductType":"Product",
            "SalesAccount":"10",
            "EffectiveDate":"2018-01-21",
            "ExpenseAccount":"58",
            "TaxapplicableOn":"MRP",
            "BrandId":"12",
            "SizeId":"6",
            "ModelNoId":"null",
            "GodownId":"8",
            "RackId":"10009",
            "IsallowBatch":"False",
            "IsBom":"False",
            "PartNo":"0",
            "Isopeningstock":"True",
            "Ismultipleunit":"False",
            "IsActive":false,
            "Extra1":"",
            "Extra2":"manufac",
            "IsshowRemember":false,
            "Narration":"desc",
            "barcode":""
        },
    "NewStores":
        [
            {
                "StoreId":"8",
                "RackId":"10009",
                "Quantity":"20",
                "Rate":"2000",
                "UnitId":"30006",
                "Amount":40000
            },
            {
                "StoreId":"7",
                "RackId":"10008",
                "Quantity":"15",
                "Rate":"2000",
                "UnitId":"30006",
                "Amount":30000
            }
        ],        
    "AutoBarcode":false,
    "IsSaveBomCheck":false,
    "IsSaveMultipleUnitCheck":false,
    "IsOpeningStock":true,
    "IsBatch":false
    }