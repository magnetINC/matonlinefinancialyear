﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace ItemImporter
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void startImport_Click(object sender, EventArgs e)
        {
            startImport.Enabled = false;
            startImport.Text = "Processing...";

            Excel.Application xlApp;
            Excel.Workbook xlWorkBook;
            Excel.Worksheet xlWorkSheet;
            Excel.Range range;

            string str="";
            int rCnt;
            int cCnt;
            int rw = 0;
            int cl = 0;

            xlApp = new Excel.Application();
            xlWorkBook = null;// xlApp.Workbooks.Open(@"C:\Magnet.official\matonline_repo\ItemImporter\bin\Debug\items.xls", 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            range = xlWorkSheet.UsedRange;
            //rw = /*range.Rows.Count*/96;
            rw = range.Rows.Count/*5*/;
            cl = /*range.Columns.Count*/21;
            string rowString = "";
            List<Model> jsonOutput = new List<Model>();

            for (rCnt = 3; rCnt <= rw; rCnt++)
            {
                //for (cCnt = 1; cCnt <= cl; cCnt++)
                //{
                //    //str += Convert.ToString((range.Cells[rCnt, cCnt] as Excel.Range).Value2)+" --- ";


                //}

                string chk = Convert.ToString((range.Cells[rCnt, 1] as Excel.Range).Value2);
                if(chk==null || chk=="")
                {
                    break;
                }

                List<NewStores> openingStocks = new List<NewStores>();
                openingStocks.Add(new NewStores
                {
                    Amount = "0.0",
                    UnitId = Convert.ToString((range.Cells[rCnt, 12] as Excel.Range).Value2),
                    RackId = "1",
                    Quantity = Convert.ToString((range.Cells[rCnt, 14] as Excel.Range).Value2),
                    Rate = Convert.ToString((range.Cells[rCnt, 20] as Excel.Range).Value2),
                    StoreId = Convert.ToString((range.Cells[rCnt, 8] as Excel.Range).Value2)
                }); //shagamu stock
                openingStocks.Add(new NewStores
                {
                    Amount = "0.0",
                    UnitId = Convert.ToString((range.Cells[rCnt, 12] as Excel.Range).Value2),
                    RackId = "1",
                    Quantity = Convert.ToString((range.Cells[rCnt, 15] as Excel.Range).Value2),
                    Rate = Convert.ToString((range.Cells[rCnt, 20] as Excel.Range).Value2),
                    StoreId = Convert.ToString((range.Cells[rCnt, 9] as Excel.Range).Value2)
                });//asaba stock
                openingStocks.Add(new NewStores
                {
                    Amount = "0.0",
                    UnitId = Convert.ToString((range.Cells[rCnt, 12] as Excel.Range).Value2),
                    RackId = "1",
                    Quantity = Convert.ToString((range.Cells[rCnt, 16] as Excel.Range).Value2),
                    Rate = Convert.ToString((range.Cells[rCnt, 20] as Excel.Range).Value2),
                    StoreId = Convert.ToString((range.Cells[rCnt, 7] as Excel.Range).Value2)
                });//abuja stock

                jsonOutput.Add(new Model
                {
                    ProductInfo = new ProductInfo
                    {
                        ProductName = Convert.ToString((range.Cells[rCnt, 6] as Excel.Range).Value2),
                        ProductCode = Convert.ToString((range.Cells[rCnt, 7] as Excel.Range).Value2),
                        PartNo = Convert.ToString((range.Cells[rCnt, 8] as Excel.Range).Value2),
                        IsActive = true,
                        IsallowBatch = false,
                        Ismultipleunit = false,
                        IsBom = false,
                        Isopeningstock = true,
                        barcode = "",
                        BrandId = Convert.ToString((range.Cells[rCnt, 10] as Excel.Range).Value2),
                        EffectiveDate = "2018-01-25",
                        ExpenseAccount = "58",
                        //Extra2 ="",
                        GodownId = "1",
                        GroupId = Convert.ToString((range.Cells[rCnt, 5] as Excel.Range).Value2),
                        IsshowRemember = "false",
                        MaximumStock = Convert.ToString((range.Cells[rCnt, 17] as Excel.Range).Value2),
                        MinimumStock = Convert.ToString((range.Cells[rCnt, 18] as Excel.Range).Value2),
                        ModelNoId = "1",
                        Mrp = "0.00000",
                        Narration = Convert.ToString((range.Cells[rCnt, 9] as Excel.Range).Value2),
                        ProductType = "Product",
                        PurchaseRate = Convert.ToString((range.Cells[rCnt, 21] as Excel.Range).Value2),
                        RackId = "1",
                        ReorderLevel = Convert.ToString((range.Cells[rCnt, 19] as Excel.Range).Value2),
                        SalesAccount = "10",
                        SalesRate = Convert.ToString((range.Cells[rCnt, 20] as Excel.Range).Value2),
                        SizeId = Convert.ToString((range.Cells[rCnt, 11] as Excel.Range).Value2),
                        TaxapplicableOn = "MRP",
                        TaxId = "10011",
                        UnitId = Convert.ToString((range.Cells[rCnt, 12] as Excel.Range).Value2),
                    },
                    AutoBarcode = true,
                    IsSaveBomCheck = false,
                    IsSaveMultipleUnitCheck = false,
                    IsBatch = false,
                    IsOpeningStock = true,
                    NewStores = openingStocks
                });

                //MessageBox.Show(str);
            }

            xlWorkBook.Close(true, null, null);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);
            
            // Pass the "list" object for conversion object to JSON string  
            string jsondata = new JavaScriptSerializer().Serialize(jsonOutput);
            //string path = Server.MapPath("~/App_Data/");
            // Write that JSON to txt file,  
            System.IO.File.WriteAllText("output.json", jsondata);

            startImport.Enabled = true;
            startImport.Text = "Start Import";
        }
    }
}
