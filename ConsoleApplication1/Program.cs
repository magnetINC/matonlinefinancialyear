﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApplication1
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] input = new int[] { 1, 1, 1, 2, 2, 3, 3, 3, 3 };            
            int count = 0;
            int strt = input[0];
            for (int i = 0; i < input.Length; i++)
            {
                int tracker = 1;
                for (int j = i + 1; j < input.Length; j++)
                {
                    if (input[i] != input[j])
                    {
                        break;
                    }
                    tracker++;
                }

                if (tracker > count)
                {
                    count = tracker;
                    strt = input[i];
                }
            }
            Console.WriteLine(count);
            Console.ReadLine();
        }        
    }
}
