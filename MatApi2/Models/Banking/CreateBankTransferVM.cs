﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Banking
{
    public class CreateBankTransferVM
    {
        public decimal ledgerId { get; set; }
        public string voucherNo { get; set; }
        public DateTime date { get; set; }
        public string transactionType { get; set; }
        public decimal totalAmount { get; set; }
        public string narration { get; set; }
        public List<CreateLineBankTransfers> lineTransfers { get; set; }
    }

    public class CreateLineBankTransfers
    {
        public decimal ledgerId { get; set; }
        public string memo { get; set; }
        public decimal amount { get; set; }
        public decimal currency { get; set; }
        public string chequeNo { get; set; }
        public DateTime chequeDate { get; set; }
    }
}