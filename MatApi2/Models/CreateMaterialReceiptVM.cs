﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class CreateMaterialReceiptVM
    {
        public string ReceiptNo { get; set; }
        public DateTime Date { get; set; }
        public decimal SupplierId { get; set; }
        public string DeliveryMode { get; set; }
        public string Narration { get; set; }
        public decimal TotalAmount { get; set; }
        public string TransportaionCompany { get; set; }
        public string LrNo { get; set; }
        public string OrderNo { get; set; }
        public decimal PricingLevelId { get; set; }
        public decimal SalesManId { get; set; }
        public decimal CurrencyId { get; set; }
        public decimal OrderMasterId { get; set; }
        public decimal OrderDetailsId { get; set; }
        public List<MaterialReceiptLineItems> LineItems { get; set; }
    }

    public class MaterialReceiptLineItems
    {
        public int SL { get; set; }
        public decimal ProductId { get; set; }
        public decimal ProductBarcode { get; set; }
        public decimal ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitId { get; set; }
        public decimal UnitConversionId { get; set; }
        public decimal StoreId { get; set; }
        public decimal RackId { get; set; }
        public decimal BatchId { get; set; }
        public decimal Rate { get; set; }
        public decimal OrderDetailsId { get; set; }
        public string VoucherNo { get;set; }
        public string InvoiceNo { get; set; }
        public decimal VoucherTypeId { get; set; }

    }

    public class RejectionOutLineItems2
    {
        public decimal SL { get; set; }
        public string Barcode { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal Quantity { get; set; }
        public string Unit { get; set; }
        public string Store { get; set; }
        public string Rack { get; set; }
        public string Batch { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
    }
}