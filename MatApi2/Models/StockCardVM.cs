﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class StockCardVM
    {
        public decimal ProductId { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public decimal PurchaseOrder { get; set; }
        public string ProductDescription { get; set; }
        public List<StockCardLocations> StockCardLocations { get; set; }

    }

    public class StockCardLocations
    {
        public decimal StoreId { get; set; }
        public string StoreName { get; set; }
        public decimal SalesOrderQuantity { get; set; }
        public decimal QuantityOnHand { get; set; }
    }
}