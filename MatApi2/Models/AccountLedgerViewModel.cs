﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class AccountLedgerViewModel
    {
        public AccountLedgerInfo AccountLedgerInfo { get; set; }
        public bool IsBankAccount { get; set; }
        public bool IsCashAccount { get; set; }
        public bool IsSundryDebtorOrCreditor { get; set; }
    }
}