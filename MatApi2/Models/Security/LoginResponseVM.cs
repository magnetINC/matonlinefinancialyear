﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Security
{
    public class LoginResponseVM
    {
        public decimal UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal StoreId { get; set; }
        public string StoreName { get; set; }
        public bool IsActive { get; set; }
        public decimal RoleId { get; set; }
        public string RoleName { get; set; }
        public DateTime DateCreated { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
    }
}