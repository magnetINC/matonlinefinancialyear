﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Inventory
{
    public class SockJournalModel  //model of sale quoation
    {
        public string FormNo { get; set; }
        public string TransDate { get; set; }
        public int Currency { get; set; }
        public int FinishedGood { get; set; }
        public int Qty { get; set; }
        public int CashNBank { get; set; }
        public string Narration { get; set; }
        public decimal SourceAmountTotal { get; set; }
        public decimal ConsumptionAmountTotal { get; set; }
        public decimal AdditionalAmountTotal { get; set; }
        public List<LineItemModel> SrcLineItems { get; set; }
        public List<ConsumptionItemModel> ConsumptionItems { get; set; }
        public List<AdditionalCostItemsModel> AdditionalCostItems { get; set; }
    }

    public class LineItemModel
    {
        public int SiNo { get; set; }
        public string Barcode { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
        public int Unit { get; set; }
        public int Store { get; set; }
        public string UnitName { get; set; }
        public string StoreName { get; set; }
        public string RackName { get; set; }
        public int Rack { get; set; }
        public string BatchName { get; set; }
        public int Batch { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }       
    }

    public class ConsumptionItemModel
    {
        public int SiNo { get; set; }
        public string Barcode { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public int Qty { get; set; }
        public int Unit { get; set; }
        public int Store { get; set; }
        public string UnitName { get; set; }
        public string StoreName { get; set; }
        public string RackName { get; set; }
        public int Rack { get; set; }
        public string BatchName { get; set; }
        public int Batch { get; set; }
        public decimal Rate { get; set; }
        public decimal Amount { get; set; }
    }

    public class AdditionalCostItemsModel
    {
        public int SiNo { get; set; }
        public int AccountLedger { get; set; }
        public string AccountLedgerName { get; set; }   
        public decimal Amount { get; set; }
    }

    public class StockJournalVM
    {
        public StockJournalMasterInfo StockJournalMasterInfo { get; set; }
        public List<StockJournalDetailsInfo> StockJournalDetailsInfoConsumption { get; set; }
        public List<StockJournalDetailsInfo> StockJournalDetailsInfoProduction { get; set; }
        public List<LedgerPostingInfo> LedgerPostingInfo { get; set; }//not used
        public AdditionalCostInfo AdditionalCostInfo { get; set; }
        public decimal TotalAdditionalCost { get; set; }
        public DateTime Date { get; set; }
        public decimal Currency { get; set; }
        public decimal AdditionalCostCashOrBankId { get; set; }
        public List<AdditionalCostItems> AdditionalCostItems { get; set; }
    }

    public class AdditionalCostItems
    {
        public decimal LedgerId { get; set; }
        public decimal Amount { get; set; }
    }
}