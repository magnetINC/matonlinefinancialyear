﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Company.Settings
{
    public class SettingsVM
    {
        public int SettingsId { get; set; }
        public string SettingsName { get; set; }
        public string Status { get; set; }
        public DateTime ExtraDate { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
    }

    public class CycleActionPrivilegeVM
    {
        public string CycleAction { get; set; }
        public decimal RoleId { get; set; }
    }
}