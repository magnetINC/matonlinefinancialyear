﻿using MatDal;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Company.Masters.Account
{
    public class AccountLedger
    {
        public tbl_AccountLedger Ledger { get; set; }
        public tbl_Route Route { get; set; }
        public tbl_Area Area { get; set; }
        public tbl_AccountGroup AccountGroup { get; set; }
        public tbl_PricingLevel PricingLevel { get; set; }
        public decimal? Balance { get; set; }
    }
}