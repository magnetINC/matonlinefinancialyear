﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public static class PublicVariables
    {
        public static DateTime FromDate;   //start of financial year
        public static DateTime ToDate;     //end of financial year

        static PublicVariables()
        {
            FromDate = new DateTime(2017,01,01);
            ToDate = new DateTime(2017,12,31);
        }
    }
}