﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory.ItemComponent
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StoreController : ApiController
    {
        GodownSP storeSp;
        public StoreController()
        {
            storeSp = new GodownSP();
        }

        public List<GodownInfo> GetStores()
        {
            var storesDt = storeSp.GodownViewAll();
            List<GodownInfo> stores = new List<GodownInfo>();
            foreach (DataRow row in storesDt.Rows)
            {
                stores.Add(new GodownInfo
                {
                    GodownId = Convert.ToDecimal(row["GodownId"]),
                    GodownName = row["GodownName"].ToString()
                });
            }
            return stores;
        }

        public GodownInfo GetStore(decimal storeId)
        {
            var storeDt = storeSp.GodownView(storeId);
            GodownInfo store = new GodownInfo
            {
                GodownId = Convert.ToDecimal(storeDt.GodownId),
                GodownName = storeDt.GodownName

            };
            return store;
        }

        [HttpPost]
        public bool DeleteStore(GodownInfo store)
        {
            if (storeSp.GodownCheckReferenceAndDelete(store.GodownId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddStore(GodownInfo store)
        {
            store.Extra1 = "";
            store.Extra2 = "";
            store.Narration = "";
            store.ExtraDate = DateTime.Now;
            if (storeSp.GodownAddWithoutSameName(store) > 0)
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditStore(GodownInfo store)
        {
            store.Extra1 = "";
            store.Extra2 = "";
            store.Narration = "";
            store.ExtraDate = DateTime.Now;
            return storeSp.GodownEdit(store);
        }
    }
}
