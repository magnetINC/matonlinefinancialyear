﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory.ItemComponent
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RackController : ApiController
    {
        RackSP rackSp;
        public RackController()
        {
            rackSp = new RackSP();
        }

        public List<RackInfo> GetRacks()
        {
            var racksDt = rackSp.RackViewAll();
            List<RackInfo> racks = new List<RackInfo>();
            foreach (DataRow row in racksDt.Rows)
            {
                racks.Add(new RackInfo
                {
                    RackId = Convert.ToDecimal(row["RackId"]),
                    RackName = row["RackName"].ToString(),
                    GodownId = Convert.ToDecimal(row["GodownId"]),
                });
            }
            return racks;
        }

        public RackInfo GetRack(decimal rackId)
        {
            var rackDt = rackSp.RackView(rackId);
            RackInfo store = new RackInfo
            {
                RackId = Convert.ToDecimal(rackDt.RackId),
                RackName = rackDt.RackName,
                GodownId= Convert.ToDecimal(rackDt.GodownId)

            };
            return store;
        }

        [HttpPost]
        public bool DeleteRack(RackInfo rack)
        {
            if (rackSp.RackDeleteReference(rack.RackId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddRack(RackInfo rack)
        {
            rack.Extra1 = "";
            rack.Extra2 = "";
            rack.Narration = "";
            rack.ExtraDate = DateTime.Now;
            if (rackSp.RackAdd(rack) > 0)
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditRack(RackInfo rack)
        {
            rack.Extra1 = "";
            rack.Extra2 = "";
            rack.Narration = "";
            rack.ExtraDate = DateTime.Now;
            return rackSp.RackEdit(rack);
        }
    }
}
