﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory.PriceList
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PricingLevelController : ApiController
    {
        PricingLevelSP pricingLevelSp;
        public PricingLevelController()
        {
            pricingLevelSp = new PricingLevelSP();
        }

        public List<PricingLevelInfo> GetPricingLevels()
        {
            var pricingLevelsDt = pricingLevelSp.PricingLevelViewAll();
            List<PricingLevelInfo> pricingLevels = new List<PricingLevelInfo>();
            foreach (DataRow row in pricingLevelsDt.Rows)
            {
                pricingLevels.Add(new PricingLevelInfo
                {
                    PricinglevelId = Convert.ToDecimal(row["PricinglevelId"]),
                    PricinglevelName = row["PricinglevelName"].ToString()
                });
            }
            return pricingLevels;
        }

        public PricingLevelInfo GetPricingLevel(decimal pricingLevelId)
        {
            var pricingLevelDt = pricingLevelSp.PricingLevelView(pricingLevelId);
            PricingLevelInfo brand = new PricingLevelInfo
            {
                PricinglevelId = Convert.ToDecimal(pricingLevelDt.PricinglevelId),
                PricinglevelName = pricingLevelDt.PricinglevelName

            };
            return pricingLevelDt;
        }

        [HttpPost]
        public bool DeletePricingLevel(PricingLevelInfo pricingLevel)
        {
            if (pricingLevelSp.PricingLevelCheckReferenceAndDelete(pricingLevel.PricinglevelId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddPricingLevel(PricingLevelInfo pricingLevel)
        {
            pricingLevel.ExtraDate = DateTime.Now;
            pricingLevel.Extra1 = "";
            pricingLevel.Extra2 = "";
            pricingLevel.Narration = "";
            if (pricingLevelSp.PricingLevelAddWithoutSamePricingLevel(pricingLevel) > 0)
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditPricingLevel(PricingLevelInfo pricingLevel)
        {
            pricingLevel.ExtraDate = DateTime.Now;
            pricingLevel.Extra1 = "";
            pricingLevel.Extra2 = "";
            pricingLevel.Narration = "";
            return pricingLevelSp.PricingLevelEditParticularFields(pricingLevel);
        }
    }
}
