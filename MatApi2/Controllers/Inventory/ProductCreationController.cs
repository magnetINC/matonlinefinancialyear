﻿using MatApi.Controllers.Reports.OtherReports;
using MatApi.Models;
using MatApi.Models.Inventory;
using MatApi.Models.Reports.OtherReports;
using MATFinancials;
using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ProductCreationController : ApiController
    {
        ProductSP productSp;
        SalesMasterSP salesMasterSp;
        AccountLedgerSP accountLedgerSp;
        public ProductCreationController()
        {
            productSp = new ProductSP();
            salesMasterSp = new SalesMasterSP();
            accountLedgerSp = new AccountLedgerSP();
        }

        public List<ProductInfo> GetProducts()
        {
            var productsDt = productSp.ProductViewAll();
            List<ProductInfo> products = new List<ProductInfo>();
            foreach (DataRow row in productsDt.Rows)
            {
                products.Add(new ProductInfo
                {
                    ProductId = Convert.ToDecimal(row["ProductId"]),
                    ProductName = row["ProductName"].ToString(),
                    ProductCode= row["ProductCode"].ToString(),
                    barcode = row["ProductCode"].ToString(),
                    BrandId= Convert.ToDecimal(row["BrandId"]),
                    //CategoryId= Convert.ToDecimal(row["CategoryId"]),
                    EffectiveDate=Convert.ToDateTime(row["EffectiveDate"]),
                    ExpenseAccount= Convert.ToDecimal(row["ExpenseAccount"]),
                    GodownId= Convert.ToDecimal(row["GodownId"]),
                    GroupId= Convert.ToDecimal(row["GroupId"]),
                    IsActive=Convert.ToBoolean(row["IsActive"]),
                    IsallowBatch= Convert.ToBoolean(row["IsallowBatch"]),
                    IsBom = Convert.ToBoolean(row["IsBom"]),
                    Ismultipleunit= Convert.ToBoolean(row["Ismultipleunit"]),
                    Isopeningstock= Convert.ToBoolean(row["Isopeningstock"]),
                    IsshowRemember= Convert.ToBoolean(row["Isopeningstock"]),
                    MaximumStock= Convert.ToDecimal(row["MaximumStock"]),
                    MinimumStock= Convert.ToDecimal(row["MinimumStock"]),
                    ModelNoId= Convert.ToDecimal(row["ModelNoId"]),
                    Mrp= Convert.ToDecimal(row["Mrp"]),
                    PartNo= row["PartNo"].ToString(),
                    //ProductType= row["ProductType"].ToString(),
                    //ProjectId= Convert.ToDecimal(row["ProjectId"]),
                    PurchaseRate= Convert.ToDecimal(row["PurchaseRate"]),
                    RackId= Convert.ToDecimal(row["RackId"]),
                    ReorderLevel= Convert.ToDecimal(row["ReorderLevel"]),
                    SalesAccount= Convert.ToDecimal(row["SalesAccount"]),
                    SalesRate= Convert.ToDecimal(row["SalesRate"]),
                    SizeId= Convert.ToDecimal(row["SizeId"]),
                    TaxapplicableOn= row["TaxapplicableOn"].ToString(),
                    TaxId= Convert.ToDecimal(row["TaxId"]),
                    UnitId= Convert.ToDecimal(row["UnitId"])                   
                });
            }
            return products;
        }

        [HttpGet]
        public HttpResponseMessage GetProduct(decimal productId)
        {
            var prod = productSp.ProductView(productId);
            dynamic response = new ExpandoObject();
            response.ProductType = prod.ProductType;
            response.ProductName = prod.ProductName;
            response.ProductCode = prod.ProductCode;
            response.Barcode = prod.barcode;
            response.PurchaseRate = prod.PurchaseRate;
            response.SalesRate = prod.SalesRate;
            response.Unit = new UnitSP().UnitView(prod.UnitId).UnitName;
            response.ExpenseAccount = new AccountLedgerSP().AccountLedgerView(prod.ExpenseAccount).LedgerName;
            response.SalesAccount= new AccountLedgerSP().AccountLedgerView(prod.SalesAccount).LedgerName;
            response.Group = new ProductGroupSP().ProductGroupView(prod.GroupId).GroupName;
            response.MinStock = prod.MinimumStock;
            response.MaxStock = prod.MaximumStock;
            response.ReorderLevel = prod.ReorderLevel;
            response.Store = new GodownSP().GodownView(prod.GodownId).GodownName;
            response.Rack = new RackSP().RackView(prod.RackId).RackName;
            response.Tax = new TaxSP().TaxView(prod.TaxId).TaxName;
            response.ApplicableOn = prod.TaxapplicableOn;
            response.EffectiveDate = prod.EffectiveDate.ToShortDateString();
            response.ModelNo = new ModelNoSP().ModelNoView(prod.ModelNoId).ModelNo;
            response.Brand = new BrandSP().BrandView(prod.BrandId).BrandName;
            response.Size = new SizeSP().SizeView(prod.SizeId).Size;
            response.MRP = prod.Mrp;
            response.Bom = prod.IsBom == true ? "Yes" : "No";
            response.MultipleUnit = prod.Ismultipleunit == true ? "Yes" : "No";
            response.IsBatch = prod.IsallowBatch == true ? "Yes" : "No";
            response.Narration = prod.Narration;

            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }

        [HttpGet]
        public HttpResponseMessage SearchProduct(string searchBy, string filter, decimal storeId=0)
        {
            SalesDetailsSP spSalesDetails = new SalesDetailsSP();
            UnitSP spUnit = new UnitSP();
            GodownSP spGodown = new GodownSP();
            BatchSP spBatch = new BatchSP();
            TaxSP spTax = new TaxSP();
            RackSP spRack = new RackSP();
            UnitConvertionSP SpUnitConvertion = new UnitConvertionSP();

            dynamic response = new ExpandoObject();
            var size = new SizeSP().SizeViewAll();
            if (searchBy == "ProductCode")
            {
                //var productDetails = spSalesDetails.SalesInvoiceDetailsViewByProductCodeForSI(28, filter);
                var productDetails = findproduct(filter, "PRODUCTCODE");
                var productId = Convert.ToDecimal(productDetails.Rows[0]["ProductId"]);
                var godownId = Convert.ToDecimal(productDetails.Rows[0]["godownId"]);
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(godownId);
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
                string productImagePath = new ProductSP().productViewByProductId(productId).Extra1;

                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                response.Size = size;
                response.ProductImagePath = productImagePath;
                response.QuantityInStock = storeId>0? getQuantityInStockForOneStore(productId, storeId):getQuantityInStockForAllStores(productId);
            }
            else if (searchBy == "ProductName")
            {
                //var productDetails = spSalesDetails.SalesInvoiceDetailsViewByProductNameForSI(28, filter);
                var productDetails = findproduct(filter, "PRODUCTNAME");
                var productId = Convert.ToDecimal(productDetails.Rows[0]["ProductId"]);
                var godownId = Convert.ToDecimal(productDetails.Rows[0]["godownId"]);
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(godownId);
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
                string productImagePath = new ProductSP().productViewByProductId(productId).Extra1;

                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                response.Size = size;
                response.ProductImagePath = productImagePath;
                response.QuantityInStock = storeId > 0 ? getQuantityInStockForOneStore(productId, storeId) : getQuantityInStockForAllStores(productId);
            }
            else if (searchBy == "Barcode")
            {
                //var productDetails = spSalesDetails.SalesInvoiceDetailsViewByBarcodeForSI(28, filter);
                var productDetails = findproduct(filter, "BARCODE");
                var productId = Convert.ToDecimal(productDetails.Rows[0]["ProductId"]);
                var godownId = Convert.ToDecimal(productDetails.Rows[0]["godownId"]);
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(godownId);
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
                string productImagePath = new ProductSP().productViewByProductId(productId).Extra1;

                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                response.Size = size;
                response.ProductImagePath = productImagePath;
                response.QuantityInStock = storeId > 0 ? getQuantityInStockForOneStore(productId, storeId) : getQuantityInStockForAllStores(productId);
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        private DataTable findproduct(string searchString, string filter)
        {
            DBMatConnection conn = new DBMatConnection();
            string query = "";
            if (filter == "PRODUCTNAME")
            {
                query = "SELECT * FROM tbl_product LEFT JOIN tbl_batch ON tbl_product.productid=tbl_batch.productid LEFT JOIN tbl_brand ON tbl_product.brandid=tbl_brand.brandid WHERE tbl_product.productName='"+searchString+"'";
            }
            else if (filter == "PRODUCTCODE")
            {
                query = "SELECT * FROM tbl_product left join tbl_batch ON tbl_product.productid=tbl_batch.productid LEFT join tbl_brand ON tbl_product.brandid=tbl_brand.brandid WHERE tbl_product.productCode='" + searchString + "'";
            }
            else if (filter == "BARCODE")
            {
                query = "SELECT * FROM tbl_product left join tbl_batch ON tbl_product.productid=tbl_batch.productid LEFT join tbl_brand ON tbl_product.brandid=tbl_brand.brandid WHERE tbl_product.barcode='" + searchString + "'";
            }
            return conn.customSelect(query);            
        }

        public decimal GetQuantityInStockForOneStore(decimal productId,decimal storeId)
        {
            return getQuantityInStockForOneStore(productId, storeId);
        }

        [HttpPost]
        public bool DeleteProduct(ProductInfo product)
        {
            if (productSp.DeleteProductWithReferenceCheck(product.ProductId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public decimal AddProduct(ItemModel input)
        {

            AddItem item = new AddItem();
            return item.SaveItem(input);
            //product.Extra1 = "";
            //product.Extra2 = "";
            //product.Narration = "";
            //product.ExtraDate = DateTime.Now;
            //if (productSp.ProductAdd(product) > 0)
            //{
            //    return true;
            //}
        }
        [HttpPost]
        public bool EditProductGroup(ProductInfo product)
        {
            product.Extra1 = "";
            product.Extra2 = "";
            product.Narration = "";
            product.ExtraDate = DateTime.Now;
            return productSp.ProductEdit(product);
        }

        public string GetNextProductCode()
        {
            return productSp.ProductMax();
        }

        public List<SalesMasterInfo> GetSalesAccounts()
        {
            var accountDt = salesMasterSp.SalesInvoiceSalesAccountModeComboFill();
            List<SalesMasterInfo> accounts = new List<SalesMasterInfo>();
            foreach (DataRow row in accountDt.Rows)
            {
                accounts.Add(new SalesMasterInfo
                {
                    LedgerId = Convert.ToDecimal(row["LedgerId"]),
                    InvoiceNo = row["ledgerName"].ToString(),

                });
            }
            return accounts;
        }

        public List<AccountLedgerInfo> GetExpenseAccount()
        {
            var accountDt = accountLedgerSp.AccountLedgerViewAll();
            List<AccountLedgerInfo> accounts = new List<AccountLedgerInfo>();
            decimal[] ledgers = new decimal[] { 11, 37 };
            var query = (from d in accountDt.AsEnumerable()
                         where ledgers.Contains(d.Field<decimal>("accountGroupId"))
                         select d).ToList();
            foreach (DataRow row in query)
            {
                accounts.Add(new AccountLedgerInfo {
                    LedgerId = Convert.ToDecimal(row["LedgerId"]),
                    LedgerName = row["LedgerName"].ToString(),
                });
            }
            return accounts;
        }

        //returns quantity in stock of a product regardless of selected store/location, i.e sum total in all stores/locations
        private decimal getQuantityInStockForAllStores(decimal productId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format(" SELECT  convert(decimal(18, 2), (ISNULL(SUM(s.inwardQty), 0) - ISNULL(SUM(s.outwardQty), 0))) AS Currentstock " +
                " FROM tbl_StockPosting s inner join tbl_product p on p.productId = s.productId " +
                " INNER JOIN tbl_Batch b on s.batchId = b.batchId " +
                " WHERE p.productId = '{0}' AND s.date <= '{1}' ", productId, MATFinancials.PublicVariables._dtToDate);
            return Convert.ToDecimal(conn.getSingleValue(queryStr));
        }

        //returns quantity in stock of a product based on selected store/location
        private decimal getQuantityInStockForOneStore(decimal productId,decimal storeId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format(" SELECT  convert(decimal(18, 2), (ISNULL(SUM(s.inwardQty), 0) - ISNULL(SUM(s.outwardQty), 0))) AS Currentstock " +
                " FROM tbl_StockPosting s inner join tbl_product p on p.productId = s.productId " +
                " INNER JOIN tbl_Batch b on s.batchId = b.batchId " +
                " WHERE p.productId = '{0}' AND s.godownId= '{1}' AND s.date <= '{2}' ", productId , storeId , MATFinancials.PublicVariables._dtToDate);
            return Convert.ToDecimal(conn.getSingleValue(queryStr));
        }

        private decimal getSalesOrderForStoreProduct(decimal productId, decimal storeId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("SELECT SUM(qty) FROM tbl_SalesOrderDetails WHERE productId={0} and extra1='{1}' ", productId, storeId);
            string val1 = conn.getSingleValue(queryStr);
            if(val1=="" || val1==string.Empty || val1==null)
            {
                val1 = "0.0";
            }

            //select * from tbl_deliverynotedetails where productid=1 and godownId=2 and orderdetails1id in(select salesOrderDetailsId from tbl_salesorderdetails where productid=1 and extra1='2')
            string queryStr2 = string.Format("select sum(qty) from tbl_deliverynotedetails where productid={0} and godownId={1} and orderdetails1id in(select salesOrderDetailsId from tbl_salesorderdetails where productid={0} and extra1='{1}')", productId,storeId);
            string val2 = conn.getSingleValue(queryStr2);
            if (val2 == "" || val2 == string.Empty || val2 == null)
            {
                val2 = "0.0";
            }
            decimal salesOrderBalance = Convert.ToDecimal(val1)-Convert.ToDecimal(val2);
            return Convert.ToDecimal(salesOrderBalance);
        }

        private DataSet getSalesOrderQuantityByProduct(decimal productId)
        {
            DBMatConnection conn = new DBMatConnection();
            //string queryStr = string.Format("SELECT tbl_SalesOrderDetails.productId,tbl_SalesOrderDetails.salesOrderMasterId,tbl_SalesOrderDetails.qty,"+
            //                                "tbl_SalesOrderDetails.extra1, tbl_Godown.godownId, tbl_Godown.godownName " +
            //                                "FROM tbl_SalesOrderDetails,tbl_Godown " +
            //                                "WHERE productId = {0} AND tbl_SalesOrderDetails.extra1=tbl_Godown.godownId", productId);
            string queryStr = string.Format("SELECT tbl_Godown.godownId, sum(tbl_SalesOrderDetails.qty) as qty "+
                                            "FROM tbl_SalesOrderDetails, tbl_Godown "+
                                            "WHERE tbl_SalesOrderDetails.productId = {0} AND tbl_SalesOrderDetails.extra1 = CAST(tbl_Godown.godownId as varchar) " +
                                            "GROUP BY tbl_Godown.godownId", productId);
            return conn.customQuery(queryStr);
        }

        [HttpGet]
        public StockCardVM ProductStockCard(decimal productId)
        {
            ProductInfo product= new ProductSP().ProductView(productId);
            StockCardVM card = new StockCardVM();
            List<StockCardLocations> cardLocations = new List<StockCardLocations>();

            //SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();
            /*DataTable dt = new DataTable(),*/
            DataTable dt2 = new DataTable();
            DataTable dtbl = new DataTable(), dtbl2 = new DataTable();
           // dt = spSalesOrderMaster.GetSalesOrderNoIncludePendingCorrespondingtoLedgerforSI(0, 0, 0);
            dt2 = spPurchaseMaster.GetOrderNoCorrespondingtoLedgerByNotInCurrPI(0, 0, 0);
            dtbl = SalaryHelper.salesOrderDetails();
            dtbl2 = SalaryHelper.purchaseOrderDetails();
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        DataRow row = dtbl.NewRow();
            //        row["salesOrderMasterId"] = Convert.ToDecimal(dt.Rows[i]["salesOrderMasterId"]);
            //        dtbl.Rows.Add(row);
            //    }
            //}

            if (dt2 != null && dt2.Rows.Count > 0)
            {
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    DataRow row = dtbl2.NewRow();
                    row["purchaseOrderMasterId"] = Convert.ToDecimal(dt2.Rows[i]["purchaseOrderMasterId"]);
                    dtbl2.Rows.Add(row);
                }
            }

            DBMatConnection conn = new DBMatConnection();
            conn.AddParameter("@productId", productId);
            conn.AddParameter("@date", DateTime.Now);
            conn.AddParameter("@salesArrayList", dtbl);
            conn.AddParameter("@purchaseArrayList", dtbl2);
            var quickView= conn.getDataSet("CurrentStockQuickView");
            
            foreach(DataRow row in quickView.Tables[0].Rows)
            {
                StockSummarySearch param = new StockSummarySearch
                {
                    ToDate= MATFinancials.PublicVariables._dtToDate,
                    ProductId=productId,
                    StoreId= Convert.ToDecimal(row[4].ToString()),
                    BatchNo="All",
                    ProductCode="",
                    RefNo=""  
                };
                StockSummary summary = stockSummary(param);
                cardLocations.Add(new StockCardLocations
                {
                    StoreId = Convert.ToDecimal(row[4].ToString()),
                    StoreName =row[2].ToString(),
                    QuantityOnHand= Convert.ToDecimal(summary.QtyBalance),
                    SalesOrderQuantity = getSalesOrderForStoreProduct(productId, Convert.ToDecimal(row[4].ToString())),
                    //SalesOrderQuantity= getSalesOrderBalanceForStore(productId, Convert.ToDecimal(row[4].ToString()))
                });
            }

            card.ProductCode = product.ProductCode;
            card.ProductId = product.ProductId;
            card.ProductName = product.ProductName;
            card.ProductDescription = product.Narration;
            card.PurchaseOrder = getPurchaseOrderBalanceForProduct(productId);
            card.StockCardLocations = cardLocations;

            return card;
        }
        

        private decimal getSalesOrderBalanceForStore(decimal productId,decimal storeId)
        {
            SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
            SalesOrderDetailsSP spSalesOrderDetails = new SalesOrderDetailsSP();
            DBMatConnection conn = new DBMatConnection();
            DataSet ds = new DataSet();
            DataTable dt = new DataTable(), dt2 = new DataTable();
            DataTable dtbl = new DataTable(), dtbl2 = new DataTable();

            decimal salesOrderBalance = 0;

            dt = spSalesOrderMaster.GetSalesOrderNoIncludePendingCorrespondingtoLedgerforSI(0, 0, 0);
            dtbl = SalaryHelper.salesOrderDetails();
            if (dt != null && dt.Rows.Count > 0)
            {
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    DataRow row = dtbl.NewRow();
                    row["salesOrderMasterId"] = Convert.ToDecimal(dt.Rows[i]["salesOrderMasterId"]);
                    dtbl.Rows.Add(row);
                }
                // get balances remaining for all the available sales order for the item
                foreach (DataRow row in dt.Rows)
                {
                    DataTable dtblDetails = new DataTable();
                    dtblDetails = spSalesOrderDetails.SalesInvoiceGridfillAgainestSalesOrderUsingSalesDetails
                       (Convert.ToDecimal(row["salesOrderMasterId"]), 0, 0);
                    //salesOrderBalance += Convert.ToDecimal(dtblDetails.AsEnumerable().Where(i => i.Field<decimal>("productId") == productId && i.Field<decimal>("godownId")==storeId)
                    //    .Select(i => i.Field<decimal>("qty")).Sum());
                    salesOrderBalance += Convert.ToDecimal(dtblDetails.AsEnumerable().Where(i => i.Field<decimal>("productId") == MATFinancials.PublicVariables.productId)
                            .Select(i => i.Field<decimal>("qty")).Sum());
                }
            }
            return salesOrderBalance;
        }

        private decimal getPurchaseOrderBalanceForProduct(decimal productId)
        {
            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();
            PurchaseOrderDetailsSP spPurchaseOrderDetails = new PurchaseOrderDetailsSP();
            DBMatConnection conn = new DBMatConnection();
            DataSet ds = new DataSet();
            DataTable dt2 = new DataTable();
            DataTable dtbl = new DataTable(), dtbl2 = new DataTable();

            decimal purchaseOrderBalance = 0;
            
            dt2 = spPurchaseMaster.GetOrderNoCorrespondingtoLedgerByNotInCurrPI(0, 0, 0);
            dtbl2 = SalaryHelper.purchaseOrderDetails();

            if (dt2 != null && dt2.Rows.Count > 0)
            {
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    DataRow row = dtbl2.NewRow();
                    row["purchaseOrderMasterId"] = Convert.ToDecimal(dt2.Rows[i]["purchaseOrderMasterId"]);
                    dtbl2.Rows.Add(row);
                }
                // get balances remaining for all the available purchase order for the item
                foreach (DataRow row in dt2.Rows)
                {
                    DataTable dtblDetails = new DataTable();
                    dtblDetails = spPurchaseOrderDetails.PurchaseOrderDetailsViewByOrderMasterIdWithRemainingByNotInCurrPI
                               (Convert.ToDecimal(row["purchaseOrderMasterId"]), 0, 0);
                    purchaseOrderBalance += Convert.ToDecimal(dtblDetails.AsEnumerable().Where(i => i.Field<decimal>("productId") == productId)
                        .Select(i => i.Field<decimal>("qty")).Sum());
                }
            }
            return purchaseOrderBalance;
        }

        //used to get quantities per store
        private StockSummary stockSummary(StockSummarySearch input)
        {
            List<StockSummary> stockSummary = new List<StockSummary>();
            StockSummary response = new StockSummary();

            decimal returnValue = 0;
            try
            {
                DBMatConnection conn = new DBMatConnection();
                StockPostingSP spstock = new StockPostingSP();
                DataTable dtbl = new DataTable();
                decimal productId = input.ProductId;
                string pCode = input.ProductCode.ToString();
                decimal storeId = input.StoreId;
                string batch = input.BatchNo;

                string queryStr = "select top 1 f.fromDate from tbl_FinancialYear f";
                DateTime lastFinYearStart = Convert.ToDateTime(conn.getSingleValue(queryStr));

                //dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime("04/30/2017") /*PublicVariables._dtToDate*/, pCode, "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, MATFinancials.PublicVariables._dtToDate, pCode, "");

                decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                decimal prevProductId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                string productName = "", storeName = "", productCode = "";
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal totalAssetValue = 0, qtyBalance = 0;
                decimal value1 = 0;
                int i = 0;
                bool isAgainstVoucher = false;

                foreach (DataRow dr in dtbl.Rows)
                {
                    currentProductId = Convert.ToDecimal(dr["productId"]);
                    currentGodownID = Convert.ToDecimal(dr["godownId"]);
                    productCode = dr["productCode"].ToString();
                    string voucherType = "", refNo = "";

                    decimal inwardQty = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == prevProductId
                                         select p.Field<decimal>("inwardQty")).Sum();
                    decimal outwardQty = (from p in dtbl.AsEnumerable()
                                          where p.Field<decimal>("productId") == prevProductId
                                          select p.Field<decimal>("outwardQty")).Sum();

                    inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                    outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                    decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                    string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                    rate2 = Convert.ToDecimal(dr["rate"]);
                    if (previousRunningAverage != 0 && currentProductId == prevProductId)
                    {
                        purchaseRate = previousRunningAverage;
                    }
                    if (currentProductId == prevProductId)
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        prevProductId = Convert.ToDecimal(dr["productId"]);
                        productCode = dr["productCode"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    else
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    //======================== 2017-02-27 =============================== //
                    //if (againstVoucherType != "NA")
                    //{
                    //    voucherType = againstVoucherType;
                    //}
                    //else
                    //{
                    //    voucherType = dr["typeOfVoucher"].ToString();
                    //}
                    refNo = dr["refNo"].ToString();
                    if (againstVoucherType != "NA")
                    {
                        refNo = dr["againstVoucherNo"].ToString();
                        isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                        againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                        //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                        string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                        string typeOfVoucher = conn.getSingleValue(queryString);
                        if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                        if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                    }
                    voucherType = dr["typeOfVoucher"].ToString();
                    //---------  COMMENT END ----------- //
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if (outwardQty2 > 0)
                    {
                        if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = purchaseRate;
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                        {
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue -= outwardQty2 * rate2;
                        }

                        else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Sales Order")
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                    }
                    else if (voucherType == "Sales Return" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Rejection In" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;

                        }
                        else
                        {
                            computedAverageRate = previousRunningAverage; // (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                        // voucherType = "Sales Return";
                    }
                    else if (voucherType == "Delivery Note")
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                    }
                    else
                    {
                        // hndles other transactions such as purchase, opening balance, etc
                        qtyBal += inwardQty2 - outwardQty2;
                        stockValue += (inwardQty2 * rate2);
                    }
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        //qtyBal = Convert.ToDecimal(dr["inwardQty"]);
                        //stockValue = inwardQty2 * rate2;
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        if (voucherType == "Sales Invoice")
                        {
                            stockValue = qtyBal * purchaseRate;
                        }
                        else
                        {
                            stockValue = qtyBal * rate2;
                        }
                        //totalAssetValue += Math.Round(value1, 2);
                        totalAssetValue += value1;
                        //i++;
                        returnValue = totalAssetValue;
                    }
                    // -----------------added 2017-02-21 -------------------- //
                    if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        if (voucherType == "Sales Invoice")
                        {
                            computedAverageRate = purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = rate2;
                        }
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        stockValue = qtyBal * computedAverageRate;
                    }
                    // ------------------------------------------------------ //

                    previousRunningAverage = computedAverageRate;

                    value1 = stockValue;
                    prevProductId = currentProductId;
                    prevGodownID = currentGodownID;

                    if (i == dtbl.Rows.Count - 1)
                    {
                        var avgCost = "";
                        if (value1 != 0 && (inwardqt - outwardqt) != 0)
                        {
                            avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                        }
                        else
                        {
                            avgCost = "0.0";
                        }

                        stockSummary.Add(new StockSummary
                        {
                            ProductId = dr["productId"].ToString(),
                            StoreId = dr["godownId"].ToString(),
                            ProductName = dr["productName"].ToString(),
                            StoreName = dr["godownName"].ToString(),
                            PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                            SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                            QtyBalance = (inwardqt - outwardqt).ToString(),
                            AvgCostValue = avgCost,
                            StockValue = value1.ToString("N2"),
                            ProductCode = dr["productCode"].ToString()
                        });
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                    {
                        var avgCost = "";
                        if (value1 != 0 && (inwardqt - outwardqt) != 0)
                        {
                            avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                        }
                        else
                        {
                            avgCost = "0.0";
                        }

                        stockSummary.Add(new StockSummary
                        {
                            ProductId = dr["productId"].ToString(),
                            StoreId = dr["godownId"].ToString(),
                            ProductName = dr["productName"].ToString(),
                            StoreName = dr["godownName"].ToString(),
                            PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                            SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                            QtyBalance = (inwardqt - outwardqt).ToString(),
                            AvgCostValue = avgCost,
                            StockValue = value1.ToString("N2"),
                            ProductCode = dr["productCode"].ToString()
                        });
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    i++;
                }
                decimal totalStockValue = 0;
                foreach (var row in stockSummary)
                {
                    totalStockValue += Convert.ToDecimal(row.StockValue);
                }

                //not needed since its not needed to be displayed
                //stockSummary.Add(new StockSummary
                //{
                //    ProductName = "",
                //    StoreName = "Total Stock Value:",
                //    PurchaseRate = "",
                //    SalesRate = "",
                //    QtyBalance = qtyBalance.ToString("N2"),
                //    AvgCostValue = (totalStockValue / qtyBalance).ToString("N2"),
                //    StockValue = totalStockValue.ToString("N2"),
                //    ProductCode = ""
                //});
                //stockSummary.Add(new StockSummary
                //{
                //    ProductName = "",
                //    StoreName = "",
                //    PurchaseRate = "",
                //    SalesRate = "",
                //    QtyBalance = "",
                //    AvgCostValue = "",
                //    StockValue = "=============",
                //    ProductCode = ""
                //});
                
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKSR1:" + ex.Message;
            }
            response = stockSummary.FirstOrDefault();
            return response;
        }

        [HttpGet]
        public DataSet GetProductStores(decimal productId)
        {
            DBMatConnection conn = new DBMatConnection();
            //string queryStr = string.Format("SELECT tbl_SalesOrderDetails.productId,tbl_SalesOrderDetails.salesOrderMasterId,tbl_SalesOrderDetails.qty,"+
            //                                "tbl_SalesOrderDetails.extra1, tbl_Godown.godownId, tbl_Godown.godownName " +
            //                                "FROM tbl_SalesOrderDetails,tbl_Godown " +
            //                                "WHERE productId = {0} AND tbl_SalesOrderDetails.extra1=tbl_Godown.godownId", productId);
            string queryStr = string.Format("SELECT distinct tbl_StockPosting.godownId as godownId,tbl_StockPosting.productId,tbl_Godown.godownName " +
                                            "FROM tbl_StockPosting,tbl_Godown " +
                                            "WHERE tbl_StockPosting.productId={0} and tbl_StockPosting.godownId=tbl_Godown.godownId ", productId);
            return conn.customQuery(queryStr);
        }
    }
}
