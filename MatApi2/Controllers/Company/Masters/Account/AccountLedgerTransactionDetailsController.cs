﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials;
using System.Dynamic;
using System.Data;

namespace MatApi.Controllers.Company.Masters.Account
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AccountLedgerTransactionDetailsController : ApiController
    {
        dynamic bankTransferParam;
        public AccountLedgerTransactionDetailsController()
        {
                       
        }

        [HttpPost]
        public HttpResponseMessage GetBankTransferDetails(dynamic bankTransferParam)
        {
            //bankTransferParam = new ExpandoObject();
            try
            {
                dynamic response = new ExpandoObject();

                ContraDetailsSP spContraDetails = new ContraDetailsSP();
                ContraMasterSP spContraMaster = new ContraMasterSP();
                ContraMasterInfo infoContraMaster = new ContraMasterInfo();
                LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
                VoucherTypeSP spVoucherType = new VoucherTypeSP();
                infoContraMaster = spContraMaster.ContraMasterView(Convert.ToDecimal(bankTransferParam.decMasterId));
                
                int inDecimalPlace = PublicVariables._inNoOfDecimalPlaces;
                DataTable dtbl = new DataTable();
                dtbl = spContraDetails.ContraDetailsViewWithMasterId(Convert.ToDecimal(bankTransferParam.decMasterId));

                response.infoContraMaster = infoContraMaster;
                response.dtbl = dtbl;
                response.balance = GetAccountBalance(Convert.ToDecimal(bankTransferParam.ledgerId));

                return Request.CreateResponse(HttpStatusCode.OK,(object)response);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message.ToString());
            }
        }

        public decimal GetAccountBalance(decimal ledgerId)
        {
            MATFinancials.DAL.DBMatConnection _db = new MATFinancials.DAL.DBMatConnection();
            DataSet ds = _db.ExecuteQuery("select isnull(sum(debit-credit),0) as balance from tbl_LedgerPosting where ledgerId ='" + ledgerId + "'");
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                decimal balance = Convert.ToDecimal(ds.Tables[0].Rows[0]["balance"]);
                return balance;
            }
            return 0;
        }
    }
}
