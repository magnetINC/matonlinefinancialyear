﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MatDal;
using System.Web.Http.Cors;
using MATFinancials;

namespace MatApi.Controllers.Company.Masters.Account
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AccountGroupController : ApiController
    {
        AccountGroupSP accountGroupSp;
        public AccountGroupController()
        {
            accountGroupSp = new AccountGroupSP();
        }

        public HttpResponseMessage GetAccountGroups()
        {
            //var accountGroupsDt = accountGroupSp.AccountGroupViewAll();
            var accountGroupsDt = accountGroupSp.AccountGroupSearch();
            return Request.CreateResponse(HttpStatusCode.OK,accountGroupsDt);
        }

        public HttpResponseMessage GetAccountGroup(decimal accountGroupId)
        {
            //var accountGroupDt = accountGroupSp.AccountGroupView(accountGroupId);
            var accountGroupDt = accountGroupSp.AccountGroupViewForUpdate(accountGroupId);
            return Request.CreateResponse(HttpStatusCode.OK, accountGroupDt);
        }

        [HttpPost]
        public bool DeleteAccountGroup(AccountGroupInfo groupInfo)
        {
            if (accountGroupSp.AccountGroupReferenceDelete(groupInfo.AccountGroupId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddAccountGroup(AccountGroupInfo groupInfo)
        {
            groupInfo.ExtraDate = DateTime.Now;
            groupInfo.Extra1 = "";
            groupInfo.Extra2 = "";
            groupInfo.Narration = "";
            if (accountGroupSp.AccountGroupAddWithIdentity(groupInfo) > 0)
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditAccountGroup(AccountGroupInfo groupInfo)
        {
            groupInfo.ExtraDate = DateTime.Now;
            groupInfo.Extra1 = "";
            groupInfo.Extra2 = "";
            groupInfo.Narration = "";
            return accountGroupSp.AccountGroupUpdate(groupInfo);
        }
    }
}