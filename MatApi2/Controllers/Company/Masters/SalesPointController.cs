﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Company.Masters
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SalesPointController : ApiController
    {
        CounterSP counterSp;
        public SalesPointController()
        {
            counterSp = new CounterSP();
        }

        public List<CounterInfo> GetCounters()
        {
            var countersDt = counterSp.CounterViewAll();
            List<CounterInfo> counters = new List<CounterInfo>();
            foreach (DataRow row in countersDt.Rows)
            {
                counters.Add(new CounterInfo
                {
                    CounterId = Convert.ToDecimal(row["CounterId"]),
                    CounterName = row["CounterName"].ToString()
                });
            }
            return counters;
        }

        public CounterInfo GetCounter(decimal counterId)
        {
            var counterDt = counterSp.CounterView(counterId);
            CounterInfo counter = new CounterInfo
            {
                CounterId = Convert.ToDecimal(counterDt.CounterId),
                Narration = counterDt.CounterName

            };
            return counter;
        }

        [HttpPost]
        public bool DeleteCounter(CounterInfo counter)
        {
            if (counterSp.CounterCheckReferenceAndDelete(counter.CounterId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddCounter(CounterInfo counter)
        {
            counter.Extra1 = "";
            counter.Extra2 = "";
            counter.Narration = "";
            counter.ExtraDate = DateTime.Now;
            if (counterSp.CounterAdd(counter) > 0)
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditCounter(CounterInfo counter)
        {
            counter.Extra1 = "";
            counter.Extra2 = "";
            counter.Narration = "";
            counter.ExtraDate = DateTime.Now;
            return counterSp.CounterEdit(counter);
        }
    }
}
