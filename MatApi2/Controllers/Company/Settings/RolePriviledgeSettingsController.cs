﻿using MatApi.Models.Company.Settings;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Company.Settings
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RolePriviledgeSettingsController : ApiController
    {
        [HttpPost]
        public bool SaveSettings(List<PrivilegeInfo> settings)
        {
            bool isSaved = false;
            new PrivilegeSP().PrivilegeDeleteTabel(settings.FirstOrDefault().RoleId);   //delete all privileges assign to a role
                                                                                        //b4 creating new ones
            foreach (var setting in settings)
            {
                setting.ExtraDate = DateTime.Now;
                setting.Extra1 = "";
                setting.Extra2 = "";
                if (new PrivilegeSP().PrivilegeAdd(setting))
                {
                    isSaved = true;
                }
            }
            return isSaved;
        }

        [HttpGet]
        public DataTable LoadSettings()
        {
            return new PrivilegeSP().PrivilegeViewAll();
        }

        [HttpGet]
        public DataTable LoadCycleActionPrivilege()
        {
            string query = "SELECT * FROM tbl_CycleActionPriviledge";
            DataTable response = null;
            try
            {
                response= new DBMatConnection().customSelect(query);
            }
            catch(Exception ex)
            {

            }
            return response;
        }

        [HttpPost]
        public void SaveCycleActionPrivilege(List<CycleActionPrivilegeVM> input)
        {         
            DataTable response = null;
            try
            {
                foreach (var cycle in input)
                {
                    string query = string.Format("UPDATE tbl_CycleActionPriviledge SET RoleId={0} WHERE CycleAction='{1}'",cycle.RoleId,cycle.CycleAction);
                    new DBMatConnection().customUpdateQuery(query);
                }
            }
            catch (Exception ex)
            {

            }
        }
    }
}
