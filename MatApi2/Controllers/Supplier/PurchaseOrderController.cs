﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials;
using System.Data;
using MatApi.Models.Supplier;
using System.Dynamic;
using MATFinancials.DAL;
using MatApi.Models.Register;

namespace MatApi.Controllers.Company.Masters
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PurchaseOrderController : ApiController
    {
        RouteSP routeSp;
        DateTime defaultDt = DateTime.Now;
        decimal decVoucherTypeId = 0;
        decimal decSuffixPrefixId = 0;
        bool isAutomatic = false;
        string strVoucherNo = string.Empty;
        string TableName = "PurchaseOrderMaster";
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        string strInvoiceNo = string.Empty;
        decimal decPurchaseOrderTypeId = 10;
        public PurchaseOrderController()
        {
            routeSp = new RouteSP();
        }

        [HttpGet]
        public HttpResponseMessage LookUpData()
        {
            dynamic response = new ExpandoObject();

            var suppliers = new TransactionsGeneralFill().CashOrPartyUnderSundryCrComboFill();
            var taxes = new TaxSP().TaxViewAll();
            var products = new ProductSP().ProductViewAll();
            var currency = new TransactionsGeneralFill().CurrencyComboByDate(DateTime.Now);
            var units = new UnitSP().UnitViewAll();

            response.Suppliers = suppliers;
            response.Taxes = taxes;
            response.Currencies = currency;
            response.Products = products;
            response.Units = units;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetSuppliers()
        {

            var suppliers = new TransactionsGeneralFill().CashOrPartyUnderSundryCrComboFill();

            return Request.CreateResponse(HttpStatusCode.OK, (object)suppliers);
        }

        [HttpPost]
        public HttpResponseMessage Registers(PurchaseOrderRegisterVM input)
        {
            var response = new PurchaseOrderMasterSP().PurchaseOrderMasterViewAll(input.InvoiceNo, input.LedgerId, input.FromDate, input.ToDate, input.Condition);
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public decimal PurchaseOrderListingCount()
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("select count(orderStatus) from tbl_PurchaseOrderMaster where orderStatus ='Pending'");
            var count = conn.getSingleValue(queryStr);

            return Convert.ToDecimal(count);
        }

        [HttpGet]
        public HttpResponseMessage PurchaseOrderListing(DateTime fromDate, DateTime toDate, string approved)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            dynamic response = new ExpandoObject(); ;
            string GetQuery = string.Format("");
            if (approved == "All" || approved == "" || approved == null)
            {
                GetQuery = string.Format("");
                GetQuery = string.Format("SELECT * FROM tbl_PurchaseOrderMaster WHERE extraDate BETWEEN '{0}' AND '{1}' ORDER BY extraDate DESC", fromDate, toDate);
            }
            else
            {
                GetQuery = string.Format("");
                GetQuery = string.Format("SELECT * FROM tbl_PurchaseOrderMaster WHERE extraDate BETWEEN '{0}' AND '{1}' AND orderStatus='{2}' ORDER BY extraDate DESC", fromDate, toDate, approved);
            }
            response = db.GetDataSet(GetQuery);

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetPurchaseOrderDetails(decimal purchaseOrderMasterId)
        {
            dynamic response = new ExpandoObject();
            var master = new PurchaseOrderMasterSP().PurchaseOrderMasterView(purchaseOrderMasterId);
            var details = new PurchaseOrderDetailsSP().PurchaseOrderDetailsViewByMasterId(purchaseOrderMasterId);

            response.Master = master;
            response.Details = details;
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage SearchProduct(string searchBy, string filter)
        {
            SalesDetailsSP spSalesDetails = new SalesDetailsSP();
            UnitSP spUnit = new UnitSP();
            GodownSP spGodown = new GodownSP();
            BatchSP spBatch = new BatchSP();
            TaxSP spTax = new TaxSP();
            RackSP spRack = new RackSP();
            UnitConvertionSP SpUnitConvertion = new UnitConvertionSP();

            dynamic response = new ExpandoObject();


            if (searchBy == "ProductCode")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByProductCodeForSI(28, filter);
                var productId = Convert.ToDecimal(productDetails.Rows[0]["ProductId"]);
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[11].ToString()));
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                //response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productId));
            }
            else if (searchBy == "ProductName")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByProductNameForSI(28, filter);
                var productId = Convert.ToDecimal(productDetails.Rows[0]["ProductId"]);
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[11].ToString()));
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                //response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productId));
            }
            else if (searchBy == "Barcode")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByBarcodeForSI(28, filter);
                var productId = Convert.ToDecimal(productDetails.Rows[0]["ProductId"]);
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[11].ToString()));
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                //response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productId));
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }


        public HttpResponseMessage GetTaxAmount(string taxId, string amount)
        {

            decimal taxAmount = 0;
            TaxSP SpTax = new TaxSP();
            var amt = Convert.ToDecimal(amount);
            TaxInfo InfoTaxObj = SpTax.TaxView(Convert.ToDecimal(taxId));
            taxAmount = Math.Round(((amt * InfoTaxObj.Rate) / (100)), PublicVariables._inNoOfDecimalPlaces);
            return Request.CreateResponse(HttpStatusCode.OK, taxAmount);
        }


        [HttpPost]
        public bool SavePurchaseOrder(PurchaseOrderModel obj)
        {
            //route.ExtraDate = DateTime.Now;
            SaveFunction(obj);
            return true;//routeSp.RouteAdd(route);
        }


        private decimal getQuantityInStock(decimal productId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format(" SELECT  convert(decimal(18, 2), (ISNULL(SUM(s.inwardQty), 0) - ISNULL(SUM(s.outwardQty), 0))) AS Currentstock " +
                " FROM tbl_StockPosting s inner join tbl_product p on p.productId = s.productId " +
                " INNER JOIN tbl_Batch b on s.batchId = b.batchId " +
                " WHERE p.productId = '{0}' AND s.date <= '{1}' ", productId, MATFinancials.PublicVariables._dtToDate);
            return Convert.ToDecimal(conn.getSingleValue(queryStr));
        }

        public decimal SaveFunction(PurchaseOrderModel obj)
        {
            decimal res = 0;
            try
            {
                PurchaseOrderMasterInfo infoPurchaseOrderMaster = new PurchaseOrderMasterInfo();
                PurchaseOrderDetailsSP spPurchaseOrderDetails = new PurchaseOrderDetailsSP();
                PurchaseOrderDetailsInfo infoPurchaseOrderDetails = new PurchaseOrderDetailsInfo();
                PurchaseOrderMasterSP spPurchaseOrderMaster = new PurchaseOrderMasterSP();
                ProductInfo infoProduct = new ProductInfo();
                ProductSP spProduct = new ProductSP();
                SettingsSP spSettings = new SettingsSP();

                infoPurchaseOrderMaster.VoucherNo = obj.OrderNo;
                infoPurchaseOrderMaster.Date = Convert.ToDateTime(obj.TransDate);
                infoPurchaseOrderMaster.DueDate = Convert.ToDateTime(obj.DueDate);
                infoPurchaseOrderMaster.LedgerId = Convert.ToDecimal(obj.Supplier);
                infoPurchaseOrderMaster.VoucherTypeId = decPurchaseOrderTypeId;
                infoPurchaseOrderMaster.InvoiceNo = obj.OrderNo;//obj.OrderNo;
                infoPurchaseOrderMaster.UserId = PublicVariables._decCurrentUserId;
                infoPurchaseOrderMaster.EmployeeId = PublicVariables._decCurrentUserId;//by default current userid used as current employeeid
                infoPurchaseOrderMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoPurchaseOrderMaster.Narration = obj.Narration.Trim();
                infoPurchaseOrderMaster.TotalAmount = Convert.ToDecimal(obj.TotalAmount);
                infoPurchaseOrderMaster.exchangeRateId = Convert.ToDecimal(obj.Currency);
                infoPurchaseOrderMaster.Extra1 = string.Empty;
                infoPurchaseOrderMaster.Extra2 = string.Empty;
                infoPurchaseOrderMaster.orderStatus = "Pending";
                var decPurchaseOrderMasterIdentity = Convert.ToDecimal(spPurchaseOrderMaster.PurchaseOrderMasterAdd(infoPurchaseOrderMaster));
                int inRowcount = obj.LineItems.Count;
                if (inRowcount > 0)
                {
                    foreach (var lintItm in obj.LineItems)
                    {
                        infoPurchaseOrderDetails.PurchaseOrderMasterId = decPurchaseOrderMasterIdentity;
                        if (lintItm.ProductCode != null && lintItm.ProductCode != "")
                        {
                            infoProduct = spProduct.ProductViewByCode(lintItm.ProductCode);
                            infoPurchaseOrderDetails.ProductId = infoProduct.ProductId;
                        }
                        var unit = new UnitSP().UnitViewAllByProductId(infoProduct.ProductId);
                        var batch = new BatchSP().BatchIdViewByProductId(infoProduct.ProductId);
                        var unitconv = new UnitConvertionSP().UnitViewAllByProductId(infoProduct.ProductId);
                        if (lintItm.Qty > 0)
                        {
                            infoPurchaseOrderDetails.Qty = Convert.ToDecimal(lintItm.Qty);
                        }
                        if (lintItm.Unit > 0)
                        {
                            infoPurchaseOrderDetails.UnitId = lintItm.Unit;
                            infoPurchaseOrderDetails.UnitConversionId = unitconv.UnitconvertionId;//Convert.ToDecimal(dgvPurchaseOrder.Rows[inI].Cells["dgvtxtUnitConversionId"].Value.ToString());
                        }
                        infoPurchaseOrderDetails.Rate = Convert.ToDecimal(lintItm.Rate);
                        infoPurchaseOrderDetails.Amount = Convert.ToDecimal(lintItm.Amount);
                        infoPurchaseOrderDetails.SlNo = Convert.ToInt32(lintItm.SiNo);
                        infoPurchaseOrderDetails.Extra1 = string.Empty;
                        infoPurchaseOrderDetails.Extra2 = string.Empty;
                        infoPurchaseOrderDetails.ExtraDate = DateTime.Now;
                        infoPurchaseOrderDetails.ProjectId = 0;
                        infoPurchaseOrderDetails.CategoryId = 0;
                        infoPurchaseOrderDetails.TaxId = lintItm.TaxId;
                        infoPurchaseOrderDetails.TaxAmount = lintItm.TaxAmount;
                        if (lintItm.Description != null && !string.IsNullOrEmpty(lintItm.Description))
                        {
                            infoPurchaseOrderDetails.itemDescription = lintItm.Description;
                        }
                        res = spPurchaseOrderDetails.PurchaseOrderDetailsAdd(infoPurchaseOrderDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "PO35:" + ex.Message;
            }
            return res;
        }

        [HttpGet]
        public string GetAutoVoucherNo()
        {
            return generateVoucherNo();
        }

        private string generateVoucherNo()
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            PurchaseOrderMasterSP spMaster = new PurchaseOrderMasterSP();
            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            SettingsSP spSettings = new SettingsSP();

            strVoucherNo = "0";
            strVoucherNo = spMaster.PurchaseOrderVoucherMasterMax(decPurchaseOrderTypeId).ToString();
            if (strVoucherNo == string.Empty)
            {
                strVoucherNo = "0";
            }
            strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPurchaseOrderTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, TableName);
            if (strVoucherNo != spMaster.PurchaseOrderVoucherMasterMax(decPurchaseOrderTypeId).ToString())
            {
                strVoucherNo = spMaster.PurchaseOrderVoucherMasterMax(decPurchaseOrderTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPurchaseOrderTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, TableName);
                if (spMaster.PurchaseOrderVoucherMasterMax(decPurchaseOrderTypeId) == "0")
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPurchaseOrderTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, TableName);
                }
            }
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decPurchaseOrderTypeId, DateTime.Now);
            strPrefix = infoSuffixPrefix.Prefix;
            strSuffix = infoSuffixPrefix.Suffix;
            var decPurchaseSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
            var strOrderNo = strPrefix + strVoucherNo + strSuffix;

            return strOrderNo;
        }
    }
}