﻿using MatApi.Models.Register;
using MatApi.Models.Supplier;
using MATFinancials;
using MATFinancials.Other;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PurchaseInvoiceController : ApiController
    {
        decimal decSalesDetailsId = 0;
        List<decimal> listofDetailsId = new List<decimal>();
        decimal TotalAmount = 0;
        string strDebitNoteMasterTableName = "DebitNoteMaster";
        string strTableName = "PurchaseMaster";
        string strPrefix="";
        string strSuffix = "";
        decimal ExchangeRateId = 1;
        string comStr = "";
        decimal decPurchaseInvoiceVoucherTypeId = 29;
        string strVoucherNo = string.Empty;
        DataTable dtForReference = new DataTable();

        public PurchaseInvoiceController()
        {

        }

        [HttpGet]
        public HttpResponseMessage InvoiceLookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var suppliers = transactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            var accountLedgers=new PurchaseMasterSP().AccountLedgerViewForAdditionalCost();
            var units = new UnitSP().UnitViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks = new RackSP().RackViewAll();
            var batches = new BatchSP().BatchViewAll();
            var tax= new TaxSP().TaxView(2);
            var taxes = new TaxSP().TaxViewAll();

            dynamic response = new ExpandoObject();
            response.Suppliers = suppliers;
            response.Currencies = currencies;
            response.AccountLedgers = accountLedgers;
            response.Units = units;
            response.Stores = stores;
            response.Racks = racks;
            response.Batches = batches;
            response.Tax = tax;
            response.Taxes = taxes;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public DataTable GetVoucherTypes(string purchaseMode)
        {
            VoucherTypeSP spVoucherType = new VoucherTypeSP();
            try
            {
                return spVoucherType.VoucherTypeSelectionComboFill(purchaseMode);
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        [HttpGet]
        public DataTable GetPurchaseOrderNumbers(decimal supplierId,decimal voucherTypeId)
        {
            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();
            DataTable dtbl = new DataTable();
            dtbl = spPurchaseMaster.GetOrderNoCorrespondingtoLedgerByNotInCurrPI(supplierId, 0,voucherTypeId);
            DataRow drow = dtbl.NewRow();
            drow["purchaseOrderMasterId"] = 0;
            drow["invoiceNo"] = string.Empty;
            dtbl.Rows.InsertAt(drow, 0);
            return dtbl;
        }

        [HttpGet]
        public DataTable GetMaterialReceiptNumbers(decimal supplierId, decimal voucherTypeId)
        {
            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();
            DataTable dtbl = new DataTable();
            dtbl = spPurchaseMaster.GetMaterialReceiptNoCorrespondingtoLedgerByNotInCurrPI(supplierId, 0, voucherTypeId);
            DataRow drow = dtbl.NewRow();
            drow["materialReceiptMasterId"] = 0;
            drow["invoiceNo"] = string.Empty;
            dtbl.Rows.InsertAt(drow, 0);
            return dtbl;
        }

        [HttpGet]
        public DataTable GetFormTypes()
        {
            VoucherTypeSP spVoucherType = new VoucherTypeSP();
            DataTable dtbl = new DataTable();
            try
            {
                dtbl = spVoucherType.VoucherTypeSelectionComboFill("Purchase Invoice");
                DataRow drow = dtbl.NewRow();
                drow["voucherTypeId"] = 0;
                drow["voucherTypeName"] = "All";
                dtbl.Rows.InsertAt(drow, 0);
                return dtbl;
            }
            catch (Exception ex)
            {                
            }
            return null;
        }

        [HttpPost]
        public HttpResponseMessage Registers(PurchaseInvoiceRegisterVM input)
        {
            var resp = new PurchaseMasterSP().PurchaseInvoiceRegisterFill(input.Column, input.FromDate,input.ToDate,input.LedgerId, input.PurchaseMode,input.VoucherType, input.InvoiceNo);
            return Request.CreateResponse(HttpStatusCode.OK,(object)resp);
        }

        [HttpPost]
        public string SavePurchaseInvoice(PurchaseInvoiceVM input)
        {
            //remove this restriction and don't type in the part value you want to apply so that the credit note will not split. twerk further
            decimal totalAdvance = 0;
            decimal totalInvoiceCreated = input.GrandTotal;

            if (totalAdvance > totalInvoiceCreated)
            {
                return "APPLIED_AMOUNT_GREATER_THAN_BILL_AMOUNT";
            }
            decimal decPurchaseMasterId = 0;
            PurchaseMasterInfo infoPurchaseMaster = new PurchaseMasterInfo();
            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();
            PurchaseDetailsInfo infoPurchaseDetails = new PurchaseDetailsInfo();
            PurchaseDetailsSP spPurchaseDetails = new PurchaseDetailsSP();
            MaterialReceiptMasterInfo infoMaterialReceiptMaster = new MaterialReceiptMasterInfo();
            MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();
            PurchaseOrderMasterInfo infoPurchaseOrderMaster = new PurchaseOrderMasterInfo();
            PurchaseOrderMasterSP spPurchaseOrderMaster = new PurchaseOrderMasterSP();
            StockPostingInfo infoStockPosting = new StockPostingInfo();
            StockPostingSP spStockPosting = new StockPostingSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            PurchaseBillTaxInfo infoPurchaseBillTax = new PurchaseBillTaxInfo();
            PurchaseBillTaxSP spPurchaseBillTax = new PurchaseBillTaxSP();
            AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
            AccountLedgerSP spAccountLedger = new AccountLedgerSP();
            UnitConvertionSP spUnitConvertion = new UnitConvertionSP();
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            int itemsinList = 0;
            decimal existingLedgerId = 0;
            try
            {
                /*-----------------------------------------Purchase Master Add----------------------------------------------------*/
                infoPurchaseMaster.AdditionalCost = input.PurchaseMasterInfo.AdditionalCost;
                infoPurchaseMaster.BillDiscount = input.PurchaseMasterInfo.BillDiscount;
                infoPurchaseMaster.CreditPeriod = input.PurchaseMasterInfo.CreditPeriod;
                infoPurchaseMaster.Date = input.PurchaseMasterInfo.Date;
                infoPurchaseMaster.ExchangeRateId = input.PurchaseMasterInfo.ExchangeRateId;
                infoPurchaseMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoPurchaseMaster.GrandTotal = input.PurchaseMasterInfo.GrandTotal;
                infoPurchaseMaster.InvoiceNo = input.PurchaseMasterInfo.VoucherNo;
                //if (isAutomatic)
                //{
                //    infoPurchaseMaster.SuffixPrefixId = decPurchaseInvoiceSuffixPrefixId;
                //    infoPurchaseMaster.VoucherNo = strVoucherNo;
                //}
                //else
                //{
                    infoPurchaseMaster.SuffixPrefixId = 0;
                    infoPurchaseMaster.VoucherNo = input.PurchaseMasterInfo.VoucherNo;
                //}
                infoPurchaseMaster.LedgerId = input.PurchaseMasterInfo.LedgerId;
                infoPurchaseMaster.LrNo = input.PurchaseMasterInfo.LrNo;
                if (input.PurchaseModeText == "Against Material Receipt")
                {
                    infoPurchaseMaster.MaterialReceiptMasterId = input.PurchaseMasterInfo.MaterialReceiptMasterId;
                }
                else
                {
                    infoPurchaseMaster.MaterialReceiptMasterId = 0;
                }
                infoPurchaseMaster.Narration = input.PurchaseMasterInfo.Narration;
                infoPurchaseMaster.PurchaseAccount = 11;    //Convert.ToDecimal(cmbPurchaseAccount.SelectedValue.ToString());    // Modified so as to report to Purchase Account
                if (input.PurchaseModeText == "Against Purchase Order")
                {
                    infoPurchaseMaster.PurchaseOrderMasterId = input.PurchaseMasterInfo.PurchaseOrderMasterId;
                }
                else
                {
                    infoPurchaseMaster.PurchaseOrderMasterId = 0;
                }
                infoPurchaseMaster.TotalAmount = input.PurchaseMasterInfo.TotalAmount;
                infoPurchaseMaster.TotalTax = input.PurchaseMasterInfo.TotalTax;
                infoPurchaseMaster.TransportationCompany = input.PurchaseMasterInfo.TransportationCompany;
                infoPurchaseMaster.UserId = PublicVariables._decCurrentUserId;
                infoPurchaseMaster.VendorInvoiceDate = input.PurchaseMasterInfo.VendorInvoiceDate;
                infoPurchaseMaster.VendorInvoiceNo = input.PurchaseMasterInfo.VendorInvoiceNo;
                infoPurchaseMaster.VoucherTypeId = decPurchaseInvoiceVoucherTypeId;
                infoPurchaseMaster.Extra1 = string.Empty;
                infoPurchaseMaster.Extra2 = string.Empty;
                infoPurchaseMaster.ExtraDate = Convert.ToDateTime(DateTime.Now);
                decPurchaseMasterId = spPurchaseMaster.PurchaseMasterAdd(infoPurchaseMaster);
                infoPurchaseOrderMaster = spPurchaseOrderMaster.PurchaseOrderMasterView(infoPurchaseMaster.PurchaseOrderMasterId);
                infoMaterialReceiptMaster = spMaterialReceiptMaster.MaterialReceiptMasterView(infoPurchaseMaster.MaterialReceiptMasterId);
                foreach (var detail in input.PurchaseDetails)
                {
                    /*-----------------------------------------Purchase Details Add----------------------------------------------------*/
                    infoPurchaseDetails.Amount = detail.Amount;
                    infoPurchaseDetails.BatchId = detail.BatchId;
                    infoPurchaseDetails.Discount = detail.Discount;
                    infoPurchaseDetails.GodownId = detail.GodownId;
                    infoPurchaseDetails.GrossAmount = detail.GrossAmount;
                    infoPurchaseDetails.NetAmount = detail.NetAmount;
                    infoPurchaseDetails.OrderDetailsId = detail.OrderDetailsId;
                    infoPurchaseDetails.ProductId = detail.ProductId;
                    infoPurchaseDetails.PurchaseMasterId = decPurchaseMasterId;
                    infoPurchaseDetails.Qty = detail.Qty;
                    infoPurchaseDetails.RackId = detail.RackId;
                    infoPurchaseDetails.Rate = detail.Rate;
                    infoPurchaseDetails.ReceiptDetailsId = detail.ReceiptDetailsId;
                    infoPurchaseDetails.SlNo = detail.SlNo;
                    infoPurchaseDetails.TaxAmount = detail.TaxAmount;
                    infoPurchaseDetails.TaxId = detail.TaxId;
                    infoPurchaseDetails.UnitConversionId = detail.UnitConversionId;
                    infoPurchaseDetails.UnitId = detail.UnitId;
                    infoPurchaseDetails.Extra1 = string.Empty;
                    infoPurchaseDetails.Extra2 = string.Empty;
                    //if (dgvrow.Cells["dgvCmbProject"].Value != null && !string.IsNullOrEmpty(dgvrow.Cells["dgvCmbProject"].Value.ToString()))
                    //{
                    //    infoPurchaseDetails.ProjectId = Convert.ToInt32(dgvrow.Cells["dgvCmbProject"].Value.ToString());
                    //}
                    //else
                    //{
                        infoPurchaseDetails.ProjectId = 0;
                    //}
                    //if (dgvrow.Cells["dgvCmbCategory"].Value != null && !string.IsNullOrEmpty(dgvrow.Cells["dgvCmbCategory"].Value.ToString()))
                    //{
                    //    infoPurchaseDetails.CategoryId = Convert.ToInt32(dgvrow.Cells["dgvCmbCategory"].Value.ToString());
                    //}
                    //else
                    //{
                        infoPurchaseDetails.CategoryId = 0;
                    //}
                    //if (dgvrow.Cells["itemDescription"].Value != null && !string.IsNullOrEmpty(dgvrow.Cells["itemDescription"].Value.ToString()))
                    //{
                        infoPurchaseDetails.itemDescription = detail.itemDescription;
                    //}
                    infoPurchaseDetails.ExtraDate = Convert.ToDateTime(DateTime.Today);
                    decSalesDetailsId = spPurchaseDetails.PurchaseDetailsAdd(infoPurchaseDetails);
                    listofDetailsId.Add(decSalesDetailsId);
                    /*-----------------------------------------Stock Posting----------------------------------------------------*/
                    infoStockPosting.BatchId = infoPurchaseDetails.BatchId;
                    infoStockPosting.Date = infoPurchaseMaster.Date;
                    infoStockPosting.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    infoStockPosting.GodownId = infoPurchaseDetails.GodownId;
                    infoStockPosting.InwardQty = infoPurchaseDetails.Qty; /// spUnitConvertion.UnitConversionRateByUnitConversionId(infoPurchaseDetails.UnitConversionId);
                    infoStockPosting.OutwardQty = 0;
                    infoStockPosting.ProductId = infoPurchaseDetails.ProductId;
                    infoStockPosting.RackId = infoPurchaseDetails.RackId;
                    infoStockPosting.Rate = infoPurchaseDetails.Rate;
                    infoStockPosting.UnitId = infoPurchaseDetails.UnitId;
                    if (infoPurchaseDetails.OrderDetailsId != 0)
                    {
                        infoStockPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                        infoStockPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                        infoStockPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                        infoStockPosting.AgainstInvoiceNo = "NA";
                        infoStockPosting.AgainstVoucherNo = "NA";
                        infoStockPosting.AgainstVoucherTypeId = 0;
                    }
                    else if (infoPurchaseDetails.ReceiptDetailsId != 0)
                    {
                        infoStockPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                        infoStockPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                        infoStockPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                        infoStockPosting.AgainstInvoiceNo = "NA";
                        infoStockPosting.AgainstVoucherNo = "NA";
                        infoStockPosting.AgainstVoucherTypeId = 0;
                    }
                    else if (infoPurchaseDetails.OrderDetailsId == 0 && infoPurchaseDetails.ReceiptDetailsId == 0)
                    {
                        infoStockPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                        infoStockPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                        infoStockPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                        infoStockPosting.AgainstInvoiceNo = "NA";
                        infoStockPosting.AgainstVoucherNo = "NA";
                        infoStockPosting.AgainstVoucherTypeId = 0;
                    }
                    infoStockPosting.Extra1 = string.Empty;
                    infoStockPosting.Extra2 = string.Empty;
                    infoStockPosting.ExtraDate = Convert.ToDateTime(DateTime.Today);
                    spStockPosting.StockPostingAdd(infoStockPosting);     // old implementation 20161201
                                                                          // Urefe added if statement so that material receipt will not affect stock when raising invoice against the material receipt 20161201, 
                                                                          // Urefe removed comment because it should affect it +ve and -ve nilling off the effect
                    if (infoMaterialReceiptMaster.VoucherTypeId == 11)
                    {
                        spStockPosting.StockPostingAdd(infoStockPosting);
                    }

                    if (infoPurchaseDetails.ReceiptDetailsId != 0)
                    {
                        infoStockPosting.InvoiceNo = infoMaterialReceiptMaster.InvoiceNo;
                        infoStockPosting.VoucherNo = infoMaterialReceiptMaster.VoucherNo;
                        infoStockPosting.VoucherTypeId = infoMaterialReceiptMaster.VoucherTypeId;
                        infoStockPosting.AgainstInvoiceNo = infoPurchaseMaster.InvoiceNo;
                        infoStockPosting.AgainstVoucherNo = infoPurchaseMaster.VoucherNo;
                        infoStockPosting.AgainstVoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                        infoStockPosting.InwardQty = 0;
                        infoStockPosting.OutwardQty = infoPurchaseDetails.Qty;/// spUnitConvertion.UnitConversionRateByUnitConversionId(infoPurchaseDetails.UnitConversionId);
                        spStockPosting.StockPostingAdd(infoStockPosting);     // old implementation 20161201
                                                                              // Urefe added if statement so that material receipt will not affect stock when raising invoice against the material receipt 20161201, 
                                                                              // Urefe removed comment because it should affect it +ve and -ve nilling off the effect
                        if (infoMaterialReceiptMaster.VoucherTypeId != 11)
                        {
                            spStockPosting.StockPostingAdd(infoStockPosting);
                        }
                    }
                }
                /*-----------------------------------------Ledger Posting----------------------------------------------------*/
                infoLedgerPosting.Credit = input.GrandTotal * spExchangeRate.ExchangeRateViewByExchangeRateId(input.PurchaseMasterInfo.ExchangeRateId);
                infoLedgerPosting.Debit = 0;
                //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                //infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.DetailsId = decPurchaseMasterId;
                infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                infoLedgerPosting.LedgerId = infoPurchaseMaster.LedgerId;
                infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                infoLedgerPosting.ExtraDate = DateTime.Now;
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                decimal decBilldiscount = input.PurchaseMasterInfo.BillDiscount;
                if (decBilldiscount > 0)
                {
                    infoLedgerPosting.Credit = decBilldiscount * spExchangeRate.ExchangeRateViewByExchangeRateId(input.PurchaseMasterInfo.ExchangeRateId);
                    infoLedgerPosting.Debit = 0;
                    //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                    infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                    infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                    infoLedgerPosting.LedgerId = 9;//ledger id of discount received
                    infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                    infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
                foreach (CustomProductInfo row in input.ProductInfo)
                {
                    LedgerPostingSP ledgerPostingExpensAccountId = new LedgerPostingSP();
                    infoLedgerPosting.Credit = 0;
                    infoLedgerPosting.Debit = row.NetAmount;   //TotalNetAmount(); //* spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(cmbCurrency.SelectedValue.ToString()));
                                                                                                                                                                                          //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                    infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                    //infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.DetailsId = listofDetailsId[itemsinList];
                    infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                    //infoLedgerPosting.LedgerId = infoPurchaseMaster.PurchaseAccount;//ledger posting of purchase account  dgvrow.Cells["dgvcmbTax"].Value.ToString
                    // ----------------- Old implementation changed by Precious and then Urefe 20160919 ------------------- //
                    //infoLedgerPosting.LedgerId = Convert.ToDecimal(row.Cells["dgvtxtExpenseAccount"].Value.ToString()); 
                    infoLedgerPosting.LedgerId = ledgerPostingExpensAccountId.ProductExpenseAccountId(row.ProductInfo.ProductCode);
                    infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                    infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);    // post to credit side
                    itemsinList++;
                }

                /*
                infoLedgerPosting.Credit = 0;
                infoLedgerPosting.Debit = TotalNetAmount(); //* spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(cmbCurrency.SelectedValue.ToString()));
                infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);
                infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                infoLedgerPosting.LedgerId = infoPurchaseMaster.PurchaseAccount;//ledger posting of purchase account
                infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                infoLedgerPosting.ExtraDate = DateTime.Now;
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting); 
                */
                foreach (var additionalCost in input.AdditionalCostInfo)
                {
                    /*-----------------------------------------Additional Cost Add----------------------------------------------------*/
                    infoAdditionalCost.Credit = 0;
                    infoAdditionalCost.Debit = additionalCost.Debit;
                    infoAdditionalCost.LedgerId = additionalCost.LedgerId;
                    infoAdditionalCost.VoucherNo = infoPurchaseMaster.VoucherNo;
                    infoAdditionalCost.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                    infoAdditionalCost.Extra1 = string.Empty;
                    infoAdditionalCost.Extra2 = string.Empty;
                    infoAdditionalCost.ExtraDate = DateTime.Now;
                    spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                    /*-----------------------------------------Additional Cost Ledger Posting----------------------------------------------------*/
                    infoLedgerPosting.Credit = 0;
                    infoLedgerPosting.Debit = infoAdditionalCost.Debit * spExchangeRate.ExchangeRateViewByExchangeRateId(input.PurchaseMasterInfo.ExchangeRateId);
                    //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                    infoLedgerPosting.Date = Convert.ToDateTime(input.PurchaseMasterInfo.Date);
                    infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                    infoLedgerPosting.LedgerId = infoAdditionalCost.LedgerId;
                    infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                    infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
                foreach (var tax in input.PurchaseBillTaxInfo)
                {
                    /*-----------------------------------------PurchaseBillTax Add----------------------------------------------------*/
                    infoPurchaseBillTax.PurchaseMasterId = decPurchaseMasterId;
                    infoPurchaseBillTax.TaxAmount = tax.TaxAmount;
                    infoPurchaseBillTax.TaxId = tax.TaxId;
                    infoPurchaseBillTax.Extra1 = string.Empty;
                    infoPurchaseBillTax.Extra2 = string.Empty;
                    infoPurchaseBillTax.ExtraDate = DateTime.Now;
                    spPurchaseBillTax.PurchaseBillTaxAdd(infoPurchaseBillTax);
                    /*-----------------------------------------Tax Ledger Posting----------------------------------------------------*/
                    infoLedgerPosting.Credit = 0;
                    infoLedgerPosting.Debit = infoPurchaseBillTax.TaxAmount * spExchangeRate.ExchangeRateViewByExchangeRateId(input.PurchaseMasterInfo.ExchangeRateId);
                    //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                    infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                    infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                    infoLedgerPosting.LedgerId = 59;
                    infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                    infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
                /*-----------------------------------------PartyBalance Posting----------------------------------------------------*/
                infoAccountLedger = spAccountLedger.AccountLedgerView(infoPurchaseMaster.LedgerId);
                if (infoAccountLedger.BillByBill == true)
                {
                    infoPartyBalance.Credit = Convert.ToDecimal(input.GrandTotal);
                    infoPartyBalance.Debit = 0;
                    if (input.PurchaseMasterInfo.CreditPeriod != string.Empty)
                    {
                        infoPartyBalance.CreditPeriod = Convert.ToInt32(input.PurchaseMasterInfo.CreditPeriod);
                    }
                    infoPartyBalance.Date = Convert.ToDateTime(input.PurchaseMasterInfo.Date);
                    infoPartyBalance.ExchangeRateId = infoPurchaseMaster.ExchangeRateId;
                    infoPartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    infoPartyBalance.LedgerId = infoPurchaseMaster.LedgerId;
                    infoPartyBalance.ReferenceType = "NEW";
                    infoPartyBalance.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                    infoPartyBalance.VoucherNo = infoPurchaseMaster.VoucherNo;
                    infoPartyBalance.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                    infoPartyBalance.AgainstInvoiceNo = "NA";
                    infoPartyBalance.AgainstVoucherNo = "NA";
                    infoPartyBalance.AgainstVoucherTypeId = 0;
                    infoPartyBalance.Extra1 = string.Empty;
                    infoPartyBalance.Extra2 = string.Empty;
                    infoPartyBalance.ExtraDate = DateTime.Now;
                    spPartyBalance.PartyBalanceAdd(infoPartyBalance);
                }
                applyDebitNote(input);

                //Messages.SavedMessage();
                //if (chkSendEmail.Checked)
                //{
                //    spPurchaseMaster = new PurchaseMasterSP();
                //    Templates PDFInvoice = new Templates();
                //    decimal decPurchaseOrderMasterId = 0;
                //    decimal decMaterialReceiptMasterId = 0;
                //    if (cmbPurchaseMode.Text == "Against Purchase Order")
                //    {
                //        decPurchaseOrderMasterId = Convert.ToDecimal(cmbOrderNo.SelectedValue.ToString());
                //    }
                //    else if (cmbPurchaseMode.Text == "Against Material Receipt")
                //    {
                //        decMaterialReceiptMasterId = Convert.ToDecimal(cmbOrderNo.SelectedValue.ToString());
                //    }
                //    DataSet dsForInvoicePdf = spPurchaseMaster.PurchaseInvoicePrinting(PublicVariables._decCurrentCompanyId, decPurchaseOrderMasterId, decMaterialReceiptMasterId, decPurchaseMasterId);
                //    invoiceEmailDetails invoiceEmailDetails = null;
                //    PDFInvoice.generateInvoicePDF(dsForInvoicePdf, Convert.ToDecimal(cmbCashOrParty.SelectedValue), "purchase", ref invoiceEmailDetails);
                //    if (File.Exists(invoiceEmailDetails.attachment))
                //    {
                //        try
                //        {
                //            frmSendMail frmSendMail = new frmSendMail();
                //            frmSendMail.MdiParent = formMDI.MDIObj;
                //            frmSendMail open = Application.OpenForms["frmSendMail"] as frmSendMail;
                //            if (open == null)
                //            {
                //                frmSendMail.WindowState = FormWindowState.Normal;
                //                frmSendMail.MdiParent = formMDI.MDIObj;
                //                frmSendMail.CallFromPurchaseInvoice(this, invoiceEmailDetails);
                //            }
                //            else
                //            {
                //                open.MdiParent = formMDI.MDIObj;
                //                open.CallFromPurchaseInvoice(this, invoiceEmailDetails);
                //                open.BringToFront();
                //                if (open.WindowState == FormWindowState.Minimized)
                //                {
                //                    open.WindowState = FormWindowState.Normal;
                //                }
                //            }
                //            this.Enabled = false;
                //        }
                //        catch (Exception ex)
                //        {
                //            formMDI.infoError.ErrorString = "PC:182" + ex.Message;
                //        }
                //    }
                //}
                //if (cbxPrintAfterSave.Checked)
                //{
                //    if (spSettings.SettingsStatusCheck("Printer") == "Dot Matrix")
                //    {
                //        PrintForDotMatrix(decPurchaseMasterId);
                //    }
                //    else
                //    {
                //        Print(decPurchaseMasterId);
                //    }
                //}
                //Clear();
                return "Purchase Order Save successfully";
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "PI38:" + ex.Message;
            }


            return "GENERAL_FAILURE";
        }

        protected void applyDebitNote(PurchaseInvoiceVM input)
        {
            #region apply credit against supplier
            DebitNoteMasterSP spDebitnoteMaster = new DebitNoteMasterSP();
            DebitNoteMasterInfo infoDebitNoteMaster = new DebitNoteMasterInfo();
            DebitNoteDetailsSP spDebitNoteDetails = new DebitNoteDetailsSP();
            DebitNoteDetailsInfo infoDebitNoteDetails = new DebitNoteDetailsInfo();
            PaymentMasterInfo infoPaymentMaster = new PaymentMasterInfo();
            PaymentDetailsInfo infoPaymentDetails = new PaymentDetailsInfo();
            PaymentMasterSP spPaymentMaster = new PaymentMasterSP();
            PaymentDetailsSP spPaymentDetails = new PaymentDetailsSP();
            JournalMasterInfo infoJournalMaster = new JournalMasterInfo();
            JournalDetailsInfo infoJournalDetails = new JournalDetailsInfo();
            JournalMasterSP spJournalMaster = new JournalMasterSP();
            JournalDetailsSP spJournalDetails = new JournalDetailsSP();
            PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            HelperClasses helperClasses = new HelperClasses();
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            PostingsHelper PostingHelper = new PostingsHelper();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            DataTable dt = new DataTable();
            decimal decSelectedCurrencyRate = 1;
            decimal existingLedgerId = 0;
            bool isRowAffected = false;

            #region Working region

            bool createNewDebitNote = false;
            DataTable dtAdvancePayments = new DataTable();
            dtAdvancePayments.Columns.Add("voucherTypeId", typeof(string));
            dtAdvancePayments.Columns.Add("display", typeof(string));
            dtAdvancePayments.Columns.Add("voucherNo", typeof(string));
            dtAdvancePayments.Columns.Add("balance", typeof(decimal));
            dtAdvancePayments.Columns.Add("exchangeRateId", typeof(string));
            dtAdvancePayments.Columns.Add("invoiceNo", typeof(string));
            dtAdvancePayments.Columns.Add("AmountToApply", typeof(decimal));

            foreach (var advance in input.AdvancePayment)
            {
                DataRow dr = dtAdvancePayments.NewRow();
                    dr[0] = advance.VoucherTypeId.ToString();
                    dr[1] = advance.Display.ToString();
                    dr[2] = advance.VoucherNo.ToString();
                    dr[3] = advance.Balance.ToString();
                    dr[4] = advance.ExchangeRateId.ToString();
                    dr[5] = advance.InvoiceNo.ToString();
                    dr[6] = advance.Balance.ToString();
                    dtAdvancePayments.Rows.Add(dr);
            }

            if (dtAdvancePayments.Rows.Count > 0)
            {
                decimal totalAdvancePaymentToApply = (from t in dtAdvancePayments.AsEnumerable()
                                                      select t.Field<decimal>("AmountToApply")).Sum();
                decimal totalInvoice = Convert.ToDecimal(input.GrandTotal);

                foreach (var advance in input.AdvancePayment)
                {
                    createNewDebitNote = false;
                    decimal voucherTypeId = Convert.ToDecimal(advance.VoucherTypeId);
                    string voucherNo = advance.VoucherNo;
                    decimal balance = Convert.ToDecimal(advance.Balance);
                    decimal amountToApply = Convert.ToDecimal(advance.AmountToApply);
                    if (balance > amountToApply)
                    {
                        createNewDebitNote = true;
                    }

                    dt = spPartyBalance.PartyBalanceViewByVoucherNoAndVoucherType(voucherTypeId, voucherNo, DateTime.Now);
                    //decimal decPartyBalanceId = Convert.ToDecimal(dt.Rows[0]["PartyBalanceId"].ToString());

                    TotalAmount = balance > amountToApply ? amountToApply : balance;

                    #region Used code
                    if (voucherTypeId == 23)        //debit note
                    {
                        bool DebitMasterEdit = false;
                        infoDebitNoteMaster.DebitNoteMasterId = helperClasses.DebitNoteMasterId(voucherNo.ToString());
                        infoDebitNoteMaster = spDebitnoteMaster.DebitNoteMasterView(infoDebitNoteMaster.DebitNoteMasterId);

                        infoDebitNoteMaster.VoucherNo = infoDebitNoteMaster.VoucherNo;
                        infoDebitNoteMaster.InvoiceNo = infoDebitNoteMaster.InvoiceNo;
                        infoDebitNoteMaster.SuffixPrefixId = infoDebitNoteMaster.SuffixPrefixId;
                        infoDebitNoteMaster.Date = Convert.ToDateTime(input.PurchaseMasterInfo.Date);
                        infoDebitNoteMaster.Narration = input.PurchaseMasterInfo.Narration;
                        infoDebitNoteMaster.UserId = PublicVariables._decCurrentUserId;
                        infoDebitNoteMaster.VoucherTypeId = infoDebitNoteMaster.VoucherTypeId;
                        infoDebitNoteMaster.FinancialYearId = Convert.ToDecimal(PublicVariables._decCurrentFinancialYearId.ToString());
                        infoDebitNoteMaster.ExtraDate = DateTime.Now;
                        infoDebitNoteMaster.Extra1 = string.Empty;
                        infoDebitNoteMaster.Extra2 = string.Empty;
                        infoDebitNoteMaster.TotalAmount = TotalAmount;
                        DebitMasterEdit = true;
                        decimal noofRowsAffected = spDebitnoteMaster.DebitNoteMasterEdit(infoDebitNoteMaster);
                        if (noofRowsAffected > 0)
                        {
                            isRowAffected = true;
                        }
                        if (isRowAffected == true)
                        {
                            dt = spDebitNoteDetails.DebitNoteDetailsViewByMasterId(infoDebitNoteMaster.DebitNoteMasterId);
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                infoDebitNoteDetails.ChequeDate = Convert.ToDateTime(input.PurchaseMasterInfo.Date);
                                infoDebitNoteDetails.ChequeNo = string.Empty;
                                if (Convert.ToDecimal(dt.Rows[i]["debit"].ToString()) == 0)
                                {
                                    infoDebitNoteDetails.Credit = TotalAmount;
                                    infoDebitNoteDetails.Debit = 0;
                                    infoDebitNoteDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                    existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                }
                                else
                                {
                                    infoDebitNoteDetails.Credit = 0;
                                    infoDebitNoteDetails.Debit = TotalAmount;
                                    infoDebitNoteDetails.LedgerId = input.PurchaseMasterInfo.LedgerId;
                                    //existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                }
                                infoDebitNoteDetails.DebitNoteDetailsId = Convert.ToDecimal(dt.Rows[i]["DebitNoteDetailsId"].ToString());
                                infoDebitNoteDetails.DebitNoteMasterId = infoDebitNoteMaster.DebitNoteMasterId;
                                infoDebitNoteDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
                                infoDebitNoteDetails.Extra1 = string.Empty;
                                infoDebitNoteDetails.Extra2 = string.Empty;
                                infoDebitNoteDetails.ExtraDate = DateTime.Now;
                                DebitMasterEdit = false;
                                //------------------Currency conversion------------------//
                                decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoDebitNoteDetails.ExchangeRateId);
                                PostingHelper.DebitNoteDetailsAddOrEdit(infoDebitNoteMaster, infoDebitNoteDetails, DebitMasterEdit, decSelectedCurrencyRate, input.PurchaseMasterInfo.VoucherNo, decPurchaseInvoiceVoucherTypeId);
                            }
                        }

                        #endregion

                        #region Create new Debit note
                        if (createNewDebitNote)
                        {
                            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                            TransactionsGeneralFill obj = new TransactionsGeneralFill();
                            spDebitnoteMaster = new DebitNoteMasterSP();
                            string strNewDebitNoteVoucherNo = string.Empty;
                            string strNewDebitNoteInvoiceNo = string.Empty;

                            if (strNewDebitNoteVoucherNo == string.Empty)
                            {
                                strNewDebitNoteVoucherNo = "0"; //strMax;
                            }
                            //===================================================================================================================//

                            VoucherTypeSP spVoucherType = new VoucherTypeSP();
                            bool isAutomaticForDebitNote = spVoucherType.CheckMethodOfVoucherNumbering(23);
                            if (isAutomaticForDebitNote)
                            {
                                strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), input.PurchaseMasterInfo.Date, strDebitNoteMasterTableName);
                                if (Convert.ToDecimal(strNewDebitNoteVoucherNo) != spDebitnoteMaster.DebitNoteMasterGetMaxPlusOne(23))
                                {
                                    strNewDebitNoteVoucherNo = spDebitnoteMaster.DebitNoteMasterGetMax(23).ToString();
                                    strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), input.PurchaseMasterInfo.Date, strDebitNoteMasterTableName);
                                    if (spDebitnoteMaster.DebitNoteMasterGetMax(23).ToString() == "0")
                                    {
                                        strNewDebitNoteVoucherNo = "0";
                                        strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), input.PurchaseMasterInfo.Date, strDebitNoteMasterTableName);
                                    }
                                }
                                infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(23, input.PurchaseMasterInfo.Date);
                                strPrefix = infoSuffixPrefix.Prefix;
                                strSuffix = infoSuffixPrefix.Suffix;
                                strNewDebitNoteInvoiceNo = strPrefix + strNewDebitNoteVoucherNo + strSuffix;
                                //txtVoucherNo.Text = strInvoiceNo;
                                //txtVoucherNo.ReadOnly = true;
                            }
                            else
                            {
                                //txtVoucherNo.ReadOnly = false;
                                //txtVoucherNo.Text = string.Empty;
                                strNewDebitNoteVoucherNo = input.PurchaseMasterInfo.VoucherNo + "_Dr";
                                strNewDebitNoteInvoiceNo = strNewDebitNoteVoucherNo;
                            }
                            // ============================ Debit note master add =========================== //
                            infoDebitNoteMaster.VoucherNo = strNewDebitNoteVoucherNo;
                            infoDebitNoteMaster.InvoiceNo = strNewDebitNoteVoucherNo;
                            infoDebitNoteMaster.SuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                            infoDebitNoteMaster.Date = input.PurchaseMasterInfo.Date;
                            infoDebitNoteMaster.Narration = input.PurchaseMasterInfo.Narration;
                            infoDebitNoteMaster.UserId = PublicVariables._decCurrentUserId;
                            infoDebitNoteMaster.VoucherTypeId = 23;
                            infoDebitNoteMaster.FinancialYearId = Convert.ToDecimal(PublicVariables._decCurrentFinancialYearId.ToString());
                            infoDebitNoteMaster.Extra1 = string.Empty;
                            infoDebitNoteMaster.Extra2 = string.Empty;
                            infoDebitNoteMaster.TotalAmount = balance - amountToApply;
                            decimal decDebitNoteMasterId = spDebitnoteMaster.DebitNoteMasterAdd(infoDebitNoteMaster);

                            // ========================== DebitNote Details Add =============================== //
                            spDebitNoteDetails = new DebitNoteDetailsSP();
                            spLedgerPosting = new LedgerPostingSP();
                            infoLedgerPosting = new LedgerPostingInfo();
                            decimal decLedgerId = 0;
                            decimal decDebit = 0;
                            decimal decCredit = 0;
                            try
                            {
                                infoDebitNoteDetails.DebitNoteMasterId = decDebitNoteMasterId;
                                infoDebitNoteDetails.ExchangeRateId = ExchangeRateId;
                                infoDebitNoteDetails.ExtraDate = DateTime.Now;
                                infoDebitNoteDetails.Extra1 = string.Empty;
                                infoDebitNoteDetails.Extra2 = string.Empty;
                                infoDebitNoteDetails.LedgerId = input.PurchaseMasterInfo.LedgerId;
                                decLedgerId = infoDebitNoteDetails.LedgerId;
                                infoDebitNoteDetails.Debit = balance - amountToApply;
                                infoDebitNoteDetails.Credit = 0;
                                decDebit = infoDebitNoteDetails.Debit * decSelectedCurrencyRate;
                                decCredit = infoDebitNoteDetails.Credit * decSelectedCurrencyRate;
                                infoDebitNoteDetails.ChequeNo = string.Empty;
                                infoDebitNoteDetails.ChequeDate = input.PurchaseMasterInfo.Date;
                                //-------------------- debit leg ---------------------------- //
                                decimal decDebitDetailsId = spDebitNoteDetails.DebitNoteDetailsAdd(infoDebitNoteDetails);

                                infoDebitNoteDetails.LedgerId = 1;
                                decLedgerId = infoDebitNoteDetails.LedgerId;
                                infoDebitNoteDetails.Debit = 0;
                                infoDebitNoteDetails.Credit = balance - amountToApply;
                                decDebit = infoDebitNoteDetails.Debit * decSelectedCurrencyRate;
                                decCredit = infoDebitNoteDetails.Credit * decSelectedCurrencyRate;

                                // -------------------- credit leg --------------------------- //
                                decimal decCreditDetailsId = spDebitNoteDetails.DebitNoteDetailsAdd(infoDebitNoteDetails);

                                // -------------------------- party balance add --------------------------- //
                                spPartyBalance = new PartyBalanceSP();
                                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                                InfopartyBalance.CreditPeriod = 0;//
                                InfopartyBalance.Date = input.PurchaseMasterInfo.Date;
                                InfopartyBalance.LedgerId = input.PurchaseMasterInfo.LedgerId;
                                InfopartyBalance.ReferenceType = "OnAccount";
                                InfopartyBalance.AgainstInvoiceNo = "0";
                                InfopartyBalance.AgainstVoucherNo = "0";
                                InfopartyBalance.AgainstVoucherTypeId = 0;
                                InfopartyBalance.VoucherTypeId = 23;
                                InfopartyBalance.InvoiceNo = strNewDebitNoteVoucherNo;
                                InfopartyBalance.VoucherNo = strNewDebitNoteVoucherNo;
                                InfopartyBalance.Credit = 0;
                                InfopartyBalance.Debit = balance - amountToApply;
                                InfopartyBalance.ExchangeRateId = 1;
                                InfopartyBalance.Extra1 = string.Empty;
                                InfopartyBalance.Extra2 = string.Empty;
                                InfopartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                                PostingHelper.PartyBalanceAdd(InfopartyBalance);

                                // =============================== ledger posting ==================================== //
                                infoLedgerPosting.ChequeDate = input.PurchaseMasterInfo.Date;
                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.Credit = 0;
                                infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                                infoLedgerPosting.Debit = balance - amountToApply;
                                infoLedgerPosting.DetailsId = decCreditDetailsId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.InvoiceNo = strNewDebitNoteVoucherNo;
                                infoLedgerPosting.LedgerId = input.PurchaseMasterInfo.LedgerId;
                                infoLedgerPosting.VoucherNo = strNewDebitNoteVoucherNo;
                                infoLedgerPosting.VoucherTypeId = 23;
                                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                                // --------------- debit leg -------------------------------------------------- //
                                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                                // --------------- credit leg --------------------------------------------------- //
                                infoLedgerPosting.Credit = balance - amountToApply;
                                infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                                infoLedgerPosting.Debit = 0;
                                infoLedgerPosting.DetailsId = decDebitDetailsId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.LedgerId = existingLedgerId;
                                infoLedgerPosting.VoucherNo = strNewDebitNoteVoucherNo;
                                infoLedgerPosting.VoucherTypeId = 23;
                                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                            }
                            catch (Exception ex) { }
                            createNewDebitNote = false;
                            #endregion
                        }
                    }
                    #region Used code
                    if (voucherTypeId == 4) // Payment voucher
                    {
                        bool PaymentMasterEdit = false;
                        comStr = string.Format("SELECT paymentMasterId FROM tbl_PaymentMaster WHERE " +
                            "voucherNo = '{0}' ", voucherNo.ToString());
                        infoPaymentMaster.PaymentMasterId = helperClasses.GetMasterId(comStr);
                        infoPaymentMaster = spPaymentMaster.PaymentMasterView(infoPaymentMaster.PaymentMasterId);

                        infoPaymentMaster.VoucherNo = infoPaymentMaster.VoucherNo;
                        infoPaymentMaster.InvoiceNo = infoPaymentMaster.InvoiceNo;
                        infoPaymentMaster.SuffixPrefixId = infoPaymentMaster.SuffixPrefixId;
                        infoPaymentMaster.Date = input.PurchaseMasterInfo.Date;
                        infoPaymentMaster.Narration = input.PurchaseMasterInfo.Narration;
                        infoPaymentMaster.UserId = PublicVariables._decCurrentUserId;
                        infoPaymentMaster.VoucherTypeId = infoPaymentMaster.VoucherTypeId;
                        infoPaymentMaster.FinancialYearId = Convert.ToDecimal(PublicVariables._decCurrentFinancialYearId.ToString());
                        infoPaymentMaster.ExtraDate = DateTime.Now;
                        infoPaymentMaster.Extra1 = string.Empty;
                        infoPaymentMaster.Extra2 = string.Empty;
                        infoPaymentMaster.TotalAmount = TotalAmount;
                        PaymentMasterEdit = true;
                        //bool isRowAffected = PostingHelper.ReceiptMasterEdit(infoPaymentMaster, decSelectedCurrencyRate);
                        isRowAffected = PostingHelper.PaymentMasterEdit(infoPaymentMaster, decSelectedCurrencyRate);

                        if (isRowAffected == true)
                        {
                            dt = spPaymentDetails.PaymentDetailsViewByMasterId(infoPaymentMaster.PaymentMasterId);
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                infoPaymentDetails.Amount = amountToApply;
                                infoPaymentDetails.ChequeDate = input.PurchaseMasterInfo.Date;
                                infoPaymentDetails.ChequeNo = string.Empty;
                                infoPaymentDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                infoPaymentDetails.PaymentDetailsId = Convert.ToDecimal(dt.Rows[i]["ReceiptDetailsId"].ToString());
                                infoPaymentDetails.PaymentMasterId = infoPaymentMaster.PaymentMasterId;
                                infoPaymentDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
                                infoPaymentDetails.Extra1 = string.Empty;
                                infoPaymentDetails.Extra2 = string.Empty;
                                infoPaymentDetails.ExtraDate = DateTime.Now;
                                PaymentMasterEdit = false;
                                //------------------Currency conversion------------------//
                                decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoPaymentDetails.ExchangeRateId);
                                PostingHelper.PaymentDetailsEdit(infoPaymentMaster, infoPaymentDetails, PaymentMasterEdit, decSelectedCurrencyRate, input.PurchaseMasterInfo.VoucherNo, decPurchaseInvoiceVoucherTypeId);
                            }
                        }
                    }
                    if (voucherTypeId == 6) // Journal voucher
                    {
                        bool JournalMasterEdit = false;
                        comStr = string.Format("SELECT journalMasterId FROM tbl_JournalMaster WHERE " +
                        "voucherNo = '{0}' ", voucherNo.ToString());
                        infoJournalMaster.JournalMasterId = helperClasses.GetMasterId(comStr);
                        infoJournalMaster = spJournalMaster.JournalMasterView(infoJournalMaster.JournalMasterId);

                        infoJournalMaster.Date = input.PurchaseMasterInfo.Date;
                        infoJournalMaster.Narration = input.PurchaseMasterInfo.Narration;
                        infoJournalMaster.UserId = PublicVariables._decCurrentUserId;
                        infoJournalMaster.FinancialYearId = Convert.ToDecimal(PublicVariables._decCurrentFinancialYearId.ToString());
                        infoJournalMaster.ExtraDate = DateTime.Now;
                        infoJournalMaster.TotalAmount = TotalAmount;
                        JournalMasterEdit = true;
                        bool isRowEdited = PostingHelper.JournalMasterEdit(infoJournalMaster, decSelectedCurrencyRate);

                        if (isRowEdited == true)
                        {
                            dt = new DataTable();
                            dt = spJournalDetails.JournalDetailsViewByMasterId(infoJournalMaster.JournalMasterId);
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (Convert.ToDecimal(dt.Rows[i]["credit"].ToString()) == 0)
                                {
                                    infoJournalDetails.Credit = 0;
                                    infoJournalDetails.Debit = TotalAmount;
                                    infoJournalDetails.LedgerId = input.PurchaseMasterInfo.LedgerId;
                                }
                                else
                                {
                                    infoJournalDetails.Credit = TotalAmount;
                                    infoJournalDetails.Debit = 0;
                                    infoJournalDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                    existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                }

                                infoJournalDetails.ChequeDate = input.PurchaseMasterInfo.Date;
                                infoJournalDetails.ChequeNo = string.Empty;
                                infoJournalDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                infoJournalDetails.JournalMasterId = infoJournalMaster.JournalMasterId;
                                infoJournalDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
                                infoJournalDetails.Extra1 = string.Empty;
                                infoJournalDetails.Extra2 = string.Empty;
                                infoJournalDetails.Memo = dt.Rows[i]["Memo"].ToString();
                                infoJournalDetails.ExtraDate = DateTime.Now;
                                JournalMasterEdit = false;
                                //------------------Currency conversion------------------//
                                decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoJournalDetails.ExchangeRateId);
                                PostingHelper.JournalDetailsEdit(infoJournalMaster, infoJournalDetails, JournalMasterEdit, decSelectedCurrencyRate, input.PurchaseMasterInfo.VoucherNo, decPurchaseInvoiceVoucherTypeId);
                            }
                        }
                    }
                    #endregion

                    #region Create new credit note
                    if (createNewDebitNote)
                    {
                        SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                        SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                        TransactionsGeneralFill obj = new TransactionsGeneralFill();
                        DebitNoteMasterSP spDebitNoteMaster = new DebitNoteMasterSP();
                        string strNewDebitNoteVoucherNo = string.Empty;
                        string strNewDebitNoteInvoiceNo = string.Empty;

                        if (strNewDebitNoteVoucherNo == string.Empty)
                        {
                            strNewDebitNoteVoucherNo = "0"; //strMax;
                        }
                        //===================================================================================================================//

                        VoucherTypeSP spVoucherType = new VoucherTypeSP();
                        bool isAutomaticForDebitNote = spVoucherType.CheckMethodOfVoucherNumbering(23);
                        if (isAutomaticForDebitNote)
                        {
                            strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), input.PurchaseMasterInfo.Date, strDebitNoteMasterTableName);
                            if (Convert.ToDecimal(strNewDebitNoteVoucherNo) != spDebitnoteMaster.DebitNoteMasterGetMaxPlusOne(23))
                            {
                                strNewDebitNoteVoucherNo = spDebitnoteMaster.DebitNoteMasterGetMax(23).ToString();
                                strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), input.PurchaseMasterInfo.Date, strDebitNoteMasterTableName);
                                if (spDebitnoteMaster.DebitNoteMasterGetMax(23).ToString() == "0")
                                {
                                    strNewDebitNoteVoucherNo = "0";
                                    strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), input.PurchaseMasterInfo.Date, strDebitNoteMasterTableName);
                                }
                            }
                            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(23, input.PurchaseMasterInfo.Date);
                            strPrefix = infoSuffixPrefix.Prefix;
                            strSuffix = infoSuffixPrefix.Suffix;
                            strNewDebitNoteInvoiceNo = strPrefix + strNewDebitNoteVoucherNo + strSuffix;
                        }
                        else
                        {
                            strNewDebitNoteVoucherNo = input.PurchaseMasterInfo.VoucherNo + "_Dr";
                            strNewDebitNoteInvoiceNo = strNewDebitNoteVoucherNo;
                        }
                        // ============================ Debit note master add =========================== //
                        infoDebitNoteMaster.VoucherNo = strNewDebitNoteVoucherNo;
                        infoDebitNoteMaster.InvoiceNo = strNewDebitNoteVoucherNo;
                        infoDebitNoteMaster.SuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                        infoDebitNoteMaster.Date = input.PurchaseMasterInfo.Date;
                        infoDebitNoteMaster.Narration = input.PurchaseMasterInfo.Narration;
                        infoDebitNoteMaster.UserId = PublicVariables._decCurrentUserId;
                        infoDebitNoteMaster.VoucherTypeId = 23;
                        infoDebitNoteMaster.FinancialYearId = Convert.ToDecimal(PublicVariables._decCurrentFinancialYearId.ToString());
                        infoDebitNoteMaster.Extra1 = string.Empty;
                        infoDebitNoteMaster.Extra2 = string.Empty;
                        infoDebitNoteMaster.TotalAmount = balance - amountToApply;
                        decimal decDebitNoteMasterId = spDebitnoteMaster.DebitNoteMasterAdd(infoDebitNoteMaster);

                        // ========================== DebitNote Details Add =============================== //
                        spDebitNoteDetails = new DebitNoteDetailsSP();
                        spLedgerPosting = new LedgerPostingSP();
                        infoLedgerPosting = new LedgerPostingInfo();
                        decimal decLedgerId = 0;
                        decimal decDebit = 0;
                        decimal decCredit = 0;
                        try
                        {
                            infoDebitNoteDetails.DebitNoteMasterId = decDebitNoteMasterId;
                            infoDebitNoteDetails.ExchangeRateId = ExchangeRateId;
                            infoDebitNoteDetails.ExtraDate = DateTime.Now;
                            infoDebitNoteDetails.Extra1 = string.Empty;
                            infoDebitNoteDetails.Extra2 = string.Empty;
                            infoDebitNoteDetails.LedgerId = input.PurchaseMasterInfo.LedgerId;
                            decLedgerId = infoDebitNoteDetails.LedgerId;
                            infoDebitNoteDetails.Debit = 0;
                            infoDebitNoteDetails.Credit = balance - amountToApply;
                            decDebit = infoDebitNoteDetails.Debit * decSelectedCurrencyRate;
                            decCredit = infoDebitNoteDetails.Credit * decSelectedCurrencyRate;
                            infoDebitNoteDetails.ChequeNo = string.Empty;
                            infoDebitNoteDetails.ChequeDate = input.PurchaseMasterInfo.Date;
                            //-------------------- credit leg ---------------------------- //
                            decimal decCreditDetailsId = spDebitNoteDetails.DebitNoteDetailsAdd(infoDebitNoteDetails);

                            infoDebitNoteDetails.LedgerId = 1;
                            decLedgerId = infoDebitNoteDetails.LedgerId;
                            infoDebitNoteDetails.Debit = balance - amountToApply;
                            infoDebitNoteDetails.Credit = 0;
                            decDebit = infoDebitNoteDetails.Debit * decSelectedCurrencyRate;
                            decCredit = infoDebitNoteDetails.Credit * decSelectedCurrencyRate;

                            // -------------------- debit leg --------------------------- //
                            decimal decDebitDetailsId = spDebitNoteDetails.DebitNoteDetailsAdd(infoDebitNoteDetails);

                            // -------------------------- party balance add --------------------------- //
                            spPartyBalance = new PartyBalanceSP();
                            PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                            InfopartyBalance.CreditPeriod = 0;//
                            InfopartyBalance.Date = input.PurchaseMasterInfo.Date;
                            InfopartyBalance.LedgerId = input.PurchaseMasterInfo.LedgerId;
                            InfopartyBalance.ReferenceType = "OnAccount";
                            InfopartyBalance.AgainstInvoiceNo = "0";
                            InfopartyBalance.AgainstVoucherNo = "0";
                            InfopartyBalance.AgainstVoucherTypeId = 0;
                            InfopartyBalance.VoucherTypeId = 23;
                            InfopartyBalance.InvoiceNo = strNewDebitNoteVoucherNo;
                            InfopartyBalance.VoucherNo = strNewDebitNoteVoucherNo;
                            InfopartyBalance.Credit = balance - amountToApply;
                            InfopartyBalance.Debit = 0;
                            InfopartyBalance.ExchangeRateId = 1;
                            InfopartyBalance.Extra1 = string.Empty;
                            InfopartyBalance.Extra2 = string.Empty;
                            InfopartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            PostingHelper.PartyBalanceAdd(InfopartyBalance);

                            // =============================== ledger posting ==================================== //
                            infoLedgerPosting.ChequeDate = input.PurchaseMasterInfo.Date;
                            infoLedgerPosting.ChequeNo = string.Empty;
                            infoLedgerPosting.Credit = 0;
                            infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                            infoLedgerPosting.Debit = balance - amountToApply;
                            infoLedgerPosting.DetailsId = decCreditDetailsId;
                            infoLedgerPosting.Extra1 = string.Empty;
                            infoLedgerPosting.Extra2 = string.Empty;
                            infoLedgerPosting.ExtraDate = DateTime.Now;
                            infoLedgerPosting.InvoiceNo = strNewDebitNoteVoucherNo;
                            infoLedgerPosting.LedgerId = input.PurchaseMasterInfo.LedgerId;
                            infoLedgerPosting.VoucherNo = strNewDebitNoteVoucherNo;
                            infoLedgerPosting.VoucherTypeId = 23;
                            infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                            // --------------- debit leg -------------------------------------------------- //
                            //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                            // --------------- credit leg --------------------------------------------------- //
                            infoLedgerPosting.Credit = balance - amountToApply;
                            infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                            infoLedgerPosting.Debit = 0;
                            infoLedgerPosting.DetailsId = decDebitDetailsId;
                            infoLedgerPosting.Extra1 = string.Empty;
                            infoLedgerPosting.LedgerId = existingLedgerId;
                            infoLedgerPosting.VoucherNo = strNewDebitNoteVoucherNo;
                            infoLedgerPosting.VoucherTypeId = 23;
                            //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                        }
                        catch (Exception ex)
                        {
                            //formMDI.infoError.ErrorString = "PI135:" + ex.Message;
                        }
                        #endregion
                    }
                }
            }
            #endregion
            #endregion
        }

        [HttpGet]
        public HttpResponseMessage GetPurchaseInvoicetLineItem(decimal receiptMasterId)
        {
            dynamic response = new ExpandoObject();
            var dtblDetails = new MaterialReceiptDetailsSP().MaterialReceiptDetailsViewByMaterialReceiptMasterIdWithRemainingByNotInCurrPI
                                    (receiptMasterId, 0, decPurchaseInvoiceVoucherTypeId);
            response.Details = dtblDetails;
            //response.ProductDetails = new ProductSP().ProductView(Convert.ToDecimal(dtblDetails.Rows[0].ItemArray[2]));
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public string GetAutoFormNo()
        {
            return AutoGenerateFormNo();
        }
        private string AutoGenerateFormNo()
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();

            strVoucherNo = "0";
            strVoucherNo = spPurchaseMaster.PurchaseMasterVoucherMax(decPurchaseInvoiceVoucherTypeId).ToString();
            if (strVoucherNo == string.Empty)
            {
                strVoucherNo = "0";
            }
            strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPurchaseInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strTableName);
            if (Convert.ToDecimal(strVoucherNo) != (spPurchaseMaster.PurchaseMasterVoucherMax(decPurchaseInvoiceVoucherTypeId)))
            {
                strVoucherNo = spPurchaseMaster.PurchaseMasterVoucherMax(decPurchaseInvoiceVoucherTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPurchaseInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strTableName);
                if (spPurchaseMaster.PurchaseMasterVoucherMax(decPurchaseInvoiceVoucherTypeId) == 0)
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPurchaseInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strTableName);
                }
            }

            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decPurchaseInvoiceVoucherTypeId, DateTime.Now);
            strPrefix = infoSuffixPrefix.Prefix;
            strSuffix = infoSuffixPrefix.Suffix;
            var decPurchaseInvoiceSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
            var FormNo = strPrefix + strVoucherNo + strSuffix;

            return FormNo;
        }
    }
}
