﻿using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RejectionInController : ApiController
    {
        public RejectionInController()
        {

        }

        [HttpGet]
        public HttpResponseMessage LookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var customers = transactionGeneralFillObj.CashOrPartyUnderSundryDrComboFill();
            var salesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();
            var pricingLevels = transactionGeneralFillObj.PricingLevelViewAll();
            var voucherTypes = transactionGeneralFillObj.VoucherTypeComboFill("Delivery Note", false);
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            dynamic response = new ExpandoObject();
            response.Customers = customers;
            response.SalesMen = salesMen;
            response.PricingLevels = pricingLevels;
            response.Currencies = currencies;
            response.VoucherTypes = voucherTypes;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetDeliveryNoteNo(decimal customerId,decimal voucherType)
        {
            DeliveryNoteMasterSP spdeliverynotemaster = new DeliveryNoteMasterSP();
            var deliveryNotesNo = spdeliverynotemaster.DeliveryNoteNoCorrespondingToLedger(customerId, 0, voucherType);
            return Request.CreateResponse(HttpStatusCode.OK, deliveryNotesNo);
        }

        [HttpGet]
        public List<RejectionInLineItems> FillGridCorrespondingToDeliveryNoteNo(decimal deliveryNoteNo)
        {
            DeliveryNoteDetailsSP SPDeliveryNoteDetails = new DeliveryNoteDetailsSP();
            List<RejectionInLineItems> response = new List<RejectionInLineItems>();
            try
            {               

                var notes=  SPDeliveryNoteDetails.DeliveryNoteDetailsViewByDeliveryNoteMasterIdWithPending(deliveryNoteNo, 0);
                for(int i= 0;i < notes.Rows.Count;i++)
                {
                    var note = notes.Rows[i].ItemArray;
                    var prod = new ProductSP().ProductView(Convert.ToDecimal(note[2]));
                    response.Add(new RejectionInLineItems {
                        Amount=Convert.ToDecimal(note[10]),
                        Barcode=note[11].ToString(),
                        Batch=note[9].ToString(),
                        ProductCode= prod.ProductCode,
                        ProductName=prod.ProductName,
                        Quantity=Convert.ToDecimal(note[3]),
                        Rack= new RackSP().RackView(Convert.ToDecimal(note[8])).RackName,
                        Rate=Convert.ToDecimal(note[4]),
                        Store=new GodownSP().GodownView(Convert.ToDecimal(note[7])).GodownName,
                        Unit = new UnitSP().UnitView(Convert.ToDecimal(note[5])).UnitName
                    });
                }
                
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RI12:" + ex.Message;
            }
            return response;
        }
    }
}
