﻿using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PendingSalesController : ApiController
    {
        public DataSet GetPendingSales()
        {
            DataSet ds = new DataSet();
            DBMatConnection connection = new DBMatConnection();
            try
            {
                ds = connection.getDataSet("PendingSalesRegisterGridfillAll");
            }
            catch (Exception EX)
            {

            }
            return ds;
        }
    }
}
