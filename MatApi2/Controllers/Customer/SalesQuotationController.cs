﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials;
using System.Data;
using MatApi.Models.Customer;
using System.Dynamic;
using MATFinancials.DAL;
using MATClassLibrary.Classes.General;

namespace MatApi.Controllers.Company.Masters
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SalesQuotationController : ApiController
    {
        RouteSP routeSp;
        public SalesQuotationController()
        {
            routeSp = new RouteSP();
        }
        
        public HttpResponseMessage GetTaxAmount(string taxId, string amount)
        {

            decimal taxAmount = 0;
            TaxSP SpTax = new TaxSP();
            var amt = Convert.ToDecimal(amount);
            TaxInfo InfoTaxObj = SpTax.TaxView(Convert.ToDecimal(taxId));
            taxAmount = Math.Round(((amt * InfoTaxObj.Rate) / (100)), PublicVariables._inNoOfDecimalPlaces);
            return Request.CreateResponse(HttpStatusCode.OK, taxAmount);
        }

        [HttpGet]
        public HttpResponseMessage GetLookups()
        {
           
            TransactionsGeneralFill trans = new TransactionsGeneralFill();
            var customers= trans.CashOrPartyUnderSundryDrComboFill();
            var currencies = trans.CurrencyComboByDate(DateTime.Now);
            var salesMen = trans.SalesmanViewAllForComboFill();
            var pricingLevel = trans.PricingLevelViewAll();
            var units = new UnitSP().UnitViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks = new RackSP().RackViewAll();
            var batches = new BatchSP().BatchViewAll();
            //var tax = new TaxSP().TaxView(2);
            var tax = new TaxSP().TaxViewAll();

            dynamic response = new ExpandoObject();
            response.Customers = customers;
            response.Currencies = currencies;
            response.SalesMen = salesMen;
            response.PricingLevels = pricingLevel;
            response.Units = units;
            response.Stores = stores;
            response.Racks = racks;
            response.Batches = batches;
            response.Tax = tax;

            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }

        [HttpPost]
        public decimal SaveSalesQuotation(SalesQuotationModel obj)
        {
            return SaveFunction(obj);
        }

        private decimal getQuantityInStock(decimal productId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format(" SELECT  convert(decimal(18, 2), (ISNULL(SUM(s.inwardQty), 0) - ISNULL(SUM(s.outwardQty), 0))) AS Currentstock " +
                " FROM tbl_StockPosting s inner join tbl_product p on p.productId = s.productId " +
                " INNER JOIN tbl_Batch b on s.batchId = b.batchId " +
                " WHERE p.productId = '{0}' AND s.date <= '{1}' ", productId, MATFinancials.PublicVariables._dtToDate);
            return Convert.ToDecimal(conn.getSingleValue(queryStr));
        }

        public decimal SaveFunction(SalesQuotationModel obj)
        {
            SalesQuotationDetailsSP SpSalesQuotationDetails = new SalesQuotationDetailsSP();
            SalesQuotationMasterSP SpSalesQuotationMaster = new SalesQuotationMasterSP();
            SalesQuotationMasterInfo infoSalesQuotationMaster = new SalesQuotationMasterInfo();
            SalesQuotationDetailsInfo infoSalesQuotationDetails = new SalesQuotationDetailsInfo();
            SettingsSP spSettings = new SettingsSP();
            ProductSP spProduct = new ProductSP();
            ProductInfo infoproduct = new ProductInfo();
            decimal returnValue = 0;
            //check if invoice number entered already exists
            string query = string.Format("SELECT voucherNo FROM tbl_SalesQuotationMaster WHERE voucherNo='{0}' AND voucherTypeId=10031", obj.Master.VoucherNo);
            if(new DBMatConnection().IsFormNumberExist(query))
            {
                returnValue = -1;
                return returnValue;
            }
            else
            {
                try
                {
                    string voucherNo = obj.Master.InvoiceNo;
                    infoSalesQuotationMaster.Date = Convert.ToDateTime(obj.Master.Date);
                    infoSalesQuotationMaster.PricinglevelId = Convert.ToDecimal(obj.Master.PricinglevelId);
                    infoSalesQuotationMaster.LedgerId = Convert.ToDecimal(obj.Master.LedgerId);
                    infoSalesQuotationMaster.EmployeeId = Convert.ToDecimal(obj.Master.EmployeeId);
                    //if (false)//(isAutomatic)
                    //{
                    //    //infoSalesQuotationMaster.SuffixPrefixId = decSalesQuotationPreffixSuffixId;
                    //    //infoSalesQuotationMaster.VoucherNo = strSalesQuotationNo;
                    //}
                    //else
                    //{
                    //SpSalesQuotationMaster.VoucherNoMax(10031);
                    infoSalesQuotationMaster.SuffixPrefixId = 0;
                    infoSalesQuotationMaster.VoucherNo = voucherNo;//SpSalesQuotationMaster.VoucherNoMax(decsalesQuotationTypeId);
                                                                              //}
                    infoSalesQuotationMaster.VoucherTypeId = 10031; //decsalesQuotationTypeId;
                    infoSalesQuotationMaster.InvoiceNo = voucherNo;
                    infoSalesQuotationMaster.FinancialYearId = 1;//PublicVariables._decCurrentFinancialYearId;
                    infoSalesQuotationMaster.TotalAmount = Convert.ToDecimal(obj.Master.TotalAmount);
                    infoSalesQuotationMaster.Narration = obj.Master.Narration;
                    infoSalesQuotationMaster.ExchangeRateId = Convert.ToDecimal(obj.Master.ExchangeRateId);
                    infoSalesQuotationMaster.Extra1 = string.Empty;
                    infoSalesQuotationMaster.Extra2 = string.Empty;
                    infoSalesQuotationMaster.taxAmount = Convert.ToDecimal(obj.Master.taxAmount);
                    infoSalesQuotationMaster.Approved = "Pending";
                    infoSalesQuotationMaster.Narration = obj.Master.Narration;
                    infoSalesQuotationMaster.userId = obj.Master.userId;

                    decimal decSalesQuotationmasterIdentity = SpSalesQuotationMaster.SalesQuotationMasterAdd(infoSalesQuotationMaster);
                    if (decSalesQuotationmasterIdentity > 0)
                    {
                        returnValue = 1;
                    }
                    var lineItemList = obj.LineItems;
                    if (lineItemList.Count > 0)
                    {
                        foreach (var lineItemObj in lineItemList)
                        {
                            var unitId = new ProductSP().ProductView(lineItemObj.ProductId).UnitId;
                            infoSalesQuotationDetails.QuotationMasterId = decSalesQuotationmasterIdentity;
                            infoSalesQuotationDetails.ProductId = lineItemObj.ProductId;
                            infoSalesQuotationDetails.Qty = Convert.ToDecimal(lineItemObj.Qty);
                            infoSalesQuotationDetails.UnitId = Convert.ToDecimal(unitId);
                            decimal unitConversion = SpSalesQuotationDetails.UnitconversionIdViewByUnitIdAndProductId(Convert.ToDecimal(unitId), lineItemObj.ProductId);
                            infoSalesQuotationDetails.UnitConversionId = unitConversion;
                            if (lineItemObj.BatchId > 0)
                            {
                                infoSalesQuotationDetails.BatchId = Convert.ToDecimal(lineItemObj.BatchId);
                            }
                            else
                            {
                                infoSalesQuotationDetails.BatchId = 0;
                            }
                            infoSalesQuotationDetails.Rate = Convert.ToDecimal(lineItemObj.Rate);
                            infoSalesQuotationDetails.Amount = Convert.ToDecimal(lineItemObj.Amount);
                            infoSalesQuotationDetails.taxAmount = Convert.ToDecimal(lineItemObj.taxAmount);
                            infoSalesQuotationDetails.taxId = Convert.ToDecimal(lineItemObj.taxId);
                            infoSalesQuotationDetails.Slno = Convert.ToInt32(lineItemObj.Slno);
                            infoSalesQuotationDetails.Extra1 = lineItemObj.Extra1;
                            infoSalesQuotationDetails.Extra2 = string.Empty;
                            //if (dgvProduct.Rows[inI].Cells["dgvCmbProject"].Value != null && dgvProduct.Rows[inI].Cells["dgvCmbProject"].Value.ToString() != string.Empty)
                            //{
                            //infoSalesQuotationDetails.Project = Convert.ToInt32(dgvProduct.Rows[inI].Cells["dgvCmbProject"].Value.ToString());
                            //}
                            //else
                            //{
                            infoSalesQuotationDetails.Project = 0;
                            //}
                            //if (dgvProduct.Rows[inI].Cells["dgvCmbCategory"].Value != null && dgvProduct.Rows[inI].Cells["dgvCmbCategory"].Value.ToString() != string.Empty)
                            //{
                            //infoSalesQuotationDetails.Category = 1;// Convert.ToInt32(dgvProduct.Rows[inI].Cells["dgvCmbCategory"].Value.ToString());
                            // }
                            //else
                            //{
                            infoSalesQuotationDetails.Category = 0;
                            // }
                            //if (dgvProduct.Rows[inI].Cells["dgvtxtDescription"].Value != null && dgvProduct.Rows[inI].Cells["dgvtxtDescription"].Value.ToString() != string.Empty)
                            //{
                            infoSalesQuotationDetails.itemDescription = "";//dgvProduct.Rows[inI].Cells["dgvtxtDescription"].Value.ToString();
                                                                           //}
                            if (SpSalesQuotationDetails.SalesQuotationDetailsAdd(infoSalesQuotationDetails) > 0)
                            {
                                returnValue = 1;
                            }
                            else
                            {
                                returnValue = 0;
                            }
                        }
                        var currrentUser = new UserSP().UserView(infoSalesQuotationMaster.userId);
                        string currrentUserLocationId = currrentUser.StoreId;
                        string ordersLocationId = obj.LineItems.First().Extra1;
                        if (currrentUserLocationId != ordersLocationId)    //means order was placed in a location different from the user
                        {                                               //location so send mail to the manager of the location the order
                                                                        //was placed for
                            try
                            {
                                string qry = "SELECT * FROM tbl_Users WHERE roleId='7' AND storeId='" + ordersLocationId + "'";
                                var managerInfo = new DBMatConnection().customSelect(qry).Rows[0].ItemArray;

                                string body = "Dear {manager name},\n";
                                body += "An order was raised for your location from another location.";
                                string subject = "Order Confirmation Request";
                                Mailer.SendMail("akindeji.o@magnetgroupng.com", managerInfo[13].ToString(), subject, body);
                            }
                            catch(Exception e)
                            {

                            }
                        }
                    }
                }
                catch (Exception ex)
                {
                    //formMDI.infoError.ErrorString = "SQ32:" + ex.Message;
                }
            }
            
            return returnValue;
        }

        [HttpGet]
        public HttpResponseMessage GetSalesQuotationMasterDetails(int id)
        {
            SalesQuotationMasterSP sqSalesQuotationMaster = new SalesQuotationMasterSP();
            var masterResult = sqSalesQuotationMaster.SalesQuotationMasterView(id);

            return Request.CreateResponse(HttpStatusCode.OK, (object)masterResult);
        }

        [HttpGet]
        public HttpResponseMessage GetSalesQuotationDetails(int id)
        {
            SalesQuotationDetailsSP sqSalesQuotationDetails = new SalesQuotationDetailsSP();
            var masterResult = sqSalesQuotationDetails.SalesQuotationDetailsViewByMasterId(id);

            return Request.CreateResponse(HttpStatusCode.OK, (object)masterResult);
        }

        [HttpGet]
        public HttpResponseMessage GetSalesQuotationForConfirmation(decimal salesQuotationMasterId)
        {
            SalesQuotationMasterSP sqSalesQuotationMaster = new SalesQuotationMasterSP();
            var masterResult = sqSalesQuotationMaster.SalesQuotationMasterView(salesQuotationMasterId);

            SalesQuotationDetailsSP sqSalesQuotationDetails = new SalesQuotationDetailsSP();
            var detailsResult = sqSalesQuotationDetails.SalesQuotationDetailsViewByMasterId(salesQuotationMasterId);

            SalesQuotationMasterInfoCustom master = new SalesQuotationMasterInfoCustom
            {
                Approved=masterResult.Approved,
                Date=masterResult.Date,
                EmployeeId=masterResult.EmployeeId,
                ExchangeRateId=masterResult.ExchangeRateId,
                InvoiceNo=masterResult.InvoiceNo,
                LedgerId=masterResult.LedgerId,
                CustomerName=new AccountLedgerSP().AccountLedgerView(masterResult.LedgerId).LedgerName,
                QuotationMasterId=masterResult.QuotationMasterId,
                TaxAmount=masterResult.taxAmount,
                VoucherTypeId=masterResult.VoucherTypeId,
                VoucherType=new VoucherTypeSP().VoucherTypeView(masterResult.VoucherTypeId).VoucherTypeName,
                TotalAmount=masterResult.TotalAmount,
                UserId=masterResult.userId,
                User=new UserSP().UserView(masterResult.userId).FirstName+" "+ new UserSP().UserView(masterResult.userId).LastName,
                VoucherNo=masterResult.VoucherNo
            };
            List<SalesQuotationDetailsInfoCustom> details = new List<SalesQuotationDetailsInfoCustom>();
            foreach(DataRow row in detailsResult.Rows)
            {
                var prod = new ProductSP().ProductView(Convert.ToDecimal(row[4].ToString()));
                details.Add(new SalesQuotationDetailsInfoCustom
                {
                    amount=Convert.ToDecimal(row[11].ToString()),
                    batchId= Convert.ToDecimal(row[12].ToString()),
                    Category=0,
                    extra1=new GodownSP().GodownView(Convert.ToDecimal(row[16].ToString())).GodownId.ToString(),
                    storeName = new GodownSP().GodownView(Convert.ToDecimal(row[16].ToString())).GodownName,
                    extra2 ="",
                    itemDescription= row[13].ToString(),
                    product=prod.ProductName,
                    productcode=prod.ProductCode,
                    barcode=prod.barcode,
                    productId= Convert.ToDecimal(row[4].ToString()),
                    Project=0,
                    qty= Convert.ToDecimal(row[8].ToString()),
                    quotationDetailsId= Convert.ToDecimal(row[0].ToString()),
                    rate= Convert.ToDecimal(row[9].ToString()),
                    slno=Convert.ToInt32(row[1].ToString()),
                    tax= "",
                    unitId= Convert.ToDecimal(row[10].ToString()),
                    unitConversionId= 1,
                    unit= new UnitSP().UnitView(Convert.ToDecimal(row[10].ToString())).UnitName,
                    taxId= Convert.ToDecimal(row[15].ToString()),
                    taxAmount= Convert.ToDecimal(row[14].ToString())
                    
                });
            }

            dynamic response = new ExpandoObject();
            response.Master = master;
            response.Details = details;
            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetSalesOrderForAuthorization(decimal salesOrderMasterId)
        {
            SalesOrderMasterSP sqSalesOrderMaster = new SalesOrderMasterSP();
            var masterResult = sqSalesOrderMaster.SalesOrderMasterView(salesOrderMasterId);

            SalesOrderDetailsSP sqSalesOrderDetails = new SalesOrderDetailsSP();
            var detailsResult = sqSalesOrderDetails.SalesOrderDetailsViewByMasterId(salesOrderMasterId);

            SalesOrderMasterInfoCustom master = new SalesOrderMasterInfoCustom
            {
                Cancelled = masterResult.Cancelled,
                Date = masterResult.Date,
                EmployeeId = masterResult.EmployeeId,
                ExchangeRateId = masterResult.ExchangeRateId,
                InvoiceNo = masterResult.InvoiceNo,
                LedgerId = masterResult.LedgerId,
                CustomerName = new AccountLedgerSP().AccountLedgerView(masterResult.LedgerId).LedgerName,
                QuotationMasterId = masterResult.QuotationMasterId,
                TaxAmount = masterResult.taxAmount,
                VoucherTypeId = masterResult.VoucherTypeId,
                VoucherType = new VoucherTypeSP().VoucherTypeView(masterResult.VoucherTypeId).VoucherTypeName,
                TotalAmount = masterResult.TotalAmount,
                UserId = masterResult.UserId,
                User = new UserSP().UserView(masterResult.UserId).FirstName + " " + new UserSP().UserView(masterResult.UserId).LastName,
                VoucherNo = masterResult.VoucherNo,
                AuthorizationStatus=masterResult.AuthorizationStatus
            };
            List<SalesQuotationDetailsInfoCustom> details = new List<SalesQuotationDetailsInfoCustom>();
            foreach (DataRow row in detailsResult.Rows)
            {
                var prod = new ProductSP().ProductView(Convert.ToDecimal(row[2].ToString()));
                decimal chk;
                details.Add(new SalesQuotationDetailsInfoCustom
                {
                    amount = Convert.ToDecimal(row[14].ToString()),
                    batchId = Convert.ToDecimal(row[15].ToString()),
                    Category = 0,
                    extra1 = decimal.TryParse(row[21].ToString(),out chk)? new GodownSP().GodownView(Convert.ToDecimal(row[21].ToString())).GodownName: row[21].ToString(),
                    extra2 = "",
                    itemDescription = "",
                    product = prod.ProductName,
                    productcode = prod.ProductCode,
                    barcode = prod.barcode,
                    productId = Convert.ToDecimal(row[2].ToString()),
                    Project = 0,
                    qty = Convert.ToDecimal(row[7].ToString()),
                    quotationDetailsId = Convert.ToDecimal(row[17].ToString()),
                    rate = Convert.ToDecimal(row[8].ToString()),
                    slno = 0,
                    tax = "",
                    unitId = 0,
                    unitConversionId = 1,
                    unit = decimal.TryParse(row[11].ToString(), out chk) ? new UnitSP().UnitView(Convert.ToDecimal(row[11].ToString())).UnitName : row[11].ToString(),/*new UnitSP().UnitView(Convert.ToDecimal(row[11].ToString())).UnitName*/
                    taxId = 0/*Convert.ToDecimal(row[20].ToString())*/,
                    taxAmount = Convert.ToDecimal(row[19].ToString())

                });
            }

            dynamic response = new ExpandoObject();
            response.Master = master;
            response.Details = details;
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public bool AuthorizeOrder(decimal salesOrderMasterId)
        {
            string authorizationDate = DateTime.Now.ToString("yyyy-MM-dd HH:mm:ss");
            string query = string.Format("UPDATE tbl_SalesOrderMaster SET AuthorizationStatus='Authorized',extra2='{1}' WHERE salesOrderMasterId={0}",salesOrderMasterId,authorizationDate);
            DBMatConnection db = new DBMatConnection();
            if(db.customUpdateQuery(query)>0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public string EditSalesQuotationDetails(SalesQuotationModel obj)
        {
            SalesQuotationMasterInfo newObj = new SalesQuotationMasterInfo();
            List<SalesQuotationDetailsInfo> newObj2 = new List<SalesQuotationDetailsInfo>();
            newObj = obj.Master;
            newObj2 = obj.LineItems;

            SalesQuotationMasterSP spSalesQuotationMaster = new SalesQuotationMasterSP();
            SalesQuotationDetailsSP spSalesQotationDetails = new SalesQuotationDetailsSP();

            spSalesQuotationMaster.SalesQuotationMasterEdit(newObj);
            var dbQuoteDetails = spSalesQotationDetails.SalesQuotationDetailsViewByMasterId(newObj.QuotationMasterId);
            for (int i = 0; i < dbQuoteDetails.Rows.Count; i++)
            {
                var exist =newObj2.Find(p => p.QuotationDetailsId == Convert.ToDecimal(dbQuoteDetails.Rows[i].ItemArray[0]));

                if (exist==null)
                {
                    spSalesQotationDetails.SalesQuotationDetailsDelete(Convert.ToDecimal(dbQuoteDetails.Rows[i].ItemArray[0]));
                }
            }

            foreach(var o in newObj2)
            {
                o.Extra2 = "";
                o.ExtraDate = DateTime.Now.Date;
                o.UnitConversionId = o.ProductId;
                o.QuotationMasterId = newObj.QuotationMasterId;

                if(o.QuotationDetailsId > 0)
                {
                    spSalesQotationDetails.SalesQuotationDetailsEdit(o);
                }
                else if(o.QuotationDetailsId == 0)
                {
                    spSalesQotationDetails.SalesQuotationDetailsAdd(o);
                }
            }
            
            return "Changes Made Successfully";
        }

        [HttpGet]
        public string DeleteSalesQuotationDetails(int id)
        {
            SalesQuotationMasterSP spSalesQuotationMaster = new SalesQuotationMasterSP();
            SalesQuotationDetailsSP spSalesQotationDetails = new SalesQuotationDetailsSP();

            spSalesQuotationMaster.SalesQuotationMasterDelete(id);
            var salesQuotation = spSalesQotationDetails.SalesQuotationDetailsViewByMasterId(id);

            for(var sq = 0; sq < salesQuotation.Rows.Count; sq++)
            {
                var decResult1 = new SalesQuotationDetailsSP().SalesQuotationDetailsDelete
                    (Convert.ToDecimal(salesQuotation.Rows[sq].ItemArray[1]));
            }
            return "Quotation Deleted Successfully.";
        }

        [HttpGet]
        public string voucherNo()
        {
            string strPrefix = "";
            string strSalesQuotationNo = "";
            string strSuffix = "";
            decimal decsalesQuotationTypeId=10031;
            decimal decSalesQuotationPreffixSuffixId = 0;
            string strInvoiceNo = "";
            string tableName = "SalesQuotationMaster";
            strSalesQuotationNo = new SalesQuotationMasterSP().SalesQuotationMasterGetMax(decsalesQuotationTypeId).ToString();
            if (strSalesQuotationNo == string.Empty)
            {
                strSalesQuotationNo = " 0";
            }
            strSalesQuotationNo = new TransactionsGeneralFill().VoucherNumberAutomaicGeneration(decsalesQuotationTypeId, Convert.ToDecimal(strSalesQuotationNo), DateTime.Now, tableName);
            if (Convert.ToDecimal(strSalesQuotationNo) != new SalesQuotationMasterSP().SalesQuotationMaxGetPlusOne(decsalesQuotationTypeId))
            {
                strSalesQuotationNo = Convert.ToString(new SalesQuotationMasterSP().SalesQuotationMaxGetPlusOne(decsalesQuotationTypeId));
                strSalesQuotationNo = new TransactionsGeneralFill().VoucherNumberAutomaicGeneration(decsalesQuotationTypeId, Convert.ToDecimal(strSalesQuotationNo), DateTime.Now, tableName);
                if (new SalesQuotationMasterSP().SalesQuotationMasterGetMax(decsalesQuotationTypeId) == "0")
                {
                    strSalesQuotationNo = "0";
                    strSalesQuotationNo = new TransactionsGeneralFill().VoucherNumberAutomaicGeneration(decsalesQuotationTypeId, Convert.ToDecimal(strSalesQuotationNo), DateTime.Now, tableName);
                }
            }
            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decsalesQuotationTypeId, DateTime.Now);
            strPrefix = infoSuffixPrefix.Prefix;
            strSuffix = infoSuffixPrefix.Suffix;
            decSalesQuotationPreffixSuffixId = infoSuffixPrefix.SuffixprefixId;
            strInvoiceNo = strPrefix + strSalesQuotationNo + strSuffix;
            return strInvoiceNo;
        }
        
    }

    public class SalesQuotationMasterInfoCustom
    {        
        public string Approved { get; set; }
        public DateTime Date { get; set; }
        public decimal EmployeeId { get; set; }
        public decimal ExchangeRateId { get; set; }
        public string InvoiceNo { get; set; }
        public decimal LedgerId { get; set; }
        public string CustomerName { get; set; }
        public decimal QuotationMasterId { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal VoucherTypeId { get; set; }
        public string VoucherType { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal UserId { get; set; }
        public string User { get; set; }
        public string VoucherNo { get; set; }
    }

    public class SalesQuotationDetailsInfoCustom
    {
        public decimal quotationDetailsId{ get; set; }
        public decimal quotationMasterId{ get; set; }
        public decimal productId{ get; set; }
        public string product { get; set; }
        public string productcode { get; set; }
        public string barcode { get; set; }
        public decimal unitId{ get; set; }
        public string unit { get; set; }
        public decimal unitConversionId{ get; set; }
        public decimal qty{ get; set; }
        public decimal rate{ get; set; }
        public decimal amount{ get; set; }
        public decimal batchId{ get; set; }
        public int slno{ get; set; }
        public DateTime extraDate{ get; set; }
        public string extra1{ get; set; }
        public string extra2{ get; set; }
        public int Project{ get; set; }
        public int Category{ get; set; }
        public string itemDescription{ get; set; }
        public decimal taxAmount{ get; set; }
        public decimal taxId { get; set; }
        public string tax { get; set; }
        public string storeName { get; set; }
    }

    public class SalesOrderMasterInfoCustom
    {
        public bool Cancelled { get; set; }
        public DateTime Date { get; set; }
        public decimal EmployeeId { get; set; }
        public decimal ExchangeRateId { get; set; }
        public string InvoiceNo { get; set; }
        public decimal LedgerId { get; set; }
        public string CustomerName { get; set; }
        public decimal QuotationMasterId { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal VoucherTypeId { get; set; }
        public string VoucherType { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal UserId { get; set; }
        public string User { get; set; }
        public string VoucherNo { get; set; }
        public string AuthorizationStatus { get; set; }
    }

    public class SalesOrderDetailsInfoCustom
    {
        public decimal quotationDetailsId { get; set; }
        public decimal quotationMasterId { get; set; }
        public decimal productId { get; set; }
        public string product { get; set; }
        public string productcode { get; set; }
        public string barcode { get; set; }
        public decimal unitId { get; set; }
        public string unit { get; set; }
        public decimal unitConversionId { get; set; }
        public decimal qty { get; set; }
        public decimal rate { get; set; }
        public decimal amount { get; set; }
        public decimal batchId { get; set; }
        public int slno { get; set; }
        public DateTime extraDate { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public int Project { get; set; }
        public int Category { get; set; }
        public string itemDescription { get; set; }
        public decimal taxAmount { get; set; }
        public decimal taxId { get; set; }
        public string tax { get; set; }
    }
}
