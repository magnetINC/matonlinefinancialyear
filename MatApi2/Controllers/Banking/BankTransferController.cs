﻿using MatApi.Models.Banking;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Banking
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BankTransferController : ApiController
    {
        decimal decContraSuffixPrefixId = 0;//to store the selected voucher type's suffixpreffixid from frmVoucherTypeSelection
        decimal DecContraVoucherTypeId = 3;//to get the selected voucher type id from frmVoucherTypeSelection
        decimal decMasterId = 0;//ContramasterId to edit delete and print
        decimal decSelectedCurrencyRate = 0;//To select current currency rate
        decimal decAmount = 0;//Amount to be convert 
        decimal decConvertRate = 0;//Converted amount
        string strBankOrCashAccount;//to get the selected value in cmbBankOrCash at teh time of ledger popup
        string strVoucherNo = string.Empty;//to save voucher no into tbl_Contra master
        string strInvoiceNo = string.Empty;//to save invoice no into tbl_Contra master
        string strPrefix = string.Empty;//to get the prefix string from frmvouchertypeselection
        string strSuffix = string.Empty;//to get the suffix string from frmvouchertypeselection
        string tableName = "ContraMaster";//to get the table name in voucher type selection
        int inNarrationCount = 0;//To check no. of lines in txtNarration
        int inArrOfRemove = 0;//number of rows removed by clicking remove button
        bool isAutomatic = false;//To checking vocher no generation auto or not
        bool isEditMode = false;//To indicate whether the form is editmode or not
        bool isValueChanged = false;//To check grid details values incomplete or not
        bool isBankAcocunt = false;//To checking whether selected gridcombo box bank or not

        [HttpGet]
        public HttpResponseMessage LoadLookUps()
        {
            dynamic response = new ExpandoObject();

            var cashorbank = new TransactionsGeneralFill().BankOrCashComboFill(false);
            var currency = new TransactionsGeneralFill().CurrencyComboByDate(DateTime.Now);
            var autoGenFormNo = GetAutoFormNo();

            response.CahsOrBank = cashorbank;
            response.Currency = currency;
            response.FormNo = autoGenFormNo;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpPost]
        public decimal SaveBankTransfer(CreateBankTransferVM input)
        {
            decimal decContraDetailsId = 0;
            try
            {
                ContraMasterSP spContraMaster = new ContraMasterSP();
                ContraMasterInfo infoContraMaster = new ContraMasterInfo();
                ContraDetailsSP spContraDetails = new ContraDetailsSP();
                ContraDetailsInfo infoCOntraDetails = new ContraDetailsInfo();
                ExchangeRateSP spExchangeRate = new ExchangeRateSP();
                decimal decIdentity = 0;
                decimal decLedgerId = 0;
                decimal decDebit = 0;
                decimal decCredit = 0;
                int inCount = input.lineTransfers.Count();
                int inValue = 0;
                for (int i = 0; i < inCount; i++)
                {
                    inValue++;
                }
                if (inValue > 0)
                {
                    if (input.totalAmount != 0)
                    {
                        infoContraMaster.LedgerId = input.ledgerId;
                        infoContraMaster.VoucherNo = input.voucherNo;
                        infoContraMaster.Date = input.date;
                        infoContraMaster.Narration = input.narration;
                        infoContraMaster.TotalAmount = input.totalAmount;
                        infoContraMaster.Extra1 = string.Empty;
                        infoContraMaster.Extra2 = string.Empty;
                        infoContraMaster.Type = input.transactionType;
                        infoContraMaster.SuffixPrefixId = decContraSuffixPrefixId;
                        infoContraMaster.VoucherTypeId = DecContraVoucherTypeId;
                        infoContraMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                        infoContraMaster.UserId = PublicVariables._decCurrentUserId;
                        infoContraMaster.InvoiceNo = input.voucherNo;
                        decIdentity = spContraMaster.ContraMasterAdd(infoContraMaster);

                        infoCOntraDetails.ContraMasterId = decIdentity;
                        infoCOntraDetails.Extra1 = string.Empty;
                        infoCOntraDetails.Extra2 = string.Empty;
                        int inRowCount = input.lineTransfers.Count();
                        //-------------------------------Saving grid details--------------------------------------------------------------------
                        for (int i = 0; i < inRowCount; i++)
                        {
                            infoCOntraDetails.LedgerId = input.lineTransfers[i].ledgerId;
                            infoCOntraDetails.Amount = input.lineTransfers[i].amount;
                            infoCOntraDetails.ChequeNo = input.lineTransfers[i].chequeNo;
                            infoCOntraDetails.ChequeDate = input.lineTransfers[i].chequeDate;
                            infoCOntraDetails.Memo = input.lineTransfers[i].memo;
                            infoCOntraDetails.ExchangeRateId = input.lineTransfers[i].currency;
                            decContraDetailsId = spContraDetails.ContraDetailsAddReturnWithhIdentity(infoCOntraDetails);

                            decLedgerId = input.lineTransfers[i].ledgerId;
                            decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(input.lineTransfers[i].currency);
                            decAmount = input.lineTransfers[i].amount;
                            decConvertRate = decAmount * decSelectedCurrencyRate;
                            if (input.transactionType == "Deposit")
                            {
                                decCredit = decConvertRate;
                                decDebit = 0;
                                LedgerPosting(decLedgerId, decCredit, decDebit, decContraDetailsId, i, input);
                            }
                            else if (input.transactionType == "Withdrawal")
                            {
                                decDebit = decConvertRate;
                                decCredit = 0;
                                LedgerPosting(decLedgerId, decCredit, decDebit, decContraDetailsId, i, input);
                            }
                        }
                    }
                    decAmount = input.totalAmount;
                    decContraDetailsId = 0;
                    if (input.transactionType == "Deposit")
                    {
                        decDebit = decAmount;
                        decCredit = 0;
                        LedgerPosting(infoContraMaster.LedgerId, decCredit, decDebit, decContraDetailsId, -1, input);
                    }
                    else if (input.transactionType == "Withdrawal")
                    {
                        decCredit = decAmount;
                        decDebit = 0;
                        LedgerPosting(infoContraMaster.LedgerId, decCredit, decDebit, decContraDetailsId, -1, input);
                    }
                        
                }
            }
            catch (Exception ex)
            {
            }
            //dynamic response = new ExpandoObject();
            //return Request.CreateResponse(HttpStatusCode.OK, (object)response);
            return decContraDetailsId;
        }

        public void LedgerPosting(decimal decid, decimal decCredit, decimal decDebit, decimal decDetailsId, int inI, CreateBankTransferVM input)
        {
            try
            {
                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                infoLedgerPosting.VoucherTypeId = DecContraVoucherTypeId;
                infoLedgerPosting.VoucherNo = input.voucherNo;
                infoLedgerPosting.Date = input.date; //PublicVariables._dtCurrentDate;
                infoLedgerPosting.LedgerId = decid;
                infoLedgerPosting.DetailsId = decDetailsId;
                infoLedgerPosting.Debit = decDebit;
                infoLedgerPosting.Credit = decCredit;
                infoLedgerPosting.InvoiceNo = input.voucherNo;
                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                if (inI > -1)
                {
                    infoLedgerPosting.ChequeNo = input.lineTransfers[inI].chequeNo;
                    infoLedgerPosting.ChequeDate = input.lineTransfers[inI].chequeDate;                }
                else
                {
                    infoLedgerPosting.ChequeNo = input.lineTransfers[0].chequeNo;
                    infoLedgerPosting.ChequeDate = input.lineTransfers[0].chequeDate;
                }
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
            }
            catch (Exception ex)
            {
            }
        }

        [HttpPost]
        public HttpResponseMessage EditBankTransfer(dynamic bankTransferParam)
        {
            try
            {
                dynamic response = new ExpandoObject();

                ContraDetailsSP spContraDetails = new ContraDetailsSP();
                ContraMasterSP spContraMaster = new ContraMasterSP();
                ContraMasterInfo infoContraMaster = new ContraMasterInfo();
                LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
                VoucherTypeSP spVoucherType = new VoucherTypeSP();
                infoContraMaster = spContraMaster.ContraMasterView(Convert.ToDecimal(bankTransferParam.decMasterId));

                int inDecimalPlace = PublicVariables._inNoOfDecimalPlaces;
                DataTable dtbl = new DataTable();
                dtbl = spContraDetails.ContraDetailsViewWithMasterId(Convert.ToDecimal(bankTransferParam.decMasterId));

                response.infoContraMaster = infoContraMaster;
                response.dtbl = dtbl;
                response.balance = GetAccountBalance(Convert.ToDecimal(bankTransferParam.ledgerId));

                return Request.CreateResponse(HttpStatusCode.OK, (object)response);
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message.ToString());
            }
        }
        
        //[HttpPost]
        public HttpResponseMessage GetCurrencyByDate(DateTime date)
        {
            DataTable dtbl = new DataTable();
            TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
            dtbl = TransactionGeneralFillObj.CurrencyComboByDate(Convert.ToDateTime(date));
            return Request.CreateResponse(HttpStatusCode.OK,dtbl);
        }


        public decimal GetAccountBalance(decimal ledgerId)
        {
            MATFinancials.DAL.DBMatConnection _db = new MATFinancials.DAL.DBMatConnection();
            DataSet ds = _db.ExecuteQuery("select isnull(sum(debit-credit),0) as balance from tbl_LedgerPosting where ledgerId ='" + ledgerId + "'");
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                decimal balance = Convert.ToDecimal(ds.Tables[0].Rows[0]["balance"]);
                return balance;
            }
            return 0;
        }
        public string VoucherNumberGeneration()
        {
            TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
            PurchaseReturnMasterSP SPPurchaseReturnMaster = new PurchaseReturnMasterSP();
            try
            {
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }
                strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(DecContraVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                if (Convert.ToDecimal(strVoucherNo) != SPPurchaseReturnMaster.PurchaseReturnMasterGetMaxPlusOne(DecContraVoucherTypeId))
                {
                    strVoucherNo = SPPurchaseReturnMaster.PurchaseReturnMasterGetMax(DecContraVoucherTypeId).ToString();
                    strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(DecContraVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                    if (SPPurchaseReturnMaster.PurchaseReturnMasterGetMax(DecContraVoucherTypeId) == "0")
                    {
                        strVoucherNo = "0";
                        strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(DecContraVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                    }
                }
                SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(DecContraVoucherTypeId, DateTime.Now);
                strPrefix = infoSuffixPrefix.Prefix;
                strSuffix = infoSuffixPrefix.Suffix;
                decContraSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                strInvoiceNo = strPrefix + strVoucherNo + strSuffix;
            }
            catch (Exception ex)
            {
            }
            return strInvoiceNo;
        }
        [HttpGet]
        public string GetAutoFormNo()
        {
            return VoucherNumberGeneration();
        }
    }
}
