//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MatApi.DBModel
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_PurchaseReturnBilltax
    {
        public decimal purchaseReturnBillTaxId { get; set; }
        public decimal purchaseReturnMasterId { get; set; }
        public Nullable<decimal> taxId { get; set; }
        public Nullable<decimal> taxAmount { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public Nullable<System.DateTime> extraDate { get; set; }
    }
}
