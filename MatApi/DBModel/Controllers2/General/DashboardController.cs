﻿using MatApi.Models;
using MatApi.Models.Register;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.General
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DashboardController : ApiController
    {
        [HttpGet]
        public DataTable GetBankLedgers()
        {
            DataTable dtbl = new DataTable();
            AccountLedgerSP sp = new AccountLedgerSP();
            //string vt = "";
            dtbl = sp.HomePageTransactionGridFill(MATFinancials.PublicVariables._dtToDate);

            return dtbl;
        }

        [HttpGet]
        public List<StockBalanceVM> GetStockBalance()
        {
            DBMatConnection conn = new DBMatConnection();
            
            string queryStr = string.Format(" SELECT p.productName, convert(decimal(18, 2), (ISNULL(SUM(s.inwardQty), 0) - ISNULL(SUM(s.outwardQty), 0))) AS Currentstock" +
                " FROM tbl_product p left join tbl_StockPosting s on p.productId = s.productId GROUP BY p.productId");

            List<StockBalanceVM> resp = new List<StockBalanceVM>();
            var products = new ProductSP().ProductViewAll();
            for(int i=0;i<products.Rows.Count;i++)
            {
                decimal pid = Convert.ToDecimal(products.Rows[i].ItemArray[0]);
                string pname = Convert.ToString(products.Rows[i].ItemArray[2]);
                decimal rLevel = Convert.ToDecimal(products.Rows[i].ItemArray[15]);

                resp.Add(new StockBalanceVM {
                    ProductId=pid,
                    InwardQty= GetTotalQuantity(pid),
                    ProductName=pname,
                    StockBalance= GetStockBalance(pid),
                    ReOrderLevel = rLevel
                });
            }

            return resp;
        }
        public decimal GetTotalQuantity(decimal pid)
        {
            DBMatConnection conn = new DBMatConnection();

            string queryStr = string.Format("select productId, sum(inwardQty) as c from tbl_StockPosting where productId={0} group by productId", pid);

            decimal result = 0;
            try
            {
                result = Convert.ToDecimal(conn.getSingleValue2(queryStr));

            }
            catch (Exception e)
            {

            }

            return Convert.ToDecimal(result);
        }
        public decimal GetStockBalance(decimal pid)
        {
            DBMatConnection conn = new DBMatConnection();

            string queryStr = string.Format("select productId, (sum(inwardQty) - sum(outwardQty)) as c from tbl_StockPosting where productId={0} group by productId",pid);

            decimal result = 0;
            try
            {
                result = Convert.ToDecimal(conn.getSingleValue2(queryStr));

            }
            catch(Exception e)
            {

            }

            return Convert.ToDecimal(result);
        } 

       [HttpGet]
        public List<TotalAmountVM> GetTotalSales()
        {
            List<TotalAmountVM> tAmount = new List<TotalAmountVM>();
            DBMatConnection conn = new DBMatConnection();

            DateTime todayDate = DateTime.Now.Date;
            DateTime yesterDate = DateTime.Now.Date.AddDays(-1);

            string queryStr = string.Format("SELECT sum(totalAmount) AS totalAmount FROM tbl_SalesMaster where date={0}", todayDate);
            string queryStr2 = string.Format("SELECT sum(totalAmount) AS totalAmount FROM tbl_SalesMaster where date={0}", yesterDate);

            var result = Convert.ToDecimal(conn.GetDataSet(queryStr));
            var result2 = Convert.ToDecimal(conn.GetDataSet(queryStr2));

            tAmount.Add(new TotalAmountVM
            {
                day = "Today",
                totalAmount = result
            });
            tAmount.Add(new TotalAmountVM
            {
                day = "Yesterday",
                totalAmount = result2
            });

            return tAmount;
        }
    }
}
