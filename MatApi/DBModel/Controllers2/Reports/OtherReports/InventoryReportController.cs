using MatApi.Models.Reports.OtherReports;
using MATFinancials;
using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Reports.OtherReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class InventoryReportController : ApiController
    {
        List<string> AllItems = new List<string> { };
        List<decimal> AllStores = new List<decimal> { };
        bool isOpeningRate;

        [HttpPost]
        public DataTable StockJournal(StockJournalVM input)
        {
            DataTable dtblReg = new DataTable();
            try
            {
                MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

                dtblReg = new StockJournalMasterSP().StockJournalReportGrideFill(input.FromDate, input.ToDate, input.VoucherType, input.VoucherNo, input.ProductCode, input.Product);
                //string GetQuery = string.Format("");
                //GetQuery = string.Format("");
                //GetQuery = string.Format("SELECT * FROM tbl_StockJournalMaster_Pending WHERE date BETWEEN '{0}' AND '{1}'", input.FromDate, input.ToDate,);
                //var result = db.GetDataSet(GetQuery);
            }
            catch (Exception ex)
            {
                // formMDI.infoError.ErrorString = "SRR2:" + ex.Message;
            }
            return dtblReg;
        }
        [HttpGet]
        public HttpResponseMessage stockJournalDetails(decimal id)
        {
            dynamic response = new ExpandoObject();
            StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
            StockJournalDetailsSP spStockJournalDetails = new StockJournalDetailsSP();
            StockJournalMasterInfo infoStockJournalMaster = new StockJournalMasterInfo();
            StockJournalDetailsInfo infoStockJournalDetails = new StockJournalDetailsInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            VoucherTypeSP spVoucherType = new VoucherTypeSP();

            DataTable dtblMaster = spStockJournalMaster.StockJournalMasterFillForRegisterOrReport(id);
            decimal StockJournalVoucherTypeId = Convert.ToDecimal(dtblMaster.Rows[0]["voucherTypeId"].ToString());
            string strVoucherNo = dtblMaster.Rows[0]["voucherNo"].ToString();
            response.Master = dtblMaster;

            DataSet dsDetails = spStockJournalDetails.StockJournalDetailsForRegisterOrReport(id);
            response.consumption = dsDetails.Tables[0];
            response.production = dsDetails.Tables[1];

            DataSet dsAdditionalCost = spAdditionalCost.StockJournalAdditionalCostForRegisteOrReport(strVoucherNo, StockJournalVoucherTypeId);
            response.cashorbank = dsAdditionalCost.Tables[0];
            response.directAdditionalCost = dsAdditionalCost.Tables[1];

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpPost]
        public DataTable StockVariance(StockVarianceVM input)
        {
            StockPostingSP spstock = new StockPostingSP();
            DataTable dtbl = new DataTable();
            //dynamic response = new ExpandoObject();
            try
            {
                string itemCode = "";
                if (!string.IsNullOrWhiteSpace(input.ItemCode))
                {
                    itemCode = input.ItemCode;
                }
                dtbl = spstock.StockVarianceReportViewAll(input.ProductId, input.StoreId, itemCode);
                var responseObj = dtbl.AsEnumerable().Select(variance => new
                {
                    SLNO = variance.Field<decimal>("SL.NO"),
                    productId = variance.Field<decimal>("productId"),
                    productCode = variance.Field<string>("productCode"),
                    productName = variance.Field<string>("productName"),
                    qty = variance.Field<decimal>("qty"),
                    godownName = variance.Field<string>("godownName"),
                    SystemStock = variance.Field<string>("SystemStock"),
                    Batch = variance.Field<string>("Batch"),
                    rate = variance.Field<decimal>("rate"),
                    //stockDifference = Convert.ToDecimal( variance.Field<string>("SystemStock") != null && !string.IsNullOrEmpty(variance.Field<string>("SystemStock")) ? variance.Field<string>("SystemStock") : "0")  - variance.Field<decimal>("qty"),        
                    stockDifference = variance.Field<decimal>("qty") - Convert.ToDecimal(variance.Field<string>("SystemStock") != null && !string.IsNullOrEmpty(variance.Field<string>("SystemStock")) ? variance.Field<string>("SystemStock") : "0"),
                    stockValue = variance.Field<decimal>("rate") * (variance.Field<decimal>("qty") - Convert.ToDecimal(variance.Field<string>("SystemStock") != null && !string.IsNullOrEmpty(variance.Field<string>("SystemStock")) ? variance.Field<string>("SystemStock") : "0")),
                    //stockDifference = Convert.ToDecimal(SystemStock) - Convert.ToDecimal(qty),
                    extraDate = variance.Field<DateTime>("extraDate").ToShortDateString()
                }).ToList();
                //response.StockVariance = responseObj;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "SVR02" + ex.Message;
            }
            return dtbl;
        }

        [HttpPost]
        public DataTable InventoryMovement(InventoryMovementVM input)
        {
            try
            {
                return new ProductSP().ProductVsBatchReportGridFill(input.VoucherType, input.VoucherNo, input.ProductGroup, input.ProductCode, input.BatchNoId,
                                                              input.FromDate, input.ToDate, input.ProductId, input.StoreId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public DataTable PhysicalStock(PhysicalStockVM input)
        {
            DataTable response = new DataTable();
            try
            {
                PhysicalStockMasterSP spPhysicalStockMaster = new PhysicalStockMasterSP();
                //dtbl = spPhysicalStockMaster.PhysicalStockReportFill(input.FromDate, input.ToDate, input.VoucherNo, input.ProductName,
                //                                                        input.ProductCode, input.VoucherTypeId, input.StrProductCode, input.StoreId);
                string query = "";

                if(input.ProductId==0 && input.StoreId==0)
                {
                    query = string.Format("select p.productcode,g.godownname,psd.qty,p.productname,b.brandname,s.size,p.purchaserate,p.salesrate,p.productid " +
                                                "from tbl_physicalstockdetails as psd " +
                                                "inner join tbl_product as p on p.productid = psd.productid " +
                                                "inner join tbl_size as s on s.sizeid = p.sizeid " +
                                                "inner join tbl_brand as b on b.brandid = p.brandid " +
                                                "inner join tbl_godown as g on g.godownid = p.godownid " +
                                                "inner join tbl_physicalstockmaster as psm on psm.physicalstockmasterid = psd.physicalstockmasterid " +
                                                "where psm.date >= '{0}' and psm.date <= '{1}' ",
                                                input.FromDate, input.ToDate);
                }
                else if(input.ProductId>0 && input.StoreId==0)
                {
                    query = string.Format("select p.productcode,g.godownname,psd.qty,p.productname,b.brandname,s.size,p.purchaserate,p.salesrate,p.productid " +
                                                "from tbl_physicalstockdetails as psd " +
                                                "inner join tbl_product as p on p.productid = psd.productid " +
                                                "inner join tbl_size as s on s.sizeid = p.sizeid " +
                                                "inner join tbl_brand as b on b.brandid = p.brandid " +
                                                "inner join tbl_godown as g on g.godownid = p.godownid " +
                                                "inner join tbl_physicalstockmaster as psm on psm.physicalstockmasterid = psd.physicalstockmasterid " +
                                                "where psm.date >= '{0}' and psm.date <= '{1}' and psd.productid = '{2}' ",
                                                input.FromDate, input.ToDate, input.ProductId);
                }
                else if(input.ProductId==0 && input.StoreId>0)
                {
                    query = string.Format("select p.productcode,g.godownname,psd.qty,p.productname,b.brandname,s.size,p.purchaserate,p.salesrate,p.productid " +
                                                "from tbl_physicalstockdetails as psd " +
                                                "inner join tbl_product as p on p.productid = psd.productid " +
                                                "inner join tbl_size as s on s.sizeid = p.sizeid " +
                                                "inner join tbl_brand as b on b.brandid = p.brandid " +
                                                "inner join tbl_godown as g on g.godownid = p.godownid " +
                                                "inner join tbl_physicalstockmaster as psm on psm.physicalstockmasterid = psd.physicalstockmasterid " +
                                                "where psm.date >= '{0}' and psm.date <= '{1}' and psd.godownid = '{2}' ",
                                                input.FromDate, input.ToDate, input.StoreId);
                }
                else if(input.ProductId>0 && input.StoreId>0)
                {
                    query = string.Format("select p.productcode,g.godownname,psd.qty,p.productname,b.brandname,s.size,p.purchaserate,p.salesrate,p.productid " +
                                                "from tbl_physicalstockdetails as psd " +
                                                "inner join tbl_product as p on p.productid = psd.productid " +
                                                "inner join tbl_size as s on s.sizeid = p.sizeid " +
                                                "inner join tbl_brand as b on b.brandid = p.brandid " +
                                                "inner join tbl_godown as g on g.godownid = p.godownid " +
                                                "inner join tbl_physicalstockmaster as psm on psm.physicalstockmasterid = psd.physicalstockmasterid " +
                                                "where psm.date >= '{0}' and psm.date <= '{1}' and psd.productid = '{2}' and psd.godownid = '{3}'",
                                                input.FromDate, input.ToDate, input.ProductId,input.StoreId);
                }
                response = new DBMatConnection().customSelect(query);

            }
            catch (Exception ex)
            {
            }
            return response;
        }

        [HttpPost]
        public DataTable ProductStatistics(InventoryStatistics input)
        {
            return new ProductSP().ProductStatisticsFill(input.BrandId, input.ModelNoId, input.SizeId, input.ProductGroupId, input.Criteria, input.BatchName, input.FromDate, input.ToDate);
        }

        [HttpPost]
        public StockReportRow StockReportDetails2(StockReportSearch input)
        {
            StockReportRow master = new StockReportRow();
            List<StockReportRowItems> details = new List<StockReportRowItems>();

            try
            {
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                string calculationMethod = string.Empty;
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    lastFinYearEnd = input.FromDate.AddDays(-1);
                }
                //if (!isFormLoad)
                //{
                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                //objValidation.DateValidationFunction(input.ToDate);
                //if (txtTodate.Text == string.Empty)
                //    txtTodate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                //objValidation.DateValidationFunction(txtFromDate);
                //if (txtFromDate.Text == string.Empty)
                //    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");

                StockPostingSP spstock = new StockPostingSP();
                DataTable dtbl = new DataTable();
                try
                {
                    //spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), produceCode, referenceNo);
                    //dtbl = spstock.StockReportDetailsGridFill(input.ProductId,input.StoreId,input.BatchNo,input.FromDate, input.ToDate,input.ProductCode, input.RefNo);
                    //for some crazy reasons, the "To date" parameter refers to the last the of the previous financial year on d desktop code
                    dtbl = spstock.StockReportDetailsGridFill(input.ProductId, input.StoreId, input.BatchNo, input.FromDate, input.FromDate.AddDays(-1), input.ProductCode, input.RefNo);
                }
                catch (Exception ex)
                {
                }
                // when there is no transaction during the period selected but there is transaction before the period selected,
                // invoke this addition (dtbl2) and it is per store 20170406
                DataTable dtbl2 = new DataTable();
                try
                {
                    dtbl2 = spstock.StockReportDetailsGridFill(input.ProductId, input.StoreId, input.BatchNo, input.FromDate, input.ToDate, input.ProductCode, input.RefNo);
                }
                catch (Exception ex)
                {
                }
                List<decimal> storesInDtbl = new List<decimal>();
                List<decimal> storesInDtbl2 = new List<decimal>();
                List<decimal> storesWithoutTransactions = new List<decimal>();

                if ((dtbl != null && dtbl.Rows.Count > 0) && (dtbl2 != null && dtbl2.Rows.Count > 0))
                {
                    storesInDtbl = dtbl.AsEnumerable().Select(n => n.Field<decimal>("godownId")).ToList();
                    storesInDtbl2 = dtbl2.AsEnumerable().Select(n => n.Field<decimal>("godownId")).ToList();
                    // select stores without transaction before the period
                    storesWithoutTransactions = storesInDtbl2.Except(storesInDtbl).ToList();
                    if (dtbl2.AsEnumerable().Where(p => storesWithoutTransactions.Contains(p.Field<decimal>("godownId"))).Any())
                    {
                        dtbl2 = dtbl2.AsEnumerable().Where(p => storesWithoutTransactions.Contains(p.Field<decimal>("godownId"))).CopyToDataTable();
                    }
                }

                // the invoked method ends here
                //dgvStockreportDetails.Rows.Clear();
                fillAllItems(input);
                var ItemsWithRecords = (dtbl.AsEnumerable().Select(j => j.Field<string>("productCode"))).ToList();
                List<string> itemsWithoutRecords = AllItems.Except(ItemsWithRecords).ToList();

                decimal currentStore = 0, previousStore = 0;
                decimal currentProductId = 0;
                decimal prevProductId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0, inwardQty = 0, outwardQty = 0, totalQuantityIn = 0, totalQuantityOut = 0;
                decimal qtyBal = 0, stockValue = 0, stockValue2 = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal totalAssetValue = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };

                foreach (DataRow dr in dtbl.Rows)
                {
                    currentProductId = Convert.ToDecimal(dr["productId"]);
                    productCode = dr["productCode"].ToString();
                    currentStore = Convert.ToDecimal(dr["godownId"]);
                    string voucherType = "", refNo = "";

                    inwardQty = (from p in dtbl.AsEnumerable()
                                 where p.Field<decimal>("productId") == prevProductId && p.Field<decimal>("godownId") == previousStore
                                 select p.Field<decimal>("inwardQty")).Sum();
                    outwardQty = (from p in dtbl.AsEnumerable()
                                  where p.Field<decimal>("productId") == prevProductId && p.Field<decimal>("godownId") == previousStore
                                  select p.Field<decimal>("outwardQty")).Sum();
                    //totalQuantityIn += inwardQty;
                    //totalQuantityOut += outwardQty;
                    inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                    outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                    decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                    string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                    rate2 = Convert.ToDecimal(dr["rate"]);
                    if (previousRunningAverage != 0 && currentProductId == prevProductId)
                    {
                        purchaseRate = previousRunningAverage;
                    }
                    //======================== 2017-02-27 =============================== //
                    //if (againstVoucherType != "NA")
                    //{
                    //    voucherType = againstVoucherType;
                    //}
                    //else
                    //{
                    //    voucherType = dr["typeOfVoucher"].ToString();
                    //}
                    refNo = dr["refNo"].ToString();
                    if (againstVoucherType != "NA")
                    {
                        refNo = dr["againstVoucherNo"].ToString();
                        isAgainstVoucher = true;
                        //againstVoucherType = dr["typeOfVoucher"].ToString();
                        againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                        //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                        string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                        string typeOfVoucher = conn.getSingleValue(queryString);
                        if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                        if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                    }
                    voucherType = dr["typeOfVoucher"].ToString();
                    //---------  COMMENT END  ----------- //
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if (outwardQty2 > 0)
                    {
                        if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                        {
                            if (qtyBal == 0)
                            {
                                //computedAverageRate = (stockValue / 1); // old changed by Urefe 2017-02-21
                                //qtyBal += inwardQty2 - outwardQty2; // old changed by Urefe 2017-02-21
                                //stockValue = computedAverageRate * 1; // old changed by Urefe 2017-02-21
                                //computedAverageRate = Convert.ToDecimal(dr["rate"]);  // old changed by Urefe 2017-02-27
                                computedAverageRate = purchaseRate;
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                //if(computedAverageRate == 0)
                                //{
                                //    stockValue = qtyBal * rate2;
                                //}
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                        {
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue -= outwardQty2 * rate2;
                        }
                        else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return")
                            || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }

                        else if (outwardQty2 > 0 && voucherType == "Sales Order")
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                    }
                    else if (voucherType == "Sales Return" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Rejection In" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;

                        }
                        else
                        {
                            computedAverageRate = previousRunningAverage;// (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                        // voucherType = "Sales Return";
                    }
                    else if (voucherType == "Delivery Note")
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                    }
                    else
                    {
                        // hndles other transactions such as purchase, opening balance, etc
                        qtyBal += inwardQty2 - outwardQty2;
                        stockValue += (inwardQty2 * rate2);
                    }
                    // insert subtotal per item per store and calculate the final subtotal per item (all stores together) totalAssetValue += 
                    if ((currentProductId != prevProductId && i != 0) || (currentStore != previousStore && i != 0))
                    {
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        if (voucherType == "Sales Invoice")
                        {
                            stockValue = qtyBal * purchaseRate;
                        }
                        else
                        {
                            stockValue = qtyBal * rate2;
                        }

                        details.Add(new StockReportRowItems
                        {
                            ProductId = "0",
                            ProductName = "",
                            StoreName = "",
                            InwardQty = (inwardQty + openingQuantityIn).ToString("N2"),
                            OutwardQty = ("-") + (outwardQty + openingQuantityOut).ToString("N2"),
                            Rate = "-",
                            AverageCost = "", // computedAverageRate.ToString("N2");
                            QtyBalance = (inwardQty - outwardQty).ToString("N2"),
                            StockValue = details[i - 1].StockValue,
                            VoucherTypeName = "Sub Total:- " + details[i - 1].ProductName
                            //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = dgvStockreportDetails.Rows[i - 1].Cells["stockValue"].Value;
                            //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Sub Total:- " + dgvStockreportDetails.Rows[i - 1].Cells["productName"].Value; //+" In "+ dgvStockreportDetails.Rows[i-1].Cells["godownName"].Value;
                        });
                        //dgvStockreportDetails.Rows[i].Cells["productId"].Value = 0;
                        //dgvStockreportDetails.Rows[i].Cells["productName"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = (inwardQty + openingQuantityIn).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ("-") + (outwardQty + openingQuantityOut).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["rate"].Value = "-";
                        //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = ""; // computedAverageRate.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = (inwardQty - outwardQty).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = dgvStockreportDetails.Rows[i - 1].Cells["stockValue"].Value;
                        //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Sub Total:- " + dgvStockreportDetails.Rows[i - 1].Cells["productName"].Value; //+" In "+ dgvStockreportDetails.Rows[i-1].Cells["godownName"].Value;
                        totalAssetValue += Convert.ToDecimal(details[i - 1].StockValue);
                        totalQuantityIn += inwardQty;
                        totalQuantityOut += outwardQty;
                        i++;
                        openingQuantityIn = 0;
                        openingQuantityOut = 0;
                    }
                    //calculation end
                    // -----------------added 2017-02-21 -------------------- //
                    if ((currentProductId != prevProductId) || (currentStore != previousStore))
                    {
                        // ---------------------- PUT THE OPENING STOCK HERE I.E AFTER THE SUBTOTAL -------------------------- //
                        computedAverageRate = rate2;
                        //decimal openingStock = methodForStockValue.currentStockValue3(lastFinYearStart, lastFinYearEnd, false, 0, false, DateTime.Now, productCode);
                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, productCode, currentStore);
                        dcOpeningStockForRollOver = itemParams.Item1;
                        openingQuantity = itemParams.Item2;
                        openingQuantityIn = itemParams.Item3;
                        openingQuantityOut = itemParams.Item4;
                        //dcOpeningStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, DateTime.Now, currentProductId, false);
                        //openingQuantity = methodForStockValue.currentStockQuantity(lastFinYearStart, lastFinYearEnd, true, DateTime.Now, currentProductId, false);
                        qtyBal += openingQuantity;
                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                        {
                            computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                            stockValue = qtyBal * computedAverageRate;
                        }
                        details.Add(new StockReportRowItems
                        {
                            ProductId = "0",
                            ProductName = "",
                            StoreName = "",
                            InwardQty = openingQuantityIn.ToString("N2"),
                            OutwardQty = openingQuantityOut.ToString("N2"),
                            Rate = "",
                            AverageCost = "",
                            QtyBalance = openingQuantity.ToString("N2"),
                            StockValue = dcOpeningStockForRollOver.ToString("N2"),
                            VoucherTypeName = "Opening Stock "
                        });
                        //dgvStockreportDetails.Rows.Add();
                        //dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        //dgvStockreportDetails.Rows[i].Cells["productId"].Value = 0;
                        //dgvStockreportDetails.Rows[i].Cells["productName"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["rate"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";

                        //totalAssetValue += Convert.ToDecimal(dcOpeningStockForRollOver);
                        totalQuantityIn += openingQuantityIn;
                        totalQuantityOut += openingQuantityOut;
                        //totalQuantityOut += outwardQty;
                        i++;

                    }
                    // ------------------------------------------------------ //

                    //dgvStockreportDetails.Rows.Add();
                    //dgvStockreportDetails.Rows[i].Cells["productId"].Value = dr["productId"];
                    //dgvStockreportDetails.Rows[i].Cells["productName"].Value = dr["productName"];
                    //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = dr["godownName"];
                    //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = Convert.ToDecimal(dr["inwardQty"]).ToString("N2");
                    //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2");
                    //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = isAgainstVoucher == true ? againstVoucherType : voucherType;
                    //isAgainstVoucher = false;
                    //dgvStockreportDetails.Rows[i].Cells["rate"].Value = Convert.ToDecimal(dr["rate"]).ToString("N2");
                    //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value =  computedAverageRate.ToString("N2");

                    //comment out by alex
                    //details.Add(new StockReportRowItems
                    //{
                    //    ProductId = dr["productId"].ToString(),
                    //    ProductName = dr["productName"].ToString(),
                    //    StoreName = dr["godownName"].ToString(),
                    //    InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                    //    OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                    //    Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                    //    AverageCost = computedAverageRate.ToString("N2"),
                    //    QtyBalance = "",
                    //    StockValue = "",
                    //    VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType
                    //});
                    //isAgainstVoucher = false;

                    string avgCost = "";
                    if (stockValue != 0 && qtyBal != 0)
                    {
                        //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = (stockValue / qtyBal).ToString("N2");
                        avgCost = (stockValue / qtyBal).ToString("N2");

                    }
                    else
                    {
                        //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = computedAverageRate.ToString("N2");
                        avgCost = computedAverageRate.ToString("N2");
                    }

                    details.Add(new StockReportRowItems
                    {
                        ProductId = dr["productId"].ToString(),
                        ProductName = dr["productName"].ToString(),
                        StoreName = dr["godownName"].ToString(),
                        InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                        OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                        VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType,
                        Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                        AverageCost = avgCost,
                        Date = Convert.ToDateTime(dr["date"]).ToShortDateString(),
                        QtyBalance = qtyBal.ToString("N2"),
                        StockValue = stockValue.ToString("N2"),
                        Batch = dr["Batch"].ToString(),
                        ProductCode = dr["productCode"].ToString(),
                        RefNo = refNo
                    });
                    isAgainstVoucher = false;

                    //dgvStockreportDetails.Rows[i].Cells["date"].Value = Convert.ToDateTime(dr["date"]).ToShortDateString();
                    //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = qtyBal.ToString("N2");
                    //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = stockValue.ToString("N2");
                    //dgvStockreportDetails.Rows[i].Cells["Batch"].Value = dr["Batch"].ToString();
                    //dgvStockreportDetails.Rows[i].Cells["productCode"].Value = dr["productCode"];
                    //dgvStockreportDetails.Rows[i].Cells["refNo"].Value = refNo; // dr["refNo"];
                    prevProductId = currentProductId;
                    previousStore = currentStore;
                    previousRunningAverage = Convert.ToDecimal(details[i].AverageCost);
                    i++;
                }
                //Add the sub total for the last item in the table
                //dgvStockreportDetails.Rows.Add();
                //if (i == 0 && i < dgvStockreportDetails.Rows.Count)
                //{
                //    i = dgvStockreportDetails.Rows.Count;
                //}
                //dgvStockreportDetails.Rows.Add();
                //dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.DeepSkyBlue;
                details.Add(new StockReportRowItems
                {
                    ProductId = "0",
                    ProductName = "",
                    StoreName = "",
                    InwardQty = inwardQty.ToString("N2"),
                    OutwardQty = ("-") + outwardQty.ToString("N2"),
                    Rate = "-",
                    AverageCost = "",
                    QtyBalance = (inwardQty - outwardQty).ToString("N2"),
                    StockValue = details[i - 1].StockValue,
                    VoucherTypeName = "Sub Total:- " + details[i - 1].ProductName
                });
                //dgvStockreportDetails.Rows[i].Cells["productId"].Value = 0;
                //dgvStockreportDetails.Rows[i].Cells["productName"].Value = "";
                //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = inwardQty.ToString("N2");
                //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ("-") + outwardQty.ToString("N2");
                //dgvStockreportDetails.Rows[i].Cells["rate"].Value = "-";
                //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "";
                //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = (inwardQty - outwardQty).ToString("N2");
                //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = dgvStockreportDetails.Rows[i - 1].Cells["stockValue"].Value;
                //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Sub Total:- " + dgvStockreportDetails.Rows[i - 1].Cells["productName"].Value; //+" In "+ dgvStockreportDetails.Rows[i-1].Cells["godownName"].Value;
                totalQuantityIn += inwardQty;
                totalQuantityOut += outwardQty;
                i++;
                //Add the sub total for the last item in the table end

                // ================== Add closing stock as opening stock if transactions have not been ran on that item in that store for that period ============== //
                if (storesWithoutTransactions.Any())
                {
                    foreach (var item in storesWithoutTransactions)
                    {
                        string productCode2 = (dtbl2.AsEnumerable().Where(m => m.Field<decimal>("godownId") == item).
                            Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString();
                        var itemParams = methodForStockValue.currentStockValue(PublicVariables._dtFromDate, Convert.ToDateTime(input.FromDate).AddDays(-1), true, productCode2, item);
                        decimal itemStockValue = itemParams.Item1;
                        decimal itemStock = itemParams.Item2;
                        //dgvStockreportDetails.Rows.Add();
                        //dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        //dgvStockreportDetails.Rows[i].Cells["productId"].Value = item.ToString();
                        //dgvStockreportDetails.Rows[i].Cells["productCode"].Value = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["productName"].Value = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("godownName"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = ""; // Convert.ToDecimal(inwardqt).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ""; //Convert.ToDecimal(outwardqt).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = itemStock.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = itemStockValue.ToString("N2");
                        string avgCost2 = "";
                        if (itemStockValue != 0 && itemStock != 0)
                        {
                            // dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = (itemStockValue / itemStock).ToString("N2");
                            avgCost2 = (itemStockValue / itemStock).ToString("N2");
                        }
                        else
                        {
                            //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "0.00";
                            avgCost2 = "0.00";
                        }
                        // dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock";

                        details.Add(new StockReportRowItems
                        {
                            ProductId = item.ToString(),
                            ProductCode = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString(),
                            ProductName = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString(),
                            StoreName = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("godownName"))).FirstOrDefault().ToString(),
                            InwardQty = "",
                            OutwardQty = "",
                            QtyBalance = itemStock.ToString("N2"),
                            StockValue = itemStockValue.ToString("N2"),
                            AverageCost = avgCost2,
                            VoucherTypeName = "Opening Stock"
                        });
                        stockValue2 += itemStockValue;
                        i++;
                    }
                }

                // ================== Add closing stock as opening stock if transactions have not been ran on that item for that period (for all items)============== //
                if (itemsWithoutRecords.Any() && input.ProductId == 0)
                {
                    ProductSP spproduct = new ProductSP();
                    DataTable dt = new DataTable();
                    dt = spproduct.ProductViewAll();
                    foreach (var item in itemsWithoutRecords)
                    {
                        //decimal itemStockValue = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, false, DateTime.Now, item, true);
                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, Convert.ToDateTime(input.FromDate).AddDays(-1), true, item, currentStore);
                        decimal itemStockValue = itemParams.Item1;
                        //decimal itemStock = methodForStockValue.currentStockQuantity(lastFinYearStart, lastFinYearEnd, false, DateTime.Now item, true);
                        decimal itemStock = itemParams.Item2;
                        //dgvStockreportDetails.Rows.Add();
                        //dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        //dgvStockreportDetails.Rows[i].Cells["productId"].Value = item.ToString();
                        //dgvStockreportDetails.Rows[i].Cells["productCode"].Value = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["productName"].Value = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = ""; // Convert.ToDecimal(inwardqt).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ""; //Convert.ToDecimal(outwardqt).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = itemStock.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = itemStockValue.ToString("N2");
                        string avgCost3 = "";
                        if (itemStockValue != 0 && itemStock != 0)
                        {
                            //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = (itemStockValue / itemStock).ToString("N2");
                            avgCost3 = (itemStockValue / itemStock).ToString("N2");
                        }
                        else
                        {
                            //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "0.00";
                            avgCost3 = "";
                        }
                        //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock";
                        details.Add(new StockReportRowItems
                        {
                            ProductId = item.ToString(),
                            ProductCode = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString(),
                            ProductName = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString(),
                            StoreName = "",
                            InwardQty = "",
                            OutwardQty = "",
                            QtyBalance = itemStock.ToString("N2"),
                            StockValue = itemStockValue.ToString("N2"),
                            VoucherTypeName = "Opening Stock",
                            AverageCost = avgCost3
                        });
                        stockValue2 += itemStockValue;
                        i++;
                    }
                }
                // ================== Add closing stock as opening stock if transactions have not been ran on that item for that period (for a single item) ============== //
                else if (itemsWithoutRecords.Any() && input.ProductId != 0 && (dtbl == null || dtbl.Rows.Count < 1))
                {
                    ProductSP spproduct = new ProductSP();
                    DataTable dt = new DataTable();
                    dt = spproduct.ProductViewAll();
                    itemsWithoutRecords.Clear(); itemsWithoutRecords.AddRange(dt.AsEnumerable().Where(k => k.Field<decimal>("productId") == input.ProductId)
                        .Select(k => k.Field<string>("productCode")).ToList());
                    foreach (var item in itemsWithoutRecords)
                    {
                        //decimal itemStockValue = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, false, DateTime.Now, item, true);
                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, Convert.ToDateTime(input.FromDate).AddDays(-1), true, item, currentStore);
                        decimal itemStockValue = itemParams.Item1;
                        //decimal itemStock = methodForStockValue.currentStockQuantity(lastFinYearStart, lastFinYearEnd, false, DateTime.Now item, true);
                        decimal itemStock = itemParams.Item2;
                        //dgvStockreportDetails.Rows.Add();
                        //dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        //dgvStockreportDetails.Rows[i].Cells["productId"].Value = item.ToString();
                        //dgvStockreportDetails.Rows[i].Cells["productCode"].Value = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["productName"].Value = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = ""; // Convert.ToDecimal(inwardqt).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ""; //Convert.ToDecimal(outwardqt).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = itemStock.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = itemStockValue.ToString("N2");
                        string avgCost4 = "";
                        if (itemStockValue != 0 && itemStock != 0)
                        {
                            //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = (itemStockValue / itemStock).ToString("N2");
                            avgCost4 = (itemStockValue / itemStock).ToString("N2");
                        }
                        else
                        {
                            //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "0.00";
                            avgCost4 = "0.00";
                        }
                        //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock";

                        details.Add(new StockReportRowItems
                        {
                            ProductId = item.ToString(),
                            ProductCode = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString(),
                            ProductName = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString(),
                            StoreName = "",
                            InwardQty = "",
                            OutwardQty = "",
                            QtyBalance = itemStock.ToString("N2"),
                            StockValue = itemStockValue.ToString("N2"),
                            VoucherTypeName = "Opening Stock",
                            AverageCost = avgCost4
                        });
                        stockValue2 += itemStockValue;
                        i++;
                    }
                }
                // ================================== end region ================================ //
                //decimal productID = Convert.ToDecimal(dgvStockreportDetails.Rows[i - 1].Cells["productId"].Value);
                #region what is this block of code meant for
                /*
                decimal inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == prevProductId
                                    select p.Field<decimal>("inwardQty")).Sum();
                decimal outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == prevProductId
                                     select p.Field<decimal>("outwardQty")).Sum();

                dgvStockreportDetails.Rows.Add();
                dgvStockreportDetails.Rows[dgvStockreportDetails.Rows.Count - 1].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                dgvStockreportDetails.Rows[i].Cells["productId"].Value = 0;
                dgvStockreportDetails.Rows[i].Cells["productName"].Value = "";
                dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = (Convert.ToDecimal(inwardqt) + openingQuantityIn).ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = (Convert.ToDecimal(outwardqt) + openingQuantityOut).ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = qtyBal.ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = stockValue.ToString("N2");
                //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = (stockValue + dcOpeningStockForRollOver).ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Sub Total:- " + dgvStockreportDetails.Rows[i != 0 ? i - 1 : i].Cells["productName"].Value; //+ " In " + dgvStockreportDetails.Rows[i !=0 ? i - 1: i].Cells["godownName"].Value;
                */
                #endregion
                //decimal totalAsset = totalAssetValue + stockValue;
                //total inward and outward quantity and total stock value
                decimal totalAsset = totalAssetValue + stockValue + stockValue2;// - dcOpeningStockForRollOver;
                //dgvStockreportDetails.Rows.Add();
                //dgvStockreportDetails.Rows.Add();
                //dgvStockreportDetails.Rows[i + 1].Cells["stockValue"].Value = "==========";
                //dgvStockreportDetails.Rows.Add();
                //dgvStockreportDetails.Rows[i + 2].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                //dgvStockreportDetails.Rows[i + 2].Cells["qtyBalalance"].Value = (totalQuantityIn - totalQuantityOut).ToString("N2");

                string avgCost5 = "";
                if (totalQuantityIn - totalQuantityOut != 0)
                {
                    //dgvStockreportDetails.Rows[i + 2].Cells["dgvtxtAverageCost"].Value = (totalAsset / (totalQuantityIn - totalQuantityOut)).ToString("N2");
                    avgCost5 = (totalAsset / (totalQuantityIn - totalQuantityOut)).ToString("N2");
                }
                //dgvStockreportDetails.Rows[i + 2].Cells["stockValue"].Value = totalAsset.ToString("N2");
                //dgvStockreportDetails.Rows[i + 2].Cells["voucherTypeName"].Value = "Total Stock Value:";
                //dgvStockreportDetails.Rows.Add();
                //dgvStockreportDetails.Rows[i + 3].Cells["stockValue"].Value = "==========";
                details.Add(new StockReportRowItems
                {
                    StockValue = "==========",
                });
                details.Add(new StockReportRowItems
                {
                    QtyBalance = (totalQuantityIn - totalQuantityOut).ToString("N2"),
                    AverageCost = avgCost5,
                    StockValue = totalAsset.ToString("N2")
                });
                details.Add(new StockReportRowItems
                {
                    StockValue = "==========",
                });
                //}
                master.StockReportRowItem = details;
            }

            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }
            return master;
        }

        [HttpPost]
        public HttpResponseMessage StockSummary(StockSummarySearch input)
        {
            List<StockSummary> stockSummary = new List<StockSummary>();
            dynamic response = new ExpandoObject();

            decimal returnValue = 0;
            try
            {
                DBMatConnection conn = new DBMatConnection();
                StockPostingSP spstock = new StockPostingSP();
                DataTable dtbl = new DataTable();
                decimal productId = input.ProductId;
                string pCode = input.ProductCode.ToString();
                decimal storeId = input.StoreId;
                string batch = input.BatchNo;

                string queryStr = "select top 1 f.fromDate from tbl_FinancialYear f";
                DateTime lastFinYearStart = Convert.ToDateTime(conn.getSingleValue(queryStr));

                //dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime("04/30/2017") /*PublicVariables._dtToDate*/, pCode, "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, PublicVariables._dtToDate, pCode, "");

                decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                decimal prevProductId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                string productName = "", storeName = "", productCode = "";
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal totalAssetValue = 0, qtyBalance = 0;
                decimal value1 = 0;
                int i = 0;
                bool isAgainstVoucher = false;

                foreach (DataRow dr in dtbl.Rows)
                {
                    currentProductId = Convert.ToDecimal(dr["productId"]);
                    currentGodownID = Convert.ToDecimal(dr["godownId"]);
                    productCode = dr["productCode"].ToString();
                    string voucherType = "", refNo = "";

                    decimal inwardQty = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == prevProductId
                                         select p.Field<decimal>("inwardQty")).Sum();
                    decimal outwardQty = (from p in dtbl.AsEnumerable()
                                          where p.Field<decimal>("productId") == prevProductId
                                          select p.Field<decimal>("outwardQty")).Sum();

                    inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                    outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                    decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                    string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                    rate2 = Convert.ToDecimal(dr["rate"]);
                    if (previousRunningAverage != 0 && currentProductId == prevProductId)
                    {
                        purchaseRate = previousRunningAverage;
                    }
                    if (currentProductId == prevProductId)
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        prevProductId = Convert.ToDecimal(dr["productId"]);
                        productCode = dr["productCode"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    else
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    //======================== 2017-02-27 =============================== //
                    //if (againstVoucherType != "NA")
                    //{
                    //    voucherType = againstVoucherType;
                    //}
                    //else
                    //{
                    //    voucherType = dr["typeOfVoucher"].ToString();
                    //}
                    refNo = dr["refNo"].ToString();
                    if (againstVoucherType != "NA")
                    {
                        refNo = dr["againstVoucherNo"].ToString();
                        isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                        againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                        //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                        string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                        string typeOfVoucher = conn.getSingleValue(queryString);
                        if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                        if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                    }
                    voucherType = dr["typeOfVoucher"].ToString();
                    //---------  COMMENT END ----------- //
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if (outwardQty2 > 0)
                    {
                        if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = purchaseRate;
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                        {
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue -= outwardQty2 * rate2;
                        }

                        else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Sales Order")
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                    }
                    else if (voucherType == "Sales Return" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Rejection In" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;

                        }
                        else
                        {
                            computedAverageRate = previousRunningAverage; // (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                        // voucherType = "Sales Return";
                    }
                    else if (voucherType == "Delivery Note")
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                    }
                    else
                    {
                        // hndles other transactions such as purchase, opening balance, etc
                        qtyBal += inwardQty2 - outwardQty2;
                        stockValue += (inwardQty2 * rate2);
                    }
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        //qtyBal = Convert.ToDecimal(dr["inwardQty"]);
                        //stockValue = inwardQty2 * rate2;
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        if (voucherType == "Sales Invoice")
                        {
                            stockValue = qtyBal * purchaseRate;
                        }
                        else
                        {
                            stockValue = qtyBal * rate2;
                        }
                        //totalAssetValue += Math.Round(value1, 2);
                        totalAssetValue += value1;
                        //i++;
                        returnValue = totalAssetValue;
                    }
                    // -----------------added 2017-02-21 -------------------- //
                    if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        if (voucherType == "Sales Invoice")
                        {
                            computedAverageRate = purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = rate2;
                        }
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        stockValue = qtyBal * computedAverageRate;
                    }
                    // ------------------------------------------------------ //

                    previousRunningAverage = computedAverageRate;

                    value1 = stockValue;
                    prevProductId = currentProductId;
                    prevGodownID = currentGodownID;

                    if (i == dtbl.Rows.Count - 1)
                    {
                        var avgCost = "";
                        if (value1 != 0 && (inwardqt - outwardqt) != 0)
                        {
                            avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                        }
                        else
                        {
                            avgCost = "0.0";
                        }

                        stockSummary.Add(new StockSummary
                        {
                            ProductId = dr["productId"].ToString(),
                            StoreId = dr["godownId"].ToString(),
                            ProductName = dr["productName"].ToString(),
                            StoreName = dr["godownName"].ToString(),
                            PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                            SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                            QtyBalance = (inwardqt - outwardqt).ToString(),
                            AvgCostValue = avgCost,
                            StockValue = value1.ToString("N2"),
                            ProductCode = dr["productCode"].ToString()
                        });
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                    {
                        var avgCost = "";
                        if (value1 != 0 && (inwardqt - outwardqt) != 0)
                        {
                            avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                        }
                        else
                        {
                            avgCost = "0.0";
                        }

                        stockSummary.Add(new StockSummary
                        {
                            ProductId = dr["productId"].ToString(),
                            StoreId = dr["godownId"].ToString(),
                            ProductName = dr["productName"].ToString(),
                            StoreName = dr["godownName"].ToString(),
                            PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                            SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                            QtyBalance = (inwardqt - outwardqt).ToString(),
                            AvgCostValue = avgCost,
                            StockValue = value1.ToString("N2"),
                            ProductCode = dr["productCode"].ToString()
                        });
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    i++;
                }
                decimal totalStockValue = 0;
                foreach (var row in stockSummary)
                {
                    totalStockValue += Convert.ToDecimal(row.StockValue);
                }

                stockSummary.Add(new StockSummary
                {
                    ProductName = "",
                    StoreName = "Total Stock Value:",
                    PurchaseRate = "",
                    SalesRate = "",
                    QtyBalance = qtyBalance.ToString("N2"),
                    AvgCostValue = (totalStockValue / qtyBalance).ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    ProductCode = ""
                });
                stockSummary.Add(new StockSummary
                {
                    ProductName = "",
                    StoreName = "",
                    PurchaseRate = "",
                    SalesRate = "",
                    QtyBalance = "",
                    AvgCostValue = "",
                    StockValue = "=============",
                    ProductCode = ""
                });

                response.TotalStockValue = totalStockValue.ToString("N2");
                response.StockSummary = stockSummary;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKSR1:" + ex.Message;
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpPost]
        public StockReportRow StockReportDetails(StockReportSearch input)
        {
            StockReportRow master = new StockReportRow();
            List<StockReportRowItems> details = new List<StockReportRowItems>();

            try
            {
                DataTable prodDtbl = new DataTable();
                prodDtbl = new ProductSP().ProductViewAll();
                for (int k = 0; k < prodDtbl.Rows.Count; k++)
                {
                    AllItems.Add(prodDtbl.Rows[k]["productCode"].ToString());
                }

                GodownSP spGodown = new GodownSP();
                DataTable storeDtbl = new DataTable();
                storeDtbl = spGodown.GodownViewAll();
                for (int k = 0; k < storeDtbl.Rows.Count; k++)
                {
                    AllStores.Add(Convert.ToDecimal(storeDtbl.Rows[k]["godownId"]));
                }

                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                string calculationMethod = string.Empty;
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    lastFinYearEnd = input.FromDate.AddDays(-1);
                }
                decimal currentProductId = 0, prevProductId = 0, prevStoreId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0;
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal stockValuePerItemPerStore = 0, totalStockValue = 0;
                decimal inwardQtyPerStore = 0, outwardQtyPerStore = 0, totalQtyPerStore = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "", productName = "", storeName = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };
                DataTable dtbl = new DataTable();
                DataTable dtblItem = new DataTable();
                DataTable dtblStore = new DataTable();
                DataTable dtblOpeningStocks = new DataTable();
                DataTable dtblOpeningStockPerItemPerStore = new DataTable();
                StockPostingSP spstock = new StockPostingSP();

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                decimal productId = input.ProductId, storeId = input.StoreId;
                string produceCode = input.ProductCode, batch = input.BatchNo, referenceNo = input.RefNo;

                //dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text), "", "");
                //dtblOpeningStocks = spstock.StockReportDetailsGridFill(0, 0, "All", PublicVariables._dtFromDate, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), "", "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo);
                dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, input.FromDate.AddDays(-1), produceCode, referenceNo);

                if (AllItems.Any())
                {
                    foreach (var item in AllItems)
                    {
                        // ======================================== for each item begins here ====================================== //
                        dtblItem = new DataTable();
                        if (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item).Any())
                        {
                            dtblItem = (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item)).CopyToDataTable();
                            productName = (dtbl.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(m => m.Field<string>("productName")).FirstOrDefault().ToString());
                        }
                        if (dtblItem != null && dtblItem.Rows.Count > 0)
                        {
                            if (AllStores.Any())
                            {
                                foreach (var store in AllStores)
                                {
                                    // =================================== for that item, for each store begins here ============================== //
                                    qtyBal = 0; stockValue = 0;
                                    stockValuePerItemPerStore = 0; //dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    dtblStore = new DataTable();

                                    // ************************ insert opening stock per item per store before other transactions ******************** //
                                    dtblOpeningStockPerItemPerStore = new DataTable();
                                    if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                    {
                                        dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store)).CopyToDataTable();
                                        if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                        {
                                            computedAverageRate = rate2;
                                            var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, item, store);
                                            dcOpeningStockForRollOver = itemParams.Item1;
                                            openingQuantity = itemParams.Item2;
                                            openingQuantityIn = openingQuantity; // itemParams.Item3;
                                            openingQuantityOut = 0; // itemParams.Item4;
                                            //qtyBal += openingQuantity;
                                            qtyBal = openingQuantity;
                                            if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                            {
                                                computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                                stockValue = qtyBal * computedAverageRate;
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = item,
                                                ProductCode = item,
                                                ProductName = productName,
                                                StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                                Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                                InwardQty = openingQuantityIn.ToString("N2"),
                                                OutwardQty = openingQuantityOut.ToString("N2"),
                                                Rate = "",
                                                AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                                QtyBalance = openingQuantity.ToString("N2"),
                                                StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                                VoucherTypeName = "Opening Stock "
                                            });

                                            //dgvStockDetailsReport.Rows[i].Cells["productId"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productName"].Value = productName;
                                            //dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            //Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(); ;
                                            //dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                            //dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                            //dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";
                                            totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                            totalQtyPerStore += openingQuantity;
                                            i++;
                                        }
                                    }
                                    // ************************ insert opening stock per item per store before other transactions end ******************** //

                                    if (dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).Any())
                                    {
                                        dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).CopyToDataTable();
                                    }
                                    if (dtblStore != null && dtblStore.Rows.Count > 0)
                                    {
                                        storeName = (dtblStore.AsEnumerable().Where(m => m.Field<decimal>("godownId") == store).Select(m => m.Field<string>("godownName")).FirstOrDefault().ToString());
                                        foreach (DataRow dr in dtblStore.Rows)
                                        {
                                            // opening stock has been factored in thinking there was no other transaction, now other transactions factored it in again, so remove it
                                            totalStockValue -= Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            //dgvStockDetailsReport.Rows[i-1].DefaultCellStyle.BackColor = Color.White;
                                            //dgvStockDetailsReport.Rows[i-1].Cells["rate"].Value = "";
                                            dcOpeningStockForRollOver = 0;  // reset the opening stock because of the loop

                                            currentProductId = Convert.ToDecimal(dr["productId"]);
                                            productCode = dr["productCode"].ToString();
                                            string voucherType = "", refNo = "";
                                            inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                                            decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                                            string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                                            if (!isOpeningRate)
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }
                                            //if (purchaseRate == 0)
                                            //{
                                            //    purchaseRate = rate2;
                                            //}
                                            if (prevStoreId != store)
                                            {
                                                previousRunningAverage = purchaseRate;
                                            }
                                            if (previousRunningAverage != 0 && currentProductId == prevProductId)
                                            {
                                                purchaseRate = previousRunningAverage;
                                            }
                                            refNo = dr["refNo"].ToString();
                                            if (againstVoucherType != "NA")
                                            {
                                                refNo = dr["againstVoucherNo"].ToString();
                                                isAgainstVoucher = true;
                                                againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                                                string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                                                string typeOfVoucher = conn.getSingleValue(queryString);
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                            }
                                            voucherType = dr["typeOfVoucher"].ToString();
                                            if (voucherType == "Stock Journal")
                                            {
                                                //voucherType = "Stock Transfer";
                                                string queryString = string.Format(" SELECT extra1 FROM tbl_StockJournalMaster WHERE voucherNo = '{0}' ", dr["refNo"]);
                                                voucherType = conn.getSingleValue(queryString);
                                            }
                                            if (outwardQty2 > 0)
                                            {
                                                if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate;
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                                                {
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue -= outwardQty2 * rate2;
                                                }
                                                else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return")
                                                    || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;// 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }

                                                else if (outwardQty2 > 0 && voucherType == "Sales Order")
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else
                                                {
                                                    if (qtyBal != 0)
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                            }
                                            else if (voucherType == "Sales Return" && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType == "Rejection In" && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;

                                                }
                                                else
                                                {
                                                    computedAverageRate = previousRunningAverage;// (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType == "Delivery Note")
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                            }
                                            else
                                            {
                                                // hndles other transactions such as purchase, opening balance, etc
                                                qtyBal += inwardQty2 - outwardQty2;
                                                stockValue += (inwardQty2 * rate2);
                                            }

                                            // ------------------------------------------------------ //
                                            string avgCost = "";
                                            if (stockValue != 0 && qtyBal != 0)
                                            {
                                                avgCost = (stockValue / qtyBal).ToString("N2");
                                            }
                                            else
                                            {
                                                avgCost = computedAverageRate.ToString("N2");
                                            }
                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = dr["productId"].ToString(),
                                                ProductCode = dr["productCode"].ToString(),
                                                StoreName = dr["godownName"].ToString(),
                                                InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                                                OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                                                Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                                AverageCost = avgCost,
                                                QtyBalance = qtyBal.ToString("N2"),
                                                StockValue = stockValue.ToString("N2"),
                                                VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType,
                                                RefNo = refNo,
                                                Batch = dr["Batch"].ToString(),
                                                Date = Convert.ToDateTime(dr["date"]).ToShortDateString(),
                                                ProductName = dr["productName"].ToString()
                                            });
                                            isAgainstVoucher = false;

                                            stockValuePerItemPerStore = Convert.ToDecimal(stockValue);
                                            prevProductId = currentProductId;
                                            previousRunningAverage = Convert.ToDecimal(avgCost);
                                            inwardQtyPerStore += Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQtyPerStore += Convert.ToDecimal(dr["outwardQty"]);
                                            i++;
                                        }
                                        // ===================== for every store for that item, put the summary =================== //
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = "",
                                            ProductCode = item,
                                            StoreName = storeName,
                                            InwardQty = (inwardQtyPerStore + openingQuantityIn).ToString("N2"),
                                            OutwardQty = ("-") + (outwardQtyPerStore + openingQuantityOut).ToString("N2"),
                                            Rate = "",
                                            AverageCost = "",
                                            QtyBalance = qtyBal.ToString("N2"),
                                            StockValue = stockValuePerItemPerStore.ToString("N2"),
                                            VoucherTypeName = "Sub total: ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName
                                        });
                                        totalStockValue += Convert.ToDecimal(stockValuePerItemPerStore.ToString("N2"));
                                        totalQtyPerStore += (inwardQtyPerStore - outwardQtyPerStore) /*+ openingQuantity*/;
                                        i++;
                                        openingQuantity = 0;
                                        inwardQtyPerStore = 0;
                                        outwardQtyPerStore = 0;
                                        qtyBal = 0;
                                        stockValue = 0;
                                        stockValuePerItemPerStore = 0;
                                        //dgvStockDetailsReport.Rows.Add();
                                        i++;
                                    }
                                    dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    prevStoreId = store;
                                }
                            }
                        }
                        // *** if no transaction for that item for all stores for that period add the opening stock for all stores per store *** //
                        else
                        {
                            foreach (var store in AllStores)
                            {
                                stockValuePerItemPerStore = 0;
                                dtblStore = new DataTable();

                                // ************************ insert opening stock per item per store ******************** //
                                dtblOpeningStockPerItemPerStore = new DataTable();
                                if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                {
                                    dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item)).CopyToDataTable();
                                    if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                    {
                                        computedAverageRate = rate2;
                                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, item, store);
                                        dcOpeningStockForRollOver = itemParams.Item1;
                                        openingQuantity = itemParams.Item2;
                                        openingQuantityIn = openingQuantity; // itemParams.Item3;
                                        openingQuantityOut = 0; // itemParams.Item4;
                                        qtyBal += openingQuantity;
                                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                        {
                                            //computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                            //stockValue = openingQuantity * computedAverageRate;
                                        }

                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = item,
                                            ProductCode = item,
                                            StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                        Where(l => l.Field<string>("productCode") == item).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                            InwardQty = openingQuantityIn.ToString("N2"),
                                            OutwardQty = openingQuantityOut.ToString("N2"),
                                            Rate = "",
                                            AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                            QtyBalance = openingQuantity.ToString("N2"),
                                            StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                            VoucherTypeName = "Opening Stock ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName
                                        });

                                        totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                        dcOpeningStockForRollOver = 0;
                                        totalQtyPerStore += openingQuantity;
                                        openingQuantityIn = 0;
                                        openingQuantityOut = 0;
                                        openingQuantity = 0;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }

                // ************************ Calculate Total **************************** //
                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = ""
                });

                var avgCost2 = "";
                if (totalQtyPerStore != 0)
                {
                    avgCost2 = (totalStockValue / totalQtyPerStore).ToString("N2");
                }
                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = avgCost2,
                    QtyBalance = totalQtyPerStore.ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    VoucherTypeName = "Total Stock Value:",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = ""
                });

                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = ""
                });
            }

            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }
            master.TotalAverageCost = 0;
            master.TotalQtyBalance = 0;
            master.TotalStockValue = 0;
            master.StockReportRowItem = details;
            return master;
        }

        private decimal getOpeningStock(string date, decimal productId, decimal storeId)
        {
            decimal output = 0.0m;
            string query = string.Format("select ISNULL(sum(inwardQty)-sum(outwardQty),0) from tbl_StockPosting where productId={0} and godownId={1} and date<='{2}'", productId, storeId, date);
            output = Convert.ToDecimal(new DBMatConnection().getSingleValue(query));
            return output;
        }

        [HttpPost]
        public List<StoreLine> CustomStockSummaryReport(CustomSummaryParam input)
        {
            var items = new ProductSP().ProductViewAll();
            var stores = new GodownSP().GodownViewAll();
            List<StoreLine> storeInfo = new List<StoreLine>();

            foreach (DataRow store in stores.Rows)
            {
                List<ItemLine> itemInfo = new List<ItemLine>();
                foreach (DataRow item in items.Rows)
                {
                    itemInfo.Add(new ItemLine
                    {
                        Description = item[1].ToString(),
                        ItemId = Convert.ToDecimal(item[0].ToString()),
                        ItemName = item[2].ToString(),
                        ProductGroupId = item[3].ToString(),
                        PartNo = item[28].ToString(),
                        Size = new SizeSP().SizeView(Convert.ToDecimal(item[6].ToString())).Size,
                        TotalOpeningStock = getOpeningStock(input.FromDate.ToString(), Convert.ToDecimal(item[0].ToString()), Convert.ToDecimal(store[0].ToString())),
                        TotalDeliveryNote = getTotalDeliveryNote(input.FromDate, input.ToDate, Convert.ToDecimal(store[0].ToString()), Convert.ToDecimal(item[0].ToString())),
                        TotalMaterialReceipt = getTotalMaterialReceipt(input.FromDate, input.ToDate, Convert.ToDecimal(store[0].ToString()), Convert.ToDecimal(item[0].ToString())),
                        TotalAdjustment = getTotalStockAdjustment(input.FromDate, input.ToDate, Convert.ToDecimal(store[0].ToString()), Convert.ToDecimal(item[0].ToString())),
                        TotalTransferOut = getTotalTransferOut(input.FromDate, input.ToDate, Convert.ToDecimal(store[0].ToString()), Convert.ToDecimal(item[0].ToString())),
                        TotalTransferIn = getTotalTransferIn(input.FromDate, input.ToDate, Convert.ToDecimal(store[0].ToString()), Convert.ToDecimal(item[0].ToString()))
                    });
                }

                storeInfo.Add(new StoreLine
                {
                    StoreId = Convert.ToDecimal(store[0].ToString()),
                    StoreName = store[1].ToString(),
                    ItemLines = itemInfo
                });

            }

            return storeInfo;
        }

        private decimal getTotalDeliveryNote(string fromDate, string toDate, decimal storeId, decimal productId)
        {
            decimal sum = 0.0m;
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("select d.productId,sum(d.qty) " +
                                         "from tbl_DeliveryNoteDetails as d " +
                                         "inner join tbl_DeliveryNoteMaster as m on m.deliveryNoteMasterId = d.deliveryNoteMasterId " +
                                         "where m.date >= '{0}' and m.date <= '{1}' and d.godownId = {2} and d.productId = {3} " +
                                         "group by d.productId", fromDate, toDate, storeId, productId);
            string val = con.getSingleValue2(query);
            if (val == "" || string.IsNullOrEmpty(val))
            {
                val = "0.0";
            }
            sum = Convert.ToDecimal(val);
            return sum;
        }

        private decimal getTotalMaterialReceipt(string fromDate, string toDate, decimal storeId, decimal productId)
        {
            decimal sum = 0.0m;
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("select d.productId,sum(d.qty) " +
                                         "from tbl_materialreceiptDetails as d " +
                                         "inner join tbl_materialreceiptMaster as m on m.materialreceiptMasterId = d.materialreceiptMasterId " +
                                         "where m.date >= '{0}' and m.date <= '{1}' and d.godownId = {2} and d.productId = {3} " +
                                         "group by d.productId", fromDate, toDate, storeId, productId);
            string val = con.getSingleValue2(query);
            if (val == "" || string.IsNullOrEmpty(val))
            {
                val = "0.0";
            }
            sum = Convert.ToDecimal(val);
            return sum;
        }

        private decimal getTotalStockAdjustment(string fromDate, string toDate, decimal storeId, decimal productId)
        {
            decimal sum = 0.0m;
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("select d.productId,sum(d.qty) " +
                                         "from tbl_StockJournalDetails as d " +
                                         "inner join tbl_StockJournalMaster as m on m.stockJournalMasterId = d.stockJournalMasterId " +
                                         "where m.date >= '{0}' and m.date <= '{1}' and d.godownId = {2} and d.productId = {3} and m.extra1='Stock Out' " +
                                         "group by d.productId", fromDate, toDate, storeId, productId);
            string val = con.getSingleValue2(query);
            if (val == "" || string.IsNullOrEmpty(val))
            {
                val = "0.0";
            }
            sum = Convert.ToDecimal(val);
            return sum;
        }

        private decimal getTotalTransferIn(string fromDate, string toDate, decimal storeId, decimal productId)
        {
            decimal sum = 0.0m;
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("select d.productId,sum(d.qty) " +
                                         "from tbl_StockJournalDetails as d " +
                                         "inner join tbl_StockJournalMaster as m on m.stockJournalMasterId = d.stockJournalMasterId " +
                                         "where m.date >= '{0}' and m.date <= '{1}' and d.godownId = {2} and d.productId = {3} and m.extra1='Production' " +
                                         "group by d.productId", fromDate, toDate, storeId, productId);
            string val = con.getSingleValue2(query);
            if (val == "" || string.IsNullOrEmpty(val))
            {
                val = "0.0";
            }
            sum = Convert.ToDecimal(val);
            return sum;
        }

        private decimal getTotalTransferOut(string fromDate, string toDate, decimal storeId, decimal productId)
        {
            decimal sum = 0.0m;
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("select d.productId,sum(d.qty) " +
                                         "from tbl_StockJournalDetails as d " +
                                         "inner join tbl_StockJournalMaster as m on m.stockJournalMasterId = d.stockJournalMasterId " +
                                         "where m.date >= '{0}' and m.date <= '{1}' and d.godownId = {2} and d.productId = {3} and m.extra1='Consumption' " +
                                         "group by d.productId", fromDate, toDate, storeId, productId);
            string val = con.getSingleValue2(query);
            if (val == "" || string.IsNullOrEmpty(val))
            {
                val = "0.0";
            }
            sum = Convert.ToDecimal(val);
            return sum;
        }

        private void fillAllItems(StockReportSearch input)
        {
            if (input.ProductCode != string.Empty || input.BatchNo != string.Empty)
            {
                try
                {
                    ProductSP spproduct = new ProductSP();
                    AllItems.Clear();
                    DataTable dt = new DataTable();
                    dt = spproduct.ProductViewAll();
                    if (dt.AsEnumerable().Where(k => k.Field<string>("productCode") == input.ProductCode).Any())
                    {
                        // filter by product code only
                        dt = dt.AsEnumerable().Where(k => k.Field<string>("productCode") == input.ProductCode).CopyToDataTable();
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            AllItems.Add(dt.Rows[j]["productCode"].ToString());
                        }
                    }
                    if (dt.AsEnumerable().Where(k => k.Field<string>("batchNo") == input.BatchNo).Any())
                    {
                        // filter by batch only
                        AllItems.Clear();
                        dt = dt.AsEnumerable().Where(k => k.Field<string>("batchNo") == input.BatchNo).CopyToDataTable();
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            AllItems.Add(dt.Rows[j]["productCode"].ToString());
                        }
                    }
                    // add filter by batch and product code combined below here
                }
                catch (Exception ex)
                {
                    //formMDI.infoError.ErrorString = "SDR1:" + ex.Message;
                }
            }

        }
    }

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class InventoryReportLookupController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage StockJournal()
        {
            dynamic response = new ExpandoObject();

            ProductSP spproduct = new ProductSP();
            DataTable dtbl = spproduct.ProductViewAll();
            DataRow dr = dtbl.NewRow();
            dr["productId"] = 0;
            dr["productName"] = "All";
            dtbl.Rows.InsertAt(dr, 0);

            response.Products = dtbl;
            response.VoucherTypes = new StockJournalMasterSP().VoucherTypeComboFillForStockJournalReport("Stock Journal", true);
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage StockVariance()
        {
            dynamic response = new ExpandoObject();

            ProductSP spproduct = new ProductSP();
            DataTable dtbl = spproduct.ProductViewAll();
            DataTable dtblStores = new GodownSP().GodownViewAll();
            DataRow dr = dtbl.NewRow();
            dr["productId"] = 0;
            dr["productName"] = "All";
            dtbl.Rows.InsertAt(dr, 0);

            DataRow drStores = dtblStores.NewRow();
            drStores["godownId"] = 0;
            drStores["godownName"] = "All";
            dtblStores.Rows.InsertAt(drStores, 0);

            response.Products = dtbl;
            response.Stores = dtblStores;
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage InventoryMovement()
        {
            dynamic response = new ExpandoObject();

            response.ProductGroup = new ProductGroupSP().ProductGroupViewAll();
            response.Batch = new BatchSP().BatchViewAll();
            response.Product = new ProductSP().ProductViewAll();
            response.Store = new GodownSP().GodownViewAll();
            response.VoucherTypes = new VoucherTypeSP().VoucherTypeSelection();

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage PhysicalStock()
        {
            dynamic response = new ExpandoObject();

            response.Product = new ProductSP().ProductViewAll();
            response.Store = new GodownSP().GodownViewAll();
            response.VoucherTypes = new VoucherTypeSP().VoucherTypeSelectionComboFill("Physical Stock");

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage StockDetails()
        {
            dynamic response = new ExpandoObject();

            response.Product = new ProductSP().ProductViewAll();
            response.Store = new GodownSP().GodownViewAll();
            response.Batch = new BatchSP().BatchViewAll();

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage InventoryStatistics()
        {
            dynamic response = new ExpandoObject();

            response.ProductGroup = new ProductGroupSP().ProductGroupViewAll();
            response.ModelNo = new ModelNoSP().ModelNoViewAll();
            response.Brand = new BrandSP().BrandViewAll();
            response.Size = new SizeSP().SizeViewAll();

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
    }

    public class ItemLine
    {
        public decimal ItemId { get; set; }
        public string ItemName { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }
        public string ProductGroupId { get; set; }
        public string Size { get; set; }
        public decimal TotalOpeningStock { get; set; }
        public decimal TotalDeliveryNote { get; set; }
        public decimal TotalMaterialReceipt { get; set; }
        public decimal TotalAdjustment { get; set; }
        public decimal TotalTransferIn { get; set; }
        public decimal TotalTransferOut { get; set; }
        public decimal Balance { get; set; }
    }
    public class StoreLine
    {
        public decimal StoreId { get; set; }
        public string StoreName { get; set; }
        public List<ItemLine> ItemLines { get; set; }
    }

    public class CustomSummaryParam
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}