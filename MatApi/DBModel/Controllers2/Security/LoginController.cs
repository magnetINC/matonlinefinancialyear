﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials;
using MatApi.Models;
using MatApi.Models.Security;

namespace MatApi.Controllers.Security
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LoginController : ApiController
    {
        UserSP userSp;
        public LoginController()
        {
            userSp = new UserSP();
        }
        [HttpPost]
        public LoginResponseVM Login(LoginRequestModel request)
        {
            LoginResponseVM response = new LoginResponseVM();
            try
            {
                decimal userId = userSp.GetUserIdAfterLogin(request.Username, encodePassword(request.Password));
                UserInfo user = userSp.UserView(userId);
                GodownInfo store = new GodownSP().GodownView(Convert.ToDecimal(user.StoreId));
                RoleInfo role = new RoleSP().RoleView(user.RoleId);

                response.UserId = user.UserId;
                response.Username = user.UserName;
                response.FirstName = user.FirstName;
                response.LastName = user.LastName;
                response.StoreId = Convert.ToDecimal(user.StoreId);
                response.StoreName = store.GodownName;
                response.RoleId = role.RoleId;
                response.RoleName = role.Role;
                response.IsActive = user.Active;
                response.DateCreated = user.ExtraDate;
            }

            catch(Exception ex)
            {

            }

            return response;
        }

        private string encodePassword(string password)
        {
            MATFinancials.Classes.Security sec = new MATFinancials.Classes.Security();
            return sec.base64Encode(password);
        }

        public UserInfo getUserDetails(decimal userId)
        {
            UserSP sp = new UserSP();
            UserInfo user =sp.UserView(userId);
            return user;
            //return new PersonInfo {
            //    FirstName="Halex",
            //    LastName="Thosyn",
            //    Username="mclux",
            //    Role="Developer",
            //    RoleId=1,
            //    UserId=1
            //};
        }
    }
}
