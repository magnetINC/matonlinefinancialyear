﻿using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Company.General_Journals
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RunGeneralJournalController : ApiController
    {
        DataTable dtblPartyBalance = new DataTable();//to store party balance entries while clicking btn_Save

        public RunGeneralJournalController()
        {
        }

        [HttpGet]
        public HttpResponseMessage GetDropdowns()
        {
            dynamic response = new ExpandoObject();
            response.AccountLedger = new AccountLedgerSP().AccountLedgerViewForJournalVoucher();
            response.Currencies = new TransactionsGeneralFill().CurrencyComboByDate(DateTime.Now);
            response.InvoiceNo = getNextInvoiceNo();
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public DataTable GetJournals()
        {
            JournalMasterSP journal = new JournalMasterSP();
            return journal.JournalMasterViewAll();
        }

        [HttpPost]
        public DataTable GetPartyBalance(PartyBalanceInvoicesVM input)
        {
            input.VoucherTypeId = 6;
            DataTable response = new DataTable();
            response = new PartyBalanceSP().PartyBalanceComboViewByLedgerId(input.LedgerId, input.DebitOrCredit, input.VoucherTypeId, input.voucherNo);
            return response;
        }

        [HttpPost]
        public void Save(SaveJournalVM input)
        {
            decimal decJournalVoucherTypeId = 6;
            try
            {
                decimal decTotalDebit = 0;
                decimal decTotalCredit = 0;

                decTotalDebit = input.PartyBalanceInfo.Sum(p=>p.Debit);
                decTotalCredit = input.PartyBalanceInfo.Sum(p => p.Credit);

                JournalMasterSP spJournalMaster = new JournalMasterSP();
                JournalDetailsSP spJournalDetails = new JournalDetailsSP();
                JournalMasterInfo infoJournalMaster = new JournalMasterInfo();
                JournalDetailsInfo infoJournalDetails = new JournalDetailsInfo();
                PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                ExchangeRateSP spExchangeRate = new ExchangeRateSP();

                infoJournalMaster.VoucherNo = input.JournalMasterInfo.VoucherNo;
                infoJournalMaster.InvoiceNo = input.JournalMasterInfo.InvoiceNo;
                infoJournalMaster.SuffixPrefixId = 0;
                infoJournalMaster.Date = DateTime.Now;
                infoJournalMaster.Narration = input.JournalMasterInfo.Narration;
                infoJournalMaster.UserId = PublicVariables._decCurrentUserId;
                infoJournalMaster.VoucherTypeId = decJournalVoucherTypeId;
                infoJournalMaster.FinancialYearId = Convert.ToDecimal(PublicVariables._decCurrentFinancialYearId.ToString());

                infoJournalMaster.Extra1 = string.Empty;
                infoJournalMaster.Extra2 = string.Empty;
                infoJournalMaster.ExtraDate = DateTime.Now;


                infoJournalMaster.TotalAmount = decTotalDebit;
                decimal decJournalMasterId = spJournalMaster.JournalMasterAdd(infoJournalMaster);

                /*******************JournalDetailsAdd and LedgerPosting*************************/
                infoJournalDetails.JournalMasterId = decJournalMasterId;
                infoJournalDetails.ExtraDate = DateTime.Now;
                infoJournalDetails.Extra1 = string.Empty;
                infoJournalDetails.Extra2 = string.Empty;
                infoJournalDetails.ChequeDate = DateTime.Now;
                infoJournalDetails.Memo = "";

                decimal decLedgerId = 0;
                decimal decDebit = 0;
                decimal decCredit = 0;
                //int inRowCount = dgvJournalVoucher.RowCount;
                //for (int inI = 0; inI < inRowCount - 1; inI++)
                for (int i = 0; i < input.JournalDetailsInfo.Count; i++)
                {
                    infoJournalDetails.LedgerId = input.JournalDetailsInfo[i].LedgerId;
                    decLedgerId = infoJournalDetails.LedgerId;
                    infoJournalDetails.Memo = input.JournalDetailsInfo[i].Memo;
                    infoJournalDetails.ProjectId = 0;
                    infoJournalDetails.CategoryId = 0;
                    infoJournalDetails.ExchangeRateId = 1;

                    //--------Currency conversion--------------//
                    decimal decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(1);
                    //decimal decAmount = Convert.ToDecimal(input.JournalDetailsInfo[i] dgvJournalVoucher.Rows[inI].Cells["dgvtxtAmount"].Value.ToString());
                    //decConvertRate = decAmount * decSelectedCurrencyRate;
                    //===========================================//
                    if (input.JournalDetailsInfo[i].Debit>0)
                    {
                        infoJournalDetails.Debit = input.JournalDetailsInfo[i].Debit;
                        infoJournalDetails.Credit = 0;
                        decDebit = input.JournalDetailsInfo[i].Debit*decSelectedCurrencyRate;
                        decCredit = infoJournalDetails.Credit;
                    }
                    else if (input.JournalDetailsInfo[i].Credit > 0)
                    {
                        infoJournalDetails.Credit = input.JournalDetailsInfo[i].Credit;
                        infoJournalDetails.Debit = 0;
                        decDebit = input.JournalDetailsInfo[i].Credit;
                        decCredit = input.JournalDetailsInfo[i].Credit*decSelectedCurrencyRate;
                    }

                    infoJournalDetails.ExchangeRateId = 1;
                    infoJournalDetails.ChequeNo = input.JournalDetailsInfo[i].ChequeNo;
                    infoJournalDetails.ChequeDate = DateTime.Now;
                    //infoJournalDetails.ChequeDate = (input.JournalDetailsInfo[i].ChequeDate==null) ? DateTime.Now : input.JournalDetailsInfo[i].ChequeDate;

                    decimal decJournalDetailsId = spJournalDetails.JournalDetailsAdd(infoJournalDetails);
                    if (decJournalDetailsId != 0)
                    {
                        PartyBalanceAddOrEdit(i,input.PartyBalanceInfo,input.JournalMasterInfo);
                        LedgerPosting(decLedgerId, decCredit, decDebit, decJournalDetailsId, input.JournalDetailsInfo[i],input.JournalMasterInfo);
                    }

                }
                
            }
            catch (Exception ex)
            {                
            }
        }

        [HttpGet]
        public decimal getNextInvoiceNo()
        {
            decimal inv = new JournalMasterSP().JournalMasterGetMax(6);
            return inv + 1;
        }

        public bool PartyBalanceAddOrEdit(int inJ, List<PartyBalanceInfo> input, JournalMasterInfo journalMaster)
        {
            bool isSave = false;
            decimal voucherTypeId = 6;

            try
            {
                int inTableRowCount = dtblPartyBalance.Rows.Count;
                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                PartyBalanceSP spPartyBalance = new PartyBalanceSP();
                InfopartyBalance.Debit = 0;
                InfopartyBalance.CreditPeriod = 0;
                InfopartyBalance.Date = DateTime.Now/*input[inJ].Date*/;
                InfopartyBalance.Credit = input[inJ].Credit;
                InfopartyBalance.ExchangeRateId = 1/*input[inJ].ExchangeRateId*/;
                InfopartyBalance.Extra1 = string.Empty;
                InfopartyBalance.Extra2 = string.Empty;
                InfopartyBalance.ExtraDate = DateTime.Now;
                InfopartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                InfopartyBalance.LedgerId = input[inJ].LedgerId;
                InfopartyBalance.ReferenceType = input[inJ].ReferenceType;
                if (input[inJ].ReferenceType == "New" || input[inJ].ReferenceType == "OnAccount")
                {
                    InfopartyBalance.AgainstInvoiceNo = "0";//dtblPartyBalance.Rows[inJ]["AgainstInvoiceNo"].ToString();
                    InfopartyBalance.AgainstVoucherNo = "0";//dtblPartyBalance.Rows[inJ]["AgainstVoucherNo"].ToString();
                    InfopartyBalance.AgainstVoucherTypeId = 0;// Convert.ToDecimal(dtblPartyBalance.Rows[inJ]["AgainstVoucherTypeId"].ToString());//decPaymentVoucherTypeId;
                    InfopartyBalance.VoucherTypeId = voucherTypeId;
                    InfopartyBalance.InvoiceNo = input[inJ].InvoiceNo;

                    InfopartyBalance.VoucherNo = input[inJ].VoucherNo;

                }
                else
                {
                    InfopartyBalance.ExchangeRateId = 1;
                    InfopartyBalance.AgainstInvoiceNo = input[inJ].AgainstInvoiceNo;

                    InfopartyBalance.AgainstVoucherNo = input[inJ].AgainstVoucherNo;

                    InfopartyBalance.AgainstVoucherTypeId = voucherTypeId;
                    InfopartyBalance.VoucherTypeId = input[inJ].VoucherTypeId;
                    InfopartyBalance.VoucherNo = input[inJ].VoucherNo;
                    InfopartyBalance.InvoiceNo = input[inJ].InvoiceNo;
                }
                if (input[inJ].PartyBalanceId.ToString() == "0")
                {
                    if (spPartyBalance.PartyBalanceAdd(InfopartyBalance) > 0)
                    {
                        isSave = true;
                    }
                }
                else
                {
                    InfopartyBalance.PartyBalanceId = input[inJ].PartyBalanceId;
                    if (spPartyBalance.PartyBalanceEdit(InfopartyBalance) > 0)
                    {
                        isSave = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RV10:" + ex.Message;
            }
            return isSave;
        }

        public void LedgerPosting(decimal decId, decimal decCredit, decimal decDebit, decimal decDetailsId, JournalDetailsInfo journalDetailsInfo,JournalMasterInfo journalMaster)
        {
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            ExchangeRateSP SpExchangRate = new ExchangeRateSP();
            decimal decOldExchange = 0;
            decimal decNewExchangeRate = 0;
            decimal decNewExchangeRateId = 0;
            decimal decOldExchangeId = 0;
            try
            {

                if (journalDetailsInfo.Extra1 == "Against")
                {
                    //infoLedgerPosting.Date = PublicVariables._dtCurrentDate;  // ------- old implementation using system date set n FormMdi ----------- //
                    infoLedgerPosting.Date = DateTime.Now/*journalMaster.Date*/;
                    infoLedgerPosting.VoucherTypeId = 6;
                    infoLedgerPosting.VoucherNo = journalMaster.VoucherNo;
                    infoLedgerPosting.DetailsId = decDetailsId;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.InvoiceNo = journalMaster.InvoiceNo;

                    infoLedgerPosting.ChequeNo = journalDetailsInfo.ChequeNo;
                    infoLedgerPosting.ChequeDate = DateTime.Now;

                    //if (dgvJournalVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value != null && dgvJournalVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value.ToString() != string.Empty)
                    //{
                    //    infoLedgerPosting.ChequeNo = dgvJournalVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value.ToString();
                    //    if (dgvJournalVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value != null && dgvJournalVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value.ToString() != string.Empty)
                    //    {
                    //        infoLedgerPosting.ChequeDate = Convert.ToDateTime(dgvJournalVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value.ToString());
                    //    }
                    //    else
                    //        infoLedgerPosting.ChequeDate = DateTime.Now;
                    //}
                    //else
                    //{
                    //    infoLedgerPosting.ChequeNo = string.Empty;
                    //    infoLedgerPosting.ChequeDate = DateTime.Now;
                    //}


                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.LedgerId = decId;
                    infoLedgerPosting.Credit = decCredit;
                    infoLedgerPosting.Debit = decDebit;

                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
                else
                {
                    //infoLedgerPosting.Date = PublicVariables._dtCurrentDate; // ------- old implementation using system date set n FormMdi ----------- //
                    infoLedgerPosting.Date = DateTime.Now;
                    infoLedgerPosting.VoucherTypeId = 6;
                    infoLedgerPosting.VoucherNo = journalMaster.VoucherNo;
                    infoLedgerPosting.DetailsId = decDetailsId;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.InvoiceNo = journalMaster.InvoiceNo;                    

                    infoLedgerPosting.ChequeNo = journalDetailsInfo.ChequeNo;
                    infoLedgerPosting.ChequeDate = DateTime.Now;

                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.LedgerId = decId;

                    foreach (DataRow dr in dtblPartyBalance.Rows)
                    {
                        if (infoLedgerPosting.LedgerId == Convert.ToDecimal(dr["LedgerId"].ToString()))
                        {
                            //decOldExchange = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                            //decNewExchangeRateId = Convert.ToDecimal(dr["CurrencyId"].ToString());
                            //decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchange);
                            //decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                            //// decConvertRate = decConvertRate + (decAmount * decSelectedCurrencyRate);     // Urefe add comment to stop duplication of values 20161101
                            //decConvertRate = (decAmount * decSelectedCurrencyRate);
                        }
                    }

                    //if (decCredit == 0)
                    //{
                    //    infoLedgerPosting.Credit = 0;
                    //    infoLedgerPosting.Debit = decConvertRate;
                    //}
                    //else
                    //{
                    //    infoLedgerPosting.Debit = 0;
                    //    infoLedgerPosting.Credit = decConvertRate;
                    //}
                    infoLedgerPosting.Credit = journalDetailsInfo.Credit;
                    infoLedgerPosting.Debit = journalDetailsInfo.Debit;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                    infoLedgerPosting.LedgerId = 12;
                    foreach (DataRow dr in dtblPartyBalance.Rows)
                    {
                        //if (Convert.ToDecimal(dgvJournalVoucher.Rows[inA].Cells["dgvcmbAccountLedger"].Value.ToString()) == Convert.ToDecimal(dr["LedgerId"].ToString()))
                        //{
                        //    if (dr["ReferenceType"].ToString() == "Against")
                        //    {
                        //        decNewExchangeRateId = Convert.ToDecimal(dr["CurrencyId"].ToString());
                        //        decNewExchangeRate = SpExchangRate.GetExchangeRateByExchangeRateId(decNewExchangeRateId);
                        //        decOldExchangeId = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                        //        decOldExchange = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchangeId);
                        //        decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                        //        decimal decForexAmount = (decAmount * decNewExchangeRate) - (decAmount * decOldExchange);
                        //        if (dr["DebitOrCredit"].ToString() == "Dr")
                        //        {
                        //            if (decForexAmount >= 0)
                        //            {

                        //                infoLedgerPosting.Debit = decForexAmount;
                        //                infoLedgerPosting.Credit = 0;
                        //            }
                        //            else
                        //            {
                        //                infoLedgerPosting.Credit = -1 * decForexAmount;
                        //                infoLedgerPosting.Debit = 0;
                        //            }
                        //        }
                        //        else
                        //        {
                        //            if (decForexAmount >= 0)
                        //            {

                        //                infoLedgerPosting.Credit = decForexAmount;
                        //                infoLedgerPosting.Debit = 0;
                        //            }
                        //            else
                        //            {
                        //                infoLedgerPosting.Debit = -1 * decForexAmount;
                        //                infoLedgerPosting.Credit = 0;
                        //            }
                        //        }
                        //        spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                        //    }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "JV20:" + ex.Message;
            }
        }

        public bool DetailsLedgerPosting(int inA, decimal decreceiptDetailsId, DateTime chequeDate, string chequeNo, ReceiptMasterInfo mast, ReceiptDetailsInfo det, List<PartyBalanceInfo> partyBalanceDetails)
        {
            bool isSaved = false;
            LedgerPostingInfo InfoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
            ExchangeRateSP SpExchangRate = new ExchangeRateSP();
            SettingsSP spSettings = new SettingsSP();
            decimal decOldExchange = 0;
            decimal decNewExchangeRate = 0;
            decimal decNewExchangeRateId = 0;
            decimal decOldExchangeId = 0;
            decimal decJournalVoucherTypeId = 6;
            // decConvertRate = 0;
            try
            {
                //if (!dgvReceiptVoucher.Rows[inA].Cells["dgvtxtAmount"].ReadOnly)
                //{

                //decimal d = Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbCurrency"].Value.ToString());
                //InfoLedgerPosting.LedgerId = Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbAccountLedger"].Value.ToString());

                //decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbCurrency"].Value.ToString()));
                //decAmount = Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvtxtAmount"].Value.ToString());
                //decConvertRate = decAmount * decSelectedCurrencyRate;

                //InfoLedgerPosting.Date = input.Date;
                //InfoLedgerPosting.Debit = 0;
                //InfoLedgerPosting.Credit = input.Credit;
                //InfoLedgerPosting.DetailsId = decreceiptDetailsId;
                //InfoLedgerPosting.Extra1 = string.Empty;
                //InfoLedgerPosting.Extra2 = string.Empty;
                //InfoLedgerPosting.InvoiceNo = input.InvoiceNo;
                //if (dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value != null && dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value.ToString() != string.Empty)
                //{
                //    InfoLedgerPosting.ChequeNo = dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value.ToString();
                //    if (dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value != null && dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value.ToString() != string.Empty)
                //    {
                //        InfoLedgerPosting.ChequeDate = Convert.ToDateTime(dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value.ToString());
                //    }
                //    else
                //        InfoLedgerPosting.ChequeDate = DateTime.Now;
                //}
                //else
                //{
                //    InfoLedgerPosting.ChequeNo = string.Empty;
                //    InfoLedgerPosting.ChequeDate = DateTime.Now;
                //}


                //InfoLedgerPosting.VoucherNo = strVoucherNo;

                //InfoLedgerPosting.VoucherTypeId = decReceiptVoucherTypeId;
                //InfoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
                //}
                //else
                //{
                InfoLedgerPosting.Date = mast.Date;
                InfoLedgerPosting.Extra1 = string.Empty;
                InfoLedgerPosting.Extra2 = string.Empty;
                InfoLedgerPosting.InvoiceNo = mast.InvoiceNo;
                InfoLedgerPosting.VoucherTypeId = decJournalVoucherTypeId;
                InfoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                InfoLedgerPosting.Debit = 0;
                InfoLedgerPosting.LedgerId = det.LedgerId;
                InfoLedgerPosting.VoucherNo = mast.VoucherNo;
                InfoLedgerPosting.DetailsId = decreceiptDetailsId;
                if (chequeDate != null && (chequeNo != "" || chequeNo != null))
                {
                    InfoLedgerPosting.ChequeDate = chequeDate;
                    InfoLedgerPosting.ChequeNo = chequeNo;
                }
                else
                {
                    InfoLedgerPosting.ChequeNo = "";
                    InfoLedgerPosting.ChequeDate = DateTime.Now;
                }

                //foreach (DataRow dr in dtblPartyBalance.Rows)
                //{
                //    if (InfoLedgerPosting.LedgerId == Convert.ToDecimal(dr["LedgerId"].ToString()))
                //    {
                //        decOldExchange = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                //        decNewExchangeRateId = Convert.ToDecimal(dr["CurrencyId"].ToString());
                //        decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchange);
                //        decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                //        //decConvertRate = decConvertRate + (decAmount * decSelectedCurrencyRate);      //old implementation takes continual sum of all rows rather than for each row 20161228
                //        decConvertRate = (decAmount * decSelectedCurrencyRate);
                //    }
                //}
                InfoLedgerPosting.Credit = partyBalanceDetails[inA].Credit;
                if (SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting) > 0)
                {
                    isSaved = true;
                }
                else
                {
                    isSaved = false;
                }

                InfoLedgerPosting.LedgerId = 12;
                InfoLedgerPosting.DetailsId = 0;
                //foreach (DataRow dr in dtblPartyBalance.Rows)
                for (int v = 0; v < partyBalanceDetails.Count; v++)
                {
                    //if (Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbAccountLedger"].Value.ToString()) == Convert.ToDecimal(dr["LedgerId"].ToString()))
                    if (partyBalanceDetails[v].ReferenceType == "Against")
                    {
                        if (partyBalanceDetails[inA].Credit >= 0)
                        {

                            InfoLedgerPosting.Credit = partyBalanceDetails[inA].Credit;
                            InfoLedgerPosting.Debit = 0;
                        }
                        else
                        {
                            InfoLedgerPosting.Debit = -1 * partyBalanceDetails[inA].Credit;
                            InfoLedgerPosting.Credit = 0;
                        }
                        if (SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting) > 0)
                        {
                            isSaved = true;
                        }
                        else
                        {
                            isSaved = false;
                        }
                        //if (dr["ReferenceType"].ToString() == "Against")
                        //{
                        //    decNewExchangeRateId = Convert.ToDecimal(dr["CurrencyId"].ToString());
                        //    decNewExchangeRate = SpExchangRate.GetExchangeRateByExchangeRateId(decNewExchangeRateId);
                        //    decOldExchangeId = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                        //    decOldExchange = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchangeId);
                        //    decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                        //    decimal decForexAmount = (decAmount * decNewExchangeRate) - (decAmount * decOldExchange);
                        //    if (decForexAmount >= 0)
                        //    {

                        //        InfoLedgerPosting.Credit = decForexAmount;
                        //        InfoLedgerPosting.Debit = 0;
                        //    }
                        //    else
                        //    {
                        //        InfoLedgerPosting.Debit = -1 * decForexAmount;
                        //        InfoLedgerPosting.Credit = 0;
                        //    }
                        //    SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
                        //}
                    }

                }


                // }

            }
            catch (Exception ex)
            {
                // formMDI.infoError.ErrorString = "RV23:" + ex.Message;
            }
            return isSaved;
        }
    }

    public class PartyBalanceInvoicesVM
    {
        public decimal LedgerId { get; set; }
        public string DebitOrCredit { get; set; }
        public decimal VoucherTypeId { get; set; }
        public string voucherNo { get; set; }
    }

    public class SaveJournalVM
    {
        public JournalMasterInfo JournalMasterInfo { get; set; }
        public List<JournalDetailsInfo> JournalDetailsInfo { get; set; }
        public List<PartyBalanceInfo> PartyBalanceInfo { get; set; }
    }        
}
