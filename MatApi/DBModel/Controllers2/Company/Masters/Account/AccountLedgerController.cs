﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MatDal;
using System.Web.Http.Cors;
using MatApi.Models.Company.Masters.Account;
using MATFinancials;
using MatApi.Models;
using System.Dynamic;

namespace MatApi.Controllers.Company.Masters.Account
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AccountLedgerController : ApiController
    {
        AccountLedgerSP accountLedgerSp;
        decimal decLedgerId;
        decimal decOpeningBalance = 0;
        AccountLedgerViewModel acc;
        bool isSundryDebtorOrCreditor = false; // To indicate whether the selected accontgroup is under sundrydebtor or creditor
        bool isBankAccount = false; // To indicate whether the selected accontgroup is under BankAccount or BankODAccount
        string strGroup;
        string strBankAccount;

        public AccountLedgerController()
        {
            accountLedgerSp = new AccountLedgerSP();
            acc = new AccountLedgerViewModel();
        }

        public HttpResponseMessage GetAccountLedgers()
        {
            DataTable dt = accountLedgerSp.AccountLedgerSearch("All", "", MatApi.Models.PublicVariables.FromDate, MatApi.Models.PublicVariables.ToDate);
            return Request.CreateResponse(HttpStatusCode.OK, dt);
        }
        
        private decimal? getLedgerBalance(List<tbl_LedgerPosting> ledgerPosting)
        {
            decimal? crBalance = 0;
            decimal? drBalance = 0;
            foreach(var posting in ledgerPosting)
            {
                crBalance +=posting.credit;
                drBalance += posting.debit;
            }
            return crBalance-drBalance;
        }
        
        public HttpResponseMessage GetLedgerDetails(decimal ledgerId)
        {
            var ledger = accountLedgerSp.AccountLedgerViewForEdit(ledgerId);
            AccountLedgerDetailsViewModel ledgerObj = new AccountLedgerDetailsViewModel
            {
                AccountLedgerInfo = ledger,
                CustomerAccountDetails = accountLedgerSp.GetLedgerDetailsFromSelectedCustomer(MatApi.Models.PublicVariables.FromDate, MatApi.Models.PublicVariables.ToDate, ledgerId),
                FromDate = MatApi.Models.PublicVariables.FromDate,
                ToDate = MatApi.Models.PublicVariables.ToDate,
                AccountGroup = new AccountGroupSP().AccountGroupView(ledger.AccountGroupId)
            };
            return Request.CreateResponse(HttpStatusCode.OK, ledgerObj);
        }

        [HttpPost]
        public HttpResponseMessage CreateAccountLedger(AccountLedgerViewModel acct)
        {
            dynamic response = new ExpandoObject();

            try
            {

                DataTable dtAccountGroup = new DataTable();
                AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                DataTable dtbl = new DataTable();

                isSundryDebtorOrCreditor = false;
                isBankAccount = false;
                dtbl = spAccountLedger.AccountLedgerForSecondaryDetails();
                for (int ini = 0; ini < dtbl.Rows.Count; ini++)
                {
                    strGroup = dtbl.Rows[ini].ItemArray[0].ToString();
                    if (strGroup == acct.AccountLedgerInfo.AccountGroupId.ToString())
                    {
                        isSundryDebtorOrCreditor = true;
                    }
                }
                dtbl = spAccountLedger.AccountLedgerForBankDetails();
                for (int ini = 0; ini < dtbl.Rows.Count; ini++)
                {
                    strBankAccount = dtbl.Rows[ini].ItemArray[0].ToString();
                    if (strBankAccount == acct.AccountLedgerInfo.ToString())
                    {
                        isBankAccount = true;
                    }
                }

                acc.IsBankAccount = isBankAccount;
                acc.IsSundryDebtorOrCreditor = isSundryDebtorOrCreditor;

                infoAccountLedger.LedgerName = acct.AccountLedgerInfo.LedgerName;
                infoAccountLedger.AccountGroupId = Convert.ToDecimal(acct.AccountLedgerInfo.AccountGroupId);
                decOpeningBalance = acct.AccountLedgerInfo.OpeningBalance;
                infoAccountLedger.OpeningBalance = decOpeningBalance;
                infoAccountLedger.CrOrDr = acct.AccountLedgerInfo.CrOrDr;
                infoAccountLedger.Narration = acct.AccountLedgerInfo.Narration;
                infoAccountLedger.IsDefault = false;
                infoAccountLedger.IsActive = acct.AccountLedgerInfo.IsActive;
                var codePrefix = (from k in dtAccountGroup.AsEnumerable()
                                  where k.Field<decimal>("accountGroupId") == Convert.ToDecimal(acct.AccountLedgerInfo.AccountGroupId)
                                  select k.Field<string>("extra1")).ToArray();
                if (acct.IsBankAccount)
                {
                    infoAccountLedger.BankAccountNumber = acct.AccountLedgerInfo.BankAccountNumber;
                    infoAccountLedger.BranchName = acct.AccountLedgerInfo.BranchName;
                    infoAccountLedger.BranchCode = acct.AccountLedgerInfo.BranchCode;
                }
                else
                {
                    if (acct.IsSundryDebtorOrCreditor)
                    {
                        infoAccountLedger.BankAccountNumber = acct.AccountLedgerInfo.BankAccountNumber;
                        infoAccountLedger.BranchName = string.Empty;
                        infoAccountLedger.BranchCode = string.Empty;
                    }
                    else
                    {
                        infoAccountLedger.BankAccountNumber = string.Empty;
                        infoAccountLedger.BranchName = string.Empty;
                        infoAccountLedger.BranchCode = string.Empty;
                    }
                }
                if (acct.IsSundryDebtorOrCreditor)
                {
                    infoAccountLedger.MailingName = acct.AccountLedgerInfo.MailingName;
                    infoAccountLedger.BankAccountNumber = acct.AccountLedgerInfo.BankAccountNumber;
                    infoAccountLedger.Address = acct.AccountLedgerInfo.Address;
                    infoAccountLedger.Phone = acct.AccountLedgerInfo.Phone;
                    infoAccountLedger.Mobile = acct.AccountLedgerInfo.Mobile;
                    infoAccountLedger.Email = acct.AccountLedgerInfo.Email;
                    infoAccountLedger.CreditPeriod = Convert.ToInt32(acct.AccountLedgerInfo.CreditPeriod);
                    infoAccountLedger.CreditLimit = Convert.ToDecimal(acct.AccountLedgerInfo.CreditLimit);
                    if (acct.AccountLedgerInfo.PricinglevelId <= 0)
                    {
                        infoAccountLedger.PricinglevelId = 1;
                    }
                    else
                    {
                        infoAccountLedger.PricinglevelId = Convert.ToDecimal(acct.AccountLedgerInfo.PricinglevelId);
                    }
                    infoAccountLedger.BillByBill = acct.AccountLedgerInfo.BillByBill;
                    infoAccountLedger.Tin = acct.AccountLedgerInfo.Tin;
                    infoAccountLedger.Cst = acct.AccountLedgerInfo.Cst;
                    infoAccountLedger.Pan = acct.AccountLedgerInfo.Pan;
                    if (acct.AccountLedgerInfo.AreaId <= 0)
                    {
                        infoAccountLedger.AreaId = 1;
                    }
                    else
                    {
                        infoAccountLedger.AreaId = Convert.ToDecimal(acct.AccountLedgerInfo.AreaId);
                    }
                    if (acct.AccountLedgerInfo.RouteId <= 0)
                    {
                        infoAccountLedger.RouteId = 1;
                    }
                    else
                    {
                        infoAccountLedger.RouteId = Convert.ToDecimal(acct.AccountLedgerInfo.RouteId);
                    }
                    if (!string.IsNullOrWhiteSpace(acct.AccountLedgerInfo.Extra1))
                    {
                        infoAccountLedger.Extra1 = codePrefix[0].ToUpper() != "NULL" ? codePrefix[0] + acct.AccountLedgerInfo.Extra1 : acct.AccountLedgerInfo.Extra1;
                    }
                    else
                    {
                        infoAccountLedger.Extra1 = string.Empty;
                    }
                    infoAccountLedger.Extra2 = string.Empty;
                    infoAccountLedger.ExtraDate = Models.PublicVariables.ToDate;
                }
                else
                {
                    infoAccountLedger.MailingName = string.Empty;
                    infoAccountLedger.BankAccountNumber = string.Empty;
                    infoAccountLedger.Address = string.Empty;
                    infoAccountLedger.State = string.Empty;
                    infoAccountLedger.Phone = string.Empty;
                    infoAccountLedger.Mobile = string.Empty;
                    infoAccountLedger.Email = string.Empty;
                    infoAccountLedger.CreditPeriod = 0;
                    infoAccountLedger.CreditLimit = 0;
                    infoAccountLedger.PricinglevelId = 0;
                    infoAccountLedger.BillByBill = false;
                    infoAccountLedger.Tin = string.Empty;
                    infoAccountLedger.Cst = string.Empty;
                    infoAccountLedger.Pan = string.Empty;
                    infoAccountLedger.RouteId = 1;
                    infoAccountLedger.AreaId = 1;
                    if (!string.IsNullOrWhiteSpace(acct.AccountLedgerInfo.Extra1))
                    {
                        infoAccountLedger.Extra1 = codePrefix[0].ToUpper() != "NULL" ? codePrefix[0] + acct.AccountLedgerInfo.Extra1 : acct.AccountLedgerInfo.Extra1;
                    }
                    else
                    {
                        infoAccountLedger.Extra1 = string.Empty;
                    }
                    infoAccountLedger.Extra2 = string.Empty;
                    infoAccountLedger.ExtraDate = Models.PublicVariables.ToDate;
                }
                if (spAccountLedger.AccountLedgerCheckExistence(acct.AccountLedgerInfo.LedgerName,0) == false)
                {
                    decLedgerId = spAccountLedger.AccountLedgerAddWithIdentity(infoAccountLedger);
                    if(decLedgerId>0)
                    {
                        response.LedgerCreation = "SUCCESS";
                    }
                    else
                    {
                        response.LedgerCreation = "FAILED";
                    }
                    if (decOpeningBalance > 0)
                    {
                        if(LedgerPostingAdd(acct)>0)
                        {
                            response.LedgerPostingAdd = "SUCCESS";
                        }
                        else
                        {
                            response.LedgerPostingAdd = "FAILED";
                        }
                        if (acct.AccountLedgerInfo.BillByBill && acct.IsSundryDebtorOrCreditor)
                        {
                            if(PartyBalanceAdd(acct)>0)
                            {
                                response.PartyBalanceAdd = "SUCCESS";
                            }
                            else
                            {
                                response.PartyBalanceAdd = "FAILED";
                            }
                        }
                    }
                }
                else
                {
                    response.LedgerCreation= "LEDGER_EXISTS";
                }
            }
            catch (Exception ex)
            { 
                response= ex.Message;
            }

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        /// <summary>
        ///Function to save ledgerposting incase of opening balance
        /// </summary>
        public int LedgerPostingAdd(AccountLedgerViewModel acct)
        {
            try
            {
                string strfinancialId;
                decOpeningBalance = Convert.ToDecimal(acct.AccountLedgerInfo.OpeningBalance);
                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                FinancialYearSP spFinancialYear = new FinancialYearSP();
                FinancialYearInfo infoFinancialYear = new FinancialYearInfo();
                infoFinancialYear = spFinancialYear.FinancialYearViewForAccountLedger(1);
                strfinancialId = infoFinancialYear.FromDate.ToString("dd-MMM-yyyy");
                infoLedgerPosting.VoucherTypeId = 1;
                infoLedgerPosting.Date = Convert.ToDateTime(strfinancialId.ToString());
                infoLedgerPosting.LedgerId = decLedgerId;
                infoLedgerPosting.VoucherNo = decLedgerId.ToString();
                if (acc.AccountLedgerInfo.CrOrDr == "Dr")
                {
                    infoLedgerPosting.Debit = decOpeningBalance;
                }
                else
                {
                    infoLedgerPosting.Credit = decOpeningBalance;
                }
                infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.InvoiceNo = decLedgerId.ToString();
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                return spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        ///Function to save opening balance to party balance
        /// </summary> 
        public int PartyBalanceAdd(AccountLedgerViewModel acct)
        {
            try
            {
                PartyBalanceInfo infoPatryBalance = new PartyBalanceInfo();
                PartyBalanceSP spPartyBalanceAdd = new PartyBalanceSP();
                AccountLedgerSP spLedger = new AccountLedgerSP();
                ExchangeRateSP spExchangeRate = new ExchangeRateSP();
                if (decOpeningBalance > 0)
                {
                    if (acct.AccountLedgerInfo.BillByBill)
                    {
                        infoPatryBalance.Date = MATFinancials.PublicVariables._dtCurrentDate;
                        infoPatryBalance.LedgerId = decLedgerId;
                        infoPatryBalance.VoucherTypeId = 1;
                        infoPatryBalance.VoucherNo = decLedgerId.ToString();
                        infoPatryBalance.AgainstVoucherTypeId = 0;
                        infoPatryBalance.AgainstVoucherNo = "0";
                        infoPatryBalance.ReferenceType = "New";
                        if (acct.AccountLedgerInfo.CrOrDr == "Dr")
                        {
                            infoPatryBalance.Debit = decOpeningBalance;
                            infoPatryBalance.Credit = 0;
                        }
                        else
                        {
                            infoPatryBalance.Debit = 0;
                            infoPatryBalance.Credit = decOpeningBalance;
                        }
                        infoPatryBalance.InvoiceNo = "0";
                        infoPatryBalance.AgainstInvoiceNo = "0";
                        infoPatryBalance.CreditPeriod = 0;
                        infoPatryBalance.ExchangeRateId = spExchangeRate.ExchangerateViewByCurrencyId(MATFinancials.PublicVariables._decCurrencyId);
                        infoPatryBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                        infoPatryBalance.Extra1 = string.Empty;
                        infoPatryBalance.Extra2 = string.Empty;
                    }
                    return spPartyBalanceAdd.PartyBalanceAdd(infoPatryBalance);
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 0;
        }
    }
}