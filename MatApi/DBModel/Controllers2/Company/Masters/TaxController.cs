﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Company.Masters
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TaxController : ApiController
    {
        TaxSP taxSp;
        public TaxController()
        {
            taxSp = new TaxSP();
        }

        public List<TaxInfo> GetTaxes()
        {
            var taxesDt = taxSp.TaxViewAll();
            List<TaxInfo> taxes = new List<TaxInfo>();
            foreach (DataRow row in taxesDt.Rows)
            {
                taxes.Add(new TaxInfo
                {
                    TaxId = Convert.ToDecimal(row["TaxId"]),
                    Rate = Convert.ToDecimal(row["Rate"]),
                    ApplicableOn = row["ApplicableOn"].ToString(),
                    TaxName = row["TaxName"].ToString(),
                    CalculatingMode = row["CalculatingMode"].ToString(),
                    IsActive = Convert.ToBoolean(row["IsActive"]),
                    type = row["type"].ToString()
                });
            }
            return taxes;
        }

        public TaxInfo GetTax(decimal taxId)
        {
            var taxDt = taxSp.TaxView(taxId);
            TaxInfo tax = new TaxInfo
            {
                TaxId = Convert.ToDecimal(taxDt.TaxId),
                Rate = Convert.ToDecimal(taxDt.Rate),
                ApplicableOn = taxDt.ApplicableOn.ToString(),
                TaxName = taxDt.TaxName.ToString(),
                CalculatingMode = taxDt.CalculatingMode.ToString(),
                IsActive = Convert.ToBoolean(taxDt.IsActive),
                type = taxDt.type.ToString()

            };
            return tax;
        }

        [HttpGet]
        public bool DeleteTax(decimal taxId,decimal ledgerId)
        {
            if (taxSp.TaxReferenceDelete(taxId,ledgerId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddTax(TaxInfo tax)
        {
            tax.ExtraDate = DateTime.Now;
            return taxSp.TaxAdd(tax);
        }
        [HttpPost]
        public bool EditCity(TaxInfo tax)
        {
            tax.ExtraDate = DateTime.Now;
            return taxSp.TaxEdit(tax);
        }
    }
}
