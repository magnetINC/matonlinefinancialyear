﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials;
using System.Data;

namespace MatApi.Controllers.Company.Masters
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StateController : ApiController
    {
        AreaSP areaSP;
        AreaInfo areaInfo;
        public StateController()
        {
            areaSP = new AreaSP();
            areaInfo = new AreaInfo();
        }

        public List<AreaInfo> GetStates()
        { 
            var areasDt= areaSP.AreaViewAll();
            List<AreaInfo> areas = new List<AreaInfo>();
            foreach(DataRow row in areasDt.Rows)
            {
                areas.Add(new AreaInfo
                {
                    AreaId = Convert.ToDecimal(row["AreaId"]),
                    AreaName=row["AreaName"].ToString(),
                    Extra1 = row["Extra1"].ToString(),
                    Extra2=row["Extra2"].ToString(),
                    ExtraDate=Convert.ToDateTime(row["ExtraDate"]),
                    Narration=row["Narration"].ToString()
                });
            }
            return areas;
        }

        public AreaInfo GetState(decimal areaId)
        {
            var areaDt = areaSP.AreaView(areaId);
            AreaInfo area = new AreaInfo
            {
                AreaId=Convert.ToDecimal(areaDt.AreaId),
                AreaName=areaDt.AreaName,
                Extra1=areaDt.Extra1,
                Extra2=areaDt.Extra2,
                Narration=areaDt.Narration,
                ExtraDate=areaDt.ExtraDate
            };
            return area;
        }

        [HttpPost]
        public bool DeleteState(AreaInfo area)
        {
            if(areaSP.AreaDeleteReference(area.AreaId)>0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddState(AreaInfo area)
        {
            area.ExtraDate = DateTime.Now;
            if(areaSP.AreaAddWithIdentity(area)>0)
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditState(AreaInfo area)
        {
           return areaSP.AreaEdit(area);
        }
    }
}
