﻿using MatApi.Models;
using MatApi.Models.Supplier;
using MATFinancials;
using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
 
    public class PaymentVoucherController : ApiController
    {
        string strVoucherNo = string.Empty;//to save voucher no into tbl_payment master
        string strInvoiceNo = string.Empty;//to save invoice no into tbl_payment master 
        string tableName = "PaymentMaster";//to get the table name in voucher type selection
        string strCashOrBank;//to get the selected value in cmbBankOrCash at teh time of ledger popup
        string strPrefix = string.Empty;//to get the prefix string from frmvouchertypeselection
        string strSuffix = string.Empty;//to get the suffix string from frmvouchertypeselection
        decimal decPaymentVoucherTypeId = 4;
        decimal creditAmount;
        decimal decDailySuffixPrefixId = 0;


        [HttpGet]
        public HttpResponseMessage LookUps()
        {
            dynamic response = new ExpandoObject();
            try
            {
                var bankOrCash = new TransactionsGeneralFill().BankOrCashComboFill(false);
                var suppliers = new AccountLedgerSP().AccountLedgerViewSuppliersOnly();
                var otherExpenses = new AccountLedgerSP().AccountLedgerViewOtherExpenses();

                response.BankOrCash = bankOrCash;
                response.Suppliers = suppliers;
                response.Expenses = otherExpenses;
            }
            catch (Exception ex)
            {
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
        [HttpGet]
        public decimal GetCurrentBalance(decimal ledgerId)
        {
            decimal Balance = 0;
            MATFinancials.DAL.DBMatConnection _db = new MATFinancials.DAL.DBMatConnection();
            DataSet ds = _db.ExecuteQuery("select isnull(sum(debit-credit),0) as balance from tbl_LedgerPosting where ledgerId ='" + ledgerId + "'");
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                Balance = Convert.ToDecimal(ds.Tables[0].Rows[0]["balance"]);
            }
            return Balance;
        }

        [HttpGet]
        public string getAutoVoucherNo()
        {
           return AutomaticVoucherGeneration();
        }
        private string AutomaticVoucherGeneration()
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            PaymentMasterSP SpPaymentMaster = new PaymentMasterSP();
            if (strVoucherNo == string.Empty)
            {
                strVoucherNo = "0";
            }
            strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPaymentVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
            if (Convert.ToDecimal(strVoucherNo) != SpPaymentMaster.PaymentMasterMax(decPaymentVoucherTypeId) + 1)
            {
                strVoucherNo = SpPaymentMaster.PaymentMasterMax(decPaymentVoucherTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPaymentVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                if (SpPaymentMaster.PaymentMasterMax(decPaymentVoucherTypeId) == 0)
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPaymentVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                }
            }
            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decPaymentVoucherTypeId, DateTime.Now);
            strPrefix = infoSuffixPrefix.Prefix;
            strSuffix = infoSuffixPrefix.Suffix;
            strInvoiceNo = strPrefix + strVoucherNo + strSuffix;

            return strInvoiceNo;
        }

        [HttpGet]
        public HttpResponseMessage getPartyBalanceComboFill(int id, string voucherNo)
        {
            var response = new PartyBalanceSP().
                PartyBalanceComboViewByLedgerId(id, "Dr", decPaymentVoucherTypeId, voucherNo);

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        public void MasterLedgerPosting(decimal decPaymentMasterId, CreatePaymentMaster input)
        {
            try
            {
                LedgerPostingInfo InfoLedgerPosting = new LedgerPostingInfo();
                LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
                ExchangeRateSP SpExchangRate = new ExchangeRateSP();
                InfoLedgerPosting.Credit = creditAmount; // Convert.ToDecimal(txtTotal.Text);
                InfoLedgerPosting.Date = DateTime.Now;
                InfoLedgerPosting.Debit = 0;
                InfoLedgerPosting.DetailsId = decPaymentMasterId;
                InfoLedgerPosting.Extra1 = string.Empty;
                InfoLedgerPosting.Extra2 = string.Empty;
                InfoLedgerPosting.InvoiceNo = input.invoiceNo;
                InfoLedgerPosting.ChequeNo = input.ChequeNo;
                InfoLedgerPosting.ChequeDate = input.ChequeDate;
                InfoLedgerPosting.LedgerId = input.LedgerId;
                InfoLedgerPosting.VoucherNo = input.VoucherNo;
                InfoLedgerPosting.VoucherTypeId = decPaymentVoucherTypeId;
                InfoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
            }
            catch (Exception ex)
            {
            }
        }
        [HttpPost]
        public void Save(CreatePaymentMaster input)
        {
            try
            {
                int inB = 0;
                PaymentMasterInfo InfoPaymentMaster = new PaymentMasterInfo();
                PaymentMasterSP SpPaymentMaster = new PaymentMasterSP();
                PaymentDetailsInfo InfoPaymentDetails = new PaymentDetailsInfo();
                PaymentDetailsSP SpPaymentDetails = new PaymentDetailsSP();
                PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                DateValidation objVal = new DateValidation();
                var decSelectedCurrencyRate = new ExchangeRateSP().GetExchangeRateByExchangeRateId(1);//Exchange rate of grid's row
                
                int inTableRowCount = input.PartyBalances.Count();

                InfoPaymentMaster.Date = input.Date;
                InfoPaymentMaster.Extra1 = "";
                InfoPaymentMaster.Extra2 = "";
                InfoPaymentMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                InfoPaymentMaster.InvoiceNo = input.invoiceNo;
                InfoPaymentMaster.LedgerId = input.LedgerId;
                InfoPaymentMaster.Narration = input.Narration;
                InfoPaymentMaster.SuffixPrefixId = decDailySuffixPrefixId;
                //decimal decTotalAmount = TotalAmountCalculation();
                InfoPaymentMaster.TotalAmount = (input.TotalAmount * decSelectedCurrencyRate);
                InfoPaymentMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                InfoPaymentMaster.VoucherNo = input.VoucherNo;
                InfoPaymentMaster.VoucherTypeId = decPaymentVoucherTypeId;
                decimal decPaymentMasterId = SpPaymentMaster.PaymentMasterAdd(InfoPaymentMaster);
                if (decPaymentMasterId != 0)
                {
                    
                    creditAmount = input.TotalAmount;
                    //if (txtPaymentAmt.Text != null && txtPaymentAmt.Text.ToString() != string.Empty && Convert.ToDecimal(txtPaymentAmt.Text) > 0)
                    //{
                    //    isWithholdingTaxApplicable = true;
                    //    creditAmount = Convert.ToDecimal(txtPaymentAmt.Text);
                    //}
                    MasterLedgerPosting(decPaymentMasterId, input);
                    //if (dgvPaymentVoucher.Rows[0].Cells["dgvCmbWithHoldingTax"].Value != null
                    //    && dgvPaymentVoucher.Rows[0].Cells["dgvCmbWithHoldingTax"].Value.ToString().Trim() != string.Empty
                    //    && txtWithHoldingTax.Text != string.Empty && Convert.ToDecimal(txtWithHoldingTax.Text) > 0)
                    //{
                    //    decimal taxId = Convert.ToDecimal(dgvPaymentVoucher.Rows[0].Cells["dgvCmbWithHoldingTax"].Value);
                    //    decimal ledgerId = dtblWithholdingTax.AsEnumerable().Where(i => i.Field<decimal>("taxId") == taxId)
                    //    .Select(i => i.Field<decimal>("ledgerId")).FirstOrDefault();
                    //    postLedgerForWithholdingTax(ledgerId);
                    //    postWithholdingTaxDetails(true, false, false);
                    //}
                }
                for (int inI = 0; inI <= input.PaymentDetails.Count; inI++)
                {
                    InfoPaymentDetails.Amount = input.PaymentDetails[inI].Amount;
                    InfoPaymentDetails.grossAmount = 0;
                    InfoPaymentDetails.ExchangeRateId = input.PaymentDetails[inI].ExchangeRateId;
                    InfoPaymentDetails.Extra1 = string.Empty;
                    InfoPaymentDetails.Extra2 = string.Empty;
                    InfoPaymentDetails.ProjectId = 1;
                    InfoPaymentDetails.CategoryId = 1;
                    InfoPaymentDetails.LedgerId = input.PaymentDetails[inI].LedgerId;
                    InfoPaymentDetails.PaymentMasterId = decPaymentMasterId;
                    InfoPaymentDetails.ChequeNo = input.ChequeNo;
                    InfoPaymentDetails.ChequeDate = input.ChequeDate;
                    InfoPaymentDetails.Memo = input.PaymentDetails[inI].Memo;
                    InfoPaymentDetails.withholdingTaxId = input.PaymentDetails[inI].TaxId;
                    InfoPaymentDetails.withholdingTaxAmount = input.PaymentDetails[inI].TaxAmount;

                    decimal decPaymentDetailsId = SpPaymentDetails.PaymentDetailsAdd(InfoPaymentDetails);
                    if (decPaymentDetailsId != 0)
                    {
                        for (int inJ = 0; inJ < inTableRowCount; inJ++)
                        {
                            if (input.PaymentDetails[inI].LedgerId == Convert.ToDecimal(input.PartyBalances[inJ].LedgerId.ToString()))
                            {
                                PartyBalanceAddOrEdit(inJ, input);
                            }
                        }
                        inB++;
                        DetailsLedgerPosting(inI, decPaymentDetailsId, input);
                    }
                }
            }
            catch (Exception ex)
            {
            }
        }
        public void PartyBalanceAddOrEdit(int inJ, CreatePaymentMaster input)
        {
            try
            {
                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                PartyBalanceSP spPartyBalance = new PartyBalanceSP();

                InfopartyBalance.Credit = 0;
                InfopartyBalance.CreditPeriod = 0;
                InfopartyBalance.Date = DateTime.Now;
                InfopartyBalance.Debit = Convert.ToDecimal(input.PartyBalances[inJ].Amount);
                InfopartyBalance.ExchangeRateId = 1;
                InfopartyBalance.Extra1 = string.Empty;
                InfopartyBalance.Extra2 = string.Empty;
                InfopartyBalance.ExtraDate = DateTime.Now;
                InfopartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                InfopartyBalance.LedgerId = Convert.ToDecimal(input.PartyBalances[inJ].LedgerId);
                InfopartyBalance.ReferenceType = input.PartyBalances[inJ].ReferenceType.ToString();
                if (input.PartyBalances[inJ].ReferenceType.ToString() == "New" || input.PartyBalances[inJ].ReferenceType.ToString() == "OnAccount")
                {
                    InfopartyBalance.AgainstInvoiceNo = "0";
                    InfopartyBalance.AgainstVoucherNo = "0";
                    InfopartyBalance.AgainstVoucherTypeId = 0;
                    InfopartyBalance.VoucherTypeId = decPaymentVoucherTypeId;
                    InfopartyBalance.InvoiceNo = input.invoiceNo;
                    InfopartyBalance.VoucherNo = input.VoucherNo;
                }
                else
                {
                    InfopartyBalance.ExchangeRateId = 1;
                    InfopartyBalance.AgainstInvoiceNo = input.invoiceNo;
                    InfopartyBalance.AgainstVoucherNo = input.VoucherNo;
                    InfopartyBalance.AgainstVoucherTypeId = decPaymentVoucherTypeId;
                    InfopartyBalance.VoucherTypeId = Convert.ToDecimal(input.PartyBalances[inJ].VoucherTypeId.ToString());
                    InfopartyBalance.VoucherNo = input.PartyBalances[inJ].VoucherNo.ToString();
                    InfopartyBalance.InvoiceNo = input.PartyBalances[inJ].InvoiceNo.ToString();
                }
                if (input.PartyBalances[inJ].PartyBalanceId.ToString() == "0")
                {
                    spPartyBalance.PartyBalanceAdd(InfopartyBalance);
                }
                else
                {
                    InfopartyBalance.PartyBalanceId = Convert.ToDecimal(input.PartyBalances[inJ].PartyBalanceId.ToString());
                    spPartyBalance.PartyBalanceEdit(InfopartyBalance);
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void DetailsLedgerPosting(int inA, decimal decPaymentDetailsId, CreatePaymentMaster input)
        {
            LedgerPostingInfo InfoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
            ExchangeRateSP SpExchangRate = new ExchangeRateSP();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();


            DataTable dtblPartyBalance = new DataTable();
            dtblPartyBalance = spPartyBalance.PartyBalanceViewByVoucherNoAndVoucherType(decPaymentVoucherTypeId, input.VoucherNo, DateTime.Now);
            int inTableRowCount = dtblPartyBalance.Rows.Count;

            decimal decOldExchange = 0;
            decimal decNewExchangeRate = 0;
            decimal decNewExchangeRateId = 0;
            decimal decOldExchangeId = 0;
            decimal decSelectedCurrencyRate = 0;
            decimal decAmount = 0;
            decimal decConvertRate = 0;
            try
            {
                if (input.PaymentDetails[inA].Status == "OnAccount")
                {
                    decimal d = input.PaymentDetails[inA].ExchangeRateId;
                    decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(d);
                    decAmount = input.PaymentDetails[inA].Amount;
                    decConvertRate = decAmount * decSelectedCurrencyRate; // isWithholdingTaxApplicable == true ? Convert.ToDecimal(txtPaymentAmt.Text) * decSelectedCurrencyRate : decAmount * decSelectedCurrencyRate;
                    InfoLedgerPosting.Credit = 0;
                    InfoLedgerPosting.Date = input.Date;
                    InfoLedgerPosting.Debit = decConvertRate;
                    InfoLedgerPosting.DetailsId = decPaymentDetailsId;
                    InfoLedgerPosting.Extra1 = string.Empty;
                    InfoLedgerPosting.Extra2 = string.Empty;
                    InfoLedgerPosting.InvoiceNo = input.invoiceNo;
                    InfoLedgerPosting.ChequeNo = input.ChequeNo;
                    InfoLedgerPosting.ChequeDate = input.ChequeDate;
                    //InfoLedgerPosting.ChequeDate = Convert.ToDateTime(txtChqDate.Text.ToString());

                    InfoLedgerPosting.LedgerId = input.PaymentDetails[inA].LedgerId;
                    InfoLedgerPosting.VoucherNo = input.VoucherNo;
                    InfoLedgerPosting.VoucherTypeId = decPaymentVoucherTypeId;
                    InfoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
                }
                else
                {
                    InfoLedgerPosting.Date = input.Date;

                    InfoLedgerPosting.Extra1 = string.Empty;
                    InfoLedgerPosting.Extra2 = string.Empty;
                    InfoLedgerPosting.InvoiceNo = input.invoiceNo;
                    InfoLedgerPosting.VoucherTypeId = decPaymentVoucherTypeId;
                    InfoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    InfoLedgerPosting.Credit = 0;
                    InfoLedgerPosting.LedgerId = input.PaymentDetails[inA].LedgerId;
                    InfoLedgerPosting.VoucherNo = input.VoucherNo;
                    InfoLedgerPosting.DetailsId = decPaymentDetailsId;
                    InfoLedgerPosting.InvoiceNo = input.VoucherNo;
                    InfoLedgerPosting.ChequeNo = input.ChequeNo;
                    InfoLedgerPosting.ChequeDate = input.ChequeDate;
                    foreach (var dr in input.PartyBalances)
                    {
                        if (InfoLedgerPosting.LedgerId == dr.LedgerId)
                        {
                            decOldExchange = 1;
                            decNewExchangeRateId = 1;
                            decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchange);
                            decAmount = Convert.ToDecimal(dr.Amount);
                            decConvertRate = (decAmount * decSelectedCurrencyRate);
                        }
                    }
                    InfoLedgerPosting.Debit = decConvertRate; // isWithholdingTaxApplicable == true ? Convert.ToDecimal(txtPaymentAmt.Text) * decSelectedCurrencyRate : decConvertRate;
                    SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);

                    InfoLedgerPosting.LedgerId = 12;
                    InfoLedgerPosting.DetailsId = 0;
                    foreach (var dr in input.PartyBalances)
                    {
                        if (input.PaymentDetails[inA].LedgerId == Convert.ToDecimal(dr.LedgerId))
                        {
                            if (dr.ReferenceType.ToString() == "Against")
                            {
                                decNewExchangeRateId = 1;
                                decNewExchangeRate = SpExchangRate.GetExchangeRateByExchangeRateId(decNewExchangeRateId);
                                decOldExchangeId = 1;
                                decOldExchange = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchangeId);
                                decAmount = Convert.ToDecimal(dr.Amount);
                                decimal decForexAmount = (decAmount * decNewExchangeRate) - (decAmount * decOldExchange);
                                if (decForexAmount >= 0)
                                {
                                    InfoLedgerPosting.Debit = decForexAmount;
                                    InfoLedgerPosting.Credit = 0;
                                }
                                else
                                {
                                    InfoLedgerPosting.Credit = -1 * decForexAmount;
                                    InfoLedgerPosting.Debit = 0;
                                }
                                SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
                            }
                        }

                    }
                }
            }
            catch (Exception ex)
            {
            }
        }

    }

}
