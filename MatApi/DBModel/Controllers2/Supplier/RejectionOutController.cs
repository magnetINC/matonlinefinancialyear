﻿using MatApi.Models;
using MatApi.Models.Register;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RejectionOutController : ApiController
    {
        decimal decRejectionOutId = 0;
        decimal decRejectionOutVoucherTypeId = 12;
        decimal decRejectionOutSuffixPrefixId = 0;
        string strCashOrParty = string.Empty;
        string strVoucherNo = string.Empty;
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        string strRejectionOutNo = string.Empty;
        string strMaterialReceiptNo = string.Empty;
        string tableName = "RejectionOutMaster";
        decimal decRejectionOutMasterIdentity = 0;
        decimal decRejectionOutDetailsIdentity = 0;
        string strRejectionOutVoucherNo = string.Empty;

        ProductInfo infoProduct = new ProductInfo();
        ProductSP spProduct = new ProductSP();
        RejectionOutDetailsInfo infoRejectionOutDetails = new RejectionOutDetailsInfo();
        RejectionOutMasterInfo infoRejectionOutMaster = new RejectionOutMasterInfo();
        RejectionOutMasterSP spRejectionOutMaster = new RejectionOutMasterSP();
        RejectionOutDetailsSP spRejectionOutDetails = new RejectionOutDetailsSP();
        StockPostingSP spStockPosting = new StockPostingSP();
        MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();

        public RejectionOutController()
        {
            
        }

        [HttpGet]
        public HttpResponseMessage RejectionOutLookups()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var suppliers = transactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            VoucherTypeSP SpVoucherType = new VoucherTypeSP();
            var voucherTypes = SpVoucherType.VoucherTypeSelectionComboFill("Material Receipt");

            dynamic response = new ExpandoObject();
            response.Suppliers = suppliers;
            response.Currencies = currencies;
            response.VoucherTypes = voucherTypes;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpPost]
        public HttpResponseMessage Registers(RejectionOutRegisterVM input)
        {
            var resp = new RejectionOutMasterSP().RejectionOutMasterSearch(input.InvoiceNo, input.LedgerId, input.FromDate, input.ToDate);
            return Request.CreateResponse(HttpStatusCode.OK, (object)resp);
        }

        [HttpGet]
        public HttpResponseMessage MaterialReceiptNumbers(decimal supplierId,decimal applyOn)
        {
            MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();
            DataTable dtbl = new DataTable();
            dtbl = spMaterialReceiptMaster.ShowMaterialReceiptNoForRejectionOut(supplierId, decRejectionOutId, applyOn);
            return Request.CreateResponse(HttpStatusCode.OK,dtbl);
        }

        [HttpGet]
        public List<RejectionOutLineItems2> GetMaterialReceiptDetails(decimal materialReceiptNo)
        {
            MaterialReceiptDetailsSP spMaterialReceiptDetails = new MaterialReceiptDetailsSP();
            DataTable dtblReceiptDetails = spMaterialReceiptDetails.ShowMaterialReceiptDetailsViewbyMaterialReceiptDetailsIdWithPending(Convert.ToDecimal(materialReceiptNo), decRejectionOutId);
            
            List<RejectionOutLineItems2> response = new List<RejectionOutLineItems2>();
            try
            {

                var receipts = dtblReceiptDetails = spMaterialReceiptDetails.ShowMaterialReceiptDetailsViewbyMaterialReceiptDetailsIdWithPending(Convert.ToDecimal(materialReceiptNo), decRejectionOutId);
                for (int i = 0; i < receipts.Rows.Count; i++)
                {
                    var note = receipts.Rows[i].ItemArray;
                    var prod = new ProductSP().ProductView(Convert.ToDecimal(note[2]));

                    response.Add(new RejectionOutLineItems2
                    {
                        Amount = Convert.ToDecimal(note[11]),
                        Barcode = note[10].ToString(),
                        Batch = note[9].ToString(),
                        ProductCode = prod.ProductCode,
                        ProductName = prod.ProductName,
                        Quantity = Convert.ToDecimal(note[3]),
                        Rack = new RackSP().RackView(Convert.ToDecimal(note[8])).RackName,
                        Rate = Convert.ToDecimal(note[4]),
                        Store = new GodownSP().GodownView(Convert.ToDecimal(note[7])).GodownName,
                        Unit = new UnitSP().UnitView(Convert.ToDecimal(note[5])).UnitName
                        
                    });
                }

            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RI12:" + ex.Message;
            }
            return response;
        }

        public string VoucherNumberGeneration(RejectionOutVM input)
        {
            try
            {
                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                RejectionOutMasterSP spRejectionOut = new RejectionOutMasterSP();
                strVoucherNo = "0";
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decRejectionOutVoucherTypeId, Convert.ToDecimal(strVoucherNo), input.Date, tableName);
                if (Convert.ToDecimal(strVoucherNo) != spRejectionOut.RejectionOutMasterGetMaxPlusOne(decRejectionOutVoucherTypeId))
                {
                    strVoucherNo = spRejectionOut.RejectionOutMasterGetMax(decRejectionOutVoucherTypeId).ToString();
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decRejectionOutVoucherTypeId, Convert.ToDecimal(strVoucherNo), input.Date, tableName);
                    if (spRejectionOut.RejectionOutMasterGetMax(decRejectionOutVoucherTypeId) == "0")
                    {
                        strVoucherNo = "0";
                        strVoucherNo = obj.VoucherNumberAutomaicGeneration(decRejectionOutVoucherTypeId, Convert.ToDecimal(strVoucherNo), input.Date, tableName);
                    }
                }
                //if (isAutomatic)
               // {
                    SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                    SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                    infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decRejectionOutVoucherTypeId, input.Date);
                    strPrefix = infoSuffixPrefix.Prefix;
                    strSuffix = infoSuffixPrefix.Suffix;
                    decRejectionOutSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                    strRejectionOutNo = strPrefix + strVoucherNo + strSuffix;
                //}
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RO2:" + ex.Message;
            }
            return strVoucherNo;
        }

        [HttpPost]
        public void SaveOrEditFunction(RejectionOutVM input)
        {
            try
            {
                input.RejectionOutNo = Convert.ToDecimal(VoucherNumberGeneration(input));
                // to take assign voucher number in case automatic voucher numbering is set to off urefe 20161208
                //if (!isAutomatic && txtRejectionOutNo.Text.Trim() != string.Empty)
                //{
                    strVoucherNo = VoucherNumberGeneration(input);
                //}
                decimal decProductId = 0;
                decimal decBatchId = 0;
                decimal decCalcQty = 0;
                StockPostingSP spStockPosting = new StockPostingSP();
                SettingsSP spSettings = new SettingsSP();
                string strStatus = spSettings.SettingsStatusCheck("NegativeStockStatus");
                bool isNegativeLedger = false;
                //int inRowCount = dgvProduct.RowCount;
                foreach(var l in input.LineItems)
                {
                    if (l.ProductId>0)
                    {
                        decProductId = Convert.ToDecimal(l.ProductId);

                        if (l.BatchId>0)
                        {
                            decBatchId = Convert.ToDecimal(l.BatchId);
                        }
                        decimal decCurrentStock = spStockPosting.StockCheckForProductSale(decProductId, decBatchId);
                        if (l.Quantity>0)
                        {
                            decCalcQty = decCurrentStock - Convert.ToDecimal(l.Quantity);
                        }
                        if (decCalcQty < 0)
                        {
                            isNegativeLedger = true;
                            break;
                        }
                    }
                }
                SaveFunction(input);
                //if (isNegativeLedger)
                //{
                //    if (strStatus == "Warn")
                //    {
                //        if (MessageBox.Show("Negative Stock balance exists,Do you want to Continue", "MAT Financials", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                //        {
                //            SaveOrEdit();
                //        }
                //    }
                //    else if (strStatus == "Block")
                //    {
                //        MessageBox.Show("Cannot continue ,due to negative stock balance", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //    }
                //    else
                //    {
                //        SaveOrEdit();
                //    }
                //}
                //else
                //{
                //    SaveOrEdit();
                //}
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RO43:" + ex.Message;
            }
        }

        public void SaveFunction(RejectionOutVM input)
        {
            try
            {

                ProductSP spProduct = new ProductSP();
                SettingsSP Spsetting = new SettingsSP();
                infoRejectionOutMaster.Date = Convert.ToDateTime(input.Date);
                infoRejectionOutMaster.LedgerId = Convert.ToDecimal(input.SupplierId);

                //if (isAutomatic == true)
                //{
                infoRejectionOutMaster.SuffixPrefixId = decRejectionOutSuffixPrefixId;
                infoRejectionOutMaster.VoucherNo = strVoucherNo;
                //}
                //else
                //{
                //    infoRejectionOutMaster.SuffixPrefixId = 0;
                //    infoRejectionOutMaster.VoucherNo = txtRejectionOutNo.Text.Trim();
                //}

                if (input.MaterialReceiptNo>0)
                {
                    infoRejectionOutMaster.MaterialReceiptMasterId = Convert.ToDecimal(input.MaterialReceiptNo);
                }
                else
                {
                    infoRejectionOutMaster.MaterialReceiptMasterId = 0;
                }

                if (input.CurrencyId>0)
                {
                    infoRejectionOutMaster.ExchangeRateId = Convert.ToDecimal(input.CurrencyId);
                }
                infoRejectionOutMaster.VoucherTypeId = decRejectionOutVoucherTypeId;

                infoRejectionOutMaster.InvoiceNo = VoucherNumberGeneration(input);
                infoRejectionOutMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                infoRejectionOutMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoRejectionOutMaster.Narration = input.Narration;
                infoRejectionOutMaster.LrNo = input.LrNo;
                infoRejectionOutMaster.TransportationCompany = input.TransportCompany;
                infoRejectionOutMaster.TotalAmount = Convert.ToDecimal(input.TotalAmount);
                infoRejectionOutMaster.Extra1 = string.Empty;
                infoRejectionOutMaster.Extra2 = string.Empty;
                if (decRejectionOutId == 0)
                {
                    decRejectionOutMasterIdentity = spRejectionOutMaster.RejectionOutMasterAddWithReturnIdentity(infoRejectionOutMaster);
                }
                else
                {
                    infoRejectionOutMaster.RejectionOutMasterId = decRejectionOutId;
                    spRejectionOutMaster.RejectionOutMasterEdit(infoRejectionOutMaster);
                }
                if (decRejectionOutId == 0)
                {
                    infoRejectionOutDetails.RejectionOutMasterId = decRejectionOutMasterIdentity;
                }
                else
                {
                    spRejectionOutDetails.RejectionOutDetailsDeleteByRejectionOutMasterId(decRejectionOutId);
                    spStockPosting.DeleteStockPostingByAgnstVouTypeIdAndAgnstVouNo(decRejectionOutVoucherTypeId, strRejectionOutVoucherNo);
                    infoRejectionOutDetails.RejectionOutMasterId = decRejectionOutId;
                }
                //int inRowcount = dgvProduct.Rows.Count;
                decimal pId = 0;
                foreach(var lineItem in input.LineItems)
                {
                    decimal matDetailsId = 0;
                    string pcode = "";
                    decimal batchId = 0;
                    decimal unitId = 0;
                    decimal storeId = 0;
                    decimal rackId = 0;

                    var mat = new MaterialReceiptDetailsSP().MaterialReceiptDetailsViewByMasterId(input.MaterialReceiptNo);

                    for (var m = 0; m < mat.Rows.Count; m++)
                    {
                        matDetailsId = Convert.ToDecimal(mat.Rows[m].ItemArray[0]);
                        unitId = Convert.ToDecimal(mat.Rows[m].ItemArray[11]);
                        pcode = mat.Rows[m].ItemArray[2].ToString();
                        batchId = Convert.ToDecimal(mat.Rows[m].ItemArray[16]);
                        storeId = Convert.ToDecimal(mat.Rows[m].ItemArray[17]);
                        rackId = Convert.ToDecimal(mat.Rows[m].ItemArray[18]);

                    }
                    //if (dgvProduct.Rows[inI].Cells["dgvtxtSlNo"].Value != null && dgvProduct.Rows[inI].Cells["dgvtxtSlNo"].Value.ToString() != string.Empty)
                    //{
                    infoRejectionOutDetails.Slno = Convert.ToInt32(lineItem.SL);
                    //}
                    if (matDetailsId>0)
                    {
                        infoRejectionOutDetails.MaterialReceiptDetailsId = Convert.ToDecimal(matDetailsId);
                    }
                    else
                    {
                        infoRejectionOutDetails.MaterialReceiptDetailsId = 0;
                    }
                    if (pcode != "")
                    {
                        infoProduct = spProduct.ProductViewByCode(pcode);
                        // infoRejectionOutDetails.ProductId = Convert.ToDecimal(dgvProduct.Rows[inI].Cells["dgvtxtProductCode"].Value.ToString()); //infoProduct.ProductId;
                        pId = Convert.ToDecimal(infoProduct.ProductId); //infoProduct.ProductId;  // added by Precious
                    }
                    if (lineItem.Quantity>0)
                    {
                        infoRejectionOutDetails.Qty = Convert.ToDecimal(lineItem.Quantity);
                    }
                    if (unitId>0)
                    {
                        infoRejectionOutDetails.UnitId = Convert.ToDecimal(unitId);
                        var unitconv = new UnitConvertionSP().UnitViewAllByProductId(infoProduct.ProductId);
                        infoRejectionOutDetails.UnitConversionId = Convert.ToDecimal(unitconv.UnitconvertionId);
                    }
                    //var batchId = new BatchSP().BatchIdViewByProductId(infoProduct.ProductId);
                    if (batchId>0)
                    {
                        infoRejectionOutDetails.BatchId = Convert.ToDecimal(batchId);
                    }
                    if (storeId>0)
                    {
                        infoRejectionOutDetails.GodownId = Convert.ToDecimal(storeId);
                    }
                    if (rackId>0)
                    {
                        infoRejectionOutDetails.RackId = Convert.ToDecimal(rackId);
                    }
                    infoRejectionOutDetails.Rate = Convert.ToDecimal(lineItem.Rate);
                    infoRejectionOutDetails.Amount = Convert.ToDecimal(lineItem.Rate*lineItem.Quantity);
                    infoRejectionOutDetails.ProjectId = Convert.ToInt32(0);
                    infoRejectionOutDetails.CategoryId = Convert.ToInt32(0);
                    infoRejectionOutDetails.Extra1 = string.Empty;
                    infoRejectionOutDetails.Extra2 = string.Empty;
                    decRejectionOutDetailsIdentity = spRejectionOutDetails.RejectionOutDetailsAddWithReturnIdentity(infoRejectionOutDetails);
                    AddStockPosting(pId,input);
                }

                if (decRejectionOutId == 0)
                {
                //    Messages.SavedMessage();
                //    if (cbxPrintAfterSave.Checked)
                //    {
                //        if (Spsetting.SettingsStatusCheck("Printer") == "Dot Matrix")
                //        {
                //            PrintForDotMatrix(decRejectionOutMasterIdentity);
                //        }
                //        else
                //        {
                //            Print(decRejectionOutMasterIdentity);
                //        }
                //    }
                //    clear();
                //}
                //else
                //{
                //    Messages.UpdatedMessage();
                //    if (cbxPrintAfterSave.Checked)
                //    {
                //        if (Spsetting.SettingsStatusCheck("Printer") == "Dot Matrix")
                //        {
                //            PrintForDotMatrix(decRejectionOutId);
                //        }
                //        else
                //        {
                //            Print(decRejectionOutId);
                //        }
                //    }
                //    this.Close();
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RO33:" + ex.Message;
            }
        }

        public decimal AddStockPosting(decimal productId,RejectionOutVM input)
        {
            try
            {
                StockPostingInfo infoStockPosting = new StockPostingInfo();
                StockPostingSP spStockPosting = new StockPostingSP();
                MaterialReceiptMasterInfo infoMaterialReceiptMaster = new MaterialReceiptMasterInfo();
                infoMaterialReceiptMaster = spMaterialReceiptMaster.MaterialReceiptMasterView(Convert.ToDecimal(input.MaterialReceiptNo));
                infoStockPosting.Date = DateTime.Parse(input.Date.ToString());
                infoStockPosting.VoucherTypeId = infoMaterialReceiptMaster.VoucherTypeId;
                infoStockPosting.VoucherNo = infoMaterialReceiptMaster.VoucherNo;
                infoStockPosting.InvoiceNo = infoMaterialReceiptMaster.InvoiceNo;
                //if (isAutomatic)
                //{
                    infoStockPosting.AgainstVoucherNo = strVoucherNo;
                    infoStockPosting.AgainstInvoiceNo = input.RejectionOutNo.ToString();
                //}
                //else
                //{
                //    infoStockPosting.AgainstVoucherNo = input.RejectionOutNo.ToString();
                //    infoStockPosting.AgainstInvoiceNo = txtRejectionOutNo.Text;
                //}
                if (decRejectionOutVoucherTypeId != 0)
                {
                    infoStockPosting.AgainstVoucherTypeId = decRejectionOutVoucherTypeId;
                }
                if (strVoucherNo != string.Empty)
                {
                    infoStockPosting.AgainstVoucherNo = strVoucherNo;
                }
                if (input.RejectionOutNo >0)
                {
                    infoStockPosting.AgainstInvoiceNo = input.RejectionOutNo.ToString();
                }
                infoStockPosting.InwardQty = 0;
                infoStockPosting.OutwardQty = infoRejectionOutDetails.Qty;
                infoStockPosting.ProductId = productId; // infoRejectionOutDetails.ProductId;
                infoStockPosting.BatchId = infoRejectionOutDetails.BatchId;
                infoStockPosting.UnitId = infoRejectionOutDetails.UnitId;
                infoStockPosting.GodownId = infoRejectionOutDetails.GodownId;
                infoStockPosting.RackId = infoRejectionOutDetails.RackId;
                infoStockPosting.Rate = infoRejectionOutDetails.Rate;
                infoStockPosting.ProjectId = infoRejectionOutDetails.ProjectId;
                infoStockPosting.CategoryId = infoRejectionOutDetails.CategoryId;
                infoStockPosting.Extra1 = string.Empty;
                infoStockPosting.Extra2 = string.Empty;
                infoStockPosting.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                return spStockPosting.StockPostingAdd(infoStockPosting);
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RO28:" + ex.Message;
            }
            return 0;
        }
    }
}
