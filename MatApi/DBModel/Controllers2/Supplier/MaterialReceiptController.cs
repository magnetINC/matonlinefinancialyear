using MatApi.Models;
using MatApi.Models.Register;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MaterialReceiptController : ApiController
    {
        string strVoucherNo = string.Empty;//To save the automatically generated voucher number
        decimal decMaterialReceiptVoucherTypeId = 11;
        decimal decDeliveryNoteSuffixPrefixId = 0;//To store the SuffixPrefix Id of the selected voucher type
        decimal decDeliveryNoteMasterId = 0;
        decimal decDelivryNoteIdToEdit = 0;//To take the deliveryNoteMasterId coming from frmDeliveryNoteRegister and frmDeliveryNoteReport
        decimal decMaterialReceiptMasterIdentity = 0;
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        decimal decMaterialReceiptSuffixPrefixId = 0;
        string strReceiptNo = string.Empty;
        string tableName = "MaterialReceiptMaster";
        DataTable dtblDetailsProd = new DataTable();

        public MaterialReceiptController()
        {

        }

        [HttpGet]
        public HttpResponseMessage InvoiceLookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var customers = transactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill();
            var salesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();
            var pricingLevels = transactionGeneralFillObj.PricingLevelViewAll();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            var applyOn = new PurchaseOrderDetailsSP().VoucherTypeCombofillforPurchaseOrderReport();
            var users = new UserSP().UserViewAll();

            dynamic response = new ExpandoObject();
            response.Customers = customers;
            response.SalesMen = salesMen;
            response.PricingLevels = pricingLevels;
            response.Currencies = currencies;
            response.ApplyOn = applyOn;
            response.Users = users;
            response.stores = response.Stores = new GodownSP().GodownViewAll();

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public decimal MaterialReceiptCount()
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("select count(approved) from tbl_MaterialReceiptMaster_Pending where approved ='Pending'");
            var count = conn.getSingleValue(queryStr);

            return Convert.ToDecimal(count);
        }

        [HttpGet]
        public DataTable GetOrderNo(decimal supplierId, decimal voucherTypeId)
        {
            return new PurchaseMasterSP().GetOrderNoCorrespondingtoLedger(supplierId, 0, voucherTypeId);
        }

        [HttpPost]
        public HttpResponseMessage Registers(MaterialReceiptRegisterVM input)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            dynamic response = new ExpandoObject(); ;

            //var response = new MaterialReceiptMasterSP().MaterialReceiptMasterViewAll(input.InvoiceNo, input.LedgerId, input.FromDate, input.ToDate);
            string GetQuery = string.Format("");
            GetQuery = string.Format("SELECT * FROM tbl_MaterialReceiptMaster WHERE date BETWEEN '{0}' AND '{1}' ORDER BY date DESC", input.FromDate, input.ToDate);

            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();
            var supp = transactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill();
            var salesMen = new UserSP().UserViewAll();
            var result = db.GetDataSet(GetQuery);

            response.Suppliers = supp;
            response.User = salesMen;
            response.Data = result;
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage RegisterDetails(decimal id)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string GetDetails = string.Format("");

            GetDetails = string.Format("SELECT * FROM tbl_MaterialReceiptDetails WHERE materialReceiptMasterId={0}", id);

            dynamic response = new ExpandoObject();
            response.units = new UnitSP().UnitViewAll();
            response.products = new ProductSP().ProductViewAll();
            response.detailsfull = db.customSelect(GetDetails);
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage RegisterDetails2(decimal id)
        {
            dynamic response = new ExpandoObject();
            response.getDetails = new MaterialReceiptDetailsSP().MaterialReceiptDetailsViewByMasterId(id);
            response.master = new MaterialReceiptMasterSP().MaterialReceiptMasterViewByReceiptMasterId(id);
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetPendingMaterialReceipt(DateTime fromDate, DateTime toDate, string approved)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            dynamic response = new ExpandoObject(); ;
            string GetQuery = string.Format("");
            if (approved == "All" || approved == "" || approved == null)
            {
                GetQuery = string.Format("");
                GetQuery = string.Format("SELECT * FROM tbl_MaterialReceiptMaster_Pending WHERE date BETWEEN '{0}' AND '{1}' ORDER BY date DESC", fromDate, toDate);
            }
            else
            {
                GetQuery = string.Format("");
                GetQuery = string.Format("SELECT * FROM tbl_MaterialReceiptMaster_Pending WHERE date BETWEEN '{0}' AND '{1}' AND approved='{2}' ORDER BY date DESC", fromDate, toDate, approved);
            }
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();
            var supp = transactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill();
            var salesMen = new UserSP().UserViewAll();
            var result = db.GetDataSet(GetQuery);

            response.Suppliers = supp;
            response.User = salesMen;
            response.Data = result;
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetMaterialReceiptLineItemFromOrderNo(decimal invoiceNo)
        {
            dynamic response = new ExpandoObject();
            dtblDetailsProd = new PurchaseOrderDetailsSP().PurchaseOrderDetailsViewByOrderMasterIdWithRemaining(invoiceNo, 0);
            response.Details = dtblDetailsProd;
            response.Units = new UnitSP().UnitViewAll();
            response.Stores = new GodownSP().GodownViewAll();
            response.Racks = new RackSP().RackViewAll();
            response.Batches = new BatchSP().BatchViewAll();
            response.Taxes = new TaxSP().TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
            //response.ProductDetails = new ProductSP().ProductView(Convert.ToDecimal(dtblDetails.Rows[0].ItemArray[2]));
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetPendingMaterialReceiptDetails(decimal materialReceiptMasterId)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string GetMaster = string.Format("");
            string GetDetails = string.Format("");

            GetMaster = string.Format("SELECT * FROM tbl_MaterialReceiptMaster_Pending WHERE materialReceiptMasterId={0}", materialReceiptMasterId);
            GetDetails = string.Format("SELECT * FROM tbl_MaterialReceiptDetails_Pending WHERE materialReceiptMasterId={0}", materialReceiptMasterId);
            dynamic response = new ExpandoObject();
            var master1 = db.customQuery(GetMaster);
            // var master2 = new PurchaseOrderMasterSP().PurchaseOrderMasterView(materialReceiptPendingMasterId);
            var detailsfull = db.customSelect(GetDetails);
            var units = new UnitSP().UnitViewAll();
            var products = new ProductSP().ProductViewAll();

            response.Master1 = master1;
            //response.Master2 = master2;
            response.FullDetails = detailsfull;
            response.products = products;
            response.units = units;
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public string GetAutoVoucherNo()
        {
            return VoucherNumberGeneration();
        }
        public string VoucherNumberGeneration()
        {
            try
            {
                //SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                //SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                //infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decMaterialReceiptVoucherTypeId, date);
                //strPrefix = infoSuffixPrefix.Prefix;
                //strSuffix = infoSuffixPrefix.Suffix;
                //decMaterialReceiptSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                //strReceiptNo = strPrefix + strVoucherNo + strSuffix;
                //return strReceiptNo;

                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                MaterialReceiptMasterSP spMaterialReceipt = new MaterialReceiptMasterSP();
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decMaterialReceiptVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                if (Convert.ToDecimal(strVoucherNo) != spMaterialReceipt.MaterialReceiptMasterGetMaxPlusOne(decMaterialReceiptVoucherTypeId))
                {
                    strVoucherNo = spMaterialReceipt.MaterialReceiptMasterGetMax(decMaterialReceiptVoucherTypeId).ToString();
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decMaterialReceiptVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                    if (spMaterialReceipt.MaterialReceiptMasterGetMax(decMaterialReceiptVoucherTypeId) == "0")
                    {
                        strVoucherNo = "0";
                        strVoucherNo = obj.VoucherNumberAutomaicGeneration(decMaterialReceiptVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                    }
                }
                strReceiptNo = strPrefix + strVoucherNo + strSuffix;


                //if (isAutomatic)
                //{
                //    SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                //    SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                //    infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decMaterialReceiptVoucherTypeId, dtpDate.Value);
                //    strPrefix = infoSuffixPrefix.Prefix;
                //    strSuffix = infoSuffixPrefix.Suffix;
                //    decMaterialReceiptSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                //    strReceiptNo = strPrefix + strVoucherNo + strSuffix;
                //    txtReceiptNo.Text = strReceiptNo;
                //    txtReceiptNo.ReadOnly = true;
                //}
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "MR1:" + ex.Message;
            }
            return strReceiptNo;
        }

        [HttpPost]
        public decimal SaveOrEdit(CreateMaterialReceiptVM input)
        {
            MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();
            try
            {
                // to take assign voucher number in case automatic voucher numbering is set to off urefe 20161208
                //if (!isAutomatic && txtReceiptNo.Text.Trim() != string.Empty)
                //{
                //    strVoucherNo = txtReceiptNo.Text.Trim();
                //}
                //dgvProduct.ClearSelection();
                //int inRow = dgvProduct.RowCount;
                input.ReceiptNo = VoucherNumberGeneration();
                String strInvoiceNo = input.ReceiptNo;
                if (input.ReceiptNo == string.Empty)
                {
                    //Messages.InformationMessage("Enter voucher number");
                    //txtReceiptNo.Focus();
                }
                else if (spMaterialReceiptMaster.MaterialReceiptNumberCheckExistence(input.ReceiptNo, decMaterialReceiptVoucherTypeId) == true)
                {
                    //Messages.InformationMessage("Receipt number already exist");
                    //txtReceiptNo.Focus();
                }
                SaveFunction(input);

                return 1;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "MR33:" + ex.Message;
                return 0;
            }
        }

        [HttpPost]
        public bool SavePending(CreateMaterialReceiptVM input)
        {
            try
            {
                var master = new PurchaseOrderMasterSP().PurchaseOrderMasterView(input.OrderMasterId);
                MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
                input.Date = DateTime.Now;
                var approved = "Pending";
                string masterQuery = string.Format("INSERT INTO tbl_MaterialReceiptMaster_Pending " +
                                                    "VALUES('{0}','{1}',{2},{3},'{4}',{5},'{6}',{7},'{8}',{9},{10},{11},'{12}',{13},'{14}',{15},'{16}','{17}','{18}', '{19}');"
                                                    , input.ReceiptNo,
                                                    input.ReceiptNo,
                                                    1,
                                                    decMaterialReceiptVoucherTypeId,
                                                    input.Date,
                                                    input.SupplierId,
                                                    "",
                                                    input.OrderMasterId,
                                                    input.Narration,
                                                    input.TotalAmount,
                                                    1,
                                                    MATFinancials.PublicVariables._decCurrentUserId,
                                                    "",
                                                    1,
                                                    "",
                                                    MATFinancials.PublicVariables._decCurrentFinancialYearId,
                                                    DateTime.Now,
                                                    "",
                                                    "",
                                                    approved);

                string detailsQuery = string.Format("");
                if (db.ExecuteNonQuery2(masterQuery))
                {
                    string id = db.getSingleValue("SELECT top 1 materialReceiptMasterId from tbl_MaterialReceiptMaster_Pending order by materialReceiptMasterId desc");
                    foreach (var row in input.LineItems)
                    {
                        var amount = row.Rate * row.Quantity;
                        detailsQuery = string.Format("INSERT INTO tbl_MaterialReceiptDetails_Pending " +
                                                    "VALUES({17},{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},'{11}','{12}','{13}',{14},{15},'{16}')"
                                                    , row.ProductId,
                                                    row.OrderDetailsId,
                                                    row.Quantity,
                                                    row.Rate,
                                                    row.UnitId,
                                                    row.UnitConversionId,
                                                    row.BatchId,
                                                    row.StoreId,
                                                    row.RackId,
                                                    amount,
                                                    row.SL,
                                                    DateTime.Now,
                                                    "",
                                                    "",
                                                    0,
                                                    0,
                                                    row.Description,
                                                    id);
                        db.ExecuteNonQuery2(detailsQuery);
                        amount = 0;
                    }
                    if (input.OrderDetailsId > 0)
                    {
                        string status = "Approved";
                        string updateQuery = string.Format("");
                        updateQuery = string.Format("UPDATE tbl_PurchaseOrderMaster " +
                                                    "SET orderStatus='{0}' " +
                                                    "WHERE purchaseOrderMasterId={1} ", status, input.OrderMasterId);
                        if (db.customUpdateQuery(updateQuery) > 0)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
                //db.CloseConnection();
            }
            catch (Exception e)
            {

            }

            return false;
        }

        public bool SaveFunction(CreateMaterialReceiptVM input)
        {
            var orderMaster = new PurchaseOrderMasterSP().PurchaseOrderMasterView(input.OrderMasterId);

            //input.ReceiptNo = "22";   //change later
            MaterialReceiptDetailsInfo infoMaterialReceiptDetails = new MaterialReceiptDetailsInfo();
            ProductInfo infoProduct = new ProductInfo();
            MaterialReceiptMasterInfo infoMaterialReceiptMaster = new MaterialReceiptMasterInfo();
            StockPostingSP spstockposting = new StockPostingSP();
            MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();
            MaterialReceiptDetailsSP spMaterialReceiptDetails = new MaterialReceiptDetailsSP();
            ProductSP spproduct = new ProductSP();
            //var orderNos = new PurchaseMasterSP().GetOrderNoCorrespondingtoLedger(20020, 0, decMaterialReceiptVoucherTypeId);
            // input.OrderNo = orderNos.Select().Where(p=>p. == input.OrderNo)
            try
            {
                input.ReceiptNo = VoucherNumberGeneration();
                infoMaterialReceiptMaster.Date = Convert.ToDateTime(input.Date);
                infoMaterialReceiptMaster.LedgerId = Convert.ToDecimal(input.SupplierId);

                infoMaterialReceiptMaster.SuffixPrefixId = 0;
                infoMaterialReceiptMaster.VoucherNo = input.ReceiptNo;

                infoMaterialReceiptMaster.VoucherTypeId = decMaterialReceiptVoucherTypeId;
                infoMaterialReceiptMaster.InvoiceNo = input.ReceiptNo;
                infoMaterialReceiptMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                infoMaterialReceiptMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoMaterialReceiptMaster.TransportationCompany = "";
                infoMaterialReceiptMaster.LrNo = "";
                infoMaterialReceiptMaster.Narration = input.Narration;
                infoMaterialReceiptMaster.OrderMasterId = input.OrderMasterId;
                infoMaterialReceiptMaster.exchangeRateId = orderMaster.exchangeRateId;//saving corresponding exchangeRateId as currencyId
                infoMaterialReceiptMaster.TotalAmount = Convert.ToDecimal(input.TotalAmount);
                infoMaterialReceiptMaster.Extra1 = string.Empty;
                infoMaterialReceiptMaster.Extra2 = string.Empty;
                infoMaterialReceiptMaster.ExtraDate = Convert.ToDateTime(DateTime.Now);
                infoMaterialReceiptMaster.Date = Convert.ToDateTime(input.Date);
                decMaterialReceiptMasterIdentity = Convert.ToDecimal(spMaterialReceiptMaster.MaterialReceiptMasterAdd(infoMaterialReceiptMaster));
                //int inRowcount = dgvProduct.Rows.Count;
                foreach (var lineItem in input.LineItems)
                {
                    if (lineItem.VoucherNo == null)
                    {
                        lineItem.VoucherNo = "";
                    }
                    if (lineItem.InvoiceNo == null)
                    {
                        lineItem.InvoiceNo = "";
                    }
                    var details = new PurchaseOrderDetailsSP().PurchaseOrderDetailsView(lineItem.OrderDetailsId);
                    infoMaterialReceiptDetails.MaterialReceiptMasterId = decMaterialReceiptMasterIdentity;
                    if (lineItem.ProductId > 0)
                    {
                        infoMaterialReceiptDetails.ProductId = lineItem.ProductId;
                    }
                    //if (dgvProduct.Rows[inI].Cells["dgvtxtPurchaseOrderDetailsId"].Value != null)
                    //{
                    //    infoMaterialReceiptDetails.OrderDetailsId = Convert.ToDecimal(dgvProduct.Rows[inI].Cells["dgvtxtPurchaseOrderDetailsId"].Value.ToString());
                    //}
                    //else
                    infoMaterialReceiptDetails.OrderDetailsId = lineItem.OrderDetailsId;
                    if (lineItem.StoreId > 0)
                    {
                        infoMaterialReceiptDetails.GodownId = Convert.ToDecimal(lineItem.StoreId);
                    }
                    else
                    {
                        infoMaterialReceiptDetails.GodownId = 1;
                    }
                    if (lineItem.RackId > 0)
                    {
                        infoMaterialReceiptDetails.RackId = Convert.ToDecimal(lineItem.RackId);
                    }
                    else
                    {
                        infoMaterialReceiptDetails.RackId = 1;
                    }
                    if (lineItem.BatchId > 0)
                    {
                        infoMaterialReceiptDetails.BatchId = new BatchSP().BatchIdViewByProductId(lineItem.ProductId);
                    }
                    else
                    {
                        infoMaterialReceiptDetails.BatchId = 1;
                    }
                    if (lineItem.Quantity > 0)
                    {
                        infoMaterialReceiptDetails.Qty = Convert.ToDecimal(lineItem.Quantity);
                    }
                    if (lineItem.UnitId > 0)
                    {
                        infoMaterialReceiptDetails.UnitId = Convert.ToDecimal(lineItem.UnitId);
                        infoMaterialReceiptDetails.UnitConversionId = Convert.ToDecimal(lineItem.UnitConversionId);
                    }
                    infoMaterialReceiptDetails.Rate = Convert.ToDecimal(lineItem.Rate);
                    infoMaterialReceiptDetails.Amount = Convert.ToDecimal(lineItem.Rate * lineItem.Quantity);
                    infoMaterialReceiptDetails.Slno = Convert.ToInt32(lineItem.SL);
                    infoMaterialReceiptDetails.Extra1 = string.Empty;
                    infoMaterialReceiptDetails.Exta2 = string.Empty;

                    infoMaterialReceiptDetails.ProjectId = 0;
                    infoMaterialReceiptDetails.CategoryId = 0;
                    if (lineItem.Description != "")
                    {
                        infoMaterialReceiptDetails.itemDescription = lineItem.Description;
                    }
                    infoMaterialReceiptDetails.ExtraDate = Convert.ToDateTime(DateTime.Now);
                    spMaterialReceiptDetails.MaterialReceiptDetailsAdd(infoMaterialReceiptDetails);
                    //-----------------Stockposting---------------------------//
                    StockPostingInfo infoStockPosting = new StockPostingInfo();
                    infoStockPosting.Date = infoMaterialReceiptMaster.Date;
                    infoStockPosting.ProductId = infoMaterialReceiptDetails.ProductId;
                    infoStockPosting.BatchId = infoMaterialReceiptDetails.BatchId;
                    infoStockPosting.UnitId = infoMaterialReceiptDetails.UnitId;
                    infoStockPosting.GodownId = infoMaterialReceiptDetails.GodownId;
                    infoStockPosting.RackId = infoMaterialReceiptDetails.RackId;
                    if (input.OrderNo != "")
                    {
                        if (lineItem.InvoiceNo != "")
                        {
                            infoStockPosting.InvoiceNo = input.ReceiptNo;
                            infoStockPosting.AgainstInvoiceNo = input.ReceiptNo;
                        }
                        else
                        {
                            infoStockPosting.InvoiceNo = input.ReceiptNo;
                            infoStockPosting.AgainstInvoiceNo = "NA";
                        }

                        if (lineItem.VoucherNo != "")
                        {
                            infoStockPosting.VoucherNo = input.ReceiptNo;
                            infoStockPosting.AgainstVoucherNo = strVoucherNo;
                        }
                        else
                        {
                            infoStockPosting.VoucherNo = strVoucherNo;
                            infoStockPosting.AgainstVoucherNo = "NA";
                        }

                        if (lineItem.VoucherTypeId != 0)
                        {
                            infoStockPosting.VoucherTypeId = lineItem.VoucherTypeId;
                            infoStockPosting.AgainstVoucherTypeId = decMaterialReceiptVoucherTypeId;
                        }
                        else
                        {
                            infoStockPosting.VoucherTypeId = decMaterialReceiptVoucherTypeId;
                            infoStockPosting.AgainstVoucherTypeId = 0;
                        }
                    }
                    else
                    {
                        infoStockPosting.InvoiceNo = input.ReceiptNo;
                        infoStockPosting.VoucherNo = strVoucherNo;
                        infoStockPosting.VoucherTypeId = decMaterialReceiptVoucherTypeId;
                        infoStockPosting.AgainstVoucherTypeId = 0;
                        infoStockPosting.AgainstVoucherNo = "NA";
                        infoStockPosting.AgainstInvoiceNo = "NA";
                    }
                    //infoStockPosting.InwardQty = Convert.ToDecimal(lineItem.Quantity) / Convert.ToDecimal(dgvProduct.Rows[inI].Cells["dgvtxtConversionRate"].Value.ToString());
                    infoStockPosting.InwardQty = Convert.ToDecimal(lineItem.Quantity) / 1;
                    infoStockPosting.OutwardQty = 0;
                    infoStockPosting.Rate = Convert.ToDecimal(lineItem.Rate);
                    infoStockPosting.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoStockPosting.Extra1 = string.Empty;
                    infoStockPosting.Extra2 = string.Empty;
                    spstockposting.StockPostingAdd(infoStockPosting);
                }
                return true;
                //DBMatConnection conn = new DBMatConnection();
                //string queryStr = string.Format("UPDATE tbl_MaterialReceiptMaster_Pending " +
                //                                "SET approved='{0}'" +
                //                                "WHERE orderMasterId={1} ", "Approved", input.OrderMasterId);
                //if (conn.customUpdateQuery(queryStr) > 0)
                //{
                //    return true;
                //}
                //return false;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "MR36:" + ex.Message;
            }
            return false;
        }
    }
}