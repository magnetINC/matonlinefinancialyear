﻿using MatApi.Models.Supplier;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SupplierCentreController : ApiController
    {
        [HttpPost]
        public DataTable GetSupplierList(SupplierCentreSearchModel SearchParameters)
        {
            try
            {
                decimal decAreaId = 0;
                decimal decRouteId = 0;
                DataTable dtbl = new DataTable();
                AccountLedgerSP spAccountledger = new AccountLedgerSP();
                if (SearchParameters.StateId < 1)
                {
                    decAreaId = 0;
                }
                else
                {
                    decAreaId = Convert.ToDecimal(SearchParameters.StateId.ToString());
                }
                if (SearchParameters.CityId < 1)
                {
                    decRouteId = 0;
                }
                else
                {
                    decRouteId = Convert.ToDecimal(SearchParameters.CityId.ToString());
                }

                int AccountStatus = Convert.ToInt32(SearchParameters.Status);
                string SupplierName = SearchParameters.SupplierName;

                dtbl = spAccountledger.SupplierSearchAll(decAreaId, decRouteId, SupplierName.Trim(), MATFinancials.PublicVariables._dtToDate, AccountStatus);

                if (AccountStatus == 1)
                {
                    dtbl = dtbl.AsEnumerable().Where(i => i.Field<decimal>("openingBalance") != 0).CopyToDataTable();
                }

                return dtbl;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "Cus16:" + ex.Message;
            }

            return null;
        }

        [HttpGet]
        public HttpResponseMessage GetLookUp()
        {
            dynamic resp = new ExpandoObject();
            var cities = new RouteSP().RouteViewAll();
            var states = new AreaSP().AreaViewAll();
            var pricingLevels = new PricingLevelSP().PricingLevelViewAll();

            resp.Cities = cities;
            resp.States = states;
            resp.PricingLevels = pricingLevels;

            return Request.CreateResponse(HttpStatusCode.OK, (object)resp);
        }

        [HttpPost]
        public string SaveNewSupplier(AccountLedgerInfo input)
        {
            decimal decledgerid = 0;
            try
            {
                AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();

                infoAccountLedger.AccountGroupId = 22;
                infoAccountLedger.LedgerName = input.LedgerName;
                infoAccountLedger.IsActive = input.IsActive;
                infoAccountLedger.OpeningBalance = 0;
                infoAccountLedger.CrOrDr = "Cr";
                infoAccountLedger.BankAccountNumber = input.BankAccountNumber;
                infoAccountLedger.BranchName = input.BranchName;
                infoAccountLedger.BranchCode = input.BranchCode;
                infoAccountLedger.Mobile = input.Mobile;
                infoAccountLedger.Address = input.Address;
                infoAccountLedger.CreditLimit = Convert.ToDecimal(input.CreditLimit);
                infoAccountLedger.CreditPeriod = Convert.ToInt32(input.CreditPeriod);
                infoAccountLedger.Cst = input.Cst;
                infoAccountLedger.AreaId = Convert.ToDecimal(input.AreaId);
                infoAccountLedger.RouteId = Convert.ToDecimal(input.RouteId);
                infoAccountLedger.MailingName = input.MailingName;
                infoAccountLedger.Phone = input.Phone;
                infoAccountLedger.Email = input.Email;
                infoAccountLedger.PricinglevelId = Convert.ToDecimal(input.PricinglevelId);
                infoAccountLedger.Tin = input.Tin;
                infoAccountLedger.Pan = "";
                infoAccountLedger.Narration = "";
                infoAccountLedger.IsDefault = false;
                infoAccountLedger.Extra1 = string.Empty;
                infoAccountLedger.Extra2 = string.Empty;
                infoAccountLedger.ExtraDate = MATFinancials.PublicVariables._dtCurrentDate;
                infoAccountLedger.IsActive = true;
                infoAccountLedger.BillByBill = true;
                if (spAccountLedger.AccountLedgerCheckExistenceForSalesman(input.LedgerName, 0) == false)
                {
                    decledgerid = spAccountLedger.AccountLedgerAddForCustomer(infoAccountLedger);
                }
                else
                {
                    string FailMessage = "Ledger name already exist";

                    return FailMessage;
                }
            }
            catch (Exception ex)
            {

            }

            return "Ledger name saved successfully";
        }

        [HttpGet]
        public HttpResponseMessage GetLedgerDetails(int ledgerId)
        {
            try
            {

                DataSet ledgerDetails = new DataSet();
                AccountLedgerSP SpAccountLedger = new AccountLedgerSP();
                dynamic resp = new ExpandoObject();
                ledgerDetails = SpAccountLedger.GetLedgerDetailsFromSelectedCustomer(MATFinancials.PublicVariables._dtFromDate, MATFinancials.PublicVariables._dtToDate, ledgerId);
                resp.LedgerDetails = ledgerDetails;
                resp.OpeningDate = MATFinancials.PublicVariables._dtFromDate;
                return Request.CreateResponse(HttpStatusCode.OK, (object)resp);
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpGet]
        public HttpResponseMessage GetSupplierDetails(int ledgerId)
        {
            try
            {
                AccountLedgerInfo infoAccountledger = new AccountLedgerInfo();
                AccountLedgerSP spAccountledger = new AccountLedgerSP();
                infoAccountledger = spAccountledger.AccountLedgerViewForSupplier(ledgerId);
                return Request.CreateResponse(HttpStatusCode.OK, (object)infoAccountledger);

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpPost]
        public string UpdateLedgerDetails(AccountLedgerInfo input)
        {
            try
            {
                AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();

                infoAccountLedger.AccountGroupId = 22;
                infoAccountLedger.LedgerId = input.LedgerId;
                infoAccountLedger.LedgerName = input.LedgerName;
                infoAccountLedger.IsActive = input.IsActive;
                infoAccountLedger.OpeningBalance = 0;
                infoAccountLedger.CrOrDr = "Cr";
                infoAccountLedger.BankAccountNumber = input.BankAccountNumber;
                infoAccountLedger.BranchName = input.BranchName;
                infoAccountLedger.BranchCode = input.BranchCode;
                infoAccountLedger.Mobile = input.Mobile;
                infoAccountLedger.Address = input.Address;
                infoAccountLedger.CreditLimit = Convert.ToDecimal(input.CreditLimit);
                infoAccountLedger.CreditPeriod = Convert.ToInt32(input.CreditPeriod);
                infoAccountLedger.Cst = input.Cst;
                infoAccountLedger.AreaId = Convert.ToDecimal(input.AreaId);
                infoAccountLedger.RouteId = Convert.ToDecimal(input.RouteId);
                infoAccountLedger.MailingName = input.MailingName;
                infoAccountLedger.Phone = input.Phone;
                infoAccountLedger.Email = input.Email;
                infoAccountLedger.PricinglevelId = Convert.ToDecimal(input.PricinglevelId);
                infoAccountLedger.Tin = input.Tin;
                infoAccountLedger.Pan = "";
                infoAccountLedger.Narration = "";
                infoAccountLedger.IsDefault = false;
                infoAccountLedger.Extra1 = string.Empty;    // to save agent code
                infoAccountLedger.Extra2 = string.Empty;
                infoAccountLedger.ExtraDate = MATFinancials.PublicVariables._dtCurrentDate;
                infoAccountLedger.IsActive = true;
                infoAccountLedger.BillByBill = true;
                spAccountLedger.AccountLedgerEdit(infoAccountLedger);
                return "Supplier Updated Successfully";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        [HttpDelete]
        public string DeleteLedger(int ledgerId)
        {
            try
            {
                var msg = "";

                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                if (spAccountLedger.AccountLedgerCheckReferences(ledgerId) == -1)
                {
                    msg = "Ledger has an existing transaction";

                    return msg;
                }
                else
                {
                    spAccountLedger.PartyBalanceDeleteByVoucherTypeVoucherNoAndReferenceType(ledgerId.ToString(), 1);
                    spAccountLedger.LedgerPostingDeleteByVoucherTypeAndVoucherNo(ledgerId.ToString(), 1);

                    msg = "Ledger deleted successfully";

                    return msg;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpGet]
        public DataTable GetAllAgentLedger()
        {
            DBMatConnection conn = new DBMatConnection();
            string query = string.Format("select ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, al.mailingName, al.routeId, al.areaId, tbl_Route.routeName, tbl_Area.areaName, " +
                                         "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance, " +
                                         "al.isActive AS Active from tbl_LedgerPosting lp " +
                                         "right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " +
                                         "left outer join tbl_Route on al.routeId = tbl_Route.routeId " +
                                         "left outer join tbl_Area on al.areaId = tbl_Area.areaId " +
                                         "where al.accountGroupId = 22 " +
                                         "group by ledgerName, al.ledgerId, lp.ledgerId, al.isActive, al.routeId, al.areaId, al.mailingName, tbl_Route.routeName, tbl_Area.areaName");
            var data = conn.customSelect(query);
            return data;
        }
    }
}
