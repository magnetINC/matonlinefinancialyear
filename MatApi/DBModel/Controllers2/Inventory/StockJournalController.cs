﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials;
using System.Data;
using MatApi.Models.Inventory;
using System.Dynamic;
using MATFinancials.DAL;


namespace MatApi.Controllers.Inventory
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StockJournalController : ApiController
    {
        decimal decVoucherTypeId = 0;
        decimal decSuffixPrefixId = 0;
        bool isAutomatic = false;
        string strVoucherNo = string.Empty;
        string TableName = "StockJournalMaster";
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        string strInvoiceNo = string.Empty;
        decimal decStockMasterId = 0;

        public StockJournalController()
        {
            decVoucherTypeId = 24;
            VoucherTypeSP spVoucherType = new VoucherTypeSP();
            isAutomatic = spVoucherType.CheckMethodOfVoucherNumbering(24);
            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(24, DateTime.Now);
            decSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
        }
            
        [HttpGet]
        public HttpResponseMessage SearchProduct(string searchBy,string filter)
        {
            SalesDetailsSP spSalesDetails = new SalesDetailsSP();
            UnitSP spUnit = new UnitSP();
            GodownSP spGodown = new GodownSP();
            BatchSP spBatch = new BatchSP();
            TaxSP spTax = new TaxSP();
            RackSP spRack = new RackSP();
            UnitConvertionSP SpUnitConvertion = new UnitConvertionSP();

            dynamic response = new ExpandoObject();


            if (searchBy == "ProductCode")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByProductCodeForSI(28, filter);
                var productId = Convert.ToDecimal(productDetails.Rows[0]["ProductId"]);
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[11].ToString()));
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                //response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productId));
            }
            else if (searchBy == "ProductName")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByProductNameForSI(28, filter);
                var productId = Convert.ToDecimal(productDetails.Rows[0]["ProductId"]);
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[11].ToString()));
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                //response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productId));
            }
            else if (searchBy == "Barcode")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByBarcodeForSI(28, filter);
                var productId = Convert.ToDecimal(productDetails.Rows[0]["ProductId"]);
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[11].ToString()));
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                //response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productId));
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }


        [HttpGet]
        public HttpResponseMessage GetLookups()
        {
            var currency= new TransactionsGeneralFill().CurrencyComboByDate(DateTime.Now);
            var finishedGoods= new ProductSP().ProductFinishedGoodsComboFill();
            var allProducts = new ProductSP().ProductViewAll();
            var allStores = new GodownSP().GodownViewAll();
            var allRacks = new RackSP().RackViewAll();
            var allUnits = new UnitSP().UnitViewAll();
            var allBatches = new BatchSP().BatchViewAll();
            var cashOrBanks = new TransactionsGeneralFill().BankOrCashComboFill(false);
            var accountLedgers = new AccountLedgerSP().AdditionalCostGet();

            dynamic response = new ExpandoObject();
            response.Currencies = currency;
            response.FinishedGoods = finishedGoods;
            response.AllProducts = allProducts;
            response.AllStores = allStores;
            response.AllRacks = allRacks;
            response.AllUnits = allUnits;
            response.AllBatches = allBatches;
            response.CashOrBanks = cashOrBanks;
            response.AccountLedgers = accountLedgers;
            response.users = new UserSP().UserViewAll();

            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }

        [HttpGet]
        public DataTable GetRawMaterialForProduct(decimal productId,decimal quantity)
        {
            return new ProductSP().RawMaterialsFillForStockJournal(productId, quantity);
        }

        public HttpResponseMessage GetTaxAmount(string taxId, string amount)
        {

            decimal taxAmount = 0;
            TaxSP SpTax = new TaxSP();
            var amt = Convert.ToDecimal(amount);
            TaxInfo InfoTaxObj = SpTax.TaxView(Convert.ToDecimal(taxId));
            taxAmount = Math.Round(((amt * InfoTaxObj.Rate) / (100)), PublicVariables._inNoOfDecimalPlaces);
            return Request.CreateResponse(HttpStatusCode.OK, taxAmount);
        }

        private string generateVoucherNo()
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            StockJournalMasterSP spMaster = new StockJournalMasterSP();
            strVoucherNo = "0";
            strVoucherNo = obj.VoucherNumberAutomaicGeneration(decVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, TableName);

            if (Convert.ToDecimal(strVoucherNo) != spMaster.StockJournalMasterMaxPlusOne(decVoucherTypeId))
            {
                strVoucherNo = spMaster.StockJournalMasterMax(decVoucherTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, TableName);
                if (spMaster.StockJournalMasterMax(decVoucherTypeId).ToString() == "0")
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, TableName);
                }
            }
            //SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            //SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            //infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decVoucherTypeId, DateTime.Now);
            //strPrefix = infoSuffixPrefix.Prefix;
            //strSuffix = infoSuffixPrefix.Suffix;
            //strInvoiceNo = strPrefix + strVoucherNo + strSuffix;
            return strVoucherNo;
        }

        [HttpGet]
        public string GetAutoVoucherNo()
        {
            return generateVoucherNo();
        }

        [HttpGet]
        public HttpResponseMessage GetPendingStockJournals(DateTime fromDate, DateTime toDate)
        {
             MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            dynamic response = new ExpandoObject(); ;
            string GetQuery = string.Format("");
            GetQuery = string.Format("");
            GetQuery = string.Format("SELECT * FROM tbl_StockJournalMaster_Pending WHERE date BETWEEN '{0}' AND '{1}' ORDER BY stockJournalMasterId DESC", fromDate, toDate);
            var result = db.GetDataSet(GetQuery);
            return Request.CreateResponse(HttpStatusCode.OK, (object)result);
        }

        [HttpGet]
        public HttpResponseMessage GetPendingStockJournalDetails(decimal id)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            dynamic response = new ExpandoObject(); ;
            string GetQuery1 = string.Format("SELECT * FROM tbl_StockJournalDetails_Pending WHERE stockJournalMasterId = '{0}' AND consumptionOrProduction='Production'",id);
            string GetQuery2 = string.Format("SELECT * FROM tbl_StockJournalDetails_Pending WHERE stockJournalMasterId = '{0}' AND consumptionOrProduction='Consumption'", id);
            string GetQuery3 = string.Format("SELECT * FROM tbl_AdditionalCost_Pending WHERE masterId = '{0}'", id);

            response.production = db.customSelect(GetQuery1);
            response.consumption = db.customSelect(GetQuery2);
            response.addCost = db.customSelect(GetQuery3);
            response.products = new ProductSP().ProductViewAll();
            response.stores = new GodownSP().GodownViewAll();
            response.ledger = new AccountLedgerSP().AccountLedgerViewAll();
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetInTransitDetails(decimal userId, decimal transferId)
        {
            var user = new UserSP().UserView(userId);
            decimal warehouseId = Convert.ToDecimal(user.StoreId);
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            dynamic response = new ExpandoObject(); ;
            string GetQuery1 = string.Format("SELECT * FROM tbl_StockJournalDetails_Pending WHERE stockJournalMasterId = '{0}' AND godownId = {1} AND consumptionOrProduction='Production'", transferId, warehouseId);
            string GetQuery2 = string.Format("SELECT * FROM tbl_StockJournalDetails_Pending WHERE stockJournalMasterId = '{0}' AND consumptionOrProduction='Consumption'", transferId);
            response.production = db.customSelect(GetQuery1);
            response.consumption = db.customSelect(GetQuery2);
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpPost]
        public bool SavePending(StockJournalVM input)
        {
            bool result = false;
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string masterQuery = string.Format("INSERT INTO tbl_StockJournalMaster_Pending " +
                                                    "VALUES('{0}','{1}',{2},{3},{4},'{5}',{6},{7})"
                                                    , input.Date,
                                                    input.StockJournalMasterInfo.Narration,
                                                    input.TotalAdditionalCost,
                                                    MATFinancials.PublicVariables._decCurrentFinancialYearId,
                                                    input.Currency,
                                                    "Pending",
                                                    input.AdditionalCostCashOrBankId,
                                                    Convert.ToDecimal(input.StockJournalMasterInfo.Extra2));
            if (db.ExecuteNonQuery2(masterQuery))
            {
                string id = db.getSingleValue("SELECT top 1 stockJournalMasterId from tbl_StockJournalMaster_Pending order by stockJournalMasterId desc");
                foreach (var row in input.StockJournalDetailsInfoProduction)
                {
                    var amount = row.Rate * row.Qty;
                    string detailsQuery1 = string.Format("INSERT INTO tbl_StockJournalDetails_Pending " +
                                                "VALUES({0},{1},{2},{3},{4},{5},{6},'{7}',{8},'{9}',{10})"
                                                , id,
                                                row.ProductId,
                                                row.Qty,
                                                row.Rate,
                                                row.GodownId,
                                                row.RackId,
                                                amount,
                                                "Production",
                                                row.Slno,
                                                "Pending",
                                                0);
                    db.ExecuteNonQuery2(detailsQuery1);
                    amount = 0;
                }
                foreach (var row in input.StockJournalDetailsInfoConsumption)
                {
                    var amount = row.Rate * row.Qty;
                    string detailsQuery2 = string.Format("INSERT INTO tbl_StockJournalDetails_Pending " +
                                                "VALUES({0},{1},{2},{3},{4},{5},{6},'{7}',{8},'{9}',{10})"
                                                , id,
                                                row.ProductId,
                                                row.Qty,
                                                row.Rate,
                                                row.GodownId,
                                                row.RackId,
                                                amount,
                                                "Consumption",
                                                row.Slno,
                                                "",
                                                0);
                    db.ExecuteNonQuery2(detailsQuery2);
                    amount = 0;
                }
                //foreach (var row in input.AdditionalCostItems)
                //{
                //    string detailsQuery3 = string.Format("INSERT INTO tbl_AdditionalCost_Pending " +
                //                                "VALUES({0},{1},{2},{3})"
                //                                , row.LedgerId,
                //                                row.Amount,
                //                                0,
                //                                id);
                //    db.ExecuteNonQuery2(detailsQuery3);
                //}
                result = true;
            }
            return result;
        }

        [HttpGet]
        public bool updateQuantity (decimal idprod, decimal idcons, decimal quantity, decimal amount)
        {
            bool result = false;
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("UPDATE tbl_StockJournalDetails_Pending " +
                                            "SET qty={0}, amount={3} " +
                                            "WHERE consumptionOrProduction='{1}' and stockJournalDetailsId={2}", quantity, "Production", idprod, amount);
            if (conn.customUpdateQuery(queryStr) > 0)
            {
                string queryStr1 = string.Format("UPDATE tbl_StockJournalDetails_Pending " +
                                            "SET qty={0}, amount={3} " +
                                            "WHERE consumptionOrProduction='{1}' and stockJournalDetailsId={2}", quantity, "Consumption", idcons, amount);
                if (conn.customUpdateQuery(queryStr1) > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        [HttpPost]
        public bool saveStockJournalInTransit(StockJournalVM input)
        {
            bool isSaved = false;

            StockJournalMasterInfo infoStockJournalMaster = new StockJournalMasterInfo();
            StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
            StockJournalDetailsInfo infoStockJournalDetailsConsumption = new StockJournalDetailsInfo();
            StockJournalDetailsInfo infoStockJournalDetailsProduction = new StockJournalDetailsInfo();
            StockJournalDetailsSP spStockJournalDetails = new StockJournalDetailsSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            StockPostingInfo infoStockPostingConsumption = new StockPostingInfo();
            StockPostingInfo infoStockPostingProduction = new StockPostingInfo();
            StockPostingSP spStockPosting = new StockPostingSP();
            AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
            strVoucherNo = generateVoucherNo();
            //if (isAutomatic == true)
            //{
            infoStockJournalMaster.SuffixPrefixId = decSuffixPrefixId;
            infoStockJournalMaster.VoucherNo = strVoucherNo;
            //}
            //else
            //{
            //    infoStockJournalMaster.SuffixPrefixId = 0;
            //    infoStockJournalMaster.VoucherNo = strVoucherNo;
            //}
            infoStockJournalMaster.ExtraDate = DateTime.Now;
            infoStockJournalMaster.InvoiceNo = strVoucherNo;
            infoStockJournalMaster.Date = input.Date;
            infoStockJournalMaster.AdditionalCost = input.TotalAdditionalCost;
            infoStockJournalMaster.VoucherNo = strVoucherNo;
            infoStockJournalMaster.VoucherTypeId = decVoucherTypeId;
            infoStockJournalMaster.Narration = input.StockJournalMasterInfo.Narration;
            infoStockJournalMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
            infoStockJournalMaster.ExchangeRateId = input.Currency;
            infoStockJournalMaster.Extra1 = input.StockJournalMasterInfo.Extra1;
            infoStockJournalMaster.Extra2 = input.pendingId.ToString();
            decStockMasterId = spStockJournalMaster.StockJournalMasterAdd(infoStockJournalMaster);
            if (decStockMasterId > 0)
            {
                isSaved = true;
            }
            else
            {
                isSaved = false;
            }

            if (input.StockJournalDetailsInfoConsumption.Count > 0)
            {
                foreach (var row in input.StockJournalDetailsInfoConsumption)
                {
                    var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(row.ProductId).UnitconvertionId;
                    infoStockJournalDetailsConsumption.StockJournalMasterId = decStockMasterId;
                    infoStockJournalDetailsConsumption.Extra1 = string.Empty;
                    infoStockJournalDetailsConsumption.Extra2 = string.Empty;
                    infoStockJournalDetailsConsumption.ExtraDate = DateTime.Now;
                    infoStockJournalDetailsConsumption.ProductId = row.ProductId;
                    infoStockJournalDetailsConsumption.Qty = row.Qty;
                    infoStockJournalDetailsConsumption.Rate = row.Rate;
                    infoStockJournalDetailsConsumption.UnitId = row.UnitId;
                    infoStockJournalDetailsConsumption.UnitConversionId = unitConvId;
                    infoStockJournalDetailsConsumption.BatchId = row.BatchId;
                    infoStockJournalDetailsConsumption.GodownId = row.GodownId;
                    infoStockJournalDetailsConsumption.RackId = row.RackId;
                    infoStockJournalDetailsConsumption.Amount = row.Amount;
                    infoStockJournalDetailsConsumption.ConsumptionOrProduction = "Consumption";
                    infoStockJournalDetailsConsumption.Slno = row.Slno;
                    isSaved = spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetailsConsumption);

                    //
                    infoStockPostingConsumption.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                    infoStockPostingConsumption.Date = input.Date;
                    infoStockPostingConsumption.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    infoStockPostingConsumption.GodownId = row.GodownId;
                    infoStockPostingConsumption.InwardQty = 0;
                    infoStockPostingConsumption.OutwardQty = infoStockJournalDetailsConsumption.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId);
                    infoStockPostingConsumption.ProductId = row.ProductId;
                    infoStockPostingConsumption.RackId = row.RackId;
                    infoStockPostingConsumption.Rate = row.Rate;
                    infoStockPostingConsumption.UnitId = row.UnitId;
                    infoStockPostingConsumption.InvoiceNo = strVoucherNo;
                    infoStockPostingConsumption.VoucherNo = strVoucherNo;
                    infoStockPostingConsumption.VoucherTypeId = decVoucherTypeId;
                    infoStockPostingConsumption.AgainstVoucherTypeId = 0;
                    infoStockPostingConsumption.AgainstInvoiceNo = "NA";
                    infoStockPostingConsumption.AgainstVoucherNo = "NA";
                    infoStockPostingConsumption.Extra1 = string.Empty;
                    infoStockPostingConsumption.Extra2 = string.Empty;
                    if (spStockPosting.StockPostingAdd(infoStockPostingConsumption) > 0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
              
                }
                DBMatConnection conn = new DBMatConnection();
                string queryStr = string.Format("UPDATE tbl_StockJournalDetails_Pending " +
                                                "SET status='{0}', decStockJournalId={3} " +
                                                "WHERE consumptionOrProduction='{1}' and stockJournalMasterId={2}", "In-Transit", "Production", input.pendingId, decStockMasterId);
                if (conn.customUpdateQuery(queryStr) > 0)
                {
                    DBMatConnection conn2 = new DBMatConnection();
                    string queryStr2 = string.Format("UPDATE tbl_StockJournalMaster_Pending " +
                                                    "SET status='{0}'" +
                                                    "WHERE stockJournalMasterId={1} ", "In-Transit", input.pendingId);
                    if (conn2.customUpdateQuery(queryStr2) > 0)
                    {
                        return true;
                    }
                }
            }
            return isSaved;
        }

        [HttpGet]
        public HttpResponseMessage GetTransferInTransit(decimal id)
        {
            var user = new UserSP().UserView(id);
            decimal warehouseId = Convert.ToDecimal(user.StoreId);
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            List<StockTransferOnDeliveryVM> Transfers = new List<StockTransferOnDeliveryVM>();

            dynamic response = new ExpandoObject(); ;
            string GetQuery = string.Format("select distinct stockJournalmasterId from tbl_StockJournaldetails_Pending where godownId = {0} AND consumptionOrProduction='Production' AND status='In-Transit'  ORDER BY stockJournalmasterId DESC", warehouseId);
            var details = db.customSelect(GetQuery);

            for(var t = 0; t < details.Rows.Count; t++)
            {
                string GetQuery1 = string.Format("select * from tbl_StockJournalmaster_Pending where stockJournalmasterId = {0}", details.Rows[t].ItemArray[0]);
                var master = db.customSelect(GetQuery1);
                Transfers.Add(new StockTransferOnDeliveryVM
                {
                    id = Convert.ToDecimal(master.Rows[0].ItemArray[0]),
                    date = Convert.ToDateTime(master.Rows[0].ItemArray[1]),
                    narration = master.Rows[0].ItemArray[2].ToString(),
                    userId = Convert.ToDecimal(master.Rows[0].ItemArray[8]),
                    status = master.Rows[0].ItemArray[6].ToString()
                });
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)Transfers);
        }

        [HttpPost]
        public bool saveStockTransferOnDelivery(StockJournalVM input)
        {
            bool isSaved = false;

            StockJournalMasterInfo infoStockJournalMaster = new StockJournalMasterInfo();
            StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
            StockJournalDetailsInfo infoStockJournalDetailsConsumption = new StockJournalDetailsInfo();
            StockJournalDetailsInfo infoStockJournalDetailsProduction = new StockJournalDetailsInfo();
            StockJournalDetailsSP spStockJournalDetails = new StockJournalDetailsSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            StockPostingInfo infoStockPostingConsumption = new StockPostingInfo();
            StockPostingInfo infoStockPostingProduction = new StockPostingInfo();
            StockPostingSP spStockPosting = new StockPostingSP();
            AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
            if (input.StockJournalDetailsInfoProduction.Count > 0)
            {
                foreach (var row in input.StockJournalDetailsInfoProduction)
                {
                    var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(row.ProductId).UnitconvertionId;
                    var unitId = new UnitSP().unitVieWForStandardRate(row.ProductId).UnitId;  //unitVieWForStandardRate is used to get unitid since no function returns
                                                                                              //unitinfo based on unit id

                    var master = new StockJournalMasterSP().StockJournalMasterView(input.pendingId);
                    infoStockJournalDetailsProduction.StockJournalMasterId = input.pendingId;
                    infoStockJournalDetailsProduction.Extra1 = string.Empty;
                    infoStockJournalDetailsProduction.Extra2 = string.Empty;
                    infoStockJournalDetailsProduction.ExtraDate = DateTime.Now;
                    infoStockJournalDetailsProduction.ProductId = row.ProductId;
                    infoStockJournalDetailsProduction.Qty = row.Qty;
                    infoStockJournalDetailsProduction.Rate = row.Rate;
                    infoStockJournalDetailsProduction.UnitId = unitId;
                    infoStockJournalDetailsProduction.UnitConversionId = unitConvId;
                    infoStockJournalDetailsProduction.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                    infoStockJournalDetailsProduction.GodownId = row.GodownId;
                    infoStockJournalDetailsProduction.RackId = row.RackId;
                    infoStockJournalDetailsProduction.Amount = row.Amount;
                    infoStockJournalDetailsProduction.ConsumptionOrProduction = "Production";
                    infoStockJournalDetailsProduction.Slno = row.Slno;
                    isSaved = spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetailsProduction);

                    infoStockPostingProduction.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                    infoStockPostingProduction.Date = input.Date;
                    infoStockPostingProduction.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    infoStockPostingProduction.GodownId = row.GodownId;
                    infoStockPostingProduction.InwardQty = row.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId);
                    infoStockPostingProduction.OutwardQty = 0;
                    infoStockPostingProduction.ProductId = row.ProductId;
                    infoStockPostingProduction.RackId = row.RackId;
                    infoStockPostingProduction.Rate = row.Rate;
                    infoStockPostingProduction.UnitId = unitId;
                    infoStockPostingProduction.InvoiceNo = master.InvoiceNo;
                    infoStockPostingProduction.VoucherNo = master.VoucherNo;
                    infoStockPostingProduction.VoucherTypeId = decVoucherTypeId;
                    infoStockPostingProduction.AgainstVoucherTypeId = 0;
                    infoStockPostingProduction.AgainstInvoiceNo = "NA";
                    infoStockPostingProduction.AgainstVoucherNo = "NA";
                    infoStockPostingProduction.Extra1 = string.Empty;
                    infoStockPostingProduction.Extra2 = string.Empty;
                    if (spStockPosting.StockPostingAdd(infoStockPostingProduction) > 0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                    DBMatConnection conn = new DBMatConnection();
                    string queryStr = string.Format("UPDATE tbl_StockJournalMaster_Pending " +
                                                    "SET status='{0}'" +
                                                    "WHERE stockJournalMasterId={1} ", "Delivered", input.AdditionalCostCashOrBankId);
                    conn.customUpdateQuery(queryStr);
                }
            }
            return isSaved;
        }

        [HttpPost]
        public bool SaveStockJournal(StockJournalVM input)
        {
            bool isSaved = false;

            StockJournalMasterInfo infoStockJournalMaster = new StockJournalMasterInfo();
            StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
            StockJournalDetailsInfo infoStockJournalDetailsConsumption = new StockJournalDetailsInfo();
            StockJournalDetailsInfo infoStockJournalDetailsProduction = new StockJournalDetailsInfo();
            StockJournalDetailsSP spStockJournalDetails = new StockJournalDetailsSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            StockPostingInfo infoStockPostingConsumption = new StockPostingInfo();
            StockPostingInfo infoStockPostingProduction = new StockPostingInfo();
            StockPostingSP spStockPosting = new StockPostingSP();
            AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
            strVoucherNo = generateVoucherNo();
            //if (isAutomatic == true)
            //{
                infoStockJournalMaster.SuffixPrefixId = decSuffixPrefixId;
                infoStockJournalMaster.VoucherNo = strVoucherNo;
            //}
            //else
            //{
            //    infoStockJournalMaster.SuffixPrefixId = 0;
            //    infoStockJournalMaster.VoucherNo = strVoucherNo;
            //}
            infoStockJournalMaster.ExtraDate = DateTime.Now;
            infoStockJournalMaster.InvoiceNo = strVoucherNo;
            infoStockJournalMaster.Date = input.Date;
            infoStockJournalMaster.AdditionalCost = input.TotalAdditionalCost;
            infoStockJournalMaster.VoucherNo = strVoucherNo;
            infoStockJournalMaster.VoucherTypeId = decVoucherTypeId;
            infoStockJournalMaster.Narration = input.StockJournalMasterInfo.Narration;
            infoStockJournalMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
            infoStockJournalMaster.ExchangeRateId = input.Currency;
            infoStockJournalMaster.Extra1 = input.StockJournalMasterInfo.Extra1;            
            infoStockJournalMaster.Extra2 = string.Empty;
            decStockMasterId = spStockJournalMaster.StockJournalMasterAdd(infoStockJournalMaster);
            if(decStockMasterId>0)
            {
                isSaved = true;
            }
            else
            {
                isSaved = false;
            }

            if (input.StockJournalDetailsInfoConsumption.Count>0)
            {
                foreach(var row in input.StockJournalDetailsInfoConsumption)
                {
                    var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(row.ProductId).UnitconvertionId;
                    infoStockJournalDetailsConsumption.StockJournalMasterId = decStockMasterId;
                    infoStockJournalDetailsConsumption.Extra1 = string.Empty;
                    infoStockJournalDetailsConsumption.Extra2 = string.Empty;
                    infoStockJournalDetailsConsumption.ExtraDate = DateTime.Now;
                    infoStockJournalDetailsConsumption.ProductId = row.ProductId;
                    infoStockJournalDetailsConsumption.Qty = row.Qty;
                    infoStockJournalDetailsConsumption.Rate = row.Rate;
                    infoStockJournalDetailsConsumption.UnitId = row.UnitId;
                    infoStockJournalDetailsConsumption.UnitConversionId = unitConvId;
                    infoStockJournalDetailsConsumption.BatchId = row.BatchId;
                    infoStockJournalDetailsConsumption.GodownId = row.GodownId;
                    infoStockJournalDetailsConsumption.RackId = row.RackId;
                    infoStockJournalDetailsConsumption.Amount = row.Amount;
                    infoStockJournalDetailsConsumption.ConsumptionOrProduction = "Consumption";
                    infoStockJournalDetailsConsumption.Slno = row.Slno;
                    isSaved= spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetailsConsumption);

                    //
                    infoStockPostingConsumption.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                    infoStockPostingConsumption.Date = input.Date;
                    infoStockPostingConsumption.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    infoStockPostingConsumption.GodownId = row.GodownId;
                    infoStockPostingConsumption.InwardQty = 0;
                    infoStockPostingConsumption.OutwardQty = infoStockJournalDetailsConsumption.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId);
                    infoStockPostingConsumption.ProductId = row.ProductId;
                    infoStockPostingConsumption.RackId = row.RackId;
                    infoStockPostingConsumption.Rate = row.Rate;
                    infoStockPostingConsumption.UnitId = row.UnitId;
                    infoStockPostingConsumption.InvoiceNo = strVoucherNo;
                    infoStockPostingConsumption.VoucherNo = strVoucherNo;
                    infoStockPostingConsumption.VoucherTypeId = decVoucherTypeId;
                    infoStockPostingConsumption.AgainstVoucherTypeId = 0;
                    infoStockPostingConsumption.AgainstInvoiceNo = "NA";
                    infoStockPostingConsumption.AgainstVoucherNo = "NA";
                    infoStockPostingConsumption.Extra1 = string.Empty;
                    infoStockPostingConsumption.Extra2 = string.Empty;
                    if(spStockPosting.StockPostingAdd(infoStockPostingConsumption)>0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                }                
            }

            if(input.StockJournalDetailsInfoProduction.Count>0)
            {
                foreach(var row in input.StockJournalDetailsInfoProduction)
                {
                    var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(row.ProductId).UnitconvertionId;
                    var unitId = new UnitSP().unitVieWForStandardRate(row.ProductId).UnitId;  //unitVieWForStandardRate is used to get unitid since no function returns
                                                                                            //unitinfo based on unit id

                    infoStockJournalDetailsProduction.StockJournalMasterId = decStockMasterId;
                    infoStockJournalDetailsProduction.Extra1 = string.Empty;
                    infoStockJournalDetailsProduction.Extra2 = string.Empty;
                    infoStockJournalDetailsProduction.ExtraDate = DateTime.Now;
                    infoStockJournalDetailsProduction.ProductId = row.ProductId;
                    infoStockJournalDetailsProduction.Qty = row.Qty;
                    infoStockJournalDetailsProduction.Rate = row.Rate;
                    infoStockJournalDetailsProduction.UnitId = unitId;
                    infoStockJournalDetailsProduction.UnitConversionId = unitConvId;
                    infoStockJournalDetailsProduction.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                    infoStockJournalDetailsProduction.GodownId = row.GodownId;
                    infoStockJournalDetailsProduction.RackId = row.RackId;
                    infoStockJournalDetailsProduction.Amount = row.Amount;
                    infoStockJournalDetailsProduction.ConsumptionOrProduction = "Production";
                    infoStockJournalDetailsProduction.Slno = row.Slno;
                    isSaved= spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetailsProduction);

                    infoStockPostingProduction.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                    infoStockPostingProduction.Date = input.Date;
                    infoStockPostingProduction.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    infoStockPostingProduction.GodownId = row.GodownId;
                    infoStockPostingProduction.InwardQty = infoStockJournalDetailsConsumption.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId);
                    infoStockPostingProduction.OutwardQty = 0;
                    infoStockPostingProduction.ProductId = row.ProductId;
                    infoStockPostingProduction.RackId = row.RackId;
                    infoStockPostingProduction.Rate = row.Rate;
                    infoStockPostingProduction.UnitId = unitId;
                    infoStockPostingProduction.InvoiceNo = strVoucherNo;
                    infoStockPostingProduction.VoucherNo = strVoucherNo;
                    infoStockPostingProduction.VoucherTypeId = decVoucherTypeId;
                    infoStockPostingProduction.AgainstVoucherTypeId = 0;
                    infoStockPostingProduction.AgainstInvoiceNo = "NA";
                    infoStockPostingProduction.AgainstVoucherNo = "NA";
                    infoStockPostingProduction.Extra1 = string.Empty;
                    infoStockPostingProduction.Extra2 = string.Empty;
                    if (spStockPosting.StockPostingAdd(infoStockPostingProduction) > 0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                }
            }

            decimal decGrandTotal = 0;
            decimal decRate = 0;
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            if(input.AdditionalCostItems.Count>0)
            {
                infoAdditionalCost.Credit = input.TotalAdditionalCost;
                infoAdditionalCost.Debit = 0;
                infoAdditionalCost.LedgerId = input.AdditionalCostCashOrBankId;
                infoAdditionalCost.VoucherNo = strVoucherNo;
                infoAdditionalCost.VoucherTypeId = decVoucherTypeId;
                infoAdditionalCost.Extra1 = string.Empty;
                infoAdditionalCost.Extra2 = string.Empty;
                infoAdditionalCost.ExtraDate = DateTime.Now;
                isSaved= spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                //....Ledger Posting Add...///
                //-------------------  Currency Conversion-----------------------------
                decGrandTotal = input.TotalAdditionalCost;
                decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(input.Currency);
                decGrandTotal = decGrandTotal * decRate;
                //---------------------------------------------------------------
                infoLedgerPosting.Credit = decGrandTotal;
                infoLedgerPosting.Debit = 0;
                infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);
                infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.InvoiceNo = strVoucherNo;
                infoLedgerPosting.LedgerId = input.AdditionalCostCashOrBankId;
                infoLedgerPosting.VoucherNo = strVoucherNo;
                infoLedgerPosting.VoucherTypeId = decVoucherTypeId;
                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                infoLedgerPosting.ExtraDate = DateTime.Now;
                if(spLedgerPosting.LedgerPostingAdd(infoLedgerPosting)>0)
                {
                    isSaved = true;
                }
                else
                {
                    isSaved = false;
                }
                foreach (var row in input.AdditionalCostItems)
                {
                    /*-----------------------------------------Additional Cost Add----------------------------------------------------*/
                    infoAdditionalCost.Credit = 0;
                    infoAdditionalCost.Debit = row.Amount;
                    infoAdditionalCost.LedgerId = row.LedgerId;
                    infoAdditionalCost.VoucherNo = strVoucherNo;
                    infoAdditionalCost.VoucherTypeId = decVoucherTypeId;
                    infoAdditionalCost.Extra1 = string.Empty;
                    infoAdditionalCost.Extra2 = string.Empty;
                    infoAdditionalCost.ExtraDate = DateTime.Now;
                    isSaved= spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                    /*-----------------------------------------Additional Cost Ledger Posting----------------------------------------------------*/
                    decimal decTotal = 0;
                    //-------------------  Currency Conversion------------------------
                    decTotal = Convert.ToDecimal(infoAdditionalCost.Debit);
                    decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(input.Currency);
                    decTotal = decTotal * decRate;
                    //---------------------------------------------------------------
                    infoLedgerPosting.Credit = 0;
                    infoLedgerPosting.Debit = decTotal;
                    infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);
                    infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.InvoiceNo = strVoucherNo;
                    infoLedgerPosting.LedgerId = row.LedgerId;
                    infoLedgerPosting.VoucherNo = strVoucherNo;
                    infoLedgerPosting.VoucherTypeId = decVoucherTypeId;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    if(spLedgerPosting.LedgerPostingAdd(infoLedgerPosting)>0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                }
            }
            //if (input.StockJournalMasterInfo.Extra1 == "Stock Transfer")
            //{
            //    DBMatConnection conn = new DBMatConnection();
            //    string queryStr = string.Format("UPDATE tbl_StockJournalMaster_Pending " +
            //                                    "SET status='{0}'" +
            //                                    "WHERE stockJournalMasterId={1} ", "Approved", input.StockJournalMasterInfo.Extra2);
            //    if (conn.customUpdateQuery(queryStr) > 0)
            //    {
            //        return true;
            //    }
            //}
            
            return isSaved;
        }

 
        //[HttpPost]
        //public bool SaveStockJournal(SockJournalModel obj)
        //{
        //    //route.ExtraDate = DateTime.Now;
        //    SaveOrEdit(obj);
        //    return true;//routeSp.RouteAdd(route);
        //}
        
        private decimal getQuantityInStock(decimal productId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format(" SELECT  convert(decimal(18, 2), (ISNULL(SUM(s.inwardQty), 0) - ISNULL(SUM(s.outwardQty), 0))) AS Currentstock " +
                " FROM tbl_StockPosting s inner join tbl_product p on p.productId = s.productId " +
                " INNER JOIN tbl_Batch b on s.batchId = b.batchId " +
                " WHERE p.productId = '{0}' AND s.date <= '{1}' ", productId, MATFinancials.PublicVariables._dtToDate);
            return Convert.ToDecimal(conn.getSingleValue(queryStr));
        }

        public void SaveOrEdit(SockJournalModel obj)
        {
            bool Isexit = false;
            StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
            try
            {
                string strVoucherNo;
                // to take assign voucher number in case automatic voucher numbering is set to off urefe 20161206
                if (true)//(!isAutomatic && txtVoucherNo.Text.Trim() != string.Empty)
                {
                    strVoucherNo = ""; //txtVoucherNo.Text.Trim();
                }
                //dgvConsumption.ClearSelection();
                //dgvProduction.ClearSelection();
                //dgvAdditionalCost.ClearSelection();
                int inRowConsumption = obj.ConsumptionItems.Count;
                int inRowProduction = obj.SrcLineItems.Count;

                if (false)//(txtVoucherNo.Text.Trim() == string.Empty)
                {
                    //Messages.InformationMessage("Enter voucher number");
                    //txtVoucherNo.Focus();
                }
                else if(false) //()(spStockJournalMaster.StockJournalInvoiceNumberCheckExistence(txtVoucherNo.Text.Trim(), 0, decVoucherTypeId) == true && btnSave.Text == "Save")
                {
                    //Messages.InformationMessage("Invoice number already exist");
                    //txtVoucherNo.Focus();
                }
                else if (false)//(obj.TransDate.Trim() == string.Empty)
                {
                    //Messages.InformationMessage("Select a date in between financial year");
                    //txtDate.Focus();
                }
                else if (false)//(obj.Currency < 1)
                {
                    //Messages.InformationMessage("Select any currency");
                    //cmbCurrency.Focus();
                }
                else
                {
                    if (false)//(rbtnManufacturing.Checked)
                    {
                        //if (inRowConsumption - 1 == 0)
                        //{
                        //    Messages.InformationMessage("Can't save Stock Journal without atleast one product with complete details");
                        //    dgvConsumption.Focus();
                        //    goto Exit;
                        //}
                        //if (inRowProduction - 1 == 0)
                        //{
                        //    Messages.InformationMessage("Can't save Stock Journal without atleast one product with complete details");
                        //    dgvProduction.Focus();
                        //    goto Exit;
                        //}

                    }
                    if (true)//(rbtnTransfer.Checked)
                    {
                        int indgvRowsConsumption = obj.ConsumptionItems.Count;
                        int indgvRowsProduction = obj.SrcLineItems.Count;

                        if (inRowConsumption - 1 == 0)
                        {
                            //Messages.InformationMessage("Can't save Stock Journal without atleast one product with complete details");
                            //dgvConsumption.Focus();
                            //goto Exit;
                        }
                        if (inRowProduction - 1 == 0)
                        {
                            //Messages.InformationMessage("Can't save Stock Journal without atleast one product with complete details");
                            //dgvProduction.Focus();
                            //goto Exit;
                        }
                        if (indgvRowsConsumption != indgvRowsProduction)
                        {
                            //Messages.InformationMessage("Please Tranfer the details");
                            //goto Exit;
                        }
                        int indexCounter = 0;
                        foreach (var productionObj in obj.SrcLineItems)
                        {
                            decimal dcConsumption = 0;
                            decimal dcProduction = 0;
                            dcProduction = Convert.ToDecimal(productionObj.Qty);
                            dcConsumption = Convert.ToDecimal(obj.ConsumptionItems[indexCounter].Qty);
                            if (productionObj.Store < 1 )
                            {
                                //Messages.InformationMessage("Rows Contains Invalid entries please fill the store Details");
                                //goto Exit;
                            }
                            if (productionObj.Store == obj.ConsumptionItems[indexCounter].Store)
                            {
                                //Messages.InformationMessage(" The Godown should be different");
                                //dgvProduction.Focus();
                                //Isexit = true;
                                //break;
                            }
                            indexCounter++;
                            if (dcConsumption != dcProduction)
                            {

                                //Messages.InformationMessage("The Quantity Should be Same");
                                //goto Exit;
                            }

                        }
                    }
                    if (false)//(rbtnStockOut.Checked)
                    {
                        //if (inRowConsumption - 1 == 0)
                        //{
                            //Messages.InformationMessage("Can't save Stock Journal without atleast one product with complete details");
                            //dgvConsumption.Focus();
                            //goto Exit;
                        //}
                    }
                    if (true)//(!Isexit)
                    {
                        if (true)//(RemoveIncompleteRowsFromConsumptionGrid())
                        {
                            if (true)//(!rbtnStockOut.Checked)
                            {
                                if (true)//(RemoveIncompleteRowsFromProductionGrid())
                                {
                                    if (obj.ConsumptionItems.Count < 1)
                                    {
                                        //MessageBox.Show("Can't save Stock Journal without atleast one product with complete details", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        //dgvConsumption.ClearSelection();
                                        //dgvConsumption.Focus();
                                        //goto Exit;
                                    }
                                    else
                                    {
                                        //if (btnSave.Text == "Save")
                                       // {

                                           // if (Messages.SaveConfirmation())
                                            //{
                                                //grandTotalAmountCalculationConsumption();
                                                //grandTotalAmountCalculationProduction();
                                                Save(obj);
                                           // }

                                        //}
                                        //if (btnSave.Text == "Update")
                                        //{
                                        //    if (Messages.UpdateConfirmation())
                                        //    {
                                        //        grandTotalAmountCalculationConsumption();
                                        //        grandTotalAmountCalculationProduction();
                                        //        Save();
                                        //    }
                                        //}
                                    }
                                }
                            }
                            else
                            {
                                //if (dgvConsumption.Rows[0].Cells["dgvtxtConsumptionProductName"].Value == null || dgvConsumption.Rows[0].Cells["dgvtxtConsumptionProductName"].Value.ToString() == string.Empty && dgvConsumption.Rows[0].Cells["dgvtxtConsumptionQty"].Value == null)
                                //{
                                //    MessageBox.Show("Can't save Stock Journals without atleast one product with complete details", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //    dgvConsumption.ClearSelection();
                                //    dgvConsumption.Focus();
                                //    goto Exit;
                                //}
                                //if (btnSave.Text == "Save")
                                //{
                                //    if (Messages.SaveConfirmation())
                                //    {
                                //        grandTotalAmountCalculationConsumption();
                                //        Save();
                                //    }
                                //}
                                //if (btnSave.Text == "Update")
                                //{
                                //    if (Messages.UpdateConfirmation())
                                //    {
                                //        grandTotalAmountCalculationConsumption();
                                //        Save();

                                //    }
                                //}

                            }
                        }
                    }
                }

              
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "SJ40:" + ex.Message;
            }
        }
        
        public void Save(SockJournalModel obj)
        {
            try
            {

                StockJournalMasterInfo infoStockJournalMaster = new StockJournalMasterInfo();
                StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
                StockJournalDetailsInfo infoStockJournalDetails = new StockJournalDetailsInfo();
                StockJournalDetailsSP spStockJournalDetails = new StockJournalDetailsSP();
                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                StockPostingInfo infoStockPosting = new StockPostingInfo();
                StockPostingSP spStockPosting = new StockPostingSP();
                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
                AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
                decimal decStockMasterId;
                if (true)//(isAutomatic == true)
                {
                   // infoStockJournalMaster.SuffixPrefixId = decSuffixPrefixId;
                    //infoStockJournalMaster.VoucherNo = strVoucherNo;
                }
                else
                {
                    infoStockJournalMaster.SuffixPrefixId = 0;
                    infoStockJournalMaster.VoucherNo = "";// strVoucherNo;
                }
                infoStockJournalMaster.ExtraDate = DateTime.Now;
                infoStockJournalMaster.InvoiceNo = "";//txtVoucherNo.Text.Trim();
                infoStockJournalMaster.Date = Convert.ToDateTime(obj.TransDate);
                infoStockJournalMaster.AdditionalCost = Convert.ToDecimal(obj.AdditionalAmountTotal);
                infoStockJournalMaster.VoucherNo = ""; //strVoucherNo;
                infoStockJournalMaster.VoucherTypeId = 1;// decVoucherTypeId;
                infoStockJournalMaster.Narration = obj.Narration.Trim();
                infoStockJournalMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoStockJournalMaster.ExchangeRateId = Convert.ToDecimal(obj.Currency);
                if (false)//(rbtnManufacturing.Checked)
                {
                    infoStockJournalMaster.Extra1 = "Manufacturing";
                }
                if(true) //(rbtnTransfer.Checked)
                {
                    infoStockJournalMaster.Extra1 = "Stock Transfer";
                }
                if(false)// (rbtnStockOut.Checked)
                {
                    infoStockJournalMaster.Extra1 = "Stock Out";
                }
                infoStockJournalMaster.Extra2 = string.Empty;
                if (true)//(btnSave.Text == "Save")
                {
                    decStockMasterId = spStockJournalMaster.StockJournalMasterAdd(infoStockJournalMaster);
                }
                else
                {
                    //infoStockJournalMaster.StockJournalMasterId = decStockJournalMasterIdForEdit;
                    //spStockJournalMaster.StockJournalMasterEdit(infoStockJournalMaster);
                    //RemoveRowStockJournalConsumptionDetails();
                    //RemoveRowStockJournalProductionDetails();
                    //if (rbtnManufacturing.Checked)
                    //{
                    //    //if (cmbFinishedGoods.SelectedIndex != 0 && txtQty.Text != string.Empty)
                    //    //{
                    //    //    txtQty_Leave(sender,e);
                    //    //}
                    //}
                    //RemoveRowStockJournalAdditionalCostDetails();
                    //spStockPosting.DeleteStockPostingForStockJournalEdit(strVoucherNo, decVoucherTypeId);
                }

                if (obj.ConsumptionItems.Count > 0)
                {
                    int inCount = obj.ConsumptionItems.Count;
                    int indexConsumptionCounter = 0;
                    foreach (var consumptionObj in obj.ConsumptionItems)
                    {
                        if (true)//(btnSave.Text == "Save")
                        {
                            infoStockJournalDetails.StockJournalMasterId = decStockMasterId;
                        }
                        else
                        {
                            infoStockJournalMaster.StockJournalMasterId = 1;//decStockJournalMasterIdForEdit;
                        }
                        infoStockJournalDetails.Extra1 = string.Empty;
                        infoStockJournalDetails.Extra2 = string.Empty;
                        infoStockJournalDetails.ExtraDate = DateTime.Now;
                        infoStockJournalDetails.ProductId = Convert.ToDecimal(consumptionObj.ProductCode);
                        infoStockJournalDetails.Qty = Convert.ToDecimal(consumptionObj.Qty);
                        infoStockJournalDetails.Rate = Convert.ToDecimal(consumptionObj.Rate);
                        infoStockJournalDetails.UnitId = Convert.ToDecimal(consumptionObj.Unit);
                        infoStockJournalDetails.UnitConversionId = 1;//Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionunitConversionId"].Value);
                        if (false)//(btnSave.Text == "Update")
                        {
                            //infoStockJournalDetails.StockJournalMasterId = decStockJournalMasterIdForEdit;
                            //if (dgvConsumption.Rows[i].Cells["dgvtxtConsumptionStockJournalDetailsId"].Value == null || dgvConsumption.Rows[i].Cells["dgvtxtConsumptionStockJournalDetailsId"].Value.ToString() == string.Empty)
                            //{

                            //    if (dgvConsumption.Rows[i].Cells["dgvcmbConsumptionBatch"].Value != null && dgvConsumption.Rows[i].Cells["dgvcmbConsumptionBatch"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.BatchId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvcmbConsumptionBatch"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.BatchId = 0;
                            //    }
                            //    if (dgvConsumption.Rows[i].Cells["dgvcmbConsumptionGodown"].Value != null && dgvConsumption.Rows[i].Cells["dgvcmbConsumptionGodown"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.GodownId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvcmbConsumptionGodown"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.GodownId = 0;
                            //    }
                            //    if (dgvConsumption.Rows[i].Cells["dgvcmbConsumptionRack"].Value != null && dgvConsumption.Rows[i].Cells["dgvcmbConsumptionRack"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.RackId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvcmbConsumptionRack"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.RackId = 0;
                            //    }
                            //    infoStockJournalDetails.Amount = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionAmount"].Value);
                            //    infoStockJournalDetails.ConsumptionOrProduction = "Consumption";
                            //    infoStockJournalDetails.Slno = Convert.ToInt32(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionSlNo"].Value);
                            //    spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetails);
                            //}
                            //else
                            //{
                            //    infoStockJournalDetails.StockJournalDetailsId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionStockJournalDetailsId"].Value);
                            //    if (dgvConsumption.Rows[i].Cells["dgvcmbConsumptionBatch"].Value != null && dgvConsumption.Rows[i].Cells["dgvcmbConsumptionBatch"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.BatchId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvcmbConsumptionBatch"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.BatchId = 0;
                            //    }
                            //    if (dgvConsumption.Rows[i].Cells["dgvcmbConsumptionGodown"].Value != null && dgvConsumption.Rows[i].Cells["dgvcmbConsumptionGodown"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.GodownId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvcmbConsumptionGodown"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.GodownId = 0;
                            //    }
                            //    if (dgvConsumption.Rows[i].Cells["dgvcmbConsumptionRack"].Value != null && dgvConsumption.Rows[i].Cells["dgvcmbConsumptionRack"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.RackId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvcmbConsumptionRack"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.RackId = 0;
                            //    }
                            //    infoStockJournalDetails.Amount = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionAmount"].Value);
                            //    infoStockJournalDetails.ConsumptionOrProduction = "Consumption";
                            //    infoStockJournalDetails.Slno = Convert.ToInt32(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionSlNo"].Value);
                            //    spStockJournalDetails.StockJournalDetailsEdit(infoStockJournalDetails);
                            //}
                        }
                        else
                        {
                            infoStockJournalDetails.BatchId = Convert.ToDecimal(consumptionObj.Batch);
                            infoStockJournalDetails.GodownId = Convert.ToDecimal(consumptionObj.Store);
                            infoStockJournalDetails.RackId = Convert.ToDecimal(consumptionObj.Rack);
                            infoStockJournalDetails.Amount = Convert.ToDecimal(consumptionObj.Amount);
                            infoStockJournalDetails.ConsumptionOrProduction = "Consumption";
                            infoStockJournalDetails.Slno = Convert.ToInt32(consumptionObj.SiNo);
                            spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetails);
                        }
                        //Stock Posting Add
                        if (false)//(btnSave.Text == "Update")
                        {
                            //infoStockPosting.BatchId = infoStockJournalDetails.BatchId;
                            //infoStockPosting.Date = Convert.ToDateTime(txtDate.Text);
                            //infoStockPosting.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            //infoStockPosting.GodownId = infoStockJournalDetails.GodownId;
                            //infoStockPosting.InwardQty = 0;
                            //infoStockPosting.OutwardQty = infoStockJournalDetails.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(infoStockJournalDetails.UnitConversionId);
                            //infoStockPosting.ProductId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionProductId"].Value);
                            //infoStockPosting.RackId = infoStockJournalDetails.RackId;
                            //infoStockPosting.Rate = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionRate"].Value);
                            //infoStockPosting.UnitId = infoStockJournalDetails.UnitId;
                            //infoStockPosting.InvoiceNo = txtVoucherNo.Text.Trim();
                            //infoStockPosting.VoucherNo = strVoucherNo;
                            //infoStockPosting.VoucherTypeId = decVoucherTypeId;
                            //infoStockPosting.AgainstVoucherTypeId = 0;
                            //infoStockPosting.AgainstInvoiceNo = "NA";
                            //infoStockPosting.AgainstVoucherNo = "NA";
                            //infoStockPosting.Extra1 = string.Empty;
                            //infoStockPosting.Extra2 = string.Empty;
                            //spStockPosting.StockPostingAdd(infoStockPosting);
                        }
                        else
                        {
                            infoStockPosting.BatchId = Convert.ToDecimal(consumptionObj.Batch);
                            infoStockPosting.Date = Convert.ToDateTime(obj.TransDate);
                            infoStockPosting.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            infoStockPosting.GodownId = Convert.ToDecimal(consumptionObj.Store);
                            infoStockPosting.InwardQty = 0;
                            infoStockPosting.OutwardQty = infoStockJournalDetails.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(infoStockJournalDetails.UnitConversionId);
                            infoStockPosting.ProductId = Convert.ToDecimal(consumptionObj.ProductCode);
                            infoStockPosting.RackId = Convert.ToDecimal(consumptionObj.Rack);
                            infoStockPosting.Rate = Convert.ToDecimal(consumptionObj.Rate);
                            infoStockPosting.UnitId = Convert.ToDecimal(consumptionObj.Unit);
                            infoStockPosting.InvoiceNo = ""; //txtVoucherNo.Text.Trim();
                            infoStockPosting.VoucherNo = "";// strVoucherNo;
                            infoStockPosting.VoucherTypeId = 1;//decVoucherTypeId;
                            infoStockPosting.AgainstVoucherTypeId = 0;
                            infoStockPosting.AgainstInvoiceNo = "NA";
                            infoStockPosting.AgainstVoucherNo = "NA";
                            infoStockPosting.Extra1 = string.Empty;
                            infoStockPosting.Extra2 = string.Empty;
                            spStockPosting.StockPostingAdd(infoStockPosting);
                        }
                    }

                }
                if (obj.SrcLineItems.Count > 0)
                {
                    int inCount = obj.SrcLineItems.Count;
                    int indexProductionCounter = 0;
                    foreach (var productionObj in obj.SrcLineItems)
                    {

                        if (false)//(btnSave.Text == "Update")
                        {
                            infoStockJournalMaster.StockJournalMasterId = 1;// decStockJournalMasterIdForEdit;
                        }
                        else
                        {
                            infoStockJournalDetails.StockJournalMasterId = decStockMasterId;
                        }
                        infoStockJournalDetails.Extra1 = string.Empty;
                        infoStockJournalDetails.Extra2 = string.Empty;
                        infoStockJournalDetails.ExtraDate = DateTime.Now;
                        infoStockJournalDetails.ProductId = Convert.ToDecimal(productionObj.ProductCode);
                        infoStockJournalDetails.Qty = Convert.ToDecimal(productionObj.Qty);
                        infoStockJournalDetails.Rate = Convert.ToDecimal(productionObj.Rate);
                        infoStockJournalDetails.UnitId = Convert.ToDecimal(productionObj.Unit);
                        infoStockJournalDetails.UnitConversionId = 1;// Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvtxtProductionunitConversionId"].Value);
                        if (false)//(btnSave.Text == "Update")
                        {
                            //infoStockJournalDetails.StockJournalMasterId = decStockJournalMasterIdForEdit;
                            //if (dgvProduction.Rows[i].Cells["dgvtxtProductionStockJournalDetailsId"].Value == null || dgvProduction.Rows[i].Cells["dgvtxtProductionStockJournalDetailsId"].Value.ToString() == string.Empty)
                            //{

                            //    if (dgvProduction.Rows[i].Cells["dgvcmbProductionBatch"].Value != null && dgvProduction.Rows[i].Cells["dgvcmbProductionBatch"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.BatchId = Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvcmbProductionBatch"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.BatchId = 0;
                            //    }
                            //    if (dgvProduction.Rows[i].Cells["dgvcmbProductionGodown"].Value != null && dgvProduction.Rows[i].Cells["dgvcmbProductionGodown"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.GodownId = Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvcmbProductionGodown"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.GodownId = 0;
                            //    }
                            //    if (dgvProduction.Rows[i].Cells["dgvcmbProductionRack"].Value != null && dgvProduction.Rows[i].Cells["dgvcmbProductionRack"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.RackId = Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvcmbProductionRack"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.RackId = 0;
                            //    }
                            //    infoStockJournalDetails.Amount = Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvtxtProductionAmount"].Value);
                            //    infoStockJournalDetails.ConsumptionOrProduction = "Production";
                            //    infoStockJournalDetails.Slno = Convert.ToInt32(dgvProduction.Rows[i].Cells["dgvtxtProductionSlNo"].Value);
                            //    spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetails);
                            //}
                            //else
                            //{
                            // infoStockJournalDetails.StockJournalDetailsId = 1;//Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvtxtProductionStockJournalDetailsId"].Value);
                            //    if (productionObj.Batch > 0)
                            //    {
                            //        infoStockJournalDetails.BatchId = Convert.ToDecimal(productionObj.Batch);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.BatchId = 0;
                            //    }
                            //    if (productionObj.Store > 0)
                            //    {
                            //        infoStockJournalDetails.GodownId = Convert.ToDecimal(productionObj.Store);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.GodownId = 0;
                            //    }
                            //    if (productionObj.Rate > 0)
                            //    {
                            //        infoStockJournalDetails.RackId = Convert.ToDecimal(productionObj.Rack);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.RackId = 0;
                            //    }
                            //    infoStockJournalDetails.Amount = Convert.ToDecimal(productionObj.Amount);
                            //    infoStockJournalDetails.ConsumptionOrProduction = "Production";
                            //    infoStockJournalDetails.Slno = Convert.ToInt32(productionObj.SiNo);
                            //    spStockJournalDetails.StockJournalDetailsEdit(infoStockJournalDetails);
                            //}
                        }
                        else
                        {
                            infoStockJournalDetails.BatchId = Convert.ToDecimal(productionObj.Batch);
                            infoStockJournalDetails.GodownId = Convert.ToDecimal(productionObj.Store);
                            infoStockJournalDetails.RackId = Convert.ToDecimal(productionObj.Rack);
                            infoStockJournalDetails.Amount = Convert.ToDecimal(productionObj.Amount);
                            infoStockJournalDetails.ConsumptionOrProduction = "Production";
                            infoStockJournalDetails.Slno = Convert.ToInt32(productionObj.SiNo);
                            spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetails);
                        }
                        //Stock Posting Add
                        if (true)//(btnSave.Text == "Save")
                        {
                            infoStockPosting.BatchId = Convert.ToDecimal(productionObj.Batch);
                            infoStockPosting.Date = Convert.ToDateTime(obj.TransDate);
                            infoStockPosting.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            infoStockPosting.GodownId = Convert.ToDecimal(productionObj.Store);
                            infoStockPosting.InwardQty = infoStockJournalDetails.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(infoStockJournalDetails.UnitConversionId);
                            infoStockPosting.OutwardQty = 0;
                            infoStockPosting.ProductId = Convert.ToDecimal(productionObj.ProductCode);
                            infoStockPosting.RackId = Convert.ToDecimal(productionObj.Rack);
                            infoStockPosting.Rate = Convert.ToDecimal(productionObj.Rate);
                            infoStockPosting.UnitId = Convert.ToDecimal(productionObj.Unit);
                            infoStockPosting.InvoiceNo = ""; //txtVoucherNo.Text.Trim();
                            infoStockPosting.VoucherNo = ""; //strVoucherNo;
                            infoStockPosting.VoucherTypeId = 1; //decVoucherTypeId;
                            infoStockPosting.AgainstVoucherTypeId = 0;
                            infoStockPosting.AgainstInvoiceNo = "NA";
                            infoStockPosting.AgainstVoucherNo = "NA";
                            infoStockPosting.Extra1 = string.Empty;
                            infoStockPosting.Extra2 = string.Empty;
                            spStockPosting.StockPostingAdd(infoStockPosting);
                        }
                        else
                        {
                            //infoStockPosting.BatchId = infoStockJournalDetails.BatchId;
                            //infoStockPosting.Date = Convert.ToDateTime(txtDate.Text);
                            //infoStockPosting.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            //infoStockPosting.GodownId = infoStockJournalDetails.GodownId;
                            //infoStockPosting.InwardQty = infoStockJournalDetails.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(infoStockJournalDetails.UnitConversionId);
                            //infoStockPosting.OutwardQty = 0;
                            //infoStockPosting.ProductId = Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvtxtProductionProductId"].Value);
                            //infoStockPosting.RackId = infoStockJournalDetails.RackId;
                            //infoStockPosting.Rate = Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvtxtProductionRate"].Value);
                            //infoStockPosting.UnitId = infoStockJournalDetails.UnitId;
                            //infoStockPosting.InvoiceNo = txtVoucherNo.Text.Trim();
                            //infoStockPosting.VoucherNo = strVoucherNo;
                            //infoStockPosting.VoucherTypeId = decVoucherTypeId;
                            //infoStockPosting.AgainstVoucherTypeId = 0;
                            //infoStockPosting.AgainstInvoiceNo = "NA";
                            //infoStockPosting.AgainstVoucherNo = "NA";
                            //infoStockPosting.Extra1 = string.Empty;
                            //infoStockPosting.Extra2 = string.Empty;
                            //spStockPosting.StockPostingAdd(infoStockPosting);

                        }
                    }

                }
                //....Additional Cost Add...////
                if (false)//(btnSave.Text == "Update")
                {
                    //spLedgerPosting.DeleteLedgerPostingForStockJournalEdit(strVoucherNo, decVoucherTypeId);//Delete
                    //spAdditionalCost.DeleteAdditionalCostForStockJournalEdit(strVoucherNo, decVoucherTypeId);//Delete
                }
                decimal decGrandTotal = 0;
                decimal decRate = 0;
                ExchangeRateSP spExchangeRate = new ExchangeRateSP();
                if (obj.AdditionalCostItems.Count > 1)
                {
                    infoAdditionalCost.Credit = Convert.ToDecimal(obj.AdditionalAmountTotal);
                    infoAdditionalCost.Debit = 0;
                    infoAdditionalCost.LedgerId = Convert.ToDecimal(obj.CashNBank);
                    infoAdditionalCost.VoucherNo = "";// strVoucherNo;
                    infoAdditionalCost.VoucherTypeId = 1;//decVoucherTypeId;
                    infoAdditionalCost.Extra1 = string.Empty;
                    infoAdditionalCost.Extra2 = string.Empty;
                    infoAdditionalCost.ExtraDate = DateTime.Now;
                    spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                    //....Ledger Posting Add...///
                    //-------------------  Currency Conversion-----------------------------
                    decGrandTotal = Convert.ToDecimal(obj.AdditionalAmountTotal);
                    decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(obj.Currency));
                    decGrandTotal = decGrandTotal * decRate;
                    //---------------------------------------------------------------
                    infoLedgerPosting.Credit = decGrandTotal;
                    infoLedgerPosting.Debit = 0;
                    infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);
                    infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.InvoiceNo = "";//txtVoucherNo.Text.Trim();
                    infoLedgerPosting.LedgerId = Convert.ToDecimal(obj.CashNBank);
                    infoLedgerPosting.VoucherNo = "";// strVoucherNo;
                    infoLedgerPosting.VoucherTypeId = 1;// decVoucherTypeId;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    if (true)//(btnSave.Text == "Save")
                    {
                        infoLedgerPosting.ExtraDate = DateTime.Now;
                    }
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                    
                        foreach (var additiobnalCostObj  in obj.AdditionalCostItems)
                    {
                        if (additiobnalCostObj.AccountLedger > 0)
                        {
                            try
                            {
                            /*-----------------------------------------Additional Cost Add----------------------------------------------------*/
                            infoAdditionalCost.Credit = 0;
                            infoAdditionalCost.Debit = Convert.ToDecimal(additiobnalCostObj.Amount);
                            infoAdditionalCost.LedgerId = Convert.ToDecimal(additiobnalCostObj.AccountLedger);
                            infoAdditionalCost.VoucherNo = "";//strVoucherNo;
                            infoAdditionalCost.VoucherTypeId = 1;// decVoucherTypeId;
                            infoAdditionalCost.Extra1 = string.Empty;
                            infoAdditionalCost.Extra2 = string.Empty;
                            infoAdditionalCost.ExtraDate = DateTime.Now;
                            spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                            /*-----------------------------------------Additional Cost Ledger Posting----------------------------------------------------*/
                            decimal decTotal = 0;
                            //-------------------  Currency Conversion------------------------
                            decTotal = Convert.ToDecimal(infoAdditionalCost.Debit);
                            decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(obj.Currency));
                            decTotal = decTotal * decRate;
                            //---------------------------------------------------------------
                            infoLedgerPosting.Credit = 0;
                            infoLedgerPosting.Debit = decTotal;
                            infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);
                            infoLedgerPosting.DetailsId = 0;
                            infoLedgerPosting.InvoiceNo = "";//txtVoucherNo.Text.Trim();
                            infoLedgerPosting.LedgerId = Convert.ToDecimal(additiobnalCostObj.AccountLedger);
                            infoLedgerPosting.VoucherNo = "";//strVoucherNo;
                            infoLedgerPosting.VoucherTypeId = 1;// decVoucherTypeId;
                            infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                            infoLedgerPosting.ChequeDate = DateTime.Now;
                            infoLedgerPosting.ChequeNo = string.Empty;
                            infoLedgerPosting.Extra1 = string.Empty;
                            infoLedgerPosting.Extra2 = string.Empty;
                            infoLedgerPosting.ExtraDate = DateTime.Now;
                            
                                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                            }
                            catch (Exception ex)
                            {

                            }




                        }
                    }
                }
                if (true)//(btnSave.Text == "Save")
                {
                    //Messages.SavedMessage();
                   // if (cbxPrintAfterSave.Checked)
                    //{
                        //Print(decStockMasterId);
                    //}
                    //Clear();
                }
                else
                {
                    //Messages.UpdatedMessage();
                    //if (cbxPrintAfterSave.Checked)
                    //{
                       // Print(decStockJournalMasterIdForEdit);
                   // }
                   // this.Close();
                }

            }

            catch (Exception ex)
            {
              //  formMDI.infoError.ErrorString = "SJ36:" + ex.Message;
            }
        }






    }
}
