﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;

namespace MatApi.Controllers.Inventory
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ItemGroupController : ApiController
    {
        ProductGroupSP productGroupSp;
        public ItemGroupController()
        {
            productGroupSp = new ProductGroupSP();
        }

        [HttpGet]
        public string groupLevels()
        {
            List<ProductGroupInfo> productGroups = new List<ProductGroupInfo>();
            string output = "";
            var productGroupsDt = productGroupSp.ProductGroupViewAll();
            
            foreach (DataRow row in productGroupsDt.Rows)
            {
                productGroups.Add(new ProductGroupInfo
                {
                    GroupId = Convert.ToDecimal(row["GroupId"]),
                    GroupName = row["GroupName"].ToString(),
                    GroupUnder = Convert.ToDecimal(row["narration"].ToString()),
                    Extra1 = row["extra1"].ToString()
                });
            }

            output += "Id-----" + "Sub Category-----" + "Category-----" + "Sub Group-----" + "Division"+Environment.NewLine;

            List<ProductGroupInfo> subcategories = new List<ProductGroupInfo>();
            subcategories = productGroups
                .Where(p => p.Extra1 == "Sub Category")
                .ToList();

            List<customGroupDisplay> jsonOutput = new List<customGroupDisplay>();
            foreach (ProductGroupInfo subcategory in subcategories)
            {
                
                ProductGroupInfo cat = productGroups.FirstOrDefault(p => p.GroupId == subcategory.GroupUnder && p.Extra1=="Category");
                ProductGroupInfo subGrp = productGroups.FirstOrDefault(p => p.GroupId == cat.GroupUnder && p.Extra1 == "Sub Group");
                ProductGroupInfo grp = productGroups.FirstOrDefault(p => p.GroupId == subGrp.GroupUnder && p.Extra1 == "Group");
                ProductGroupInfo div = productGroups.FirstOrDefault(p => p.GroupId == grp.GroupUnder && p.Extra1 == "Division");

                jsonOutput.Add(new customGroupDisplay
                {
                    GroupId = subcategory.GroupId.ToString(),
                    Subcategory = subcategory.GroupName,
                    Category = cat.GroupName,
                    Subgroup=grp.GroupName,
                    Group=grp.GroupName,
                    Division=div.GroupName
                });

                output += subcategory.GroupId+"-----" + subcategory.GroupName+"-----" + ((cat==null)?"N/A":cat.GroupName)+"-----" + ((subGrp == null) ? "N/A" : subGrp.GroupName) + "-----" +(( grp == null) ? "N/A" : grp.GroupName) + "-----"+ ((div == null) ? "N/A" : div.GroupName) + Environment.NewLine;
            }
            string jsondata = new JavaScriptSerializer().Serialize(jsonOutput);
            //string path = Server.MapPath("~/App_Data/");
            // Write that JSON to txt file,  
            System.IO.File.WriteAllText(HttpContext.Current.Server.MapPath("~/App_Data/output.json"), jsondata);
            System.IO.File.WriteAllText(HttpContext.Current.Server.MapPath("~/App_Data/subcategories.txt"), output);
            return output;
        }

        public List<ProductGroupInfo> GetProductGroups()
        {
            var productGroupsDt = productGroupSp.ProductGroupViewAll();
            //var productGroupsDt = productGroupSp.ProductGroupViewForComboFillForProductGroup();
            List<ProductGroupInfo> productGroups = new List<ProductGroupInfo>();
            foreach (DataRow row in productGroupsDt.Rows)
            {
                //if (Convert.ToDecimal(row["GroupId"])==1)
                //{
                //    continue;   //don't show the first group
                //}
                //else
                //{
                //    productGroups.Add(new ProductGroupInfo
                //    {
                //        GroupId = Convert.ToDecimal(row["GroupId"]),
                //        GroupName = row["GroupName"].ToString(),
                //        GroupUnder = Convert.ToDecimal(row["narration"].ToString())
                //    });
                //}
                productGroups.Add(new ProductGroupInfo
                {
                    GroupId = Convert.ToDecimal(row["GroupId"]),
                    GroupName = row["GroupName"].ToString(),
                    GroupUnder = Convert.ToDecimal(row["narration"].ToString()),
                    Extra1= row["extra1"].ToString()
                });

            }
            return productGroups;
        }

        private string findGroupName(decimal groupId)
        {
            var productGroupsDt = productGroupSp.ProductGroupViewAll();
            foreach (DataRow row in productGroupsDt.Rows)
            {
                if (Convert.ToDecimal(row["GroupId"]) == groupId)
                {
                    return row["GroupName"].ToString();
                }
            }
            return "";
        }

        public ProductGroupInfo GetProductGroup(decimal groupId)
        {
            var productGroupDt = productGroupSp.ProductGroupView(groupId);
            ProductGroupInfo productGroup = new ProductGroupInfo
            {
                GroupId = Convert.ToDecimal(productGroupDt.GroupId),
                GroupName = productGroupDt.GroupName,
                Extra1=productGroupDt.Extra1
            };
            return productGroup;
        }

        [HttpPost]
        public bool DeleteProductGroup(ProductGroupInfo productGroup)
        {
            if (productGroupSp.ProductGroupReferenceDelete(productGroup.GroupId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public ProductGroupInfo AddProductGroup(ProductGroupInfo productGroup)
        {
            productGroup.Extra1 = productGroup.Extra1;
            productGroup.Extra2 = "";
            productGroup.Narration = "";
            productGroup.ExtraDate = DateTime.Now;
            if (productGroupSp.ProductGroupAdd(productGroup) > 0)
            {
                return productGroup;
            }
            return null;
        }

        [HttpPost]
        public ProductGroupInfo EditProductGroup(ProductGroupInfo productGroup)
        {
            productGroup.Extra1 = "";
            productGroup.Extra2 = "";
            productGroup.Narration = "";
            productGroup.ExtraDate = DateTime.Now;
            if(productGroupSp.ProductGroupEdit(productGroup))
            {
                return productGroup;
            }
            return null;
        }
    }

    public class customGroupDisplay
    {
        public string GroupId { get; set; }
        public string Subcategory { get; set; }
        public string Category { get; set; }
        public string Subgroup { get; set; }
        public string Group { get; set; }
        public string Division { get; set; }
    }
}
