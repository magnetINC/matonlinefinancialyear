﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory.ItemComponent
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UnitController : ApiController
    {
        UnitSP unitSp;
        public UnitController()
        {
            unitSp = new UnitSP();
        }

        public List<UnitInfo> GetUnits()
        {
            var unitsDt = unitSp.UnitViewAll();
            List<UnitInfo> units = new List<UnitInfo>();
            foreach (DataRow row in unitsDt.Rows)
            {
                units.Add(new UnitInfo
                {
                    UnitId = Convert.ToDecimal(row["UnitId"]),
                    UnitName = row["UnitName"].ToString(),
                    //formalName = row["formalName"].ToString(),
                    noOfDecimalplaces = Convert.ToDecimal(row["noOfDecimalplaces"]),
                    //Extra1 = row["Extra1"].ToString(),
                    //Extra2 = row["Extra2"].ToString(),
                    Narration = row["Narration"].ToString()
                });
            }
            return units;
        }

        public UnitInfo GetUnit(decimal unitId)
        {
            var unitDt = unitSp.UnitView(unitId);
            UnitInfo unit = new UnitInfo
            {
                UnitId = Convert.ToDecimal(unitDt.UnitId),
                UnitName = unitDt.UnitName,
                //formalName = unitDt.formalName,
                noOfDecimalplaces = Convert.ToDecimal(unitDt.noOfDecimalplaces),
                //Extra1=unitDt.Extra1,
                //Extra2 = unitDt.Extra2,
                //ExtraDate = Convert.ToDateTime(unitDt.ExtraDate),
                Narration = unitDt.Narration

            };
            return unit;
        }

        [HttpPost]
        public bool DeleteUnit(UnitInfo unit)
        {
            if (unitSp.UnitDeleteCheck(unit.UnitId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public decimal AddUnit(UnitInfo unit)
        {
            if (unitSp.UnitCheckExistence(unit.UnitName, 0) == true)
            {
                return 2;
            }
            else
            {
                unit.Extra1 = "";
                unit.Extra2 = "";
                unit.Narration = "";
                unit.formalName = "";
                unit.ExtraDate = DateTime.Now;
                if (unitSp.UnitAdd(unit) > 0)
                {
                    return 1;
                }
            }

            return 0;
        }
        [HttpPost]
        public bool EditUnit(UnitInfo unit)
        {
            unit.Extra1 = "";
            unit.Extra2 = "";
            unit.Narration = "";
            unit.formalName = "";
            unit.ExtraDate = DateTime.Now;
            return unitSp.UnitEdit(unit);
        }
    }
}
