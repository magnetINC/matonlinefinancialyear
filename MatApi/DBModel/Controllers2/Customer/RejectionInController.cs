﻿using MatApi.Models;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RejectionInController : ApiController
    {
        public RejectionInController()
        {

        }

        decimal decRejectionInVoucherTypeId = 18;
        decimal decRejectionInSuffixPrefixId = 0;
        decimal decRejectionInIdToEdit = 0;
        decimal decCurrentConversionRate = 0;
        decimal decCurrentRate = 0;
        string strVoucherNo = string.Empty;
        string strSalesman = string.Empty;
        string strCashorParty = string.Empty;
        string strRejectionInVoucherNo = string.Empty;
        TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();
        RejectionInMasterSP spRejectionInMaster = new RejectionInMasterSP();

        [HttpGet]
        public HttpResponseMessage LookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var customers = transactionGeneralFillObj.CashOrPartyUnderSundryDrComboFill();
            var salesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();
            var pricingLevels = transactionGeneralFillObj.PricingLevelViewAll();
            var voucherTypes = transactionGeneralFillObj.VoucherTypeComboFill("Delivery Note", false);
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            dynamic response = new ExpandoObject();
            response.Customers = customers;
            response.SalesMen = salesMen;
            response.PricingLevels = pricingLevels;
            response.Currencies = currencies;
            response.VoucherTypes = voucherTypes;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetDeliveryNoteNo(decimal customerId, decimal voucherType)
        {
            DeliveryNoteMasterSP spdeliverynotemaster = new DeliveryNoteMasterSP();
            var deliveryNotesNo = spdeliverynotemaster.DeliveryNoteNoCorrespondingToLedger(customerId, 0, voucherType);
            return Request.CreateResponse(HttpStatusCode.OK, deliveryNotesNo);
        }

        [HttpGet]
        public List<RejectionInLineItems> FillGridCorrespondingToDeliveryNoteNo(decimal deliveryNoteNo)
        {
            DeliveryNoteDetailsSP SPDeliveryNoteDetails = new DeliveryNoteDetailsSP();
            List<RejectionInLineItems> response = new List<RejectionInLineItems>();
            try
            {
                var notes = SPDeliveryNoteDetails.DeliveryNoteDetailsViewByDeliveryNoteMasterIdWithPending(deliveryNoteNo, 0);
                for (int i = 0; i < notes.Rows.Count; i++)
                {
                    var note = notes.Rows[i].ItemArray;
                    var prod = new ProductSP().ProductView(Convert.ToDecimal(note[2]));
                    response.Add(new RejectionInLineItems
                    {
                        DeliveryNoteDetailsId = Convert.ToDecimal(note[0]),
                        Amount = Convert.ToDecimal(note[10]),
                        Barcode = note[11].ToString(),
                        Batch = note[9].ToString(),
                        BatchId = Convert.ToDecimal(note[9]),
                        ProductId = prod.ProductId,
                        ProductCode = prod.ProductCode,
                        ProductName = prod.ProductName,
                        Quantity = Convert.ToDecimal(note[3]),
                        Rack = new RackSP().RackView(Convert.ToDecimal(note[8])).RackName,
                        RackId = Convert.ToDecimal(note[8]),
                        Rate = Convert.ToDecimal(note[4]),
                        Store = new GodownSP().GodownView(Convert.ToDecimal(note[7])).GodownName,
                        StoreId = Convert.ToDecimal(note[7]),
                        Unit = new UnitSP().UnitView(Convert.ToDecimal(note[5])).UnitName,
                        UnitId = Convert.ToDecimal(note[5]),
                        UnitConversionId = Convert.ToDecimal(note[6])
                    });
                }

            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RI12:" + ex.Message;
            }
            return response;
        }

        //[HttpPost]
        //public bool savePendingRejectionIn(CreateRejectionInVM input)
        //{
        //    try
        //    {
        //        MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
        //        input.Date = DateTime.Now;
        //        var status = "Pending";
        //        string masterQuery = string.Format("INSERT INTO tbl_RejectionInMaster_Pending " +
        //                                            "VALUES('{0}','{1}',{2},{3},'{4}',{5},'{6}',{7},'{8}',{9},{10},{11},'{12}',{13},'{14}',{15},'{16}','{17}','{18}', '{19}');"
        //                                            , input.ReceiptNo,
        //                                            input.ReceiptNo,
        //                                            1,
        //                                            decMaterialReceiptVoucherTypeId,
        //                                            input.Date,
        //                                            input.SupplierId,
        //                                            "",
        //                                            input.OrderMasterId,
        //                                            input.Narration,
        //                                            input.TotalAmount,
        //                                            1,
        //                                            MATFinancials.PublicVariables._decCurrentUserId,
        //                                            "",
        //                                            1,
        //                                            "",
        //                                            MATFinancials.PublicVariables._decCurrentFinancialYearId,
        //                                            DateTime.Now,
        //                                            "",
        //                                            "",
        //                                            approved);

        //        string detailsQuery = string.Format("");
        //        if (db.ExecuteNonQuery2(masterQuery))
        //        {
        //            string id = db.getSingleValue("SELECT top 1 materialReceiptMasterId from tbl_MaterialReceiptMaster_Pending order by materialReceiptMasterId desc");
        //            foreach (var row in input.LineItems)
        //            {
        //                var amount = row.Rate * row.Quantity;
        //                detailsQuery = string.Format("INSERT INTO tbl_MaterialReceiptDetails_Pending " +
        //                                            "VALUES({17},{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},'{11}','{12}','{13}',{14},{15},'{16}')"
        //                                            , row.ProductId,
        //                                            row.OrderDetailsId,
        //                                            row.Quantity,
        //                                            row.Rate,
        //                                            row.UnitId,
        //                                            row.UnitConversionId,
        //                                            row.BatchId,
        //                                            row.StoreId,
        //                                            row.RackId,
        //                                            amount,
        //                                            row.SL,
        //                                            DateTime.Now,
        //                                            "",
        //                                            "",
        //                                            0,
        //                                            0,
        //                                            row.Description,
        //                                            id);
        //                db.ExecuteNonQuery2(detailsQuery);
        //                amount = 0;
        //            }
        //            if (input.OrderDetailsId > 0)
        //            {
        //                string status = "Approved";
        //                string updateQuery = string.Format("");
        //                updateQuery = string.Format("UPDATE tbl_PurchaseOrderMaster " +
        //                                            "SET orderStatus='{0}' " +
        //                                            "WHERE purchaseOrderMasterId={1} ", status, input.OrderMasterId);
        //                if (db.customUpdateQuery(updateQuery) > 0)
        //                {
        //                    return true;
        //                }
        //            }
        //            else
        //            {
        //                return true;
        //            }
        //        }
        //        //db.CloseConnection();
        //    }
        //    catch (Exception e)
        //    {

        //    }

        //    return false;
        //}
        [HttpGet]
        public HttpResponseMessage getPendingRejectionIn(DateTime fromDate, DateTime toDate, string approved)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            dynamic response = new ExpandoObject();

            string GetQuery = string.Format("");
            if (approved == "All" || approved == "" || approved == null)
            {
                GetQuery = string.Format("");
                GetQuery = string.Format("SELECT * FROM tbl_RejectionInMaster_Pending WHERE date BETWEEN '{0}' AND '{1}' ORDER BY date DESC", fromDate, toDate);
            }
            else
            {
                GetQuery = string.Format("");
                GetQuery = string.Format("SELECT * FROM tbl_RejectionInMaster_Pending WHERE date BETWEEN '{0}' AND '{1}' AND status='{2}' ORDER BY date DESC", fromDate, toDate, approved);
            }
            response.pendingRejectionIns = db.GetDataSet(GetQuery);
            response.Products = new ProductSP().ProductViewAll();
            response.unit = new UnitSP().UnitViewAll();
            response.batch = new BatchSP().BatchViewAll();
            response.stores = new GodownSP().GodownViewAll();
            response.ledgers = new AccountLedgerSP().AccountLedgerViewCustomerOnly();
            response.users = new UserSP().UserViewAll();
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage getPendingRejectionInDetails(decimal id)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            dynamic response = new ExpandoObject();

            string getQuery = "";
            getQuery = string.Format("SELECT * FROM tbl_RejectionInDetails_Pending WHERE rejectionInMasterId = {0}", id);
            response.details = db.GetDataSet(getQuery);

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpPost]
        public string savePendingRejectionIn(CreateRejectionInVM input)
        {
            var result = "";

            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string masterQuery = string.Format("INSERT INTO tbl_RejectionInMaster_Pending " +
                                                    "VALUES('{0}',{1},{2},{3},'{4}',{5},{6},'{7}','{8}',{9},'{10}')"
                                                    , input.Date,
                                                    input.CustomerId,
                                                    input.PricingLevelId,
                                                    input.UserId,
                                                    input.Narration,
                                                    input.TotalAmount,
                                                    input.UserId,
                                                    input.LrNo,
                                                    input.TransportationCompany,
                                                    input.DeliveryNoteMasterId,
                                                    "Pending");
            if (db.ExecuteNonQuery2(masterQuery))
            {
                string id = db.getSingleValue("SELECT top 1 rejectionInMasterId from tbl_RejectionInMaster_Pending order by rejectionInMasterId desc");
                foreach (var row in input.LineItems)
                {
                    //var amount = row.Rate * row.Qty;
                    string detailsQuery1 = string.Format("INSERT INTO tbl_RejectionInDetails_Pending " +
                                                "VALUES({0},{1},{2},{3},{4},{5},{6},'{7}',{8},'{9}',{10},{11},'{12}','{13}','{14}',{15},{16})"
                                                , id,
                                                row.DeliveryNoteDetailsId,
                                                row.ProductId,
                                                row.Quantity,
                                                row.Rate,
                                                row.UnitId,
                                                row.UnitConversionId,
                                                row.BatchId,
                                                row.StoreId,
                                                row.RackId,
                                                row.Amount,
                                                row.SL,
                                                DateTime.Now,
                                                "",
                                                "",
                                                1,
                                                1);
                    if (db.ExecuteNonQuery2(detailsQuery1))
                    {
                        result = "success";
                    }
                    else
                    {
                        result = "failed";
                    }
                }
            }

            return result;
        }

        [HttpPost]
        public string saveRejectionIn(CreateRejectionInVM input)
        {
            try
            {
                VoucherNoGeneration();

                decimal decIdentity = 0;
                DeliveryNoteMasterSP SpDeliveryNoteMaster = new DeliveryNoteMasterSP();
                DeliveryNoteMasterInfo InfoDeliveryNoteMaster = new DeliveryNoteMasterInfo();
                InfoDeliveryNoteMaster = SpDeliveryNoteMaster.DeliveryNoteMasterView(input.DeliveryNoteMasterId);
                DeliveryNoteDetailsInfo infoDeliveryNoteDetails = new DeliveryNoteDetailsInfo();
                var details = new DeliveryNoteDetailsSP().DeliveryNoteDetailsViewByDeliveryNoteMasterId(input.DeliveryNoteMasterId);
                RejectionInMasterInfo InfoRejectionInMaster = new RejectionInMasterInfo();
                RejectionInDetailsInfo InfoRejectionInDetails = new RejectionInDetailsInfo();
                RejectionInDetailsSP SpRejectionInDetails = new RejectionInDetailsSP();
                StockPostingInfo InfoStockPosting = new StockPostingInfo();
                StockPostingSP SpStockPosting = new StockPostingSP();

                InfoRejectionInMaster.VoucherNo = strVoucherNo;
                InfoRejectionInMaster.InvoiceNo = strVoucherNo;
                InfoRejectionInMaster.VoucherTypeId = decRejectionInVoucherTypeId;
                InfoRejectionInMaster.SuffixPrefixId = decRejectionInSuffixPrefixId;
                InfoRejectionInMaster.Date = input.Date;
                InfoRejectionInMaster.LedgerId = input.CustomerId;
                InfoRejectionInMaster.PricinglevelId = input.PricingLevelId;
                InfoRejectionInMaster.EmployeeId = input.UserId;
                InfoRejectionInMaster.Narration = input.Narration;
                InfoRejectionInMaster.ExchangeRateId = 1;
                InfoRejectionInMaster.TotalAmount = input.TotalAmount;
                InfoRejectionInMaster.UserId = input.UserId;
                InfoRejectionInMaster.LrNo = input.LrNo;
                InfoRejectionInMaster.TransportationCompany = input.TransportationCompany;
                InfoRejectionInMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                InfoRejectionInMaster.Extra1 = string.Empty;
                InfoRejectionInMaster.Extra2 = string.Empty;
                InfoRejectionInMaster.DeliveryNoteMasterId = input.DeliveryNoteMasterId;

                if (decRejectionInIdToEdit == 0)
                {
                    decIdentity = spRejectionInMaster.RejectionInMasterAdd(InfoRejectionInMaster);
                }
                else
                {
                    InfoRejectionInMaster.RejectionInMasterId = decRejectionInIdToEdit;
                    spRejectionInMaster.RejectionInMasterEdit(InfoRejectionInMaster);
                }
                if (decRejectionInIdToEdit == 0)
                {
                    InfoRejectionInDetails.RejectionInMasterId = decIdentity;
                }
                else
                {
                    SpRejectionInDetails.DeleteRejectionInDetailsByRejectionInMasterId(decRejectionInIdToEdit);
                    SpStockPosting.DeleteStockPostingByAgnstVouTypeIdAndAgnstVouNo(decRejectionInVoucherTypeId, strVoucherNo);
                    InfoRejectionInDetails.RejectionInMasterId = decRejectionInIdToEdit;
                }
                foreach (var lineItem in input.LineItems)
                {
                    InfoRejectionInDetails.DeliveryNoteDetailsId = lineItem.DeliveryNoteDetailsId;
                    InfoRejectionInDetails.ProductId = lineItem.ProductId;
                    InfoRejectionInDetails.Qty = lineItem.Quantity;
                    InfoRejectionInDetails.Rate = lineItem.Rate;
                    InfoRejectionInDetails.UnitId = lineItem.UnitId;
                    InfoRejectionInDetails.UnitConversionId = lineItem.UnitConversionId;
                    InfoRejectionInDetails.BatchId = lineItem.BatchId;
                    InfoRejectionInDetails.GodownId = lineItem.StoreId;
                    InfoRejectionInDetails.RackId = lineItem.RackId;
                    InfoRejectionInDetails.Amount = lineItem.Amount;
                    InfoRejectionInDetails.SlNo = Convert.ToInt32(lineItem.SL);
                    InfoRejectionInDetails.Extra1 = string.Empty;
                    InfoRejectionInDetails.Extra2 = string.Empty;
                    InfoRejectionInDetails.ProjectId = 1;
                    InfoRejectionInDetails.CategoryId = 1;
                    SpRejectionInDetails.RejectionInDetailsAdd(InfoRejectionInDetails);

                    InfoStockPosting.Date = input.Date;
                    InfoStockPosting.VoucherTypeId = InfoDeliveryNoteMaster.VoucherTypeId;
                    InfoStockPosting.VoucherNo = InfoDeliveryNoteMaster.VoucherNo;
                    InfoStockPosting.InvoiceNo = InfoDeliveryNoteMaster.InvoiceNo;
                    InfoStockPosting.ProductId = lineItem.ProductId;
                    InfoStockPosting.BatchId = lineItem.BatchId;
                    InfoStockPosting.UnitId = lineItem.UnitId;
                    InfoStockPosting.GodownId = lineItem.StoreId;
                    InfoStockPosting.RackId = lineItem.RackId;
                    InfoStockPosting.ProjectId = 0;
                    InfoStockPosting.CategoryId = 0;
                    InfoStockPosting.AgainstVoucherTypeId = decRejectionInVoucherTypeId;
                    InfoStockPosting.AgainstInvoiceNo = strVoucherNo;
                    InfoStockPosting.AgainstVoucherNo = strVoucherNo;
                    InfoStockPosting.InwardQty = lineItem.Quantity;
                    InfoStockPosting.OutwardQty = 0;
                    InfoStockPosting.Rate = lineItem.Rate;
                    InfoStockPosting.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    InfoStockPosting.Extra1 = string.Empty;
                    InfoStockPosting.Extra2 = string.Empty;
                    SpStockPosting.StockPostingAdd(InfoStockPosting);
                }
                DBMatConnection conn = new DBMatConnection();
                string queryStr = string.Format("UPDATE tbl_RejectionInMaster_Pending " +
                                                "SET status='{0}'" +
                                                "WHERE deliveryNoteMasterId={1} ", "Approved", input.DeliveryNoteMasterId);
                if (conn.customUpdateQuery(queryStr) > 0)
                {
                    return "Rejection Successfully Saved";
                }
                return "Rejection not Successfully Saved";
            }
            catch (Exception ex)
            {

            }
            return "Rejection Successfully Saved"; ;
        }
        public string VoucherNoGeneration()
        {
            string strRejectionIn = "RejectionInMaster";
            string strSuffix = string.Empty;
            string strPrefix = string.Empty;
            string strInvoiceNo = string.Empty;
            try
            {
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }
                strVoucherNo = transactionGeneralFillObj.VoucherNumberAutomaicGeneration(decRejectionInVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strRejectionIn);

                SuffixPrefixSP spSuffixPrefix = new SuffixPrefixSP();
                SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                infoSuffixPrefix = spSuffixPrefix.GetSuffixPrefixDetails(decRejectionInVoucherTypeId, DateTime.Now);
                strPrefix = infoSuffixPrefix.Prefix;
                strSuffix = infoSuffixPrefix.Suffix;
                strInvoiceNo = strPrefix + strVoucherNo + strSuffix;
            }
            catch (Exception ex)
            {
            }
            return strVoucherNo;
        }
    }
}