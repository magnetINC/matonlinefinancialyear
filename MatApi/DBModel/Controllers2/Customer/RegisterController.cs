using MatApi.Models;
using MatApi.Models.Customer;
using MatApi.Models.Register;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RegisterController : ApiController
    {
        decimal decQuotationMasterId = 0;
        TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();

        [HttpGet]
        public HttpResponseMessage CashOrPartyComboFill()
        {
            try
            {
                var cashOrParty = TransactionGeneralFillObj.CashOrPartyUnderSundryDrComboFill(true);

                return Request.CreateResponse(HttpStatusCode.OK, (object)cashOrParty);

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpGet]
        public DataTable SalesModeComboFill(int id)
        {
            decimal decVoucherTypeId = 0;
            DataTable dtbl = new DataTable();
            SalesDetailsSP spSalesDetails = new SalesDetailsSP();
            try
            {
                decVoucherTypeId = id;
                dtbl = spSalesDetails.voucherNoViewAllByVoucherTypeIdForSi(decVoucherTypeId);
                DataRow drow = dtbl.NewRow();
                drow["invoiceNo"] = "All";
                dtbl.Rows.InsertAt(drow, 0);

                return dtbl;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        [HttpGet]
        public DataTable voucherTypeComboFill()
        {
            var vType = new TransactionsGeneralFill().VoucherTypeComboFill("Rejection In", true);
            DataRow drow = vType.NewRow();
            drow["voucherTypeName"] = "-Select Type-";
            vType.Rows.InsertAt(drow, 0);
            return vType;
        }

        [HttpGet]
        public DataTable cashOrBankComboFill()
        {
            try
            {
                DataTable dtbl = new DataTable();
                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                dtbl = obj.AccountLedgerComboFill();

                // modify dtbl to get only cash and banks
                decimal[] ledgers = new decimal[] { 27, 28 };
                var query = (from d in dtbl.AsEnumerable()
                             where ledgers.Contains(d.Field<decimal>("accountGroupId"))
                             select d).ToList();
                dtbl = query.CopyToDataTable();

                return dtbl;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        [HttpGet]
        public DataTable AccountLedgerComboFill()
        {
            try
            {
                DataTable dtbl = new DataTable();
                PDCPayableMasterSP sppdcpayable = new PDCPayableMasterSP();
                dtbl = sppdcpayable.AccountLedgerComboFill(false);
                DataRow dr = dtbl.NewRow();
                dr["ledgerId"] = 0;
                dr["ledgerName"] = "All";
                dtbl.Rows.InsertAt(dr, 0);

                return dtbl;
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        [HttpPost]
        public List<SalesOrderConfirmationListVM> OrderConfirmationListing(SalesOrderListingSearchParameters searchParam)
        {
            List<SalesOrderConfirmationListVM> response = new List<SalesOrderConfirmationListVM>();
            string salesQuotationMasterListQuery = "";
            if (searchParam.condition == "")
            {
                salesQuotationMasterListQuery = string.Format("select * from tbl_salesquotationmaster where date>='{0}' and date<='{1}'",searchParam.fromDate,searchParam.toDate);
            }
            else
            {
                salesQuotationMasterListQuery = string.Format("select * from tbl_salesquotationmaster where date>='{0}' and date<='{1}' and approved='{2}'", searchParam.fromDate, searchParam.toDate,searchParam.condition);
            }
            DataTable salesQuotationMasterList = new DBMatConnection().customSelect(salesQuotationMasterListQuery);
            DataTable accountLedgers = new AccountLedgerSP().AccountLedgerViewAll();
            DataTable voucherTypes = new VoucherTypeSP().VoucherTypeViewAll();
            DataTable users = new UserSP().UserViewAll();

            foreach (DataRow dr in salesQuotationMasterList.Rows)
            {
                UserInfo salesOfficer = new UserSP().UserView(Convert.ToDecimal(dr[17].ToString()));
                UserInfo gm = new UserSP().UserView(searchParam.gmUserId);
                if(gm.StoreId==salesOfficer.StoreId)    //check if this gm is in the same warehouse with the salesofficer
                {
                    //check if items in the order are placed for location of this gm
                    string itemLocationQuery = string.Format("select extra1 from tbl_salesquotationdetails where quotationmasterid={0}", dr[0].ToString());
                    string itemLocation = new DBMatConnection().getSingleValue(itemLocationQuery);
                    if(itemLocation==gm.StoreId)
                    {
                        UserInfo user = new UserSP().UserView(Convert.ToDecimal(dr[17].ToString()));
                        //for(int i=0;i<users.Rows[0].ItemArray.Table.Rows.Count;i++)
                        //{
                        //    if(users.Rows[0].Table.Rows[i].)
                        //}
                        response.Add(new SalesOrderConfirmationListVM
                        {
                            approved = dr[9].ToString(),
                            date = Convert.ToDateTime(dr[5].ToString()).ToString("dd-MMM-yyyy"),
                            invoiceNo = dr[1].ToString(),
                            ledgerId = dr[7].ToString(),
                            ledgerName = new AccountLedgerSP().AccountLedgerView(Convert.ToDecimal(dr[7].ToString())).LedgerName,
                            //ledgerName = "",
                            narration = dr[11].ToString(),
                            QuotationMasterId = dr[0].ToString(),
                            totalAmount = dr[10].ToString(),
                            userName = user.FirstName + " " + user.LastName ,
                            voucherTypeName = new VoucherTypeSP().VoucherTypeView(Convert.ToDecimal(dr[3].ToString())).VoucherTypeName
                        });
                    }
                }
            }
                
            return response;
        }

        [HttpPost]
        public List<SalesOrderConfirmationListVM> OtherLocationOrderConfirmationListing(SalesOrderListingSearchParameters searchParam)
        {
            List<SalesOrderConfirmationListVM> response = new List<SalesOrderConfirmationListVM>();
            string salesQuotationMasterListQuery = "";
            if (searchParam.condition == "")
            {
                salesQuotationMasterListQuery = string.Format("select * from tbl_salesquotationmaster where date>='{0}' and date<='{1}'", searchParam.fromDate, searchParam.toDate);
            }
            else
            {
                salesQuotationMasterListQuery = string.Format("select * from tbl_salesquotationmaster where date>='{0}' and date<='{1}' and approved='Pending' and (crossCheck is null or crossCheck='{2}') ", searchParam.fromDate, searchParam.toDate, searchParam.condition);
            }
            DataTable salesQuotationMasterList = new DBMatConnection().customSelect(salesQuotationMasterListQuery);
            foreach (DataRow dr in salesQuotationMasterList.Rows)
            {
                UserInfo salesOfficer = new UserSP().UserView(Convert.ToDecimal(dr[17].ToString()));
                UserInfo gm = new UserSP().UserView(searchParam.gmUserId);//check if items in the order are placed for location of this gm

                string itemLocationQuery = string.Format("select extra1 from tbl_salesquotationdetails where quotationmasterid={0}", dr[0].ToString());
                string itemLocation = new DBMatConnection().getSingleValue(itemLocationQuery);

                if (gm.StoreId == salesOfficer.StoreId)    //check if this gm is in the same warehouse with the salesofficer
                {
                    if (itemLocation != gm.StoreId)
                    {
                        UserInfo user = new UserSP().UserView(Convert.ToDecimal(dr[17].ToString()));
                        response.Add(new SalesOrderConfirmationListVM
                        {
                            approved = dr[9].ToString(),
                            date = Convert.ToDateTime(dr[5].ToString()).ToString("dd-MMM-yyyy"),
                            invoiceNo = dr[1].ToString(),
                            ledgerId = dr[7].ToString(),
                            //ledgerName="",
                            ledgerName = new AccountLedgerSP().AccountLedgerView(Convert.ToDecimal(dr[7].ToString())).LedgerName,
                            narration = dr[11].ToString(),
                            QuotationMasterId = dr[0].ToString(),
                            totalAmount = dr[10].ToString(),
                            userName = user.FirstName + " " + user.LastName,
                            crossChecked = dr[19].ToString(),
                            voucherTypeName =new VoucherTypeSP().VoucherTypeView(Convert.ToDecimal(dr[3].ToString())).VoucherTypeName
                        });
                    }
                }

                //logic of condition below: if the order was not raised by someone in the GM's location and the item are to be picked from the GM's location
                //and the crosscheck status is pending(i.e it has been approved by the first gm which is the gm of the originating warehouse) or crosscheck status
                //is approved which means it has been approved by the gm where items will be picked from, then it should show on the list
                //NB: other locations tab only contains orders that has been approved by the logged in GM and the ones that have first approval of the
                //GM where the order was raised from
                if (gm.StoreId != salesOfficer.StoreId && itemLocation == gm.StoreId && (dr[19].ToString() == "Pending" || dr[19].ToString() == "Approved") )
                {
                    UserInfo user = new UserSP().UserView(Convert.ToDecimal(dr[17].ToString()));
                    response.Add(new SalesOrderConfirmationListVM
                    {
                        approved = dr[9].ToString(),
                        date = dr[5].ToString(),
                        invoiceNo = dr[1].ToString(),
                        ledgerId = dr[7].ToString(),
                        ledgerName = new AccountLedgerSP().AccountLedgerView(Convert.ToDecimal(dr[7].ToString())).LedgerName,
                        narration = dr[11].ToString(),
                        QuotationMasterId = dr[0].ToString(),
                        totalAmount = dr[10].ToString(),
                        userName = user.FirstName + " " + user.LastName,
                        crossChecked = dr[19].ToString(),
                        voucherTypeName = new VoucherTypeSP().VoucherTypeView(Convert.ToDecimal(dr[3].ToString())).VoucherTypeName
                    });
                }
            }

            return response;
        }

        [HttpPost]
        public DataTable SalesQuotation(SalesQuotationSearchParameters searchParam)
        {
            try
            {
                string strCondition = string.Empty;
                string strInvoiceNo = string.Empty;
                decimal decLedgerId = searchParam.ledgerId;

                DataTable dtblSalesQuotationMasterRegister = new DataTable();
                SalesQuotationMasterSP SpSalesQuotationMaster = new SalesQuotationMasterSP();

                if (searchParam.ledgerId == 0)
                {
                    decLedgerId = -1;
                }

                string invoiceNo = "";
                invoiceNo = searchParam.qoutationNo;

                dtblSalesQuotationMasterRegister = SpSalesQuotationMaster.SalesQuotationRegisterSearch(invoiceNo, decLedgerId, searchParam.fromDate, searchParam.toDate, searchParam.condition);

                string salesQuotationMasterListQuery = "";
                if (searchParam.condition == "")
                {
                    salesQuotationMasterListQuery = string.Format("select * from tbl_salesquotationmaster where date>='{0}' and date<='{1}'", searchParam.fromDate, searchParam.toDate);
                }
                else
                {
                    salesQuotationMasterListQuery = string.Format("select * from tbl_salesquotationmaster where date>='{0}' and date<='{1}' and approved='{4}'", searchParam.fromDate, searchParam.toDate, searchParam.condition);
                }
                DataTable salesQuotationMasterList = new DBMatConnection().customSelect(salesQuotationMasterListQuery);
                return salesQuotationMasterList;
                //foreach (DataRow dr in dtblSalesQuotationMasterRegister.Rows)
                //    UserInfo user = new UserSP().UserView(quot.userId);
                //UserInfo gm = new UserSP().UserView(searchParam.gmUserId);

                //List<SalesOrderConfirmationListVM> respnse = new List<SalesOrderConfirmationListVM>();
                //foreach (DataRow dr in dtblSalesQuotationMasterRegister.Rows)
                //{
                //    SalesQuotationMasterInfo quot = new SalesQuotationMasterSP().SalesQuotationMasterView(Convert.ToDecimal(dr[7].ToString()));
                //    UserInfo user = new UserSP().UserView(quot.userId);
                //    UserInfo gm = new UserSP().UserView(searchParam.gmUserId);

                //    if (gm.StoreId == user.StoreId)
                //    {
                //        //string chkQuery = "select extra1 from tbl_salesquotationdetails where quotationmasterid={0}";
                //        //string storeToCheck = new DBMatConnection().getSingleValue(chkQuery);
                //        respnse.Add(new SalesOrderConfirmationListVM
                //        {
                //            approved = dr[9].ToString(),
                //            date = dr[4].ToString(),
                //            invoiceNo = dr[1].ToString(),
                //            ledgerId = dr[2].ToString(),
                //            ledgerName = dr[5].ToString(),
                //            narration = dr[8].ToString(),
                //            QuotationMasterId = dr[7].ToString(),
                //            totalAmount = dr[6].ToString(),
                //            userName = dr[10].ToString(),
                //            voucherTypeName = dr[3].ToString()
                //        });
                //    }

                //}
                ////respnse.MyLocationOrders = "";
                ////respnse.OtherLocationsOrders = "";
                //return respnse;
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        [HttpGet]
        public HttpResponseMessage LookUps()
        {
            try
            {
                dynamic resp = new ExpandoObject();
                resp.cust = new AccountLedgerSP().AccountLedgerViewCustomerOnly();
                resp.users = new UserSP().UserViewAll();

                return Request.CreateResponse(HttpStatusCode.OK, (object)resp);

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpPost]
        public DataTable salesQuotationRegister(SalesQuotationSearchParameters searchParam)
        {
            string strCondition = string.Empty;
            string strInvoiceNo = string.Empty;
            decimal decLedgerId = searchParam.ledgerId;

            DataTable dtblSalesQuotationMasterRegister = new DataTable();
            SalesQuotationMasterSP SpSalesQuotationMaster = new SalesQuotationMasterSP();

            if (searchParam.ledgerId == 0)
            {
                decLedgerId = -1;
            }

            string invoiceNo = "";
            invoiceNo = searchParam.qoutationNo;

            dtblSalesQuotationMasterRegister = SpSalesQuotationMaster.SalesQuotationRegisterSearch(invoiceNo, decLedgerId, searchParam.fromDate, searchParam.toDate, searchParam.condition);

            return dtblSalesQuotationMasterRegister;
        }
        [HttpPost]
        public List<SalesOrderAuthorizationListVM> SalesOrder(SalesOrderSearchParameters searchParam)
        {
            try
            {
                string strCondition = string.Empty;
                string strInvoiceNo = string.Empty;
                decimal decLedgerId = searchParam.ledgerId;

                DataTable dtblSalesOrderRegister = new DataTable();
                SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
                string invoiceNo = "";

                if (searchParam.salesOrderNo == string.Empty)
                {
                    searchParam.salesOrderNo = "All";
                }
                if (searchParam.ledgerId == 0)
                {
                    decLedgerId = -1;
                }

                //DateTime fromDate = Convert.ToDateTime(searchParam.fromDate);
                //DateTime toDate = Convert.ToDateTime(searchParam.toDate);

                invoiceNo = searchParam.salesOrderNo;

                dtblSalesOrderRegister = spSalesOrderMaster.SalesOrderRegisterSearch(invoiceNo, decLedgerId, searchParam.fromDate, searchParam.toDate, searchParam.condition);
                List<SalesOrderAuthorizationListVM> respnse = new List<SalesOrderAuthorizationListVM>();
                foreach (DataRow dr in dtblSalesOrderRegister.Rows)
                {
                    SalesOrderMasterInfo quot = new SalesOrderMasterSP().SalesOrderMasterView(Convert.ToDecimal(dr[1].ToString()));
                    string query = string.Format("select userId from tbl_User where userName='{0}'", dr[11].ToString());
                    decimal userIdFromUsername = Convert.ToDecimal(new DBMatConnection().getSingleValue(query));
                    string query2 = string.Format("select top 1 extra1 from tbl_salesorderdetails where salesordermasterid={0}", quot.SalesOrderMasterId);
                    decimal itemStoreId = Convert.ToDecimal(new DBMatConnection().getSingleValue(query2));
                    UserInfo user = new UserSP().UserView(userIdFromUsername);
                    UserInfo gm = new UserSP().UserView(searchParam.gmUserId);

                    if (gm.StoreId == itemStoreId.ToString())
                    {
                        respnse.Add(new SalesOrderAuthorizationListVM
                        {
                            AuthorizationStatus = dr[13].ToString(),
                            date = dr[5].ToString(),
                            invoiceNo = dr[2].ToString(),
                            dueDate = dr[7].ToString(),
                            ledgerName = dr[6].ToString(),
                            QuotationNo = dr[2].ToString(),
                            salesOrderMasterId = dr[1].ToString(),
                            totalAmount = dr[9].ToString(),
                            userName = dr[11].ToString(),
                            voucherTypeName = dr[3].ToString()
                        });
                    }

                }
                return respnse;
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        [HttpPost]
        public DataTable SalesOrderRegister(SalesOrderSearchParameters searchParam)
        {
            string strCondition = string.Empty;
            string strInvoiceNo = string.Empty;
            decimal decLedgerId = searchParam.ledgerId;

            DataTable dtblSalesOrderRegister = new DataTable();
            SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
            string invoiceNo = "";

            if (searchParam.salesOrderNo == string.Empty)
            {
                searchParam.salesOrderNo = "All";
            }
            if (searchParam.ledgerId == 0)
            {
                decLedgerId = -1;
            }

            //DateTime fromDate = Convert.ToDateTime(searchParam.fromDate);
            //DateTime toDate = Convert.ToDateTime(searchParam.toDate);

            invoiceNo = searchParam.salesOrderNo;

            dtblSalesOrderRegister = spSalesOrderMaster.SalesOrderRegisterSearch(invoiceNo, decLedgerId, searchParam.fromDate, searchParam.toDate, searchParam.condition);

            return dtblSalesOrderRegister;
        }
        //[HttpPost]
        //public DataTable SalesOrderRegister(SalesOrderSearchParameters searchParam)
        //{
        //    string strCondition = string.Empty;
        //    string strInvoiceNo = string.Empty;
        //    decimal decLedgerId = searchParam.ledgerId;

        //    DataTable dtblSalesOrderRegister = new DataTable();
        //    SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
        //    string invoiceNo = "";

        //    if (searchParam.salesOrderNo == string.Empty)
        //    {
        //        searchParam.salesOrderNo = "All";
        //    }
        //    if (searchParam.ledgerId == 0)
        //    {
        //        decLedgerId = -1;
        //    }

        //    //DateTime fromDate = Convert.ToDateTime(searchParam.fromDate);
        //    //DateTime toDate = Convert.ToDateTime(searchParam.toDate);

        //    invoiceNo = searchParam.salesOrderNo;

        //    dtblSalesOrderRegister = spSalesOrderMaster.SalesOrderRegisterSearch(invoiceNo, decLedgerId, searchParam.fromDate, searchParam.toDate, searchParam.condition);

        //    List<SalesOrderAuthorizationListVM> respnse = new List<SalesOrderAuthorizationListVM>();
        //    foreach (DataRow dr in dtblSalesOrderRegister.Rows)
        //    {
        //        SalesQuotationMasterInfo quot = new SalesQuotationMasterSP().SalesQuotationMasterView(Convert.ToDecimal(dr[7].ToString()));
        //        UserInfo user = new UserSP().UserView(quot.userId);
        //        UserInfo gm = new UserSP().UserView(searchParam.gmUserId);

        //        if (gm.StoreId == user.StoreId)
        //        {
        //            respnse.Add(new SalesOrderConfirmationListVM
        //            {
        //                AuthorizationStatus = dr[14].ToString(),
        //                date = dr[5].ToString(),
        //                invoiceNo = dr[2].ToString(),
        //                dueDate = dr[7].ToString(),
        //                ledgerName = dr[6].ToString(),
        //                QuotationNo = dr[2].ToString(),
        //                salesOrderMasterId = dr[1].ToString(),
        //                totalAmount = dr[9].ToString(),
        //                userName = dr[11].ToString(),
        //                voucherTypeName = dr[3].ToString()
        //                approved = dr[9].ToString(),
        //                date = dr[4].ToString(),
        //                invoiceNo = dr[1].ToString(),
        //                ledgerId = dr[2].ToString(),
        //                ledgerName = dr[5].ToString(),
        //                narration = dr[8].ToString(),
        //                QuotationMasterId = dr[7].ToString(),
        //                totalAmount = dr[6].ToString(),
        //                userName = dr[10].ToString(),
        //                voucherTypeName = dr[3].ToString()
        //            });
        //        }

        //    }
        //    return respnse;
        //}

        [HttpPost]
        public List<WaybillResonse> WaybillListing(WaybillParam searchParam)
        {
            List<WaybillResonse> response = new List<WaybillResonse>();
            DBMatConnection con = new DBMatConnection();
            string query = "";
            if (searchParam.condition == "All")
            {
                query = string.Format("select * from tbl_salesordermaster where extra2>='{0}' and extra2<='{1}' and AuthorizationStatus in ('Authorized','Processed') and vouchertypeid=10030 order by extradate desc",
                searchParam.fromDate, searchParam.toDate, searchParam.condition);
            }
            else
            {
                query = string.Format("select * from tbl_salesordermaster where extra2>='{0}' and extra2<='{1}' and AuthorizationStatus='{2}' and vouchertypeid=10030 order by extradate desc",
                searchParam.fromDate, searchParam.toDate, searchParam.condition);
            }

            DataTable result = con.customSelect(query);
            foreach (DataRow row in result.Rows)
            {
                UserInfo user = new UserSP().UserView(Convert.ToDecimal(row[13].ToString()));
                UserInfo gm = new UserSP().UserView(searchParam.UserId);
                string query2 = string.Format("select top 1 extra1 from tbl_salesorderdetails where salesordermasterid={0}", Convert.ToDecimal(row[0].ToString()));
                decimal itemStoreId = Convert.ToDecimal(new DBMatConnection().getSingleValue(query2));
                if (itemStoreId.ToString() == gm.StoreId)
                {
                    response.Add(new WaybillResonse
                    {
                        SalesOrderMasterId = Convert.ToDecimal(row[0].ToString()),
                        AgentId = Convert.ToDecimal(row[8].ToString()),
                        AgentName = new AccountLedgerSP().AccountLedgerView(Convert.ToDecimal(row[8].ToString())).LedgerName,
                        AuthorizationDate = Convert.ToDateTime(row[19].ToString()).ToShortDateString(),
                        OrderDate = Convert.ToDateTime(row[5].ToString()).ToShortDateString(),
                        Status = row[21].ToString(),
                        VoucherNo = row[1].ToString()
                    });
                }

            }
            return response;
        }

        [HttpGet]
        public decimal OrderConfirmationCount()
        {
            decimal userId = MATFinancials.PublicVariables._decCurrentUserId;
            var user = new UserSP().UserView(userId);
            string storeId = user.StoreId;
            decimal count = 0;

            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("select * from tbl_SalesQuotationMaster where approved ='Pending'");
            var orders = conn.customSelect(queryStr);

            foreach (DataRow o in orders.Rows)
            {
                decimal Id = Convert.ToDecimal(o[17]);
                var nUser = new UserSP().UserView(Id);
                string sId = nUser.StoreId;
                if (sId == storeId)
                {
                    count++;
                }
            }
            return count;
        }

        [HttpGet]
        public decimal OrderAuthorisationCount()
        {
            decimal userId = MATFinancials.PublicVariables._decCurrentUserId;
            var user = new UserSP().UserView(userId);
            string storeId = user.StoreId;
            decimal count = 0;

            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("select * from tbl_SalesOrderMaster where AuthorizationStatus ='Pending'");
            var orders = conn.customSelect(queryStr);

            foreach (DataRow o in orders.Rows)
            {
                decimal Id = Convert.ToDecimal(o[13]);
                var nUser = new UserSP().UserView(Id);
                string sId = nUser.StoreId;
                if (sId == storeId)
                {
                    count++;
                }
            }
            return count;
        }

        [HttpGet]
        public decimal ReleaseFormCount()
        {
            decimal userId = MATFinancials.PublicVariables._decCurrentUserId;
            var user = new UserSP().UserView(userId);
            string storeId = user.StoreId;
            decimal count = 0;

            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("select * from tbl_salesordermaster where AuthorizationStatus = 'Authorized' and vouchertypeid=10030");
            var orders = conn.customSelect(queryStr);

            foreach (DataRow o in orders.Rows)
            {
                decimal Id = Convert.ToDecimal(o[13]);
                var nUser = new UserSP().UserView(Id);
                string sId = nUser.StoreId;
                if (sId == storeId)
                {
                    count++;
                }
            }
            return count;
        }

        [HttpPost]
        public DataTable DeliveryNote(DeliveryNoteSearchParameters searchParam)
        {
            DataTable resp = new DataTable();
            DataTable dtblDeliveryNote = new DataTable();
            resp.Columns.Add("slNo", typeof(string));
            resp.Columns.Add("deliveryNoteMasterId", typeof(string));
            resp.Columns.Add("invoiceNo", typeof(string));
            resp.Columns.Add("voucherTypeName", typeof(string));
            resp.Columns.Add("Date", typeof(string));
            resp.Columns.Add("CashOrParty", typeof(string));
            resp.Columns.Add("Amount", typeof(string));
            resp.Columns.Add("narration", typeof(string));
            resp.Columns.Add("currencyName", typeof(string));
            resp.Columns.Add("userName", typeof(string));
            //resp.Columns.Add("authorizedBy", typeof(string));
            resp.Columns.Add("OrderNoOrQuotationNo", typeof(string));
            resp.Columns.Add("status", typeof(string));

            try
            {
                string strCondition = "";
                string strInvoiceNo = "";

                DeliveryNoteMasterSP spDeliveryNoteMaster = new DeliveryNoteMasterSP();

                if (searchParam.deliveryNoteNo == "")
                {
                    strInvoiceNo = "";
                }
                else
                {
                    strInvoiceNo = searchParam.deliveryNoteNo;
                }
                dtblDeliveryNote = spDeliveryNoteMaster.DeliveryNoteRegisterGridFillCorrespondingToInvoiceNoAndLedger(strInvoiceNo,
                    searchParam.ledgerId, searchParam.fromDate, searchParam.toDate, MATFinancials.PublicVariables._inNoOfDecimalPlaces);

                foreach (DataRow dt in dtblDeliveryNote.Rows)
                {
                    var currentUser = new UserSP().UserView(searchParam.userId);
                    decimal itemsLocation = getDeliveryNoteItemStore(Convert.ToDecimal(dt.ItemArray[1]));

                    string query = "SELECT RoleId FROM tbl_CycleActionPriviledge WHERE CycleAction='Audit Control'";
                    var roleId = new DBMatConnection().getSingleValue(query);

                    //var orderUserId = new SalesOrderMasterSP().SalesOrderMasterView(Convert.ToDecimal(dt.ItemArray[10])).UserId;
                    //var confirmedBy = new UserSP().UserView(orderUserId);
                    var deliveryNoteUserId = new DeliveryNoteMasterSP().DeliveryNoteMasterView(Convert.ToDecimal(dt.ItemArray[1])).UserId;
                    resp.Rows.Add(new Object[] {
                                dt.ItemArray[0],
                                dt.ItemArray[1],
                                dt.ItemArray[2],
                                dt.ItemArray[3],
                                dt.ItemArray[4],
                                dt.ItemArray[5],
                                dt.ItemArray[6],
                                dt.ItemArray[7],
                                dt.ItemArray[8],
                                new UserSP().UserView(deliveryNoteUserId).FirstName +" "+new UserSP().UserView(deliveryNoteUserId).LastName,
                                //confirmedBy.FirstName+" "+confirmedBy.LastName,
                                dt.ItemArray[10],
                                dt.ItemArray[11]
                            });

                    //if (roleId != null)
                    //{
                    //    if (roleId == "1")
                    //    {
                    //        if (dt.ItemArray[11].ToString() == "Processed")
                    //        {
                    //            resp.Rows.Add(new Object[] {
                    //                dt.ItemArray[0],
                    //                dt.ItemArray[1],
                    //                dt.ItemArray[2],
                    //                dt.ItemArray[3],
                    //                dt.ItemArray[4],
                    //                dt.ItemArray[5],
                    //                dt.ItemArray[6],
                    //                dt.ItemArray[7],
                    //                dt.ItemArray[8],
                    //                new UserSP().UserView(deliveryNoteUserId).FirstName +" "+new UserSP().UserView(deliveryNoteUserId).LastName,
                    //                //confirmedBy.FirstName+" "+confirmedBy.LastName,
                    //                dt.ItemArray[10],
                    //                dt.ItemArray[11]
                    //            });
                    //        }
                    //    }
                    //    else
                    //    {
                    //        if (currentUser.StoreId == itemsLocation.ToString() && currentUser.RoleId.ToString() == roleId)
                    //        {
                    //            if (dt.ItemArray[11].ToString() == "Processed")
                    //            {
                    //                resp.Rows.Add(new Object[] {
                    //                dt.ItemArray[0],
                    //                dt.ItemArray[1],
                    //                dt.ItemArray[2],
                    //                dt.ItemArray[3],
                    //                dt.ItemArray[4],
                    //                dt.ItemArray[5],
                    //                dt.ItemArray[6],
                    //                dt.ItemArray[7],
                    //                dt.ItemArray[8],
                    //                new UserSP().UserView(deliveryNoteUserId).FirstName +" "+new UserSP().UserView(deliveryNoteUserId).LastName,
                    //                //confirmedBy.FirstName+" "+confirmedBy.LastName,
                    //                dt.ItemArray[10],
                    //                dt.ItemArray[11]
                    //            });
                    //            }
                    //        }
                    //    }

                    //}                    

                }

                //return dtblDeliveryNote;
            }
            catch (Exception ex)
            {
            }
            return resp;
        }
        
        [HttpPost]
        public HttpResponseMessage getDeliveryNoteMaster(DeliveryNoteSearchParameters searchParam)
        {
            string strCondition = "";
            string strInvoiceNo = "";

            if (searchParam.deliveryNoteNo == "")
            {
                strInvoiceNo = "";
            }
            else
            {
                strInvoiceNo = searchParam.deliveryNoteNo;
            }
            var master = new DeliveryNoteMasterSP().DeliveryNoteRegisterGridFillCorrespondingToInvoiceNoAndLedger(strInvoiceNo,
                    searchParam.ledgerId, searchParam.fromDate, searchParam.toDate, MATFinancials.PublicVariables._inNoOfDecimalPlaces);
            return Request.CreateResponse(HttpStatusCode.OK, (object)master);
        }

        [HttpGet]
        public HttpResponseMessage getDeliveryNoteDetails(int deliveryNoteMasterId)
        {
            var details = new DeliveryNoteDetailsSP().DeliveryNoteDetailsViewByDeliveryNoteMasterId(deliveryNoteMasterId);
            return Request.CreateResponse(HttpStatusCode.OK, (object)details);
        }

        [HttpPost]
        public DataTable DeliveryNoteForProcessing(DeliveryNoteSearchParameters searchParam)
        {
            DataTable resp = new DataTable();
            DataTable dtblDeliveryNote = new DataTable();
            resp.Columns.Add("slNo", typeof(string));
            resp.Columns.Add("deliveryNoteMasterId", typeof(string));
            resp.Columns.Add("invoiceNo", typeof(string));
            resp.Columns.Add("voucherTypeName", typeof(string));
            resp.Columns.Add("status", typeof(string));
            resp.Columns.Add("Date", typeof(string));
            resp.Columns.Add("CashOrParty", typeof(string));
            resp.Columns.Add("Amount", typeof(string));
            resp.Columns.Add("narration", typeof(string));
            resp.Columns.Add("currencyName", typeof(string));
            resp.Columns.Add("userName", typeof(string));
            resp.Columns.Add("OrderNoOrQuotationNo", typeof(string));

            try
            {
                string strCondition = "";
                string strInvoiceNo = "";

                DeliveryNoteMasterSP spDeliveryNoteMaster = new DeliveryNoteMasterSP();

                if (searchParam.deliveryNoteNo == "")
                {
                    strInvoiceNo = "";
                }
                else
                {
                    strInvoiceNo = searchParam.deliveryNoteNo;
                }
                dtblDeliveryNote = spDeliveryNoteMaster.DeliveryNoteRegisterGridFillCorrespondingToInvoiceNoAndLedger(strInvoiceNo,
                    searchParam.ledgerId, searchParam.fromDate, searchParam.toDate, MATFinancials.PublicVariables._inNoOfDecimalPlaces);

                foreach (DataRow dt in dtblDeliveryNote.Rows)
                {
                    var currentUser = new UserSP().UserView(searchParam.userId);
                    decimal itemsLocation = getDeliveryNoteItemStore(Convert.ToDecimal(dt.ItemArray[1]));

                    string query = "SELECT RoleId FROM tbl_CycleActionPriviledge WHERE CycleAction='Warehouse Manager'";
                    var roleId = new DBMatConnection().getSingleValue(query);
                    if (roleId != null)
                    {
                        if (roleId == "1")
                        {
                            resp.Rows.Add(new Object[] {
                                        dt.ItemArray[0],
                                        dt.ItemArray[1],
                                        dt.ItemArray[2],
                                        dt.ItemArray[3],
                                        dt.ItemArray[4],
                                        dt.ItemArray[5],
                                        dt.ItemArray[6],
                                        dt.ItemArray[7],
                                        dt.ItemArray[8],
                                        dt.ItemArray[9],
                                        dt.ItemArray[10],
                                        dt.ItemArray[11]
                                    });
                        }
                        else
                        {
                            if (currentUser.StoreId == itemsLocation.ToString() && currentUser.RoleId.ToString() == roleId)
                            {
                                resp.Rows.Add(new Object[] {
                                dt.ItemArray[0],
                                dt.ItemArray[1],
                                dt.ItemArray[2],
                                dt.ItemArray[3],
                                dt.ItemArray[4],
                                dt.ItemArray[5],
                                dt.ItemArray[6],
                                dt.ItemArray[7],
                                dt.ItemArray[8],
                                dt.ItemArray[9],
                                dt.ItemArray[10],
                                dt.ItemArray[11]
                            });
                            }
                        }
                    }

                }

                //return dtblDeliveryNote;
            }
            catch (Exception ex)
            {
            }
            return resp;
        }

        private decimal getDeliveryNoteItemStore(decimal deliveryNoteMasterId)
        {
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("SELECT DISTINCT godownId FROM tbl_deliverynotedetails WHERE deliveryNoteMasterId={0}", deliveryNoteMasterId);
            return Convert.ToDecimal(con.getSingleValue(query));
        }

        private string getDeliveryNoteStatus(decimal deliveryNoteMasterId)
        {
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("SELECT status FROM tbl_deliverynotemaster WHERE deliveryNoteMasterId={0}", deliveryNoteMasterId);
            return con.getSingleValue(query);
        }

        [HttpPost]
        public DataTable RejectionIn(RejectionInSearchParameters searchParam)
        {
            RejectionInMasterSP SpRejectionInMaster = new RejectionInMasterSP();
            DataTable dtbl = new DataTable();
            try
            {
                decimal decLedgerId = searchParam.ledgerId;
                string strInvoiceNo = "";

                if (searchParam.rejectionInNo == "")
                {
                    strInvoiceNo = "";
                }
                else
                {
                    strInvoiceNo = searchParam.rejectionInNo;
                }

                if (searchParam.ledgerId == 0 || searchParam.ledgerId == -1)
                {
                    decLedgerId = -1;
                }

                return dtbl = SpRejectionInMaster.RejectionInRegisterFill(searchParam.fromDate, searchParam.toDate, decLedgerId,
                    strInvoiceNo, searchParam.voucherTypeId);
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        [HttpPost]
        public DataTable SalesInvoice(SalesInvoiceSearchParameters searchParam)
        {
            string strVoucherNo = "";
            SalesMasterSP spSalesmaster = new SalesMasterSP();
            DataTable dtblSalesInvoice = new DataTable();
            try
            {
                if (searchParam.voucherNo == "")
                {
                    strVoucherNo = "";
                }
                else
                {
                    strVoucherNo = searchParam.voucherNo;
                }
                dtblSalesInvoice = spSalesmaster.SalesInvoiceRegisterGridfill(searchParam.fromDate, searchParam.toDate,
                    searchParam.voucherTypeId, searchParam.ledgerId, strVoucherNo, searchParam.salesMode);

                return dtblSalesInvoice;
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        [HttpPost]
        public DataTable PDCReceivable(PDCReceivableSearchParameters searchParam)
        {
            try
            {
                if (searchParam.ledgerName == "")
                {
                    searchParam.ledgerName = "ALL";
                }
                DataTable dtbl = new DataTable();
                PDCReceivableMasterSP spPdcreceivable = new PDCReceivableMasterSP();
                dtbl = spPdcreceivable.PDCReceivableRegisterSearch(searchParam.fromDate, searchParam.toDate,
                    searchParam.formNo, searchParam.ledgerName);
                return dtbl;
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        [HttpPost]
        public DataTable Receipt(ReceiptSearchParameters searchParam)
        {
            try
            {
                ReceiptMasterSP SpReceiptMaster = new ReceiptMasterSP();
                ReceiptMasterInfo InfoReceiptMaster = new ReceiptMasterInfo();
                DataTable dtblReceipt = new DataTable();

                dtblReceipt = SpReceiptMaster.ReceiptMasterSearch(searchParam.fromDate, searchParam.toDate,
                    searchParam.ledgerId, searchParam.formNo);

                return dtblReceipt;
            }
            catch (Exception ex)
            {
            }

            return null;
        }

        [HttpPost]
        public DataTable CreditNote(CreditNoteSearchParameters searchParam)
        {
            string strVoucherNo = "";
            CreditNoteMasterSP spCreditNoteMaster = new CreditNoteMasterSP();
            DataTable dtblCreditNote = new DataTable();
            try
            {
                if (searchParam.formNo == "")
                {
                    strVoucherNo = "";
                }
                else
                {
                    strVoucherNo = searchParam.formNo;
                }
                dtblCreditNote = spCreditNoteMaster.CreditNoteRegisterSearch(strVoucherNo, searchParam.fromDate.ToString(), searchParam.toDate.ToString());

                return dtblCreditNote;
            }
            catch (Exception ex)
            {

            }

            return null;
        }

        [HttpGet]
        public DataTable ReceiptDetails(decimal receiptMasterId)
        {
            ReceiptDetailsSP SpReceiptDetails = new ReceiptDetailsSP();
            DataTable dtblRceiptDetails = new DataTable();

            dtblRceiptDetails = SpReceiptDetails.ReceiptDetailsViewByMasterId(receiptMasterId);

            return dtblRceiptDetails;
        }

    }

    public class WaybillParam
    {
        public string fromDate { get; set; }
        public string toDate { get; set; }
        public string condition { get; set; }
        public decimal UserId { get; set; }
    }

    public class WaybillResonse
    {
        public decimal SalesOrderMasterId { get; set; }
        public string VoucherNo { get; set; }
        public string OrderDate { get; set; }
        public string AuthorizationDate { get; set; }
        public decimal AgentId { get; set; }
        public string AgentName { get; set; }
        public string Status { get; set; }

    }
}