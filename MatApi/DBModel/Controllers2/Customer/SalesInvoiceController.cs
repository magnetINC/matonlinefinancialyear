﻿using MatApi.Models.Customer;
using MATFinancials;
using MATFinancials.Other;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SalesInvoiceController : ApiController
    {
        decimal DecSalesInvoiceVoucherTypeId = 28;
        decimal decSalseInvoiceSuffixPrefixId = 0;
        string strVoucherNo = string.Empty;
        string strInvoiceNo = string.Empty;
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        decimal decSalesDetailsId = 0;
        List<decimal> listofDetailsId = new List<decimal>();

        [HttpGet]
        public HttpResponseMessage InvoiceLookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var customers = transactionGeneralFillObj.CashOrPartyUnderSundryDrComboFill();
            var salesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();
            var pricingLevels = transactionGeneralFillObj.PricingLevelViewAll();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            var units = new UnitSP().UnitViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks = new RackSP().RackViewAll();
            var batches = new BatchSP().BatchViewAll();
            var tax = new TaxSP().TaxViewAll();


            dynamic response = new ExpandoObject();
            response.Customers = customers;
            response.SalesMen = salesMen;
            response.PricingLevels = pricingLevels;
            response.Currencies = currencies;
            response.Units = units;
            response.Stores = stores;
            response.Racks = racks;
            response.Batches = batches;
            response.Taxes = tax;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetSalesModeOrderNo(decimal ledgerId, decimal voucherTypeId, decimal salesInvoiceToEdit = 0)
        {
            //salesquota = 15
            //delivery 17
            //salesorder 16
            SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
            DeliveryNoteMasterSP spDeliveryNoteMasterSp = new DeliveryNoteMasterSP();
            SalesQuotationMasterSP spSalesQuotationMasterSp = new SalesQuotationMasterSP();

            if (voucherTypeId == 10030)
            {
                var salesOrders = spSalesOrderMaster.GetSalesOrderNoIncludePendingCorrespondingtoLedgerforSI(ledgerId, salesInvoiceToEdit, voucherTypeId);
                return Request.CreateResponse(HttpStatusCode.OK, salesOrders);
            }
            if (voucherTypeId == 17)
            {
                var deliveryNotes = spDeliveryNoteMasterSp.GetDeleveryNoteNoIncludePendingCorrespondingtoLedgerForSI(ledgerId, salesInvoiceToEdit, voucherTypeId);
                return Request.CreateResponse(HttpStatusCode.OK, deliveryNotes);
            }
            if (voucherTypeId == 10031)
            {
                var salesQuotations = spSalesQuotationMasterSp.GetSalesQuotationIncludePendingCorrespondingtoLedgerForSI(ledgerId, salesInvoiceToEdit, voucherTypeId);
                return Request.CreateResponse(HttpStatusCode.OK, salesQuotations);
            }

            return Request.CreateResponse(HttpStatusCode.OK, "SALES_MODE_FAILURE");
        }

        [HttpGet]
        public HttpResponseMessage GetSalesModeItems(decimal orderNo)
        {
            DeliveryNoteMasterSP SPDeliveryNoteMaster = new DeliveryNoteMasterSP();
            DeliveryNoteDetailsSP SPDeliveryNoteDetails = new DeliveryNoteDetailsSP();

            DataTable dtblMaster = SPDeliveryNoteMaster.SalesInvoiceGridfillAgainestDeliveryNote(orderNo);
            DataTable dtblDetails = SPDeliveryNoteDetails.SalesInvoiceGridfillAgainestDeliveryNoteUsingDeliveryNoteDetails(orderNo, 0, DecSalesInvoiceVoucherTypeId);

            dynamic response = new ExpandoObject();
            response.Master = dtblMaster;
            response.Details = dtblDetails;
            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }

        [HttpPost]
        public bool SaveSalesInvoice(SalesInvoiceVM input)
        {
            bool isSavedSuccessfully = false;

            SalesMasterSP spSalesMaster = new SalesMasterSP();
            SalesDetailsSP spSalesDetails = new SalesDetailsSP();
            //StockPostingInfo infoStockPosting = new StockPostingInfo();
           // SalesMasterInfo InfoSalesMaster = new SalesMasterInfo();
            //SalesDetailsInfo InfoSalesDetails = new SalesDetailsInfo();
            StockPostingSP spStockPosting = new StockPostingSP();
            //AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            SalesBillTaxInfo infoSalesBillTax = new SalesBillTaxInfo();
            SalesBillTaxSP spSalesBillTax = new SalesBillTaxSP();
            UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
            HelperClasses helperClasses = new HelperClasses();
            try
            {
                input.SalesMasterInfo.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                input.SalesMasterInfo.SuffixPrefixId = 0;
                
                input.SalesMasterInfo.SalesAccount = 10;  //Convert.ToDecimal(cmbSalesAccount.SelectedValue.ToString());

                input.SalesMasterInfo.UserId = PublicVariables._decCurrentUserId;
                input.SalesMasterInfo.LrNo = "NIL";
                input.SalesMasterInfo.TransportationCompany = "NIL";
                input.SalesMasterInfo.POS = false;
                input.SalesMasterInfo.CounterId = 0;
                input.SalesMasterInfo.Narration = "";
                input.SalesMasterInfo.CustomerName = new AccountLedgerSP().AccountLedgerView(input.SalesMasterInfo.LedgerId).LedgerName;
                input.SalesMasterInfo.ExtraDate = DateTime.Now;
                input.SalesMasterInfo.Extra1 = string.Empty;
                input.SalesMasterInfo.Extra2 = string.Empty;
                input.SalesMasterInfo.POSTellerNo = string.Empty;
                input.SalesMasterInfo.transactionDay = helperClasses.GetCurrentDay(PublicVariables._decCurrentUserId);
                decimal decSalesMasterId = spSalesMaster.SalesMasterAdd(input.SalesMasterInfo);           // updates sales master table
                if(decSalesMasterId>0)
                {
                    isSavedSuccessfully = true;
                }
                string strAgainstInvoiceN0 = input.SalesMasterInfo.InvoiceNo;
                for (int i=0;i<input.SalesDetailsInfo.Count;i++)
                {
                    var unc = new UnitConvertionSP().UnitconversionIdViewByUnitIdAndProductId(input.SalesDetailsInfo[i].UnitId, input.SalesDetailsInfo[i].ProductId);
                    input.SalesDetailsInfo[i].UnitConversionId = unc;
                    input.SalesDetailsInfo[i].SalesMasterId = decSalesMasterId;      // assigns SalesMasterDetailsID
                    input.SalesDetailsInfo[i].ExtraDate = DateTime.Now;
                    input.SalesDetailsInfo[i].Extra1 = string.Empty;
                    input.SalesDetailsInfo[i].Extra2 = string.Empty;
                    decSalesDetailsId = spSalesDetails.SalesDetailsAdd(input.SalesDetailsInfo[i]);       // updates sales details table
                    if(decSalesDetailsId>0)
                    {
                        isSavedSuccessfully = true;
                    }
                    else
                    {
                        isSavedSuccessfully = false;
                    }
                    listofDetailsId.Add(decSalesDetailsId);

                    //if (dgvSalesInvoice.Rows[inI].Cells["dgvcmbSalesInvoiceGodown"].Value != null && dgvSalesInvoice.Rows[inI].Cells["dgvcmbSalesInvoiceGodown"].Value.ToString() != string.Empty)
                    //{
                    //    infoStockPosting.GodownId = Convert.ToDecimal(dgvSalesInvoice.Rows[inI].Cells["dgvcmbSalesInvoiceGodown"].Value.ToString());
                    //}
                    //else
                    //{
                    //    infoStockPosting.GodownId = 0;
                    //}
                    //if (dgvSalesInvoice.Rows[inI].Cells["dgvcmbSalesInvoiceRack"].Value != null && dgvSalesInvoice.Rows[inI].Cells["dgvcmbSalesInvoiceRack"].Value.ToString() != string.Empty)
                    //{
                    //    infoStockPosting.RackId = Convert.ToDecimal(dgvSalesInvoice.Rows[inI].Cells["dgvcmbSalesInvoiceRack"].Value.ToString());
                    //}
                    //else
                    //{
                    //    infoStockPosting.RackId = 0;
                    //}
                    input.StockPostingInfo = new StockPostingInfo();
                    input.StockPostingInfo.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    input.StockPostingInfo.ExtraDate = DateTime.Now;
                    input.StockPostingInfo.Date = DateTime.Now;
                    input.StockPostingInfo.Extra1 = string.Empty;
                    input.StockPostingInfo.Extra2 = string.Empty;
                    input.StockPostingInfo.ProjectId = 0;
                    input.StockPostingInfo.CategoryId = 0;

                    //it will be used when populating invoice from delivery note
                    //if (dgvSalesInvoice.Rows[inI].Cells["dgvtxtSalesInvoiceDeliveryNoteDetailsId"].Value != null)
                    //{
                    //    if (Convert.ToDecimal(dgvSalesInvoice.Rows[inI].Cells["dgvtxtSalesInvoiceDeliveryNoteDetailsId"].Value.ToString()) != 0)
                    //    {
                    //        infoStockPosting.InwardQty = InfoSalesDetails.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(InfoSalesDetails.UnitConversionId);
                    //        infoStockPosting.OutwardQty = 0;
                    //        infoStockPosting.VoucherNo = strVoucherNoTostockPost;
                    //        infoStockPosting.AgainstVoucherNo = strVoucherNo;
                    //        infoStockPosting.InvoiceNo = strInvoiceNoTostockPost;
                    //        infoStockPosting.AgainstInvoiceNo = strAgainstInvoiceN0;
                    //        infoStockPosting.VoucherTypeId = decVouchertypeIdTostockPost;
                    //        infoStockPosting.AgainstVoucherTypeId = DecSalesInvoiceVoucherTypeId;
                    //        spStockPosting.StockPostingAdd(infoStockPosting);     // old implementation 20161201
                    //                                                              //// Urefe added if statement so that sales invoice against delivery not will not affect stock
                    //                                                              //if (InfoSalesMaster.DeliveryNoteMasterId == 0)
                    //                                                              //{
                    //                                                              //    spStockPosting.StockPostingAdd(infoStockPosting);
                    //                                                              //}
                    //    }
                    //}
                    input.StockPostingInfo.InwardQty = 0;
                    input.StockPostingInfo.OutwardQty = input.SalesDetailsInfo[i].Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(input.SalesDetailsInfo[i].UnitConversionId);
                    input.StockPostingInfo.VoucherNo = input.SalesMasterInfo.VoucherNo;
                    input.StockPostingInfo.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                    input.StockPostingInfo.InvoiceNo = input.SalesMasterInfo.InvoiceNo;
                    input.StockPostingInfo.AgainstInvoiceNo = "NA";
                    input.StockPostingInfo.AgainstVoucherNo = "NA";
                    input.StockPostingInfo.AgainstVoucherTypeId = 0;
                    input.StockPostingInfo.Extra1 = string.Empty;
                    input.StockPostingInfo.Extra2 = string.Empty;
                    spStockPosting.StockPostingAdd(input.StockPostingInfo);     // old implementation 20161201
                                                                          //// Urefe added if statement so that sales invoice against delivery not will not affect stock
                                                                          //if (InfoSalesMaster.DeliveryNoteMasterId == 0)
                                                                          //{
                                                                          //    spStockPosting.StockPostingAdd(infoStockPosting);
                                                                          //}
                }
                //for(int i=0;i<input.SalesBillTaxInfo.Count;i++)
                for (int i=0;i<1;i++)   //1 is used since we'll be implementing tax
                {
                    infoSalesBillTax.SalesMasterId = decSalesMasterId;
                    infoSalesBillTax.TaxAmount = input.SalesMasterInfo.TaxAmount;
                    infoSalesBillTax.TaxId = input.SalesDetailsInfo[0].TaxId;   //since same tax is maintained for all items hence pick first tax in item list
                    infoSalesBillTax.ExtraDate = DateTime.Now;
                    infoSalesBillTax.Extra1 = string.Empty;
                    infoSalesBillTax.Extra2 = string.Empty;
                    if(spSalesBillTax.SalesBillTaxAdd(infoSalesBillTax)>0)
                    {
                        isSavedSuccessfully = true;
                    }
                    else
                    {
                        isSavedSuccessfully = false;
                    }
                }
                
                //applyCreditNote();    //don't apply credit note now(recommended by Mr Alex), we'll create another window
                                        //to apply credit note

                isSavedSuccessfully= ledgerPostingAdd(decSalesMasterId, decSalesDetailsId,input.SalesMasterInfo,input.SalesDetailsInfo,input.SalesBillTaxInfo);     // calls a function to post to ledger
                if (spSalesMaster.SalesInvoiceInvoicePartyCheckEnableBillByBillOrNot(input.SalesMasterInfo.LedgerId))
                {
                    isSavedSuccessfully= partyBalanceAdd(input.SalesMasterInfo);
                }
                
            }
            catch (Exception ex)
            {
            }

            return isSavedSuccessfully;
        }

        public bool partyBalanceAdd(SalesMasterInfo input)
        {
            PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
            SalesMasterInfo InfoSalesMaster = new SalesMasterInfo();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            bool isSaved = false;

            try
            {
                infoPartyBalance.Date = input.Date;
                infoPartyBalance.LedgerId = input.LedgerId;
                infoPartyBalance.VoucherNo = strVoucherNo;
                infoPartyBalance.InvoiceNo = input.InvoiceNo;
                infoPartyBalance.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                infoPartyBalance.AgainstVoucherTypeId = 0;
                infoPartyBalance.AgainstVoucherNo = "0";
                infoPartyBalance.AgainstInvoiceNo = "0";
                infoPartyBalance.ReferenceType = "New";
                infoPartyBalance.Debit = input.GrandTotal;
                infoPartyBalance.Credit = 0;
                infoPartyBalance.CreditPeriod = input.CreditPeriod;
                infoPartyBalance.ExchangeRateId = input.ExchangeRateId;
                infoPartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoPartyBalance.ExtraDate = DateTime.Now;
                infoPartyBalance.Extra1 = string.Empty;
                infoPartyBalance.Extra2 = string.Empty;
                if(spPartyBalance.PartyBalanceAdd(infoPartyBalance)>0)
                {
                    isSaved = true;
                }
            }
            catch (Exception ex)
            {
               // formMDI.infoError.ErrorString = "SI74:" + ex.Message;
            }
            return isSaved;
        }

        public bool ledgerPostingAdd(decimal decSalesMasterId, decimal decSalesDetailsId,SalesMasterInfo salesMasterInfo,List<SalesDetailsInfo> salesDetailsInfo, List<SalesBillTaxInfo> salesTax)
        {
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            SalesMasterInfo InfoSalesMaster = new SalesMasterInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            decimal decRate = 0;
            decimal decimalGrantTotal = 0;
            decimal decTotalAmount = 0;
            int itemsinList = 0;
            bool isSaved = false;

            try
            {
                decimalGrantTotal = salesMasterInfo.GrandTotal;
                decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(salesMasterInfo.ExchangeRateId);
                decimalGrantTotal = decimalGrantTotal * decRate;
                infoLedgerPosting.Debit = decimalGrantTotal;
                infoLedgerPosting.Credit = 0;
                infoLedgerPosting.Date = salesMasterInfo.Date;
                infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                infoLedgerPosting.VoucherNo = strVoucherNo;
                infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                infoLedgerPosting.LedgerId = salesMasterInfo.LedgerId;
                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.DetailsId = decSalesMasterId;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.Date = DateTime.Now;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                if(spLedgerPosting.LedgerPostingAdd(infoLedgerPosting)>0)        // posts to debit side
                {
                    isSaved = true;
                }
                ///---------credit section
                ///
                #region new implementation
                foreach (var row in salesDetailsInfo)
                {
                    LedgerPostingSP ledgerPostingSalesAccountId = new LedgerPostingSP();
                    decTotalAmount = row.NetAmount;
                    decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(salesMasterInfo.ExchangeRateId);
                    decTotalAmount = decTotalAmount * decRate;
                    infoLedgerPosting.Debit = 0;
                    infoLedgerPosting.Credit = row.NetAmount;
                    infoLedgerPosting.Date = salesMasterInfo.Date;
                    infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                    infoLedgerPosting.VoucherNo = strVoucherNo;
                    infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                    // Old implementation changed by Urefe because of Precious' work that removed/hide sales account selection from UI
                    // infoLedgerPosting.LedgerId = Convert.ToDecimal(row.Cells["dgvtxtsalesAccount"].Value.ToString()); 
                    infoLedgerPosting.LedgerId = ledgerPostingSalesAccountId.ProductSalesAccountId(new ProductSP().ProductView(row.ProductId).ProductCode);
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    //infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.DetailsId = listofDetailsId[itemsinList]; //decSalesDetailsId;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    if(spLedgerPosting.LedgerPostingAdd(infoLedgerPosting)>0)    // post to credit side
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                    itemsinList++;
                }
                #endregion
                
                decimal decBillDis = 0;
                decBillDis = salesMasterInfo.BillDiscount;
                decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(salesMasterInfo.ExchangeRateId);
                decBillDis = decBillDis * decRate;
                if (decBillDis > 0)
                {
                    infoLedgerPosting.Debit = decBillDis;
                    infoLedgerPosting.Credit = 0;
                    infoLedgerPosting.Date = salesMasterInfo.Date;
                    infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                    infoLedgerPosting.VoucherNo = strVoucherNo;
                    infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                    infoLedgerPosting.LedgerId = 8;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    if(spLedgerPosting.LedgerPostingAdd(infoLedgerPosting)>0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                }
                foreach(var row in salesTax)
                {
                    decimal decTaxAmount = 0;
                    decTaxAmount = row.TaxAmount;
                    decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(salesMasterInfo.ExchangeRateId);
                    decTaxAmount = decTaxAmount * decRate;
                    if (decTaxAmount > 0)
                    {
                        infoLedgerPosting.Debit = 0;
                        infoLedgerPosting.Credit = decTaxAmount;
                        infoLedgerPosting.Date = salesMasterInfo.Date;
                        infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                        infoLedgerPosting.VoucherNo = strVoucherNo;
                        infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                        infoLedgerPosting.LedgerId = salesMasterInfo.LedgerId;
                        infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                        infoLedgerPosting.DetailsId = 0;
                        infoLedgerPosting.ChequeNo = string.Empty;
                        infoLedgerPosting.ChequeDate = DateTime.Now;
                        infoLedgerPosting.Extra1 = string.Empty;
                        infoLedgerPosting.Extra2 = string.Empty;
                        if(spLedgerPosting.LedgerPostingAdd(infoLedgerPosting)>0)
                        {
                            isSaved = true;
                        }
                        else
                        {
                            isSaved = false;
                        }
                    }
                }
                
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "SI73:" + ex.Message;
            }
            return isSaved;
        }

        private void suffixPrefix()
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            SalesMasterSP spSalesMaster = new SalesMasterSP();
            if (strVoucherNo == string.Empty)
            {
                strVoucherNo = "0";
            }
            strVoucherNo = obj.VoucherNumberAutomaicGeneration(DecSalesInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");
            if (Convert.ToDecimal(strVoucherNo) != (spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId)))
            {
                strVoucherNo = spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(DecSalesInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");
                if (spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId) == 0)
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(DecSalesInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");
                }
            }
            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(DecSalesInvoiceVoucherTypeId, DateTime.Now);
            strPrefix = infoSuffixPrefix.Prefix;
            strSuffix = infoSuffixPrefix.Suffix;
            strInvoiceNo = strPrefix + strVoucherNo + strSuffix;
            decSalseInvoiceSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
        }
    }
}
