﻿ using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.DBModel
{
    public class TrailBalanceModel
    {
        public AccountGroup Group { get; set; }
        public List<AccountLedger> Ledgers { get; set; }  = new List<AccountLedger>();
    }

    public class TrailBalanceHeadModel
    {
        public List<TrailBalanceModel> HeadGroup { get; set; } = new List<TrailBalanceModel>();

        public List<TrailBalanceTransModel> HeadLedgers { get; set; } = new List<TrailBalanceTransModel>();

    }

    public class TrailBalanceTransModel
    {
        public AccountGroup Account { get; set; } = new AccountGroup();
        public List<TrailBalanceTransactionModel> Ledgers { get; set; } = new List<TrailBalanceTransactionModel>();
    }

    public class TrailBalanceTransactionModel
    {
        public AccountLedger Ledger { get; set; } = new AccountLedger();
        public List<LedgerPosting> Transactions { get; set; } = new List<LedgerPosting>();
    }


    public  class LedgerPosting
    {
        public decimal ledgerPostingId { get; set; }
        public   System.DateTime   date { get; set; }
        public   decimal   voucherTypeId { get; set; }
        public string voucherNo { get; set; }
        public   decimal   ledgerId { get; set; }
        public   decimal   debit { get; set; }
        public   decimal   credit { get; set; }
        public   decimal   detailsId { get; set; }
        public   decimal   yearId { get; set; }
        public string invoiceNo { get; set; }
        public string chequeNo { get; set; }
        public   System.DateTime   chequeDate { get; set; }
        public   System.DateTime   extraDate { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }

        public AccountLedger tbl_AccountLedger { get; set; } = new AccountLedger();
    }

    public  class AccountLedger
    {
        public decimal ledgerId { get; set; }
        public   decimal   accountGroupId { get; set; }
        public string ledgerName { get; set; }
        public   decimal   openingBalance { get; set; }
        public   bool   isDefault { get; set; }
        public string crOrDr { get; set; }
        public string narration { get; set; }
        public string mailingName { get; set; }
        public string address { get; set; }
        public string phone { get; set; }
        public string mobile { get; set; }
        public string email { get; set; }
        public   int   creditPeriod { get; set; }
        public   decimal   creditLimit { get; set; }
        public   decimal   pricinglevelId { get; set; }
        public   bool   billByBill { get; set; }
        public string tin { get; set; }
        public string cst { get; set; }
        public string pan { get; set; }
        public   decimal   routeId { get; set; }
        public string bankAccountNumber { get; set; }
        public string branchName { get; set; }
        public string branchCode { get; set; }
        public   System.DateTime   extraDate { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public   decimal   areaId { get; set; }
        public   bool   isActive { get; set; }

      
    }

    public  class AccountGroup
    {
        public decimal accountGroupId { get; set; }
        public string accountGroupName { get; set; }
        public   decimal   groupUnder { get; set; }
        public string narration { get; set; }
        public   bool   isDefault { get; set; }
        public string nature { get; set; }
        public string affectGrossProfit { get; set; }
        public   System.DateTime   extraDate { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }

        //public virtual ICollection<tbl_AccountLedger   tbl_AccountLedger { get; set; }
    }

}