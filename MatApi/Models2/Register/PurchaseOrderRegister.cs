﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Register
{
    public class PurchaseOrderRegister
    {
    }

    public class PurchaseOrderRegisterVM
    {
        public string InvoiceNo { get; set; }
        public decimal LedgerId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Condition { get; set; }
    }

    public class MaterialReceiptRegisterVM
    {
        public string InvoiceNo { get; set; }
        public decimal LedgerId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Condition { get; set; }
    }

    public class RejectionOutRegisterVM
    {
        public string InvoiceNo { get; set; }
        public decimal LedgerId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Condition { get; set; }
    }
    public class PaymentRegisterVM
    {
        public string InvoiceNo { get; set; }
        public decimal LedgerId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class PurchaseInvoiceRegisterVM
    {
        public string InvoiceNo { get; set; }
        public decimal LedgerId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public string Column { get; set; }
        public string PurchaseMode { get; set; }
        public decimal VoucherType { get; set; }
    }

    public class PurchaseReturnRegisterVM
    {
        public string InvoiceNo { get; set; }
        public decimal LedgerId { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal AgainstInvoiceNo { get; set; }
        public decimal VoucherType { get; set; }
    }
}