﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class CreatePurchaseReturnVM
    {
        public string ReturnNo { get; set; }
        public DateTime Date { get; set; }
        public decimal SupplierId { get; set; }
        public string Narration { get; set; }
        public decimal TotalAmount { get; set; }
        public string TransportationCompany { get; set; }
        public string LrNo { get; set; }
        public decimal SalesManId { get; set; }
        public decimal CurrencyId { get; set; }
        public decimal PurchaseMasterId { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal NetAmount { get; set; }
        public decimal GrandTotal { get; set; }
        public List<PurchaseReturnLineItems> LineItems { get; set; }
    }

    public class PurchaseReturnLineItems
    {
        public int SL { get; set; }
        public decimal ProductId { get; set; }
        public decimal ProductBarcode { get; set; }
        public decimal ProductCode { get; set; }
        public string ProductName { get; set; }
        public int CategoryId { get; set; }
        public int Projectid { get; set; }
        public decimal Discount { get; set; }
        public decimal DiscountPercent { get; set; }
        public decimal GodownId { get; set; }
        public decimal GrossAmount { get; set; }
        public decimal NetAmount { get; set; }
        public decimal MaterialReceiptId { get; set; }
        public decimal PurchaseDetailsId { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitId { get; set; }
        public decimal UnitConversionId { get; set; }
        public decimal RackId { get; set; }
        public decimal BatchId { get; set; }
        public decimal Rate { get; set; }
        public string VoucherNo { get; set; }
        public string InvoiceNo { get; set; }
        public decimal PurchaseOrderDetailsId { get; set; }
        public decimal TaxId { get; set; }
        public decimal taxAmount { get; set; }
        public decimal Amount { get; set; }
        public decimal PurchaseReturnDetailId { get; set; }
        public decimal VoucherTypeId { get; set; }
    }
}