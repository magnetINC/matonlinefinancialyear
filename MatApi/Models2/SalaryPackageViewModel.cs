﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class SalaryPackageViewModel
    {
        public DataTable PayElement { get; set; }
        public DataTable SalaryPackage { get; set; }
    }
}