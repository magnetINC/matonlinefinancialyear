﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Customer
{
    public class SalesOrderVM
    {
        public List<SalesOrderDetailsInfo> infoSalesOrderDetails { get; set; }
        public SalesOrderMasterInfo infoSalesOrderMaster { get; set; }
    }

    public class SalesOrderConfirmationListVM
    {
        public string QuotationMasterId { get; set; }
        public string approved { get; set; }
        public string date { get; set; }
        public string invoiceNo { get; set; }
        public string ledgerId { get; set; }
        public string ledgerName { get; set; }
        public string narration { get; set; }
        public string totalAmount { get; set; }
        public string userName { get; set; }
        public string voucherTypeName { get; set; }
        public string crossChecked { get; set; }
    }

    public class SalesOrderAuthorizationListVM
    {
        public string salesOrderMasterId { get; set; }
        public string AuthorizationStatus { get; set; }
        public string QuotationNo { get; set; }
        public string date { get; set; }
        public string dueDate { get; set; }
        public string invoiceNo { get; set; }
        public string ledgerName { get; set; }
        public string totalAmount { get; set; }
        public string userName { get; set; }
        public string voucherTypeName { get; set; }
    }
}