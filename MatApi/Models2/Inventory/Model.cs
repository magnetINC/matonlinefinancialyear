﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ItemImporter
{
    public class Model
    {
        public ProductInfo ProductInfo { get; set; }
        public List<NewStores> NewStores { get; set; }
        public bool AutoBarcode { get; set; }
        public bool IsSaveBomCheck { get; set; }
        public bool IsSaveMultipleUnitCheck { get; set; }
        public bool IsOpeningStock { get; set; }
        public bool IsBatch { get; set; }
    }

    public class ProductInfo
    {
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string PurchaseRate { get; set; }
        public string SalesRate { get; set; }
        public string Mrp { get; set; }
        public string MaximumStock { get; set; }
        public string MinimumStock { get; set; }
        public string ReorderLevel { get; set; }
        public string TaxId { get; set; }
        public string UnitId { get; set; }
        public string GroupId { get; set; }
        public string ProductType { get; set; }
        public string SalesAccount { get; set; }
        public string EffectiveDate { get; set; }
        public string ExpenseAccount { get; set; }
        public string TaxapplicableOn { get; set; }
        public string BrandId { get; set; }
        public string SizeId { get; set; }
        public string ModelNoId { get; set; }
        public string GodownId { get; set; }
        public string RackId { get; set; }
        public bool IsallowBatch { get; set; }
        public bool IsBom { get; set; }
        public string PartNo { get; set; }
        public bool Isopeningstock { get; set; }
        public bool Ismultipleunit { get; set; }
        public bool IsActive { get; set; }
        public string Extra2 { get; set; }
        public string IsshowRemember { get; set; }
        public string Narration { get; set; }
        public string barcode { get; set; }
    }
    public class NewStores
    {
        public string StoreId { get; set; }
        public string RackId { get; set; }
        public string Quantity { get; set; }
        public string Rate { get; set; }
        public string UnitId { get; set; }
        public string Amount { get; set; }
    }
}
