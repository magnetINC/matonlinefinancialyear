﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Inventory.Report
{
    public class StockDetailsModel  //model to add items to stores when creating an "opening stock" item
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
        public string ProductCode { get; set; }
        public int ProductId { get; set; }
        public int Store { get; set; }
        public int Batch { get; set; }      
    }

    public class StockDetailsReportViewModel  //model to add items to stores when creating an "opening stock" item
    {
        public string Store { get; set; }
        public string Item { get; set; }
        public string ProductCode { get; set; }
        public string ProductName { get; set; }
        public string ProductId { get; set; }
        public string Transaction { get; set; }
        public string RefNo { get; set; }
        public string Batch { get; set; }
        public string Date { get; set; }
        public string Rate { get; set; }
        public string QtyIn { get; set; }
        public string QtyOut { get; set; }
        public string QtyBal { get; set; }
        public string AvgCost { get; set; }
        public string StockVal { get; set; }
        public string VoucherTypeName { get; set; }
        public string SubTotalBackColor { get; set; }
        public string GrandTotalColor { get; set; }
        


    }
}