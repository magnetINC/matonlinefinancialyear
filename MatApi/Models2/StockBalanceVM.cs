﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class StockBalanceVM
    {
        public decimal ProductId { get; set; }
        public string ProductName { get; set; }
        public decimal InwardQty { get; set; }
        public decimal StockBalance { get; set; }
        public decimal ReOrderLevel { get; set; }
    }
}