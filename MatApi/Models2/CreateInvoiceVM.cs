﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class CreateInvoiceVM
    {
        public decimal GrandTotal { get; set; }
        public bool IsAdvancePayment { get; set; }
        public string InvoiceNumber { get; set; }
        public string SalesMode { get; set; }
        public bool IsTax { get; set; }
        public bool IsPrintAfterSave { get; set; }
        public bool IsSendMailAfterSave { get; set; }
        public bool IsCashOrBank { get; set; }
        public decimal TotalTaxAmount { get; set; }
        public decimal SalesModeOrderNo { get; set; }
        public DateTime Date { get; set; }
        public decimal Currrency { get; set; }
        public decimal CreditPeriod { get; set; }
        public string Narration { get; set; }
        public string CashOrBank { get; set; }
        public decimal Customer { get; set; }
        public decimal BillDiscount { get; set; }
        public StockPostingInfo StockPosting { get; set; }
        public List<AdvancePayment> AdvancePayments { get; set; }
        public SalesMasterInfo SalesMasterInfo { get; set; }
        public List<SalesDetailsInfo> ItemLines { get; set; }
        public List<SalesBillTaxInfo> SalesBillTaxInfo { get; set; }
        public List<SalesInvoiceTax> SalesInvoiceTax { get; set; }
    }

    public class AdvancePayment
    {
        public int VoucherTypeId { get; set; }
        public string Display { get; set; }
        public decimal VoucherNo { get; set; }
        public decimal Balance { get; set; }
        public int ExchangeRateId { get; set; }
        public decimal InvoiceNo { get; set; }
        public bool IsApply { get; set; }
    }

    public class SalesInvoiceTax
    {
        public decimal TaxId { get; set; }
        public decimal TaxLedgerId { get; set; }
        public decimal TaxAmount { get; set; }
    }



}