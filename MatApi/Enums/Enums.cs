﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Enums
{
    public enum TransactionsEnum
    {
        DeliveryNoteMaster, DeliverNoteDetail, RejectionInMaster, RejectionInDetail,
        SalesMaster, SalesDetail, SalesReturnMaster, SalesReturnDetail,
        RejectionOutMaster, RejectionOutDetail,
        PurchaseMaster, PurchaseDetail,
        PurchaseReturn
    }

    public enum TransactionOperationEnums
    {
        Create, Update, Edit, Delete
    }
    
}