﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class AccountLedgerViewModel
    {
      
        public AccountLedgerInfo AccountLedgerInfo { get; set; }
       

        public bool IsBankAccount { get; set; }
        public bool IsCashAccount { get; set; }
        public bool IsSundryDebtorOrCreditor { get; set; }
    }




    public class AccountLedgerInfoRoot
    {

        public string AccountGroupId { get; set; }
        public string LedgerName { get; set; }
        public string Extra1 { get; set; }
        public string OpeningBalance { get; set; }
        public string CrOrDr { get; set; }
        public string Narration { get; set; }
        public string MailingName { get; set; }
        public string Address { get; set; }
        public string State { get; set; }
        public string Phone { get; set; }
        public string Mobile { get; set; }
        public string Email { get; set; }
        public string CreditPeriod { get; set; }
        public string CreditLimit { get; set; }
        public string PricinglevelId { get; set; }
        public string BillByBill { get; set; }
        public string Tin { get; set; }
        public string Cst { get; set; }
        public string Pan { get; set; }
        public object RouteId { get; set; }
        public string BankAccountNumber { get; set; }
        public string BranchName { get; set; }
        public string BranchCode { get; set; }
        public string AreaId { get; set; }


        //public string AccountGroupId { get; set; }
        //public string LedgerName { get; set; }
        //public string Extra1 { get; set; }
        //public string OpeningBalance { get; set; }
        //public string CrOrDr { get; set; }
        //public string Narration { get; set; }
        //public string MailingName { get; set; }
        //public string Address { get; set; }
        //public string State { get; set; }
        //public string Phone { get; set; }
        //public string Mobile { get; set; }
        //public string Email { get; set; }
        //public string CreditPeriod { get; set; }
        //public string CreditLimit { get; set; }
        //public string PricinglevelId { get; set; }
        //public bool BillByBill { get; set; }
        //public string Tin { get; set; }
        //public string Cst { get; set; }
        //public string Pan { get; set; }
        //public object RouteId { get; set; }
        //public string BankAccountNumber { get; set; }
        //public string BranchName { get; set; }
        //public string BranchCode { get; set; }
        //public string AreaId { get; set; } = "0";

        public string Extra2 { get; set; } = "";
        public bool IsActive { get; set; } = false;
        public decimal LedgerId { get; set; } = 0;
        public bool IsDefault { get; set; } = false;
        public DateTime ExtraDate { get; set; } = DateTime.Now;

        //public string AccountGroupId { get; set; }
        //public string LedgerName { get; set; }
        //public string Extra1 { get; set; }
        //public string OpeningBalance { get; set; }
        //public string CrOrDr { get; set; }
        //public string Narration { get; set; }
        //public string MailingName { get; set; }
        //public string Address { get; set; }
        //public string State { get; set; }
        //public string Phone { get; set; }
        //public string Mobile { get; set; }
        //public string Email { get; set; }
        //public string CreditPeriod { get; set; }
        //public string CreditLimit { get; set; }
        //public string PricinglevelId { get; set; }
        //public string BillByBill { get; set; }
        //public string Tin { get; set; }
        //public string Cst { get; set; }
        //public string Pan { get; set; }
        //public object RouteId { get; set; }
        //public string BankAccountNumber { get; set; }
        //public string BranchName { get; set; }
        //public string BranchCode { get; set; }
        //public string AreaId { get; set; }
        //public bool IsActive { get; set; }

    }

   
    public class RootAccountLedgerInfo
    {
        public AccountLedgerInfoRoot AccountLedgerInfo { get; set; }
        public bool IsBankAccount { get; set; }
        public bool IsCashAccount { get; set; }
        public bool IsSundryDebtorOrCreditor { get; set; }
    }

}