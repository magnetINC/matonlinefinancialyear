﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MatApi.Services;

namespace MatApi.Models
{
    public static class Constants
    {
        public static int OpeningStockVoucherTypeId = 2;
        public static string IncomeAccount = "Income";
        public static string ExpenseAccount = "Expenses";
        public static int Delivery_NoteVoucherTypeId = 17;
        public static int Rejection_In_VoucherTypeId = 18;
        public static int SalesInvoice_VoucherTypeId = 28;
        public static int POS_VoucherTypeId = 10034;
        public static int SalesReturn_VoucherTypeId = 20;
        public static int StockJournal_VoucherTypeId = 24;


        public static int MaterialReceiptVoucherTypeId = 11;
        public static int RejectionOut_VoucherTypeId = 12;
        public static int PurchaseInvoice_VoucherTypeId = 29;
        public static int PurchaseInvoice2_VoucherTypeId = 29;
        public static int PettyExpenses_VoucherTypeId = 10035;
        public static int PurchaseReturn_VoucherTypeId = 14;



        public static int SalesMode_Against_Delivery_Note = 17;
        public static int SalesMode_Against_Sales_Order = 10030;
        public static int PurchaseMode_PurchaseInvoice = 29;
    }

    public static class LedgerConstants
    {
        public static int SalesAccount = 10;
        public static decimal GoodsInTransit = new LedgerServices().GetGoodInTransistId();
        public static int Discount_Received = 9;
        public static int Discount_Allowed = 8;
        public static int Vat = 59;
    }

    public static class AccountGroupConstants
    {
        public static int SalesIncome = 10;
        public static int CostOfSale = 11;
        public static int DutiesAndTaxes = 20;

       // public static int ServiceIncome = 12;
        public static int DirectExpenses = 13;
        public static int OtherIncome = 14;
        public static int Expenses = 15;
        public static int OtherExpenses = 47;

        public static int properties = 4;
        public static int otherAssets = 5;
        public static int currentAssets = 6;


        public static int cash = 27;
        public static int bank = 28;

        public static int equity_reserve = 1;
        public static int longTermLiability = 2;
        public static int nonCurrentLiabilities = 36;

        public static int currentLiablity = 3; 
        public static int AccountPayable = 22; 
        public static int AccountRecievable = 26;
        public static int Primary = 0;
        public static int Stock = 27;
    }

}