﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class PayElementViewModel
    {
        public DataTable AccountLedger { get; set; }
        public DataTable PayElement { get; set; }
        public DataTable AccountGroup { get; set; }
    }
}