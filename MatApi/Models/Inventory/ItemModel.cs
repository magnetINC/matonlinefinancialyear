﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Inventory
{
    public class CustomItemList
    {
        public decimal ProductId { get; set; }
        public string ProductName { get; set; }
        public string ProductCode { get; set; }
        public string Barcode { get; set; }
        public decimal SalesRate { get; set; }
        public decimal PurchaseRate { get; set; }
        public decimal StoreId { get; set; }
        public string StoreName { get; set; }
        public decimal SubCategoryId { get; set; }
        public string SubCategory { get; set; }
        public decimal CategoryId { get; set; }
        public string Category { get; set; }
        public decimal SubGroupId { get; set; }
        public string SubGroup { get; set; }
    }

    public class NewStoreModel  //model to add items to stores when creating an "opening stock" item
    {
        public int Id { get; set; }
        public decimal StoreId { get; set; }
        public string Store { get; set; }
        public decimal RackId { get; set; }
        public string Rack { get; set; }
        public string Batch { get; set; }
        public DateTime MfD { get; set; }
        public DateTime ExpD { get; set; }
        public decimal Quantity { get; set; }
        public decimal Rate { get; set; }
        public decimal UnitId { get; set; }
        public decimal Unit { get; set; }
        public decimal Amount { get; set; }
    }

    public class NewStoreModel1  //model to add items to stores when creating an "opening stock" item
    {
        public int Id { get; set; }
        public decimal StoreId { get; set; }
        public string Store { get; set; }
        public decimal RackId { get; set; }
        public string Rack { get; set; }
        public string Batch { get; set; }
        public DateTime MfD { get; set; }
        public DateTime ExpD { get; set; }
        public decimal Quantity { get; set; }

        public decimal inwardQty { get; set; }
        
        public decimal Rate { get; set; }
        public decimal UnitId { get; set; }
        public string Unit { get; set; }
        public decimal Amount { get; set; }
    }

    public class NewBomModel    //model to populate new bom items
    {
        public decimal BomId { get; set; }
        public decimal RawMaterialId { get; set; }
        public decimal UnitId { get; set; }
        public decimal Quantity { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public DateTime ExtraDate { get; set; }
    }

    public class NewMultipleUnitModel
    {
        public int Sn { get; set; }
        public decimal ConversionRate { get; set; }
        public decimal UnitId { get; set; }
        public string Quantities { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public DateTime ExtraDate { get; set; }
        public decimal ProductId { get; set; }
    }
    public class NewStockPostingModel
    {
        public decimal StockPostingId{ get; set; }
        public DateTime Date{ get; set; }
        public decimal VoucherTypeId{ get; set; }
        public string VoucherNo{ get; set; }
        public string InvoiceNo{ get; set; }
        public decimal ProductId{ get; set; }
        public decimal BatchId{ get; set; }
        public decimal UnitId{ get; set; }
        public decimal GodownId{ get; set; }
        public decimal RackId{ get; set; }
        public decimal AgainstVoucherTypeId{ get; set; }
        public string AgainstInvoiceNo{ get; set; }
        public string AgainstVoucherNo{ get; set; }
        public decimal InwardQty{ get; set; }
        public decimal OutwardQty{ get; set; }
        public decimal Rate{ get; set; }
        public decimal FinancialYearId{ get; set; }
        public DateTime ExtraDate{ get; set; }
        public string Extra1{ get; set; }
        public string Extra2{ get; set; }
        public int ProjectId{ get; set; }
        public int CategoryId{ get; set; }
    }
    public class ItemStockDelete
    {
        public string id { get; set; }
    }
    public class NewStockPosting
    {
        public string storeId { get; set; }
        public string store { get; set; }
        public string rackId { get; set; }
        public string rack { get; set; }

        public string batch { get; set; }
        public string mfD { get; set; }
        public string expD { get; set; }
        public string quantity { get; set; }

        public string unit { get; set; }
        public string rate { get; set; }

        public string amount { get; set; }
        public string unitId { get; set; }

        public string id { get; set; }

    }
    public class ItemModel12
    {
        public List<NewStockPosting> StockPosting { get; set; }
        public List<ItemStockDelete> stockDelete { get; set; }
        public ProductSP ProductSp { get; set; }
        public MATClassLibrary.Classes.ProductModels.ProductInfo ProductInfo { get; set; }
        public UnitConvertionSP UnitConvertion { get; set; }
        public UnitConvertionInfo UnitConvertionInfo { get; set; }
        public List<NewStoreModel1> NewStores { get; set; }
        public List<NewBomModel> NewBoms { get; set; }
        public List<NewMultipleUnitModel> NewMultipleUnits { get; set; }
        public List<NewStockPostingModel> NewStockPostings { get; set; }
        public bool AutoBarcode { get; set; }
        public bool IsSaveBomCheck { get; set; }
        public bool IsSaveMultipleUnitCheck { get; set; }
        public bool IsOpeningStock { get; set; }
        public bool IsBatch { get; set; }
    }

    public class ItemModel
    {
        public List<ItemStockDelete> stockDelete { get; set; }
        public List<NewStockPosting> StockPosting { get; set; }
        public ProductSP ProductSp { get; set; }
        public MATClassLibrary.Classes.ProductModels.ProductInfo  ProductInfo { get; set; }
        public UnitConvertionSP UnitConvertion { get; set; }
        public UnitConvertionInfo UnitConvertionInfo { get; set; }
        public List<NewStoreModel> NewStores { get; set; }
        public List<NewBomModel> NewBoms { get; set; }
        public List<NewMultipleUnitModel> NewMultipleUnits { get; set; }
        public List<NewStockPostingModel> NewStockPostings { get; set; }
        public bool AutoBarcode { get; set; }
        public bool IsSaveBomCheck { get; set; }
        public bool IsSaveMultipleUnitCheck { get; set; }
        public bool IsOpeningStock { get; set; }
        public bool IsBatch { get; set; }
        public bool IsNorService { get; set; }
    }

    public class EditItemModel
    {
        public ProductSP ProductSp { get; set; }
        public ProductInfo ProductInfo { get; set; }
        public UnitConvertionSP UnitConvertion { get; set; }
        public UnitConvertionInfo UnitConvertionInfo { get; set; }
        public List<NewStoreModel> NewStores { get; set; }
        public List<NewBomModel> NewBoms { get; set; }
        public List<NewMultipleUnitModel> NewMultipleUnits { get; set; }
        public List<NewStockPostingModel> NewStockPostings { get; set; }
        public bool AutoBarcode { get; set; }
        public bool IsSaveBomCheck { get; set; }
        public bool IsSaveMultipleUnitCheck { get; set; }
        public bool IsOpeningStock { get; set; }
        public bool IsBatch { get; set; }
    }

    public class NewAddItem
    {
        public static int inBatchIdWithPartNoNA;  //identity for "batch with barcode"
        public static decimal decBatchId;

     
        public static bool PostledgerItems(decimal productId, List<NewStoreModel1> stores, DateTime ledgerDate)
        {
            try
            {
                string strfinancialId;
                FinancialYearSP spFinancialYear = new FinancialYearSP();
                FinancialYearInfo infoFinancialYear = new FinancialYearInfo();
                infoFinancialYear = spFinancialYear.FinancialYearViewForAccountLedger(1);
                strfinancialId = infoFinancialYear.FromDate.ToString("dd-MMM-yyyy");
                LedgerPostingInfo info = new LedgerPostingInfo();
                LedgerPostingSP sp = new LedgerPostingSP();
                decimal OpeningStock = 0;
                foreach (var store in stores)
                {
                    OpeningStock += Convert.ToDecimal(store.Amount);
                }
                info.Date = ledgerDate;
                info.VoucherTypeId = 2;
                info.VoucherNo = "55";
                info.LedgerId = 55;
                info.Debit = 0;
                info.Credit = OpeningStock;
                info.DetailsId = 0;
                info.YearId = PublicVariables.CurrentFinicialYearId;
                info.InvoiceNo = productId.ToString();
                info.ChequeNo = "";
                info.ChequeDate = DateTime.Now;
                info.ExtraDate = DateTime.Now;
                info.Extra1 = productId.ToString();
                info.Extra2 = "";
                sp.LedgerPostingAdd(info);

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }


        public static int BatchWithBarCode(ItemModel12 item, decimal productId)
        {
            try
            {
                BatchSP spBatch = new BatchSP();
                BatchInfo infoBatch = new BatchInfo();
                Int64 inBarcode = spBatch.AutomaticBarcodeGeneration();
                infoBatch.BatchNo = "NA";
                infoBatch.ExpiryDate = DateTime.Now;
                infoBatch.ManufacturingDate = DateTime.Now;
                infoBatch.partNo = item.ProductInfo.PartNo;
                infoBatch.ProductId = productId;
                infoBatch.narration = string.Empty;
                infoBatch.ExtraDate = DateTime.Now;
                if (item.AutoBarcode == false && item.ProductInfo.barcode != "")  // precious
                {
                    infoBatch.barcode = item.ProductInfo.barcode;
                }
                else
                {
                    infoBatch.barcode = Convert.ToString(inBarcode);
                }
                infoBatch.Extra1 = string.Empty;
                infoBatch.Extra2 = string.Empty;
                return inBatchIdWithPartNoNA = spBatch.BatchAddWithBarCode(infoBatch);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static bool UpdateledgerItems(decimal productId, List<NewStoreModel1> stores, List<NewStoreModel1> NewStores, DateTime ledgerDate , string VoucherNo,  decimal VoucherTypeId , bool HasDeleted  , decimal AmountDeleting )
        {
            try
            {
                string strfinancialId;
                FinancialYearSP spFinancialYear = new FinancialYearSP();
                FinancialYearInfo infoFinancialYear = new FinancialYearInfo();
                infoFinancialYear = spFinancialYear.FinancialYearViewForAccountLedger(1);
                strfinancialId = infoFinancialYear.FromDate.ToString("dd-MMM-yyyy");
                LedgerPostingInfo info = new LedgerPostingInfo();
                LedgerPostingSP sp = new LedgerPostingSP();
                decimal OpeningStock = 0;

                try
                {
                    if (stores.Count > 0)
                    {
                        foreach (var store in stores)
                        {
                            OpeningStock += Convert.ToDecimal(store.Rate * store.Quantity);
                        }

                    }


                    if (NewStores.Count > 0)
                    {
                        foreach (var store in NewStores)
                        {
                            OpeningStock += Convert.ToDecimal(store.Rate * store.inwardQty);
                        }
                    }
                }
                catch(Exception ex)
                {

                }
               
                info.Date = ledgerDate;
                info.VoucherTypeId = VoucherTypeId;
                info.VoucherNo = VoucherNo;
                info.LedgerId = 55;
                info.Debit = 0;
                info.DetailsId = 0;
                info.YearId = PublicVariables.CurrentFinicialYearId;
                info.InvoiceNo = productId.ToString();
                info.ChequeNo = "";
                info.ChequeDate = DateTime.Now;
                info.ExtraDate = DateTime.Now;
                info.Extra1 = productId.ToString();
                info.Extra2 = "";
               
                //Not Used for updating  sp.LedgerPostingAdd(info);
                var context = new DBModel.DBMATAccounting_MagnetEntities1();

                var ListLeadger = context.tbl_LedgerPosting.Where(u => u.invoiceNo == productId.ToString() && u.extra1 == productId.ToString() && u.voucherTypeId == VoucherTypeId).ToList();

                if(ListLeadger.Count > 0)
                {
                    foreach (var itm in ListLeadger)
                    {
                        itm.date = ledgerDate;
                        itm.voucherTypeId = VoucherTypeId;
                        itm.voucherNo = VoucherNo;
                        context.Entry(itm).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();
                    }
                }
              
                if(OpeningStock > 0)
                {
                    var Leadger = context.tbl_LedgerPosting.Where(u => u.invoiceNo == productId.ToString() && u.extra1 == productId.ToString() && u.voucherTypeId == VoucherTypeId && u.voucherNo == VoucherNo).ToList();

                    var LastLeadger = Leadger[Leadger.Count - 1];

                    if (LastLeadger != null)
                    {
                        if (/*OpeningStock > LastLeadger.credit &&*/ HasDeleted)
                        {
                            info.Debit = LastLeadger.credit.Value;
                            info.Credit = 0;
                            sp.LedgerPostingAdd(info);

                            info.Debit = 0;
                            info.Credit = OpeningStock;
                            sp.LedgerPostingAdd(info);
                        }
                        else
                        {
                            info.Credit = 0;
                            info.Debit = LastLeadger.credit.Value;
                            sp.LedgerPostingAdd(info);

                            info.Debit = 0;
                            info.Credit = OpeningStock;
                            sp.LedgerPostingAdd(info);

                        }
                    }
                    else
                    {
                        info.Credit = OpeningStock;
                        sp.LedgerPostingAdd(info);
                    }
                }
                else
                {
                    var Leadger = context.tbl_LedgerPosting.Where(u => u.invoiceNo == productId.ToString() && u.extra1 == productId.ToString() && u.voucherTypeId == VoucherTypeId && u.voucherNo == VoucherNo).ToList();

                    var LastLeadger = Leadger[Leadger.Count - 1];

                    if (LastLeadger != null)
                    {
                        if (/*OpeningStock > LastLeadger.credit &&*/ HasDeleted)
                        {
                            info.Credit = 0;
                            info.Debit = LastLeadger.credit.Value;
                            sp.LedgerPostingAdd(info);
                        }
                        else
                        {
                            info.Credit = 0;
                            info.Debit = LastLeadger.credit.Value;
                            sp.LedgerPostingAdd(info);
                        }
                       
                    }
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static decimal StockPostingTableFill(List<NewStoreModel1> stores, decimal productId, ItemModel12 item, int Type = 0, string VoucherNo = "", double VoucherID = 0)
        {
            try
            {
                BatchSP spBatch = new BatchSP();
                StockPostingSP spStockPosting = new StockPostingSP();
                StockPostingInfo infoStockPosting;

                var DateFf = Convert.ToDateTime(item.ProductInfo.EffectiveDate);

                var context = new DBModel.DBMATAccounting_MagnetEntities1();

                var ListStockPostings = context.tbl_StockPosting.Where(u => u.productId == productId && u.voucherTypeId == 2).ToList();

                if (ListStockPostings.Count > 0)
                {
                    foreach (var itm in ListStockPostings)
                    {
                        itm.date = DateFf;
                        itm.voucherTypeId = 2;
                        itm.voucherNo = VoucherNo;
                        context.Entry(itm).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();
                    }
                }

                if (item.NewStores.Count > 0)
                {
                    foreach (var store in stores)
                    {
                        infoStockPosting = new StockPostingInfo
                        {
                            AgainstInvoiceNo = string.Empty,
                            AgainstVoucherNo = string.Empty,
                            Date = Convert.ToDateTime(item.ProductInfo.EffectiveDate),
                            AgainstVoucherTypeId = 0,
                            InvoiceNo = Convert.ToString(productId),
                            VoucherNo = VoucherNo,
                            ProductId = productId,
                            VoucherTypeId = (decimal)VoucherID,
                            UnitId = Convert.ToDecimal(store.UnitId),
                            InwardQty = Convert.ToDecimal(store.Quantity),
                            OutwardQty = 0,
                            Rate = Convert.ToDecimal(store.Rate),
                            FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId,
                            Extra1 = string.Empty,
                            Extra2 = string.Empty,
                            ExtraDate = DateTime.Now,
                            RackId = store.RackId,
                            GodownId = store.StoreId,
                            ProjectId = 0,
                            CategoryId = 0
                        };

                        //var infoBatch = new BatchInfo
                        // {
                        //     ProductId = productId,
                        //     Extra1 = string.Empty,
                        //     Extra2 = string.Empty,
                        //     ExtraDate = DateTime.Now,
                        //     barcode = Convert.ToString(spBatch.AutomaticBarcodeGeneration()),
                        //     narration = " ", 
                        // };

                        //decBatchId = spBatch.BatchAddReturnIdentity(infoBatch);

                        if (store.RackId == 0)
                        {
                            infoStockPosting.RackId = 0;
                        }

                        if (!item.IsBatch)
                        {
                            infoStockPosting.BatchId = inBatchIdWithPartNoNA;
                        }

                        if (StockPostingAdd(infoStockPosting) < 1)
                        {
                            return 0;
                        }

                        //if (spStockPosting.StockPostingAdd(infoStockPosting) < 1)
                        //{
                        //    return 0;
                        //}
                    }
                }

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static decimal DeleteStockItem(decimal productId, List<ItemStockDelete> stocksDelete)
        {

            try
            {
                var context = new DBModel.DBMATAccounting_MagnetEntities1();
                decimal totalAmount = 0;
                string VoucherNo = "";

                foreach (var itm in stocksDelete)
                {
                    var id = Convert.ToDecimal(itm.id);
                    var stockPostingProduct = context.tbl_StockPosting.Where(a => a.stockPostingId == id && a.productId == productId && a.voucherTypeId == 2).FirstOrDefault();
                    if (stockPostingProduct != null)
                    {
                        totalAmount += stockPostingProduct.rate.Value;
                        VoucherNo = stockPostingProduct.voucherNo;
                    }

                }

                foreach (var itm in stocksDelete)
                {
                    int pa = Convert.ToInt32(itm.id);

                    var stockPostingProduct = context.tbl_StockPosting.Where(a => a.stockPostingId == pa && a.voucherTypeId == 2).FirstOrDefault();
                    if (stockPostingProduct != null)
                    {
                        if (stockPostingProduct.batchId != null && stockPostingProduct.batchId > 0)
                        {
                            var Batches = context.tbl_Batch.Where(a => a.batchId == stockPostingProduct.batchId && a.productId == pa).FirstOrDefault();

                            if (Batches != null)
                            {
                                context.tbl_Batch.Remove(Batches);
                                context.SaveChanges();
                            }
                        }

                        context.tbl_StockPosting.Remove(stockPostingProduct);
                        context.SaveChanges();
                    }

                }

                //  NewAddItem.PostDebiteledgerItems(productId, totalAmount, VoucherNo,  0);

                return totalAmount;

            }
            catch (Exception ex)
            {
                return 0;
            }

        }

        public static bool PostDebiteledgerItems(decimal productId, decimal Amount,string VoucherNo, int type = 0)
        {
            try
            {

                string strfinancialId;
                FinancialYearSP spFinancialYear = new FinancialYearSP();
                FinancialYearInfo infoFinancialYear = new FinancialYearInfo();
                infoFinancialYear = spFinancialYear.FinancialYearViewForAccountLedger(1);
                strfinancialId = infoFinancialYear.FromDate.ToString("dd-MMM-yyyy");
                LedgerPostingInfo info = new LedgerPostingInfo();
                LedgerPostingSP sp = new LedgerPostingSP();

                info.Date = DateTime.Now;
                info.VoucherTypeId = 2;
                info.VoucherNo = VoucherNo;
                info.LedgerId = 596;
                info.Credit = 0;
                info.DetailsId = 0;
                info.YearId = PublicVariables.CurrentFinicialYearId;
                info.InvoiceNo = productId.ToString();
                info.ChequeNo = "";
                info.ChequeDate = DateTime.Now;
                info.ExtraDate = DateTime.Now;
                info.Extra1 = productId.ToString();
                info.Extra2 = "";

                var context = new DBModel.DBMATAccounting_MagnetEntities1();
                var Leadger = context.tbl_LedgerPosting.Where(u => u.invoiceNo == productId.ToString() && u.extra1 == productId.ToString() && u.voucherNo == VoucherNo && u.voucherTypeId == 2).ToList();

                var ListLeadger = Leadger[Leadger.Count - 1];

                if (ListLeadger != null)
                {

                    info.Debit = (decimal)Amount;
                    info.Credit = (ListLeadger.credit.Value - (decimal)Amount);
                    //Leadger.credit = (Leadger.credit + info.Credit);
                    sp.LedgerPostingAdd(info);

                    //if (type == 0)
                    //{

                    //}
                    //else
                    //{

                    //}
                   
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public static decimal StockPostingAdd(StockPostingInfo stockpostinginfo)
        {
            //SqlTransaction transaction;
            //if (sqlcon.State == ConnectionState.Closed)
            //{
            //    sqlcon.Open();
            //}
            //SqlCommand command = sqlcon.CreateCommand();
            //transaction = sqlcon.BeginTransaction("LocalTransaction");
            ////command.Connection = sqlcon;
            //command.Transaction = transaction;

            decimal decProductId = 0;
            try
            {
                var context = new DBModel.DBMATAccounting_MagnetEntities1();

                var stockPosting = new DBModel.tbl_StockPosting();

                stockPosting.extra2 = stockpostinginfo.Extra2;
                stockPosting.batchId = stockpostinginfo.BatchId;
                stockPosting.productId = stockpostinginfo.ProductId;
                stockPosting.financialYearId = stockpostinginfo.FinancialYearId;
                stockPosting.extraDate = DateTime.Now;
                stockPosting.extra1 = stockpostinginfo.Extra1;
                stockPosting.rackId = stockpostinginfo.RackId;
                stockPosting.voucherNo = stockpostinginfo.VoucherNo;
                stockPosting.CategoryId = stockpostinginfo.CategoryId;
                stockPosting.ProjectId = stockpostinginfo.ProjectId;
                stockPosting.unitId = stockpostinginfo.UnitId;
                stockPosting.voucherTypeId = stockpostinginfo.VoucherTypeId;
                stockPosting.invoiceNo = stockpostinginfo.InvoiceNo;
                stockPosting.rate = stockpostinginfo.Rate;
                stockPosting.godownId = stockpostinginfo.GodownId;
                stockPosting.date = stockpostinginfo.Date;
                stockPosting.againstInvoiceNo = stockpostinginfo.AgainstInvoiceNo;
                stockPosting.againstVoucherNo = stockpostinginfo.AgainstVoucherNo;
                stockPosting.againstVoucherTypeId = stockpostinginfo.AgainstVoucherTypeId;
                stockPosting.inwardQty = stockpostinginfo.InwardQty;
                stockPosting.outwardQty = stockpostinginfo.OutwardQty;

                context.tbl_StockPosting.Add(stockPosting);
                context.SaveChanges();
                decProductId = stockPosting.stockPostingId;

                return decProductId;

                //if (sqlcon.State == ConnectionState.Closed)
                //{
                //    sqlcon.Open();
                //}

                //SqlCommand sccmd = new SqlCommand("dbo.StockPostingAdd", sqlcon);
                //sccmd.CommandType = CommandType.StoredProcedure;
                //SqlParameter sprmparam = new SqlParameter();
                //sprmparam = sccmd.Parameters.Add("@date", SqlDbType.DateTime);
                //sprmparam.Value = stockpostinginfo.Date;
                //sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.VoucherTypeId;
                //sprmparam = sccmd.Parameters.Add("@voucherNo", SqlDbType.VarChar);
                //sprmparam.Value = stockpostinginfo.VoucherNo;
                //sprmparam = sccmd.Parameters.Add("@invoiceNo", SqlDbType.VarChar);
                //sprmparam.Value = stockpostinginfo.InvoiceNo;
                //sprmparam = sccmd.Parameters.Add("@productId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.ProductId;
                //sprmparam = sccmd.Parameters.Add("@batchId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.BatchId;
                //sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.UnitId;
                //sprmparam = sccmd.Parameters.Add("@godownId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.GodownId;
                //sprmparam = sccmd.Parameters.Add("@rackId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.RackId;
                //sprmparam = sccmd.Parameters.Add("@againstVoucherTypeId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.AgainstVoucherTypeId;
                //sprmparam = sccmd.Parameters.Add("@againstInvoiceNo", SqlDbType.VarChar);
                //sprmparam.Value = stockpostinginfo.AgainstInvoiceNo;
                //sprmparam = sccmd.Parameters.Add("@againstVoucherNo", SqlDbType.VarChar);
                //sprmparam.Value = stockpostinginfo.AgainstVoucherNo;
                //sprmparam = sccmd.Parameters.Add("@inwardQty", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.InwardQty;
                //sprmparam = sccmd.Parameters.Add("@outwardQty", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.OutwardQty;
                //sprmparam = sccmd.Parameters.Add("@rate", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.Rate;
                //sprmparam = sccmd.Parameters.Add("@financialYearId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.FinancialYearId;
                //sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                //sprmparam.Value = stockpostinginfo.Extra1;
                //sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                //sprmparam.Value = stockpostinginfo.Extra2;
                //sprmparam = sccmd.Parameters.Add("@ProjectId", SqlDbType.Int);
                //sprmparam.Value = stockpostinginfo.ProjectId;
                //sprmparam = sccmd.Parameters.Add("@CategoryId", SqlDbType.Int);
                //sprmparam.Value = stockpostinginfo.CategoryId;
                //decProductId = Convert.ToDecimal(sccmd.ExecuteScalar());
                //return sccmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                try
                {

                    //transaction.Rollback();
                }
                catch (Exception ex2)
                {
                    //  MessageBox.Show(ex2.ToString());
                }
            }
            finally
            {
                // sqlcon.Close();
            }

            return decProductId;
        }

        public static bool ValidateOpeningStock(int productId , DateTime datetime)
        {
            try
            {

                var context = new DBModel.DBMATAccounting_MagnetEntities1();

                var fromDate = MATFinancials.PublicVariables._dtFromDate;
                var toDate = MATFinancials.PublicVariables._dtToDate;

                var OpeningStocks1 = context.tbl_StockPosting.Where(u => u.productId == productId && u.voucherTypeId == 2 && (u.date >= fromDate && u.date <= toDate)).FirstOrDefault();

                if(OpeningStocks1 == null)
                {
                    return false;
                }

               var ListOfStockPostings = context.tbl_StockPosting.Where(u => u.productId == OpeningStocks1.productId && u.voucherTypeId != 2 && (u.date >= fromDate && u.date <= toDate) && datetime > u.date && OpeningStocks1.date > u.date).ToList();

                if ((datetime >= OpeningStocks1.date) && ListOfStockPostings.Count > 0)
                {
                    return true;
                }

                ListOfStockPostings = context.tbl_StockPosting.Where(u => u.productId == OpeningStocks1.productId && u.voucherTypeId != 2 && (u.date >= fromDate && u.date <= toDate) && datetime > u.date).ToList();

                if ((datetime >= OpeningStocks1.date) && ListOfStockPostings.Count > 0)
                {
                    return true;
                }

                 ListOfStockPostings = context.tbl_StockPosting.Where(u => u.productId == OpeningStocks1.productId && u.voucherTypeId != 2 && (u.date >= fromDate && u.date <= toDate)).ToList();


                ListOfStockPostings = context.tbl_StockPosting.Where(u => u.productId == OpeningStocks1.productId && u.voucherTypeId != 2 && (u.date >= fromDate && u.date <= toDate) && u.date > datetime).ToList();

                if ((OpeningStocks1.date >= datetime || datetime < OpeningStocks1.date) && ListOfStockPostings.Count > 0)
                {
                    return false;
                }

                ListOfStockPostings = context.tbl_StockPosting.Where(u => u.productId == OpeningStocks1.productId && u.voucherTypeId != 2 && (u.date >= fromDate && u.date <= toDate) && u.date > datetime && u.date > OpeningStocks1.date).ToList();

                if ((OpeningStocks1.date >= datetime || datetime < OpeningStocks1.date) && ListOfStockPostings.Count > 0)
                {
                    return false;
                }

                if ((OpeningStocks1.date >= datetime || OpeningStocks1.date <= datetime) && ListOfStockPostings.Count <= 0)
                {
                    return false;
                }

                return false;

                var OpeningStocks = context.tbl_StockPosting.Where(u => u.productId == productId && u.voucherTypeId == 2 && (u.date >= fromDate && u.date <= toDate)).ToList();

                var listAva = new List<bool>();

                foreach (var opitem in OpeningStocks)
                {
                    if (datetime > opitem.date)
                    {

                        if ((opitem.date >= fromDate && opitem.date <= toDate))
                        {
                            ListOfStockPostings = context.tbl_StockPosting.Where(u => u.productId == opitem.productId && u.voucherTypeId != 2 && (u.date >= fromDate && u.date <= toDate)).ToList();

                            var HasBeforeTransactions = ListOfStockPostings.Where(u => u.date > opitem.date && u.date > datetime).Any();
                            if (HasBeforeTransactions && datetime > opitem.date)
                            {
                                return false;
                            }

                            HasBeforeTransactions = ListOfStockPostings.Where(u => opitem.date > u.date).Any();
                            if (HasBeforeTransactions && datetime < opitem.date)
                            {
                                return false;
                            }

                            HasBeforeTransactions = ListOfStockPostings.Where(u => u.date > opitem.date).Any();

                            if (HasBeforeTransactions && datetime > opitem.date)
                            {
                                return true;
                            }
                        }

                    }
                    else
                    {
                        if ((opitem.date >= fromDate && opitem.date <= toDate))
                        {
                            ListOfStockPostings = context.tbl_StockPosting.Where(u => u.productId == opitem.productId && u.voucherTypeId != 2 && (u.date >= fromDate && u.date <= toDate)).ToList();

                            var HasBeforeTransactions = ListOfStockPostings.Where(u => u.date > opitem.date && u.date > datetime).Any();
                            if (HasBeforeTransactions && datetime > opitem.date)
                            {
                                return false;
                            }

                             HasBeforeTransactions = ListOfStockPostings.Where(u => opitem.date > u.date).Any();
                            if (HasBeforeTransactions && datetime < opitem.date)
                            {
                                return false;
                            }

                            HasBeforeTransactions = ListOfStockPostings.Where(u => u.date > opitem.date).Any();

                            if (HasBeforeTransactions && datetime > opitem.date)
                            {
                                return true;
                            }
                        }
                    }

                }

               


                return false;
            }
            catch(Exception ex)
            {
                return true;
            }
        }


        public static decimal BatchTableWithStockAndProductBatchFill(List<NewStoreModel1> stores, decimal productId, ItemModel12 item, int InfoType = 0, string VoucherNo = "", double VoucherID = 0)
        {
            try
            {
                BatchSP spBatch = new BatchSP();
                BatchInfo infoBatch;
                var spStockPosting = new StockPostingSP();
                StockPostingInfo infoStockPosting;

                var DateFf = Convert.ToDateTime(item.ProductInfo.EffectiveDate);

                var context = new DBModel.DBMATAccounting_MagnetEntities1();


                var ListStockPostings = context.tbl_StockPosting.Where(u => u.productId == productId && u.voucherTypeId == 2).ToList();

                if (ListStockPostings.Count > 0)
                {
                    foreach (var itm in ListStockPostings)
                    {
                        itm.date = DateFf;
                        itm.voucherNo = VoucherNo;
                        itm.voucherTypeId = 2;
                        context.Entry(itm).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();
                    }
                }

                if (item.NewStores != null && item.NewStores.Count > 0)
                {
                    foreach (var store in stores)
                    {
                        infoStockPosting = new StockPostingInfo
                        {
                            AgainstInvoiceNo = string.Empty,
                            AgainstVoucherNo = string.Empty,
                            Date = Convert.ToDateTime(item.ProductInfo.EffectiveDate),
                            AgainstVoucherTypeId = 0,
                            InvoiceNo = Convert.ToString(productId),
                            VoucherNo = VoucherNo,
                            ProductId = productId,
                            VoucherTypeId = 2,
                            UnitId = Convert.ToDecimal(store.UnitId),
                            InwardQty = Convert.ToDecimal(store.Quantity),
                            OutwardQty = 0,
                            Rate = Convert.ToDecimal(store.Rate),
                            FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId,
                            Extra1 = string.Empty,
                            Extra2 = string.Empty,
                            ExtraDate = DateTime.Now,
                            RackId = store.RackId,
                            GodownId = store.StoreId,
                            ProjectId = 0,
                            CategoryId = 0,
                        };


                        if (string.IsNullOrEmpty(store.Batch) || string.IsNullOrWhiteSpace(store.Batch))
                        {
                            store.Batch = "NA";
                        }

                        if (!item.ProductInfo.IsallowBatch)
                        {
                            store.Batch = "NA";
                        }

                        infoBatch = new BatchInfo
                        {
                            ManufacturingDate = store.MfD,
                            ExpiryDate = store.ExpD,
                            BatchNo = store.Batch,
                            ProductId = productId,
                            Extra1 = string.Empty,
                            Extra2 = string.Empty,
                            ExtraDate = DateTime.Now,
                            barcode = Convert.ToString(spBatch.AutomaticBarcodeGeneration()),
                            narration = string.Empty,

                        };

                        decBatchId = spBatch.BatchAddReturnIdentity(infoBatch);

                        if (store.RackId == 0)
                        {
                            infoStockPosting.RackId = 0;
                        }
                        else
                        {
                            infoStockPosting.RackId = store.RackId;
                        }
                        if (store.StoreId == 0)
                        {
                            infoStockPosting.GodownId = 1;
                        }
                        else
                        {
                            infoStockPosting.GodownId = store.StoreId;
                        }

                        if (!item.ProductInfo.IsallowBatch)
                        {
                            infoStockPosting.BatchId = 0;
                        }
                        else
                        {
                            infoStockPosting.BatchId = decBatchId;
                        }

                        infoStockPosting.BatchId = decBatchId;

                        var chk = StockPostingAdd(infoStockPosting);
                        if (chk < 1)
                        {
                            return 0;
                        }

                        //if (spStockPosting.StockPostingAdd(infoStockPosting) < 1)
                        //{
                        //    return 0;
                        //}

                    }
                }
  
                return 1;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "PC:18" + ex.Message;
                return 0;
            }
        }
    }

    public class AddItem
    {
        int inBatchIdWithPartNoNA;  //identity for "batch with barcode"
        decimal decBatchId;
        public decimal SaveItem(ItemModel input)
        {
            ProductSP spProduct = new ProductSP();
            MATClassLibrary.Classes.ProductModels.ProductInfo infoProduct = new MATClassLibrary.Classes.ProductModels.ProductInfo();
            UnitConvertionSP spUnitConvertion = new UnitConvertionSP();
            UnitConvertionInfo infoUnitConvertion = new UnitConvertionInfo();

            infoProduct.ProductName = input.ProductInfo.ProductName;
            infoProduct.ProductCode = input.ProductInfo.ProductCode;
            infoProduct.PurchaseRate = input.ProductInfo.PurchaseRate;
            infoProduct.SalesRate = input.ProductInfo.SalesRate;
            infoProduct.Mrp = input.ProductInfo.Mrp;

            infoProduct.MaximumStock = input.ProductInfo.MaximumStock;
            infoProduct.MinimumStock = input.ProductInfo.MinimumStock;
            infoProduct.ReorderLevel = input.ProductInfo.ReorderLevel;
            infoProduct.Extra1 = input.ProductInfo.Extra1;
            infoProduct.Extra2 = input.ProductInfo.Extra2;
            infoProduct.ExtraDate = DateTime.Now;
            infoProduct.TaxId = input.ProductInfo.TaxId;
            infoProduct.UnitId = input.ProductInfo.UnitId;
            infoProduct.GroupId = input.ProductInfo.GroupId;
            infoProduct.Narration = input.ProductInfo.Narration;
            // code segment modified by ___precious 04/08/2016 (copied by alex from matfinancial desktop 19/06/2017)
            infoProduct.ProductType = input.ProductInfo.ProductType;
            infoProduct.SalesAccount = input.ProductInfo.SalesAccount;
            infoProduct.EffectiveDate = Convert.ToDateTime(input.ProductInfo.EffectiveDate);
            infoProduct.ExpenseAccount = input.ProductInfo.ExpenseAccount;

            infoUnitConvertion.UnitId = input.ProductInfo.UnitId;
            infoUnitConvertion.ConversionRate = 1;
            infoUnitConvertion.Quantities = string.Empty;
            infoUnitConvertion.Extra1 = string.Empty;
            infoUnitConvertion.Extra2 = string.Empty;
            infoUnitConvertion.ExtraDate = DateTime.Now;
            if (input.ProductInfo.TaxapplicableOn == "Rate")
            {
                infoProduct.TaxapplicableOn = "Rate";
            }
            else
            {
                infoProduct.TaxapplicableOn = input.ProductInfo.TaxapplicableOn;
            }
            if (input.ProductInfo.BrandId != 0)
            {
                infoProduct.BrandId = input.ProductInfo.BrandId;
            }
            else
            {
                infoProduct.BrandId = 1;
            }
            if (input.ProductInfo.SizeId != 0)
            {
                infoProduct.SizeId = input.ProductInfo.SizeId;
            }
            else
            {
                infoProduct.SizeId = 1;
            }
            if (input.ProductInfo.ModelNoId != 0)
            {
                infoProduct.ModelNoId = input.ProductInfo.ModelNoId;
            }
            else
            {
                infoProduct.ModelNoId = 1;
            }
            if (input.ProductInfo.GodownId != 0)
            {
                infoProduct.GodownId = input.ProductInfo.GodownId;
            }
            else
            {
                infoProduct.GodownId = 1;
            }
            if (input.ProductInfo.RackId != 0)
            {
                infoProduct.RackId = input.ProductInfo.RackId;
            }
            else
            {
                infoProduct.RackId = 1;
            }
            infoProduct.IsallowBatch = input.ProductInfo.IsallowBatch;
            infoProduct.IsBom = input.ProductInfo.IsBom;
            infoProduct.Isopeningstock = input.ProductInfo.Isopeningstock;
            if (infoProduct.Isopeningstock == true)
            {
                infoProduct.PurchaseRate = input.NewStores.FirstOrDefault().Rate;
                //use the first rate for purchase rate since all stores use same rate
            }
            infoProduct.Ismultipleunit = input.ProductInfo.Ismultipleunit;

            infoProduct.IsActive = input.ProductInfo.IsActive;
            infoProduct.IsshowRemember = input.ProductInfo.IsshowRemember;
            infoProduct.EffectiveDate = input.ProductInfo.EffectiveDate;

            if (infoProduct.Isopeningstock)
            {
                if (!infoProduct.IsallowBatch)
                {
                    decimal productIdentity = spProduct.ProductAdd(infoProduct);
                    if (productIdentity > 0)
                    {
                       
                        PostledgerItems(productIdentity, input.NewStores, input.ProductInfo.EffectiveDate, input.ProductInfo.VoucherId);  /// a call to Update Ledger When an item is Created
                        infoUnitConvertion.ProductId = productIdentity;

                        spUnitConvertion.UnitConvertionAdd(infoUnitConvertion);

                        BatchWithBarCode(input, productIdentity);
                        if (input.IsSaveBomCheck)
                        {
                            int bm = BomTableFill(input.NewBoms, productIdentity);
                        }
                        if (input.IsSaveMultipleUnitCheck)
                        {
                            int unt = UnitConvertionTableFill(input.NewMultipleUnits, productIdentity);
                        }
                        if (input.IsOpeningStock)
                        {
                            decimal stk = StockPostingTableFill(input.NewStores, productIdentity, input, 0, input.ProductInfo.VoucherId);
                        }
                        return productIdentity;
                    }
                }

                if (infoProduct.IsallowBatch)
                {
                    decimal productIdentity = spProduct.ProductAdd(infoProduct);
                    if (productIdentity > 0)
                    {
                        PostledgerItems(productIdentity, input.NewStores, input.ProductInfo.EffectiveDate, input.ProductInfo.VoucherId);  /// a call to Update Ledger When an item is Created
                        infoUnitConvertion.ProductId = productIdentity;
                        spUnitConvertion.UnitConvertionAdd(infoUnitConvertion);
                        if (input.IsSaveBomCheck)
                        {
                            int bm = BomTableFill(input.NewBoms, productIdentity);
                        }
                        if (input.IsSaveMultipleUnitCheck)
                        {
                            int unt = UnitConvertionTableFill(input.NewMultipleUnits, productIdentity);
                        }
                        BatchTableWithStockAndProductBatchFill(input.NewStores, productIdentity, input, 0,  input.ProductInfo.VoucherId);
                        return productIdentity;
                    }
                }
            }
            else
            {
                decimal productIdentity = spProduct.ProductAdd(infoProduct);
                if (productIdentity > 0)
                {
                    PostledgerItems(productIdentity, input.NewStores, input.ProductInfo.EffectiveDate, input.ProductInfo.VoucherId);

                    if (input.NewStores.Count > 0)
                    { /// a call to Update Ledger When an item is Created
                        infoUnitConvertion.ProductId = productIdentity;
                      
                        if (spUnitConvertion.UnitConvertionAdd(infoUnitConvertion) > 0)
                        {
                            if (input.IsSaveBomCheck)
                            {
                                int bm = BomTableFill(input.NewBoms, productIdentity);
                            }
                            if (input.IsSaveMultipleUnitCheck)
                            {
                                int unt = UnitConvertionTableFill(input.NewMultipleUnits, productIdentity);
                            }
                            if (BatchWithBarCode(input, productIdentity) > 0)
                            {
                                //return "SUCCESS";
                            }
                        }
                    }
                    else
                    {
                        infoUnitConvertion.ProductId = productIdentity;

                        spUnitConvertion.UnitConvertionAdd(infoUnitConvertion);

                        if (input.NewBoms.Count > 0)
                        {
                            int bm = BomTableFill(input.NewBoms, productIdentity);
                        }

                        if (input.NewMultipleUnits.Count > 0)
                        {
                            int unt = UnitConvertionTableFill(input.NewMultipleUnits, productIdentity);
                        }

                        if (BatchWithBarCode(input, productIdentity) > 0)
                        {
                            //return "SUCCESS";
                        }
                    }
                }

                return productIdentity;
            }

            return 0;
        }

        public static void DeleteStockItem(List<ItemStockDelete> stocksDelete)
        {

            try
            {
                var context = new DBModel.DBMATAccounting_MagnetEntities1();

                foreach (var itm in stocksDelete)
                {
                    int pa = Convert.ToInt32(itm.id);
                    var stockPostingProduct = context.tbl_StockPosting.Where(a => a.stockPostingId == pa).FirstOrDefault();
                    if (stockPostingProduct != null)
                    {
                        if (stockPostingProduct.batchId != null && stockPostingProduct.batchId > 0)
                        {
                            var Batches = context.tbl_Batch.Where(a => a.batchId == stockPostingProduct.batchId && a.productId == pa).FirstOrDefault();

                            if (Batches != null)
                            {
                                context.tbl_Batch.Remove(Batches);
                                context.SaveChanges();
                            }
                        }

                        new AddItem().PostDebiteledgerItems(stockPostingProduct.productId.Value, stockPostingProduct.rate.Value, 0);

                        //var Leadger = context.tbl_LedgerPosting.Where(u => u.invoiceNo == stockPostingProduct.productId.ToString() && u.extra1 == stockPostingProduct.productId.ToString()).FirstOrDefault();
                        //if (Leadger != null)
                        //{
                        //    //Leadger.debit = (Leadger.debit + stockPostingProduct.rate);
                        //    //context.Entry(Leadger).State = System.Data.Entity.EntityState.Modified;
                        //    //context.SaveChanges();
                        //}

                        context.tbl_StockPosting.Remove(stockPostingProduct);
                        context.SaveChanges();
                    }

                }
            }
            catch (Exception ex)
            {

            }

        }


        public bool UpdateledgerItems(decimal productId, List<NewStoreModel> stores, DateTime ledgerDate)
        {
            try
            {
                string strfinancialId;
                FinancialYearSP spFinancialYear = new FinancialYearSP();
                FinancialYearInfo infoFinancialYear = new FinancialYearInfo();
                infoFinancialYear = spFinancialYear.FinancialYearViewForAccountLedger(1);
                strfinancialId = infoFinancialYear.FromDate.ToString("dd-MMM-yyyy");
                LedgerPostingInfo info = new LedgerPostingInfo();
                LedgerPostingSP sp = new LedgerPostingSP();
                decimal OpeningStock = 0;
                foreach (var store in stores)
                {
                    OpeningStock += Convert.ToDecimal(store.Amount);
                }
                info.Date = ledgerDate;
                info.VoucherTypeId = 2;
                info.VoucherNo = "55";
                info.LedgerId = 55;
                info.Debit = 0;
                info.DetailsId = 0;
                info.YearId = PublicVariables.CurrentFinicialYearId;
                info.InvoiceNo = productId.ToString();
                info.ChequeNo = "";
                info.ChequeDate = DateTime.Now;
                info.ExtraDate = DateTime.Now;
                info.Extra1 = productId.ToString();
                info.Extra2 = "";

                //Not Used for updating  sp.LedgerPostingAdd(info);

                var context = new DBModel.DBMATAccounting_MagnetEntities1();
                var Leadger = context.tbl_LedgerPosting.Where(u => u.invoiceNo == productId.ToString() && u.extra1 == productId.ToString() && u.ledgerId != 596 && u.ledgerId == 55).FirstOrDefault();
                if (Leadger != null)
                {
                    if (OpeningStock > Leadger.credit)
                    {
                        info.Credit = (Leadger.credit.Value - OpeningStock);
                    }
                    else
                    {
                        info.Debit = (Leadger.credit.Value - OpeningStock);
                    }

                    sp.LedgerPostingAdd(info);
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        public bool PostledgerItems(decimal productId, List<NewStoreModel> stores, DateTime ledgerDate, double VoucherNo)
        {
            try
            {
                string strfinancialId;
                BatchSP spBatch = new BatchSP();
                FinancialYearSP spFinancialYear = new FinancialYearSP();
                FinancialYearInfo infoFinancialYear = new FinancialYearInfo();
                infoFinancialYear = spFinancialYear.FinancialYearViewForAccountLedger(1);
                strfinancialId = infoFinancialYear.FromDate.ToString("dd-MMM-yyyy");
                LedgerPostingInfo info = new LedgerPostingInfo();
                LedgerPostingSP sp = new LedgerPostingSP();
                decimal OpeningStock = 0;

                var infoBatch = new BatchInfo
                {
                    ManufacturingDate = DateTime.Now,
                    ExpiryDate = DateTime.Now,
                    BatchNo = "NA",
                    ProductId = productId,
                    Extra1 = string.Empty,
                    Extra2 = string.Empty,
                    ExtraDate = DateTime.Now,
                    barcode = Convert.ToString(spBatch.AutomaticBarcodeGeneration()),
                    narration = " ",
                    partNo = "0"
                };

                decBatchId = spBatch.BatchAddReturnIdentity(infoBatch);

                foreach (var store in stores)
                {
                    OpeningStock += Convert.ToDecimal(store.Rate * store.Quantity);
                }
            
                info.Date = ledgerDate;
                info.VoucherTypeId = 2;
                info.VoucherNo = VoucherNo.ToString();
                info.LedgerId = 55;
                info.Debit = 0;
                info.Credit = OpeningStock;
                info.DetailsId = 0;
                info.YearId = PublicVariables.CurrentFinicialYearId;
                info.InvoiceNo = productId.ToString();
                info.ChequeNo = "";
                info.ChequeDate = DateTime.Now;
                info.ExtraDate = DateTime.Now;
                info.Extra1 = productId.ToString();
                info.Extra2 = "";
                sp.LedgerPostingAdd(info);

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }


        public bool PostDebiteledgerItems(decimal productId, decimal Amount, int type = 0)
        {
            try
            {

                string strfinancialId;
                FinancialYearSP spFinancialYear = new FinancialYearSP();
                FinancialYearInfo infoFinancialYear = new FinancialYearInfo();
                infoFinancialYear = spFinancialYear.FinancialYearViewForAccountLedger(1);
                strfinancialId = infoFinancialYear.FromDate.ToString("dd-MMM-yyyy");
                LedgerPostingInfo info = new LedgerPostingInfo();
                LedgerPostingSP sp = new LedgerPostingSP();

                info.Date = DateTime.Now;
                info.VoucherTypeId = 2;
                info.VoucherNo = "55";
                info.LedgerId = 596;
                info.Credit = 0;
                info.DetailsId = 0;
                info.YearId = PublicVariables.CurrentFinicialYearId;
                info.InvoiceNo = productId.ToString();
                info.ChequeNo = "";
                info.ChequeDate = DateTime.Now;
                info.ExtraDate = DateTime.Now;
                info.Extra1 = productId.ToString();
                info.Extra2 = "";

                var context = new DBModel.DBMATAccounting_MagnetEntities1();
                var Leadger = context.tbl_LedgerPosting.Where(u => u.invoiceNo == productId.ToString() && u.extra1 == productId.ToString() && u.ledgerId != 596 && u.ledgerId == 55).FirstOrDefault();
                if (Leadger != null)
                {

                    if (type == 0)
                    {

                    }
                    else
                    {

                    }
                    info.Debit = (Leadger.credit.Value - (decimal)Amount);
                    //Leadger.credit = (Leadger.credit + info.Credit);
                    sp.LedgerPostingAdd(info);
                }

                return true;

            }
            catch (Exception ex)
            {
                return false;
            }

        }

        /// <summary>
        /// Function to save batch
        /// </summary>
        public int BatchWithBarCode(ItemModel item, decimal productId)
        {
            try
            {
                BatchSP spBatch = new BatchSP();
                BatchInfo infoBatch = new BatchInfo();
                Int64 inBarcode = spBatch.AutomaticBarcodeGeneration();
                infoBatch.BatchNo = "NA";
                infoBatch.ExpiryDate = DateTime.Now;
                infoBatch.ManufacturingDate = DateTime.Now;
                infoBatch.partNo = item.ProductInfo.PartNo;
                infoBatch.ProductId = productId;
                infoBatch.narration = string.Empty;
                infoBatch.ExtraDate = DateTime.Now;
                if (item.AutoBarcode == false && item.ProductInfo.barcode != "")  // precious
                {
                    infoBatch.barcode = item.ProductInfo.barcode;
                }
                else
                {
                    infoBatch.barcode = Convert.ToString(inBarcode);
                }
                infoBatch.Extra1 = string.Empty;
                infoBatch.Extra2 = string.Empty;
                return inBatchIdWithPartNoNA = spBatch.BatchAddWithBarCode(infoBatch);
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        /// <summary>
        /// Function to add data to tbl_Bom
        /// </summary>
        public int BomTableFill(List<NewBomModel> boms, decimal productId)
        {
            try
            {
                BomInfo infoBom = new BomInfo();
                BomSP spBom = new BomSP();

                foreach (var bom in boms)
                {
                    infoBom = new BomInfo();
                    infoBom.RowmaterialId = bom.RawMaterialId;
                    infoBom.UnitId = bom.UnitId;
                    infoBom.Quantity = bom.Quantity == null || bom.Quantity < 1 ? 0 : bom.Quantity;
                    infoBom.Extra1 = "";
                    infoBom.Extra2 = "";
                    infoBom.ExtraDate = DateTime.Now;
                    if (spBom.BomFromDatatable(infoBom, productId) < 1)    //means a bom item fails to add
                    {
                        return 0;
                    }
                }

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 0;
        }

        /// <summary>
        /// Function to add multiple unit details to tbl_UnitConversion table
        /// </summary>
        public int UnitConvertionTableFill(List<NewMultipleUnitModel> units, decimal productId)
        {
            try
            {
                UnitConvertionSP spUnitConvertion = new UnitConvertionSP();
                UnitConvertionInfo infoUnitConversion = new UnitConvertionInfo();
                foreach (var unit in units)
                {
                    if (unit.ConversionRate != 0)
                    {
                        infoUnitConversion.ConversionRate = unit.ConversionRate;
                        infoUnitConversion.UnitId = unit.UnitId;
                        infoUnitConversion.Quantities = unit.Quantities;
                        infoUnitConversion.Extra1 = unit.Extra1;
                        infoUnitConversion.Extra2 = unit.Extra2;
                        infoUnitConversion.ExtraDate = unit.ExtraDate;
                        infoUnitConversion.ProductId = productId;
                        return spUnitConvertion.UnitConvertionAdd(infoUnitConversion);
                    }
                }
            }
            catch (Exception ex)
            {
                return 0;
            }
            return 0;
        }

        /// <summary>
        /// Function to save data to StockPosting Table
        /// </summary>
        public decimal StockPostingTableFill(List<NewStoreModel> stores, decimal productId, ItemModel item, int Type = 0 , double VoucherNo = 55)
        {
            try
            {
                BatchSP spBatch = new BatchSP();
                StockPostingSP spStockPosting = new StockPostingSP();
                StockPostingInfo infoStockPosting;
                foreach (var store in stores)
                {
                    infoStockPosting = new StockPostingInfo
                    {
                        AgainstInvoiceNo = string.Empty,
                        AgainstVoucherNo = string.Empty,
                        Date = Convert.ToDateTime(item.ProductInfo.EffectiveDate),
                        AgainstVoucherTypeId = 0,
                        InvoiceNo = Convert.ToString(productId),
                        VoucherNo = VoucherNo.ToString(),
                        ProductId = productId,
                        VoucherTypeId = 2,
                        UnitId = Convert.ToDecimal(store.UnitId),
                        InwardQty = Convert.ToDecimal(store.Quantity),
                        OutwardQty = 0,
                        Rate = Convert.ToDecimal(store.Rate),
                        FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId,
                        Extra1 = string.Empty,
                        Extra2 = string.Empty,
                        ExtraDate = DateTime.Now,
                        RackId = store.RackId,
                        GodownId = store.StoreId,
                        ProjectId = 1,
                        CategoryId = 1
                    };

                    //var infoBatch = new BatchInfo
                    // {
                    //     ProductId = productId,
                    //     Extra1 = string.Empty,
                    //     Extra2 = string.Empty,
                    //     ExtraDate = DateTime.Now,
                    //     barcode = Convert.ToString(spBatch.AutomaticBarcodeGeneration()),
                    //     narration = " ", 
                    // };

                    //decBatchId = spBatch.BatchAddReturnIdentity(infoBatch);

                    if (store.RackId == 0)
                    {
                        infoStockPosting.RackId = 0;
                    }

                    if (!item.IsBatch)
                    {
                        infoStockPosting.BatchId = inBatchIdWithPartNoNA;
                    }

                    if (StockPostingAdd(infoStockPosting) < 1)
                    {
                        return 0;
                    }

                    //if (spStockPosting.StockPostingAdd(infoStockPosting) < 1)
                    //{
                    //    return 0;
                    //}
                }

                return 1;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }


        public decimal StockPostingAdd(StockPostingInfo stockpostinginfo)
        {
            //SqlTransaction transaction;
            //if (sqlcon.State == ConnectionState.Closed)
            //{
            //    sqlcon.Open();
            //}
            //SqlCommand command = sqlcon.CreateCommand();
            //transaction = sqlcon.BeginTransaction("LocalTransaction");
            ////command.Connection = sqlcon;
            //command.Transaction = transaction;

            decimal decProductId = 0;
            try
            {
                var context = new DBModel.DBMATAccounting_MagnetEntities1();

                var stockPosting = new DBModel.tbl_StockPosting();

                stockPosting.extra2 = stockpostinginfo.Extra2;
                stockPosting.batchId = stockpostinginfo.BatchId;
                stockPosting.productId = stockpostinginfo.ProductId;
                stockPosting.financialYearId = stockpostinginfo.FinancialYearId;
                stockPosting.extraDate = DateTime.Now;
                stockPosting.extra1 = stockpostinginfo.Extra1;
                stockPosting.rackId = stockpostinginfo.RackId;
                stockPosting.voucherNo = stockpostinginfo.VoucherNo;
                stockPosting.CategoryId = stockpostinginfo.CategoryId;
                stockPosting.ProjectId = stockpostinginfo.ProjectId;
                stockPosting.unitId = stockpostinginfo.UnitId;
                stockPosting.voucherTypeId = stockpostinginfo.VoucherTypeId;
                stockPosting.invoiceNo = stockpostinginfo.InvoiceNo;
                stockPosting.rate = stockpostinginfo.Rate;
                stockPosting.godownId = stockpostinginfo.GodownId;
                stockPosting.date = stockpostinginfo.Date;
                stockPosting.againstInvoiceNo = stockpostinginfo.AgainstInvoiceNo;
                stockPosting.againstVoucherNo = stockpostinginfo.AgainstVoucherNo;
                stockPosting.againstVoucherTypeId = stockpostinginfo.AgainstVoucherTypeId;
                stockPosting.inwardQty = stockpostinginfo.InwardQty;
                stockPosting.outwardQty = stockpostinginfo.OutwardQty;

                context.tbl_StockPosting.Add(stockPosting);
                context.SaveChanges();
                decProductId = stockPosting.stockPostingId;

                return decProductId;

                //if (sqlcon.State == ConnectionState.Closed)
                //{
                //    sqlcon.Open();
                //}

                //SqlCommand sccmd = new SqlCommand("dbo.StockPostingAdd", sqlcon);
                //sccmd.CommandType = CommandType.StoredProcedure;
                //SqlParameter sprmparam = new SqlParameter();
                //sprmparam = sccmd.Parameters.Add("@date", SqlDbType.DateTime);
                //sprmparam.Value = stockpostinginfo.Date;
                //sprmparam = sccmd.Parameters.Add("@voucherTypeId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.VoucherTypeId;
                //sprmparam = sccmd.Parameters.Add("@voucherNo", SqlDbType.VarChar);
                //sprmparam.Value = stockpostinginfo.VoucherNo;
                //sprmparam = sccmd.Parameters.Add("@invoiceNo", SqlDbType.VarChar);
                //sprmparam.Value = stockpostinginfo.InvoiceNo;
                //sprmparam = sccmd.Parameters.Add("@productId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.ProductId;
                //sprmparam = sccmd.Parameters.Add("@batchId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.BatchId;
                //sprmparam = sccmd.Parameters.Add("@unitId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.UnitId;
                //sprmparam = sccmd.Parameters.Add("@godownId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.GodownId;
                //sprmparam = sccmd.Parameters.Add("@rackId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.RackId;
                //sprmparam = sccmd.Parameters.Add("@againstVoucherTypeId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.AgainstVoucherTypeId;
                //sprmparam = sccmd.Parameters.Add("@againstInvoiceNo", SqlDbType.VarChar);
                //sprmparam.Value = stockpostinginfo.AgainstInvoiceNo;
                //sprmparam = sccmd.Parameters.Add("@againstVoucherNo", SqlDbType.VarChar);
                //sprmparam.Value = stockpostinginfo.AgainstVoucherNo;
                //sprmparam = sccmd.Parameters.Add("@inwardQty", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.InwardQty;
                //sprmparam = sccmd.Parameters.Add("@outwardQty", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.OutwardQty;
                //sprmparam = sccmd.Parameters.Add("@rate", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.Rate;
                //sprmparam = sccmd.Parameters.Add("@financialYearId", SqlDbType.Decimal);
                //sprmparam.Value = stockpostinginfo.FinancialYearId;
                //sprmparam = sccmd.Parameters.Add("@extra1", SqlDbType.VarChar);
                //sprmparam.Value = stockpostinginfo.Extra1;
                //sprmparam = sccmd.Parameters.Add("@extra2", SqlDbType.VarChar);
                //sprmparam.Value = stockpostinginfo.Extra2;
                //sprmparam = sccmd.Parameters.Add("@ProjectId", SqlDbType.Int);
                //sprmparam.Value = stockpostinginfo.ProjectId;
                //sprmparam = sccmd.Parameters.Add("@CategoryId", SqlDbType.Int);
                //sprmparam.Value = stockpostinginfo.CategoryId;
                //decProductId = Convert.ToDecimal(sccmd.ExecuteScalar());
                //return sccmd.ExecuteNonQuery();

            }
            catch (Exception ex)
            {
                try
                {

                    //transaction.Rollback();
                }
                catch (Exception ex2)
                {
                    //  MessageBox.Show(ex2.ToString());
                }
            }
            finally
            {
                // sqlcon.Close();
            }

            return decProductId;
        }

        /// <summary>
        /// Function to add data to StockTable With Batch
        /// </summary>
        public decimal BatchTableWithStockAndProductBatchFill(List<NewStoreModel> stores, decimal productId, ItemModel item, int InfoType = 0 , double VoucherNo = 55)
        {
            try
            {
                BatchSP spBatch = new BatchSP();
                BatchInfo infoBatch;
                var spStockPosting = new StockPostingSP();
                StockPostingInfo infoStockPosting;
                var HasBatch = false;

                foreach (var store in stores)
                {
                    infoStockPosting = new StockPostingInfo
                    {

                        AgainstInvoiceNo = string.Empty,
                        AgainstVoucherNo = string.Empty,
                        Date = Convert.ToDateTime(item.ProductInfo.EffectiveDate),
                        AgainstVoucherTypeId = 0,
                        InvoiceNo = Convert.ToString(productId),
                        VoucherNo = VoucherNo.ToString(),
                        ProductId = productId,
                        VoucherTypeId = 2,
                        UnitId = Convert.ToDecimal(store.UnitId),
                        InwardQty = Convert.ToDecimal(store.Quantity),
                        OutwardQty = 0,
                        Rate = Convert.ToDecimal(store.Rate),
                        FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId,
                        Extra1 = string.Empty,
                        Extra2 = string.Empty,
                        ExtraDate = DateTime.Now,
                        RackId = store.RackId,
                        GodownId = store.StoreId,
                        ProjectId = 0,
                        CategoryId = 0,
                    };

                    if(!HasBatch)
                    {
                        infoBatch = new BatchInfo
                        {
                            ManufacturingDate = store.MfD,
                            ExpiryDate = store.ExpD,
                            BatchNo = store.Batch,
                            ProductId = productId,
                            Extra1 = string.Empty,
                            Extra2 = string.Empty,
                            ExtraDate = DateTime.Now,
                            barcode = Convert.ToString(spBatch.AutomaticBarcodeGeneration()),
                            narration = " ",

                        };

                        decBatchId = spBatch.BatchAddReturnIdentity(infoBatch);
                        HasBatch = true;
                    }
                   

                    if (store.RackId == 0)
                    {
                        infoStockPosting.RackId = 0;
                    }
                    else
                    {
                        infoStockPosting.RackId = store.RackId;
                    }
                    if (store.StoreId == 0)
                    {
                        infoStockPosting.GodownId = 1;
                    }
                    else
                    {
                        infoStockPosting.GodownId = store.StoreId;
                    }

                    if (!item.ProductInfo.IsallowBatch)
                    {
                        infoStockPosting.BatchId = 0;
                    }
                    else
                    {
                        infoStockPosting.BatchId = decBatchId;
                    }

                    var chk = StockPostingAdd(infoStockPosting);
                    if (chk < 1)
                    {
                        return 0;
                    }

                    //if (spStockPosting.StockPostingAdd(infoStockPosting) < 1)
                    //{
                    //    return 0;
                    //}

                }

                HasBatch = false;
                return 1;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "PC:18" + ex.Message;
                return 0;
            }
        }
   
    
    
    }
}