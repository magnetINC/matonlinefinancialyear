﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class AccountLedgerDetailsViewModel
    {
        public AccountLedgerInfo AccountLedgerInfo { get; set; }
        public DataSet CustomerAccountDetails { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public AccountGroupInfo AccountGroup { get; set; }
        public DataTable AccountGroupsList { get; set; }
    }
}