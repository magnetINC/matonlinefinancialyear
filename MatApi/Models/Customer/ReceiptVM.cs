﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Customer
{
    public class ReceiptVM
    {
        public decimal TotalPaymentAmount { get; set; }
        public ReceiptMasterInfo ReceiptMasterInfo { get; set; }
        public List<PartyBalanceInfo> PartyBalanceInfo { get; set; }
        public List<ReceiptDetailsInfo> ReceiptDetailsInfo { get; set; }
    }

    public class EditReceiptVM
    {
        public decimal receiptMasterId { get; set; }
        public PartyBalanceInfo PartyBalanceInfo { get; set; }
        public ReceiptDetailsInfo ReceiptDetailsInfo { get; set; }
    }
}