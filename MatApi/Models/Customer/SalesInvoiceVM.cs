﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Customer
{
    public class SalesInvoiceVM
    {
        public string SalesMode { get; set; }
        public SalesMasterInfo SalesMasterInfo { get; set; }
        public List<SalesDetailsInfo> SalesDetailsInfo { get; set; }
        public StockPostingInfo StockPostingInfo { get; set; }
        public List<SalesBillTaxInfo> SalesBillTaxInfo { get; set; }
        public AdditionalCostInfo AdditionalCostInfo { get; set; }
        public PartyBalanceInfo PartyBalanceInfo { get; set; }
        public int PendingSalesMasterId { get; set; }
    }
}