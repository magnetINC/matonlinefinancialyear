﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class RejectionOutVM
    {
        public DateTime Date { get; set; }
        public decimal SupplierId { get; set; }
        public decimal RejectionOutNo { get; set; }
        public decimal MaterialReceiptNo { get; set; }
        public decimal CurrencyId { get; set; }
        public decimal VoucherTypeId { get; set; }
        public string LrNo { get; set; }
        public string TransportCompany { get; set; }
        public string Narration { get; set; }
        public decimal TotalAmount { get; set; }
        public string DoneBy { get; set; }
        public List<RejectionOutLineItems> LineItems { get; set; }
    }

    public class RejectionOutLineItems
    {
        public int SL { get; set; }
        public decimal MaterialReceiptDetailsId { get; set; }
        public decimal ProductId { get; set; }
        public decimal ProductBarcode { get; set; }
        public decimal ProductCode { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitId { get; set; }
        public decimal UnitConversionId { get; set; }
        public decimal StoreId { get; set; }
        public decimal RackId { get; set; }
        public decimal BatchId { get; set; }
        public decimal Rate { get; set; }

    }
}