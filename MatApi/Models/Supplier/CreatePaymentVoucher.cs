﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Supplier
{
    public class CreatePaymentMaster
    {
        public decimal PayMasterId { get; set; }
        public DateTime Date { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        //public decimal FinancialYearId { get; set; }
        public string InvoiceNo { get; set; }
        public decimal LedgerId { get; set; }
        public string Narration { get; set; }
        public decimal TotalAmount { get; set; }

        public string VoucherNo { get; set; }
        public string ChequeNo { get; set; }
        public DateTime ChequeDate { get; set; }
        public string DoneBy { get; set; }
        public decimal userId { get; set; }
        public List<CreatePaymentDetails> PaymentDetails { get; set; }
        public List<CreatePartBalanceInfo> PartyBalances { get; set; }
    }

    public class CreatePaymentDetails
    {
        public decimal Amount { get; set; }
        public decimal GrossAmount { get; set; }
        public decimal ExchangeRateId { get; set; }
        public int ProjectId { get; set; }
        public int CategoryId { get; set; }
        public decimal LedgerId { get; set; }
        public string Memo { get; set; }
        public decimal TaxId { get; set; }
        public decimal TaxAmount { get; set; }
        public string Status { get; set; }
        public string InvoiceNo { get; set; }
    }
    public class CreatePartBalanceInfo
    {
        public decimal Amount { get; set; }
        public decimal LedgerId { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
        public string ReferenceType { get; set; }
        public string VoucherNo { get; set; }
        public string InvoiceNo { get; set; }
        public string AgainstInvoiceNo { get; set; }
        public string AgainstVoucherNo { get; set; }
        public decimal PartyBalanceId { get; set; }
        public decimal VoucherTypeId { get; set; }
        public decimal ExchangeRateId { get; set; }
    }
}