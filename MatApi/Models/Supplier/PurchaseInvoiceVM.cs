﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Supplier
{
    public class PurchaseInvoiceVM
    {
        public string VoucherNumber { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal TotalAdvance { get; set; }
        public string PurchaseModeText { get; set; }
        public string DoneBy { get; set; }

        public PurchaseMasterInfo PurchaseMasterInfo { get; set; }
        public List<PurchaseDetailsInfo> PurchaseDetails { get; set; }
        public List<StockPostingInfo> StockPostingInfo { get; set; }
        public List<LedgerPostingInfo> LedgerPostingInfo { get; set; }
        public List<AdditionalCostInfo> AdditionalCostInfo { get; set; }
        public List<PurchaseBillTaxInfo> PurchaseBillTaxInfo { get; set; }
        public List<PartyBalanceInfo> PartyBalanceInfo { get; set; }
        public List<AdvancePayment> AdvancePayment { get; set; }
        public DebitNoteMasterInfo DebitNoteMasterInfo { get; set; }
        public PartyBalanceInfo DebitNotePartyBalance { get; set; }
        public LedgerPostingInfo DebitNoteLedgerPosting { get; set; }
        public PaymentMasterInfo DebitNotePaymentMaster { get; set; }
        public PaymentDetailsInfo DebitNotePaymentDetails { get; set; }
        public JournalMasterInfo DebitNoteJournalMasterInfo { get; set; }
        public JournalDetailsInfo DebitNoteJournalDetails { get; set; }
        public DebitNoteMasterInfo DebitNoteMasterInfo2 { get; set; }
        public List<CustomProductInfo> ProductInfo { get; set; }
    }

    public class AdvancePayment
    {
        public string VoucherTypeId { get; set; }
        public string Display { get; set; }
        public string VoucherNo { get; set; }
        public decimal Balance { get; set; }
        public string ExchangeRateId { get; set; }
        public string InvoiceNo { get; set; }
        public string AmountToApply { get; set; }
    }

    public class CustomProductInfo
    {
        public ProductInfo ProductInfo { get; set; }
        public decimal NetAmount { get; set; }
    }
}