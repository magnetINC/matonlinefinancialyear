﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MATFinancials;

namespace MatApi.Models
{
    public static class PublicVariables
    {

        public static bool IsObjectInDate(DateTime EffectiveDate, DateTime PublicVariablesFromDate , DateTime PublicVariablesToDate)
        {
            try
            {
                var IsInRightYear = false;
                var effectiveDate = EffectiveDate;
                var ToFinancialYear = PublicVariablesToDate;
                var FromFinancialYear = PublicVariablesFromDate;

                if (/*effectiveDate >= FromFinancialYear &&*/ effectiveDate <= ToFinancialYear)
                {
                    IsInRightYear = true;
                }
                else
                {
                    IsInRightYear = false;
                }

                return IsInRightYear;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static DateTime FromDate; //start of financial year
        public static DateTime ToDate; //end of financial year

        public static string SessionName = "ApplicationSession";
        public class SessionMode
        {
            public DateTime StartDate { set; get; }
            public DateTime EndDate { set; get; } 

            public int financialYearId { set; get; }
            public int UserId { set; get; }
        }
        public static bool CheckIfSessionExit()
        {
            var session = HttpContext.Current.Session;
            if (session != null)
            {
                if (session[SessionName] != null)
                {
                    return true;
                }
                return false;
            }
            return false;
        }

        public static string DataConnectionString { get; set; }
        
        public static void SaveSession(SessionMode sessionMode , string DataConnections)
        {
            var session = HttpContext.Current.Session;

           DataConnectionString = DataConnections;

            if (session == null)
            {
                try
                {

                    if (session == null || session[SessionName] == null)
                    {
                        FromDate = new DateTime(sessionMode.StartDate.Year, sessionMode.StartDate.Month, sessionMode.StartDate.Day);
                        ToDate = new DateTime(sessionMode.EndDate.Year, sessionMode.EndDate.Month, sessionMode.EndDate.Day);

                        CurrentFinicialYearId = sessionMode.financialYearId;
                        CurrentUserId = sessionMode.UserId;
                        //HttpContext.Current.Session.Add(SessionName, sessionMode);
                        return;
                    }
                }
                catch(Exception ex)
                {
                    //HttpContext.Current.Session.Remove(SessionName);
                    //HttpContext.Current.Session.Add(SessionName, sessionMode);
                    return;
                }

                return;
            }
            else
            {
                FromDate = new DateTime(sessionMode.StartDate.Year, sessionMode.StartDate.Month, sessionMode.StartDate.Day);
                ToDate = new DateTime(sessionMode.EndDate.Year, sessionMode.EndDate.Month, sessionMode.EndDate.Day);

                CurrentFinicialYearId = sessionMode.financialYearId;
                CurrentUserId = sessionMode.UserId;
            }
            return;
        }

        static PublicVariables()
        {
            //FromDate = new DateTime(2017, 01, 01);
            //ToDate = new DateTime(2030, 12, 31);
        }

        public static int CurrentFinicialYearId {
            get;
            set;
        }

        public static decimal CurrentUserId { get; set; }
        public static UserInfo CurrentUser { get; set; }
    }
}