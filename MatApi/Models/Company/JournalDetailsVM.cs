﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Company
{
    public class JournalViewVM
    {
        public decimal JournalMasterId { get; set; }
        public string JournalNo { get; set; }
        public decimal UserId { get; set; }
        public string UserFullName { get; set; }
        public string Memo { get; set; }
        public decimal TotalAmount { get; set; }
        public string JournalDate { get; set; }
        public List<CustomJournalDetailsVM> Details { get; set; }
    }

    public class CustomJournalDetailsVM
    {
        public decimal JournalMaster { get; set; }
        public decimal JournalDetails { get; set; }
        public decimal LedgerId { get; set; }
        public string LedgerName { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
        public string Memo { get; set; }
        public DateTime ChequeDate { get; set; }
        public string ChequeNo { get; set; }
    }
}