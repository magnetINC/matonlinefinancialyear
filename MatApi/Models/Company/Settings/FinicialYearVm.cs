﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace MatApi.Models.Company.Settings
{
    public class FinicialYearVm
    {
        [Required(ErrorMessage = "Start Date Beginnings")]
        public DateTime StartDate { get; set; }

        [Required(ErrorMessage = "End Date Beginnings")]
        public DateTime EndDate { get; set; }

        
        public bool includeSign { get; set; }

        public string shortNote { get; set; }

    }
}