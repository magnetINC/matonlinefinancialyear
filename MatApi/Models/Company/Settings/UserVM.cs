﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Company.Settings
{
    public class UserVm
    {
        public int userId { get; set; }
        public string narration { get; set; }
        public bool active { get; set; }
    }
}