﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class MatResponse
    {
        public int ResponseCode { get; set; }
        public string ResponseMessage { get; set; }
        public dynamic Response { get; set; }
    }
}