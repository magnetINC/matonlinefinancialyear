﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models
{
    public class FarmBuildActivitieModel
    {
        public List<JMasterHeader> record = new List<JMasterHeader>();

        public JMasterHeader1 record1 = new JMasterHeader1();
    }

    public class JMasterHeader
    {
        public JMaster StockMaster { get; set; }
        public List<JMasterDetails> StockDetail { get; set; }
    }

    public class JMasterHeader1
    {
        public JMaster StockMaster { get; set; }
        public List<JMasterDetailsHead> StockDetail = new List<JMasterDetailsHead>();
      
    }

    public class AdditionalCts
    {
        public MatApi.DBModel.tbl_AdditionalCost AdditionalCost = new MatApi.DBModel.tbl_AdditionalCost();
        public string AccountLedgerName { get; set; }
        public MatApi.DBModel.tbl_Project Project { get; set; }
        public MatApi.DBModel.tbl_Category Category { get; set; }
    }


    public class JMasterDetailsHead
    {
        public JMasterDetails Stock { get; set; } = new JMasterDetails();
        public List<AdditionalCts> AdditionalCosts = new List<AdditionalCts>();
        public Product Product = new Product();
        public Unit Unit = new Unit();
        public Project Project = new Project();
        public Category Category = new Category();
        public Batch Batch = new Batch();
        public Godown Store = new Godown();
        public Bom BOM = new Bom();
    }

    public  class Bom
    {
        public decimal bomId { get; set; }
        public Nullable<decimal> productId { get; set; }
        public Nullable<decimal> rowmaterialId { get; set; }
        public Nullable<decimal> quantity { get; set; }
        public Nullable<decimal> unitId { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public Nullable<System.DateTime> extraDate { get; set; }
    }
    public  class Godown
    {
        public decimal godownId { get; set; }
        public string godownName { get; set; }
        public string narration { get; set; }
        public Nullable<System.DateTime> extraDate { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }

       
    }
    public class Batch
    {
        public decimal batchId { get; set; }
        public string batchNo { get; set; }
        public Nullable<decimal> productId { get; set; }
        public string barcode { get; set; }
        public string partNo { get; set; }
        public Nullable<System.DateTime> manufacturingDate { get; set; }
        public Nullable<System.DateTime> expiryDate { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public Nullable<System.DateTime> extraDate { get; set; }
        public string narration { get; set; }
    }
    public  class Category
    {
        public int CategoryId { get; set; }
        public string CategoryName { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<int> BelongsToCategoryId { get; set; }
    }
    public  class Project
    {
        public int ProjectId { get; set; }
        public string ProjectName { get; set; }
        public Nullable<int> CreatedBy { get; set; }
        public Nullable<System.DateTime> CreatedOn { get; set; }
        public Nullable<int> ModifiedBy { get; set; }
        public Nullable<System.DateTime> ModifiedOn { get; set; }
        public Nullable<System.DateTime> CloseDate { get; set; }
        public Nullable<int> ProjectOwnerId { get; set; }
    }
    public  class Unit
    {
        public decimal unitId { get; set; }
        public string unitName { get; set; }
        public string narration { get; set; }
        public Nullable<int> noOfDecimalplaces { get; set; }
        public string formalName { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public Nullable<System.DateTime> extraDate { get; set; }
    }
    public class Product
    {
        public decimal productId { get; set; }
        public string productCode { get; set; }
        public string productName { get; set; }
        public Nullable<decimal> groupId { get; set; }
        public Nullable<decimal> brandId { get; set; }
        public Nullable<decimal> unitId { get; set; }
        public Nullable<decimal> sizeId { get; set; }
        public Nullable<decimal> modelNoId { get; set; }
        public Nullable<decimal> taxId { get; set; }
        public string taxapplicableOn { get; set; }
        public Nullable<decimal> purchaseRate { get; set; }
        public Nullable<decimal> salesRate { get; set; }
        public Nullable<decimal> mrp { get; set; }
        public Nullable<decimal> minimumStock { get; set; }
        public Nullable<decimal> maximumStock { get; set; }
        public Nullable<decimal> reorderLevel { get; set; }
        public Nullable<decimal> godownId { get; set; }
        public Nullable<decimal> rackId { get; set; }
        public Nullable<bool> isallowBatch { get; set; }
        public Nullable<bool> ismultipleunit { get; set; }
        public Nullable<bool> isBom { get; set; }
        public Nullable<bool> isopeningstock { get; set; }
        public string narration { get; set; }
        public Nullable<bool> isActive { get; set; }
        public Nullable<bool> isshowRemember { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public Nullable<System.DateTime> extraDate { get; set; }
        public string partNo { get; set; }
        public string productType { get; set; }
        public Nullable<decimal> salesAccount { get; set; }
        public Nullable<System.DateTime> effectiveDate { get; set; }
        public Nullable<decimal> expenseAccount { get; set; }

     
    }
    public class JMaster
    {
        public decimal stockJournalMasterId { get; set; }
        public string voucherNo { get; set; }
        public string invoiceNo { get; set; }
        public decimal suffixPrefixId { get; set; }
        public decimal voucherTypeId { get; set; }
        public System.DateTime date { get; set; }
        public string narration { get; set; }
        public decimal additionalCost { get; set; }
        public decimal financialYearId { get; set; }
        public decimal exchangeRateId { get; set; }
        public System.DateTime extraDate { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
    }
    public class JMasterDetails
    {
        public decimal stockJournalDetailsId { get; set; }
        public   decimal   stockJournalMasterId { get; set; }
        public   decimal   productId { get; set; }
        public   decimal   qty { get; set; }
        public   decimal   rate { get; set; }
        public   decimal   unitId { get; set; }
        public   decimal   unitConversionId { get; set; }
        public   decimal   batchId { get; set; }
        public   decimal   godownId { get; set; }
        public   decimal   rackId { get; set; }
        public   decimal   amount { get; set; }
        public string consumptionOrProduction { get; set; }
        public   int   slno { get; set; }
        public   System.DateTime   extraDate { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public   int   projectId { get; set; }
        public   int   categoryId { get; set; }
    }
}