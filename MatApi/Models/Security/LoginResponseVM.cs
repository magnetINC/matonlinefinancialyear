﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Security
{
    public class LoginResponseVM
    {
        public string UserLogin { get; set; }
        public string Password { get; set; }

        public decimal UserId { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public decimal StoreId { get; set; }
        public string StoreName { get; set; }
        public bool IsActive { get; set; }
        public decimal RoleId { get; set; }
        public string RoleName { get; set; }
        public DateTime DateCreated { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string FinancialYearDisplay { get; set; }

        public int FinancialYear { get; set; }

        public bool IsInCurrentFinancialYear { get; set; }

        public DateTime _dtFromDate { get; set; }
        public DateTime _dtToDate { get; set; }
        public int _decCurrentFinancialYearId { get; set; }

        public int CurrentFinicialId { get; set; }
        public DateTime ToDate { get; set; }
        public DateTime FromDate { get; set; }
        public int CurrentFinicialYearId { get; set; }

        public const string FinanacialYearSession = "FinanacialYearSession";

        public UserInfo userInfo { get; set; }
        
    }
}