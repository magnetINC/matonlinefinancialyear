﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.SupplierReports
{
    public class MaterialReceiptReportVM
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public decimal formType { get; set; }//vouchertype
        public decimal suppliers { get; set; }//ledger
        public string status { get; set; }
        //public string formNo { get; set; }//voucherNo
        public decimal orderNo { get; set; }//orderID
       // public string productCode { get; set; }
    }
}