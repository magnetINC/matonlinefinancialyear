﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.SupplierReports
{
    public class PurchaseReturnReportVM
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }

        public decimal formType { get; set; }

        public decimal supplier { get; set; }

        //public string formNo { get; set; }

        //public decimal invoiceNo { get; set; }

        public string product { get; set; }
    }
}