﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.SupplierReports
{
    public class RejectionOutReportVM
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public decimal supplier { get; set; }
        public decimal formType { get; set; }
        public string formNo { get; set; }
        public string productCode { get; set; }
        public string productName { get; set; }
        public decimal MaterialReceiptNo { get; set; }


    }
}