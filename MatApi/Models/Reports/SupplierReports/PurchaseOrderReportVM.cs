﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.SupplierReports
{
    public class PurchaseOrderReportVM
    {
       // public string voucherNo { get; set; }
        public DateTime fromDate { get; set; }

        public DateTime toDate { get; set; }
        public decimal suppliers { get; set; }

        public decimal voucherType { get; set; }
        public string status { get; set; }
    }
}