﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.SupplierReports
{
    public class PurchaseSummaryVM
    {
        public string forType { get; set; }

        public DateTime fromDate { get; set; }

        public decimal supplier { get; set; }

        public string purchaseMode { get; set; }

        public decimal formType { get; set; }

        public string productCode { get; set; }

        public DateTime toDate { get; set; }

        public string Status { get; set; }

        public string formNo { get; set; }

        public string productName { get; set; }

        public string strColumn { get; set; }

        public string voucherNo { get; set; }

        public string orderNo { get; set; }
    }

}