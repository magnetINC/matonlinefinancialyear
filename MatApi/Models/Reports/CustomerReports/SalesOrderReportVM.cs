﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.CustomerReports
{
    public class SalesOrderReportVM
    {
        public DateTime fromDate { get; set; }

        public DateTime toDate { get; set; }

        public decimal formType { get; set; }

        public decimal customer { get; set; }

        public decimal salesMan { get; set; }

        public string quotationNo { get; set; }

        public decimal productGroup { get; set; }

        public decimal state { get; set; }

        public decimal city { get; set; }
    }
}