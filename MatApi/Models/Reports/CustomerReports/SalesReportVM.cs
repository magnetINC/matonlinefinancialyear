﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.CustomerReports
{
    public class SalesReportVM
    {
        public DateTime fromDate { get; set; }

        public DateTime toDate { get; set; }

        public decimal formType { get; set; }

        public decimal customer { get; set; }

        public decimal state { get; set; }

        public decimal city { get; set; }

        public string salesMode { get; set; }

        public decimal modelNo { get; set; }

        //public decimal salesMan { get; set; }

        public string productName { get; set; }
        public string status { get; set; }
    }
}