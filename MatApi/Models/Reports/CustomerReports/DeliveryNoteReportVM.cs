﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.CustomerReports
{
    public class DeliveryNoteReportVM
    {
        public DateTime fromDate { get; set; }

        public DateTime toDate { get; set; }

        public decimal formType { get; set; }

        public string type { get; set; }
        public decimal typeId { get; set; }

        public decimal salesMan { get; set; }

        public string status { get; set; }

        public string productName { get; set; }

        public decimal customer { get; set; }

        public string orderNo { get; set; }
    }
}