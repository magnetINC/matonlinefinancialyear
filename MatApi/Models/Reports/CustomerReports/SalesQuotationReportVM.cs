﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.CustomerReports
{
    public class SalesQuotationReportVM
    {
        public DateTime fromDate { get; set; }

        public DateTime toDate { get; set; }

        public decimal formType { get; set; }

        public decimal customer { get; set; }

        //public string productCode { get; set; }

        public string productName { get; set; }

        //public string formNo { get; set; }

       // public string status { get; set; }

        public decimal  salesMan { get; set; }
    }
}