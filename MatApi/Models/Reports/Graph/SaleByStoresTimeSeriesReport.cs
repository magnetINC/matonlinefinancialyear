﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.Graph
{
    public class SaleByStoresTimeSeriesReport
    {
        public string StoreName{get;set;}
        public decimal Sale { get; set; }
        public DateTime Date{get;set;}
    }
}