﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.FinancialStatements
{
    public class CustomBankRegisterVM
    {
        public decimal LedgerPostingId { get; set; }
        public decimal LedgerId { get; set; }
        public string LedgerName { get; set; }
        public decimal AccountGroupId { get; set; }
        public string AccountGroupName { get; set; }
        public string VoucherNo { get; set; }
        public string Date { get; set; }
        public decimal VoucherTypeId { get; set; }
        public string VoucherTypeName { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
    }

    public class CustomBankRegisterParam
    {
        public decimal AgentId { get; set; }
        public decimal BankId { get; set; }
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class CustomBankRegisterResponseVM
    {
        public decimal BankId { get; set; }
        public string BankName { get; set; }
        public List<CustomBankRegisterVM> AgentLists { get; set; }
    }
}