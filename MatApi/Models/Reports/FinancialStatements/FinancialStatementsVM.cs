﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Models.Reports.FinancialStatements
{
    public class FinancialStatementsVM
    {

    }

    public class TrialBalanceParam
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class BalanceSheetParam
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }

        public int PreviousYear { get; set; }
}

    public class TrialBalanceResponseVM
    {
        public string AccountGroupId { get; set; }
        public string LedgerName { get; set; }
        public string DebitDifference { get; set; }
        public string CreditDifference { get; set; }
    }
    
    public class BalanceSheetResponseVM
    {
        public string TxtAsset { get; set; }
        public string GroupId1 { get; set; }
        public string GroupId2 { get; set; }
        public string Amount1 { get; set; }
        public string Amount2 { get; set; }
        public string Amount3 { get; set; }
    }
    public class ProfitAndLossParam
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }

    public class ProfitAndLossResponseVM
    {
        public string TxtIncome { get; set; }
        public string TxtGroupId2 { get; set; }
        public string TxtGroupId1 { get; set; }
        public string TxtAmount2 { get; set; }
        public string TxtAmount1 { get; set; }
    }
}