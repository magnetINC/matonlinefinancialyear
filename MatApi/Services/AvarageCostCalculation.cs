﻿
using MatApi.Models.Reports.OtherReports;
using MATFinancials;
using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;
using MatApi.Services;



namespace MatApi.Services
{
   
    public class AvarageCostCalculation
    {
        static bool isOpeningRate = false;
        static List<string> AllItems = new List<string> { };
        static List<decimal> AllStores = new List<decimal> { };
        public static string Calculation(StockReportSearch input)
        {
            var avgCost2 = "";
            StockReportRow master = new StockReportRow();
            List<StockReportRowItems> details = new List<StockReportRowItems>();

            try
            {
                DataTable prodDtbl = new DataTable();
                prodDtbl = new ProductSP().ProductViewAll();
                for (int k = 0; k < prodDtbl.Rows.Count; k++)
                {
                    var FinancialDate = Convert.ToDateTime(prodDtbl.Rows[k]["effectiveDate"].ToString());

                    if (FinancialDate >= input.FromDate && FinancialDate <= input.ToDate)
                    {

                    }

                    AllItems.Add(prodDtbl.Rows[k]["productCode"].ToString());
                }

                AllItems = AllItems.ToList().Distinct().ToList();
                GodownSP spGodown = new GodownSP();
                DataTable storeDtbl = new DataTable();
                storeDtbl = spGodown.GodownViewAll();
                for (int k = 0; k < storeDtbl.Rows.Count; k++)
                {
                    AllStores.Add(Convert.ToDecimal(storeDtbl.Rows[k]["godownId"]));
                }

                AllStores = AllStores.ToList().Distinct().ToList();
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                string calculationMethod = string.Empty;
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = DateTime.UtcNow;
                try
                {
                    lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                }
                catch (Exception ex)
                {

                }

                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    //lastFinYearEnd = input.FromDate.AddDays(-1);
                    lastFinYearEnd = input.ToDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                }
                decimal currentProductId = 0, prevProductId = 0, prevStoreId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0;
                decimal qtyBal = 0, stockValue = 0, OpeningStockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal stockValuePerItemPerStore = 0, totalStockValue = 0;
                decimal inwardQtyPerStore = 0, outwardQtyPerStore = 0, totalQtyPerStore = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "", productName = "", storeName = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };
                DataTable dtbl = new DataTable();
                DataTable dtblItem = new DataTable();
                DataTable dtblStore = new DataTable();
                DataTable dtblOpeningStocks = new DataTable();
                DataTable dtblOpeningStockPerItemPerStore = new DataTable();
                var spstock = new MATFinancials.StockPostingSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                decimal productId = input.ProductId, storeId = input.StoreId;
                string produceCode = input.ProductCode, batch = input.BatchNo, referenceNo = input.RefNo;
                input.ToDate = input.ToDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                //dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text), "", "");
                //dtblOpeningStocks = spstock.StockReportDetailsGridFill(0, 0, "All", PublicVariables._dtFromDate, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), "", "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);
                if (batch != "" || input.ProjectId != 0 || input.CategoryId != 0 || productId != 0 || storeId != 0 || produceCode != "")

                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);
                else
                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);

                if (AllItems.Any())
                {
                    foreach (var item in AllItems)
                    {
                        // ======================================== for each item begins here ====================================== //

                        dtblItem = new DataTable();

                        if (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item).Any())
                        {
                            dtblItem = (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item)).CopyToDataTable();

                            productName = (dtbl.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(m => m.Field<string>("productName")).FirstOrDefault().ToString());
                        }

                        if (productName.ToLower().Contains("CHINCHIN".ToLower()) || productName.ToLower().Equals("CHINCHIN".ToLower()))
                        {

                        }

                        if (dtblItem != null && dtblItem.Rows.Count > 0)
                        {
                            if (AllStores.Any())
                            {
                                foreach (var store in AllStores)
                                {
                                    // =================================== for that item, for each store begins here ============================== //


                                    qtyBal = 0; stockValue = 0;
                                    stockValuePerItemPerStore = 0; //dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    dtblStore = new DataTable();

                                    // ************************ insert opening stock per item per store before other transactions ******************** //
                                    dtblOpeningStockPerItemPerStore = new DataTable();
                                    if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                    {
                                        dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store)).CopyToDataTable();
                                        if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                        {
                                            computedAverageRate = rate2;
                                            var itemParams = methodForStockValue.currentStockValue1235(lastFinYearStart, lastFinYearEnd, true, item, store);
                                            dcOpeningStockForRollOver = itemParams.Item1;
                                            openingQuantity = itemParams.Item2;
                                            openingQuantityIn = itemParams.Item3;
                                            openingQuantityOut = itemParams.Item4;
                                            qtyBal += openingQuantity;
                                            qtyBal = 0;
                                            // qtyBal = openingQuantity;
                                            if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                            {
                                                computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                                stockValue = qtyBal * computedAverageRate;
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = item,
                                                ProductCode = item,
                                                ProductName = productName,
                                                StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                                Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                                InwardQty = openingQuantityIn.ToString("N2"),
                                                OutwardQty = openingQuantityOut.ToString("N2"),
                                                Rate = "",
                                                AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                                QtyBalance = openingQuantity.ToString("N2"),
                                                StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                                VoucherTypeName = "Opening Stock "
                                            });

                                            //dgvStockDetailsReport.Rows[i].Cells["productId"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productName"].Value = productName;
                                            //dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            //Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(); ;
                                            //dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                            //dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                            //dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";

                                            //New Comments
                                            totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            // dcOpeningStockForRollOver;
                                            //totalQtyPerStore += openingQuantity;
                                            //i++;
                                            //Old Comments
                                        }
                                    }
                                    // ************************ insert opening stock per item per store before other transactions end ******************** //

                                    dtblStore = null;

                                    if (dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).Any())
                                    {
                                        dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).CopyToDataTable();
                                    }

                                    if (dtblStore != null && dtblStore.Rows.Count > 0)
                                    {
                                        storeName = (dtblStore.AsEnumerable().Where(m => m.Field<decimal>("godownId") == store).Select(m => m.Field<string>("godownName")).FirstOrDefault().ToString());

                                        foreach (DataRow dr in dtblStore.Rows)
                                        {
                                            // opening stock has been factored in thinking there was no other transaction, now other transactions factored it in again, so remove it
                                            totalStockValue -= Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            //dgvStockDetailsReport.Rows[i-1].DefaultCellStyle.BackColor = Color.White;
                                            //dgvStockDetailsReport.Rows[i-1].Cells["rate"].Value = "";
                                            dcOpeningStockForRollOver = 0;  // reset the opening stock because of the loop

                                            currentProductId = Convert.ToDecimal(dr["productId"]);
                                            productCode = dr["productCode"].ToString();
                                            string voucherType = "", refNo = "";
                                            inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);

                                            decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                                            string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);

                                            var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            if (!isOpeningRate)
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }
                                            else
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }

                                            //if (purchaseRate == 0)
                                            //{
                                            //    purchaseRate = rate2;
                                            //}

                                            if (prevStoreId != store)
                                            {
                                                previousRunningAverage = purchaseRate;
                                            }

                                            if (previousRunningAverage != 0 && currentProductId == prevProductId)
                                            {
                                                purchaseRate = previousRunningAverage;
                                            }

                                            refNo = dr["refNo"].ToString();

                                            if (againstVoucherType != "NA")
                                            {
                                                refNo = dr["againstVoucherNo"].ToString();
                                                isAgainstVoucher = true;
                                                againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                                                string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                                                string typeOfVoucher = conn.getSingleValue(queryString);
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note".ToLower() && typeOfVoucher.ToLower() == "sales invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt".ToLower() && typeOfVoucher.ToLower() == "purchase invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                            }

                                            voucherType = dr["typeOfVoucher"].ToString();

                                            if (voucherType.ToLower() == "Stock Journal".ToLower())
                                            {
                                                //voucherType = "Stock Transfer";
                                                string queryString = string.Format(" SELECT extra1 FROM tbl_StockJournalMaster WHERE voucherNo = '{0}' ", dr["refNo"]);
                                                voucherType = conn.getSingleValue(queryString);
                                            }

                                            if (outwardQty2 > 0)
                                            {
                                                if (voucherType.ToLower() == "Sales Invoice".ToLower() && outwardQty2 > 0)
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate;
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Material Receipt".ToLower())
                                                {
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue -= outwardQty2 * rate2;
                                                }
                                                else if (outwardQty2 > 0 && (voucherType.ToLower() == "Rejection Out".ToLower()) || (voucherType.ToLower() == "Purchase Return".ToLower())
                                                    || (voucherType.ToLower() == "Sales Return".ToLower()) || (voucherType.ToLower() == "Rejection In".ToLower() || voucherType.ToLower() == "Stock Transfer".ToLower()))
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;// 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Sales Order".ToLower())
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else
                                                {
                                                    if (qtyBal != 0)
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Return".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Rejection In".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Invoice".ToLower() && inwardQty2 > 0)
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;

                                                }
                                                else
                                                {
                                                    computedAverageRate = previousRunningAverage;// (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Delivery Note".ToLower())
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Opening Stock".ToLower())
                                            {
                                                // openingQuantityIn = 0;

                                                if (qtyBal != 0)
                                                {
                                                    //computedAverageRate = (stockValue / qtyBal);

                                                    // qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }
                                                else
                                                {
                                                    //computedAverageRate = (stockValue / 1);
                                                    //qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }

                                                OpeningStockValue += (inwardQty2 * rate2);
                                            }
                                            else
                                            {
                                                // hndles other transactions such as purchase, opening balance, etc
                                                qtyBal += inwardQty2 - outwardQty2;
                                                stockValue += (inwardQty2 * rate2);

                                            }

                                            // ------------------------------------------------------ //
                                            //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            string avgCost = "";
                                            if (stockValue != 0 && qtyBal != 0)
                                            {
                                                avgCost = (stockValue / qtyBal).ToString("N2");
                                                //avgCost = (stockValue / qtyBal * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }
                                            else
                                            {
                                                avgCost = computedAverageRate.ToString("N2");
                                                //avgCost = (computedAverageRate/ SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = dr["productId"].ToString(),
                                                ProductCode = dr["productCode"].ToString(),
                                                StoreName = dr["godownName"].ToString(),
                                                InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                                                OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                                                Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                                AverageCost = avgCost,
                                                QtyBalance = qtyBal.ToString("N2"),
                                                StockValue = stockValue.ToString("N2"),
                                                VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType,
                                                RefNo = refNo,
                                                Batch = string.IsNullOrEmpty(dr["Batch"].ToString()) ? "" : dr["Batch"].ToString(),
                                                Date = Convert.ToDateTime(dr["date"]).ToShortDateString(),
                                                ProductName = dr["productName"].ToString(),
                                                ProjectId = Convert.ToDecimal(dr["projectId"]),
                                                CategoryId = Convert.ToDecimal(dr["categoryId"]),
                                                UnitName = dr["unitName"].ToString()
                                            });

                                            isAgainstVoucher = false;

                                            stockValuePerItemPerStore = Convert.ToDecimal(stockValue);
                                            prevProductId = currentProductId;
                                            previousRunningAverage = Convert.ToDecimal(avgCost);
                                            inwardQtyPerStore += Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQtyPerStore += Convert.ToDecimal(dr["outwardQty"]);
                                            i++;
                                        }
                                        // ===================== for every store for that item, put the summary =================== //
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = "",
                                            ProductCode = item,
                                            StoreName = storeName,
                                            InwardQty = (inwardQtyPerStore /*+ openingQuantityIn*/).ToString("N2"),
                                            OutwardQty = ("-") + (outwardQtyPerStore /*+ openingQuantityOut*/).ToString("N2"),
                                            Rate = "",
                                            AverageCost = "",
                                            QtyBalance = qtyBal.ToString("N2"),
                                            StockValue = stockValuePerItemPerStore.ToString("N2"),
                                            VoucherTypeName = "Sub total: ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = "",
                                        });

                                        totalStockValue += Convert.ToDecimal(stockValuePerItemPerStore.ToString("N2"));
                                        totalQtyPerStore += qtyBal /*(inwardQtyPerStore - outwardQtyPerStore) + openingQuantity*/;

                                        openingQuantity = 0;
                                        inwardQtyPerStore = 0;
                                        outwardQtyPerStore = 0;
                                        qtyBal = 0;
                                        stockValue = 0;
                                        stockValuePerItemPerStore = 0;
                                        //dgvStockDetailsReport.Rows.Add();
                                        i++;
                                    }

                                    dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    prevStoreId = store;
                                }
                            }
                        }
                        // *** if no transaction for that item for all stores for that period add the ockock for all stores per store *** //
                        else
                        {
                            foreach (var store in AllStores)
                            {
                                stockValuePerItemPerStore = 0;
                                dtblStore = new DataTable();
                                // ************************ insert opening stock per item per store ******************** //
                                dtblOpeningStockPerItemPerStore = new DataTable();
                                if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                {
                                    dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item)).CopyToDataTable();

                                    if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                    {
                                        computedAverageRate = rate2;
                                        var itemParams = methodForStockValue.currentStockValue1235(lastFinYearStart, lastFinYearEnd, true, item, store);
                                        dcOpeningStockForRollOver = itemParams.Item1;
                                        openingQuantity = itemParams.Item2;
                                        openingQuantityIn = itemParams.Item3;
                                        openingQuantityOut = itemParams.Item4;
                                        qtyBal += openingQuantity;
                                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                        {
                                            //computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                            //stockValue = openingQuantity * computedAverageRate;
                                        }
                                        var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = item,
                                            ProductCode = item,
                                            StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                        Where(l => l.Field<string>("productCode") == item).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                            InwardQty = openingQuantityIn.ToString("N2"),
                                            OutwardQty = openingQuantityOut.ToString("N2"),
                                            Rate = "",
                                            AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                            //AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2") : "",
                                            QtyBalance = openingQuantity.ToString("N2"),
                                            StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                            VoucherTypeName = "Opening Stock ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = ""
                                        });

                                        totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                        dcOpeningStockForRollOver = 0;
                                        totalQtyPerStore += openingQuantity;
                                        openingQuantityIn = 0;
                                        openingQuantityOut = 0;
                                        openingQuantity = 0;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }

                // ************************ Calculate Total **************************** //
                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

             
                if (totalQtyPerStore != 0)
                {
                    avgCost2 = (totalStockValue / totalQtyPerStore).ToString("N2");
                }
                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = avgCost2,
                    QtyBalance = totalQtyPerStore.ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    VoucherTypeName = "Total Stock Value:",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var tyr = OpeningStockValue;
            }

            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }



            master.TotalAverageCost = 0;
            master.TotalQtyBalance = 0;
            master.TotalStockValue = 0;
            master.StockReportRowItem = details;



            return avgCost2;
        }
    }
}