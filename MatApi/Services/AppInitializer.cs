﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using MatApi.DBModel;
using MATFinancials;

namespace MatApi.Services
{
    public static class AppInitializer
    {
        //Initilize Finicial year 
        public static void InitFinicialYear()
        {
            //get company 
            try
            {
                using (var context = new DBMATAccounting_MagnetEntities1())
                {

                    var company = context.tbl_Company.FirstOrDefault();
                    if (company == null) return;
                    var finicialYears = context.tbl_FinancialYear.ToList();
                    var start = DateTime.UtcNow;
                    var end = DateTime.UtcNow.AddMonths(-11);
                    if (!finicialYears.Any())
                    {
                        //create a finicial year 
                        var newYear = new tbl_FinancialYear
                        {
                            fromDate = start,
                            toDate = end
                        };
                        context.tbl_FinancialYear.Add(newYear);
                        context.SaveChanges();
                        //update company 
                        company.currentDate = start;
                        company.extraDate = end;
                        company.extra1 = newYear.financialYearId.ToString();
                        context.Entry(company).State = EntityState.Modified;
                        context.SaveChanges();
                    }
                    company.extra1 = company.extra1 == String.Empty ? "0" : company.extra1;

                    var companyFinicialId = Convert.ToDecimal(company.extra1);
                    var finicialYear = context.tbl_FinancialYear.Where(a => a.financialYearId == companyFinicialId).FirstOrDefault();
                    if (finicialYear != null)
                    {
                        PublicVariables.CurrentFinicialId = (int)finicialYear.financialYearId;
                        PublicVariables._decCurrentFinancialYearId = (int)finicialYear.financialYearId;
                        MatApi.Models.PublicVariables.CurrentFinicialYearId = (int)finicialYear.financialYearId;
                    }
                    PublicVariables._dtFromDate = Convert.ToDateTime(company.currentDate);
                    PublicVariables._dtToDate = Convert.ToDateTime(company.extraDate);
                    MatApi.Models.PublicVariables.ToDate = Convert.ToDateTime(company.extraDate);
                    MatApi.Models.PublicVariables.FromDate = Convert.ToDateTime(company.currentDate);
                }
            }
            catch(Exception ex)
            {

            }
            
        }
    }
}