﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MatApi.DBModel;
using MatApi.Models;
using MatApi.Models.Reports.OtherReports;
using MATClassLibrary.Classes.SP;
using MATFinancials;
using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using PublicVariables = MatApi.Models.PublicVariables;

namespace MatApi.Services
{
    public class FinicialServcies
    {
        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
       
        public async Task<decimal> GetPurchases(DateTime? date, DateTime? from)
        {
            try
            {
                if (date == null)
                {
                    date = DateTime.Now;
                }

                if (from == null)
                {
                    from = DateTime.Now;
                }

                var totalPurchases = await context.tbl_PurchaseMaster.Where(u => u.date >= from && u.date <= date).SumAsync(a => a.totalAmount);

                var totalReturns = await context.tbl_StockPosting
                                    .Where(a => a.voucherTypeId == Constants.PurchaseReturn_VoucherTypeId
                                                && a.date <= date
                                                )
                                    .SumAsync(a => a.outwardQty * a.rate);

                var purchases = Convert.ToDecimal(Convert.ToDecimal(totalPurchases) - Convert.ToDecimal(totalReturns));
                return purchases;
            }
            catch(Exception ex)
            {
                return 0;
            }
           
        }

        public decimal GetOpeningStock1(DateTime? from = null, DateTime? to = null, int finicialYearId = 0)
        {
            decimal openingStock = 0;
            try
            {
                if (finicialYearId == 0)
                {
                    finicialYearId = PublicVariables.CurrentFinicialYearId;
                }
                //&& a.financialYearId == finicialYearId
                var stock = context.tbl_StockPosting
                    .Where(a => a.date >= from && a.date <= to && a.voucherTypeId == 2).ToList();

                var inwardSum = 0.0M;
                var outwardSum = 0.0M;

                foreach (var i in stock)
                {
                    var inwardRate = i.inwardQty.Value * i.rate.Value; //GetCostPrice((int)i.productId);
                    var outwardRate = i.outwardQty.Value * i.rate.Value; //GetCostPrice((int)i.productId);
                    inwardSum += (decimal)inwardRate;
                    outwardSum += (decimal)outwardRate;
                }

                openingStock = inwardSum - outwardSum;

                var openingStockRollOver = GetOpeningStockRollOver(finicialYearId).Result;

                return openingStock /*+ openingStockRollOver*/;
            }
            catch (Exception ex)
            {
                return 0;
            }

            //if (date == null)
            //{
            //    date = DateTime.MaxValue;
            //}
            //decimal dcOpeningStockForRollOver = 0;
            //string calculationMethod = string.Empty;
            //SettingsInfo InfoSettings = new SettingsInfo();
            //SettingsSP SpSettings = new SettingsSP();
            ////GeneralQueries queryForRetainedEarning = new GeneralQueries();
            ////--------------- Selection Of Calculation Method According To Settings ------------------// 
            //if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
            //{
            //    calculationMethod = "FIFO";
            //}
            //else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
            //{
            //    calculationMethod = "Average Cost";
            //}
            //else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
            //{
            //    calculationMethod = "High Cost";
            //}
            //else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
            //{
            //    calculationMethod = "Low Cost";
            //}
            //else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
            //{
            //    calculationMethod = "Last Purchase Rate";
            //}


            //FinancialStatementSP SpFinance1 = new FinancialStatementSP();
            //CurrencyInfo InfoCurrency = new CurrencyInfo();

            ////GeneralQueries methodForStockValue = new GeneralQueries();
            //DBMatConnection conn = new DBMatConnection();
            //int inDecimalPlaces = InfoCurrency.NoOfDecimalPlaces;
            ////decimal dcClosingStock = SpFinance1.StockValueGetOnDate(date, calculationMethod, false, false);
            ////dcClosingStock = Math.Round(dcClosingStock, inDecimalPlaces);
            ////---------------------Opening Stock-----------------------
            ////decimal dcOpeninggStock = SpFinance1.StockValueGetOnDate(PublicVariables._dtFromDate, calculationMethod, true, true);           
            ////string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", MATFinancials.PublicVariables._dtToDate.ToString());
            ////string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", MATFinancials.PublicVariables._dtFromDate.ToString());

            //decimal dcOpeninggStock = SpFinance1.StockValueGetOnDate(Convert.ToDateTime(date), calculationMethod, true, false);
            //dcOpeninggStock = dcOpeninggStock + dcOpeningStockForRollOver;
            //   return dcOpeninggStock;
            //DataRow dr = dtTrial.NewRow();  // to add new row
            //dr["accountGroupId"] = -4;
            //dr["debit"] = 0.0;
            //dr["credit"] = 0.0;
            //dr["creditDifference"] = 0.0;
            //dr["debitDifference"] = dcOpeninggStock;
            //dr["accountGroupName"] = "Stock";
            //dtTrial.Rows.Add(dr); 

        }
        public decimal GetOpeningStock(DateTime? date = null, int finicialYearId = 0)
        {

            try
            {
                if (finicialYearId == 0)
                {
                    finicialYearId = PublicVariables.CurrentFinicialYearId;
                }

                var stock = context.tbl_StockPosting.Where(a => a.voucherTypeId == Constants.OpeningStockVoucherTypeId && a.financialYearId == finicialYearId)
                    .Where(a => a.date <= date).ToList();

                var inwardSum = 0.0M;
                var outwardSum = 0.0M;
                foreach (var i in stock)
                {
                    var inwardRate = i.inwardQty * i.rate.Value;  //GetCostPrice((int)i.productId);
                    var outwardRate = i.outwardQty * i.rate.Value; //GetCostPrice((int)i.productId);
                    inwardSum += (decimal)inwardRate;
                    outwardSum += (decimal)outwardRate;
                }

                var openingStock = inwardSum - outwardSum;

                var openingStockRollOver = GetOpeningStockRollOver(finicialYearId).Result;

                return openingStock + openingStockRollOver;
            }
            catch(Exception ex)
            {
                return 0;
            }

            //if (date == null)
            //{
            //    date = DateTime.MaxValue;
            //}
            //decimal dcOpeningStockForRollOver = 0;
            //string calculationMethod = string.Empty;
            //SettingsInfo InfoSettings = new SettingsInfo();
            //SettingsSP SpSettings = new SettingsSP();
            ////GeneralQueries queryForRetainedEarning = new GeneralQueries();
            ////--------------- Selection Of Calculation Method According To Settings ------------------// 
            //if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
            //{
            //    calculationMethod = "FIFO";
            //}
            //else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
            //{
            //    calculationMethod = "Average Cost";
            //}
            //else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
            //{
            //    calculationMethod = "High Cost";
            //}
            //else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
            //{
            //    calculationMethod = "Low Cost";
            //}
            //else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
            //{
            //    calculationMethod = "Last Purchase Rate";
            //}


            //FinancialStatementSP SpFinance1 = new FinancialStatementSP();
            //CurrencyInfo InfoCurrency = new CurrencyInfo();

            ////GeneralQueries methodForStockValue = new GeneralQueries();
            //DBMatConnection conn = new DBMatConnection();
            //int inDecimalPlaces = InfoCurrency.NoOfDecimalPlaces;
            ////decimal dcClosingStock = SpFinance1.StockValueGetOnDate(date, calculationMethod, false, false);
            ////dcClosingStock = Math.Round(dcClosingStock, inDecimalPlaces);
            ////---------------------Opening Stock-----------------------
            ////decimal dcOpeninggStock = SpFinance1.StockValueGetOnDate(PublicVariables._dtFromDate, calculationMethod, true, true);           
            ////string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", MATFinancials.PublicVariables._dtToDate.ToString());
            ////string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", MATFinancials.PublicVariables._dtFromDate.ToString());

            //decimal dcOpeninggStock = SpFinance1.StockValueGetOnDate(Convert.ToDateTime(date), calculationMethod, true, false);
            //dcOpeninggStock = dcOpeninggStock + dcOpeningStockForRollOver;
         //   return dcOpeninggStock;
            //DataRow dr = dtTrial.NewRow();  // to add new row
            //dr["accountGroupId"] = -4;
            //dr["debit"] = 0.0;
            //dr["credit"] = 0.0;
            //dr["creditDifference"] = 0.0;
            //dr["debitDifference"] = dcOpeninggStock;
            //dr["accountGroupName"] = "Stock";
            //dtTrial.Rows.Add(dr); 

        }

        public async Task<decimal> GetOpeningStockRollOver(int finicialYear)
        {
            try
            {
                var previousYear = GetPreviousFinicialYearId(finicialYear);
                if (previousYear != 0)
                {
                    // previous year exists 
                    var previousFinicialYear = context.tbl_FinancialYear.Find(previousYear);
                    if (previousFinicialYear != null)
                    {
                        // closing stock of last year is an opening stock of the current
                        var openingStockOfLast = await GetClosingStock(Convert.ToDateTime(previousFinicialYear.fromDate),
                            previousFinicialYear.toDate);
                        return openingStockOfLast;
                    }
                }

                return 0;
            }
            catch(Exception ex)
            {
                return 0;
            }

           
        }
        public async Task<decimal> GetClosingStock(DateTime? fromDate , DateTime? date , decimal finicialYearId = 0)
        {
          
            var openingStock = 0.0M;
            try
            {
                if (date == null) date = DateTime.MaxValue;

                if (finicialYearId == 0)
                {
                    finicialYearId = PublicVariables.CurrentFinicialYearId;
                }

                //get all stock posted in via purchase invoice , opening Stock and material reciept  
               var  stocks = await context.tbl_StockPosting.Where(a => a.financialYearId == finicialYearId && (a.date >= fromDate && a.date <= date) && a.voucherTypeId == Constants.OpeningStockVoucherTypeId
                                                                     || a.voucherTypeId == Constants.PurchaseInvoice_VoucherTypeId
                                                                     || a.voucherTypeId == Constants.PurchaseInvoice2_VoucherTypeId
                                                                     || a.voucherTypeId == Constants.MaterialReceiptVoucherTypeId
                                                                     || a.voucherTypeId == Constants.StockJournal_VoucherTypeId).ToListAsync();

                var products = stocks.Select(a => Convert.ToDecimal(a.productId)).Distinct();

               
                foreach (var i in products)
                {
                    var value = Convert.ToDecimal(stocks.Where(a => a.productId == i).Sum(a => a.inwardQty * a.rate));

                    var qty = Convert.ToDecimal(stocks.Where(a => a.productId == i).Sum(a => a.inwardQty));

                    var qtyRemaining = await GetRemainingStockQty(i, fromDate.Value, date.Value, finicialYearId);

                    if (qtyRemaining == 0)
                    {
                        openingStock += 0;
                    }
                    else
                    {
                        if (qty != 0)
                        {
                            var openStockForProduct = (value / qty) * qtyRemaining;
                            openingStock += openStockForProduct;
                        }
                    }
                }
            }
            catch(Exception ex)
            {

            }

            return openingStock;
        }


        public async Task<decimal> GetClosingStock1(DateTime? fromDate, DateTime? date, decimal finicialYearId = 0)
        {

            var ClosingStock = 0.0M;
            try
            {
                if (date == null) date = DateTime.MaxValue;

                if (finicialYearId == 0)
                {
                    finicialYearId = PublicVariables.CurrentFinicialYearId;
                }

                //get all stock posted in via purchase invoice , opening Stock and material reciept  
                var stocks = await context.tbl_StockPosting.Where(a => (a.date.Value >= fromDate && a.date <= date.Value)).ToListAsync();

                var products = stocks.Select(a => Convert.ToDecimal(a.productId)).Distinct();

                foreach (var i in products)
                {
                    var value = stocks.Where(a => a.productId == i).FirstOrDefault();

                    if (value != null)
                    {

                        var RemainingStock = value.inwardQty.Value - value.outwardQty.Value;

                        ClosingStock += (RemainingStock * RemainingStock);
                    }


                }
            }
            catch (Exception ex)
            {

            }

            return ClosingStock;
        }

        public async Task<decimal> GetRemainingStockQty(decimal productId , DateTime from , DateTime to, decimal finicialYearId =0)
        {
            decimal remaining = 0;
            try
            {

                if (finicialYearId == 0)
                {
                    finicialYearId = PublicVariables.CurrentFinicialYearId;
                }

                var stocks = await context.tbl_StockPosting.Where(a => a.financialYearId == finicialYearId &&  a.productId == productId && (a.date >= from && a.date <= to)   && (a.voucherTypeId == Constants.OpeningStockVoucherTypeId
                                                                     || a.voucherTypeId == Constants.PurchaseInvoice_VoucherTypeId
                                                                     || a.voucherTypeId == Constants.PurchaseInvoice2_VoucherTypeId
                                                                     || a.voucherTypeId == Constants.MaterialReceiptVoucherTypeId
                                                                     || a.voucherTypeId == Constants.StockJournal_VoucherTypeId)).ToListAsync();

                // stock coming In 
                var stockIn = stocks.Sum(a => a.inwardQty);
                // stock coming out  
                var stockOut = stocks.Sum(a => a.outwardQty);

                 remaining = Convert.ToDecimal(stockIn - stockOut);
                return remaining;
            }
            catch(Exception ex)
            {
                return remaining;
            }
        }


        public decimal GetTotalStockCost(DateTime date)
        {
           try
            {
                var stock = context.tbl_StockPosting.Where(a => a.voucherTypeId != Constants.OpeningStockVoucherTypeId)
               .Where(a => a.date <= date).ToList();

                var inwardSum = 0.0M;
                var outwardSum = 0.0M;
                foreach (var i in stock)
                {
                    var inwardRate = i.inwardQty * GetCostPrice((int)i.productId);
                    var outwardRate = i.outwardQty * GetCostPrice((int)i.productId);
                    inwardSum += (decimal)inwardRate;
                    outwardSum += (decimal)outwardRate;
                }
                return inwardSum - outwardSum;
            }
            catch(Exception ex)
            {
                return 0;
            }

          
        }

        public decimal GetCostPrice(int productId)
        {
            var p = context.tbl_Product.Where(a => a.productId == productId).FirstOrDefault();
            if (p != null)
            {
                return (decimal)p.purchaseRate;
            }

            return 0.0M;
        }

        public async Task<decimal> GetRemainingStockQty()
        {

            var stocks = await context.tbl_StockPosting
                .ToListAsync();

            // stock coming In 
            var stockIn = stocks.Sum(a => a.inwardQty);
            // stock coming out  
            var stockOut = stocks.Sum(a => a.outwardQty);

            var remaining = Convert.ToDecimal(stockIn - stockOut);
            return remaining;
        }

        public decimal GetRemainingStock(DateTime to)
        {
            var inventory =  GetStockValueInTime(DateTime.Now, to , null).TotalStockValue;
            if(inventory == string.Empty)
            {
                return 0.0M;
            }
            //var inventory = GetOpeningStock(to) + GetTotalStockCost(to);
            return Convert.ToDecimal(inventory);
        }


        public decimal GetRemainingStock(DateTime from,  DateTime to)
        {
            var inventory = GetStockValueInTime(from, to, null).TotalStockValue;
            if (inventory == string.Empty)
            {
                return 0.0M;
            }
            //var inventory = GetOpeningStock(to) + GetTotalStockCost(to);
            return Convert.ToDecimal(inventory);
        }



        public decimal GetRemainingStockFinYear(DateTime from, DateTime to, List<DatePickerModel> SelectedFinanacialYearList)
        {
            var inventory = GetStockValueInTime(from, to, SelectedFinanacialYearList).TotalStockValue;
            if (inventory == string.Empty)
            {
                return 0.0M;
            }
            //var inventory = GetOpeningStock(to) + GetTotalStockCost(to);
            return Convert.ToDecimal(inventory);
        }


        public class GetStockValue
        {
           public string TotalStockValue { get; set; } = "";
           public  List<StockSummary> StockSummary { get; set; }
        }

        public GetStockValue GetStockValueInTime(DateTime from, DateTime to, List<DatePickerModel> datePickers)
        {
            List<StockSummary> stockSummary = new List<StockSummary>();
            GetStockValue response = new GetStockValue();

            decimal returnValue = 0;

            try
            {
                if(datePickers!=null && datePickers.Count > 0)
                {

                    DBMatConnection conn = new DBMatConnection();
                    StockPostingSP spstock = new StockPostingSP();
                    DataTable dtbl = new DataTable();
                    decimal productId = 0;
                    string pCode = "";
                    decimal storeId = 0;
                    string batch = "All";
                    to = to.AddSeconds(-1);

                    string queryStr = "select top 1 f.fromDate from tbl_FinancialYear f";
                    DateTime lastFinYearStart = Convert.ToDateTime(conn.getSingleValue(queryStr));

                    //dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime("04/30/2017") /*PublicVariables._dtToDate*/, pCode, "");
                    var spstk = new MATFinancials.StockPostingSP();
                    dtbl = spstk.StockReportDetailsGridFill(productId, storeId, batch, from, to, pCode, "", 0, 0);

                    decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                    decimal prevProductId = 0;
                    decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                    string productName = "", storeName = "", productCode = "";
                    decimal qtyBal = 0, stockValue = 0;
                    decimal computedAverageRate = 0, previousRunningAverage = 0;
                    decimal totalAssetValue = 0, qtyBalance = 0;
                    decimal value1 = 0;
                    int i = 0;
                    bool isAgainstVoucher = false;

                    foreach (DataRow dr in dtbl.Rows)
                    {
                        currentProductId = Convert.ToDecimal(dr["productId"]);
                        currentGodownID = Convert.ToDecimal(dr["godownId"]);
                        productCode = dr["productCode"].ToString();
                        string voucherType = "", refNo = "";

                        decimal inwardQty = (from p in dtbl.AsEnumerable()
                                             where p.Field<decimal>("productId") == prevProductId
                                             select p.Field<decimal>("inwardQty")).Sum();
                        decimal outwardQty = (from p in dtbl.AsEnumerable()
                                              where p.Field<decimal>("productId") == prevProductId
                                              select p.Field<decimal>("outwardQty")).Sum();

                        inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                        outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                        decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                        string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                        rate2 = Convert.ToDecimal(dr["rate"]);
                        if (previousRunningAverage != 0 && currentProductId == prevProductId)
                        {
                            purchaseRate = previousRunningAverage;
                        }
                        if (currentProductId == prevProductId)
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            prevProductId = Convert.ToDecimal(dr["productId"]);
                            productCode = dr["productCode"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        else
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        //======================== 2017-02-27 =============================== //
                        //if (againstVoucherType != "NA")
                        //{
                        //    voucherType = againstVoucherType;
                        //}
                        //else
                        //{
                        //    voucherType = dr["typeOfVoucher"].ToString();
                        //}
                        refNo = dr["refNo"].ToString();
                        if (againstVoucherType != "NA")
                        {
                            refNo = dr["againstVoucherNo"].ToString();
                            isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                            againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                            //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                            string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                            string typeOfVoucher = conn.getSingleValue(queryString);
                            if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                            if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                        }
                        voucherType = dr["typeOfVoucher"].ToString();
                        //---------  COMMENT END ----------- //
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if (outwardQty2 > 0)
                        {
                            if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = purchaseRate;
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                            {
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue -= outwardQty2 * rate2;
                            }

                            else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Sales Order")
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else
                            {
                                if (qtyBal != 0)
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                        }
                        else if (voucherType == "Sales Return" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Rejection In" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;

                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            // voucherType = "Sales Return";
                        }
                        else if (voucherType == "Delivery Note")
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                        }
                        else
                        {
                            // hndles other transactions such as purchase, opening balance, etc
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += (inwardQty2 * rate2);
                        }
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            //qtyBal = Convert.ToDecimal(dr["inwardQty"]);
                            //stockValue = inwardQty2 * rate2;
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            if (voucherType == "Sales Invoice")
                            {
                                stockValue = qtyBal * purchaseRate;
                            }
                            else
                            {
                                stockValue = qtyBal * rate2;
                            }
                            //totalAssetValue += Math.Round(value1, 2);
                            totalAssetValue += value1;
                            //i++;
                            returnValue = totalAssetValue;
                        }
                        // -----------------added 2017-02-21 -------------------- //
                        if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            if (voucherType == "Sales Invoice")
                            {
                                computedAverageRate = purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = rate2;
                            }
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            stockValue = qtyBal * computedAverageRate;
                        }
                        // ------------------------------------------------------ //

                        previousRunningAverage = computedAverageRate;

                        value1 = stockValue;
                        prevProductId = currentProductId;
                        prevGodownID = currentGodownID;

                        if (i == dtbl.Rows.Count - 1)
                        {
                            var avgCost = "";
                            if (value1 != 0 && (inwardqt - outwardqt) != 0)
                            {
                                avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                            }
                            else
                            {
                                avgCost = "0.0";
                            }

                            stockSummary.Add(new StockSummary
                            {
                                ProductId = dr["productId"].ToString(),
                                StoreId = dr["godownId"].ToString(),
                                ProductName = dr["productName"].ToString(),
                                StoreName = dr["godownName"].ToString(),
                                PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                                SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                QtyBalance = (inwardqt - outwardqt).ToString(),
                                AvgCostValue = avgCost,
                                StockValue = value1.ToString("N2"),
                                ProductCode = dr["productCode"].ToString(),
                                UnitName = string.IsNullOrEmpty(dr["unitName"].ToString()) ? "" : dr["unitName"].ToString()
                            });
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                        {
                            var avgCost = "";
                            if (value1 != 0 && (inwardqt - outwardqt) != 0)
                            {
                                avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                            }
                            else
                            {
                                avgCost = "0.0";
                            }

                            stockSummary.Add(new StockSummary
                            {
                                ProductId = dr["productId"].ToString(),
                                StoreId = dr["godownId"].ToString(),
                                ProductName = dr["productName"].ToString(),
                                StoreName = dr["godownName"].ToString(),
                                PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                                SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                QtyBalance = (inwardqt - outwardqt).ToString(),
                                AvgCostValue = avgCost,
                                StockValue = value1.ToString("N2"),
                                ProductCode = dr["productCode"].ToString(),
                                UnitName = string.IsNullOrEmpty(dr["unitName"].ToString()) ? "" : dr["unitName"].ToString()
                            });
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        i++;
                    }


                    decimal totalStockValue = 0;
                    foreach (var row in stockSummary)
                    {
                        totalStockValue += Convert.ToDecimal(row.StockValue);
                    }

                    stockSummary.Add(new StockSummary
                    {
                        ProductName = "",
                        StoreName = "Total Stock Value:",
                        PurchaseRate = "",
                        SalesRate = "",
                        QtyBalance = qtyBalance.ToString("N2"),
                        AvgCostValue = (totalStockValue / qtyBalance).ToString("N2"),
                        StockValue = totalStockValue.ToString("N2"),
                        ProductCode = ""
                    });

                    response.TotalStockValue = totalStockValue.ToString("N2");
                    response.StockSummary = stockSummary;







                }
                else
                {

                    DBMatConnection conn = new DBMatConnection();
                    StockPostingSP spstock = new StockPostingSP();
                    DataTable dtbl = new DataTable();
                    decimal productId = 0;
                    string pCode = "";
                    decimal storeId = 0;
                    string batch = "All";
                    to = to.AddSeconds(-1);


                 string queryStr = "select top 1 f.fromDate from tbl_FinancialYear f";
                    DateTime lastFinYearStart = Convert.ToDateTime(conn.getSingleValue(queryStr));
                 lastFinYearStart = from;
                  //dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime("04/30/2017") /*PublicVariables._dtToDate*/, pCode, "");
                  var spstk = new MATFinancials.StockPostingSP();
                    dtbl = spstk.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, to, pCode, "", 0, 0);

                    decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                    decimal prevProductId = 0;
                    decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                    string productName = "", storeName = "", productCode = "";
                    decimal qtyBal = 0, stockValue = 0;
                    decimal computedAverageRate = 0, previousRunningAverage = 0;
                    decimal totalAssetValue = 0, qtyBalance = 0;
                    decimal value1 = 0;
                    int i = 0;
                    bool isAgainstVoucher = false;

                    foreach (DataRow dr in dtbl.Rows)
                    {
                        currentProductId = Convert.ToDecimal(dr["productId"]);
                        currentGodownID = Convert.ToDecimal(dr["godownId"]);
                        productCode = dr["productCode"].ToString();
                        string voucherType = "", refNo = "";

                        decimal inwardQty = (from p in dtbl.AsEnumerable()
                                             where p.Field<decimal>("productId") == prevProductId
                                             select p.Field<decimal>("inwardQty")).Sum();
                        decimal outwardQty = (from p in dtbl.AsEnumerable()
                                              where p.Field<decimal>("productId") == prevProductId
                                              select p.Field<decimal>("outwardQty")).Sum();

                        inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                        outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);

                        decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                        string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                        rate2 = Convert.ToDecimal(dr["rate"]);
                        if (previousRunningAverage != 0 && currentProductId == prevProductId)
                        {
                            purchaseRate = previousRunningAverage;
                        }
                        if (currentProductId == prevProductId)
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            prevProductId = Convert.ToDecimal(dr["productId"]);
                            productCode = dr["productCode"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        else
                        {
                            productName = dr["productName"].ToString();
                            storeName = dr["godownName"].ToString();
                            inwardqt = (from p in dtbl.AsEnumerable()
                                        where p.Field<decimal>("productId") == currentProductId
                                         && p.Field<decimal>("godownId") == currentGodownID
                                        select p.Field<decimal>("inwardQty")).Sum();

                            outwardqt = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == currentProductId
                                          && p.Field<decimal>("godownId") == currentGodownID
                                         select p.Field<decimal>("outwardQty")).Sum();
                        }
                        //======================== 2017-02-27 =============================== //
                        //if (againstVoucherType != "NA")
                        //{
                        //    voucherType = againstVoucherType;
                        //}
                        //else
                        //{
                        //    voucherType = dr["typeOfVoucher"].ToString();
                        //}
                        refNo = dr["refNo"].ToString();
                        if (againstVoucherType != "NA")
                        {
                            refNo = dr["againstVoucherNo"].ToString();
                            isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                            againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                            //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                            string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                            string typeOfVoucher = conn.getSingleValue(queryString);
                            if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                            if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                            {
                                againstVoucherType = dr["typeOfVoucher"].ToString();
                            }
                        }
                        voucherType = dr["typeOfVoucher"].ToString();
                        //---------  COMMENT END ----------- //
                        if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        if (outwardQty2 > 0)
                        {
                            if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = purchaseRate;
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                            {
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue -= outwardQty2 * rate2;
                            }

                            else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else if (outwardQty2 > 0 && voucherType == "Sales Order")
                            {
                                if (qtyBal == 0)
                                {
                                    computedAverageRate = (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * 1;
                                }
                                else
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                            else
                            {
                                if (qtyBal != 0)
                                {
                                    computedAverageRate = (stockValue / qtyBal);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                                else
                                {
                                    computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                    qtyBal += inwardQty2 - outwardQty2;
                                    stockValue = computedAverageRate * qtyBal;
                                }
                            }
                        }
                        else if (voucherType == "Sales Return" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Rejection In" && inwardQty2 > 0)
                        {   //added 18112016
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;

                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            // voucherType = "Sales Return";
                        }
                        else if (voucherType == "Delivery Note")
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue += inwardQty2 * purchaseRate;
                            }
                        }
                        else if (voucherType == "Stock Journal")
                        {
                            voucherType = "Stock Transfer";
                        }
                        else if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            //qtyBal = Convert.ToDecimal(dr["inwardQty"]);
                            //stockValue = inwardQty2 * rate2;
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            if (voucherType == "Sales Invoice")
                            {
                                stockValue = qtyBal * purchaseRate;
                            }
                            else
                            {
                                stockValue = qtyBal * rate2;
                            }
                            //totalAssetValue += Math.Round(value1, 2);
                            totalAssetValue += value1;
                            //i++;
                            returnValue = totalAssetValue;
                        }
                        // -----------------added 2017-02-21 -------------------- //
                       else if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                        {
                            if (voucherType == "Sales Invoice")
                            {
                                computedAverageRate = purchaseRate;
                            }
                            else
                            {
                                computedAverageRate = rate2;
                            }
                            qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                            stockValue = qtyBal * computedAverageRate;
                        }
                        else
                        {
                            // hndles other transactions such as purchase, opening balance, etc
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += (inwardQty2 * rate2);
                        }
                        // ------------------------------------------------------ //

                        previousRunningAverage = computedAverageRate;

                        value1 = stockValue;
                        prevProductId = currentProductId;
                        prevGodownID = currentGodownID;

                        if (i == dtbl.Rows.Count - 1)
                        {
                            var avgCost = "";
                            if (value1 != 0 && (inwardqt - outwardqt) != 0)
                            {
                                avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                            }
                            else
                            {
                                avgCost = "0.0";
                            }

                            stockSummary.Add(new StockSummary
                            {
                                ProductId = dr["productId"].ToString(),
                                StoreId = dr["godownId"].ToString(),
                                ProductName = dr["productName"].ToString(),
                                StoreName = dr["godownName"].ToString(),
                                PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                                SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                QtyBalance = (inwardqt - outwardqt).ToString(),
                                AvgCostValue = avgCost,
                                StockValue = value1.ToString("N2"),
                                ProductCode = dr["productCode"].ToString(),
                                UnitName = string.IsNullOrEmpty(dr["unitName"].ToString()) ? "" : dr["unitName"].ToString()
                            });
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                        {
                            var avgCost = "";
                            if (value1 != 0 && (inwardqt - outwardqt) != 0)
                            {
                                avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                            }
                            else
                            {
                                avgCost = "0.0";
                            }

                            stockSummary.Add(new StockSummary
                            {
                                ProductId = dr["productId"].ToString(),
                                StoreId = dr["godownId"].ToString(),
                                ProductName = dr["productName"].ToString(),
                                StoreName = dr["godownName"].ToString(),
                                PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                                SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                QtyBalance = (inwardqt - outwardqt).ToString(),
                                AvgCostValue = avgCost,
                                StockValue = value1.ToString("N2"),
                                ProductCode = dr["productCode"].ToString(),
                                UnitName = string.IsNullOrEmpty(dr["unitName"].ToString()) ? "" : dr["unitName"].ToString()
                            });
                            qtyBalance += (inwardqt - outwardqt);
                        }
                        i++;
                    }


                    decimal totalStockValue = 0;
                    foreach (var row in stockSummary)
                    {
                        totalStockValue += Convert.ToDecimal(row.StockValue);
                    }

                    stockSummary.Add(new StockSummary
                    {
                        ProductName = "",
                        StoreName = "Total Stock Value:",
                        PurchaseRate = "",
                        SalesRate = "",
                        QtyBalance = qtyBalance.ToString("N2"),
                        AvgCostValue = (totalStockValue / qtyBalance).ToString("N2"),
                        StockValue = totalStockValue.ToString("N2"),
                        ProductCode = ""
                    });

                    response.TotalStockValue = totalStockValue.ToString("N2");
                    response.StockSummary = stockSummary;

                }


            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKSR1:" + ex.Message;
            }

            return response;
        }


        public async Task<decimal> GetProfitLoss(DateTime from, DateTime? to = null)
        {
            var ledgerPosting = await context.tbl_LedgerPosting.Include(a => a.tbl_AccountLedger)
              .Include(a => a.tbl_AccountLedger.tbl_AccountGroup).Where(a => a.date >= from && a.date <= to)
              //.Where(a => a.tbl_AccountLedger.tbl_AccountGroup.nature.ToLower() == Constants.IncomeAccount.ToLower()
              //            &&
              //            a.tbl_AccountLedger.tbl_AccountGroup.nature.ToLower() == Constants.ExpenseAccount.ToLower())
              .ToListAsync();



            var totalSalesIncome = 0.0m;
            
            var directExpenses = 0.0m;
            var otherIncome = 0.0m;
            var indirectExpense = 0.0m;
            var otherExpense = 0.0m;

            var grossProfit = 0.0M;
            var operatingProfit = 0.0M;
            var netProfit = 0.0M;

            foreach (var i in ledgerPosting)
            {
                var thisGroupId = i.tbl_AccountLedger.accountGroupId;
                if (thisGroupId == AccountGroupConstants.SalesIncome)
                {
                    //Sales Income 
                    totalSalesIncome += Convert.ToDecimal(i.credit - i.debit);
                }

            
                if (thisGroupId == AccountGroupConstants.DirectExpenses)
                {
                    directExpenses += Convert.ToDecimal(i.credit + i.debit);
                }

                if (thisGroupId == AccountGroupConstants.OtherIncome)
                {
                    otherIncome += Convert.ToDecimal(i.credit + i.debit);
                }   

                if (thisGroupId == AccountGroupConstants.Expenses)
                {
                    indirectExpense += Convert.ToDecimal(i.credit + i.debit);
                }

            }

            decimal openingStock = 0;  // GetOpeningStock(to);

            var purchases = await GetPurchases(to, from);
            decimal closingStock = 0; // await GetClosingStock(from, to);

            var AskOpenStock = GetOpeningStockers(new StockReportSearch() { BatchNo = "All", CategoryId = 0, ProductCode = "", ProductId = 0, ProjectId = 0, RefNo = "", StoreId = 0, FromDate = from, ToDate = to.Value });

            if (AskOpenStock != null)
            {
                closingStock = AskOpenStock.totalStockValue;
                openingStock = AskOpenStock.OpeningStockValue;
            }

            grossProfit = (totalSalesIncome ) -
                          (directExpenses + otherExpense + openingStock + purchases - closingStock);

            operatingProfit = grossProfit + otherIncome - indirectExpense;
            netProfit = operatingProfit - otherExpense;
            return netProfit;
        }

        public dynamic GetOpeningStockers(StockReportSearch input)
        {

            decimal totalStockValue1 = 0;
            List<string> AllItems = new List<string> { };
            List<decimal> AllStores = new List<decimal> { };
            StockReportRow master = new StockReportRow();
            List<StockReportRowItems> details = new List<StockReportRowItems>();
            decimal qtyBal = 0, stockValue = 0, OpeningStockValue = 0;
            try
            {
                DataTable prodDtbl = new DataTable();
                prodDtbl = new ProductSP().ProductViewAll();
                for (int k = 0; k < prodDtbl.Rows.Count; k++)
                {
                    var FinancialDate = Convert.ToDateTime(prodDtbl.Rows[k]["effectiveDate"].ToString());

                    if (FinancialDate >= input.FromDate && FinancialDate <= input.ToDate)
                    {

                    }

                    AllItems.Add(prodDtbl.Rows[k]["productCode"].ToString());
                }

                AllItems = AllItems.ToList().Distinct().ToList();
                GodownSP spGodown = new GodownSP();
                DataTable storeDtbl = new DataTable();
                storeDtbl = spGodown.GodownViewAll();
                for (int k = 0; k < storeDtbl.Rows.Count; k++)
                {
                    AllStores.Add(Convert.ToDecimal(storeDtbl.Rows[k]["godownId"]));
                }

                AllStores = AllStores.ToList().Distinct().ToList();
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                string calculationMethod = string.Empty;
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", MATFinancials.PublicVariables._dtToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", MATFinancials.PublicVariables._dtFromDate.ToString());
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : MATFinancials.PublicVariables._dtFromDate;

                DateTime lastFinYearEnd = DateTime.UtcNow;
                try
                {
                    lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : MATFinancials.PublicVariables._dtFromDate;
                }
                catch (Exception ex)
                {

                }

                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    //lastFinYearEnd = input.FromDate.AddDays(-1);
                    lastFinYearEnd = input.ToDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                }
                decimal currentProductId = 0, prevProductId = 0, prevStoreId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0;

                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal stockValuePerItemPerStore = 0, totalStockValue = 0;
                decimal inwardQtyPerStore = 0, outwardQtyPerStore = 0, totalQtyPerStore = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "", productName = "", storeName = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };
                DataTable dtbl = new DataTable();
                DataTable dtblItem = new DataTable();
                DataTable dtblStore = new DataTable();
                DataTable dtblOpeningStocks = new DataTable();
                DataTable dtblOpeningStockPerItemPerStore = new DataTable();
                var spstock = new MATFinancials.StockPostingSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                decimal productId = input.ProductId, storeId = input.StoreId;
                string produceCode = input.ProductCode, batch = input.BatchNo, referenceNo = input.RefNo;
                input.ToDate = input.ToDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                //dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text), "", "");
                //dtblOpeningStocks = spstock.StockReportDetailsGridFill(0, 0, "All", PublicVariables._dtFromDate, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), "", "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);
                if (batch != "" || input.ProjectId != 0 || input.CategoryId != 0 || productId != 0 || storeId != 0 || produceCode != "")


                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);
                else
                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);


                if (AllItems.Any())
                {
                    foreach (var item in AllItems)
                    {
                        // ======================================== for each item begins here ====================================== //

                        dtblItem = new DataTable();

                        if (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item).Any())
                        {
                            dtblItem = (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item)).CopyToDataTable();

                            productName = (dtbl.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(m => m.Field<string>("productName")).FirstOrDefault().ToString());
                        }

                        if (productName.ToLower().Contains("CHINCHIN".ToLower()) || productName.ToLower().Equals("CHINCHIN".ToLower()))
                        {

                        }

                        if (dtblItem != null && dtblItem.Rows.Count > 0)
                        {
                            if (AllStores.Any())
                            {
                                foreach (var store in AllStores)
                                {
                                    // =================================== for that item, for each store begins here ============================== //


                                    qtyBal = 0; stockValue = 0;
                                    stockValuePerItemPerStore = 0; //dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    dtblStore = new DataTable();

                                    // ************************ insert opening stock per item per store before other transactions ******************** //
                                    dtblOpeningStockPerItemPerStore = new DataTable();
                                    if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                    {
                                        dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store)).CopyToDataTable();
                                        if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                        {
                                            computedAverageRate = rate2;
                                            var itemParams = methodForStockValue.currentStockValue1235(lastFinYearStart, lastFinYearEnd, true, item, store);
                                            dcOpeningStockForRollOver = itemParams.Item1;
                                            openingQuantity = itemParams.Item2;
                                            openingQuantityIn = itemParams.Item3;
                                            openingQuantityOut = itemParams.Item4;
                                            qtyBal += openingQuantity;
                                            qtyBal = 0;
                                            // qtyBal = openingQuantity;
                                            if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                            {
                                                computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                                stockValue = qtyBal * computedAverageRate;
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = item,
                                                ProductCode = item,
                                                ProductName = productName,
                                                StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                                Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                                InwardQty = openingQuantityIn.ToString("N2"),
                                                OutwardQty = openingQuantityOut.ToString("N2"),
                                                Rate = "",
                                                AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                                QtyBalance = openingQuantity.ToString("N2"),
                                                StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                                VoucherTypeName = "Opening Stock "
                                            });

                                            //dgvStockDetailsReport.Rows[i].Cells["productId"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productName"].Value = productName;
                                            //dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            //Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(); ;
                                            //dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                            //dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                            //dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";

                                            //New Comments
                                            totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            // dcOpeningStockForRollOver;
                                            //totalQtyPerStore += openingQuantity;
                                            //i++;
                                            //Old Comments
                                        }
                                    }
                                    // ************************ insert opening stock per item per store before other transactions end ******************** //

                                    dtblStore = null;

                                    if (dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).Any())
                                    {
                                        dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).CopyToDataTable();
                                    }

                                    if (dtblStore != null && dtblStore.Rows.Count > 0)
                                    {
                                        storeName = (dtblStore.AsEnumerable().Where(m => m.Field<decimal>("godownId") == store).Select(m => m.Field<string>("godownName")).FirstOrDefault().ToString());

                                        foreach (DataRow dr in dtblStore.Rows)
                                        {
                                            // opening stock has been factored in thinking there was no other transaction, now other transactions factored it in again, so remove it
                                            totalStockValue -= Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            //dgvStockDetailsReport.Rows[i-1].DefaultCellStyle.BackColor = Color.White;
                                            //dgvStockDetailsReport.Rows[i-1].Cells["rate"].Value = "";
                                            dcOpeningStockForRollOver = 0;  // reset the opening stock because of the loop

                                            currentProductId = Convert.ToDecimal(dr["productId"]);
                                            productCode = dr["productCode"].ToString();
                                            string voucherType = "", refNo = "";
                                            inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);

                                            decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                                            string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);

                                            var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            bool isOpeningRate = false;

                                            if (false)
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }
                                            else
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }

                                            //if (purchaseRate == 0)
                                            //{
                                            //    purchaseRate = rate2;
                                            //}

                                            if (prevStoreId != store)
                                            {
                                                previousRunningAverage = purchaseRate;
                                            }

                                            if (previousRunningAverage != 0 && currentProductId == prevProductId)
                                            {
                                                purchaseRate = previousRunningAverage;
                                            }

                                            refNo = dr["refNo"].ToString();

                                            if (againstVoucherType != "NA")
                                            {
                                                refNo = dr["againstVoucherNo"].ToString();
                                                isAgainstVoucher = true;
                                                againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                                                string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                                                string typeOfVoucher = conn.getSingleValue(queryString);
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note".ToLower() && typeOfVoucher.ToLower() == "sales invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt".ToLower() && typeOfVoucher.ToLower() == "purchase invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                            }

                                            voucherType = dr["typeOfVoucher"].ToString();

                                            if (voucherType.ToLower() == "Stock Journal".ToLower())
                                            {
                                                //voucherType = "Stock Transfer";
                                                string queryString = string.Format(" SELECT extra1 FROM tbl_StockJournalMaster WHERE voucherNo = '{0}' ", dr["refNo"]);
                                                voucherType = conn.getSingleValue(queryString);
                                            }

                                            if (outwardQty2 > 0)
                                            {
                                                if (voucherType.ToLower() == "Sales Invoice".ToLower() && outwardQty2 > 0)
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate;
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Material Receipt".ToLower())
                                                {
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue -= outwardQty2 * rate2;
                                                }
                                                else if (outwardQty2 > 0 && (voucherType.ToLower() == "Rejection Out".ToLower()) || (voucherType.ToLower() == "Purchase Return".ToLower())
                                                    || (voucherType.ToLower() == "Sales Return".ToLower()) || (voucherType.ToLower() == "Rejection In".ToLower() || voucherType.ToLower() == "Stock Transfer".ToLower()))
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;// 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Sales Order".ToLower())
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else
                                                {
                                                    if (qtyBal != 0)
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Return".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Rejection In".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Invoice".ToLower() && inwardQty2 > 0)
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;

                                                }
                                                else
                                                {
                                                    computedAverageRate = previousRunningAverage;// (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Delivery Note".ToLower())
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Opening Stock".ToLower())
                                            {
                                                // openingQuantityIn = 0;

                                                if (qtyBal != 0)
                                                {
                                                    //computedAverageRate = (stockValue / qtyBal);

                                                    // qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }
                                                else
                                                {
                                                    //computedAverageRate = (stockValue / 1);
                                                    //qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }

                                                OpeningStockValue += (inwardQty2 * rate2);
                                            }
                                            else
                                            {
                                                // hndles other transactions such as purchase, opening balance, etc
                                                qtyBal += inwardQty2 - outwardQty2;
                                                stockValue += (inwardQty2 * rate2);

                                            }

                                            // ------------------------------------------------------ //
                                            //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            string avgCost = "";
                                            if (stockValue != 0 && qtyBal != 0)
                                            {
                                                avgCost = (stockValue / qtyBal).ToString("N2");
                                                //avgCost = (stockValue / qtyBal * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }
                                            else
                                            {
                                                avgCost = computedAverageRate.ToString("N2");
                                                //avgCost = (computedAverageRate/ SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = dr["productId"].ToString(),
                                                ProductCode = dr["productCode"].ToString(),
                                                StoreName = dr["godownName"].ToString(),
                                                InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                                                OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                                                Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                                AverageCost = avgCost,
                                                QtyBalance = qtyBal.ToString("N2"),
                                                StockValue = stockValue.ToString("N2"),
                                                VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType,
                                                RefNo = refNo,
                                                Batch = string.IsNullOrEmpty(dr["Batch"].ToString()) ? "" : dr["Batch"].ToString(),
                                                Date = Convert.ToDateTime(dr["date"]).ToShortDateString(),
                                                ProductName = dr["productName"].ToString(),
                                                ProjectId = Convert.ToDecimal(dr["projectId"]),
                                                CategoryId = Convert.ToDecimal(dr["categoryId"]),
                                                UnitName = dr["unitName"].ToString()
                                            });

                                            isAgainstVoucher = false;

                                            stockValuePerItemPerStore = Convert.ToDecimal(stockValue);
                                            prevProductId = currentProductId;
                                            previousRunningAverage = Convert.ToDecimal(avgCost);
                                            inwardQtyPerStore += Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQtyPerStore += Convert.ToDecimal(dr["outwardQty"]);
                                            i++;
                                        }
                                        // ===================== for every store for that item, put the summary =================== //
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = "",
                                            ProductCode = item,
                                            StoreName = storeName,
                                            InwardQty = (inwardQtyPerStore /*+ openingQuantityIn*/).ToString("N2"),
                                            OutwardQty = ("-") + (outwardQtyPerStore /*+ openingQuantityOut*/).ToString("N2"),
                                            Rate = "",
                                            AverageCost = "",
                                            QtyBalance = qtyBal.ToString("N2"),
                                            StockValue = stockValuePerItemPerStore.ToString("N2"),
                                            VoucherTypeName = "Sub total: ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = "",
                                        });

                                        totalStockValue += Convert.ToDecimal(stockValuePerItemPerStore.ToString("N2"));
                                        totalQtyPerStore += qtyBal /*(inwardQtyPerStore - outwardQtyPerStore) + openingQuantity*/;

                                        openingQuantity = 0;
                                        inwardQtyPerStore = 0;
                                        outwardQtyPerStore = 0;
                                        qtyBal = 0;
                                        stockValue = 0;
                                        stockValuePerItemPerStore = 0;
                                        //dgvStockDetailsReport.Rows.Add();
                                        i++;
                                    }

                                    dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    prevStoreId = store;
                                }
                            }
                        }
                        // *** if no transaction for that item for all stores for that period add the ockock for all stores per store *** //
                        else
                        {
                            foreach (var store in AllStores)
                            {
                                stockValuePerItemPerStore = 0;
                                dtblStore = new DataTable();
                                // ************************ insert opening stock per item per store ******************** //
                                dtblOpeningStockPerItemPerStore = new DataTable();
                                if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                {
                                    dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item)).CopyToDataTable();

                                    if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                    {
                                        computedAverageRate = rate2;
                                        var itemParams = methodForStockValue.currentStockValue1235(lastFinYearStart, lastFinYearEnd, true, item, store);
                                        dcOpeningStockForRollOver = itemParams.Item1;
                                        openingQuantity = itemParams.Item2;
                                        openingQuantityIn = itemParams.Item3;
                                        openingQuantityOut = itemParams.Item4;
                                        qtyBal += openingQuantity;
                                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                        {
                                            //computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                            //stockValue = openingQuantity * computedAverageRate;
                                        }
                                        var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = item,
                                            ProductCode = item,
                                            StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                        Where(l => l.Field<string>("productCode") == item).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                            InwardQty = openingQuantityIn.ToString("N2"),
                                            OutwardQty = openingQuantityOut.ToString("N2"),
                                            Rate = "",
                                            AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                            //AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2") : "",
                                            QtyBalance = openingQuantity.ToString("N2"),
                                            StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                            VoucherTypeName = "Opening Stock ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = ""
                                        });

                                        totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                        dcOpeningStockForRollOver = 0;
                                        totalQtyPerStore += openingQuantity;
                                        openingQuantityIn = 0;
                                        openingQuantityOut = 0;
                                        openingQuantity = 0;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }

                // ************************ Calculate Total **************************** //
                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var avgCost2 = "";
                if (totalQtyPerStore != 0)
                {
                    avgCost2 = (totalStockValue / totalQtyPerStore).ToString("N2");
                }
                totalStockValue1 = totalStockValue;

                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = avgCost2,
                    QtyBalance = totalQtyPerStore.ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    VoucherTypeName = "Total Stock Value:",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var tyr = OpeningStockValue;
            }

            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }
            master.TotalAverageCost = 0;
            master.TotalQtyBalance = 0;
            master.TotalStockValue = 0;
            master.StockReportRowItem = details;


            return new { totalStockValue = totalStockValue1, OpeningStockValue = OpeningStockValue };
        }

        public decimal GetPreviousFinicialYearId(int currentFinicialYearId)
        {
            var currentYear = context.tbl_FinancialYear.Find(currentFinicialYearId);
            if (currentYear != null)
            {
                //get last year by subtracting 12 months from endOf year date
                var lastEndOfYearDate = currentYear.toDate.Value.AddMonths(-12); 

                var allFinicialYear = context.tbl_FinancialYear.ToList();

                //find date between lastEndof Year among all finical years and return that date  
               var lastYear = allFinicialYear.FirstOrDefault(a => a.fromDate >= lastEndOfYearDate 
                                                                  && a.toDate <= lastEndOfYearDate);
               if (lastYear != null)
               {
                   return lastYear.financialYearId;
               }

            }

            return 0;
        }


        public tbl_FinancialYear GetPreviousFinicialYear(int currentYearId)
        {
            var currentYear = context.tbl_FinancialYear.Find(currentYearId);
            if (currentYear != null)
            {
                //get last year by subtracting 12 months from endOf year date
                var lastEndOfYearDate = currentYear.toDate.Value.AddMonths(-12);

                var allFinicialYear = context.tbl_FinancialYear.ToList();

                //find date between lastEndof Year among all finical years and return that date  
                var lastYear = allFinicialYear.FirstOrDefault(a => a.fromDate >= lastEndOfYearDate
                                                                   && a.toDate <= lastEndOfYearDate);
                if (lastYear != null)
                {
                    return lastYear;
                }
            }
            return null;
        }
     
    }
}