﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using MatApi.DBModel;
using MatApi.Models;

namespace MatApi.Services
{
    public class LedgerServices
    {
       public DBMATAccounting_MagnetEntities1 context;

       public LedgerServices()
       {
           context   = new DBMATAccounting_MagnetEntities1();
       }

       public  decimal GetGoodInTransistId()
       {
           var g = context.tbl_AccountLedger.FirstOrDefault(a =>
               a.ledgerName.ToLower() == "Goods in Transit".ToLower());
           if (g != null)
           {
               return g.ledgerId;
           }
           // fix value initially created 
           return 110434;
       }

       public decimal GetAccountPayable(DateTime from, DateTime? to)
       {
           if (to == null)
           {
               to = DateTime.MaxValue;
           }
           var ledgersPostingValue = context.tbl_LedgerPosting
               .Where(a => a.tbl_AccountLedger.accountGroupId == AccountGroupConstants.AccountPayable && 
                           a.date >= from 
                           && a.date <= to
                           )
               .Sum(a=> a.credit- a.debit );
           return Convert.ToDecimal(ledgersPostingValue);
       }

       public async Task<decimal> GetAccountRecieveAble(DateTime from, DateTime? to)
       {
           if (to == null)
           {
               to = DateTime.MaxValue;
           }

           var ledgersPostingValue = await context.tbl_LedgerPosting
               .Where(a => a.tbl_AccountLedger.accountGroupId == AccountGroupConstants.AccountRecievable &&
                           a.date >= from
                           && a.date <= to
               )
               .SumAsync(a => a.debit - a.credit)
               .ConfigureAwait(false);

           return Convert.ToDecimal(ledgersPostingValue);
       }
    }
}