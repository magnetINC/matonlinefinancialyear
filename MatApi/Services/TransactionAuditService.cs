﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MatApi.DBModel;

namespace MatApi.Services
{
    public class TransactionAuditService
    {
        public DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();

        public bool Create(TransactionAuditLog auditLog)
        {
            context.TransactionAuditLogs.Add(auditLog);
            context.SaveChanges();
            return true;
        }

        public bool CreateMany(List<TransactionAuditLog> auditLogs)
        {
            context.TransactionAuditLogs.AddRange(auditLogs);
            context.SaveChanges();
            return true;
        }
    }
}