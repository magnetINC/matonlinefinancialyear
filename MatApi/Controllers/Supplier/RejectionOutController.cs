﻿using MatApi.Models;
using MatApi.Models.Register;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.UI.WebControls;
using MatApi.DBModel;
using MatApi.Dto;
using MatApi.Enums;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using PublicVariables = MatApi.Models.PublicVariables;
using EntityState = System.Data.Entity.EntityState;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RejectionOutController : ApiController
    {
        decimal decRejectionOutId = 0;
        decimal decRejectionOutVoucherTypeId = 12;
        decimal decRejectionOutSuffixPrefixId = 0;
        string strCashOrParty = string.Empty;
        string strVoucherNo = string.Empty;
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        string strRejectionOutNo = string.Empty;
        string strMaterialReceiptNo = string.Empty;
        string tableName = "RejectionOutMaster";
        decimal decRejectionOutMasterIdentity = 0;
        decimal decRejectionOutDetailsIdentity = 0;
        string strRejectionOutVoucherNo = string.Empty;

        ProductInfo infoProduct = new ProductInfo();
        ProductSP spProduct = new ProductSP();
        RejectionOutDetailsInfo infoRejectionOutDetails = new RejectionOutDetailsInfo();
        RejectionOutMasterInfo infoRejectionOutMaster = new RejectionOutMasterInfo();
        RejectionOutMasterSP spRejectionOutMaster = new RejectionOutMasterSP();
        RejectionOutDetailsSP spRejectionOutDetails = new RejectionOutDetailsSP();
        StockPostingSP spStockPosting = new StockPostingSP();
        MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();

        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public RejectionOutController()
        {
            
        }

        [HttpGet]
        public HttpResponseMessage RejectionOutLookups()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var suppliers = transactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            VoucherTypeSP SpVoucherType = new VoucherTypeSP();
            var voucherTypes = SpVoucherType.VoucherTypeSelectionComboFill("Material Receipt");

            dynamic response = new ExpandoObject();
            response.Suppliers = suppliers;
            response.Currencies = currencies;
            response.VoucherTypes = voucherTypes;
            response.Batches = new BatchSP().BatchViewAll();
            response.Unit = new UnitSP().UnitViewAll();
            response.allproducts = new ProductSP().ProductViewAll();
            response.stores = new GodownSP().GodownViewAll();
            response.taxes = new TaxSP().TaxViewAll();
            response.racks = new RackSP().RackViewAll();
            response.rejectionoutNo = VoucherNumberGeneration(new RejectionOutVM { Date =  DateTime.Now});

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpPost]
        public HttpResponseMessage Registers(RejectionOutRegisterVM input)
        {
            var resp = new RejectionOutMasterSP().RejectionOutMasterSearch(input.InvoiceNo, input.LedgerId, input.FromDate, input.ToDate, input.DoneBy, input.VoucherNo);
            return Request.CreateResponse(HttpStatusCode.OK, (object)resp);
        }

        [HttpGet]
        public HttpResponseMessage MaterialReceiptNumbers(decimal supplierId,decimal applyOn)
        {
            MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();
            DataTable dtbl = new DataTable();
            dtbl = spMaterialReceiptMaster.ShowMaterialReceiptNoForRejectionOut(supplierId, decRejectionOutId, applyOn);
            return Request.CreateResponse(HttpStatusCode.OK,dtbl);
        }

        [HttpGet]
        public DataTable GetMaterialReceiptDetails(decimal materialReceiptNo)
        {
            MaterialReceiptDetailsSP spMaterialReceiptDetails = new MaterialReceiptDetailsSP();
            DataTable dtblReceiptDetails = spMaterialReceiptDetails.ShowMaterialReceiptDetailsViewbyMaterialReceiptDetailsIdWithPending(Convert.ToDecimal(materialReceiptNo), decRejectionOutId);

            //List<RejectionOutLineItems2> response = new List<RejectionOutLineItems2>();
            //try
            //{

            //    var receipts = dtblReceiptDetails = spMaterialReceiptDetails.ShowMaterialReceiptDetailsViewbyMaterialReceiptDetailsIdWithPending(Convert.ToDecimal(materialReceiptNo), decRejectionOutId);
            //    for (int i = 0; i < receipts.Rows.Count; i++)
            //    {
            //        var note = receipts.Rows[i].ItemArray;
            //        var prod = new ProductSP().ProductView(Convert.ToDecimal(note[2]));

            //        response.Add(new RejectionOutLineItems2
            //        {
            //            Amount = Convert.ToDecimal(note[11]),
            //            Barcode = note[10].ToString(),
            //            Batch = note[9].ToString(),
            //            ProductCode = prod.ProductCode,
            //            ProductName = prod.ProductName,
            //            Quantity = Convert.ToDecimal(note[3]),
            //            Rack = new RackSP().RackView(Convert.ToDecimal(note[8])).RackName,
            //            Rate = Convert.ToDecimal(note[4]),
            //            Store = new GodownSP().GodownView(Convert.ToDecimal(note[7])).GodownName,
            //            Unit = new UnitSP().UnitView(Convert.ToDecimal(note[5])).UnitName

            //        });
            //    }

            //}
            //catch (Exception ex)
            //{
            //    formMDI.infoError.ErrorString = "RI12:" + ex.Message;
            //}
            return dtblReceiptDetails;
        }

        public string VoucherNumberGeneration(RejectionOutVM input)
        {
            try
            {
                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                RejectionOutMasterSP spRejectionOut = new RejectionOutMasterSP();
                strVoucherNo = "0";
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decRejectionOutVoucherTypeId, Convert.ToDecimal(strVoucherNo), input.Date, tableName);
                if (Convert.ToDecimal(strVoucherNo) != spRejectionOut.RejectionOutMasterGetMaxPlusOne(decRejectionOutVoucherTypeId))
                {
                    strVoucherNo = spRejectionOut.RejectionOutMasterGetMax(decRejectionOutVoucherTypeId).ToString();
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decRejectionOutVoucherTypeId, Convert.ToDecimal(strVoucherNo), input.Date, tableName);
                    if (spRejectionOut.RejectionOutMasterGetMax(decRejectionOutVoucherTypeId) == "0")
                    {
                        strVoucherNo = "0";
                        strVoucherNo = obj.VoucherNumberAutomaicGeneration(decRejectionOutVoucherTypeId, Convert.ToDecimal(strVoucherNo), input.Date, tableName);
                    }
                }
                //if (isAutomatic)
               // {
                    SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                    SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                    infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decRejectionOutVoucherTypeId, input.Date);
                    strPrefix = infoSuffixPrefix.Prefix;
                    strSuffix = infoSuffixPrefix.Suffix;
                    decRejectionOutSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                    strRejectionOutNo = strPrefix + strVoucherNo + strSuffix;
                //}
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RO2:" + ex.Message;
            }
            return strVoucherNo;
        }

        [HttpPost]
        public void SaveOrEditFunction(RejectionOutVM input)
        {
            try
            {
                //input.RejectionOutNo = Convert.ToDecimal(VoucherNumberGeneration(input));
                // to take assign voucher number in case automatic voucher numbering is set to off urefe 20161208
                //if (!isAutomatic && txtRejectionOutNo.Text.Trim() != string.Empty)
                //{
                //Used Incoming RejectedOutNo as VoucherNo
                strVoucherNo = input.RejectionOutNo.ToString(); 
                   //strVoucherNo = VoucherNumberGeneration(input);
                //}
                decimal decProductId = 0;
                decimal decBatchId = 0;
                decimal decCalcQty = 0;
                StockPostingSP spStockPosting = new StockPostingSP();
                SettingsSP spSettings = new SettingsSP();
                string strStatus = spSettings.SettingsStatusCheck("NegativeStockStatus");
                bool isNegativeLedger = false;
                //int inRowCount = dgvProduct.RowCount;
                foreach(var l in input.LineItems)
                {
                    if (l.ProductId>0)
                    {
                        decProductId = Convert.ToDecimal(l.ProductId);

                        if (l.BatchId>0)
                        {
                            decBatchId = Convert.ToDecimal(l.BatchId);
                        }
                        decimal decCurrentStock = spStockPosting.StockCheckForProductSale(decProductId, decBatchId);
                        if (l.Quantity>0)
                        {
                            decCalcQty = decCurrentStock - Convert.ToDecimal(l.Quantity);
                        }
                        if (decCalcQty < 0)
                        {
                            isNegativeLedger = true;
                            break;
                        }
                    }
                }
                SaveFunction(input);
                //if (isNegativeLedger)
                //{
                //    if (strStatus == "Warn")
                //    {
                //        if (MessageBox.Show("Negative Stock balance exists,Do you want to Continue", "MAT Financials", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                //        {
                //            SaveOrEdit();
                //        }
                //    }
                //    else if (strStatus == "Block")
                //    {
                //        MessageBox.Show("Cannot continue ,due to negative stock balance", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Stop);
                //    }
                //    else
                //    {
                //        SaveOrEdit();
                //    }
                //}
                //else
                //{
                //    SaveOrEdit();
                //}
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RO43:" + ex.Message;
            }
        }

        public void SaveFunction(RejectionOutVM input)
        {
            try
            {

                ProductSP spProduct = new ProductSP();
                SettingsSP Spsetting = new SettingsSP();
                infoRejectionOutMaster.Date = Convert.ToDateTime(input.Date);
                infoRejectionOutMaster.LedgerId = Convert.ToDecimal(input.SupplierId);
                input.VoucherTypeId = Constants.RejectionOut_VoucherTypeId;
                //if (isAutomatic == true)
                //{
                infoRejectionOutMaster.SuffixPrefixId = decRejectionOutSuffixPrefixId;
                infoRejectionOutMaster.VoucherNo = strVoucherNo;
                //}
                //else
                //{
                //    infoRejectionOutMaster.SuffixPrefixId = 0;
                //    infoRejectionOutMaster.VoucherNo = txtRejectionOutNo.Text.Trim();
                //}

                if (input.MaterialReceiptNo>0)
                {
                    infoRejectionOutMaster.MaterialReceiptMasterId = Convert.ToDecimal(input.MaterialReceiptNo);
                }
                else
                {
                    infoRejectionOutMaster.MaterialReceiptMasterId = 0;
                }

                if (input.CurrencyId>0)
                {
                    infoRejectionOutMaster.ExchangeRateId = Convert.ToDecimal(input.CurrencyId);
                }
                infoRejectionOutMaster.VoucherTypeId = decRejectionOutVoucherTypeId;
                

                //infoRejectionOutMaster.InvoiceNo = VoucherNumberGeneration(input);
                //Used Incoming RejectedOutNo
                infoRejectionOutMaster.InvoiceNo = input.RejectionOutNo.ToString();
                
                infoRejectionOutMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                infoRejectionOutMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoRejectionOutMaster.Narration = input.Narration;
                infoRejectionOutMaster.LrNo = input.LrNo;
                infoRejectionOutMaster.TransportationCompany = input.TransportCompany;
                infoRejectionOutMaster.TotalAmount = Convert.ToDecimal(input.TotalAmount);
                infoRejectionOutMaster.Extra1 = string.Empty;
                infoRejectionOutMaster.Extra2 = string.Empty;
                infoRejectionOutMaster.DoneBy = input.DoneBy;
                if (decRejectionOutId == 0)
                {
                    decRejectionOutMasterIdentity = spRejectionOutMaster.RejectionOutMasterAddWithReturnIdentity(infoRejectionOutMaster);
                }
                else
                {
                    infoRejectionOutMaster.RejectionOutMasterId = decRejectionOutId;
                    spRejectionOutMaster.RejectionOutMasterEdit(infoRejectionOutMaster);
                }
                if (decRejectionOutId == 0)
                {
                    infoRejectionOutDetails.RejectionOutMasterId = decRejectionOutMasterIdentity;
                }
                else
                {
                    spRejectionOutDetails.RejectionOutDetailsDeleteByRejectionOutMasterId(decRejectionOutId);
                    spStockPosting.DeleteStockPostingByAgnstVouTypeIdAndAgnstVouNo(decRejectionOutVoucherTypeId, strRejectionOutVoucherNo);
                    infoRejectionOutDetails.RejectionOutMasterId = decRejectionOutId;
                }
                //int inRowcount = dgvProduct.Rows.Count;
                decimal pId = 0;
                foreach(var lineItem in input.LineItems)
                {
                    decimal prodId = Convert.ToDecimal(lineItem.ProductId);
                    pId = prodId;
                    infoRejectionOutDetails.Slno = Convert.ToInt32(lineItem.SL);
                    infoRejectionOutDetails.MaterialReceiptDetailsId = lineItem.MaterialReceiptDetailsId;
                    infoProduct = spProduct.ProductView(lineItem.ProductId);
                    infoRejectionOutDetails.Qty = Convert.ToDecimal(lineItem.Quantity);
                    infoRejectionOutDetails.ProductId = prodId;
                    infoRejectionOutDetails.UnitId = lineItem.UnitId;
                    var unitconv = new UnitConvertionSP().UnitViewAllByProductId(infoProduct.ProductId);
                    infoRejectionOutDetails.UnitConversionId = Convert.ToDecimal(unitconv.UnitconvertionId);
                    infoRejectionOutDetails.BatchId = lineItem.BatchId;
                    infoRejectionOutDetails.GodownId = lineItem.StoreId;
                    infoRejectionOutDetails.RackId = lineItem.RackId;
                    infoRejectionOutDetails.Rate = Convert.ToDecimal(lineItem.Rate);
                    infoRejectionOutDetails.Amount = Convert.ToDecimal(lineItem.Rate*lineItem.Quantity);
                    //infoRejectionOutDetails.ProjectId = 0;
                    infoRejectionOutDetails.CategoryId = 0;
                    infoRejectionOutDetails.Extra1 = string.Empty;
                    infoRejectionOutDetails.Extra2 = string.Empty;
                    decRejectionOutDetailsIdentity = spRejectionOutDetails.RejectionOutDetailsAddWithReturnIdentity(infoRejectionOutDetails);
                    AddStockPosting(pId,input);

                    var product = context.tbl_Product.Find(lineItem.ProductId);
                    if (product != null)
                    {
                        // Rejection Out GoodinTransit Ledger Posting 
                        var ledgerExpense = context.tbl_LedgerPosting
                            .FirstOrDefault(a => a.ledgerId == product.expenseAccount);
                        LedgerPosting(new LedgerPostingInfo()
                        {
                         InvoiceNo   = infoRejectionOutMaster.InvoiceNo ,
                         Debit = Convert.ToDecimal(product.purchaseRate * lineItem.Quantity),
                          Date   = infoRejectionOutMaster.Date,
                          ExtraDate = DateTime.UtcNow,
                          LedgerId = LedgerConstants.GoodsInTransit,
                          VoucherTypeId = infoRejectionOutMaster.VoucherTypeId,
                          Credit = 0,
                          Extra2 = "",
                          Extra1 = "",
                          VoucherNo = infoRejectionOutMaster.InvoiceNo ,
                          ChequeDate = DateTime.UtcNow,
                          ChequeNo = "",
                          DetailsId = decRejectionOutDetailsIdentity,
                          YearId = PublicVariables.CurrentFinicialYearId
                          
                        });
                    }
                   
                }

                if (decRejectionOutId == 0)
                {

                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RO33:" + ex.Message;
            }
        }
        public bool LedgerPosting(LedgerPostingInfo infoLedgerPosting)
        {
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            var post =      spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
            if (post > 0) return true;
          
            return false;
        }
        public decimal AddStockPosting(decimal productId,RejectionOutVM input)
        {
            try
            {
                StockPostingInfo infoStockPosting = new StockPostingInfo();
                var spStockPosting = new MatApi.Services.StockPostingSP();
                //MaterialReceiptMasterInfo infoMaterialReceiptMaster = new MaterialReceiptMasterInfo();
                //infoMaterialReceiptMaster = spMaterialReceiptMaster.MaterialReceiptMasterView(Convert.ToDecimal(input.MaterialReceiptNo));
                infoStockPosting.Date = DateTime.Parse(input.Date.ToString());
                infoStockPosting.VoucherTypeId = input.VoucherTypeId;
                infoStockPosting.VoucherNo = input.RejectionOutNo.ToString();
                infoStockPosting.InvoiceNo = input.RejectionOutNo.ToString();
                //if (isAutomatic)
                //{
                    infoStockPosting.AgainstVoucherNo = strVoucherNo;
                    infoStockPosting.AgainstInvoiceNo = input.RejectionOutNo.ToString();
                //}
                //else
                //{
                //    infoStockPosting.AgainstVoucherNo = input.RejectionOutNo.ToString();
                //    infoStockPosting.AgainstInvoiceNo = txtRejectionOutNo.Text;
                //}
                if (decRejectionOutVoucherTypeId != 0)
                {
                    infoStockPosting.AgainstVoucherTypeId = decRejectionOutVoucherTypeId;
                }
                if (strVoucherNo != string.Empty)
                {
                    infoStockPosting.AgainstVoucherNo = strVoucherNo;
                }
                if (input.RejectionOutNo >0)
                {
                    infoStockPosting.AgainstInvoiceNo = input.RejectionOutNo.ToString();
                }
                infoStockPosting.InwardQty = 0;
                infoStockPosting.OutwardQty = infoRejectionOutDetails.Qty;
                infoStockPosting.ProductId = infoRejectionOutDetails.ProductId; // infoRejectionOutDetails.ProductId;
                infoStockPosting.BatchId = infoRejectionOutDetails.BatchId;
                infoStockPosting.UnitId = infoRejectionOutDetails.UnitId;
                infoStockPosting.GodownId = infoRejectionOutDetails.GodownId;
                infoStockPosting.RackId = infoRejectionOutDetails.RackId;
                infoStockPosting.Rate = infoRejectionOutDetails.Rate;
                infoStockPosting.ProjectId = infoRejectionOutDetails.ProjectId;
                infoStockPosting.CategoryId = infoRejectionOutDetails.CategoryId;
                infoStockPosting.Extra1 = string.Empty;
                infoStockPosting.Extra2 = string.Empty;
                infoStockPosting.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                return spStockPosting.StockPostingAdd(infoStockPosting);
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RO28:" + ex.Message;
            }
            return 0;
        }

        
        [HttpPost]
        public IHttpActionResult Edit([FromBody] EditRejectionOutDto record)
        {
            var auditLogs = new List<TransactionAuditLog>();
            var master = context.tbl_RejectionOutMaster.Find(record.RejectionOutMaster.rejectionOutMasterId);
            var rejectionOutVochurId = Constants.RejectionOut_VoucherTypeId;
            if (master == null) return NotFound();

            // edit master    
            //create old audit log value 
            auditLogs.Add(new TransactionAuditLog()
            {
                TransId = master.rejectionOutMasterId.ToString(),
                DateTime = DateTime.UtcNow,
                Amount = master.totalAmount,
                VochureTypeId = Constants.RejectionOut_VoucherTypeId,
                VochureName = TransactionsEnum.RejectionOutMaster.ToString(),
                AdditionalInformation = "Edited Rejection Out Master",
                UserId = PublicVariables.CurrentUserId,
                Status = TransactionOperationEnums.Edit.ToString()
            });
            //master.ledgerId = record.RejectionInMaster.ledgerId;
            master.totalAmount = record.RejectionOutMaster.totalAmount;
            master.narration = record.RejectionOutMaster.narration;
            master.transportationCompany = record.RejectionOutMaster.transportationCompany;
            context.Entry(master).State = EntityState.Modified;


            var currentDetails =
                context.tbl_RejectionOutDetails.Where(a => a.rejectionOutMasterId == master.rejectionOutMasterId).ToList();
            var detailIds = currentDetails.Select(a => a.rejectionOutDetailsId).ToList();
            var updatedIds = new List<decimal>();
            foreach (var i in record.RejectionOutDetails)
            {
                //get item to update

                var item = currentDetails.Find(a => a.rejectionOutDetailsId == i.rejectionOutDetailsId);
            
                if (item != null)
                {
                    auditLogs.Add(new TransactionAuditLog()
                    {
                        TransId = item.rejectionOutDetailsId.ToString(),
                        DateTime = DateTime.UtcNow,
                        Amount = item.amount,
                        Quantity = item.qty,
                        VochureTypeId = Constants.Rejection_In_VoucherTypeId,
                        VochureName = TransactionsEnum.RejectionInDetail.ToString(),
                        AdditionalInformation = "Edited Rejection Out  detail Record",
                        UserId = PublicVariables.CurrentUserId,
                        Status = TransactionOperationEnums.Edit.ToString()

                    });
                    item.qty = i.qty;
                    item.amount = item.rate * i.qty;
                    context.Entry(item).State = EntityState.Modified;
                    updatedIds.Add(item.rejectionOutDetailsId);
                }
                if (i.rejectionOutDetailsId == 0)
                {
                    i.extraDate = DateTime.UtcNow;
                    context.tbl_RejectionOutDetails.Add(i);
                }
            }
            var itemToDeleteIds = detailIds.Except(updatedIds);
            foreach (var i in itemToDeleteIds)
            {
                var item = currentDetails.FirstOrDefault(a => a.rejectionOutDetailsId == i);
                if (item != null)
                {
                    context.tbl_RejectionOutDetails.Remove(item);
                }
            }

            context.SaveChanges();

            //update stock posting  
            //get stock posting base on the  deliveryNoteVochureType Id, and its Vochure number that can be found in masterRecord
            var stockPosting = context.tbl_StockPosting.Where(a =>
                a.voucherTypeId == rejectionOutVochurId && a.voucherNo == master.voucherNo).ToList();
            // delete stock posting 
            if (stockPosting.Any())
            {
                context.tbl_StockPosting.RemoveRange(stockPosting);
            }
            //get new updated items 
            var updatedItems = context.tbl_RejectionInDetails
                .Where(a => a.rejectionInMasterId == master.rejectionOutMasterId)
                .ToList();

            // add new stock posting to update record
            foreach (var i in updatedItems)
            {
                var product = context.tbl_Product.Find(i.productId);
                context.tbl_StockPosting.Add(new DBModel.tbl_StockPosting
                {
                    //set items here 
                    batchId = i.batchId,
                    CategoryId = i.CategoryId,
                    productId = i.productId,
                    ProjectId = i.ProjectId,
                    extra2 = i.extra2,
                    voucherTypeId = rejectionOutVochurId,
                    date = DateTime.UtcNow,
                    extraDate = DateTime.UtcNow,
                    financialYearId = PublicVariables.CurrentFinicialYearId,
                    extra1 = "",
                    rackId = i.rackId,
                    voucherNo = master.voucherNo,
                    invoiceNo = master.invoiceNo,
                    unitId = i.unitId,
                    rate = product.purchaseRate,
                    godownId = i.godownId,
                    outwardQty = 0,
                    inwardQty = i.qty,
                    againstInvoiceNo = "",
                    againstVoucherNo = "",
                    againstVoucherTypeId = 0
                });

                context.SaveChanges();
            }

            //get all ledger posting base on the vocheur id from the master, then delete them and create a new one 
            var oldLedgerPosting = context.tbl_LedgerPosting
                .Where(a => a.voucherNo == master.voucherNo && a.voucherTypeId == rejectionOutVochurId)
                .ToList();
            if (oldLedgerPosting.Any())
            {
                context.tbl_LedgerPosting.RemoveRange(oldLedgerPosting);
            }
            //add new ledger base on the updated item and debit the good in transit ledger; 
            var goodInTransitLedger = LedgerConstants.GoodsInTransit;

            foreach (var i in updatedItems)
            {
                var product = context.tbl_Product.Find(i.productId);
                context.tbl_LedgerPosting.Add(new DBModel.tbl_LedgerPosting()
                {
                    voucherTypeId = Constants.Rejection_In_VoucherTypeId,
                    date = DateTime.UtcNow,
                    ledgerId = goodInTransitLedger,
                    extra2 = "",
                    extraDate = DateTime.UtcNow,
                    voucherNo = master.voucherNo,
                    extra1 = "",
                    credit =0,
                    debit = i.qty * i.rate,
                    invoiceNo = master.invoiceNo,
                    chequeDate = DateTime.UtcNow,
                    yearId = PublicVariables.CurrentFinicialYearId,
                    detailsId = i.rejectionInDetailsId,
                    chequeNo = ""
                });
                context.SaveChanges();
            }
            if (auditLogs.Any())
            {
                new Services.TransactionAuditService().CreateMany(auditLogs);
            }
            return Ok(new { status = 200, message = "Edit Made Successful" });
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var rejectionItems = context.tbl_RejectionOutMaster.Find(id);

            if (rejectionItems != null)
            {
                //check if sales has rejection items deliveryNote id
                //var sales = context.tbl_SalesMaster
                //    .Where(a => a.deliveryNoteMasterId == rejectionItems.deliveryNoteMasterId)
                //    .ToList();
                //if (sales.Any())
                //    return Ok(new
                //    {
                //        status = 200,
                //        message = "Cannot Delete Item now, Since record now since record has an invoice"
                //    });

                //delete ledger transaction  

                var ledgers = context.tbl_LedgerPosting
                    .Where(a => a.voucherNo == rejectionItems.voucherNo &&
                                a.voucherTypeId == Constants.RejectionOut_VoucherTypeId)
                    .ToList();
                if (ledgers.Any())
                {
                    context.tbl_LedgerPosting.RemoveRange(ledgers);
                }

                //delete stock posting
                var stock = context.tbl_StockPosting
                    .Where(a => a.voucherNo == rejectionItems.voucherNo &&
                                              a.voucherTypeId == Constants.RejectionOut_VoucherTypeId)
                    .ToList();
                if (stock.Any())
                {
                    context.tbl_StockPosting.RemoveRange(stock);
                }

                //var deliveryMaster = rejectionItems.tbl_DeliveryNoteMaster;
                //var rejectedItems = rejectionItems.tbl_RejectionInDetails;
                // update deliveryMasterItem  
                //deliveryMaster.totalAmount = deliveryMaster.totalAmount + rejectionItems.totalAmount;
                //context.Entry(deliveryMaster).State = EntityState.Modified;

                ////update deliveryDetail Items 
                //foreach (var i in rejectedItems)
                //{
                //    var deliverItem = deliveryMaster.tbl_DeliveryNoteDetails
                //        .FirstOrDefault(a => a.deliveryNoteDetailsId == i.deliveryNoteDetailsId);
                //    deliverItem.qty = deliverItem.qty + i.qty;
                //    deliverItem.amount = deliverItem.rate * i.qty;
                //    context.Entry(deliverItem).State = EntityState.Modified;
                //}



                //get record before delete and add to  
                List<TransactionAuditLog> itemBefore = rejectionItems.tbl_RejectionOutDetails.Select(a => new TransactionAuditLog()
                {

                    Quantity = a.qty,
                    Amount = a.qty * a.amount,
                    UserId = PublicVariables.CurrentUserId,
                    VochureName = Enums.TransactionsEnum.RejectionOutDetail.ToString(),
                    VochureTypeId = Constants.Rejection_In_VoucherTypeId,
                    Status = Enums.TransactionOperationEnums.Delete.ToString(),
                    DateTime = DateTime.UtcNow,
                    AdditionalInformation = "Rejection Out items deleted",
                    TransId = a.rejectionOutDetailsId.ToString(),
                }).ToList();  

                itemBefore.Add(new TransactionAuditLog()
                {
                    TransId = rejectionItems.rejectionOutMasterId.ToString(),
                    Amount = rejectionItems.totalAmount,
                    UserId = PublicVariables.CurrentUserId,
                    VochureName = Enums.TransactionsEnum.RejectionOutMaster.ToString(),
                    VochureTypeId = Constants.Rejection_In_VoucherTypeId,
                    Status = Enums.TransactionOperationEnums.Delete.ToString(),
                    DateTime = DateTime.UtcNow,
                    AdditionalInformation = "Rejection Out Master Item deleted",
                });

                //remove master and detail (details are automatically removed because of cascade enforce in the reletionship

                context.tbl_RejectionOutMaster.Remove(rejectionItems);
                if (context.SaveChanges() > 0)
                {
                    new Services.TransactionAuditService().CreateMany(itemBefore);
                    return Ok(new { status = 200, message = "Record Deleted Successfully" });
                }
                else
                {
                    return Ok(new { status = 200, message = "Something went wrong unable to delete items now" });
                }
            }
            return BadRequest("Something went wrong");
        }


        [HttpGet]
        public IHttpActionResult GetRejectionDetails(int id)
        {
            try
            {
                context = new DBMATAccounting_MagnetEntities1();
                context.Configuration.LazyLoadingEnabled = false;
                context.Configuration.ProxyCreationEnabled = false;
                var rejectionDetails =
                    context.tbl_RejectionOutDetails.Where(a => a.rejectionOutMasterId == id)
                        .Include(a => a.tbl_RejectionOutMaster)
                        .Include(a => a.tbl_Product)
                        .Include(a=> a.tbl_MaterialReceiptDetails)
                    .Select(a => new
                     {
                         RejectionDetails = a,
                         Product = a.tbl_Product,
                         RejectionMaster = a.tbl_RejectionOutMaster, 
                         Initial = a.tbl_MaterialReceiptDetails,
                         Store = context.tbl_Godown.Where(c => c.godownId == a.godownId).FirstOrDefault(),
                         Unit = context.tbl_Unit.Where(c =>c.unitId == a.unitId).FirstOrDefault(),
                         Ledger = context.tbl_AccountLedger.Where( c=>c.ledgerId== a.tbl_RejectionOutMaster.ledgerId).FirstOrDefault(),
                         User = context.tbl_User.FirstOrDefault( c=>c.userId ==  a.tbl_RejectionOutMaster.userId)
                     }).ToList()
                    ;

                return Ok( rejectionDetails );
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return Json("") ;
        }
    }
}
