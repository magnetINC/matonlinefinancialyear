﻿using MatApi.Models.Register;
using MatApi.Models.Supplier;
using MATFinancials;
using MATFinancials.Other;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;
using MatApi.Dto;
using MatApi.Enums;
using MatApi.Models;
using PublicVariables = MATFinancials.PublicVariables;
using EntityState = System.Data.Entity.EntityState;
using MatDal;
using tbl_Batch = MatApi.DBModel.tbl_Batch;
using tbl_PurchaseMaster = MatApi.DBModel.tbl_PurchaseMaster;
using TransactionAuditLog = MatApi.DBModel.TransactionAuditLog;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PurchaseInvoiceController : ApiController
    {
        decimal decSalesDetailsId = 0;
        List<decimal> listofDetailsId = new List<decimal>();
        decimal TotalAmount = 0;
        string strDebitNoteMasterTableName = "DebitNoteMaster";
        string strTableName = "PurchaseMaster";
        string strPrefix = "";
        string strSuffix = "";
        decimal ExchangeRateId = 1;
        string comStr = "";
        decimal decPurchaseInvoiceVoucherTypeId = 29;
        string strVoucherNo = string.Empty;
        DataTable dtForReference = new DataTable();

        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();

        public PurchaseInvoiceController()
        {

        }

        [HttpGet]
        public HttpResponseMessage InvoiceLookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var suppliers = transactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            var accountLedgers = new PurchaseMasterSP().AccountLedgerViewForAdditionalCost();
            var units = new UnitSP().UnitViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks = new RackSP().RackViewAll();
            var batches = new BatchSP().BatchViewAll();
            var tax = new TaxSP().TaxView(2);
            var taxes = new TaxSP().TaxViewAll();

            dynamic response = new ExpandoObject();
            response.Suppliers = suppliers;
            response.Currencies = currencies;
            response.AccountLedgers = accountLedgers;
            response.Units = units;
            response.Stores = stores;
            response.Racks = racks;
            response.Batches = batches;
            response.Tax = tax;
            response.Taxes = taxes;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public DataTable GetVoucherTypes(string purchaseMode)
        {
            VoucherTypeSP spVoucherType = new VoucherTypeSP();
            try
            {
                return spVoucherType.VoucherTypeSelectionComboFill(purchaseMode);
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        [HttpGet]
        public DataTable GetPurchaseOrderNumbers(decimal supplierId, decimal voucherTypeId)
        {
            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();
            DataTable dtbl = new DataTable();
            dtbl = spPurchaseMaster.GetOrderNoCorrespondingtoLedgerByNotInCurrPI(supplierId, 0, voucherTypeId);
            DataRow drow = dtbl.NewRow();
            drow["purchaseOrderMasterId"] = 0;
            drow["invoiceNo"] = string.Empty;
            dtbl.Rows.InsertAt(drow, 0);
            return dtbl;
        }

        [HttpGet]
        public DataTable GetMaterialReceiptNumbers(decimal supplierId, decimal voucherTypeId)
        {
            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();
            DataTable dtbl = new DataTable();
            dtbl = spPurchaseMaster.GetMaterialReceiptNoCorrespondingtoLedgerByNotInCurrPI(supplierId, 0, voucherTypeId);
            DataRow drow = dtbl.NewRow();
            drow["materialReceiptMasterId"] = 0;
            drow["invoiceNo"] = string.Empty;
            dtbl.Rows.InsertAt(drow, 0);
            return dtbl;
        }

        [HttpGet]
        public DataTable GetFormTypes()
        {
            VoucherTypeSP spVoucherType = new VoucherTypeSP();
            DataTable dtbl = new DataTable();
            try
            {
                dtbl = spVoucherType.VoucherTypeSelectionComboFill("Purchase Invoice");
                DataRow drow = dtbl.NewRow();
                drow["voucherTypeId"] = 0;
                drow["voucherTypeName"] = "All";
                dtbl.Rows.InsertAt(drow, 0);
                return dtbl;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        [HttpPost]
        public HttpResponseMessage Registers(PurchaseInvoiceRegisterVM input)
        {
            var resp = new PurchaseMasterSP().PurchaseInvoiceRegisterFill(input.Column, input.FromDate,input.ToDate,input.LedgerId, input.PurchaseMode,input.VoucherType, input.InvoiceNo, input.DoneBy);
            return Request.CreateResponse(HttpStatusCode.OK,(object)resp);
        }

        [HttpGet]
        public DataTable RegisterDetails(decimal id)
        {
            var details = new PurchaseDetailsSP().PurchaseDetailsViewByPurchaseMasterId(id);
            return details;
        }

        public decimal PurchaseMasterAdd(PurchaseMasterInfo purchasemasterinfo)
        {
            decimal decPurchaseMasterId = 0;

            try
            {
                var NewPurchaseMaster = new tbl_PurchaseMaster()
                {
                    doneby = purchasemasterinfo.DoneBy,
                    voucherNo = purchasemasterinfo.InvoiceNo,
                    additionalCost = purchasemasterinfo.AdditionalCost,
                    billDiscount = purchasemasterinfo.BillDiscount,
                    creditPeriod = purchasemasterinfo.CreditPeriod,
                    date = purchasemasterinfo.Date,
                    exchangeRateId = purchasemasterinfo.ExchangeRateId,
                    extra1 = purchasemasterinfo.Extra1,
                    extra2 = purchasemasterinfo.Extra2,
                    extraDate = purchasemasterinfo.ExtraDate,
                    financialYearId = purchasemasterinfo.FinancialYearId,
                    grandTotal = purchasemasterinfo.GrandTotal,
                    invoiceNo = purchasemasterinfo.InvoiceNo,
                    ledgerId = purchasemasterinfo.LedgerId,
                    materialReceiptMasterId = purchasemasterinfo.MaterialReceiptMasterId,
                    narration = purchasemasterinfo.Narration,
                    purchaseAccount = purchasemasterinfo.PurchaseAccount,
                    purchaseOrderMasterId = purchasemasterinfo.PurchaseOrderMasterId,
                    suffixPrefixId = purchasemasterinfo.SuffixPrefixId,
                    totalAmount = purchasemasterinfo.TotalAmount,
                    voucherTypeId = purchasemasterinfo.VoucherTypeId,
                    vendorInvoiceNo = purchasemasterinfo.VendorInvoiceNo,
                    vendorInvoiceDate = purchasemasterinfo.VendorInvoiceDate,
                    userId = purchasemasterinfo.UserId,
                    transportationCompany = purchasemasterinfo.TransportationCompany,
                    totalTax = purchasemasterinfo.TotalTax,
                    lrNo = purchasemasterinfo.InvoiceNo,
                };
                context.tbl_PurchaseMaster.Add(NewPurchaseMaster);
                decPurchaseMasterId = context.SaveChanges();

                decPurchaseMasterId = NewPurchaseMaster.purchaseMasterId;

            }
            catch (Exception ex)
            {
               
            }
            finally
            {
               
            }
            return decPurchaseMasterId;
        }

        [HttpPost]
        public string SavePurchaseInvoice(PurchaseInvoiceVM input)
        {
            //remove this restriction and don't type in the part value you want to apply so that the credit note will not split. tweak further
            decimal totalAdvance = 0;
            decimal totalInvoiceCreated = input.GrandTotal;

            if (totalAdvance > totalInvoiceCreated)
            {
                return "APPLIED_AMOUNT_GREATER_THAN_BILL_AMOUNT";
            }
            decimal decPurchaseMasterId = 0;
            PurchaseMasterInfo infoPurchaseMaster = new PurchaseMasterInfo();
            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();
            PurchaseDetailsInfo infoPurchaseDetails = new PurchaseDetailsInfo();
            PurchaseDetailsSP spPurchaseDetails = new PurchaseDetailsSP();
            MaterialReceiptMasterInfo infoMaterialReceiptMaster = new MaterialReceiptMasterInfo();
            MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();
            PurchaseOrderMasterInfo infoPurchaseOrderMaster = new PurchaseOrderMasterInfo();
            PurchaseOrderMasterSP spPurchaseOrderMaster = new PurchaseOrderMasterSP();
            StockPostingInfo infoStockPosting = new StockPostingInfo();
            StockPostingSP spStockPosting = new StockPostingSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            PurchaseBillTaxInfo infoPurchaseBillTax = new PurchaseBillTaxInfo();
            PurchaseBillTaxSP spPurchaseBillTax = new PurchaseBillTaxSP();
            AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
            AccountLedgerSP spAccountLedger = new AccountLedgerSP();
            UnitConvertionSP spUnitConvertion = new UnitConvertionSP();
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            int itemsinList = 0;
            decimal existingLedgerId = 0;
            var grandTotal = 0.0M;
            var totalAmount = 0.0M;
            var totalTax = 0.0M;
            var totalDiscount = 0.0M;
            try
            {
                try
                {

                    if(input.PurchaseMasterInfo.Date.Year != PublicVariables._dtToDate.Year)
                    {
                        try
                        {

                            if(input.PurchaseMasterInfo.VendorInvoiceDate.Year == PublicVariables._dtToDate.Year)
                            {
                                var DateIn = Convert.ToDateTime(input.PurchaseMasterInfo.VendorInvoiceDate);
                                input.PurchaseMasterInfo.Date = DateIn;
                            }
                            else
                            {
                                return "Invalid Invioce Date or Transaction Date";
                            }
                        }
                        catch (Exception ex21)
                        {
                            return "Invalid Invioce Date or Transaction Date";
                        }
                    }

                   
                }
                catch(Exception ex)
                {
                    try
                    {
                        var DateIn = Convert.ToDateTime(input.PurchaseMasterInfo.VendorInvoiceDate);
                        input.PurchaseMasterInfo.Date = DateIn;
                    }
                    catch (Exception ex21)
                    {
                        return "Invalid Invioce Date or Transaction Date";
                    }
                }

                /*-----------------------------------------Purchase Master Add----------------------------------------------------*/
                infoPurchaseMaster.AdditionalCost = input.PurchaseMasterInfo.AdditionalCost;
                infoPurchaseMaster.BillDiscount = input.PurchaseMasterInfo.BillDiscount;
                infoPurchaseMaster.CreditPeriod = input.PurchaseMasterInfo.CreditPeriod;
                infoPurchaseMaster.Date = input.PurchaseMasterInfo.Date;
                infoPurchaseMaster.ExchangeRateId = input.PurchaseMasterInfo.ExchangeRateId;
                infoPurchaseMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoPurchaseMaster.GrandTotal = input.PurchaseMasterInfo.GrandTotal;
                infoPurchaseMaster.InvoiceNo = input.PurchaseMasterInfo.VoucherNo;
                //infoPurchaseMaster.DoneBy = input.PurchaseMasterInfo.DoneBy.FirstOrDefault().ToString();
                //if (isAutomatic)
                //{
                //    infoPurchaseMaster.SuffixPrefixId = decPurchaseInvoiceSuffixPrefixId;
                //    infoPurchaseMaster.VoucherNo = strVoucherNo;
                //}
                //else
                //{
                infoPurchaseMaster.SuffixPrefixId = 0;
                infoPurchaseMaster.VoucherNo = input.PurchaseMasterInfo.VoucherNo;
                //infoPurchaseMaster.DoneBy = input.PurchaseMasterInfo.DoneBy;
                //}
                infoPurchaseMaster.LedgerId = input.PurchaseMasterInfo.LedgerId;
                infoPurchaseMaster.LrNo = input.PurchaseMasterInfo.LrNo;
                if (input.PurchaseModeText == "Against Material Receipt")
                {
                    infoPurchaseMaster.MaterialReceiptMasterId = input.PurchaseMasterInfo.MaterialReceiptMasterId;
                }
                else
                {
                    infoPurchaseMaster.MaterialReceiptMasterId = 0;
                }

                infoPurchaseMaster.Narration = input.PurchaseMasterInfo.Narration;
                infoPurchaseMaster.PurchaseAccount =
                    11; //Convert.ToDecimal(cmbPurchaseAccount.SelectedValue.ToString());    // Modified so as to report to Purchase Account
                if (input.PurchaseModeText == "Against Purchase Order")
                {
                    infoPurchaseMaster.PurchaseOrderMasterId = input.PurchaseMasterInfo.PurchaseOrderMasterId;
                }
                else
                {
                    infoPurchaseMaster.PurchaseOrderMasterId = 0;
                }

                /*foreach (var detail in input.PurchaseDetails)
                {
                    totalAmount += detail.Rate * detail.Qty;
                    totalDiscount += detail.Discount;
                    //totalTax += detail.TaxAmount;
                }*/

                //totalTax = input.PurchaseMasterInfo.TotalTax;
                //grandTotal = ((totalTax + totalAmount) - totalDiscount) - input.PurchaseMasterInfo.BillDiscount;
                infoPurchaseMaster.TotalAmount = input.PurchaseMasterInfo.TotalAmount - input.PurchaseMasterInfo.TotalTax;
                //infoPurchaseMaster.GrandTotal = grandTotal;
                infoPurchaseMaster.TotalTax = input.PurchaseMasterInfo.TotalTax;
                infoPurchaseMaster.TransportationCompany = input.PurchaseMasterInfo.TransportationCompany;
                infoPurchaseMaster.UserId = PublicVariables._decCurrentUserId;
                infoPurchaseMaster.VendorInvoiceDate = input.PurchaseMasterInfo.VendorInvoiceDate;
                infoPurchaseMaster.VendorInvoiceNo = input.PurchaseMasterInfo.VendorInvoiceNo;
                infoPurchaseMaster.VoucherTypeId = decPurchaseInvoiceVoucherTypeId;
                infoPurchaseMaster.Extra1 = string.Empty;
                infoPurchaseMaster.Extra2 = string.Empty;
                infoPurchaseMaster.ExtraDate = Convert.ToDateTime(DateTime.Now);
                infoPurchaseMaster.DoneBy = input.PurchaseMasterInfo.DoneBy;

                //decPurchaseMasterId = spPurchaseMaster.PurchaseMasterAdd(infoPurchaseMaster);

                decPurchaseMasterId = PurchaseMasterAdd(infoPurchaseMaster);

                infoPurchaseOrderMaster =
                    spPurchaseOrderMaster.PurchaseOrderMasterView(infoPurchaseMaster.PurchaseOrderMasterId);
                infoMaterialReceiptMaster =
                    spMaterialReceiptMaster.MaterialReceiptMasterView(infoPurchaseMaster.MaterialReceiptMasterId);




                foreach (var detail in input.PurchaseDetails)
                {
                    //check for batch, if its null 
                    //if not null , check if it exist
                    //if false create a new one base on  
                    /*-----------------------------------------Purchase Details Add----------------------------------------------------*/
                    var batchId = detail.BatchId;
                    var batch = context.tbl_Batch.Where(a => a.batchId == batchId && a.productId == detail.ProductId)
                        .FirstOrDefault();
                    if (batch == null)
                    {
                        // create a new batch 
                        var newBatch = new tbl_Batch()
                        {
                            batchNo = "AUTO_" + DateTime.UtcNow.ToShortDateString() + "_" + detail.Amount,
                            productId = detail.ProductId,
                            extraDate = DateTime.Now
                        };
                        context.tbl_Batch.Add(newBatch);
                        context.SaveChanges();
                        detail.BatchId = newBatch.batchId;
                    }

                    var product = context.tbl_Product.Find(detail.ProductId);
                    if (product != null)
                    {
                        //check for store 
                        var store = context.tbl_Godown.Find(detail.GodownId);
                        if (store == null)
                        {
                            detail.GodownId = context.tbl_Godown.FirstOrDefault().godownId;
                        }

                    }

                    //check if batch belongToA
                    infoPurchaseDetails.Amount = detail.Amount;
                    infoPurchaseDetails.BatchId = detail.BatchId;
                    infoPurchaseDetails.Discount = detail.Discount;
                    infoPurchaseDetails.GodownId = detail.GodownId;
                    infoPurchaseDetails.GrossAmount = detail.GrossAmount;
                    infoPurchaseDetails.NetAmount = detail.NetAmount;
                    infoPurchaseDetails.OrderDetailsId = detail.OrderDetailsId;
                    infoPurchaseDetails.ProductId = detail.ProductId;
                    infoPurchaseDetails.PurchaseMasterId = decPurchaseMasterId;
                    infoPurchaseDetails.Qty = detail.Qty;
                    infoPurchaseDetails.RackId = detail.RackId;
                    infoPurchaseDetails.Rate = detail.Rate;
                    infoPurchaseDetails.ReceiptDetailsId = detail.ReceiptDetailsId;
                    infoPurchaseDetails.SlNo = detail.SlNo;
                    infoPurchaseDetails.TaxAmount = (detail.TaxAmount != 0) ? detail.TaxAmount : (detail.Amount - detail.NetAmount);
                    infoPurchaseDetails.TaxId = detail.TaxId;
                    infoPurchaseDetails.UnitConversionId = detail.UnitConversionId;
                    infoPurchaseDetails.UnitId = detail.UnitId;
                    infoPurchaseDetails.Extra1 = string.Empty;
                    infoPurchaseDetails.Extra2 = string.Empty;
                    //if (dgvrow.Cells["dgvCmbProject"].Value != null && !string.IsNullOrEmpty(dgvrow.Cells["dgvCmbProject"].Value.ToString()))
                    //{
                    //    infoPurchaseDetails.ProjectId = Convert.ToInt32(dgvrow.Cells["dgvCmbProject"].Value.ToString());
                    //}
                    //else
                    //{
                    infoPurchaseDetails.ProjectId = detail.ProjectId;
                    //}
                    //if (dgvrow.Cells["dgvCmbCategory"].Value != null && !string.IsNullOrEmpty(dgvrow.Cells["dgvCmbCategory"].Value.ToString()))
                    //{
                    //    infoPurchaseDetails.CategoryId = Convert.ToInt32(dgvrow.Cells["dgvCmbCategory"].Value.ToString());
                    //}
                    //else
                    //{
                    infoPurchaseDetails.CategoryId = detail.CategoryId;
                    //}
                    //if (dgvrow.Cells["itemDescription"].Value != null && !string.IsNullOrEmpty(dgvrow.Cells["itemDescription"].Value.ToString()))
                    //{
                    infoPurchaseDetails.itemDescription = detail.itemDescription;
                    //}
                    infoPurchaseDetails.ExtraDate = Convert.ToDateTime(DateTime.Today);
                    decSalesDetailsId = spPurchaseDetails.PurchaseDetailsAdd(infoPurchaseDetails);
                    listofDetailsId.Add(decSalesDetailsId);

                    //check if its a material receipt if true return 

                    if (input.PurchaseModeText != "Against Material Receipt")
                    {
                        infoStockPosting.BatchId = infoPurchaseDetails.BatchId;
                        infoStockPosting.Date = input.PurchaseMasterInfo.Date;
                        infoStockPosting.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                        infoStockPosting.GodownId = infoPurchaseDetails.GodownId;
                        infoStockPosting.InwardQty =
                            infoPurchaseDetails
                                .Qty; /// spUnitConvertion.UnitConversionRateByUnitConversionId(infoPurchaseDetails.UnitConversionId);
                        infoStockPosting.OutwardQty = 0;
                        infoStockPosting.ProductId = infoPurchaseDetails.ProductId;
                        infoStockPosting.RackId = infoPurchaseDetails.RackId;
                        infoStockPosting.Rate = infoPurchaseDetails.Rate;
                        infoStockPosting.UnitId = infoPurchaseDetails.UnitId;
                        if (infoPurchaseDetails.OrderDetailsId != 0)
                        {
                            infoStockPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                            infoStockPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                            infoStockPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                            infoStockPosting.AgainstInvoiceNo = "NA";
                            infoStockPosting.AgainstVoucherNo = "NA";
                            infoStockPosting.AgainstVoucherTypeId = 0;
                        }
                        else if (infoPurchaseDetails.ReceiptDetailsId != 0)
                        {
                            infoStockPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                            infoStockPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                            infoStockPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                            infoStockPosting.AgainstInvoiceNo = "NA";
                            infoStockPosting.AgainstVoucherNo = "NA";
                            infoStockPosting.AgainstVoucherTypeId = 0;
                        }
                        else if (infoPurchaseDetails.OrderDetailsId == 0 && infoPurchaseDetails.ReceiptDetailsId == 0)
                        {
                            infoStockPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                            infoStockPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                            infoStockPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                            infoStockPosting.AgainstInvoiceNo = "NA";
                            infoStockPosting.AgainstVoucherNo = "NA";
                            infoStockPosting.AgainstVoucherTypeId = 0;
                        }

                        infoStockPosting.Extra1 = string.Empty;
                        infoStockPosting.Extra2 = string.Empty;
                        infoStockPosting.ExtraDate = Convert.ToDateTime(DateTime.Today);
                        var spStockPosting1 = new MatApi.Services.StockPostingSP();
                        spStockPosting1.StockPostingAdd(infoStockPosting); // old implementation 20161201
                        // Urefe added if statement so that material receipt will not affect stock when raising invoice against the material receipt 20161201, 
                        // Urefe removed comment because it should affect it +ve and -ve nilling off the effect
                        if (infoMaterialReceiptMaster.VoucherTypeId == 11)
                        {
                            spStockPosting1.StockPostingAdd(infoStockPosting);
                        }

                        if (infoPurchaseDetails.ReceiptDetailsId != 0)
                        {
                            infoStockPosting.InvoiceNo = infoMaterialReceiptMaster.InvoiceNo;
                            infoStockPosting.VoucherNo = infoMaterialReceiptMaster.VoucherNo;
                            infoStockPosting.VoucherTypeId = infoMaterialReceiptMaster.VoucherTypeId;
                            infoStockPosting.AgainstInvoiceNo = infoPurchaseMaster.InvoiceNo;
                            infoStockPosting.AgainstVoucherNo = infoPurchaseMaster.VoucherNo;
                            infoStockPosting.AgainstVoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                            infoStockPosting.InwardQty = 0;
                            infoStockPosting.OutwardQty =
                                infoPurchaseDetails
                                    .Qty; /// spUnitConvertion.UnitConversionRateByUnitConversionId(infoPurchaseDetails.UnitConversionId);
                            spStockPosting1.StockPostingAdd(infoStockPosting); // old implementation 20161201
                            // Urefe added if statement so that material receipt will not affect stock when raising invoice against the material receipt 20161201, 
                            // Urefe removed comment because it should affect it +ve and -ve nilling off the effect
                            if (infoMaterialReceiptMaster.VoucherTypeId != 11)
                            {
                                spStockPosting1.StockPostingAdd(infoStockPosting);
                            }
                        }


                    }
                }

                /*-----------------------------------------Ledger Posting----------------------------------------------------*/

                //account payable or cash ledger posting
                infoLedgerPosting.Credit = input.GrandTotal *
                                           spExchangeRate.ExchangeRateViewByExchangeRateId(input.PurchaseMasterInfo
                                               .ExchangeRateId);
                infoLedgerPosting.Debit = 0;
                //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                //infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.DetailsId = decPurchaseMasterId;
                infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                infoLedgerPosting.LedgerId = infoPurchaseMaster.LedgerId;
                infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                infoLedgerPosting.ExtraDate = DateTime.Now;
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                /// cost of sales debited
                ///

                decimal decBilldiscount = input.PurchaseMasterInfo.BillDiscount;
                if (decBilldiscount > 0)
                {
                    infoLedgerPosting.Credit = decBilldiscount *
                                               spExchangeRate.ExchangeRateViewByExchangeRateId(input.PurchaseMasterInfo
                                                   .ExchangeRateId);
                    infoLedgerPosting.Debit = 0;
                    //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                    infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                    infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                    infoLedgerPosting.LedgerId = 9; //ledger id of discount received
                    infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                    infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }

                /*-----------------------------------------PartyBalance Posting----------------------------------------------------*/
                //infoAccountLedger = spAccountLedger.AccountLedgerView(infoPurchaseMaster.LedgerId);
                ////PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
                //if (infoAccountLedger.BillByBill == true)
                //{}
                grandTotal = input.GrandTotal;
                infoPartyBalance.Credit = Convert.ToDecimal(grandTotal);
                infoPartyBalance.Debit = 0;
                if (input.PurchaseMasterInfo.CreditPeriod != string.Empty)
                {
                    infoPartyBalance.CreditPeriod = Convert.ToInt32(input.PurchaseMasterInfo.CreditPeriod);
                }

                infoPartyBalance.Date = Convert.ToDateTime(input.PurchaseMasterInfo.Date);
                infoPartyBalance.ExchangeRateId = infoPurchaseMaster.ExchangeRateId;
                infoPartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoPartyBalance.LedgerId = infoPurchaseMaster.LedgerId;
                infoPartyBalance.ReferenceType = "NEW";
                infoPartyBalance.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                infoPartyBalance.VoucherNo = infoPurchaseMaster.VoucherNo;
                infoPartyBalance.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                infoPartyBalance.AgainstInvoiceNo = "NA";
                infoPartyBalance.AgainstVoucherNo = "NA";
                infoPartyBalance.AgainstVoucherTypeId = 0;
                infoPartyBalance.Extra1 = string.Empty;
                infoPartyBalance.Extra2 = string.Empty;
                infoPartyBalance.ExtraDate = DateTime.Now;
                spPartyBalance.PartyBalanceAdd(infoPartyBalance);
                

                //  Goods in Transist
                foreach (var i in input.PurchaseDetails)
                {
                    // cost of sale dr
                    var product = context.tbl_Product.Find(i.ProductId);
                    //var ledger = context.tbl_AccountLedger.Find(product.salesAccount);
                    //    LedgerPostingSP ledgerPostingExpensAccountId = new LedgerPostingSP();
                    //    infoLedgerPosting.Credit = 0;
                    //    infoLedgerPosting.Debit = Convert.ToDecimal(product.purchaseRate) * i.Qty;   //TotalNetAmount(); //* spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(cmbCurrency.SelectedValue.ToString()));
                    //                                             //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                    //    infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                    //    //infoLedgerPosting.DetailsId = 0;
                    //    infoLedgerPosting.DetailsId = listofDetailsId[itemsinList];
                    //    infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                    //    //infoLedgerPosting.LedgerId = infoPurchaseMaster.PurchaseAccount;//ledger posting of purchase account  dgvrow.Cells["dgvcmbTax"].Value.ToString
                    //    // ----------------- Old implementation changed by Precious and then Urefe 20160919 ------------------- //
                    //    //infoLedgerPosting.LedgerId = Convert.ToDecimal(row.Cells["dgvtxtExpenseAccount"].Value.ToString()); 
                    //    //infoLedgerPosting.LedgerId = ledgerPostingExpensAccountId.ProductExpenseAccountId(row.ProductInfo.ProductCode);
                    //    infoLedgerPosting.LedgerId = Convert.ToDecimal(product.expenseAccount);
                    //   // product.expenseAccount;    //  110423;
                    //    infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                    //    infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                    //    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    //    infoLedgerPosting.ChequeDate = DateTime.Now;
                    //    infoLedgerPosting.ChequeNo = string.Empty;
                    //    infoLedgerPosting.Extra1 = string.Empty;
                    //    infoLedgerPosting.Extra2 = string.Empty;
                    //    infoLedgerPosting.ExtraDate = DateTime.Now;
                    //    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);    // post to credit side 

                    //Good in Transist from material recept
                    if (input.PurchaseModeText == "Against Material Receipt")
                    {
                        infoLedgerPosting.Credit = 0;
                        infoLedgerPosting.Debit =
                            Convert.ToDecimal(i.Rate) *
                            i.Qty; //TotalNetAmount(); //* spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(cmbCurrency.SelectedValue.ToString()));
                        //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                        infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                        //infoLedgerPosting.DetailsId = 0;
                        infoLedgerPosting.DetailsId = listofDetailsId[itemsinList];
                        infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                        //infoLedgerPosting.LedgerId = infoPurchaseMaster.PurchaseAccount;//ledger posting of purchase account  dgvrow.Cells["dgvcmbTax"].Value.ToString
                        // ----------------- Old implementation changed by Precious and then Urefe 20160919 ------------------- //
                        //infoLedgerPosting.LedgerId = Convert.ToDecimal(row.Cells["dgvtxtExpenseAccount"].Value.ToString()); 
                        //infoLedgerPosting.LedgerId = ledgerPostingExpensAccountId.ProductExpenseAccountId(row.ProductInfo.ProductCode);
                        infoLedgerPosting.LedgerId = LedgerConstants.GoodsInTransit;
                        //infoLedgerPosting.LedgerId = Convert.ToDecimal(product.expenseAccount);
                        // product.expenseAccount;    //  110423;
                        infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                        infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                        infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                        infoLedgerPosting.ChequeDate = DateTime.Now;
                        infoLedgerPosting.ChequeNo = string.Empty;
                        infoLedgerPosting.Extra1 = string.Empty;
                        infoLedgerPosting.Extra2 = string.Empty;
                        infoLedgerPosting.ExtraDate = DateTime.Now;
                        spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                    }

                    foreach (var additionalCost in input.AdditionalCostInfo)
                    {
                        /*-----------------------------------------Additional Cost Add----------------------------------------------------*/
                        infoAdditionalCost.Credit = 0;
                        infoAdditionalCost.Debit = additionalCost.Debit;
                        infoAdditionalCost.LedgerId = additionalCost.LedgerId;
                        infoAdditionalCost.VoucherNo = infoPurchaseMaster.VoucherNo;
                        infoAdditionalCost.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                        infoAdditionalCost.Extra1 = string.Empty;
                        infoAdditionalCost.Extra2 = string.Empty;
                        infoAdditionalCost.ExtraDate = DateTime.Now;
                        spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                        /*-----------------------------------------Additional Cost Ledger Posting----------------------------------------------------*/
                        infoLedgerPosting.Credit = 0;
                        infoLedgerPosting.Debit = infoAdditionalCost.Debit *
                                                  spExchangeRate.ExchangeRateViewByExchangeRateId(
                                                      input.PurchaseMasterInfo.ExchangeRateId);
                        //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                        infoLedgerPosting.Date = Convert.ToDateTime(input.PurchaseMasterInfo.Date);
                        infoLedgerPosting.DetailsId = 0;
                        infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                        infoLedgerPosting.LedgerId = infoAdditionalCost.LedgerId;
                        infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                        infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                        infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                        infoLedgerPosting.ChequeDate = DateTime.Now;
                        infoLedgerPosting.ChequeNo = string.Empty;
                        infoLedgerPosting.Extra1 = string.Empty;
                        infoLedgerPosting.Extra2 = string.Empty;
                        infoLedgerPosting.ExtraDate = DateTime.Now;
                        spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                    }

                    foreach (var tax in input.PurchaseBillTaxInfo)
                    {
                        /*-----------------------------------------PurchaseBillTax Add----------------------------------------------------*/
                        infoPurchaseBillTax.PurchaseMasterId = decPurchaseMasterId;
                        infoPurchaseBillTax.TaxAmount = tax.TaxAmount;
                        infoPurchaseBillTax.TaxId = tax.TaxId;
                        infoPurchaseBillTax.Extra1 = string.Empty;
                        infoPurchaseBillTax.Extra2 = string.Empty;
                        infoPurchaseBillTax.ExtraDate = DateTime.Now;
                        spPurchaseBillTax.PurchaseBillTaxAdd(infoPurchaseBillTax);
                        /*-----------------------------------------Tax Ledger Posting----------------------------------------------------*/
                        infoLedgerPosting.Credit = 0;
                        infoLedgerPosting.Debit = infoPurchaseBillTax.TaxAmount *
                                                  spExchangeRate.ExchangeRateViewByExchangeRateId(
                                                      input.PurchaseMasterInfo.ExchangeRateId);
                        //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                        infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                        infoLedgerPosting.DetailsId = 0;
                        infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                        infoLedgerPosting.LedgerId = 59;
                        infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                        infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                        infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                        infoLedgerPosting.ChequeDate = DateTime.Now;
                        infoLedgerPosting.ChequeNo = string.Empty;
                        infoLedgerPosting.Extra1 = string.Empty;
                        infoLedgerPosting.Extra2 = string.Empty;
                        infoLedgerPosting.ExtraDate = DateTime.Now;
                        spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                    }

                    ///*-----------------------------------------PartyBalance Posting----------------------------------------------------*/
                    //infoAccountLedger = spAccountLedger.AccountLedgerView(infoPurchaseMaster.LedgerId);
                    ////PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
                    //if (infoAccountLedger.BillByBill == true)
                    //{
                    //    grandTotal = input.GrandTotal;
                    //    infoPartyBalance.Credit = Convert.ToDecimal(grandTotal);
                    //    infoPartyBalance.Debit = 0;
                    //    if (input.PurchaseMasterInfo.CreditPeriod != string.Empty)
                    //    {
                    //        infoPartyBalance.CreditPeriod = Convert.ToInt32(input.PurchaseMasterInfo.CreditPeriod);
                    //    }

                    //    infoPartyBalance.Date = Convert.ToDateTime(input.PurchaseMasterInfo.Date);
                    //    infoPartyBalance.ExchangeRateId = infoPurchaseMaster.ExchangeRateId;
                    //    infoPartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    //    infoPartyBalance.LedgerId = infoPurchaseMaster.LedgerId;
                    //    infoPartyBalance.ReferenceType = "NEW";
                    //    infoPartyBalance.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                    //    infoPartyBalance.VoucherNo = infoPurchaseMaster.VoucherNo;
                    //    infoPartyBalance.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                    //    infoPartyBalance.AgainstInvoiceNo = "NA";
                    //    infoPartyBalance.AgainstVoucherNo = "NA";
                    //    infoPartyBalance.AgainstVoucherTypeId = 0;
                    //    infoPartyBalance.Extra1 = string.Empty;
                    //    infoPartyBalance.Extra2 = string.Empty;
                    //    infoPartyBalance.ExtraDate = DateTime.Now;
                    //    spPartyBalance.PartyBalanceAdd(infoPartyBalance);
                    //}

                    applyDebitNote(input);

                }

                if (input.PurchaseModeText == "Against Material Receipt")
                {
                    return "Against Material Receipt Save successfully";
                }

                 return "Purchase Order Save successfully";
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "PI38:" + ex.Message;
            }


            return "GENERAL_FAILURE";
        }

        protected void applyDebitNote(PurchaseInvoiceVM input)
        {
            #region apply credit against supplier
            DebitNoteMasterSP spDebitnoteMaster = new DebitNoteMasterSP();
            DebitNoteMasterInfo infoDebitNoteMaster = new DebitNoteMasterInfo();
            DebitNoteDetailsSP spDebitNoteDetails = new DebitNoteDetailsSP();
            DebitNoteDetailsInfo infoDebitNoteDetails = new DebitNoteDetailsInfo();
            PaymentMasterInfo infoPaymentMaster = new PaymentMasterInfo();
            PaymentDetailsInfo infoPaymentDetails = new PaymentDetailsInfo();
            PaymentMasterSP spPaymentMaster = new PaymentMasterSP();
            PaymentDetailsSP spPaymentDetails = new PaymentDetailsSP();
            JournalMasterInfo infoJournalMaster = new JournalMasterInfo();
            JournalDetailsInfo infoJournalDetails = new JournalDetailsInfo();
            JournalMasterSP spJournalMaster = new JournalMasterSP();
            JournalDetailsSP spJournalDetails = new JournalDetailsSP();
            PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            HelperClasses helperClasses = new HelperClasses();
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            PostingsHelper PostingHelper = new PostingsHelper();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            DataTable dt = new DataTable();
            decimal decSelectedCurrencyRate = 1;
            decimal existingLedgerId = 0;
            bool isRowAffected = false;

            #region Working region

            bool createNewDebitNote = false;
            DataTable dtAdvancePayments = new DataTable();
            dtAdvancePayments.Columns.Add("voucherTypeId", typeof(string));
            dtAdvancePayments.Columns.Add("display", typeof(string));
            dtAdvancePayments.Columns.Add("voucherNo", typeof(string));
            dtAdvancePayments.Columns.Add("balance", typeof(decimal));
            dtAdvancePayments.Columns.Add("exchangeRateId", typeof(string));
            dtAdvancePayments.Columns.Add("invoiceNo", typeof(string));
            dtAdvancePayments.Columns.Add("AmountToApply", typeof(decimal));

            foreach (var advance in input.AdvancePayment)
            {
                DataRow dr = dtAdvancePayments.NewRow();
                dr[0] = advance.VoucherTypeId.ToString();
                dr[1] = advance.Display.ToString();
                dr[2] = advance.VoucherNo.ToString();
                dr[3] = advance.Balance.ToString();
                dr[4] = advance.ExchangeRateId.ToString();
                dr[5] = advance.InvoiceNo.ToString();
                dr[6] = advance.Balance.ToString();
                dtAdvancePayments.Rows.Add(dr);
            }

            if (dtAdvancePayments.Rows.Count > 0)
            {
                decimal totalAdvancePaymentToApply = (from t in dtAdvancePayments.AsEnumerable()
                                                      select t.Field<decimal>("AmountToApply")).Sum();
                decimal totalInvoice = Convert.ToDecimal(input.GrandTotal);

                foreach (var advance in input.AdvancePayment)
                {
                    createNewDebitNote = false;
                    decimal voucherTypeId = Convert.ToDecimal(advance.VoucherTypeId);
                    string voucherNo = advance.VoucherNo;
                    decimal balance = Convert.ToDecimal(advance.Balance);
                    decimal amountToApply = Convert.ToDecimal(advance.AmountToApply);
                    if (balance > amountToApply)
                    {
                        createNewDebitNote = true;
                    }

                    dt = spPartyBalance.PartyBalanceViewByVoucherNoAndVoucherType(voucherTypeId, voucherNo, DateTime.Now);
                    //decimal decPartyBalanceId = Convert.ToDecimal(dt.Rows[0]["PartyBalanceId"].ToString());

                    TotalAmount = balance > amountToApply ? amountToApply : balance;

                    #region Used code
                    if (voucherTypeId == 23)        //debit note
                    {
                        bool DebitMasterEdit = false;
                        infoDebitNoteMaster.DebitNoteMasterId = helperClasses.DebitNoteMasterId(voucherNo.ToString());
                        infoDebitNoteMaster = spDebitnoteMaster.DebitNoteMasterView(infoDebitNoteMaster.DebitNoteMasterId);

                        infoDebitNoteMaster.VoucherNo = infoDebitNoteMaster.VoucherNo;
                        infoDebitNoteMaster.InvoiceNo = infoDebitNoteMaster.InvoiceNo;
                        infoDebitNoteMaster.SuffixPrefixId = infoDebitNoteMaster.SuffixPrefixId;
                        infoDebitNoteMaster.Date = Convert.ToDateTime(input.PurchaseMasterInfo.Date);
                        infoDebitNoteMaster.Narration = input.PurchaseMasterInfo.Narration;
                        infoDebitNoteMaster.UserId = PublicVariables._decCurrentUserId;
                        infoDebitNoteMaster.VoucherTypeId = infoDebitNoteMaster.VoucherTypeId;
                        infoDebitNoteMaster.FinancialYearId = Convert.ToDecimal(PublicVariables._decCurrentFinancialYearId.ToString());
                        infoDebitNoteMaster.ExtraDate = DateTime.Now;
                        infoDebitNoteMaster.Extra1 = string.Empty;
                        infoDebitNoteMaster.Extra2 = string.Empty;
                        infoDebitNoteMaster.TotalAmount = TotalAmount;
                        DebitMasterEdit = true;
                        decimal noofRowsAffected = spDebitnoteMaster.DebitNoteMasterEdit(infoDebitNoteMaster);
                        if (noofRowsAffected > 0)
                        {
                            isRowAffected = true;
                        }
                        if (isRowAffected == true)
                        {
                            dt = spDebitNoteDetails.DebitNoteDetailsViewByMasterId(infoDebitNoteMaster.DebitNoteMasterId);
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                infoDebitNoteDetails.ChequeDate = Convert.ToDateTime(input.PurchaseMasterInfo.Date);
                                infoDebitNoteDetails.ChequeNo = string.Empty;
                                if (Convert.ToDecimal(dt.Rows[i]["debit"].ToString()) == 0)
                                {
                                    infoDebitNoteDetails.Credit = TotalAmount;
                                    infoDebitNoteDetails.Debit = 0;
                                    infoDebitNoteDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                    existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                }
                                else
                                {
                                    infoDebitNoteDetails.Credit = 0;
                                    infoDebitNoteDetails.Debit = TotalAmount;
                                    infoDebitNoteDetails.LedgerId = input.PurchaseMasterInfo.LedgerId;
                                    //existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                }
                                infoDebitNoteDetails.DebitNoteDetailsId = Convert.ToDecimal(dt.Rows[i]["DebitNoteDetailsId"].ToString());
                                infoDebitNoteDetails.DebitNoteMasterId = infoDebitNoteMaster.DebitNoteMasterId;
                                infoDebitNoteDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
                                infoDebitNoteDetails.Extra1 = string.Empty;
                                infoDebitNoteDetails.Extra2 = string.Empty;
                                infoDebitNoteDetails.ExtraDate = DateTime.Now;
                                DebitMasterEdit = false;
                                //------------------Currency conversion------------------//
                                decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoDebitNoteDetails.ExchangeRateId);
                                PostingHelper.DebitNoteDetailsAddOrEdit(infoDebitNoteMaster, infoDebitNoteDetails, DebitMasterEdit, decSelectedCurrencyRate, input.PurchaseMasterInfo.VoucherNo, decPurchaseInvoiceVoucherTypeId);
                            }
                        }

                        #endregion

                        #region Create new Debit note
                        if (createNewDebitNote)
                        {
                            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                            TransactionsGeneralFill obj = new TransactionsGeneralFill();
                            spDebitnoteMaster = new DebitNoteMasterSP();
                            string strNewDebitNoteVoucherNo = string.Empty;
                            string strNewDebitNoteInvoiceNo = string.Empty;

                            if (strNewDebitNoteVoucherNo == string.Empty)
                            {
                                strNewDebitNoteVoucherNo = "0"; //strMax;
                            }
                            //===================================================================================================================//

                            VoucherTypeSP spVoucherType = new VoucherTypeSP();
                            bool isAutomaticForDebitNote = spVoucherType.CheckMethodOfVoucherNumbering(23);
                            if (isAutomaticForDebitNote)
                            {
                                strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), input.PurchaseMasterInfo.Date, strDebitNoteMasterTableName);
                                if (Convert.ToDecimal(strNewDebitNoteVoucherNo) != spDebitnoteMaster.DebitNoteMasterGetMaxPlusOne(23))
                                {
                                    strNewDebitNoteVoucherNo = spDebitnoteMaster.DebitNoteMasterGetMax(23).ToString();
                                    strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), input.PurchaseMasterInfo.Date, strDebitNoteMasterTableName);
                                    if (spDebitnoteMaster.DebitNoteMasterGetMax(23).ToString() == "0")
                                    {
                                        strNewDebitNoteVoucherNo = "0";
                                        strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), input.PurchaseMasterInfo.Date, strDebitNoteMasterTableName);
                                    }
                                }
                                infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(23, input.PurchaseMasterInfo.Date);
                                strPrefix = infoSuffixPrefix.Prefix;
                                strSuffix = infoSuffixPrefix.Suffix;
                                strNewDebitNoteInvoiceNo = strPrefix + strNewDebitNoteVoucherNo + strSuffix;
                                //txtVoucherNo.Text = strInvoiceNo;
                                //txtVoucherNo.ReadOnly = true;
                            }
                            else
                            {
                                //txtVoucherNo.ReadOnly = false;
                                //txtVoucherNo.Text = string.Empty;
                                strNewDebitNoteVoucherNo = input.PurchaseMasterInfo.VoucherNo + "_Dr";
                                strNewDebitNoteInvoiceNo = strNewDebitNoteVoucherNo;
                            }
                            // ============================ Debit note master add =========================== //
                            infoDebitNoteMaster.VoucherNo = strNewDebitNoteVoucherNo;
                            infoDebitNoteMaster.InvoiceNo = strNewDebitNoteVoucherNo;
                            infoDebitNoteMaster.SuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                            infoDebitNoteMaster.Date = input.PurchaseMasterInfo.Date;
                            infoDebitNoteMaster.Narration = input.PurchaseMasterInfo.Narration;
                            infoDebitNoteMaster.UserId = PublicVariables._decCurrentUserId;
                            infoDebitNoteMaster.VoucherTypeId = 23;
                            infoDebitNoteMaster.FinancialYearId = Convert.ToDecimal(PublicVariables._decCurrentFinancialYearId.ToString());
                            infoDebitNoteMaster.Extra1 = string.Empty;
                            infoDebitNoteMaster.Extra2 = string.Empty;
                            infoDebitNoteMaster.TotalAmount = balance - amountToApply;
                            decimal decDebitNoteMasterId = spDebitnoteMaster.DebitNoteMasterAdd(infoDebitNoteMaster);

                            // ========================== DebitNote Details Add =============================== //
                            spDebitNoteDetails = new DebitNoteDetailsSP();
                            spLedgerPosting = new LedgerPostingSP();
                            infoLedgerPosting = new LedgerPostingInfo();
                            decimal decLedgerId = 0;
                            decimal decDebit = 0;
                            decimal decCredit = 0;
                            try
                            {
                                infoDebitNoteDetails.DebitNoteMasterId = decDebitNoteMasterId;
                                infoDebitNoteDetails.ExchangeRateId = ExchangeRateId;
                                infoDebitNoteDetails.ExtraDate = DateTime.Now;
                                infoDebitNoteDetails.Extra1 = string.Empty;
                                infoDebitNoteDetails.Extra2 = string.Empty;
                                infoDebitNoteDetails.LedgerId = input.PurchaseMasterInfo.LedgerId;
                                decLedgerId = infoDebitNoteDetails.LedgerId;
                                infoDebitNoteDetails.Debit = balance - amountToApply;
                                infoDebitNoteDetails.Credit = 0;
                                decDebit = infoDebitNoteDetails.Debit * decSelectedCurrencyRate;
                                decCredit = infoDebitNoteDetails.Credit * decSelectedCurrencyRate;
                                infoDebitNoteDetails.ChequeNo = string.Empty;
                                infoDebitNoteDetails.ChequeDate = input.PurchaseMasterInfo.Date;
                                //-------------------- debit leg ---------------------------- //
                                decimal decDebitDetailsId = spDebitNoteDetails.DebitNoteDetailsAdd(infoDebitNoteDetails);

                                infoDebitNoteDetails.LedgerId = 1;
                                decLedgerId = infoDebitNoteDetails.LedgerId;
                                infoDebitNoteDetails.Debit = 0;
                                infoDebitNoteDetails.Credit = balance - amountToApply;
                                decDebit = infoDebitNoteDetails.Debit * decSelectedCurrencyRate;
                                decCredit = infoDebitNoteDetails.Credit * decSelectedCurrencyRate;

                                // -------------------- credit leg --------------------------- //
                                decimal decCreditDetailsId = spDebitNoteDetails.DebitNoteDetailsAdd(infoDebitNoteDetails);

                                // -------------------------- party balance add --------------------------- //
                                spPartyBalance = new PartyBalanceSP();
                                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                                InfopartyBalance.CreditPeriod = 0;//
                                InfopartyBalance.Date = input.PurchaseMasterInfo.Date;
                                InfopartyBalance.LedgerId = input.PurchaseMasterInfo.LedgerId;
                                InfopartyBalance.ReferenceType = "OnAccount";
                                InfopartyBalance.AgainstInvoiceNo = "0";
                                InfopartyBalance.AgainstVoucherNo = "0";
                                InfopartyBalance.AgainstVoucherTypeId = 0;
                                InfopartyBalance.VoucherTypeId = 23;
                                InfopartyBalance.InvoiceNo = strNewDebitNoteVoucherNo;
                                InfopartyBalance.VoucherNo = strNewDebitNoteVoucherNo;
                                InfopartyBalance.Credit = 0;
                                InfopartyBalance.Debit = balance - amountToApply;
                                InfopartyBalance.ExchangeRateId = 1;
                                InfopartyBalance.Extra1 = string.Empty;
                                InfopartyBalance.Extra2 = string.Empty;
                                InfopartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                                PostingHelper.PartyBalanceAdd(InfopartyBalance);

                                // =============================== ledger posting ==================================== //
                                infoLedgerPosting.ChequeDate = input.PurchaseMasterInfo.Date;
                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.Credit = 0;
                                infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                                infoLedgerPosting.Debit = balance - amountToApply;
                                infoLedgerPosting.DetailsId = decCreditDetailsId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.InvoiceNo = strNewDebitNoteVoucherNo;
                                infoLedgerPosting.LedgerId = input.PurchaseMasterInfo.LedgerId;
                                infoLedgerPosting.VoucherNo = strNewDebitNoteVoucherNo;
                                infoLedgerPosting.VoucherTypeId = 23;
                                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                                // --------------- debit leg -------------------------------------------------- //
                                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                                // --------------- credit leg --------------------------------------------------- //
                                infoLedgerPosting.Credit = balance - amountToApply;
                                infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                                infoLedgerPosting.Debit = 0;
                                infoLedgerPosting.DetailsId = decDebitDetailsId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.LedgerId = existingLedgerId;
                                infoLedgerPosting.VoucherNo = strNewDebitNoteVoucherNo;
                                infoLedgerPosting.VoucherTypeId = 23;
                                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                            }
                            catch (Exception ex) { }
                            createNewDebitNote = false;
                            #endregion
                        }
                    }
                    #region Used code
                    if (voucherTypeId == 4) // Payment voucher
                    {
                        bool PaymentMasterEdit = false;
                        comStr = string.Format("SELECT paymentMasterId FROM tbl_PaymentMaster WHERE " +
                            "voucherNo = '{0}' ", voucherNo.ToString());
                        infoPaymentMaster.PaymentMasterId = helperClasses.GetMasterId(comStr);
                        infoPaymentMaster = spPaymentMaster.PaymentMasterView(infoPaymentMaster.PaymentMasterId);

                        infoPaymentMaster.VoucherNo = infoPaymentMaster.VoucherNo;
                        infoPaymentMaster.InvoiceNo = infoPaymentMaster.InvoiceNo;
                        infoPaymentMaster.SuffixPrefixId = infoPaymentMaster.SuffixPrefixId;
                        infoPaymentMaster.Date = input.PurchaseMasterInfo.Date;
                        infoPaymentMaster.Narration = input.PurchaseMasterInfo.Narration;
                        infoPaymentMaster.UserId = PublicVariables._decCurrentUserId;
                        infoPaymentMaster.VoucherTypeId = infoPaymentMaster.VoucherTypeId;
                        infoPaymentMaster.FinancialYearId = Convert.ToDecimal(PublicVariables._decCurrentFinancialYearId.ToString());
                        infoPaymentMaster.ExtraDate = DateTime.Now;
                        infoPaymentMaster.Extra1 = string.Empty;
                        infoPaymentMaster.Extra2 = string.Empty;
                        infoPaymentMaster.TotalAmount = TotalAmount;
                        PaymentMasterEdit = true;
                        //bool isRowAffected = PostingHelper.ReceiptMasterEdit(infoPaymentMaster, decSelectedCurrencyRate);
                        isRowAffected = PostingHelper.PaymentMasterEdit(infoPaymentMaster, decSelectedCurrencyRate);

                        if (isRowAffected == true)
                        {
                            dt = spPaymentDetails.PaymentDetailsViewByMasterId(infoPaymentMaster.PaymentMasterId);
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                infoPaymentDetails.Amount = amountToApply;
                                infoPaymentDetails.ChequeDate = input.PurchaseMasterInfo.Date;
                                infoPaymentDetails.ChequeNo = string.Empty;
                                infoPaymentDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                infoPaymentDetails.PaymentDetailsId = Convert.ToDecimal(dt.Rows[i]["ReceiptDetailsId"].ToString());
                                infoPaymentDetails.PaymentMasterId = infoPaymentMaster.PaymentMasterId;
                                infoPaymentDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
                                infoPaymentDetails.Extra1 = string.Empty;
                                infoPaymentDetails.Extra2 = string.Empty;
                                infoPaymentDetails.ExtraDate = DateTime.Now;
                                PaymentMasterEdit = false;
                                //------------------Currency conversion------------------//
                                decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoPaymentDetails.ExchangeRateId);
                                PostingHelper.PaymentDetailsEdit(infoPaymentMaster, infoPaymentDetails, PaymentMasterEdit, decSelectedCurrencyRate, input.PurchaseMasterInfo.VoucherNo, decPurchaseInvoiceVoucherTypeId);
                            }
                        }
                    }
                    if (voucherTypeId == 6) // Journal voucher
                    {
                        bool JournalMasterEdit = false;
                        comStr = string.Format("SELECT journalMasterId FROM tbl_JournalMaster WHERE " +
                        "voucherNo = '{0}' ", voucherNo.ToString());
                        infoJournalMaster.JournalMasterId = helperClasses.GetMasterId(comStr);
                        infoJournalMaster = spJournalMaster.JournalMasterView(infoJournalMaster.JournalMasterId);

                        infoJournalMaster.Date = input.PurchaseMasterInfo.Date;
                        infoJournalMaster.Narration = input.PurchaseMasterInfo.Narration;
                        infoJournalMaster.UserId = PublicVariables._decCurrentUserId;
                        infoJournalMaster.FinancialYearId = Convert.ToDecimal(PublicVariables._decCurrentFinancialYearId.ToString());
                        infoJournalMaster.ExtraDate = DateTime.Now;
                        infoJournalMaster.TotalAmount = TotalAmount;
                        JournalMasterEdit = true;
                        bool isRowEdited = PostingHelper.JournalMasterEdit(infoJournalMaster, decSelectedCurrencyRate);

                        if (isRowEdited == true)
                        {
                            dt = new DataTable();
                            dt = spJournalDetails.JournalDetailsViewByMasterId(infoJournalMaster.JournalMasterId);
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (Convert.ToDecimal(dt.Rows[i]["credit"].ToString()) == 0)
                                {
                                    infoJournalDetails.Credit = 0;
                                    infoJournalDetails.Debit = TotalAmount;
                                    infoJournalDetails.LedgerId = input.PurchaseMasterInfo.LedgerId;
                                }
                                else
                                {
                                    infoJournalDetails.Credit = TotalAmount;
                                    infoJournalDetails.Debit = 0;
                                    infoJournalDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                    existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                }

                                infoJournalDetails.ChequeDate = input.PurchaseMasterInfo.Date;
                                infoJournalDetails.ChequeNo = string.Empty;
                                infoJournalDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                infoJournalDetails.JournalMasterId = infoJournalMaster.JournalMasterId;
                                infoJournalDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
                                infoJournalDetails.Extra1 = string.Empty;
                                infoJournalDetails.Extra2 = string.Empty;
                                infoJournalDetails.Memo = dt.Rows[i]["Memo"].ToString();
                                infoJournalDetails.ExtraDate = DateTime.Now;
                                JournalMasterEdit = false;
                                //------------------Currency conversion------------------//
                                decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoJournalDetails.ExchangeRateId);
                                /*
                                 *  1. using ledgerId, get accountLedgerEntity
                                 *  2. fetch accountgroup for the ledger 
                                 *  3. if accountLedger.accountgroupid is not account payable id, set false, else set true || for 'Account Payables' | 'Account Receivables'
                                 */
                                var accountLedgerEntity = context.tbl_AccountLedger.AsEnumerable().FirstOrDefault(x => x.ledgerId == infoJournalDetails.LedgerId);
                                if (accountLedgerEntity == null) throw new Exception($"Error fetching Account Ledger with ledger id: {infoJournalDetails.LedgerId}");
                                var ledgerAccountGroup = context.tbl_AccountGroup.AsEnumerable().FirstOrDefault(x => x.accountGroupId == accountLedgerEntity.accountGroupId);
                                if (ledgerAccountGroup == null) throw new Exception($"Error fetching Account Group for  Ledger with ledger id: {infoJournalDetails.LedgerId}");
                                var isPayable = ledgerAccountGroup.accountGroupName.Trim() == "Account Payables" ? true : false;

                                PostingHelper.JournalDetailsEdit(infoJournalMaster, infoJournalDetails, isPayable, decSelectedCurrencyRate, input.PurchaseMasterInfo.VoucherNo, decPurchaseInvoiceVoucherTypeId);
                            }
                        }
                    }
                    #endregion

                    #region Create new credit note
                    if (createNewDebitNote)
                    {
                        SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                        SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                        TransactionsGeneralFill obj = new TransactionsGeneralFill();
                        DebitNoteMasterSP spDebitNoteMaster = new DebitNoteMasterSP();
                        string strNewDebitNoteVoucherNo = string.Empty;
                        string strNewDebitNoteInvoiceNo = string.Empty;

                        if (strNewDebitNoteVoucherNo == string.Empty)
                        {
                            strNewDebitNoteVoucherNo = "0"; //strMax;
                        }
                        //===================================================================================================================//

                        VoucherTypeSP spVoucherType = new VoucherTypeSP();
                        bool isAutomaticForDebitNote = spVoucherType.CheckMethodOfVoucherNumbering(23);
                        if (isAutomaticForDebitNote)
                        {
                            strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), input.PurchaseMasterInfo.Date, strDebitNoteMasterTableName);
                            if (Convert.ToDecimal(strNewDebitNoteVoucherNo) != spDebitnoteMaster.DebitNoteMasterGetMaxPlusOne(23))
                            {
                                strNewDebitNoteVoucherNo = spDebitnoteMaster.DebitNoteMasterGetMax(23).ToString();
                                strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), input.PurchaseMasterInfo.Date, strDebitNoteMasterTableName);
                                if (spDebitnoteMaster.DebitNoteMasterGetMax(23).ToString() == "0")
                                {
                                    strNewDebitNoteVoucherNo = "0";
                                    strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), input.PurchaseMasterInfo.Date, strDebitNoteMasterTableName);
                                }
                            }
                            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(23, input.PurchaseMasterInfo.Date);
                            strPrefix = infoSuffixPrefix.Prefix;
                            strSuffix = infoSuffixPrefix.Suffix;
                            strNewDebitNoteInvoiceNo = strPrefix + strNewDebitNoteVoucherNo + strSuffix;
                        }
                        else
                        {
                            strNewDebitNoteVoucherNo = input.PurchaseMasterInfo.VoucherNo + "_Dr";
                            strNewDebitNoteInvoiceNo = strNewDebitNoteVoucherNo;
                        }
                        // ============================ Debit note master add =========================== //
                        infoDebitNoteMaster.VoucherNo = strNewDebitNoteVoucherNo;
                        infoDebitNoteMaster.InvoiceNo = strNewDebitNoteVoucherNo;
                        infoDebitNoteMaster.SuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                        infoDebitNoteMaster.Date = input.PurchaseMasterInfo.Date;
                        infoDebitNoteMaster.Narration = input.PurchaseMasterInfo.Narration;
                        infoDebitNoteMaster.UserId = PublicVariables._decCurrentUserId;
                        infoDebitNoteMaster.VoucherTypeId = 23;
                        infoDebitNoteMaster.FinancialYearId = Convert.ToDecimal(PublicVariables._decCurrentFinancialYearId.ToString());
                        infoDebitNoteMaster.Extra1 = string.Empty;
                        infoDebitNoteMaster.Extra2 = string.Empty;
                        infoDebitNoteMaster.TotalAmount = balance - amountToApply;
                        decimal decDebitNoteMasterId = spDebitnoteMaster.DebitNoteMasterAdd(infoDebitNoteMaster);

                        // ========================== DebitNote Details Add =============================== //
                        spDebitNoteDetails = new DebitNoteDetailsSP();
                        spLedgerPosting = new LedgerPostingSP();
                        infoLedgerPosting = new LedgerPostingInfo();
                        decimal decLedgerId = 0;
                        decimal decDebit = 0;
                        decimal decCredit = 0;
                        try
                        {
                            infoDebitNoteDetails.DebitNoteMasterId = decDebitNoteMasterId;
                            infoDebitNoteDetails.ExchangeRateId = ExchangeRateId;
                            infoDebitNoteDetails.ExtraDate = DateTime.Now;
                            infoDebitNoteDetails.Extra1 = string.Empty;
                            infoDebitNoteDetails.Extra2 = string.Empty;
                            infoDebitNoteDetails.LedgerId = input.PurchaseMasterInfo.LedgerId;
                            decLedgerId = infoDebitNoteDetails.LedgerId;
                            infoDebitNoteDetails.Debit = 0;
                            infoDebitNoteDetails.Credit = balance - amountToApply;
                            decDebit = infoDebitNoteDetails.Debit * decSelectedCurrencyRate;
                            decCredit = infoDebitNoteDetails.Credit * decSelectedCurrencyRate;
                            infoDebitNoteDetails.ChequeNo = string.Empty;
                            infoDebitNoteDetails.ChequeDate = input.PurchaseMasterInfo.Date;
                            //-------------------- credit leg ---------------------------- //
                            decimal decCreditDetailsId = spDebitNoteDetails.DebitNoteDetailsAdd(infoDebitNoteDetails);

                            infoDebitNoteDetails.LedgerId = 1;
                            decLedgerId = infoDebitNoteDetails.LedgerId;
                            infoDebitNoteDetails.Debit = balance - amountToApply;
                            infoDebitNoteDetails.Credit = 0;
                            decDebit = infoDebitNoteDetails.Debit * decSelectedCurrencyRate;
                            decCredit = infoDebitNoteDetails.Credit * decSelectedCurrencyRate;

                            // -------------------- debit leg --------------------------- //
                            decimal decDebitDetailsId = spDebitNoteDetails.DebitNoteDetailsAdd(infoDebitNoteDetails);

                            // -------------------------- party balance add --------------------------- //
                            spPartyBalance = new PartyBalanceSP();
                            PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                            InfopartyBalance.CreditPeriod = 0;//
                            InfopartyBalance.Date = input.PurchaseMasterInfo.Date;
                            InfopartyBalance.LedgerId = input.PurchaseMasterInfo.LedgerId;
                            InfopartyBalance.ReferenceType = "OnAccount";
                            InfopartyBalance.AgainstInvoiceNo = "0";
                            InfopartyBalance.AgainstVoucherNo = "0";
                            InfopartyBalance.AgainstVoucherTypeId = 0;
                            InfopartyBalance.VoucherTypeId = 23;
                            InfopartyBalance.InvoiceNo = strNewDebitNoteVoucherNo;
                            InfopartyBalance.VoucherNo = strNewDebitNoteVoucherNo;
                            InfopartyBalance.Credit = balance - amountToApply;
                            InfopartyBalance.Debit = 0;
                            InfopartyBalance.ExchangeRateId = 1;
                            InfopartyBalance.Extra1 = string.Empty;
                            InfopartyBalance.Extra2 = string.Empty;
                            InfopartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            PostingHelper.PartyBalanceAdd(InfopartyBalance);

                            // =============================== ledger posting ==================================== //
                            infoLedgerPosting.ChequeDate = input.PurchaseMasterInfo.Date;
                            infoLedgerPosting.ChequeNo = string.Empty;
                            infoLedgerPosting.Credit = 0;
                            infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                            infoLedgerPosting.Debit = balance - amountToApply;
                            infoLedgerPosting.DetailsId = decCreditDetailsId;
                            infoLedgerPosting.Extra1 = string.Empty;
                            infoLedgerPosting.Extra2 = string.Empty;
                            infoLedgerPosting.ExtraDate = DateTime.Now;
                            infoLedgerPosting.InvoiceNo = strNewDebitNoteVoucherNo;
                            infoLedgerPosting.LedgerId = input.PurchaseMasterInfo.LedgerId;
                            infoLedgerPosting.VoucherNo = strNewDebitNoteVoucherNo;
                            infoLedgerPosting.VoucherTypeId = 23;
                            infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                            // --------------- debit leg -------------------------------------------------- //
                            //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                            // --------------- credit leg --------------------------------------------------- //
                            infoLedgerPosting.Credit = balance - amountToApply;
                            infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                            infoLedgerPosting.Debit = 0;
                            infoLedgerPosting.DetailsId = decDebitDetailsId;
                            infoLedgerPosting.Extra1 = string.Empty;
                            infoLedgerPosting.LedgerId = existingLedgerId;
                            infoLedgerPosting.VoucherNo = strNewDebitNoteVoucherNo;
                            infoLedgerPosting.VoucherTypeId = 23;
                            //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                        }
                        catch (Exception ex)
                        {
                            //formMDI.infoError.ErrorString = "PI135:" + ex.Message;
                        }
                        #endregion
                    }
                }
            }
            #endregion
            #endregion
        }

        [HttpGet]
        public DataTable GetPurchaseInvoicetLineItemFromMaterialReceipt(decimal receiptMasterId)
        {
            var dtblDetails = new MaterialReceiptDetailsSP().MaterialReceiptDetailsViewByMaterialReceiptMasterIdWithRemainingByNotInCurrPI
                                    (receiptMasterId, 0, decPurchaseInvoiceVoucherTypeId);
            return dtblDetails;
        }

        [HttpGet]
        public DataTable GetPurchaseInvoiceLineItemFromPurchaseOrder(decimal orderMasterId)
        {

            var dtblDetails = new PurchaseOrderDetailsSP().PurchaseOrderDetailsViewByOrderMasterIdWithRemainingByNotInCurrPI(orderMasterId, 0, decPurchaseInvoiceVoucherTypeId);
            return dtblDetails;
        }

        [HttpGet]
        public string GetAutoFormNo()
        {
            return AutoGenerateFormNo();
        }

        public IHttpActionResult GetDetails(int id)
        {
            context = new DBMATAccounting_MagnetEntities1();
            context.Configuration.LazyLoadingEnabled = false;
            context.Configuration.ProxyCreationEnabled = false;

            var details = context.tbl_PurchaseDetails
                .Include(a => a.tbl_PurchaseMaster)
                .Include(a => a.tbl_Product)
                .Where(a => a.purchaseMasterId == id).ToList();

            var record = new
            {
                details = details.Select(a => new
                {
                    detail = a,
                    product = a.tbl_Product
                }),
                master = details.Select(a => a.tbl_PurchaseMaster).FirstOrDefault()
            };
            return Ok(record);
        }

        [HttpPost]
        public IHttpActionResult Edit([FromBody] EditPurchaseInvoiceDto edit)
        {
            if (edit == null) return BadRequest("No Data Found");

            var auditLogs = new List<DBModel.TransactionAuditLog>();
            var vochureType = Constants.PurchaseInvoice_VoucherTypeId;
            var master = context.tbl_PurchaseMaster.Find(edit.PurchaseMaster.purchaseMasterId);
            if (master != null)
            {
                //check for sales return 
                auditLogs.Add(new DBModel.TransactionAuditLog()
                {
                    TransId = master.purchaseMasterId.ToString(),
                    DateTime = DateTime.UtcNow,
                    Amount = master.totalAmount,
                    VochureTypeId = vochureType,
                    VochureName = TransactionsEnum.PurchaseMaster.ToString(),
                    AdditionalInformation = "Edited Purchase Master",
                    UserId = PublicVariables._decCurrentUserId,
                    Status = TransactionOperationEnums.Edit.ToString()
                });

                master.narration = edit.PurchaseMaster.narration;
                master.totalAmount = edit.PurchaseMaster.totalAmount;
                master.billDiscount = edit.PurchaseMaster.billDiscount;
                master.grandTotal = edit.PurchaseMaster.grandTotal;
                master.date = edit.PurchaseMaster.date;
                master.vendorInvoiceDate = edit.PurchaseMaster.vendorInvoiceDate;
                master.vendorInvoiceNo = edit.PurchaseMaster.vendorInvoiceNo;
                master.ledgerId = edit.PurchaseMaster.ledgerId;
                context.Entry(master).State = EntityState.Modified;
                context.SaveChanges();
                //edit line items
                var details = context.tbl_PurchaseDetails.ToList().Where(a => a.purchaseMasterId == edit.PurchaseMaster.purchaseMasterId);

                var detailIds = details.Select(a => Convert.ToDecimal(a.purchaseDetailsId)).ToList();
                var updatedIds = new List<decimal>();


                foreach (var i in edit.PurchaseDetails)
                {
                    //get item to update

                    var item = details.FirstOrDefault(a => a.purchaseDetailsId == i.purchaseDetailsId);

                    if (item != null)
                    {
                        auditLogs.Add(new TransactionAuditLog()
                        {
                            TransId = item.purchaseDetailsId.ToString(),
                            DateTime = DateTime.UtcNow,
                            Amount = item.amount,
                            Quantity = item.qty,
                            VochureTypeId = vochureType,
                            VochureName = TransactionsEnum.PurchaseDetail.ToString(),
                            AdditionalInformation = "Edited Purchase Line  detail Record",
                            UserId = PublicVariables._decCurrentUserId,
                            Status = TransactionOperationEnums.Edit.ToString()
                        });
                        item.discount = i.discount;
                        item.qty = i.qty;
                        item.amount = item.rate * i.qty;
                        context.Entry(item).State = EntityState.Modified;
                        updatedIds.Add(item.purchaseDetailsId);
                    }
                    if (i.purchaseDetailsId == 0)
                    {
                        i.extraDate = DateTime.UtcNow;
                        context.tbl_PurchaseDetails.Add(i);
                    }
                }

                var itemToDeleteIds = detailIds.Except(updatedIds);
                foreach (var i in itemToDeleteIds)
                {
                    var item = details.FirstOrDefault(a => a.purchaseDetailsId == i);
                    if (item != null)
                    {
                        context.tbl_PurchaseDetails.Remove(item);
                    }
                }
                context.SaveChanges();

                var stockPosting = context.tbl_StockPosting.Where(a =>
                    a.voucherTypeId == vochureType && a.voucherNo == master.voucherNo).ToList();
                // delete stock posting 
                if (stockPosting.Any())
                {
                    context.tbl_StockPosting.RemoveRange(stockPosting);
                }
                //get new updated items 
                var updatedItems = context.tbl_PurchaseDetails
                    .Where(a => a.purchaseMasterId
                                   == master.purchaseMasterId)
                    .ToList();

                // add new stock posting to update record
                foreach (var i in updatedItems)
                {
                    var product = context.tbl_Product.Find(i.productId);
                    context.tbl_StockPosting.Add(new DBModel.tbl_StockPosting
                    {
                        //set items here 
                        batchId = i.batchId,
                        CategoryId = i.CategoryId,
                        productId = i.productId,
                        ProjectId = i.ProjectId,
                        extra2 = i.extra2,
                        voucherTypeId = Constants.PurchaseInvoice_VoucherTypeId,
                        date = DateTime.UtcNow,
                        extraDate = DateTime.UtcNow,
                        financialYearId = PublicVariables._decCurrentFinancialYearId,
                        extra1 = "",
                        rackId = i.rackId,
                        voucherNo = edit.PurchaseMaster.voucherNo,
                        invoiceNo = edit.PurchaseMaster.invoiceNo,
                        unitId = i.unitId,
                        rate = product.purchaseRate,
                        godownId = i.godownId,
                        outwardQty = 0,
                        inwardQty = i.qty,
                        againstInvoiceNo = "",
                        againstVoucherNo = "",
                        againstVoucherTypeId = 0
                    });
                }
                context.SaveChanges();
                //get all ledger posting base on the vocheur id from the master, then delete them and create a new one 
                var oldLedgerPosting = context.tbl_LedgerPosting
                    .Where(a => a.voucherNo == edit.PurchaseMaster.voucherNo && a.voucherTypeId == vochureType)
                    .ToList();
                if (oldLedgerPosting.Any())
                {
                    context.tbl_LedgerPosting.RemoveRange(oldLedgerPosting);
                }

                //LedgerPosting Account Payable (Supplier Account)
                context.tbl_LedgerPosting.Add(new DBModel.tbl_LedgerPosting()
                {
                    voucherTypeId = Constants.PurchaseInvoice_VoucherTypeId,
                    date = DateTime.UtcNow,
                    ledgerId = master.ledgerId,
                    extra2 = "",
                    extraDate = DateTime.UtcNow,
                    voucherNo = edit.PurchaseMaster.voucherNo,
                    extra1 = "",
                    credit = edit.PurchaseMaster.grandTotal,
                    debit = 0,
                    invoiceNo = edit.PurchaseMaster.invoiceNo,
                    chequeDate = DateTime.UtcNow,
                    yearId = PublicVariables._decCurrentFinancialYearId,
                    detailsId = edit.PurchaseMaster.purchaseMasterId,
                    chequeNo = ""
                });


                var goodInTransitLedger = LedgerConstants.GoodsInTransit;
                foreach (var i in updatedItems)
                {
                    var product = context.tbl_Product.Find(i.productId);
                    if (master.materialReceiptMasterId > 0)
                    {
                        context.tbl_LedgerPosting.Add(new DBModel.tbl_LedgerPosting()
                        {
                            voucherTypeId = vochureType,
                            date = DateTime.UtcNow,   
                            ledgerId = goodInTransitLedger,
                            extra2 = "",
                            extraDate = DateTime.UtcNow,
                            voucherNo = edit.PurchaseMaster.voucherNo,
                            extra1 = "",
                            debit = i.qty * i.rate,
                            credit = 0,
                            invoiceNo = edit.PurchaseMaster.invoiceNo,
                            chequeDate = DateTime.UtcNow,
                            yearId = PublicVariables._decCurrentFinancialYearId,
                            detailsId = i.purchaseDetailsId,
                            chequeNo = ""
                        });
                    }

                    context.SaveChanges();

                }
                if (auditLogs.Any())
                {
                    new Services.TransactionAuditService().CreateMany(auditLogs);
                }
                return Ok(new { status = 200, message = "Edit Made Successful" });

            }

            return InternalServerError();
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var audits = new List<TransactionAuditLog>();
            var master = context.tbl_PurchaseMaster.Find(id);
            if (master == null) return NotFound();

            //check if it has deliveryNote 
            var material = context.tbl_MaterialReceiptMaster.Find(master.materialReceiptMasterId);
            if (material != null)
                return Ok(new { status = 200, message = "cant delete this item because it has a reference" });
            var order = context.tbl_PurchaseOrderMaster.Find(master.purchaseOrderMasterId);
            if (order != null) return Ok(new { status = 200, message = "cant delete this item because it has a reference to order" });

            //To delete a purchaseinvoice from partyBalance
            var partyBalance = context.tbl_PartyBalance.Where(a => a.voucherTypeId == Constants.PurchaseInvoice_VoucherTypeId 
            && a.voucherNo == master.voucherNo);
            if (partyBalance.Any())
            {
                context.tbl_PartyBalance.RemoveRange(partyBalance);
            }
            else
            {
                //To restrict partyBalances been deleted when payment is currently going on it.
                if (partyBalance.Any()) return Ok(new { status = 200, message = "cant delete this purchaseInvoice because it has a reference to a payment" });
            }

            //find ledgerPosting and Delete  

            var ledgerPosting = context.tbl_LedgerPosting.Where(a =>
                a.voucherTypeId == Constants.PurchaseInvoice_VoucherTypeId
                && a.voucherNo == master.voucherNo);
            if (ledgerPosting.Any())
            {
                context.tbl_LedgerPosting.RemoveRange(ledgerPosting);

            }
            //find all stock posting and delete 
            var stocks = context.tbl_StockPosting.Where(a => a.voucherTypeId == Constants.PurchaseInvoice_VoucherTypeId &&
                                                 a.voucherNo == master.voucherNo).ToList();
            if (stocks.Any())
            {
                context.tbl_StockPosting.RemoveRange(stocks);
            }
            audits.AddRange(master.tbl_PurchaseDetails.Select(a => new TransactionAuditLog
            {
                Quantity = a.qty,
                Amount = a.amount * a.qty,
                UserId = PublicVariables._decCurrentUserId,
                VochureName = Enums.TransactionsEnum.PurchaseDetail.ToString(),
                DateTime = DateTime.Now,
                Status = Enums.TransactionOperationEnums.Delete.ToString(),
                AdditionalInformation = "Delete Items From Sales Details",
                VochureTypeId = Constants.SalesInvoice_VoucherTypeId,
                TransId = a.purchaseDetailsId.ToString(),
            }));
            audits.Add(new TransactionAuditLog()
            {

                Amount = master.totalAmount,
                UserId = PublicVariables._decCurrentUserId,
                VochureName = Enums.TransactionsEnum.SalesMaster.ToString(),
                DateTime = DateTime.Now,
                Status = Enums.TransactionOperationEnums.Delete.ToString(),
                AdditionalInformation = "Delete Items From Purchase Master",
                VochureTypeId = Constants.SalesInvoice_VoucherTypeId,
                TransId = master.purchaseMasterId.ToString(),
            });

            //delete master and detail in 
            context.tbl_PurchaseMaster.Remove(master);
            context.SaveChanges();
            new Services.TransactionAuditService().CreateMany(audits);

            return Ok(new { status = 200, message = "Delete Made Successful" });
        }


        public decimal GetVoucherMaxNumber(decimal decPurchaseInvoiceVoucherTypeId)
        {
            decimal strVoucherNo = 0;

            try
            {

                var context = new MatApi.DBModel.DBMATAccounting_MagnetEntities1();

                var purchaseMaster = context.tbl_PurchaseMaster.Where(pm => pm.voucherTypeId == decPurchaseInvoiceVoucherTypeId).ToList();

                var LastTransacMid = purchaseMaster[purchaseMaster.Count - 2];
                var LastTransacLast = purchaseMaster[purchaseMaster.Count - 1];

               // string numericVoucherNo = new String(LastTransacLast.voucherNo.Where(Char.IsDigit).ToArray());

                // strVoucherNo = Convert.ToDecimal(LastTransacMid.voucherNo);

                try
                {
                    strVoucherNo = Convert.ToDecimal(LastTransacLast.voucherNo);
                }
                catch (Exception ex3)
                {
                   var midset =  purchaseMaster.Where(pm => pm.voucherNo.All(char.IsDigit)).ToList();

                    var MaxNumber = midset.Max(i => Convert.ToDecimal(i.voucherNo));

                    strVoucherNo = MaxNumber;
                }
            }
            catch(Exception ex)
            {

            }

            return strVoucherNo;
        }


        private string AutoGenerateFormNo()
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();

            strVoucherNo = "0";
            strVoucherNo = GetVoucherMaxNumber(decPurchaseInvoiceVoucherTypeId).ToString();

            if (strVoucherNo == string.Empty)
            {
                strVoucherNo = "0";
            }

            strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPurchaseInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strTableName);

            //spPurchaseMaster.PurchaseMasterVoucherMax

            if (Convert.ToDecimal(strVoucherNo) != (GetVoucherMaxNumber(decPurchaseInvoiceVoucherTypeId)))
            {
                strVoucherNo = GetVoucherMaxNumber(decPurchaseInvoiceVoucherTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPurchaseInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strTableName);

                if (GetVoucherMaxNumber(decPurchaseInvoiceVoucherTypeId) == 0)
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPurchaseInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strTableName);
                }
            }

            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decPurchaseInvoiceVoucherTypeId, DateTime.Now);
            strPrefix = infoSuffixPrefix.Prefix;
            strSuffix = infoSuffixPrefix.Suffix;
            var decPurchaseInvoiceSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
            var FormNo = strPrefix + strVoucherNo + strSuffix;

            return FormNo;
        }
    }
}
