﻿using MatApi.DBModel;
using MatApi.Models;
using MatApi.Models.Supplier;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using static MatApi.Controllers.Customer.CustomerCentreController;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SupplierCentreController : ApiController
    {
        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        DateTime dtmVoucherDate = MATFinancials.PublicVariables._dtCurrentDate;
        [HttpPost]
        public DataTable GetSupplierList(SupplierCentreSearchModel SearchParameters)
        {
            try
            {
                decimal decAreaId = 0;
                decimal decRouteId = 0;
                DataTable dtbl = new DataTable();
                AccountLedgerSP spAccountledger = new AccountLedgerSP();
                if (SearchParameters.StateId < 1)
                {
                    decAreaId = 0;
                }
                else
                {
                    decAreaId = Convert.ToDecimal(SearchParameters.StateId.ToString());
                }
                if (SearchParameters.CityId < 1)
                {
                    decRouteId = 0;
                }
                else
                {
                    decRouteId = Convert.ToDecimal(SearchParameters.CityId.ToString());
                }

                int AccountStatus = Convert.ToInt32(SearchParameters.Status);
                string SupplierName = SearchParameters.SupplierName;

                dtbl = spAccountledger.SupplierSearchAll(decAreaId, decRouteId, SupplierName.Trim(), MATFinancials.PublicVariables._dtToDate, AccountStatus);

                if (AccountStatus == 1)
                {
                    dtbl = dtbl.AsEnumerable().Where(i => i.Field<decimal>("openingBalance") != 0).CopyToDataTable();
                }

                return dtbl;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "Cus16:" + ex.Message;
            }

            return null;
        }

        [HttpGet]
        public HttpResponseMessage GetLookUp()
        {
            dynamic resp = new ExpandoObject();
            var cities = new RouteSP().RouteViewAll();
            var states = new AreaSP().AreaViewAll();
            var pricingLevels = new PricingLevelSP().PricingLevelViewAll();

            resp.Cities = cities;
            resp.States = states;
            resp.PricingLevels = pricingLevels;

            return Request.CreateResponse(HttpStatusCode.OK, (object)resp);
        }

        [HttpPost]
        public MatResponse SaveNewSupplier(List<AccountLedgerInfo> input)
        {
            decimal decledgerid = 0;
            MatResponse matResponse = new MatResponse();
            try
            {
                foreach (AccountLedgerInfo item in input)
                {
                    AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
                    AccountLedgerSP spAccountLedger = new AccountLedgerSP();

                    infoAccountLedger.AccountGroupId = 22;
                    infoAccountLedger.LedgerName = item.LedgerName;
                    infoAccountLedger.IsActive = item.IsActive;
                    infoAccountLedger.OpeningBalance = 0;
                    infoAccountLedger.CrOrDr = "Cr";
                    infoAccountLedger.BankAccountNumber = item.BankAccountNumber;
                    infoAccountLedger.BranchName = item.BranchName;
                    infoAccountLedger.BranchCode = item.BranchCode;
                    infoAccountLedger.Mobile = item.Mobile;
                    infoAccountLedger.Address = item.Address;
                    infoAccountLedger.CreditLimit = Convert.ToDecimal(item.CreditLimit);
                    infoAccountLedger.CreditPeriod = Convert.ToInt32(item.CreditPeriod);
                    infoAccountLedger.Cst = item.Cst;
                    infoAccountLedger.AreaId = Convert.ToDecimal(item.AreaId);
                    infoAccountLedger.RouteId = Convert.ToDecimal(item.RouteId);
                    infoAccountLedger.MailingName = item.MailingName;
                    infoAccountLedger.Phone = item.Phone;
                    infoAccountLedger.Email = item.Email;
                    infoAccountLedger.PricinglevelId = Convert.ToDecimal(item.PricinglevelId);
                    infoAccountLedger.Tin = item.Tin;
                    infoAccountLedger.Pan = "";
                    infoAccountLedger.Narration = "";
                    infoAccountLedger.IsDefault = false;
                    infoAccountLedger.Extra1 = string.Empty;
                    infoAccountLedger.Extra2 = string.Empty;
                    infoAccountLedger.ExtraDate = MATFinancials.PublicVariables._dtCurrentDate;
                    infoAccountLedger.IsActive = true;
                    infoAccountLedger.BillByBill = true;
                    if (spAccountLedger.AccountLedgerCheckExistenceForSalesman(item.LedgerName, 0) == false)
                    {
                        decledgerid = spAccountLedger.AccountLedgerAddForCustomer(infoAccountLedger);
                    }
                    else
                    {
                        string FailMessage = "Ledger name '" + item.LedgerName + "' already exists!";
                        matResponse.ResponseCode = 300;
                        matResponse.ResponseMessage = FailMessage;
                        return matResponse;
                    }
                }
               
            }
            catch (Exception ex)
            {

            }
            matResponse.ResponseCode = 200;
            matResponse.ResponseMessage = "Ledger(s) saved successfully!";
            return matResponse;
        }

        [HttpGet]
        public HttpResponseMessage GetLedgerDetails(System.DateTime DateFrom1, System.DateTime DateTo1, int ledgerId)
        {
            try
            {

                DataSet ledgerDetails = new DataSet();
                AccountLedgerSP SpAccountLedger = new AccountLedgerSP();
                dynamic resp = new ExpandoObject();
                //{1/1/2017 12:00:00 AM} {3/31/2100 12:00:00 AM}
                //had to comment out the below and change the DateTime parameters to that of MatFinancials because not all rows were being returned
                //ledgerDetails = SpAccountLedger.GetLedgerDetailsFromSelectedCustomer(MATFinancials.PublicVariables._dtFromDate, MATFinancials.PublicVariables._dtToDate, ledgerId);
                DateTime fromDate = DateFrom1;
                DateTime toDate = DateTo1;
                ledgerDetails = SpAccountLedger.GetLedgerDetailsFromSelectedCustomer(fromDate, toDate,ledgerId);
                resp.LedgerDetails = ledgerDetails;
                resp.OpeningDate = fromDate.ToShortDateString();//MATFinancials.PublicVariables._dtFromDate;

                return Request.CreateResponse(HttpStatusCode.OK, (object)resp);
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpPost]
        public HttpResponseMessage GetPartyBalanceDetails(SuppliersVm obj)
        {
            dynamic resp = new ExpandoObject();
            try
            {
                PartyBalanceSP SpPartyBalance = new PartyBalanceSP();

                    ExchangeRateSP spExchangeRate = new ExchangeRateSP();
                    ExchangeRateInfo infoExchangeRate = new ExchangeRateInfo();
                    //string str = dgvPartyBalance.CurrentRow.Cells["dgvcmbVoucherType"].Value.ToString();
                    //string[] words;
                    //words = str.Split('_');
                    DataTable dtbl1 = new DataTable();

                var firstFinancialObj = context.tbl_FinancialYear.FirstOrDefault();

                dtbl1 = SpPartyBalance.PartyBalanceComboViewByLedgerId(firstFinancialObj.fromDate.Value, MATFinancials.PublicVariables._dtToDate, obj.LedgerId, obj.CrDr, obj.VoucherTypeId, obj.AccountGroupId.ToString());

                //var dtbl = dtbl1.Rows.Count == 0 ? dtbl1 : dtbl1.AsEnumerable().Where(x => x.Field<string>("display").Contains("Purchase")).CopyToDataTable();
                var dtbl = new DataTable();
                if (dtbl1.Rows.Count == 0)
                {
                        dtbl = new DataTable();
                }
                else
                {
                    var displayFieldCount = dtbl1.AsEnumerable().Where(x => x.Field<string>("display").Contains("Purchase")).Count();
                    if(displayFieldCount > 0)
                    {
                        dtbl = dtbl1.AsEnumerable().Where(x => x.Field<string>("display").Contains("Purchase")).CopyToDataTable();
                    }
                    else
                    {
                        dtbl = new DataTable();
                    }
                }
                   
                    resp.PartyDetails = dtbl;
              
            }
            catch (Exception ex)
            {
                //log error Message here ...
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)resp);
        }

        [HttpGet]
        public HttpResponseMessage GetSupplierDetails(int ledgerId)
        {
            try
            {
                AccountLedgerInfo infoAccountledger = new AccountLedgerInfo();
                AccountLedgerSP spAccountledger = new AccountLedgerSP();
                infoAccountledger = spAccountledger.AccountLedgerViewForSupplier(ledgerId);
                return Request.CreateResponse(HttpStatusCode.OK, (object)infoAccountledger);

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpPost]
        public string UpdateLedgerDetails(AccountLedgerInfo input)
        {
            try
            {
                AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();

                infoAccountLedger.AccountGroupId = 22;
                infoAccountLedger.LedgerId = input.LedgerId;
                infoAccountLedger.LedgerName = input.LedgerName;
                infoAccountLedger.IsActive = input.IsActive;
                infoAccountLedger.OpeningBalance = 0;
                infoAccountLedger.CrOrDr = "Cr";
                infoAccountLedger.BankAccountNumber = input.BankAccountNumber;
                infoAccountLedger.BranchName = input.BranchName;
                infoAccountLedger.BranchCode = input.BranchCode;
                infoAccountLedger.Mobile = input.Mobile;
                infoAccountLedger.Address = input.Address;
                infoAccountLedger.CreditLimit = Convert.ToDecimal(input.CreditLimit);
                infoAccountLedger.CreditPeriod = Convert.ToInt32(input.CreditPeriod);
                infoAccountLedger.Cst = input.Cst;
                infoAccountLedger.AreaId = Convert.ToDecimal(input.AreaId);
                infoAccountLedger.RouteId = Convert.ToDecimal(input.RouteId);
                infoAccountLedger.MailingName = input.MailingName;
                infoAccountLedger.Phone = input.Phone;
                infoAccountLedger.Email = input.Email;
                infoAccountLedger.PricinglevelId = Convert.ToDecimal(input.PricinglevelId);
                infoAccountLedger.Tin = input.Tin;
                infoAccountLedger.Pan = "";
                infoAccountLedger.Narration = "";
                infoAccountLedger.IsDefault = false;
                infoAccountLedger.Extra1 = string.Empty;    // to save agent code
                infoAccountLedger.Extra2 = string.Empty;
                infoAccountLedger.ExtraDate = MATFinancials.PublicVariables._dtCurrentDate;
                infoAccountLedger.IsActive = true;
                infoAccountLedger.BillByBill = true;
                spAccountLedger.AccountLedgerEdit(infoAccountLedger);
                return "Supplier Updated Successfully";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        [HttpGet]
        public string DeleteLedger(int ledgerId)
        {
            try
            {
                var msg = "";

                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                if (spAccountLedger.AccountLedgerCheckReferences(ledgerId) == -1)
                {
                    msg = "Ledger has an existing transaction";

                    return msg;
                }
                else
                {
                    spAccountLedger.PartyBalanceDeleteByVoucherTypeVoucherNoAndReferenceType(ledgerId.ToString(), 1);
                    spAccountLedger.LedgerPostingDeleteByVoucherTypeAndVoucherNo(ledgerId.ToString(), 1);

                    msg = "Ledger deleted successfully";

                    return msg;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpGet]
        public List<OpeningBalance> GetAllAgentLedger()
        {
            try
            {
                DBMatConnection conn = new DBMatConnection();

                var DateFrom0 = MatApi.Models.PublicVariables.FromDate;
                var DateTo0 = MatApi.Models.PublicVariables.ToDate;
                var FYearId = MatApi.Models.PublicVariables.CurrentFinicialYearId;

                DateFrom0 = DateFrom0.AddDays(-1);

                var DateFrom = DateFrom0.Year + "-" + DateFrom0.Month + "-" + DateFrom0.Day  /*?? MatApi.Models.PublicVariables.FromDate.ToShortDateString()*/;
                var DateTo = DateTo0.Year + "-" + DateTo0.Month + "-" + DateTo0.Day/*?? MatApi.Models.PublicVariables.ToDate.ToShortDateString()*/;

                var FyearQuery = string.Format("  (lp.date between CAST('" + DateFrom + "' AS datetime) and CAST('" + DateTo + "' AS datetime))  and  ");

                string query = string.Format(" select ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, al.extra1, al.mailingName, al.routeId, al.areaId, tbl_Route.routeName, tbl_Area.areaName, " +
                                             //  "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance, " +
                                             "ISNULL(convert(decimal(18, 2), sum(isnull(credit, 0) - isnull(debit, 0))), 0) as openingBalance, " +
                                             " al.isActive AS Active from tbl_LedgerPosting lp " +
                                             " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " +
                                             " left outer join tbl_Route on al.routeId = tbl_Route.routeId " +
                                             " left outer join tbl_Area on al.areaId = tbl_Area.areaId " +
                                             " where al.accountGroupId = 22  " +
                                             " group by ledgerName, al.ledgerId, lp.ledgerId, al.isActive, al.routeId, al.extra1, al.areaId, al.mailingName, tbl_Route.routeName, tbl_Area.areaName ");

                var data = conn.customSelect(query);

                var ListOpeningBalance = new List<OpeningBalance>();

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        var newOpeningAmt = new OpeningBalance();
                        newOpeningAmt.Active = row["Active"];
                        newOpeningAmt.areaId = row["areaId"];
                        newOpeningAmt.areaName = row["areaName"];
                        newOpeningAmt.extra1 = row["extra1"];
                        newOpeningAmt.ledgerId = row["ledgerId"];
                        newOpeningAmt.ledgerName = row["ledgerName"];
                        newOpeningAmt.mailingName = row["mailingName"];
                        newOpeningAmt.routeId = row["routeId"];
                        newOpeningAmt.routeName = row["routeName"];
                        try
                        {
                            var Amt = double.Parse(row["openingBalance"].ToString());
                            if (Amt != null && (Amt > 0 || Amt.ToString().Contains("-")))
                            {
                                newOpeningAmt.openingBalance = 0;
                            }
                            else
                            {
                                newOpeningAmt.openingBalance = 0;
                            }
                        }
                        catch (Exception ex)
                        {
                            newOpeningAmt.openingBalance = 0;
                        }
                        ListOpeningBalance.Add(newOpeningAmt);
                    }
                }

                query = string.Format("select ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, al.extra1, al.mailingName, al.routeId, al.areaId, tbl_Route.routeName, tbl_Area.areaName, " +
                                            //  "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance, " +
                                            "ISNULL(convert(decimal(18, 2), sum(isnull(credit, 0) - isnull(debit, 0))), 0) as openingBalance, " +
                                            " al.isActive AS Active from tbl_LedgerPosting lp " +
                                            " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " +
                                            " left outer join tbl_Route on al.routeId = tbl_Route.routeId " +
                                            " left outer join tbl_Area on al.areaId = tbl_Area.areaId " +
                                            " where " + FyearQuery + " al.accountGroupId = 22  " +
                                            " group by ledgerName, al.ledgerId, lp.ledgerId, al.isActive, al.routeId, al.extra1, al.areaId, al.mailingName, tbl_Route.routeName, tbl_Area.areaName ");

                var Tabledata = conn.customSelect(query);

                var ListOpeningBalance1 = new List<OpeningBalance>();

                if (Tabledata.Rows.Count > 0)
                {
                    foreach (DataRow row in Tabledata.Rows)
                    {
                        var newOpeningAmt = new OpeningBalance();
                        newOpeningAmt.Active = row["Active"];
                        newOpeningAmt.areaId = row["areaId"];
                        newOpeningAmt.areaName = row["areaName"];
                        newOpeningAmt.extra1 = row["extra1"];
                        newOpeningAmt.ledgerId = row["ledgerId"];
                        newOpeningAmt.ledgerName = row["ledgerName"];
                        newOpeningAmt.mailingName = row["mailingName"];
                        newOpeningAmt.routeId = row["routeId"];
                        newOpeningAmt.routeName = row["routeName"];

                        try
                        {
                            var Amt = double.Parse(row["openingBalance"].ToString());
                            if (Amt != null && (Amt > 0 || Amt.ToString().Contains("-")))
                            {
                                newOpeningAmt.openingBalance = Amt;
                            }
                            else
                            {
                                newOpeningAmt.openingBalance = 0;
                            }
                        }
                        catch (Exception ex)
                        {
                            newOpeningAmt.openingBalance = 0;
                        }

                        ListOpeningBalance1.Add(newOpeningAmt);
                    }

                    for (var i = 0; i <= ListOpeningBalance.Count - 1; i++)
                    {
                        var LedgerId = ListOpeningBalance[i].ledgerId;
                        var LedgerList = ListOpeningBalance1.Where(lb => lb.ledgerId == LedgerId).FirstOrDefault();
                        if (LedgerList != null)
                        {
                            if (Convert.ToDouble(LedgerList.openingBalance) > 0 || LedgerList.openingBalance.ToString().Contains("-"))
                            {
                                ListOpeningBalance[i].openingBalance = Convert.ToDouble(LedgerList.openingBalance);
                            }
                            else
                            {
                                ListOpeningBalance[i].openingBalance = 0;
                            }
                        }
                        else
                        {
                            ListOpeningBalance[i].openingBalance = 0;
                        }
                    }

                    DateFrom = DateFrom0.Year + "-" + DateFrom0.Month + "-" + DateFrom0.Day  /*?? MatApi.Models.PublicVariables.FromDate.ToShortDateString()*/;
                    DateTo = DateTo0.Year + "-" + DateTo0.Month + "-" + DateTo0.Day/*?? MatApi.Models.PublicVariables.ToDate.ToShortDateString()*/;

                    var FinancialYear = context.tbl_FinancialYear.FirstOrDefault();

                    if (FinancialYear != null)
                    {
                        DateTo0 = DateFrom0.AddDays(-1);

                        DateFrom = FinancialYear.fromDate.Value.Year + "-" + FinancialYear.fromDate.Value.Month + "-" + FinancialYear.fromDate.Value.Day;
                        DateTo = DateTo0.Year + "-" + DateTo0.Month + "-" + DateTo0.Day;

                        FyearQuery = string.Format("  where (lp.date between CAST('" + DateFrom + "' AS datetime) and CAST('" + DateTo + "' AS datetime)) ");

                        query = string.Format("select lp.ledgerId, convert(decimal(18, 2), sum(lp.debit)) as openDebit, convert(decimal(18, 2), sum(lp.credit)) as openCredit  from  tbl_LedgerPosting lp " + " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " + FyearQuery + "  and al.accountGroupId = 22 group by lp.ledgerId");

                        var data1 = conn.customSelect(query);

                        var ListOpenBalan = new List<OpenBalan>();

                        if (data1.Rows.Count > 0)
                        {
                            foreach (DataRow row in data1.Rows)
                            {
                                var newOpeningAmt = new OpenBalan();
                                newOpeningAmt.openCredit = row["openCredit"];
                                newOpeningAmt.openDebit = row["openDebit"];
                                newOpeningAmt.ledgerId = row["ledgerId"];
                                if (string.IsNullOrEmpty(Convert.ToString(newOpeningAmt.openCredit)) || string.IsNullOrWhiteSpace(Convert.ToString(newOpeningAmt.openCredit)) || Convert.ToString(newOpeningAmt.openCredit) == "{}")
                                {
                                    newOpeningAmt.openCredit = 0;
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(newOpeningAmt.openDebit)) || string.IsNullOrWhiteSpace(Convert.ToString(newOpeningAmt.openDebit)) || Convert.ToString(newOpeningAmt.openDebit) == "{}")
                                {
                                    newOpeningAmt.openDebit = 0;
                                }
                                ListOpenBalan.Add(newOpeningAmt);
                            }
                        }

                        for (var i = 0; i <= ListOpeningBalance.Count - 1; i++)
                        {
                            var LedgerId = ListOpeningBalance[i].ledgerId;
                            var LedgerList = ListOpenBalan.Where(lb => lb.ledgerId == LedgerId).FirstOrDefault();
                            if (LedgerList != null)
                            {
                                var BalanceTop = Convert.ToDouble(LedgerList.openCredit) - Convert.ToDouble(LedgerList.openDebit);

                                ListOpeningBalance[i].openingBalance = Convert.ToDouble(ListOpeningBalance[i].openingBalance) + BalanceTop;

                                //if (Convert.ToDouble(ListOpeningBalance[i].openingBalance) > 0)
                                //{

                                //}
                                //else
                                //{
                                //    ListOpeningBalance[i].openingBalance = 0;
                                //}
                            }
                        }
                    }
                }

                return ListOpeningBalance;
            }
            catch (Exception ex)
            {
                return new List<OpeningBalance>();
            }

        }

        [HttpGet]
        public List<OpeningBalance> GetAllAgentLedger1(DateTime DateFrom1, DateTime DateTo1)
        {
            try
            {
                DBMatConnection conn = new DBMatConnection();

                var DateFrom = DateFrom1.Year + "-" + DateFrom1.Month + "-" + DateFrom1.Day  /*?? MatApi.Models.PublicVariables.FromDate.ToShortDateString()*/;
                var DateTo = DateTo1.Year + "-" + DateTo1.Month + "-" + DateTo1.Day/*?? MatApi.Models.PublicVariables.ToDate.ToShortDateString()*/;

                var FyearQuery = string.Format("  (lp.date between CAST('" + DateFrom + "' AS datetime) and CAST('" + DateTo + "' AS datetime))  and  ");

                string query = string.Format(" select ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, al.extra1 , al.mailingName, al.routeId, al.areaId, tbl_Route.routeName, tbl_Area.areaName, " +
                                             //  "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance, " +
                                             "ISNULL(convert(decimal(18, 2), sum(isnull(credit, 0) - isnull(debit, 0))), 0) as openingBalance, " +
                                             " al.isActive AS Active from tbl_LedgerPosting lp " +
                                             " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " +
                                             " left outer join tbl_Route on al.routeId = tbl_Route.routeId " +
                                             " left outer join tbl_Area on al.areaId = tbl_Area.areaId " +
                                             " where al.accountGroupId = 22  " +
                                             " group by ledgerName, al.ledgerId, lp.ledgerId, al.isActive, al.routeId, al.extra1, al.areaId, al.mailingName, tbl_Route.routeName, tbl_Area.areaName ");

                var data = conn.customSelect(query);

                var ListOpeningBalance = new List<OpeningBalance>();

                if (data.Rows.Count > 0)
                {
                    foreach (DataRow row in data.Rows)
                    {
                        var newOpeningAmt = new OpeningBalance();
                        newOpeningAmt.Active = row["Active"];
                        newOpeningAmt.areaId = row["areaId"];
                        newOpeningAmt.areaName = row["areaName"];
                        newOpeningAmt.extra1 = row["extra1"];
                        newOpeningAmt.ledgerId = row["ledgerId"];
                        newOpeningAmt.ledgerName = row["ledgerName"];
                        newOpeningAmt.extra1 = row["extra1"];
                        newOpeningAmt.routeId = row["routeId"];
                        newOpeningAmt.routeName = row["routeName"];
                        newOpeningAmt.mailingName = row["mailingName"];

                        try
                        {
                            var Amt = double.Parse(row["openingBalance"].ToString());
                            if (Amt != null && (Amt > 0 || Amt.ToString().Contains("-")))
                            {
                                newOpeningAmt.openingBalance = 0;
                            }
                            else
                            {
                                newOpeningAmt.openingBalance = 0;
                            }
                        }
                        catch (Exception ex)
                        {
                            newOpeningAmt.openingBalance = 0;
                        }

                        ListOpeningBalance.Add(newOpeningAmt);
                    }
                }

                query = string.Format("select ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, al.extra1, al.mailingName, al.routeId, al.areaId, tbl_Route.routeName, tbl_Area.areaName, " +
                                            //  "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance, " +
                                            "ISNULL(convert(decimal(18, 2), sum(isnull(credit, 0) - isnull(debit, 0))), 0) as openingBalance, " +
                                            " al.isActive AS Active from tbl_LedgerPosting lp " +
                                            " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " +
                                            " left outer join tbl_Route on al.routeId = tbl_Route.routeId " +
                                            " left outer join tbl_Area on al.areaId = tbl_Area.areaId " +
                                            " where " + FyearQuery + " al.accountGroupId = 22  " +
                                            " group by ledgerName, al.ledgerId, lp.ledgerId, al.isActive, al.routeId, al.extra1, al.areaId, al.mailingName, tbl_Route.routeName, tbl_Area.areaName ");

                var Tabledata = conn.customSelect(query);

                var ListOpeningBalance1 = new List<OpeningBalance>();

                if (Tabledata.Rows.Count > 0)
                {
                    foreach (DataRow row in Tabledata.Rows)
                    {
                        var newOpeningAmt = new OpeningBalance();
                        newOpeningAmt.Active = row["Active"];
                        newOpeningAmt.areaId = row["areaId"];
                        newOpeningAmt.areaName = row["areaName"];
                        newOpeningAmt.extra1 = row["extra1"];
                        newOpeningAmt.ledgerId = row["ledgerId"];
                        newOpeningAmt.ledgerName = row["ledgerName"];
                        newOpeningAmt.extra1 = row["extra1"];
                        newOpeningAmt.routeId = row["routeId"];
                        newOpeningAmt.routeName = row["routeName"];
                        newOpeningAmt.mailingName = row["mailingName"];

                        try
                        {
                            var Amt = double.Parse(row["openingBalance"].ToString());
                            if (Amt != null && (Amt > 0 || Amt.ToString().Contains("-")))
                            {
                                newOpeningAmt.openingBalance = Amt;
                            }
                            else
                            {
                                newOpeningAmt.openingBalance = 0;
                            }
                        }
                        catch (Exception ex)
                        {
                            newOpeningAmt.openingBalance = 0;
                        }

                        ListOpeningBalance1.Add(newOpeningAmt);
                    }

                    for (var i = 0; i <= ListOpeningBalance.Count - 1; i++)
                    {
                        var LedgerId = ListOpeningBalance[i].ledgerId;
                        var LedgerList = ListOpeningBalance1.Where(lb => lb.ledgerId == LedgerId).FirstOrDefault();
                        if (LedgerList != null)
                        {
                            if (Convert.ToDouble(LedgerList.openingBalance) > 0 || LedgerList.openingBalance.ToString().Contains("-"))
                            {
                                ListOpeningBalance[i].openingBalance = Convert.ToDouble(LedgerList.openingBalance);
                            }
                            else
                            {
                                ListOpeningBalance[i].openingBalance = 0;
                            }
                        }
                        else
                        {
                            ListOpeningBalance[i].openingBalance = 0;
                        }
                    }

                    DateFrom = DateFrom1.Year + "-" + DateFrom1.Month + "-" + DateFrom1.Day  /*?? MatApi.Models.PublicVariables.FromDate.ToShortDateString()*/;
                    DateTo = DateTo1.Year + "-" + DateTo1.Month + "-" + DateTo1.Day/*?? MatApi.Models.PublicVariables.ToDate.ToShortDateString()*/;

                    var FinancialYear = context.tbl_FinancialYear.FirstOrDefault();

                    if (FinancialYear != null)
                    {
                        DateTo1 = DateFrom1.AddDays(-1);

                        DateFrom = FinancialYear.fromDate.Value.Year + "-" + FinancialYear.fromDate.Value.Month + "-" + FinancialYear.fromDate.Value.Day;
                        DateTo = DateTo1.Year + "-" + DateTo1.Month + "-" + DateTo1.Day;

                        FyearQuery = string.Format("  where (lp.date between CAST('" + DateFrom + "' AS datetime) and CAST('" + DateTo + "' AS datetime)) ");

                        query = string.Format("select lp.ledgerId, convert(decimal(18, 2), sum(lp.debit)) as openDebit, convert(decimal(18, 2), sum(lp.credit)) as openCredit  from  tbl_LedgerPosting lp " + " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " + FyearQuery + "  and al.accountGroupId = 22 group by lp.ledgerId");

                        var data1 = conn.customSelect(query);

                        var ListOpenBalan = new List<OpenBalan>();

                        if (data1.Rows.Count > 0)
                        {
                            foreach (DataRow row in data1.Rows)
                            {
                                var newOpeningAmt = new OpenBalan();
                                newOpeningAmt.openCredit = row["openCredit"];
                                newOpeningAmt.openDebit = row["openDebit"];
                                newOpeningAmt.ledgerId = row["ledgerId"];
                                if (string.IsNullOrEmpty(Convert.ToString(newOpeningAmt.openCredit)) || string.IsNullOrWhiteSpace(Convert.ToString(newOpeningAmt.openCredit)) || Convert.ToString(newOpeningAmt.openCredit) == "{}")
                                {
                                    newOpeningAmt.openCredit = 0;
                                }
                                if (string.IsNullOrEmpty(Convert.ToString(newOpeningAmt.openDebit)) || string.IsNullOrWhiteSpace(Convert.ToString(newOpeningAmt.openDebit)) || Convert.ToString(newOpeningAmt.openDebit) == "{}")
                                {
                                    newOpeningAmt.openDebit = 0;
                                }
                                ListOpenBalan.Add(newOpeningAmt);
                            }

                        }


                        for (var i = 0; i <= ListOpeningBalance.Count - 1; i++)
                        {
                            var LedgerId = ListOpeningBalance[i].ledgerId;
                            var LedgerList = ListOpenBalan.Where(lb => lb.ledgerId == LedgerId).FirstOrDefault();
                            if (LedgerList != null)
                            {
                                var BalanceTop = Convert.ToDouble(LedgerList.openCredit) - Convert.ToDouble(LedgerList.openDebit);

                                ListOpeningBalance[i].openingBalance = Convert.ToDouble(ListOpeningBalance[i].openingBalance) + BalanceTop;

                                //if (Convert.ToDouble(ListOpeningBalance[i].openingBalance) > 0)
                                //{

                                //}
                                //else
                                //{
                                //    ListOpeningBalance[i].openingBalance = 0;
                                //}
                            }
                        }
                    }
                }

                return ListOpeningBalance;
            }
            catch(Exception ex)
            {
                return new List<OpeningBalance>(); 
            }
        }

        [HttpGet]
        public OpenBalan GetLedgerOpeningBalance(System.DateTime DateFrom1, System.DateTime DateTo1, int ledgerId = 0)
        {
            DBMatConnection conn = new DBMatConnection();

            var DateFrom = DateFrom1.Year + "-" + DateFrom1.Month + "-" + DateFrom1.Day  /*?? MatApi.Models.PublicVariables.FromDate.ToShortDateString()*/;
            var DateTo = DateTo1.Year + "-" + DateTo1.Month + "-" + DateTo1.Day/*?? MatApi.Models.PublicVariables.ToDate.ToShortDateString()*/;

            var FinancialYear = context.tbl_FinancialYear.FirstOrDefault();

            if (FinancialYear != null)
            {
                DateTo1 = DateFrom1.AddDays(-1);

                DateFrom = FinancialYear.fromDate.Value.Year + "-" + FinancialYear.fromDate.Value.Month + "-" + FinancialYear.fromDate.Value.Day;
                DateTo = DateTo1.Year + "-" + DateTo1.Month + "-" + DateTo1.Day;

                var FyearQuery = string.Format("  where (lp.date between CAST('" + DateFrom + "' AS datetime) and CAST('" + DateTo + "' AS datetime)) and lp.ledgerId=" + ledgerId);

                var query = string.Format("select convert(decimal(18, 2), sum(lp.debit)) as openDebit, convert(decimal(18, 2), sum(lp.credit)) as openCredit  from  tbl_LedgerPosting lp " + " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId and al.ledgerId=" + ledgerId + FyearQuery + "  and al.accountGroupId = 22 ");

                var data1 = conn.customSelect(query);

                var ListOpeningBalance = new List<OpenBalan>();
                var newOpeningAmt = new OpenBalan();
                if (data1.Rows.Count > 0)
                {
                    foreach (DataRow row in data1.Rows)
                    {
                        newOpeningAmt.openCredit = row["openCredit"];
                        newOpeningAmt.openDebit = row["openDebit"];
                        if (string.IsNullOrEmpty(Convert.ToString(newOpeningAmt.openCredit)) || string.IsNullOrWhiteSpace(Convert.ToString(newOpeningAmt.openCredit)) || Convert.ToString(newOpeningAmt.openCredit) == "{}")
                        {
                            newOpeningAmt.openCredit = 0;
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(newOpeningAmt.openDebit)) || string.IsNullOrWhiteSpace(Convert.ToString(newOpeningAmt.openDebit)) || Convert.ToString(newOpeningAmt.openDebit) == "{}")
                        {
                            newOpeningAmt.openDebit = 0;
                        }
                    }
                }

                return newOpeningAmt;
            }

            return null;
        }

    }
    public class SuppliersVm
    {
        private decimal ledgerId;
        public decimal accountGroupId;

        public decimal LedgerId
        {
            get { return ledgerId; }
            set { ledgerId = value; }
        }
        public decimal AccountGroupId
        {
            get { return accountGroupId; }
            set { accountGroupId = value; }
        }

        public int VoucherTypeId { get; set; }
        public int VoucherNo { get; set; }

        public string CrDr { get; set; }

    }
}
