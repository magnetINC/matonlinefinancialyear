﻿using MatApi.Models;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace MatApi.Controllers.Supplier
{
    public class AccruedExpensesController : ApiController
    {
        [HttpGet]
        public MatResponse GetLookUps()
        {
            DBMatConnection db = new DBMatConnection();
            MatResponse response = new MatResponse();
            dynamic lookUps = new ExpandoObject();
            try
            {
                decimal formNo = 0;

                string id = db.getSingleValue("SELECT top 1 FormNo from tbl_AccruedExpensesMaster order by AccruedMasterId desc");
                if (id == "")
                {
                    formNo = 1;
                }
                else
                {
                    formNo = Convert.ToDecimal(id) + 1;
                }

                lookUps.formNo = formNo;
                lookUps.suppliers = new AccountLedgerSP().AccountLedgerViewSuppliersOnly();
                lookUps.expenses = new AccountLedgerSP().AccountLedgerViewOtherExpenses();
                lookUps.allLedgers = new AccountLedgerSP().AccountLedgerViewAllForComboBox();
                lookUps.currencies = new TransactionsGeneralFill().CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);

                response.ResponseCode = 200;
                response.ResponseMessage = "success!";
                response.Response = lookUps;
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong.";
            }
            return response;
        }
        [HttpPost]
        public MatResponse SaveAccruedExpenses(AccruedExpenseMaster input)
        {
            DBMatConnection db = new DBMatConnection();
            MatResponse response = new MatResponse();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();

            try
            {
                string masterQuery = string.Empty;
                string detailsQuery = string.Empty;

                masterQuery = string.Format("INSERT INTO tbl_AccruedExpensesMaster " +
                                                    "VALUES({0},'{1}','{2}','{3}','{4}',{5},{6})"
                                                    , input.Supplierid,
                                                    input.InvoiceNo,
                                                    input.FormNo,
                                                    input.TransactionDate,
                                                    input.InvoiceDate,
                                                    input.TotalAmount,
                                                    input.Currencyid);
                if (db.ExecuteNonQuery2(masterQuery))
                {
                    string id = db.getSingleValue("SELECT top 1 AccruedMasterId from tbl_AccruedExpensesMaster order by AccruedMasterId desc");

                    infoLedgerPosting.Credit = input.TotalAmount * new ExchangeRateSP().ExchangeRateViewByExchangeRateId(input.Currencyid);
                    infoLedgerPosting.Debit = 0;
                    //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                    infoLedgerPosting.Date = input.TransactionDate;
                    //infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.DetailsId = Convert.ToDecimal(id);
                    infoLedgerPosting.InvoiceNo = input.InvoiceNo;
                    infoLedgerPosting.LedgerId = input.Supplierid;
                    infoLedgerPosting.VoucherNo = input.InvoiceNo;
                    infoLedgerPosting.VoucherTypeId = 10032;
                    infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                    AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                    AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
                    PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
                    PartyBalanceSP spPartyBalance = new PartyBalanceSP();
                    infoAccountLedger = spAccountLedger.AccountLedgerView(input.Supplierid);
                    if (infoAccountLedger.BillByBill == true)
                    {
                        infoPartyBalance.Credit = Convert.ToDecimal(input.TotalAmount);
                        infoPartyBalance.Debit = 0;
                        infoPartyBalance.CreditPeriod = 0;
                        infoPartyBalance.Date = input.TransactionDate;
                        infoPartyBalance.ExchangeRateId = input.Currencyid;
                        infoPartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                        infoPartyBalance.LedgerId = input.Supplierid;
                        infoPartyBalance.ReferenceType = "NEW";
                        infoPartyBalance.InvoiceNo = input.InvoiceNo;
                        infoPartyBalance.VoucherNo = input.InvoiceNo;
                        infoPartyBalance.VoucherTypeId = 10032;
                        infoPartyBalance.AgainstInvoiceNo = "NA";
                        infoPartyBalance.AgainstVoucherNo = "NA";
                        infoPartyBalance.AgainstVoucherTypeId = 0;
                        infoPartyBalance.Extra1 = string.Empty;
                        infoPartyBalance.Extra2 = string.Empty;
                        infoPartyBalance.ExtraDate = DateTime.Now;
                        spPartyBalance.PartyBalanceAdd(infoPartyBalance);
                    }

                    foreach (var row in input.details)
                    {
                        detailsQuery = string.Format("INSERT INTO tbl_AccruedExpenseDetails " +
                                                    "VALUES({0},{1},{2},'{3}')"
                                                    , id,
                                                    row.LedgerId,
                                                    row.Amount,
                                                    row.Memo);
                        if(db.ExecuteNonQuery2(detailsQuery))
                        {
                            string dId = db.getSingleValue("SELECT top 1 AccruedDetailsId from tbl_AccruedExpenseDetails order by AccruedDetailsId desc");

                            infoLedgerPosting.Credit = 0;
                            infoLedgerPosting.Debit = row.Amount;   //TotalNetAmount(); //* spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(cmbCurrency.SelectedValue.ToString()));
                                                                       //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                            infoLedgerPosting.Date = input.TransactionDate;
                            //infoLedgerPosting.DetailsId = 0;
                            infoLedgerPosting.DetailsId = Convert.ToDecimal(dId);
                            infoLedgerPosting.InvoiceNo = input.InvoiceNo;
                            //infoLedgerPosting.LedgerId = infoPurchaseMaster.PurchaseAccount;//ledger posting of purchase account  dgvrow.Cells["dgvcmbTax"].Value.ToString
                            // ----------------- Old implementation changed by Precious and then Urefe 20160919 ------------------- //
                            //infoLedgerPosting.LedgerId = Convert.ToDecimal(row.Cells["dgvtxtExpenseAccount"].Value.ToString()); 
                            infoLedgerPosting.LedgerId = row.LedgerId;
                            infoLedgerPosting.VoucherNo = input.InvoiceNo;
                            infoLedgerPosting.VoucherTypeId = 10032;
                            infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                            infoLedgerPosting.ChequeDate = DateTime.Now;
                            infoLedgerPosting.ChequeNo = string.Empty;
                            infoLedgerPosting.Extra1 = string.Empty;
                            infoLedgerPosting.Extra2 = string.Empty;
                            infoLedgerPosting.ExtraDate = DateTime.Now;
                            spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                            response.ResponseCode = 200;
                            response.ResponseMessage = "success";
                        }
                    }
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong.";
                response.Response = e;
            }
            return response;
        }
        [HttpGet]
        public MatResponse GetAccruedExpensesForRegister()
        {
            MatResponse response = new MatResponse();
            DBMatConnection db = new DBMatConnection();

            try {
                string getQuery = string.Format("SELECT * FROM tbl_AccruedExpensesMaster");

                response.ResponseCode = 200;
                response.ResponseMessage = "sucess!";
                response.Response = db.customSelect(getQuery);
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong.";
            }

            return response;
        }
    }
    public class AccruedExpenseMaster
    {
        public decimal AccruedExpenseMasterId { get; set; }
        public decimal Supplierid { get; set; }
        public decimal TotalTaxAmount { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal GrandTotal { get; set; }
        public string Narration { get; set; }
        public DateTime InvoiceDate { get; set; }
        public DateTime TransactionDate { get; set; }
        public string InvoiceNo { get; set; }
        public string FormNo { get; set; }
        public decimal Currencyid { get; set; }
        public List<AccruedExpenseDetails> details { get; set; }
    }
    public class AccruedExpenseDetails
    {
        public decimal AccruedExpenseDetailsId { get; set; }
        public decimal AccruedExpenseMasterId { get; set; }
        public decimal TaxId { get; set; }
        public decimal LedgerId { get; set; }
        public string Memo { get; set; }
        public decimal Amount { get; set; }
        public decimal TaxAmount { get; set; }
    }
}
