﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials;
using System.Data;
using MatApi.Models.Supplier;
using System.Dynamic;
using MatApi.DBModel;
using MATFinancials.DAL;
using MatApi.Models.Register;

namespace MatApi.Controllers.Company.Masters
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PurchaseOrderController : ApiController
    {
        RouteSP routeSp;
        DateTime defaultDt = DateTime.Now;
        decimal decVoucherTypeId = 0;
        decimal decSuffixPrefixId = 0;
        bool isAutomatic = false;
        string strVoucherNo = string.Empty;
        string TableName = "PurchaseOrderMaster";
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        string strInvoiceNo = string.Empty;
        decimal decPurchaseOrderTypeId = 10;

        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public PurchaseOrderController()
        {
            routeSp = new RouteSP();
        }

        [HttpGet]
        public HttpResponseMessage LookUpData()
        {
            dynamic response = new ExpandoObject();

            var suppliers = new TransactionsGeneralFill().CashOrPartyUnderSundryCrComboFill();
            DataTable dt = new TaxSP().TaxViewAll().AsEnumerable()
                            .Where(r => r.Field<string>("taxName") != "VAT")
                            .CopyToDataTable();
            var taxes = dt;
            var products = new ProductSP().ProductViewAll();
            var currency = new TransactionsGeneralFill().CurrencyComboByDate(DateTime.Now);
            var units = new UnitSP().UnitViewAll();

            response.Suppliers = suppliers;
            response.Taxes = taxes;
            response.Currencies = currency;
            response.Products = products;
            response.Units = units;

            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }




        [HttpGet]
        public HttpResponseMessage LookUpData1()
        {
            dynamic response = new ExpandoObject();

            var suppliers = new TransactionsGeneralFill().CashOrPartyUnderSundryCrComboFill();
            DataTable dt = new TaxSP().TaxViewAll().AsEnumerable()
                            .Where(r => r.Field<string>("taxName") != "VAT")
                            .CopyToDataTable();
            var taxes = dt;
           //var products = new ProductSP().ProductViewAll();
            var currency = new TransactionsGeneralFill().CurrencyComboByDate(DateTime.Now);
            var units = new UnitSP().UnitViewAll();

            response.Suppliers = suppliers;
            response.Taxes = taxes;
            response.Currencies = currency;

            ProductSP productSp = new ProductSP();
            var products = new List<MATFinancials.ProductInfo1>();

            try
            {
                var productsDt = productSp.ProductViewAll123();
                products = new List<MATFinancials.ProductInfo1>();

                foreach (var row in productsDt)
                {
                    var product = new MATFinancials.ProductInfo1
                    {
                        ProductId = Convert.ToDecimal(row.productId),
                        ProductName = row.productName.ToString(),
                        ProductCode = row.productCode.ToString(),
                        barcode = row.productCode.ToString(),
                        BrandId = Convert.ToDecimal(row.brandId),
                        //CategoryId= Convert.ToDecimal(row["CategoryId"]),
                        EffectiveDate = Convert.ToDateTime(row.effectiveDate),
                        ExpenseAccount = Convert.ToDecimal(row.expenseAccount),
                        GodownId = Convert.ToDecimal(row.godownId),
                        GroupId = Convert.ToDecimal(row.groupId),
                        IsActive = Convert.ToBoolean(row.isActive),
                        IsallowBatch = Convert.ToBoolean(row.isallowBatch),
                        IsBom = Convert.ToBoolean(row.isBom),
                        Ismultipleunit = Convert.ToBoolean(row.ismultipleunit),
                        Isopeningstock = Convert.ToBoolean(row.isopeningstock),
                        IsshowRemember = Convert.ToBoolean(row.isshowRemember),
                        MaximumStock = Convert.ToDecimal(row.maximumStock),
                        MinimumStock = Convert.ToDecimal(row.minimumStock),
                        ModelNoId = Convert.ToDecimal(row.modelNoId),
                        Mrp = Convert.ToDecimal(row.mrp),
                        PartNo = row.partNo.ToString(),
                        //ProductType= row["ProductType"].ToString(),
                        //ProjectId= Convert.ToDecimal(row["ProjectId"]),
                        PurchaseRate = Convert.ToDecimal(row.purchaseRate),
                        RackId = Convert.ToDecimal(row.rackId),
                        ReorderLevel = Convert.ToDecimal(row.reorderLevel),
                        SalesAccount = Convert.ToDecimal(row.salesAccount),
                        SalesRate = Convert.ToDecimal(row.salesRate),
                        SizeId = Convert.ToDecimal(row.sizeId),
                        TaxapplicableOn = row.taxapplicableOn.ToString(),
                        TaxId = Convert.ToDecimal(row.taxId),
                        UnitId = Convert.ToDecimal(row.unitId),
                    };

                    var Result = MatApi.Models.PublicVariables.IsObjectInDate(product.EffectiveDate, Models.PublicVariables.FromDate, Models.PublicVariables.ToDate);

                    if (Result)
                    {
                        product.IsInRightYear = true;
                    }
                    else
                    {
                        product.IsInRightYear = false;
                    }

                    products.Add(product);
                }
            }
            catch (Exception ex)
            {

            }

            response.Products = products;

            response.Units = units;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetSuppliers()
        {

            var suppliers = new TransactionsGeneralFill().CashOrPartyUnderSundryCrComboFill();

            return Request.CreateResponse(HttpStatusCode.OK, (object)suppliers);
        }

        [HttpPost]
        public HttpResponseMessage Registers(PurchaseOrderRegisterVM input)
        {
            var response= new PurchaseOrderMasterSP().PurchaseOrderMasterViewAll(input.InvoiceNo, input.LedgerId, input.FromDate, input.ToDate, input.Condition, input.DoneBy);
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public decimal PurchaseOrderListingCount()
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("select count(orderStatus) from tbl_PurchaseOrderMaster where orderStatus ='Pending'");
            var count = conn.getSingleValue(queryStr);

            return Convert.ToDecimal(count);
        }

        [HttpGet]
        public HttpResponseMessage PurchaseOrderListing(DateTime fromDate, DateTime toDate, string approved)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            dynamic response = new ExpandoObject(); ;
            string GetQuery = string.Format("");
            if (approved == "All" || approved == "" || approved == null)
            {
                GetQuery = string.Format("");
                GetQuery = string.Format("SELECT * FROM tbl_PurchaseOrderMaster WHERE extraDate BETWEEN '{0}' AND '{1}' ORDER BY extraDate DESC", fromDate, toDate);
            }
            else
            {
                GetQuery = string.Format("");
                GetQuery = string.Format("SELECT * FROM tbl_PurchaseOrderMaster WHERE extraDate BETWEEN '{0}' AND '{1}' AND orderStatus='{2}' ORDER BY extraDate DESC", fromDate, toDate, approved);
            }
            response = db.GetDataSet(GetQuery);
            
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetPurchaseOrderDetails(decimal purchaseOrderMasterId)
        {
            dynamic response = new ExpandoObject();
            var master = new PurchaseOrderMasterSP().PurchaseOrderMasterView(purchaseOrderMasterId);
            var details = new PurchaseOrderDetailsSP().PurchaseOrderDetailsViewByMasterId(purchaseOrderMasterId);

            response.Master = master;
            response.Details = details;
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
     
        [HttpGet]
        public HttpResponseMessage SearchProduct(string searchBy,string filter)
        {
            SalesDetailsSP spSalesDetails = new SalesDetailsSP();
            UnitSP spUnit = new UnitSP();
            GodownSP spGodown = new GodownSP();
            BatchSP spBatch = new BatchSP();
            TaxSP spTax = new TaxSP();
            RackSP spRack = new RackSP();
            UnitConvertionSP SpUnitConvertion = new UnitConvertionSP();

            dynamic response = new ExpandoObject();


            if (searchBy == "ProductCode")
            {
                var productDetails = new ProductSP().ProductViewByCode(filter);
                var productId = productDetails.ProductId;
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                if(unit == null || unit.Rows.Count == 0)
                {
                    unit = GetDefaultProductUnit(productDetails);
                }
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.GodownId));
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                //response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productId));
            }
            else if (searchBy == "ProductName")
            {
                var productDetails = new ProductSP().ProductViewByName(filter);
                var productId = productDetails.ProductId;
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                if (unit == null || unit.Rows.Count == 0)
                {
                    unit = GetDefaultProductUnit(productDetails);
                }
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.GodownId));
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                //response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productId));
            }
            else if (searchBy == "Barcode")
            {
                var productDetails = new ProductSP().ProductViewByCode(filter);
                var productId = productDetails.ProductId;
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                if (unit == null || unit.Rows.Count == 0)
                {
                    unit = GetDefaultProductUnit(productDetails);
                }
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.GodownId));
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                //response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productId));
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        private DataTable GetDefaultProductUnit(ProductInfo product)
        {
            var result = new DataTable();
            var resultCols = new List<string>() { "unitId", "unitName", "unitconversionId", "conversionRate", "defaulunitId" };
            DataColumn dtColumn;
            DataRow myDataRow;
            try
            {
                foreach (var item in resultCols)
                {
                    dtColumn = new DataColumn();
                    dtColumn.DataType = typeof(String);
                    dtColumn.ColumnName = item;
                    result.Columns.Add(dtColumn);
                }
                var dbUnit = context.tbl_Unit.FirstOrDefault(x =>x.unitId == product.UnitId);

                myDataRow = result.NewRow();
                myDataRow["unitId"] = product.UnitId;
                myDataRow["unitName"] = dbUnit.unitName;
                myDataRow["unitconversionId"] = null;
                myDataRow["conversionRate"] = 1.00;
                myDataRow["defaulunitId"] = product.UnitId;


                result.Rows.Add(myDataRow);

            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
            }
            return result;
        }

        public HttpResponseMessage GetTaxAmount(string taxId, string amount)
        {

            decimal taxAmount = 0;
            TaxSP SpTax = new TaxSP();
            var amt = Convert.ToDecimal(amount);
            TaxInfo InfoTaxObj = SpTax.TaxView(Convert.ToDecimal(taxId));
            taxAmount = Math.Round(((amt * InfoTaxObj.Rate) / (100)), PublicVariables._inNoOfDecimalPlaces);
            return Request.CreateResponse(HttpStatusCode.OK, taxAmount);
        }

 
        [HttpPost]
        public bool SavePurchaseOrder(PurchaseOrderModel obj)
        {
            //route.ExtraDate = DateTime.Now;

            if (SaveFunction(obj) == 0) {
                return false;
            }
            else
            {
                return true;
            }
           //routeSp.RouteAdd(route);
        }


        private decimal getQuantityInStock(decimal productId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format(" SELECT  convert(decimal(18, 2), (ISNULL(SUM(s.inwardQty), 0) - ISNULL(SUM(s.outwardQty), 0))) AS Currentstock " +
                " FROM tbl_StockPosting s inner join tbl_product p on p.productId = s.productId " +
                " INNER JOIN tbl_Batch b on s.batchId = b.batchId " +
                " WHERE p.productId = '{0}' AND s.date <= '{1}' ", productId, MATFinancials.PublicVariables._dtToDate);
            return Convert.ToDecimal(conn.getSingleValue(queryStr));
        }

        public decimal SaveFunction(PurchaseOrderModel obj)
        {
            decimal res = 0;
            try
            {
                PurchaseOrderMasterInfo infoPurchaseOrderMaster = new PurchaseOrderMasterInfo();
                PurchaseOrderDetailsSP spPurchaseOrderDetails = new PurchaseOrderDetailsSP();
                PurchaseOrderDetailsInfo infoPurchaseOrderDetails = new PurchaseOrderDetailsInfo();
                PurchaseOrderMasterSP spPurchaseOrderMaster = new PurchaseOrderMasterSP();
                ProductInfo infoProduct = new ProductInfo();
                ProductSP spProduct = new ProductSP();
                SettingsSP spSettings = new SettingsSP();
                
                infoPurchaseOrderMaster.VoucherNo = obj.OrderNo;
                infoPurchaseOrderMaster.Date = Convert.ToDateTime(obj.TransDate);
                infoPurchaseOrderMaster.DueDate = Convert.ToDateTime(obj.DueDate);
                infoPurchaseOrderMaster.LedgerId = Convert.ToDecimal(obj.Supplier);
                infoPurchaseOrderMaster.VoucherTypeId = decPurchaseOrderTypeId;
                infoPurchaseOrderMaster.InvoiceNo = obj.OrderNo;//obj.OrderNo;
                infoPurchaseOrderMaster.UserId = PublicVariables._decCurrentUserId;
                infoPurchaseOrderMaster.EmployeeId = PublicVariables._decCurrentUserId;//by default current userid used as current employeeid
                infoPurchaseOrderMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                var totalAmount = 0.0m;
                foreach(var i in obj.LineItems)
                {
                    totalAmount += i.Amount;
                }
                infoPurchaseOrderMaster.Narration = obj.Narration.Trim();
                infoPurchaseOrderMaster.DoneBy = obj.DoneBy.Trim();
                infoPurchaseOrderMaster.TotalAmount = Convert.ToDecimal(totalAmount);
                infoPurchaseOrderMaster.exchangeRateId = Convert.ToDecimal(obj.Currency);
                infoPurchaseOrderMaster.Extra1 = string.Empty;
                infoPurchaseOrderMaster.Extra2 = string.Empty;
                infoPurchaseOrderMaster.orderStatus = "Pending";
                var decPurchaseOrderMasterIdentity = Convert.ToDecimal(spPurchaseOrderMaster.PurchaseOrderMasterAdd(infoPurchaseOrderMaster));

                int inRowcount = obj.LineItems.Count;
                if (inRowcount > 0)
                {
                    foreach (var lintItm in obj.LineItems)
                    {
                        infoPurchaseOrderDetails.PurchaseOrderMasterId = decPurchaseOrderMasterIdentity;
                        if (lintItm.ProductCode != null && lintItm.ProductCode != "")
                        {
                            infoProduct = spProduct.ProductViewByCode(lintItm.ProductCode);
                            infoPurchaseOrderDetails.ProductId = infoProduct.ProductId;
                        }
                        var unit = new UnitSP().UnitViewAllByProductId(infoProduct.ProductId);
                        var batch = new BatchSP().BatchIdViewByProductId(infoProduct.ProductId);
                        var unitconv = new UnitConvertionSP().UnitViewAllByProductId(infoProduct.ProductId);
                        if (lintItm.Qty > 0)
                        {
                            infoPurchaseOrderDetails.Qty = Convert.ToDecimal(lintItm.Qty);
                        }
                        infoPurchaseOrderDetails.UnitId = Convert.ToDecimal(unit.Rows[0].ItemArray[0]);
                        infoPurchaseOrderDetails.UnitConversionId = unitconv.UnitconvertionId;//Convert.ToDecimal(dgvPurchaseOrder.Rows[inI].Cells["dgvtxtUnitConversionId"].Value.ToString());
                        infoPurchaseOrderDetails.Rate = Convert.ToDecimal(lintItm.Rate);
                        infoPurchaseOrderDetails.Amount = Convert.ToDecimal(lintItm.Amount);
                        infoPurchaseOrderDetails.SlNo = Convert.ToInt32(lintItm.SiNo);
                        infoPurchaseOrderDetails.Extra1 = string.Empty;
                        infoPurchaseOrderDetails.Extra2 = string.Empty;
                        infoPurchaseOrderDetails.ExtraDate = DateTime.Now;
                        infoPurchaseOrderDetails.ProjectId = 0;
                        infoPurchaseOrderDetails.CategoryId = 0;
                        infoPurchaseOrderDetails.TaxId = lintItm.TaxId;
                        infoPurchaseOrderDetails.TaxAmount = lintItm.tAmount;
                        if (lintItm.Description != null && !string.IsNullOrEmpty(lintItm.Description))
                        {
                            infoPurchaseOrderDetails.itemDescription = lintItm.Description;
                        }
                        res = spPurchaseOrderDetails.PurchaseOrderDetailsAdd(infoPurchaseOrderDetails);
                    }
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "PO35:" + ex.Message;
            }
            return res;
        }

        [HttpGet]
        public string GetAutoVoucherNo()
        {
            return generateVoucherNo();
            
        }

        private string generateVoucherNo()
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            PurchaseOrderMasterSP spMaster = new PurchaseOrderMasterSP();
            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            SettingsSP spSettings = new SettingsSP();

            strVoucherNo = "0";
            strVoucherNo = spMaster.PurchaseOrderVoucherMasterMax(decPurchaseOrderTypeId).ToString();
            if (strVoucherNo == string.Empty)
            {
                strVoucherNo = "0";
            }
            strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPurchaseOrderTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, TableName);
            if (strVoucherNo != spMaster.PurchaseOrderVoucherMasterMax(decPurchaseOrderTypeId).ToString())
            {
                strVoucherNo = spMaster.PurchaseOrderVoucherMasterMax(decPurchaseOrderTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPurchaseOrderTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, TableName);
                if (spMaster.PurchaseOrderVoucherMasterMax(decPurchaseOrderTypeId) == "0")
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPurchaseOrderTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, TableName);
                }
            }
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decPurchaseOrderTypeId, DateTime.Now);
            strPrefix = infoSuffixPrefix.Prefix;
            strSuffix = infoSuffixPrefix.Suffix;
            var decPurchaseSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
            var strOrderNo = strPrefix + strVoucherNo + strSuffix;

            return strOrderNo;
        }

        [HttpGet]
        public bool DeletePurchaseOrder(decimal id)
        {
            //PurchaseOrderMasterSP spPurchaseOrderMaster = new PurchaseOrderMasterSP();
            //PurchaseOrderDetailsSP spPurchaseOrderDetails = new PurchaseOrderDetailsSP();



            //if (spPurchaseOrderMaster.PurchaseOrderMasterDelete(id) > 0)
            //{
            //    DBMatConnection db = new DBMatConnection();

            //    string deleteQuery = string.Format("DELETE FROM tbl_PurchaseOrderDetails WHERE purchaseOrderMasterId = {0}", id);
            //    return db.ExecuteNonQuery2(deleteQuery);
            //}
            //else
            //{
            //    return false;
            //} 
            try
            {
                var orderMaster = context.tbl_PurchaseOrderMaster.Find(id);
                if (orderMaster != null)
                {
                    var purchaseOrderDetails =
                        context.tbl_PurchaseOrderDetails.Where(a => a.purchaseOrderMasterId == id).ToList();
                    context.tbl_PurchaseOrderMaster.Remove(orderMaster);
                    context.tbl_PurchaseOrderDetails.RemoveRange(purchaseOrderDetails);
                    context.SaveChanges();
                    return true;
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            return false;
        }
        [HttpPost]
        public bool EditPurchaseOrder(PurchaseOrderModel obj)
        {
            bool res = false;
            try
            {
                PurchaseOrderMasterInfo infoPurchaseOrderMaster = new PurchaseOrderMasterInfo();
                PurchaseOrderDetailsSP spPurchaseOrderDetails = new PurchaseOrderDetailsSP();
                PurchaseOrderDetailsInfo infoPurchaseOrderDetails = new PurchaseOrderDetailsInfo();
                PurchaseOrderMasterSP spPurchaseOrderMaster = new PurchaseOrderMasterSP();
                ProductInfo infoProduct = new ProductInfo();
                ProductSP spProduct = new ProductSP();
                SettingsSP spSettings = new SettingsSP();

                var newObj2 = obj.LineItems;

                infoPurchaseOrderMaster.VoucherNo = obj.OrderNo;
                infoPurchaseOrderMaster.PurchaseOrderMasterId = obj.orderId;
                infoPurchaseOrderMaster.Date = Convert.ToDateTime(obj.TransDate);
                infoPurchaseOrderMaster.DueDate = Convert.ToDateTime(obj.DueDate);
                infoPurchaseOrderMaster.LedgerId = Convert.ToDecimal(obj.Supplier);
                infoPurchaseOrderMaster.VoucherTypeId = decPurchaseOrderTypeId;
                infoPurchaseOrderMaster.InvoiceNo = obj.OrderNo;//obj.OrderNo;
                infoPurchaseOrderMaster.UserId = PublicVariables._decCurrentUserId;
                infoPurchaseOrderMaster.EmployeeId = PublicVariables._decCurrentUserId;//by default current userid used as current employeeid
                infoPurchaseOrderMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoPurchaseOrderMaster.Narration = obj.Narration.Trim(); 
                var totalAmount = 0.0m;
                foreach(var i in obj.LineItems)
                {
                    totalAmount += i.Amount;
                }
                infoPurchaseOrderMaster.TotalAmount = Convert.ToDecimal(totalAmount);
                infoPurchaseOrderMaster.exchangeRateId = Convert.ToDecimal(obj.Currency);
                infoPurchaseOrderMaster.Extra1 = string.Empty;
                infoPurchaseOrderMaster.Extra2 = string.Empty;
                infoPurchaseOrderMaster.orderStatus = "Pending";
                spPurchaseOrderMaster.PurchaseOrderMasterEdit(infoPurchaseOrderMaster);
              
                DataTable dbOrderDetails = spPurchaseOrderDetails.PurchaseOrderDetailsViewByMasterId(obj.orderId);
                for (int i = 0; i < dbOrderDetails.Rows.Count; i++)
                {
                    var exist = newObj2.Find(p => p.orderDetailsId == Convert.ToDecimal(dbOrderDetails.Rows[i].ItemArray[0]));

                    if (exist == null)
                    {
                        //spPurchaseOrderDetails.PurchaseOrderDetailsDelete(Convert.ToDecimal(dbOrderDetails.Rows[i].ItemArray[0]));
                        DBMatConnection conn = new DBMatConnection();
                        string deleteQuery = string.Format("DELETE FROM tbl_purchaseOrderDetails WHERE purchaseOrderDetailsId={0}",
                            Convert.ToDecimal(dbOrderDetails.Rows[i].ItemArray[0]));
                        conn.ExecuteNonQuery2(deleteQuery);
                    }
                }

                foreach (var lintItm in newObj2)
                {
                    if (lintItm.orderDetailsId == 0)
                    {
                        infoPurchaseOrderDetails.PurchaseOrderMasterId = obj.orderId;
                        if (lintItm.ProductCode != null && lintItm.ProductCode != "")
                        {
                            infoProduct = spProduct.ProductViewByCode(lintItm.ProductCode);
                            infoPurchaseOrderDetails.ProductId = infoProduct.ProductId;
                        }
                        var unit = new UnitSP().UnitViewAllByProductId(infoProduct.ProductId);
                        var batch = new BatchSP().BatchIdViewByProductId(infoProduct.ProductId);
                        var unitconv = new UnitConvertionSP().UnitViewAllByProductId(infoProduct.ProductId);
                        if (lintItm.Qty > 0)
                        {
                            infoPurchaseOrderDetails.Qty = Convert.ToDecimal(lintItm.Qty);
                        }
                        infoPurchaseOrderDetails.UnitId = Convert.ToDecimal(unit.Rows[0].ItemArray[0]);
                        infoPurchaseOrderDetails.UnitConversionId = unitconv.UnitconvertionId;//Convert.ToDecimal(dgvPurchaseOrder.Rows[inI].Cells["dgvtxtUnitConversionId"].Value.ToString());
                        infoPurchaseOrderDetails.Rate = Convert.ToDecimal(lintItm.Rate);
                        infoPurchaseOrderDetails.Amount = Convert.ToDecimal(lintItm.Amount);
                        infoPurchaseOrderDetails.SlNo = Convert.ToInt32(lintItm.SiNo);
                        infoPurchaseOrderDetails.Extra1 = string.Empty;
                        infoPurchaseOrderDetails.Extra2 = string.Empty;
                        infoPurchaseOrderDetails.ExtraDate = DateTime.Now;
                        infoPurchaseOrderDetails.ProjectId = 0;
                        infoPurchaseOrderDetails.CategoryId = 0;
                        infoPurchaseOrderDetails.TaxId = lintItm.TaxId;
                        infoPurchaseOrderDetails.TaxAmount = lintItm.tAmount;
                        if (lintItm.Description != null && !string.IsNullOrEmpty(lintItm.Description))
                        {
                            infoPurchaseOrderDetails.itemDescription = lintItm.Description;
                        }
                        spPurchaseOrderDetails.PurchaseOrderDetailsAdd(infoPurchaseOrderDetails);
                        res = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "PO35:" + ex.Message;
            }
            return res;
        }
    }
}
