﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials;
using MatApi.Models.Register;
using System.Data;
using MatApi.DBModel;
using System.Data.Entity;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PaymentsController : ApiController
    {
        private DBMATAccounting_MagnetEntities1 db = new DBMATAccounting_MagnetEntities1();


        public PaymentsController()
        {

        }

        [HttpGet]
        public DataTable GetSuppliers()
        {
            DataTable dtbl = new DataTable();
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            dtbl = obj.BankOrCashComboFill(false);
            return dtbl;
        }

        [HttpPost]
        public HttpResponseMessage Registers(PaymentRegisterVM input)
        {
            var resp = new PaymentMasterSP().PaymentMasterSearch(input.FromDate, input.ToDate, input.LedgerId, input.InvoiceNo, input.DoneBy, input.UserId, input.voucherNo);
            return Request.CreateResponse(HttpStatusCode.OK,(object)resp);
        }

        [HttpGet]
        public HttpResponseMessage GetPaymentDetails(decimal masterid)
        {
            var currency = string.Empty;
            var master = new PaymentMasterSP().PaymentMasterView(masterid);
            var details = new PaymentDetailsSP().PaymentDetailsViewByMasterId(masterid);

            var projects = db.tbl_Project;
            var categories = db.tbl_Category; 

            if (details.Rows.Count > 0) {
                
               currency = new PaymentDetailsSP().GetCurrencyNameByExchangeRate(details.Rows[0]["exchangeRateId"].ToString());
                
            }

            var resp = new {
                    master,
                    details,
                    currency,
                    projects,
                    categories
            };

            return Request.CreateResponse(HttpStatusCode.OK, (object)resp);
        }

        [HttpDelete]
        public IHttpActionResult Delete(decimal masterid)
        {
            new PaymentMasterSP().PaymentsDelete(masterid);

            return Ok(new { status = 200, message = "Delete Made Successful" });
        }
    }
}
