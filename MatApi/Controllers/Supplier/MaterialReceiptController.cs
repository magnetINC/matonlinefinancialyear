using MatApi.Models;
using MatApi.Models.Register;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;
using MatApi.Models.Supplier;
using MATFinancials.Other;
using EntityState = System.Data.Entity.EntityState;
using System.Data.Entity.SqlServer;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class MaterialReceiptController : ApiController
    {
        string strVoucherNo = string.Empty;//To save the automatically generated voucher number
        decimal decMaterialReceiptVoucherTypeId = 11;
        decimal decDeliveryNoteSuffixPrefixId = 0;//To store the SuffixPrefix Id of the selected voucher type
        decimal decDeliveryNoteMasterId = 0;
        decimal decDelivryNoteIdToEdit = 0;//To take the deliveryNoteMasterId coming from frmDeliveryNoteRegister and frmDeliveryNoteReport
        decimal decMaterialReceiptMasterIdentity = 0;
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        decimal decMaterialReceiptSuffixPrefixId = 0;
        string strReceiptNo = string.Empty;
        string tableName = "MaterialReceiptMaster";
        DataTable dtblDetailsProd = new DataTable();
        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        
        public MaterialReceiptController()
        {

        }

        [HttpGet]
        public HttpResponseMessage InvoiceLookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var customers = transactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill();
            var salesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();
            var pricingLevels = transactionGeneralFillObj.PricingLevelViewAll();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            var applyOn = new PurchaseOrderDetailsSP().VoucherTypeCombofillforPurchaseOrderReport();
            var users = new UserSP().UserViewAll();

            dynamic response = new ExpandoObject();
            response.Customers = customers;
            response.SalesMen = salesMen;
            response.PricingLevels = pricingLevels;
            response.Currencies = currencies;
            response.ApplyOn = applyOn;
            response.Users = users;
            response.stores = response.Stores = new GodownSP().GodownViewAll();

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public decimal MaterialReceiptCount()
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("select count(approved) from tbl_MaterialReceiptMaster_Pending where approved ='Pending'");
            var count = conn.getSingleValue(queryStr);

            return Convert.ToDecimal(count);
        }

        [HttpGet]
        public DataTable GetOrderNo(decimal supplierId, decimal voucherTypeId)
        {
            return new PurchaseMasterSP().GetOrderNoCorrespondingtoLedger(supplierId, 0, voucherTypeId);
        }

        [HttpPost]
        public HttpResponseMessage Registers(MaterialReceiptRegisterVM input)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            dynamic response = new ExpandoObject(); ;

            //var response = new MaterialReceiptMasterSP().MaterialReceiptMasterViewAll(input.InvoiceNo, input.LedgerId, input.FromDate, input.ToDate);
            string GetQuery = string.Format("");
            GetQuery = string.Format("SELECT * FROM tbl_MaterialReceiptMaster WHERE date BETWEEN '{0}' AND '{1}' ORDER BY date DESC", input.FromDate, input.ToDate);

          /*  var obj = new List<dynamic>();
            var data = context.tbl_MaterialReceiptMaster.ToList();
            foreach (var a in data)
            {
                var rec = Convert.ToInt32(a.doneBy);
                List<tbl_User> dat = context.tbl_User.Where(x => x.userId == rec).ToList();
            };*/

            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();
            var supp = transactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill();
            var salesMen = new UserSP().UserViewAll();
            var result = db.GetDataSet(GetQuery);

            response.Suppliers = supp;
            response.User = salesMen;
            response.Data = result;
            response.warehouses = new GodownSP().GodownViewAll();
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage RegisterDetails(decimal id)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string GetDetails = string.Format("");

            GetDetails = string.Format("SELECT * FROM tbl_MaterialReceiptDetails WHERE materialReceiptMasterId={0}", id);

            dynamic response = new ExpandoObject();
            response.units = new UnitSP().UnitViewAll();
            response.products = new ProductSP().ProductViewAll();
            response.detailsfull = db.customSelect(GetDetails);
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage RegisterDetails2(decimal id)
        {
            dynamic response = new ExpandoObject();
            response.getDetails = new MaterialReceiptDetailsSP().MaterialReceiptDetailsViewByMasterId(id);
            response.master = new MaterialReceiptMasterSP().MaterialReceiptMasterViewByReceiptMasterId(id);
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetPendingMaterialReceipt(DateTime fromDate, DateTime toDate, string approved)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            dynamic response = new ExpandoObject(); ;
            string GetQuery = string.Format("");
            if (approved == "All" || approved == "" || approved == null)
            {
                GetQuery = string.Format("");
                GetQuery = string.Format("SELECT * FROM tbl_MaterialReceiptMaster_Pending WHERE date BETWEEN '{0}' AND '{1}' ORDER BY date DESC", fromDate, toDate);
            }
            else
            {
                GetQuery = string.Format("");
                GetQuery = string.Format("SELECT * FROM tbl_MaterialReceiptMaster_Pending WHERE date BETWEEN '{0}' AND '{1}' AND approved='{2}' ORDER BY date DESC", fromDate, toDate, approved);
            }
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();
            var supp = transactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill();
            var salesMen = new UserSP().UserViewAll();
            var result = db.GetDataSet(GetQuery);

            response.Suppliers = supp;
            response.User = salesMen;
            response.Data = result;
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetMaterialReceiptLineItemFromOrderNo(decimal invoiceNo)
        {
            dynamic response = new ExpandoObject();
            dtblDetailsProd = new PurchaseOrderDetailsSP().PurchaseOrderDetailsViewByOrderMasterIdWithRemaining(invoiceNo, 0);
            response.Details = dtblDetailsProd;
            response.Units = new UnitSP().UnitViewAll();
            response.Stores = new GodownSP().GodownViewAll();
            response.Racks = new RackSP().RackViewAll();
            response.Batches = new BatchSP().BatchViewAll();
            response.Taxes = new TaxSP().TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
            //response.ProductDetails = new ProductSP().ProductView(Convert.ToDecimal(dtblDetails.Rows[0].ItemArray[2]));
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public IHttpActionResult GetMaterialReceiptLineItemFromMasterId(int masterId)
        {
            var lineItem = new List<MaterialReceiptLineItems>();
            var items = context.tbl_MaterialReceiptDetails.Where(a => a.materialReceiptMasterId == masterId).ToList();
            var master = context.tbl_MaterialReceiptMaster.Find(masterId); 
            
            object obj;
            foreach (var i in items)
            {
                var product = context.tbl_Product.Where(a => a.productId == i.productId).FirstOrDefault();
                
                lineItem.Add( new MaterialReceiptLineItems()
                {
                    MaterialDetailId =(int) i.materialReceiptDetailsId,
                    BatchId = Convert.ToDecimal(i.batchId),
                    UnitId = Convert.ToDecimal(i.batchId),
                    ProductId =  Convert.ToDecimal(i.productId),
                    Description = i.itemDescription,
                    InvoiceNo = master.invoiceNo,
                    OrderDetailsId =  Convert.ToDecimal(i.orderDetailsId),
                    ProductBarcode =  Convert.ToDecimal( product.productId),
                    ProductCode=  product.productCode,
                    ProductName = product.productName,
                    Quantity =  Convert.ToDecimal( i.qty),
                    RackId =  Convert.ToDecimal(i.rackId),
                    Rate =  Convert.ToDecimal( i.rate),
                    SL =  Convert.ToInt32( i.slno),
                    StoreId =  Convert.ToDecimal( i.godownId),
                    VoucherNo = master.voucherNo,
                    VoucherTypeId = Convert.ToDecimal(master.voucherTypeId),
                    UnitConversionId =  Convert.ToDecimal(i.unitConversionId),
                    Amount = Convert.ToDecimal(i.amount),
                    
                });
            }

            if (master != null)
            {
                var puchaseOrder = context.tbl_PurchaseOrderMaster
                    .Where(a => a.purchaseOrderMasterId == master.orderMasterId).FirstOrDefault();
                return Json(new
                {
                    voucherTypeId = master.voucherTypeId,
                    ledgerId = master.ledgerId,
                    invoiceNo = master.invoiceNo,
                    lineItem = lineItem,
                    orderNo = puchaseOrder?.invoiceNo,
                    vendorInvoiceNo = master.lrNo,
                    date = master.date,
                });
            }
            return Json(""); 
        }
        [HttpGet]
        public HttpResponseMessage GetPendingMaterialReceiptDetails(decimal materialReceiptMasterId)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string GetMaster = string.Format("");
            string GetDetails = string.Format("");

            GetMaster = string.Format("SELECT * FROM tbl_MaterialReceiptMaster_Pending WHERE materialReceiptMasterId={0}", materialReceiptMasterId);
            GetDetails = string.Format("SELECT * FROM tbl_MaterialReceiptDetails_Pending WHERE materialReceiptMasterId={0}", materialReceiptMasterId);
            dynamic response = new ExpandoObject();
            var master1 = db.customQuery(GetMaster);
            // var master2 = new PurchaseOrderMasterSP().PurchaseOrderMasterView(materialReceiptPendingMasterId);
            var detailsfull = db.customSelect(GetDetails);
            var units = new UnitSP().UnitViewAll();
            var products = new ProductSP().ProductViewAll();

            response.Master1 = master1;
            //response.Master2 = master2;
            response.FullDetails = detailsfull;
            response.products = products;
            response.units = units;
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public string GetAutoVoucherNo()
        {
            return VoucherNumberGeneration();
        }
        public string VoucherNumberGeneration()
        {
            try
            {
                //SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                //SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                //infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decMaterialReceiptVoucherTypeId, date);
                //strPrefix = infoSuffixPrefix.Prefix;
                //strSuffix = infoSuffixPrefix.Suffix;
                //decMaterialReceiptSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                //strReceiptNo = strPrefix + strVoucherNo + strSuffix;
                //return strReceiptNo;

                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                MaterialReceiptMasterSP spMaterialReceipt = new MaterialReceiptMasterSP();
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }

                //spMaterialReceipt.MaterialReceiptMasterGetMaxPlusOne 

                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decMaterialReceiptVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                if (Convert.ToDecimal(strVoucherNo) != GetMaterialReceiptMasterGetMax(decMaterialReceiptVoucherTypeId))
                {
                    strVoucherNo = GetMaterialReceiptMasterGetMax(decMaterialReceiptVoucherTypeId).ToString();
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decMaterialReceiptVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                    if (GetMaterialReceiptMasterGetMax(decMaterialReceiptVoucherTypeId).ToString() == "0")
                    {
                        strVoucherNo = "0";
                        strVoucherNo = obj.VoucherNumberAutomaicGeneration(decMaterialReceiptVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                    }
                }

                strReceiptNo = strPrefix + strVoucherNo + strSuffix;


                //if (isAutomatic)
                //{
                //    SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                //    SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                //    infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decMaterialReceiptVoucherTypeId, dtpDate.Value);
                //    strPrefix = infoSuffixPrefix.Prefix;
                //    strSuffix = infoSuffixPrefix.Suffix;
                //    decMaterialReceiptSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                //    strReceiptNo = strPrefix + strVoucherNo + strSuffix;
                //    txtReceiptNo.Text = strReceiptNo;
                //    txtReceiptNo.ReadOnly = true;
                //}
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "MR1:" + ex.Message;
            }
            return strReceiptNo;
        }


        public decimal GetMaterialReceiptMasterGetMax(decimal decVoucherTypeId)
        {
            decimal strVoucherNo = 0;

            try
            {

                var context = new MatApi.DBModel.DBMATAccounting_MagnetEntities1();

                var purchaseMaster = context.tbl_MaterialReceiptMaster.Where(pm => pm.voucherTypeId == decVoucherTypeId).ToList();

                var LastTransacMid = purchaseMaster[purchaseMaster.Count - 2];
                var LastTransacLast = purchaseMaster[purchaseMaster.Count - 1];

                // string numericVoucherNo = new String(LastTransacLast.voucherNo.Where(Char.IsDigit).ToArray());

                // strVoucherNo = Convert.ToDecimal(LastTransacMid.voucherNo);

                try
                {
                    strVoucherNo = Convert.ToDecimal(LastTransacLast.voucherNo);
                }
                catch (Exception ex3)
                {
                    var midset = purchaseMaster.Where(pm => pm.voucherNo.All(char.IsDigit)).ToList();

                    var MaxNumber = midset.Max(i => Convert.ToDecimal(i.voucherNo));

                    strVoucherNo = MaxNumber;
                }
            }
            catch (Exception ex)
            {

            }

            return strVoucherNo;
        }

        [HttpPost]
        public MatResponse SaveOrEdit(CreateMaterialReceiptVM input)
        {
            MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();
            try
            {
                // to take assign voucher number in case automatic voucher numbering is set to off urefe 20161208
                //if (!isAutomatic && txtReceiptNo.Text.Trim() != string.Empty)
                //{
                //    strVoucherNo = txtReceiptNo.Text.Trim();
                //}
                //dgvProduct.ClearSelection();
                //int inRow = dgvProduct.RowCount;
                String strInvoiceNo = VoucherNumberGeneration();
               
                if (spMaterialReceiptMaster.MaterialReceiptNumberCheckExistence(input.ReceiptNo, decMaterialReceiptVoucherTypeId) == true)
                {
                    //Messages.InformationMessage("Receipt number already exist");
                    //txtReceiptNo.Focus();
                }
             

            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "MR33:" + ex.Message;
               // return 0;
            }
            return SaveFunction(input);
        }

        [HttpPost]
        public bool SavePending(CreateMaterialReceiptVM input)
        {
            try
            {
                var master = new PurchaseOrderMasterSP().PurchaseOrderMasterView(input.OrderMasterId);
                MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
                input.Date = DateTime.Now;
                var approved = "Pending";
                string masterQuery = string.Format("INSERT INTO tbl_MaterialReceiptMaster_Pending " +
                                                    "VALUES('{0}','{1}',{2},{3},'{4}',{5},'{6}',{7},'{8}',{9},{10},{11},'{12}',{13},'{14}',{15},'{16}','{17}','{18}', '{19}');"
                                                    , input.ReceiptNo,
                                                    input.ReceiptNo,
                                                    1,
                                                    decMaterialReceiptVoucherTypeId,
                                                    input.Date,
                                                    input.SupplierId,
                                                    "",
                                                    input.OrderMasterId,
                                                    input.Narration,
                                                    input.TotalAmount,
                                                    1,
                                                    MATFinancials.PublicVariables._decCurrentUserId,
                                                    "",
                                                    1,
                                                    "",
                                                    MATFinancials.PublicVariables._decCurrentFinancialYearId,
                                                    DateTime.Now,
                                                    "",
                                                    "",
                                                    approved);

                string detailsQuery = string.Format("");
                if (db.ExecuteNonQuery2(masterQuery))
                {
                    string id = db.getSingleValue("SELECT top 1 materialReceiptMasterId from tbl_MaterialReceiptMaster_Pending order by materialReceiptMasterId desc");
                    foreach (var row in input.LineItems)
                    {
                        var amount = row.Rate * row.Quantity;
                        detailsQuery = string.Format("INSERT INTO tbl_MaterialReceiptDetails_Pending " +
                                                    "VALUES({17},{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},'{11}','{12}','{13}',{14},{15},'{16}')"
                                                    , row.ProductId,
                                                    row.OrderDetailsId,
                                                    row.Quantity,
                                                    row.Rate,
                                                    row.UnitId,
                                                    row.UnitConversionId,
                                                    row.BatchId,
                                                    row.StoreId,
                                                    row.RackId,
                                                    amount,
                                                    row.SL,
                                                    DateTime.Now,
                                                    "",
                                                    "",
                                                    0,
                                                    0,
                                                    row.Description,
                                                    id);
                        db.ExecuteNonQuery2(detailsQuery);
                        amount = 0;
                    }
                    if (input.OrderDetailsId > 0)
                    {
                        string status = "Approved";
                        string updateQuery = string.Format("");
                        updateQuery = string.Format("UPDATE tbl_PurchaseOrderMaster " +
                                                    "SET orderStatus='{0}' " +
                                                    "WHERE purchaseOrderMasterId={1} ", status, input.OrderMasterId);
                        if (db.customUpdateQuery(updateQuery) > 0)
                        {
                            return true;
                        }
                    }
                    else
                    {
                        return true;
                    }
                }
                //db.CloseConnection();
            }
            catch (Exception e)
            {

            }

            return false;
        }
        [HttpPost]
        public string MaterialReceiptFormNoMax()
        {
            string formNoMax = "";

            try
            {
                MaterialReceiptMasterSP spMaterialReceipt = new MaterialReceiptMasterSP();
                formNoMax = spMaterialReceipt.MaterialReceiptMasterGetMax(11);
            }
            catch (Exception ex)
            {
            }

            return formNoMax.ToString();
        }
        public MatResponse SaveFunction(CreateMaterialReceiptVM input)
        {
            var orderMaster = new PurchaseOrderMasterSP().PurchaseOrderMasterView(input.OrderMasterId);

            //input.ReceiptNo = "22";   //change later
            MaterialReceiptDetailsInfo infoMaterialReceiptDetails = new MaterialReceiptDetailsInfo();
            ProductInfo infoProduct = new ProductInfo();
            MaterialReceiptMasterInfo infoMaterialReceiptMaster = new MaterialReceiptMasterInfo();
            StockPostingSP spstockposting = new StockPostingSP();
            MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();
            MaterialReceiptDetailsSP spMaterialReceiptDetails = new MaterialReceiptDetailsSP();
            ProductSP spproduct = new ProductSP();
            MatResponse response = new MatResponse();


            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            //AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
            //AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            //PurchaseBillTaxInfo infoPurchaseBillTax = new PurchaseBillTaxInfo();
            //PurchaseBillTaxSP spPurchaseBillTax = new PurchaseBillTaxSP();
            AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
            AccountLedgerSP spAccountLedger = new AccountLedgerSP();
            //UnitConvertionSP spUnitConvertion = new UnitConvertionSP();
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();


            string suffixResponse = string.Empty;
            //var orderNos = new PurchaseMasterSP().GetOrderNoCorrespondingtoLedger(20020, 0, decMaterialReceiptVoucherTypeId);
            // input.OrderNo = orderNos.Select().Where(p=>p. == input.OrderNo)
            try
            {
                bool checkExistence = spMaterialReceiptMaster.MaterialReceiptNumberCheckExistence(input.ReceiptNo, 11);
                if (checkExistence)
                {
                    input.ReceiptNo = VoucherNumberGeneration();
                    suffixResponse = "Your Form Number has been incremented to " + input.ReceiptNo + ".";
                }
                infoMaterialReceiptMaster.Date = Convert.ToDateTime(input.Date);
                infoMaterialReceiptMaster.LedgerId = Convert.ToDecimal(input.SupplierId);

                infoMaterialReceiptMaster.SuffixPrefixId = 0;
                infoMaterialReceiptMaster.VoucherNo = input.ReceiptNo;

                infoMaterialReceiptMaster.VoucherTypeId = decMaterialReceiptVoucherTypeId;
                infoMaterialReceiptMaster.InvoiceNo = input.ReceiptNo;
                infoMaterialReceiptMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                infoMaterialReceiptMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoMaterialReceiptMaster.TransportationCompany = "";
                infoMaterialReceiptMaster.LrNo = "";
                infoMaterialReceiptMaster.Narration = input.Narration;
                infoMaterialReceiptMaster.DoneBy = input.DoneBy;
                infoMaterialReceiptMaster.OrderMasterId = input.OrderMasterId;
                infoMaterialReceiptMaster.exchangeRateId = orderMaster.exchangeRateId == 0 ? 1: orderMaster.exchangeRateId;//saving corresponding exchangeRateId as currencyId
                infoMaterialReceiptMaster.TotalAmount = Convert.ToDecimal(input.TotalAmount);
                infoMaterialReceiptMaster.Extra1 = string.Empty;
                infoMaterialReceiptMaster.Extra2 = string.Empty;
                infoMaterialReceiptMaster.ExtraDate = Convert.ToDateTime(DateTime.Now);
                infoMaterialReceiptMaster.Date = Convert.ToDateTime(input.Date);

                infoMaterialReceiptMaster.LrNo = input.WayBill;

                foreach (var lineItem in input.LineItems)
                {
                    if (lineItem.ProductId <= 0)
                    {
                        response.ResponseCode = 402;
                        response.ResponseMessage = "Remove all line items and add them properly!";
                        return response;
                    } 
                }

                decMaterialReceiptMasterIdentity = Convert.ToDecimal(spMaterialReceiptMaster.MaterialReceiptMasterAdd(infoMaterialReceiptMaster));
                //int inRowcount = dgvProduct.Rows.Count;
                foreach (var lineItem in input.LineItems)
                {

                   
                    var unit = new UnitSP().UnitViewAllByProductId(lineItem.ProductId);
                    var unitconv = new UnitConvertionSP().UnitViewAllByProductId(lineItem.ProductId);
                    var batch = new BatchSP().BatchIdViewByProductId(lineItem.ProductId);
                    if (lineItem.VoucherNo == null)
                    {
                        lineItem.VoucherNo = "";
                    }
                    if (lineItem.InvoiceNo == null)
                    {
                        lineItem.InvoiceNo = "";
                    }
                    var details = new PurchaseOrderDetailsSP().PurchaseOrderDetailsView(lineItem.OrderDetailsId);
                    infoMaterialReceiptDetails.MaterialReceiptMasterId = decMaterialReceiptMasterIdentity;
                    if (lineItem.ProductId > 0)
                    {
                        infoMaterialReceiptDetails.ProductId = lineItem.ProductId;
                    }
                    //if (dgvProduct.Rows[inI].Cells["dgvtxtPurchaseOrderDetailsId"].Value != null)
                    //{
                    //    infoMaterialReceiptDetails.OrderDetailsId = Convert.ToDecimal(dgvProduct.Rows[inI].Cells["dgvtxtPurchaseOrderDetailsId"].Value.ToString());
                    //}
                    //else
                    infoMaterialReceiptDetails.OrderDetailsId = lineItem.OrderDetailsId;
                    if (lineItem.StoreId > 0)
                    {
                        infoMaterialReceiptDetails.GodownId = Convert.ToDecimal(lineItem.StoreId);
                    }
                    else
                    {
                        infoMaterialReceiptDetails.GodownId = context.tbl_Godown.FirstOrDefault().godownId;
                    }
                    if (lineItem.RackId > 0)
                    {
                        infoMaterialReceiptDetails.RackId = Convert.ToDecimal(lineItem.RackId);
                    }
                    else
                    {
                        var rackId = 0.0M;
                        var rack = context.tbl_Rack.FirstOrDefault();
                        if(rack != null)
                        {
                            rackId = rack.rackId;
                        }

                        infoMaterialReceiptDetails.RackId = rackId;
                    }
                    if (lineItem.BatchId > 0)
                    {
                        infoMaterialReceiptDetails.BatchId = new BatchSP().BatchIdViewByProductId(lineItem.ProductId);
                    }
                    else
                    {
                        //infoMaterialReceiptDetails.BatchId = 1;
                        infoMaterialReceiptDetails.BatchId = Convert.ToDecimal(batch.ToString());
                    }
                    if (lineItem.Quantity > 0)
                    {
                        infoMaterialReceiptDetails.Qty = Convert.ToDecimal(lineItem.Quantity);
                    }
                    if (lineItem.UnitId > 0)
                    {
                        infoMaterialReceiptDetails.UnitId = Convert.ToDecimal(lineItem.UnitId);
                        infoMaterialReceiptDetails.UnitConversionId = Convert.ToDecimal(lineItem.UnitConversionId);
                    }
                    else
                    {
                        if(unit.Rows.Count > 0)
                        {
                            infoMaterialReceiptDetails.UnitId = Convert.ToDecimal(unit.Rows[0].ItemArray[0]);
                            infoMaterialReceiptDetails.UnitConversionId = unitconv.UnitconvertionId;
                        }
                        else
                        {
                            infoMaterialReceiptDetails.UnitId = 0;
                            infoMaterialReceiptDetails.UnitConversionId = 0;
                        }
                    }
                    infoMaterialReceiptDetails.Rate = Convert.ToDecimal(lineItem.Rate);
                    infoMaterialReceiptDetails.Amount = Convert.ToDecimal(lineItem.Rate * lineItem.Quantity);
                    infoMaterialReceiptDetails.Slno = Convert.ToInt32(lineItem.SL);
                    infoMaterialReceiptDetails.Extra1 = string.Empty;
                    infoMaterialReceiptDetails.Exta2 = string.Empty;

                    infoMaterialReceiptDetails.ProjectId = 0;
                    infoMaterialReceiptDetails.CategoryId = 0;
                    if (lineItem.Description != "")
                    {
                        infoMaterialReceiptDetails.itemDescription = lineItem.Description;
                    }
                    infoMaterialReceiptDetails.ExtraDate = Convert.ToDateTime(DateTime.Now);
                    spMaterialReceiptDetails.MaterialReceiptDetailsAdd(infoMaterialReceiptDetails);
                    //-----------------Stockposting---------------------------//
                    StockPostingInfo infoStockPosting = new StockPostingInfo();
                    infoStockPosting.Date = infoMaterialReceiptMaster.Date;
                    infoStockPosting.ProductId = infoMaterialReceiptDetails.ProductId;
                    infoStockPosting.BatchId = infoMaterialReceiptDetails.BatchId;
                    infoStockPosting.UnitId = infoMaterialReceiptDetails.UnitId;
                    infoStockPosting.GodownId = infoMaterialReceiptDetails.GodownId;
                    infoStockPosting.RackId = infoMaterialReceiptDetails.RackId;
                    if (input.OrderNo != "")
                    {
                        if (lineItem.InvoiceNo != "")
                        {
                            infoStockPosting.InvoiceNo = input.ReceiptNo;
                            infoStockPosting.AgainstInvoiceNo = input.ReceiptNo;
                        }
                        else
                        {
                            infoStockPosting.InvoiceNo = input.ReceiptNo;
                            infoStockPosting.AgainstInvoiceNo = "NA";
                        }

                        if (lineItem.VoucherNo != "")
                        {
                            infoStockPosting.VoucherNo = input.ReceiptNo;
                            infoStockPosting.AgainstVoucherNo = strVoucherNo;
                        }
                        else
                        {
                            infoStockPosting.VoucherNo = strVoucherNo;
                            infoStockPosting.AgainstVoucherNo = "NA";
                        }

                        if (lineItem.VoucherTypeId != 0)
                        {
                            infoStockPosting.VoucherTypeId = lineItem.VoucherTypeId;
                            infoStockPosting.AgainstVoucherTypeId = decMaterialReceiptVoucherTypeId;
                        }
                        else
                        {
                            infoStockPosting.VoucherTypeId = decMaterialReceiptVoucherTypeId;
                            infoStockPosting.AgainstVoucherTypeId = 0;
                        }
                    }
                    else
                    {
                        infoStockPosting.InvoiceNo = input.ReceiptNo;
                        infoStockPosting.VoucherNo = strVoucherNo;
                        infoStockPosting.VoucherTypeId = decMaterialReceiptVoucherTypeId;
                        infoStockPosting.AgainstVoucherTypeId = 0;
                        infoStockPosting.AgainstVoucherNo = "NA";
                        infoStockPosting.AgainstInvoiceNo = "NA";
                    }
                    //infoStockPosting.InwardQty = Convert.ToDecimal(lineItem.Quantity) / Convert.ToDecimal(dgvProduct.Rows[inI].Cells["dgvtxtConversionRate"].Value.ToString());
                    infoStockPosting.InwardQty = Convert.ToDecimal(lineItem.Quantity) / 1;
                    infoStockPosting.OutwardQty = 0;
                    infoStockPosting.Rate = Convert.ToDecimal(lineItem.Rate);
                    infoStockPosting.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoStockPosting.Extra1 = string.Empty;
                    infoStockPosting.Extra2 = string.Empty;
                    var spStockPosting3 = new MatApi.Services.StockPostingSP();
                    spStockPosting3.StockPostingAdd(infoStockPosting); 

                }
                
                /*-----------------------------------------Ledger Posting----------------------------------------------------*/
                foreach (var i in input.LineItems)
                      {
                          //goods in transist 
                          var product = context.tbl_Product.Find(i.ProductId);
                          var ledger =
                              context.tbl_AccountLedger.Find(product.expenseAccount);
                          //infoLedgerPosting.Credit =  i.Quantity* Convert.ToDecimal(product.purchaseRate) * spExchangeRate.ExchangeRateViewByExchangeRateId(infoMaterialReceiptMaster.exchangeRateId);
                          infoLedgerPosting.Credit = Convert.ToDecimal(i.Rate * i.Quantity);
                          infoLedgerPosting.Debit = 0;
                          //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                          infoLedgerPosting.Date = infoMaterialReceiptMaster.Date;
                          //infoLedgerPosting.DetailsId = 0;
                          infoLedgerPosting.DetailsId = decMaterialReceiptMasterIdentity;
                          infoLedgerPosting.InvoiceNo = infoMaterialReceiptMaster.InvoiceNo;
                          infoLedgerPosting.LedgerId = LedgerConstants.GoodsInTransit; /*ledger.ledgerId;*/
                          infoLedgerPosting.VoucherNo = infoMaterialReceiptMaster.VoucherNo;
                          infoLedgerPosting.VoucherTypeId = infoMaterialReceiptMaster.VoucherTypeId;
                          infoLedgerPosting.YearId =MatApi.Models.PublicVariables.CurrentFinicialYearId;
                          infoLedgerPosting.ChequeDate = DateTime.Now;
                          infoLedgerPosting.ChequeNo = string.Empty;
                          infoLedgerPosting.Extra1 = string.Empty;
                          infoLedgerPosting.Extra2 = string.Empty;
                          infoLedgerPosting.ExtraDate = DateTime.Now;
                         // spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                      }
                      //get ledger for cost of sales  ; 
                      
               
                //decimal decBilldiscount = infoMaterialReceiptMaster.BillDiscount;
                //if (decBilldiscount > 0)
                //{
                //    infoLedgerPosting.Credit = decBilldiscount * spExchangeRate.ExchangeRateViewByExchangeRateId(infoMaterialReceiptMaster.exchangeRateId);
                //    infoLedgerPosting.Debit = 0;
                //    //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                //    infoLedgerPosting.Date = infoMaterialReceiptMaster.Date;
                //    infoLedgerPosting.DetailsId = 0;
                //    infoLedgerPosting.InvoiceNo = infoMaterialReceiptMaster.InvoiceNo;
                //    infoLedgerPosting.LedgerId = 9;//ledger id of discount received
                //    infoLedgerPosting.VoucherNo = infoMaterialReceiptMaster.VoucherNo;
                //    infoLedgerPosting.VoucherTypeId = infoMaterialReceiptMaster.VoucherTypeId;
                //    infoLedgerPosting.YearId = MatApi.Models.PublicVariables.CurrentFinicialYearId;
                //    infoLedgerPosting.ChequeDate = DateTime.Now;
                //    infoLedgerPosting.ChequeNo = string.Empty;
                //    infoLedgerPosting.Extra1 = string.Empty;
                //    infoLedgerPosting.Extra2 = string.Empty;
                //    infoLedgerPosting.ExtraDate = DateTime.Now;
                //    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                //}
              
              
               
                //foreach (var additionalCost in input.AdditionalCostInfo)
                //{
                //    /*-----------------------------------------Additional Cost Add----------------------------------------------------*/
                //    infoAdditionalCost.Credit = 0;
                //    infoAdditionalCost.Debit = additionalCost.Debit;
                //    infoAdditionalCost.LedgerId = additionalCost.LedgerId;
                //    infoAdditionalCost.VoucherNo = infoMaterialReceiptMaster.VoucherNo;
                //    infoAdditionalCost.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                //    infoAdditionalCost.Extra1 = string.Empty;
                //    infoAdditionalCost.Extra2 = string.Empty;
                //    infoAdditionalCost.ExtraDate = DateTime.Now;
                //    spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                //    /*-----------------------------------------Additional Cost Ledger Posting----------------------------------------------------*/
                //    infoLedgerPosting.Credit = 0;
                //    infoLedgerPosting.Debit = infoAdditionalCost.Debit * spExchangeRate.ExchangeRateViewByExchangeRateId(input.PurchaseMasterInfo.ExchangeRateId);
                //    //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                //    infoLedgerPosting.Date = Convert.ToDateTime(input.PurchaseMasterInfo.Date);
                //    infoLedgerPosting.DetailsId = 0;
                //    infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                //    infoLedgerPosting.LedgerId = infoAdditionalCost.LedgerId;
                //    infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                //    infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                //    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //    infoLedgerPosting.ChequeDate = DateTime.Now;
                //    infoLedgerPosting.ChequeNo = string.Empty;
                //    infoLedgerPosting.Extra1 = string.Empty;
                //    infoLedgerPosting.Extra2 = string.Empty;
                //    infoLedgerPosting.ExtraDate = DateTime.Now;
                //    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                //}
                //foreach (var tax in input.PurchaseBillTaxInfo)
                //{
                //    /*-----------------------------------------PurchaseBillTax Add----------------------------------------------------*/
                //    infoPurchaseBillTax.PurchaseMasterId = decPurchaseMasterId;
                //    infoPurchaseBillTax.TaxAmount = tax.TaxAmount;
                //    infoPurchaseBillTax.TaxId = tax.TaxId;
                //    infoPurchaseBillTax.Extra1 = string.Empty;
                //    infoPurchaseBillTax.Extra2 = string.Empty;
                //    infoPurchaseBillTax.ExtraDate = DateTime.Now;
                //    spPurchaseBillTax.PurchaseBillTaxAdd(infoPurchaseBillTax);
                //    /*-----------------------------------------Tax Ledger Posting----------------------------------------------------*/
                //    infoLedgerPosting.Credit = 0;
                //    infoLedgerPosting.Debit = infoPurchaseBillTax.TaxAmount * spExchangeRate.ExchangeRateViewByExchangeRateId(input.PurchaseMasterInfo.ExchangeRateId);
                //    //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                //    infoLedgerPosting.Date = input.PurchaseMasterInfo.Date;
                //    infoLedgerPosting.DetailsId = 0;
                //    infoLedgerPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                //    infoLedgerPosting.LedgerId = 59;
                //    infoLedgerPosting.VoucherNo = infoPurchaseMaster.VoucherNo;
                //    infoLedgerPosting.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                //    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //    infoLedgerPosting.ChequeDate = DateTime.Now;
                //    infoLedgerPosting.ChequeNo = string.Empty;
                //    infoLedgerPosting.Extra1 = string.Empty;
                //    infoLedgerPosting.Extra2 = string.Empty;
                //    infoLedgerPosting.ExtraDate = DateTime.Now;
                //    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                //}
                /*-----------------------------------------PartyBalance Posting----------------------------------------------------*/
                infoAccountLedger = spAccountLedger.AccountLedgerView( infoMaterialReceiptMaster.LedgerId);
                if (infoAccountLedger.BillByBill == true)
                {
                    infoPartyBalance.Credit = Convert.ToDecimal( infoMaterialReceiptMaster.TotalAmount);
                    infoPartyBalance.Debit = 0;
                    //if (input.PurchaseMasterInfo.CreditPeriod != string.Empty)
                    //{
                    //    infoPartyBalance.CreditPeriod = Convert.ToInt32(input.PurchaseMasterInfo.CreditPeriod);
                    //}
                    infoPartyBalance.Date = Convert.ToDateTime( infoMaterialReceiptMaster.Date);
                    infoPartyBalance.ExchangeRateId =  infoMaterialReceiptMaster.exchangeRateId;
                    infoPartyBalance.FinancialYearId = MatApi.Models.PublicVariables.CurrentFinicialYearId;
                    infoPartyBalance.LedgerId =  infoMaterialReceiptMaster.LedgerId;
                    infoPartyBalance.ReferenceType = "NEW";
                    infoPartyBalance.InvoiceNo =  infoMaterialReceiptMaster.InvoiceNo;
                    infoPartyBalance.VoucherNo =  infoMaterialReceiptMaster.VoucherNo;
                    infoPartyBalance.VoucherTypeId =  infoMaterialReceiptMaster.VoucherTypeId;
                    infoPartyBalance.AgainstInvoiceNo = "NA";
                    infoPartyBalance.AgainstVoucherNo = "NA";
                    infoPartyBalance.AgainstVoucherTypeId = 0;
                    infoPartyBalance.Extra1 = string.Empty;
                    infoPartyBalance.Extra2 = string.Empty;
                    infoPartyBalance.ExtraDate = DateTime.Now;
                    spPartyBalance.PartyBalanceAdd(infoPartyBalance);
                }
              
                response.ResponseCode = 200;
                response.ResponseMessage = "Material Receipt Confirmed Successfully." + suffixResponse;
                return response;
                //return true;
                //DBMatConnection conn = new DBMatConnection();
                //string queryStr = string.Format("UPDATE tbl_MaterialReceiptMaster_Pending " +
                //                                "SET approved='{0}'" +
                //                                "WHERE orderMasterId={1} ", "Approved", input.OrderMasterId);
                //if (conn.customUpdateQuery(queryStr) > 0)
                //{
                //    return true;
                //}
                //return false;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "MR36:" + ex.Message;

            }
            //return false;
            response.ResponseCode = 402;
            response.ResponseMessage = "Something went wrong!";
            return response;
        }  

        [HttpPost]
        public MatResponse EditFunction(CreateMaterialReceiptVM input)
            {
            var orderMaster = new PurchaseOrderMasterSP().PurchaseOrderMasterView(input.OrderMasterId);
            
            //input.ReceiptNo = "22";   //change later
            MaterialReceiptDetailsInfo infoMaterialReceiptDetails = new MaterialReceiptDetailsInfo();
            ProductInfo infoProduct = new ProductInfo();
            MaterialReceiptMasterInfo infoMaterialReceiptMaster = new MaterialReceiptMasterInfo();
            StockPostingSP spstockposting = new StockPostingSP();
            MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();
            MaterialReceiptDetailsSP spMaterialReceiptDetails = new MaterialReceiptDetailsSP();
            ProductSP spproduct = new ProductSP();
            MatResponse response = new MatResponse();
            string suffixResponse = string.Empty;
            //get MaterialMaster 
            try
            {

              var masterMaterialReceipt = context.tbl_MaterialReceiptMaster
                .Where(a => a.materialReceiptMasterId == input.MaterialMasterId).FirstOrDefault();
            if (masterMaterialReceipt != null)
            {
                //edit it 
                masterMaterialReceipt.date = Convert.ToDateTime(input.Date);

                masterMaterialReceipt.ledgerId = Convert.ToDecimal(input.SupplierId);

                masterMaterialReceipt.suffixPrefixId = 0;
                masterMaterialReceipt.voucherNo = input.ReceiptNo;

                masterMaterialReceipt.voucherTypeId = decMaterialReceiptVoucherTypeId;
                masterMaterialReceipt.invoiceNo = input.ReceiptNo;
                masterMaterialReceipt.userId = MATFinancials.PublicVariables._decCurrentUserId;
                    masterMaterialReceipt.lrNo = input.WayBill;
                masterMaterialReceipt.narration = input.Narration;
                masterMaterialReceipt.orderMasterId = input.OrderMasterId;
               //saving corresponding exchangeRateId as currencyId
               masterMaterialReceipt.totalAmount = Convert.ToDecimal(input.TotalAmount);
               //save changes 
              context.Entry(masterMaterialReceipt).State = EntityState.Modified;
              context.SaveChanges();

              //get all current line items from db 
              var lineIds = context.tbl_MaterialReceiptDetails
                  .Where(a => a.materialReceiptMasterId == input.MaterialMasterId).Select(a=>a.materialReceiptDetailsId).ToList();
              //iterate over dbline to get items that are not passed from controller and delete them 
              var detailIds = new List<decimal>();
              foreach (var i in input.LineItems)
              {
                        detailIds.Add(i.MaterialDetailId);
              }

              var deletedIds = lineIds.Except(detailIds);
              foreach (var i in deletedIds)
              {
                  var find = context.tbl_MaterialReceiptDetails.Find(i);
                  if (find != null)
                  {
                      context.tbl_MaterialReceiptDetails.Remove(find);
                      context.SaveChanges();
                  }
              }

                    //Delete the previous stocks
                    var stockPosting = context.tbl_StockPosting.Where(a =>
                         a.voucherTypeId == Constants.MaterialReceiptVoucherTypeId && a.voucherNo == masterMaterialReceipt.voucherNo).ToList();
                    // delete stock posting 
                    if (stockPosting.Any())
                    {
                        context.tbl_StockPosting.RemoveRange(stockPosting);
                    }

                    foreach (var i in input.LineItems)
              {


                        #region Process Parameter

                        var unit = new UnitSP().UnitViewAllByProductId(i.ProductId);
                        var unitconv = new UnitConvertionSP().UnitViewAllByProductId(i.ProductId);
                        var batch = new BatchSP().BatchIdViewByProductId(i.ProductId);
                        
                        if (i.StoreId > 0)
                        {
                            i.StoreId = Convert.ToDecimal(i.StoreId);
                        }
                        else
                        {
                            i.StoreId = context.tbl_Godown.FirstOrDefault().godownId;
                        }
                        if (i.RackId > 0)
                        {
                            i.RackId = Convert.ToDecimal(i.RackId);
                        }
                        else
                        {
                            var rackId = 0.0M;
                            var rack = context.tbl_Rack.FirstOrDefault();
                            if (rack != null)
                            {
                                rackId = rack.rackId;
                            }

                            i.RackId = rackId;
                        }
                        if (i.BatchId > 0)
                        {
                            i.BatchId = new BatchSP().BatchIdViewByProductId(i.ProductId);
                        }
                        else
                        {
                            i.BatchId = Convert.ToDecimal(batch.ToString());
                        }
                       
                        if (i.UnitId > 0)
                        {
                            i.UnitId = Convert.ToDecimal(i.UnitId);
                            i.UnitConversionId = Convert.ToDecimal(i.UnitConversionId);
                        }
                        else
                        {

                            i.UnitId = Convert.ToDecimal(unit.Rows[0].ItemArray[0]);
                            i.UnitConversionId = unitconv.UnitconvertionId;
                        }

                        #endregion


                        // check if line item exist then edit else create a new one 
                        var d = context.tbl_MaterialReceiptDetails.Where(a =>
                          a.materialReceiptMasterId == input.MaterialMasterId && a.materialReceiptDetailsId == i.MaterialDetailId).FirstOrDefault();
                  if (d != null)
                  {
                      //update
                        if (i.ProductId > 0)
                    {
                        d.productId = i.ProductId;
                    }
                    //if (dgvProduct.Rows[inI].Cells["dgvtxtPurchaseOrderDetailsId"].Value != null)
                    //{
                    //    infoMaterialReceiptDetails.OrderDetailsId = Convert.ToDecimal(dgvProduct.Rows[inI].Cells["dgvtxtPurchaseOrderDetailsId"].Value.ToString());
                    //}
                    //else
                    d.orderDetailsId = i.OrderDetailsId;
                    if (i.StoreId > 0)
                    {
                        d.godownId = Convert.ToDecimal(i.StoreId);
                    }
                    else
                    {
                        d.godownId = 1;
                    }
                    if (i.RackId > 0)
                    {
                        d.rackId = Convert.ToDecimal(i.RackId);
                    }
                    else
                    {
                        infoMaterialReceiptDetails.RackId = 1;
                    }
                    if (i.BatchId > 0)
                    {
                        d.batchId = new BatchSP().BatchIdViewByProductId(i.ProductId);
                    }
                    else
                    {
                        d.batchId = 1;
                    }
                    if (i.Quantity > 0)
                    {
                        d.qty = Convert.ToDecimal(i.Quantity);
                    }
                    if (i.UnitId > 0)
                    {
                        d.unitId = Convert.ToDecimal(i.UnitId);
                        d.unitConversionId = Convert.ToDecimal(i.UnitConversionId);
                    }
                    d.rate = Convert.ToDecimal(i.Rate);
                    d.amount = Convert.ToDecimal(i.Rate * i.Quantity);
                    d.slno = Convert.ToInt32(i.SL);
                    d.extra1 = string.Empty;
                    d.exta2 = string.Empty;

                    d.ProjectId = 0;
                    d.CategoryId = 0;
                    if (i.Description != "")
                    {
                        d.itemDescription = i.Description;
                    }
                    d.extraDate = Convert.ToDateTime(DateTime.Now);
                    context.Entry(d).State = EntityState.Modified;
                    context.SaveChanges();
                  }
                  else
                  {
                          if (i.VoucherNo == null)
                    {
                        i.VoucherNo = "";
                    }
                    if (i.InvoiceNo == null)
                    {
                        i.InvoiceNo = "";
                    }
                  //  var details = new PurchaseOrderDetailsSP().PurchaseOrderDetailsView(lineItem.OrderDetailsId);
                    infoMaterialReceiptDetails.MaterialReceiptMasterId = input.MaterialMasterId;
                    if (i.ProductId > 0)
                    {
                        infoMaterialReceiptDetails.ProductId = i.ProductId;
                    }
                    //if (dgvProduct.Rows[inI].Cells["dgvtxtPurchaseOrderDetailsId"].Value != null)
                    //{
                    //    infoMaterialReceiptDetails.OrderDetailsId = Convert.ToDecimal(dgvProduct.Rows[inI].Cells["dgvtxtPurchaseOrderDetailsId"].Value.ToString());
                    //}
                    //else
                    infoMaterialReceiptDetails.OrderDetailsId = i.OrderDetailsId;
                    if (i.StoreId > 0)
                    {
                        infoMaterialReceiptDetails.GodownId = Convert.ToDecimal(i.StoreId);
                    }
                    else
                    {
                        infoMaterialReceiptDetails.GodownId = 1;
                    }
                    if (i.RackId > 0)
                    {
                        infoMaterialReceiptDetails.RackId = Convert.ToDecimal(i.RackId);
                    }
                    else
                    {
                        infoMaterialReceiptDetails.RackId = 1;
                    }
                    if (i.BatchId > 0)
                    {
                        infoMaterialReceiptDetails.BatchId = new BatchSP().BatchIdViewByProductId(i.ProductId);
                    }
                    else
                    {
                        infoMaterialReceiptDetails.BatchId = 1;
                    }
                    if (i.Quantity > 0)
                    {
                        infoMaterialReceiptDetails.Qty = Convert.ToDecimal(i.Quantity);
                    }
                    if (i.UnitId > 0)
                    {
                        infoMaterialReceiptDetails.UnitId = Convert.ToDecimal(i.UnitId);
                        infoMaterialReceiptDetails.UnitConversionId = Convert.ToDecimal(i.UnitConversionId);
                    }
                    infoMaterialReceiptDetails.Rate = Convert.ToDecimal(i.Rate);
                    infoMaterialReceiptDetails.Amount = Convert.ToDecimal(i.Rate * i.Quantity);
                    infoMaterialReceiptDetails.Slno = Convert.ToInt32(i.SL);
                    infoMaterialReceiptDetails.Extra1 = string.Empty;
                    infoMaterialReceiptDetails.Exta2 = string.Empty;

                    infoMaterialReceiptDetails.ProjectId = 0;
                    infoMaterialReceiptDetails.CategoryId = 0;
                    if (i.Description != "")
                    {
                        infoMaterialReceiptDetails.itemDescription = i.Description;
                    }
                    infoMaterialReceiptDetails.ExtraDate = Convert.ToDateTime(DateTime.Now);
                    spMaterialReceiptDetails.MaterialReceiptDetailsAdd(infoMaterialReceiptDetails);
                  }  


                      //-----------------Stockposting---------------------------//
                    StockPostingInfo infoStockPosting = new StockPostingInfo();
                    infoStockPosting.Date = (DateTime)masterMaterialReceipt.date;

                    if (i.StoreId > 0)
                    {
                        infoStockPosting.GodownId = Convert.ToDecimal(i.StoreId);
                    }
                    else
                    {
                        infoStockPosting.GodownId = 1;
                    }

                    if (i.RackId > 0)
                    {
                        infoStockPosting.RackId = Convert.ToDecimal(i.RackId);
                    }
                    else
                    {
                        infoStockPosting.RackId = 1;
                    }

                    if (i.BatchId > 0)
                    {
                        infoStockPosting.BatchId = new BatchSP().BatchIdViewByProductId(i.ProductId);
                    }
                    else
                    {
                        infoStockPosting.BatchId = 1;
                    }


                    infoStockPosting.ProductId = i.ProductId;
                    infoStockPosting.UnitId = i.UnitId;

                    if (input.OrderNo != "")
                    {
                        if (i.InvoiceNo != "")
                        {
                            infoStockPosting.InvoiceNo = input.ReceiptNo;
                            infoStockPosting.AgainstInvoiceNo = input.ReceiptNo;
                        }
                        else
                        {
                            infoStockPosting.InvoiceNo = input.ReceiptNo;
                            infoStockPosting.AgainstInvoiceNo = "NA";
                        }

                        if (i.VoucherNo != "")
                        {
                            infoStockPosting.VoucherNo = input.ReceiptNo;
                            infoStockPosting.AgainstVoucherNo = strVoucherNo;
                        }
                        else
                        {
                            infoStockPosting.VoucherNo = strVoucherNo;
                            infoStockPosting.AgainstVoucherNo = "NA";
                        }

                        if (i.VoucherTypeId != 0)
                        {
                            infoStockPosting.VoucherTypeId = i.VoucherTypeId;
                            infoStockPosting.AgainstVoucherTypeId = decMaterialReceiptVoucherTypeId;
                        }
                        else
                        {
                            infoStockPosting.VoucherTypeId = decMaterialReceiptVoucherTypeId;
                            infoStockPosting.AgainstVoucherTypeId = 0;
                        }
                    }
                    else
                    {
                        infoStockPosting.InvoiceNo = input.ReceiptNo;
                        infoStockPosting.VoucherNo = input.ReceiptNo;
                        infoStockPosting.VoucherTypeId = decMaterialReceiptVoucherTypeId;
                        infoStockPosting.AgainstVoucherTypeId = 0;
                        infoStockPosting.AgainstVoucherNo = "NA";
                        infoStockPosting.AgainstInvoiceNo = "NA";
                    }
                    //infoStockPosting.InwardQty = Convert.ToDecimal(lineItem.Quantity) / Convert.ToDecimal(dgvProduct.Rows[inI].Cells["dgvtxtConversionRate"].Value.ToString());
                    infoStockPosting.InwardQty = Convert.ToDecimal(i.Quantity) / 1;
                    infoStockPosting.OutwardQty = 0;
                    infoStockPosting.Rate = Convert.ToDecimal(i.Rate);
                    infoStockPosting.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoStockPosting.Extra1 = string.Empty;
                    infoStockPosting.Extra2 = string.Empty;
                        var spStockPosting1 = new MatApi.Services.StockPostingSP();
                        spStockPosting1.StockPostingAdd(infoStockPosting);
              }

            //Update Goods in Transit Account
            //First Delete the LedgerPosting and Re-Add the LedgerPosting

            var GiTs = context.tbl_LedgerPosting.Where(a => a.ledgerId == LedgerConstants.GoodsInTransit && a.voucherTypeId == decMaterialReceiptVoucherTypeId && a.voucherNo == input.ReceiptNo && a.detailsId == input.MaterialMasterId);

            if(GiTs != null)
            {
                //Delete the LedgerPosting
                context.tbl_LedgerPosting.RemoveRange(GiTs);
                context.SaveChanges();

                //Re-Add the LedgerPosting
                        
                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();

                foreach (var i in input.LineItems)
                {
                    //goods in transist 
                    infoLedgerPosting.Credit = Convert.ToDecimal(i.Rate * i.Quantity);
                    infoLedgerPosting.Debit = 0;
                    infoLedgerPosting.Date = Convert.ToDateTime(input.Date);
                    infoLedgerPosting.DetailsId = input.MaterialMasterId;
                    infoLedgerPosting.InvoiceNo = input.ReceiptNo;
                    infoLedgerPosting.LedgerId = LedgerConstants.GoodsInTransit; 
                    infoLedgerPosting.VoucherNo = input.ReceiptNo;
                    infoLedgerPosting.VoucherTypeId = decMaterialReceiptVoucherTypeId;
                    infoLedgerPosting.YearId = MatApi.Models.PublicVariables.CurrentFinicialYearId;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }

            }

                    response.ResponseCode = 200;
                    response.ResponseMessage = "Material Receipt Edited Successfully." + suffixResponse;
                    return response;

                }
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }

            response.ResponseCode = 402;
            response.ResponseMessage = "Something went wrong!";
            return response;
        }

          [HttpGet]
        public IHttpActionResult Delete(int id)
          {
              var masterReciept = context.tbl_MaterialReceiptMaster.Find(id);
              var detailRecipt = context.tbl_MaterialReceiptDetails.Where(a => a.materialReceiptMasterId == id)
                  .ToList();
              var GiTs = context.tbl_LedgerPosting.Where(a => a.ledgerId == LedgerConstants.GoodsInTransit && a.voucherTypeId == masterReciept.voucherTypeId && a.voucherNo == masterReciept.voucherNo).ToList();

              var StockMaterialReceipt = context.tbl_StockPosting.Where(a => a.voucherTypeId == masterReciept.voucherTypeId && a.voucherNo == masterReciept.voucherNo).ToList();


            if (masterReciept != null)
              {
                
                  foreach (var i in detailRecipt)
                  {
                    //-----------------Stockposting---------------------------//
                    /*StockPostingSP spstockposting = new StockPostingSP();
                    StockPostingInfo infoStockPosting = new StockPostingInfo();
                    infoStockPosting.Date = Convert.ToDateTime(masterReciept.date);
                    infoStockPosting.ProductId = Convert.ToDecimal(i.productId);
                    infoStockPosting.BatchId = Convert.ToDecimal(i.batchId);
                    infoStockPosting.UnitId = Convert.ToDecimal(i.unitId);
                    infoStockPosting.GodownId = Convert.ToDecimal(i.godownId);
                    infoStockPosting.RackId =Convert.ToDecimal( i.rackId);


                    infoStockPosting.InvoiceNo = masterReciept.invoiceNo;
                    infoStockPosting.AgainstInvoiceNo = masterReciept.invoiceNo;;
                    infoStockPosting.VoucherNo = masterReciept.voucherNo;
                    infoStockPosting.AgainstVoucherNo = masterReciept.voucherNo;

                    infoStockPosting.VoucherTypeId = Convert.ToDecimal(masterReciept.voucherTypeId);
                    infoStockPosting.AgainstVoucherTypeId = decMaterialReceiptVoucherTypeId;
                  
                    //infoStockPosting.InwardQty = Convert.ToDecimal(lineItem.Quantity) / Convert.ToDecimal(dgvProduct.Rows[inI].Cells["dgvtxtConversionRate"].Value.ToString());
                    infoStockPosting.OutwardQty= Convert.ToDecimal(i.qty) / 1;
                    infoStockPosting.InwardQty = 0;
                    infoStockPosting.Rate = Convert.ToDecimal(i.rate);
                    infoStockPosting.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoStockPosting.Extra1 = string.Empty;
                    infoStockPosting.Extra2 = string.Empty;
                    spstockposting.StockPostingAdd(infoStockPosting); */

                    //del details 
                    context.tbl_MaterialReceiptDetails.Remove(i);
                    context.SaveChanges();
                  }

                  context.tbl_MaterialReceiptMaster.Remove(masterReciept);
                  context.SaveChanges();

                  context.tbl_StockPosting.RemoveRange(StockMaterialReceipt);
                  context.SaveChanges();

                  context.tbl_LedgerPosting.RemoveRange(GiTs);
                  context.SaveChanges();

                return Ok(true);
              }
              return Ok(false);
          }   
    }
}