﻿using MatApi.Models;
using MatApi.Models.Register;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Windows.Media.Animation;
using MatApi.DBModel;
using MatApi.Dto;
using MatApi.Enums;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using PublicVariables = MATFinancials.PublicVariables;
using EntityState = System.Data.Entity.EntityState;

namespace MatApi.Controllers.Supplier
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PurchaseReturnController : ApiController
    {
        string strCashorParty = string.Empty;
        string strPurchaseAccount = string.Empty;
        string strProductCode = string.Empty;
        string strVoucherNo = string.Empty;
        string strTaxComboFill = string.Empty;
        string strInvoiceNo = string.Empty;
        string strReturnNo = string.Empty;
        bool isFromPurchaseAccountCombo = false;
        bool isFromCashOrPartyCombo = false;
        bool isFromPurchaseReturn = false;
        bool isDontExecuteCashorParty = false;
        bool isDontExecuteVoucherType = false;
        bool isAmountcalc = true;
        bool isAutomatic = false;
        bool isValueChanged = false;
        bool isInvoiceFil = false;
        bool isEditFill = false;
        decimal decPurchaseReturnVoucherTypeId = 14;
        decimal decPurchaseReturnSuffixPrefixId = 0;
        decimal decPurchaseReturnMasterId = 0;
        decimal decPurchaseReturnTypeId = 0;
        decimal decQty = 0;
        decimal decRate = 0;
        decimal decBatchId = 0;
        decimal decMasterId = -2;
        decimal decAgainstVoucherTypeId = 0;
        int inMaxCount = 0;
        int inNarrationCount = 0;
        PurchaseMasterInfo infoPurchaseMaster = new PurchaseMasterInfo();
        PurchaseReturnMasterInfo infoPurchaseReturnMaster = new PurchaseReturnMasterInfo(); 

        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public PurchaseReturnController()
        {

        }
        [HttpGet]
        public HttpResponseMessage InvoiceLookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var products = new ProductSP().ProductViewAll();
            var customers = transactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill();
            var salesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();
            var pricingLevels = transactionGeneralFillObj.PricingLevelViewAll();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            var applyOn = new PurchaseDetailsSP().VoucherTypeComboFillForPurchaseInvoice();
            var units = new UnitSP().UnitViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks = new RackSP().RackViewAll();
            var batches = new BatchSP().BatchViewAll();
            var tax = new TaxSP().TaxViewAll();

            dynamic response = new ExpandoObject();
            response.Products = products;
            response.Customers = customers;
            response.SalesMen = salesMen;
            response.PricingLevels = pricingLevels;
            response.Currencies = currencies;
            response.ApplyOn = applyOn;
            response.Units = units;
            response.Stores = stores;
            response.Racks = racks;
            response.Batches = batches;
            response.Tax = tax;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public MatResponse GetVoucherTypes()
        {
            var response = new MatResponse();

            try
            {
                VoucherTypeSP spVoucherType = new VoucherTypeSP();
                DataTable dtbl = new DataTable();
                dtbl = spVoucherType.VoucherTypeViewAll();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch(Exception e)
            {
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }

            return response;
        }

        [HttpGet]
        public HttpResponseMessage InvoiceNoComboFill(decimal decLedger, decimal decvoucherTypeId)
        {
            //DataTable dtblInvoiceNo = new DataTable();
            //PurchaseMasterSP SPPurchaseMaster = new PurchaseMasterSP();

            //dtblInvoiceNo = SPPurchaseMaster.GetInvoiceNoCorrespondingtoLedger(decLedger, 0, decvoucherTypeId); 
            var invoice = context.tbl_PurchaseMaster
                .Where(a => a.ledgerId == decLedger && a.voucherTypeId == decvoucherTypeId).ToList()
                .Select(a => new
                {
                    purchaseMasterId = a.purchaseMasterId,
                    invoiceNo = a.invoiceNo
                });
            return Request.CreateResponse(HttpStatusCode.OK, invoice);
        }

        [HttpGet]
        public IHttpActionResult GetPurchaseDetails(decimal purchaseMasterId)
        {
            //var purchaseDetails = new PurchaseDetailsSP().PurchaseDetailsViewByPurchaseMasterIdWithRemaining(purchaseMasterId, decPurchaseReturnMasterId, 29);

            var details = context.tbl_PurchaseDetails
                .Include(a=>a.tbl_PurchaseMaster)
                .Include(a=>a.tbl_Product)
                .Where(a => a.purchaseMasterId == purchaseMasterId).ToList();
            var obj = details.Select(a => new
            {
               purchaseMasterId = a.purchaseMasterId,
                receiptDetailsId = a.receiptDetailsId,
                orderDetailsId = a.orderDetailsId,
                productId = a.productId,
                productName = a.tbl_Product.productName,
                unitId = a.unitId,
                unitConversionId = a.unitConversionId,
                currencyId = 0,
                discount = a.discount,
                taxId = a.taxId,
                extraDate = a.extraDate,
                extra1 = a.extra1,
                extra2 = a.extra2,
                qty = a.qty,
                grossAmount =a.grossAmount,
                rate = a.rate,
                barcode = a.tbl_Product.productCode,
                amount = a.amount,
                godownId = a.godownId,
                rackId = a.rackId,
                batchId= a.batchId,
                netAmount = a.netAmount,
                ProjectId = a.ProjectId,
                CategoryId = a.CategoryId,
                voucherTypeId = a.tbl_PurchaseMaster.voucherTypeId,
                voucherNo = a.tbl_PurchaseMaster.voucherNo,
                invoiceNo = a.tbl_PurchaseMaster.invoiceNo,
                productCode = a.tbl_Product.productCode,
                conversionRate = 1,
                noOfDecimalPlaces = 2
            });

            return Ok(obj);
        }

     
        public DataTable GetInvoiceNumbers(decimal supplierId,decimal voucherTypeId)
        {
            decimal decLedgerId = 0;
            decimal decVoucherId = 0;
            try
            {
                DataTable dtbl = new DataTable();
                decLedgerId = (supplierId== 0 ) ? -1 : supplierId;
                decVoucherId = (voucherTypeId == 0) ? -1 : voucherTypeId;
                dtbl = new PurchaseReturnMasterSP().GetInvoiceNoCorrespondingtoLedgerForPurchaseReturnReport(decLedgerId, decVoucherId);
                if (dtbl != null)
                {
                    DataRow dr = dtbl.NewRow();
                    dr["purchaseMasterId"] = 0;
                    dr["invoiceNo"] = "All";
                    dtbl.Rows.InsertAt(dr, 0);                    
                }
                return dtbl;
            }
            catch (Exception ex)
            {
            }
            return null;
        }

        [HttpPost]
        public HttpResponseMessage Registers(PurchaseReturnRegisterVM input)
        {
            var resp =new PurchaseReturnMasterSP().PurchaseReturnRegisterFill(input.FromDate, input.ToDate, input.LedgerId, input.InvoiceNo, input.AgainstInvoiceNo, input.VoucherType, input.DoneBy, input.VoucherNo);
            return Request.CreateResponse(HttpStatusCode.OK,(object)resp);
        }

        [HttpPost]
        public string Save(CreatePurchaseReturnVM input)
        {
            PurchaseMasterSP SPPurchaseMaster = new PurchaseMasterSP();
            PurchaseReturnMasterSP SPPurchaseReturnMaster = new PurchaseReturnMasterSP();
            PurchaseReturnDetailsSP SPPurchaseReturnDetails = new PurchaseReturnDetailsSP();
            PurchaseReturnDetailsInfo infoPurchaseReturnDetails = new PurchaseReturnDetailsInfo();
            StockPostingInfo infoStockPosting = new StockPostingInfo();
            var spStockPosting = new MatApi.Services.StockPostingSP();
            UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
            AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
            AccountLedgerSP spAccountLedger = new AccountLedgerSP();
            PurchaseReturnBilltaxInfo infoPurchaseReturnBillTax = new PurchaseReturnBilltaxInfo();
            PurchaseReturnBilltaxSP spPurchaseReturnBillTax = new PurchaseReturnBilltaxSP();
            SettingsSP spSettings = new SettingsSP();
            UnitSP spUnit = new UnitSP();
            DataTable dtblPurchaseMasterViewById = new DataTable();
            string strAgainstVoucherNo = string.Empty;
            string strAgainstInvoiceNo = string.Empty;
            decimal decPurchaseReturnMasterIds = 0;
            decimal decPurchaseMasterId = 0;
            decimal decDiscount = 0;
            decimal decExchangeRate = 0;
            decimal decDis = 0;

            try
            {
                var returnNo = input.LineItems.FirstOrDefault().InvoiceNo;
                    infoPurchaseReturnMaster.VoucherNo = input.ReturnNo ;
                    infoPurchaseReturnMaster.InvoiceNo = input.ReturnNo ;
                if (decPurchaseReturnVoucherTypeId != 0)
                {
                    infoPurchaseReturnMaster.VoucherTypeId = decPurchaseReturnVoucherTypeId;
                }
                infoPurchaseReturnMaster.SuffixPrefixId = (decPurchaseReturnSuffixPrefixId != 0) ? decPurchaseReturnSuffixPrefixId : 0;
                infoPurchaseReturnMaster.LedgerId = input.SupplierId;
                //infoPurchaseReturnMaster.PurchaseAccount = Convert.ToDecimal(cmbPurchaseAccount.SelectedValue.ToString());    // -- old implementation selects purchase account from dropdown -- //
                //if (decPurchaseReturnVoucherTypeId != 0)
                    //infoPurchaseReturnMaster.PurchaseAccount = spLedgerPosting.ProductExpenseAccountId();      // -- new implementation uses the product default/selected purchase account -- //
                if (input.SupplierId != 0 )
                {
                    infoPurchaseReturnMaster.PurchaseMasterId = input.PurchaseMasterId;
                    decPurchaseMasterId = input.PurchaseMasterId;
                }
                else
                {
                    infoPurchaseReturnMaster.PurchaseMasterId = 0;
                }

                var grandTotal = 0.0m;
                var grossTotal = 0.0m;
                var taxTotal = 0.0m;
                var discountTotal = 0.0m;
                var netTotal = 0.0m;
                var totalAmount = 0.0m;
                foreach (var i in input.LineItems)
                {
                    taxTotal += i.taxAmount;
                    discountTotal += i.Discount;
                    netTotal += i.NetAmount;
                    grossTotal += i.GrossAmount;
                    
                }

                infoPurchaseReturnMaster.ExchangeRateId = input.CurrencyId;
                infoPurchaseReturnMaster.Narration = input.Narration;
                infoPurchaseReturnMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                infoPurchaseReturnMaster.LrNo = input.LrNo;
                infoPurchaseReturnMaster.TransportationCompany = input.TransportationCompany;
                infoPurchaseReturnMaster.Date = input.Date;
                infoPurchaseReturnMaster.TotalAmount = netTotal;
                infoPurchaseReturnMaster.TotalTax = taxTotal;
                infoPurchaseReturnMaster.Discount = discountTotal;
                infoPurchaseReturnMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoPurchaseReturnMaster.Extra1 = string.Empty;
                infoPurchaseReturnMaster.Extra2 = string.Empty;
                infoPurchaseReturnMaster.ExtraDate = DateTime.Now;
                infoPurchaseReturnMaster.GrandTotal = grossTotal;
                infoPurchaseReturnMaster.DoneBy = input.DoneBy;

                decPurchaseReturnMasterIds = SPPurchaseReturnMaster.PurchaseReturnMasterAddWithReturnIdentity(infoPurchaseReturnMaster);

                infoLedgerPosting.Date = infoPurchaseReturnMaster.Date;
                infoLedgerPosting.VoucherTypeId = infoPurchaseReturnMaster.VoucherTypeId;
                infoLedgerPosting.VoucherNo = infoPurchaseReturnMaster.VoucherNo;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.InvoiceNo = infoPurchaseReturnMaster.InvoiceNo;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                infoLedgerPosting.DetailsId = decPurchaseReturnMasterIds;
                //infoLedgerPosting.LedgerId = infoPurchaseReturnMaster.PurchaseAccount;
                //infoLedgerPosting.Debit = 0;

                //infoLedgerPosting.Credit = input.NetAmount * spExchangeRate.ExchangeRateViewByExchangeRateId(input.CurrencyId)  ;
                infoLedgerPosting.ExtraDate = DateTime.Now;

                //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                //accountpayable ledger posting 

                infoLedgerPosting.LedgerId = infoPurchaseReturnMaster.LedgerId;
                infoLedgerPosting.Debit = input.GrandTotal * spExchangeRate.ExchangeRateViewByExchangeRateId(input.CurrencyId);
                infoLedgerPosting.ExtraDate = DateTime.Now;
                infoLedgerPosting.Credit = 0;
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                //foreach (var prt in input.LineItems)
                //{
                //    if (prt.TaxId != 0)
                //    {
                //        infoLedgerPosting.LedgerId = input.SupplierId;
                //        infoLedgerPosting.Credit = prt.Amount;
                //        infoLedgerPosting.Debit = 0;
                //        infoLedgerPosting.ExtraDate = DateTime.Now;
                //        spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                //    }
                //}

                if (input.DiscountAmount != 0)
                {
                    decDis = input.DiscountAmount;
                }
                if (decDis >= 0)
                {
                    infoLedgerPosting.Debit = 0;
                    infoLedgerPosting.Credit = decDis;
                    infoLedgerPosting.LedgerId = 9;
                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
                
                foreach (var pr in input.LineItems)
                {
                    if (pr.ProductId != 0)
                    {
                        infoPurchaseReturnDetails.ExtraDate = DateTime.Now;
                        infoPurchaseReturnDetails.Extra1 = string.Empty;
                        infoPurchaseReturnDetails.Extra2 = string.Empty;
                        infoPurchaseReturnDetails.PurchaseReturnMasterId =  decPurchaseReturnMasterIds;
                        infoPurchaseReturnDetails.ProductId = pr.ProductId;
                        infoPurchaseReturnDetails.Qty = pr.Quantity;
                        infoPurchaseReturnDetails.Rate = pr.Rate;
                        infoPurchaseReturnDetails.UnitId = pr.UnitId;
                        infoPurchaseReturnDetails.UnitConversionId = SPUnitConversion.UnitconversionIdViewByUnitIdAndProductId(infoPurchaseReturnDetails.UnitId, infoPurchaseReturnDetails.ProductId);
                        }
                        infoPurchaseReturnDetails.Discount = pr.Discount;
                        if (pr.TaxId != 0)
                        {
                            infoPurchaseReturnDetails.TaxId = pr.TaxId;
                            if (strTaxComboFill != string.Empty)
                            {
                            infoPurchaseReturnDetails.TaxAmount = pr.taxAmount;
                            }
                        }
                        else
                        {
                            infoPurchaseReturnDetails.TaxId = 0;
                        }
                        if (pr.BatchId != 0)
                        {
                            infoPurchaseReturnDetails.BatchId = pr.BatchId;
                        }
                        else
                        {
                            infoPurchaseReturnDetails.GodownId = 0;
                        }
                        if (pr.GodownId != 0)
                        {
                            infoPurchaseReturnDetails.GodownId = pr.GodownId;
                        }
                        else
                        {
                            infoPurchaseReturnDetails.RackId = 0;
                        }
                        if (pr.RackId != 0)
                        {
                            infoPurchaseReturnDetails.RackId = pr.RackId;
                        }
                        infoPurchaseReturnDetails.GrossAmount = pr.GrossAmount;
                        infoPurchaseReturnDetails.NetAmount = pr.NetAmount;
                        infoPurchaseReturnDetails.Amount = pr.Amount;
                        infoPurchaseReturnDetails.SlNo = pr.SL;
                        if (pr.Projectid != 0)
                        {
                            infoPurchaseReturnDetails.ProjectId = pr.Projectid;
                        }
                        else
                        {
                            infoPurchaseReturnDetails.ProjectId = 0;
                        }
                        if (pr.CategoryId != 0)
                        {
                            infoPurchaseReturnDetails.CategoryId = pr.CategoryId;
                        }
                        else
                        {
                            infoPurchaseReturnDetails.CategoryId = 0;
                        }
                        if (pr.Description != null && pr.Description != string.Empty)
                        {
                            infoPurchaseReturnDetails.itemDescription = pr.Description;
                        }
                    infoPurchaseReturnDetails.PurchaseDetailsId = pr.PurchaseDetailsId;
                        if (pr.PurchaseReturnDetailId != 0)
                        {
                            if (pr.PurchaseReturnDetailId == 0)
                            {
                                SPPurchaseReturnDetails.PurchaseReturnDetailsAddWithReturnIdentity(infoPurchaseReturnDetails);
                            }
                            else
                            {
                                infoPurchaseReturnDetails.PurchaseReturnDetailsId = pr.PurchaseReturnDetailId;
                                SPPurchaseReturnDetails.PurchaseReturnDetailsEdit(infoPurchaseReturnDetails);
                            }
                        }
                        else
                        {
                            SPPurchaseReturnDetails.PurchaseReturnDetailsAddWithReturnIdentity(infoPurchaseReturnDetails);
                        }
                        
                        infoPurchaseMaster = SPPurchaseMaster.PurchaseMasterView(infoPurchaseReturnMaster.PurchaseMasterId);
                        infoStockPosting.Date = infoPurchaseReturnMaster.Date;
                        infoStockPosting.ProductId = infoPurchaseReturnDetails.ProductId;
                        infoStockPosting.BatchId = infoPurchaseReturnDetails.BatchId;
                        infoStockPosting.UnitId = infoPurchaseReturnDetails.UnitId;
                        infoStockPosting.GodownId = infoPurchaseReturnDetails.GodownId;
                        infoStockPosting.RackId = infoPurchaseReturnDetails.RackId;
                        decimal decConversionId = SPUnitConversion.UnitConversionRateByUnitConversionId(infoPurchaseReturnDetails.UnitConversionId);
                        //infoStockPosting.OutwardQty = infoPurchaseReturnDetails.Qty / (decConversionId == 0 ? 1 : decConversionId);
                        infoStockPosting.OutwardQty = infoPurchaseReturnDetails.Qty;
                        infoStockPosting.InwardQty = 0;
                        infoStockPosting.Rate = infoPurchaseReturnDetails.Rate;
                        infoStockPosting.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                        infoStockPosting.Extra1 = string.Empty;
                        infoStockPosting.Extra2 = string.Empty;
                        if (infoPurchaseReturnDetails.PurchaseDetailsId != 0)
                        {
                            infoStockPosting.AgainstVoucherTypeId = decPurchaseReturnVoucherTypeId; // infoPurchaseMaster.VoucherTypeId;
                            infoStockPosting.AgainstVoucherNo = pr.VoucherNo; // infoPurchaseMaster.VoucherNo;
                            infoStockPosting.AgainstInvoiceNo = pr.InvoiceNo; // infoPurchaseMaster.InvoiceNo;
                            infoStockPosting.VoucherNo = infoPurchaseMaster.VoucherNo; // strVoucherNo;
                            infoStockPosting.InvoiceNo = infoPurchaseMaster.InvoiceNo; // txtReturnNo.Text.Trim();
                            infoStockPosting.VoucherTypeId = pr.VoucherTypeId; // decPurchaseReturnVoucherTypeId;
                            decAgainstVoucherTypeId = infoStockPosting.VoucherTypeId;
                        }
                        else
                        {
                            infoStockPosting.AgainstVoucherTypeId = 0;
                            infoStockPosting.AgainstVoucherNo = "NA";
                            infoStockPosting.AgainstInvoiceNo = "NA";
                            infoStockPosting.VoucherNo = infoPurchaseReturnMaster.VoucherNo;
                            infoStockPosting.InvoiceNo = infoPurchaseReturnMaster.InvoiceNo;
                            infoStockPosting.VoucherTypeId = decPurchaseReturnVoucherTypeId;
                            decAgainstVoucherTypeId = 0;
                        }
                        spStockPosting.StockPostingAdd(infoStockPosting);
                    }
                infoAccountLedger = spAccountLedger.AccountLedgerView(infoPurchaseReturnMaster.LedgerId);
                if (infoAccountLedger.BillByBill == true)
                {
                    infoPartyBalance.Date = infoPurchaseReturnMaster.Date;
                    infoPartyBalance.LedgerId = infoPurchaseReturnMaster.LedgerId;
                    if (decAgainstVoucherTypeId != 0)
                    {
                        infoPartyBalance.VoucherTypeId = infoPurchaseMaster.VoucherTypeId;
                        infoPartyBalance.VoucherNo = infoPurchaseMaster.VoucherNo;
                        infoPartyBalance.InvoiceNo = infoPurchaseMaster.InvoiceNo;
                        infoPartyBalance.AgainstVoucherTypeId = infoPurchaseReturnMaster.VoucherTypeId;
                        infoPartyBalance.AgainstVoucherNo = infoPurchaseReturnMaster.VoucherNo;
                        infoPartyBalance.AgainstInvoiceNo = infoPurchaseReturnMaster.InvoiceNo;
                        infoPartyBalance.ReferenceType = "Against";
                    }
                    else
                    {
                        infoPartyBalance.VoucherTypeId = infoPurchaseReturnMaster.VoucherTypeId;
                        infoPartyBalance.VoucherNo = infoPurchaseReturnMaster.VoucherNo;
                        infoPartyBalance.InvoiceNo = infoPurchaseReturnMaster.InvoiceNo;
                        infoPartyBalance.AgainstVoucherTypeId = 0;
                        infoPartyBalance.AgainstVoucherNo = "NA";
                        infoPartyBalance.AgainstInvoiceNo = "NA";
                        infoPartyBalance.ReferenceType = "New";
                    }
                    infoPartyBalance.Debit = infoPurchaseReturnMaster.TotalAmount;
                    infoPartyBalance.Credit = 0;
                    infoPartyBalance.CreditPeriod = 0;
                    infoPartyBalance.ExchangeRateId = infoPurchaseReturnMaster.ExchangeRateId;
                    infoPartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoPartyBalance.Extra1 = string.Empty;
                    infoPartyBalance.Extra2 = string.Empty;
                    spPartyBalance.PartyBalanceAdd(infoPartyBalance);
                }

                // delete previous tax that was entered
                DBMatConnection conn = new DBMatConnection();
                string queryStr = string.Format("select prb.purchaseReturnBillTaxId from tbl_PurchaseReturnBilltax prb WHERE prb.purchaseReturnMasterId = {0}", decPurchaseReturnMasterId);
                string returnedValue = conn.getSingleValue(queryStr);
                if (returnedValue != null && returnedValue != string.Empty && returnedValue != string.Empty)
                {
                    spPurchaseReturnBillTax.PurchaseReturnBilltaxDelete(Convert.ToDecimal(returnedValue));
                }
                foreach (var prt in input.LineItems)
                {
                    if (prt.TaxId != 0)
                    {
                        infoPurchaseReturnBillTax.PurchaseReturnMasterId = decPurchaseReturnMasterIds; // decPurchaseReturnMasterIds;
                        infoPurchaseReturnBillTax.TaxId = prt.TaxId;
                        infoPurchaseReturnBillTax.TaxAmount = prt.taxAmount;
                        infoPurchaseReturnBillTax.Extra1 = string.Empty;
                        infoPurchaseReturnBillTax.Extra2 = string.Empty;
                        spPurchaseReturnBillTax.PurchaseReturnBilltaxAdd(infoPurchaseReturnBillTax);
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return "Purchase Return Saved";
        }

        public string VoucherNumberGeneration()
        {
            string strPrefix = string.Empty;
            string strSuffix = string.Empty;
            string tableName = "PurchaseReturnMaster";
            string strReturnNo = string.Empty;
            TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
            PurchaseReturnMasterSP SPPurchaseReturnMaster = new PurchaseReturnMasterSP();
            try
            {
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }
                strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(decPurchaseReturnVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                if (Convert.ToDecimal(strVoucherNo) != SPPurchaseReturnMaster.PurchaseReturnMasterGetMaxPlusOne(decPurchaseReturnVoucherTypeId))
                {
                    strVoucherNo = SPPurchaseReturnMaster.PurchaseReturnMasterGetMax(decPurchaseReturnVoucherTypeId).ToString();
                    strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(decPurchaseReturnVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                    if (SPPurchaseReturnMaster.PurchaseReturnMasterGetMax(decPurchaseReturnVoucherTypeId) == "0")
                    {
                        strVoucherNo = "0";
                        strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(decPurchaseReturnVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                    }
                }
                SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decPurchaseReturnVoucherTypeId, DateTime.Now);
                strPrefix = infoSuffixPrefix.Prefix;
                strSuffix = infoSuffixPrefix.Suffix;
                decPurchaseReturnSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                strReturnNo = strPrefix + strVoucherNo + strSuffix;
            }
            catch (Exception ex)
            {
            }
            return strReturnNo;
        }
        [HttpGet]
        public string GetAutoFormNo()
        {
            return VoucherNumberGeneration();
        }

        [HttpGet]
        public IHttpActionResult GetPurchaseReturn(int? ledgerId = null, string invoiceNo = null, DateTime? from =null, DateTime? to=null)
        {

            var obj = new List<dynamic>();
            var data = context.tbl_PurchaseReturnMaster.ToList();
            
            foreach (var a in data)
            {
                if (a.doneby != null)
                {
                    try
                    {
                        var rec = Int64.Parse(a.doneby);

                        var b = context.tbl_AccountLedger.Where(x => x.ledgerId == a.ledgerId).SingleOrDefault();
                        obj.Add(new
                        {
                            txtId = a.purchaseReturnMasterId,
                            total = a.totalAmount,
                            invoiceNo = a.invoiceNo,
                            //voucherNo = Convert.ToString(a.purchaseMasterId.Value),
                            //voucherNo = a.purchaseMasterId,
                            voucherNo = a.voucherNo,
                            //voucherNo = context.tbl_PurchaseMaster.Where(d => d.purchaseMasterId == a.purchaseMasterId).SingleOrDefault()?.purchaseMasterId,
                            date = a.date,
                            supplierName = b.ledgerName,
                            supplierId = b.ledgerId,
                            doneBy = context.tbl_User.Where(x => x.userId == rec).FirstOrDefault()?.userName,
                            userName = context.tbl_User.Where(c => c.userId == a.userId).FirstOrDefault()?.userName
                        });
                    }
                    catch (Exception ex)
                    {

                        throw;
                    }
                }
            }
            if (ledgerId != null)
            {
                obj = obj.Where(a => a.supplierId == ledgerId).ToList();
            }

            if (invoiceNo != null)
            {
                obj = obj.Where(a =>a.invoiceNo == invoiceNo).ToList();
            }

            if (from != null)
            {
                obj = obj.Where(a =>a.date >= from).ToList();

            }
            if (to != null)
            {
                obj = obj.Where(a =>a.date <= to).ToList();

            }
              return Json(obj);
        }

        [HttpGet]
        public IHttpActionResult GetPurchaseReturnDetail(int purchaseReturnMasterId)
        {
            var data = context.tbl_PurchaseReturnDetails
                .Include(a=>a.tbl_PurchaseReturnMaster)
                .Where(a => a.purchaseReturnMasterId == purchaseReturnMasterId)
                .ToList()
                .Select((a) =>
                    new
                    {
                        Detail = a,
                        Initial = context.tbl_PurchaseDetails.Find(a.purchaseDetailsId),
                        Product = context.tbl_Product.Where(b => b.productId == a.productId).FirstOrDefault(),
                        Store = context.tbl_Godown.Where(b => b.godownId == a.godownId).ToList().FirstOrDefault()?.godownName,
                        Master = a.tbl_PurchaseReturnMaster
                    });
            return Ok(data);
        }

        [HttpPost]
        public IHttpActionResult Edit(EditPurchaseReturnDto edit)
        {
            if (edit == null) return BadRequest("No Data Found");
            var auditLogs = new List<TransactionAuditLog>();
            var vochureType = Constants.PurchaseReturn_VoucherTypeId;
            var salesMaster = context.tbl_PurchaseReturnMaster.Find(edit.PurchaseReturnMaster.purchaseMasterId);
            if (salesMaster != null)
            {
                //check for sales return 
                auditLogs.Add(new TransactionAuditLog()
                {
                    TransId = salesMaster.purchaseMasterId.ToString(),
                    DateTime = DateTime.UtcNow,
                    Amount = salesMaster.totalAmount,
                    VochureTypeId = vochureType,
                    VochureName = TransactionsEnum.PurchaseReturn.ToString(),
                    AdditionalInformation = "Edited Sales Return Master",
                    UserId = PublicVariables._decCurrentUserId,
                    Status = TransactionOperationEnums.Edit.ToString()
                });

                salesMaster.narration = edit.PurchaseReturnMaster.narration;
                salesMaster.totalAmount = edit.PurchaseReturnMaster.totalAmount;

                salesMaster.grandTotal = edit.PurchaseReturnMaster.grandTotal;

                context.Entry(salesMaster).State = EntityState.Modified;


                //edit line items
                var details = salesMaster.tbl_PurchaseReturnDetails;

                var detailIds = details.Select(a => Convert.ToDecimal(a.purchaseReturnDetailsId)).ToList();
                var updatedIds = new List<decimal>();


                foreach (var i in edit.PurchaseReturnDetails)
                {
                    //get item to update

                    var item = details.FirstOrDefault(a => a.purchaseReturnDetailsId == i.purchaseReturnDetailsId);

                    if (item != null)
                    {
                        auditLogs.Add(new TransactionAuditLog()
                        {
                            TransId = item.purchaseReturnDetailsId.ToString(),
                            DateTime = DateTime.UtcNow,
                            Amount = item.amount,
                            Quantity = item.qty,
                            VochureTypeId = vochureType,
                            VochureName = TransactionsEnum.PurchaseMaster.ToString(),
                            AdditionalInformation = "Edited Purchase  Return Record",
                            UserId = PublicVariables._decCurrentUserId,
                            Status = TransactionOperationEnums.Edit.ToString()
                        });
                        item.discount = i.discount;
                        item.qty = i.qty;
                        item.amount = item.rate * i.qty;
                        context.Entry(item).State = EntityState.Modified;
                        updatedIds.Add(item.purchaseReturnDetailsId);
                    }
                    if (i.purchaseDetailsId == 0)
                    {
                        i.extraDate = DateTime.UtcNow;
                        context.tbl_PurchaseReturnDetails.Add(i);
                    }
                }

                var itemToDeleteIds = detailIds.Except(updatedIds);
                foreach (var i in itemToDeleteIds)
                {
                    var item = details.FirstOrDefault(a => a.purchaseReturnDetailsId == i);
                    if (item != null)
                    {
                        context.tbl_PurchaseReturnDetails.Remove(item);
                    }
                }
                context.SaveChanges();

                var stockPosting = context.tbl_StockPosting.Where(a =>
                    a.voucherTypeId == vochureType && a.voucherNo == salesMaster.voucherNo).ToList();
                // delete stock posting 
                if (stockPosting.Any())
                {
                    context.tbl_StockPosting.RemoveRange(stockPosting);
                }
                //get new updated items 
                var updatedItems = context.tbl_PurchaseReturnDetails
                    .Where(a => a.purchaseReturnMasterId == salesMaster.purchaseReturnMasterId)
                    .ToList();

                // add new stock posting to update record
                foreach (var i in updatedItems)
                {
                    var product = context.tbl_Product.Find(i.productId);
                    context.tbl_StockPosting.Add(new DBModel.tbl_StockPosting
                    {
                        //set items here 
                        batchId = i.batchId,
                        CategoryId = i.CategoryId,
                        productId = i.productId,
                        ProjectId = i.ProjectId,
                        extra2 = i.extra2,
                        voucherTypeId = Constants.SalesReturn_VoucherTypeId,
                        date = DateTime.UtcNow,
                        extraDate = DateTime.UtcNow,
                        financialYearId = PublicVariables._decCurrentFinancialYearId,
                        extra1 = "",
                        rackId = i.rackId,
                        voucherNo = salesMaster.voucherNo,
                        invoiceNo = salesMaster.invoiceNo,
                        unitId = i.unitId,
                        rate = product.purchaseRate,
                        godownId = i.godownId,
                        outwardQty = 0,
                        inwardQty = i.qty,
                        againstInvoiceNo = "",
                        againstVoucherNo = "",
                        againstVoucherTypeId = 0
                    });
                }
                context.SaveChanges();
                //get all ledger posting base on the vocheur id from the master, then delete them and create a new one 
                var oldLedgerPosting = context.tbl_LedgerPosting
                    .Where(a => a.voucherNo == salesMaster.voucherNo && a.voucherTypeId == Constants.SalesReturn_VoucherTypeId)
                    .ToList();
                if (oldLedgerPosting.Any())
                {
                    context.tbl_LedgerPosting.RemoveRange(oldLedgerPosting);
                }
                var goodInTransitLedger = LedgerConstants.GoodsInTransit;
                foreach (var i in updatedItems)
                {
                    var product = context.tbl_Product.Find(i.productId);

                   
                    //Account Payable 
                    context.tbl_LedgerPosting.Add(new DBModel.tbl_LedgerPosting()
                    {
                        voucherTypeId = Constants.SalesReturn_VoucherTypeId,
                        date = DateTime.UtcNow,
                        ledgerId = salesMaster.ledgerId,
                        extra2 = "",
                        extraDate = DateTime.UtcNow,
                        voucherNo = salesMaster.voucherNo,
                        extra1 = "",
                        debit = i.qty * i.rate,
                        credit = 0,
                        invoiceNo = salesMaster.invoiceNo,
                        chequeDate = DateTime.UtcNow,
                        yearId = PublicVariables._decCurrentFinancialYearId,
                        detailsId = i.purchaseReturnDetailsId,
                        chequeNo = ""
                    });
                    context.SaveChanges();
                }
                if (auditLogs.Any())
                {
                    new Services.TransactionAuditService().CreateMany(auditLogs);
                }
                return Ok(new { status = 200, message = "Edit Made Successful" });

            }

            return InternalServerError();
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var audits = new List<TransactionAuditLog>();
            var master = context.tbl_SalesReturnMaster.Find(id);

            if (master == null) return NotFound();

            var vochureId = Constants.PurchaseReturn_VoucherTypeId;

            //find ledgerPosting and Delete  

            var ledgerPosting = context.tbl_LedgerPosting.Where(a =>
                a.voucherTypeId == vochureId
                && a.voucherNo == master.voucherNo);
            if (ledgerPosting.Any())
            {
                context.tbl_LedgerPosting.RemoveRange(ledgerPosting);
                //audits.AddRange( ledgerPosting.Select(a=> new TransactionAuditLog
                //{

                //}));
            }
            //find all stock posting and delete 
            var stocks = context.tbl_StockPosting.Where(a => a.voucherTypeId == vochureId &&
                                                 a.voucherNo == master.voucherNo).ToList();
            if (stocks.Any())
            {
                context.tbl_StockPosting.RemoveRange(stocks);
            }
            audits.AddRange(master.tbl_SalesReturnDetails.Select(a => new TransactionAuditLog
            {
                Quantity = a.qty,
                Amount = a.amount * a.qty,
                UserId = PublicVariables._decCurrentUserId,
                VochureName = Enums.TransactionsEnum.PurchaseReturn.ToString(),
                DateTime = DateTime.Now,
                Status = Enums.TransactionOperationEnums.Delete.ToString(),
                AdditionalInformation = "Delete Items From Purchase Return Details",
                VochureTypeId = Constants.SalesInvoice_VoucherTypeId,
                TransId = a.salesDetailsId.ToString(),
            }));
            audits.Add(new TransactionAuditLog()
            {

                Amount = master.totalAmount,
                UserId = PublicVariables._decCurrentUserId,
                VochureName = Enums.TransactionsEnum.SalesMaster.ToString(),
                DateTime = DateTime.Now,
                Status = Enums.TransactionOperationEnums.Delete.ToString(),
                AdditionalInformation = "Delete Items From Purchase Return Master",
                VochureTypeId = Constants.SalesInvoice_VoucherTypeId,
                TransId = master.salesMasterId.ToString(),
            });

            //delete master and detail in 
            context.tbl_SalesReturnMaster.Remove(master);
            context.SaveChanges();
            new Services.TransactionAuditService().CreateMany(audits);


            return Ok(new { status = 200, message = "Delete Made Successful" });
        }

    }
}
