﻿using MatApi.DBModel;
using MatApi.Dto;
using MatApi.Models;
using MatApi.Models.Reports.FinancialStatements;
using MatApi.Models.Reports.OtherReports;
using MatApi.Services;
using MATClassLibrary.Classes.SP;
using MATFinancials;
using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Security.Principal;
using System.Text;
using System.Threading.Tasks;
using System.Web.Hosting;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Script.Serialization;
using System.Windows.Forms.VisualStyles;
using PublicVariables = MATFinancials.PublicVariables;


namespace MatApi.Controllers.Reports.FinancialStatements
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FinancialStatementsController : ApiController
    {
        public DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();

        [HttpPost]
        public MatResponse GetTrialBalanceReport(TrialBalanceParam input)
        {
            MatResponse response = new MatResponse();
            List<TrialBalanceResponseVM> trialBalanceOutput = new List<TrialBalanceResponseVM>();

            try
            {
                decimal dcOpeningStockForRollOver = 0;
                string calculationMethod = string.Empty;
                SettingsInfo InfoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                //GeneralQueries queryForRetainedEarning = new GeneralQueries();
                //--------------- Selection Of Calculation Method According To Settings ------------------// 
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }

                //input.FromDate = MATFinancials.PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                input.FromDate = PublicVariables._dtFromDate.ToShortDateString();

                FinancialStatementSP SpFinance1 = new FinancialStatementSP();
                CurrencyInfo InfoCurrency = new CurrencyInfo();
                CurrencySP SpCurrency = new CurrencySP();
                //GeneralQueries methodForStockValue = new GeneralQueries();
                DBMatConnection conn = new DBMatConnection();
                int inDecimalPlaces = InfoCurrency.NoOfDecimalPlaces;
                decimal dcClosingStock = SpFinance1.StockValueGetOnDate(Convert.ToDateTime(input.ToDate),
                    calculationMethod, false, false);
                dcClosingStock = Math.Round(dcClosingStock, inDecimalPlaces);
                //---------------------Opening Stock-----------------------
                //decimal dcOpeninggStock = SpFinance1.StockValueGetOnDate(PublicVariables._dtFromDate, calculationMethod, true, true);           
                string queryStr =
                    string.Format(
                        "select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ",
                        MATFinancials.PublicVariables._dtToDate.ToString());
                string queryStr2 =
                    string.Format(
                        "select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ",
                        MATFinancials.PublicVariables._dtFromDate.ToString());
                decimal dcOpeninggStock =
                    SpFinance1.StockValueGetOnDate(Convert.ToDateTime(input.ToDate), calculationMethod, true, true);
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty
                    ? Convert.ToDateTime(conn.getSingleValue(queryStr2))
                    : MATFinancials.PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty
                    ? Convert.ToDateTime(conn.getSingleValue(queryStr))
                    : MATFinancials.PublicVariables._dtFromDate.AddDays(-1);
                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    lastFinYearEnd = Convert.ToDateTime(input.FromDate).AddDays(-1);
                }

                //lastFinYearEnd = new DateTime(2018, 12, 31);
                //lastFinYearStart = new DateTime(2018, 1, 1);
                //dcOpeningStockForRollOver = SpFinance1.StockValueGetOnDateForRollOver(DateTime.Parse(PublicVariables._dtToDate.ToString()), calculationMethod, false, false, true);
                //dcOpeningStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, false, 0, false, DateTime.Now);
                dcOpeninggStock = dcOpeninggStock + dcOpeningStockForRollOver;

                /// -----------------------section -----------------
                ///
                //input.ToDate = new DateTime(2019,12,31 ).ToShortDateString();
                FinancialStatementSP financialStatement = new FinancialStatementSP();
                DataTable dtTrial = new DataTable();
                dtTrial = financialStatement.TrialBalanceGridFill(Convert.ToDateTime(input.ToDate));
                var querySumCr = 0m;
                var querySumDr = 0m;

                // ----------------- calculate and replace retained earning in the datatable ---------------- //
                // decimal retainedEarning = queryForRetainedEarning.retainedEarnings(Convert.ToDateTime(input.ToDate));
                // string retainedEarnings = retainedEarning > 0 ? retainedEarning.ToString() : "(" + (retainedEarning * -1).ToString() + ")";
                //if (dtTrial.AsEnumerable().Where(m => m.Field<string>("accountGroupName").ToLower() == "retained earning").Any())
                //{
                //    foreach (DataRow row in dtTrial.Rows)
                //    {
                //        if (row["accountGroupName"].ToString().Trim().ToLower() == "retained earning".ToLower())
                //        {
                //            if (retainedEarning > 0)
                //            {
                //                row["credit"] = retainedEarning;
                //                row["creditDifference"] = retainedEarning;
                //            }
                //            else
                //            {
                //                retainedEarning = retainedEarning * -1;
                //                row["debit"] = retainedEarning;
                //                row["debitDifference"] = retainedEarning;
                //            }
                //            row["accountGroupName"] = "Retained Earning";
                //        }
                //    }
                //}
                //else if (retainedEarning != 0)
                //{
                //    if (retainedEarning > 0)
                //    {
                //        DataRow dRE1 = dtTrial.NewRow();
                //        dRE1["accountGroupId"] = 0;
                //        dRE1["credit"] = retainedEarning;
                //        dRE1["creditDifference"] = retainedEarning;
                //        dRE1["debit"] = 0.0;
                //        dRE1["debitDifference"] = 0.00;
                //        dRE1["accountGroupName"] = "Retained Earning";
                //        dtTrial.Rows.Add(dRE1);
                //    }
                //    else if (retainedEarning < 0)
                //    {
                //        retainedEarning = retainedEarning * -1;
                //        DataRow dRE2 = dtTrial.NewRow();
                //        dRE2["accountGroupId"] = 0;
                //        dRE2["credit"] = 0.0;
                //        dRE2["creditDifference"] = 0.0;
                //        dRE2["debit"] = retainedEarning;
                //        dRE2["debitDifference"] = retainedEarning;
                //        dRE2["accountGroupName"] = "Retained Earning";
                //        dtTrial.Rows.Add(dRE2);
                //    }
                //}
                DataRow dr = dtTrial.NewRow(); // to add new row
                dr["accountGroupId"] = -4;
                dr["debit"] = 0.0;
                dr["credit"] = 0.0;
                dr["creditDifference"] = 0.0;
                dr["debitDifference"] = dcOpeninggStock;
                dr["accountGroupName"] = "Stock";
                dtTrial.Rows.Add(dr);

                querySumCr = (from c in dtTrial.AsEnumerable() select c.Field<decimal>("creditDifference")).Sum();
                querySumDr = (from c in dtTrial.AsEnumerable() select c.Field<decimal>("debitDifference")).Sum();

                DataRow drSum = dtTrial.NewRow(); // to add new row                  
                drSum["accountGroupId"] = -2;
                drSum["debit"] = 0.0;
                drSum["credit"] = 0.0;
                drSum["creditDifference"] = querySumCr;
                drSum["debitDifference"] = querySumDr;
                drSum["accountGroupName"] = "TOTAL";

                //dtTrial.Rows.Add(drNewLine);    
                dtTrial.Rows.Add(drSum);

                //dgvTrailBalance.DataSource = query;            
                //dgvTrailBalance.Rows.Clear();   // clears the previous record before adding new ones, based on filter.
                foreach (DataRow trialRows in dtTrial.Rows)
                {
                    if (Convert.ToDecimal(trialRows["accountGroupId"].ToString()) == -2)
                    {
                        trialBalanceOutput.Add(new TrialBalanceResponseVM
                        {
                            AccountGroupId = "",
                            CreditDifference = "____________________",
                            DebitDifference = "_____________________"
                        });
                        //dgvTrailBalance.Rows.Add();
                        //dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["ledgerName"].Value = "";
                        //dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["debitDifference"].Value = "_______________";
                        //dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["creditDifference"].Value = "_______________";
                    }

                    //dgvTrailBalance.Rows.Add();
                    //dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["ledgerName"].Value = trialRows["accountGroupName"];
                    //dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["debitDifference"].Value = Convert.ToDecimal(trialRows["debitDifference"]).ToString("N2");
                    //dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["creditDifference"].Value = Convert.ToDecimal(trialRows["creditDifference"]).ToString("N2");
                    //dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["accountGroupId"].Value = trialRows["accountGroupId"];
                    decimal debitDiff = Convert.ToDecimal(trialRows["debitDifference"]);
                    decimal ceditDiff = Convert.ToDecimal(trialRows["creditDifference"]);

                    FinancialStatementSP SpFinance = new FinancialStatementSP();

                    decimal direct = SpFinance.GetDirectPostingIntoRetainedEanings(Convert.ToDateTime(input.FromDate),
                        Convert.ToDateTime(input.ToDate));
                    if (trialRows["accountGroupName"].ToString().ToLower() == "retained earning".ToLower())
                    {
                        debitDiff = debitDiff - direct;
                        ceditDiff = ceditDiff - direct;
                    }

                    trialBalanceOutput.Add(new TrialBalanceResponseVM
                    {
                        AccountGroupId = trialRows["accountGroupId"].ToString(),
                        CreditDifference = Convert.ToDecimal(trialRows["creditDifference"]).ToString("N2"),
                        DebitDifference = Convert.ToDecimal(trialRows["debitDifference"]).ToString("N2"),
                        LedgerName = trialRows["accountGroupName"].ToString()
                    });
                }

                trialBalanceOutput.Add(new TrialBalanceResponseVM
                {
                    AccountGroupId = "",
                    CreditDifference = "===================",
                    DebitDifference = "==================="
                });
                //dgvTrailBalance.Rows.Add();
                //dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["ledgerName"].Value = "";
                //dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["debitDifference"].Value = "===============";
                //dgvTrailBalance.Rows[dgvTrailBalance.Rows.Count - 1].Cells["creditDifference"].Value = "===============";
                //lblcreditSum.Text = querySumCr.ToString("N2");
                //lbldebitSum.Text = querySumDr.ToString("N2");
                response.ResponseCode = 200;
                response.Response = trialBalanceOutput;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
            }

            return response;
        }

        [HttpPost]
        public MatResponse GetBalanceSheetReport(BalanceSheetParam input)
        {
          
            input.ToDate = Convert.ToDateTime(input.ToDate).AddDays(1).AddSeconds(-1).ToString();
            ;
            input.FromDate = Convert.ToDateTime(input.FromDate).AddDays(1).AddSeconds(-1).ToString();
            ;
            MatResponse response = new MatResponse();
            List<BalanceSheetResponseVM> output = new List<BalanceSheetResponseVM>();
            try
            {
                bool ShowInventory = false;
                bool ShowProfitAndLoss = false;
                decimal Cost = 0;
                decimal AccDep = 0;
                decimal TotalEquitiesAndLiabilities = 0;
                decimal Total = 0;
                decimal TotalELSubGroup = 0;
                int OrderNumber = 0;
                string PropertyPlantsAndEquipment = "";
                decimal ClosingStock = 0;
                decimal ClosingStockForRollOver = 0;
                DataSet dsetProfitAndLoss = null;
                DataSet dsetProfitAndLossRollOver = null;
                string calculationMethod = "";
                decimal decPrintOrNot = 0;

                DBMatConnection conn = new DBMatConnection();
                string queryStr =
                    string.Format(
                        "select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ",
                        MATFinancials.PublicVariables._dtToDate.ToString());
                string queryStr2 =
                    string.Format(
                        "select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ",
                        MATFinancials.PublicVariables._dtFromDate.ToString());
                //DateTime lastFinYearEnd = DateTime.Parse(txtFromDate.Text); // conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty
                    ? Convert.ToDateTime(conn.getSingleValue(queryStr2))
                    : MATFinancials.PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty
                    ? Convert.ToDateTime(conn.getSingleValue(queryStr))
                    : MATFinancials.PublicVariables._dtFromDate.AddDays(-1);
                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    lastFinYearEnd = Convert.ToDateTime(MATFinancials.PublicVariables._dtFromDate.AddDays(-1));
                }

                GeneralQueries methodForStockValue = new GeneralQueries();

                CurrencyInfo InfoCurrency = new CurrencyInfo();
                CurrencySP SpCurrency = new CurrencySP();
                InfoCurrency = SpCurrency.CurrencyView(1);
                int inDecimalPlaces = InfoCurrency.NoOfDecimalPlaces;

                FinancialStatementSP SpFinance = new FinancialStatementSP();
                DataSet DsetBalanceSheet = new DataSet();
                DataSet DsetBalanceSheetForRollOver = new DataSet();
                DataTable dtbl = new DataTable();
                DataTable dtblGroup = new DataTable();
                DataTable dtblSubGroups = new DataTable();
                SettingsInfo InfoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                AccountGroupSP spAccountGroup = new AccountGroupSP();
                //--------------- Selection Of Calculation Method According To Settings ------------------// 
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }

                int LiabilityRow = 0;
                int AssetRow = 0;
                decimal TotalAssets = 0;
                decimal TotalLiabilities = 0;
                decimal TotalPPE = 0;
                decimal TotalNBV = 0;
                decimal TotalEL = 0;
                decimal TotalInventory = 0;
                decimal dcOpeningStockForRollOver = 0;
                decimal dcOpeningStockForBalanceSheetRollOver = 0;
                decimal TotalCost = 0;
                decimal TotalDep = 0;

                decimal OpeningStock = SpFinance.StockValueGetOnDate(Convert.ToDateTime(input.ToDate), calculationMethod, true, true);
                OpeningStock = Math.Round(OpeningStock, inDecimalPlaces);

                dcOpeningStockForBalanceSheetRollOver =
                    SpFinance.StockValueGetOnDateForRollOver(Convert.ToDateTime(input.ToDate), calculationMethod, true,
                        true, true);

                dcOpeningStockForRollOver = SpFinance.StockValueGetOnDateForRollOver(
                    DateTime.Parse(MATFinancials.PublicVariables._dtToDate.ToString()), calculationMethod, false, false,
                    true);

                OpeningStock = OpeningStock + dcOpeningStockForRollOver;
                DsetBalanceSheet = SpFinance.BalanceSheet(MATFinancials.PublicVariables._dtFromDate,
                    DateTime.Parse(input.ToDate));

                DsetBalanceSheetForRollOver =
                    SpFinance.BalanceSheetForRollOver(MATFinancials.PublicVariables._dtFromDate,
                        DateTime.Parse(input.ToDate));

                //------------------- Asset -------------------------------// 
                //------------------- Non Current Asset -------------------------------// 
                dtbl = DsetBalanceSheet.Tables[0];
                output.Add(new BalanceSheetResponseVM
                {
                    TxtAsset = "NON CURRENT ASSETS".ToUpper()
                });

                output.Add(new BalanceSheetResponseVM
                {
                    Amount1 = "Cost",
                    Amount2 = "Acc. Depreciation",
                    Amount3 = "NBV"
                });

                AssetRow ++;

                foreach (DataRow rw in dtbl.Rows)
                {
                    int id = Convert.ToInt32(rw["ID"].ToString());

                    decimal balance = (rw["ID"].ToString() == "6")
                        ? Convert.ToDecimal(rw["Balance"].ToString()) + OpeningStock
                        : Convert.ToDecimal(rw["Balance"].ToString());
                    PropertyPlantsAndEquipment = rw["Name"].ToString().ToLower();

                    if (balance > 0)
                    {
                        output.Add(new BalanceSheetResponseVM
                        {
                            TxtAsset = rw["Name"].ToString().ToUpper(),
                            GroupId1 = rw["ID"].ToString().ToUpper()
                        });
                        AssetRow++;
                    }
                    else
                    {
                        balance = balance * -1;
                        output.Add(new BalanceSheetResponseVM
                        {
                            TxtAsset = rw["Name"].ToString().ToUpper(),
                            GroupId2 = rw["ID"].ToString().ToUpper()
                        });
                        TotalLiabilities += balance;
                        AssetRow++;
                    }

                    // -------------------- place inventories in current assets --------------------------------------- //
                    if (rw["Name"].ToString().ToLower() == "current assets")
                    {
                        ShowInventory = true;
                    }

                    //----------------- get ledgers under the groups with the current id add them to gridview -------------------//
                    if (Convert.ToInt32(rw["ID"].ToString()) == id)
                    {
                        //----------------- Inventories -------------------//
                        if (ShowInventory)
                        {
                            decimal Inventories = 0;
                            //Inventories = SpFinance.StockValueGetOnDate(Convert.ToDateTime(txtToDate.Text), calculationMethod, false, false); // old implementation 20171801
                            Inventories = methodForStockValue.currentStockValue(lastFinYearStart,
                                Convert.ToDateTime(input.ToDate), false, 0, false, DateTime.Now);

                            Inventories = Math.Round(Inventories, inDecimalPlaces); // - OpeningStock;
                            if (Inventories != 0)
                            {
                                TotalInventory += Inventories;
                                if (Inventories >= 0)
                                {
                                    output.Add(new BalanceSheetResponseVM
                                    {
                                        TxtAsset = "Inventories",
                                        Amount2 = Inventories.ToString("N2")
                                    });
                                    AssetRow++;
                                }
                                else
                                {
                                    Inventories = Inventories * -1;
                                    AssetRow++;
                                    output.Add(new BalanceSheetResponseVM
                                    {
                                        TxtAsset = "Inventories",
                                        Amount2 = "(" + Inventories.ToString("N2") + ")"
                                    });
                                }
                            }

                            ShowInventory = false;
                        }

                        dtblGroup = new DataTable();

                        //if (cmbProject.SelectedIndex == 0 && cmbCategory.SelectedIndex == 0)
                        //{
                        dtblGroup = spAccountGroup.AccountGroupWiseReportViewAll(id, Convert.ToDateTime(input.FromDate),
                            Convert.ToDateTime(input.ToDate));
                        //}
                        //if (cmbProject.SelectedIndex > 0 && cmbCategory.SelectedIndex > 0)
                        //{
                        //    int ProjectId = Convert.ToInt32(cmbProject.SelectedValue);
                        //    int CategoryId = Convert.ToInt32(cmbCategory.SelectedValue);
                        //    dtblGroup = spAccountGroup.AccountGroupWiseReportViewAllByProjectAndCategory(id, Convert.ToDateTime(dtfromdate.ToString()), Convert.ToDateTime(txtToDate.Text), ProjectId, CategoryId);
                        //}
                        //if (cmbProject.SelectedIndex > 0 && cmbCategory.SelectedIndex == 0)
                        //{
                        //    int ProjectId = Convert.ToInt32(cmbProject.SelectedValue);
                        //    dtblGroup = spAccountGroup.AccountGroupWiseReportViewAllByProject(id, Convert.ToDateTime(dtfromdate.ToString()), Convert.ToDateTime(txtToDate.Text), ProjectId);
                        //}
                        //if (cmbProject.SelectedIndex == 0 && cmbCategory.SelectedIndex > 0)
                        //{
                        //    int CategoryId = Convert.ToInt32(cmbCategory.SelectedValue);
                        //    dtblGroup = spAccountGroup.AccountGroupWiseReportViewAllByCategory(id, Convert.ToDateTime(dtfromdate.ToString()), Convert.ToDateTime(txtToDate.Text), CategoryId);
                        //}
                        if (dtblGroup != null && dtblGroup.Rows.Count > 0)
                        {
                            TotalPPE = 0;
                            Total = 0;
                            TotalDep = 0 + TotalInventory;
                            TotalCost = 0;
                            TotalNBV = 0;

                            foreach (DataRow row in dtblGroup.Rows)
                            {
                                //decimal PPEbalance = Convert.ToDecimal(row["balance1"].ToString());
                                decimal NBV = Convert.ToDecimal(row["balance1"].ToString());
                                decimal debit = Convert.ToDecimal(row["debit"]);
                                decimal credit = Convert.ToDecimal(row["credit"]);
                                decimal PPEbalance = debit - credit;

                                BalanceSheetResponseVM temp = new BalanceSheetResponseVM();
                                temp.TxtAsset = row["name"].ToString();
                                temp.Amount2 = PPEbalance < 0
                                    ? "(" + (PPEbalance * -1).ToString("N2") + ")"
                                    : PPEbalance.ToString("N2"); //Convert.ToDecimal(row["PPEbalance"]).ToString("N2");

                                if (PropertyPlantsAndEquipment == "property plant & equipment")
                                {
                                    temp.Amount1 = debit < 0
                                        ? "(" + (debit * -1).ToString("N2") + ")"
                                        : debit.ToString("N2"); // Convert.ToDecimal(row["debit"]).ToString("N2");
                                    //dgvReport.Rows[AssetRow].Cells["Amount2"].Value = credit < 0 ? "(" + (credit * -1).ToString("N2") + ")" : credit.ToString("N2"); //Convert.ToDecimal(row["credit"]).ToString("N2");
                                    if (credit < 0)
                                    {
                                        credit = credit * -1;
                                    }

                                    temp.Amount2 = "(" + (credit).ToString("N2") + ")";
                                    temp.Amount3 = PPEbalance < 0
                                        ? "(" + (PPEbalance * -1).ToString("N2") + ")"
                                        : PPEbalance.ToString("N2");
                                }

                                TotalCost += debit;
                                TotalDep += credit;
                                TotalPPE += PPEbalance;
                                TotalNBV += NBV;

                                AssetRow++;
                                output.Add(temp);
                            }

                            string tmpAmt = "";
                            if (PropertyPlantsAndEquipment == "property plant & equipment")
                            {
                                tmpAmt = "__________";
                            }

                            output.Add(new BalanceSheetResponseVM
                            {
                                Amount1 = tmpAmt,
                                Amount2 = "_______________",
                                Amount3 = "________________"
                            });
                            AssetRow++;
                            if (PropertyPlantsAndEquipment == "property plant & equipment")
                            {
                                output.Add(new BalanceSheetResponseVM
                                {
                                    Amount1 = TotalCost < 0
                                        ? "(" + (TotalCost * -1).ToString("N2") + ")"
                                        : TotalCost.ToString("N2"),
                                    Amount2 = "(" + (TotalDep).ToString("N2") + ")",
                                    Amount3 = TotalPPE < 0
                                        ? "(" + (TotalPPE * -1).ToString("N2") + ")"
                                        : TotalPPE.ToString("N2"),
                                    GroupId1 = rw["ID"].ToString()
                                });

                                //dgvReport.Rows[AssetRow].Cells["Amount2"].Value = TotalDep < 0 ? "(" + (TotalDep * -1).ToString("N2") + ")" : TotalDep.ToString("N2");
                                if (TotalDep < 0)
                                {
                                    TotalDep = TotalDep * -1;
                                }

                                Total += TotalPPE;

                                output.Add(new BalanceSheetResponseVM
                                {
                                    Amount1 = tmpAmt,
                                    Amount2 = "_______________",
                                    Amount3 = "________________",
                                    GroupId1 = rw["ID"].ToString()
                                });
                            }
                            else
                            {
                                //decimal SummedValue = (TotalNBV + TotalInventory);
                                decimal SummedValue = (TotalPPE + TotalInventory);
                                //Total += (TotalNBV + TotalInventory);
                                Total += (TotalPPE + TotalInventory);
                                AssetRow = AssetRow - 2;
                                output.Add(new BalanceSheetResponseVM
                                {
                                    Amount3 = SummedValue < 0
                                        ? "(" + (SummedValue * -1).ToString("N2") + ")"
                                        : SummedValue.ToString("N2"),
                                    GroupId1 = rw["ID"].ToString()
                                });
                            }

                            TotalAssets += Total;
                            AssetRow++;
                            //dgvReport.Rows[AssetRow].DefaultCellStyle.Font.Underline = DataGridViewContentAlignment.TopRight;
                            var tmp1 = "";
                            var tmp2 = "";
                            if (PropertyPlantsAndEquipment == "property plant & equipment")
                            {
                                tmp1 = "__________";
                                tmp2 = "__________";
                            }

                            // ------------------ pass the group id into the current row with the summed value -------------------- //

                            // ----------------------------------------------------------------------------------------------//
                            output.Add(new BalanceSheetResponseVM
                            {
                                Amount1 = tmp1,
                                Amount2 = tmp2,
                                GroupId1 = rw["ID"].ToString()
                            });
                            AssetRow++;
                        }
                    }
                }

                // --------------------------------------------------- TOTAL ASSETS ---------------------------------------------------- //
                output.Add(new BalanceSheetResponseVM
                {
                    TxtAsset = "TOTAL ASSETS",
                    Amount3 = TotalAssets < 0
                        ? "(" + (TotalAssets * -1).ToString("N2") + ")"
                        : (TotalAssets).ToString("N2")
                });

                decPrintOrNot = TotalAssets;

                AssetRow++;
                output.Add(new BalanceSheetResponseVM
                {
                    Amount3 = "_______________"
                });
                AssetRow++;

                //------------------------------------------ EQUITIES AND LIABILITIES ------------------------------------------//                                
                output.Add(new BalanceSheetResponseVM
                {
                    TxtAsset = "EQUITIES AND LIABILITIES"
                });

                dtbl = DsetBalanceSheet.Tables[1];
                foreach (DataRow rw in dtbl.Rows)
                {
                    int id = Convert.ToInt32(rw["ID"].ToString());
                    decimal balance = Convert.ToDecimal(rw["Balance"].ToString());
                    if (balance < 0)
                    {
                        output.Add(new BalanceSheetResponseVM
                        {
                            TxtAsset = rw["name"].ToString(),
                            GroupId2 = Convert.ToDecimal(rw["ID"]).ToString()
                        });
                        AssetRow++;
                    }
                    else
                    {
                        output.Add(new BalanceSheetResponseVM
                        {
                            TxtAsset = rw["name"].ToString(),
                            GroupId2 = Convert.ToDecimal(rw["ID"]).ToString()
                        });
                        TotalLiabilities += balance;
                        AssetRow++;
                    }

                    if (rw["Name"].ToString().ToLower() == "equity and reserves")
                    {
                        OrderNumber = 1;
                    }

                    if (Convert.ToInt32(rw["ID"].ToString()) == 1)
                    {
                        ShowProfitAndLoss = true;
                    }

                    //----------------- get ledgers of groups with the current id but position Non-Current Liabilities ontop Current Liabilities with order number 2 as the second to come -------------------//
                    if (Convert.ToInt32(rw["ID"].ToString()) == id)
                    {
                        OrderNumber++;
                        dtblGroup = new DataTable();
                        dtblGroup = spAccountGroup.AccountGroupWiseReportViewAll(id, Convert.ToDateTime(input.FromDate),
                            Convert.ToDateTime(input.ToDate));

                        if (dtblGroup != null && dtblGroup.Rows.Count > 0)
                        {
                            TotalELSubGroup = 0;

                            foreach (DataRow row in dtblGroup.Rows)
                            {
                                decimal Balance1 = Convert.ToDecimal(row["balance1"].ToString());
                                decimal debit = Convert.ToDecimal(row["debit"].ToString());
                                decimal credit = Convert.ToDecimal(row["credit"].ToString());
                                decimal Balance2 = credit - debit;
                                string bal2 = "";
                                if (Balance2 > 0)
                                {
                                    bal2 = (Balance2).ToString("N2");
                                }

                                if (Balance2 < 0)
                                {
                                    bal2 = "(" + (Balance2 * -1).ToString("N2") + ")";
                                }

                                output.Add(new BalanceSheetResponseVM
                                {
                                    TxtAsset = row["name"].ToString(),
                                    Amount2 = bal2
                                });
                                TotalELSubGroup += Balance2;
                                AssetRow++;
                            }

                            //----------------------------- Profit or loss Account ---------------------------//

                            #region p/l here

                            //----------------------------- Closing Stock--------------------------------------------------- //

                            if (ShowProfitAndLoss)
                            {
                                decimal profitOrLoss =
                                    methodForStockValue.profitOrLoss(Convert.ToDateTime(input.ToDate),
                                        calculationMethod, false);
                                if (profitOrLoss >= 0) //(decCurrentProfitLoss >= 0)
                                {
                                    foreach (DataRow dRow in DsetBalanceSheet.Tables[2].Rows)
                                    {
                                        if (dRow["Name"].ToString() == "Profit And Loss Account")
                                        {
                                            output.Add(new BalanceSheetResponseVM
                                            {
                                                TxtAsset = "Profit/Loss for the Year",
                                                Amount2 = Math.Round(profitOrLoss,
                                                    MATFinancials.PublicVariables._inNoOfDecimalPlaces).ToString("N2"),
                                                GroupId1 = dRow["ID"].ToString()
                                            });
                                            TotalELSubGroup = TotalELSubGroup + Math.Round(profitOrLoss,
                                                                  MATFinancials.PublicVariables._inNoOfDecimalPlaces);
                                            AssetRow++;
                                        }
                                    }
                                }
                                else
                                {
                                    profitOrLoss = profitOrLoss * -1;
                                    foreach (DataRow dRow in DsetBalanceSheet.Tables[2].Rows)
                                    {
                                        if (dRow["Name"].ToString() == "Profit And Loss Account")
                                        {
                                            output.Add(new BalanceSheetResponseVM
                                            {
                                                TxtAsset = "Profit/Loss for the Year",
                                                Amount2 = "(" + Math.Round(profitOrLoss,
                                                                  MATFinancials.PublicVariables._inNoOfDecimalPlaces)
                                                              .ToString("N2") + ")",
                                                GroupId1 = dRow["ID"].ToString()
                                            });
                                            //TotalELSubGroup = TotalELSubGroup + Math.Round(decTotalProfitAndLoss + decCurrentProfitLoss * -1, PublicVariables._inNoOfDecimalPlaces);
                                            TotalELSubGroup = TotalELSubGroup - Math.Round(profitOrLoss,
                                                                  MATFinancials.PublicVariables._inNoOfDecimalPlaces);
                                            AssetRow++;
                                        }
                                    }
                                }

                                // ---------------------------- Asset-------------------------------------------- //

                                // ------------------------- Retained Earnings ---------------------- //
                                string bal3 = "";
                                decimal retainedEarnings =
                                    methodForStockValue.retainedEarnings(
                                        Convert.ToDateTime(input
                                            .ToDate)); //decTotalProfitAndLoss + decCurrentProfitLossRollover + decRetainedEarning;
                                decimal direct = SpFinance.GetDirectPostingIntoRetainedEanings(
                                    Convert.ToDateTime(input.FromDate), Convert.ToDateTime(input.ToDate));
                                retainedEarnings = retainedEarnings - direct;

                                if (retainedEarnings >= 0)
                                {
                                    bal3 = Math.Round(retainedEarnings,
                                        MATFinancials.PublicVariables._inNoOfDecimalPlaces).ToString("N2");
                                    TotalELSubGroup =
                                        TotalELSubGroup +
                                        retainedEarnings; // (decTotalProfitAndLoss + decCurrentProfitLossRollover + decRetainedEarning);
                                }
                                else
                                {
                                    retainedEarnings = retainedEarnings * -1;
                                    //dgvReport.Rows[AssetRow].Cells["Amount2"].Style.ForeColor = Color.Red;
                                    bal3 = "(" + Math.Round(retainedEarnings,
                                               MATFinancials.PublicVariables._inNoOfDecimalPlaces).ToString("N2") + ")";
                                    TotalELSubGroup =
                                        TotalELSubGroup -
                                        retainedEarnings; // (decTotalProfitAndLoss + decCurrentProfitLossRollover + decRetainedEarning);
                                }

                                output.Add(new BalanceSheetResponseVM
                                {
                                    Amount2 = bal3,
                                    TxtAsset = "Retained Earnings"
                                });
                                AssetRow++;
                                ShowProfitAndLoss = false;
                            }

                            #endregion

                            output.Add(new BalanceSheetResponseVM
                            {
                                Amount2 = "________________"
                            });
                            AssetRow++;
                        }

                        //dgvReport.Rows[AssetRow - 2].Cells["Amount3"].Value = TotalELSubGroup.ToString("N2");
                        //dgvReport.Rows[AssetRow - 2].Cells["GroupId2"].Value = rw["ID"].ToString();

                        string amt33 = TotalELSubGroup.ToString("N2");
                        string grp2 = rw["ID"].ToString();

                        TotalEquitiesAndLiabilities += TotalELSubGroup;
                        if (TotalELSubGroup < 0)
                        {
                            TotalELSubGroup = TotalELSubGroup * -1;
                            //dgvReport.Rows[AssetRow - 2].Cells["Amount3"].Value = "(" + TotalELSubGroup.ToString("N2") + ")";
                            //dgvReport.Rows[AssetRow - 2].Cells["GroupId2"].Value = rw["ID"].ToString();
                            amt33 = "(" + TotalELSubGroup.ToString("N2") + ")";
                            grp2 = rw["ID"].ToString();
                            output.Add(new BalanceSheetResponseVM
                            {
                                Amount3 = amt33,
                                GroupId2 = grp2
                            });
                        }

                        //output.Add(new BalanceSheetResponseVM {
                        //    Amount3=amt33,
                        //    GroupId2=grp2
                        //});
                    }
                }

                output.Add(new BalanceSheetResponseVM
                {
                    Amount3 = "________________"
                });
                AssetRow++;

                // -------------------- Get total equities and liabilities ---------------------------------------------------- //
                //AssetRow++;

                string asset = "Total Equity and Liabilities".ToUpper();
                string amt3 = "";
                if (TotalEquitiesAndLiabilities > 0)
                {
                    amt3 = (TotalEquitiesAndLiabilities).ToString("N2");
                }
                else
                {
                    amt3 = "(" + (TotalEquitiesAndLiabilities * -1).ToString("N2") + ")";
                }

                decimal decPrintOrNot1 = TotalEquitiesAndLiabilities;

                output.Add(new BalanceSheetResponseVM
                {
                    TxtAsset = asset,
                    Amount3 = amt3
                });

                AssetRow++;
                output.Add(new BalanceSheetResponseVM
                {
                    Amount3 = "_______________"
                });
                AssetRow++;

                //---------------------Profit And Loss---------------------------------------------------------------------------------------------------------------
                response.ResponseCode = 200;
                response.Response = output;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.Response = ex.Message;
            }

            return response;
        }

        [HttpPost]
        public MatResponse GetProfitAndLossByConventional(ProfitAndLossParam input)
        {
            MatResponse response = new MatResponse();
            List<ProfitAndLossResponseVM> profitAndLoss = new List<ProfitAndLossResponseVM>();

            try
            {
                decimal TotalRevenue = 0;
                decimal TotalOpeningStockAndExpenses = 0;
                decimal GrossProfitOrLoss = 0;
                decimal OperatingExpenses = 0;
                decimal OperatingProfitOrLos = 0;
                decimal dcOpeningStock = 0;
                decimal dcClosingStock = 0;
                decimal dcOpeningStockForRollOver = 0;
                decimal currentStockValue = 0;
                string calculationMethod = "";

                GeneralQueries methodForStockValue = new GeneralQueries();
                DBMatConnection conn = new DBMatConnection();

                CurrencyInfo InfoCurrency = new CurrencyInfo();
                CurrencySP SpCurrency = new CurrencySP();
                InfoCurrency = SpCurrency.CurrencyView(1);
                int inDecimalPlaces = InfoCurrency.NoOfDecimalPlaces;

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DataTable dtblFinancial = new DataTable();
                DataSet DsetProfitAndLoss = new DataSet();
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                //---------check  calculation method
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }

                //dcOpeningStock = spFinancial.StockValueGetOnDate(PublicVariables._dtFromDate, DateTime.Parse(txtToDate.Text), calculationMethod, true, false);
                //dcOpeningStockForRollOver = spFinancial.StockValueGetOnDateForRollOver(DateTime.Parse(PublicVariables._dtToDate.ToString()), calculationMethod, false, false, true);
                //currentStockValue = methodForStockValue.currentStockValue(Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtToDate.Text), false, 0, false, DateTime.Now);
                string queryStr =
                    string.Format(
                        "select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ",
                        MATFinancials.PublicVariables._dtToDate.ToString());
                string queryStr2 =
                    string.Format(
                        "select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ",
                        MATFinancials.PublicVariables._dtFromDate.ToString());
                //DateTime lastFinYearEnd = DateTime.Parse(txtFromDate.Text); // conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty
                    ? Convert.ToDateTime(conn.getSingleValue(queryStr2))
                    : MATFinancials.PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty
                    ? Convert.ToDateTime(conn.getSingleValue(queryStr))
                    : MATFinancials.PublicVariables._dtFromDate.AddDays(-1);
                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    lastFinYearEnd = Convert.ToDateTime(input.FromDate).AddDays(-1);
                }

                DsetProfitAndLoss =
                    spFinancial.ProfitAndLossAnalysis(DateTime.Parse(input.FromDate), DateTime.Parse(input.ToDate));
                dcOpeningStock = spFinancial.StockValueGetOnDate(DateTime.Parse(input.FromDate),
                    DateTime.Parse(input.ToDate), calculationMethod, true, false);
                dcOpeningStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd,
                    false, 0, false, DateTime.Now);
                dcOpeningStock = dcOpeningStock + dcOpeningStockForRollOver;
                currentStockValue = methodForStockValue.currentStockValue(lastFinYearStart,
                    Convert.ToDateTime(input.ToDate), false, 0, false, DateTime.Now); //this should work instaed   //

                // ---------------------------------- REVENUE ----------------------------------//
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "REVENUE"
                });
                //---Sales Account  -Credit
                dtblFinancial = new DataTable();
                dtblFinancial = DsetProfitAndLoss.Tables[1];

                decimal dcSalesAccount = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = decimal.Parse(rw["Credit"].ToString().ToString());
                        dcSalesAccount += dcBalance;
                        TotalRevenue += dcBalance;
                    }
                }

                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "Sales Accounts",
                    TxtGroupId2 = "10",
                    TxtAmount2 = dcSalesAccount < 0
                        ? "(" + (dcSalesAccount * -1).ToString("N2") + ")"
                        : dcSalesAccount.ToString("N2")
                });
                //----Direct Income 
                dtblFinancial = new DataTable();
                dtblFinancial = DsetProfitAndLoss.Tables[3];

                decimal dcDirectIncoome = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = Convert.ToDecimal(rw["Credit"].ToString());
                        dcDirectIncoome += dcBalance;
                        TotalRevenue += dcBalance;
                    }
                }

                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "Direct Incomes",
                    TxtGroupId2 = "12",
                    TxtAmount2 = dcDirectIncoome.ToString()
                });
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount2 = "__________________"
                });

                // ---------------------------------- Total Revenue ------------------------------------------------------------ //
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "TOTAL REVENUE",
                    TxtAmount2 = TotalRevenue < 0
                        ? "(" + (TotalRevenue * -1).ToString("N2")
                        : TotalRevenue.ToString("N2")
                });

                // ---------------------------------- COST OF SALES ----------------------------------//
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "COST OF SALES"
                });
                //---- Opening Stock
                var openStock = GetStock(Convert.ToDateTime(input.ToDate));
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "Opening Stock",
                    TxtAmount1 = Math.Round(openStock, inDecimalPlaces).ToString("N2")
                });
                //if (dcOpeningStock >= 0)
                //{
                //    profitAndLoss.Add(new ProfitAndLossResponseVM
                //    {
                //        TxtIncome = "Opening Stock",
                //        TxtAmount1= Math.Round(dcOpeningStock, inDecimalPlaces).ToString("N2")
                //    });
                //}
                //else
                //{
                //    profitAndLoss.Add(new ProfitAndLossResponseVM
                //    {
                //        TxtIncome = "Opening Stock",
                //        TxtAmount1 = (Math.Round(dcOpeningStock * -1, inDecimalPlaces)).ToString("N2")
                //    });
                //}
                TotalOpeningStockAndExpenses += openStock;
                /// ---Purchases - Debit
                dtblFinancial = new DataTable();
                dtblFinancial = DsetProfitAndLoss.Tables[0];
                decimal dcPurchaseAccount = 0m;
                string grpId = "";
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = decimal.Parse(rw["Debit"].ToString().ToString());
                        dcPurchaseAccount += dcBalance;
                    }

                    grpId = "11";
                }

                TotalOpeningStockAndExpenses += dcPurchaseAccount;
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "Purchases",
                    TxtAmount1 = dcPurchaseAccount < 0
                        ? "(" + (dcPurchaseAccount * -1).ToString("N2")
                        : dcPurchaseAccount.ToString("N2"),
                    TxtGroupId1 = grpId
                });
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount1 = "_____________________"
                });
                // ----------------------------- Sum opening stock and purchases -------------------------------------------------//
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount1 = TotalOpeningStockAndExpenses < 0
                        ? "(" + (TotalOpeningStockAndExpenses * -1).ToString("N2") + ")"
                        : TotalOpeningStockAndExpenses.ToString("N2")
                });
                decimal PlusDirectExpenses = TotalOpeningStockAndExpenses;
                //---Direct Expense
                dtblFinancial = new DataTable();
                dtblFinancial = DsetProfitAndLoss.Tables[2];
                decimal dcDirectExpense = 0m;
                string grp2 = "";
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = Convert.ToDecimal(rw["Debit"].ToString());
                        dcDirectExpense += dcBalance;
                        PlusDirectExpenses += dcBalance;
                    }

                    grp2 = "13";
                }

                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtGroupId1 = grp2,
                    TxtIncome = "Direct Expenses",
                    TxtAmount1 = dcDirectExpense < 0
                        ? "(" + (dcDirectExpense * -1).ToString("N2") + ")"
                        : dcDirectExpense.ToString("N2")
                });
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount1 = "____________________"
                });

                // ------------------------ add direct expenses to opening stock and purchases -----------------------------------------------//
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount1 = PlusDirectExpenses < 0
                        ? "(" + (PlusDirectExpenses * -1).ToString("N2") + ")"
                        : PlusDirectExpenses.ToString("N2")
                });

                //Closing Stock 
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "Closing Stock",
                    TxtAmount1 = currentStockValue > 0
                        ? currentStockValue.ToString("N2")
                        : "(" + (currentStockValue * -1).ToString("N2") +
                          ")", // ClosingStock < 0 ? "(" + (ClosingStock * -1).ToString("N2") + ")" : "(" + ClosingStock.ToString("N2") + ")"; commented on 20170117,
                    TxtAmount2 = (PlusDirectExpenses - currentStockValue) > 0
                        ? (PlusDirectExpenses - currentStockValue).ToString("N2")
                        : "(" + ((PlusDirectExpenses - currentStockValue) * -1).ToString("N2") +
                          ")", //ClosingStockPlusDirectExpenses < 0 ? "(" + (ClosingStockPlusDirectExpenses * -1).ToString("N2") + ")" : ClosingStockPlusDirectExpenses.ToString("N2");       commented on 20170117
                });

                //if (currentStockValue < 0)
                //{
                //    dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Style.ForeColor = Color.Red;
                //}
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount1 = "___________________",
                    TxtAmount2 = "___________________"
                });

                // ------------------------------- Gross profit or loss --------------------------------------------------- //
                //GrossProfitOrLoss = TotalRevenue - (PlusDirectExpenses - dcClosingStock);
                GrossProfitOrLoss = TotalRevenue - (PlusDirectExpenses - currentStockValue);

                if (GrossProfitOrLoss < 0)
                {
                    //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Style.ForeColor = Color.Red;
                }

                decimal decgranIncTotal = GrossProfitOrLoss;
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "GROSS PROFIT/(LOSS)",
                    TxtAmount2 = GrossProfitOrLoss < 0
                        ? "(" + (GrossProfitOrLoss * -1).ToString("N2")
                        : GrossProfitOrLoss.ToString("N2")
                });
                ///---Other Income
                dtblFinancial = new DataTable();
                dtblFinancial = DsetProfitAndLoss.Tables[5];

                decimal dcIndirectIncome = 0m;
                string grp3 = "";
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = Convert.ToDecimal(rw["Credit"].ToString());
                        dcIndirectIncome += dcBalance;
                    }

                    grp3 = "14";
                }

                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "Other Income",
                    TxtGroupId2 = grp3,
                    TxtAmount2 = dcIndirectIncome < 0
                        ? "(" + (dcIndirectIncome * -1).ToString("N2") + ")"
                        : dcIndirectIncome.ToString("N2")
                });

                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount2 = "___________________"
                });
                // --------------------------------------------- sum other income with gross profit (loss) ------------------------------- //

                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount2 = (dcIndirectIncome + GrossProfitOrLoss) < 0
                        ? "(" + (dcIndirectIncome + GrossProfitOrLoss * -1).ToString("N2") + ")"
                        : (dcIndirectIncome + GrossProfitOrLoss).ToString("N2")
                });
                OperatingProfitOrLos = OperatingProfitOrLos + (dcIndirectIncome + GrossProfitOrLoss);

                // ----------------------------------------------- Operating Expenses ------------------------------------------- //
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "OPERATING EXPENSES"
                });
                ///------Indirect Expense 
                dtblFinancial = new DataTable();
                dtblFinancial = DsetProfitAndLoss.Tables[4];

                decimal dcIndirectExpense = 0;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal dcBalance = Convert.ToDecimal(rw["Debit"].ToString());
                        dcIndirectExpense += dcBalance;
                    }

                    profitAndLoss.Add(new ProfitAndLossResponseVM
                    {
                        TxtIncome = "Indirect Expenses",
                        TxtGroupId1 = "15",
                        TxtAmount1 = dcIndirectExpense < 0
                            ? "(" + (dcIndirectExpense * -1).ToString("N2")
                            : dcIndirectExpense.ToString("N2")
                    });
                    OperatingExpenses += dcIndirectExpense;
                }

                // ----------------------------------------- Total operating expenses ------------------------------------- //
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "Indirect Expenses",
                    TxtAmount2 = OperatingExpenses < 0
                        ? "(" + (OperatingExpenses * -1).ToString("N2")
                        : OperatingExpenses.ToString("N2")
                });
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount1 = "__________________",
                    TxtAmount2 = "__________________"
                });
                // ------------------------------------------ Operating profit (loss) ----------------------------------- //

                OperatingProfitOrLos = OperatingProfitOrLos - OperatingExpenses;
                if (OperatingProfitOrLos < 0)
                {
                    //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Style.ForeColor = Color.Red;
                }

                decimal decgranExTotal = OperatingProfitOrLos;
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "OPERATING PROFIT (LOSS)",
                    TxtAmount2 = OperatingProfitOrLos < 0
                        ? "(" + (OperatingProfitOrLos * -1).ToString("N2") + ")"
                        : OperatingProfitOrLos.ToString("N2")
                });
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount2 = "______________"
                });
                //---- Calculating Grand total

                response.ResponseCode = 200;
                response.Response = profitAndLoss;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = ex.Message.ToString();
            }

            return response;
        }

        [HttpPost]
        public MatResponse GetProfitAndLossByStandard(ProfitAndLossParam input)
        {
            MatResponse response = new MatResponse();
            List<ProfitAndLossResponseVM> profitAndLoss = new List<ProfitAndLossResponseVM>();

            try
            {
                decimal TotalRevenue = 0;
                decimal totalCostofSales = 0;
                decimal TotalOpeningStockAndExpenses = 0;
                decimal GrossProfitOrLoss = 0;
                decimal OperatingExpenses = 0;
                decimal OperatingProfitOrLos = 0;
                decimal dcOpeningStockForRollOver = 0;
                decimal dcOpeningStock = 0;

                decimal decgranExTotal = 0;
                decimal decgranIncTotal = 0;
                CurrencyInfo InfoCurrency = new CurrencyInfo();
                CurrencySP SpCurrency = new CurrencySP();
                InfoCurrency = SpCurrency.CurrencyView(1);
                int inDecimalPlaces = InfoCurrency.NoOfDecimalPlaces;

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DataTable dtblFinancial = new DataTable();
                DataSet DsetProfitAndLoss = new DataSet();
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                GeneralQueries methodForStockValue = new GeneralQueries();
                DBMatConnection conn = new DBMatConnection();
                string calculationMethod = "";
                //---------check  calculation method
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }

                DsetProfitAndLoss = spFinancial.ProfitAndLossAnalysisByCostOfSales(DateTime.Parse(input.FromDate),
                    DateTime.Parse(input.ToDate));
                DataTable dtOpeningStock = new DataTable();
                DataTable dtOpeningStockForRollOver = new DataTable();
                dtOpeningStock = spFinancial.StockValueOnDateByAVCOForOpeningStockByCostOfSales(
                    DateTime.Parse(input.FromDate), DateTime.Parse(input.ToDate), calculationMethod, true, false,
                    false);
                dtOpeningStockForRollOver = spFinancial.StockValueOnDateByAVCOForOpeningStockByCostOfSales(
                    DateTime.Parse(input.FromDate), DateTime.Parse(input.ToDate), calculationMethod, true, false, true);

                if (dtOpeningStock != null && dtOpeningStock.Rows.Count > 0 && dtOpeningStockForRollOver != null &&
                    dtOpeningStockForRollOver.Rows.Count > 0)
                {
                    var rollOverList = (from i in dtOpeningStock.AsEnumerable()
                        join j in dtOpeningStockForRollOver.AsEnumerable() on i.Field<decimal>("ledgerId") equals
                            j.Field<decimal>("ledgerId") //into newTable
                        select new
                        {
                            ledgerId = j.Field<decimal>("ledgerId"),
                            ledgerName = j.Field<string>("ledgerName"),
                            stockValue = j.Field<decimal>("stockValue") + i.Field<decimal>("stockValue")

                        }).ToList();

                    dtOpeningStock.Rows.Clear();
                    foreach (var item in rollOverList)
                    {
                        DataRow dr = dtOpeningStock.NewRow();
                        dr["ledgerId"] = item.ledgerId;
                        dr["ledgerName"] = item.ledgerName;
                        dr["stockValue"] = item.stockValue;
                        dtOpeningStock.Rows.Add(dr);
                    }

                }

                else if ((dtOpeningStock == null || dtOpeningStock.Rows.Count < 1) &&
                         (dtOpeningStockForRollOver != null || dtOpeningStockForRollOver.Rows.Count > 0))
                {
                    //dtOpeningStock = dtOpeningStockForRollOver; since we now use the stock formular in GeneralQueries.CurrentStock
                }

                // ---------------------------------- REVENUE ----------------------------------//
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "REVENUE",

                });

                //---Sales Account  -Credit
                dtblFinancial = new DataTable();
                //dtblFinancial = DsetProfitAndLoss.Tables[1];
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;

                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding; 
                        profitAndLoss.Add(new ProfitAndLossResponseVM
                        {
                            TxtIncome = rw["ledgerName"].ToString().ToUpper(),
                            TxtAmount2 = Convert.ToDecimal(rw["Credit"]).ToString("N2"),

                        });
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = rw["ledgerName"].ToString().ToUpper();
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = Convert.ToDecimal(rw["Credit"]).ToString("N2");
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtLedgerId"].Value = Convert.ToDecimal(rw["ledgerId"]).ToString();
                        TotalRevenue += Convert.ToDecimal(rw["Credit"]);
                        //dgvProfitAndLoss.Rows.Add();
                    }

                    profitAndLoss.Add(new ProfitAndLossResponseVM
                    {
                        TxtGroupId1 = "11"
                    });
                    //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "11";
                }

                //----Direct Income 
                dtblFinancial = new DataTable();
                dtblFinancial = DsetProfitAndLoss.Tables[3];
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        // dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding; 
                        profitAndLoss.Add(new ProfitAndLossResponseVM
                        {
                            TxtIncome = rw["ledgerName"].ToString().ToUpper(),
                            TxtAmount2 = Convert.ToDecimal(rw["Credit"]).ToString("N2"),

                        });
                        //  dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = rw["ledgerName"].ToString().ToUpper();
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = Convert.ToDecimal(rw["Credit"]).ToString("N2");
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtLedgerId"].Value = Convert.ToDecimal(rw["ledgerId"]).ToString();
                        TotalRevenue += Convert.ToDecimal(rw["Credit"]);
                        //   dgvProfitAndLoss.Rows.Add();
                    }

                    profitAndLoss.Add(new ProfitAndLossResponseVM
                    {
                        TxtGroupId1 = "11"
                    });
                    //  dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "11";
                }

                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount2 = "______________"
                });
                // ---------------------------------- Total Revenue ------------------------------------------------------------ //
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "TOTAL REVENUE",
                    TxtAmount2 = TotalRevenue < 0
                        ? "(" + (TotalRevenue * -1).ToString("N2")
                        : TotalRevenue.ToString("N2")
                });

                // dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "TOTAL REVENUE";
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = TotalRevenue < 0 ? "(" + (TotalRevenue * -1).ToString("N2") : TotalRevenue.ToString("N2");

                // ---------------------------------- COST OF SALES ----------------------------------//
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "COST OF SALES",
                    //TxtAmount2 = TotalRevenue < 0 ? "(" + (TotalRevenue * -1).ToString("N2") : TotalRevenue.ToString("N2")
                });
                //dgvProfitAndLoss.Rows.Add();
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "COST OF SALES";
                //dgvProfitAndLoss.Rows.Add();
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;

                ///// ---Purchases (cost of sales) - Debit
                dtblFinancial = new DataTable();

                dtblFinancial = DsetProfitAndLoss.Tables[0];
                DataTable dtClosingStock = spFinancial.StockValueGetOnDateByCostOfSales(DateTime.Parse(input.ToDate),
                    calculationMethod, false, false);
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;

                // check for opening and closing stock without sales
                List<decimal> ledgersNotInLedgerPosting = new List<decimal>();
                List<decimal> ledgersinLedgerPosting = new List<decimal>();
                if (dtClosingStock.Rows.Count > 0)
                {
                    for (int i = 0; i < dtClosingStock.Rows.Count; i++)
                    {
                        decimal ledgerId = Convert.ToDecimal((dtClosingStock.Rows[i]["ledgerId"]).ToString());
                        ledgersNotInLedgerPosting.Add(ledgerId);
                    }
                }

                if (dtblFinancial.Rows.Count > 0)
                {
                    for (int i = 0; i < dtblFinancial.Rows.Count; i++)
                    {
                        decimal ledgerId = Convert.ToDecimal((dtblFinancial.Rows[i]["ledgerId"]).ToString());
                        ledgersinLedgerPosting.Add(ledgerId);
                    }
                }

                List<decimal> unCommonLedgers = ledgersNotInLedgerPosting.Except(ledgersinLedgerPosting).ToList();

                string queryStr =
                    string.Format(
                        "select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ",
                        PublicVariables._dtToDate.ToString());
                string queryStr2 =
                    string.Format(
                        "select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ",
                        PublicVariables._dtFromDate.ToString());
                DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty
                    ? Convert.ToDateTime(conn.getSingleValue(queryStr))
                    : PublicVariables._dtFromDate.AddDays(-1);
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty
                    ? Convert.ToDateTime(conn.getSingleValue(queryStr2))
                    : PublicVariables._dtFromDate;
                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    lastFinYearEnd = Convert.ToDateTime(input.FromDate).AddDays(-1);
                }

                decimal dcPurchaseAccount = 0m;
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        decimal ledgerId = Convert.ToDecimal(rw["ledgerId"].ToString());
                        decimal openingStock = (from d in dtOpeningStock.AsEnumerable()
                            where d.Field<decimal>("ledgerId") == ledgerId
                            select d.Field<decimal>("stockValue")).FirstOrDefault();

                        decimal openingStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart,
                            lastFinYearEnd, true, ledgerId, false,
                            DateTime.Now); // new opening stock method by average cost 20170120
                        openingStock = openingStock + openingStockForRollOver;
                        //decimal closingStock = (from e in dtClosingStock.AsEnumerable()
                        //                        where e.Field<decimal>("ledgerId") == ledgerId
                        //                        select e.Field<decimal>("stockValue")).Sum();

                        decimal closingStock = methodForStockValue.currentStockValue(lastFinYearStart,
                            Convert.ToDateTime(input.ToDate), true, ledgerId, false,
                            lastFinYearEnd); //this should work instaed   //

                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                        decimal dcBalance = decimal.Parse(rw["Debit"].ToString().ToString());
                        dcPurchaseAccount += dcBalance;
                        decimal costOfSales = Convert.ToDecimal(rw["Debit"]) + openingStock - closingStock;
                        profitAndLoss.Add(new ProfitAndLossResponseVM
                        {
                            TxtIncome = rw["ledgerName"].ToString().ToUpper(),
                            TxtAmount1 = costOfSales > 0
                                ? costOfSales.ToString("N2")
                                : "(" + (costOfSales * -1).ToString("N2") + ")"
                            //TxtAmount2 = TotalRevenue < 0 ? "(" + (TotalRevenue * -1).ToString("N2") : TotalRevenue.ToString("N2")
                        });
                        totalCostofSales += Convert.ToDecimal(rw["Debit"]) + openingStock - closingStock;

                    }
                }

                if (unCommonLedgers.Any())
                {
                    for (int j = 0; j < unCommonLedgers.Count; j++)
                    {
                        //decimal value = Convert.ToDecimal(dtClosingStock.AsEnumerable().Where(d => d.Field<decimal>("ledgerId") == Convert.ToDecimal(unCommonLedgers[j].ToString())).Select(e => e.Field<decimal>("ledgerId")).ToString());
                        decimal openingStock = (from f in dtOpeningStock.AsEnumerable()
                            where f.Field<decimal>("ledgerId") == Convert.ToDecimal(unCommonLedgers[j].ToString())
                            select f.Field<decimal>("stockValue")).FirstOrDefault();

                        decimal openingStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart,
                            lastFinYearEnd, true, Convert.ToDecimal(unCommonLedgers[j].ToString()), false,
                            DateTime.Now); // new opening stock method by average cost 20170120
                        openingStock = openingStock + openingStockForRollOver;
                        //decimal closingStock = (from g in dtClosingStock.AsEnumerable()
                        //                        where g.Field<decimal>("ledgerId") == Convert.ToDecimal(unCommonLedgers[j].ToString())
                        //                        select g.Field<decimal>("stockValue")).FirstOrDefault();

                        decimal closingStock = methodForStockValue.currentStockValue(lastFinYearStart,
                            Convert.ToDateTime(input.ToDate), true, Convert.ToDecimal(unCommonLedgers[j].ToString()),
                            false, DateTime.Now); // new closing stock method by average cost 20170120

                        string ledgerName = (from g in dtClosingStock.AsEnumerable()
                            where g.Field<decimal>("ledgerId") == Convert.ToDecimal(unCommonLedgers[j].ToString())
                            select g.Field<string>("ledgerName")).FirstOrDefault();
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding;
                        decimal dcBalance = decimal.Parse(dtClosingStock.Rows[j]["stockValue"].ToString().ToString());
                        dcPurchaseAccount += dcBalance;
                        profitAndLoss.Add(new ProfitAndLossResponseVM
                        {
                            TxtIncome = ledgerName.ToUpper(),
                            TxtAmount1 = (openingStock - closingStock).ToString("N2")
                            //TxtAmount2 = TotalRevenue < 0 ? "(" + (TotalRevenue * -1).ToString("N2") : TotalRevenue.ToString("N2")
                        });
                        totalCostofSales += (openingStock - closingStock);

                    }
                }

                decimal PlusDirectExpenses = TotalOpeningStockAndExpenses;
                ////---Direct Expense
                dtblFinancial = new DataTable();
                dtblFinancial = DsetProfitAndLoss.Tables[2];
                if (dtblFinancial.Rows.Count > 0)
                {
                    decimal directExpenses = 0;
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        directExpenses += Convert.ToDecimal(rw["Debit"]);
                        totalCostofSales += Convert.ToDecimal(rw["Debit"]);
                    }

                    if (directExpenses != 0)
                    {

                        profitAndLoss.Add(new ProfitAndLossResponseVM
                        {
                            TxtIncome = "DIRECT EXPENSES",
                            TxtAmount1 = directExpenses.ToString("N2")
                            //TxtAmount2 = TotalRevenue < 0 ? "(" + (TotalRevenue * -1).ToString("N2") : TotalRevenue.ToString("N2")
                        });
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "DIRECT EXPENSES";
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = directExpenses.ToString("N2");
                        ////dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtledgerId"].Value = Convert.ToDecimal(rw["ledgerId"]).ToString();
                        //dgvProfitAndLoss.Rows.Add();
                    }

                    profitAndLoss.Add(new ProfitAndLossResponseVM
                    {
                        TxtGroupId1 = "11"
                        //TxtAmount2 = TotalRevenue < 0 ? "(" + (TotalRevenue * -1).ToString("N2") : TotalRevenue.ToString("N2")
                    });
                    //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "11";
                }

                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount2 = totalCostofSales > 0
                        ? totalCostofSales.ToString("N2")
                        : "(" + (totalCostofSales * -1).ToString("N2") + ")",

                    //TxtAmount2 = TotalRevenue < 0 ? "(" + (TotalRevenue * -1).ToString("N2") : TotalRevenue.ToString("N2")
                });
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 2].Cells["dgvtxtAmount2"].Value = totalCostofSales > 0 ? totalCostofSales.ToString("N2") : "(" + (totalCostofSales * -1).ToString("N2") + ")";
                //dgvProfitAndLoss.Rows.Add();
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount2 = "______________",
                    TxtGroupId1 = "11"

                    //TxtAmount2 = TotalRevenue < 0 ? "(" + (TotalRevenue * -1).ToString("N2") : TotalRevenue.ToString("N2")
                });
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 2].Cells["dgvtxtAmount2"].Value = "______________";
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 2].Cells["dgvtxtGroupId1"].Value = "11";

                // ------------------------------- Gross profit or loss --------------------------------------------------- //
                GrossProfitOrLoss = (TotalRevenue - totalCostofSales);
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "GROSS PROFIT/(LOSS)",
                    TxtAmount2 = GrossProfitOrLoss < 0
                        ? "(" + (GrossProfitOrLoss * -1).ToString("N2")
                        : GrossProfitOrLoss.ToString("N2"),
                    //TxtAmount2 = TotalRevenue < 0 ? "(" + (TotalRevenue * -1).ToString("N2") : TotalRevenue.ToString("N2")
                });
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "GROSS PROFIT/(LOSS)";
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = GrossProfitOrLoss < 0 ? "(" + (GrossProfitOrLoss * -1).ToString("N2") : GrossProfitOrLoss.ToString("N2");

                decgranIncTotal = GrossProfitOrLoss;

                ///---Other Income 
                //dgvProfitAndLoss.Rows.Add();
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "OTHER INCOME",
                    //TxtAmount2 = TotalRevenue < 0 ? "(" + (TotalRevenue * -1).ToString("N2") : TotalRevenue.ToString("N2")
                });

                dtblFinancial = new DataTable();
                dtblFinancial = DsetProfitAndLoss.Tables[5];
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        profitAndLoss.Add(new ProfitAndLossResponseVM
                        {
                            TxtIncome = rw["ledgerName"].ToString().ToUpper(),
                            TxtAmount2 = Convert.ToDecimal(rw["Credit"]).ToString("N2")
                        });

                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = rw["ledgerName"].ToString().ToUpper();
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = Convert.ToDecimal(rw["Credit"]).ToString("N2");
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtledgerId"].Value = Convert.ToDecimal(rw["ledgerId"]).ToString();
                        GrossProfitOrLoss += Convert.ToDecimal(rw["Credit"]);

                    }

                    profitAndLoss.Add(new ProfitAndLossResponseVM
                    {

                        TxtAmount2 = "______________",
                        TxtGroupId1 = "11"
                    });
                    //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";
                    //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "11";
                    //// --------------------------------------------- sum other income with gross profit (loss) ------------------------------- //
                    //dgvProfitAndLoss.Rows.Add(); 
                    profitAndLoss.Add(new ProfitAndLossResponseVM
                    {

                        TxtAmount2 = GrossProfitOrLoss.ToString("N2"),

                    });
                    //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = GrossProfitOrLoss.ToString("N2");
                }

                // ----------------------------------------------- Operating Expenses ------------------------------------------- //
                //dgvProfitAndLoss.Rows.Add(); 
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "OPERATING EXPENSES",

                });
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "OPERATING EXPENSES";

                ///------Indirect Expense 
                dtblFinancial = new DataTable();
                dtblFinancial = DsetProfitAndLoss.Tables[4];
                //dgvProfitAndLoss.Rows.Add();
                if (dtblFinancial.Rows.Count > 0)
                {
                    foreach (DataRow rw in dtblFinancial.Rows)
                    {
                        // dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Padding = newPadding; 
                        profitAndLoss.Add(new ProfitAndLossResponseVM
                        {
                            TxtIncome = rw["ledgerName"].ToString().ToUpper(),
                            TxtAmount1 = Convert.ToDecimal(rw["Debit"]) > 0
                                ? Convert.ToDecimal(rw["Debit"]).ToString("N2")
                                : "(" + (Convert.ToDecimal(rw["Debit"]) * -1).ToString("N2") + ")"
                        });
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = rw["ledgerName"].ToString().ToUpper();
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtledgerId"].Value = rw["ledgerId"].ToString();
                        //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = Convert.ToDecimal(rw["Debit"]) > 0 ?
                        //  Convert.ToDecimal(rw["Debit"]).ToString("N2") : "(" + (Convert.ToDecimal(rw["Debit"]) * -1).ToString("N2") + ")";
                        OperatingExpenses += Convert.ToDecimal(rw["Debit"]);
                        //dgvProfitAndLoss.Rows.Add();
                    }

                    profitAndLoss.Add(new ProfitAndLossResponseVM
                    {
                        TxtGroupId1 = "11"

                    });
                    //   dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtGroupId1"].Value = "11";
                }

                // ----------------------------------------- Total operating expenses ------------------------------------- //
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount2 = OperatingExpenses < 0
                        ? "(" + (OperatingExpenses * -1).ToString("N2")
                        : OperatingExpenses.ToString("N2"),
                    TxtAmount1 = "______________",


                });
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount2 = "______________",
                });
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 2].Cells["dgvtxtAmount2"].Value = OperatingExpenses < 0 ? "(" + (OperatingExpenses * -1).ToString("N2") : OperatingExpenses.ToString("N2");

                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount1"].Value = "______________";
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";
                //// ------------------------------------------ Operating profit (loss) ----------------------------------- //
                // dgvProfitAndLoss.Rows.Add();
                OperatingProfitOrLos = GrossProfitOrLoss - OperatingExpenses;

                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.BackColor = Color.SkyBlue;
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].DefaultCellStyle.Font = newFont;
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtIncome = "OPERATING PROFIT (LOSS)",
                    TxtAmount2 = OperatingProfitOrLos < 0
                        ? "(" + (OperatingProfitOrLos * -1).ToString("N2")
                        : OperatingProfitOrLos.ToString("N2"),
                });
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtIncome"].Value = "OPERATING PROFIT (LOSS)";
                //dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = OperatingProfitOrLos < 0 ?
                //    "(" + (OperatingProfitOrLos * -1).ToString("N2") : OperatingProfitOrLos.ToString("N2");

                decgranExTotal = OperatingProfitOrLos;

                //  dgvProfitAndLoss.Rows.Add();
                profitAndLoss.Add(new ProfitAndLossResponseVM
                {
                    TxtAmount2 = "______________",
                });
                // dgvProfitAndLoss.Rows[dgvProfitAndLoss.Rows.Count - 1].Cells["dgvtxtAmount2"].Value = "______________";
                //---- Calculating Grand total 
                response.ResponseCode = 200;
                response.Response = profitAndLoss;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = ex.Message.ToString();
                //   formMDI.infoError.ErrorString = "PAL1:" + ex.Message;
            }

            return response;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetTrialBalance(DateTime dateTime)
        {

            var isGoodInTrans = false;
            // get ledger posting records by grouping accountGroups, Ledgers 
            //dateTime = dateTime.AddDays(1).AddSeconds(-1);

            var HeadGroup = new TrailBalanceHeadModel();
            var Groups = new List<TrailBalanceModel>();
            var groups = new List<TrailBalanceTransModel>();

            var AccountGroups = await context.tbl_AccountGroup.ToListAsync();

            AccountGroups.ForEach(a =>
            {
                if (a != null)
                {
                    var Ledgers = context.tbl_AccountLedger.Where(b => b.accountGroupId == a.accountGroupId).OrderBy(o => o.ledgerName).ToList();

                    var newTrailMode = new TrailBalanceModel();

                    var newAcctGroup = new AccountGroup()
                    {
                        accountGroupId = a.accountGroupId,
                        accountGroupName = a.accountGroupName,
                        affectGrossProfit = a.affectGrossProfit,
                        extra1 = a.extra1,
                        extra2 = a.extra2,
                        extraDate = a.extraDate.Value,
                        groupUnder = a.groupUnder.Value,
                        isDefault = a.isDefault.Value,
                        narration = a.narration,
                        nature = a.nature,
                    };
                    newTrailMode.Group = newAcctGroup;

                    if (Ledgers!=null && Ledgers.Count > 0)
                    {
                        foreach(var itm in Ledgers)
                        {
                            var newAccountLedger = new AccountLedger()
                            {
                                accountGroupId = itm.accountGroupId.Value,
                                ledgerId = itm.ledgerId,
                                address = itm.address,
                                areaId = itm.areaId.Value,
                                bankAccountNumber = itm.bankAccountNumber,
                                billByBill = itm.billByBill.Value,
                                branchCode = itm.branchCode,
                                branchName = itm.branchName,
                                creditLimit = itm.creditLimit.Value,
                                creditPeriod = itm.creditPeriod.Value,
                                crOrDr = itm.crOrDr,
                                cst = itm.cst,
                                email = itm.email,
                                extra1 = itm.extra1,
                                extra2 = itm.extra2,
                                extraDate = itm.extraDate.Value,
                                isActive = itm.isActive.Value,
                                isDefault = itm.isDefault.Value,
                                ledgerName = itm.ledgerName,
                                mailingName = itm.mailingName,
                                mobile = itm.mobile,
                                narration = itm.narration,
                                openingBalance = itm.openingBalance.Value,
                                pan = itm.pan,
                                phone = itm.phone,
                                pricinglevelId = itm.pricinglevelId.Value,
                                routeId = itm.routeId.Value,
                                tin = itm.tin
                            };

                            newTrailMode.Ledgers.Add(newAccountLedger);
                        }
                    }

                    Groups.Add(newTrailMode);
                }
            });

            HeadGroup.HeadGroup.AddRange(Groups);

            Groups.ForEach(a =>
            {
                if (a != null)
                {
                    var newTrailMode = new TrailBalanceTransModel();
                    var newAccountGroup = new AccountGroup()
                    {
                        accountGroupId = a.Group.accountGroupId,
                        accountGroupName = a.Group.accountGroupName,
                        affectGrossProfit = a.Group.affectGrossProfit,
                        extra1 = a.Group.extra1,
                        extra2 = a.Group.extra2,
                        extraDate = a.Group.extraDate,
                        groupUnder = a.Group.groupUnder,
                        isDefault = a.Group.isDefault,
                        narration = a.Group.narration,
                        nature = a.Group.narration,
                    };
                    newTrailMode.Account = newAccountGroup;

                    if (a.Ledgers != null && a.Ledgers.Count > 0)
                    {
                        a.Ledgers.ForEach( l => {

                            if (l != null)
                            {
                                var newLedgers = new TrailBalanceTransactionModel();

                                var newAccountLedger = new AccountLedger()
                                {
                                    accountGroupId = l.accountGroupId,
                                    ledgerId = l.ledgerId,
                                    address = l.address,
                                    areaId = l.areaId,
                                    bankAccountNumber = l.bankAccountNumber,
                                    billByBill = l.billByBill,
                                    branchCode = l.branchCode,
                                    branchName = l.branchName,
                                    creditLimit = l.creditLimit,
                                    creditPeriod = l.creditPeriod,
                                    crOrDr = l.crOrDr,
                                    cst = l.cst,
                                    email = l.email,
                                    extra1 = l.extra1,
                                    extra2 = l.extra2,
                                    extraDate = l.extraDate,
                                    isActive = l.isActive,
                                    isDefault = l.isDefault,
                                    ledgerName = l.ledgerName,
                                    mailingName = l.mailingName,
                                    mobile = l.mobile,
                                    narration = l.narration,
                                    openingBalance = l.openingBalance,
                                    pan = l.pan,
                                    phone = l.phone,
                                    pricinglevelId = l.pricinglevelId,
                                    routeId = l.routeId,
                                    tin = l.tin
                                };
                                    
                                newLedgers.Ledger = newAccountLedger;

                                var Trans =  GetLedgerPosting1(l.ledgerId, PublicVariables._dtFromDate, dateTime);

                                if (l.ledgerId == 110434)
                                {

                                }

                                //                   var g = context.tbl_AccountLedger.FirstOrDefault(aq =>
                                //aq.ledgerName.ToLower() == "Goods in Transit".ToLower());
                                //                   if (g != null)
                                //                   {
                                //                        return g.ledgerId;
                                //                   }
                                //                    fix value initially created 
                                //                    return 110434;

                                var OutDeliveryNoteAndMatirial = Trans.Where(ot => ot.ledgerId.Value != 110434).ToList();
                                //&& ot.voucherTypeId == 11 && ot.voucherTypeId == 17

                                var GoodIntransit = Trans.Where(ot => ot.ledgerId.Value == 110434).ToList();

                                if (GoodIntransit.Count > 0)
                                {
                                    var amt = GetDeliveryNoteAndMatrialReciept(dateTime);
                                    if(amt > 0)
                                    {
                                        GoodIntransit[0].debit = Decimal.Parse(amt.ToString("0.00"));       
                                        //Math.Round((double)Convert.ToInt32(amt), 2); 
                                        GoodIntransit[0].credit = 0;
                                    }
                                    else
                                    {

                                        GoodIntransit[0].credit = Decimal.Parse(Convert.ToDecimal(amt.ToString().Replace("-", "")).ToString("0.00"));
                                        GoodIntransit[0].debit = 0;
                                    }

                                    OutDeliveryNoteAndMatirial.Add(GoodIntransit[0]);
                                }
                                else
                                {
                                    if (l.ledgerId == 110434 && isGoodInTrans == false)
                                    {
                                        var NewGoodsInTransit = new MatApi.DBModel.tbl_LedgerPosting()
                                        {
                                            ledgerPostingId =  11642,
                                            date = dateTime,
                                            voucherTypeId = 10,
                                            voucherNo = "67",
                                            ledgerId = l.ledgerId,
                                            debit = 0,
                                            credit = 0,
                                            detailsId = 0,
                                            yearId = 2019,
                                            invoiceNo = "67",
                                            chequeNo = "677",
                                            chequeDate = DateTime.Now,
                                            extraDate = DateTime.Now,
                                            extra1 = "",
                                            extra2 = ""
                                        };

                                        var amt = GetDeliveryNoteAndMatrialReciept(dateTime);
                                        if (amt > 0)
                                        {
                                            NewGoodsInTransit.debit = Decimal.Parse(amt.ToString("0.00"));
                                            //Math.Round((double)Convert.ToInt32(amt), 2); 
                                            NewGoodsInTransit.credit = 0;
                                        }
                                        else
                                        {

                                            NewGoodsInTransit.credit = Decimal.Parse(Convert.ToDecimal(amt.ToString().Replace("-", "")).ToString("0.00"));
                                            NewGoodsInTransit.debit = 0;
                                        }

                                        isGoodInTrans = true;
                                        OutDeliveryNoteAndMatirial.Add(NewGoodsInTransit);
                                    }
                                }

                                var LedgerPostings = TransactionLedger(OutDeliveryNoteAndMatirial);

                                foreach (var itmPosting in LedgerPostings)
                                {
                                    var newLedgerPosting = new LedgerPosting()
                                    {
                                        chequeDate = itmPosting.chequeDate.Value,
                                        chequeNo = itmPosting.chequeNo,
                                        credit = itmPosting.credit.Value,
                                        date = itmPosting.date.Value,
                                        debit = itmPosting.debit.Value,
                                        detailsId = itmPosting.detailsId.Value,
                                        extra1 = itmPosting.extra1,
                                        extra2 = itmPosting.extra2,
                                        extraDate = itmPosting.extraDate.Value,
                                        invoiceNo = itmPosting.invoiceNo,
                                        ledgerId = itmPosting.ledgerId.Value,
                                        ledgerPostingId = itmPosting.ledgerPostingId,

                                    };

                                    newLedgerPosting.tbl_AccountLedger = newAccountLedger;

                                    newLedgers.Transactions.Add(newLedgerPosting);
                                }

                                newTrailMode.Ledgers.Add(newLedgers);
                            }

                        });
                    }

                    groups.Add(newTrailMode);
                }
            });

            HeadGroup.HeadLedgers.AddRange(groups);

            //var groups = context.tbl_AccountGroup.OrderBy(g => g.accountGroupName).ToList().Select(a => new
            //    {
            //        Group = a,
            //        Ledgers = context.tbl_AccountLedger.Where(b => b.accountGroupId == a.accountGroupId)
            //            .OrderBy(o => o.ledgerName).ToList()
            //    })
            //    .Select(a => new
            //    {
            //        Account = a.Group,
            //        Ledgers = a.Ledgers.Select(c => new
            //        {
            //            Ledger = c,
            //            Transactions = TransactionLedger(GetLedgerPosting(c.ledgerId, dateTime))

            //        }),
            //    });

          //  var json = JsonConvert.SerializeObject(groups);

            //  var openingStock =  new FinicialServcies().GetOpeningStock(dateTime);
            //var inventory = openingStock + new FinicialServcies().GetTotalStockCost(dateTime); 
            //   var inventory = await (new FinicialServcies().GetClosingStock(PublicVariables._dtFromDate, dateTime));

            //   inventory = new FinicialServcies().GetRemainingStock(PublicVariables._dtFromDate, dateTime);
            decimal inventory = 0;

            var AskOpenStock = GetAllStockDetails(new StockReportSearch() { BatchNo = "All", CategoryId = 0, ProductCode = "", ProductId = 0, ProjectId = 0, RefNo = "", StoreId = 0, FromDate = PublicVariables._dtFromDate, ToDate = dateTime });
            //var AskOpenStock = GetOpeningStockers(new StockReportSearch() { BatchNo = "All", CategoryId = 0, ProductCode = "", ProductId = 0, ProjectId = 0, RefNo = "", StoreId = 0, FromDate = PublicVariables._dtFromDate, ToDate = dateTime });

            //if (AskOpenStock != null)
            //{
            //    inventory = AskOpenStock.totalStockValue;
            //}
            if(AskOpenStock.Count() > 0 && AskOpenStock.ContainsKey("TotalStockValue"))
            {
                inventory = Convert.ToDecimal(AskOpenStock["TotalStockValue"].ToString("#.##"));
            }
            var stock = new
            {
                accountName = "Stock",
                credit = 0.0,
                debit = inventory
            };

            return Ok(new
            {
                stock = stock,
                groups = groups
            });
        }

        public decimal GetDeliveryNoteAndMatrialReciept(DateTime Todate)
        {
            try
            {

                var listDeliveryNotes = new List<tbl_DeliveryNoteDetails>();
                decimal DeliveryNoteTotalAmt = 0;
                decimal MatirialRecipteTotalAmt = 0;

                var context = new DBMATAccounting_MagnetEntities1();
                var DeliveryNoteMasterList = new List<tbl_DeliveryNoteMaster>();

                var tbl_DeliveryNoteMaster = context.tbl_DeliveryNoteMaster.ToList();
                foreach (var itm in tbl_DeliveryNoteMaster)
                {
                    var itmS = context.tbl_SalesMaster.Where(pm => pm.deliveryNoteMasterId == itm.deliveryNoteMasterId).Any();
                    if (!itmS)
                    {
                        DeliveryNoteMasterList.Add(itm);
                    }
                }

                var FDateFrom = PublicVariables._dtFromDate;
                var FDateTo = Todate;

                DeliveryNoteMasterList.Where(dn => dn.date >= FDateFrom && dn.date <= FDateTo).ToList().ForEach(dm =>
                {
                    var dnd = context.tbl_DeliveryNoteDetails.Where(dd => dd.deliveryNoteMasterId == dm.deliveryNoteMasterId).ToList();
                    if (dnd != null)
                    {
                        listDeliveryNotes.AddRange(dnd);
                    }
                });

                listDeliveryNotes.ForEach(dn =>
                {
                    var dnd = context.tbl_RejectionInDetails.Where(dd => dd.deliveryNoteDetailsId == dn.deliveryNoteDetailsId && dd.productId == dn.productId).FirstOrDefault();
                    if (dnd != null)
                    {
                        var avCostProduct = StockReportDetails(dn.productId.Value);
                        var QtyRemain = dn.qty.Value - dnd.qty.Value;
                        var TotalAmt = avCostProduct * QtyRemain;

                        DeliveryNoteTotalAmt += (TotalAmt);
                    }
                    else
                    {
                        var avCostProduct = StockReportDetails(dn.productId.Value);
                        var QtyRemain = dn.qty.Value;
                        var TotalAmt = avCostProduct * QtyRemain;

                        DeliveryNoteTotalAmt += (TotalAmt);
                    }
                });

                var MaterialReceiptMasterList = context.tbl_MaterialReceiptMaster.ToList();
                var tbl_MaterialReceiptMaster = new List<tbl_MaterialReceiptMaster>();
                foreach (var itm in MaterialReceiptMasterList)
                {
                    var itmS = context.tbl_PurchaseMaster.Where(pm => pm.materialReceiptMasterId == itm.materialReceiptMasterId).Any();
                    if (!itmS)
                    {
                        tbl_MaterialReceiptMaster.Add(itm);
                    }
                }

                var listMaterialReceiptDetails = new List<tbl_MaterialReceiptDetails>();
                tbl_MaterialReceiptMaster.Where(dn => dn.date >= FDateFrom && dn.date <= FDateTo).ToList().ForEach(dm =>
                {
                    var dnd = context.tbl_MaterialReceiptDetails.Where(dd => dd.materialReceiptMasterId == dm.materialReceiptMasterId).ToList();
                    if (dnd != null)
                    {
                        listMaterialReceiptDetails.AddRange(dnd);
                    }
                });

                listMaterialReceiptDetails.ForEach(dn =>
                {
                    var dnd = context.tbl_RejectionOutDetails.Where(dd => dd.materialReceiptDetailsId == dn.materialReceiptDetailsId && dd.productId == dn.productId).FirstOrDefault();
                    // var product = context.tbl_Product.Where(p => p.productId == dn.productId).FirstOrDefault();
                    if (dnd != null)
                    {
                        var avCostProduct = StockReportDetails(dn.productId.Value);

                        var TotMR = dn.qty.Value * dn.rate.Value;
                        var TotRO = dnd.qty.Value * avCostProduct;

                        MatirialRecipteTotalAmt += (TotMR - TotRO);
                    }
                    else
                    {
                        // var avCostProduct = StockReportDetails(dn.productId.Value);
                        var TotMR = dn.qty.Value * dn.rate.Value;
                        MatirialRecipteTotalAmt += (TotMR);
                    }
                });

                return DeliveryNoteTotalAmt - MatirialRecipteTotalAmt;

                //if (MatirialRecipteTotalAmt > DeliveryNoteTotalAmt)
                //   return MatirialRecipteTotalAmt - DeliveryNoteTotalAmt;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public decimal GetDeliveryNoteAndMatrialReciept()
        {
            try
            {

                var listDeliveryNotes = new List<tbl_DeliveryNoteDetails>();
                decimal DeliveryNoteTotalAmt = 0;
                decimal MatirialRecipteTotalAmt = 0;

                var context = new DBMATAccounting_MagnetEntities1();
                var DeliveryNoteMasterList = new List<tbl_DeliveryNoteMaster>();

                var tbl_DeliveryNoteMaster = context.tbl_DeliveryNoteMaster.ToList();
                foreach (var itm in tbl_DeliveryNoteMaster)
                {
                    var itmS = context.tbl_SalesMaster.Where(pm => pm.deliveryNoteMasterId == itm.deliveryNoteMasterId).Any();
                    if (!itmS)
                    {
                        DeliveryNoteMasterList.Add(itm);
                    }
                }

             
                var FDateFrom = PublicVariables._dtFromDate;
                var FDateTo = PublicVariables._dtToDate;

                DeliveryNoteMasterList.Where(dn => dn.date >= FDateFrom && dn.date <= FDateTo).ToList().ForEach(dm =>
                {
                    var dnd = context.tbl_DeliveryNoteDetails.Where(dd => dd.deliveryNoteMasterId == dm.deliveryNoteMasterId).ToList();
                    if (dnd != null)
                    {
                        listDeliveryNotes.AddRange(dnd);
                    }
                });

                listDeliveryNotes.ForEach(dn =>
                {
                    var dnd = context.tbl_RejectionInDetails.Where(dd => dd.deliveryNoteDetailsId == dn.deliveryNoteDetailsId && dd.productId == dn.productId).FirstOrDefault();
                    if (dnd != null)
                    {
                        var avCostProduct = StockReportDetails(dn.productId.Value);
                        var QtyRemain = dn.qty.Value - dnd.qty.Value;
                        var TotalAmt = avCostProduct * QtyRemain;

                        DeliveryNoteTotalAmt += (TotalAmt);
                    }
                    else
                    {
                        var avCostProduct = StockReportDetails(dn.productId.Value);
                        var QtyRemain = dn.qty.Value;
                        var TotalAmt = avCostProduct * QtyRemain;

                        DeliveryNoteTotalAmt += (TotalAmt);
                    }
                });

                var MaterialReceiptMasterList = context.tbl_MaterialReceiptMaster.ToList();
                var tbl_MaterialReceiptMaster = new List<tbl_MaterialReceiptMaster>();
                foreach (var itm in MaterialReceiptMasterList)
                {
                    var itmS = context.tbl_PurchaseMaster.Where(pm => pm.materialReceiptMasterId == itm.materialReceiptMasterId).Any();
                    if (!itmS)
                    {
                        tbl_MaterialReceiptMaster.Add(itm);
                    }
                }

                var listMaterialReceiptDetails = new List<tbl_MaterialReceiptDetails>();
                tbl_MaterialReceiptMaster.Where(dn => dn.date >= FDateFrom && dn.date <= FDateTo).ToList().ForEach(dm =>
                {
                    var dnd = context.tbl_MaterialReceiptDetails.Where(dd => dd.materialReceiptMasterId == dm.materialReceiptMasterId).ToList();
                    if (dnd != null)
                    {
                        listMaterialReceiptDetails.AddRange(dnd);
                    }
                });


                listMaterialReceiptDetails.ForEach(dn =>
                {
                    var dnd = context.tbl_RejectionOutDetails.Where(dd => dd.materialReceiptDetailsId == dn.materialReceiptDetailsId && dd.productId == dn.productId).FirstOrDefault();
                   // var product = context.tbl_Product.Where(p => p.productId == dn.productId).FirstOrDefault();
                    if (dnd != null)
                    {
                        var avCostProduct = StockReportDetails(dn.productId.Value);
                        
                        var TotMR = dn.qty.Value * dn.rate.Value;
                        var TotRO = dnd.qty.Value * avCostProduct;

                        MatirialRecipteTotalAmt += (TotMR - TotRO);
                    }
                    else
                    {
                        // var avCostProduct = StockReportDetails(dn.productId.Value);
                        var TotMR = dn.qty.Value * dn.rate.Value;
                        MatirialRecipteTotalAmt += (TotMR);
                    }
                });



                return DeliveryNoteTotalAmt - MatirialRecipteTotalAmt;

                //if (MatirialRecipteTotalAmt > DeliveryNoteTotalAmt)
                 //   return MatirialRecipteTotalAmt - DeliveryNoteTotalAmt;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public List<tbl_LedgerPosting> GetLedgerPosting(decimal ledgerId, DateTime date)
        {
            var ledgerPostings = context.tbl_LedgerPosting.Where(a => a.ledgerId == ledgerId && a.date.Value.Year <= date.Year).ToList();
            return ledgerPostings;
        }

        public List<tbl_LedgerPosting> GetLedgerPosting1(decimal ledgerId, DateTime from, DateTime to)
        {
            var ledgerPostings =  context.tbl_LedgerPosting.Where(a => a.ledgerId == ledgerId && a.date >= from && a.date <= to).ToList();
            return ledgerPostings;
        }

        public List<tbl_LedgerPosting> TransactionLedger(List<tbl_LedgerPosting> ledgerPostings)
        {
            //foreach (var i in ledgerPostings)
            //{
            //    if (i.credit > 0.00M && i.debit > 0.00M)
            //    {
            //        var debitDiff = i.debit - i.credit;
            //        if (debitDiff > 0)
            //        {
            //            i.debit = debitDiff;
            //            i.credit = 0.00m;
            //        }
            //        else
            //        {
            //            i.credit = debitDiff;
            //            i.debit = 0.0M;
            //        }
            //    }
            //}

            return ledgerPostings;
        }

        public decimal GetStock(DateTime date)
        {
            decimal dcOpeningStockForRollOver = 0;
            string calculationMethod = string.Empty;
            SettingsInfo InfoSettings = new SettingsInfo();
            SettingsSP SpSettings = new SettingsSP();
            //GeneralQueries queryForRetainedEarning = new GeneralQueries();
            //--------------- Selection Of Calculation Method According To Settings ------------------// 
            if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
            {
                calculationMethod = "FIFO";
            }
            else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
            {
                calculationMethod = "Average Cost";
            }
            else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
            {
                calculationMethod = "High Cost";
            }
            else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
            {
                calculationMethod = "Low Cost";
            }
            else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
            {
                calculationMethod = "Last Purchase Rate";
            }


            FinancialStatementSP SpFinance1 = new FinancialStatementSP();
            CurrencyInfo InfoCurrency = new CurrencyInfo();

            //GeneralQueries methodForStockValue = new GeneralQueries();
            DBMatConnection conn = new DBMatConnection();
            int inDecimalPlaces = InfoCurrency.NoOfDecimalPlaces;
            //decimal dcClosingStock = SpFinance1.StockValueGetOnDate(date, calculationMethod, false, false);
            //dcClosingStock = Math.Round(dcClosingStock, inDecimalPlaces);
            //---------------------Opening Stock-----------------------
            //decimal dcOpeninggStock = SpFinance1.StockValueGetOnDate(PublicVariables._dtFromDate, calculationMethod, true, true);           
            //string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", MATFinancials.PublicVariables._dtToDate.ToString());
            //string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", MATFinancials.PublicVariables._dtFromDate.ToString());

            decimal dcOpeninggStock =
                SpFinance1.StockValueGetOnDate(Convert.ToDateTime(date), calculationMethod, true, false);
            dcOpeninggStock = dcOpeninggStock + dcOpeningStockForRollOver;
            return dcOpeninggStock;
            //DataRow dr = dtTrial.NewRow();  // to add new row
            //dr["accountGroupId"] = -4;
            //dr["debit"] = 0.0;
            //dr["credit"] = 0.0;
            //dr["creditDifference"] = 0.0;
            //dr["debitDifference"] = dcOpeninggStock;
            //dr["accountGroupName"] = "Stock";
            //dtTrial.Rows.Add(dr); 

        }

        public dynamic GetOpeningStockers(StockReportSearch input)
        {

            decimal totalStockValue1 = 0;
            List<string> AllItems = new List<string> { };
            List<decimal> AllStores = new List<decimal> { };
            StockReportRow master = new StockReportRow();
            List<StockReportRowItems> details = new List<StockReportRowItems>();
            decimal qtyBal = 0, stockValue = 0, OpeningStockValue = 0;
            try
            {
                DataTable prodDtbl = new DataTable();
                prodDtbl = new ProductSP().ProductViewAll();
                for (int k = 0; k < prodDtbl.Rows.Count; k++)
                {
                    var FinancialDate = Convert.ToDateTime(prodDtbl.Rows[k]["effectiveDate"].ToString());

                    if (FinancialDate >= input.FromDate && FinancialDate <= input.ToDate)
                    {

                    }

                    AllItems.Add(prodDtbl.Rows[k]["productCode"].ToString());
                }

                AllItems = AllItems.ToList().Distinct().ToList();
                GodownSP spGodown = new GodownSP();
                DataTable storeDtbl = new DataTable();
                storeDtbl = spGodown.GodownViewAll();
                for (int k = 0; k < storeDtbl.Rows.Count; k++)
                {
                    AllStores.Add(Convert.ToDecimal(storeDtbl.Rows[k]["godownId"]));
                }

                AllStores = AllStores.ToList().Distinct().ToList();
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                string calculationMethod = string.Empty;
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", input.ToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", input.FromDate.ToString());
                DateTime lastFinYearStart = input.FromDate;

                //conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;

                DateTime lastFinYearEnd = DateTime.UtcNow;
                try
                {
                    lastFinYearEnd = input.ToDate;

                    //conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                }
                catch (Exception ex)
                {

                }

                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    //lastFinYearEnd = input.FromDate.AddDays(-1);
                    lastFinYearEnd = input.ToDate;

                }

                lastFinYearEnd = input.ToDate;
                decimal currentProductId = 0, prevProductId = 0, prevStoreId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0;
               
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal stockValuePerItemPerStore = 0, totalStockValue = 0;
                decimal inwardQtyPerStore = 0, outwardQtyPerStore = 0, totalQtyPerStore = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "", productName = "", storeName = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };
                DataTable dtbl = new DataTable();
                DataTable dtblItem = new DataTable();
                DataTable dtblStore = new DataTable();
                DataTable dtblOpeningStocks = new DataTable();
                DataTable dtblOpeningStockPerItemPerStore = new DataTable();
                var spstock = new MATFinancials.StockPostingSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                decimal productId = input.ProductId, storeId = input.StoreId;
                string produceCode = input.ProductCode, batch = input.BatchNo, referenceNo = input.RefNo;
                input.ToDate = input.ToDate;

                //dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text), "", "");
                //dtblOpeningStocks = spstock.StockReportDetailsGridFill(0, 0, "All", PublicVariables._dtFromDate, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), "", "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);
                if (batch != "" || input.ProjectId != 0 || input.CategoryId != 0 || productId != 0 || storeId != 0 || produceCode != "")


                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);
                else
                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);


                if (AllItems.Any())
                {
                    foreach (var item in AllItems)
                    {
                        // ======================================== for each item begins here ====================================== //

                        dtblItem = new DataTable();

                        if (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item).Any())
                        {
                            dtblItem = (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item)).CopyToDataTable();

                            productName = (dtbl.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(m => m.Field<string>("productName")).FirstOrDefault().ToString());
                        }

                        if (productName.ToLower().Contains("CHINCHIN".ToLower()) || productName.ToLower().Equals("CHINCHIN".ToLower()))
                        {

                        }

                        if (dtblItem != null && dtblItem.Rows.Count > 0)
                        {
                            if (AllStores.Any())
                            {
                                foreach (var store in AllStores)
                                {
                                    // =================================== for that item, for each store begins here ============================== //


                                    qtyBal = 0; stockValue = 0;
                                    stockValuePerItemPerStore = 0; //dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    dtblStore = new DataTable();

                                    // ************************ insert opening stock per item per store before other transactions ******************** //
                                    dtblOpeningStockPerItemPerStore = new DataTable();
                                    if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                    {
                                        dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store)).CopyToDataTable();
                                        if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                        {
                                            computedAverageRate = rate2;
                                            var itemParams = methodForStockValue.currentStockValue1235(lastFinYearStart, lastFinYearEnd, true, item, store);
                                            dcOpeningStockForRollOver = itemParams.Item1;
                                            openingQuantity = itemParams.Item2;
                                            openingQuantityIn = itemParams.Item3;
                                            openingQuantityOut = itemParams.Item4;
                                            qtyBal += openingQuantity;
                                            qtyBal = 0;
                                            // qtyBal = openingQuantity;
                                            if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                            {
                                                computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                                stockValue = qtyBal * computedAverageRate;
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = item,
                                                ProductCode = item,
                                                ProductName = productName,
                                                StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                                Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                                InwardQty = openingQuantityIn.ToString("N2"),
                                                OutwardQty = openingQuantityOut.ToString("N2"),
                                                Rate = "",
                                                AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                                QtyBalance = openingQuantity.ToString("N2"),
                                                StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                                VoucherTypeName = "Opening Stock "
                                            });

                                            //dgvStockDetailsReport.Rows[i].Cells["productId"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productName"].Value = productName;
                                            //dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            //Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(); ;
                                            //dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                            //dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                            //dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";

                                            //New Comments
                                            totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            // dcOpeningStockForRollOver;
                                            //totalQtyPerStore += openingQuantity;
                                            //i++;
                                            //Old Comments
                                        }
                                    }
                                    // ************************ insert opening stock per item per store before other transactions end ******************** //

                                    dtblStore = null;

                                    if (dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).Any())
                                    {
                                        dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).CopyToDataTable();
                                    }

                                    if (dtblStore != null && dtblStore.Rows.Count > 0)
                                    {
                                        storeName = (dtblStore.AsEnumerable().Where(m => m.Field<decimal>("godownId") == store).Select(m => m.Field<string>("godownName")).FirstOrDefault().ToString());

                                        foreach (DataRow dr in dtblStore.Rows)
                                        {
                                            // opening stock has been factored in thinking there was no other transaction, now other transactions factored it in again, so remove it
                                            totalStockValue -= Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            //dgvStockDetailsReport.Rows[i-1].DefaultCellStyle.BackColor = Color.White;
                                            //dgvStockDetailsReport.Rows[i-1].Cells["rate"].Value = "";
                                            dcOpeningStockForRollOver = 0;  // reset the opening stock because of the loop

                                            currentProductId = Convert.ToDecimal(dr["productId"]);
                                            productCode = dr["productCode"].ToString();
                                            string voucherType = "", refNo = "";
                                            inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);

                                            decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                                            string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                                            decimal unitConvId = 0;
                                            try
                                            {
                                                unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;
                                            }
                                           catch (Exception es)
                                            {
                                                unitConvId = 0;
                                            }
                                           
                                            bool isOpeningRate = false;

                                            if (false)
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }
                                            else
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }

                                            //if (purchaseRate == 0)
                                            //{
                                            //    purchaseRate = rate2;
                                            //}

                                            if (prevStoreId != store)
                                            {
                                                previousRunningAverage = purchaseRate;
                                            }

                                            if (previousRunningAverage != 0 && currentProductId == prevProductId)
                                            {
                                                purchaseRate = previousRunningAverage;
                                            }

                                            refNo = dr["refNo"].ToString();

                                            if (againstVoucherType != "NA")
                                            {
                                                refNo = dr["againstVoucherNo"].ToString();
                                                isAgainstVoucher = true;
                                                againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                                                string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                                                string typeOfVoucher = conn.getSingleValue(queryString);
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note".ToLower() && typeOfVoucher.ToLower() == "sales invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt".ToLower() && typeOfVoucher.ToLower() == "purchase invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                            }

                                            voucherType = dr["typeOfVoucher"].ToString();

                                            if (voucherType.ToLower() == "Stock Journal".ToLower())
                                            {
                                                //voucherType = "Stock Transfer";
                                                string queryString = string.Format(" SELECT extra1 FROM tbl_StockJournalMaster WHERE voucherNo = '{0}' ", dr["refNo"]);
                                                voucherType = conn.getSingleValue(queryString);
                                            }

                                            if (outwardQty2 > 0)
                                            {
                                                if (voucherType.ToLower() == "Sales Invoice".ToLower() && outwardQty2 > 0)
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate;
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Material Receipt".ToLower())
                                                {
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue -= outwardQty2 * rate2;
                                                }
                                                else if (outwardQty2 > 0 && (voucherType.ToLower() == "Rejection Out".ToLower()) || (voucherType.ToLower() == "Purchase Return".ToLower())
                                                    || (voucherType.ToLower() == "Sales Return".ToLower()) || (voucherType.ToLower() == "Rejection In".ToLower() || voucherType.ToLower() == "Stock Transfer".ToLower()))
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;// 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Sales Order".ToLower())
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else
                                                {
                                                    if (qtyBal != 0)
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Return".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Rejection In".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Invoice".ToLower() && inwardQty2 > 0)
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;

                                                }
                                                else
                                                {
                                                    computedAverageRate = previousRunningAverage;// (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Delivery Note".ToLower())
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Opening Stock".ToLower())
                                            {
                                                // openingQuantityIn = 0;

                                                if (qtyBal != 0)
                                                {
                                                    //computedAverageRate = (stockValue / qtyBal);

                                                    // qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }
                                                else
                                                {
                                                    //computedAverageRate = (stockValue / 1);
                                                    //qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }

                                                OpeningStockValue += (inwardQty2 * rate2);
                                            }
                                            else
                                            {
                                                // hndles other transactions such as purchase, opening balance, etc
                                                qtyBal += inwardQty2 - outwardQty2;
                                                stockValue += (inwardQty2 * rate2);

                                            }

                                            // ------------------------------------------------------ //
                                            //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            string avgCost = "";
                                            if (stockValue != 0 && qtyBal != 0)
                                            {
                                                avgCost = (stockValue / qtyBal).ToString("N2");
                                                //avgCost = (stockValue / qtyBal * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }
                                            else
                                            {
                                                avgCost = computedAverageRate.ToString("N2");
                                                //avgCost = (computedAverageRate/ SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = dr["productId"].ToString(),
                                                ProductCode = dr["productCode"].ToString(),
                                                StoreName = dr["godownName"].ToString(),
                                                InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                                                OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                                                Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                                AverageCost = avgCost,
                                                QtyBalance = qtyBal.ToString("N2"),
                                                StockValue = stockValue.ToString("N2"),
                                                VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType,
                                                RefNo = refNo,
                                                Batch = string.IsNullOrEmpty(dr["Batch"].ToString()) ? "" : dr["Batch"].ToString(),
                                                Date = Convert.ToDateTime(dr["date"]).ToShortDateString(),
                                                ProductName = dr["productName"].ToString(),
                                                ProjectId = Convert.ToDecimal(dr["projectId"]),
                                                CategoryId = Convert.ToDecimal(dr["categoryId"]),
                                                UnitName = dr["unitName"].ToString()
                                            });

                                            isAgainstVoucher = false;

                                            stockValuePerItemPerStore = Convert.ToDecimal(stockValue);
                                            prevProductId = currentProductId;
                                            previousRunningAverage = Convert.ToDecimal(avgCost);
                                            inwardQtyPerStore += Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQtyPerStore += Convert.ToDecimal(dr["outwardQty"]);
                                            i++;
                                        }
                                        // ===================== for every store for that item, put the summary =================== //
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = "",
                                            ProductCode = item,
                                            StoreName = storeName,
                                            InwardQty = (inwardQtyPerStore /*+ openingQuantityIn*/).ToString("N2"),
                                            OutwardQty = ("-") + (outwardQtyPerStore /*+ openingQuantityOut*/).ToString("N2"),
                                            Rate = "",
                                            AverageCost = "",
                                            QtyBalance = qtyBal.ToString("N2"),
                                            StockValue = stockValuePerItemPerStore.ToString("N2"),
                                            VoucherTypeName = "Sub total: ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = "",
                                        });

                                        totalStockValue += Convert.ToDecimal(stockValuePerItemPerStore.ToString("N2"));
                                        totalQtyPerStore += qtyBal /*(inwardQtyPerStore - outwardQtyPerStore) + openingQuantity*/;

                                        openingQuantity = 0;
                                        inwardQtyPerStore = 0;
                                        outwardQtyPerStore = 0;
                                        qtyBal = 0;
                                        stockValue = 0;
                                        stockValuePerItemPerStore = 0;
                                        //dgvStockDetailsReport.Rows.Add();
                                        i++;
                                    }

                                    dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    prevStoreId = store;
                                }
                            }
                        }
                        // *** if no transaction for that item for all stores for that period add the ockock for all stores per store *** //
                        else
                        {
                            foreach (var store in AllStores)
                            {
                                stockValuePerItemPerStore = 0;
                                dtblStore = new DataTable();
                                // ************************ insert opening stock per item per store ******************** //
                                dtblOpeningStockPerItemPerStore = new DataTable();
                                if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                {
                                    dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item)).CopyToDataTable();

                                    if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                    {
                                        computedAverageRate = rate2;
                                        var itemParams = methodForStockValue.currentStockValue1235(lastFinYearStart, lastFinYearEnd, true, item, store);
                                        dcOpeningStockForRollOver = itemParams.Item1;
                                        openingQuantity = itemParams.Item2;
                                        openingQuantityIn = itemParams.Item3;
                                        openingQuantityOut = itemParams.Item4;
                                        qtyBal += openingQuantity;
                                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                        {
                                            //computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                            //stockValue = openingQuantity * computedAverageRate;
                                        }
                                        var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = item,
                                            ProductCode = item,
                                            StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                        Where(l => l.Field<string>("productCode") == item).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                            InwardQty = openingQuantityIn.ToString("N2"),
                                            OutwardQty = openingQuantityOut.ToString("N2"),
                                            Rate = "",
                                            AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                            //AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2") : "",
                                            QtyBalance = openingQuantity.ToString("N2"),
                                            StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                            VoucherTypeName = "Opening Stock ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = ""
                                        });

                                        totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                        dcOpeningStockForRollOver = 0;
                                        totalQtyPerStore += openingQuantity;
                                        openingQuantityIn = 0;
                                        openingQuantityOut = 0;
                                        openingQuantity = 0;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }

                // ************************ Calculate Total **************************** //
                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var avgCost2 = "";
                if (totalQtyPerStore != 0)
                {
                    avgCost2 = (totalStockValue / totalQtyPerStore).ToString("N2");
                }
                totalStockValue1 = totalStockValue;

                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = avgCost2,
                    QtyBalance = totalQtyPerStore.ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    VoucherTypeName = "Total Stock Value:",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var tyr = OpeningStockValue;
            }

            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }
            master.TotalAverageCost = 0;
            master.TotalQtyBalance = 0;
            master.TotalStockValue = 0;
            master.StockReportRowItem = details;


            return new { totalStockValue = totalStockValue1, OpeningStockValue = OpeningStockValue } ;
        }

        private Dictionary<string, decimal> GetAllStockDetails(StockReportSearch input)
        {
            Dictionary<string, decimal> result = new Dictionary<string, decimal>();
            try
            {
                /*
                 *stock algorithm 
                    1. get all stocks ordered by extraDate asc
                    2. get all products - productId, productName
                    3. group stocks by productId
                    4.  a. class ItemStockObj
		                    - BalanceQuantity 
		                    - AggregateValue 
		                    - AverageCost 
	                    b. groupData = new Dictionary<string, itemStockObj>();
                StockComputationObject

                    5. foreach itemStockGroup in stockGroup
	                    a. foreach stock in itemStockGroup
		                    i. create an itemStockObj for this stock
		                    ii.    if inwardQty > 0
			                       BalanceQuantity += stock.inwardQty
			                       AggregateValue += stock.inwardQty * rate 
			                       AverageCost = AggregateValue / BalanceQuantity
			   
			                       if outwardQty > 0 
			                       BalanceQuantity -= stock.outwardQty
		                    iii. groupData[stock.VoucherTypeName] = itemStockObj
                 * 
                 */

                var stocks = context.tbl_StockPosting.AsEnumerable()
                                    .Where(x => x.extraDate <= input.ToDate)
                                    .OrderBy(x => x.extraDate)
                                    .ToList();
                var products = context.tbl_Product.AsEnumerable()
                                    .OrderBy(x => x.extraDate)
                                    .ToList();

                var productStockDetail = new Dictionary<decimal, StockComputationObject>();
                var productStockDetail2 = new Dictionary<string, StockComputationObject>();
                var stocksGroupedByProductId = stocks.GroupBy(x => x.productId);
                foreach (var productGroup in stocksGroupedByProductId)
                {
                    var dataInstace = new StockComputationObject();
                    var productGroupId = productGroup.Key.Value;
                    var productItem = products.FirstOrDefault(x => x.productId == productGroupId);
                    if(productItem != null)
                    {
                        foreach (var itemStock in productGroup)
                        {
                            if(itemStock?.inwardQty > 0)
                            {
                                dataInstace.BalanceQuantity += itemStock.inwardQty.Value;
                                dataInstace.AggregateValue += (itemStock.inwardQty.Value * Math.Abs(itemStock.rate.Value));
                                dataInstace.AverageCost = dataInstace.AggregateValue / dataInstace.BalanceQuantity;
                            }
                            else if(itemStock.outwardQty > 0)
                            {
                                dataInstace.BalanceQuantity -= itemStock.outwardQty.Value;
                                dataInstace.AggregateValue -= (itemStock.outwardQty.Value * dataInstace.AverageCost);
                            }
                        }
                        productStockDetail[productGroupId] = dataInstace;
                        productStockDetail2[productItem.productName] = dataInstace;

                    }
                    else
                    {
                        // _logger.LogInformation($"Unable to fetch product with id {productGroupId}");
                    }
                }

                //productStockDetail now contains dictionary averagecost details for each product 
                result["TotalQuantity"] = productStockDetail.Sum(x =>x.Value.BalanceQuantity);
                result["TotalAverageCost"] = productStockDetail.Sum(x =>x.Value.AverageCost);
                result["TotalStockValue"] = productStockDetail.Sum(x =>x.Value.AggregateValue);

                //var filePath = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "productStockDetail.csv");
                //var filePath2 = Path.Combine(HostingEnvironment.ApplicationPhysicalPath, "stockDetails.txt");
                ////File.WriteAllText(filePath, new JavaScriptSerializer().Serialize(productStockDetail2));
                //List<string> fileContent = new List<string>();
                //fileContent.Add($"ProductName,BalanceQuantity,AverageCost,AggregateValue");
                //foreach (var kv in productStockDetail2)
                //{
                //    string key = kv.Key;
                //    var balanceQuantity = kv.Value.BalanceQuantity;
                //    var aggregateValue = kv.Value.AggregateValue;
                //    var averageCost = kv.Value.AverageCost;
                //    fileContent.Add($"{key},{balanceQuantity},{averageCost},{aggregateValue}");

                //}
                //File.WriteAllLines(filePath, fileContent);
                //File.WriteAllText(filePath2, new JavaScriptSerializer().Serialize(result));


            }
            catch (Exception ex)
            {
                
            }
            return result;
        }

        List<string> AllItems = new List<string>();
        List<decimal> AllStores = new List<decimal>();
        bool isOpeningRate = false;
        public decimal StockReportDetails(decimal ProductId)
        {
            StockReportRow master = new StockReportRow();
            List<StockReportRowItems> details = new List<StockReportRowItems>();
            StockReportSearch input = new StockReportSearch();


            input.FromDate = PublicVariables._dtFromDate;
            input.ToDate = PublicVariables._dtToDate;

            var ListLastFyear = context.tbl_FinancialYear.Where(fy => fy.fromDate < input.FromDate && fy.toDate < input.FromDate).ToList();

            var BalanceDateDate = input.FromDate;

            if (ListLastFyear.Count > 0)
            {
                BalanceDateDate = ListLastFyear.Min(cfy => cfy.fromDate.Value);
            }

            input.FromDate = BalanceDateDate;

            input.BatchNo = "All";
            input.CategoryId = 0;
            input.ProjectId = 0;
            input.TransactionType = "0";
            input.StoreId = 0;
            input.ProductId = ProductId;
            input.ProductCode = "";
            input.RefNo = "";

            try
            {
                DataTable prodDtbl = new DataTable();
                prodDtbl = new ProductSP().ProductViewAll();
                for (int k = 0; k < prodDtbl.Rows.Count; k++)
                {
                    var FinancialDate = Convert.ToDateTime(prodDtbl.Rows[k]["effectiveDate"].ToString());

                    if (FinancialDate >= input.FromDate && FinancialDate <= input.ToDate)
                    {

                    }

                    AllItems.Add(prodDtbl.Rows[k]["productCode"].ToString());
                }

                AllItems = AllItems.ToList().Distinct().ToList();
                GodownSP spGodown = new GodownSP();
                DataTable storeDtbl = new DataTable();
                storeDtbl = spGodown.GodownViewAll();
                for (int k = 0; k < storeDtbl.Rows.Count; k++)
                {
                    AllStores.Add(Convert.ToDecimal(storeDtbl.Rows[k]["godownId"]));
                }

                AllStores = AllStores.ToList().Distinct().ToList();
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                string calculationMethod = string.Empty;
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = DateTime.UtcNow;
                try
                {
                    lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                }
                catch (Exception ex)
                {

                }

                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    //lastFinYearEnd = input.FromDate.AddDays(-1);
                    lastFinYearEnd = input.ToDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                }
                decimal currentProductId = 0, prevProductId = 0, prevStoreId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0;
                decimal qtyBal = 0, stockValue = 0, OpeningStockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal stockValuePerItemPerStore = 0, totalStockValue = 0;
                decimal inwardQtyPerStore = 0, outwardQtyPerStore = 0, totalQtyPerStore = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "", productName = "", storeName = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };
                DataTable dtbl = new DataTable();
                DataTable dtblItem = new DataTable();
                DataTable dtblStore = new DataTable();
                DataTable dtblOpeningStocks = new DataTable();
                DataTable dtblOpeningStockPerItemPerStore = new DataTable();
                var spstock = new MATFinancials.StockPostingSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                decimal productId = input.ProductId, storeId = input.StoreId;
                string produceCode = input.ProductCode, batch = input.BatchNo, referenceNo = input.RefNo;
                input.ToDate = input.ToDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                //dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text), "", "");
                //dtblOpeningStocks = spstock.StockReportDetailsGridFill(0, 0, "All", PublicVariables._dtFromDate, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), "", "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));
                if (batch != "" || input.ProjectId != 0 || input.CategoryId != 0 || productId != 0 || storeId != 0 || produceCode != "")

                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));
                else
                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));


                if (AllItems.Any())
                {
                    foreach (var item in AllItems)
                    {
                        // ======================================== for each item begins here ====================================== //

                        dtblItem = new DataTable();

                        if (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item).Any())
                        {
                            dtblItem = (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item)).CopyToDataTable();

                            productName = (dtbl.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(m => m.Field<string>("productName")).FirstOrDefault().ToString());
                        }

                        if (productName.ToLower().Contains("CHINCHIN".ToLower()) || productName.ToLower().Equals("CHINCHIN".ToLower()))
                        {

                        }

                        if (dtblItem != null && dtblItem.Rows.Count > 0)
                        {
                            if (AllStores.Any())
                            {
                                foreach (var store in AllStores)
                                {
                                    // =================================== for that item, for each store begins here ============================== //

                                    qtyBal = 0; stockValue = 0;
                                    stockValuePerItemPerStore = 0; //dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    dtblStore = new DataTable();

                                    // ************************ insert opening stock per item per store before other transactions ******************** //
                                    dtblOpeningStockPerItemPerStore = new DataTable();
                                    if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                    {
                                        dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store)).CopyToDataTable();
                                        if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                        {
                                            computedAverageRate = rate2;
                                            var itemParams = methodForStockValue.currentStockValue1235(lastFinYearStart, lastFinYearEnd, true, item, store);
                                            dcOpeningStockForRollOver = itemParams.Item1;
                                            openingQuantity = itemParams.Item2;
                                            openingQuantityIn = itemParams.Item3;
                                            openingQuantityOut = itemParams.Item4;
                                            qtyBal += openingQuantity;
                                            qtyBal = 0;
                                            // qtyBal = openingQuantity;
                                            if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                            {
                                                computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                                stockValue = qtyBal * computedAverageRate;
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = item,
                                                ProductCode = item,
                                                ProductName = productName,
                                                StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                                Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                                InwardQty = openingQuantityIn.ToString("N2"),
                                                OutwardQty = openingQuantityOut.ToString("N2"),
                                                Rate = "",
                                                AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                                QtyBalance = openingQuantity.ToString("N2"),
                                                StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                                VoucherTypeName = "Opening Stock "
                                            });

                                            //dgvStockDetailsReport.Rows[i].Cells["productId"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productName"].Value = productName;
                                            //dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            //Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(); ;
                                            //dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                            //dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                            //dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";

                                            //New Comments
                                            totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            // dcOpeningStockForRollOver;
                                            //totalQtyPerStore += openingQuantity;
                                            //i++;
                                            //Old Comments
                                        }
                                    }
                                    // ************************ insert opening stock per item per store before other transactions end ******************** //

                                    dtblStore = null;

                                    if (dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).Any())
                                    {
                                        //if (input.TransactionType.ToLower() == "All".ToLower())
                                        //{

                                        //}
                                        //else
                                        //{
                                        //    //dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store  && j.Field<string>("typeOfVoucher").ToLower() == input.TransactionType.ToLower()).CopyToDataTable();
                                        //}


                                        dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).CopyToDataTable();


                                    }

                                    if (dtblStore != null && dtblStore.Rows.Count > 0)
                                    {
                                        storeName = (dtblStore.AsEnumerable().Where(m => m.Field<decimal>("godownId") == store).Select(m => m.Field<string>("godownName")).FirstOrDefault().ToString());


                                        foreach (DataRow dr in dtblStore.Rows)
                                        {
                                            // opening stock has been factored in thinking there was no other transaction, now other transactions factored it in again, so remove it
                                            totalStockValue -= Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            //dgvStockDetailsReport.Rows[i-1].DefaultCellStyle.BackColor = Color.White;
                                            //dgvStockDetailsReport.Rows[i-1].Cells["rate"].Value = "";
                                            dcOpeningStockForRollOver = 0;  // reset the opening stock because of the loop

                                            currentProductId = Convert.ToDecimal(dr["productId"]);
                                            productCode = dr["productCode"].ToString();
                                            string voucherType = "", refNo = "";
                                            inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);

                                            decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                                            string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);

                                            var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            if (!isOpeningRate)
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }
                                            else
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }

                                            //if (purchaseRate == 0)
                                            //{
                                            //    purchaseRate = rate2;
                                            //}

                                            if (prevStoreId != store)
                                            {
                                                previousRunningAverage = purchaseRate;
                                            }

                                            if (previousRunningAverage != 0 && currentProductId == prevProductId)
                                            {
                                                purchaseRate = previousRunningAverage;
                                            }

                                            refNo = dr["refNo"].ToString();

                                            if (againstVoucherType != "NA")
                                            {
                                                refNo = dr["againstVoucherNo"].ToString();
                                                isAgainstVoucher = true;
                                                againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                                                string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                                                string typeOfVoucher = conn.getSingleValue(queryString);
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note".ToLower() && typeOfVoucher.ToLower() == "sales invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt".ToLower() && typeOfVoucher.ToLower() == "purchase invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                            }

                                            voucherType = dr["typeOfVoucher"].ToString();

                                            if (voucherType.ToLower() == "Stock Journal".ToLower())
                                            {
                                                //voucherType = "Stock Transfer";
                                                string queryString = string.Format(" SELECT extra1 FROM tbl_StockJournalMaster WHERE voucherNo = '{0}' ", dr["refNo"]);
                                                voucherType = conn.getSingleValue(queryString);
                                            }

                                            if (outwardQty2 > 0)
                                            {
                                                if (voucherType.ToLower() == "Sales Invoice".ToLower() && outwardQty2 > 0)
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate;
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Material Receipt".ToLower())
                                                {
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue -= outwardQty2 * rate2;
                                                }
                                                else if (outwardQty2 > 0 && (voucherType.ToLower() == "Rejection Out".ToLower()) || (voucherType.ToLower() == "Purchase Return".ToLower())
                                                    || (voucherType.ToLower() == "Sales Return".ToLower()) || (voucherType.ToLower() == "Rejection In".ToLower() || voucherType.ToLower() == "Stock Transfer".ToLower()))
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;// 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Sales Order".ToLower())
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else
                                                {
                                                    if (qtyBal != 0)
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Return".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Rejection In".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Invoice".ToLower() && inwardQty2 > 0)
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;

                                                }
                                                else
                                                {
                                                    computedAverageRate = previousRunningAverage;// (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Delivery Note".ToLower())
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Opening Stock".ToLower())
                                            {
                                                // openingQuantityIn = 0;

                                                if (qtyBal != 0)
                                                {
                                                    //computedAverageRate = (stockValue / qtyBal);

                                                    // qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }
                                                else
                                                {
                                                    //computedAverageRate = (stockValue / 1);
                                                    //qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }

                                                OpeningStockValue += (inwardQty2 * rate2);
                                            }
                                            else
                                            {
                                                // hndles other transactions such as purchase, opening balance, etc
                                                qtyBal += inwardQty2 - outwardQty2;
                                                stockValue += (inwardQty2 * rate2);

                                            }

                                            // ------------------------------------------------------ //
                                            //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            string avgCost = "";
                                            if (stockValue != 0 && qtyBal != 0)
                                            {
                                                avgCost = (stockValue / qtyBal).ToString("N2");
                                                //avgCost = (stockValue / qtyBal * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }
                                            else
                                            {
                                                avgCost = computedAverageRate.ToString("N2");
                                                //avgCost = (computedAverageRate/ SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = dr["productId"].ToString(),
                                                ProductCode = dr["productCode"].ToString(),
                                                StoreName = dr["godownName"].ToString(),
                                                InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                                                OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                                                Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                                AverageCost = avgCost,
                                                QtyBalance = qtyBal.ToString("N2"),
                                                StockValue = stockValue.ToString("N2"),
                                                VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType,
                                                RefNo = refNo,
                                                Batch = string.IsNullOrEmpty(dr["Batch"].ToString()) ? "" : dr["Batch"].ToString(),
                                                Date = Convert.ToDateTime(dr["date"]).ToShortDateString(),
                                                ProductName = dr["productName"].ToString(),
                                                ProjectId = Convert.ToDecimal(dr["projectId"]),
                                                CategoryId = Convert.ToDecimal(dr["categoryId"]),
                                                UnitName = dr["unitName"].ToString()
                                            });

                                            isAgainstVoucher = false;

                                            stockValuePerItemPerStore = Convert.ToDecimal(stockValue);
                                            prevProductId = currentProductId;
                                            previousRunningAverage = Convert.ToDecimal(avgCost);
                                            inwardQtyPerStore += Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQtyPerStore += Convert.ToDecimal(dr["outwardQty"]);
                                            i++;
                                        }
                                        // ===================== for every store for that item, put the summary =================== //
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = "",
                                            ProductCode = item,
                                            StoreName = storeName,
                                            InwardQty = (inwardQtyPerStore /*+ openingQuantityIn*/).ToString("N2"),
                                            OutwardQty = ("-") + (outwardQtyPerStore /*+ openingQuantityOut*/).ToString("N2"),
                                            Rate = "",
                                            AverageCost = "",
                                            QtyBalance = qtyBal.ToString("N2"),
                                            StockValue = stockValuePerItemPerStore.ToString("N2"),
                                            VoucherTypeName = "Sub total: ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = "",
                                        });

                                        totalStockValue += Convert.ToDecimal(stockValuePerItemPerStore.ToString("N2"));
                                        totalQtyPerStore += qtyBal /*(inwardQtyPerStore - outwardQtyPerStore) + openingQuantity*/;

                                        openingQuantity = 0;
                                        inwardQtyPerStore = 0;
                                        outwardQtyPerStore = 0;
                                        qtyBal = 0;
                                        stockValue = 0;
                                        stockValuePerItemPerStore = 0;
                                        //dgvStockDetailsReport.Rows.Add();
                                        i++;
                                    }

                                    dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    prevStoreId = store;
                                }
                            }
                        }
                        // *** if no transaction for that item for all stores for that period add the ockock for all stores per store *** //
                        else
                        {
                            foreach (var store in AllStores)
                            {
                                stockValuePerItemPerStore = 0;
                                dtblStore = new DataTable();
                                // ************************ insert opening stock per item per store ******************** //
                                dtblOpeningStockPerItemPerStore = new DataTable();
                                if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                {
                                    dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item)).CopyToDataTable();

                                    if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                    {
                                        computedAverageRate = rate2;
                                        var itemParams = methodForStockValue.currentStockValue1235(lastFinYearStart, lastFinYearEnd, true, item, store);
                                        dcOpeningStockForRollOver = itemParams.Item1;
                                        openingQuantity = itemParams.Item2;
                                        openingQuantityIn = itemParams.Item3;
                                        openingQuantityOut = itemParams.Item4;
                                        qtyBal += openingQuantity;
                                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                        {
                                            //computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                            //stockValue = openingQuantity * computedAverageRate;
                                        }
                                        var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = item,
                                            ProductCode = item,
                                            StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                        Where(l => l.Field<string>("productCode") == item).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                            InwardQty = openingQuantityIn.ToString("N2"),
                                            OutwardQty = openingQuantityOut.ToString("N2"),
                                            Rate = "",
                                            AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                            //AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2") : "",
                                            QtyBalance = openingQuantity.ToString("N2"),
                                            StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                            VoucherTypeName = "Opening Stock ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = ""
                                        });

                                        totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                        dcOpeningStockForRollOver = 0;
                                        totalQtyPerStore += openingQuantity;
                                        openingQuantityIn = 0;
                                        openingQuantityOut = 0;
                                        openingQuantity = 0;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }

                // ************************ Calculate Total **************************** //
                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var avgCost2 = "";

                if (totalQtyPerStore != 0)
                {
                    avgCost2 = (totalStockValue / totalQtyPerStore).ToString("N2");
                }

                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = avgCost2,
                    QtyBalance = totalQtyPerStore.ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    VoucherTypeName = "Total Stock Value:",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var tyr = OpeningStockValue;

                master.TotalAverageCost = (totalStockValue / totalQtyPerStore);
                master.TotalQtyBalance = 0;
                master.TotalStockValue = 0;
                master.StockReportRowItem = details;
                return master.TotalAverageCost;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }

            master.TotalAverageCost = 0;
            master.TotalQtyBalance = 0;
            master.TotalStockValue = 0;
            master.StockReportRowItem = details;
            return master.TotalAverageCost;
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetProfitLossConvention(DateTime? from = null, DateTime? to = null)
        {
            try
            {
                var listCostOfSaleL = new List<string>();
                var listCostOfSaleLA = new List<string>();
                var listCostOfSaleLAG = new List<string>();

                if (from == null)
                {
                    from = PublicVariables._dtFromDate;
                }

                if (to == null)
                {
                    to = DateTime.UtcNow;
                }
                else
                {
                    to = Convert.ToDateTime(to)/*.AddDays(1)*/;
                }

                decimal finicialYearId = 0;
                if (finicialYearId == 0)
                {
                    finicialYearId = PublicVariables._decCurrentFinancialYearId;
                }

                var ledgerPosting = await context.tbl_LedgerPosting
                    .Include(a => a.tbl_AccountLedger)
                    .Include(a => a.tbl_AccountLedger.tbl_AccountGroup)
                    .Where(a => a.date >= from && a.date <= to /*&& a.yearId == finicialYearId*/)
                    .ToListAsync();

                var totalSalesIncome = 0.0m;
                var totalServiceIncome = 0.0m;
                var directExpenses = 0.0m;
                var otherIncome = 0.0m;
                var indirectExpense = 0.0m;
                var otherExpense = 0.0m;

                var totalRevenue = 0.0M;
                var grossprofit = 0.0M;
                var operatingProfit = 0.0M;
                var revenueList = new List<AccountDetailDto>();

                foreach (var i in ledgerPosting)
                {
                    var thisGroupId = i.tbl_AccountLedger.accountGroupId;
                    if (thisGroupId == AccountGroupConstants.SalesIncome)
                    {
                        //Sales Income  
                        totalSalesIncome += Convert.ToDecimal(i.credit - i.debit);
                    }

                    if (thisGroupId == AccountGroupConstants.DirectExpenses)
                    {
                        directExpenses += Convert.ToDecimal(i.debit - i.credit);
                    }

                    if (thisGroupId == AccountGroupConstants.OtherIncome)
                    {
                        if (i.tbl_AccountLedger != null && i.tbl_AccountLedger.ledgerName !=null && i.tbl_AccountLedger.ledgerName != "Exchange Gain/Loss")
                        {
                            otherIncome += Convert.ToDecimal(i.debit - i.credit);
                        }
                        else
                        {
                            otherIncome += Convert.ToDecimal(i.debit - i.credit);
                        }

                        //if (i.tbl_AccountLedger.ledgerName != "Exchange Gain/Loss")
                        //{
                        //    otherIncome += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit));
                        //}
                    }
                    //else
                    //{
                    //    otherIncome -= Convert.ToDecimal(i.credit - i.debit);
                    //}
                    if (thisGroupId == AccountGroupConstants.Expenses)
                    {
                        indirectExpense += Convert.ToDecimal(i.debit - i.credit);
                    }
                    if (thisGroupId == AccountGroupConstants.OtherExpenses)
                    {
                        otherExpense += Convert.ToDecimal(i.debit - i.credit);
                    }
                    if (thisGroupId == AccountGroupConstants.CostOfSale)
                    {
                        
                        listCostOfSaleL.Add(i.tbl_AccountLedger.ledgerName);
                        listCostOfSaleLA.Add(i.tbl_AccountLedger.ledgerId.ToString());
                        listCostOfSaleLAG.Add(i.voucherTypeId.ToString());
                    }
                }

                var list1 = JsonConvert.SerializeObject(listCostOfSaleL.ToList().Distinct().ToList());
                var list2 = JsonConvert.SerializeObject(listCostOfSaleLA.ToList().Distinct().ToList());
                var list3 = JsonConvert.SerializeObject(listCostOfSaleLAG.ToList().Distinct().ToList());


                var AcctLega = ledgerPosting.Select(a => a.tbl_AccountLedger).Distinct().ToList();
                foreach (var i in AcctLega)
                {
                    if (i.accountGroupId == AccountGroupConstants.SalesIncome)
                    {
                        revenueList.Add(new AccountDetailDto()
                        {
                            AccountName = i.ledgerName,
                            Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit - a.debit)),
                            Type = "R"
                        });

                    }
                }

                var finance = new FinicialServcies();
               
                var openingStock = finance.GetOpeningStock1(from, to);
               
                var AskOpenStock = GetOpeningStockers(new StockReportSearch() { BatchNo = "All", CategoryId = 0, ProductCode = "", ProductId = 0, ProjectId = 0, RefNo = "", StoreId = 0, FromDate = from.Value, ToDate = to.Value });

                if(AskOpenStock!=null)
                {
                    openingStock = AskOpenStock.OpeningStockValue;
                }

                var AskOpenStockTest = GetAllStockDetails(new StockReportSearch() { BatchNo = "All", CategoryId = 0, ProductCode = "", ProductId = 0, ProjectId = 0, RefNo = "", StoreId = 0, FromDate = from.Value, ToDate = to.Value });

                var purchases = await finance.GetPurchases(to.Value, from.Value);
                var closingStock = await finance.GetClosingStock1(from.Value, to);
                var inventory = finance.GetRemainingStock(from.Value, to.Value);

                if (AskOpenStockTest.Count() > 0 && AskOpenStockTest.ContainsKey("TotalStockValue"))
                {
                    inventory = Convert.ToDecimal(AskOpenStockTest["TotalStockValue"].ToString("#.##"));
                }

                revenueList.Add(new AccountDetailDto()
                {
                    AccountName = "",
                    Type = "R-H"

                });

                revenueList.Add(new AccountDetailDto()
                    {
                        AccountName = "Total Revenue",
                        Value = totalRevenue = totalServiceIncome + totalSalesIncome,
                        isSubTotal = true,
                        Type = "R"
                    });

                var accounts = new List<ProfitLossDto>()
                {             
                    new ProfitLossDto()
                {
                    AccountHeadLine = "Revenue",
                    AccountDetails = revenueList
                }           
                };

                accounts.Add(new ProfitLossDto()
                {
                    AccountHeadLine = "Cost Of Sales",
                    AccountDetails = new List<AccountDetailDto>()
                {
                    new AccountDetailDto()
                    {
                        AccountName = "Opening Stock",
                        Value = openingStock,
                        Type = "L"
                    },
                    new AccountDetailDto()
                    {
                        AccountName = "Purchases",
                        Value = purchases,
                        Type = "L"
                    },
                    new AccountDetailDto()
                    {
                        AccountName = "",
                        Type = "L-H"

                    },
                    new AccountDetailDto()
                    {
                        AccountName = "",
                        Value =  openingStock + purchases,
                        isSubTotal = true,
                        Type = "L"
                    },
                    new AccountDetailDto()
                    {
                        AccountName = "Direct Expenses",
                        Value = directExpenses,
                        Type = "L"
                    },
                    new AccountDetailDto()
                    {
                        AccountName = "",
                        Value =  openingStock + purchases + directExpenses ,
                        isSubTotal = true,
                        Type = "L"
                    },
                    new AccountDetailDto()
                    {
                        AccountName = "Closing Stock",
                        //Value = closingStock
                        Value = inventory //Augmetrix
                        , Type = "L"
                    },
                    new AccountDetailDto()
                    {
                        AccountName = "Total Cost of Sales",
                        Value = openingStock + purchases + directExpenses - inventory,
                        Type = "R",
                        isSubTotal = true
                    },
                    new AccountDetailDto()
                    {
                        AccountName = "",
                        Type = "R-H"
                    },
                    new AccountDetailDto()
                    {
                        AccountName = "Gross Profit and Loss",
                        Value = grossprofit = (totalSalesIncome + totalServiceIncome) - (directExpenses + openingStock + purchases - inventory),
                        isSubTotal = true,
                        Type = "R" //align to right

                    }
                }
                });

                accounts.Add(new ProfitLossDto()
                {
                    AccountHeadLine = "Other Income",
                    AccountDetails = new List<AccountDetailDto>
                {
                    new AccountDetailDto()
                    {
                        AccountName = "Other Income ",
                        Value = otherIncome,
                        Type = "R" //align to right

                    },
                    new AccountDetailDto()
                    {
                        AccountName = "",
                        Value = otherIncome + grossprofit,
                        Type = "R" ,//align to right
                        isSubTotal = true
                    },
                    new AccountDetailDto()
                    {
                        AccountName = "",
                        Type = "R-H"
                    }
                }
                });
                accounts.Add(new ProfitLossDto()
                {
                    AccountHeadLine = "Operating Expenses",
                    AccountDetails = new List<AccountDetailDto>()
                    {
                        new AccountDetailDto()
                        {
                            AccountName = "Operating Expenses",
                            Value = indirectExpense,
                            Type = "R" //align to right

                        },

                        new AccountDetailDto()
                        {
                            AccountName = "Operating Profit/Loss",
                            Value = operatingProfit = grossprofit + otherIncome - indirectExpense ,
                            Type = "R",
                            isSubTotal = true
                        },

                    }
                }

                );

                accounts.Add(new ProfitLossDto()
                {
                    AccountHeadLine = "Other Expenses",
                    AccountDetails = new List<AccountDetailDto>()
                    {

                        new AccountDetailDto()
                        {
                            AccountName = "Other Expenses",
                            Value = otherExpense,
                            Type = "R",
                            isSubTotal = true
                        },
                        new AccountDetailDto()
                        {
                            AccountName = "",
                            Type = "R-H"
                        },
                        new AccountDetailDto()
                        {
                            AccountName = "Net Profit/Loss",
                            Value = operatingProfit -  otherExpense,
                            Type = "R",
                            isSubTotal = true
                        },
                        new AccountDetailDto()
                        {
                            AccountName = "",
                            Type = "R-H"
                        },
                        new AccountDetailDto()
                        {
                            AccountName = "",
                            Type = "R-H"
                        },
                    }
                }

                );

                return Ok(accounts);
            }
            catch(Exception ex)
            {
                return null;
            }
       
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetProfitAndLossStandard(DateTime? from = null, DateTime? to = null)
        {
            if (from == null)
            {
                from = PublicVariables._dtFromDate;
            }

            if (to == null)
            {
                to = DateTime.UtcNow;
            }
            else
            {
                to = Convert.ToDateTime(to)/*.AddDays(1)*/;
            }

            var ledgers = await context.tbl_AccountLedger.Include(a => a.tbl_AccountGroup)
                .Include(a => a.tbl_LedgerPosting).ToListAsync();

            var totalSalesIncome = 0.0m;
           
            var totalcostOfSale = 0.0m;
            var directExpenses = 0.0m;
            var otherIncome = 0.0m;
            var indirectExpense = 0.0m;
            var otherExpense = 0.0m;

            var grossprofit = 0.0M;
            var operatingProfit = 0.0M;

            var costOfSalesGroup = new List<AccountDetailDto>();
            var directExpensList = new List<AccountDetailDto>();
            var otherExpensList = new List<AccountDetailDto>();
            var otherIncomeList = new List<AccountDetailDto>();
            var indirectExpenseList = new List<AccountDetailDto>();
            var revenueList = new List<AccountDetailDto>();
            var listCostOfSaleL = new List<string>();
            var listCostOfSaleLA = new List<string>();
            var listCostOfSaleLAG = new List<string>();

            foreach (var i in ledgers)
            {
                i.tbl_LedgerPosting = i.tbl_LedgerPosting.Where(a => a.date >= from && a.date <= to).ToList();
                var thisGroupId = i.accountGroupId;
                if (thisGroupId == AccountGroupConstants.SalesIncome)
                {
                    //Sales Income 
                  
                        revenueList.Add(new AccountDetailDto()
                        {
                            AccountName = i.ledgerName,
                            Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit - a.debit)),
                            Type = "R"
                        });

                   
                    totalSalesIncome += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit - a.debit));
                }

                if (thisGroupId == AccountGroupConstants.CostOfSale)
                {

                    //var listDeliveryNotes = new List<tbl_DeliveryNoteDetails>();
                    //decimal listTotalAmt = 0;

                    //var context = new DBMATAccounting_MagnetEntities1();
                    //var DeliveryNoteMasterList = new List<tbl_DeliveryNoteMaster>();

                    //var tbl_DeliveryNoteMaster = context.tbl_DeliveryNoteMaster.ToList();
                    //foreach (var itm in tbl_DeliveryNoteMaster)
                    //{
                    //    var itmS = context.tbl_SalesMaster.Where(pm => pm.deliveryNoteMasterId == itm.deliveryNoteMasterId).Any();
                    //    if (!itmS)
                    //    {
                    //        DeliveryNoteMasterList.Add(itm);
                    //    }
                    //}

                    //DeliveryNoteMasterList.ForEach(dm =>
                    //{
                    //    var dnd = context.tbl_DeliveryNoteDetails.Where(dd => dd.deliveryNoteMasterId == dm.deliveryNoteMasterId).ToList();
                    //    if (dnd != null)
                    //    {
                    //        listDeliveryNotes.AddRange(dnd);
                    //    }
                    //});

                    //listDeliveryNotes.ForEach(dn =>
                    //{
                    //    var dnd = context.tbl_RejectionInDetails.Where(dd => dd.deliveryNoteDetailsId == dn.deliveryNoteDetailsId && dd.productId == dn.productId).FirstOrDefault();
                    //    if (dnd != null)
                    //    {
                    //        var avCostProduct = StockReportDetails(dn.productId.Value);
                    //        var QtyRemain = dn.qty.Value - dnd.qty.Value;
                    //        var TotalAmt = avCostProduct * QtyRemain;

                    //        listTotalAmt += (TotalAmt);
                    //    }
                    //    else
                    //    {
                    //        var avCostProduct = StockReportDetails(dn.productId.Value);
                    //        var QtyRemain = dn.qty.Value;
                    //        var TotalAmt = avCostProduct * QtyRemain;

                    //        listTotalAmt += (TotalAmt);
                    //    }
                    //});

                    //var MaterialReceiptMasterList = context.tbl_MaterialReceiptMaster.ToList();
                    //var tbl_MaterialReceiptMaster = new List<tbl_MaterialReceiptMaster>();
                    //foreach (var itm in MaterialReceiptMasterList)
                    //{
                    //    var itmS = context.tbl_PurchaseMaster.Where(pm => pm.materialReceiptMasterId == itm.materialReceiptMasterId).Any();
                    //    if(!itmS)
                    //    {
                    //        tbl_MaterialReceiptMaster.Add(itm);
                    //    }
                    //}

                    //var listMaterialReceiptDetails = new List<tbl_MaterialReceiptDetails>();
                    //tbl_MaterialReceiptMaster.ForEach(dm =>
                    //{
                    //    var dnd = context.tbl_MaterialReceiptDetails.Where(dd => dd.materialReceiptMasterId == dm.materialReceiptMasterId).ToList();
                    //    if (dnd != null)
                    //    {
                    //        listMaterialReceiptDetails.AddRange(dnd);
                    //    }
                    //});


                    //listMaterialReceiptDetails.ForEach(dn =>
                    //{
                    //    var dnd = context.tbl_RejectionOutDetails.Where(dd => dd.materialReceiptDetailsId == dn.materialReceiptDetailsId && dd.productId == dn.productId).FirstOrDefault();
                    //    if (dnd != null)
                    //    {
                    //        var avCostProduct = StockReportDetails(dn.productId.Value);
                    //        var QtyRemain = dn.qty.Value - dnd.qty.Value;
                    //        var TotalAmt = avCostProduct * QtyRemain;

                    //        listTotalAmt += (TotalAmt);
                    //    }
                    //    else
                    //    {
                    //        var avCostProduct = StockReportDetails(dn.productId.Value);
                    //        var QtyRemain = dn.qty.Value;
                    //        var TotalAmt = avCostProduct * QtyRemain;

                    //        listTotalAmt += (TotalAmt);
                    //    }
                    //});

                    // (i.tbl_LedgerPosting.Sum(a => a.debit - a.credit));

                    if (110434 == i.ledgerId)
                    {
                        totalcostOfSale += GetDeliveryNoteAndMatrialReciept(); //Convert.ToDecimal
                        costOfSalesGroup.Add(new AccountDetailDto()
                        {
                            AccountName = i.ledgerName,
                            Value = GetDeliveryNoteAndMatrialReciept()
                        });
                    }
                    else
                    {
                        totalcostOfSale += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit));
                        costOfSalesGroup.Add(new AccountDetailDto()
                        {
                            AccountName = i.ledgerName,
                            Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit))
                        });
                    }

                    //listCostOfSaleL.Add(i.ledgerName);
                    ////listCostOfSaleLA.Add(i.ledgerId.ToString());
                    //foreach(var itm in i.tbl_LedgerPosting)
                    //{
                    //    listCostOfSaleLAG.Add(itm.voucherTypeId.ToString());
                    //}

                }
                if (thisGroupId == AccountGroupConstants.DirectExpenses)
                {
                    directExpenses += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit));
                    directExpensList.Add(new AccountDetailDto()
                    {
                        AccountName = i.ledgerName,
                        Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit))
                    });
                }             
                if (thisGroupId == AccountGroupConstants.OtherIncome)
                {
                    if (i.ledgerName != "Exchange Gain/Loss")
                    {
                        otherIncome += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit));

                        otherIncomeList.Add(new AccountDetailDto()
                        {
                            AccountName = i.ledgerName,
                            Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit)),
                            Type = "R"
                        });
                    }
                }
                if (thisGroupId == AccountGroupConstants.Expenses)
                {
                    indirectExpense += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit));

                    indirectExpenseList.Add(new AccountDetailDto()
                    {
                        AccountName = i.ledgerName,
                        Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit)) , 
                        Type = "R"
                    });
                }
                if (thisGroupId == AccountGroupConstants.OtherExpenses)
                {
                    otherExpense += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit ));
                    otherExpensList.Add(new AccountDetailDto()
                    {
                        AccountName = i.ledgerName,
                        Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit)),
                        Type = "R"

                    });
                }
            }

            listCostOfSaleLAG = listCostOfSaleLAG.ToList().Distinct().ToList();
            var list1 = JsonConvert.SerializeObject(listCostOfSaleL.ToList().Distinct().ToList());
            var list2 = JsonConvert.SerializeObject(listCostOfSaleLA.ToList().Distinct().ToList());
            var list3 = JsonConvert.SerializeObject(listCostOfSaleLAG.ToList().Distinct().ToList());

            costOfSalesGroup.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "L-H"

            });
            costOfSalesGroup.Add(new AccountDetailDto()
            {
                Value = totalcostOfSale,
                AccountName = "Total Cost",
                isSubTotal = true,
                Type = "L"
            });
            directExpensList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "L-H"

            });
            directExpensList.Add(new AccountDetailDto()
            {
                Value = directExpenses,
                AccountName = "Total Direct Expenses",
                isSubTotal = true,
                Type = "L"
            });
            directExpensList.Add(new AccountDetailDto()
            {
                Value = directExpenses + totalcostOfSale,
                AccountName = "Total Cost of Sales",
                isSubTotal = true,
                Type = "L"
            });
            directExpensList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
         
            directExpensList.Add(new AccountDetailDto()
            {
                Value = grossprofit = totalSalesIncome - totalcostOfSale + directExpenses ,
                AccountName = "Gross Profit/loss ",
                isSubTotal = true,
                Type = "R"
            });
            otherIncomeList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            otherIncomeList.Add(new AccountDetailDto()
            {
                Value = otherIncome + grossprofit,
                AccountName = "",
                isSubTotal = true,
                Type = "R"


            });
            indirectExpenseList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            indirectExpenseList.Add(new AccountDetailDto()
            {
                Value = indirectExpense,
                AccountName = "Total Operating Expenses",
                isSubTotal = true,
                Type = "R"

            });

            indirectExpenseList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            indirectExpenseList.Add(new AccountDetailDto()
            {
                Value = operatingProfit = grossprofit+ otherIncome - indirectExpense ,
                AccountName = "Operating profit/Loss",
                isSubTotal = true,
                Type = "R"
            });
            revenueList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            //otherExpensList.Add(new AccountDetailDto()
            //{
            //    Value = otherExpense,
            //    AccountName = "Total Other Expenses",
            //    isSubTotal = true,
            //    Type = "R"
            //});
            otherExpensList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            otherExpensList.Add(new AccountDetailDto()
            {
                Value = operatingProfit -  otherExpense,
                AccountName = "Net Profit/Loss",
                isSubTotal = true,
                Type = "R"
            });
            otherExpensList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            otherExpensList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            revenueList.Add(new AccountDetailDto()
                {
                    AccountName = "Total Revenue",
                    Value  =   totalSalesIncome,
                    isSubTotal = true,
                    Type = "R"
                });

            var accounts = new List<ProfitLossDto>()
            {
                new ProfitLossDto()
                {
                    AccountHeadLine = "Revenue",
                    AccountDetails = revenueList

                },
                new ProfitLossDto()
                {
                    AccountHeadLine = "Cost of Sales",
                    AccountDetails = costOfSalesGroup
                },

                new ProfitLossDto()
                {
                    AccountHeadLine = "Direct Expenses",
                    AccountDetails = directExpensList

                },
                new ProfitLossDto()
                { AccountHeadLine = "Other Income", AccountDetails = otherIncomeList },
                new ProfitLossDto()
                {
                    AccountHeadLine = "Operating Expenses",
                    AccountDetails = indirectExpenseList

                },
                new ProfitLossDto()
                {
                AccountHeadLine = "Other Expenses",
                AccountDetails = otherExpensList

            }
            };

            return Ok(accounts);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetBalanceSheetReport(DateTime? from = null, DateTime? to = null, string FinList = "")
        {
            if (from == null)
            {
                from = PublicVariables._dtFromDate;
            }

            if (to == null)
            {
                to = PublicVariables._dtToDate;
            }

            //to = to == null ? DateTime.MaxValue : Convert.ToDateTime(to).AddDays(1); 

                var ledgers = await context.tbl_AccountLedger.Include(a => a.tbl_AccountGroup).Include(a => a.tbl_LedgerPosting).ToListAsync();

                var propertyList = new List<AccountBalanceSheetDto>();
                var totalPropertyList = new List<AccountBalanceSheetDto>();

                var otherAssets  = new List<AccountBalanceSheetDto>();
                var totalOtherAssets  = new List<AccountBalanceSheetDto>();

                var currentAssets = new List<AccountBalanceSheetDto>();
                var totalCurrentAssets  = new List<AccountBalanceSheetDto>();

                 var equityReserve = new List<AccountBalanceSheetDto>();
                 var totalEquityReserve = new List<AccountBalanceSheetDto>();

                var nonCurrentLiabilty = new List<AccountBalanceSheetDto>();
                var totalNonCurrentLiabilty = new List<AccountBalanceSheetDto>();

                var currentLiablity = new List<AccountBalanceSheetDto>();
                var totalcurrentLiablity = new List<AccountBalanceSheetDto>();

            var HasFinanacialYearSearch = false;
            List<DatePickerModel> FinanacialYearSearch = null;
            decimal inventory = 0;
            decimal profitLoss = 0;

            try
            {
                FinanacialYearSearch = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DatePickerModel>>(FinList);
                if(FinanacialYearSearch!=null && FinanacialYearSearch.Count() >0)
                {
                    HasFinanacialYearSearch = true;
                }
               
            }
            catch(Exception ex)
            {
                HasFinanacialYearSearch = false;
            }

            //var AskOpenStock = GetOpeningStockers(new StockReportSearch() { BatchNo = "All", CategoryId = 0, ProductCode = "", ProductId = 0, ProjectId = 0, RefNo = "", StoreId = 0, FromDate = from.Value, ToDate = to.Value });

            //if (AskOpenStock != null)
            //{
            //    inventory = AskOpenStock.totalStockValue;
            //}

            var AskOpenStock = GetAllStockDetails(new StockReportSearch() { BatchNo = "All", CategoryId = 0, ProductCode = "", ProductId = 0, ProjectId = 0, RefNo = "", StoreId = 0, FromDate = from.Value, ToDate = to.Value });

            if (AskOpenStock.Count() > 0 && AskOpenStock.ContainsKey("TotalStockValue"))
            {
                inventory = Convert.ToDecimal(AskOpenStock["TotalStockValue"].ToString("#.##"));
            }


            if (HasFinanacialYearSearch)
            {
                //inventory = new FinicialServcies().GetRemainingStockFinYear(from.Value,  to.Value, FinanacialYearSearch);

                profitLoss = await (new FinicialServcies().GetProfitLoss(from.Value, to.Value));

                if (AskOpenStock.Count() > 0 && AskOpenStock.ContainsKey("TotalStockValue"))
                {
                    inventory = Convert.ToDecimal(AskOpenStock["TotalStockValue"].ToString("#.##"));
                }
            }
            else
            {
                //inventory = new FinicialServcies().GetRemainingStock(to.Value);

                if (AskOpenStock.Count() > 0 && AskOpenStock.ContainsKey("TotalStockValue"))
                {
                    inventory = Convert.ToDecimal(AskOpenStock["TotalStockValue"].ToString("#.##"));
                }

                profitLoss = await (new FinicialServcies().GetProfitLoss(from.Value, to.Value));
            }

            var ListLastFyear = context.tbl_FinancialYear.Where(fy => fy.fromDate < from.Value && fy.toDate < from.Value).ToList();

            var BalanceDateDate = from.Value;

            if (ListLastFyear.Count > 0)
            {
                BalanceDateDate = ListLastFyear.Min(cfy => cfy.fromDate.Value);
            }

            foreach (var i in ledgers)
            {
                    i.tbl_LedgerPosting = i.tbl_LedgerPosting.Where(a => a.date >= BalanceDateDate && a.date <= to.Value).ToList();

                    var thisGroupId = i.accountGroupId;
                    var sumDeb = i.tbl_LedgerPosting.Sum(a => a.debit);
                    var sumCr = i.tbl_LedgerPosting.Sum(a => a.credit); 
                   
                if(sumCr == 0 && sumDeb == 0)
                    {
                        continue;
                    }
                  
                if (thisGroupId == AccountGroupConstants.properties)
                    {
                        // Cost 
                        var debit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit));  
                        // Accumulated Depreciation
                        var credit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit));  
                        // Net Book Value  (nbv)
                        var nbv = debit - credit;
                        propertyList.Add(new AccountBalanceSheetDto()
                        {
                            AccountName = i.ledgerName , 
                            Cost =  debit,
                            AccDep =  credit,
                            Nbv =  nbv, 
                            Type = "L-C-R"
                        });
                    }

                if (thisGroupId == AccountGroupConstants.otherAssets)
                    {
                        // Cost 
                        var debit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit));
                        // Accumulated Depreciation
                        var credit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit));
                        // Net Book Value  (nbv)
                        var nbv = debit - credit;
                        otherAssets.Add(new AccountBalanceSheetDto()
                        {
                            AccountName = i.ledgerName,
                            Cost = debit,
                            AccDep = credit,
                            Nbv = nbv, 
                            Type = "C"
                        });
                    }

                if (thisGroupId == AccountGroupConstants.currentAssets) 
                    {
                        // Cost 
                        var debit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit));
                        // Accumulated Depreciation
                        var credit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit));
                        // Net Book Value  (nbv)
                        var nbv = debit - credit;
                        currentAssets.Add(new AccountBalanceSheetDto()
                        {
                            AccountName = i.ledgerName,
                            Cost = debit,
                            AccDep = credit,
                            Nbv = nbv,
                            Type = "C"
                        });
                    }
                   
                if (thisGroupId == AccountGroupConstants.cash)
                    {
                        // Cost 
                        var debit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit));
                        // Accumulated Depreciation
                        var credit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit));
                        // Net Book Value  (nbv)
                        var nbv = debit - credit;
                        currentAssets.Add(new AccountBalanceSheetDto()
                        {
                            AccountName = i.ledgerName,
                            Cost = debit,
                            AccDep = credit,
                            Nbv = nbv,
                            Type = "C"
                        });
                    }

                if (thisGroupId == AccountGroupConstants.bank)
                    {

                        // Cost 

                        var debit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit));

                        // Accumulated Depreciation
                        var credit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit));

                        // Net Book Value  (nbv)  

                        var nbv = debit - credit;
                        currentAssets.Add(new AccountBalanceSheetDto()
                        {
                            AccountName = i.ledgerName,
                            Cost = debit,
                            AccDep = credit,
                            Nbv = nbv,
                            Type = "C"
                        });
                    }

                if (thisGroupId == AccountGroupConstants.equity_reserve)
                    {
                        // Cost 
                        var debit = Convert.ToDecimal(i.tbl_LedgerPosting/*.Where(c=> c.credit <=0 && c.debit > 0)*/.Sum(a => a.debit));
                        // Accumulated Depreciation
                        var credit = Convert.ToDecimal(i.tbl_LedgerPosting/*.Where(c => c.debit <= 0 && c.credit > 0)*/.Sum(a => a.credit));
                        // Net Book Value  (nbv)
                        var nbv = credit - debit; 
                        equityReserve.Add(new AccountBalanceSheetDto()
                        {
                            AccountName = i.ledgerName,
                            Cost = debit,
                            AccDep = credit,
                            Nbv = nbv,
                            Type = "C"

                        });
                    }

                if (thisGroupId == AccountGroupConstants.DutiesAndTaxes)
                {
                    // Cost 
                    var debit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit));
                    // Accumulated Depreciation
                    var credit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit));
                    // Net Book Value  (nbv)
                    var nbv = credit - debit;
                    equityReserve.Add(new AccountBalanceSheetDto()
                    {
                        AccountName = i.ledgerName,
                        Cost = debit,
                        AccDep = credit,
                        Nbv = nbv,
                        Type = "C"

                    });
                }

                if (thisGroupId == AccountGroupConstants.nonCurrentLiabilities)
                    {
                        // Cost 
                        var debit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit));
                        // Accumulated Depreciation
                        var credit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit));
                        // Net Book Value  (nbv)
                        var nbv =  credit -debit;
                        nonCurrentLiabilty.Add(new AccountBalanceSheetDto()
                        {
                            AccountName = i.ledgerName,
                            Cost = debit,
                            AccDep = credit,
                            Nbv = nbv,
                            Type = "C"

                        });
                    }
                    
                if (thisGroupId == AccountGroupConstants.currentLiablity)
                    {
                        // Cost 
                        var debit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit));
                        // Accumulated Depreciation
                        var credit = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit));
                        // Net Book Value  (nbv)
                        var nbv =  credit -debit;
                        currentLiablity.Add(new AccountBalanceSheetDto()
                        {
                            AccountName = i.ledgerName,
                            Cost = debit,
                            AccDep = credit,
                            Nbv = nbv,
                            Type = "C"

                        });
                    }
            }

            currentAssets.Add(new AccountBalanceSheetDto()
            {
                AccountName = "Stock",
                Cost = 0 , 
                AccDep = 0,
                Nbv = inventory,
                Type = "C"

            });

            var ledgerService = new LedgerServices();
            var accountRecievable = await ledgerService.GetAccountRecieveAble(from.Value, to.Value);
            if (accountRecievable != 0)
            {
                currentAssets.Add(new AccountBalanceSheetDto()
                {
                    AccountName = "Account Receivable",
                    Cost = 0,
                    AccDep = 0,
                    Nbv = accountRecievable,
                    Type = "C"

                });
            }

            var accountPayable = ledgerService.GetAccountPayable(from.Value, to.Value);
            if (accountPayable != 0)
            {
                currentLiablity.Add(new AccountBalanceSheetDto()
                {
                    AccountName = "Account Payable",
                    Cost = 0,
                    AccDep = 0,
                    Nbv = accountPayable,
                    Type = "C"
                });
            }
           
            equityReserve.Add(new AccountBalanceSheetDto()
            {
                AccountName = "Profit and Loss",
                Cost = 0,
                AccDep = 0,
                Nbv = profitLoss,
                Type = "C"
            });

            totalPropertyList.Add(new AccountBalanceSheetDto()
                    {
                        AccountName = "Sub total",
                        Cost = propertyList.Sum(a => a.Cost),
                        AccDep = propertyList.Sum(a => a.AccDep),

                        Nbv = propertyList.Sum(a => a.Nbv),
                        isSubTotal = true,
                        Type = "L-C-R"

            });
            totalPropertyList.Add(new AccountBalanceSheetDto()
            {
                AccountName = "",
                Type = "C-R-H"
            });
           

            totalOtherAssets.Add(new AccountBalanceSheetDto()
            {
                AccountName = "Total Other Assets",
                //Nbv = otherAssets.Sum(a => a.Cost) + otherAssets.Sum(a => a.Cost) - otherAssets.Sum(a => a.AccDep) +
                //      otherAssets.Sum(a => a.AccDep), 
                Nbv = otherAssets.Sum(a=>a.Nbv),
                Type = "R",
                isSubTotal = true

            });
            totalOtherAssets.Add(new AccountBalanceSheetDto()
            {
                AccountName = "",
                Type = "C-R-H"
            });

            totalCurrentAssets.Add(new AccountBalanceSheetDto()
            {
                AccountName = "Total current assets",
                //Nbv = currentAssets.Sum(a => a.Cost) - currentAssets.Sum(a => a.AccDep), 
                Nbv =  currentAssets.Sum(a=>a.Nbv),
                Type = "R",
                isSubTotal = true

            });
            totalCurrentAssets.Add(new AccountBalanceSheetDto()
            {
                AccountName = "",
                Type = "C-R-H"
            });

            var propertyToAdd = new List<AccountBalanceSheetDto>();

            propertyToAdd.AddRange(propertyList);
            propertyToAdd.AddRange(totalPropertyList);
                    var otherAssetToAdd = new List<AccountBalanceSheetDto>() ; 

            otherAssetToAdd.AddRange(otherAssets);
            otherAssetToAdd.AddRange(totalOtherAssets);

            var currentAssetToAdd = new List<AccountBalanceSheetDto>();
            currentAssetToAdd.AddRange(currentAssets); 
            currentAssetToAdd.AddRange(totalCurrentAssets);
            var totCura = currentAssets.Sum(a => a.Nbv);
            var totOtherA = otherAssets.Sum(a => a.Nbv);
            var totalPro = propertyList.Sum(a => a.Nbv);
            currentAssetToAdd.Add(new AccountBalanceSheetDto()
            {
                AccountName = "--------Total Assets--------",
                Cost = currentAssets.Sum(a => a.Cost) + otherAssets.Sum(a => a.Cost),
                AccDep =  currentAssets.Sum(a => a.AccDep) + otherAssets.Sum(a => a.AccDep),
                //Nbv = currentAssets.Sum(a => a.Cost) + otherAssets.Sum(a => a.Cost) - currentAssets.Sum(a => a.AccDep) + otherAssets.Sum(a => a.AccDep),
               Nbv = currentAssets.Sum(a => a.Nbv) + otherAssets.Sum(a => a.Nbv) + propertyList.Sum(a=>a.Nbv),
                Type = "R",
                isSubTotal = true
            });
            currentAssetToAdd.Add(new AccountBalanceSheetDto()
            {
                AccountName = "",
                Type = "R"
            });

            decimal acumCost = 0, acumAccDep=0;  
            totalEquityReserve.Add(new AccountBalanceSheetDto()
            {
                AccountName = "Total Equity and Reserve",
                //Nbv = equityReserve.Sum(a => a.Cost) - equityReserve.Sum(a => a.AccDep),
                Nbv = equityReserve.Sum(a => a.Nbv),
                Type = "R",
                isSubTotal = true

            });
            totalEquityReserve.Add(new AccountBalanceSheetDto()
            {
                AccountName = "",
                Type = "C-R-H"
            });
            totalNonCurrentLiabilty.Add(new AccountBalanceSheetDto()
            {
                AccountName = "Total Non-current Liability",
                Nbv = nonCurrentLiabilty.Sum(a => a.Nbv) ,
                Type = "R",
                isSubTotal = true
            });
            totalNonCurrentLiabilty.Add(new AccountBalanceSheetDto()
            {
                AccountName = "",
                Type = "C-R-H"
            });
            totalcurrentLiablity.Add(new AccountBalanceSheetDto()
            {
                AccountName = "Total Current Liability",
                Nbv = currentLiablity.Sum(a => a.Nbv),
                Type = "R-H",
                isSubTotal = true
            });
            totalcurrentLiablity.Add(new AccountBalanceSheetDto()
            {
                AccountName = "",
                Type = "C-R-H"
            });

            var equatiyToAdd = new List<AccountBalanceSheetDto>();
           equatiyToAdd.AddRange(equityReserve);
           equatiyToAdd.AddRange(totalEquityReserve);

           var nonCurrentLibalityToAdd = new List<AccountBalanceSheetDto>();
           nonCurrentLibalityToAdd.AddRange(nonCurrentLiabilty);
           nonCurrentLibalityToAdd.AddRange(totalNonCurrentLiabilty);

           var currentLibalityToAdd = new List<AccountBalanceSheetDto>();
           currentLibalityToAdd.AddRange(currentLiablity);  
           currentLibalityToAdd.Add(new AccountBalanceSheetDto()
            {
                AccountName = "--------Total Equity and Libilities--------",
                Cost = acumCost = currentLiablity.Sum(a => a.Cost) + nonCurrentLiabilty.Sum(a => a.Cost) + equityReserve.Sum(a => a.Cost),
                AccDep  = acumAccDep = currentLiablity.Sum(a => a.AccDep) + nonCurrentLiabilty.Sum(a => a.AccDep) + equityReserve.Sum(a => a.AccDep),
                Nbv = currentLiablity.Sum(a => a.Nbv) + nonCurrentLiabilty.Sum(a => a.Nbv) + equityReserve.Sum(a => a.Nbv),
                Type = "R",
                isSubTotal = true
            });
           currentLibalityToAdd.Add(new AccountBalanceSheetDto()
           {
               AccountName = "",
               Type = "R-H"
           });
            var balanceSheet = new  List<BalanceSheetDto>(){
                   new BalanceSheetDto()
                   {
                       AccountHeadLine = "Non Current Assets",
                       AccountDetails = new List<AccountBalanceSheetDto>().ToList()
                   },
                   new BalanceSheetDto()
                   {
                       AccountHeadLine = "Properties, Plant and Equipments",
                       AccountDetails = propertyToAdd
                   },
                   new BalanceSheetDto()
                   {
                       AccountHeadLine = "Other Assets",
                       AccountDetails =  otherAssetToAdd
                   },
                   new BalanceSheetDto()
                   {
                       AccountHeadLine = "Current Assets",
                       AccountDetails = currentAssetToAdd
                   },

                   new BalanceSheetDto()
                   {
                       AccountHeadLine = "Equities and Liability",
                       AccountDetails = new List<AccountBalanceSheetDto>().ToList()
                   },
                   new BalanceSheetDto()
                   {
                       AccountHeadLine = "Equity and Reserves",
                       AccountDetails = equatiyToAdd
                   },
                   new BalanceSheetDto()
                   {
                       AccountHeadLine = "Non current Liability",
                       AccountDetails = nonCurrentLibalityToAdd
                   },
                   new BalanceSheetDto()
                   {
                       AccountHeadLine = "Current Liability",
                       AccountDetails = currentLibalityToAdd
                   },
               };
                
           return Ok(balanceSheet);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetProfitAndLossByProject(DateTime? from = null, DateTime? to = null, int? projectId =null)
        {
            if (from == null)
            {
                from = PublicVariables._dtFromDate;
            }

            if (to == null)
            {
                to = DateTime.UtcNow;
            }

           /* var musclesLambda = db.Muscles
            .Where(m => arrayOfLongs.Contains(m.MainMusleGroupID))
            .Select(m => new { m.MusleName, m.ID })
            .Take(40);*/

            //Get all Id from the consolidated view transaction for the particular project.
            
            List<string> ListofSelectedLId = new List<string>();

            var ledgers = await context.tbl_AccountLedger
                .Include(a => a.tbl_AccountGroup)
                .Include(a => a.tbl_LedgerPosting.Where(b => ListofSelectedLId.Contains(b.voucherNo)))
                .ToListAsync();



            if (projectId != null && projectId != 0)
            {
                //ledgers.Where(a => a.tbl_LedgerPosting.Where());

            }


            var totalSalesIncome = 0.0m;

            var totalcostOfSale = 0.0m;
            var directExpenses = 0.0m;
            var otherIncome = 0.0m;
            var indirectExpense = 0.0m;
            var otherExpense = 0.0m;


            var grossprofit = 0.0M;
            var operatingProfit = 0.0M;

            var costOfSalesGroup = new List<AccountDetailDto>();
            var directExpensList = new List<AccountDetailDto>();
            var otherExpensList = new List<AccountDetailDto>();
            var otherIncomeList = new List<AccountDetailDto>();
            var indirectExpenseList = new List<AccountDetailDto>();
            var revenueList = new List<AccountDetailDto>();
            foreach (var i in ledgers)
            {
                i.tbl_LedgerPosting = i.tbl_LedgerPosting.Where(a => a.date >= from && a.date <= to).ToList();
                var thisGroupId = i.accountGroupId;
                if (thisGroupId == AccountGroupConstants.SalesIncome)
                {
                    //Sales Income 

                    revenueList.Add(new AccountDetailDto()
                    {
                        AccountName = i.ledgerName,
                        Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit - a.debit)),
                        Type = "R"
                    });


                    totalSalesIncome += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit - a.debit));
                }



                if (thisGroupId == AccountGroupConstants.CostOfSale)
                {
                    totalcostOfSale += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit));

                    costOfSalesGroup.Add(new AccountDetailDto()
                    {
                        AccountName = i.ledgerName,
                        Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit))
                    });
                }

                if (thisGroupId == AccountGroupConstants.DirectExpenses)
                {
                    directExpenses += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit));
                    directExpensList.Add(new AccountDetailDto()
                    {
                        AccountName = i.ledgerName,
                        Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit))
                    });
                }

                if (thisGroupId == AccountGroupConstants.OtherIncome)
                {
                    otherIncome += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit - a.debit));
                    otherIncomeList.Add(new AccountDetailDto()
                    {
                        AccountName = i.ledgerName,
                        Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.credit - a.debit)),
                        Type = "R"
                    });
                }

                if (thisGroupId == AccountGroupConstants.DirectExpenses)
                {
                    indirectExpense += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit));

                    indirectExpenseList.Add(new AccountDetailDto()
                    {
                        AccountName = i.ledgerName,
                        Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit)),
                        Type = "R"
                    });
                }
                if (thisGroupId == AccountGroupConstants.OtherExpenses)
                {
                    otherExpense += Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit));
                    otherExpensList.Add(new AccountDetailDto()
                    {
                        AccountName = i.ledgerName,
                        Value = Convert.ToDecimal(i.tbl_LedgerPosting.Sum(a => a.debit - a.credit)),
                        Type = "R"

                    });
                }
            }

            costOfSalesGroup.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "L-H"

            });
            costOfSalesGroup.Add(new AccountDetailDto()
            {
                Value = totalcostOfSale,
                AccountName = "Total Cost",
                isSubTotal = true,
                Type = "L"
            });
            directExpensList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "L-H"

            });
            directExpensList.Add(new AccountDetailDto()
            {
                Value = directExpenses,
                AccountName = "Total Direct Expenses",
                isSubTotal = true,
                Type = "L"
            });
            directExpensList.Add(new AccountDetailDto()
            {
                Value = directExpenses + totalcostOfSale,
                AccountName = "Total Cost of Sales",
                isSubTotal = true,
                Type = "L"
            });
            directExpensList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });

            directExpensList.Add(new AccountDetailDto()
            {
                Value = grossprofit = totalSalesIncome - totalcostOfSale + directExpenses,
                AccountName = "Gross Profit/loss ",
                isSubTotal = true,
                Type = "R"
            });
            otherIncomeList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            otherIncomeList.Add(new AccountDetailDto()
            {
                Value = otherIncome + grossprofit,
                AccountName = "",
                isSubTotal = true,
                Type = "R"


            });
            indirectExpenseList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            indirectExpenseList.Add(new AccountDetailDto()
            {
                Value = indirectExpense,
                AccountName = "Total Operating Expenses",
                isSubTotal = true,
                Type = "R"

            });

            indirectExpenseList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            indirectExpenseList.Add(new AccountDetailDto()
            {
                Value = operatingProfit = grossprofit + otherIncome - indirectExpense,
                AccountName = "Operating profit/Loss",
                isSubTotal = true,
                Type = "R"
            });
            revenueList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            //otherExpensList.Add(new AccountDetailDto()
            //{
            //    Value = otherExpense,
            //    AccountName = "Total Other Expenses",
            //    isSubTotal = true,
            //    Type = "R"
            //});
            otherExpensList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            otherExpensList.Add(new AccountDetailDto()
            {
                Value = operatingProfit - otherExpense,
                AccountName = "Net Profit/Loss",
                isSubTotal = true,
                Type = "R"
            });
            otherExpensList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            otherExpensList.Add(new AccountDetailDto()
            {
                AccountName = "",
                Type = "R-H"

            });
            revenueList.Add(
                new AccountDetailDto()
                {
                    AccountName = "Total Revenue",
                    Value = totalSalesIncome,
                    isSubTotal = true,
                    Type = "R"
                });

            var accounts = new List<ProfitLossDto>()
            {
                new ProfitLossDto()
                {
                    AccountHeadLine = "Revenue",
                    AccountDetails = revenueList

                },
                new ProfitLossDto()
                {
                    AccountHeadLine = "Cost of Sales",
                    AccountDetails = costOfSalesGroup
                },

                new ProfitLossDto()
                {
                    AccountHeadLine = "Direct Expenses",
                    AccountDetails = directExpensList

                },
                new ProfitLossDto()
                {
                AccountHeadLine = "Other Income",
                AccountDetails = otherIncomeList

                  },
                new ProfitLossDto()
                {
                    AccountHeadLine = "Operating Expenses",
                    AccountDetails = indirectExpenseList

                },
                new ProfitLossDto()
                {
                AccountHeadLine = "Other Expenses",
                AccountDetails = otherExpensList

            }
            };

            return Ok(accounts);
        }

        [HttpPost]
        public async Task<DatePickerModel> GetDateRange(List<DatePickerModel> AllDate)
        {
            
            try
            {
               // var listDates = Newtonsoft.Json.JsonConvert.DeserializeObject<List<DatePickerModel>>(AllDate);

                if(AllDate==null || AllDate.Count < 0)
                {
                    return null;
                }

                var ListAllFinancialDate = new List<DateTime>();

                foreach (var itm in AllDate)
                {
                    ListAllFinancialDate.Add(new DateTime(itm.FiFromYear, itm.FiFromMonth, itm.FiFromDay));
                    ListAllFinancialDate.Add(new DateTime(itm.FiToYear, itm.FiToMonth, itm.FiToDay));
                }

                var minDate = ListAllFinancialDate.Min(x => x.Date);

             //   var LowestDate = ListAllFinancialDate.OrderBy(x => x.Date).Select(x => x.Date).FirstOrDefault();

               // LowestDate = ListAllFinancialDate.OrderByDescending(x => x.Date).Select(x => x.Date).FirstOrDefault();

                var maxDate = ListAllFinancialDate.Max(x => x.Date);

              //  maxDate = ListAllFinancialDate.OrderByDescending(x => x.Date).Select(x => x.Date).First();

                var BackDate = new DatePickerModel() { FiToDay = maxDate.Day, FiToMonth = maxDate.Month, FiToYear = maxDate.Year, FiFromYear = minDate.Year, FiFromMonth = minDate.Month, FiFromDay = minDate.Day };

                return BackDate;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public async Task<IHttpActionResult> ChangeDateRange(string AllDate)
        {
            try
            {
                var listDates = Newtonsoft.Json.JsonConvert.DeserializeObject<DatePickerModel>(AllDate);

            
                return Ok(listDates);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }


    public class StockComputationObject
    {
        public decimal BalanceQuantity { get; set; }	
        public decimal AggregateValue { get; set; }	
        public decimal AverageCost { get; set; }
    }


}
