﻿using MatApi.Models.Reports.FinancialStatements;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Reports.FinancialStatements
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CustomBankRegisterController : ApiController
    {
        //[HttpPost]
        //public List<CustomBankRegisterVM> BankRegister(CustomBankRegisterParam param)
        //{
        //    List<CustomBankRegisterVM> bankRegisterList = new List<CustomBankRegisterVM>();
        //    string query = string.Format("select tbl_ledgerposting.ledgerpostingid,tbl_ledgerposting.ledgerid,"+
        //                "tbl_accountledger.ledgername, tbl_accountgroup.accountgroupid, tbl_accountgroup.accountgroupname,"+
        //                "tbl_ledgerposting.voucherno, tbl_ledgerposting.date, tbl_ledgerposting.vouchertypeid,"+
        //                "tbl_vouchertype.vouchertypename, tbl_ledgerposting.credit, tbl_ledgerposting.debit "+
        //                "from tbl_ledgerposting "+
        //                "inner join tbl_accountledger on tbl_accountledger.ledgerid = tbl_ledgerposting.ledgerid "+
        //                "inner join tbl_vouchertype on tbl_vouchertype.vouchertypeid = tbl_ledgerposting.vouchertypeid "+
        //                "inner join tbl_accountgroup on tbl_accountgroup.accountgroupid = tbl_accountledger.accountgroupid "+
        //                "where tbl_ledgerposting.vouchertypeid = 5 and tbl_accountledger.accountgroupid in(28, 26) and "+
        //                "tbl_ledgerposting.date>='{0}' and tbl_ledgerposting.date<='{1}'"
        //        ,param.FromDate,param.ToDate);

        //    DataTable bankRegisterDt = new DBMatConnection().customSelect(query);
        //    for(int i=0;i<bankRegisterDt.Rows.Count;i++)
        //    {
        //        bankRegisterList.Add(new CustomBankRegisterVM {
        //            AccountGroupId=Convert.ToDecimal(bankRegisterDt.Rows[i].ItemArray[3].ToString()),
        //            AccountGroupName= bankRegisterDt.Rows[i].ItemArray[4].ToString(),
        //            Credit= Convert.ToDecimal(bankRegisterDt.Rows[i].ItemArray[9].ToString()),
        //            Date=Convert.ToDateTime(bankRegisterDt.Rows[i].ItemArray[6].ToString()),
        //            Debit=Convert.ToDecimal(bankRegisterDt.Rows[i].ItemArray[10].ToString()),
        //            LedgerId=Convert.ToDecimal(bankRegisterDt.Rows[i].ItemArray[1].ToString()),
        //            LedgerName= bankRegisterDt.Rows[i].ItemArray[2].ToString(),
        //            LedgerPostingId=Convert.ToDecimal(bankRegisterDt.Rows[i].ItemArray[0].ToString()),
        //            VoucherNo= bankRegisterDt.Rows[i].ItemArray[5].ToString(),
        //            VoucherTypeId=Convert.ToDecimal(bankRegisterDt.Rows[i].ItemArray[7].ToString()),
        //            VoucherTypeName= bankRegisterDt.Rows[i].ItemArray[8].ToString()
        //        });
        //    }
        //    var groupedCustomerList = bankRegisterList
        //            .GroupBy(u => u.VoucherNo)
        //            //.Select(grp => grp.ToList())
        //            .ToList();
        //    for(int i=0;i< groupedCustomerList.Count;i++)
        //    {
        //       // groupedCustomerList[i].
        //    }
        //   // var k = groupedCustomerList.Where(p=>p.);
        //    var banksDt = new TransactionsGeneralFill().BankComboFill();
        //    List<AccountLedgerInfo> allBanks = new List<AccountLedgerInfo>();
        //    for(int i=0;i<banksDt.Rows.Count;i++)
        //    {
        //        allBanks.Add(new AccountLedgerInfo {
        //            LedgerId= Convert.ToDecimal(banksDt.Rows[i].ItemArray[1].ToString()),
        //            LedgerName= banksDt.Rows[i].ItemArray[0].ToString()
        //        });
        //    }
        //    List<CustomBankRegisterVM> banksInLedgerPosting = bankRegisterList.Where(p=>p.AccountGroupId==28).ToList();
        //    List<CustomBankRegisterVM> agents= bankRegisterList.Where(p => p.AccountGroupId == 26).ToList();

        //    List<CustomBankRegisterResponseVM> response = new List<CustomBankRegisterResponseVM>();
        //    for (int i = 0; i < allBanks.Count; i++)
        //    {
        //        List<CustomBankRegisterVM> bankAgentList = new List<CustomBankRegisterVM>();
        //        for(int k=0;k< banksInLedgerPosting.Count;k++)
        //        {
        //            //bankAgentList = agents.Where(p => p.VoucherNo == banksInLedgerPosting[k].VoucherNo).ToList();
        //        }
        //        for (int j = 0; j < bankRegisterList.Count; j++)
        //        {

        //            //bankAgentList = agents.Where(p => p.VoucherNo == banksInLedgerPosting[k].VoucherNo).ToList();
        //        }


        //        response.Add(new CustomBankRegisterResponseVM
        //        {
        //            BankId= allBanks[i].LedgerId,
        //            BankName= allBanks[i].LedgerName,
        //            AgentLists = bankAgentList
        //        });
        //    }
        //    return response;
        //}

        [HttpPost]
        public List<CustomBankRegisterResponseVM> BankRegister(CustomBankRegisterParam param)
        {
            List<CustomBankRegisterVM> bankRegisterList = new List<CustomBankRegisterVM>();
            string query = string.Format("select tbl_ledgerposting.ledgerpostingid,tbl_ledgerposting.ledgerid," +
                        "tbl_accountledger.ledgername, tbl_accountgroup.accountgroupid, tbl_accountgroup.accountgroupname," +
                        "tbl_ledgerposting.voucherno, tbl_ledgerposting.date, tbl_ledgerposting.vouchertypeid," +
                        "tbl_vouchertype.vouchertypename, tbl_ledgerposting.credit, tbl_ledgerposting.debit " +
                        "from tbl_ledgerposting " +
                        "inner join tbl_accountledger on tbl_accountledger.ledgerid = tbl_ledgerposting.ledgerid " +
                        "inner join tbl_vouchertype on tbl_vouchertype.vouchertypeid = tbl_ledgerposting.vouchertypeid " +
                        "inner join tbl_accountgroup on tbl_accountgroup.accountgroupid = tbl_accountledger.accountgroupid " +
                        "where tbl_ledgerposting.vouchertypeid = 5 and tbl_accountledger.accountgroupid in(28, 26) and " +
                        "tbl_ledgerposting.date>='{0}' and tbl_ledgerposting.date<='{1}'"
                , param.FromDate, param.ToDate);

            DataTable bankRegisterDt = new DBMatConnection().customSelect(query);
            for (int i = 0; i < bankRegisterDt.Rows.Count; i++)
            {
                bankRegisterList.Add(new CustomBankRegisterVM
                {
                    AccountGroupId = Convert.ToDecimal(bankRegisterDt.Rows[i].ItemArray[3].ToString()),
                    AccountGroupName = bankRegisterDt.Rows[i].ItemArray[4].ToString(),
                    Credit = Convert.ToDecimal(bankRegisterDt.Rows[i].ItemArray[9].ToString()),
                    Date = Convert.ToDateTime(bankRegisterDt.Rows[i].ItemArray[6]).ToString(),
                    Debit = Convert.ToDecimal(bankRegisterDt.Rows[i].ItemArray[10].ToString()),
                    LedgerId = Convert.ToDecimal(bankRegisterDt.Rows[i].ItemArray[1].ToString()),
                    LedgerName = bankRegisterDt.Rows[i].ItemArray[2].ToString(),
                    LedgerPostingId = Convert.ToDecimal(bankRegisterDt.Rows[i].ItemArray[0].ToString()),
                    VoucherNo = bankRegisterDt.Rows[i].ItemArray[5].ToString(),
                    VoucherTypeId = Convert.ToDecimal(bankRegisterDt.Rows[i].ItemArray[7].ToString()),
                    VoucherTypeName = bankRegisterDt.Rows[i].ItemArray[8].ToString()
                });
            }
            
            var banksDt = new TransactionsGeneralFill().BankComboFill();
            List<AccountLedgerInfo> allBanks = new List<AccountLedgerInfo>();
            for (int i = 0; i < banksDt.Rows.Count; i++)
            {
                allBanks.Add(new AccountLedgerInfo
                {
                    LedgerId = Convert.ToDecimal(banksDt.Rows[i].ItemArray[1].ToString()),
                    LedgerName = banksDt.Rows[i].ItemArray[0].ToString()
                });
            }

            List<CustomBankRegisterResponseVM> response = new List<CustomBankRegisterResponseVM>();

            if(param.BankId==0)
            {
                for (int i = 0; i < allBanks.Count; i++)
                {
                    decimal bankId = allBanks[i].LedgerId;
                    List<CustomBankRegisterVM> bankAgents = new List<CustomBankRegisterVM>();
                    for (int j = 0; j < bankRegisterList.Count; j++)
                    {
                        if (bankId == bankRegisterList[j].LedgerId)
                        {
                            List<CustomBankRegisterVM> foundPostings = bankRegisterList.Where(p => p.VoucherNo == bankRegisterList[j].VoucherNo && p.AccountGroupId == 26).ToList();
                            for (int k = 0; k < foundPostings.Count; k++)
                            {
                                if (param.AgentId == 0)
                                {
                                    bankAgents.Add(new CustomBankRegisterVM
                                    {
                                        AccountGroupId = foundPostings[k].AccountGroupId,
                                        AccountGroupName = foundPostings[k].AccountGroupName,
                                        Credit = foundPostings[k].Credit,
                                        Date = Convert.ToDateTime(foundPostings[k].Date).ToString("dd-MM-yyyy"),
                                        Debit = foundPostings[k].Debit,
                                        LedgerId = foundPostings[k].LedgerId,
                                        LedgerName = foundPostings[k].LedgerName,
                                        LedgerPostingId = foundPostings[k].LedgerPostingId,
                                        VoucherNo = foundPostings[k].VoucherNo,
                                        VoucherTypeId = foundPostings[k].VoucherTypeId,
                                        VoucherTypeName = foundPostings[k].VoucherTypeName
                                    });
                                }
                                else if (param.AgentId > 0 && param.AgentId == foundPostings[k].LedgerId)
                                {
                                    bankAgents.Add(new CustomBankRegisterVM
                                    {
                                        AccountGroupId = foundPostings[k].AccountGroupId,
                                        AccountGroupName = foundPostings[k].AccountGroupName,
                                        Credit = foundPostings[k].Credit,
                                        Date = Convert.ToDateTime(foundPostings[k].Date).ToString("dd-MM-yyyy"),
                                        Debit = foundPostings[k].Debit,
                                        LedgerId = foundPostings[k].LedgerId,
                                        LedgerName = foundPostings[k].LedgerName,
                                        LedgerPostingId = foundPostings[k].LedgerPostingId,
                                        VoucherNo = foundPostings[k].VoucherNo,
                                        VoucherTypeId = foundPostings[k].VoucherTypeId,
                                        VoucherTypeName = foundPostings[k].VoucherTypeName
                                    });
                                }
                            }
                        }
                    }
                    response.Add(new CustomBankRegisterResponseVM
                    {
                        BankId = allBanks[i].LedgerId,
                        BankName = allBanks[i].LedgerName,
                        AgentLists = bankAgents
                    });
                }
            }
            else
            {
                AccountLedgerInfo bankObj = allBanks.FirstOrDefault(p => p.LedgerId == param.BankId);
                decimal bankId = bankObj.LedgerId;
                List<CustomBankRegisterVM> bankAgents = new List<CustomBankRegisterVM>();
                for (int j = 0; j < bankRegisterList.Count; j++)
                {
                    if (bankId == bankRegisterList[j].LedgerId)
                    {
                        List<CustomBankRegisterVM> foundPostings = bankRegisterList.Where(p => p.VoucherNo == bankRegisterList[j].VoucherNo && p.AccountGroupId == 26).ToList();
                        for (int k = 0; k < foundPostings.Count; k++)
                        {
                            if(param.AgentId==0)
                            {
                                bankAgents.Add(new CustomBankRegisterVM
                                {
                                    AccountGroupId = foundPostings[k].AccountGroupId,
                                    AccountGroupName = foundPostings[k].AccountGroupName,
                                    Credit = foundPostings[k].Credit,
                                    Date = Convert.ToDateTime(foundPostings[k].Date).ToString("dd-MM-yyyy"),
                                    Debit = foundPostings[k].Debit,
                                    LedgerId = foundPostings[k].LedgerId,
                                    LedgerName = foundPostings[k].LedgerName,
                                    LedgerPostingId = foundPostings[k].LedgerPostingId,
                                    VoucherNo = foundPostings[k].VoucherNo,
                                    VoucherTypeId = foundPostings[k].VoucherTypeId,
                                    VoucherTypeName = foundPostings[k].VoucherTypeName
                                });
                            }
                            else if(param.AgentId>0 && param.AgentId== foundPostings[k].LedgerId)
                            {
                                bankAgents.Add(new CustomBankRegisterVM
                                {
                                    AccountGroupId = foundPostings[k].AccountGroupId,
                                    AccountGroupName = foundPostings[k].AccountGroupName,
                                    Credit = foundPostings[k].Credit,
                                    Date = Convert.ToDateTime(foundPostings[k].Date).ToString("dd-MM-yyyy"),
                                    Debit = foundPostings[k].Debit,
                                    LedgerId = foundPostings[k].LedgerId,
                                    LedgerName = foundPostings[k].LedgerName,
                                    LedgerPostingId = foundPostings[k].LedgerPostingId,
                                    VoucherNo = foundPostings[k].VoucherNo,
                                    VoucherTypeId = foundPostings[k].VoucherTypeId,
                                    VoucherTypeName = foundPostings[k].VoucherTypeName
                                });
                            }
                        }
                    }
                }
                response.Add(new CustomBankRegisterResponseVM
                {
                    BankId = bankObj.LedgerId,
                    BankName = bankObj.LedgerName,
                    AgentLists = bankAgents
                });
            }
            return response;
        }

        public HttpResponseMessage GetLedgers()
        {
            var banks = new TransactionsGeneralFill().BankComboFill();
            var agents = new TransactionsGeneralFill().CashOrPartyUnderSundryDrComboFill();
            dynamic response = new ExpandoObject();
            response.Banks = banks;
            response.Agents = agents;
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
    }

}
