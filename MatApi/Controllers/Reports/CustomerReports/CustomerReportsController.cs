﻿
using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using System.Net.Http;
using System.Web.Http;
using MatApi.Models.Reports.CustomerReports;
using MATFinancials.DAL;
using System.Dynamic;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Reports.CustomerReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    //for all customer reports api
    public class CustomerReportsController : ApiController
    {
        //to get voucher types for day/shift report
        [HttpGet]
        public MatResponse GetVoucherTypes()
        {
            var response = new MatResponse();

            try
            {
                VoucherTypeSP spVoucherType = new VoucherTypeSP();
                DataTable dtbl = new DataTable();
                dtbl = spVoucherType.VoucherTypeViewAll();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch(Exception e)
            {
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }

            return response;
        }

        [HttpGet]
        public MatResponse GetLookUps()
        {
            var response = new MatResponse();
            dynamic lookups = new ExpandoObject();
            try
            {
                SalesDetailsSP spSalesDetails = new SalesDetailsSP();
                TransactionsGeneralFill obj = new TransactionsGeneralFill();

                lookups.formTypes = spSalesDetails.VoucherTypeNameComboFillForSalesInvoiceRegister();
                lookups.customers = obj.CashOrPartyUnderSundryDrComboFill(true);
                lookups.states = new GodownSP().GodownViewAll(); // new AreaSP().AreaViewFOrCombofill(); /* Using State/Area Field to Implement Filter By Location */
                lookups.products = new ProductSP().ProductViewAllForComboBox();
                               

                response.Response = lookups;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }

            return response;
        }


        //to get ledgers for day/shift report

        [HttpGet]
        public MatResponse GetLedgers()
        {
            var response = new MatResponse();

            try
            {
                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                DataTable dtbl = new DataTable();
                dtbl = obj.AccountLedgerComboFill();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }

            return response;
        }

        [HttpGet]
        public MatResponse GetShiftReport(DateTime from, DateTime to, decimal vType, decimal ledger)

        {
            var response = new MatResponse();

            try
            {
                FinancialStatementSP spFinance = new FinancialStatementSP();
                
                var dtbl = spFinance.DayBook(from, to,vType, ledger,true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }

            return response;

        }

        //to get voucher type for Sales quotation report
        [HttpGet]
        public MatResponse GetVoucherType()
        {
            var response = new MatResponse();

            try
            {
                SalesQuotationDetailsSP spSalesQuotationDetails = new SalesQuotationDetailsSP();
                DataTable dtbl = new DataTable();
                dtbl = spSalesQuotationDetails.VoucherTypeCombofillforSalesQuotationReport();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }

            return response;
        }

        //to get sales man for slaes quotation report

        [HttpGet]
        public MatResponse GetSalesMan()
        {
            var response = new MatResponse();

            try
            {
                TransactionsGeneralFill TransactionGenericFillObj = new TransactionsGeneralFill();
                DataTable dtbl = new DataTable();
                dtbl =  TransactionGenericFillObj.SalesmanViewAllForComboFill(true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;

        }

        //to gwt custommer for sales quotation report
        [HttpGet]
        public MatResponse GetCustomers()
        {
            var response = new MatResponse();

            try
            {
                TransactionsGeneralFill TransactionGenericFillObj = new TransactionsGeneralFill();
                DataTable dtbl = new DataTable();
                dtbl = TransactionGenericFillObj.CashOrPartyUnderSundryDrComboFill(true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;

        }

        //to get product name for sales quotation report
        [HttpGet]
        public MatResponse GetProducts()
        {
            var response = new MatResponse();

            try
            {
                ProductSP spproduct = new ProductSP();
                DataTable dtbl = spproduct.ProductViewAll();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;

        }



        [HttpPost]
        public MatResponse GetSalesQuotationReportDetails(SalesQuotationReportVM s)
        {
            var response = new MatResponse();

            try
            {
                SalesQuotationMasterSP spSalesQuotationMaster = new SalesQuotationMasterSP();
                var dtblSalesQuotationReport = spSalesQuotationMaster.SalesQuotationReportSearch("", s.customer, s.fromDate, s.toDate, "All", -1, s.formType, "", "");
                response.Response = dtblSalesQuotationReport;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }

            return response;
        }

        //Now to get all fillups for Sales order report
        //used previous salesman report
        //to get quotations
        [HttpGet]
        public MatResponse GetQuotation(decimal ledgerId)
        {
            var response = new MatResponse();

            try
            {
                SalesQuotationMasterSP SPSalesQuotationMaster = new SalesQuotationMasterSP();
               var dtbl = SPSalesQuotationMaster.GetQuotationNoCorrespondingtoLedgerForSalesOrderRpt(ledgerId);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;

        }

        //to get productGroups
        [HttpGet]
        public MatResponse GetProductGroups()
        {
            var response = new MatResponse();

            try
            {
                ProductGroupSP spProductGroup = new ProductGroupSP();
                DataTable dtbl = spProductGroup.ProductGroupViewForComboFill();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;

        }
        //to get vouchertypes
        [HttpGet]
        public MatResponse GetVTypes()
        {
            var response = new MatResponse();

            try
            {
                SalesOrderDetailsSP spSalesOrderDetails = new SalesOrderDetailsSP();
                DataTable dtbl = spSalesOrderDetails.VoucherTypeCombofillforSalesOrderReport().AsEnumerable()
                            .Where(r => r.Field<string>("voucherTypeName") == "Sales Order" || r.Field<string>("voucherTypeName") == "All")
                            .CopyToDataTable();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;

        }

        //to get areas
        [HttpGet]
        public MatResponse GetAreas()
        {
            var response = new MatResponse();

            try
            {
                AreaSP spArea = new AreaSP();
                DataTable dtbl = spArea.AreaViewFOrCombofill();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;

        }

        //to get routes
        [HttpGet]
        public MatResponse GetRoutes()
        {
            var response = new MatResponse();

            try
            {
                RouteSP spRoute = new RouteSP();
                DataTable dtbl = spRoute.RouteViewForComboFill();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
       }



        [HttpPost]
        public MatResponse GetSalesOrderReportDetails(SalesOrderReportVM s)
        {
            var response = new MatResponse();

            try
            {
                SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
                
                var dtblSalesOrderReport = spSalesOrderMaster.SalesOrderReportViewAll("-1", s.customer, "", s.formType, s.fromDate, s.toDate, "All", s.salesMan, s.quotationNo, s.state, s.productGroup, s.city);
                response.Response = dtblSalesOrderReport;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }

            return response;
        }

        //to get fillups for sales report
        //used prviously defined fill up for customers
        //to get formtypes
        //used previous area
        [HttpGet]
        public MatResponse GetvoucherTypesForSales()
        {
            var response = new MatResponse();

            try
            {
                SalesDetailsSP spSalesDetails = new SalesDetailsSP();
                var dtbl = spSalesDetails.VoucherTypeNameComboFillForSalesInvoiceRegister();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //get model no
        [HttpGet]
        public MatResponse GetModelNo()
        {
            var response = new MatResponse();

            try
            {
                TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
                var dtbl = TransactionGeneralFillObj.ModelNoViewAll(true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to get salesmen 
        [HttpGet]
        public MatResponse GetSalesMenforSalesReport()
        {
            var response = new MatResponse();

            try
            {
                TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
                var dtbl = TransactionGeneralFillObj.SalesmanViewAllForComboFill(true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to get products
        [HttpGet]
        public MatResponse GetProductsForSalesReport()
        {
            var response = new MatResponse();

            try
            {
                ProductSP spProduct = new ProductSP();
                DataTable dtbl = new DataTable();

                dtbl = spProduct.ProductViewAll();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to get routes for sales report
        [HttpGet]
        public MatResponse GetRoutesForSalesReport()
        {
            var response = new MatResponse();

            try
            {
                TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
                DataTable dtbl = new DataTable();

                dtbl = TransactionGeneralFillObj.RouteViewAll(true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to get areas for sales report
        [HttpGet]
        public MatResponse GetAreasForSalesReport()
        {
            var response = new MatResponse();

            try
            {
                TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
                DataTable dtbl = new DataTable();
                dtbl = TransactionGeneralFillObj.AreaViewAll(true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        [HttpPost]
        public MatResponse GetSalesReportDetails(SalesReportVM s)
        {
            var response = new MatResponse();

            try
            {
                SalesMasterSP spSalesMaster = new SalesMasterSP();
                DataTable dtbl = new DataTable();

                dtbl = spSalesMaster.SalesInvoiceReportFill(s.fromDate, s.toDate.AddDays(+1).AddSeconds(-1), s.formType, s.customer, s.state, s.salesMode, 0, s.productName, "", s.status, 0, 0, "");
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.Response = e.Message;
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }


        //to get fill ups for Sales Return Report
        //to get voucher types 
        [HttpGet]
        public MatResponse GetVoucherT()
        {
            var response = new MatResponse();
            try
            {
                SalesReturnMasterSP spSalesReturnMaster = new SalesReturnMasterSP();
                DataTable dtbld = new DataTable();
                var dtbl = spSalesReturnMaster.VoucherTypeComboFillOfSalesReturnReport("Sales Return", true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to get customers for sales return report
        [HttpGet]
        public MatResponse GetCusts()
        {
            var response = new MatResponse();

            try
            {
                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                DataTable dtbl = new DataTable();
                dtbl = obj.CashOrPartyUnderSundryDrComboFill(true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to get products for sales return report
        [HttpGet]
        public MatResponse GetProds()
        {
            var response = new MatResponse();

            try
            {
                ProductSP spProduct = new ProductSP();
                DataTable dtbl = new DataTable();
                dtbl = spProduct.ProductViewAll();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to get salesMen for sales return report
        [HttpGet]
        public MatResponse GetSalesM()
        {
            var response = new MatResponse();

            try
            {
                SalesReturnMasterSP spSalesReturnMaster = new SalesReturnMasterSP();
                DataTable dtbl = new DataTable();
                dtbl = spSalesReturnMaster.SalesManComboFillOfSalesReturnReport(true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetSalesReturnReport(DateTime fromDate, DateTime toDate, decimal formType, decimal customer, decimal salesMan, string product)
        {
            var response = new MatResponse();
            if(product == null)
            {
                product = "";
            }
            try
            {
                SalesReturnMasterSP spSalesReturnMaster = new SalesReturnMasterSP();
                DataTable dtbl = new DataTable();
                dtbl = spSalesReturnMaster.SalesReturnReportGrideFill(fromDate, toDate,customer, formType, salesMan,product, "");
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }


        //for delivery Note report 
        //Fill-ups
        //to get vouchertype for deliverynote
        [HttpGet]
        public MatResponse GetVoucherTypeForDeliveryNote()
        {
            var response = new MatResponse();

            try
            {
                VoucherTypeSP spVoucherType = new VoucherTypeSP();
                DataTable dtbl = new DataTable();
                dtbl = spVoucherType.VoucherTypeSelectionComboFill("Delivery Note").AsEnumerable()
                            .Where(r => r.Field<string>("voucherTypeName") == "Sales Order")
                            .CopyToDataTable();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to get salesMen for delivrynote
        [HttpGet]
        public MatResponse GetSalesMenForDeliveryNote()
        {
            var response = new MatResponse();

            try
            {
                TransactionsGeneralFill TransactionGenerateFillObj = new TransactionsGeneralFill();
                DataTable dtbl = new DataTable();
                dtbl = TransactionGenerateFillObj.SalesmanViewAllForComboFill(true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to get deliverymode/Type for deliverynote
        [HttpGet]
        public MatResponse GetTypeforDeliveryNote()
        {
            var response = new MatResponse();

            try
            {
                DeliveryNoteMasterSP spDeliveryNoteMaster = new DeliveryNoteMasterSP();
                DataTable dtbl = new DataTable();
                dtbl = spDeliveryNoteMaster.VoucherTypeViewAllCorrespondingToSalesOrderAndSalesQuotation();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetCustomersForDeliveryNote()
        {
            var response = new MatResponse();
            try
            {
                TransactionsGeneralFill TransactionGenerateFillObj = new TransactionsGeneralFill();
                DataTable dtbl = new DataTable();
                dtbl = TransactionGenerateFillObj.CashOrPartyUnderSundryDrComboFill(true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to customers




        //for product fillup, used previously defined fillup from salesreport

        //to get deliveryNote
        [HttpPost]
        public MatResponse GetDeliveryNoteReport(DeliveryNoteReportVM d)
        {
            var response = new MatResponse();

            try
            {
                DeliveryNoteMasterSP spDeliveryNoteMaster = new DeliveryNoteMasterSP();
                DataTable dtblReport = new DataTable();
                //need to rework this
                dtblReport = spDeliveryNoteMaster.DeliveryNoteReportGridFill(d.fromDate, d.toDate,d.customer, 0, d.productName, "", d.formType, 
                    d.status, MATFinancials.PublicVariables._inNoOfDecimalPlaces, d.type, d.orderNo, d.typeId);
                response.Response = dtblReport;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";


            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "failed";

            }
            return response;
        }

        //for Rejection In Report Fill-ups
        //to get delivery notes
        [HttpGet]
        public MatResponse GetDeliveryNotes()
        {
            var response = new MatResponse();

            try
            {
                RejectionInMasterSP spRejectionInMaster = new RejectionInMasterSP();
                DataTable dtbl = new DataTable();
                dtbl = spRejectionInMaster.DeliveryNoteNoComboFillToLedger(0, true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to get voucher types
        [HttpGet]
        public MatResponse GetVoucherTypeForRejectionIn()
        {
            var response = new MatResponse();
            try
            {
                RejectionInMasterSP spRejectionInMaster = new RejectionInMasterSP();
                var dtbl = spRejectionInMaster.VoucherTypeSelectionFill("Rejection In", true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to get customers 
        [HttpGet]
        public MatResponse GetCustomersForRejectionIn()
        {
            var response = new MatResponse();
            try
            {
                TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();
                var dtbl = transactionGeneralFillObj.CashOrPartyUnderSundryDrComboFill(true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        //to get salesmen 
        [HttpGet]
        public MatResponse GetSalesmenForRejectionIn()
        {
            var response = new MatResponse();
            try
            {
                TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();
                var dtbl = transactionGeneralFillObj.SalesmanViewAllForComboFill(true);
                    response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 200;
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetRejectionInReport(DateTime fromDate, DateTime toDate,decimal formtype, decimal customer, decimal deliveryNote, decimal salesman)
        {
            var response = new MatResponse();
            try
            {
                RejectionInMasterSP SpRejectionInMaster = new RejectionInMasterSP();
                var dtbl = SpRejectionInMaster.RejectionInReportFill(fromDate, toDate, formtype, "", customer, deliveryNote, salesman, "");
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;
        }

        //to get receipt report fill ups
        //to get voucher types
        [HttpGet]
        public MatResponse GetVouchersForReceipt()
        {
            var response = new MatResponse();
            try
            {
                VoucherTypeSP SpVoucherType = new VoucherTypeSP();
                DataTable dtbl = new DataTable();
                dtbl = SpVoucherType.VoucherTypeViewAll();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;

            }
            catch(Exception e)
            {
                response.ResponseMessage = "failed";
                response.ResponseCode = 500;
            }

            return response;
        }

        //to get cashbanks
        [HttpGet]
        public MatResponse GetCashorBanksForReceipt()
        {
            var response = new MatResponse();
            try
            {
                DataTable dtbl = new DataTable();
                TransactionsGeneralFill Obj = new TransactionsGeneralFill();
                dtbl = Obj.BankOrCashComboFill(false);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;

            }
            catch (Exception e)
            {
                response.ResponseMessage = "failed";
                response.ResponseCode = 500;
            }

            return response;
        }

        [HttpGet]
        public MatResponse GetLedgersForReceipt()
        {
            var response = new MatResponse();
            try
            {
                DataTable dtbl = new DataTable();
                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                dtbl = obj.AccountLedgerComboFill();

                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;

            }
            catch (Exception e)
            {
                response.ResponseMessage = "failed";
                response.ResponseCode = 500;
            }

            return response;
        }

        [HttpGet]
        public MatResponse GetReceiptDetails(DateTime from, DateTime to, decimal formtype, decimal ledger, decimal cashbank)
        {
            var response = new MatResponse();
            try
            {
                DataTable dtbl = new DataTable();
                ReceiptMasterSP SpPaymentMaster = new ReceiptMasterSP();
                dtbl = SpPaymentMaster.ReceiptReportSearch(from, to, ledger, formtype, cashbank);

                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;

            }
            catch (Exception e)
            {
                response.ResponseMessage = "failed";
                response.ResponseCode = 500;
            }

            return response;
        }

        public MatResponse CustomerBalance(DateTime toDate)
        {
            MatResponse response = new MatResponse();

            try
            {
                DataSet dsCustomer = new DataSet();
                DataTable dtblCustomer = new DataTable();
                AccountLedgerSP spAccountledger = new AccountLedgerSP();

                DBMatConnection connection = new DBMatConnection();
                string query = string.Format("select row_number() over(order by cast(al.ledgerId as int)) as slNO, ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, " +
                    "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance from tbl_LedgerPosting lp " +
                    "right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId where al.accountGroupId = 26 AND lp.date <= '{0}'" +
                    "group by ledgerName, lp.ledgerId, al.ledgerId order by ledgerName", toDate);
                dsCustomer = connection.ExecuteQuery(query);
                dtblCustomer = dsCustomer.Tables[0]; ;
                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = dtblCustomer;
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }

            return response;
        }

    }
}
