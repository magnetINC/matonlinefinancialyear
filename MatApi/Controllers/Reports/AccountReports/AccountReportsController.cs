﻿//using DocumentFormat.OpenXml.Drawing.Charts;
using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using MATFinancials.DAL;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Reports.AccountReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AccountReportsController : ApiController
    {
        //for Account Ledger Report
        //to get account ledgers
        public MatResponse GetLedgers(decimal acctGrpId)
        {
            var response = new MatResponse();

            try
            {

                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                DataTable dtblAccountLedger = new DataTable();
                dtblAccountLedger = spAccountLedger.AccountLedgerViewByAccountGroup(acctGrpId);
                response.Response = dtblAccountLedger;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;
        }

        //to get account groups
        public MatResponse GetAccountGroups()
        {
            var response = new MatResponse();

            try
            {

                AccountGroupSP spAccountGroup = new AccountGroupSP();
                DataTable dtblAccountGroup = new DataTable();
                dtblAccountGroup = spAccountGroup.AccountGroupViewAllComboFillForAccountLedger();
                response.Response = dtblAccountGroup;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;
        }

        public MatResponse GetAccountLedgerDetails(DateTime from, DateTime to, decimal ledger, decimal group)
        {
            var response = new MatResponse();
            try
            {
                AccountLedgerSP SpAccountLedger = new AccountLedgerSP();
                DBMatConnection conn = new DBMatConnection();
                var dt = new DataTable();
                DataSet ds = new DataSet();
                conn.AddParameter("@fromDate", from);
                conn.AddParameter("@toDate", to);
                conn.AddParameter("@accountGroupId", group);
                conn.AddParameter("@ledgerId", ledger);
                ds = conn.getDataSet("AccountLedgerReportFill");

                // dtblAccountGroup = spAccountGroup.AccountGroupViewAllComboFillForAccountLedger();
                dt = ds.Tables[1];
                response.Response = dt;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;
        }

        //for account group reports
        [HttpGet]
        public MatResponse GetAccountGroupDetails(DateTime from, DateTime to)
        {
            var response = new MatResponse();
            try
            {
                AccountGroupSP SpAccountGroup = new AccountGroupSP();
                DBMatConnection conn = new DBMatConnection();
                var dt = new DataTable();
                DataSet ds = new DataSet();
                conn.AddParameter("@fromDate", from);
                conn.AddParameter("@toDate", to);
                ds = conn.getDataSet("AccountGroupReportViewAll");

                //dtblAccountGroup = spAccountGroup.AccountGroupViewAllComboFillForAccountLedger();
                dt = ds.Tables[0];
                response.Response = dt;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;
        }

    }
}
