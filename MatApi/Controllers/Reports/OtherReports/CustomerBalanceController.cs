﻿using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Reports.OtherReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CustomerBalanceController : ApiController
    {
        [HttpGet]
        public DataTable CustomerBalanceSummary(string toDate)
        {
            int gridRow = 0;
            DateTime date = Convert.ToDateTime(toDate);
            DateTime datePlusOne = date.AddDays(1);
            toDate = Convert.ToString(datePlusOne);
            decimal totalCredit = 0;
            DataSet dsCustomer = new DataSet();
            DataTable dtblCustomer = new DataTable();
            AccountLedgerSP spAccountledger = new AccountLedgerSP();
            try
            {
                DBMatConnection connection = new DBMatConnection();
                string query = string.Format("select row_number() over(order by cast(al.ledgerId as int)) as slNO, ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, " +
                    "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance from tbl_LedgerPosting lp " +
                    "right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId where al.accountGroupId = 26 AND lp.date <= '{0}'" +
                    "group by ledgerName, lp.ledgerId, al.ledgerId order by ledgerName", toDate);
                dsCustomer = connection.ExecuteQuery(query);
                
                dtblCustomer = dsCustomer.Tables[0];

                dtblCustomer = (from d in dtblCustomer.AsEnumerable()
                                where d.Field<decimal>("openingBalance") != 0
                                select d).CopyToDataTable();

                int gridRowNumber = 0;
                for (int i = 0; i < dtblCustomer.Rows.Count; i++)
                {
                    //dgvCustomerBalance.Rows.Add();
                    //gridRowNumber++;
                    //dgvCustomerBalance.Rows[i].Cells["dgvtxtSNo"].Value = gridRowNumber;
                    //dgvCustomerBalance.Rows[i].Cells["dgvtxtLedgerName"].Value = dtblCustomer.Rows[i]["ledgerName"].ToString();
                    //dgvCustomerBalance.Rows[i].Cells["dgvtxtBalance"].Value = Convert.ToDecimal(dtblCustomer.Rows[i]["openingBalance"]).ToString("N2");
                    totalCredit += Convert.ToDecimal(dtblCustomer.Rows[i]["openingBalance"]);
                }
                //gridRow = dgvCustomerBalance.Rows.Count - 1;
                //dgvCustomerBalance.Rows.Add();
                //dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtLedgerName"].Value = "";
                //dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtBalance"].Value = "____________________";
                //dgvCustomerBalance.Rows.Add();
                //gridRow++;
                //dgvCustomerBalance.Rows[gridRow].DefaultCellStyle.Font = new Font(dgvCustomerBalance.Font, FontStyle.Bold);
                //dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtLedgerName"].Value = "Total";
                //dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtBalance"].Value = Convert.ToDecimal(totalCredit).ToString("N2");
                //dgvCustomerBalance.Rows.Add();
                //gridRow++;
                //dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtLedgerName"].Value = "";
                //dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtBalance"].Value = "====================";
                //intNoOfRowsToPrint = dtblCustomer.Rows.Count;
            }
            catch (Exception ex)
            {

            }
            return dtblCustomer;
        }
    }
}
