﻿using MatApi.Models;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Data;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Dynamic;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Reports.OtherReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BankReportsController : ApiController
    {
        [HttpGet]
        public MatResponse LookUps()
        {
            MatResponse response = new MatResponse();
            dynamic lookUps = new ExpandoObject();
            try
            {
                lookUps.cashOrBank = new ContraMasterSP().CashOrBankComboFill();
                lookUps.ledgers = new AccountLedgerSP().AccountLedgerViewAllForComboBox();
                lookUps.banks = new AccountLedgerSP().BankledgerComboFill();

                response.ResponseCode = 200;
                response.Response = lookUps;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "sorry, something went wrong";
            }
            return response;
        }
        [HttpGet]
        public MatResponse BankTransferReport(DateTime fromDate, DateTime toDate, string transType, string ledger)
        {
            MatResponse response = new MatResponse();

            try
            {
                DataTable dtbl = new DataTable();
                dtbl = new ContraMasterSP().ContraReport(fromDate, toDate, "ALL", ledger, transType);

                response.ResponseCode = 200;
                response.Response = dtbl;
                response.ResponseMessage = "success!";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "sorry, something went wrong";
            }
            return response;
        }
        [HttpGet]
        public MatResponse CashOrBankReport(DateTime fromDate, DateTime toDate)
        {
            MatResponse response = new MatResponse();

            try
            {
                DataTable dtbl = new DataTable();
                dtbl = new FinancialStatementSP().CashOrBankBookGridFill(fromDate, toDate, "", true);

                response.ResponseCode = 200;
                response.Response = dtbl;
                response.ResponseMessage = "success!";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "sorry, something went wrong";
            }
            return response;
        }
        [HttpGet]
        public MatResponse ChequeOrBankReport(DateTime fromDate, DateTime toDate, DateTime chequeFromDate, DateTime chequeToDate, decimal ledger, bool type)
        {
            MatResponse response = new MatResponse();

            try
            {
                DataTable dtbl = new DataTable();
                dtbl = new PDCPayableMasterSP().ChequeReportGridFill(ledger, "", fromDate, toDate, chequeFromDate, chequeToDate, type);

                response.ResponseCode = 200;
                response.Response = dtbl;
                response.ResponseMessage = "success!";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "sorry, something went wrong";
            }
            return response;
        }
        [HttpGet]
        public MatResponse BankReconcilationReport(DateTime fromDate, DateTime toDate, DateTime chequeFromDate, DateTime chequeToDate, decimal ledger, bool type)
        {
            MatResponse response = new MatResponse();

            try
            {
                DataTable dtbl = new DataTable();
                dtbl = new PDCPayableMasterSP().ChequeReportGridFill(ledger, "", fromDate, toDate, chequeFromDate, chequeToDate, type);

                response.ResponseCode = 200;
                response.Response = dtbl;
                response.ResponseMessage = "success!";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "sorry, something went wrong";
            }
            return response;
        }
    }
}
