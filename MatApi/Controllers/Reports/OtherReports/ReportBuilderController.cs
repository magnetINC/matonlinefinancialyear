﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;
using MatApi.Models;

namespace MatApi.Controllers.Reports.OtherReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReportBuilderController : ApiController
    {
       DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();

        [HttpGet]
        public IHttpActionResult GetSalesInvoice(int masterId)
        {
            var company = context.tbl_Company.FirstOrDefault();
            var sales = context.tbl_SalesMaster.Where(a => a.salesMasterId == masterId)
                .Include(a => a.tbl_SalesDetails)
                .Include(a=>a.tbl_SalesDetails.Select(p=>p.tbl_Product))
                .Include(a=>a.tbl_AccountLedger)
                .Select(a => new
                {
                    master = a, 
                    details = a.tbl_SalesDetails.Select(c=> new
                    {
                        product = c.tbl_Product, 
                        detail = c, 
                        unit = context.tbl_Unit.Where( d=>d.unitId == c.unitId).FirstOrDefault()
                    }), 
                    customer = a.tbl_AccountLedger
                })
                .FirstOrDefault();
            if (sales == null) return NotFound();
            var customer = sales.customer;
            var receipt = context.tbl_ReceiptMaster.Where(a =>
                    a.voucherTypeId == Constants.SalesInvoice_VoucherTypeId && a.voucherNo == sales.master.voucherNo)
                .FirstOrDefault();
            var subtotal = sales.details.Sum(a => a.detail.amount);
            var billDiscount = sales.master.billDiscount;
            var total = subtotal - billDiscount;
            var vat = sales.details.Sum(a => a.detail.taxAmount);
            var grandTotal = total + vat;
            var appliedAmount = 0.0M;
            if (receipt != null)
            {
                appliedAmount = Convert.ToDecimal(receipt.totalAmount);
            }

            var balance = grandTotal - appliedAmount;
            var summary = new
            {
                subtotal = subtotal,
                billDiscount = billDiscount,
                total = total,
                vat = vat,
                grandTotal = grandTotal, 
                appliedAmount = appliedAmount,
                balance = balance
            };
            var obj = new
            {
                company = company,
                customer = customer,
                transactions = sales,
                summary = summary
            };
            return Ok(obj);
        }
    }
}
