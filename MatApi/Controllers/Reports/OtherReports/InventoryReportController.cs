using MatApi.Models.Reports.OtherReports;
using MATFinancials;
using MATFinancials.Classes.HelperClasses;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;
using MatApi.Services;

namespace MatApi.Controllers.Reports.OtherReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class InventoryReportController : ApiController
    {
        List<string> AllItems = new List<string> ();
        List<productModel> AllItemss = new List<productModel> ();

        public class productModel
        {
            public string ProductId { get; set; }
            public string ProductCode { get; set; }
        }
        List<decimal> AllStores = new List<decimal> { };
        bool isOpeningRate;

        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        [HttpPost]
        public DataTable StockJournal(StockJournalVM input)
        {
            DataTable dtblReg = new DataTable();
            try
            {
                MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

                dtblReg = new StockJournalMasterSP().StockJournalReportGrideFill(input.FromDate, input.ToDate, input.VoucherType, input.VoucherNo, input.ProductCode, input.Product);
                //string GetQuery = string.Format("");
                //GetQuery = string.Format("");
                //GetQuery = string.Format("SELECT * FROM tbl_StockJournalMaster_Pending WHERE date BETWEEN '{0}' AND '{1}'", input.FromDate, input.ToDate,);
                //var result = db.GetDataSet(GetQuery);
            }
            catch (Exception ex)
            {
                // formMDI.infoError.ErrorString = "SRR2:" + ex.Message;
            }
            return dtblReg;
        }
        [HttpGet]
        public HttpResponseMessage stockJournalDetails(decimal id)
        {
            dynamic response = new ExpandoObject();
            StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
            StockJournalDetailsSP spStockJournalDetails = new StockJournalDetailsSP();
            StockJournalMasterInfo infoStockJournalMaster = new StockJournalMasterInfo();
            StockJournalDetailsInfo infoStockJournalDetails = new StockJournalDetailsInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            VoucherTypeSP spVoucherType = new VoucherTypeSP();

            DataTable dtblMaster = spStockJournalMaster.StockJournalMasterFillForRegisterOrReport(id);
            decimal StockJournalVoucherTypeId = Convert.ToDecimal(dtblMaster.Rows[0]["voucherTypeId"].ToString());
            string strVoucherNo = dtblMaster.Rows[0]["voucherNo"].ToString();
            response.Master = dtblMaster;

            DataSet dsDetails = spStockJournalDetails.StockJournalDetailsForRegisterOrReport(id);
            response.consumption = dsDetails.Tables[0];
            response.production = dsDetails.Tables[1];

            DataSet dsAdditionalCost = spAdditionalCost.StockJournalAdditionalCostForRegisteOrReport(strVoucherNo, StockJournalVoucherTypeId);
            response.cashorbank = dsAdditionalCost.Tables[0];
            response.directAdditionalCost = dsAdditionalCost.Tables[1];

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpPost]
        public DataTable StockVariance(StockVarianceVM input)
        {
            var spstock = new MATFinancials.StockPostingSP();
            DataTable dtbl = new DataTable();
            //dynamic response = new ExpandoObject();
            try
            {
                string itemCode = "";
                if (!string.IsNullOrWhiteSpace(input.ItemCode))
                {
                    itemCode = input.ItemCode;
                }
                dtbl = spstock.StockVarianceReportViewAll(input.ProductId, input.StoreId, itemCode);
                var responseObj = dtbl.AsEnumerable().Select(variance => new
                {
                    SLNO = variance.Field<decimal>("SL.NO"),
                    productId = variance.Field<decimal>("productId"),
                    productCode = variance.Field<string>("productCode"),
                    productName = variance.Field<string>("productName"),
                    qty = variance.Field<decimal>("qty"),
                    godownName = variance.Field<string>("godownName"),
                    SystemStock = variance.Field<string>("SystemStock"),
                    Batch = string.IsNullOrEmpty(variance.Field<string>("Batch")) ? "" : variance.Field<string>("Batch"),
                    rate = variance.Field<decimal>("rate"),

                    //stockDifference = Convert.ToDecimal( variance.Field<string>("SystemStock") != null && !string.IsNullOrEmpty(variance.Field<string>("SystemStock")) ? variance.Field<string>("SystemStock") : "0")  - variance.Field<decimal>("qty"),        
                    stockDifference = variance.Field<decimal>("qty") - Convert.ToDecimal(variance.Field<string>("SystemStock") != null && !string.IsNullOrEmpty(variance.Field<string>("SystemStock")) ? variance.Field<string>("SystemStock") : "0"),
                    stockValue = variance.Field<decimal>("rate") * (variance.Field<decimal>("qty") - Convert.ToDecimal(variance.Field<string>("SystemStock") != null && !string.IsNullOrEmpty(variance.Field<string>("SystemStock")) ? variance.Field<string>("SystemStock") : "0")),
                    //stockDifference = Convert.ToDecimal(SystemStock) - Convert.ToDecimal(qty),
                    extraDate = variance.Field<DateTime>("extraDate").ToShortDateString()
                }).ToList();
                //response.StockVariance = responseObj;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "SVR02" + ex.Message;
            }
            return dtbl;
        }

        [HttpPost]
        public DataTable InventoryMovement(InventoryMovementVM input)
        {
            try
            {
                return new ProductSP().ProductVsBatchReportGridFill(input.VoucherType, input.VoucherNo, input.ProductGroup, input.ProductCode, input.BatchNoId,input.FromDate, input.ToDate, input.ProductId, input.StoreId);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public DataTable PhysicalStock(PhysicalStockVM input)
        {
            DataTable response = new DataTable();
            try
            {
                PhysicalStockMasterSP spPhysicalStockMaster = new PhysicalStockMasterSP();
                //dtbl = spPhysicalStockMaster.PhysicalStockReportFill(input.FromDate, input.ToDate, input.VoucherNo, input.ProductName,
                //                                                        input.ProductCode, input.VoucherTypeId, input.StrProductCode, input.StoreId);
                string query = "";

                if(input.ProductId==0 && input.StoreId==0)
                {
                    query = string.Format("select p.productcode,g.godownname,psd.qty,p.productname,b.brandname,s.size,p.purchaserate,p.salesrate,p.productid " +
                                                "from tbl_physicalstockdetails as psd " +
                                                "inner join tbl_product as p on p.productid = psd.productid " +
                                                "inner join tbl_size as s on s.sizeid = p.sizeid " +
                                                "inner join tbl_brand as b on b.brandid = p.brandid " +
                                                "inner join tbl_godown as g on g.godownid = p.godownid " +
                                                "inner join tbl_physicalstockmaster as psm on psm.physicalstockmasterid = psd.physicalstockmasterid " +
                                                "where psm.date >= '{0}' and psm.date <= '{1}' ",
                                                input.FromDate, input.ToDate);
                }
                else if(input.ProductId>0 && input.StoreId==0)
                {
                    query = string.Format("select p.productcode,g.godownname,psd.qty,p.productname,b.brandname,s.size,p.purchaserate,p.salesrate,p.productid " +
                                                "from tbl_physicalstockdetails as psd " +
                                                "inner join tbl_product as p on p.productid = psd.productid " +
                                                "inner join tbl_size as s on s.sizeid = p.sizeid " +
                                                "inner join tbl_brand as b on b.brandid = p.brandid " +
                                                "inner join tbl_godown as g on g.godownid = p.godownid " +
                                                "inner join tbl_physicalstockmaster as psm on psm.physicalstockmasterid = psd.physicalstockmasterid " +
                                                "where psm.date >= '{0}' and psm.date <= '{1}' and psd.productid = '{2}' ",
                                                input.FromDate, input.ToDate, input.ProductId);
                }
                else if(input.ProductId==0 && input.StoreId>0)
                {
                    query = string.Format("select p.productcode,g.godownname,psd.qty,p.productname,b.brandname,s.size,p.purchaserate,p.salesrate,p.productid " +
                                                "from tbl_physicalstockdetails as psd " +
                                                "inner join tbl_product as p on p.productid = psd.productid " +
                                                "inner join tbl_size as s on s.sizeid = p.sizeid " +
                                                "inner join tbl_brand as b on b.brandid = p.brandid " +
                                                "inner join tbl_godown as g on g.godownid = p.godownid " +
                                                "inner join tbl_physicalstockmaster as psm on psm.physicalstockmasterid = psd.physicalstockmasterid " +
                                                "where psm.date >= '{0}' and psm.date <= '{1}' and psd.godownid = '{2}' ",
                                                input.FromDate, input.ToDate, input.StoreId);
                }
                else if(input.ProductId>0 && input.StoreId>0)
                {
                    query = string.Format("select p.productcode,g.godownname,psd.qty,p.productname,b.brandname,s.size,p.purchaserate,p.salesrate,p.productid " +
                                                "from tbl_physicalstockdetails as psd " +
                                                "inner join tbl_product as p on p.productid = psd.productid " +
                                                "inner join tbl_size as s on s.sizeid = p.sizeid " +
                                                "inner join tbl_brand as b on b.brandid = p.brandid " +
                                                "inner join tbl_godown as g on g.godownid = p.godownid " +
                                                "inner join tbl_physicalstockmaster as psm on psm.physicalstockmasterid = psd.physicalstockmasterid " +
                                                "where psm.date >= '{0}' and psm.date <= '{1}' and psd.productid = '{2}' and psd.godownid = '{3}'",
                                                input.FromDate, input.ToDate, input.ProductId,input.StoreId);
                }
                response = new DBMatConnection().customSelect(query);

            }
            catch (Exception ex)
            {
            }
            return response;
        }

        [HttpPost]
        public DataTable ProductStatistics(InventoryStatistics input)
        {
            return new ProductSP().ProductStatisticsFill(input.BrandId, input.ModelNoId, input.SizeId, input.ProductGroupId, input.Criteria, input.BatchName, input.FromDate, input.ToDate);
        }

        [HttpPost]
        public StockReportRow StockReportDetails1(StockReportSearch input)
        {
            StockReportRow master = new StockReportRow();
            List<StockReportRowItems> details = new List<StockReportRowItems>();

            try
            {
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                string calculationMethod = string.Empty;
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
               // string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
               // string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
               DateTime lastFinYearStart = PublicVariables._dtFromDate;//= conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
               DateTime lastFinYearEnd = PublicVariables._dtFromDate.AddDays(-1); //= conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);

               var lastFinicialYear =
                   new FinicialServcies().GetPreviousFinicialYear(PublicVariables.CurrentFinicialId);
               if (lastFinicialYear != null)
               {
                   lastFinYearEnd = lastFinicialYear.toDate.Value;
                   lastFinYearStart = lastFinicialYear.fromDate.Value;
               }
               
                //if (!isFormLoad)
                //{
                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                //objValidation.DateValidationFunction(input.ToDate);
                //if (txtTodate.Text == string.Empty)
                //    txtTodate.Text = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                //objValidation.DateValidationFunction(txtFromDate);
                //if (txtFromDate.Text == string.Empty)
                //    txtFromDate.Text = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");

                var spstock = new MATFinancials.StockPostingSP();
                DataTable dtbl = new DataTable();
                try
                {
                    //spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), produceCode, referenceNo);
                    //dtbl = spstock.StockReportDetailsGridFill(input.ProductId,input.StoreId,input.BatchNo,input.FromDate, input.ToDate,input.ProductCode, input.RefNo);
                    //for some crazy reasons, the "To date" parameter refers to the last the of the previous financial year on d desktop code
                    dtbl = spstock.StockReportDetailsGridFill(input.ProductId, input.StoreId, input.BatchNo, input.FromDate, input.ToDate, input.ProductCode, input.RefNo);
                }
                catch (Exception ex)
                {
                }
                // when there is no transaction during the period selected but there is transaction before the period selected,
                // invoke this addition (dtbl2) and it is per store 20170406
                DataTable dtbl2 = new DataTable();
                try
                {
                    dtbl2 = spstock.StockReportDetailsGridFill(input.ProductId, input.StoreId, input.BatchNo, input.FromDate, input.ToDate, input.ProductCode, input.RefNo);
                }
                catch (Exception ex)
                {
                }
                List<decimal> storesInDtbl = new List<decimal>();
                List<decimal> storesInDtbl2 = new List<decimal>();
                List<decimal> storesWithoutTransactions = new List<decimal>();

                if ((dtbl != null && dtbl.Rows.Count > 0) && (dtbl2 != null && dtbl2.Rows.Count > 0))
                {
                    storesInDtbl = dtbl.AsEnumerable().Select(n => n.Field<decimal>("godownId")).ToList();
                    storesInDtbl2 = dtbl2.AsEnumerable().Select(n => n.Field<decimal>("godownId")).ToList();
                    // select stores without transaction before the period
                    storesWithoutTransactions = storesInDtbl2.Except(storesInDtbl).ToList();
                    if (dtbl2.AsEnumerable().Where(p => storesWithoutTransactions.Contains(p.Field<decimal>("godownId"))).Any())
                    {
                        dtbl2 = dtbl2.AsEnumerable().Where(p => storesWithoutTransactions.Contains(p.Field<decimal>("godownId"))).CopyToDataTable();
                    }
                }

                // the invoked method ends here
                //dgvStockreportDetails.Rows.Clear();
                fillAllItems(input);
                var ItemsWithRecords = (dtbl.AsEnumerable().Select(j => j.Field<string>("productCode"))).ToList();
                List<string> itemsWithoutRecords = AllItems.Except(ItemsWithRecords).ToList();

                decimal currentStore = 0, previousStore = 0;
                decimal currentProductId = 0;
                decimal prevProductId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0, inwardQty = 0, outwardQty = 0, totalQuantityIn = 0, totalQuantityOut = 0;
                decimal qtyBal = 0, stockValue = 0, stockValue2 = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal totalAssetValue = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };

                foreach (DataRow dr in dtbl.Rows)
                {
                    currentProductId = Convert.ToDecimal(dr["productId"]);
                    productCode = dr["productCode"].ToString();
                    currentStore = Convert.ToDecimal(dr["godownId"]);
                    string voucherType = "", refNo = "";

                    inwardQty = (from p in dtbl.AsEnumerable()
                                 where p.Field<decimal>("productId") == prevProductId && p.Field<decimal>("godownId") == previousStore
                                 select p.Field<decimal>("inwardQty")).Sum();
                    outwardQty = (from p in dtbl.AsEnumerable()
                                  where p.Field<decimal>("productId") == prevProductId && p.Field<decimal>("godownId") == previousStore
                                  select p.Field<decimal>("outwardQty")).Sum();
                    //totalQuantityIn += inwardQty;
                    //totalQuantityOut += outwardQty;
                    inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                    outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                    decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                    string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                    rate2 = Convert.ToDecimal(dr["rate"]);
                    if (previousRunningAverage != 0 && currentProductId == prevProductId)
                    {
                        purchaseRate = previousRunningAverage;
                    }
                    //======================== 2017-02-27 =============================== //
                    //if (againstVoucherType != "NA")
                    //{
                    //    voucherType = againstVoucherType;
                    //}
                    //else
                    //{
                    //    voucherType = dr["typeOfVoucher"].ToString();
                    //}
                    refNo = dr["refNo"].ToString();
                    if (againstVoucherType != "NA")
                    {
                        refNo = dr["againstVoucherNo"].ToString();
                        isAgainstVoucher = true;
                        //againstVoucherType = dr["typeOfVoucher"].ToString();
                        againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                        //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                        string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                        string typeOfVoucher = conn.getSingleValue(queryString);
                        if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                        if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                    }
                    voucherType = dr["typeOfVoucher"].ToString();
                    //---------  COMMENT END  ----------- //
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if (outwardQty2 > 0)
                    {
                        if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                        {
                            if (qtyBal == 0)
                            {
                                //computedAverageRate = (stockValue / 1); // old changed by Urefe 2017-02-21
                                //qtyBal += inwardQty2 - outwardQty2; // old changed by Urefe 2017-02-21
                                //stockValue = computedAverageRate * 1; // old changed by Urefe 2017-02-21
                                //computedAverageRate = Convert.ToDecimal(dr["rate"]);  // old changed by Urefe 2017-02-27
                                computedAverageRate = purchaseRate;
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                //if(computedAverageRate == 0)
                                //{
                                //    stockValue = qtyBal * rate2;
                                //}
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                        {
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue -= outwardQty2 * rate2;
                        }
                        else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return")
                            || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }

                        else if (outwardQty2 > 0 && voucherType == "Sales Order")
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                    }
                    else if (voucherType == "Sales Return" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Rejection In" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;

                        }
                        else
                        {
                            computedAverageRate = previousRunningAverage;// (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                        // voucherType = "Sales Return";
                    }
                    else if (voucherType == "Delivery Note")
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                    }
                    else
                    {
                        // hndles other transactions such as purchase, opening balance, etc
                        qtyBal += inwardQty2 - outwardQty2;
                        stockValue += (inwardQty2 * rate2);
                    }
                    // insert subtotal per item per store and calculate the final subtotal per item (all stores together) totalAssetValue += 
                    if ((currentProductId != prevProductId && i != 0) || (currentStore != previousStore && i != 0))
                    {
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        if (voucherType == "Sales Invoice")
                        {
                            stockValue = qtyBal * purchaseRate;
                        }
                        else
                        {
                            stockValue = qtyBal * rate2;
                        }

                        details.Add(new StockReportRowItems
                        {
                            ProductId = "0",
                            ProductName = "",
                            StoreName = "",
                            InwardQty = (inwardQty + openingQuantityIn).ToString("N2"),
                            OutwardQty = ("-") + (outwardQty + openingQuantityOut).ToString("N2"),
                            Rate = "-",
                            AverageCost = "", // computedAverageRate.ToString("N2");
                            QtyBalance = (inwardQty - outwardQty).ToString("N2"),
                            StockValue = details[i - 1].StockValue,
                            VoucherTypeName = "Sub Total:- " + details[i - 1].ProductName,
                            Batch = "",
                            ProjectId = 0,
                            CategoryId =0,
                            UnitName = ""
                            //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = dgvStockreportDetails.Rows[i - 1].Cells["stockValue"].Value;
                            //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Sub Total:- " + dgvStockreportDetails.Rows[i - 1].Cells["productName"].Value; //+" In "+ dgvStockreportDetails.Rows[i-1].Cells["godownName"].Value;
                        });
                        //dgvStockreportDetails.Rows[i].Cells["productId"].Value = 0;
                        //dgvStockreportDetails.Rows[i].Cells["productName"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = (inwardQty + openingQuantityIn).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ("-") + (outwardQty + openingQuantityOut).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["rate"].Value = "-";
                        //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = ""; // computedAverageRate.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = (inwardQty - outwardQty).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = dgvStockreportDetails.Rows[i - 1].Cells["stockValue"].Value;
                        //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Sub Total:- " + dgvStockreportDetails.Rows[i - 1].Cells["productName"].Value; //+" In "+ dgvStockreportDetails.Rows[i-1].Cells["godownName"].Value;
                        totalAssetValue += Convert.ToDecimal(details[i - 1].StockValue);
                        totalQuantityIn += inwardQty;
                        totalQuantityOut += outwardQty;
                        i++;
                        openingQuantityIn = 0;
                        openingQuantityOut = 0;
                    }
                    //calculation end
                    // -----------------added 2017-02-21 -------------------- //
                    if ((currentProductId != prevProductId) || (currentStore != previousStore))
                    {
                        // ---------------------- PUT THE OPENING STOCK HERE I.E AFTER THE SUBTOTAL -------------------------- //
                        computedAverageRate = rate2;
                        //decimal openingStock = methodForStockValue.currentStockValue3(lastFinYearStart, lastFinYearEnd, false, 0, false, DateTime.Now, productCode);
                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, productCode, currentStore);
                        dcOpeningStockForRollOver = itemParams.Item1;
                        openingQuantity = itemParams.Item2;
                        openingQuantityIn = itemParams.Item3;
                        openingQuantityOut = itemParams.Item4;
                        //dcOpeningStockForRollOver = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, DateTime.Now, currentProductId, false);
                        //openingQuantity = methodForStockValue.currentStockQuantity(lastFinYearStart, lastFinYearEnd, true, DateTime.Now, currentProductId, false);
                        qtyBal += openingQuantity;
                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                        {
                            computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                            stockValue = qtyBal * computedAverageRate;
                        }
                        details.Add(new StockReportRowItems
                        {
                            ProductId = "0",
                            ProductName = "",
                            StoreName = "",
                            InwardQty = openingQuantityIn.ToString("N2"),
                            OutwardQty = openingQuantityOut.ToString("N2"),
                            Rate = "",
                            AverageCost = "",
                            QtyBalance = openingQuantity.ToString("N2"),
                            StockValue = dcOpeningStockForRollOver.ToString("N2"),
                            VoucherTypeName = "Opening Stock ",
                            Batch = "",
                            ProjectId = 0,
                            CategoryId = 0,
                            UnitName = ""
                        });
                        //dgvStockreportDetails.Rows.Add();
                        //dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        //dgvStockreportDetails.Rows[i].Cells["productId"].Value = 0;
                        //dgvStockreportDetails.Rows[i].Cells["productName"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["rate"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";

                        //totalAssetValue += Convert.ToDecimal(dcOpeningStockForRollOver);
                        totalQuantityIn += openingQuantityIn;
                        totalQuantityOut += openingQuantityOut;
                        //totalQuantityOut += outwardQty;
                        i++;

                    }
                    // ------------------------------------------------------ //

                    //dgvStockreportDetails.Rows.Add();
                    //dgvStockreportDetails.Rows[i].Cells["productId"].Value = dr["productId"];
                    //dgvStockreportDetails.Rows[i].Cells["productName"].Value = dr["productName"];
                    //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = dr["godownName"];
                    //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = Convert.ToDecimal(dr["inwardQty"]).ToString("N2");
                    //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2");
                    //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = isAgainstVoucher == true ? againstVoucherType : voucherType;
                    //isAgainstVoucher = false;
                    //dgvStockreportDetails.Rows[i].Cells["rate"].Value = Convert.ToDecimal(dr["rate"]).ToString("N2");
                    //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value =  computedAverageRate.ToString("N2");

                    //comment out by alex
                    //details.Add(new StockReportRowItems
                    //{
                    //    ProductId = dr["productId"].ToString(),
                    //    ProductName = dr["productName"].ToString(),
                    //    StoreName = dr["godownName"].ToString(),
                    //    InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                    //    OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                    //    Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                    //    AverageCost = computedAverageRate.ToString("N2"),
                    //    QtyBalance = "",
                    //    StockValue = "",
                    //    VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType
                    //});
                    //isAgainstVoucher = false;

                    string avgCost = "";
                    if (stockValue != 0 && qtyBal != 0)
                    {
                        //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = (stockValue / qtyBal).ToString("N2");
                        avgCost = (stockValue / qtyBal).ToString("N2");

                    }
                    else
                    {
                        //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = computedAverageRate.ToString("N2");
                        avgCost = computedAverageRate.ToString("N2");
                    }

                    details.Add(new StockReportRowItems
                    {
                        ProductId = string.IsNullOrEmpty(dr["productId"].ToString()) ? "" : dr["productId"].ToString(),
                    
                        ProductName = string.IsNullOrEmpty(dr["ProductName"].ToString()) ? "" : dr["ProductName"].ToString(),
                        StoreName = string.IsNullOrEmpty(dr["godownName"].ToString()) ? "" : dr["godownName"].ToString(),
                        InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                        OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                        VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType,
                        Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                        AverageCost = string.IsNullOrEmpty(avgCost) ? "" : avgCost,
                        Date = Convert.ToDateTime(dr["date"]).ToShortDateString(),
                        QtyBalance = string.IsNullOrEmpty(qtyBal.ToString("N2")) ? "" : qtyBal.ToString("N2"),
                        StockValue = string.IsNullOrEmpty(stockValue.ToString("N2")) ? "" : stockValue.ToString("N2"),
                        Batch  = string.IsNullOrEmpty(dr["Batch"].ToString()) ? "" : dr["Batch"].ToString(),
                        ProductCode = string.IsNullOrEmpty(dr["productCode"].ToString()) ? "" : dr["productCode"].ToString(),
                        RefNo = string.IsNullOrEmpty(refNo) ? "" : refNo,
                        ProjectId = Convert.ToDecimal(dr["ProjectId"].ToString()),
                        CategoryId =  Convert.ToDecimal(dr["CategoryId"].ToString()),
                        UnitName = string.IsNullOrEmpty(dr["UnitName"].ToString()) ? "" : dr["UnitName"].ToString()
                    });
                    isAgainstVoucher = false;

                    //dgvStockreportDetails.Rows[i].Cells["date"].Value = Convert.ToDateTime(dr["date"]).ToShortDateString();
                    //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = qtyBal.ToString("N2");
                    //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = stockValue.ToString("N2");
                    //dgvStockreportDetails.Rows[i].Cells["Batch"].Value = dr["Batch"].ToString();
                    //dgvStockreportDetails.Rows[i].Cells["productCode"].Value = dr["productCode"];
                    //dgvStockreportDetails.Rows[i].Cells["refNo"].Value = refNo; // dr["refNo"];
                    prevProductId = currentProductId;
                    previousStore = currentStore;
                    previousRunningAverage = Convert.ToDecimal(details[i].AverageCost);
                    i++;
                }
                //Add the sub total for the last item in the table
                //dgvStockreportDetails.Rows.Add();
                //if (i == 0 && i < dgvStockreportDetails.Rows.Count)
                //{
                //    i = dgvStockreportDetails.Rows.Count;
                //}
                //dgvStockreportDetails.Rows.Add();
                //dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.DeepSkyBlue;
                details.Add(new StockReportRowItems
                {
                    ProductId = "0",
                    ProductName = "",
                    StoreName = "",
                    InwardQty = inwardQty.ToString("N2"),
                    OutwardQty = ("-") + outwardQty.ToString("N2"),
                    Rate = "-",
                    AverageCost = "",
                    QtyBalance = (inwardQty - outwardQty).ToString("N2"),
                    StockValue = details[i - 1].StockValue,
                    VoucherTypeName = "Sub Total:- " + details[i - 1].ProductName,
                    Batch = "",
                    ProjectId = 0,
                    CategoryId = 0,
                    UnitName = ""
                });
                //dgvStockreportDetails.Rows[i].Cells["productId"].Value = 0;
                //dgvStockreportDetails.Rows[i].Cells["productName"].Value = "";
                //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = inwardQty.ToString("N2");
                //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ("-") + outwardQty.ToString("N2");
                //dgvStockreportDetails.Rows[i].Cells["rate"].Value = "-";
                //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "";
                //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = (inwardQty - outwardQty).ToString("N2");
                //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = dgvStockreportDetails.Rows[i - 1].Cells["stockValue"].Value;
                //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Sub Total:- " + dgvStockreportDetails.Rows[i - 1].Cells["productName"].Value; //+" In "+ dgvStockreportDetails.Rows[i-1].Cells["godownName"].Value;
                totalQuantityIn += inwardQty;
                totalQuantityOut += outwardQty;
                i++;
                //Add the sub total for the last item in the table end

                // ================== Add closing stock as opening stock if transactions have not been ran on that item in that store for that period ============== //
                if (storesWithoutTransactions.Any())
                {
                    foreach (var item in storesWithoutTransactions)
                    {
                        string productCode2 = (dtbl2.AsEnumerable().Where(m => m.Field<decimal>("godownId") == item).
                            Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString();
                        var itemParams = methodForStockValue.currentStockValue(PublicVariables._dtFromDate, Convert.ToDateTime(input.FromDate).AddDays(-1), true, productCode2, item);
                        decimal itemStockValue = itemParams.Item1;
                        decimal itemStock = itemParams.Item2;
                        //dgvStockreportDetails.Rows.Add();
                        //dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        //dgvStockreportDetails.Rows[i].Cells["productId"].Value = item.ToString();
                        //dgvStockreportDetails.Rows[i].Cells["productCode"].Value = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["productName"].Value = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("godownName"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = ""; // Convert.ToDecimal(inwardqt).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ""; //Convert.ToDecimal(outwardqt).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = itemStock.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = itemStockValue.ToString("N2");
                        string avgCost2 = "";
                        if (itemStockValue != 0 && itemStock != 0)
                        {
                            // dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = (itemStockValue / itemStock).ToString("N2");
                            avgCost2 = (itemStockValue / itemStock).ToString("N2");
                        }
                        else
                        {
                            //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "0.00";
                            avgCost2 = "0.00";
                        }
                        // dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock";

                        details.Add(new StockReportRowItems
                        {
                            ProductId = item.ToString(),
                            ProductCode = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString(),
                            ProductName = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString(),
                            StoreName = (dtbl2.AsEnumerable().Where(m => m.Field<string>("productCode") == productCode2).Select(n => n.Field<string>("godownName"))).FirstOrDefault().ToString(),
                            InwardQty = "",
                            OutwardQty = "",
                            QtyBalance = itemStock.ToString("N2"),
                            StockValue = itemStockValue.ToString("N2"),
                            AverageCost = avgCost2,
                            VoucherTypeName = "Opening Stock",
                            Batch = "",
                            ProjectId = 0,
                            CategoryId = 0,
                            UnitName = ""
                        });
                        stockValue2 += itemStockValue;
                        i++;
                    }
                }

                // ================== Add closing stock as opening stock if transactions have not been ran on that item for that period (for all items)============== //
                if (itemsWithoutRecords.Any() && input.ProductId == 0)
                {
                    ProductSP spproduct = new ProductSP();
                    DataTable dt = new DataTable();
                    dt = spproduct.ProductViewAll();
                    foreach (var item in itemsWithoutRecords)
                    {
                        //decimal itemStockValue = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, false, DateTime.Now, item, true);
                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, Convert.ToDateTime(input.FromDate).AddDays(-1), true, item, currentStore);
                        decimal itemStockValue = itemParams.Item1;
                        //decimal itemStock = methodForStockValue.currentStockQuantity(lastFinYearStart, lastFinYearEnd, false, DateTime.Now item, true);
                        decimal itemStock = itemParams.Item2;
                        //dgvStockreportDetails.Rows.Add();
                        //dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        //dgvStockreportDetails.Rows[i].Cells["productId"].Value = item.ToString();
                        //dgvStockreportDetails.Rows[i].Cells["productCode"].Value = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["productName"].Value = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = ""; // Convert.ToDecimal(inwardqt).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ""; //Convert.ToDecimal(outwardqt).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = itemStock.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = itemStockValue.ToString("N2");
                        string avgCost3 = "";
                        if (itemStockValue != 0 && itemStock != 0)
                        {
                            //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = (itemStockValue / itemStock).ToString("N2");
                            avgCost3 = (itemStockValue / itemStock).ToString("N2");
                        }
                        else
                        {
                            //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "0.00";
                            avgCost3 = "";
                        }
                        //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock";
                        details.Add(new StockReportRowItems
                        {
                            ProductId = item.ToString(),
                            ProductCode = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString(),
                            ProductName = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString(),
                            StoreName = "",
                            InwardQty = "",
                            OutwardQty = "",
                            QtyBalance = itemStock.ToString("N2"),
                            StockValue = itemStockValue.ToString("N2"),
                            VoucherTypeName = "Opening Stock",
                            AverageCost = avgCost3,
                            Batch = "",
                            ProjectId = 0,
                            CategoryId = 0,
                            UnitName = ""
                        });
                        stockValue2 += itemStockValue;
                        i++;
                    }
                }
                // ================== Add closing stock as opening stock if transactions have not been ran on that item for that period (for a single item) ============== //
                else if (itemsWithoutRecords.Any() && input.ProductId != 0 && (dtbl == null || dtbl.Rows.Count < 1))
                {
                    ProductSP spproduct = new ProductSP();
                    DataTable dt = new DataTable();
                    dt = spproduct.ProductViewAll();
                    itemsWithoutRecords.Clear(); itemsWithoutRecords.AddRange(dt.AsEnumerable().Where(k => k.Field<decimal>("productId") == input.ProductId)
                        .Select(k => k.Field<string>("productCode")).ToList());
                    foreach (var item in itemsWithoutRecords)
                    {
                        //decimal itemStockValue = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, false, DateTime.Now, item, true);
                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, Convert.ToDateTime(input.FromDate).AddDays(-1), true, item, currentStore);
                        decimal itemStockValue = itemParams.Item1;
                        //decimal itemStock = methodForStockValue.currentStockQuantity(lastFinYearStart, lastFinYearEnd, false, DateTime.Now item, true);
                        decimal itemStock = itemParams.Item2;
                        //dgvStockreportDetails.Rows.Add();
                        //dgvStockreportDetails.Rows[i].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                        //dgvStockreportDetails.Rows[i].Cells["productId"].Value = item.ToString();
                        //dgvStockreportDetails.Rows[i].Cells["productCode"].Value = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["productName"].Value = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString();
                        //dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                        //dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = ""; // Convert.ToDecimal(inwardqt).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = ""; //Convert.ToDecimal(outwardqt).ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = itemStock.ToString("N2");
                        //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = itemStockValue.ToString("N2");
                        string avgCost4 = "";
                        if (itemStockValue != 0 && itemStock != 0)
                        {
                            //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = (itemStockValue / itemStock).ToString("N2");
                            avgCost4 = (itemStockValue / itemStock).ToString("N2");
                        }
                        else
                        {
                            //dgvStockreportDetails.Rows[i].Cells["dgvtxtAverageCost"].Value = "0.00";
                            avgCost4 = "0.00";
                        }
                        //dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock";

                        details.Add(new StockReportRowItems
                        {
                            ProductId = item.ToString(),
                            ProductCode = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productCode"))).FirstOrDefault().ToString(),
                            ProductName = (dt.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(n => n.Field<string>("productname"))).FirstOrDefault().ToString(),
                            StoreName = "",
                            InwardQty = "",
                            OutwardQty = "",
                            QtyBalance = itemStock.ToString("N2"),
                            StockValue = itemStockValue.ToString("N2"),
                            VoucherTypeName = "Opening Stock",
                            AverageCost = avgCost4,
                            Batch = "",
                            ProjectId = 0,
                            CategoryId = 0,
                            UnitName = ""
                        });
                        stockValue2 += itemStockValue;
                        i++;
                    }
                }
                // ================================== end region ================================ //
                //decimal productID = Convert.ToDecimal(dgvStockreportDetails.Rows[i - 1].Cells["productId"].Value);
                #region what is this block of code meant for
                /*
                decimal inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == prevProductId
                                    select p.Field<decimal>("inwardQty")).Sum();
                decimal outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == prevProductId
                                     select p.Field<decimal>("outwardQty")).Sum();

                dgvStockreportDetails.Rows.Add();
                dgvStockreportDetails.Rows[dgvStockreportDetails.Rows.Count - 1].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                dgvStockreportDetails.Rows[i].Cells["productId"].Value = 0;
                dgvStockreportDetails.Rows[i].Cells["productName"].Value = "";
                dgvStockreportDetails.Rows[i].Cells["godownName"].Value = "";
                dgvStockreportDetails.Rows[i].Cells["inwardQty"].Value = (Convert.ToDecimal(inwardqt) + openingQuantityIn).ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["outwardQty"].Value = (Convert.ToDecimal(outwardqt) + openingQuantityOut).ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["qtyBalalance"].Value = qtyBal.ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = stockValue.ToString("N2");
                //dgvStockreportDetails.Rows[i].Cells["stockValue"].Value = (stockValue + dcOpeningStockForRollOver).ToString("N2");
                dgvStockreportDetails.Rows[i].Cells["voucherTypeName"].Value = "Sub Total:- " + dgvStockreportDetails.Rows[i != 0 ? i - 1 : i].Cells["productName"].Value; //+ " In " + dgvStockreportDetails.Rows[i !=0 ? i - 1: i].Cells["godownName"].Value;
                */
                #endregion
                //decimal totalAsset = totalAssetValue + stockValue;
                //total inward and outward quantity and total stock value
                decimal totalAsset = totalAssetValue + stockValue + stockValue2;// - dcOpeningStockForRollOver;
                //dgvStockreportDetails.Rows.Add();
                //dgvStockreportDetails.Rows.Add();
                //dgvStockreportDetails.Rows[i + 1].Cells["stockValue"].Value = "==========";
                //dgvStockreportDetails.Rows.Add();
                //dgvStockreportDetails.Rows[i + 2].DefaultCellStyle.BackColor = Color.LightSteelBlue;
                //dgvStockreportDetails.Rows[i + 2].Cells["qtyBalalance"].Value = (totalQuantityIn - totalQuantityOut).ToString("N2");

                string avgCost5 = "";
                if (totalQuantityIn - totalQuantityOut != 0)
                {
                    //dgvStockreportDetails.Rows[i + 2].Cells["dgvtxtAverageCost"].Value = (totalAsset / (totalQuantityIn - totalQuantityOut)).ToString("N2");
                    avgCost5 = (totalAsset / (totalQuantityIn - totalQuantityOut)).ToString("N2");
                }
                //dgvStockreportDetails.Rows[i + 2].Cells["stockValue"].Value = totalAsset.ToString("N2");
                //dgvStockreportDetails.Rows[i + 2].Cells["voucherTypeName"].Value = "Total Stock Value:";
                //dgvStockreportDetails.Rows.Add();
                //dgvStockreportDetails.Rows[i + 3].Cells["stockValue"].Value = "==========";
                details.Add(new StockReportRowItems
                {
                    StockValue = "==========",
                });
                details.Add(new StockReportRowItems
                {
                    QtyBalance = string.IsNullOrEmpty((totalQuantityIn - totalQuantityOut).ToString("N2")) ? "" : (totalQuantityIn - totalQuantityOut).ToString("N2"),
                    AverageCost = string.IsNullOrEmpty(avgCost5) ? "" : avgCost5,
                    StockValue = string.IsNullOrEmpty(totalAsset.ToString("N2")) ? "" : totalAsset.ToString("N2")
                    
                });
                details.Add(new StockReportRowItems
                {
                    StockValue = "==========",
                });
                //}
                master.StockReportRowItem = details;
            }

            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }
            return master;
        }

        [HttpPost]
        public StockReportRow StockReportDetails90(StockReportSearch input)
        {
            StockReportRow master = new StockReportRow();
            List<StockReportRowItems> details = new List<StockReportRowItems>();

            try
            {
                var prodDtbl = new DataTable();
                prodDtbl = new ProductSP().ProductViewAll();
                for (int k = 0; k < prodDtbl.Rows.Count; k++)
                {
                    var FinancialDate = Convert.ToDateTime(prodDtbl.Rows[k]["effectiveDate"].ToString());

                    if(FinancialDate >= input.FromDate && FinancialDate <= input.ToDate)
                    {
                        AllItems.Add(prodDtbl.Rows[k]["productCode"].ToString());
                    }
                }

                GodownSP spGodown = new GodownSP();
                var storeDtbl = new DataTable();
                storeDtbl = spGodown.GodownViewAll();
                for (int k = 0; k < storeDtbl.Rows.Count; k++)
                {

                    AllStores.Add(Convert.ToDecimal(storeDtbl.Rows[k]["godownId"]));
                }

                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                string calculationMethod = string.Empty;
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", input.ToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", input.FromDate.ToString());
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : input.FromDate;
                DateTime lastFinYearEnd = DateTime.UtcNow;
                try
                {
                    lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : input.FromDate.AddDays(-1);
                }
                catch (Exception ex)
                {

                }

                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    //lastFinYearEnd = input.FromDate.AddDays(-1);
                    lastFinYearEnd = input.ToDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                }
                decimal currentProductId = 0, prevProductId = 0, prevStoreId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0;
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal stockValuePerItemPerStore = 0, totalStockValue = 0;
                decimal inwardQtyPerStore = 0, outwardQtyPerStore = 0, totalQtyPerStore = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "", productName = "", storeName = "";
                int i = 0;


                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };


                DataTable dtbl = new DataTable();
                DataTable dtblItem = new DataTable();
                DataTable dtblStore = new DataTable();
                DataTable dtblOpeningStocks = new DataTable();
                DataTable dtblOpeningStockPerItemPerStore = new DataTable();
                MATFinancials.StockPostingSP spstock = new MATFinancials.StockPostingSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                decimal productId = input.ProductId, storeId = input.StoreId;
                string produceCode = input.ProductCode, batch = input.BatchNo, referenceNo = input.RefNo;
                input.ToDate = input.ToDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                //dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text), "", "");
                //dtblOpeningStocks = spstock.StockReportDetailsGridFill(0, 0, "All", PublicVariables._dtFromDate, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), "", "");
                dtbl = spstock.StockReportDetailsGridFill1(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);
                if (batch != "" || input.ProjectId != 0 || input.CategoryId != 0 || productId != 0 || storeId != 0 || produceCode != "")
                    dtblOpeningStocks = spstock.StockReportDetailsGridFill1(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);
                else
                    dtblOpeningStocks = spstock.StockReportDetailsGridFill1(productId, storeId, batch, lastFinYearStart, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);


                if (AllItems.Any())
                {
                    foreach (var item in AllItems)
                    {
                        // ======================================== for each item begins here ====================================== //
                        dtblItem = new DataTable();
                        if (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item).Any())
                        {
                            dtblItem = (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item)).CopyToDataTable();
                            productName = (dtbl.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(m => m.Field<string>("productName")).FirstOrDefault().ToString());
                        }
                        if (dtblItem != null && dtblItem.Rows.Count > 0)
                        {
                            if (AllStores.Any())
                            {
                                foreach (var store in AllStores)
                                {
                                    // =================================== for that item, for each store begins here ============================== //
                                    qtyBal = 0; stockValue = 0;
                                    stockValuePerItemPerStore = 0; //dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    dtblStore = new DataTable();

                                    // ************************ insert opening stock per item per store before other transactions ******************** //
                                    dtblOpeningStockPerItemPerStore = new DataTable();
                                    if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                    {
                                        dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store)).CopyToDataTable();
                                        if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                        {
                                            computedAverageRate = rate2;
                                            var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, item, store);
                                            dcOpeningStockForRollOver = itemParams.Item1;
                                            openingQuantity = itemParams.Item2;
                                            openingQuantityIn = openingQuantity; // itemParams.Item3;
                                            openingQuantityOut = 0; // itemParams.Item4;
                                            //qtyBal += openingQuantity;
                                            qtyBal = openingQuantity;
                                            if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                            {
                                                computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                                stockValue = qtyBal * computedAverageRate;
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = item,
                                                ProductCode = item,
                                                ProductName = productName,
                                                StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                                Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                                InwardQty = openingQuantityIn.ToString("N2"),
                                                OutwardQty = openingQuantityOut.ToString("N2"),
                                                Rate = "",
                                                AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                                QtyBalance = openingQuantity.ToString("N2"),
                                                StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                                VoucherTypeName = "Opening Stock "
                                            });

                                            //dgvStockDetailsReport.Rows[i].Cells["productId"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productName"].Value = productName;
                                            //dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            //Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(); ;
                                            //dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                            //dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                            //dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";
                                            totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                            totalQtyPerStore += openingQuantity;
                                            i++;
                                        }
                                    }
                                    // ************************ insert opening stock per item per store before other transactions end ******************** //

                                    if (dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).Any())
                                    {
                                        dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).CopyToDataTable();
                                    }
                                    if (dtblStore != null && dtblStore.Rows.Count > 0)
                                    {
                                        storeName = (dtblStore.AsEnumerable().Where(m => m.Field<decimal>("godownId") == store).Select(m => m.Field<string>("godownName")).FirstOrDefault().ToString());
                                        foreach (DataRow dr in dtblStore.Rows)
                                        {
                                            // opening stock has been factored in thinking there was no other transaction, now other transactions factored it in again, so remove it
                                            totalStockValue -= Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            //dgvStockDetailsReport.Rows[i-1].DefaultCellStyle.BackColor = Color.White;
                                            //dgvStockDetailsReport.Rows[i-1].Cells["rate"].Value = "";
                                            dcOpeningStockForRollOver = 0;  // reset the opening stock because of the loop

                                            currentProductId = Convert.ToDecimal(dr["productId"]);
                                            productCode = dr["productCode"].ToString();
                                            string voucherType = "", refNo = "";
                                            inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                                            decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                                            string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                                            var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;
                                            if (!isOpeningRate)
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }

                                            //if (purchaseRate == 0)
                                            //{
                                            //    purchaseRate = rate2;
                                            //}

                                            if (prevStoreId != store)
                                            {
                                                previousRunningAverage = purchaseRate;
                                            }
                                            if (previousRunningAverage != 0 && currentProductId == prevProductId)
                                            {
                                                purchaseRate = previousRunningAverage;
                                            }
                                            refNo = dr["refNo"].ToString();
                                            if (againstVoucherType != "NA")
                                            {
                                                refNo = dr["againstVoucherNo"].ToString();
                                                isAgainstVoucher = true;
                                                againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                                                string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                                                string typeOfVoucher = conn.getSingleValue(queryString);
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note".ToLower() && typeOfVoucher.ToLower() == "sales invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                            }
                                            voucherType = dr["typeOfVoucher"].ToString();
                                            if (voucherType.ToLower() == "Stock Journal".ToLower())
                                            {
                                                //voucherType = "Stock Transfer";
                                                string queryString = string.Format(" SELECT extra1 FROM tbl_StockJournalMaster WHERE voucherNo = '{0}' ", dr["refNo"]);
                                                voucherType = conn.getSingleValue(queryString);
                                            }
                                            if (outwardQty2 > 0)
                                            {
                                                if (voucherType.ToLower() == "Sales Invoice".ToLower() && outwardQty2 > 0)
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate;
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Material Receipt".ToLower())
                                                {
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue -= outwardQty2 * rate2;
                                                }
                                                else if (outwardQty2 > 0 && (voucherType.ToLower() == "Rejection Out".ToLower()) || (voucherType.ToLower() == "Purchase Return".ToLower())
                                                    || (voucherType.ToLower() == "Sales Return".ToLower()) || (voucherType.ToLower() == "Rejection In".ToLower() || voucherType.ToLower() == "Stock Transfer".ToLower()))
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;// 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }

                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Sales Order".ToLower())
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else
                                                {
                                                    if (qtyBal != 0)
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Return".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Rejection In".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Invoice".ToLower() && inwardQty2 > 0)
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;

                                                }
                                                else
                                                {
                                                    computedAverageRate = previousRunningAverage;// (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Delivery Note".ToLower())
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Opening Stock".ToLower())
                                            {
                                                openingQuantityIn = 0;
                                                if (qtyBal != 0)
                                                {
                                                    //computedAverageRate = (stockValue / qtyBal);

                                                    qtyBal += inwardQty2 - (inwardQty2 + outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }
                                                else
                                                {
                                                    //computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - (inwardQty2 + outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }
                                            }
                                            else
                                            {
                                                // hndles other transactions such as purchase, opening balance, etc
                                                qtyBal += inwardQty2 - outwardQty2;
                                                stockValue += (inwardQty2 * rate2);
                                            }

                                            // ------------------------------------------------------ //
                                            //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            string avgCost = "";
                                            if (stockValue != 0 && qtyBal != 0)
                                            {
                                                avgCost = (stockValue / qtyBal).ToString("N2");
                                                //avgCost = (stockValue / qtyBal * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }
                                            else
                                            {
                                                avgCost = computedAverageRate.ToString("N2");
                                                //avgCost = (computedAverageRate/ SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = dr["productId"].ToString(),
                                                ProductCode = dr["productCode"].ToString(),
                                                StoreName = dr["godownName"].ToString(),
                                                InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                                                OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                                                Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                                AverageCost = avgCost,
                                                QtyBalance = qtyBal.ToString("N2"),
                                                StockValue = stockValue.ToString("N2"),
                                                VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType,
                                                RefNo = refNo,
                                                Batch = string.IsNullOrEmpty(dr["Batch"].ToString()) ? "" : dr["Batch"].ToString(),
                                                Date = Convert.ToDateTime(dr["date"]).ToShortDateString(),
                                                ProductName = dr["productName"].ToString(),
                                                ProjectId = Convert.ToDecimal(dr["projectId"]),
                                                CategoryId = Convert.ToDecimal(dr["categoryId"]),
                                                UnitName = dr["unitName"].ToString()
                                            });
                                            isAgainstVoucher = false;

                                            stockValuePerItemPerStore = Convert.ToDecimal(stockValue);
                                            prevProductId = currentProductId;
                                            previousRunningAverage = Convert.ToDecimal(avgCost);
                                            inwardQtyPerStore += Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQtyPerStore += Convert.ToDecimal(dr["outwardQty"]);
                                            i++;
                                        }
                                        // ===================== for every store for that item, put the summary =================== //
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = "",
                                            ProductCode = item,
                                            StoreName = storeName,
                                            InwardQty = (inwardQtyPerStore + openingQuantityIn).ToString("N2"),
                                            OutwardQty = ("-") + (outwardQtyPerStore + openingQuantityOut).ToString("N2"),
                                            Rate = "",
                                            AverageCost = "",
                                            QtyBalance = qtyBal.ToString("N2"),
                                            StockValue = stockValuePerItemPerStore.ToString("N2"),
                                            VoucherTypeName = "Sub total: ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = ""
                                        });

                                        totalStockValue += Convert.ToDecimal(stockValuePerItemPerStore.ToString("N2"));

                                        totalQtyPerStore += (inwardQtyPerStore - outwardQtyPerStore) /*+ openingQuantity*/;
                                        i++;
                                        openingQuantity = 0;
                                        inwardQtyPerStore = 0;
                                        outwardQtyPerStore = 0;
                                        qtyBal = 0;
                                        stockValue = 0;
                                        stockValuePerItemPerStore = 0;
                                        //dgvStockDetailsReport.Rows.Add();
                                        i++;
                                    }
                                    dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    prevStoreId = store;
                                }
                            }
                        }
                        // *** if no transaction for that item for all stores for that period add the ockock for all stores per store *** //
                        else
                        {
                            foreach (var store in AllStores)
                            {
                                stockValuePerItemPerStore = 0;
                                dtblStore = new DataTable();

                                // ************************ insert opening stock per item per store ******************** //
                                dtblOpeningStockPerItemPerStore = new DataTable();
                                if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                {
                                    dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item)).CopyToDataTable();
                                    if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                    {
                                        computedAverageRate = rate2;
                                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, item, store);
                                        dcOpeningStockForRollOver = itemParams.Item1;
                                        openingQuantity = itemParams.Item2;
                                        openingQuantityIn = openingQuantity; // itemParams.Item3;
                                        openingQuantityOut = 0; // itemParams.Item4;
                                        qtyBal += openingQuantity;
                                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                        {
                                            //computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                            //stockValue = openingQuantity * computedAverageRate;
                                        }
                                        var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = item,
                                            ProductCode = item,
                                            StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                        Where(l => l.Field<string>("productCode") == item).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                            InwardQty = openingQuantityIn.ToString("N2"),
                                            OutwardQty = openingQuantityOut.ToString("N2"),
                                            Rate = "",
                                            AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                            //AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2") : "",
                                            QtyBalance = openingQuantity.ToString("N2"),
                                            StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                            VoucherTypeName = "Opening Stock ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = ""
                                        });

                                        totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                        dcOpeningStockForRollOver = 0;
                                        totalQtyPerStore += openingQuantity;
                                        openingQuantityIn = 0;
                                        openingQuantityOut = 0;
                                        openingQuantity = 0;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }

                // ************************ Calculate Total **************************** //
                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var avgCost2 = "";
                if (totalQtyPerStore != 0)
                {
                    avgCost2 = (totalStockValue / totalQtyPerStore).ToString("N2");
                }
                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = avgCost2,
                    QtyBalance = totalQtyPerStore.ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    VoucherTypeName = "Total Stock Value:",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });
            }

            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }
            master.TotalAverageCost = 0;
            master.TotalQtyBalance = 0;
            master.TotalStockValue = 0;
            master.StockReportRowItem = details;
            return master;
        }

        [HttpPost]
        public StockReportRow StockReportDetails(StockReportSearch input)
        {
           

            StockReportRow master = new StockReportRow();
            List<StockReportRowItems> details = new List<StockReportRowItems>();

            try
            {
                DataTable prodDtbl = new DataTable();
                prodDtbl = new ProductSP().ProductViewAll();
                for (int k = 0; k < prodDtbl.Rows.Count; k++)
                {
                    var FinancialDate = Convert.ToDateTime(prodDtbl.Rows[k]["effectiveDate"].ToString());

                    if (FinancialDate >= input.FromDate && FinancialDate <= input.ToDate)
                    {

                    }

                    var newpro = new productModel()
                    {
                        ProductCode = prodDtbl.Rows[k]["productCode"].ToString(),
                        ProductId = prodDtbl.Rows[k]["productId"].ToString()
                    };

                    AllItemss.Add(newpro);
                }

                AllItemss = AllItemss.ToList().Distinct().ToList();
                GodownSP spGodown = new GodownSP();
                DataTable storeDtbl = new DataTable();
                storeDtbl = spGodown.GodownViewAll();
                for (int k = 0; k < storeDtbl.Rows.Count; k++)
                {
                   
                    AllStores.Add(Convert.ToDecimal(storeDtbl.Rows[k]["godownId"]));
                }

                AllStores = AllStores.ToList().Distinct().ToList();
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                string calculationMethod = string.Empty;
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = DateTime.UtcNow;
                try
                {
                    lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate/*.AddDays(-1)*/;
                }
                catch (Exception ex)
                {

                }

                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    //lastFinYearEnd = input.FromDate.AddDays(-1);
                    lastFinYearEnd = input.ToDate/*.AddHours(23).AddMinutes(59).AddSeconds(59)*/;

                }

                lastFinYearEnd = input.ToDate;
                decimal currentProductId = 0, prevProductId = 0, prevStoreId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0;
                decimal qtyBal = 0, stockValue = 0, OpeningStockValue =0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal stockValuePerItemPerStore = 0, totalStockValue = 0;
                decimal inwardQtyPerStore = 0, outwardQtyPerStore = 0, totalQtyPerStore = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "", productName = "", storeName = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };
                DataTable dtbl = new DataTable();

                DataTable dtbl2 = new DataTable();
                DataTable dtblItem = new DataTable();
                DataTable dtblStore = new DataTable();
                DataTable dtblOpeningStocks = new DataTable();

                DataTable dtblOpeningStocks2 = new DataTable();

                DataTable dtblOpeningStockPerItemPerStore = new DataTable();
                var spstock = new MATFinancials.StockPostingSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                decimal productId = input.ProductId, storeId = input.StoreId;
                string produceCode = input.ProductCode, batch = input.BatchNo, referenceNo = input.RefNo;
                input.ToDate = input.ToDate/*.AddHours(23).AddMinutes(59).AddSeconds(59)*/;

                var firstFinancialYear = context.tbl_FinancialYear.AsEnumerable().OrderBy(x => x.fromDate).First();
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, firstFinancialYear.fromDate.Value, input.FromDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));

                if (batch != "" || input.ProjectId != 0 || input.CategoryId != 0 || productId != 0 || storeId != 0 || produceCode != "")

                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));
                else
                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));


                // StockReportDetailsGridFill
                DateTime BusinessBeginDate = DateTime.Now;
                DateTime BusinessBeginEndDate = DateTime.Now;


                try
                {
                    BusinessBeginEndDate = new DateTime(input.FromDate.Year, input.FromDate.Month, input.FromDate.Day);

                    BusinessBeginEndDate = BusinessBeginEndDate.AddDays(-1);

                    var ListLastFyear = context.tbl_FinancialYear.Where(fy => fy.fromDate < input.FromDate && fy.toDate < input.FromDate).ToList();

                    var BalanceBeginDate =  new DateTime(input.FromDate.Year, input.FromDate.Month, input.FromDate.Day);
                
                    if (ListLastFyear.Count > 0)
                    {
                        BalanceBeginDate = ListLastFyear.Min(cfy => cfy.fromDate.Value);
                    }

                    BusinessBeginDate = BalanceBeginDate;
                }
                catch (Exception dateEx)
                {

                }

                dtbl2 = spstock.StockReportDetailsGridFill(productId, storeId, batch, BusinessBeginDate, BusinessBeginEndDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));
                if (batch != "" || input.ProjectId != 0 || input.CategoryId != 0 || productId != 0 || storeId != 0 || produceCode != "")

                    dtblOpeningStocks2 = spstock.StockReportDetailsGridFill(productId, storeId, batch, BusinessBeginDate, BusinessBeginEndDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));
                else
                    dtblOpeningStocks2 = spstock.StockReportDetailsGridFill(productId, storeId, batch, BusinessBeginDate, BusinessBeginEndDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));

                var datatblUpper = new DataTable();
                datatblUpper = spstock.StockReportDetailsGridFill(productId, storeId, batch, BusinessBeginDate, BusinessBeginEndDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));

                if (AllItemss.Any())
                {
                    foreach (var item in AllItemss)
                    {
                        // ======================================== for each item begins here ====================================== //

                        dtblItem = new DataTable();

                        if (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item.ProductCode).Any())
                        {
                            dtblItem = (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item.ProductCode)).CopyToDataTable();

                            productName = (dtbl.AsEnumerable().Where(m => m.Field<string>("productCode") == item.ProductCode).Select(m => m.Field<string>("productName")).FirstOrDefault().ToString());
                        }

                        if(productName.ToLower().Contains("CHINCHIN".ToLower()) || productName.ToLower().Equals("CHINCHIN".ToLower()))
                        {

                        }

                        if (dtblItem != null && dtblItem.Rows.Count > 0)
                        {
                            if (AllStores.Any())
                            {
                                foreach (var store in AllStores)
                                {
                                    // =================================== for that item, for each store begins here ============================== //
                                    var prolistDist = AllItemss.Where(pl => pl == item).ToList();
                                    var strlistDist = AllStores.Where(pl => pl == store).ToList();

                                   // dataupper12 =  (datatblUpper.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productId") == item.ProductId)).CopyToDataTable();

                                   var dataupper12 = spstock.StockReportDetailsGridFill(Convert.ToDecimal(item.ProductId), store, batch, BusinessBeginDate, BusinessBeginEndDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));

                                    var OpenBalance = GetOpenningBalanceStockReportDetails(input, store.ToString(), item.ProductCode, item.ProductId , dtblOpeningStocks2,  dtbl2 , prolistDist, strlistDist , dataupper12, BusinessBeginDate, BusinessBeginEndDate);

                                    decimal ResultOpenBalanceQty = 0;
                                    qtyBal = 0;
                                    stockValue = 0;
                                    stockValuePerItemPerStore = 0; //dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;

                                    if (OpenBalance.Count > 0)
                                    {
                                        details.AddRange(OpenBalance);
                                        ResultOpenBalanceQty = OpenBalance[0].QtyBalance2;
                                        qtyBal += ResultOpenBalanceQty;
                                        stockValue += OpenBalance[0].StockValue2;
                                    }
                                    else
                                    {
                                        //details.Add(new StockReportRowItems
                                        //{
                                        //    ProductId = "",
                                        //    ProductCode = item.ProductCode,
                                        //    StoreName = storeName,
                                        //    InwardQty = "", //(inwardQtyPerStore /*+ openingQuantityIn*/).ToString("N2"),
                                        //    OutwardQty = "", //("-") + (outwardQtyPerStore /*+ openingQuantityOut*/).ToString("N2"),
                                        //    Rate = "" /*Rate12.ToString("N2")*/,
                                        //    AverageCost = "0",
                                        //    QtyBalance = qtyBal.ToString("N2"),
                                        //    StockValue = "0",
                                        //    VoucherTypeName = "Opening Balance: ",
                                        //    RefNo = "Opening Balance",
                                        //    Batch = "Opening Balance",
                                        //    Date = BusinessBeginEndDate.ToShortDateString(),
                                        //    ProductName = productName,
                                        //    UnitName = "",
                                        //    CategoryId = 0,
                                        //    ProjectId = 0,
                                        //    QtyBalance2 = 0,
                                        //    StockValue2 = 0,
                                        //});
                                    }


                                    dtblStore = new DataTable();
                                    // ************************ insert opening stock per item per store before other transactions ******************** //
                                    dtblOpeningStockPerItemPerStore = new DataTable();
                                    if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item.ProductCode).Any())
                                    {
                                        dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store)).CopyToDataTable();
                                        if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                        {
                                            computedAverageRate = rate2;
                                            var itemParams = methodForStockValue.currentStockValue1235(lastFinYearStart, lastFinYearEnd, true, item.ProductCode, store);
                                            dcOpeningStockForRollOver = itemParams.Item1;
                                            openingQuantity = itemParams.Item2;
                                            openingQuantityIn =  itemParams.Item3;
                                            openingQuantityOut =  itemParams.Item4;
                                            // qtyBal += openingQuantity;
                                            // qtyBal = 0;
                                            // qtyBal = openingQuantity;

                                            if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                            {
                                                computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                                //stockValue = qtyBal * computedAverageRate;
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = item.ProductCode,
                                                ProductCode = item.ProductCode,
                                                ProductName = productName,
                                                StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                                Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                                InwardQty = openingQuantityIn.ToString("N2"),
                                                OutwardQty = openingQuantityOut.ToString("N2"),
                                                Rate = "",
                                                AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                                QtyBalance = openingQuantity.ToString("N2"),
                                                StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                                VoucherTypeName = "Opening Stock "
                                            });

                                            //dgvStockDetailsReport.Rows[i].Cells["productId"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productName"].Value = productName;
                                            //dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            //Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(); ;
                                            //dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                            //dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                            //dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";

                                            //New Comments
                                            totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); 
                                            // dcOpeningStockForRollOver;
                                            //totalQtyPerStore += openingQuantity;
                                            //i++;
                                            //Old Comments
                                        }
                                    }
                                    // ************************ insert opening stock per item per store before other transactions end ******************** //

                                    dtblStore = null;

                                    if (dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).Any())
                                    {
                                        //if (input.TransactionType.ToLower() == "All".ToLower())
                                        //{
                                            
                                        //}
                                        //else
                                        //{
                                        //    //dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store  && j.Field<string>("typeOfVoucher").ToLower() == input.TransactionType.ToLower()).CopyToDataTable();
                                        //}


                                        dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).CopyToDataTable();


                                    }

                                    if (dtblStore != null && dtblStore.Rows.Count > 0)
                                    {
                                            storeName = (dtblStore.AsEnumerable().Where(m => m.Field<decimal>("godownId") == store).Select(m => m.Field<string>("godownName")).FirstOrDefault().ToString());

                                        foreach (DataRow dr in dtblStore.Rows)
                                        {
                                            // opening stock has been factored in thinking there was no other transaction, now other transactions factored it in again, so remove it
                                            totalStockValue -= Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            //dgvStockDetailsReport.Rows[i-1].DefaultCellStyle.BackColor = Color.White;
                                            //dgvStockDetailsReport.Rows[i-1].Cells["rate"].Value = "";
                                            dcOpeningStockForRollOver = 0;  // reset the opening stock because of the loop

                                            currentProductId = Convert.ToDecimal(dr["productId"]);
                                            productCode = dr["productCode"].ToString();
                                            string voucherType = "", refNo = "";
                                            inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                                          
                                            decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                                            string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);

                                            var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            if (!isOpeningRate)
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }
                                            else
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }

                                            //if (purchaseRate == 0)
                                            //{
                                            //    purchaseRate = rate2;
                                            //}

                                            if (prevStoreId != store)
                                            {
                                                previousRunningAverage = purchaseRate;
                                            }

                                            if (previousRunningAverage != 0 && currentProductId == prevProductId)
                                            {
                                                purchaseRate = previousRunningAverage;
                                            }

                                            refNo = dr["refNo"].ToString();

                                            if (againstVoucherType != "NA")
                                            {
                                                refNo = dr["againstVoucherNo"].ToString();
                                                isAgainstVoucher = true;
                                                againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                                                string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                                                string typeOfVoucher = conn.getSingleValue(queryString);
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note".ToLower() && typeOfVoucher.ToLower() == "sales invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt".ToLower() && typeOfVoucher.ToLower() == "purchase invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                            }
                                        
                                            voucherType = dr["typeOfVoucher"].ToString();
                                         
                                            if (voucherType.ToLower() == "Stock Journal".ToLower())
                                            {
                                                //voucherType = "Stock Transfer";
                                                string queryString = string.Format(" SELECT extra1 FROM tbl_StockJournalMaster WHERE voucherNo = '{0}' ", dr["refNo"]);
                                                voucherType = conn.getSingleValue(queryString);
                                            }

                                            if (outwardQty2 > 0)
                                            {
                                                if (voucherType.ToLower() == "Sales Invoice".ToLower() && outwardQty2 > 0)
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate;
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Material Receipt".ToLower())
                                                {
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue -= outwardQty2 * rate2;
                                                }
                                                else if (outwardQty2 > 0 && (voucherType.ToLower() == "Rejection Out".ToLower()) || (voucherType.ToLower() == "Purchase Return".ToLower())
                                                    || (voucherType.ToLower() == "Sales Return".ToLower()) || (voucherType.ToLower() == "Rejection In".ToLower() || voucherType.ToLower() == "Stock Transfer".ToLower()))
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;// 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Sales Order".ToLower())
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else
                                                {
                                                    if (qtyBal != 0)
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Return".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Rejection In".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Invoice".ToLower() && inwardQty2 > 0)
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;

                                                }
                                                else
                                                {
                                                    computedAverageRate = previousRunningAverage;// (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Delivery Note".ToLower())
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Opening Stock".ToLower())
                                            {
                                               // openingQuantityIn = 0;

                                                if (qtyBal != 0)
                                                {
                                                    //computedAverageRate = (stockValue / qtyBal);

                                                    // qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }
                                                else
                                                {
                                                    //computedAverageRate = (stockValue / 1);
                                                    //qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }

                                                OpeningStockValue += (inwardQty2 * rate2);
                                            }
                                            else
                                            {
                                                // hndles other transactions such as purchase, opening balance, etc
                                                qtyBal += inwardQty2 - outwardQty2;
                                                stockValue += (inwardQty2 * rate2);
                                               
                                            }

                                            // ------------------------------------------------------ //
                                            //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            string avgCost = "";
                                            if (stockValue != 0 && qtyBal != 0)
                                            {
                                                avgCost = (stockValue / qtyBal).ToString("N2");
                                                //avgCost = (stockValue / qtyBal * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }
                                            else
                                            {
                                                avgCost = computedAverageRate.ToString("N2");
                                                //avgCost = (computedAverageRate/ SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }

                                           

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = dr["productId"].ToString(),
                                                ProductCode = dr["productCode"].ToString(),
                                                StoreName = dr["godownName"].ToString(),
                                                InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                                                OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                                                Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                                AverageCost = avgCost,
                                                QtyBalance = qtyBal.ToString("N2"),
                                                StockValue = stockValue.ToString("N2"),
                                                VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType,
                                                RefNo = refNo,
                                                Batch = string.IsNullOrEmpty(dr["Batch"].ToString()) ? "" : dr["Batch"].ToString(),
                                                Date = Convert.ToDateTime(dr["date"]).ToShortDateString(),
                                                ProductName = dr["productName"].ToString(),
                                                ProjectId = Convert.ToDecimal(dr["projectId"]),
                                                CategoryId = Convert.ToDecimal(dr["categoryId"]),
                                                UnitName = dr["unitName"].ToString()
                                            });

                                            isAgainstVoucher = false;

                                            stockValuePerItemPerStore = Convert.ToDecimal(stockValue);
                                            prevProductId = currentProductId;
                                            previousRunningAverage = Convert.ToDecimal(avgCost);
                                            inwardQtyPerStore += Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQtyPerStore += Convert.ToDecimal(dr["outwardQty"]);
                                            i++;
                                        }
                                        // ===================== for every store for that item, put the summary =================== //
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = "",
                                            ProductCode = item.ProductCode,
                                            StoreName = storeName,
                                            InwardQty = (inwardQtyPerStore /*+ openingQuantityIn*/).ToString("N2"),
                                            OutwardQty = ("-") + (outwardQtyPerStore /*+ openingQuantityOut*/).ToString("N2"),
                                            Rate = "",
                                            AverageCost = "",
                                            QtyBalance = qtyBal.ToString("N2"),
                                            StockValue = stockValuePerItemPerStore.ToString("N2"),
                                            VoucherTypeName = "Sub total: ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = "", 
                                        });

                                        totalStockValue += Convert.ToDecimal(stockValuePerItemPerStore.ToString("N2"));
                                        totalQtyPerStore += qtyBal /*(inwardQtyPerStore - outwardQtyPerStore) + openingQuantity*/;
                                       
                                        openingQuantity = 0;
                                        inwardQtyPerStore = 0;
                                        outwardQtyPerStore = 0;
                                        qtyBal = 0;
                                        stockValue = 0;
                                        stockValuePerItemPerStore = 0;
                                        //dgvStockDetailsReport.Rows.Add();
                                        i++;
                                    }

                                    dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    prevStoreId = store;
                                }
                            }
                        }
                        // *** if no transaction for that item for all stores for that period add the ockock for all stores per store *** //
                        else
                        {
                            foreach (var store in AllStores)
                            {
                                stockValuePerItemPerStore = 0;
                                dtblStore = new DataTable();
                                // ************************ insert opening stock per item per store ******************** //
                                dtblOpeningStockPerItemPerStore = new DataTable();
                                if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item.ProductCode).Any())
                                {
                                    dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item.ProductCode)).CopyToDataTable();

                                    if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                    {
                                        computedAverageRate = rate2;
                                        var itemParams = methodForStockValue.currentStockValue1235(lastFinYearStart, lastFinYearEnd, true, item.ProductCode, store);
                                        dcOpeningStockForRollOver = itemParams.Item1;
                                        openingQuantity = itemParams.Item2;
                                        openingQuantityIn =  itemParams.Item3;
                                        openingQuantityOut =  itemParams.Item4;
                                        qtyBal += openingQuantity;
                                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                        {
                                            //computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                            //stockValue = openingQuantity * computedAverageRate;
                                        }
                                        var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = item.ProductCode,
                                            ProductCode = item.ProductCode,
                                            StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                        Where(l => l.Field<string>("productCode") == item.ProductCode).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                            InwardQty = openingQuantityIn.ToString("N2"),
                                            OutwardQty = openingQuantityOut.ToString("N2"),
                                            Rate = "",
                                            AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                            //AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2") : "",
                                            QtyBalance = openingQuantity.ToString("N2"),
                                            StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                            VoucherTypeName = "Opening Stock ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = ""
                                        });

                                        totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                        dcOpeningStockForRollOver = 0;
                                        totalQtyPerStore += openingQuantity;
                                        openingQuantityIn = 0;
                                        openingQuantityOut = 0;
                                        openingQuantity = 0;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }

                // ************************ Calculate Total **************************** //
                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var avgCost2 = "";
                if (totalQtyPerStore != 0)
                {
                    avgCost2 = (totalStockValue / totalQtyPerStore).ToString("N2");
                }

                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = avgCost2,
                    QtyBalance = totalQtyPerStore.ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    VoucherTypeName = "Total Stock Value:",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var tyr = OpeningStockValue;



            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }

            master.TotalAverageCost = 0;
            master.TotalQtyBalance = 0;
            master.TotalStockValue = 0;
            master.StockReportRowItem = details;

            //var OpenningBalance = GetOpenningBalanceStockReportDetails(input);
           // master;//.OpenningBalanceStockDetails1 = OpenningBalance;

            return master;
        }

        //StockReportDetailsGridFill

        public List<StockReportRowItems> GetOpenningBalanceStockReportDetails(StockReportSearch inputSearch, string storeId1, string ProductCode1, string ProductCodeId, DataTable dtblOpeningStocks2, DataTable dtbl2, List<productModel> products, List<decimal> AllStoresByEach, DataTable datatblUpper, DateTime BusinessBeginDate, DateTime BusinessBeginEndDate)
        {
            StockReportRow master = new StockReportRow();
            List<StockReportRowItems> details = new List<StockReportRowItems>();
            decimal Rate12 = 0;
            var input = new StockReportSearch()
            {
                BatchNo = inputSearch.BatchNo,
                CategoryId = inputSearch.CategoryId,
                FromDate = BusinessBeginEndDate,
                ProductCode = ProductCode1,
                ProductId = Convert.ToDecimal(ProductCodeId),
                ProjectId = inputSearch.ProjectId,
                RefNo = inputSearch.RefNo,
                StoreId = Convert.ToDecimal(storeId1),
                ToDate = BusinessBeginDate,
                TransactionType = inputSearch.TransactionType,
            };

            // input.ProductCode = ProductCode1;
            input.ProductId = Convert.ToDecimal(ProductCodeId);
            input.StoreId = Convert.ToDecimal(storeId1);
          
            try
            {
                //input.ToDate = input.ToDate.AddDays(-1);

                //var ListLastFyear = context.tbl_FinancialYear.Where(fy => fy.fromDate < input.FromDate && fy.toDate < input.FromDate).ToList();

                //var BalanceDateDate = input.FromDate;

                //if (ListLastFyear.Count > 0)
                //{
                //    BalanceDateDate = ListLastFyear.Min(cfy => cfy.fromDate.Value);
                //}

                //input.FromDate = BalanceDateDate;
            }
            catch (Exception dateEx)
            {

            }

            try
            {
                // DataTable prodDtbl = new DataTable();
                //prodDtbl = new ProductSP().ProductViewAll();
                //for (int k = 0; k < prodDtbl.Rows.Count; k++)
                //{
                //    var FinancialDate = Convert.ToDateTime(prodDtbl.Rows[k]["effectiveDate"].ToString());
                //    if (FinancialDate >= input.FromDate && FinancialDate <= input.ToDate)
                //    {

                //    }
                //    AllItems.Add(prodDtbl.Rows[k]["productCode"].ToString());
                //}

                var OneItems = new List<productModel>();
                OneItems.AddRange(products);
                OneItems = OneItems.ToList().Distinct().ToList();

                //GodownSP spGodown = new GodownSP();
                // DataTable storeDtbl = new DataTable();
                //storeDtbl = spGodown.GodownViewAll();
                //for (int k = 0; k < storeDtbl.Rows.Count; k++)
                //{
                //    AllStores.Add(Convert.ToDecimal(storeDtbl.Rows[k]["godownId"]));
                //}

                var OneStore = new List<decimal>();
                OneStore.AddRange(AllStoresByEach);
                OneStore = OneStore.ToList().Distinct().ToList();

                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                string calculationMethod = string.Empty;
             
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
             
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();

                //string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                //string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                //DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;

                DateTime lastFinYearStart = input.FromDate;

                DateTime lastFinYearEnd = DateTime.UtcNow;

                lastFinYearEnd = input.ToDate;

                decimal currentProductId = 0, prevProductId = 0, prevStoreId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0;
                decimal qtyBal = 0, stockValue = 0, OpeningStockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal stockValuePerItemPerStore = 0, totalStockValue = 0;
                decimal inwardQtyPerStore = 0, outwardQtyPerStore = 0, totalQtyPerStore = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "", productName = "", storeName = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };
                DataTable dtbl = new DataTable();
                DataTable dtblItem = new DataTable();
                DataTable dtblStore = new DataTable();
                DataTable dtblOpeningStocks = new DataTable();
                DataTable dtblOpeningStockPerItemPerStore = new DataTable();
                var spstock = new MATFinancials.StockPostingSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                decimal productId = input.ProductId, storeId = input.StoreId;
                string produceCode = input.ProductCode, batch = input.BatchNo, referenceNo = input.RefNo;
                input.ToDate = input.ToDate;

                //dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text), "", "");
                //dtblOpeningStocks = spstock.StockReportDetailsGridFill(0, 0, "All", PublicVariables._dtFromDate, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), "", "");

                // dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));

                dtblOpeningStocks = dtblOpeningStocks2;
                dtbl = dtbl2;

                //if (batch != "" || input.ProjectId != 0 || input.CategoryId != 0 || productId != 0 || storeId != 0 || produceCode != "")

                //    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));
                //else
                //    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));

                if (OneItems.Any())
                {
                    foreach (var item in OneItems)
                    {
                        // ======================================== for each item begins here ====================================== //
                        dtblItem = new DataTable();

                        if (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item.ProductCode).Any())
                        {
                            dtblItem = (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item.ProductCode)).CopyToDataTable();

                            productName = (dtbl.AsEnumerable().Where(m => m.Field<string>("productCode") == item.ProductCode).Select(m => m.Field<string>("productName")).FirstOrDefault().ToString());
                        }

                        if (productName.ToLower().Contains("CHINCHIN".ToLower()) || productName.ToLower().Equals("CHINCHIN".ToLower()))
                        {

                        }

                        if (dtblItem != null && dtblItem.Rows.Count > 0)
                        {
                            if (OneStore.Any())
                            {
                                foreach (var store in OneStore)
                                {
                                    // =================================== for that item, for each store begins here ============================== //

                                    qtyBal = 0; stockValue = 0;
                                    stockValuePerItemPerStore = 0; //dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    dtblStore = new DataTable();

                                    // ************************ insert opening stock per item per store before other transactions ******************** //
                                    dtblOpeningStockPerItemPerStore = new DataTable();
                                    if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item.ProductCode).Any())
                                    {
                                        dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store)).CopyToDataTable();
                                        if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                        {
                                            computedAverageRate = rate2;
                                            var itemParams = methodForStockValue.currentStockValue12350(lastFinYearStart, lastFinYearEnd, true, item.ProductCode, store, datatblUpper);
                                            dcOpeningStockForRollOver = itemParams.Item1;
                                            openingQuantity = itemParams.Item2;
                                            openingQuantityIn = itemParams.Item3;
                                            openingQuantityOut = itemParams.Item4;
                                            qtyBal += openingQuantity;
                                            qtyBal = 0;
                                            // qtyBal = openingQuantity;
                                            if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                            {
                                                computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                                stockValue = qtyBal * computedAverageRate;
                                            }

                                            //details.Add(new StockReportRowItems
                                            //{
                                            //    ProductId = item,
                                            //    ProductCode = item,
                                            //    ProductName = productName,
                                            //    StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            //                    Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                            //    InwardQty = openingQuantityIn.ToString("N2"),
                                            //    OutwardQty = openingQuantityOut.ToString("N2"),
                                            //    Rate = "",
                                            //    AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                            //    QtyBalance = openingQuantity.ToString("N2"),
                                            //    StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                            //    VoucherTypeName = "Opening Stock "
                                            //});

                                            //dgvStockDetailsReport.Rows[i].Cells["productId"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productName"].Value = productName;
                                            //dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            //Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(); ;
                                            //dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                            //dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                            //dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";

                                            //New Comments
                                            totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            // dcOpeningStockForRollOver;
                                            //totalQtyPerStore += openingQuantity;
                                            //i++;
                                            //Old Comments
                                        }
                                    }
                                    // ************************ insert opening stock per item per store before other transactions end ******************** //

                                    dtblStore = null;

                                    if (dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).Any())
                                    {
                                        //if (input.TransactionType.ToLower() == "All".ToLower())
                                        //{

                                        //}
                                        //else
                                        //{
                                        //    //dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store  && j.Field<string>("typeOfVoucher").ToLower() == input.TransactionType.ToLower()).CopyToDataTable();
                                        //}


                                        dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).CopyToDataTable();


                                    }

                                    if (dtblStore != null && dtblStore.Rows.Count > 0)
                                    {
                                        storeName = (dtblStore.AsEnumerable().Where(m => m.Field<decimal>("godownId") == store).Select(m => m.Field<string>("godownName")).FirstOrDefault().ToString());


                                        foreach (DataRow dr in dtblStore.Rows)
                                        {
                                            // opening stock has been factored in thinking there was no other transaction, now other transactions factored it in again, so remove it
                                            totalStockValue -= Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            //dgvStockDetailsReport.Rows[i-1].DefaultCellStyle.BackColor = Color.White;
                                            //dgvStockDetailsReport.Rows[i-1].Cells["rate"].Value = "";
                                            dcOpeningStockForRollOver = 0;  // reset the opening stock because of the loop

                                            currentProductId = Convert.ToDecimal(dr["productId"]);
                                            productCode = dr["productCode"].ToString();
                                            string voucherType = "", refNo = "";
                                            inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);

                                            decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                                            string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);

                                            var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            if (!isOpeningRate)
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }
                                            else
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }

                                            //if (purchaseRate == 0)
                                            //{
                                            //    purchaseRate = rate2;
                                            //}

                                            if (prevStoreId != store)
                                            {
                                                previousRunningAverage = purchaseRate;
                                            }

                                            if (previousRunningAverage != 0 && currentProductId == prevProductId)
                                            {
                                                purchaseRate = previousRunningAverage;
                                            }

                                            refNo = dr["refNo"].ToString();

                                            if (againstVoucherType != "NA")
                                            {
                                                refNo = dr["againstVoucherNo"].ToString();
                                                isAgainstVoucher = true;
                                                againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                                                string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                                                string typeOfVoucher = conn.getSingleValue(queryString);
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note".ToLower() && typeOfVoucher.ToLower() == "sales invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt".ToLower() && typeOfVoucher.ToLower() == "purchase invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                            }

                                            voucherType = dr["typeOfVoucher"].ToString();

                                            if (voucherType.ToLower() == "Stock Journal".ToLower())
                                            {
                                                //voucherType = "Stock Transfer";
                                                string queryString = string.Format(" SELECT extra1 FROM tbl_StockJournalMaster WHERE voucherNo = '{0}' ", dr["refNo"]);
                                                voucherType = conn.getSingleValue(queryString);
                                            }

                                            if (outwardQty2 > 0)
                                            {
                                                if (voucherType.ToLower() == "Sales Invoice".ToLower() && outwardQty2 > 0)
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate;
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Material Receipt".ToLower())
                                                {
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue -= outwardQty2 * rate2;
                                                }
                                                else if (outwardQty2 > 0 && (voucherType.ToLower() == "Rejection Out".ToLower()) || (voucherType.ToLower() == "Purchase Return".ToLower())
                                                    || (voucherType.ToLower() == "Sales Return".ToLower()) || (voucherType.ToLower() == "Rejection In".ToLower() || voucherType.ToLower() == "Stock Transfer".ToLower()))
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;// 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Sales Order".ToLower())
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else
                                                {
                                                    if (qtyBal != 0)
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Return".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Rejection In".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Invoice".ToLower() && inwardQty2 > 0)
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;

                                                }
                                                else
                                                {
                                                    computedAverageRate = previousRunningAverage;// (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Delivery Note".ToLower())
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Opening Stock".ToLower())
                                            {
                                                // openingQuantityIn = 0;

                                                if (qtyBal != 0)
                                                {
                                                    //computedAverageRate = (stockValue / qtyBal);

                                                    // qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }
                                                else
                                                {
                                                    //computedAverageRate = (stockValue / 1);
                                                    //qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }

                                                OpeningStockValue += (inwardQty2 * rate2);
                                            }
                                            else
                                            {
                                                // hndles other transactions such as purchase, opening balance, etc
                                                qtyBal += inwardQty2 - outwardQty2;
                                                stockValue += (inwardQty2 * rate2);

                                            }

                                            // ------------------------------------------------------ //
                                            //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            string avgCost = "";
                                            if (stockValue != 0 && qtyBal != 0)
                                            {
                                                avgCost = (stockValue / qtyBal).ToString("N2");
                                                //avgCost = (stockValue / qtyBal * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }
                                            else
                                            {
                                                avgCost = computedAverageRate.ToString("N2");
                                                //avgCost = (computedAverageRate/ SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }

                                            //details.Add(new StockReportRowItems
                                            //{
                                            //    ProductId = dr["productId"].ToString(),
                                            //    ProductCode = dr["productCode"].ToString(),
                                            //    StoreName = dr["godownName"].ToString(),
                                            //    InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                                            //    OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                                            //    Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                            //    AverageCost = avgCost,
                                            //    QtyBalance = qtyBal.ToString("N2"),
                                            //    StockValue = stockValue.ToString("N2"),
                                            //    VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType,
                                            //    RefNo = refNo,
                                            //    Batch = string.IsNullOrEmpty(dr["Batch"].ToString()) ? "" : dr["Batch"].ToString(),
                                            //    Date = Convert.ToDateTime(dr["date"]).ToShortDateString(),
                                            //    ProductName = dr["productName"].ToString(),
                                            //    ProjectId = Convert.ToDecimal(dr["projectId"]),
                                            //    CategoryId = Convert.ToDecimal(dr["categoryId"]),
                                            //    UnitName = dr["unitName"].ToString()
                                            //});

                                            isAgainstVoucher = false;
                                            Rate12 += Convert.ToDecimal(dr["rate"]);
                                            stockValuePerItemPerStore = Convert.ToDecimal(stockValue);
                                            prevProductId = currentProductId;
                                            previousRunningAverage = Convert.ToDecimal(avgCost);
                                            inwardQtyPerStore += Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQtyPerStore += Convert.ToDecimal(dr["outwardQty"]);
                                            i++;
                                        }

                                        // ===================== for every store for that item, put the summary =================== //
                                        totalStockValue += Convert.ToDecimal(stockValuePerItemPerStore.ToString("N2"));

                                        totalQtyPerStore += qtyBal;
                                        var avgCost0 = (totalStockValue / totalQtyPerStore).ToString("N2");

                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = "",
                                            ProductCode = item.ProductCode,
                                            StoreName = storeName,
                                            InwardQty = "", //(inwardQtyPerStore /*+ openingQuantityIn*/).ToString("N2"),
                                            OutwardQty = "", //("-") + (outwardQtyPerStore /*+ openingQuantityOut*/).ToString("N2"),
                                            Rate = "" /*Rate12.ToString("N2")*/,
                                            AverageCost = avgCost0,
                                            QtyBalance = qtyBal.ToString("N2"),
                                            StockValue = totalStockValue.ToString("N2"),
                                            VoucherTypeName = "Opening Balance: ",
                                            RefNo = "Opening Balance",
                                            Batch = "Opening Balance",
                                            Date = input.ToDate.ToShortDateString(),
                                            ProductName = productName,
                                            UnitName = "",
                                            CategoryId = 0,
                                            ProjectId = 0,
                                             QtyBalance2 = qtyBal,
                                              StockValue2 = totalStockValue,
                                        });

                                        /*(inwardQtyPerStore - outwardQtyPerStore) + openingQuantity*/
                                        ;

                                        avgCost0 = "";
                                        totalQtyPerStore = 0;
                                        totalStockValue = 0;
                                        Rate12 = 0;
                                        openingQuantity = 0;
                                        inwardQtyPerStore = 0;
                                        outwardQtyPerStore = 0;
                                        qtyBal = 0;
                                        stockValue = 0;
                                        stockValuePerItemPerStore = 0;
                                        //dgvStockDetailsReport.Rows.Add();
                                        i++;
                                    }

                                    dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    prevStoreId = store;
                                }
                            }
                        }
                        // *** if no transaction for that item for all stores for that period add the ockock for all stores per store *** //
                        else
                        {
                            foreach (var store in AllStores)
                            {
                                stockValuePerItemPerStore = 0;
                                dtblStore = new DataTable();
                                // ************************ insert opening stock per item per store ******************** //
                                dtblOpeningStockPerItemPerStore = new DataTable();
                                if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item.ProductCode).Any())
                                {
                                    dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item.ProductCode)).CopyToDataTable();

                                    if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                    {
                                        computedAverageRate = rate2;
                                        var itemParams = methodForStockValue.currentStockValue12350(lastFinYearStart, lastFinYearEnd, true, item.ProductCode, store, datatblUpper);
                                        dcOpeningStockForRollOver = itemParams.Item1;
                                        openingQuantity = itemParams.Item2;
                                        openingQuantityIn = itemParams.Item3;
                                        openingQuantityOut = itemParams.Item4;
                                        qtyBal += openingQuantity;
                                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                        {
                                            //computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                            //stockValue = openingQuantity * computedAverageRate;
                                        }
                                        var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                        //details.Add(new StockReportRowItems
                                        //{
                                        //    ProductId = item.ProductCode,
                                        //    ProductCode = item.ProductCode,
                                        //    StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                        //                Where(l => l.Field<string>("productCode") == item.ProductCode).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                        //    InwardQty = openingQuantityIn.ToString("N2"),
                                        //    OutwardQty = openingQuantityOut.ToString("N2"),
                                        //    Rate = "",
                                        //    AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                        //    //AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2") : "",
                                        //    QtyBalance = openingQuantity.ToString("N2"),
                                        //    StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                        //    VoucherTypeName = "Opening Stock ",
                                        //    RefNo = "",
                                        //    Batch = "",
                                        //    Date = "",
                                        //    ProductName = productName,
                                        //    UnitName = ""
                                        //});

                                        totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                        dcOpeningStockForRollOver = 0;
                                        totalQtyPerStore += openingQuantity;
                                        openingQuantityIn = 0;
                                        openingQuantityOut = 0;
                                        openingQuantity = 0;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }

                // ************************ Calculate Total **************************** //
                //details.Add(new StockReportRowItems
                //{
                //    ProductId = "",
                //    ProductCode = "",
                //    StoreName = "",
                //    InwardQty = "",
                //    OutwardQty = "",
                //    Rate = "",
                //    AverageCost = "",
                //    QtyBalance = "",
                //    StockValue = "==========",
                //    VoucherTypeName = "",
                //    RefNo = "",
                //    Batch = "",
                //    Date = "",
                //    ProductName = "",
                //    UnitName = ""
                //});

                var avgCost2 = "";
                if (totalQtyPerStore != 0)
                {
                    avgCost2 = (totalStockValue / totalQtyPerStore).ToString("N2");
                }

                //details.Add(new StockReportRowItems
                //{
                //    ProductId = "",
                //    ProductCode = "",
                //    StoreName = "",
                //    InwardQty = "",
                //    OutwardQty = "",
                //    Rate = "",
                //    AverageCost = avgCost2,
                //    QtyBalance = totalQtyPerStore.ToString("N2"),
                //    StockValue = totalStockValue.ToString("N2"),
                //    VoucherTypeName = "Total Stock Value:",
                //    RefNo = "",
                //    Batch = "",
                //    Date = "",
                //    ProductName = "",
                //    UnitName = ""
                //});

                //details.Add(new StockReportRowItems
                //{
                //    ProductId = "",
                //    ProductCode = "",
                //    StoreName = "",
                //    InwardQty = "",
                //    OutwardQty = "",
                //    Rate = "",
                //    AverageCost = "",
                //    QtyBalance = "",
                //    StockValue = "==========",
                //    VoucherTypeName = "",
                //    RefNo = "",
                //    Batch = "",
                //    Date = "",
                //    ProductName = "",
                //    UnitName = ""
                //});

                var tyr = OpeningStockValue;
            }

            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }

            master.TotalAverageCost = 0;
            master.TotalQtyBalance = 0;
            master.TotalStockValue = 0;
            master.StockReportRowItem = details;

           // var OpenningBalance = GetOpenningBalanceStockReportDetails(input);
          //  master.OpenningBalanceStockDetails = OpenningBalance;

            return details;
        }

        private List<OpenningBalanceStockReadModel> GetOpenningBalanceStockReportDetails2(StockReportSearch input)
        {

            var SumOpenningBalance = new List<OpenningBalanceStockReadModel>();
            var SearchTypeVoucher = "All (*)";
            var SearchTypeStore = "All (*)";
            var SearchTypeProduct = "All (*)";
            var SearchTypeBatch = "All (*)";
            var SearchTypeProject = "All (*)";
            var SearchTypeCategory = "All (*)";

            try
            {
                try
                {
                    input.ToDate = input.FromDate.AddDays(-1);

                    var ListLastFyear = context.tbl_FinancialYear.Where(fy => fy.fromDate < input.FromDate && fy.toDate < input.FromDate).ToList();

                    var BalanceDateDate = input.FromDate;
               
                    if (ListLastFyear.Count > 0)
                    {
                        BalanceDateDate = ListLastFyear.Min(cfy => cfy.fromDate.Value);
                    }

                    input.FromDate = BalanceDateDate;
                }
                catch(Exception dateEx)
                {

                }

                var AllProduct = context.tbl_Product.ToList();
                var AllStore = context.tbl_Godown.ToList();

                if (input.ProductCode != "")
                {
                    AllProduct = context.tbl_Product.Where(pd=>pd.productCode == input.ProductCode).ToList();
                }

                var VoucherType = Convert.ToDecimal(input.TransactionType);

                var StockPostings = context.tbl_StockPosting.Where(sp => sp.date >= input.FromDate && sp.date <= input.ToDate).ToList();

                if (input.StoreId != 0 && input.BatchNo != "All" && input.CategoryId != 0 && input.ProjectId != 0 && VoucherType != 0 && input.ProductId != 0)
                {
                    StockPostings = context.tbl_StockPosting.Where(sp => sp.godownId == input.StoreId && sp.batchId == context.tbl_Batch.Where(bh => bh.batchId == sp.batchId && bh.batchNo == input.BatchNo).FirstOrDefault().batchId && sp.CategoryId == input.CategoryId && sp.ProjectId == input.ProjectId && sp.voucherTypeId == VoucherType && sp.productId == input.ProductId && (sp.date >= input.FromDate && sp.date <= input.ToDate)).ToList();

                }
                else if (input.StoreId == 0 && input.BatchNo == "All" && input.CategoryId != 0 && input.ProjectId == 0 && VoucherType != 0 && input.ProductId != 0)
                {
                    StockPostings = context.tbl_StockPosting.Where(sp => sp.godownId == input.StoreId || sp.batchId == context.tbl_Batch.Where(bh => bh.batchId == sp.batchId || bh.batchNo == input.BatchNo).FirstOrDefault().batchId || sp.CategoryId == input.CategoryId || sp.ProjectId == input.ProjectId || sp.voucherTypeId == VoucherType || sp.productId == input.ProductId && (sp.date >= input.FromDate && sp.date <= input.ToDate)).ToList();

                }
                else
                {



                }

                #region
                var VoucherTypes = context.tbl_VoucherType.Where(v => v.voucherTypeId == VoucherType).FirstOrDefault();

                if (VoucherTypes != null)
                {
                    SearchTypeVoucher = VoucherTypes.voucherTypeName;
                }

                var ProductType = context.tbl_Product.Where(v => v.productId == input.ProjectId).FirstOrDefault();

                if (ProductType != null)
                {
                    SearchTypeProduct = ProductType.productName;
                }

                var StoreType = context.tbl_Godown.Where(v => v.godownId == input.StoreId).FirstOrDefault();

                if (StoreType != null)
                {
                    SearchTypeStore = StoreType.godownName;
                }

                var ProjectType = context.tbl_Project.Where(v => v.ProjectId == input.ProjectId).FirstOrDefault();

                if (ProjectType != null)
                {
                    SearchTypeProject = ProjectType.ProjectName;
                }

                var CategoryType = context.tbl_Category.Where(v => v.CategoryId == input.CategoryId).FirstOrDefault();

                if (CategoryType != null)
                {
                    SearchTypeCategory = CategoryType.CategoryName;
                }

                if (input.BatchNo != "All")
                {
                    SearchTypeBatch = input.BatchNo;
                }
                #endregion

                decimal InWard, OutWard, Rate, QtyRemain, StockValue, AvaCost;

                foreach (var pro in AllProduct)
                {
                    foreach(var store in AllStore)
                    {
                        InWard = 0; OutWard = 0; Rate = 0; QtyRemain = 0; StockValue = 0; AvaCost = 0;

                         var StockPostingsLooping = StockPostings.Where(sp => sp.productId.Value == input.ProductId && sp.godownId.Value == store.godownId).ToList();

                        var newOpenStockBala = new OpenningBalanceStockReadModel()
                        {
                            Store = store.godownName,
                            Category = SearchTypeCategory,
                            VoucherType = SearchTypeVoucher,
                            Project = SearchTypeProject,
                            ProductName = pro.productName,
                            Title = "Openning Balance",
                            Batch = SearchTypeBatch,
                            DateStart = input.FromDate.ToShortDateString(),
                            DateEnd = input.ToDate.ToShortDateString(),
                        };

                        foreach (var sp in StockPostingsLooping)
                        {
                            InWard += sp.inwardQty.Value;
                            OutWard += sp.outwardQty.Value;
                            Rate += sp.rate.Value;
                            QtyRemain += sp.inwardQty.Value - sp.outwardQty.Value;
                            StockValue += (sp.inwardQty.Value - sp.outwardQty.Value) * sp.rate.Value;
                            AvaCost = StockValue / QtyRemain;
                        }

                        newOpenStockBala.StockValue = StockValue;
                        newOpenStockBala.Rate = Rate;
                        newOpenStockBala.QtyRemain = QtyRemain;
                        newOpenStockBala.DateStart = input.FromDate.ToShortDateString();
                        newOpenStockBala.DateEnd = input.ToDate.ToShortDateString();

                        SumOpenningBalance.Add(newOpenStockBala);
                    }
                }

                return SumOpenningBalance;
            }
            catch (Exception ex)
            {
                
            }

            return SumOpenningBalance;
        }

        [HttpPost]
        public HttpResponseMessage StockSummary(StockSummarySearch input)
        {
            List<StockSummary> stockSummary = new List<StockSummary>();
            dynamic response = new ExpandoObject();

            decimal returnValue = 0;
            try
            {
                DBMatConnection conn = new DBMatConnection();
                var spstock = new MATFinancials.StockPostingSP();
                DataTable dtbl = new DataTable();
                decimal productId = input.ProductId;
                string pCode = input.ProductCode.ToString();
                decimal storeId = input.StoreId;
                string batch = input.BatchNo;

                string queryStr = "select top 1 f.fromDate from tbl_FinancialYear f";
                DateTime lastFinYearStart = Convert.ToDateTime(conn.getSingleValue(queryStr));

                //dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime("04/30/2017") /*PublicVariables._dtToDate*/, pCode, "");

                //dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, PublicVariables._dtFromDate, PublicVariables._dtToDate, pCode, "", 0, 0, decimal.Parse(input.TransactionType));

                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, PublicVariables._dtToDate, pCode, "", 0, 0, decimal.Parse(input.TransactionType));

                decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                decimal prevProductId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                string productName = "", storeName = "", productCode = "";
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal totalAssetValue = 0, qtyBalance = 0;
                decimal value1 = 0;
                int i = 0;
                bool isAgainstVoucher = false;

                foreach (DataRow dr in dtbl.Rows)
                {
                    currentProductId = Convert.ToDecimal(dr["productId"]);
                    currentGodownID = Convert.ToDecimal(dr["godownId"]);
                    productCode = dr["productCode"].ToString();
                    string voucherType = "", refNo = "";

                    decimal inwardQty = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == prevProductId
                                         select p.Field<decimal>("inwardQty")).Sum();
                    decimal outwardQty = (from p in dtbl.AsEnumerable()
                                          where p.Field<decimal>("productId") == prevProductId
                                          select p.Field<decimal>("outwardQty")).Sum();

                    inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                    outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                    decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                    string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                    rate2 = Convert.ToDecimal(dr["rate"]);
                    if (previousRunningAverage != 0 && currentProductId == prevProductId)
                    {
                        purchaseRate = previousRunningAverage;
                    }
                    if (currentProductId == prevProductId)
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        prevProductId = Convert.ToDecimal(dr["productId"]);
                        productCode = dr["productCode"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    else
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    //======================== 2017-02-27 =============================== //
                    //if (againstVoucherType != "NA")
                    //{
                    //    voucherType = againstVoucherType;
                    //}
                    //else
                    //{
                    //    voucherType = dr["typeOfVoucher"].ToString();
                    //}
                    refNo = dr["refNo"].ToString();
                    if (againstVoucherType != "NA")
                    {
                        refNo = dr["againstVoucherNo"].ToString();
                        isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                        againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                        //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                        string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                        string typeOfVoucher = conn.getSingleValue(queryString);
                        if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                        if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                    }
                    voucherType = dr["typeOfVoucher"].ToString();
                    //---------  COMMENT END ----------- //
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if (outwardQty2 > 0)
                    {
                        if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = purchaseRate;
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                        {
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue -= outwardQty2 * rate2;
                        }

                        else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Sales Order")
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                    }
                    else if (voucherType == "Sales Return" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Rejection In" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;

                        }
                        else
                        {
                            computedAverageRate = previousRunningAverage; // (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                        // voucherType = "Sales Return";
                    }
                    else if (voucherType == "Delivery Note")
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                    }
                    else
                    {
                        // hndles other transactions such as purchase, opening balance, etc
                        qtyBal += inwardQty2 - outwardQty2;
                        stockValue += (inwardQty2 * rate2);
                    }
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        //qtyBal = Convert.ToDecimal(dr["inwardQty"]);
                        //stockValue = inwardQty2 * rate2;
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        if (voucherType == "Sales Invoice")
                        {
                            stockValue = qtyBal * purchaseRate;
                        }
                        else
                        {
                            stockValue = qtyBal * rate2;
                        }
                        //totalAssetValue += Math.Round(value1, 2);
                        totalAssetValue += value1;
                        //i++;
                        returnValue = totalAssetValue;
                    }
                    // -----------------added 2017-02-21 -------------------- //
                    if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        if (voucherType == "Sales Invoice")
                        {
                            computedAverageRate = purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = rate2;
                        }
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        stockValue = qtyBal * computedAverageRate;
                    }
                    // ------------------------------------------------------ //

                    previousRunningAverage = computedAverageRate;

                    value1 = stockValue;
                    prevProductId = currentProductId;
                    prevGodownID = currentGodownID;

                    if (i == dtbl.Rows.Count - 1)
                    {
                        var avgCost = "";
                        if (value1 != 0 && (inwardqt - outwardqt) != 0)
                        {
                            avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                        }
                        else
                        {
                            avgCost = "0.0";
                        }

                        stockSummary.Add(new StockSummary
                        {
                            ProductId = dr["productId"].ToString(),
                            StoreId = dr["godownId"].ToString(),
                            ProductName = dr["productName"].ToString(),
                            StoreName = dr["godownName"].ToString(),
                            PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                            SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                            QtyBalance = (inwardqt - outwardqt).ToString(),
                            AvgCostValue = avgCost,
                            StockValue = value1.ToString("N2"),
                            ProductCode = dr["productCode"].ToString(),
                            UnitName = string.IsNullOrEmpty(dr["unitName"].ToString()) ? "" : dr["unitName"].ToString()
                        });
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                    {
                        var avgCost = "";
                        if (value1 != 0 && (inwardqt - outwardqt) != 0)
                        {
                            avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                        }
                        else
                        {
                            avgCost = "0.0";
                        }

                        stockSummary.Add(new StockSummary
                        {
                            ProductId = dr["productId"].ToString(),
                            StoreId = dr["godownId"].ToString(),
                            ProductName = dr["productName"].ToString(),
                            StoreName = dr["godownName"].ToString(),
                            PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                            SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                            QtyBalance = (inwardqt - outwardqt).ToString(),
                            AvgCostValue = avgCost,
                            StockValue = value1.ToString("N2"),
                            ProductCode = dr["productCode"].ToString(),
                            UnitName = string.IsNullOrEmpty(dr["unitName"].ToString()) ? "" : dr["unitName"].ToString()
                        });
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    i++;
                }
                decimal totalStockValue = 0;
                foreach (var row in stockSummary)
                {
                    totalStockValue += Convert.ToDecimal(row.StockValue);
                }

                stockSummary.Add(new StockSummary
                {
                    ProductName = "",
                    StoreName = "Total Stock Value:",
                    PurchaseRate = "",
                    SalesRate = "",
                    QtyBalance = qtyBalance.ToString("N2"),
                    AvgCostValue = (totalStockValue / qtyBalance).ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    ProductCode = ""
                });
                stockSummary.Add(new StockSummary
                {
                    ProductName = "",
                    StoreName = "",
                    PurchaseRate = "",
                    SalesRate = "",
                    QtyBalance = "",
                    AvgCostValue = "",
                    StockValue = "=============",
                    ProductCode = ""
                });

                response.TotalStockValue = totalStockValue.ToString("N2");
                response.StockSummary = stockSummary;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKSR1:" + ex.Message;
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpPost]
        public StockReportRow StockReportDetails12(StockReportSearch input)
        {
            StockReportRow master = new StockReportRow();
            List<StockReportRowItems> details = new List<StockReportRowItems>();

            try
            {
                DataTable prodDtbl = new DataTable();
                prodDtbl = new ProductSP().ProductViewAll();
                for (int k = 0; k < prodDtbl.Rows.Count; k++)
                {
                    AllItems.Add(prodDtbl.Rows[k]["productCode"].ToString());
                }

                GodownSP spGodown = new GodownSP();
                DataTable storeDtbl = new DataTable();
                storeDtbl = spGodown.GodownViewAll();
                for (int k = 0; k < storeDtbl.Rows.Count; k++)
                {
                    AllStores.Add(Convert.ToDecimal(storeDtbl.Rows[k]["godownId"]));
                }

                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                string calculationMethod = string.Empty;
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = DateTime.UtcNow;
                try
                {
                     lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                }
                catch(Exception ex)
                {

                }

                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    //lastFinYearEnd = input.FromDate.AddDays(-1);
                    lastFinYearEnd = input.ToDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                }
                decimal currentProductId = 0, prevProductId = 0, prevStoreId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0;
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal stockValuePerItemPerStore = 0, totalStockValue = 0;
                decimal inwardQtyPerStore = 0, outwardQtyPerStore = 0, totalQtyPerStore = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "", productName = "", storeName = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };
                DataTable dtbl = new DataTable();
                DataTable dtblItem = new DataTable();
                DataTable dtblStore = new DataTable();
                DataTable dtblOpeningStocks = new DataTable();
                DataTable dtblOpeningStockPerItemPerStore = new DataTable();
                var spstock = new MATFinancials.StockPostingSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                decimal productId = input.ProductId, storeId = input.StoreId;
                string produceCode = input.ProductCode, batch = input.BatchNo, referenceNo = input.RefNo;
                input.ToDate = input.ToDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                //dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text), "", "");
                //dtblOpeningStocks = spstock.StockReportDetailsGridFill(0, 0, "All", PublicVariables._dtFromDate, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), "", "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);
                if(batch != "" || input.ProjectId != 0 || input.CategoryId != 0 || productId != 0 || storeId != 0 || produceCode != "")
                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);
                else
                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId);


                if (AllItems.Any())
                {
                    foreach (var item in AllItems)
                    {
                        // ======================================== for each item begins here ====================================== //
                        dtblItem = new DataTable();
                        if (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item).Any())
                        {
                            dtblItem = (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item)).CopyToDataTable();
                            productName = (dtbl.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(m => m.Field<string>("productName")).FirstOrDefault().ToString());
                        }
                        if (dtblItem != null && dtblItem.Rows.Count > 0)
                        {
                            if (AllStores.Any())
                            {
                                foreach (var store in AllStores)
                                {
                                    // =================================== for that item, for each store begins here ============================== //
                                    qtyBal = 0; stockValue = 0;
                                    stockValuePerItemPerStore = 0; //dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    dtblStore = new DataTable();

                                    // ************************ insert opening stock per item per store before other transactions ******************** //
                                    dtblOpeningStockPerItemPerStore = new DataTable();
                                    if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                    {
                                        dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store)).CopyToDataTable();
                                        if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                        {
                                            computedAverageRate = rate2;
                                            var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, item, store);
                                            dcOpeningStockForRollOver = itemParams.Item1;
                                            openingQuantity = itemParams.Item2;
                                            openingQuantityIn = openingQuantity; // itemParams.Item3;
                                            openingQuantityOut = 0; // itemParams.Item4;
                                            //qtyBal += openingQuantity;
                                            qtyBal = openingQuantity;
                                            if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                            {
                                                computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                                stockValue = qtyBal * computedAverageRate;
                                            }

                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = item,
                                                ProductCode = item,
                                                ProductName = productName,
                                                StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                                Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                                InwardQty = openingQuantityIn.ToString("N2"),
                                                OutwardQty = openingQuantityOut.ToString("N2"),
                                                Rate = "",
                                                AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                                QtyBalance = openingQuantity.ToString("N2"),
                                                StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                                VoucherTypeName = "Opening Stock "
                                            });

                                            //dgvStockDetailsReport.Rows[i].Cells["productId"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productName"].Value = productName;
                                            //dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            //Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(); ;
                                            //dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                            //dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                            //dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";
                                            totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                            totalQtyPerStore += openingQuantity;
                                            i++;
                                        }
                                    }
                                    // ************************ insert opening stock per item per store before other transactions end ******************** //

                                    if (dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).Any())
                                    {
                                        dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).CopyToDataTable();
                                    }
                                    if (dtblStore != null && dtblStore.Rows.Count > 0)
                                    {
                                        storeName = (dtblStore.AsEnumerable().Where(m => m.Field<decimal>("godownId") == store).Select(m => m.Field<string>("godownName")).FirstOrDefault().ToString());
                                        foreach (DataRow dr in dtblStore.Rows)
                                        {
                                            // opening stock has been factored in thinking there was no other transaction, now other transactions factored it in again, so remove it
                                            totalStockValue -= Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            //dgvStockDetailsReport.Rows[i-1].DefaultCellStyle.BackColor = Color.White;
                                            //dgvStockDetailsReport.Rows[i-1].Cells["rate"].Value = "";
                                            dcOpeningStockForRollOver = 0;  // reset the opening stock because of the loop

                                            currentProductId = Convert.ToDecimal(dr["productId"]);
                                            productCode = dr["productCode"].ToString();
                                            string voucherType = "", refNo = "";
                                            inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                                            decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                                            string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                                            var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;
                                            if (!isOpeningRate)
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }
                                            //if (purchaseRate == 0)
                                            //{
                                            //    purchaseRate = rate2;
                                            //}
                                            if (prevStoreId != store)
                                            {
                                                previousRunningAverage = purchaseRate;
                                            }
                                            if (previousRunningAverage != 0 && currentProductId == prevProductId)
                                            {
                                                purchaseRate = previousRunningAverage;
                                            }
                                            refNo = dr["refNo"].ToString();
                                            if (againstVoucherType != "NA")
                                            {
                                                refNo = dr["againstVoucherNo"].ToString();
                                                isAgainstVoucher = true;
                                                againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                                                string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                                                string typeOfVoucher = conn.getSingleValue(queryString);
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                            }
                                            voucherType = dr["typeOfVoucher"].ToString();
                                            if (voucherType == "Stock Journal")
                                            {
                                                //voucherType = "Stock Transfer";
                                                string queryString = string.Format(" SELECT extra1 FROM tbl_StockJournalMaster WHERE voucherNo = '{0}' ", dr["refNo"]);
                                                voucherType = conn.getSingleValue(queryString);
                                            }
                                            if (outwardQty2 > 0)
                                            {
                                                if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate;
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                                                {
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue -= outwardQty2 * rate2;
                                                }
                                                else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return")
                                                    || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;// 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }

                                                else if (outwardQty2 > 0 && voucherType == "Sales Order")
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else
                                                {
                                                    if (qtyBal != 0)
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                            }
                                            else if (voucherType == "Sales Return" && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType == "Rejection In" && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;

                                                }
                                                else
                                                {
                                                    computedAverageRate = previousRunningAverage;// (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType == "Delivery Note")
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                            }
                                            else if (voucherType == "Opening Stock")
                                            {
                                                openingQuantityIn = 0;
                                                if (qtyBal != 0)
                                                {
                                                    //computedAverageRate = (stockValue / qtyBal);
                                                    
                                                    qtyBal += inwardQty2  - (inwardQty2 + outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }
                                                else
                                                {
                                                    //computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - (inwardQty2 + outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }
                                            }
                                            else
                                            {
                                                // hndles other transactions such as purchase, opening balance, etc
                                                qtyBal += inwardQty2 - outwardQty2;
                                                stockValue += (inwardQty2 * rate2);
                                            }

                                            // ------------------------------------------------------ //
                                            //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            string avgCost = "";
                                            if (stockValue != 0 && qtyBal != 0)
                                            {
                                                avgCost = (stockValue / qtyBal).ToString("N2");
                                                //avgCost = (stockValue / qtyBal * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }
                                            else
                                            {
                                                avgCost = computedAverageRate.ToString("N2");
                                                //avgCost = (computedAverageRate/ SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }
                                            details.Add(new StockReportRowItems
                                            {
                                                ProductId = dr["productId"].ToString(),
                                                ProductCode = dr["productCode"].ToString(),
                                                StoreName = dr["godownName"].ToString(),
                                                InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                                                OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                                                Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                                AverageCost = avgCost,
                                                QtyBalance = qtyBal.ToString("N2"),
                                                StockValue = stockValue.ToString("N2"),
                                                VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType,
                                                RefNo = refNo,
                                                Batch = string.IsNullOrEmpty(dr["Batch"].ToString()) ? "" : dr["Batch"].ToString(),
                                                Date = Convert.ToDateTime(dr["date"]).ToShortDateString(),
                                                ProductName = dr["productName"].ToString(),
                                                ProjectId = Convert.ToDecimal(dr["projectId"]),
                                                CategoryId = Convert.ToDecimal(dr["categoryId"]),
                                                UnitName = dr["unitName"].ToString()
                                            });
                                            isAgainstVoucher = false;

                                            stockValuePerItemPerStore = Convert.ToDecimal(stockValue);
                                            prevProductId = currentProductId;
                                            previousRunningAverage = Convert.ToDecimal(avgCost);
                                            inwardQtyPerStore += Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQtyPerStore += Convert.ToDecimal(dr["outwardQty"]);
                                            i++;
                                        }
                                        // ===================== for every store for that item, put the summary =================== //
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = "",
                                            ProductCode = item,
                                            StoreName = storeName,
                                            InwardQty = (inwardQtyPerStore + openingQuantityIn).ToString("N2"),
                                            OutwardQty = ("-") + (outwardQtyPerStore + openingQuantityOut).ToString("N2"),
                                            Rate = "",
                                            AverageCost = "",
                                            QtyBalance = qtyBal.ToString("N2"),
                                            StockValue = stockValuePerItemPerStore.ToString("N2"),
                                            VoucherTypeName = "Sub total: ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = ""
                                        });
                                        totalStockValue += Convert.ToDecimal(stockValuePerItemPerStore.ToString("N2"));
                                        totalQtyPerStore += (inwardQtyPerStore - outwardQtyPerStore) /*+ openingQuantity*/;
                                        i++;
                                        openingQuantity = 0;
                                        inwardQtyPerStore = 0;
                                        outwardQtyPerStore = 0;
                                        qtyBal = 0;
                                        stockValue = 0;
                                        stockValuePerItemPerStore = 0;
                                        //dgvStockDetailsReport.Rows.Add();
                                        i++;
                                    }
                                    dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    prevStoreId = store;
                                }
                            }
                        }
                        // *** if no transaction for that item for all stores for that period add the ockock for all stores per store *** //
                        else
                        {
                            foreach (var store in AllStores)
                            {
                                stockValuePerItemPerStore = 0;
                                dtblStore = new DataTable();

                                // ************************ insert opening stock per item per store ******************** //
                                dtblOpeningStockPerItemPerStore = new DataTable();
                                if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                {
                                    dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item)).CopyToDataTable();
                                    if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                    {
                                        computedAverageRate = rate2;
                                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, item, store);
                                        dcOpeningStockForRollOver = itemParams.Item1;
                                        openingQuantity = itemParams.Item2;
                                        openingQuantityIn = openingQuantity; // itemParams.Item3;
                                        openingQuantityOut = 0; // itemParams.Item4;
                                        qtyBal += openingQuantity;
                                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                        {
                                            //computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                            //stockValue = openingQuantity * computedAverageRate;
                                        }
                                        var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;
                                        details.Add(new StockReportRowItems
                                        {
                                            ProductId = item,
                                            ProductCode = item,
                                            StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                        Where(l => l.Field<string>("productCode") == item).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                            InwardQty = openingQuantityIn.ToString("N2"),
                                            OutwardQty = openingQuantityOut.ToString("N2"),
                                            Rate = "",
                                            AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                            //AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2") : "",
                                            QtyBalance = openingQuantity.ToString("N2"),
                                            StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                            VoucherTypeName = "Opening Stock ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = ""
                                        });

                                        totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                        dcOpeningStockForRollOver = 0;
                                        totalQtyPerStore += openingQuantity;
                                        openingQuantityIn = 0;
                                        openingQuantityOut = 0;
                                        openingQuantity = 0;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }

                // ************************ Calculate Total **************************** //
                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var avgCost2 = "";
                if (totalQtyPerStore != 0)
                {
                    avgCost2 = (totalStockValue / totalQtyPerStore).ToString("N2");
                }
                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = avgCost2,
                    QtyBalance = totalQtyPerStore.ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    VoucherTypeName = "Total Stock Value:",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                details.Add(new StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });
            }

            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }
            master.TotalAverageCost = 0;
            master.TotalQtyBalance = 0;
            master.TotalStockValue = 0;
            master.StockReportRowItem = details;
            return master;
        }

        [HttpGet]
        public HttpResponseMessage GetStockStatusValue()
        {
            List<StockSummary> stockSummary = new List<StockSummary>();
            dynamic response = new ExpandoObject();

            decimal returnValue = 0;
            try
            {
                DBMatConnection conn = new DBMatConnection();
                var spstock = new MATFinancials.StockPostingSP();
                DataTable dtbl = new DataTable();
                decimal productId = 0;
                string pCode = "";
                decimal storeId = 0;
                string batch = "All";


                string queryStr = "select top 1 f.fromDate from tbl_FinancialYear f";
                DateTime lastFinYearStart = Convert.ToDateTime(conn.getSingleValue(queryStr));

                //dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime("04/30/2017") /*PublicVariables._dtToDate*/, pCode, "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, PublicVariables._dtToDate, pCode, "", 0, 0);

                decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                decimal prevProductId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                string productName = "", storeName = "", productCode = "";
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal totalAssetValue = 0, qtyBalance = 0;
                decimal value1 = 0;
                int i = 0;
                bool isAgainstVoucher = false;

                foreach (DataRow dr in dtbl.Rows)
                {
                    currentProductId = Convert.ToDecimal(dr["productId"]);
                    currentGodownID = Convert.ToDecimal(dr["godownId"]);
                    productCode = dr["productCode"].ToString();
                    string voucherType = "", refNo = "";

                    decimal inwardQty = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == prevProductId
                                         select p.Field<decimal>("inwardQty")).Sum();
                    decimal outwardQty = (from p in dtbl.AsEnumerable()
                                          where p.Field<decimal>("productId") == prevProductId
                                          select p.Field<decimal>("outwardQty")).Sum();

                    inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                    outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                    decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                    string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                    rate2 = Convert.ToDecimal(dr["rate"]);
                    if (previousRunningAverage != 0 && currentProductId == prevProductId)
                    {
                        purchaseRate = previousRunningAverage;
                    }
                    if (currentProductId == prevProductId)
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        prevProductId = Convert.ToDecimal(dr["productId"]);
                        productCode = dr["productCode"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    else
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    //======================== 2017-02-27 =============================== //
                    //if (againstVoucherType != "NA")
                    //{
                    //    voucherType = againstVoucherType;
                    //}
                    //else
                    //{
                    //    voucherType = dr["typeOfVoucher"].ToString();
                    //}
                    refNo = dr["refNo"].ToString();
                    if (againstVoucherType != "NA")
                    {
                        refNo = dr["againstVoucherNo"].ToString();
                        isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                        againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                        //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                        string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                        string typeOfVoucher = conn.getSingleValue(queryString);
                        if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                        if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                    }
                    voucherType = dr["typeOfVoucher"].ToString();
                    //---------  COMMENT END ----------- //
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if (outwardQty2 > 0)
                    {
                        if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = purchaseRate;
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                        {
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue -= outwardQty2 * rate2;
                        }

                        else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Sales Order")
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                    }
                    else if (voucherType == "Sales Return" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Rejection In" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;

                        }
                        else
                        {
                            computedAverageRate = previousRunningAverage; // (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                        // voucherType = "Sales Return";
                    }
                    else if (voucherType == "Delivery Note")
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                    }
                    else
                    {
                        // hndles other transactions such as purchase, opening balance, etc
                        qtyBal += inwardQty2 - outwardQty2;
                        stockValue += (inwardQty2 * rate2);
                    }
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        //qtyBal = Convert.ToDecimal(dr["inwardQty"]);
                        //stockValue = inwardQty2 * rate2;
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        if (voucherType == "Sales Invoice")
                        {
                            stockValue = qtyBal * purchaseRate;
                        }
                        else
                        {
                            stockValue = qtyBal * rate2;
                        }
                        //totalAssetValue += Math.Round(value1, 2);
                        totalAssetValue += value1;
                        //i++;
                        returnValue = totalAssetValue;
                    }
                    // -----------------added 2017-02-21 -------------------- //
                    if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        if (voucherType == "Sales Invoice")
                        {
                            computedAverageRate = purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = rate2;
                        }
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        stockValue = qtyBal * computedAverageRate;
                    }
                    // ------------------------------------------------------ //

                    previousRunningAverage = computedAverageRate;

                    value1 = stockValue;
                    prevProductId = currentProductId;
                    prevGodownID = currentGodownID;

                    if (i == dtbl.Rows.Count - 1)
                    {
                        var avgCost = "";
                        if (value1 != 0 && (inwardqt - outwardqt) != 0)
                        {
                            avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                        }
                        else
                        {
                            avgCost = "0.0";
                        }

                        stockSummary.Add(new StockSummary
                        {
                            ProductId = dr["productId"].ToString(),
                            StoreId = dr["godownId"].ToString(),
                            ProductName = dr["productName"].ToString(),
                            StoreName = dr["godownName"].ToString(),
                            PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                            SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                            QtyBalance = (inwardqt - outwardqt).ToString(),
                            AvgCostValue = avgCost,
                            StockValue = value1.ToString("N2"),
                            ProductCode = dr["productCode"].ToString(),
                            UnitName = string.IsNullOrEmpty(dr["unitName"].ToString()) ? "" : dr["unitName"].ToString()
                        });
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                    {
                        var avgCost = "";
                        if (value1 != 0 && (inwardqt - outwardqt) != 0)
                        {
                            avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                        }
                        else
                        {
                            avgCost = "0.0";
                        }

                        stockSummary.Add(new StockSummary
                        {
                            ProductId = dr["productId"].ToString(),
                            StoreId = dr["godownId"].ToString(),
                            ProductName = dr["productName"].ToString(),
                            StoreName = dr["godownName"].ToString(),
                            PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                            SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                            QtyBalance = (inwardqt - outwardqt).ToString(),
                            AvgCostValue = avgCost,
                            StockValue = value1.ToString("N2"),
                            ProductCode = dr["productCode"].ToString(),
                            UnitName = string.IsNullOrEmpty(dr["unitName"].ToString()) ? "" : dr["unitName"].ToString()
                        });
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    i++;
                }
                decimal totalStockValue = 0;
                foreach (var row in stockSummary)
                {
                    totalStockValue += Convert.ToDecimal(row.StockValue);
                }

                stockSummary.Add(new StockSummary
                {
                    ProductName = "",
                    StoreName = "Total Stock Value:",
                    PurchaseRate = "",
                    SalesRate = "",
                    QtyBalance = qtyBalance.ToString("N2"),
                    AvgCostValue = (totalStockValue / qtyBalance).ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    ProductCode = ""
                });

                response.TotalStockValue = totalStockValue.ToString("N2");
                response.StockSummary = stockSummary;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKSR1:" + ex.Message;
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        private decimal getOpeningStock(string date, decimal productId, decimal storeId)
        {
            decimal output = 0.0m;
            string query = string.Format("select ISNULL(sum(inwardQty)-sum(outwardQty),0) from tbl_StockPosting where productId={0} and godownId={1} and date<='{2}'", productId, storeId, date);
            output = Convert.ToDecimal(new DBMatConnection().getSingleValue(query));
            return output;
        }
        [HttpGet]
        public List<StockPostingInfo> getOpeningStockForProduct(decimal productId)
        {
            int openingStockVoucherTypeId = 2;
            List<StockPostingInfo> stockPostingInfoList = new List<StockPostingInfo>();
            var stockPostingSp = new MATFinancials.StockPostingSP();

            try
            {
                var openingStockDtbl = stockPostingSp.getOpeningStockForProduct(productId, openingStockVoucherTypeId);
                foreach (DataRow rows in openingStockDtbl.Rows)
                {
                    stockPostingInfoList.Add(new StockPostingInfo
                    {
                        
                        ProductName = rows["productName"].ToString(),
                        Warehouse = rows["warehouse"].ToString(),
                        Date = Convert.ToDateTime(rows["date"]),
                        StockPostingId = Convert.ToDecimal(rows["stockPostingId"]),
                        InwardQty = Convert.ToInt32(rows["inwardQty"]),
                        Rate = Convert.ToDecimal(rows["rate"]),
                        BatchNo = rows["batchNo"].ToString(),
                        UnitName = rows["unitName"].ToString()

                    });
                }
                return stockPostingInfoList;
            }
            catch (Exception ex)
            {

                throw;
            }
            
        }

        [HttpPost]
        public List<StoreLine> CustomStockSummaryReport(CustomSummaryParam input)
        {
            var items = new ProductSP().ProductViewAll();
            var stores = new GodownSP().GodownViewAll();
            var sizesDt = new SizeSP().SizeViewAll();
            var productGroupsDt = new ProductGroupSP().ProductGroupViewAll();

            List<SizeInfo> sizes = new List<SizeInfo>();
            for(int i=0;i<sizesDt.Rows.Count;i++)
            {
                sizes.Add(new SizeInfo {
                    SizeId= Convert.ToDecimal(sizesDt.Rows[i].ItemArray[0]),
                    Size= sizesDt.Rows[i].ItemArray[1].ToString()
                });
            }

            List<ProductGroupInfo> productGroups = new List<ProductGroupInfo>();
            for (int i = 0; i < productGroupsDt.Rows.Count; i++)
            {
                productGroups.Add(new ProductGroupInfo {
                    GroupId= Convert.ToDecimal(productGroupsDt.Rows[i].ItemArray[1]),
                    GroupName= productGroupsDt.Rows[i].ItemArray[2].ToString(),
                    GroupUnder= Convert.ToDecimal(productGroupsDt.Rows[i].ItemArray[3]),
                    Extra1= productGroupsDt.Rows[i].ItemArray[4].ToString()
                });
            }

            List<StoreLine> storeInfo = new List<StoreLine>();

            foreach (DataRow store in stores.Rows)
            {
                List<ItemLine> itemInfo = new List<ItemLine>();
                foreach (DataRow item in items.Rows)
                {
                    decimal division = 0;
                    decimal group = 0;
                    decimal subGroup = 0;
                    decimal category = 0;
                    decimal subCategory = 0;
                    ProductGroupInfo prodGrp = productGroups.FirstOrDefault(p=>p.GroupId==Convert.ToDecimal(item[3].ToString()));

                    ProductGroupInfo div = null;
                    ProductGroupInfo grp = null;
                    ProductGroupInfo subGrp = null;
                    ProductGroupInfo cat = null;
                    ProductGroupInfo subCat = null;

                    if(prodGrp.Extra1=="Sub Category")
                    {
                        subCat = prodGrp;
                        cat = productGroups.FirstOrDefault(p=>p.GroupId==subCat.GroupUnder);
                        subGrp = productGroups.FirstOrDefault(p => p.GroupId == cat.GroupUnder);
                        grp= productGroups.FirstOrDefault(p => p.GroupId == subGrp.GroupUnder);
                        div= productGroups.FirstOrDefault(p => p.GroupId == grp.GroupUnder);
                    }
                    //if (prodGrp.Extra1 == "Category")
                    //{
                    //    subCat = null;
                    //    cat = prodGrp;
                    //    subGrp = productGroups.FirstOrDefault(p => p.GroupUnder == cat.GroupUnder);
                    //    grp = productGroups.FirstOrDefault(p => p.GroupUnder == subGrp.GroupUnder);
                    //    div = productGroups.FirstOrDefault(p => p.GroupUnder == grp.GroupUnder);
                    //}
                    //if (prodGrp.Extra1 == "Sub Group")
                    //{
                    //    subCat = null;
                    //    cat = null;
                    //    subGrp = prodGrp;
                    //    grp = productGroups.FirstOrDefault(p => p.GroupUnder == subGrp.GroupUnder);
                    //    div = productGroups.FirstOrDefault(p => p.GroupUnder == grp.GroupUnder);
                    //}
                    //if (prodGrp.Extra1 == "Group")
                    //{
                    //    subCat = null;
                    //    cat = null;
                    //    subGrp = null;
                    //    grp = prodGrp;
                    //    div = productGroups.FirstOrDefault(p => p.GroupUnder == grp.GroupUnder);
                    //}
                    //if (prodGrp.Extra1 == "Division")
                    //{
                    //    subCat = null;
                    //    cat = null;
                    //    subGrp = null;
                    //    grp = null;
                    //    div = prodGrp;
                    //}

                    itemInfo.Add(new ItemLine
                    {
                        Description = item[1].ToString(),
                        ItemId = Convert.ToDecimal(item[0].ToString()),
                        ItemName = item[2].ToString(),
                        ProductGroupId = item[3].ToString(),
                        DivisionId=(div==null) ? 0 : div.GroupId,
                        GroupId=(grp==null) ? 0 : grp.GroupId,
                        SubGroupId=(subGrp==null)?0:subGrp.GroupId,
                        CategoryId=(cat==null)?0:cat.GroupId,
                        SubCategoryId=(subCat==null)?0:subCat.GroupId,
                        PartNo = item[28].ToString(),
                        Size = sizes.FirstOrDefault(p=>p.SizeId== Convert.ToDecimal(item[6].ToString())).Size,
                        TotalOpeningStock = getOpeningStock(input.FromDate.ToString(), Convert.ToDecimal(item[0].ToString()), Convert.ToDecimal(store[0].ToString())),
                        TotalDeliveryNote = getTotalDeliveryNote(input.FromDate, input.ToDate, Convert.ToDecimal(store[0].ToString()), Convert.ToDecimal(item[0].ToString())),
                        TotalMaterialReceipt = getTotalMaterialReceipt(input.FromDate, input.ToDate, Convert.ToDecimal(store[0].ToString()), Convert.ToDecimal(item[0].ToString())),
                        TotalAdjustment = getTotalStockAdjustment(input.FromDate, input.ToDate, Convert.ToDecimal(store[0].ToString()), Convert.ToDecimal(item[0].ToString())),
                        TotalTransferOut = getTotalTransferOut(input.FromDate, input.ToDate, Convert.ToDecimal(store[0].ToString()), Convert.ToDecimal(item[0].ToString())),
                        TotalTransferIn = getTotalTransferIn(input.FromDate, input.ToDate, Convert.ToDecimal(store[0].ToString()), Convert.ToDecimal(item[0].ToString()))
                    });
                }

                storeInfo.Add(new StoreLine
                {
                    StoreId = Convert.ToDecimal(store[0].ToString()),
                    StoreName = store[1].ToString(),
                    ItemLines = itemInfo
                });

            }

            return storeInfo;
        }

        private decimal getTotalDeliveryNote(string fromDate, string toDate, decimal storeId, decimal productId)
        {
            decimal sum = 0.0m;
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("select d.productId,sum(d.qty) " +
                                         "from tbl_DeliveryNoteDetails as d " +
                                         "inner join tbl_DeliveryNoteMaster as m on m.deliveryNoteMasterId = d.deliveryNoteMasterId " +
                                         "where m.date >= '{0}' and m.date <= '{1}' and d.godownId = {2} and d.productId = {3} " +
                                         "group by d.productId", fromDate, toDate, storeId, productId);
            string val = con.getSingleValue2(query);
            if (val == "" || string.IsNullOrEmpty(val))
            {
                val = "0.0";
            }
            sum = Convert.ToDecimal(val);
            return sum;
        }

        private decimal getTotalMaterialReceipt(string fromDate, string toDate, decimal storeId, decimal productId)
        {
            decimal sum = 0.0m;
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("select d.productId,sum(d.qty) " +
                                         "from tbl_materialreceiptDetails as d " +
                                         "inner join tbl_materialreceiptMaster as m on m.materialreceiptMasterId = d.materialreceiptMasterId " +
                                         "where m.date >= '{0}' and m.date <= '{1}' and d.godownId = {2} and d.productId = {3} " +
                                         "group by d.productId", fromDate, toDate, storeId, productId);
            string val = con.getSingleValue2(query);
            if (val == "" || string.IsNullOrEmpty(val))
            {
                val = "0.0";
            }
            sum = Convert.ToDecimal(val);
            return sum;
        }

        private decimal getTotalStockAdjustment(string fromDate, string toDate, decimal storeId, decimal productId)
        {
            decimal sum = 0.0m;
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("select d.productId,sum(d.qty) " +
                                         "from tbl_StockJournalDetails as d " +
                                         "inner join tbl_StockJournalMaster as m on m.stockJournalMasterId = d.stockJournalMasterId " +
                                         "where m.date >= '{0}' and m.date <= '{1}' and d.godownId = {2} and d.productId = {3} and m.extra1='Stock Out' " +
                                         "group by d.productId", fromDate, toDate, storeId, productId);
            string val = con.getSingleValue2(query);
            if (val == "" || string.IsNullOrEmpty(val))
            {
                val = "0.0";
            }
            sum = Convert.ToDecimal(val);
            return sum;
        }

        private decimal getTotalTransferIn(string fromDate, string toDate, decimal storeId, decimal productId)
        {
            decimal sum = 0.0m;
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("select d.productId,sum(d.qty) " +
                                         "from tbl_StockJournalDetails as d " +
                                         "inner join tbl_StockJournalMaster as m on m.stockJournalMasterId = d.stockJournalMasterId " +
                                         "where m.date >= '{0}' and m.date <= '{1}' and d.godownId = {2} and d.productId = {3} and m.extra1='Production' " +
                                         "group by d.productId", fromDate, toDate, storeId, productId);
            string val = con.getSingleValue2(query);
            if (val == "" || string.IsNullOrEmpty(val))
            {
                val = "0.0";
            }
            sum = Convert.ToDecimal(val);
            return sum;
        }

        private decimal getTotalTransferOut(string fromDate, string toDate, decimal storeId, decimal productId)
        {
            decimal sum = 0.0m;
            DBMatConnection con = new DBMatConnection();
            string query = string.Format("select d.productId,sum(d.qty) " +
                                         "from tbl_StockJournalDetails as d " +
                                         "inner join tbl_StockJournalMaster as m on m.stockJournalMasterId = d.stockJournalMasterId " +
                                         "where m.date >= '{0}' and m.date <= '{1}' and d.godownId = {2} and d.productId = {3} and m.extra1='Consumption' " +
                                         "group by d.productId", fromDate, toDate, storeId, productId);
            string val = con.getSingleValue2(query);
            if (val == "" || string.IsNullOrEmpty(val))
            {
                val = "0.0";
            }
            sum = Convert.ToDecimal(val);
            return sum;
        }

        private void fillAllItems(StockReportSearch input)
        {
            if (input.ProductCode != string.Empty || input.BatchNo != string.Empty)
            {
                try
                {
                    ProductSP spproduct = new ProductSP();
                    AllItems.Clear();
                    DataTable dt = new DataTable();
                    dt = spproduct.ProductViewAll();
                    if (dt.AsEnumerable().Where(k => k.Field<string>("productCode") == input.ProductCode).Any())
                    {
                        // filter by product code only
                        dt = dt.AsEnumerable().Where(k => k.Field<string>("productCode") == input.ProductCode).CopyToDataTable();
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            AllItems.Add(dt.Rows[j]["productCode"].ToString());
                        }
                    }
                    if (dt.AsEnumerable().Where(k => k.Field<string>("batchNo") == input.BatchNo).Any())
                    {
                        // filter by batch only
                        AllItems.Clear();
                        dt = dt.AsEnumerable().Where(k => k.Field<string>("batchNo") == input.BatchNo).CopyToDataTable();
                        for (int j = 0; j < dt.Rows.Count; j++)
                        {
                            AllItems.Add(dt.Rows[j]["productCode"].ToString());
                        }
                    }
                    // add filter by batch and product code combined below here
                }
                catch (Exception ex)
                {
                    //formMDI.infoError.ErrorString = "SDR1:" + ex.Message;
                }
            }

        }
    }

    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class InventoryReportLookupController : ApiController
    {
        [HttpGet]
        public HttpResponseMessage StockJournal()
        {
            dynamic response = new ExpandoObject();

            ProductSP spproduct = new ProductSP();
            DataTable dtbl = spproduct.ProductViewAll();
            DataRow dr = dtbl.NewRow();
            dr["productId"] = 0;
            dr["productName"] = "All";
            dtbl.Rows.InsertAt(dr, 0);

            response.Products = dtbl;
            response.VoucherTypes = new StockJournalMasterSP().VoucherTypeComboFillForStockJournalReport("Stock Journal", true);
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage StockVariance()
        {
            dynamic response = new ExpandoObject();

            ProductSP spproduct = new ProductSP();
            DataTable dtbl = spproduct.ProductViewAll();
            DataTable dtblStores = new GodownSP().GodownViewAll();
            DataRow dr = dtbl.NewRow();
            dr["productId"] = 0;
            dr["productName"] = "All";
            dtbl.Rows.InsertAt(dr, 0);

            DataRow drStores = dtblStores.NewRow();
            drStores["godownId"] = 0;
            drStores["godownName"] = "All";
            dtblStores.Rows.InsertAt(drStores, 0);

            response.Products = dtbl;
            response.Stores = dtblStores;
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage InventoryMovement()
        {
            dynamic response = new ExpandoObject();

            response.ProductGroup = new ProductGroupSP().ProductGroupViewAll();
            response.Batch = new BatchSP().BatchViewAll();
            response.Product = new ProductSP().ProductViewAll();
            response.Store = new GodownSP().GodownViewAll();
            response.VoucherTypes = new VoucherTypeSP().VoucherTypeSelection();

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage PhysicalStock()
        {
            dynamic response = new ExpandoObject();

            response.Product = new ProductSP().ProductViewAll();
            response.Store = new GodownSP().GodownViewAll();
            response.VoucherTypes = new VoucherTypeSP().VoucherTypeSelectionComboFill("Physical Stock");

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage StockDetails()
        {
            dynamic response = new ExpandoObject();

            response.Product = new ProductSP().ProductViewAll();
            response.Store = new GodownSP().GodownViewAll();
            response.Batch = new BatchSP().BatchViewAll();

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage InventoryStatistics()
        {
            dynamic response = new ExpandoObject();

            response.ProductGroup = new ProductGroupSP().ProductGroupViewAll();
            response.ModelNo = new ModelNoSP().ModelNoViewAll();
            response.Brand = new BrandSP().BrandViewAll();
            response.Size = new SizeSP().SizeViewAll();

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
    }

    public class ItemLine
    {
        public decimal ItemId { get; set; }
        public string ItemName { get; set; }
        public string PartNo { get; set; }
        public string Description { get; set; }
        public string ProductGroupId { get; set; }
        public string Size { get; set; }
        public decimal TotalOpeningStock { get; set; }
        public decimal TotalDeliveryNote { get; set; }
        public decimal TotalMaterialReceipt { get; set; }
        public decimal TotalAdjustment { get; set; }
        public decimal TotalTransferIn { get; set; }
        public decimal TotalTransferOut { get; set; }
        public decimal Balance { get; set; }
        public decimal DivisionId { get; set; }
        public decimal GroupId { get; set; }
        public decimal SubGroupId { get; set; }
        public decimal CategoryId { get; set; }
        public decimal SubCategoryId { get; set; }
    }
    public class StoreLine
    {
        public decimal StoreId { get; set; }
        public string StoreName { get; set; }
        public List<ItemLine> ItemLines { get; set; }
    }

    public class CustomSummaryParam
    {
        public string FromDate { get; set; }
        public string ToDate { get; set; }
    }
}