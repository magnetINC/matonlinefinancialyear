﻿using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Dynamic;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Reports.GeneralReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TaxReportController : ApiController
    {
        public TaxReportController()
        {

        }

        [HttpGet]
        public MatResponse GetLookUps()
        {
            var response = new MatResponse();
            dynamic lookUps = new ExpandoObject();
            try
            {
                TaxSP spTax = new TaxSP();
                VoucherTypeSP spVoucherType = new VoucherTypeSP();
                DataTable dtbl = new DataTable();
                DataTable dtbl2 = new DataTable();

                dtbl = spTax.TaxViewAll();
                DataRow dr = dtbl.NewRow();
                dr["taxName"] = "All";
                dr["taxId"] = 0;
                dtbl.Rows.InsertAt(dr, 0);

                dtbl2 = spVoucherType.TypeOfVoucherCombofillForVatAndTaxReport();
                DataRow dr2 = dtbl2.NewRow();
                dr2["typeOfVoucher"] = "All";
                dr2["voucherTypeId"] = 0;
                dtbl2.Rows.InsertAt(dr2, 0);

                lookUps.Taxes = dtbl;
                lookUps.TypeOfVoucher = dtbl2;

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server error";
                response.Response = ex;
            }

            return response;
        }

        [HttpGet]
        public MatResponse GetVoucherTypes(string voucherName)
        {
            var response = new MatResponse();
            try
            {
                DataTable dtbl = new DataTable();
                VoucherTypeSP spVoucherType = new VoucherTypeSP();

                dtbl = spVoucherType.VoucherTypeCombofillForTaxAndVat(voucherName);

                DataRow dr = dtbl.NewRow();
                dr["voucherTypeName"] = "All";
                dr["voucherTypeId"] = 0;
                dtbl.Rows.InsertAt(dr, 0);

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = dtbl;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server error";
                response.Response = ex;
            }

            return response;
        }

        [HttpPost]
        public MatResponse GetTaxReport(TaxReportSearchParam input)
        {
            var response = new MatResponse();
            try
            {
                if(input.billOrProductWise == "Bill Wise")
                {
                    DataTable dtbl = new DataTable();
                    dtbl = new TaxSP().TaxReportGridFillByBillWise(input.fromDate, input.toDate, input.taxId, input.voucherTypeId, input.typeOfVoucher, input.inputOrOutput);

                    response.ResponseCode = 200;
                    response.ResponseMessage = "success";
                    response.Response = dtbl;
                }
                else
                {
                    DataTable dtbl = new DataTable();
                    dtbl = new TaxSP().TaxReportGridFillByProductwise(input.fromDate, input.toDate, input.taxId, input.voucherTypeId, input.typeOfVoucher, input.inputOrOutput);

                    response.ResponseCode = 200;
                    response.ResponseMessage = "success";
                    response.Response = dtbl;
                }
                
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server error";
                response.Response = ex;
            }

            return response;
        }
    }

    public class TaxReportSearchParam
    {
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public decimal taxId { get; set; }
        public decimal voucherTypeId { get; set; }
        public string typeOfVoucher { get; set; }
        public string billOrProductWise { get; set; }
        public bool inputOrOutput { get; set; }

    }
}
