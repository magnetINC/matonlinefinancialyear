﻿using MatApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using MATFinancials;
using System.Web.Http.Cors;
using MatApi.Models.Reports.SupplierReports;
using System.Dynamic;
using MATFinancials.DAL;

namespace MatApi.Controllers.Reports.PurchaseReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PurchaseReportsController : ApiController
    {
        [HttpGet]
        public MatResponse GetAllProducts()
        {
            var response = new MatResponse();
            try
            {
                ProductSP spProduct = new ProductSP();
                
                DataTable dtbl = spProduct.ProductViewAll();
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";

            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failure";

            }

            return response;
        }

        [HttpGet]
        public MatResponse GetAllSuppliers()
        {
            var response = new MatResponse();

            try
            {
                AccountLedgerSP sp = new AccountLedgerSP();
                DataTable dtbl = new DataTable();
                dtbl = sp.AccountLedgerViewSuppliersOnly();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }

            catch(Exception e)
            {
              
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;

            }
            return response;
          
        }

        //to get form type or voucherType
        [HttpGet]
        public MatResponse GetAllFormType()
        {
            var response = new MatResponse();

            try
            {
                VoucherTypeSP spVoucherType = new VoucherTypeSP();
                DataTable dtbl = new DataTable();
                dtbl = spVoucherType.VoucherTypeSelectionComboFill("Purchase Invoice");
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";

            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";

            }
            return response;
        }

        [HttpGet]
        public MatResponse GetAllVoucherType()
        {
            var response = new MatResponse();
            try
            {
                PurchaseOrderDetailsSP spPurchaseOrderDetails = new PurchaseOrderDetailsSP();
                var dtbl = new DataTable();
                dtbl = spPurchaseOrderDetails.VoucherTypeCombofillforPurchaseOrderReport();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch(Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }

            return response;
        }

        [HttpGet]
        public MatResponse GetSuppliersForPurchaseOrder()
        {
            var response = new MatResponse();
            try
            {
                TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
                var dtbl = new DataTable();
                dtbl = TransactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill(true);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch(Exception ex)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;

            }


            return response;
        }

        [HttpPost]
        public MatResponse PurchaseSummary(PurchaseSummaryVM p)
        {
            var response = new MatResponse();

           try
            {
                DataTable dtbl = new DataTable();
                PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();

               dtbl = spPurchaseMaster.PurchaseInvoiceReportFill(1,p.strColumn, p.fromDate, p.toDate,
               p.supplier, p.Status, p.purchaseMode,
              0, "",
               p.formType, "", "", p.productName);
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "successful";

            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }

            return response;
        }


        [HttpPost]
        public MatResponse PurchaseReturnSummary()
        {
            var response = new MatResponse();


            return response;

        }

        [HttpPost]
        public MatResponse PurchaseOrderReport(PurchaseOrderReportVM p)
        {
            var response = new MatResponse();
            try
            {
                PurchaseOrderMasterSP spPurchaseOrderMaster = new PurchaseOrderMasterSP();
                DataTable dtbl = new DataTable();
                dtbl = spPurchaseOrderMaster.PurchaseOrdeReportViewAll("-1", p.suppliers, p.voucherType, p.fromDate, p.toDate, p.status);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;

            }
            catch(Exception ex)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;

            }
            return response;
        }

        //to get voucher type for Material receipt report
        [HttpGet]
        public MatResponse GetVoucherTypes()
        {
            var response = new MatResponse();
            try
            {
                MaterialReceiptDetailsSP spMaterialReceiptDetails = new MaterialReceiptDetailsSP();
                var dtbl = new DataTable();
                dtbl = spMaterialReceiptDetails.VoucherTypeCombofillforMaterialReceipt();
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }


            return response;
        }

        //to get suppliers for matrerial receipt report
        [HttpGet]
        public MatResponse GetSuppliers()
        {
            var response = new MatResponse();

            try
            {
                TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
                var dtbl = new DataTable();
                dtbl = TransactionGeneralFillObj.CashOrPartyUnderSundryCrComboFill(true);
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetInvoiceNo(decimal supplierId)
        {
            var response = new MatResponse();
            try
            {
                DataTable dtbl = new DataTable();
                MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();
                dtbl = spMaterialReceiptMaster.GetOrderNoCorrespondingtoLedgerForMaterialReceiptRpt(supplierId);
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";

            }
            return response;
        }

        [HttpPost]
        public MatResponse MaterialReceiptReportDetails(MaterialReceiptReportVM m)
        {
            var response = new MatResponse();

            try
            {
                DataTable dtblMaterialReceiptRegister = new DataTable();
                MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();
                dtblMaterialReceiptRegister = spMaterialReceiptMaster.MaterialReceiptReportViewAll(m.orderNo,"", m.suppliers, m.formType, "", m.fromDate, m.toDate, m.status);
                response.Response = dtblMaterialReceiptRegister;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch(Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;
        }


        //For Rejection out Report
        [HttpPost]
        public MatResponse RejectionOutReportDetails(RejectionOutReportVM r)
        {
            var response = new MatResponse();
            try
            {
                RejectionOutMasterSP spRejectionOutMaster = new RejectionOutMasterSP();
                DataTable dtbl = spRejectionOutMaster.RejectionOutReportFill(r.formNo, r.productCode, r.productName, r.supplier, r.fromDate, r.toDate, r.MaterialReceiptNo, r.formType);
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";

            }
            return response;
        }

        //to get voucher types for Rejection out report
        [HttpGet]
        public MatResponse VoucherTypesForReject()
        {
            var response = new MatResponse();
            try
            {
                RejectionOutDetailsSP spRejectionOutDetails = new RejectionOutDetailsSP();
                DataTable dtbl = new DataTable();
                dtbl = spRejectionOutDetails.VoucherTypeComboFillForRejectionOutReport();
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;
        }
        //to get material receipts for Reject out report
        [HttpGet]
        public MatResponse GetMaterialReceipts(decimal supplierId)
        {
            var response = new MatResponse();
            try
            {
                MaterialReceiptMasterSP spMaterialReceiptMaster = new MaterialReceiptMasterSP();
                DataTable d = spMaterialReceiptMaster.MaterialReceiptNoCorrespondingToLedgerForReport(supplierId);
                response.Response = d;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
             return response;
        }

        [HttpPost]
        public MatResponse PurchaseReturnReport(PurchaseReturnReportVM p)
           {
            var response = new MatResponse();
            try
            {
                PurchaseReturnMasterSP spPurchaseReturnMaster = new PurchaseReturnMasterSP();
                DataTable d = spPurchaseReturnMaster.PurchaseReturnReportGridFill(p.fromDate, p.toDate, p.supplier, p.formType, -1, p.product, "");
                response.Response = d;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;

        }

        [HttpGet]
        public MatResponse GetVoucherTypesForPurchaseReturns()
        {
            var response = new MatResponse();

            try
            {
                PurchaseReturnDetailsSP spPurchaseReturnDetails = new PurchaseReturnDetailsSP();
                DataTable dtbl = spPurchaseReturnDetails.VoucherTypeComboFillForPurchaseReturn();
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }

            return response;
        }

        [HttpGet]
        public MatResponse GetProductsForPurchaseReturns()
        {
            var response = new MatResponse();
            try
            {
                ProductSP spproduct = new ProductSP();
                DataTable dtbl = spproduct.ProductViewAll();
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }


            return response;
        }

        public MatResponse GetInvoiveNo()
        {

            return new MatResponse();
        }

        //cash or bank details for payment report
        [HttpGet]
        public MatResponse GetCashorBank()
        {
            var response = new MatResponse();
            try
            {
                DataTable dtbl = new DataTable();
                TransactionsGeneralFill Obj = new TransactionsGeneralFill();
                dtbl = Obj.BankOrCashComboFill(false);
                response.Response = dtbl;

                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }


            return response;
        }
        
        //get acct ledger details for payment report
        [HttpGet]
        public MatResponse GetAcctLedgerDetails()
        {
            var response = new MatResponse();
            try
            {
                DataTable dtbl = new DataTable();
                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                dtbl = obj.AccountLedgerComboFill();
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }   
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }


            return response;
        }

        [HttpGet]
        public MatResponse GetVoucherDetails()
        {
            var response = new MatResponse();
            try
            {
                VoucherTypeSP SpVoucherType = new VoucherTypeSP();
                DataTable dtbl = new DataTable();
                dtbl = SpVoucherType.VoucherTypeViewAll();
                response.Response = dtbl;

                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
             return response;
        }



        [HttpGet]
        public MatResponse GetPaymentReportDetails(DateTime fromDate, DateTime toDate, decimal formType, decimal cashOrBank, decimal acctLedger)
        {
            var response = new MatResponse();
            try
            {
                PaymentMasterSP SpPaymentMaster = new PaymentMasterSP();
                DataTable dtbl = new DataTable();
                dtbl = SpPaymentMaster.PaymentReportSearch(fromDate,toDate,acctLedger,formType,cashOrBank);
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;
        }


        //to get account ledger for ageing report
        [HttpGet]
        public MatResponse GetAccountLedgers()
        {
            var response = new MatResponse();
            try
            {
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
               
                DataTable dtbl = spAccountLedger.AccountLedgerViewSuppliersOnly();

                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;
        }



        [HttpPost]
        public MatResponse GetAgeingReportDetails(DateTime ageing, decimal acctLedger)
        {
            var response = new MatResponse();
            try
            {
                PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
                DataTable dtbl = new DataTable();
                dtbl = SpPartyBalance.AgeingReportLedgerReceivable(ageing, acctLedger);
                //the rest of the response
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetSupplierBalanceReportDetails(DateTime toDate)
        {
            var response = new MatResponse();
            try
            {
                int gridRow = 0;
                decimal totalCredit = 0;
                DataSet dsSuppliers = new DataSet();
                DataTable dtblSuppliers = new DataTable();
                AccountLedgerSP spAccountledger = new AccountLedgerSP();
                try
                {
                    DBMatConnection connection = new DBMatConnection();

                    /*string query = string.Format("select row_number() over(order by cast(al.ledgerId as int)) as slNO, ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, " +
                        "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance from tbl_LedgerPosting lp " +
                        "right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId where al.accountGroupId = 22 AND lp.date <= '{0}'" +
                        "group by ledgerName, lp.ledgerId, al.ledgerId order by ledgerName", toDate);*/

                    //Inverse of Customer Balance Process (Credit - Debit)

                    string query = string.Format("select row_number() over(order by cast(al.ledgerId as int)) as slNO, ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, " +
                        "ISNULL(convert(decimal(18, 2), sum(isnull(credit, 0) - isnull(debit, 0))), 0) as openingBalance from tbl_LedgerPosting lp " +
                        "right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId where al.accountGroupId = 22 AND lp.date <= '{0}'" +
                        "group by ledgerName, lp.ledgerId, al.ledgerId order by ledgerName", toDate);

                    dsSuppliers = connection.ExecuteQuery(query);

                    dtblSuppliers = dsSuppliers.Tables[0];

                    dtblSuppliers = (from d in dtblSuppliers.AsEnumerable()
                                    where d.Field<decimal>("openingBalance") != 0
                                    select d).CopyToDataTable();

                    int gridRowNumber = 0;
                    for (int i = 0; i < dtblSuppliers.Rows.Count; i++)
                    {
                        //dgvCustomerBalance.Rows.Add();
                        //gridRowNumber++;
                        //dgvCustomerBalance.Rows[i].Cells["dgvtxtSNo"].Value = gridRowNumber;
                        //dgvCustomerBalance.Rows[i].Cells["dgvtxtLedgerName"].Value = dtblCustomer.Rows[i]["ledgerName"].ToString();
                        //dgvCustomerBalance.Rows[i].Cells["dgvtxtBalance"].Value = Convert.ToDecimal(dtblCustomer.Rows[i]["openingBalance"]).ToString("N2");
                        totalCredit += Convert.ToDecimal(dtblSuppliers.Rows[i]["openingBalance"]);
                    }
                    //gridRow = dgvCustomerBalance.Rows.Count - 1;
                    //dgvCustomerBalance.Rows.Add();
                    //dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtLedgerName"].Value = "";
                    //dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtBalance"].Value = "____________________";
                    //dgvCustomerBalance.Rows.Add();
                    //gridRow++;
                    //dgvCustomerBalance.Rows[gridRow].DefaultCellStyle.Font = new Font(dgvCustomerBalance.Font, FontStyle.Bold);
                    //dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtLedgerName"].Value = "Total";
                    //dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtBalance"].Value = Convert.ToDecimal(totalCredit).ToString("N2");
                    //dgvCustomerBalance.Rows.Add();
                    //gridRow++;
                    //dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtLedgerName"].Value = "";
                    //dgvCustomerBalance.Rows[gridRow].Cells["dgvtxtBalance"].Value = "====================";
                    //intNoOfRowsToPrint = dtblCustomer.Rows.Count;
                }
                catch (Exception ex)
                {

                }
                response.Response = dtblSuppliers;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
                //the rest of the response
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }
            return response;
        }
    }
}
