﻿using MatApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using MATFinancials;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Reports.BankingReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BankingReportsController : ApiController
    {

       
        [HttpGet]
        public MatResponse GetAllBanks()
        {
            var response = new MatResponse();

            try
            {
                ContraMasterSP spContraMaster = new ContraMasterSP();
                DataTable dtbl = new DataTable();
                dtbl = spContraMaster.CashOrBankComboFill();
                response.ResponseCode = 200;
                response.ResponseMessage = "Success";
                response.Response = dtbl;

            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";


            }

            return response;
        }


        [HttpGet]
       public MatResponse CashorBankReports(DateTime from, DateTime to)
        {
            var response = new MatResponse();

            try
            {
                var spFinancialStatement = new FinancialStatementSP();
                DataTable dtbl = new DataTable();
                dtbl = spFinancialStatement.CashOrBankBookGridFill(Convert.ToDateTime(from), Convert.ToDateTime(to), "", true);
                response.ResponseCode = 200;
                response.ResponseMessage = "Success";
                response.Response = dtbl;

            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
                
                
            }

            return response;
        }

        [HttpGet]
        //api for contrareportsView
        public MatResponse ContraReports(string ledgerName, string type, DateTime from, DateTime to)
        {
            var response = new MatResponse();

            try
            {
                ContraMasterSP spContraMaster = new ContraMasterSP();
                DataTable dtbl = new DataTable();
            
                dtbl = spContraMaster.ContraReport(from, to, "ALL", ledgerName, type);

                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Success";
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }

            return response;
        }


    }
}
