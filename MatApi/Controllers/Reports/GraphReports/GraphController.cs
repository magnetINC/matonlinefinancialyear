﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;
using MatApi.Models;
using MatApi.Models.Reports.Graph;


namespace MatApi.Controllers.Reports.GraphReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class GraphController : ApiController
    {
        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();

        [HttpGet]
        public IHttpActionResult SalesRate()
        {
            try
            {
                var year = PublicVariables.FromDate.Year;
                var sales = context.tbl_StockPosting.Where(a => /*a.date.Value.Year >= year &&*/ a.voucherTypeId != Constants.OpeningStockVoucherTypeId).OrderBy(a => a.date).ToList();
                var salesRate = new List<SaleByStoresTimeSeriesReport>();
                var salesRateGrouped = new List<SaleByStoresTimeSeriesReport>();
                foreach (var i in sales)
                {
                    if (i?.date != null)
                        salesRate.Add(new SaleByStoresTimeSeriesReport()
                        {
                            Date = Convert.ToDateTime(i.date.Value.ToString("yyyy MMMM dd")),
                            Sale = Convert.ToDecimal(i.outwardQty * i.rate),
                            StoreName = context.tbl_Godown.Where(a => a.godownId == i.godownId).FirstOrDefault()?.godownName
                        });
                }
                var grp = salesRate.GroupBy(a => new { StoreName = a.StoreName, Date = a.Date });

                foreach (var g in grp)
                {
                    var first = g.FirstOrDefault();
                    first.Sale = g.Sum(c => c.Sale);
                    salesRateGrouped.Add(first);
                }

                return Ok(salesRateGrouped);
            }
            catch(Exception ex)
            {
                return null;
            }
        }
    }
}
