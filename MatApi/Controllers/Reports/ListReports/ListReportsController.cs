﻿using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Data;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Reports.ListReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ListReportsController : ApiController
    {
        //For Account List Report
        //to get account group fill ups 
        public MatResponse GetAccountGroups()
        {
            var response = new MatResponse();

            try
            {
                AccountGroupSP spAccountGroup = new AccountGroupSP();
                DataTable dtblAccountGroupSearch = new DataTable();
                dtblAccountGroupSearch = spAccountGroup.AccountGroupViewAllComboFill();
                response.Response = dtblAccountGroupSearch;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }

            return response;
        }
        public MatResponse GetAccountListDetails(decimal acctgroupId)
        {
            var response = new MatResponse();

            try
            {
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
               // GeneralQueries queryForRetainedEarning = new GeneralQueries();
                DataTable dtbl = new DataTable();
                dtbl = spAccountLedger.AccountListViewAll(acctgroupId, MATFinancials.PublicVariables._dtFromDate, MATFinancials.PublicVariables._dtToDate);
                response.Response = dtbl;
                response.ResponseCode = 200;
                response.ResponseMessage = "Successful";
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
            }

            return response;
        }
    }
}
