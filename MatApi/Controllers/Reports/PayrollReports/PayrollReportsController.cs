﻿using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Reports.PayrollReports
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PayrollReportsController : ApiController
    {
        //for advance payment reporys
        public MatResponse GetEmpCodes()
        {
            var response = new MatResponse();

            try
            {
                EmployeeSP spEmployee = new EmployeeSP();
                var dtblEmployee = spEmployee.EmployeeViewAll();
                response.Response = dtblEmployee;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch(Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }

            return response;
        }

        public MatResponse GetAdvancePaymentDetails(DateTime from, DateTime to, string empCode, DateTime month)
        {
            var response = new MatResponse();

            try
            {
                AdvancePaymentSP spAdvancePayment = new AdvancePaymentSP();
                var dtbl = spAdvancePayment.AdvancePaymentViewAllForAdvancePaymentReport(from,to, empCode,month);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }

            return response;
        }


        //for bonusDeduction report
        //would use already defined method to get employeeCodes


        //to get designation fill-ups
        [HttpGet]
        public MatResponse GetDesignations()
        {
            var response = new MatResponse();

            try
            {
                DesignationSP spDesignation = new DesignationSP();
                var dtblDesignation = new DataTable();
                dtblDesignation = spDesignation.DesignationViewAll();
                response.Response = dtblDesignation;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }

            return response;
        }

        [HttpGet]
        public MatResponse GetBonusDeductionDetails(string from, string to, string month, string designation, string employee, string bonusOrDeduction )
        {
            var response = new MatResponse();

            try
            {
                BonusDedutionSP spBonusDeduction = new BonusDedutionSP();
                    var dtbl = spBonusDeduction.BonusDeductionReportGridFill(from, to, month, designation, employee, bonusOrDeduction);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }

            return response;
        }


        //for DailyAttendance Report
        //used previously defined method to get employees details
        //and previously defined method to get designations
        [HttpGet]
        public MatResponse GetDailyAttendanceDetails(string date, string employee, string designation)
        {
            var response = new MatResponse();

            try
            {
                DailyAttendanceMasterSP spDailyAttendanceMaster = new DailyAttendanceMasterSP();
                var dtbl = spDailyAttendanceMaster.DailyAttendanceViewForDailyAttendanceReport(date,"0", employee,designation);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }

            return response;
        }

        //for Daily Salary Report
        //used previously defined method to get employees details
        //and previously defined method to get designations 
        [HttpGet]
        public MatResponse GetDailySalaryReportDetails(string emp, string designation, DateTime from, DateTime to)
        {
            var response = new MatResponse();

            try
            {
                EmployeeSP spEmployee = new EmployeeSP();
                var dtbl = spEmployee.EmployeeViewAllForDailySalaryReport(emp,designation, from, to,"paid");
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }

            return response;
        }

        //for Employee  Report
        //used previously defined method to get employees details
        //and previously defined method to get designations 
        [HttpGet]
        public MatResponse GetEmployeeReportDetails(string employee, string designation,string status)
        {
            var response = new MatResponse();

            try
            {
                EmployeeSP spEmployee = new EmployeeSP();
                var dtbl = spEmployee.EmployeeViewAllEmployeeReport(designation, employee, status);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }

            return response;
        }

        //for PayHeadReport Report
        [HttpGet]
        public MatResponse GetPayHeadNames()
        {
            var response = new MatResponse();

            try
            {
                PayHeadSP spPayHead = new PayHeadSP();
                DataTable dtblPayHead = spPayHead.PayHeadViewAll();
                response.Response = dtblPayHead;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }

            return response;
        }

        [HttpGet]
        public MatResponse GetPayHeadReportDetails(string payHeadName, string type)
        {
            var response = new MatResponse();

            try
            {
                PayHeadSP spPayHead = new PayHeadSP();
                DataTable dtbl = spPayHead.PayHeadViewAllForPayHeadReport(payHeadName, type);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }

            return response;
        }


        //for Salary Package Details Report
        [HttpGet]
        public MatResponse GetPackageNames()
        {
            var response = new MatResponse();
            try
            {
                SalaryPackageSP spSalaryPackage = new SalaryPackageSP();
                DataTable dtbl = spSalaryPackage.SalaryPackageViewAll();
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch(Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetSalaryPackageReportDetails(string packageName)
        {
            var response = new MatResponse();
            try
            {
                SalaryPackageDetailsSP spSalaryPackageDetails = new SalaryPackageDetailsSP();
                DataTable dtbl = spSalaryPackageDetails.SalaryPackageDetailsForSalaryPackageDetailsReport(packageName);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }
            return response;
        }

        //for Salary package report, 
        //used previously defined fill-ups to get package name
        
        [HttpGet]
        public MatResponse GetSalaryPackageDetails(string packageName, string status)
        {
            var response = new MatResponse();
            try
            {
                SalaryPackageSP spSalaryPackage = new SalaryPackageSP();
                DataTable dtbl = spSalaryPackage.SalaryPackageViewAllForSalaryPackageReport(packageName, status);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }
            return response;
        }

        //for Monthly Salary report, 
        //used previously defined fill-ups to get package name
        [HttpGet]
        public MatResponse GetMonthlySalaryReportDetails(DateTime from, DateTime to,string employee, string designation, DateTime month)
        {
            var response = new MatResponse();
            try
            {
                MonthlySalarySP spMonthlySalary = new MonthlySalarySP();
                DataTable dtbl = spMonthlySalary.MonthlySalryViewAllForMonthlySalaryReports(from, to, designation,employee, month);
                response.Response = dtbl;
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }
            return response;
        }


        //for Employee Address Book Report
        //to get all employee list
        [HttpGet]
        public MatResponse GetEmployeeAddressBookDetails()
        {
            var response = new MatResponse();

            try
            {
                EmployeeSP spEmployee = new EmployeeSP();
                response.Response= spEmployee.EmployeeViewAllForEmployeeAddressBook("All", "", "","");
                response.ResponseMessage = "Successful";
                response.ResponseCode = 200;
            }
            catch(Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }


            return response;
        }

        [HttpGet]
        public MatResponse GetMonthlyAttendanceReportDetails(string designation, string status,DateTime month)
        {
           // DataTable dtblStatus;
            var response = new MatResponse(); 

            try
            {
                DailyAttendanceMasterSP spDailyAttendanceMaster = new DailyAttendanceMasterSP();
                EmployeeSP spEmployee = new EmployeeSP();
                DataTable dtblEmployee = new DataTable();
                dtblEmployee = spEmployee.EmployeeViewByDesignationAndStatus(designation, status);

                if (dtblEmployee.Rows.Count > 0)
                {
                    DataTable dtblStatus  = new DataTable();
                    for (int i = 0; i < dtblEmployee.Rows.Count; i++)
                    {
                       dtblStatus = spDailyAttendanceMaster.MonthlyAttendanceReportFill(month, Convert.ToDecimal(dtblEmployee.Rows[i]["employeeId"].ToString()));
                    }
                    response.Response = dtblStatus; 
                    response.ResponseMessage = "Successful";
                    response.ResponseCode = 200;
                }
            }
            catch (Exception e)
            {
                response.ResponseMessage = "Failed";
                response.ResponseCode = 500;
            }
            return response;
        }

    }
}
