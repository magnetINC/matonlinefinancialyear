﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Web.Http.Cors;
using DocumentFormat.OpenXml.ExtendedProperties;
using MatApi.DBModel;
using MatApi.Models.Company.Settings;
using MATFinancials;
using HttpGetAttribute = System.Web.Http.HttpGetAttribute;
using HttpPostAttribute = System.Web.Http.HttpPostAttribute;

namespace MatApi.Controllers.Company.Settings
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class FinancialYearController : ApiController
    {
        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        // GET: FinancialYear
        [HttpGet]
        public IHttpActionResult List()
        {
            var list = context.tbl_FinancialYear.ToList();
            return Ok(list); 
        }

        [HttpPost]
        public IHttpActionResult Create([FromBody] FinicialYearVm finicialYearVm)
        {
            if (ModelState.IsValid)
            {


                if (finicialYearVm.StartDate == finicialYearVm.EndDate)
                {
                  
                    return Ok(new { status = 100, message = "Start date cannot be equal to end date, Try again." });
                }

                if (finicialYearVm.StartDate >= finicialYearVm.EndDate)
                {
                    return Ok(new { status = 100, message = "Start date cannot be greater than end date, Try again." });
                }

                if(finicialYearVm.EndDate.Year > finicialYearVm.StartDate.Year)
                {

                    var fmS = 12 - finicialYearVm.StartDate.Month;

                    var fmEnd = finicialYearVm.EndDate.Month;

                    var RemainingMonth = (fmS + fmEnd);

                    if (RemainingMonth < 12 /* || RemainingMonth1 < 12*/)
                    {
                        return Ok(new { status = 100, message = "New financial year settings cant not be less than 12 months, Try again." });
                    }
                }

                if (finicialYearVm.EndDate.Year == finicialYearVm.StartDate.Year)
                {
                    var IsLessThan12Month = (finicialYearVm.EndDate.Month);

                    if (IsLessThan12Month < 12)
                    {
                        return Ok(new { status = 100, message = "New financial year settings cant not be less than 12 months, Try again." });
                    }
                }

                var IsMatchPreviousDate = context.tbl_FinancialYear.Where(f => finicialYearVm.StartDate <= f.fromDate || finicialYearVm.StartDate <= f.toDate).Any();
                if (IsMatchPreviousDate)
                {
                    return Ok(new { status = 100, message = "Date range already exiting, Try again." });
                }

                IsMatchPreviousDate = context.tbl_FinancialYear.Where(f => finicialYearVm.EndDate <= f.fromDate || finicialYearVm.EndDate <= f.toDate).Any();
                if (IsMatchPreviousDate)
                {
                   
                    return Ok(new { status = 100, message = "Date range already exiting, Try again." });
                }

                IsMatchPreviousDate = context.tbl_FinancialYear.Where(f => finicialYearVm.EndDate <= f.toDate  &&  f.fromDate >= finicialYearVm.StartDate).Any();
                if (IsMatchPreviousDate)
                {
                    
                    return Ok(new { status = 100, message = "Date range already exiting, Try again." });
                }

                context = new DBMATAccounting_MagnetEntities1();
                var companys = context.tbl_FinancialYear.ToList();
                if(companys!=null && companys.Count >0)
                {
                    foreach (var itm in companys)
                    {
                        itm.IsFinanacialYearExpired = false;
                        context.Entry(itm).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();
                    }
                }
               
                context.tbl_FinancialYear.Add(new tbl_FinancialYear()
                {
                    fromDate = finicialYearVm.StartDate, 
                    toDate = finicialYearVm.EndDate,
                    extraDate = DateTime.UtcNow,
                    extra1 = "",
                    extra2 = "",
                    IsFinanacialYearExpired = true
                });
                context.SaveChanges();

                var companyProfile =  context.tbl_Company.FirstOrDefault();
                if(companyProfile != null)
                {
                    companyProfile.currentDate = finicialYearVm.StartDate;
                    context.Entry(companyProfile).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                }


                try
                {
                    if (finicialYearVm.includeSign)
                    {
                        context = new DBMATAccounting_MagnetEntities1();
                        var Result = context.LoginDates.ToList();

                        foreach (var itm in Result)
                        {
                            itm.SignOutNoticesBoard = finicialYearVm.shortNote;
                            itm.SignOutPeriod = DateTime.UtcNow;
                            itm.SignOutDate = DateTime.UtcNow;
                            itm.Status = "Generic SignOut";
                            context.Entry(itm).State = System.Data.Entity.EntityState.Modified;
                            context.SaveChanges();
                        }
                    }

                }
                catch (Exception ex)
                {

                }

                return Ok(new { status = 200, message="Operation Successful" });
            }

            return Ok(new { status = 100, message = "Something went wrong, Please check your date range" });
           
        }

        [HttpGet]
        public IHttpActionResult  DeleteYear(int id)
        {
            var find = context.tbl_FinancialYear.Find(id);
            if (find != null)
            {
                var isUsed = context.tbl_SalesMaster.Any(a => a.financialYearId == id);
                if (isUsed)
                {
                    return Ok(new { status =400, message="Delete Failed, Year has been used to process record" });
                }

                else
                {
                    context.tbl_FinancialYear.Remove(find);
                    context.SaveChanges();
                    return Ok(new { status =200, message="Operation Successful" });
                }
                
            }

            return BadRequest("Something went Wrong");
        }
       
        [HttpGet]
        public IHttpActionResult SetYear(int id)
        {
            
            var find = context.tbl_FinancialYear.Find(id); 
            if (find != null)
            {
                 //var company =    context.tbl_Company.FirstOrDefault();
                 //if (company == null) return BadRequest("No Company Found");

                    //company.currentDate = find.fromDate;
                    //company.extraDate = find.toDate;
                    //company.extra1 = find.financialYearId.ToString();
                    //context.Entry(company).State = System.Data.Entity.EntityState.Modified;
                    //context.SaveChanges();

                 PublicVariables._dtFromDate = Convert.ToDateTime(find.fromDate);
                 PublicVariables._dtToDate = Convert.ToDateTime(find.toDate);

                 PublicVariables._decCurrentFinancialYearId = find.financialYearId;
                 PublicVariables.CurrentFinicialId = (int) find.financialYearId;

                 MatApi.Models.PublicVariables.ToDate = Convert.ToDateTime(find.toDate);
                 MatApi.Models.PublicVariables.FromDate = Convert.ToDateTime(find.fromDate);
                 MatApi.Models.PublicVariables.CurrentFinicialYearId = (int) find.financialYearId;

                try
                {
                    Models.Security.LoginResponseVM response = new Models.Security.LoginResponseVM();
                    //var newContext = new MatDal.DBMATAccounting_MagnetEntities();
                    //var Result = newContext.tbl_FinancialYear.ToList();
                    if (find != null)
                    {
                        if (find != null)
                        {
                            response.FinancialYearDisplay = find.fromDate.Value.ToShortDateString() + "  -  " + find.toDate.Value.ToShortDateString();
                            response.FinancialYear = find.fromDate.Value.Year;

                            response._dtToDate = Convert.ToDateTime(find.toDate.Value);
                            response._dtFromDate = Convert.ToDateTime(find.fromDate.Value);
                            response._decCurrentFinancialYearId = (int)find.financialYearId;
                           
                            response.CurrentFinicialId = (int)find.financialYearId;
                            response.ToDate = Convert.ToDateTime(find.toDate.Value);
                            response.FromDate = Convert.ToDateTime(find.fromDate.Value);
                            response.CurrentFinicialYearId = (int)find.financialYearId;
                            response._decCurrentFinancialYearId = (int)find.financialYearId;
                            response.IsInCurrentFinancialYear = false;
                           
                        }
                    }

                    return Ok(new { status = 200, message = "Operation Successful", response = response });
                }
                catch (Exception ex)
                {

                }

                return BadRequest("Something went Wrong");
            }

            return BadRequest("Something went Wrong");
        }

        [HttpGet]
        public IHttpActionResult SetYear1(int id)
        {

            try
            {
                var finds = context.tbl_FinancialYear.ToList();
                if(finds!=null && finds.Count>0)
                {
                    foreach(var itm in finds)
                    {
                        itm.IsFinanacialYearExpired = false;
                        context.Entry(itm).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();
                    }
                }
                
                var find = context.tbl_FinancialYear.Find(id);
                if (find != null)
                {


                    find.IsFinanacialYearExpired = true;
                    context.Entry(find).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();

                    PublicVariables._dtFromDate = Convert.ToDateTime(find.fromDate);
                    PublicVariables._dtToDate = Convert.ToDateTime(find.toDate);

                    PublicVariables._decCurrentFinancialYearId = find.financialYearId;
                    PublicVariables.CurrentFinicialId = (int)find.financialYearId;

                    MatApi.Models.PublicVariables.ToDate = Convert.ToDateTime(find.toDate);
                    MatApi.Models.PublicVariables.FromDate = Convert.ToDateTime(find.fromDate);
                    MatApi.Models.PublicVariables.CurrentFinicialYearId = (int)find.financialYearId;

                    try
                    {
                        Models.Security.LoginResponseVM response = new Models.Security.LoginResponseVM();
                        //var newContext = new MatDal.DBMATAccounting_MagnetEntities();
                        //var Result = newContext.tbl_FinancialYear.ToList();
                        if (find != null)
                        {
                            if (find != null)
                            {
                                response.FinancialYearDisplay = find.fromDate.Value.ToShortDateString() + "  -  " + find.toDate.Value.ToShortDateString();
                                response.FinancialYear = find.fromDate.Value.Year;

                                response._dtToDate = Convert.ToDateTime(find.toDate.Value);
                                response._dtFromDate = Convert.ToDateTime(find.fromDate.Value);
                                response._decCurrentFinancialYearId = (int)find.financialYearId;

                                response.CurrentFinicialId = (int)find.financialYearId;
                                response.ToDate = Convert.ToDateTime(find.toDate.Value);
                                response.FromDate = Convert.ToDateTime(find.fromDate.Value);
                                response.CurrentFinicialYearId = (int)find.financialYearId;

                                response.IsInCurrentFinancialYear = true;
                            }
                        }

                        return Ok(new { status = 200, message = "Operation Successful", response = response });
                    }
                    catch (Exception ex)
                    {

                    }

                    return BadRequest("Something went Wrong");
                }

            }
            catch (Exception ex)
            {

            }
            return BadRequest("Something went Wrong");
        }

        [HttpGet]
        public IHttpActionResult ChangeFinancialYear(int id)
        {
            try
            {
                var companys = context.tbl_FinancialYear.ToList();
                foreach(var itm in companys)
                {
                    itm.IsFinanacialYearExpired = false;
                    context.Entry(itm).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                }

                var find = context.tbl_FinancialYear.Where(a => a.financialYearId == id).FirstOrDefault();
                if (find != null)
                {
                    find.IsFinanacialYearExpired = true;
                    context.Entry(find).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                    return Ok(new { status = 200, message = "Operation Successful" });
                }

            }
            catch (Exception ex)
            {

            }
            return BadRequest("Something went Wrong");
        }

        [HttpGet]
        public IHttpActionResult GetCurrentFinancialYear()
        {
            var company =    context.tbl_Company.FirstOrDefault();
            if (company == null) return BadRequest("No Company");
           
            var yearId =  company.extra1;
            var finicialYear = context.tbl_FinancialYear.Where(a => a.IsFinanacialYearExpired == true).FirstOrDefault();
          
            if (finicialYear == null) return Ok(new {status = 400, data = "No Financial year set"});
          
            return Ok(new {status = 200, data = finicialYear});
        }
    }
}