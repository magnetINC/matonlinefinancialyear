﻿using MatApi.Models.Company.Settings;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Company.Settings
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SettingsController : ApiController
    { 
        [HttpGet]
        public DataTable LoadSettings()
        {
            SettingsSP spSettings = new SettingsSP();
            DataTable dtbl = new DataTable();
            dtbl = spSettings.SettingsViewAll();
            return dtbl;
        }

        [HttpPost]
        public bool UpdateSettings(List<SettingsVM> settings)
        {
            bool isAllSettingsUpdated = false;
            foreach(var setting in settings)
            {
                if(UpdateSettings(setting))
                {
                    isAllSettingsUpdated = true;
                }
                else
                {
                    isAllSettingsUpdated = false;
                }
            }
            return isAllSettingsUpdated;
        }

        [HttpGet]
        private bool UpdateSettings(SettingsVM input)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("UPDATE tbl_Settings " +
                                            "SET status='{0}' " +
                                            "WHERE settingsId={1} ", input.Status,input.SettingsId);
            if(conn.customUpdateQuery(queryStr)>0)
            {
                return true;
            }
            return false;
        }
    }
}
