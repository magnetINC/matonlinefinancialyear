﻿using MatApi.Models.Company.Settings;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;

namespace MatApi.Controllers.Company.Settings
{
    [EnableCors(origins: "*", headers: "*", methods: "*")] 
    public class UserCreationController : ApiController
    {
        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public UserCreationController()
        {

        }

        [HttpGet]
        public IHttpActionResult GetUser(int id)
        {
            var user = context.tbl_User.Find(id);
            return Ok(user);
        }
        [HttpGet]
        public IHttpActionResult GetUser()
        {
            var users = context.tbl_User.ToList();
            return Ok(users);
        }

        [HttpGet]
        public DataTable GetStores()
        {
            return new GodownSP().GodownViewAll();
        }

        [HttpGet]
        public HttpResponseMessage GetAllUses()   //all users including admin
        {
            UserSP spUser = new UserSP();
            RoleSP roles = new RoleSP();
            dynamic response = new ExpandoObject();
            response.Users = spUser.UserViewAll();
            response.Roles = roles.RoleViewAll();
            response.Stores= new GodownSP().GodownViewAll();
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
        public DataTable GetUsers()
        {
            try
            {
                UserSP spUser = new UserSP();
                return spUser.UserCreationViewForGridFill("", "All");
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "UC:7" + ex.Message;
            }
            return null;
        }

        [HttpPost]
        public string AddUser(UserInfo input)
        {
            try
            {
                UserInfo infoUser = new UserInfo();
                UserSP spUser = new UserSP();
                infoUser.UserName = input.UserName;
                infoUser.Password = new MATFinancials.Classes.Security().base64Encode(input.Password);
                infoUser.FirstName = input.FirstName;
                infoUser.LastName = input.LastName;
                infoUser.StoreId = input.StoreId;
                infoUser.Active = input.Active;
                infoUser.RoleId = input.RoleId;
                infoUser.Narration = input.Narration!= null ? input.Narration : "";
                infoUser.Extra1 = string.Empty;
                infoUser.Extra2 = string.Empty;
                string strUserName = input.UserName;
                if (spUser.UserCreationCheckExistence(0, strUserName) == false)
                {
                    var chk = spUser.UserAdd(infoUser);
                    if (chk>0)
                    {
                        return spUser.GetUserIdAfterLogin(infoUser.UserName,infoUser.Password).ToString();
                    }
                }
                else
                {
                    return "USERNAME_EXISTS";
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "UC:3" + ex.Message;
            }
            return "";
        }

        [HttpPost]
        public string EditUser(UserInfo input)
        {
            try
            {
                string employeeUserId = input.UserId.ToString();
                tbl_Employee salesManEmployee = context.tbl_Employee.SingleOrDefault(p => p.extra1 == employeeUserId);
                if (salesManEmployee != null)
                    UpdateSalesMan(input, salesManEmployee);

                UserSP spUser = new UserSP();
                //password is not suppose to be set here but we av to set the password cz the stored procedure takes password
                //parameter hence user encoded password is fetched and sent with edit object
                var userpass= spUser.UserView(input.UserId).Password;

                if (!string.IsNullOrEmpty(input.Password))
                {
                    userpass = new MATFinancials.Classes.Security().base64Encode(input.Password);

                }


                UserInfo infoUser = new UserInfo();
                infoUser.UserId = input.UserId;
                infoUser.UserName = input.UserName;
                infoUser.Password = userpass;
                infoUser.FirstName = input.FirstName;
                infoUser.LastName = input.LastName;
                infoUser.StoreId = input.StoreId;
                infoUser.Active = input.Active;
                infoUser.RoleId = input.RoleId;
                infoUser.Narration = input.Narration != null ? input.Narration : "";
                infoUser.Extra1 = string.Empty;
                infoUser.Extra2 = string.Empty;
                string strUserName = input.UserName;
                if (spUser.UserEdit(infoUser) > 0)
                {
                    return "SUCCESS";
                }
                //if (spUser.UserCreationCheckExistence(0, strUserName) == false)
                //{
                //    if (spUser.UserEdit(infoUser) > 0)
                //    {
                //        return "SUCCESS";
                //    }
                //}
                //else
                //{
                //    return "USERNAME_EXISTS";
                //}
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "UC:3" + ex.Message;
            }
            return "Error";
        }

        [HttpPost]
        public bool DeleteUser(UserInfo input)
        {
            UserInfo infoUser = new UserInfo();
            UserSP spUser = new UserSP();
            if ((spUser.UserCreationReferenceDelete(input.UserId)>0))
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool BlockUserAccount(UserVm input)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("UPDATE tbl_User " +
                                            "SET active='{0}', narration='{1}' " +
                                            "WHERE userId={2} ", input.active, input.narration, input.userId);
            if (conn.customUpdateQuery(queryStr) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool ChangePassword(UserInfo input)
        {
            DBMatConnection conn = new DBMatConnection();
            string hashedPwd= new MATFinancials.Classes.Security().base64Encode(input.Password);
            string queryStr = string.Format("UPDATE tbl_User SET password='{0}' WHERE userId={1} ", 
                                                hashedPwd, input.UserId);
            if (conn.customUpdateQuery(queryStr) > 0)
            {
                return true;
            }
            return false;
        }
        public void UpdateSalesMan(UserInfo input, tbl_Employee salesManEmployee)
        {
            EmployeeSP SpEmployee = new EmployeeSP();
            tbl_Employee SalesManEmployee = context.tbl_Employee.SingleOrDefault(p => p.employeeId == salesManEmployee.employeeId);
            SalesManEmployee.employeeCode = input.UserName;
            SalesManEmployee.designationId = Convert.ToDecimal(SpEmployee.SalesmanGetDesignationId());
            SalesManEmployee.employeeName = input.FirstName + " " + input.LastName;
            SalesManEmployee.narration = input.Narration;
            SalesManEmployee.isActive = input.Active;
            context.SaveChanges();
        }


    }
}
