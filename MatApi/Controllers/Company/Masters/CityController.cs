﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials;
using System.Data;

namespace MatApi.Controllers.Company.Masters
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CityController : ApiController
    {
        RouteSP routeSp;
        public CityController()
        {
            routeSp = new RouteSP();
        }

        public List<RouteInfo> GetCities()
        {
            var routesDt = routeSp.RouteViewAll();
            List<RouteInfo> routes = new List<RouteInfo>();
            foreach(DataRow row in routesDt.Rows)
            {
                routes.Add(new RouteInfo
                {
                    RouteId = Convert.ToDecimal(row["RouteId"]),
                    AreaId = Convert.ToDecimal(row["AreaId"]),
                    RouteName = row["RouteName"].ToString(),
                    Extra1= row["Extra1"].ToString(),
                    Extra2= row["Extra2"].ToString(),
                    ExtraDate = Convert.ToDateTime(row["ExtraDate"]),
                    Narration = row["Narration"].ToString()
                });
            }
            return routes;
        }

        public RouteInfo GetCity(decimal cityId)
        {
            var cityDt = routeSp.RouteView(cityId);
            RouteInfo route = new RouteInfo
            {
                RouteId =Convert.ToDecimal(cityDt.RouteId),
                AreaId =cityDt.AreaId,
                RouteName =cityDt.RouteName,
                Extra1=cityDt.Extra1,
                Extra2 = cityDt.Extra2,
                Narration = cityDt.Narration,
                ExtraDate = cityDt.ExtraDate

            };
            return route;
        }

        [HttpPost]
        public bool DeleteCity(RouteInfo route)
        {
            if(routeSp.RouteDeleting(route.RouteId)>0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public decimal AddCity(RouteInfo route)
        {
            route.ExtraDate = DateTime.Now;
            if(routeSp.RouteCheckExistence(route.RouteName, route.RouteId, route.AreaId))
            {
                return -1;
            }
            else
            {
                if(routeSp.RouteAdd(route))
                {
                    return 1;
                }
                else
                {
                    return 0;
                }
            }
        }
        [HttpPost]
        public bool EditCity(RouteInfo route)
        {
            //route.ExtraDate = DateTime.Now; //--oni
            return routeSp.RouteEdit(route);
        }
    }
}
