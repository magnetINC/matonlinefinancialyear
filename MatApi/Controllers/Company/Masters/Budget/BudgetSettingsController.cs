﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Dynamic;
using MatApi.Models;
using MATFinancials;
using System.Data;
using MATFinancials.DAL;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Company.Masters.Budget
{
    [EnableCors(origins:"*", headers:"*", methods:"*")]
    public class BudgetSettingsController : ApiController
    {
        BudgetMasterSP spBudgetMaster = new BudgetMasterSP();
        BudgetMasterInfo infoBudgetMaster = new BudgetMasterInfo();
        BudgetDetailsSP spBudgetDetails = new BudgetDetailsSP();
        BudgetDetailsInfo infoBudgetDetails = new BudgetDetailsInfo();
        decimal decBudgetMasterIdentity = 0;
        decimal decBudgetId;
        decimal decTxtTotalDebit = 0;
        decimal decTxtTotalCredit = 0;
        decimal decAmount = 0;
        int inKeyPressCount = 0;
        bool isValueChanged = false;
        int inUpdatingRowIndexForPartyRemove = -1;
        decimal decUpdatingLedgerForPartyremove = 0;

        public BudgetSettingsController()
        {

        }
        [HttpGet]
        public MatResponse GetLookUps()
        {
            MatResponse response = new MatResponse();
            try
            {
                dynamic lookups = new ExpandoObject();

                lookups.accounts = new AccountLedgerSP().AccountLedgerViewAll();
                lookups.groups = new AccountGroupSP().AccountGroupViewAll();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookups;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetBudgetSettings(string accountType)
        {
            MatResponse response = new MatResponse();
            DBMatConnection db = new DBMatConnection();
            string query = string.Empty;
            try
            {
                query = string.Format("SELECT * FROM tbl_BudgetMaster WHERE type='{0}'", accountType);

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = db.customSelect(query);
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }

        [HttpPost]
        public MatResponse SaveBudgetSettings(BudgetVM input)
        {
            MatResponse response = new MatResponse();
            try
            {
                var master = input.master;
                var details = input.details;

                decBudgetMasterIdentity = spBudgetMaster.BudgetMasterAdd(master);

                int inRowCount = details.Count();
                for (int inI = 0; inI < inRowCount ; inI++)
                {
                    details[inI].BudgetMasterId = decBudgetMasterIdentity;
                    spBudgetDetails.BudgetDetailsAdd(details[inI]);

                    response.ResponseCode = 200;
                    response.ResponseMessage = "success";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetBudgetSettingDetails(decimal budgetSettingId)
        {
            MatResponse response = new MatResponse();
            try
            {
                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = spBudgetDetails.BudgetDetailsViewByMasterId(budgetSettingId);
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
        [HttpGet]
        public MatResponse DeleteBudgetSettings(decimal budgetSettingId)
        {
            MatResponse response = new MatResponse();
            try
            {
                if (spBudgetMaster.BudgetMasterDelete(budgetSettingId) == -1)
                {
                    response.ResponseCode = 203;
                    response.ResponseMessage = "Reference Exists for this budget setting";
                }
                else
                {
                    response.ResponseCode = 200;
                    response.ResponseMessage = "Budget Deleted Successfully";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }

        [HttpPost]
        public MatResponse EditBudgetSettings(BudgetVM input)
        {
            MatResponse response = new MatResponse();
            try
            {
                var master = input.master;
                var details = input.details;

                spBudgetMaster.BudgetMasterEdit(master);

                var dbBudgetDetails = spBudgetDetails.BudgetDetailsViewByMasterId(master.BudgetMasterId);
                for (int i = 0; i < dbBudgetDetails.Rows.Count; i++)
                {
                    var exist = details.Find(p => p.BudgetDetailsId == Convert.ToDecimal(dbBudgetDetails.Rows[i].ItemArray[0]));

                    if (exist == null)
                    {
                        spBudgetDetails.BudgetDetailsDelete(Convert.ToDecimal(dbBudgetDetails.Rows[i].ItemArray[0]));
                    }
                }


                int inRowCount = details.Count();
                for (int inI = 0; inI < inRowCount; inI++)
                {
                   if (details[inI].BudgetDetailsId != 0)
                    {
                        details[inI].BudgetMasterId = master.BudgetMasterId;
                        spBudgetDetails.BudgetDetailsEdit(details[inI]);

                        response.ResponseCode = 200;
                        response.ResponseMessage = "success";
                    }
                    if (details[inI].BudgetDetailsId == 0)
                    {
                        details[inI].BudgetMasterId = master.BudgetMasterId;
                        spBudgetDetails.BudgetDetailsAdd(details[inI]);

                        response.ResponseCode = 200;
                        response.ResponseMessage = "success";
                    }
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
    }
    public class BudgetVM
    {
        public BudgetMasterInfo master { get; set; }
        public List<BudgetDetailsInfo> details { get; set; }
    }
}
