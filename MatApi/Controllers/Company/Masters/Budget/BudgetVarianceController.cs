﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http;
using System.Dynamic;
using MatApi.Models;
using MATFinancials;
using System.Data;
using MATFinancials.DAL;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Company.Masters.Budget
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BudgetVarianceController : ApiController
    {
        public BudgetVarianceController()
        {

        }
        [HttpGet]
        public MatResponse GetLookUps()
        {
            MatResponse response = new MatResponse();
            BudgetMasterSP spbudgetmaster = new BudgetMasterSP();

            string query = string.Empty;
            try
            {
                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = spbudgetmaster.BudgetViewAll();
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
        [HttpGet]
        public MatResponse GetBudgetVariance(decimal decId)
        {
            MatResponse response = new MatResponse();
            BudgetMasterSP spbudgetmaster = new BudgetMasterSP();

            string query = string.Empty;
            try
            {
                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = spbudgetmaster.BudgetVariance(decId);

            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
    }
}
