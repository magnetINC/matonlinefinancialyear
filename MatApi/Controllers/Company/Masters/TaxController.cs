﻿using MatApi.DBModel;
using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Company.Masters
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class TaxController : ApiController
    {
        TaxSP taxSp;
        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public TaxController()
        {
            taxSp = new TaxSP();
        }

        public List<TaxInfo> GetTaxes()
        {
            var taxesDt = taxSp.TaxViewAll();
            List<TaxInfo> taxes = new List<TaxInfo>();
            foreach (DataRow row in taxesDt.Rows)
            {
                taxes.Add(new TaxInfo
                {
                    TaxId = Convert.ToDecimal(row["TaxId"]),
                    Rate = Convert.ToDecimal(row["Rate"]),
                    ApplicableOn = row["ApplicableOn"].ToString(),
                    TaxName = row["TaxName"].ToString(),
                    CalculatingMode = row["CalculatingMode"].ToString(),
                    IsActive = Convert.ToBoolean(row["IsActive"]),
                    Type = row["type"].ToString(),
                    Narration = row["Narration"].ToString()
                });
            }
            return taxes;
        }

        public SmartShopResponse TaxList()
        {
            var taxesDt = taxSp.TaxViewAll();
            List<TaxInfo> taxes = new List<TaxInfo>();
            foreach (DataRow row in taxesDt.Rows)
            {
                taxes.Add(new TaxInfo
                {
                    TaxId = Convert.ToDecimal(row["TaxId"]),
                    Rate = Convert.ToDecimal(row["Rate"]),
                    ApplicableOn = row["ApplicableOn"].ToString(),
                    TaxName = row["TaxName"].ToString(),
                    CalculatingMode = row["CalculatingMode"].ToString(),
                    IsActive = Convert.ToBoolean(row["IsActive"]),
                    Type = row["type"].ToString()
                });
            }
            SmartShopResponse resp = new SmartShopResponse();
            resp.data = taxes;
            return resp;
        }

        public TaxInfo GetTax(decimal taxId)
        {
            var taxDt = taxSp.TaxView(taxId);
            TaxInfo tax = new TaxInfo
            {
                TaxId = Convert.ToDecimal(taxDt.TaxId),
                Rate = Convert.ToDecimal(taxDt.Rate),
                ApplicableOn = taxDt.ApplicableOn.ToString(),
                TaxName = taxDt.TaxName.ToString(),
                CalculatingMode = taxDt.CalculatingMode.ToString(),
                IsActive = Convert.ToBoolean(taxDt.IsActive),
                Type = taxDt.Type.ToString()

            };
            return tax;
        }

        [HttpPost]
        public bool DeleteTax(TaxInfo tax)
        {
            var ledgerIdString = context.tbl_Tax.Where(t => t.taxId == tax.TaxId).Select(t => t.extra1).FirstOrDefault();
            decimal ledgerId = Convert.ToDecimal(ledgerIdString);
            if (taxSp.TaxReferenceDelete(tax.TaxId,ledgerId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddTax(TaxInfo tax)
        {
            tax.ExtraDate = DateTime.Now;
            decimal ledgerId = CreateLedger(tax);
            tax.Extra1 = ledgerId.ToString();

            if (taxSp.TaxAdd(tax))
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditTax(TaxInfo tax)
        {   
            var ledgerId = context.tbl_Tax.Where(t => t.taxId == tax.TaxId).Select(t => t.extra1).FirstOrDefault();
            decimal ledgerIdDec = Convert.ToDecimal(ledgerId);
            var ledgerObj = context.tbl_AccountLedger.Where(l => l.ledgerId == ledgerIdDec).FirstOrDefault();
            ledgerObj.ledgerName = tax.TaxName;
            ledgerObj.mailingName = tax.TaxName;
            ledgerObj.isActive = tax.IsActive;
            context.SaveChanges();
            tax.ExtraDate = DateTime.Now;
            tax.Extra1 = ledgerId;
            return taxSp.TaxEdit(tax);
        }
        /// <summary>
        /// Creating one ledger for the purticular tax
        /// </summary>
        public decimal CreateLedger(TaxInfo tax)
        {
            decimal ledgerId = 0;
            try
            {
                AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                infoAccountLedger.AccountGroupId = 20;
                infoAccountLedger.LedgerName = tax.TaxName;
                infoAccountLedger.OpeningBalance = 0;
                infoAccountLedger.IsDefault = false;
                infoAccountLedger.CrOrDr = "Cr";
                infoAccountLedger.Narration = string.Empty;
                infoAccountLedger.MailingName = tax.TaxName;
                infoAccountLedger.Address = string.Empty;
                infoAccountLedger.Phone = string.Empty;
                infoAccountLedger.Mobile = string.Empty;
                infoAccountLedger.Email = string.Empty;
                infoAccountLedger.CreditPeriod = 0;
                infoAccountLedger.CreditLimit = 0;
                infoAccountLedger.PricinglevelId = 0;
                infoAccountLedger.BillByBill = false;
                infoAccountLedger.Tin = string.Empty;
                infoAccountLedger.Cst = string.Empty;
                infoAccountLedger.Pan = string.Empty;
                infoAccountLedger.RouteId = 1;
                infoAccountLedger.BankAccountNumber = string.Empty;
                infoAccountLedger.BranchName = string.Empty;
                infoAccountLedger.BranchCode = string.Empty;
                infoAccountLedger.ExtraDate = DateTime.Now;
                infoAccountLedger.Extra1 = string.Empty;
                infoAccountLedger.Extra2 = string.Empty;
                infoAccountLedger.AreaId = 1;
                infoAccountLedger.IsActive = tax.IsActive;
                ledgerId = spAccountLedger.AccountLedgerAddWithIdentity(infoAccountLedger);
            }
            catch (Exception ex)
            {
                
            }
            return ledgerId;
        }  
    }
}
