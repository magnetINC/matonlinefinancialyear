﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using MatDal;
using System.Web.Http.Cors;
using MATFinancials;

namespace MatApi.Controllers.Company.Masters.Account
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DropdownController : ApiController
    {
        TransactionsGeneralFill dropdown;
        public DropdownController()
        {
            dropdown = new TransactionsGeneralFill();
        }

        [HttpGet]
        public HttpResponseMessage SalesManDropdown()
        {
            //var accountGroupDt = accountGroupSp.AccountGroupView(accountGroupId);
            var salesMan = dropdown.SalesmanViewAllForComboFill(false);
            return Request.CreateResponse(HttpStatusCode.OK, salesMan);
        }

        [HttpGet]
        public HttpResponseMessage PricingLevelDropdown()
        {
            //var accountGroupDt = accountGroupSp.AccountGroupView(accountGroupId);
            var pricingLevel = dropdown.PricingLevelViewAll();
            return Request.CreateResponse(HttpStatusCode.OK, pricingLevel);
        }

        [HttpGet]
        public HttpResponseMessage CurrencyDropdown()
        {
            //var accountGroupDt = accountGroupSp.AccountGroupView(accountGroupId);
            var currency = dropdown.CurrencyComboByDate(DateTime.Now);
            return Request.CreateResponse(HttpStatusCode.OK, currency);
        }

    }
}