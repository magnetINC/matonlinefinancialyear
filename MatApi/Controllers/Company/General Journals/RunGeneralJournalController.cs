﻿using MatApi.Models;
using MatApi.Models.Company;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;

namespace MatApi.Controllers.Company.General_Journals
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RunGeneralJournalController : ApiController
    {
        DataTable dtblPartyBalance = new DataTable();//to store party balance entries while clicking btn_Save

        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public RunGeneralJournalController()
        {
        }

        [HttpGet]
        public HttpResponseMessage GetDropdowns()
        {
            dynamic response = new ExpandoObject();
            var ledgers = context.tbl_AccountLedger
                .Where(a => a.accountGroupId != AccountGroupConstants.CostOfSale)
                
                .Include(a => a.tbl_AccountGroup).Select(a=> new
                {
                    ledgerId = a.ledgerId,
                    ledgerName = a.ledgerName,
                    accountGroupName = a.tbl_AccountGroup.accountGroupName,
                    accountNature = a.tbl_AccountGroup.nature
                })
                .OrderBy(a=>a.ledgerName)
                .ToList();

            //response.AccountLedger = new AccountLedgerSP().AccountLedgerViewForJournalVoucher();
            response.AccountLedger = ledgers;
            response.Currencies = new TransactionsGeneralFill().CurrencyComboByDate(DateTime.Now);
            response.InvoiceNo = getNextInvoiceNo();
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public List<tbl_JournalMaster> GetJournals()
        {

            var FromDate = MatApi.Models.PublicVariables.FromDate;
            var ToDate = MatApi.Models.PublicVariables.ToDate;
            var CurrentFinicialYearId = MatApi.Models.PublicVariables.CurrentFinicialYearId;
           
            var ledgers = context.tbl_JournalMaster.Where(p =>( p.date >= FromDate && p.date <= ToDate)).ToList();

            var count = ledgers.Count - 1;

            for (var i = 0; i <= count; i++)
            {
                try
                {
                    var UserId = Convert.ToDecimal(ledgers[i].doneby);
                    var doneby = context.tbl_User.Where(u => u.userId == UserId).FirstOrDefault();
                    ledgers[i].doneby = doneby.userName;
                }
                catch(Exception ex)
                {

                }
            }

            return ledgers.ToList();
            //JournalMasterSP journal = new JournalMasterSP();
            //return journal.JournalMasterViewAll();
        }

        [HttpPost]
        public DataTable GetPartyBalance(PartyBalanceInvoicesVM input)
        {
            input.VoucherTypeId = 6;
            DataTable response = new DataTable();
            response = new PartyBalanceSP().PartyBalanceComboViewByLedgerId(input.LedgerId, input.DebitOrCredit, input.VoucherTypeId, input.voucherNo);
            return response;
        }

        [HttpPost]
        public bool Save(SaveJournalVM input)
        {
            bool isSaved = false;
            decimal decJournalVoucherTypeId = 6;
            //var userId = MatApi.Models.PublicVariables.CurrentUserId;
            try
            {
                //if (!MATFinancials.CheckUserPrivilege.PrivilegeCheck(Convert.ToDecimal(userId), "frmGeneralJournal", "Save"))
                //{
                //    isSaved = false;
                //    return isSaved;
                //}
                decimal decTotalDebit = 0;
                decimal decTotalCredit = 0;

                for (int i = 0; i < input.JournalDetailsInfo.Count; i++)
                {
                    decTotalDebit += input.JournalDetailsInfo[i].Debit;
                    decTotalCredit += input.JournalDetailsInfo[i].Credit;
                }

                JournalMasterSP spJournalMaster = new JournalMasterSP();
                JournalDetailsSP spJournalDetails = new JournalDetailsSP();
                JournalMasterInfo infoJournalMaster = new JournalMasterInfo();
                JournalDetailsInfo infoJournalDetails = new JournalDetailsInfo();
                PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                ExchangeRateSP spExchangeRate = new ExchangeRateSP();

                //check if invoice number entered already exists
                string query = string.Format("SELECT voucherNo FROM tbl_JournalMaster WHERE voucherNo='{0}' AND voucherTypeId=6", input.JournalMasterInfo.InvoiceNo);
                if (new DBMatConnection().IsFormNumberExist(query))
                {
                    //returnValue = -1;
                    decimal nextInvoiceNo = spJournalMaster.JournalMasterGetMaxPlusOne(decJournalVoucherTypeId);
                    input.JournalMasterInfo.InvoiceNo = nextInvoiceNo.ToString();
                }

                infoJournalMaster.VoucherNo = input.JournalMasterInfo.InvoiceNo;
                infoJournalMaster.InvoiceNo = input.JournalMasterInfo.InvoiceNo;
                infoJournalMaster.SuffixPrefixId = 0;
                infoJournalMaster.Date = input.JournalMasterInfo.Date;
                infoJournalMaster.Narration = input.JournalMasterInfo.Narration;
                infoJournalMaster.DoneBy = input.JournalMasterInfo.DoneBy;
                infoJournalMaster.UserId = input.JournalMasterInfo.UserId; //MATFinancials.PublicVariables._decCurrentUserId;
                infoJournalMaster.VoucherTypeId = decJournalVoucherTypeId;
                infoJournalMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());

                infoJournalMaster.Extra1 = string.Empty;
                infoJournalMaster.Extra2 = string.Empty;
                infoJournalMaster.ExtraDate = DateTime.Now;
                //infoJournalMaster.DoneBy = input.JournalMasterInfo.DoneBy;


                infoJournalMaster.TotalAmount = decTotalDebit;
                decimal decJournalMasterId = spJournalMaster.JournalMasterAdd(infoJournalMaster);
                if (decJournalMasterId > 0)
                {
                    isSaved = true;
                }
                /*******************JournalDetailsAdd and LedgerPosting*************************/
                infoJournalDetails.JournalMasterId = decJournalMasterId;
                infoJournalDetails.ExtraDate = DateTime.Now;
                infoJournalDetails.Extra1 = string.Empty;
                infoJournalDetails.Extra2 = string.Empty;
                infoJournalDetails.ChequeDate = DateTime.Now;
                infoJournalDetails.Memo = "";

                decimal decLedgerId = 0;
                decimal decDebit = 0;
                decimal decCredit = 0;
                //int inRowCount = dgvJournalVoucher.RowCount;
                //for (int inI = 0; inI < inRowCount - 1; inI++)
                for (int i = 0; i < input.JournalDetailsInfo.Count; i++)
                {
                    infoJournalDetails.LedgerId = input.JournalDetailsInfo[i].LedgerId;
                    decLedgerId = infoJournalDetails.LedgerId;
                    infoJournalDetails.Memo = input.JournalDetailsInfo[i].Memo;
                    infoJournalDetails.ProjectId = input.JournalDetailsInfo[i].ProjectId;
                    infoJournalDetails.CategoryId = input.JournalDetailsInfo[i].CategoryId;
                    infoJournalDetails.ExchangeRateId = 1;

                    //--------Currency conversion--------------//
                    decimal decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(1);
                    //decimal decAmount = Convert.ToDecimal(input.JournalDetailsInfo[i] dgvJournalVoucher.Rows[inI].Cells["dgvtxtAmount"].Value.ToString());
                    //decConvertRate = decAmount * decSelectedCurrencyRate;
                    //===========================================//
                    if (input.JournalDetailsInfo[i].Debit > 0)
                    {
                        infoJournalDetails.Debit = input.JournalDetailsInfo[i].Debit;
                        infoJournalDetails.Credit = 0;
                        decDebit = input.JournalDetailsInfo[i].Debit * decSelectedCurrencyRate;
                        decCredit = infoJournalDetails.Credit;
                    }
                    else if (input.JournalDetailsInfo[i].Credit > 0)
                    {
                        infoJournalDetails.Credit = input.JournalDetailsInfo[i].Credit;
                        infoJournalDetails.Debit = 0;
                        decDebit = input.JournalDetailsInfo[i].Credit;
                        decCredit = input.JournalDetailsInfo[i].Credit * decSelectedCurrencyRate;
                    }

                    infoJournalDetails.ExchangeRateId = 1;
                    infoJournalDetails.ChequeNo = input.JournalDetailsInfo[i].ChequeNo;
                    infoJournalDetails.ChequeDate = DateTime.Now;
                    //infoJournalDetails.ChequeDate = (input.JournalDetailsInfo[i].ChequeDate==null) ? DateTime.Now : input.JournalDetailsInfo[i].ChequeDate;

                    decimal decJournalDetailsId = spJournalDetails.JournalDetailsAdd(infoJournalDetails);
                    if (decJournalDetailsId != 0)
                    {
                        PartyBalanceAddOrEdit(i, input.PartyBalanceInfo, input.JournalMasterInfo);
                        LedgerPosting(decLedgerId, decCredit, decDebit, decJournalDetailsId, input.JournalDetailsInfo[i], input.JournalMasterInfo);
                    }

                }

                //store JournalPartyBalance | input.JournalPartyBalance
                if(input.JournalPartyBalanceInfo.Any())
                {
                    bool storeResult = StoreJournalPartyBalance(input);
                }
            }
            catch (Exception ex)
            {
                isSaved = false;
            }
            return isSaved;
        }

        private bool StoreJournalPartyBalance(SaveJournalVM input) //List<PartyBalanceInfo> journalPartyBalances, JournalMasterInfo journalMasterInfo)
        {
            bool result = false;
            try
            {
                PartyBalanceSP spPartyBalance = new PartyBalanceSP();
                foreach (var item in input.JournalPartyBalanceInfo)
                {
                    var InfopartyBalance = new tbl_PartyBalance();
                    InfopartyBalance.creditPeriod = 0;
                    InfopartyBalance.date = input.JournalMasterInfo.Date;
                    InfopartyBalance.exchangeRateId = 1;
                    InfopartyBalance.extra1 = string.Empty;
                    InfopartyBalance.extra2 = string.Empty;
                    InfopartyBalance.extraDate = DateTime.Now;
                    InfopartyBalance.financialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;

                    InfopartyBalance.againstInvoiceNo = item.AgainstInvoiceNo;
                    InfopartyBalance.againstVoucherNo = item.AgainstVoucherNo;
                    InfopartyBalance.credit = item.Credit;
                    InfopartyBalance.debit = item.Debit;
                    InfopartyBalance.invoiceNo = item.InvoiceNo;
                    InfopartyBalance.voucherNo = item.VoucherNo;
                    InfopartyBalance.voucherTypeId = item.VoucherTypeId;
                    InfopartyBalance.againstVoucherTypeId = item.AgainstVoucherTypeId;
                    InfopartyBalance.ledgerId = item.LedgerId;
                    InfopartyBalance.referenceType = item.ReferenceType;

                    context.tbl_PartyBalance.Add(InfopartyBalance);
                }
                context.SaveChanges();
                result = true;
            }
            catch (Exception ex)
            {
                //log exception ...
                result = false;
            }
            return result;
        }

        [HttpGet]
        public decimal getNextInvoiceNo()
        {
            decimal inv = new JournalMasterSP().JournalMasterGetMax(6);
            return inv + 1;
        }

        public bool PartyBalanceAddOrEdit(int inJ, List<PartyBalanceInfo> input, JournalMasterInfo journalMaster)
        {
            bool isSave = false;
            decimal voucherTypeId = 6;

            try
            {
                int inTableRowCount = dtblPartyBalance.Rows.Count;
                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                PartyBalanceSP spPartyBalance = new PartyBalanceSP();
                InfopartyBalance.Debit = 0;
                InfopartyBalance.CreditPeriod = 0;
                InfopartyBalance.Date = journalMaster.Date /*DateTime.Now*//*input[inJ].Date*/;
                InfopartyBalance.Credit = input[inJ].Credit;
                InfopartyBalance.ExchangeRateId = 1/*input[inJ].ExchangeRateId*/;
                InfopartyBalance.Extra1 = string.Empty;
                InfopartyBalance.Extra2 = string.Empty;
                InfopartyBalance.ExtraDate = DateTime.Now;
                InfopartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                InfopartyBalance.LedgerId = input[inJ].LedgerId;
                InfopartyBalance.ReferenceType = input[inJ].ReferenceType;
                if (input[inJ].ReferenceType == "New" || input[inJ].ReferenceType == "OnAccount")
                {
                    InfopartyBalance.AgainstInvoiceNo = "0";//dtblPartyBalance.Rows[inJ]["AgainstInvoiceNo"].ToString();
                    InfopartyBalance.AgainstVoucherNo = "0";//dtblPartyBalance.Rows[inJ]["AgainstVoucherNo"].ToString();
                    InfopartyBalance.AgainstVoucherTypeId = 0;// Convert.ToDecimal(dtblPartyBalance.Rows[inJ]["AgainstVoucherTypeId"].ToString());//decPaymentVoucherTypeId;
                    InfopartyBalance.VoucherTypeId = voucherTypeId;
                    InfopartyBalance.InvoiceNo = input[inJ].InvoiceNo;

                    InfopartyBalance.VoucherNo = input[inJ].VoucherNo;

                }
                else
                {
                    InfopartyBalance.ExchangeRateId = 1;
                    InfopartyBalance.AgainstInvoiceNo = input[inJ].AgainstInvoiceNo;

                    InfopartyBalance.AgainstVoucherNo = input[inJ].AgainstVoucherNo;

                    InfopartyBalance.AgainstVoucherTypeId = voucherTypeId;
                    InfopartyBalance.VoucherTypeId = input[inJ].VoucherTypeId;
                    InfopartyBalance.VoucherNo = input[inJ].VoucherNo;
                    InfopartyBalance.InvoiceNo = input[inJ].InvoiceNo;
                }
                if (input[inJ].PartyBalanceId.ToString() == "0")
                {
                    if (spPartyBalance.PartyBalanceAdd(InfopartyBalance) > 0)
                    {
                        isSave = true;
                    }
                }
                else
                {
                    InfopartyBalance.PartyBalanceId = input[inJ].PartyBalanceId;
                    if (spPartyBalance.PartyBalanceEdit(InfopartyBalance) > 0)
                    {
                        isSave = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RV10:" + ex.Message;
            }
            return isSave;
        }

        public void LedgerPosting(decimal decId, decimal decCredit, decimal decDebit, decimal decDetailsId, JournalDetailsInfo journalDetailsInfo, JournalMasterInfo journalMaster)
        {
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            ExchangeRateSP SpExchangRate = new ExchangeRateSP();
            decimal decOldExchange = 0;
            decimal decNewExchangeRate = 0;
            decimal decNewExchangeRateId = 0;
            decimal decOldExchangeId = 0;
           
            try
            {

                if (journalDetailsInfo.Extra1 == "Against")
                {
                    //infoLedgerPosting.Date = PublicVariables._dtCurrentDate;  // ------- old implementation using system date set n FormMdi ----------- //
                    infoLedgerPosting.Date = /*DateTime.Now*/journalMaster.Date;
                    infoLedgerPosting.VoucherTypeId = 6;
                    infoLedgerPosting.VoucherNo = journalMaster.VoucherNo;
                    infoLedgerPosting.DetailsId = decDetailsId;
                    infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.InvoiceNo = journalMaster.InvoiceNo;

                    infoLedgerPosting.ChequeNo = journalDetailsInfo.ChequeNo;
                    infoLedgerPosting.ChequeDate = /*DateTime.Now*/journalMaster.Date;

                    //if (dgvJournalVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value != null && dgvJournalVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value.ToString() != string.Empty)
                    //{
                    //    infoLedgerPosting.ChequeNo = dgvJournalVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value.ToString();
                    //    if (dgvJournalVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value != null && dgvJournalVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value.ToString() != string.Empty)
                    //    {
                    //        infoLedgerPosting.ChequeDate = Convert.ToDateTime(dgvJournalVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value.ToString());
                    //    }
                    //    else
                    //        infoLedgerPosting.ChequeDate = DateTime.Now;
                    //}
                    //else
                    //{
                    //    infoLedgerPosting.ChequeNo = string.Empty;
                    //    infoLedgerPosting.ChequeDate = DateTime.Now;
                    //}


                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.LedgerId = decId;
                    infoLedgerPosting.Credit = decCredit;
                    infoLedgerPosting.Debit = decDebit;

                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
                else
                {
                    //infoLedgerPosting.Date = PublicVariables._dtCurrentDate; // ------- old implementation using system date set n FormMdi ----------- //
                    infoLedgerPosting.Date = journalMaster.Date/*DateTime.Now*/;
                    infoLedgerPosting.VoucherTypeId = 6;
                    infoLedgerPosting.VoucherNo = journalMaster.VoucherNo;
                    infoLedgerPosting.DetailsId = decDetailsId;
                    infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.InvoiceNo = journalMaster.InvoiceNo;

                    infoLedgerPosting.ChequeNo = journalDetailsInfo.ChequeNo;
                    infoLedgerPosting.ChequeDate = journalMaster.Date/*DateTime.Now*/;

                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.LedgerId = decId;

                    foreach (DataRow dr in dtblPartyBalance.Rows)
                    {
                        if (infoLedgerPosting.LedgerId == Convert.ToDecimal(dr["LedgerId"].ToString()))
                        {
                            //decOldExchange = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                            //decNewExchangeRateId = Convert.ToDecimal(dr["CurrencyId"].ToString());
                            //decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchange);
                            //decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                            //// decConvertRate = decConvertRate + (decAmount * decSelectedCurrencyRate);     // Urefe add comment to stop duplication of values 20161101
                            //decConvertRate = (decAmount * decSelectedCurrencyRate);
                        }
                    }

                    //if (decCredit == 0)
                    //{
                    //    infoLedgerPosting.Credit = 0;
                    //    infoLedgerPosting.Debit = decConvertRate;
                    //}
                    //else
                    //{
                    //    infoLedgerPosting.Debit = 0;
                    //    infoLedgerPosting.Credit = decConvertRate;
                    //}
                    infoLedgerPosting.Credit = journalDetailsInfo.Credit;
                    infoLedgerPosting.Debit = journalDetailsInfo.Debit;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                    infoLedgerPosting.LedgerId = 12;
                    foreach (DataRow dr in dtblPartyBalance.Rows)
                    {
                        //if (Convert.ToDecimal(dgvJournalVoucher.Rows[inA].Cells["dgvcmbAccountLedger"].Value.ToString()) == Convert.ToDecimal(dr["LedgerId"].ToString()))
                        //{
                        //    if (dr["ReferenceType"].ToString() == "Against")
                        //    {
                        //        decNewExchangeRateId = Convert.ToDecimal(dr["CurrencyId"].ToString());
                        //        decNewExchangeRate = SpExchangRate.GetExchangeRateByExchangeRateId(decNewExchangeRateId);
                        //        decOldExchangeId = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                        //        decOldExchange = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchangeId);
                        //        decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                        //        decimal decForexAmount = (decAmount * decNewExchangeRate) - (decAmount * decOldExchange);
                        //        if (dr["DebitOrCredit"].ToString() == "Dr")
                        //        {
                        //            if (decForexAmount >= 0)
                        //            {

                        //                infoLedgerPosting.Debit = decForexAmount;
                        //                infoLedgerPosting.Credit = 0;
                        //            }
                        //            else
                        //            {
                        //                infoLedgerPosting.Credit = -1 * decForexAmount;
                        //                infoLedgerPosting.Debit = 0;
                        //            }
                        //        }
                        //        else
                        //        {
                        //            if (decForexAmount >= 0)
                        //            {

                        //                infoLedgerPosting.Credit = decForexAmount;
                        //                infoLedgerPosting.Debit = 0;
                        //            }
                        //            else
                        //            {
                        //                infoLedgerPosting.Debit = -1 * decForexAmount;
                        //                infoLedgerPosting.Credit = 0;
                        //            }
                        //        }
                        //        spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                        //    }
                        //}
                    }
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "JV20:" + ex.Message;
            }
        }
        public void LedgerPostingEdit(decimal decId, decimal decCredit, decimal decDebit, decimal decDetailsId, JournalDetailsInfo journalDetailsInfo, JournalMasterInfo journalMaster)
        {
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            ExchangeRateSP SpExchangRate = new ExchangeRateSP();
            decimal decOldExchange = 0;
            decimal decNewExchangeRate = 0;
            decimal decNewExchangeRateId = 0;
            decimal decOldExchangeId = 0;
            infoLedgerPosting.LedgerId = journalDetailsInfo.LedgerId;
            try
            {

                if (journalDetailsInfo.Extra1 == "Against")
                {
                    //infoLedgerPosting.Date = PublicVariables._dtCurrentDate;  // ------- old implementation using system date set n FormMdi ----------- //
                    infoLedgerPosting.Date = /*DateTime.Now*/journalMaster.Date;
                    infoLedgerPosting.VoucherTypeId = journalMaster.VoucherTypeId;
                    infoLedgerPosting.VoucherNo = journalMaster.VoucherNo;
                    infoLedgerPosting.DetailsId = decDetailsId;
                    infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.InvoiceNo = journalMaster.InvoiceNo;

                    infoLedgerPosting.ChequeNo = journalDetailsInfo.ChequeNo;
                    infoLedgerPosting.ChequeDate = /*DateTime.Now*/journalMaster.Date;

                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.LedgerId = decId;
                    infoLedgerPosting.Credit = decCredit;
                    infoLedgerPosting.Debit = decDebit;

                    spLedgerPosting.LedgerPostingEditByDetailsId(infoLedgerPosting);
                }
                else
                {
                    //infoLedgerPosting.Date = PublicVariables._dtCurrentDate; // ------- old implementation using system date set n FormMdi ----------- //
                    infoLedgerPosting.Date = journalMaster.Date/*DateTime.Now*/;
                    infoLedgerPosting.VoucherTypeId = journalMaster.VoucherTypeId;
                    infoLedgerPosting.VoucherNo = journalMaster.VoucherNo;
                    infoLedgerPosting.DetailsId = decDetailsId;
                    infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.InvoiceNo = journalMaster.InvoiceNo;

                    infoLedgerPosting.ChequeNo = journalDetailsInfo.ChequeNo;
                    infoLedgerPosting.ChequeDate = journalMaster.Date/*DateTime.Now*/;

                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.LedgerId = decId;
                    infoLedgerPosting.Credit = journalDetailsInfo.Credit;
                    infoLedgerPosting.Debit = journalDetailsInfo.Debit;
                    spLedgerPosting.LedgerPostingEditByDetailsId(infoLedgerPosting);
                    //infoLedgerPosting.LedgerId = 12;
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "JV20:" + ex.Message;
            }
        }

        public bool DetailsLedgerPosting(int inA, decimal decreceiptDetailsId, DateTime chequeDate, string chequeNo, ReceiptMasterInfo mast, ReceiptDetailsInfo det, List<PartyBalanceInfo> partyBalanceDetails)
        {
            bool isSaved = false;
            LedgerPostingInfo InfoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
            ExchangeRateSP SpExchangRate = new ExchangeRateSP();
            SettingsSP spSettings = new SettingsSP();
            decimal decOldExchange = 0;
            decimal decNewExchangeRate = 0;
            decimal decNewExchangeRateId = 0;
            decimal decOldExchangeId = 0;
            decimal decJournalVoucherTypeId = 6;
            // decConvertRate = 0;
            try
            {
                //if (!dgvReceiptVoucher.Rows[inA].Cells["dgvtxtAmount"].ReadOnly)
                //{

                //decimal d = Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbCurrency"].Value.ToString());
                //InfoLedgerPosting.LedgerId = Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbAccountLedger"].Value.ToString());

                //decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbCurrency"].Value.ToString()));
                //decAmount = Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvtxtAmount"].Value.ToString());
                //decConvertRate = decAmount * decSelectedCurrencyRate;

                //InfoLedgerPosting.Date = input.Date;
                //InfoLedgerPosting.Debit = 0;
                //InfoLedgerPosting.Credit = input.Credit;
                //InfoLedgerPosting.DetailsId = decreceiptDetailsId;
                //InfoLedgerPosting.Extra1 = string.Empty;
                //InfoLedgerPosting.Extra2 = string.Empty;
                //InfoLedgerPosting.InvoiceNo = input.InvoiceNo;
                //if (dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value != null && dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value.ToString() != string.Empty)
                //{
                //    InfoLedgerPosting.ChequeNo = dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value.ToString();
                //    if (dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value != null && dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value.ToString() != string.Empty)
                //    {
                //        InfoLedgerPosting.ChequeDate = Convert.ToDateTime(dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value.ToString());
                //    }
                //    else
                //        InfoLedgerPosting.ChequeDate = DateTime.Now;
                //}
                //else
                //{
                //    InfoLedgerPosting.ChequeNo = string.Empty;
                //    InfoLedgerPosting.ChequeDate = DateTime.Now;
                //}


                //InfoLedgerPosting.VoucherNo = strVoucherNo;

                //InfoLedgerPosting.VoucherTypeId = decReceiptVoucherTypeId;
                //InfoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
                //}
                //else
                //{
                InfoLedgerPosting.Date = mast.Date;
                InfoLedgerPosting.Extra1 = string.Empty;
                InfoLedgerPosting.Extra2 = string.Empty;
                InfoLedgerPosting.InvoiceNo = mast.InvoiceNo;
                InfoLedgerPosting.VoucherTypeId = decJournalVoucherTypeId;
                InfoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                InfoLedgerPosting.Debit = 0;
                InfoLedgerPosting.LedgerId = det.LedgerId;
                InfoLedgerPosting.VoucherNo = mast.VoucherNo;
                InfoLedgerPosting.DetailsId = decreceiptDetailsId;
                if (chequeDate != null && (chequeNo != "" || chequeNo != null))
                {
                    InfoLedgerPosting.ChequeDate = chequeDate;
                    InfoLedgerPosting.ChequeNo = chequeNo;
                }
                else
                {
                    InfoLedgerPosting.ChequeNo = "";
                    InfoLedgerPosting.ChequeDate = mast.Date/*DateTime.Now*/;
                }

                //foreach (DataRow dr in dtblPartyBalance.Rows)
                //{
                //    if (InfoLedgerPosting.LedgerId == Convert.ToDecimal(dr["LedgerId"].ToString()))
                //    {
                //        decOldExchange = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                //        decNewExchangeRateId = Convert.ToDecimal(dr["CurrencyId"].ToString());
                //        decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchange);
                //        decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                //        //decConvertRate = decConvertRate + (decAmount * decSelectedCurrencyRate);      //old implementation takes continual sum of all rows rather than for each row 20161228
                //        decConvertRate = (decAmount * decSelectedCurrencyRate);
                //    }
                //}
                InfoLedgerPosting.Credit = partyBalanceDetails[inA].Credit;
                if (SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting) > 0)
                {
                    isSaved = true;
                }
                else
                {
                    isSaved = false;
                }

                InfoLedgerPosting.LedgerId = 12;
                InfoLedgerPosting.DetailsId = 0;
                //foreach (DataRow dr in dtblPartyBalance.Rows)
                for (int v = 0; v < partyBalanceDetails.Count; v++)
                {
                    //if (Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbAccountLedger"].Value.ToString()) == Convert.ToDecimal(dr["LedgerId"].ToString()))
                    if (partyBalanceDetails[v].ReferenceType == "Against")
                    {
                        if (partyBalanceDetails[inA].Credit >= 0)
                        {

                            InfoLedgerPosting.Credit = partyBalanceDetails[inA].Credit;
                            InfoLedgerPosting.Debit = 0;
                        }
                        else
                        {
                            InfoLedgerPosting.Debit = -1 * partyBalanceDetails[inA].Credit;
                            InfoLedgerPosting.Credit = 0;
                        }
                        if (SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting) > 0)
                        {
                            isSaved = true;
                        }
                        else
                        {
                            isSaved = false;
                        }
                        //if (dr["ReferenceType"].ToString() == "Against")
                        //{
                        //    decNewExchangeRateId = Convert.ToDecimal(dr["CurrencyId"].ToString());
                        //    decNewExchangeRate = SpExchangRate.GetExchangeRateByExchangeRateId(decNewExchangeRateId);
                        //    decOldExchangeId = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                        //    decOldExchange = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchangeId);
                        //    decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                        //    decimal decForexAmount = (decAmount * decNewExchangeRate) - (decAmount * decOldExchange);
                        //    if (decForexAmount >= 0)
                        //    {

                        //        InfoLedgerPosting.Credit = decForexAmount;
                        //        InfoLedgerPosting.Debit = 0;
                        //    }
                        //    else
                        //    {
                        //        InfoLedgerPosting.Debit = -1 * decForexAmount;
                        //        InfoLedgerPosting.Credit = 0;
                        //    }
                        //    SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
                        //}
                    }

                }


                // }

            }
            catch (Exception ex)
            {
                // formMDI.infoError.ErrorString = "RV23:" + ex.Message;
            }
            return isSaved;
        }

        [HttpGet]
        public JournalViewVM ViewJournal(decimal journalMasterId)
        {
            try
            {

                var master = new JournalMasterSP().JournalMasterView(journalMasterId);
                var details = new JournalDetailsSP().JournalDetailsViewByMasterId(journalMasterId);
                var acctLedgersDt = new AccountLedgerSP().AccountLedgerViewAll();
                //var ledgerDetails = new LedgerPostingSP().LedgerPostingViewByLedgerID(decimal.Parse(acctLedgersDt.Rows[0].ItemArray[0].ToString()));
                UserInfo user = new UserSP().UserView(master.UserId);
                List<AccountLedgerInfo> ledgers = new List<AccountLedgerInfo>();
                for (int i = 0; i < acctLedgersDt.Rows.Count; i++)
                {
                    ledgers.Add(new AccountLedgerInfo
                    {
                        LedgerId = Convert.ToDecimal(acctLedgersDt.Rows[i].ItemArray[0]),
                        LedgerName = acctLedgersDt.Rows[i].ItemArray[2].ToString(),
                    });
                }


                List<CustomJournalDetailsVM> custDetails = new List<CustomJournalDetailsVM>();

                try
                {
                    for (int i = 0; i < details.Rows.Count; i++)
                    {
                        var Result = ledgers.Where(p => p.LedgerId == Convert.ToDecimal(details.Rows[i].ItemArray[2])).FirstOrDefault();

                        if (Result != null)
                        {
                            custDetails.Add(new CustomJournalDetailsVM
                            {
                                ChequeDate = Convert.ToDateTime(details.Rows[i].ItemArray[7]),
                                ChequeNo = details.Rows[i].ItemArray[6].ToString(),
                                Credit = Convert.ToDecimal(details.Rows[i].ItemArray[3]),
                                Debit = Convert.ToDecimal(details.Rows[i].ItemArray[4]),
                                JournalDetails = Convert.ToDecimal(details.Rows[i].ItemArray[0]),
                                JournalMaster = Convert.ToDecimal(details.Rows[i].ItemArray[1]),
                                LedgerId = Convert.ToDecimal(details.Rows[i].ItemArray[2]),
                                LedgerName = Result.LedgerName.ToString(),
                                Memo = details.Rows[i].ItemArray[9].ToString(),
                                ProjectId = details.Rows[i].ItemArray[12].ToString(),
                                CategoryId = details.Rows[i].ItemArray[13].ToString(),
                                JournalDetailsId = Convert.ToDecimal(details.Rows[i].ItemArray[0]),
                            });
                        }
                        else
                        {
                            custDetails.Add(new CustomJournalDetailsVM
                            {
                                ChequeDate = Convert.ToDateTime(details.Rows[i].ItemArray[7]),
                                ChequeNo = details.Rows[i].ItemArray[6].ToString(),
                                Credit = Convert.ToDecimal(details.Rows[i].ItemArray[3]),
                                Debit = Convert.ToDecimal(details.Rows[i].ItemArray[4]),
                                JournalDetails = Convert.ToDecimal(details.Rows[i].ItemArray[0]),
                                JournalMaster = Convert.ToDecimal(details.Rows[i].ItemArray[1]),
                                LedgerId = 0,
                                LedgerName = "",
                                Memo = details.Rows[i].ItemArray[9].ToString(),
                                ProjectId = details.Rows[i].ItemArray[12].ToString(),
                                CategoryId = details.Rows[i].ItemArray[13].ToString(),
                                JournalDetailsId = Convert.ToDecimal(details.Rows[i].ItemArray[0]),
                            });
                        }

                    }
                }
                catch(Exception eix)
                {

                }


                JournalViewVM response = new JournalViewVM();
                response.JournalMasterId = master.JournalMasterId;
                response.JournalNo = master.InvoiceNo;
                response.JournalDate = master.Date.ToString("dd/MM/yyyy");
                response.UserId = master.UserId;
                response.UserFullName = user.FirstName + " " + user.LastName;
                response.TotalAmount = master.TotalAmount;
                response.Details = custDetails;

                return response;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        public MatResponse DeleteJournal(decimal id, string voucherNo)
        {
            MatResponse response = new MatResponse();
            try
            {
                JournalMasterSP spJournalMaster = new JournalMasterSP();
                PartyBalanceSP spPartyBalance = new PartyBalanceSP();

                if (!spPartyBalance.PartyBalanceCheckReference(6, voucherNo))
                {
                    spJournalMaster.JournalVoucherDelete(id, 6, voucherNo);

                    response.ResponseCode = 200;
                    response.ResponseMessage = "Journal Deleted successfully";
                }
                else
                {
                    response.ResponseCode = 403;
                    response.ResponseMessage = "Reference exist. Cannot delete";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Sorry, Something went wrong";
            }
            return response;
        }
        [HttpGet]
        public MatResponse DeleteJournalEntries(decimal journalMasterId, string journalEntries)
        {
            MatResponse response = new MatResponse();
            var master = new JournalMasterSP().JournalMasterView(journalMasterId);
            var details = new JournalDetailsSP().JournalDetailsViewByMasterId(journalMasterId);
            //List<int> entryList = journalEntries.Split('_').Select(Int32.Parse).ToList();
            // List<int> entryList = new List<int>(Array.ConvertAll(journalEntries.Split('_'), int.Parse)); 
            List<string> entryListString = journalEntries.Split('_').ToList();
            List<int> entryList = new List<int>();
            for (int i = 1; i < entryListString.Count; i++)
            {
                int item = int.Parse(entryListString[i]);
                entryList.Add(item);
            }
            decimal debit = 0;
            decimal credit = 0;

            for (int i = 0; i < details.Rows.Count; i++)
            {
                int id = int.Parse(details.Rows[i].ItemArray[0].ToString());
                if (entryList.Contains(id))
                {
                    Decimal debitNo = Convert.ToDecimal(details.Rows[i].ItemArray[4]);
                    debit -= debitNo;
                    Decimal creditNo = Convert.ToDecimal(details.Rows[i].ItemArray[3]);
                    credit += creditNo;
                }
            }
            //if ((credit + debit) != 0)
            //{
            //    response.ResponseCode = 403;
            //    response.ResponseMessage = "Cannot delete. There is a variance between credit and debit.";

            //}

            //else
            //{
                foreach (var item in entryList)
                {
                    var detail = new JournalDetailsSP();
                    decimal itemDecimal = (decimal)item;
                    detail.JournalDetailsDelete(itemDecimal);
                    new LedgerPostingSP().LedgerPostDeleteByJournalDetailsId(itemDecimal);
                //new PartyBalanceSP().PartyBalanceComboViewByLedgerId(input.LedgerId, input.DebitOrCredit, input.VoucherTypeId, input.voucherNo);
                // details.JournalDetailsDelete(item);

            }
                //JournalMasterInfo journalMasterInfo = new JournalMasterInfo
                //{
                //    JournalMasterId = master.JournalMasterId,
                //    VoucherNo = master.VoucherNo,
                //    InvoiceNo = master.InvoiceNo,
                //    SuffixPrefixId = master.SuffixPrefixId,
                //    Date = master.Date,
                //    TotalAmount = (master.TotalAmount - credit),
                //    Narration = master.Narration,
                //    UserId = master.UserId,
                //    VoucherTypeId = master.VoucherTypeId,
                //    FinancialYearId = master.FinancialYearId,
                //    ExtraDate = master.ExtraDate,
                //    Extra1 = master.Extra1,
                //    Extra2 = master.Extra2
                //};
                //var editMaster = new JournalMasterSP().JournalMasterEdit(journalMasterInfo);
                response.ResponseCode = 200;

          
            return response;
        }
        [HttpPost]
        public MatResponse UpdateJournal(UpdateJournalVM input)
        {
            MatResponse response = new MatResponse();
            try
            {
                
                ExchangeRateSP spExchangeRate = new ExchangeRateSP();
                PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
                List<PartyBalanceInfo> PartyBalanceInfo = new List<PartyBalanceInfo>();
                var master = new JournalMasterSP().JournalMasterView(input.JournalMasterId);
                decimal decTotalDebit = 0;
                decimal decTotalCredit = 0;
                for (int i = 0; i < input.JournalDetailsInfo.Count; i++)
                {
                    decTotalDebit += input.JournalDetailsInfo[i].Debit;
                    decTotalCredit += input.JournalDetailsInfo[i].Credit;
                }
                if (decTotalCredit != decTotalDebit)
                {
                    response.ResponseCode = 200;
                    response.ResponseMessage = "There is a variance between total debit and credit";
                }
                else
                {
                    for (int i = 0; i < input.JournalDetailsInfo.Count; i++)
                    {
                        if (input.JournalDetailsInfo[i].JournalDetailsId == 0)
                        {
                            JournalDetailsInfo infoJournalDetails = new JournalDetailsInfo();
                            JournalDetailsSP spJournalDetails = new JournalDetailsSP();
                            //JournalDetails add
                            infoJournalDetails.JournalMasterId = input.JournalMasterId;
                            infoJournalDetails.ExtraDate = DateTime.Now;
                            infoJournalDetails.Extra1 = string.Empty;
                            infoJournalDetails.Extra2 = string.Empty;
                            infoJournalDetails.ChequeDate = DateTime.Now;
                            infoJournalDetails.Memo = input.JournalDetailsInfo[i].Memo;

                            decimal decLedgerId = 0;
                            decimal decDebit = 0;
                            decimal decCredit = 0;
                            //int inRowCount = dgvJournalVoucher.RowCount;
                            //for (int inI = 0; inI < inRowCount - 1; inI++)
                            //for (int i = 0; i < input.JournalDetailsInfo.Count; i++)
                            //{
                            infoJournalDetails.LedgerId = input.JournalDetailsInfo[i].LedgerId;
                            decLedgerId = infoJournalDetails.LedgerId;
                            //infoJournalDetails.Memo = input.JournalDetailsInfo[i].Memo;
                            infoJournalDetails.ProjectId = input.JournalDetailsInfo[i].ProjectId;
                            infoJournalDetails.CategoryId = input.JournalDetailsInfo[i].CategoryId;
                            infoJournalDetails.ExchangeRateId = 1;

                            //--------Currency conversion--------------//
                            decimal decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(1);
                            //decimal decAmount = Convert.ToDecimal(input.JournalDetailsInfo[i] dgvJournalVoucher.Rows[inI].Cells["dgvtxtAmount"].Value.ToString());
                            //decConvertRate = decAmount * decSelectedCurrencyRate;
                            //===========================================//
                            if (input.JournalDetailsInfo[i].Debit > 0)
                            {
                                infoJournalDetails.Debit = input.JournalDetailsInfo[i].Debit;
                                infoJournalDetails.Credit = 0;
                                decDebit = input.JournalDetailsInfo[i].Debit * decSelectedCurrencyRate;
                                decCredit = infoJournalDetails.Credit;
                            }
                            else if (input.JournalDetailsInfo[i].Credit > 0)
                            {
                                infoJournalDetails.Credit = input.JournalDetailsInfo[i].Credit;
                                infoJournalDetails.Debit = 0;
                                decDebit = input.JournalDetailsInfo[i].Credit;
                                decCredit = input.JournalDetailsInfo[i].Credit * decSelectedCurrencyRate;
                            }

                            infoJournalDetails.ExchangeRateId = 1;
                            infoJournalDetails.ChequeNo = input.JournalDetailsInfo[i].ChequeNo;
                            infoJournalDetails.ChequeDate = DateTime.Now;
                            //infoJournalDetails.ChequeDate = (input.JournalDetailsInfo[i].ChequeDate==null) ? DateTime.Now : input.JournalDetailsInfo[i].ChequeDate;

                            decimal decJournalDetailsId = spJournalDetails.JournalDetailsAdd(infoJournalDetails);
                            int inj = 0;
                            PartyBalanceAddOrEdit(inj, PartyBalanceInfo, master);
                            LedgerPosting(decLedgerId, decCredit, decDebit, decJournalDetailsId, input.JournalDetailsInfo[i], master);
                            response.ResponseCode = 200;
                            response.ResponseMessage = "Journal Updated Successfully";
                            //end of JournalDetails add
                        }
                        else
                        {
                            JournalDetailsInfo infoJournalDetails = new JournalDetailsInfo();
                            JournalDetailsSP spJournalDetails = new JournalDetailsSP();
                            //JournalDetails edit
                            infoJournalDetails.JournalMasterId = input.JournalMasterId;
                            infoJournalDetails.JournalDetailsId = input.JournalDetailsInfo[i].JournalDetailsId;
                            infoJournalDetails.ExtraDate = DateTime.Now;
                            infoJournalDetails.Extra1 = string.Empty;
                            infoJournalDetails.Extra2 = string.Empty;
                            infoJournalDetails.ChequeDate = DateTime.Now;
                            infoJournalDetails.ChequeNo = input.JournalDetailsInfo[i].ChequeNo;
                            infoJournalDetails.Memo = input.JournalDetailsInfo[i].Memo;

                            decimal decLedgerId = 0;
                            decimal decDebit = 0;
                            decimal decCredit = 0;
                            //int inRowCount = dgvJournalVoucher.RowCount;
                            //for (int inI = 0; inI < inRowCount - 1; inI++)
                            //for (int i = 0; i < input.JournalDetailsInfo.Count; i++)
                            //{
                            infoJournalDetails.LedgerId = input.JournalDetailsInfo[i].LedgerId;
                            decLedgerId = infoJournalDetails.LedgerId;
                            //infoJournalDetails.Memo = input.JournalDetailsInfo[i].Memo;
                            infoJournalDetails.ProjectId = input.JournalDetailsInfo[i].ProjectId;
                            infoJournalDetails.CategoryId = input.JournalDetailsInfo[i].CategoryId;
                            infoJournalDetails.ExchangeRateId = 1;

                            //--------Currency conversion--------------//
                            decimal decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(1);
                            //decimal decAmount = Convert.ToDecimal(input.JournalDetailsInfo[i] dgvJournalVoucher.Rows[inI].Cells["dgvtxtAmount"].Value.ToString());
                            //decConvertRate = decAmount * decSelectedCurrencyRate;
                            //===========================================//
                            if (input.JournalDetailsInfo[i].Debit > 0)
                            {
                                infoJournalDetails.Debit = input.JournalDetailsInfo[i].Debit;
                                infoJournalDetails.Credit = 0;
                                decDebit = input.JournalDetailsInfo[i].Debit * decSelectedCurrencyRate;
                                decCredit = infoJournalDetails.Credit;
                            }
                            else if (input.JournalDetailsInfo[i].Credit > 0)
                            {
                                infoJournalDetails.Credit = input.JournalDetailsInfo[i].Credit;
                                infoJournalDetails.Debit = 0;
                                decDebit = input.JournalDetailsInfo[i].Credit;
                                decCredit = input.JournalDetailsInfo[i].Credit * decSelectedCurrencyRate;
                            }

                            decimal decJournalDetailsId = input.JournalDetailsInfo[i].JournalDetailsId;
                            infoJournalDetails.ExchangeRateId = 1;
                            infoJournalDetails.ChequeNo = input.JournalDetailsInfo[i].ChequeNo;
                            infoJournalDetails.ChequeDate = DateTime.Now;
                            //infoJournalDetails.ChequeDate = (input.JournalDetailsInfo[i].ChequeDate==null) ? DateTime.Now : input.JournalDetailsInfo[i].ChequeDate;

                            spJournalDetails.JournalDetailsEdit(infoJournalDetails);
                            PartyBalanceAddOrEdit(i, PartyBalanceInfo, master);
                            LedgerPostingEdit(decLedgerId, decCredit, decDebit, decJournalDetailsId, input.JournalDetailsInfo[i], master);
                            response.ResponseCode = 200;
                            response.ResponseMessage = "Journal Updated Successfully";
                        }
                    }
                    JournalMasterInfo journalMasterInfo = new JournalMasterInfo
                    {
                        JournalMasterId = master.JournalMasterId,
                        VoucherNo = master.VoucherNo,
                        InvoiceNo = master.InvoiceNo,
                        SuffixPrefixId = master.SuffixPrefixId,
                        Date = master.Date,
                        TotalAmount = decTotalCredit,
                        Narration = master.Narration,
                        UserId = master.UserId,
                        VoucherTypeId = master.VoucherTypeId,
                        FinancialYearId = master.FinancialYearId,
                        ExtraDate = master.ExtraDate,
                        Extra1 = master.Extra1,
                        Extra2 = master.Extra2
                    };
                    var editMaster = new JournalMasterSP().JournalMasterEdit(journalMasterInfo);
                }

                return response;
            }
            catch(Exception e)
            {
                return response;
            }
        }
        [HttpPost]
        public MatResponse EditJournal(SaveJournalVM input)
        {
            MatResponse response = new MatResponse();
            try
            {

            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Sorry, Something went wrong";
            }
            return response;
        }

        public class JournalViewVM
        {
            public string JournalDate { get; set; }
            public decimal JournalMasterId { get; set; }
            public string JournalNo { get; set; }
            public decimal TotalAmount { get; set; }
            public string UserFullName { get; set; }
            public decimal UserId { get; set; }
            public List<CustomJournalDetailsVM> Details { get; set; }
        }

        public class CustomJournalDetailsVM
        {
            public DateTime ChequeDate { get; set; }
            public string ChequeNo { get; set; }
            public decimal Credit { get; set; }
            public decimal Debit { get; set; }
            public decimal JournalDetails { get; set; }
            public decimal JournalMaster { get; set; }
            public decimal LedgerId { get; set; }

            public decimal JournalDetailsId { get; set; }
            
            public string LedgerName { get; set; }
            public string Memo { get; set; }
            public string ProjectId { get; set; }
            public string CategoryId { get; set; }
        }
    }

    public class PartyBalanceInvoicesVM
    {
        public decimal LedgerId { get; set; }
        public string DebitOrCredit { get; set; }
        public decimal VoucherTypeId { get; set; }
        public string voucherNo { get; set; }
    }

    public class SaveJournalVM
    {
        public JournalMasterInfo JournalMasterInfo { get; set; }
        public List<JournalDetailsInfo> JournalDetailsInfo { get; set; }
        public List<PartyBalanceInfo> PartyBalanceInfo { get; set; }
        public List<PartyBalanceInfo> JournalPartyBalanceInfo { get; set; }
    }
    public class UpdateJournalVM
    {
        public int JournalMasterId { get; set; }
        public List<JournalDetailsInfo> JournalDetailsInfo { get; set; }
    }


}
