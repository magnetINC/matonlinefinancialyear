﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials.DAL;
using MATFinancials.Classes.HelperClasses;
using MatApi.Models.Inventory.Report;
using System.Dynamic;

namespace MatApi.Controllers.Inventory.Report
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StockDetailsController : ApiController
    {
        BatchSP batchSp;
        public StockDetailsController()
        {
            batchSp = new BatchSP();
        }
        string calculationMethod = string.Empty;
        List<string> AllItems = new List<string> { };
        List<decimal> AllStores = new List<decimal> { };
        ProductSP spproduct = new ProductSP();
        List<StockDetailsReportViewModel> ReportViewList = new List<StockDetailsReportViewModel> { };
        
        [HttpPost]
        public HttpResponseMessage GetStockDetails(StockDetailsModel obj)
        {
            var SerchObject = obj;
            if (obj.FromDate== "default" || obj.ToDate == "default") { 
            var DefaultObj = new StockDetailsModel()
            {
                Store = 0,
                Batch = 0,
                ProductId = 0,
                ProductCode = "",
                FromDate = "01/01/2017",
                ToDate = "12/31/2017"

            };
                SerchObject = DefaultObj;
            }
            GodownFill();
            ProductFill();
            BatchFill();

            dynamic resp = ReportData(SerchObject);
            return Request.CreateResponse(HttpStatusCode.OK, (object)resp);
        }
       
        public HttpResponseMessage GetAllProducts()
        {
            ProductSP spproduct = new ProductSP();
            var products = spproduct.ProductViewAll();
            return Request.CreateResponse(HttpStatusCode.OK, products);
        }
        public HttpResponseMessage GetAllStores()
        {
            GodownSP spGodown = new GodownSP();
            var stores = spGodown.GodownViewAll();
            return Request.CreateResponse(HttpStatusCode.OK, stores);
        }

        public HttpResponseMessage GetAllBatches()
        {
            BatchSP spBatch = new BatchSP();
            var batches = spBatch.BatchViewAllDistinct();
            return Request.CreateResponse(HttpStatusCode.OK, batches);
        }

        public void ProductFill()
        {
            try
            {
                ProductSP spproduct = new ProductSP();
                DataTable dtbl = new DataTable();
                dtbl = spproduct.ProductViewAll();
                for (int i = 0; i < dtbl.Rows.Count; i++)
                {
                    AllItems.Add(dtbl.Rows[i]["productCode"].ToString());
                }
            }
            catch (Exception ex)
            {
            }
        }
        public void GodownFill()
        {
            try
            {
                GodownSP spGodown = new GodownSP();
                DataTable dtbl = new DataTable();
                dtbl = spGodown.GodownViewAll();
                for (int i = 0; i < dtbl.Rows.Count; i++)
                {
                    AllStores.Add(Convert.ToDecimal(dtbl.Rows[i]["godownId"]));
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD5:" + ex.Message;
            }
        }
        public void BatchFill()
        {
            try
            {
                BatchSP spBatch = new BatchSP();
                DataTable dtbl = new DataTable();
                dtbl = spBatch.BatchViewAllDistinct();
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD13" + ex.Message;
            }
        }

        dynamic response = new ExpandoObject();
        bool isFormLoad = false, isOpeningRate = false;
        

        public List<StockDetailsReportViewModel> ReportData(StockDetailsModel obj)
        {
            try
            {
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }
                DBMatConnection conn = new DBMatConnection();
                GeneralQueries methodForStockValue = new GeneralQueries();
                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", Convert.ToDateTime("2017-31-12 12:00:00"));
                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", Convert.ToDateTime("2017-01-01 12:00:00"));
                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;
                DateTime lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    lastFinYearEnd = Convert.ToDateTime(obj.FromDate).AddDays(-1);
                }
                decimal currentProductId = 0, prevProductId = 0, prevStoreId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0;
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal stockValuePerItemPerStore = 0, totalStockValue = 0;
                decimal inwardQtyPerStore = 0, outwardQtyPerStore = 0, totalQtyPerStore = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "", productName = "", storeName = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };
                DataTable dtbl = new DataTable();
                DataTable dtblItem = new DataTable();
                DataTable dtblStore = new DataTable();
                DataTable dtblOpeningStocks = new DataTable();
                DataTable dtblOpeningStockPerItemPerStore = new DataTable();
                StockPostingSP spstock = new StockPostingSP();

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                objValidation.DateValidationFunction(obj.ToDate);
                if (obj.ToDate == string.Empty)
                    obj.ToDate = PublicVariables._dtToDate.ToString("dd-MMM-yyyy");
                objValidation.DateValidationFunction(obj.FromDate);
                if (obj.FromDate == string.Empty)
                    obj.FromDate = PublicVariables._dtFromDate.ToString("dd-MMM-yyyy");
                decimal productId = 0, storeId = 0;
                string produceCode = "", batch = "All", referenceNo = "";
                if (obj.ProductId > 0)
                {
                    productId = Convert.ToDecimal(obj.ProductId);
                }
                if (obj.Store > 0)
                {
                    storeId = Convert.ToDecimal(obj.Store);
                }
                if (obj.Batch > 0)
                {
                    batch = obj.Batch.ToString();
                }
                if (obj.ProductCode != string.Empty)
                {
                    produceCode = obj.ProductCode;
                }
                //if (txtRefNo.Text.ToString().Trim() != string.Empty)
                // {
                referenceNo = "";// txtRefNo.Text.ToString();
               // }
                //dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text), "", "");
                //dtblOpeningStocks = spstock.StockReportDetailsGridFill(0, 0, "All", PublicVariables._dtFromDate, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), "", "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, Convert.ToDateTime(obj.FromDate), Convert.ToDateTime(obj.ToDate), produceCode, referenceNo);
                dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime(obj.FromDate).AddDays(-1), produceCode, referenceNo);

                if (AllItems.Any())
                {
                    //.Rows.Clear();
                    foreach (var item in AllItems)
                    {
                        // ======================================== for each item begins here ====================================== //
                        dtblItem = new DataTable();
                        if (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item).Any())
                        {
                            dtblItem = (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item)).CopyToDataTable();
                            productName = (dtbl.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(m => m.Field<string>("productName")).FirstOrDefault().ToString());
                        }
                        if (dtblItem != null && dtblItem.Rows.Count > 0)
                        {
                            if (AllStores.Any())
                            {
                                foreach (var store in AllStores)
                                {                       
                                    // =================================== for that item, for each store begins here ============================== //
                                    qtyBal = 0; stockValue = 0;
                                    stockValuePerItemPerStore = 0; //dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    dtblStore = new DataTable();

                                    // ************************ insert opening stock per item per store before other transactions ******************** //
                                    dtblOpeningStockPerItemPerStore = new DataTable();
                                    if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                    {
                                        dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store)).CopyToDataTable();
                                        if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                        {
                                            computedAverageRate = rate2;
                                            var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, item, store);
                                            dcOpeningStockForRollOver = itemParams.Item1;
                                            openingQuantity = itemParams.Item2;
                                            openingQuantityIn = openingQuantity; // itemParams.Item3;
                                            openingQuantityOut = 0; // itemParams.Item4;
                                            //qtyBal += openingQuantity;
                                            qtyBal = openingQuantity;
                                            if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                            {
                                                computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                                stockValue = qtyBal * computedAverageRate;
                                            }

                                            //++++++++++++++++++++++++++dgvStockDetailsReport.Rows.Add();
                                            StockDetailsReportViewModel storeViewObj = new StockDetailsReportViewModel();
                                            //dgvStockDetailsReport.Rows[i].DefaultCellStyle.BackColor = Color.GreenYellow;
                                            storeViewObj.ProductId = item;
                                            storeViewObj.ProductCode = item;
                                            storeViewObj.ProductName = productName;
                                            storeViewObj.Store = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(); ;
                                            storeViewObj.QtyIn = openingQuantityIn.ToString("N2");
                                            storeViewObj.QtyOut = openingQuantityOut.ToString("N2");
                                            storeViewObj.Rate = "";
                                            storeViewObj.AvgCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                            storeViewObj.QtyBal = openingQuantity.ToString("N2");
                                            storeViewObj.StockVal = dcOpeningStockForRollOver.ToString("N2");
                                            storeViewObj.VoucherTypeName = "Opening Stock ";
                                            totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                            totalQtyPerStore += openingQuantity;
                                            i++;
                                            ReportViewList.Add(storeViewObj);
                                        }
                                    }
                                    // ************************ insert opening stock per item per store before other transactions end ******************** //

                                    if (dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).Any())
                                    {
                                        dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).CopyToDataTable();
                                    }
                                    if (dtblStore != null && dtblStore.Rows.Count > 0)
                                    {
                                        storeName = (dtblStore.AsEnumerable().Where(m => m.Field<decimal>("godownId") == store).Select(m => m.Field<string>("godownName")).FirstOrDefault().ToString());
                                        foreach (DataRow dr in dtblStore.Rows)
                                        {
                                            // opening stock has been factored in thinking there was no other transaction, now other transactions factored it in again, so remove it
                                            totalStockValue -= Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            //dgvStockDetailsReport.Rows[i-1].DefaultCellStyle.BackColor = Color.White;
                                            //dgvStockDetailsReport.Rows[i-1].Cells["rate"].Value = "";
                                            dcOpeningStockForRollOver = 0;  // reset the opening stock because of the loop

                                            currentProductId = Convert.ToDecimal(dr["productId"]);
                                            productCode = dr["productCode"].ToString();
                                            string voucherType = "", refNo = "";
                                            inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                                            decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                                            string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                                            if (!isOpeningRate)
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }
                                            //if (purchaseRate == 0)
                                            //{
                                            //    purchaseRate = rate2;
                                            //}
                                            if (prevStoreId != store)
                                            {
                                                previousRunningAverage = purchaseRate;
                                            }
                                            if (previousRunningAverage != 0 && currentProductId == prevProductId)
                                            {
                                                purchaseRate = previousRunningAverage;
                                            }
                                            refNo = dr["refNo"].ToString();
                                            if (againstVoucherType != "NA")
                                            {
                                                refNo = dr["againstVoucherNo"].ToString();
                                                isAgainstVoucher = true;
                                                againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                                                string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                                                string typeOfVoucher = conn.getSingleValue(queryString);
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                            }
                                            voucherType = dr["typeOfVoucher"].ToString();
                                            if (voucherType == "Stock Journal")
                                            {
                                                //voucherType = "Stock Transfer";
                                                string queryString = string.Format(" SELECT extra1 FROM tbl_StockJournalMaster WHERE voucherNo = {0} ", dr["refNo"]);
                                                voucherType = conn.getSingleValue(queryString);
                                            }
                                            if (outwardQty2 > 0)
                                            {
                                                if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate;
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                                                {
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue -= outwardQty2 * rate2;
                                                }
                                                else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return")
                                                    || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;// 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }

                                                else if (outwardQty2 > 0 && voucherType == "Sales Order")
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else
                                                {
                                                    if (qtyBal != 0)
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                            }
                                            else if (voucherType == "Sales Return" && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType == "Rejection In" && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;

                                                }
                                                else
                                                {
                                                    computedAverageRate = previousRunningAverage;// (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType == "Delivery Note")
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                            }
                                            else
                                            {
                                                // hndles other transactions such as purchase, opening balance, etc
                                                qtyBal += inwardQty2 - outwardQty2;
                                                stockValue += (inwardQty2 * rate2);
                                            }

                                            // ------------------------------------------------------ //
                                            StockDetailsReportViewModel storeViewObj1 = new StockDetailsReportViewModel();
                                            //++++++++++++++++dgvStockDetailsReport.Rows.Add();
                                            storeViewObj1.ProductId = dr["productId"].ToString();
                                            storeViewObj1.ProductName = dr["productName"].ToString();
                                            storeViewObj1.Store = dr["godownName"].ToString();
                                            storeViewObj1.QtyIn = Convert.ToDecimal(dr["inwardQty"]).ToString("N2");
                                            storeViewObj1.QtyOut = (" -") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2");
                                            storeViewObj1.VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType;
                                            isAgainstVoucher = false;
                                            storeViewObj1.Rate = Convert.ToDecimal(dr["rate"]).ToString("N2");

                                            if (stockValue != 0 && qtyBal != 0)
                                            {
                                                storeViewObj1.AvgCost = (stockValue / qtyBal).ToString("N2");
                                            }
                                            else
                                            {
                                                storeViewObj1.AvgCost = computedAverageRate.ToString("N2");
                                            }
                                            storeViewObj1.Date = Convert.ToDateTime(dr["date"]).ToShortDateString();
                                            storeViewObj1.QtyBal = qtyBal.ToString("N2");
                                            storeViewObj1.StockVal = stockValue.ToString("N2");
                                            storeViewObj1.Batch = dr["Batch"].ToString();
                                            storeViewObj1.ProductCode = dr["productCode"].ToString();
                                            storeViewObj1.RefNo = refNo;
                                            stockValuePerItemPerStore = 500;//++++++++++++++++Convert.ToDecimal(ReportViewList[i].StockVal);
                                            prevProductId = currentProductId;
                                            previousRunningAverage = 400;//Convert.ToDecimal(ReportViewList[i].AvgCost);
                                            inwardQtyPerStore += Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQtyPerStore += Convert.ToDecimal(dr["outwardQty"]);
                                            i++;
                                            ReportViewList.Add(storeViewObj1);
                                        }
                                        // ===================== for every store for that item, put the summary =================== //
                                        //++++++++++++++++++++++dgvStockDetailsReport.Rows.Add();
                                        StockDetailsReportViewModel storeViewObj2 = new StockDetailsReportViewModel();
                                        storeViewObj2.SubTotalBackColor = "Blue";
                                        storeViewObj2.Rate = "";
                                        storeViewObj2.ProductId = "";
                                        storeViewObj2.ProductName = productName;
                                        storeViewObj2.Store = storeName;
                                        storeViewObj2.QtyIn = (inwardQtyPerStore + openingQuantityIn).ToString("N2");
                                        storeViewObj2.QtyOut = ("-") + (outwardQtyPerStore + openingQuantityOut).ToString("N2");
                                        storeViewObj2.VoucherTypeName = "Sub total: ";
                                        storeViewObj2.Date = "";
                                        storeViewObj2.QtyBal = (inwardQtyPerStore - outwardQtyPerStore + openingQuantity).ToString("N2");
                                        storeViewObj2.StockVal = stockValuePerItemPerStore.ToString("N2");
                                        storeViewObj2.Batch = "";
                                        storeViewObj2.ProductCode = item;
                                        storeViewObj2.RefNo = "";
                                        totalStockValue += Convert.ToDecimal(stockValuePerItemPerStore.ToString("N2"));
                                        totalQtyPerStore += inwardQtyPerStore - outwardQtyPerStore + openingQuantity;
                                        i++;
                                        openingQuantity = 0;
                                        inwardQtyPerStore = 0;
                                        outwardQtyPerStore = 0;
                                        qtyBal = 0;
                                        stockValue = 0;
                                        stockValuePerItemPerStore = 0;
                                        //++++++++++++++++++++++++++dgvStockDetailsReport.Rows.Add();==================space
                                        ReportViewList.Add(storeViewObj2);
                                        i++;
                                    }
                                    dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    prevStoreId = store;
                                }
                            }
                        }
                        // *** if no transaction for that item for all stores for that period add the opening stock for all stores per store *** //
                        else
                        {
                            foreach (var store in AllStores)
                            {
                                stockValuePerItemPerStore = 0;
                                dtblStore = new DataTable();

                                // ************************ insert opening stock per item per store ******************** //
                                dtblOpeningStockPerItemPerStore = new DataTable();
                                if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                {
                                    dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item)).CopyToDataTable();
                                    if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                    {
                                        computedAverageRate = rate2;
                                        var itemParams = methodForStockValue.currentStockValue(lastFinYearStart, lastFinYearEnd, true, item, store);
                                        dcOpeningStockForRollOver = itemParams.Item1;
                                        openingQuantity = itemParams.Item2;
                                        openingQuantityIn = openingQuantity; // itemParams.Item3;
                                        openingQuantityOut = 0; // itemParams.Item4;
                                        qtyBal += openingQuantity;
                                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                        {
                                            //computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                            //stockValue = openingQuantity * computedAverageRate;
                                        }

                                        //+++++++++++++++++++++++++dgvStockDetailsReport.Rows.Add();
                                        StockDetailsReportViewModel storeViewObj3 = new StockDetailsReportViewModel();
                                        //dgvStockDetailsReport.Rows[i].DefaultCellStyle.BackColor = Color.GreenYellow;
                                        storeViewObj3.ProductId = item;
                                        storeViewObj3.ProductCode = item;
                                        storeViewObj3.ProductName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            Where(l => l.Field<string>("productCode") == item).Select(l => l.Field<string>("productName")).FirstOrDefault()).ToString();
                                        storeViewObj3.Store = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            Where(l => l.Field<string>("productCode") == item).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString();
                                        storeViewObj3.QtyIn = openingQuantityIn.ToString("N2");
                                        storeViewObj3.QtyOut = openingQuantityOut.ToString("N2");
                                        storeViewObj3.Rate = "";
                                        storeViewObj3.AvgCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                        storeViewObj3.QtyBal = openingQuantity.ToString("N2");
                                        storeViewObj3.StockVal = dcOpeningStockForRollOver.ToString("N2");
                                        storeViewObj3.VoucherTypeName = "Opening Stock ";
                                        totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                        dcOpeningStockForRollOver = 0;
                                        totalQtyPerStore += openingQuantity;
                                        openingQuantityIn = 0;
                                        openingQuantityOut = 0;
                                        openingQuantity = 0;
                                        i++;
                                        ReportViewList.Add(storeViewObj3);
                                    }
                                }
                            }
                        }
                    }
                }

                // ************************ Calculate Total **************************** //
                //dgvStockDetailsReport.Rows.Add();
                // dgvStockDetailsReport.Rows.Add();
                StockDetailsReportViewModel storeViewObj4 = new StockDetailsReportViewModel();
                //storeViewObj4.Rows.Add();
                storeViewObj4.GrandTotalColor = "DarkBlue";
                storeViewObj4.QtyBal = totalQtyPerStore.ToString("N2");
                if (totalQtyPerStore != 0)
                {
                    storeViewObj4.AvgCost = (totalStockValue / totalQtyPerStore).ToString("N2");
                }
                storeViewObj4.StockVal = totalStockValue.ToString("N2");
                storeViewObj4.VoucherTypeName = "Total Stock Value:";
                //storeViewObj4.Rows.Add();

                ReportViewList.Add(storeViewObj4);
            }

            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }
            return ReportViewList;
        }

    }

}
