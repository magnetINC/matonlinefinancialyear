using MatApi.Models.Inventory;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PhysicalStockController : ApiController
    {
        decimal decPhysicalStockVoucherTypeId = 25;
        decimal decPhysicalStockMasterIdentity = 0;
        decimal decConversionId = 0;

        [HttpPost]
        public bool SavePhysicalStock(PhysicalStockVM input)
        {
            try
            {
                ProductInfo infoProduct = new ProductInfo();
                ProductSP spProduct = new ProductSP();
                PhysicalStockMasterInfo infoPhysicalStockMaster = new PhysicalStockMasterInfo();
                PhysicalStockDetailsSP spPhysicalStockDetails = new PhysicalStockDetailsSP();
                PhysicalStockMasterSP spPhysicalStockMaster = new PhysicalStockMasterSP();

                decimal voucherNo = VoucherNumberGeneration();
                input.StockMaster.VoucherNo = voucherNo.ToString();
                input.StockMaster.InvoiceNo = voucherNo.ToString();

                //infoPhysicalStockMaster.VoucherNo = input.StockMaster.VoucherNo;
                infoPhysicalStockMaster.Date = Convert.ToDateTime(input.StockMaster.Date);
                infoPhysicalStockMaster.Narration = input.StockMaster.Narration;
                string s = input.TotalAmount.ToString();
                infoPhysicalStockMaster.TotalAmount = input.TotalAmount;
                //if (isAutomatic)
                //{
                //    infoPhysicalStockMaster.SuffixPrefixId = decPhysicalStockSuffixPrefixId;
                //    infoPhysicalStockMaster.VoucherNo = strVoucherNo;
                //    infoPhysicalStockMaster.InvoiceNo = txtVoucherNo.Text;
                //}
                //else
                //{
                infoPhysicalStockMaster.SuffixPrefixId = 0;
                infoPhysicalStockMaster.VoucherNo = input.StockMaster.VoucherNo;
                infoPhysicalStockMaster.InvoiceNo = input.StockMaster.InvoiceNo;
                //}
                infoPhysicalStockMaster.VoucherTypeId = decPhysicalStockVoucherTypeId;
                infoPhysicalStockMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoPhysicalStockMaster.Extra1 = input.StockMaster.Extra1;
                infoPhysicalStockMaster.Extra2 = string.Empty;
                decPhysicalStockMasterIdentity = Convert.ToDecimal(spPhysicalStockMaster.PhysicalStockMasterAdd(infoPhysicalStockMaster));

                int successfulSavedDetailsCount = 0;
                PhysicalStockDetailsInfo infoPhysicalStockDetails = new PhysicalStockDetailsInfo();
                foreach (var detail in input.StockDetails)
                {
                    var prod = new ProductSP().ProductView(detail.ProductId);
                    var uc = new UnitConvertionSP().UnitconversionIdViewByUnitIdAndProductId(prod.UnitId, detail.ProductId);
                    var batchId = new BatchSP().BatchIdViewByProductId(detail.ProductId);
                    infoPhysicalStockDetails.PhysicalStockMasterId = decPhysicalStockMasterIdentity;
                    infoPhysicalStockDetails.ProductId = detail.ProductId;
                    infoPhysicalStockDetails.GodownId = detail.GodownId;
                    infoPhysicalStockDetails.RackId = 10009/*detail.RackId*/;
                    infoPhysicalStockDetails.BatchId = batchId;
                    infoPhysicalStockDetails.Qty = detail.Qty;
                    infoPhysicalStockDetails.UnitId = prod.UnitId/*detail.UnitId*/;
                    infoPhysicalStockDetails.UnitConversionId = uc;
                    infoPhysicalStockDetails.Rate = detail.Rate;
                    infoPhysicalStockDetails.Amount = detail.Amount;
                    infoPhysicalStockDetails.Slno = detail.Slno;
                    infoPhysicalStockDetails.Extra1 = string.Empty;
                    infoPhysicalStockDetails.Extra2 = string.Empty;
                    decimal chk1 = spPhysicalStockDetails.PhysicalStockDetailsAdd(infoPhysicalStockDetails);
                    updateDetailsPendingTable(detail.PhysicalStockDetailsId);
                    decimal decPId = infoPhysicalStockDetails.ProductId;
                    string strVoucher = infoPhysicalStockMaster.VoucherNo;

                    var chk2 = AddtoStockPosting(decPId, strVoucher, infoPhysicalStockMaster.Date, infoPhysicalStockDetails);
                    if (chk1 > 0 && chk2 > 0)
                    {
                        successfulSavedDetailsCount = successfulSavedDetailsCount + 1;
                    }
                }
                if (successfulSavedDetailsCount == input.StockDetails.Count)   //means number of stockposting and stock details saved
                {                                                           //equals number of details items posted
                    return true;
                }
                //if (dgvPhysicalStock.RowCount > 1)
                //{
                //    if (cbxPrint.Checked)
                //    {
                //        if (spSettings.SettingsStatusCheck("Printer") == "Dot Matrix")
                //        {
                //            //PrintForDotMatrix(decPhysicalStockMasterIdentity);
                //        }
                //        else
                //        {
                //            //Print(decPhysicalStockMasterIdentity);
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "PS27:" + ex.Message;
            }
            return false;
        }

        [HttpPost]
        public bool SavePhysicalStockPending(PhysicalStockVM input)
        {
            bool isSaved = false;
            try
            {
                ProductInfo infoProduct = new ProductInfo();
                ProductSP spProduct = new ProductSP();
                PhysicalStockMasterInfo infoPhysicalStockMaster = new PhysicalStockMasterInfo();
                PhysicalStockDetailsSP spPhysicalStockDetails = new PhysicalStockDetailsSP();
                PhysicalStockMasterSP spPhysicalStockMaster = new PhysicalStockMasterSP();

                decimal voucherNo = VoucherNumberGeneration();
                input.StockMaster.VoucherNo = voucherNo.ToString();
                input.StockMaster.InvoiceNo = voucherNo.ToString();

                infoPhysicalStockMaster.VoucherNo = input.StockMaster.VoucherNo;
                infoPhysicalStockMaster.Date = Convert.ToDateTime(input.StockMaster.Date);
                infoPhysicalStockMaster.Narration = input.StockMaster.Narration;
                string s = input.TotalAmount.ToString();
                infoPhysicalStockMaster.TotalAmount = input.TotalAmount;
                //if (isAutomatic)
                //{
                //    infoPhysicalStockMaster.SuffixPrefixId = decPhysicalStockSuffixPrefixId;
                //    infoPhysicalStockMaster.VoucherNo = strVoucherNo;
                //    infoPhysicalStockMaster.InvoiceNo = txtVoucherNo.Text;
                //}
                //else
                //{
                infoPhysicalStockMaster.SuffixPrefixId = 0;
                infoPhysicalStockMaster.VoucherNo = input.StockMaster.VoucherNo;
                infoPhysicalStockMaster.InvoiceNo = input.StockMaster.InvoiceNo;
                //}
                infoPhysicalStockMaster.VoucherTypeId = decPhysicalStockVoucherTypeId;
                infoPhysicalStockMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoPhysicalStockMaster.Extra1 = input.StockMaster.Extra1;
                infoPhysicalStockMaster.Extra2 = string.Empty;
                decPhysicalStockMasterIdentity = Convert.ToDecimal(spPhysicalStockMaster.PhysicalStockMasterPendingAdd(infoPhysicalStockMaster));

                PhysicalStockDetailsInfo infoPhysicalStockDetails = new PhysicalStockDetailsInfo();
                foreach (var detail in input.StockDetails)
                {
                    var uc = new UnitConvertionSP().UnitconversionIdViewByUnitIdAndProductId(detail.UnitId, detail.ProductId);
                    var batchId = new BatchSP().BatchIdViewByProductId(detail.ProductId);
                    infoPhysicalStockDetails.PhysicalStockMasterId = decPhysicalStockMasterIdentity;
                    infoPhysicalStockDetails.ProductId = detail.ProductId;
                    infoPhysicalStockDetails.GodownId = detail.GodownId;
                    infoPhysicalStockDetails.RackId = 10009/*detail.RackId*/;
                    infoPhysicalStockDetails.BatchId = batchId;
                    infoPhysicalStockDetails.Qty = detail.Qty;
                    infoPhysicalStockDetails.UnitId = detail.UnitId;
                    infoPhysicalStockDetails.UnitConversionId = uc;
                    infoPhysicalStockDetails.Rate = detail.Rate;
                    infoPhysicalStockDetails.Amount = detail.Amount;
                    infoPhysicalStockDetails.Slno = detail.Slno;
                    infoPhysicalStockDetails.Extra1 = "Pending";
                    infoPhysicalStockDetails.Extra2 = string.Empty;
                    decimal chk1 = spPhysicalStockDetails.PhysicalStockDetailsAddPending(infoPhysicalStockDetails);
                    decimal decPId = infoPhysicalStockDetails.ProductId;
                    string strVoucher = infoPhysicalStockMaster.VoucherNo;

                    if (chk1 > 0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                }
                //if (successfulSavedDetailsCount == input.StockDetails.Count)   //means number of stockposting and stock details saved
                //{                                                           //equals number of details items posted
                //    return true;
                //}
                //if (dgvPhysicalStock.RowCount > 1)
                //{
                //    if (cbxPrint.Checked)
                //    {
                //        if (spSettings.SettingsStatusCheck("Printer") == "Dot Matrix")
                //        {
                //            //PrintForDotMatrix(decPhysicalStockMasterIdentity);
                //        }
                //        else
                //        {
                //            //Print(decPhysicalStockMasterIdentity);
                //        }
                //    }
                //}
                return isSaved;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "PS27:" + ex.Message;
            }
            return false;
        }

        [HttpPost]
        public DataTable PhysicalStockListing(PhysicalStockListingSearchVM input)
        {
            List<PhysicalStockListingResponseVM> response = new List<PhysicalStockListingResponseVM>();
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string query = "";
            DataTable master = null;
            if (input.Status == "Pending")
            {
                //query = string.Format("SELECT m.physicalStockMasterId,d.extraDate, d.physicalStockDetailsId, d.productId, d.qty, d.rate,"+
                //                        "d.unitId, d.unitConversionId, d.batchId, d.godownId, d.rackId, d.amount, p.productName "+ 
                //                        "FROM tbl_physicalstockmaster_pending as m, tbl_physicalstockdetails_pending as d left join tbl_Product as p on p.productId = d.productId "+
                //                         "WHERE m.date >= '{0}' AND m.date <= '{1}' ", input.FromDate,input.ToDate);
                query = string.Format("SELECT * FROM tbl_physicalstockmaster_pending as m WHERE m.date >= '{0}' AND m.date <= '{1}' AND physicalStockMasterId in (select physicalStockMasterId from tbl_PhysicalStockDetails_Pending where extra1='Pending')", input.FromDate, input.ToDate);
                master = db.customSelect(query);
            }
            else if (input.Status == "Confirmed")
            {
                //query = string.Format("SELECT m.physicalStockMasterId,d.extraDate, d.physicalStockDetailsId, d.productId, d.qty, d.rate," +
                //                        "d.unitId, d.unitConversionId, d.batchId, d.godownId, d.rackId, d.amount, p.productName "+
                //                        "FROM tbl_physicalstockmaster as m, tbl_physicalstockdetails as d left join tbl_Product as p on p.productId = d.productId "+
                //                         "WHERE m.date >= '{0}' AND m.date <= '{1}' ", input.FromDate,input.ToDate);
                query = string.Format("SELECT * FROM tbl_physicalstockmaster as m WHERE m.date >= '{0}' AND m.date <= '{1}'", input.FromDate, input.ToDate);
                master = db.customSelect(query);
            }
            return master;
        }

        [HttpGet]
        public DataTable GetStockCountDetails(decimal physicalStockMasterId, string status)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string query = "";
            DataTable master = null;
            if (status == "Pending")
            {
                query = string.Format("SELECT * FROM tbl_physicalstockdetails_pending as d join tbl_product as p on d.productid=p.productid WHERE d.physicalstockmasterid={0} AND d.extra1='Pending'", physicalStockMasterId);
                master = db.customSelect(query);
            }
            else if (status == "All_Pending")
            {
                query = string.Format("SELECT * FROM tbl_physicalstockdetails_pending as d join tbl_product as p on d.productid=p.productid WHERE d.physicalstockmasterid={0}", physicalStockMasterId);
                master = db.customSelect(query);
            }
            else if (status == "Confirmed")
            {
                query = string.Format("SELECT * FROM tbl_physicalstockdetails as d join tbl_product as p on d.productid=p.productid WHERE d.physicalstockmasterid={0}", physicalStockMasterId);
                master = db.customSelect(query);
            }
            return master;
        }

        public decimal AddtoStockPosting(decimal decProductId, string strMaster, DateTime date, PhysicalStockDetailsInfo infoPhysicalStockDetails)
        {
            try
            {
                var spStockPosting = new StockPostingSP();
                StockPostingInfo infoStockPosting = new StockPostingInfo();
                decimal decCurrentQty = infoPhysicalStockDetails.Qty;
                decimal decGId = infoPhysicalStockDetails.GodownId;
                decimal decBId = infoPhysicalStockDetails.BatchId;
                decimal decRId = 10009/*infoPhysicalStockDetails.RackId*/;
                decimal decPSMID = infoPhysicalStockDetails.PhysicalStockMasterId;
                decimal decOldStock = spStockPosting.ProductGetCurrentStock(decProductId, decGId, decBId, decRId, PublicVariables._dtFromDate, PublicVariables._dtToDate);


                // code segment to implement as to populate current system stockQty.
                MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
                DataSet ds = db.ExecuteQuery(string.Format("UPDATE tbl_PhysicalStockDetails set extra1 = '"
                        + decOldStock + "' where productId = {0} and godownId ={1} and batchId ={2} and PhysicalStockMasterId ={3}",
                        decProductId, decGId, decBId, /*decRId,*/ decPSMID));

                if (decCurrentQty >= 0)
                {
                    if (decOldStock >= 0)
                    {
                        decimal decBalance = decCurrentQty - decOldStock;
                        if (decBalance >= 0)
                        {
                            infoStockPosting.InwardQty = decBalance;
                            infoStockPosting.OutwardQty = 0;
                        }
                        else
                        {
                            infoStockPosting.InwardQty = 0;
                            infoStockPosting.OutwardQty = -decBalance;
                        }
                    }
                    else
                    {
                        infoStockPosting.InwardQty = -decOldStock + decCurrentQty;
                        infoStockPosting.OutwardQty = 0;
                    }
                }
                else
                {
                    if (decOldStock >= 0)
                    {
                        infoStockPosting.InwardQty = 0;
                        infoStockPosting.OutwardQty = -decCurrentQty + decOldStock;
                    }
                    else
                    {
                        decimal decBalance = -decCurrentQty + decOldStock;
                        if (decBalance >= 0)
                        {
                            infoStockPosting.InwardQty = 0;
                            infoStockPosting.OutwardQty = decBalance;
                        }
                        else
                        {
                            infoStockPosting.InwardQty = -decBalance;
                            infoStockPosting.OutwardQty = 0;
                        }
                    }
                }
                infoStockPosting.VoucherNo = strMaster;
                infoStockPosting.BatchId = infoPhysicalStockDetails.BatchId;
                infoStockPosting.Date = date;
                infoStockPosting.Extra1 = string.Empty;
                infoStockPosting.Extra2 = string.Empty;
                infoStockPosting.GodownId = infoPhysicalStockDetails.GodownId;
                infoStockPosting.ProductId = decProductId;
                infoStockPosting.Rate = infoPhysicalStockDetails.Rate;
                infoStockPosting.UnitId = infoPhysicalStockDetails.UnitId;
                infoStockPosting.RackId = 10009/*infoPhysicalStockDetails.RackId*/;
                infoStockPosting.AgainstVoucherTypeId = 0;
                infoStockPosting.AgainstVoucherNo = "NA";
                infoStockPosting.AgainstInvoiceNo = "NA";
                infoStockPosting.VoucherTypeId = decPhysicalStockVoucherTypeId;
                infoStockPosting.InvoiceNo = strMaster;
                var spStockPosting1 = new MatApi.Services.StockPostingSP();
                return spStockPosting1.StockPostingAdd(infoStockPosting);
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "PS21:" + ex.Message;
            }
            return 0;
        }

        public decimal VoucherNumberGeneration()
        {
            try
            {
                decimal voucherNo = 0;
                TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
                string strTableName = "PhysicalStockMaster";
                PhysicalStockMasterSP spPhysicalStockMaster = new PhysicalStockMasterSP();

                voucherNo = Convert.ToDecimal(TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(decPhysicalStockVoucherTypeId, Convert.ToDecimal(voucherNo), DateTime.Now, strTableName));
                //if (Convert.ToDecimal(voucherNo) != (spPhysicalStockMaster.PhysicalStockMasterVoucherMax(decPhysicalStockVoucherTypeId)))
                //{
                //    voucherNo = Convert.ToDecimal(spPhysicalStockMaster.PhysicalStockMasterVoucherMax(decPhysicalStockVoucherTypeId).ToString());
                //    voucherNo =Convert.ToDecimal(TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(decPhysicalStockVoucherTypeId, Convert.ToDecimal(voucherNo), DateTime.Now, strTableName));
                //    if (spPhysicalStockMaster.PhysicalStockMasterVoucherMax(decPhysicalStockVoucherTypeId) == 0)
                //    {
                //        voucherNo = 0;
                //        voucherNo = Convert.ToDecimal(TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(decPhysicalStockVoucherTypeId, Convert.ToDecimal(voucherNo), DateTime.Now, strTableName));
                //    }
                //}
                return voucherNo;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "PS18:" + ex.Message;
            }
            return 0;
        }

        private bool updateDetailsPendingTable(decimal physicalStockDetailsId)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string query = "";
            query = string.Format("UPDATE tbl_physicalstockdetails_pending SET extra1='Confirmed' WHERE physicalstockdetailsid={0}", physicalStockDetailsId);
            if (db.customUpdateQuery(query) > 0)
            {
                return true;
            }
            return false;
        }

    }

    public class PhysicalStockListingSearchVM
    {
        public string Status { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
    }

    public class PhysicalStockListingResponseVM
    {
        public PhysicalStockMasterInfo Master { get; set; }
        //public List<PhysicalStockDetailsInfo> Details { get; set; }
    }
}