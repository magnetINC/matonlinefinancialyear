﻿using MatApi.Models;
using MatApi.Models.Inventory;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;
using System.Threading.Tasks;

namespace MatApi.Controllers.Inventory
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class EditItemController : ApiController
    {
        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
    
        [HttpGet]
        public HttpResponseMessage GetLookups(int id)
        {
            dynamic response = new ExpandoObject();
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var size = new SizeSP().SizeViewAll();
            var units = new UnitSP().UnitViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks = new RackSP().RackViewAll();
            var brand = new BrandSP().BrandViewAll();
            var allTaxes = new TaxSP().TaxViewAll();
            var productGroup = new ProductGroupSP().ProductGroupViewAll();
        
            var expAccount = new AccountLedgerSP().AccountLedgerViewAll();
            decimal[] ledgers = new decimal[] { 11, 37 };
            var query = (from d in expAccount.AsEnumerable()
                             where ledgers.Contains(d.Field<decimal>("accountGroupId"))
                             select d).ToList();
            expAccount = query.CopyToDataTable();

            var salesAccounts = new SalesMasterSP().SalesInvoiceSalesAccountModeComboFill();
            var bom = new BomSP().ProduBomForEdit(id);
            var products = new ProductSP().ProductViewAll();
            var stockPosting = new StockPostingSP().StockPostingViewAll();
            var batch = new BatchSP().BatchViewAll();
            var modelNos = new ModelNoSP().ModelNoViewAll();

            response.Sizes = size;
            response.Units = units;
            response.Stores = stores;
            response.Racks = racks;
            response.Brands = brand;
            response.Taxes = allTaxes;
            response.ProductGroup = productGroup;
            response.ModelNos = modelNos;
            response.SalesAccount = salesAccounts;
            response.ExpenseAccount = expAccount;
            response.Bom = bom;
            response.Products = products;
            response.Stocks = stockPosting;
            response.Batch = batch;
            response.ModelNos = modelNos;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetLookups12(int id)
        {
            try
            {
                dynamic response = new ExpandoObject();
                TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

                var size = new SizeSP().SizeViewAll1();
                var units = new UnitSP().UnitViewAll1();
                var stores = new GodownSP().GodownViewAll1();
                var racks = new RackSP().RackViewAll1();
                var brand = new BrandSP().BrandViewAll1();
                var allTaxes = new TaxSP().TaxViewAll1();
                var productGroup = new ProductGroupSP().ProductGroupViewAll1();

                var expAccount = new AccountLedgerSP().AccountLedgerViewAll1();
                decimal[] ledgers = new decimal[] { 11, 37 };
                var query = (from d in expAccount.AsEnumerable()
                             where ledgers.Contains(Convert.ToDecimal(d.accountGroupId.ToString()))
                             select d).ToList();
                expAccount = query.ToList();

                var salesAccounts = new SalesMasterSP().SalesInvoiceSalesAccountModeComboFill1();
                var bom = new BomSP().ProduBomForEdit1(id);
                var products = new ProductSP().ProductViewAll1();
                var stockPosting = new StockPostingSP().StockPostingViewAll1();

                stockPosting = stockPosting.Where(u => u.productId == id && u.voucherTypeId == 2).ToList();

                var hh = stockPosting.Where(u => u.productId == id).FirstOrDefault();
              
                var batch = new BatchSP().BatchViewAll1();
                var modelNos = new ModelNoSP().ModelNoViewAll1();

                List<BatchInfo> batches = new List<BatchInfo>();
                try
                {
                    decimal pro = Convert.ToDecimal(id.ToString());
                    var batchss = context.tbl_Batch.Where(u => u.productId == pro).ToList();
                    if (batchss != null && batchss.Count >= 1)
                    {
                        foreach (var i in batchss)
                        {
                            var Batc = new BatchInfo
                            {
                                BatchId = i.batchId,
                                BatchNo = i.batchNo,
                                ProductId = Convert.ToInt32(i.productId.Value),
                                narration = i.narration
                            };
                            if (i.manufacturingDate.HasValue)
                            {
                                Batc.ManufacturingDate1 = i.manufacturingDate.Value.ToShortDateString();
                                Batc.ManufacturingDate = i.manufacturingDate.Value;
                            }
                            if (i.expiryDate.HasValue)
                            {
                                Batc.ExpiryDate = i.expiryDate.Value;
                                Batc.ExpiryDate1 = i.expiryDate.Value.ToShortDateString();
                            }
                            batches.Add(Batc);
                        }
                       
                    }

                }
                catch (Exception exx)
                {

                }

                response.Sizes = size.ToList().Distinct().ToList();
                response.Units = units.ToList().Distinct().ToList();
                response.Stores = stores.ToList().Distinct().ToList();
                response.Racks = racks.ToList().Distinct().ToList();
                response.Brands = brand.ToList().Distinct().ToList();
                response.Taxes = allTaxes.ToList().Distinct().ToList();
                response.ProductGroup = productGroup.ToList().Distinct().ToList();
                response.ModelNos = modelNos.ToList().Distinct().ToList();
                response.SalesAccount = salesAccounts.ToList().Distinct().ToList();
                response.ExpenseAccount = expAccount.ToList().Distinct().ToList();
                response.Bom = bom.ToList().Distinct().ToList();
                response.Products = products.ToList().Distinct().ToList();
                response.Stocks = stockPosting.ToList().Distinct().ToList();
                /*stockPosting.ToList().Distinct().ToList();*/
                response.Batch = batch.ToList().Distinct().ToList();
                response.ModelNos = modelNos.ToList().Distinct().ToList();
                response.batchs = batches.ToList().Distinct().ToList();

                return Request.CreateResponse(HttpStatusCode.OK, (object)response);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        public async Task<HttpResponseMessage> GetLooks()
        {
            try
            {
                dynamic response = new ExpandoObject();
               
                var stores = new GodownSP().GodownViewAll1();
                var racks = new RackSP().RackViewAll1();
                var batch = new BatchSP().BatchViewAll1();
              
                response.Stores = stores.ToList().Distinct().ToList();
                response.Racks = racks.ToList().Distinct().ToList();
                response.Batch = batch.ToList().Distinct().ToList();

                return Request.CreateResponse(HttpStatusCode.OK, (object)response);
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        [HttpGet]
        public HttpResponseMessage GetProductDetails(int id)
        {
            var ProductDetails = new ProductSP().ProductView(id);

            return Request.CreateResponse(HttpStatusCode.OK, (object)ProductDetails);
        }

        [HttpPost]
        public string UpdateProductDetails(ItemModel input)
        {
            try
            {
                ProductSP prodSP = new ProductSP();
                input.ProductInfo.ExtraDate = DateTime.Now;
                var res = prodSP.ProductEdit(input.ProductInfo);

                BomSP bomSP = new BomSP();
                List<BomInfo> bomInfo = new List<BomInfo>();

                if (res == true)
                {
                    foreach (var o in input.NewBoms)
                    {
                        bomInfo.Add
                            (new BomInfo
                            {
                                Extra1 = o.Extra1,
                                Extra2 = o.Extra2,
                                ExtraDate = DateTime.Now,
                                Quantity = o.Quantity,
                                RowmaterialId = o.RawMaterialId,
                                UnitId = o.UnitId,
                                ProductId = input.ProductInfo.ProductId,
                                BomId = o.BomId
                            });
                    }
                    var boms = bomSP.ProduBomForEdit(input.ProductInfo.ProductId);
                    for (int i = 0; i < boms.Rows.Count; i++)
                    {
                        var exist = bomInfo.Find(p => p.ProductId == Convert.ToDecimal(boms.Rows[i].ItemArray[1]));

                        if (exist != null)
                        {
                            bomSP.BomDelete(Convert.ToDecimal(boms.Rows[i].ItemArray[0]));
                        }
                    }
                    foreach (var o in bomInfo)
                    {

                        if (o.BomId > 0)
                        {
                            bomSP.BomAdd(o);
                        }
                        else if (o.BomId == 0)
                        {
                            o.ExtraDate = DateTime.Now;
                            bomSP.BomAdd(o);
                        }
                    }
                }
                else
                {
                    return "failed";
                }
            }
            catch (Exception ex)
            {

            }
            return "";
        }

        [HttpGet]
        public MatResponse VoucherValidating(string VoucherId)
        {
            MatResponse response = new MatResponse();

            var context = new DBModel.DBMATAccounting_MagnetEntities1();
            var IsExitVoucherSL = context.tbl_LedgerPosting.Where(u => u.voucherNo == VoucherId).Any();
            var IsExitVoucherSP = context.tbl_StockPosting.Where(u => u.voucherNo == VoucherId).Any();

            if (IsExitVoucherSL || IsExitVoucherSP)
            {
                response.ResponseCode = 900; // Voucher Checking
                response.ResponseMessage = "Voucher No is already exitng, Try again.";
                return response;
            }

            response.ResponseCode = 200; // Voucher Checking
            response.ResponseMessage = "Voucher Not exitng.";
            return response;
        }

        [HttpPost]
        public async Task<string> UpdateProductDetails1(ItemModel12 input)
        {
            try
            {

                ProductSP prodSP = new ProductSP();
                input.ProductInfo.ExtraDate = DateTime.Now;

                var IsHasOpenStock = NewAddItem.ValidateOpeningStock((int)input.ProductInfo.ProductId, input.ProductInfo.EffectiveDate);
                if (IsHasOpenStock)
                {
                    return "HasTransactions";
                }

                var res = prodSP.ProductEdit(input.ProductInfo);

                BomSP bomSP = new BomSP();
                List<BomInfo> bomInfo = new List<BomInfo>();

                //if (input != null && input.NewStores.Count > 0)
                //{
                //   // input.NewStores.Remove(input.stockDelete);
                //}

                var NewStores = new List<NewStoreModel1>();
                bool hasDeleteStock = false;
                decimal AmtDeleteStock = 0;
                if (input.stockDelete.Count > 0)
                {
                    hasDeleteStock = true;
                    AmtDeleteStock = NewAddItem.DeleteStockItem(input.ProductInfo.ProductId, input.stockDelete);     
                }

                try
                {
                    NewStores.AddRange(input.NewStores);
                }
                catch(Exception ex)
                {

                }

                if (input.ProductInfo.Isopeningstock)
                {
                    if (!input.ProductInfo.Isopeningstock)
                    {
                        if (input.NewStores == null || input.NewStores.Count < 0)
                        {
                            input.NewStores = new List<NewStoreModel1>();
                        }
                        else
                        {
                            if (input.NewStores.Count > 0)
                            {
                                input.NewStores.Clear();
                            }
                        }

                        foreach (var itm in input.StockPosting)
                        {
                            var Amoun = new NewStoreModel1();
                            Amoun.Amount = Convert.ToDecimal(itm.amount);
                            //Amoun.Batch = itm.batch;
                            //Amoun.ExpD = Convert.ToDateTime(itm.expD);
                            //Amoun.MfD = Convert.ToDateTime(itm.mfD);
                            Amoun.Quantity = Convert.ToDecimal(itm.quantity);
                            Amoun.inwardQty = Convert.ToDecimal(itm.quantity);
                            Amoun.Rack = itm.rack;
                            Amoun.RackId = Convert.ToDecimal(itm.rackId);
                            Amoun.Rate = Convert.ToDecimal(itm.rate);
                            Amoun.Store = itm.store;
                            Amoun.StoreId = Convert.ToDecimal(itm.storeId);
                            Amoun.Unit = itm.unit;
                            Amoun.UnitId = Convert.ToDecimal(itm.unitId);
                            input.NewStores.Add(Amoun);
                        }

                        NewAddItem.UpdateledgerItems(input.ProductInfo.ProductId, input.NewStores, NewStores, input.ProductInfo.EffectiveDate , input.ProductInfo.VoucherId.ToString(), 2, hasDeleteStock, AmtDeleteStock);

                        NewAddItem.BatchWithBarCode(input, input.ProductInfo.ProductId);

                        decimal stk = NewAddItem.StockPostingTableFill(input.NewStores, input.ProductInfo.ProductId, input, 2, input.ProductInfo.VoucherId.ToString(), 2);

                    }
                    else
                    {

                        if (input.NewStores == null || input.NewStores.Count < 0)
                        {
                            input.NewStores = new List<NewStoreModel1>();
                        }
                        else
                        {
                            if (input.NewStores.Count > 0)
                            {
                                input.NewStores.Clear();
                            }
                        }

                        foreach (var itm in input.StockPosting)
                        {
                            var Amoun = new NewStoreModel1();
                            Amoun.Amount = Convert.ToDecimal(itm.amount);
                            Amoun.Batch = itm.batch;
                            Amoun.Quantity = Convert.ToDecimal(itm.quantity);
                            Amoun.inwardQty = Convert.ToDecimal(itm.quantity);

                            Amoun.Rate = Convert.ToDecimal(itm.rate);
                            Amoun.Store = itm.store;
                            Amoun.StoreId = Convert.ToDecimal(itm.storeId);
                            Amoun.Unit = itm.unit;
                            Amoun.UnitId = Convert.ToDecimal(itm.unitId);

                            try
                            {
                                Amoun.ExpD = Convert.ToDateTime(itm.expD);
                            }
                            catch (Exception exx)
                            {
                                Amoun.ExpD = DateTime.UtcNow;
                            }

                            try
                            {
                                Amoun.MfD = Convert.ToDateTime(itm.mfD);
                            }
                            catch (Exception exx)
                            {
                                Amoun.MfD = DateTime.UtcNow;
                            }

                            try
                            {
                                Amoun.Rack = itm.rack;
                                Amoun.RackId = Convert.ToDecimal(itm.rackId);
                            }
                            catch (Exception exx)
                            {
                                Amoun.Rack = itm.rack;
                                Amoun.RackId = 0;
                            }

                            input.NewStores.Add(Amoun);
                        }
                       
                        NewAddItem.UpdateledgerItems(input.ProductInfo.ProductId, input.NewStores, NewStores, input.ProductInfo.EffectiveDate, input.ProductInfo.VoucherId.ToString(), 2, hasDeleteStock, AmtDeleteStock);

                       
                        NewAddItem.BatchTableWithStockAndProductBatchFill(input.NewStores, input.ProductInfo.ProductId, input, 2, input.ProductInfo.VoucherId.ToString(), 2);
                    }
                }
                else
                {
                    NewAddItem.UpdateledgerItems(input.ProductInfo.ProductId, input.NewStores, NewStores, input.ProductInfo.EffectiveDate, input.ProductInfo.VoucherId.ToString(), 2, hasDeleteStock, AmtDeleteStock);
                }

                if (res == true)
                {
                    foreach (var o in input.NewBoms)
                    {
                        bomInfo.Add
                            (new BomInfo
                            {
                                Extra1 = o.Extra1,
                                Extra2 = o.Extra2,
                                ExtraDate = DateTime.Now,
                                Quantity = o.Quantity,
                                RowmaterialId = o.RawMaterialId,
                                UnitId = o.UnitId,
                                ProductId = input.ProductInfo.ProductId,
                                BomId = o.BomId
                            });
                    }

                    var boms = bomSP.ProduBomForEdit12(input.ProductInfo.ProductId);
                    for (int i = 0; i < boms.Count; i++)
                    {
                        var exist = bomInfo.Find(p => p.ProductId == Convert.ToDecimal(boms[i].productId));

                        if (exist != null)
                        {
                            bomSP.BomDelete(Convert.ToDecimal(boms[i].bomId));
                        }
                    }

                    foreach (var o in bomInfo)
                    {

                        if (o.BomId > 0)
                        {
                            bomSP.BomAdd(o);
                        }
                        else if (o.BomId == 0)
                        {
                            o.ExtraDate = DateTime.Now;
                            bomSP.BomAdd(o);
                        }
                    }
                }
                else
                {
                    return "failed";
                }
            }
            catch (Exception ex)
            {

            }
            return "";
        }
        [HttpGet]
        public string DeleteItem(decimal id)
        {
            ProductSP ProductDetails = new ProductSP();
            var res = ProductDetails.ProductReferenceCheck(id);
            var str = "";
            if(res == false)
            {
                var delres = ProductDetails.ProductDelete(id);

                if(delres == true)
                {
                    var stockPostingProduct =
                        context.tbl_StockPosting.Where(a => a.productId == id).ToList();
                    context.tbl_StockPosting.RemoveRange(stockPostingProduct);
                    context.SaveChanges();
                    str = "Item Deleted";
                }
                else
                {
                    str = "A problem occured";
                }
            }
            else
            {
                str = "Unable to delete. Item contains reference.";
            }

            return str;
        }
    }
}
