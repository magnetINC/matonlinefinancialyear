﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials;
using System.Data;
using MatApi.Models.Inventory;
using System.Dynamic;
using System.Web.UI.WebControls;
using MatApi.DBModel;
using MATFinancials.DAL;
using Microsoft.Ajax.Utilities;
using System.Threading.Tasks;
using System.Data.Entity;

namespace MatApi.Controllers.Inventory
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StockJournalController : ApiController
    {
        decimal decVoucherTypeId = 0;
        decimal decSuffixPrefixId = 0;
        bool isAutomatic = false;
        string strVoucherNo = string.Empty;
        string TableName = "StockJournalMaster";
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        string strInvoiceNo = string.Empty;
        decimal decStockMasterId = 0;

        public DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        private bool _unknown;

        public StockJournalController()
        {
            decVoucherTypeId = 24;
            VoucherTypeSP spVoucherType = new VoucherTypeSP();
            isAutomatic = spVoucherType.CheckMethodOfVoucherNumbering(24);
            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(24, DateTime.Now);
            decSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
        }

        [HttpGet]
        public HttpResponseMessage SearchProduct(string searchBy, string filter)
        {
            SalesDetailsSP spSalesDetails = new SalesDetailsSP();
            UnitSP spUnit = new UnitSP();
            GodownSP spGodown = new GodownSP();
            BatchSP spBatch = new BatchSP();
            TaxSP spTax = new TaxSP();
            RackSP spRack = new RackSP();
            UnitConvertionSP SpUnitConvertion = new UnitConvertionSP();

            dynamic response = new ExpandoObject();

            if (searchBy == "ProductCode")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByProductCodeForSI(28, filter);
                var productId = Convert.ToDecimal(productDetails.Rows[0]["ProductId"]);
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[11].ToString()));
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                //response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productId));
            }
            else if (searchBy == "ProductName")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByProductNameForSI(28, filter);
                var productId = Convert.ToDecimal(productDetails.Rows[0]["ProductId"]);
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[11].ToString()));
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                //response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productId));
            }
            else if (searchBy == "Barcode")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByBarcodeForSI(28, filter);
                var productId = Convert.ToDecimal(productDetails.Rows[0]["ProductId"]);
                var unit = SpUnitConvertion.UnitConversionIdAndConRateViewallByProductId(productId.ToString());
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[11].ToString()));
                var batch = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productId));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Unit = unit;
                response.Stores = stores;
                response.Racks = racks;
                response.Batch = batch;
                response.Taxs = taxes;
                //response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productId));
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetLookups()
        {
            var currency = new TransactionsGeneralFill().CurrencyComboByDate(DateTime.Now);
            var finishedGoods = new ProductSP().ProductFinishedGoodsComboFill();
            //  var allProducts = new ProductSP().ProductViewAll();
            var allProducts = new ProductSP().ProductViewAll1();
            var allStores = new GodownSP().GodownViewAll();
            var allRacks = new RackSP().RackViewAll();
            var allUnits = new UnitSP().UnitViewAll();
            var allBatches = new BatchSP().BatchViewAll();
            var cashOrBanks = new TransactionsGeneralFill().BankOrCashComboFill(false);
            var accountLedgers = new AccountLedgerSP().AdditionalCostGet();

            dynamic response = new ExpandoObject();
            //response.formNoForTransfer = Convert.ToDecimal(id.ToString()) + 1;
            response.Currencies = currency;
            response.FinishedGoods = finishedGoods;

            var FromDate = PublicVariables._dtFromDate;
            var ToDate = PublicVariables._dtToDate;

            allProducts = allProducts.Where(b => b.effectiveDate >= FromDate && b.effectiveDate <= ToDate).ToList().OrderBy(o => o.productName).ToList().OrderBy(o => o.effectiveDate).ToList();

            response.AllProducts = allProducts;
            response.AllStores = allStores;
            response.AllRacks = allRacks;
            response.AllUnits = allUnits;
            response.AllBatches = allBatches;
            response.CashOrBanks = cashOrBanks;
            response.AccountLedgers = accountLedgers;
            response.users = new UserSP().UserViewAll();

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetLookupsFarmBuild()
        {

            var allProducts = new ProductSP().ProductViewAll1();

            dynamic response = new ExpandoObject();

            var FromDate = PublicVariables._dtFromDate;
            var ToDate = PublicVariables._dtToDate;

            //allProducts = allProducts.Where(b => b.effectiveDate >= FromDate && b.effectiveDate <= ToDate).ToList().OrderBy(o => o.productName).ToList().OrderBy(o => o.effectiveDate).ToList();

            response.AllProducts = allProducts;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public decimal GetFormNoForTransfer()
        {
            /**
             * FormNo For Transfer approval ledge processing
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            decimal formNo = 0;

            string id = db.getSingleValue("SELECT top 1 formNo from tbl_StockJournalMaster_Pending order by stockJournalMasterId desc");
            if (id.IsNullOrWhiteSpace())
            {
                id = "0";
            }
            if(Convert.ToDecimal(id) == 0)
            {
                formNo = 1;
            }
            else
            {
                formNo = Convert.ToDecimal(id) + 1;
            }
            
            return formNo;
            **/

            return Convert.ToDecimal(generateVoucherNo());

        }

        [HttpGet]
        public DataTable GetRawMaterialForProduct(decimal productId)
        {
            //var items =   new ProductSP().RawMaterialsFillForStockJournal(productId, quantity);
            //return items

            //var query = string.Format(@"select  
            //                                    productId,
            //                                    productCode,
            //                                    productName,
            //                                    barcode,
            //                                    CONVERT(decimal(18,2),Qty) As Qty,
            //                                    unitId,
            //                                    rackId,
            //                                    godownId,batchId,
            //                                    CONVERT(decimal(18,2),salesRate) as salesRate,
            //                                    unitconversionId
            //                                    from
            //                                    (
            //                                    SELECT     P.productCode, P.productId, P.productName,tbl_Batch.batchId, tbl_Rack.rackId, tbl_Godown.godownId, B.quantity AS Qty, 
            //                                                          tbl_Batch.barcode,P.salesRate, U.unitId, UC.unitconversionId
            //                                    FROM                  tbl_Product AS P INNER JOIN
            //                                                          tbl_Bom AS B ON B.rowmaterialId = P.productId INNER JOIN
            //                                                          tbl_Godown ON P.godownId = tbl_Godown.godownId 
            //                                           INNER JOIN tbl_Batch ON P.productId = tbl_Batch.productId 
            //                                           FULL Outer JOIN
            //                                                          tbl_Rack ON P.rackId = tbl_Rack.rackId INNER JOIN
            //                                                          tbl_Unit AS U ON P.unitId = U.unitId INNER JOIN
            //                                                          tbl_UnitConvertion AS UC ON P.productId = UC.productId AND U.unitId = UC.unitId

            //                                    WHERE     (B.productId = '{0}')) as TEMP", productId);
            var query = string.Format(@"SELECT     P.productCode, P.productId, P.productName, tbl_Rack.rackId, tbl_Godown.godownId, CONVERT(decimal(18,2),B.quantity) As Qty, 
                                                                      CONVERT(decimal(18,2),P.salesRate) as salesRate, U.unitId, UC.unitconversionId
                                                FROM                  tbl_Product AS P INNER JOIN
                                                                      tbl_Bom AS B ON B.rowmaterialId = P.productId 
																	  --INNER JOIN
                                                                      --tbl_Godown ON P.godownId = tbl_Godown.godownId 
					                                                  FULL Outer JOIN
																	  tbl_Godown ON P.godownId = tbl_Godown.godownId INNER JOIN
                                                                      tbl_Rack ON P.rackId = tbl_Rack.rackId INNER JOIN
                                                                      tbl_Unit AS U ON P.unitId = U.unitId INNER JOIN
                                                                      tbl_UnitConvertion AS UC ON P.productId = UC.productId AND U.unitId = UC.unitId
	
                                                WHERE     (B.productId = {0})", productId);
            var items = new DBMatConnection().CustomQueryToDataTable(query);
            return items
                ;
        }

        [HttpGet]
        public IHttpActionResult GetAllRawMaterial()
        {
            try
            {
                var FromDate = PublicVariables._dtFromDate;
                var ToDate = PublicVariables._dtToDate;
                //Where(b=> b.extraDate >= FromDate && b.extraDate <= ToDate)
                //.Where(b => b.effectiveDate >= FromDate && b.effectiveDate <= ToDate)
                var raw = context.tbl_Bom.ToList().Select(a => new
                {
                    Product = context.tbl_Product.ToList().Where(p => p.productId == a.rowmaterialId),
                    Raw = a
                }).ToList();

                return Ok(raw);
            }
            catch (Exception es)
            {
                return null;
            }
        }

        [HttpGet]
        public IHttpActionResult GetAllRawMaterial12()
        {
            try
            {
                var FromDate = PublicVariables._dtFromDate;
                var ToDate = PublicVariables._dtToDate;

                var raw = context.tbl_Bom.ToList().Select(a => new
                {
                    Product = context.tbl_Product.ToList().Where(p => p.productId == a.rowmaterialId),
                    Raw = a
                }).ToList();

                return Ok(raw);
            }
            catch (Exception es)
            {
                return null;
            }
        }

        public class RawMaterial
        {
            public Bom Raw { get; set; }
            public Product Product { get; set; }
        }

        public class Bom
        {
            public decimal bomId { get; set; }
            public decimal productId { get; set; }
            public decimal rowmaterialId { get; set; }
            public decimal quantity { get; set; }
            public decimal unitId { get; set; }
            public string extra1 { get; set; }
            public string extra2 { get; set; }
            public DateTime extraDate { get; set; }
        }

        public class Product
        {
            public decimal productId { get; set; }
            public string productCode { get; set; }
            public string productName { get; set; }
            public decimal groupId { get; set; }
            public decimal brandId { get; set; }
            public decimal unitId { get; set; }
            public decimal sizeId { get; set; }
            public decimal modelNoId { get; set; }
            public decimal taxId { get; set; }
            public string taxapplicableOn { get; set; }
            public decimal purchaseRate { get; set; }
            public decimal salesRate { get; set; }
            public decimal mrp { get; set; }
            public decimal minimumStock { get; set; }
            public decimal maximumStock { get; set; }
            public decimal reorderLevel { get; set; }
            public decimal godownId { get; set; }
            public decimal rackId { get; set; }
            public bool isallowBatch { get; set; }
            public bool ismultipleunit { get; set; }
            public bool isBom { get; set; }
            public bool isopeningstock { get; set; }
            public string narration { get; set; }
            public bool isActive { get; set; }
            public bool isshowRemember { get; set; }
            public string extra1 { get; set; }
            public string extra2 { get; set; }
            public System.DateTime extraDate { get; set; }
            public string partNo { get; set; }
            public string productType { get; set; }
            public decimal salesAccount { get; set; }
            public System.DateTime effectiveDate { get; set; }
            public decimal expenseAccount { get; set; }
        }

        public class BomVM
        {
            public decimal productId { get; set; }
            public dynamic productCode { get; set; }
            public decimal Qty { get; set; }
            public string productName { get; set; }
            public decimal rackId { get; set; }
            public decimal godownId { get; set; }
            public int QtyAvailable { get; set; }
            public decimal salesRate { get; set; }
            public decimal unitId { get; set; }
            public decimal unitConversionId { get; set; }

        }

        [HttpGet]
        public List<BomVM> GetRawMaterialForProductV112(decimal productId, int StoreId)
        {
            try
            {
                var query = string.Format(@"SELECT     P.productCode, P.productId, P.productName, tbl_Rack.rackId, tbl_Godown.godownId, CONVERT(decimal(18,2),B.quantity) As Qty, 
                                                                      CONVERT(decimal(18,2),P.salesRate) as salesRate, U.unitId, UC.unitconversionId
                                                FROM                  tbl_Product AS P INNER JOIN
                                                                      tbl_Bom AS B ON B.rowmaterialId = P.productId --INNER JOIN
                                                                      --tbl_Godown ON P.godownId = tbl_Godown.godownId 
                                                                      FULL Outer JOIN
                                                                      --tbl_StockPosting ON P.productId = tbl_StockPosting.godownId INNER JOIN
        									                          tbl_Godown ON P.godownId = tbl_Godown.godownId INNER JOIN
                                                                      tbl_Rack ON P.rackId = tbl_Rack.rackId INNER JOIN
                                                                      tbl_Unit AS U ON P.unitId = U.unitId INNER JOIN
                                                                      tbl_UnitConvertion AS UC ON P.productId = UC.productId AND U.unitId = UC.unitId

                                                WHERE  B.productId = " + productId);
                //P.godownId= " + StoreId + " and
                var items = new DBMatConnection().CustomQueryToDataTable(query);
                List<BomVM> data = new List<BomVM>();
                foreach (DataRow row in items.Rows)
                {
                    decimal prodId = Convert.ToDecimal(row["productId"]);
                    decimal storeId = Convert.ToDecimal(row["godownId"]);

                    BomVM obj = new BomVM();

                    obj.productId = Convert.ToInt32(row["productId"]);
                    obj.productName = Convert.ToString(row["productName"]);
                    obj.QtyAvailable = getProductAvailableQty(StoreId, prodId);
                    try
                    {
                        obj.Qty = Convert.ToInt32(row["Qty"]);
                    }
                    catch (Exception ex)
                    {
                        obj.Qty = 0;
                    }
                    try
                    {
                        obj.salesRate = Convert.ToInt32(row["salesRate"]);
                    }
                    catch (Exception ex)
                    {
                        obj.salesRate = 0;
                    }
                    obj.productCode = row["productCode"];
                    try
                    {
                        obj.unitId = Convert.ToInt32(row["unitId"]);
                    }
                    catch (Exception ex)
                    {
                        obj.unitId = 0;
                    }

                    try
                    {
                        obj.godownId = Convert.ToInt32(row["godownId"]);
                    }
                    catch (Exception ex)
                    {
                        obj.godownId = 0;
                    }

                    try
                    {
                        obj.rackId = Convert.ToInt32(row["rackId"]);
                    }
                    catch (Exception ex)
                    {
                        obj.rackId = 0;
                    }

                    try
                    {
                        obj.unitConversionId = Convert.ToInt32(row["unitconversionId"]);
                    }
                    catch (Exception ex)
                    {
                        obj.unitConversionId = 0;
                    }



                    data.Add(obj);
                }
                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int getProductAvailableQty12(decimal? storeId, decimal? prodId)
        {
            try
            {
                var result = 0;
                string query = "";

                query = "select (isnull(sum(a.inwardQty), 0) - isnull(sum(outwardQty), 0)) as Qty from tbl_StockPosting a left join tbl_User u on u.storeId = a.godownId left join tbl_Product b on a.productId = b.productId and a.godownId = b.godownId and b.godownId = u.storeId  where b.ProductId = " + prodId.Value + " and a.ProductId = " + prodId.Value + " and u.storeId = " + storeId.Value + " and u.godownId = " + storeId.Value + " and u.godownId = " + storeId.Value;

                query = "select (isnull(sum(inwardQty), 0) - isnull(sum(outwardQty), 0)) as Qty  from tbl_StockPosting where productId = " + prodId.Value + " and godownId = " + storeId.Value;

                DataSet ds = new DBMatConnection().ExecuteQuery(query);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    result = Convert.ToInt32(row["Qty"]);
                }

                return result;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        [HttpGet]
        public List<BomVM> GetRawMaterialForProductV1(decimal productId, int StoreId)
        {
            try
            {
                var query = string.Format(@"SELECT     P.productCode, P.productId, P.productName, tbl_Rack.rackId, tbl_Godown.godownId, CONVERT(decimal(18,2),B.quantity) As Qty, 
                                                                      CONVERT(decimal(18,2),P.salesRate) as salesRate, U.unitId, UC.unitconversionId
                                                FROM                  tbl_Product AS P INNER JOIN
                                                                      tbl_Bom AS B ON B.rowmaterialId = P.productId --INNER JOIN
                                                                      --tbl_Godown ON P.godownId = tbl_Godown.godownId 
                                                                      FULL Outer JOIN
                                                                      --tbl_StockPosting ON P.productId = tbl_StockPosting.godownId INNER JOIN
        									                          tbl_Godown ON P.godownId = tbl_Godown.godownId INNER JOIN
                                                                      tbl_Rack ON P.rackId = tbl_Rack.rackId INNER JOIN
                                                                      tbl_Unit AS U ON P.unitId = U.unitId INNER JOIN
                                                                      tbl_UnitConvertion AS UC ON P.productId = UC.productId AND U.unitId = UC.unitId

                                                WHERE  B.productId = " + productId);
                //P.godownId= " + StoreId + " and
                var items = new DBMatConnection().CustomQueryToDataTable(query);
                List<BomVM> data = new List<BomVM>();
                foreach (DataRow row in items.Rows)
                {
                    decimal prodId = Convert.ToDecimal(row["productId"]);
                    decimal storeId = Convert.ToDecimal(row["godownId"]);

                    BomVM obj = new BomVM();

                    obj.productId = Convert.ToInt32(row["productId"]);
                    obj.productName = Convert.ToString(row["productName"]);
                    obj.QtyAvailable = getProductAvailableQty(StoreId, prodId);



                    try
                    {
                        obj.Qty = Convert.ToDecimal(row["Qty"]);
                    }
                    catch (Exception ex)
                    {
                        obj.Qty = 0;
                    }
                    try
                    {
                        obj.salesRate = Convert.ToInt32(row["salesRate"]);
                    }
                    catch (Exception ex)
                    {
                        obj.salesRate = 0;
                    }
                    obj.productCode = row["productCode"];
                    try
                    {
                        obj.unitId = Convert.ToInt32(row["unitId"]);
                    }
                    catch (Exception ex)
                    {
                        obj.unitId = 0;
                    }

                    try
                    {
                        obj.godownId = Convert.ToInt32(row["godownId"]);
                    }
                    catch (Exception ex)
                    {
                        obj.godownId = 0;
                    }

                    try
                    {
                        obj.rackId = Convert.ToInt32(row["rackId"]);
                    }
                    catch (Exception ex)
                    {
                        obj.rackId = 0;
                    }

                    try
                    {
                        obj.unitConversionId = Convert.ToInt32(row["unitconversionId"]);
                    }
                    catch (Exception ex)
                    {
                        obj.unitConversionId = 0;
                    }

                    data.Add(obj);

                }

                return data;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public int getProductAvailableQty(decimal? storeId, decimal? prodId)
        {
            try
            {
                int result = 0;
                string query = "";

                query = "select (isnull(sum(a.inwardQty), 0) - isnull(sum(outwardQty), 0)) as Qty from tbl_StockPosting a left join tbl_User u on u.storeId = a.godownId left join tbl_Product b on a.productId = b.productId and a.godownId = b.godownId and b.godownId = u.storeId  where b.ProductId = " + prodId.Value + " and a.ProductId = " + prodId.Value + " and u.storeId = " + storeId.Value + " and u.godownId = " + storeId.Value + " and u.godownId = " + storeId.Value;


                var fromDate = MATFinancials.PublicVariables._dtFromDate;

                var toDate = MATFinancials.PublicVariables._dtToDate;


                query = "select (isnull(sum(inwardQty), 0) - isnull(sum(outwardQty), 0)) as Qty  from tbl_StockPosting where productId = " + prodId.Value + " and godownId = " + storeId.Value + "  and date >= '" + fromDate.Year + "-" + fromDate.Month + "-" + fromDate.Day + "' AND date <= '" + toDate.Year + "-" + toDate.Month + "-" + toDate.Day + "'";

                DataSet ds = new DBMatConnection().ExecuteQuery(query);
                if (ds.Tables[0].Rows.Count > 0)
                {
                    DataRow row = ds.Tables[0].Rows[0];
                    result = Convert.ToInt32(row["Qty"]);
                }

                if (result.ToString().StartsWith("-1"))
                {
                    // result = -0;
                }

                return result;
            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        //[HttpGet]
        //public List<BomVM> GetRawMaterialForProduct(decimal productId)
        //{

        //    //int qty = getProductAvailableQty(productId);
        //    //var items =   new ProductSP().RawMaterialsFillForStockJournal(productId, quantity);
        //    //return items

        //    //var query = string.Format(@"select  
        //    //                                    productId,
        //    //                                    productCode,
        //    //                                    productName,
        //    //                                    barcode,
        //    //                                    CONVERT(decimal(18,2),Qty) As Qty,
        //    //                                    unitId,
        //    //                                    rackId,
        //    //                                    godownId,batchId,
        //    //                                    CONVERT(decimal(18,2),salesRate) as salesRate,
        //    //                                    unitconversionId
        //    //                                    from
        //    //                                    (
        //    //                                    SELECT     P.productCode, P.productId, P.productName,tbl_Batch.batchId, tbl_Rack.rackId, tbl_Godown.godownId, B.quantity AS Qty, 
        //    //                                                          tbl_Batch.barcode,P.salesRate, U.unitId, UC.unitconversionId
        //    //                                    FROM                  tbl_Product AS P INNER JOIN
        //    //                                                          tbl_Bom AS B ON B.rowmaterialId = P.productId INNER JOIN
        //    //                                                          tbl_Godown ON P.godownId = tbl_Godown.godownId 
        //    //                                           INNER JOIN tbl_Batch ON P.productId = tbl_Batch.productId 
        //    //                                           FULL Outer JOIN
        //    //                                                          tbl_Rack ON P.rackId = tbl_Rack.rackId INNER JOIN
        //    //                                                          tbl_Unit AS U ON P.unitId = U.unitId INNER JOIN
        //    //                                                          tbl_UnitConvertion AS UC ON P.productId = UC.productId AND U.unitId = UC.unitId

        //    //                                    WHERE     (B.productId = '{0}')) as TEMP", productId);
        //    var query = string.Format(@"SELECT     P.productCode, P.productId, P.productName, tbl_Rack.rackId, tbl_Godown.godownId, CONVERT(decimal(18,2),B.quantity) As Qty, 
        //                                                              CONVERT(decimal(18,2),P.salesRate) as salesRate, U.unitId, UC.unitconversionId
        //                                        FROM                  tbl_Product AS P INNER JOIN
        //                                                              tbl_Bom AS B ON B.rowmaterialId = P.productId 
        //									                          --INNER JOIN
        //                                                              --tbl_Godown ON P.godownId = tbl_Godown.godownId 
        //                                                              FULL Outer JOIN
        //                                                              --tbl_StockPosting ON P.productId = tbl_StockPosting.godownId INNER JOIN
        //									                          tbl_Godown ON P.godownId = tbl_Godown.godownId INNER JOIN
        //                                                              tbl_Rack ON P.rackId = tbl_Rack.rackId INNER JOIN
        //                                                              tbl_Unit AS U ON P.unitId = U.unitId INNER JOIN
        //                                                              tbl_UnitConvertion AS UC ON P.productId = UC.productId AND U.unitId = UC.unitId

        //                                        WHERE     (B.productId = {0})", productId);
        //    var items = new DBMatConnection().CustomQueryToDataTable(query);
        //    List<BomVM> data = new List<BomVM>();
        //    foreach(DataRow row in items.Rows)
        //    {
        //        decimal prodId = Convert.ToDecimal(row["productId"]);
        //        decimal storeId = Convert.ToDecimal(row["godownId"]);

        //        BomVM obj = new BomVM
        //        {
        //            productId = Convert.ToInt32(row["productId"]),
        //            productName = Convert.ToString(row["productName"]),
        //            QtyAvailable = getProductAvailableQty(storeId, prodId),
        //            salesRate = Convert.ToInt32(row["salesRate"]),
        //            Qty = Convert.ToInt32(row["Qty"]),
        //            productCode = Convert.ToInt32(row["productCode"]),
        //            unitId = Convert.ToInt32(row["unitId"]),
        //            rackId = Convert.ToInt32(row["rackId"]),
        //            godownId = Convert.ToInt32(row["godownId"]),
        //            unitConversionId = Convert.ToInt32(row["unitconversionId"]),
        //        };
        //        data.Add(obj);
        //    }
        //    return data;
        //}

        //public int getProductAvailableQty(decimal? storeId, decimal? prodId)
        //{
        //    try
        //    {
        //        var result = 0;
        //        string query = "";

        //        query = "select (isnull(sum(a.inwardQty), 0) - isnull(sum(outwardQty), 0)) as Qty from tbl_StockPosting a left join tbl_User u on u.storeId = a.godownId left join tbl_Product b on a.productId = b.productId and a.godownId = b.godownId and b.godownId = u.storeId  where b.ProductId = " + prodId + " and a.ProductId = " + prodId + " and u.storeId = " + storeId + " and u.godownId = " + storeId + " and u.godownId = " + storeId;

        //        query = "select (isnull(sum(inwardQty), 0) - isnull(sum(outwardQty), 0)) as Qty  from tbl_StockPosting where productId = " + prodId + " and godownId = " + storeId;

        //        DataSet ds = new DBMatConnection().ExecuteQuery(query);
        //        if (ds.Tables[0].Rows.Count > 0)
        //        {
        //            DataRow row = ds.Tables[0].Rows[0];
        //            result = Convert.ToInt32(row["Qty"]);
        //        }
        //        return result;
        //    }
        //    catch(Exception ex)
        //    {
        //        return 0;
        //    }
        //}





        //[HttpGet]
        //public DataTable GetConsumbtionForProduct(decimal productId,decimal quantity)
        //{
        //    return new ProductSP().RawMaterialsFillForStockJournal(productId, quantity);
        //}

        [HttpPost]
        public bool SaveStockJournal(StockJournalVM input)
        {
            bool isSaved = false;

            try
            {
                StockJournalMasterInfo infoStockJournalMaster = new StockJournalMasterInfo();
                StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
                StockJournalDetailsInfo infoStockJournalDetailsConsumption = new StockJournalDetailsInfo();
                StockJournalDetailsInfo infoStockJournalDetailsProduction = new StockJournalDetailsInfo();
                StockJournalDetailsSP spStockJournalDetails = new StockJournalDetailsSP();
                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();


                var spStockPosting = new MatApi.Services.StockPostingSP();
                AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
                AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();

                input.StockJournalMasterInfo.Date = input.Date;
                input.StockJournalMasterInfo.ExtraDate = input.Date;

                strVoucherNo = generateVoucherNo();
                //if (isAutomatic == true)
                //{
                infoStockJournalMaster.SuffixPrefixId = decSuffixPrefixId;
                infoStockJournalMaster.VoucherNo = strVoucherNo;
                //}
                //else
                //{
                //    infoStockJournalMaster.SuffixPrefixId = 0;
                //    infoStockJournalMaster.VoucherNo = strVoucherNo;
                //}

                infoStockJournalMaster.ExtraDate = DateTime.Now;
                infoStockJournalMaster.InvoiceNo = strVoucherNo;
                infoStockJournalMaster.Date = input.Date;
                infoStockJournalMaster.AdditionalCost = input.TotalAdditionalCost;
                infoStockJournalMaster.VoucherNo = strVoucherNo;
                infoStockJournalMaster.VoucherTypeId = decVoucherTypeId;
                infoStockJournalMaster.Narration = input.StockJournalMasterInfo.Narration;
                infoStockJournalMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoStockJournalMaster.ExchangeRateId = input.Currency;
                infoStockJournalMaster.Extra1 = input.StockJournalMasterInfo.Extra1;
                infoStockJournalMaster.Extra2 = input.StockJournalMasterInfo.Extra2;
                decStockMasterId = spStockJournalMaster.StockJournalMasterAdd(infoStockJournalMaster);
                if (decStockMasterId > 0)
                {
                    isSaved = true;
                }
                else
                {
                    isSaved = false;
                }
                var stockJournalAction = "";

                if (input.StockJournalDetailsInfoConsumption.Count > 0)
                {
                    var stCount = input.StockJournalDetailsInfoConsumption.Count - 1;

                    for (var i = 0; i <= stCount; i++)
                    {
                        if (input.StockJournalDetailsInfoConsumption[i].stockJournalAction == "Manufacturing")
                        {
                            var avCostProduct = StockReportDetails(input.StockJournalDetailsInfoConsumption[i].ProductId);
                            input.StockJournalDetailsInfoConsumption[i].Rate = avCostProduct;
                        }
                    }

                }

                if (input.StockJournalDetailsInfoProduction.Count > 0)
                {
                    var stCount = input.StockJournalDetailsInfoProduction.Count - 1;

                    for (var i = 0; i <= stCount; i++)
                    {
                        if (input.StockJournalDetailsInfoProduction[i].stockJournalAction == "Manufacturing")
                        {
                            var avCostProduct = StockReportDetails(input.StockJournalDetailsInfoProduction[i].ProductId);
                            input.StockJournalDetailsInfoProduction[i].Rate = avCostProduct;
                        }
                    }
                }

                if (input.StockJournalDetailsInfoConsumption.Count > 0)
                {
                    foreach (var row in input.StockJournalDetailsInfoConsumption)
                    {
                        infoStockJournalDetailsConsumption.Qty = row.Qty;
                        //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(row.ProductId).UnitconvertionId;
                        var unitConvId = row.UnitConversionId;
                        var infoStockPostingConsumption = new StockPostingInfo();
                        var tblStockJournalDetail = new tbl_StockJournalDetails();

                        tblStockJournalDetail.stockJournalMasterId = decStockMasterId;
                        tblStockJournalDetail.extra1 = string.Empty;
                        tblStockJournalDetail.extra2 = string.Empty;
                        //tblStockJournalDetail.extraDate = DateTime.Now;
                        tblStockJournalDetail.productId = row.ProductId;
                        tblStockJournalDetail.qty = row.Qty;
                        tblStockJournalDetail.rate = row.Rate;
                        tblStockJournalDetail.unitId = row.UnitId;
                        tblStockJournalDetail.unitConversionId = unitConvId;
                        tblStockJournalDetail.batchId = row.BatchId;
                        tblStockJournalDetail.godownId = row.GodownId;
                        tblStockJournalDetail.rackId = row.RackId;
                        tblStockJournalDetail.amount = row.Amount;
                        tblStockJournalDetail.consumptionOrProduction = "Consumption";
                        tblStockJournalDetail.slno = row.Slno;
                        tblStockJournalDetail.projectId = row.ProjectId;
                        tblStockJournalDetail.categoryId = row.CategoryId;
                        tblStockJournalDetail.extraDate = input.Date;
                        context.tbl_StockJournalDetails.Add(tblStockJournalDetail);
                        context.SaveChanges();

                        //  isSaved= spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetailsConsumption);

                        if (row.stockJournalAction == "Manufacturing")
                        {
                            infoStockPostingConsumption.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                            infoStockPostingConsumption.Date = input.Date;
                            infoStockPostingConsumption.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            infoStockPostingConsumption.GodownId = row.GodownId;
                            infoStockPostingConsumption.InwardQty = 0;

                            int RemainingQty = 0;

                            try
                            {

                                //if (row.Qty >= (int)row.QtyAvailable)
                                //{
                                //    RemainingQty = -(int)(row.Qty);
                                //}
                                //else
                                //{

                                //}

                                RemainingQty = (int)row.Qty;
                                //(int)row.QtyAvailable -
                                infoStockPostingConsumption.OutwardQty = (decimal)RemainingQty;

                                // var ur = infoStockJournalDetailsConsumption.Qty;
                            }
                            catch (Exception ex)
                            {

                            }

                            infoStockPostingConsumption.ProductId = row.ProductId;
                            infoStockPostingConsumption.RackId = row.RackId;
                            infoStockPostingConsumption.Rate = row.Rate;
                            infoStockPostingConsumption.UnitId = row.UnitId;
                            infoStockPostingConsumption.InvoiceNo = strVoucherNo;
                            infoStockPostingConsumption.VoucherNo = strVoucherNo;
                            infoStockPostingConsumption.VoucherTypeId = decVoucherTypeId;
                            infoStockPostingConsumption.AgainstVoucherTypeId = 0;
                            infoStockPostingConsumption.AgainstInvoiceNo = "NA";
                            infoStockPostingConsumption.AgainstVoucherNo = "NA";
                            infoStockPostingConsumption.Extra1 = string.Empty;
                            infoStockPostingConsumption.Extra2 = string.Empty;
                            infoStockPostingConsumption.CategoryId = row.CategoryId;
                            infoStockPostingConsumption.ProjectId = row.ProjectId;
                            if (row.HasBatchSelected)
                            {
                                infoStockPostingConsumption.BatchId = row.BatchId;
                            }
                            if (spStockPosting.StockPostingAdd(infoStockPostingConsumption) > 0)
                            {
                                isSaved = true;
                            }
                            else
                            {
                                isSaved = false;
                            }
                        }
                        else if (row.stockJournalAction == "Stock Out")
                        {
                            stockJournalAction = row.stockJournalAction;
                            infoStockPostingConsumption.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                            infoStockPostingConsumption.Date = input.Date;
                            infoStockPostingConsumption.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            infoStockPostingConsumption.GodownId = row.GodownId;
                            infoStockPostingConsumption.InwardQty = 0;

                            try
                            {
                                decimal RemainingQty = 0;

                                //if (row.Qty >= (int)row.QtyAvailable)
                                //{
                                //    RemainingQty = -(int)(row.Qty);
                                //}
                                //else
                                //{
                                //    RemainingQty =   (int)(row.Qty);
                                //}

                                RemainingQty = row.Qty;
                                //row.QtyAvailable - 
                                infoStockPostingConsumption.OutwardQty = (decimal)RemainingQty;

                                // var ur = infoStockJournalDetailsConsumption.Qty;
                            }
                            catch (Exception ex)
                            {

                            }

                            infoStockPostingConsumption.ProductId = row.ProductId;
                            infoStockPostingConsumption.RackId = row.RackId;
                            infoStockPostingConsumption.Rate = row.Rate;
                            infoStockPostingConsumption.UnitId = row.UnitId;
                            infoStockPostingConsumption.InvoiceNo = strVoucherNo;
                            infoStockPostingConsumption.VoucherNo = strVoucherNo;
                            infoStockPostingConsumption.VoucherTypeId = decVoucherTypeId;
                            infoStockPostingConsumption.AgainstVoucherTypeId = 0;
                            infoStockPostingConsumption.AgainstInvoiceNo = "NA";
                            infoStockPostingConsumption.AgainstVoucherNo = "NA";
                            infoStockPostingConsumption.Extra1 = string.Empty;
                            infoStockPostingConsumption.Extra2 = string.Empty;
                            infoStockPostingConsumption.CategoryId = row.CategoryId;
                            infoStockPostingConsumption.ProjectId = row.ProjectId;
                            if (row.HasBatchSelected)
                            {
                                infoStockPostingConsumption.BatchId = row.BatchId;
                            }
                            if (spStockPosting.StockPostingAdd(infoStockPostingConsumption) > 0)
                            {
                                isSaved = true;
                            }
                            else
                            {
                                isSaved = false;
                            }



                        }
                        else if (row.stockJournalAction == "Stock Transfer")
                        {

                            infoStockPostingConsumption.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                            infoStockPostingConsumption.Date = input.Date;
                            infoStockPostingConsumption.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            infoStockPostingConsumption.GodownId = row.GodownId;
                            infoStockPostingConsumption.InwardQty = 0;

                            int RemainingQty = 0;

                            try
                            {

                                //if (row.Qty >= (int)row.QtyAvailable)
                                //{
                                //    RemainingQty = -(int)row.Qty;
                                //}
                                //else
                                //{
                                //    RemainingQty = (int)row.QtyAvailable - (int)row.Qty;
                                //}

                                infoStockPostingConsumption.OutwardQty = row.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId);

                                //var ur = infoStockJournalDetailsConsumption.Qty;
                            }
                            catch (Exception ex)
                            {

                            }

                            infoStockPostingConsumption.ProductId = row.ProductId;
                            infoStockPostingConsumption.RackId = row.RackId;
                            infoStockPostingConsumption.Rate = row.Rate;
                            infoStockPostingConsumption.UnitId = row.UnitId;
                            infoStockPostingConsumption.InvoiceNo = strVoucherNo;
                            infoStockPostingConsumption.VoucherNo = strVoucherNo;
                            infoStockPostingConsumption.VoucherTypeId = decVoucherTypeId;
                            infoStockPostingConsumption.AgainstVoucherTypeId = 0;
                            infoStockPostingConsumption.AgainstInvoiceNo = "NA";
                            infoStockPostingConsumption.AgainstVoucherNo = "NA";
                            infoStockPostingConsumption.Extra1 = string.Empty;
                            infoStockPostingConsumption.Extra2 = string.Empty;
                            infoStockPostingConsumption.CategoryId = row.CategoryId;
                            infoStockPostingConsumption.ProjectId = row.ProjectId;
                            if (row.HasBatchSelected)
                            {
                                infoStockPostingConsumption.BatchId = row.BatchId;
                            }
                            if (spStockPosting.StockPostingAdd(infoStockPostingConsumption) > 0)
                            {
                                isSaved = true;
                            }
                            else
                            {
                                isSaved = false;
                            }

                        }

                    }

                    var context12 = new DBMATAccounting_MagnetEntities1();
                    foreach (var row in input.StockJournalDetailsInfoConsumption)
                    {
                        if (row.stockJournalAction == "Stock Out")
                        {
                            var AvagCost = Services.AvarageCostCalculation.Calculation(new Models.Reports.OtherReports.StockReportSearch() { FromDate = PublicVariables._dtFromDate, ToDate = PublicVariables._dtToDate, ProductId = row.ProductId, StoreId = row.GodownId, BatchNo = "All", CategoryId = 0, ProductCode = "", ProjectId = 0, RefNo = "" });

                            var proEx = context12.tbl_Product.ToList().Where(p => p.productId == row.ProductId).FirstOrDefault();

                            var TotalAmt = AvagCost == "" || AvagCost == null ? 0 : row.Qty * Convert.ToDecimal(AvagCost);

                            if (proEx != null)
                            {
                                infoLedgerPosting = new LedgerPostingInfo();
                                infoLedgerPosting.Credit = 0;
                                infoLedgerPosting.Debit = TotalAmt;
                                infoLedgerPosting.Date = input.Date;
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = strVoucherNo;
                                infoLedgerPosting.LedgerId = proEx.expenseAccount.Value;
                                infoLedgerPosting.VoucherNo = strVoucherNo;
                                infoLedgerPosting.VoucherTypeId = decVoucherTypeId;
                                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.ChequeDate = input.Date;
                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                if (spLedgerPosting.LedgerPostingAdd(infoLedgerPosting) > 0)
                                {

                                }
                                else
                                {

                                }
                            }
                        }
                    }
                }

                if (input.StockJournalDetailsInfoProduction.Count > 0)
                {
                    foreach (var row in input.StockJournalDetailsInfoProduction)
                    {
                        //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(row.ProductId).UnitconvertionId;

                        //infoStockJournalDetailsProduction.StockJournalMasterId = decStockMasterId;
                        //infoStockJournalDetailsProduction.Extra1 = string.Empty;
                        //infoStockJournalDetailsProduction.Extra2 = string.Empty;
                        //infoStockJournalDetailsProduction.ExtraDate = DateTime.Now;
                        //infoStockJournalDetailsProduction.ProductId = row.ProductId;
                        //infoStockJournalDetailsProduction.Qty = row.Qty;
                        //infoStockJournalDetailsProduction.Rate = row.Rate;
                        //infoStockJournalDetailsProduction.UnitId = unitId;
                        //infoStockJournalDetailsProduction.UnitConversionId = unitConvId;
                        //infoStockJournalDetailsProduction.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                        //infoStockJournalDetailsProduction.GodownId = row.GodownId;
                        //infoStockJournalDetailsProduction.RackId = row.RackId;
                        //infoStockJournalDetailsProduction.Amount = row.Amount;
                        //infoStockJournalDetailsProduction.ConsumptionOrProduction = "Production";
                        //infoStockJournalDetailsProduction.Slno = row.Slno;
                        //infoStockJournalDetailsProduction.CategoryId = row.CategoryId;
                        //infoStockJournalDetailsProduction.ProjectId = row.ProjectId;
                        //isSaved = spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetailsProduction);

                        if (row.stockJournalAction == "Stock Transfer")
                        {
                            var unitConvId = row.UnitConversionId;
                            var unitId = new UnitSP().unitVieWForStandardRate(row.ProductId).UnitId;  //unitVieWForStandardRate is used to get unitid since no function returns
                                                                                                      //unitinfo based on unit id
                            var tblStockJournalDetail = new tbl_StockJournalDetails();
                            tblStockJournalDetail.stockJournalMasterId = decStockMasterId;
                            tblStockJournalDetail.extra1 = string.Empty;
                            tblStockJournalDetail.extra2 = string.Empty;
                            tblStockJournalDetail.extraDate = input.Date;
                            tblStockJournalDetail.productId = row.ProductId;
                            tblStockJournalDetail.qty = row.Qty;
                            tblStockJournalDetail.rate = row.Rate;
                            tblStockJournalDetail.unitId = row.UnitId;
                            tblStockJournalDetail.unitConversionId = unitConvId;
                            tblStockJournalDetail.batchId = row.BatchId;
                            tblStockJournalDetail.godownId = row.GodownId;
                            tblStockJournalDetail.rackId = row.RackId;
                            tblStockJournalDetail.amount = row.Amount;
                            tblStockJournalDetail.consumptionOrProduction = "Production";
                            tblStockJournalDetail.slno = row.Slno;
                            tblStockJournalDetail.projectId = row.ProjectId;
                            tblStockJournalDetail.categoryId = row.CategoryId;
                            context.tbl_StockJournalDetails.Add(tblStockJournalDetail);
                            context.SaveChanges();


                            var infoStockPostingProduction = new StockPostingInfo();
                            infoStockPostingProduction.BatchId = row.BatchId;// new BatchSP().BatchIdViewByProductId(row.ProductId);
                            infoStockPostingProduction.Date = input.Date;
                            infoStockPostingProduction.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            infoStockPostingProduction.GodownId = row.GodownId;
                            infoStockPostingProduction.InwardQty = row.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId);
                            infoStockPostingProduction.OutwardQty = 0;
                            infoStockPostingProduction.ProductId = row.ProductId;
                            infoStockPostingProduction.RackId = row.RackId;
                            infoStockPostingProduction.Rate = row.Rate;
                            infoStockPostingProduction.UnitId = unitId;
                            infoStockPostingProduction.InvoiceNo = strVoucherNo;
                            infoStockPostingProduction.VoucherNo = strVoucherNo;
                            infoStockPostingProduction.VoucherTypeId = decVoucherTypeId;
                            infoStockPostingProduction.AgainstVoucherTypeId = 0;
                            infoStockPostingProduction.AgainstInvoiceNo = "NA";
                            infoStockPostingProduction.AgainstVoucherNo = "NA";
                            infoStockPostingProduction.CategoryId = row.CategoryId;
                            infoStockPostingProduction.ProjectId = row.ProjectId;
                            infoStockPostingProduction.Extra1 = string.Empty;
                            infoStockPostingProduction.Extra2 = string.Empty;
                            if (!string.IsNullOrWhiteSpace(row.NewBatchName))
                            {
                                var newBatch = new tbl_Batch()
                                {
                                    manufacturingDate = DateTime.UtcNow,
                                    productId = row.ProductId,
                                };

                                if (string.IsNullOrWhiteSpace(row.NewBatchName) || string.IsNullOrEmpty(row.NewBatchName))
                                {
                                    row.NewBatchName = "NA";
                                }

                                newBatch.batchNo = row.NewBatchName;
                                context.tbl_Batch.Add(newBatch);
                                context.SaveChanges();
                                infoStockPostingProduction.BatchId = newBatch.batchId;
                            }
                            if (spStockPosting.StockPostingAdd(infoStockPostingProduction) > 0)
                            {
                                isSaved = true;
                            }
                            else
                            {
                                isSaved = false;
                            }
                        }
                        else if (row.stockJournalAction == "Manufacturing")
                        {
                            var unitConvId = row.UnitConversionId;
                            var unitId = new UnitSP().unitVieWForStandardRate(row.ProductId).UnitId;  //unitVieWForStandardRate is used to get unitid since no function returns
                                                                                                      //unitinfo based on unit id
                            var tblStockJournalDetail = new tbl_StockJournalDetails();
                            tblStockJournalDetail.stockJournalMasterId = decStockMasterId;
                            tblStockJournalDetail.extra1 = string.Empty;
                            tblStockJournalDetail.extra2 = string.Empty;
                            tblStockJournalDetail.extraDate = input.Date;
                            tblStockJournalDetail.productId = row.ProductId;
                            tblStockJournalDetail.qty = row.Qty;
                            tblStockJournalDetail.rate = row.Rate;
                            tblStockJournalDetail.unitId = row.UnitId;
                            tblStockJournalDetail.unitConversionId = unitConvId;
                            tblStockJournalDetail.batchId = row.BatchId;
                            tblStockJournalDetail.godownId = row.GodownId;
                            tblStockJournalDetail.rackId = row.RackId;
                            tblStockJournalDetail.amount = row.Amount;
                            tblStockJournalDetail.consumptionOrProduction = "Production";
                            tblStockJournalDetail.slno = row.Slno;
                            tblStockJournalDetail.projectId = row.ProjectId;
                            tblStockJournalDetail.categoryId = row.CategoryId;
                            context.tbl_StockJournalDetails.Add(tblStockJournalDetail);
                            context.SaveChanges();

                            var infoStockPostingProduction = new StockPostingInfo();
                            infoStockPostingProduction.BatchId = row.BatchId;// new BatchSP().BatchIdViewByProductId(row.ProductId);
                            infoStockPostingProduction.Date = input.Date;
                            infoStockPostingProduction.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            infoStockPostingProduction.GodownId = row.GodownId;
                            infoStockPostingProduction.InwardQty = row.Qty;/// SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId);
                            infoStockPostingProduction.OutwardQty = 0;
                            infoStockPostingProduction.ProductId = row.ProductId;
                            infoStockPostingProduction.RackId = row.RackId;
                            infoStockPostingProduction.Rate = row.Rate;
                            infoStockPostingProduction.UnitId = unitId;
                            infoStockPostingProduction.InvoiceNo = strVoucherNo;
                            infoStockPostingProduction.VoucherNo = strVoucherNo;
                            infoStockPostingProduction.VoucherTypeId = decVoucherTypeId;
                            infoStockPostingProduction.AgainstVoucherTypeId = 0;
                            infoStockPostingProduction.AgainstInvoiceNo = "NA";
                            infoStockPostingProduction.AgainstVoucherNo = "NA";
                            infoStockPostingProduction.CategoryId = row.CategoryId;
                            infoStockPostingProduction.ProjectId = row.ProjectId;
                            infoStockPostingProduction.Extra1 = tblStockJournalDetail.consumptionOrProduction;
                            infoStockPostingProduction.Extra2 = tblStockJournalDetail.stockJournalDetailsId.ToString();
                            if (!string.IsNullOrWhiteSpace(row.NewBatchName))
                            {
                                var newBatch = new tbl_Batch()
                                {
                                    manufacturingDate = DateTime.UtcNow,
                                    productId = row.ProductId,
                                };

                                if (string.IsNullOrWhiteSpace(row.NewBatchName) || string.IsNullOrEmpty(row.NewBatchName))
                                {
                                    row.NewBatchName = "NA";
                                }

                                newBatch.batchNo = row.NewBatchName;
                                context.tbl_Batch.Add(newBatch);
                                context.SaveChanges();
                                infoStockPostingProduction.BatchId = newBatch.batchId;
                            }
                            if (spStockPosting.StockPostingAdd(infoStockPostingProduction) > 0)
                            {
                                isSaved = true;
                            }
                            else
                            {
                                isSaved = false;
                            }
                        }
                    }
                }

                decimal decGrandTotal = 0;
                decimal decRate = 0;
                ExchangeRateSP spExchangeRate = new ExchangeRateSP();
                if (input.AdditionalCostItems.Count > 0)
                {
                    //infoAdditionalCost.Credit = input.TotalAdditionalCost;
                    //infoAdditionalCost.Debit = 0;
                    //infoAdditionalCost.LedgerId = input.AdditionalCostCashOrBankId;
                    //infoAdditionalCost.VoucherNo = strVoucherNo;
                    //infoAdditionalCost.VoucherTypeId = decVoucherTypeId;
                    //infoAdditionalCost.Extra1 = string.Empty;
                    //infoAdditionalCost.Extra2 = string.Empty;
                    //infoAdditionalCost.ExtraDate = DateTime.Now;

                    //infoAdditionalCost.ProjectId id= input.
                    //context.tbl_AdditionalCost.Add(new tbl_AdditionalCost
                    //{
                    //    ledgerId = infoAdditionalCost.LedgerId,
                    //    extra2 = infoAdditionalCost.Extra2,
                    //    extraDate = infoAdditionalCost.ExtraDate,
                    //    extra1 = infoAdditionalCost.Extra1,
                    //    voucherNo = infoAdditionalCost.VoucherNo,
                    //    voucherTypeId = infoAdditionalCost.VoucherTypeId,
                    //    credit = infoAdditionalCost.Credit,
                    //    debit = infoAdditionalCost.Debit,
                    //});
                    //context.SaveChanges();
                    //isSaved = true;
                    //isSaved= spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                    //....Ledger Posting Add...///
                    //-------------------  Currency Conversion-----------------------------
                    //decGrandTotal = input.TotalAdditionalCost;
                    //decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(input.Currency);
                    //decGrandTotal = decGrandTotal * decRate;
                    ////---------------------------------------------------------------
                    //infoLedgerPosting.Credit = decGrandTotal;
                    //infoLedgerPosting.Debit = 0;
                    //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);
                    //infoLedgerPosting.DetailsId = 0;
                    //infoLedgerPosting.InvoiceNo = strVoucherNo;
                    //infoLedgerPosting.LedgerId = input.AdditionalCostCashOrBankId;
                    //infoLedgerPosting.VoucherNo = strVoucherNo;
                    //infoLedgerPosting.VoucherTypeId = decVoucherTypeId;
                    //infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    //infoLedgerPosting.ChequeDate = DateTime.Now;
                    //infoLedgerPosting.ChequeNo = string.Empty;
                    //infoLedgerPosting.Extra1 = string.Empty;
                    //infoLedgerPosting.Extra2 = string.Empty;
                    //infoLedgerPosting.ExtraDate = DateTime.Now;
                    //if(spLedgerPosting.LedgerPostingAdd(infoLedgerPosting)>0)
                    //{
                    //    isSaved = true;
                    //}
                    //else
                    //{
                    //    isSaved = false;
                    //}
                    foreach (var row in input.AdditionalCostItems)
                    {
                        /*-----------------------------------------Additional Cost Add----------------------------------------------------*/
                        infoAdditionalCost.Credit = 0;
                        infoAdditionalCost.Debit = row.Amount;
                        infoAdditionalCost.LedgerId = row.LedgerId;
                        infoAdditionalCost.VoucherNo = strVoucherNo;
                        infoAdditionalCost.VoucherTypeId = decVoucherTypeId;
                        infoAdditionalCost.StockJournalMasterId = decStockMasterId;
                        infoAdditionalCost.Extra1 = string.Empty;
                        infoAdditionalCost.Extra2 = string.Empty;
                        infoAdditionalCost.ExtraDate = DateTime.Now;
                        context.tbl_AdditionalCost.Add(new tbl_AdditionalCost()
                        {
                            ledgerId = row.LedgerId,
                            extra2 = "",
                            extraDate = DateTime.Now,
                            extra1 = "",
                            credit = 0,
                            debit = row.Amount,
                            voucherNo = strVoucherNo,
                            voucherTypeId = decVoucherTypeId,
                            projectId = row.ProjectId,
                            categoryId = row.CategoryId,
                            stockJornalMasterId = decStockMasterId
                        });
                        context.SaveChanges();
                        isSaved = true;
                        //isSaved= spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                        /*-----------------------------------------Additional Cost Ledger Posting----------------------------------------------------*/
                        decimal decTotal = 0;
                        //-------------------  Currency Conversion------------------------
                        decTotal = Convert.ToDecimal(infoAdditionalCost.Debit);
                        decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(input.Currency);
                        decTotal = decTotal * decRate;
                        //---------------------------------------------------------------
                        infoLedgerPosting.Credit = 0;
                        infoLedgerPosting.Debit = decTotal;
                        infoLedgerPosting.Date = input.Date;
                        infoLedgerPosting.DetailsId = 0;
                        infoLedgerPosting.InvoiceNo = strVoucherNo;
                        infoLedgerPosting.LedgerId = row.LedgerId;
                        infoLedgerPosting.VoucherNo = strVoucherNo;
                        infoLedgerPosting.VoucherTypeId = decVoucherTypeId;
                        infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                        infoLedgerPosting.ChequeDate = input.Date;
                        infoLedgerPosting.ChequeNo = string.Empty;
                        infoLedgerPosting.Extra1 = string.Empty;
                        infoLedgerPosting.Extra2 = string.Empty;
                        infoLedgerPosting.ExtraDate = DateTime.Now;
                        if (spLedgerPosting.LedgerPostingAdd(infoLedgerPosting) > 0)
                        {
                            isSaved = true;
                        }
                        else
                        {
                            isSaved = false;
                        }
                    }
                }
                //if (input.StockJournalMasterInfo.Extra1 == "Stock Transfer")
                //{
                //    DBMatConnection conn = new DBMatConnection();
                //    string queryStr = string.Format("UPDATE tbl_StockJournalMaster_Pending " +
                //                                    "SET status='{0}'" +
                //                                    "WHERE stockJournalMasterId={1} ", "Approved", input.StockJournalMasterInfo.Extra2);
                //    if (conn.customUpdateQuery(queryStr) > 0)
                //    {
                //        return true;
                //    }
                //}
            }
            catch (Exception ex)
            {

            }

            return isSaved;
        }




        List<string> AllItems = new List<string>();
        List<decimal> AllStores = new List<decimal>();
        bool isOpeningRate = false;
        public decimal StockReportDetails(decimal ProductId)
        {
            Models.Reports.OtherReports.StockReportRow master = new Models.Reports.OtherReports.StockReportRow();
            List<Models.Reports.OtherReports.StockReportRowItems> details = new List<Models.Reports.OtherReports.StockReportRowItems>();
            Models.Reports.OtherReports.StockReportSearch input = new Models.Reports.OtherReports.StockReportSearch();

            input.FromDate = PublicVariables._dtFromDate;
            input.ToDate = PublicVariables._dtToDate;

            var ListLastFyear = context.tbl_FinancialYear.Where(fy => fy.fromDate < input.FromDate && fy.toDate < input.FromDate).ToList();

            var BalanceDateDate = input.FromDate;

            if (ListLastFyear.Count > 0)
            {
                BalanceDateDate = ListLastFyear.Min(cfy => cfy.fromDate.Value);
            }

            input.FromDate = BalanceDateDate;

            input.BatchNo = "All";
            input.CategoryId = 0;
            input.ProjectId = 0;
            input.TransactionType = "0";
            input.StoreId = 0;
            input.ProductId = ProductId;
            input.ProductCode = "";
            input.RefNo = "";

            try
            {
                DataTable prodDtbl = new DataTable();
                prodDtbl = new ProductSP().ProductViewAll();
                for (int k = 0; k < prodDtbl.Rows.Count; k++)
                {
                    var FinancialDate = Convert.ToDateTime(prodDtbl.Rows[k]["effectiveDate"].ToString());

                    if (FinancialDate >= input.FromDate && FinancialDate <= input.ToDate)
                    {

                    }

                    AllItems.Add(prodDtbl.Rows[k]["productCode"].ToString());
                }

                AllItems = AllItems.ToList().Distinct().ToList();
                GodownSP spGodown = new GodownSP();
                DataTable storeDtbl = new DataTable();
                storeDtbl = spGodown.GodownViewAll();
                for (int k = 0; k < storeDtbl.Rows.Count; k++)
                {
                    AllStores.Add(Convert.ToDecimal(storeDtbl.Rows[k]["godownId"]));
                }

                AllStores = AllStores.ToList().Distinct().ToList();
                SettingsInfo infoSettings = new SettingsInfo();
                SettingsSP SpSettings = new SettingsSP();
                string calculationMethod = string.Empty;
                if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "FIFO")
                {
                    calculationMethod = "FIFO";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Average Cost")
                {
                    calculationMethod = "Average Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "High Cost")
                {
                    calculationMethod = "High Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Low Cost")
                {
                    calculationMethod = "Low Cost";
                }
                else if (SpSettings.SettingsStatusCheck("StockValueCalculationMethod") == "Last Purchase Rate")
                {
                    calculationMethod = "Last Purchase Rate";
                }

                DBMatConnection conn = new DBMatConnection();
                MATFinancials.Classes.HelperClasses.GeneralQueries methodForStockValue = new MATFinancials.Classes.HelperClasses.GeneralQueries();

                string queryStr = string.Format("select top 1 f.toDate from tbl_FinancialYear f where f.toDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtToDate.ToString());

                string queryStr2 = string.Format("select top 1 f.fromDate from tbl_FinancialYear f where f.fromDate < '{0}' order by f.financialYearId desc ", PublicVariables._dtFromDate.ToString());

                DateTime lastFinYearStart = conn.getSingleValue(queryStr2) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr2)) : PublicVariables._dtFromDate;

                DateTime lastFinYearEnd = DateTime.UtcNow;

                try
                {
                    lastFinYearEnd = conn.getSingleValue(queryStr) != string.Empty ? Convert.ToDateTime(conn.getSingleValue(queryStr)) : PublicVariables._dtFromDate.AddDays(-1);
                }
                catch (Exception ex)
                {

                }

                if (conn.getSingleValue(queryStr) != string.Empty)
                {
                    lastFinYearEnd = Convert.ToDateTime(conn.getSingleValue(queryStr));
                }
                else
                {
                    //lastFinYearEnd = input.FromDate.AddDays(-1);
                    lastFinYearEnd = input.ToDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                }
                decimal currentProductId = 0, prevProductId = 0, prevStoreId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, rate2 = 0;
                decimal qtyBal = 0, stockValue = 0, OpeningStockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal stockValuePerItemPerStore = 0, totalStockValue = 0;
                decimal inwardQtyPerStore = 0, outwardQtyPerStore = 0, totalQtyPerStore = 0;
                decimal dcOpeningStockForRollOver = 0, openingQuantity = 0, openingQuantityIn = 0, openingQuantityOut = 0;
                bool isAgainstVoucher = false;
                string productCode = "", productName = "", storeName = "";
                int i = 0;
                string[] averageCostVouchers = new string[5] { "Delivery Note", "Rejection In", "Sales Invoice", "Sales Return", "Physical Stock" };
                DataTable dtbl = new DataTable();
                DataTable dtblItem = new DataTable();
                DataTable dtblStore = new DataTable();
                DataTable dtblOpeningStocks = new DataTable();
                DataTable dtblOpeningStockPerItemPerStore = new DataTable();
                var spstock = new MATFinancials.StockPostingSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();

                FinancialStatementSP spFinancial = new FinancialStatementSP();
                DateValidation objValidation = new DateValidation();
                decimal productId = input.ProductId, storeId = input.StoreId;
                string produceCode = input.ProductCode, batch = input.BatchNo, referenceNo = input.RefNo;
                input.ToDate = input.ToDate.AddHours(23).AddMinutes(59).AddSeconds(59);

                //dtbl = spstock.StockReportDetailsGridFill(0, 0, "All", Convert.ToDateTime(txtFromDate.Text), Convert.ToDateTime(txtTodate.Text), "", "");
                //dtblOpeningStocks = spstock.StockReportDetailsGridFill(0, 0, "All", PublicVariables._dtFromDate, Convert.ToDateTime(txtFromDate.Text).AddDays(-1), "", "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));
                if (batch != "" || input.ProjectId != 0 || input.CategoryId != 0 || productId != 0 || storeId != 0 || produceCode != "")

                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));
                else
                    dtblOpeningStocks = spstock.StockReportDetailsGridFill(productId, storeId, batch, input.FromDate, input.ToDate, produceCode, referenceNo, input.ProjectId, input.CategoryId, decimal.Parse(input.TransactionType));


                if (AllItems.Any())
                {
                    foreach (var item in AllItems)
                    {
                        // ======================================== for each item begins here ====================================== //
                        dtblItem = new DataTable();

                        if (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item).Any())
                        {
                            dtblItem = (dtbl.AsEnumerable().Where(j => j.Field<string>("productCode") == item)).CopyToDataTable();

                            productName = (dtbl.AsEnumerable().Where(m => m.Field<string>("productCode") == item).Select(m => m.Field<string>("productName")).FirstOrDefault().ToString());
                        }

                        if (productName.ToLower().Contains("CHINCHIN".ToLower()) || productName.ToLower().Equals("CHINCHIN".ToLower()))
                        {

                        }

                        if (dtblItem != null && dtblItem.Rows.Count > 0)
                        {
                            if (AllStores.Any())
                            {
                                foreach (var store in AllStores)
                                {
                                    // =================================== for that item, for each store begins here ============================== //

                                    qtyBal = 0; stockValue = 0;
                                    stockValuePerItemPerStore = 0; //dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    dtblStore = new DataTable();

                                    // ************************ insert opening stock per item per store before other transactions ******************** //
                                    dtblOpeningStockPerItemPerStore = new DataTable();
                                    if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                    {
                                        dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store)).CopyToDataTable();
                                        if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                        {
                                            computedAverageRate = rate2;
                                            var itemParams = methodForStockValue.currentStockValue1235(lastFinYearStart, lastFinYearEnd, true, item, store);
                                            dcOpeningStockForRollOver = itemParams.Item1;
                                            openingQuantity = itemParams.Item2;
                                            openingQuantityIn = itemParams.Item3;
                                            openingQuantityOut = itemParams.Item4;
                                            qtyBal += openingQuantity;
                                            qtyBal = 0;
                                            // qtyBal = openingQuantity;
                                            if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                            {
                                                computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                                stockValue = qtyBal * computedAverageRate;
                                            }

                                            details.Add(new Models.Reports.OtherReports.StockReportRowItems
                                            {
                                                ProductId = item,
                                                ProductCode = item,
                                                ProductName = productName,
                                                StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                                Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                                InwardQty = openingQuantityIn.ToString("N2"),
                                                OutwardQty = openingQuantityOut.ToString("N2"),
                                                Rate = "",
                                                AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                                QtyBalance = openingQuantity.ToString("N2"),
                                                StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                                VoucherTypeName = "Opening Stock "
                                            });

                                            //dgvStockDetailsReport.Rows[i].Cells["productId"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productCode"].Value = item;
                                            //dgvStockDetailsReport.Rows[i].Cells["productName"].Value = productName;
                                            //dgvStockDetailsReport.Rows[i].Cells["godownName"].Value = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                            //Where(l => l.Field<decimal>("godownId") == store).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(); ;
                                            //dgvStockDetailsReport.Rows[i].Cells["inwardQty"].Value = openingQuantityIn.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["outwardQty"].Value = openingQuantityOut.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["rate"].Value = "";
                                            //dgvStockDetailsReport.Rows[i].Cells["dgvtxtAverageCost"].Value = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "";
                                            //dgvStockDetailsReport.Rows[i].Cells["qtyBalalance"].Value = openingQuantity.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["stockValue"].Value = dcOpeningStockForRollOver.ToString("N2");
                                            //dgvStockDetailsReport.Rows[i].Cells["voucherTypeName"].Value = "Opening Stock ";

                                            //New Comments
                                            totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            // dcOpeningStockForRollOver;
                                            //totalQtyPerStore += openingQuantity;
                                            //i++;
                                            //Old Comments
                                        }
                                    }
                                    // ************************ insert opening stock per item per store before other transactions end ******************** //

                                    dtblStore = null;

                                    if (dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).Any())
                                    {
                                        //if (input.TransactionType.ToLower() == "All".ToLower())
                                        //{

                                        //}
                                        //else
                                        //{
                                        //    //dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store  && j.Field<string>("typeOfVoucher").ToLower() == input.TransactionType.ToLower()).CopyToDataTable();
                                        //}


                                        dtblStore = dtblItem.AsEnumerable().Where(j => j.Field<decimal>("godownId") == store).CopyToDataTable();


                                    }

                                    if (dtblStore != null && dtblStore.Rows.Count > 0)
                                    {
                                        storeName = (dtblStore.AsEnumerable().Where(m => m.Field<decimal>("godownId") == store).Select(m => m.Field<string>("godownName")).FirstOrDefault().ToString());


                                        foreach (DataRow dr in dtblStore.Rows)
                                        {
                                            // opening stock has been factored in thinking there was no other transaction, now other transactions factored it in again, so remove it
                                            totalStockValue -= Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2"));
                                            //dgvStockDetailsReport.Rows[i-1].DefaultCellStyle.BackColor = Color.White;
                                            //dgvStockDetailsReport.Rows[i-1].Cells["rate"].Value = "";
                                            dcOpeningStockForRollOver = 0;  // reset the opening stock because of the loop

                                            currentProductId = Convert.ToDecimal(dr["productId"]);
                                            productCode = dr["productCode"].ToString();
                                            string voucherType = "", refNo = "";
                                            inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);

                                            decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                                            string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);

                                            var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            if (!isOpeningRate)
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }
                                            else
                                            {
                                                rate2 = Convert.ToDecimal(dr["rate"]);
                                                isOpeningRate = false;
                                            }

                                            //if (purchaseRate == 0)
                                            //{
                                            //    purchaseRate = rate2;
                                            //}

                                            if (prevStoreId != store)
                                            {
                                                previousRunningAverage = purchaseRate;
                                            }

                                            if (previousRunningAverage != 0 && currentProductId == prevProductId)
                                            {
                                                purchaseRate = previousRunningAverage;
                                            }

                                            refNo = dr["refNo"].ToString();

                                            if (againstVoucherType != "NA")
                                            {
                                                refNo = dr["againstVoucherNo"].ToString();
                                                isAgainstVoucher = true;
                                                againstVoucherType = dr["AgainstVoucherTypeName"].ToString();

                                                string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                                                string typeOfVoucher = conn.getSingleValue(queryString);
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note".ToLower() && typeOfVoucher.ToLower() == "sales invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                                if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt".ToLower() && typeOfVoucher.ToLower() == "purchase invoice".ToLower())
                                                {
                                                    againstVoucherType = dr["typeOfVoucher"].ToString();
                                                }
                                            }

                                            voucherType = dr["typeOfVoucher"].ToString();

                                            if (voucherType.ToLower() == "Stock Journal".ToLower())
                                            {
                                                //voucherType = "Stock Transfer";
                                                string queryString = string.Format(" SELECT extra1 FROM tbl_StockJournalMaster WHERE voucherNo = '{0}' ", dr["refNo"]);
                                                voucherType = conn.getSingleValue(queryString);
                                            }

                                            if (outwardQty2 > 0)
                                            {
                                                if (voucherType.ToLower() == "Sales Invoice".ToLower() && outwardQty2 > 0)
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate;
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Material Receipt".ToLower())
                                                {
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue -= outwardQty2 * rate2;
                                                }
                                                else if (outwardQty2 > 0 && (voucherType.ToLower() == "Rejection Out".ToLower()) || (voucherType.ToLower() == "Purchase Return".ToLower())
                                                    || (voucherType.ToLower() == "Sales Return".ToLower()) || (voucherType.ToLower() == "Rejection In".ToLower() || voucherType.ToLower() == "Stock Transfer".ToLower()))
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = purchaseRate; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;// 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else if (outwardQty2 > 0 && voucherType.ToLower() == "Sales Order".ToLower())
                                                {
                                                    if (qtyBal == 0)
                                                    {
                                                        computedAverageRate = (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * 1;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                                else
                                                {
                                                    if (qtyBal != 0)
                                                    {
                                                        computedAverageRate = (stockValue / qtyBal);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                    else
                                                    {
                                                        computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                                        qtyBal += inwardQty2 - outwardQty2;
                                                        stockValue = computedAverageRate * qtyBal;
                                                    }
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Return".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Rejection In".ToLower() && inwardQty2 > 0)
                                            {   //added 18112016
                                                if (qtyBal == 0)
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * 1;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Sales Invoice".ToLower() && inwardQty2 > 0)
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;

                                                }
                                                else
                                                {
                                                    computedAverageRate = previousRunningAverage;// (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue = computedAverageRate * qtyBal;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Delivery Note".ToLower())
                                            {
                                                if (qtyBal != 0)
                                                {
                                                    computedAverageRate = (stockValue / qtyBal);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                                else
                                                {
                                                    computedAverageRate = (stockValue / 1);
                                                    qtyBal += inwardQty2 - outwardQty2;
                                                    stockValue += inwardQty2 * purchaseRate;
                                                }
                                            }
                                            else if (voucherType.ToLower() == "Opening Stock".ToLower())
                                            {
                                                // openingQuantityIn = 0;

                                                if (qtyBal != 0)
                                                {
                                                    //computedAverageRate = (stockValue / qtyBal);

                                                    // qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }
                                                else
                                                {
                                                    //computedAverageRate = (stockValue / 1);
                                                    //qtyBal = inwardQty2 /*- (inwardQty2 + outwardQty2)*/;
                                                    qtyBal = inwardQty2 - (outwardQty2);
                                                    stockValue = (inwardQty2 * rate2);
                                                }

                                                OpeningStockValue += (inwardQty2 * rate2);
                                            }
                                            else
                                            {
                                                // hndles other transactions such as purchase, opening balance, etc
                                                qtyBal += inwardQty2 - outwardQty2;
                                                stockValue += (inwardQty2 * rate2);

                                            }

                                            // ------------------------------------------------------ //
                                            //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;

                                            string avgCost = "";
                                            if (stockValue != 0 && qtyBal != 0)
                                            {
                                                avgCost = (stockValue / qtyBal).ToString("N2");
                                                //avgCost = (stockValue / qtyBal * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }
                                            else
                                            {
                                                avgCost = computedAverageRate.ToString("N2");
                                                //avgCost = (computedAverageRate/ SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2");
                                            }

                                            details.Add(new Models.Reports.OtherReports.StockReportRowItems
                                            {
                                                ProductId = dr["productId"].ToString(),
                                                ProductCode = dr["productCode"].ToString(),
                                                StoreName = dr["godownName"].ToString(),
                                                InwardQty = Convert.ToDecimal(dr["inwardQty"]).ToString("N2"),
                                                OutwardQty = ("-") + Convert.ToDecimal(dr["outwardQty"]).ToString("N2"),
                                                Rate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                                                AverageCost = avgCost,
                                                QtyBalance = qtyBal.ToString("N2"),
                                                StockValue = stockValue.ToString("N2"),
                                                VoucherTypeName = isAgainstVoucher == true ? againstVoucherType : voucherType,
                                                RefNo = refNo,
                                                Batch = string.IsNullOrEmpty(dr["Batch"].ToString()) ? "" : dr["Batch"].ToString(),
                                                Date = Convert.ToDateTime(dr["date"]).ToShortDateString(),
                                                ProductName = dr["productName"].ToString(),
                                                ProjectId = Convert.ToDecimal(dr["projectId"]),
                                                CategoryId = Convert.ToDecimal(dr["categoryId"]),
                                                UnitName = dr["unitName"].ToString()
                                            });

                                            isAgainstVoucher = false;

                                            stockValuePerItemPerStore = Convert.ToDecimal(stockValue);
                                            prevProductId = currentProductId;
                                            previousRunningAverage = Convert.ToDecimal(avgCost);
                                            inwardQtyPerStore += Convert.ToDecimal(dr["inwardQty"]);
                                            outwardQtyPerStore += Convert.ToDecimal(dr["outwardQty"]);
                                            i++;
                                        }
                                        // ===================== for every store for that item, put the summary =================== //
                                        details.Add(new Models.Reports.OtherReports.StockReportRowItems
                                        {
                                            ProductId = "",
                                            ProductCode = item,
                                            StoreName = storeName,
                                            InwardQty = (inwardQtyPerStore /*+ openingQuantityIn*/).ToString("N2"),
                                            OutwardQty = ("-") + (outwardQtyPerStore /*+ openingQuantityOut*/).ToString("N2"),
                                            Rate = "",
                                            AverageCost = "",
                                            QtyBalance = qtyBal.ToString("N2"),
                                            StockValue = stockValuePerItemPerStore.ToString("N2"),
                                            VoucherTypeName = "Sub total: ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = "",
                                        });

                                        totalStockValue += Convert.ToDecimal(stockValuePerItemPerStore.ToString("N2"));
                                        totalQtyPerStore += qtyBal /*(inwardQtyPerStore - outwardQtyPerStore) + openingQuantity*/;

                                        openingQuantity = 0;
                                        inwardQtyPerStore = 0;
                                        outwardQtyPerStore = 0;
                                        qtyBal = 0;
                                        stockValue = 0;
                                        stockValuePerItemPerStore = 0;
                                        //dgvStockDetailsReport.Rows.Add();
                                        i++;
                                    }

                                    dcOpeningStockForRollOver = 0; openingQuantity = 0; openingQuantityIn = 0; openingQuantityOut = 0;
                                    prevStoreId = store;
                                }
                            }
                        }
                        // *** if no transaction for that item for all stores for that period add the ockock for all stores per store *** //
                        else
                        {
                            foreach (var store in AllStores)
                            {
                                stockValuePerItemPerStore = 0;
                                dtblStore = new DataTable();
                                // ************************ insert opening stock per item per store ******************** //
                                dtblOpeningStockPerItemPerStore = new DataTable();
                                if (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item).Any())
                                {
                                    dtblOpeningStockPerItemPerStore = (dtblOpeningStocks.AsEnumerable().Where(l => l.Field<decimal>("godownId") == store && l.Field<string>("productCode") == item)).CopyToDataTable();

                                    if (dtblOpeningStockPerItemPerStore != null && dtblOpeningStockPerItemPerStore.Rows.Count > 0)
                                    {
                                        computedAverageRate = rate2;
                                        var itemParams = methodForStockValue.currentStockValue1235(lastFinYearStart, lastFinYearEnd, true, item, store);
                                        dcOpeningStockForRollOver = itemParams.Item1;
                                        openingQuantity = itemParams.Item2;
                                        openingQuantityIn = itemParams.Item3;
                                        openingQuantityOut = itemParams.Item4;
                                        qtyBal += openingQuantity;
                                        if (dcOpeningStockForRollOver != 0 && openingQuantity != 0)
                                        {
                                            //computedAverageRate = dcOpeningStockForRollOver / openingQuantity;
                                            //stockValue = openingQuantity * computedAverageRate;
                                        }
                                        var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(currentProductId).UnitconvertionId;
                                        details.Add(new Models.Reports.OtherReports.StockReportRowItems
                                        {
                                            ProductId = item,
                                            ProductCode = item,
                                            StoreName = (dtblOpeningStockPerItemPerStore.AsEnumerable().
                                                        Where(l => l.Field<string>("productCode") == item).Select(l => l.Field<string>("godownName")).FirstOrDefault()).ToString(),
                                            InwardQty = openingQuantityIn.ToString("N2"),
                                            OutwardQty = openingQuantityOut.ToString("N2"),
                                            Rate = "",
                                            AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity).ToString("N2") : "",
                                            //AverageCost = openingQuantity != 0 ? (dcOpeningStockForRollOver / openingQuantity * SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId)).ToString("N2") : "",
                                            QtyBalance = openingQuantity.ToString("N2"),
                                            StockValue = dcOpeningStockForRollOver.ToString("N2"),
                                            VoucherTypeName = "Opening Stock ",
                                            RefNo = "",
                                            Batch = "",
                                            Date = "",
                                            ProductName = productName,
                                            UnitName = ""
                                        });

                                        totalStockValue += Convert.ToDecimal(dcOpeningStockForRollOver.ToString("N2")); // dcOpeningStockForRollOver;
                                        dcOpeningStockForRollOver = 0;
                                        totalQtyPerStore += openingQuantity;
                                        openingQuantityIn = 0;
                                        openingQuantityOut = 0;
                                        openingQuantity = 0;
                                        i++;
                                    }
                                }
                            }
                        }
                    }
                }

                // ************************ Calculate Total **************************** //
                details.Add(new Models.Reports.OtherReports.StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var avgCost2 = "";

                if (totalQtyPerStore != 0)
                {
                    avgCost2 = (totalStockValue / totalQtyPerStore).ToString("N2");
                }

                details.Add(new Models.Reports.OtherReports.StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = avgCost2,
                    QtyBalance = totalQtyPerStore.ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    VoucherTypeName = "Total Stock Value:",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                details.Add(new Models.Reports.OtherReports.StockReportRowItems
                {
                    ProductId = "",
                    ProductCode = "",
                    StoreName = "",
                    InwardQty = "",
                    OutwardQty = "",
                    Rate = "",
                    AverageCost = "",
                    QtyBalance = "",
                    StockValue = "==========",
                    VoucherTypeName = "",
                    RefNo = "",
                    Batch = "",
                    Date = "",
                    ProductName = "",
                    UnitName = ""
                });

                var tyr = OpeningStockValue;

                master.TotalAverageCost = (totalStockValue / totalQtyPerStore);
                master.TotalQtyBalance = 0;
                master.TotalStockValue = 0;
                master.StockReportRowItem = details;
                return master.TotalAverageCost;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKRD8:" + ex.Message;
            }

            master.TotalAverageCost = 0;
            master.TotalQtyBalance = 0;
            master.TotalStockValue = 0;
            master.StockReportRowItem = details;
            return master.TotalAverageCost;
        }



        public HttpResponseMessage GetTaxAmount(string taxId, string amount)
        {

            decimal taxAmount = 0;
            TaxSP SpTax = new TaxSP();
            var amt = Convert.ToDecimal(amount);
            TaxInfo InfoTaxObj = SpTax.TaxView(Convert.ToDecimal(taxId));
            taxAmount = Math.Round(((amt * InfoTaxObj.Rate) / (100)), PublicVariables._inNoOfDecimalPlaces);
            return Request.CreateResponse(HttpStatusCode.OK, taxAmount);
        }

        private string generateVoucherNo()
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            StockJournalMasterSP spMaster = new StockJournalMasterSP();
            strVoucherNo = "0";
            strVoucherNo = obj.VoucherNumberAutomaicGeneration(decVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, TableName);

            if (Convert.ToDecimal(strVoucherNo) != spMaster.StockJournalMasterMaxPlusOne(decVoucherTypeId))
            {
                strVoucherNo = spMaster.StockJournalMasterMax(decVoucherTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, TableName);
                if (spMaster.StockJournalMasterMax(decVoucherTypeId).ToString() == "0")
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, TableName);
                }
            }
            //SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            //SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            //infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decVoucherTypeId, DateTime.Now);
            //strPrefix = infoSuffixPrefix.Prefix;
            //strSuffix = infoSuffixPrefix.Suffix;
            //strInvoiceNo = strPrefix + strVoucherNo + strSuffix;
            return strVoucherNo;
        }

        [HttpGet]
        public string GetAutoVoucherNo()
        {
            return generateVoucherNo();
        }

        [HttpGet]
        public HttpResponseMessage GetStockJournals(DateTime fromDate, DateTime toDate, string status = null)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            dynamic response = new ExpandoObject(); ;
            string GetQuery = string.Format("");
            GetQuery = string.Format("");
            if (status == "" || status == null)
            {
                GetQuery = string.Format("SELECT * FROM tbl_StockJournalMaster WHERE date BETWEEN '{0}' AND '{1}'ORDER BY stockJournalMasterId DESC", fromDate, toDate);
            }
            else
            {
                GetQuery = string.Format("SELECT * FROM tbl_StockJournalMaster WHERE date BETWEEN '{0}' AND '{1}' AND status = '{2}' ORDER BY stockJournalMasterId DESC", fromDate, toDate, status);
            }

            var result = db.GetDataSet(GetQuery);
            return Request.CreateResponse(HttpStatusCode.OK, (object)result);
        }

        [HttpGet]
        public async Task<IHttpActionResult> GetStockRecord(DateTime? fromDate = null, DateTime? toDate = null, int? projectId = null,
            int? storeId = null)
        {
            var begin = DateTime.UtcNow.AddDays(-1);
            var end = DateTime.UtcNow.AddDays(1);

            if (fromDate != null)
            {
                begin = Convert.ToDateTime(fromDate.Value);
            }

            if (toDate != null)
            {
                end = Convert.ToDateTime(toDate.Value);
            }

            //if (context.Database.Connection.State == ConnectionState.Closed)
            //    context.Database.Connection.Open();

            #region
            //var records = await context.tbl_StockJournalMaster.Where(pa => pa.date >= begin && pa.date <= end).ToListAsync();

            //var RecordsJMasterHeaders = new MatApi.Models.JMasterHeader();
            //var record = new MatApi.Models.JMasterHeader1();
            //var Headers = new MatApi.Models.FarmBuildActivitieModel();

            //foreach (var itm in records)
            //{
            //    var newHeader = new Models.JMasterHeader()
            //    {
            //        StockMaster = new Models.JMaster()
            //        {
            //            additionalCost = itm.additionalCost.Value,
            //            date = itm.date.Value,
            //            exchangeRateId = itm.exchangeRateId.Value,
            //            extra1 = itm.extra1,
            //            extra2 = itm.extra2,
            //            extraDate = itm.extraDate.Value,
            //            financialYearId = itm
            //        .financialYearId.Value,
            //            invoiceNo = itm
            //        .invoiceNo,
            //            narration = itm
            //        .narration,
            //            stockJournalMasterId = itm
            //        .stockJournalMasterId,
            //            suffixPrefixId = itm
            //        .suffixPrefixId.Value,
            //            voucherNo = itm
            //        .voucherNo,
            //            voucherTypeId = itm
            //        .voucherTypeId.Value
            //        },
            //    };

            //    var Jstockdetails = context.tbl_StockJournalDetails.Where(b => b.stockJournalMasterId == itm.stockJournalMasterId).ToList();

            //    if (Jstockdetails != null)
            //    {
            //        newHeader.StockDetail = new List<Models.JMasterDetails>();

            //        foreach (var itmdetails in Jstockdetails)
            //        {
            //            newHeader.StockDetail.Add(new Models.JMasterDetails() { amount = itmdetails.amount.Value, batchId = itmdetails.batchId.Value, categoryId = itmdetails.categoryId.Value, consumptionOrProduction = itmdetails.consumptionOrProduction, extra1 = itmdetails.extra1, extra2 = itmdetails.extra2, extraDate = itmdetails.extraDate.Value, godownId = itmdetails.godownId.Value, productId = itmdetails.productId.Value, projectId = itmdetails.projectId.Value, qty = itmdetails.qty.Value, rackId = itmdetails.rackId.Value, rate = itmdetails.rate.Value, slno = itmdetails.slno.Value, stockJournalDetailsId = itmdetails.stockJournalDetailsId, stockJournalMasterId = itmdetails.stockJournalDetailsId, unitConversionId = itmdetails.unitConversionId.Value, unitId = itmdetails.unitId.Value });
            //        }
            //    }

            //    Headers.record.Add(newHeader);

            //    Headers.record.ForEach(a => {

            //        record.StockMaster = a.StockMaster;

            //        a.StockDetail.ForEach(jd => {

            //            var prod = new Models.JMasterDetailsHead();
            //            prod.Stock = jd;

            //            var Product = context.tbl_Product.Where(b => b.productId == jd.productId).FirstOrDefault();
            //            if (Product != null)
            //            {
            //                prod.Product = new Models.Product()
            //                {
            //                    brandId = Product.brandId,
            //                    minimumStock = Product.minimumStock,
            //                    maximumStock = Product.maximumStock,
            //                    purchaseRate = Product.purchaseRate,
            //                    productCode = Product.productCode,
            //                    effectiveDate = Product.effectiveDate,
            //                    expenseAccount = Product.expenseAccount,
            //                    extra1 = Product.extra1,
            //                    extra2 = Product.extra2,
            //                    extraDate = Product.extraDate,
            //                    godownId = Product.godownId,
            //                    groupId = Product.groupId,
            //                    unitId = Product.unitId,
            //                    isActive = Product.isActive,
            //                    isallowBatch = Product.isallowBatch,
            //                    isBom = Product.isBom,
            //                    ismultipleunit = Product.ismultipleunit,
            //                    isopeningstock = Product.isopeningstock,
            //                    isshowRemember = Product.isshowRemember,
            //                    modelNoId = Product.modelNoId,
            //                    mrp = Product.mrp,
            //                    narration = Product.narration,
            //                    partNo = Product.partNo,
            //                    productId = Product.productId,
            //                    productName = Product.productName,
            //                    productType = Product.productType,
            //                    rackId = Product.rackId,
            //                    reorderLevel = Product.reorderLevel,
            //                    salesAccount = Product.salesAccount,
            //                    salesRate = Product.salesRate,
            //                    sizeId = Product.sizeId,
            //                    taxapplicableOn = Product.taxapplicableOn,
            //                    taxId = Product.taxId,
            //                };
            //            }

            //            var unit = context.tbl_Unit.Where(b => b.unitId == jd.unitId).FirstOrDefault();
            //            if (unit != null)
            //            {
            //                prod.Unit = new Models.Unit()
            //                {
            //                    extra1 = unit.extra1,
            //                    extra2 = unit.extra2,
            //                    extraDate = unit.extraDate,
            //                    unitId = unit.unitId,
            //                    narration = unit.narration,
            //                    formalName = unit.formalName,
            //                    noOfDecimalplaces = unit.noOfDecimalplaces,
            //                    unitName = unit.unitName
            //                };
            //            }

            //            var Project = context.tbl_Project.Where(b => b.ProjectId == jd.projectId).FirstOrDefault();
            //            if (Project != null)
            //            {
            //                prod.Project = new Models.Project()
            //                {
            //                    CloseDate = Project.CloseDate,
            //                    CreatedBy = Project.CreatedBy,
            //                    CreatedOn = Project.CreatedOn,
            //                    ModifiedBy = Project.ModifiedBy,
            //                    ModifiedOn = Project.ModifiedOn,
            //                    ProjectId = Project.ProjectId,
            //                    ProjectName = Project.ProjectName,
            //                    ProjectOwnerId = Project.ProjectOwnerId
            //                };
            //            }

            //            var Category = context.tbl_Category.Where(b => b.CategoryId == jd.categoryId).FirstOrDefault();
            //            if (Category != null)
            //            {
            //                prod.Category = new Models.Category()
            //                {
            //                    CreatedBy = Category.CreatedBy,
            //                    CreatedOn = Category.CreatedOn,
            //                    ModifiedBy = Category.ModifiedBy,
            //                    ModifiedOn = Category.ModifiedOn,
            //                    CategoryId = Category.CategoryId,
            //                    BelongsToCategoryId = Category.BelongsToCategoryId,
            //                    CategoryName = Category.CategoryName
            //                };
            //            }

            //            var Batch = context.tbl_Batch.Where(b => b.batchId == jd.batchId).FirstOrDefault();
            //            if (Batch != null)
            //            {
            //                prod.Batch = new Models.Batch()
            //                {
            //                    barcode = Batch.barcode,
            //                    batchId = Batch.batchId,
            //                    batchNo = Batch.batchNo,
            //                    expiryDate = Batch.expiryDate,
            //                    extra1 = Batch.extra1,
            //                    extra2 = Batch.extra2,
            //                    extraDate = Batch.extraDate,
            //                    manufacturingDate = Batch.manufacturingDate,
            //                    narration = Batch.narration,
            //                    partNo = Batch.partNo,
            //                    productId = Batch.productId,
            //                };
            //            }

            //            var Store = context.tbl_Godown.Where(b => b.godownId == jd.godownId).FirstOrDefault();
            //            if (Store != null)
            //            {
            //                prod.Store = new Models.Godown()
            //                {
            //                    extra1 = Store.extra1,
            //                    extra2 = Store.extra2,
            //                    extraDate = Store.extraDate,
            //                    narration = Store.narration,
            //                    godownId = Store.godownId,
            //                    godownName = Store.godownName
            //                };
            //            }

            //            var Bom = context.tbl_Bom.Where(b => b.rowmaterialId == jd.productId).FirstOrDefault();
            //            if (Bom != null)
            //            {
            //                prod.BOM = new Models.Bom()
            //                {
            //                    extra1 = Bom.extra1,
            //                    extra2 = Bom.extra2,
            //                    extraDate = Bom.extraDate,
            //                    bomId = Bom.bomId,
            //                    productId = Bom.productId,
            //                    quantity = Bom.quantity,
            //                    rowmaterialId = Bom.rowmaterialId,
            //                    unitId = Bom.unitId, 
            //                };
            //            }

            //            var AdditionalCosts = context.tbl_AdditionalCost.Where(b => b.stockJornalMasterId == a.StockMaster.stockJournalMasterId).ToList();
            //            if (AdditionalCosts != null)
            //            {
            //                prod.AdditionalCosts = new List<Models.AdditionalCts>();

            //                AdditionalCosts.ForEach(ac => {

            //                    var AddCosts = new Models.AdditionalCts();
            //                    AddCosts.AdditionalCost = ac;

            //                    var Aln = context.tbl_AccountLedger.Where(b => b.ledgerId == ac.ledgerId).FirstOrDefault();

            //                    if (Aln != null)
            //                    {
            //                        AddCosts.AccountLedgerName = Aln.ledgerName;
            //                    }

            //                   var Projectr = context.tbl_Project.Where(b => b.ProjectId == ac.projectId).FirstOrDefault();

            //                    if (Projectr != null)
            //                    {
            //                        AddCosts.Project = Projectr;
            //                    }

            //                    var Categoryr = context.tbl_Category.Where(b => b.CategoryId == ac.categoryId).FirstOrDefault();

            //                    if (Categoryr != null)
            //                    {
            //                        AddCosts.Category = Categoryr;
            //                    }

            //                    prod.AdditionalCosts.Add(AddCosts);
            //                });
            //            }

            //            record.StockDetail.Add(prod);
            //        });                 
            //    });
            // }
            #endregion

            #region
            var record = context.tbl_StockJournalMaster.Where(pa => pa.date >= begin && pa.date <= end).ToList()
                    .Select(a => new
                    {
                        StockMaster = a,
                        StockDetail = context.tbl_StockJournalDetails.Where(b => b.stockJournalMasterId == a.stockJournalMasterId).ToList()
                    }).Select(a => new
                    {
                        StockMaster = a.StockMaster,
                        StockDetail = a.StockDetail.Select(c => new
                        {
                            Stock = c,
                            Product = context.tbl_Product.Where(b => b.productId == c.productId).FirstOrDefault(),
                            Unit = context.tbl_Unit.Where(b => b.unitId == c.unitId).FirstOrDefault(),
                            Project = context.tbl_Project.Where(b => b.ProjectId == c.projectId).FirstOrDefault(),
                            Category = context.tbl_Category.Where(b => b.CategoryId == c.categoryId).FirstOrDefault(),
                            Batch = context.tbl_Batch.Where(b => b.batchId == c.batchId).FirstOrDefault(),
                            Store = context.tbl_Godown.Where(b => b.godownId == c.godownId).FirstOrDefault(),

                            BOM = context.tbl_Bom.Where(b => b.rowmaterialId == c.productId && b.productId == context.tbl_StockJournalDetails.Where(s => s.stockJournalMasterId == c.stockJournalMasterId && s.consumptionOrProduction.ToLower() == "Production".ToLower()).FirstOrDefault().productId).FirstOrDefault(),



                        }),

                        AdditionalCosts = context.tbl_AdditionalCost.Where(b => b.stockJornalMasterId == a.StockMaster.stockJournalMasterId).ToList()
                            .Select(d => new
                            {
                                AdditionalCost = d,
                                AccountLedgerName = context.tbl_AccountLedger.Where(b => b.ledgerId == d.ledgerId)
                                    .Select(b => b.ledgerName).FirstOrDefault(),
                                Project = context.tbl_Project.Where(b => b.ProjectId == d.projectId).FirstOrDefault(),
                                Category = context.tbl_Category.Where(b => b.CategoryId == d.categoryId).FirstOrDefault()
                            })
                    });
            #endregion

            return Ok(record);
        }

        [HttpPost]
        public IHttpActionResult Update(dynamic input)
        {
            if(input != null)
            {
                string sRate = input.Rate;
                string sMaster = input.JMasterId;
                string sDetails  = input.JMasterDetailsId;

                var DetailId = Convert.ToDecimal(sDetails);
                var MasterId = Convert.ToDecimal(sMaster);
                var Rate = Convert.ToDecimal(sRate);

                var SJDetails = context.tbl_StockJournalDetails.Where(s => s.stockJournalDetailsId == DetailId && s.stockJournalMasterId == MasterId).FirstOrDefault();
                if(SJDetails != null)
                {
                    SJDetails.rate = Rate;
                    context.Entry(SJDetails).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();

                    var StockPosting = context.tbl_StockPosting.Where(s => s.extra2 == SJDetails.stockJournalDetailsId.ToString() && s.extra1 == SJDetails.consumptionOrProduction).FirstOrDefault();

                    if(StockPosting != null)
                    {
                        StockPosting.rate = Rate;
                        context.Entry(StockPosting).State = System.Data.Entity.EntityState.Modified;
                        context.SaveChanges();
                    }

                }
            }
          
           return Ok("");
        }

        [HttpGet]
        public IHttpActionResult GetStockProfitabilityRecord(DateTime? fromDate = null, DateTime? toDate = null, int? projectId = null, int? bomId = null, int? storeId = null)
        {
            var begin = DateTime.UtcNow.AddDays(-1);
            var end = DateTime.UtcNow.AddDays(1);

            if (fromDate != null)
            {
                begin = Convert.ToDateTime(fromDate);
            }
            if (toDate != null)
            {
                end = Convert.ToDateTime(toDate);
                end = end.AddDays(+1).AddSeconds(-1);
            }

            IQueryable<tbl_StockJournalDetails> q = context.tbl_StockJournalDetails;

            q = q.Where(pa => pa.extraDate >= begin && pa.extraDate <= end);

            if (projectId != null && projectId != 0)
            {
                q = q.Where(p => p.projectId == projectId);
            }

            if (bomId != null && bomId != 0)
            {
                var ids = context.tbl_Bom.Where(b => b.productId == bomId).Select(e => e.rowmaterialId);
                q = q.Where(p => ids.Contains(p.productId) || p.productId == bomId);

                q = q.Where(p => !(p.productId != bomId && p.consumptionOrProduction == "Production"));

                q = q.Where(p => !(p.productId == bomId && p.consumptionOrProduction == "Consumption"));
            }

            //if (storeId != null)
            // {
            //    q = q.Where(p => p.godownId == storeId);
            //}
            // etc. the other parameters

            // var result = q.ToList();

            //e

            int year = DateTime.Now.Year;
            //DateTime firstDay = new DateTime(year, 1, 1);
            DateTime lastDay = new DateTime(year, 12, 31);
            int fromDateYear = Convert.ToDateTime(fromDate).Year;
            DateTime firstDay = new DateTime(fromDateYear, 1, 1);

            var record = context.tbl_StockJournalMaster.Where(pa => pa.extraDate >= begin && pa.extraDate <= end).ToList()
                    .Select(a => new
                    {
                        StockMaster = a,
                        StockDetail = q.Where(b => b.stockJournalMasterId == a.stockJournalMasterId).ToList()//context.tbl_StockJournalDetails.Where(b => b.stockJournalMasterId == a.stockJournalMasterId).ToList()
                    }).Select(a => new
                    {
                        StockMaster = a.StockMaster,
                        StockDetail = a.StockDetail.Select(c => new
                        {
                            Stock = c,
                            Product = context.tbl_Product.Where(b => b.productId == c.productId).FirstOrDefault(),
                            Unit = context.tbl_Unit.Where(b => b.unitId == c.unitId).FirstOrDefault(),
                            Project = context.tbl_Project.Where(b => b.ProjectId == c.projectId).FirstOrDefault(),
                            Category = context.tbl_Category.Where(b => b.CategoryId == c.categoryId).FirstOrDefault(),
                            Batch = context.tbl_Batch.Where(b => b.batchId == c.batchId).FirstOrDefault(),
                            Store = context.tbl_Godown.Where(b => b.godownId == c.godownId).FirstOrDefault(),
                            BOM = context.tbl_Bom.Where(b => b.rowmaterialId == c.productId).FirstOrDefault(),
                            AverageCost = context.tbl_StockPosting
                                            .Where(b => b.productId == c.productId && b.date >= PublicVariables._dtFromDate && b.date <= PublicVariables._dtToDate && b.inwardQty > 0).ToList()
                                             .Select(d => new { inwardQty = d.inwardQty, rate = d.rate })
                        }),
                        AdditionalCosts = context.tbl_AdditionalCost.Where(b => b.stockJornalMasterId == a.StockMaster.stockJournalMasterId).ToList()
                            .Select(d => new {
                                AdditionalCost = d,
                                AccountLedgerName = context.tbl_AccountLedger.Where(b => b.ledgerId == d.ledgerId)
                                    .Select(b => b.ledgerName).FirstOrDefault(),
                                Project = context.tbl_Project.Where(b => b.ProjectId == d.projectId).FirstOrDefault(),
                                Category = context.tbl_Category.Where(b => b.CategoryId == d.categoryId).FirstOrDefault()
                            })
                    });


            return Ok(record);

        }
        [HttpGet]
        public IHttpActionResult GetStockProfitabilityByDepartmentRecord(DateTime? fromDate = null, DateTime? toDate = null, int? categoryId = null,
         int? storeId = null)
        {
            var begin = DateTime.UtcNow;
            var end = DateTime.UtcNow;

            if (fromDate != null)
            {
                begin = Convert.ToDateTime(fromDate);
            }
            if (toDate != null)
            {
                end = Convert.ToDateTime(toDate);
                end = end.AddDays(+1).AddSeconds(-1);
            }
            //s
            IQueryable<tbl_StockJournalDetails> q = context.tbl_StockJournalDetails;

            q = q.Where(pa => pa.extraDate >= begin && pa.extraDate <= end);

            if (categoryId != null && categoryId != 0)
            {
                q = q.Where(p => p.categoryId == categoryId);
            }
            else
            {
                q = q.Where(p => p.categoryId != null);
            }
            //if (storeId != null)
            // {
            //    q = q.Where(p => p.godownId == storeId);
            //}
            // etc. the other parameters

            // var result = q.ToList();

            //e

            int year = DateTime.Now.Year;
            //DateTime firstDay = new DateTime(year, 1, 1);
            DateTime lastDay = new DateTime(year, 12, 31);
            int fromDateYear = Convert.ToDateTime(fromDate).Year;
            DateTime firstDay = new DateTime(fromDateYear, 1, 1);

            var record = context.tbl_StockJournalMaster.Where(pa => pa.extraDate >= begin && pa.extraDate <= end).ToList()
                    .Select(a => new
                    {
                        StockMaster = a,
                        StockDetail = q.Where(b => b.stockJournalMasterId == a.stockJournalMasterId).ToList()//context.tbl_StockJournalDetails.Where(b => b.stockJournalMasterId == a.stockJournalMasterId).ToList()
                    }).Select(a => new
                    {
                        StockMaster = a.StockMaster,
                        StockDetail = a.StockDetail.Select(c => new
                        {
                            Stock = c,
                            Product = context.tbl_Product.Where(b => b.productId == c.productId).SingleOrDefault(),
                            Unit = context.tbl_Unit.Where(b => b.unitId == c.unitId).SingleOrDefault(),
                            Project = context.tbl_Project.Where(b => b.ProjectId == c.projectId).SingleOrDefault(),
                            Category = context.tbl_Category.Where(b => b.CategoryId == c.categoryId).SingleOrDefault(),
                            Batch = context.tbl_Batch.Where(b => b.batchId == c.batchId).SingleOrDefault(),
                            Store = context.tbl_Godown.Where(b => b.godownId == c.godownId).SingleOrDefault(),
                            BOM = context.tbl_Bom.Where(b => b.rowmaterialId == c.productId).SingleOrDefault(),
                            AverageCost = context.tbl_StockPosting
                                            .Where(b => b.productId == c.productId && b.date >= PublicVariables._dtFromDate && b.date <= PublicVariables._dtToDate && b.inwardQty > 0).ToList()
                                             .Select(d => new { inwardQty = d.inwardQty, rate = d.rate })
                        }),
                        AdditionalCosts = context.tbl_AdditionalCost.Where(b => b.stockJornalMasterId == a.StockMaster.stockJournalMasterId).ToList()
                            .Select(d => new {
                                AdditionalCost = d,
                                AccountLedgerName = context.tbl_AccountLedger.Where(b => b.ledgerId == d.ledgerId)
                                    .Select(b => b.ledgerName).SingleOrDefault(),
                                Project = context.tbl_Project.Where(b => b.ProjectId == d.projectId).SingleOrDefault(),
                                Category = context.tbl_Category.Where(b => b.CategoryId == d.categoryId).SingleOrDefault()
                            })
                    });


            return Ok(record);

        }
        [HttpGet]

        public HttpResponseMessage GetPendingStockJournals(DateTime fromDate, DateTime toDate, string status = null)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            dynamic response = new ExpandoObject(); ;
            string GetQuery = string.Format("");
            GetQuery = string.Format("");
            if (status == "" || status == null)
            {
                GetQuery = string.Format("SELECT * FROM tbl_StockJournalMaster_Pending WHERE date BETWEEN '{0}' AND '{1}'ORDER BY stockJournalMasterId DESC", fromDate, toDate);
            }
            else
            {
                GetQuery = string.Format("SELECT * FROM tbl_StockJournalMaster_Pending WHERE date BETWEEN '{0}' AND '{1}' AND status = '{2}' ORDER BY stockJournalMasterId DESC", fromDate, toDate, status);
            }

            var result = db.GetDataSet(GetQuery);
            return Request.CreateResponse(HttpStatusCode.OK, (object)result);
        }
        [HttpGet]
        public HttpResponseMessage GetPendingStockJournalDetails(decimal id)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            dynamic response = new ExpandoObject(); ;
            string GetQuery1 = string.Format("SELECT * FROM tbl_StockJournalDetails_Pending WHERE stockJournalMasterId = {0} AND consumptionOrProduction='Production'", id);
            string GetQuery2 = string.Format("SELECT * FROM tbl_StockJournalDetails_Pending WHERE stockJournalMasterId = {0} AND consumptionOrProduction='Consumption'", id);
            string GetQuery3 = string.Format("SELECT * FROM tbl_StockJournalMaster_Pending WHERE stockJournalMasterId = {0}", id);

            response.production = db.customSelect(GetQuery1);
            response.consumption = db.customSelect(GetQuery2);
            response.master = db.customSelect(GetQuery3);
            response.products = new ProductSP().ProductViewAll();
            response.stores = new GodownSP().GodownViewAll();
            response.ledger = new AccountLedgerSP().AccountLedgerViewAll();
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetStockJournalDetails(decimal id)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            dynamic response = new ExpandoObject(); ;
            string GetQuery1 = string.Format("SELECT * FROM tbl_StockJournalDetails WHERE stockJournalMasterId = {0} AND consumptionOrProduction='Production'", id);
            string GetQuery2 = string.Format("SELECT * FROM tbl_StockJournalDetails  WHERE stockJournalMasterId = {0} AND consumptionOrProduction='Consumption'", id);
            string GetQuery3 = string.Format("SELECT * FROM tbl_StockJournalMaster WHERE stockJournalMasterId = {0}", id);

            response.production = db.customSelect(GetQuery1);
            response.consumption = db.customSelect(GetQuery2);
            response.master = db.customSelect(GetQuery3);
            response.products = new ProductSP().ProductViewAll();
            response.stores = new GodownSP().GodownViewAll();
            response.ledger = new AccountLedgerSP().AccountLedgerViewAll();
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetInTransitDetails(decimal userId, decimal transferId)
        {
            var user = new UserSP().UserView(userId);
            decimal warehouseId = Convert.ToDecimal(user.StoreId);
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            dynamic response = new ExpandoObject(); ;
            string GetQuery1 = string.Format("SELECT * FROM tbl_StockJournalDetails_Pending WHERE stockJournalMasterId = '{0}' AND godownId = {1} AND consumptionOrProduction='Production'", transferId, warehouseId);
            string GetQuery2 = string.Format("SELECT * FROM tbl_StockJournalDetails_Pending WHERE stockJournalMasterId = '{0}' AND consumptionOrProduction='Consumption'", transferId);
            response.production = db.customSelect(GetQuery1);
            response.consumption = db.customSelect(GetQuery2);
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpPost]
        public bool SavePending(StockJournalVM input)
        {
            bool result = false;
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string masterQuery = string.Format("INSERT INTO tbl_StockJournalMaster_Pending " +
                                                    "VALUES('{0}','{1}',{2},{3},{4},'{5}',{6},{7},'{8}')"
                                                    , input.Date,
                                                    input.StockJournalMasterInfo.Narration,
                                                    input.TotalAdditionalCost,
                                                    MATFinancials.PublicVariables._decCurrentFinancialYearId,
                                                    input.Currency,
                                                    "Pending",
                                                    input.AdditionalCostCashOrBankId,
                                                    Convert.ToDecimal(input.StockJournalMasterInfo.Extra2),
                                                    input.StockJournalMasterInfo.InvoiceNo);
            if (db.ExecuteNonQuery2(masterQuery))
            {
                string id = db.getSingleValue("SELECT top 1 stockJournalMasterId from tbl_StockJournalMaster_Pending order by stockJournalMasterId desc");
                foreach (var row in input.StockJournalDetailsInfoProduction)
                {
                    var amount = row.Rate * row.Qty;
                    string detailsQuery1 = string.Format("INSERT INTO tbl_StockJournalDetails_Pending " +
                                                "VALUES({0},{1},{2},{3},{4},{5},{6},'{7}',{8},'{9}',{10})"
                                                , id,
                                                row.ProductId,
                                                row.Qty,
                                                row.Rate,
                                                row.GodownId,
                                                row.RackId,
                                                amount,
                                                "Production",
                                                row.Slno,
                                                "Pending",
                                                0);
                    db.ExecuteNonQuery2(detailsQuery1);
                    amount = 0;
                }
                foreach (var row in input.StockJournalDetailsInfoConsumption)
                {
                    var amount = row.Rate * row.Qty;
                    string detailsQuery2 = string.Format("INSERT INTO tbl_StockJournalDetails_Pending " +
                                                "VALUES({0},{1},{2},{3},{4},{5},{6},'{7}',{8},'{9}',{10})"
                                                , id,
                                                row.ProductId,
                                                row.Qty,
                                                row.Rate,
                                                row.GodownId,
                                                row.RackId,
                                                amount,
                                                "Consumption",
                                                row.Slno,
                                                "",
                                                0);
                    db.ExecuteNonQuery2(detailsQuery2);
                    amount = 0;
                }
                //foreach (var row in input.AdditionalCostItems)
                //{
                //    string detailsQuery3 = string.Format("INSERT INTO tbl_AdditionalCost_Pending " +
                //                                "VALUES({0},{1},{2},{3})"
                //                                , row.LedgerId,
                //                                row.Amount,
                //                                0,
                //                                id);
                //    db.ExecuteNonQuery2(detailsQuery3);
                //}
                result = true;
            }
            return result;
        }

        [HttpGet]
        public bool updateQuantity(decimal idprod, decimal idcons, decimal quantity, decimal amount)
        {
            bool result = false;
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("UPDATE tbl_StockJournalDetails_Pending " +
                                            "SET qty={0}, amount={3} " +
                                            "WHERE consumptionOrProduction='{1}' and stockJournalDetailsId={2}", quantity, "Production", idprod, amount);
            if (conn.customUpdateQuery(queryStr) > 0)
            {
                string queryStr1 = string.Format("UPDATE tbl_StockJournalDetails_Pending " +
                                            "SET qty={0}, amount={3} " +
                                            "WHERE consumptionOrProduction='{1}' and stockJournalDetailsId={2}", quantity, "Consumption", idcons, amount);
                if (conn.customUpdateQuery(queryStr1) > 0)
                {
                    result = true;
                }
            }
            return result;
        }

        [HttpPost]
        public bool saveStockJournalInTransit(StockJournalVM input)
        {
            bool isSaved = false;

            StockJournalMasterInfo infoStockJournalMaster = new StockJournalMasterInfo();
            StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
            StockJournalDetailsInfo infoStockJournalDetailsConsumption = new StockJournalDetailsInfo();
            StockJournalDetailsInfo infoStockJournalDetailsProduction = new StockJournalDetailsInfo();
            StockJournalDetailsSP spStockJournalDetails = new StockJournalDetailsSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            StockPostingInfo infoStockPostingConsumption = new StockPostingInfo();
            StockPostingInfo infoStockPostingProduction = new StockPostingInfo();
            var spStockPosting = new MatApi.Services.StockPostingSP();
            AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
            strVoucherNo = generateVoucherNo();
            //if (isAutomatic == true)
            //{
            infoStockJournalMaster.SuffixPrefixId = decSuffixPrefixId;
            infoStockJournalMaster.VoucherNo = input.StockJournalMasterInfo.InvoiceNo;
            //}
            //else
            //{
            //    infoStockJournalMaster.SuffixPrefixId = 0;
            //    infoStockJournalMaster.VoucherNo = strVoucherNo;
            //}
            infoStockJournalMaster.ExtraDate = DateTime.Now;
            infoStockJournalMaster.InvoiceNo = input.StockJournalMasterInfo.InvoiceNo;
            infoStockJournalMaster.Date = input.Date;
            infoStockJournalMaster.AdditionalCost = input.TotalAdditionalCost;
            infoStockJournalMaster.VoucherNo = input.StockJournalMasterInfo.InvoiceNo;
            infoStockJournalMaster.VoucherTypeId = decVoucherTypeId;
            infoStockJournalMaster.Narration = input.StockJournalMasterInfo.Narration;
            infoStockJournalMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
            infoStockJournalMaster.ExchangeRateId = input.Currency;
            infoStockJournalMaster.Extra1 = input.StockJournalMasterInfo.Extra1;
            infoStockJournalMaster.Extra2 = input.pendingId.ToString();
            decStockMasterId = spStockJournalMaster.StockJournalMasterAdd(infoStockJournalMaster);
            if (decStockMasterId > 0)
            {
                isSaved = true;
            }
            else
            {
                isSaved = false;
            }

            if (input.StockJournalDetailsInfoConsumption.Count > 0)
            {
                foreach (var row in input.StockJournalDetailsInfoConsumption)
                {
                    var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(row.ProductId).UnitconvertionId;
                    infoStockJournalDetailsConsumption.StockJournalMasterId = decStockMasterId;
                    infoStockJournalDetailsConsumption.Extra1 = string.Empty;
                    infoStockJournalDetailsConsumption.Extra2 = string.Empty;
                    infoStockJournalDetailsConsumption.ExtraDate = DateTime.Now;
                    infoStockJournalDetailsConsumption.ProductId = row.ProductId;
                    infoStockJournalDetailsConsumption.Qty = row.Qty;
                    infoStockJournalDetailsConsumption.Rate = row.Rate;
                    infoStockJournalDetailsConsumption.UnitId = row.UnitId;
                    infoStockJournalDetailsConsumption.UnitConversionId = unitConvId;
                    infoStockJournalDetailsConsumption.BatchId = row.BatchId;
                    infoStockJournalDetailsConsumption.GodownId = row.GodownId;
                    infoStockJournalDetailsConsumption.RackId = row.RackId;
                    infoStockJournalDetailsConsumption.Amount = row.Amount;
                    infoStockJournalDetailsConsumption.ConsumptionOrProduction = "Consumption";
                    infoStockJournalDetailsConsumption.Slno = row.Slno;
                    isSaved = spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetailsConsumption);

                    //
                    infoStockPostingConsumption.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                    infoStockPostingConsumption.Date = input.Date;
                    infoStockPostingConsumption.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    infoStockPostingConsumption.GodownId = row.GodownId;
                    infoStockPostingConsumption.InwardQty = 0;
                    infoStockPostingConsumption.OutwardQty = infoStockJournalDetailsConsumption.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId);
                    infoStockPostingConsumption.ProductId = row.ProductId;
                    infoStockPostingConsumption.RackId = row.RackId;
                    infoStockPostingConsumption.Rate = row.Rate;
                    infoStockPostingConsumption.UnitId = row.UnitId;
                    infoStockPostingConsumption.InvoiceNo = input.StockJournalMasterInfo.InvoiceNo;
                    infoStockPostingConsumption.VoucherNo = input.StockJournalMasterInfo.InvoiceNo;
                    infoStockPostingConsumption.VoucherTypeId = decVoucherTypeId;
                    infoStockPostingConsumption.AgainstVoucherTypeId = 0;
                    infoStockPostingConsumption.AgainstInvoiceNo = "NA";
                    infoStockPostingConsumption.AgainstVoucherNo = "NA";
                    infoStockPostingConsumption.Extra1 = string.Empty;
                    infoStockPostingConsumption.Extra2 = string.Empty;
                    infoStockPostingConsumption.CategoryId = row.CategoryId;
                    infoStockPostingConsumption.ProjectId = row.ProjectId;


                    if (spStockPosting.StockPostingAdd(infoStockPostingConsumption) > 0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }

                }
                DBMatConnection conn = new DBMatConnection();
                string queryStr = string.Format("UPDATE tbl_StockJournalDetails_Pending " +
                                                "SET status='{0}', decStockJournalId={3} " +
                                                "WHERE consumptionOrProduction='{1}' and stockJournalMasterId={2}", "In-Transit", "Production", input.pendingId, decStockMasterId);
                if (conn.customUpdateQuery(queryStr) > 0)
                {
                    DBMatConnection conn2 = new DBMatConnection();
                    string queryStr2 = string.Format("UPDATE tbl_StockJournalMaster_Pending " +
                                                    "SET status='{0}'" +
                                                    "WHERE stockJournalMasterId={1} ", "In-Transit", input.pendingId);
                    if (conn2.customUpdateQuery(queryStr2) > 0)
                    {
                        return true;
                    }
                }
            }
            return isSaved;
        }

        [HttpGet]
        public HttpResponseMessage GetTransferInTransit(decimal id)
        {
            var user = new UserSP().UserView(id);
            decimal warehouseId = Convert.ToDecimal(user.StoreId);
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            List<StockTransferOnDeliveryVM> Transfers = new List<StockTransferOnDeliveryVM>();

            dynamic response = new ExpandoObject();
            string GetQuery = string.Format("select distinct stockJournalmasterId from tbl_StockJournaldetails_Pending where godownId = {0} AND consumptionOrProduction='Production' AND status='In-Transit'  ORDER BY stockJournalmasterId DESC", warehouseId);
            var details = db.customSelect(GetQuery);

            for (var t = 0; t < details.Rows.Count; t++)
            {
                string GetQuery1 = string.Format("select * from tbl_StockJournalmaster_Pending where stockJournalmasterId = {0}", details.Rows[t].ItemArray[0]);
                var master = db.customSelect(GetQuery1);
                Transfers.Add(new StockTransferOnDeliveryVM
                {
                    id = Convert.ToDecimal(master.Rows[0].ItemArray[0]),
                    date = Convert.ToDateTime(master.Rows[0].ItemArray[1]),
                    narration = master.Rows[0].ItemArray[2].ToString(),
                    userId = Convert.ToDecimal(master.Rows[0].ItemArray[8]),
                    status = master.Rows[0].ItemArray[6].ToString()
                });
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)Transfers);
        }

        [HttpPost]
        public bool saveStockTransferOnDelivery(StockJournalVM input)
        {
            bool isSaved = false;

            StockJournalMasterInfo infoStockJournalMaster = new StockJournalMasterInfo();
            StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
            StockJournalDetailsInfo infoStockJournalDetailsConsumption = new StockJournalDetailsInfo();
            StockJournalDetailsInfo infoStockJournalDetailsProduction = new StockJournalDetailsInfo();
            StockJournalDetailsSP spStockJournalDetails = new StockJournalDetailsSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            StockPostingInfo infoStockPostingConsumption = new StockPostingInfo();
            StockPostingInfo infoStockPostingProduction = new StockPostingInfo();
            var spStockPosting = new MatApi.Services.StockPostingSP();
            AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
            if (input.StockJournalDetailsInfoProduction.Count > 0)
            {
                foreach (var row in input.StockJournalDetailsInfoProduction)
                {
                    var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(row.ProductId).UnitconvertionId;
                    var unitId = new UnitSP().unitVieWForStandardRate(row.ProductId).UnitId;  //unitVieWForStandardRate is used to get unitid since no function returns
                                                                                              //unitinfo based on unit id

                    var master = new StockJournalMasterSP().StockJournalMasterView(input.pendingId);
                    infoStockJournalDetailsProduction.StockJournalMasterId = input.pendingId;
                    infoStockJournalDetailsProduction.Extra1 = string.Empty;
                    infoStockJournalDetailsProduction.Extra2 = string.Empty;
                    infoStockJournalDetailsProduction.ExtraDate = DateTime.Now;
                    infoStockJournalDetailsProduction.ProductId = row.ProductId;
                    infoStockJournalDetailsProduction.Qty = row.Qty;
                    infoStockJournalDetailsProduction.Rate = row.Rate;
                    infoStockJournalDetailsProduction.UnitId = unitId;
                    infoStockJournalDetailsProduction.UnitConversionId = unitConvId;
                    infoStockJournalDetailsProduction.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                    infoStockJournalDetailsProduction.GodownId = row.GodownId;
                    infoStockJournalDetailsProduction.RackId = row.RackId;
                    infoStockJournalDetailsProduction.Amount = row.Amount;
                    infoStockJournalDetailsProduction.ConsumptionOrProduction = "Production";
                    infoStockJournalDetailsProduction.Slno = row.Slno;
                    isSaved = spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetailsProduction);

                    infoStockPostingProduction.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                    infoStockPostingProduction.Date = input.Date;
                    infoStockPostingProduction.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    infoStockPostingProduction.GodownId = row.GodownId;
                    infoStockPostingProduction.InwardQty = row.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId);
                    infoStockPostingProduction.OutwardQty = 0;
                    infoStockPostingProduction.ProductId = row.ProductId;
                    infoStockPostingProduction.RackId = row.RackId;
                    infoStockPostingProduction.Rate = row.Rate;
                    infoStockPostingProduction.UnitId = unitId;
                    infoStockPostingProduction.InvoiceNo = master.InvoiceNo;
                    infoStockPostingProduction.VoucherNo = master.VoucherNo;
                    infoStockPostingProduction.VoucherTypeId = decVoucherTypeId;
                    infoStockPostingProduction.AgainstVoucherTypeId = 0;
                    infoStockPostingProduction.AgainstInvoiceNo = "NA";
                    infoStockPostingProduction.AgainstVoucherNo = "NA";
                    infoStockPostingProduction.Extra1 = string.Empty;
                    infoStockPostingProduction.Extra2 = string.Empty;
                    if (spStockPosting.StockPostingAdd(infoStockPostingProduction) > 0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                    DBMatConnection conn = new DBMatConnection();
                    string queryStr = string.Format("UPDATE tbl_StockJournalMaster_Pending " +
                                                    "SET status='{0}'" +
                                                    "WHERE stockJournalMasterId={1} ", "Delivered", input.AdditionalCostCashOrBankId);
                    conn.customUpdateQuery(queryStr);
                }
            }
            return isSaved;
        }

        [HttpPost]
        public bool SaveStockJournal12(StockJournalVM input)
        {
            bool isSaved = false;
            StockJournalMasterInfo infoStockJournalMaster = new StockJournalMasterInfo();
            StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
            StockJournalDetailsInfo infoStockJournalDetailsConsumption = new StockJournalDetailsInfo();
            StockJournalDetailsInfo infoStockJournalDetailsProduction = new StockJournalDetailsInfo();
            StockJournalDetailsSP spStockJournalDetails = new StockJournalDetailsSP();
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            StockPostingInfo infoStockPostingConsumption = new StockPostingInfo();
            StockPostingInfo infoStockPostingProduction = new StockPostingInfo();
            var spStockPosting = new MatApi.Services.StockPostingSP();
            AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
            strVoucherNo = generateVoucherNo();
            //if (isAutomatic == true)
            //{
            infoStockJournalMaster.SuffixPrefixId = decSuffixPrefixId;
            infoStockJournalMaster.VoucherNo = strVoucherNo;
            //}
            //else
            //{
            //    infoStockJournalMaster.SuffixPrefixId = 0;
            //    infoStockJournalMaster.VoucherNo = strVoucherNo;
            //}
            infoStockJournalMaster.ExtraDate = DateTime.Now;
            infoStockJournalMaster.InvoiceNo = strVoucherNo;
            infoStockJournalMaster.Date = input.Date;
            infoStockJournalMaster.AdditionalCost = input.TotalAdditionalCost;
            infoStockJournalMaster.VoucherNo = strVoucherNo;
            infoStockJournalMaster.VoucherTypeId = decVoucherTypeId;
            infoStockJournalMaster.Narration = input.StockJournalMasterInfo.Narration;
            infoStockJournalMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
            infoStockJournalMaster.ExchangeRateId = input.Currency;
            infoStockJournalMaster.Extra1 = input.StockJournalMasterInfo.Extra1;
            infoStockJournalMaster.Extra2 = input.StockJournalMasterInfo.Extra2;
            decStockMasterId = spStockJournalMaster.StockJournalMasterAdd(infoStockJournalMaster);
            if (decStockMasterId > 0)
            {
                isSaved = true;
            }
            else
            {
                isSaved = false;
            }

            if (input.StockJournalDetailsInfoConsumption.Count > 0)
            {
                foreach (var row in input.StockJournalDetailsInfoConsumption)
                {
                    infoStockJournalDetailsConsumption.Qty = row.Qty;
                    //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(row.ProductId).UnitconvertionId;
                    var unitConvId = row.UnitConversionId;
                    var tblStockJournalDetail = new tbl_StockJournalDetails();
                    tblStockJournalDetail.stockJournalMasterId = decStockMasterId;
                    tblStockJournalDetail.extra1 = string.Empty;
                    tblStockJournalDetail.extra2 = string.Empty;
                    tblStockJournalDetail.extraDate = DateTime.Now;
                    tblStockJournalDetail.productId = row.ProductId;
                    tblStockJournalDetail.qty = row.Qty;
                    tblStockJournalDetail.rate = row.Rate;
                    tblStockJournalDetail.unitId = row.UnitId;
                    tblStockJournalDetail.unitConversionId = unitConvId;
                    tblStockJournalDetail.batchId = row.BatchId;
                    tblStockJournalDetail.godownId = row.GodownId;
                    tblStockJournalDetail.rackId = row.RackId;
                    tblStockJournalDetail.amount = row.Amount;
                    tblStockJournalDetail.consumptionOrProduction = "Consumption";
                    tblStockJournalDetail.slno = row.Slno;
                    tblStockJournalDetail.projectId = row.ProjectId;
                    tblStockJournalDetail.categoryId = row.CategoryId;
                    context.tbl_StockJournalDetails.Add(tblStockJournalDetail);
                    context.SaveChanges();

                    //
                    //  isSaved= spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetailsConsumption);
                    // //
                    infoStockPostingConsumption.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                    infoStockPostingConsumption.Date = input.Date;
                    infoStockPostingConsumption.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    infoStockPostingConsumption.GodownId = row.GodownId;
                    infoStockPostingConsumption.InwardQty = 0;
                    infoStockPostingConsumption.OutwardQty = infoStockJournalDetailsConsumption.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId);
                    infoStockPostingConsumption.ProductId = row.ProductId;
                    infoStockPostingConsumption.RackId = row.RackId;
                    infoStockPostingConsumption.Rate = row.Rate;
                    infoStockPostingConsumption.UnitId = row.UnitId;
                    infoStockPostingConsumption.InvoiceNo = strVoucherNo;
                    infoStockPostingConsumption.VoucherNo = strVoucherNo;
                    infoStockPostingConsumption.VoucherTypeId = decVoucherTypeId;
                    infoStockPostingConsumption.AgainstVoucherTypeId = 0;
                    infoStockPostingConsumption.AgainstInvoiceNo = "NA";
                    infoStockPostingConsumption.AgainstVoucherNo = "NA";
                    infoStockPostingConsumption.Extra1 = string.Empty;
                    infoStockPostingConsumption.Extra2 = string.Empty;
                    infoStockPostingConsumption.CategoryId = row.CategoryId;
                    infoStockPostingConsumption.ProjectId = row.ProjectId;
                    if (row.HasBatchSelected)
                    {
                        infoStockPostingConsumption.BatchId = row.BatchId;
                    }
                    if (spStockPosting.StockPostingAdd(infoStockPostingConsumption) > 0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                }
            }

            if (input.StockJournalDetailsInfoProduction.Count > 0)
            {
                foreach (var row in input.StockJournalDetailsInfoProduction)
                {
                    //var unitConvId = new UnitConvertionSP().UnitViewAllByProductId(row.ProductId).UnitconvertionId;
                    var unitConvId = row.UnitConversionId;
                    var unitId = new UnitSP().unitVieWForStandardRate(row.ProductId).UnitId;  //unitVieWForStandardRate is used to get unitid since no function returns
                                                                                              //unitinfo based on unit id
                    var tblStockJournalDetail = new tbl_StockJournalDetails();
                    tblStockJournalDetail.stockJournalMasterId = decStockMasterId;
                    tblStockJournalDetail.extra1 = string.Empty;
                    tblStockJournalDetail.extra2 = string.Empty;
                    tblStockJournalDetail.extraDate = DateTime.Now;
                    tblStockJournalDetail.productId = row.ProductId;
                    tblStockJournalDetail.qty = row.Qty;
                    tblStockJournalDetail.rate = row.Rate;
                    tblStockJournalDetail.unitId = row.UnitId;
                    tblStockJournalDetail.unitConversionId = unitConvId;
                    tblStockJournalDetail.batchId = row.BatchId;
                    tblStockJournalDetail.godownId = row.GodownId;
                    tblStockJournalDetail.rackId = row.RackId;
                    tblStockJournalDetail.amount = row.Amount;
                    tblStockJournalDetail.consumptionOrProduction = "Production";
                    tblStockJournalDetail.slno = row.Slno;
                    tblStockJournalDetail.projectId = row.ProjectId;
                    tblStockJournalDetail.categoryId = row.CategoryId;
                    context.tbl_StockJournalDetails.Add(tblStockJournalDetail);
                    context.SaveChanges();

                    //infoStockJournalDetailsProduction.StockJournalMasterId = decStockMasterId;
                    //infoStockJournalDetailsProduction.Extra1 = string.Empty;
                    //infoStockJournalDetailsProduction.Extra2 = string.Empty;
                    //infoStockJournalDetailsProduction.ExtraDate = DateTime.Now;
                    //infoStockJournalDetailsProduction.ProductId = row.ProductId;
                    //infoStockJournalDetailsProduction.Qty = row.Qty;
                    //infoStockJournalDetailsProduction.Rate = row.Rate;
                    //infoStockJournalDetailsProduction.UnitId = unitId;
                    //infoStockJournalDetailsProduction.UnitConversionId = unitConvId;
                    //infoStockJournalDetailsProduction.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                    //infoStockJournalDetailsProduction.GodownId = row.GodownId;
                    //infoStockJournalDetailsProduction.RackId = row.RackId;
                    //infoStockJournalDetailsProduction.Amount = row.Amount;
                    //infoStockJournalDetailsProduction.ConsumptionOrProduction = "Production";
                    //infoStockJournalDetailsProduction.Slno = row.Slno;
                    //infoStockJournalDetailsProduction.CategoryId = row.CategoryId;
                    //infoStockJournalDetailsProduction.ProjectId = row.ProjectId;

                    //isSaved = spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetailsProduction);

                    infoStockPostingProduction.BatchId = new BatchSP().BatchIdViewByProductId(row.ProductId);
                    infoStockPostingProduction.Date = input.Date;
                    infoStockPostingProduction.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    infoStockPostingProduction.GodownId = row.GodownId;
                    infoStockPostingProduction.InwardQty = row.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(unitConvId);
                    infoStockPostingProduction.OutwardQty = 0;
                    infoStockPostingProduction.ProductId = row.ProductId;
                    infoStockPostingProduction.RackId = row.RackId;
                    infoStockPostingProduction.Rate = row.Rate;
                    infoStockPostingProduction.UnitId = unitId;
                    infoStockPostingProduction.InvoiceNo = strVoucherNo;
                    infoStockPostingProduction.VoucherNo = strVoucherNo;
                    infoStockPostingProduction.VoucherTypeId = decVoucherTypeId;
                    infoStockPostingProduction.AgainstVoucherTypeId = 0;
                    infoStockPostingProduction.AgainstInvoiceNo = "NA";
                    infoStockPostingProduction.AgainstVoucherNo = "NA";
                    infoStockPostingProduction.CategoryId = row.CategoryId;
                    infoStockPostingProduction.ProjectId = row.ProjectId;
                    infoStockPostingProduction.Extra1 = string.Empty;
                    infoStockPostingProduction.Extra2 = string.Empty;
                    if (!string.IsNullOrWhiteSpace(row.NewBatchName))
                    {
                        var newBatch = new tbl_Batch()
                        {
                            batchNo = row.NewBatchName,
                            manufacturingDate = DateTime.UtcNow,
                            productId = row.ProductId,
                        };
                        context.tbl_Batch.Add(newBatch);
                        context.SaveChanges();
                        infoStockPostingProduction.BatchId = newBatch.batchId;
                    }
                    if (spStockPosting.StockPostingAdd(infoStockPostingProduction) > 0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                }
            }

            decimal decGrandTotal = 0;
            decimal decRate = 0;
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            if (input.AdditionalCostItems.Count > 0)
            {
                //infoAdditionalCost.Credit = input.TotalAdditionalCost;
                //infoAdditionalCost.Debit = 0;
                //infoAdditionalCost.LedgerId = input.AdditionalCostCashOrBankId;
                //infoAdditionalCost.VoucherNo = strVoucherNo;
                //infoAdditionalCost.VoucherTypeId = decVoucherTypeId;
                //infoAdditionalCost.Extra1 = string.Empty;
                //infoAdditionalCost.Extra2 = string.Empty;
                //infoAdditionalCost.ExtraDate = DateTime.Now;

                //infoAdditionalCost.ProjectId id= input.
                //context.tbl_AdditionalCost.Add(new tbl_AdditionalCost
                //{
                //    ledgerId = infoAdditionalCost.LedgerId,
                //    extra2 = infoAdditionalCost.Extra2,
                //    extraDate = infoAdditionalCost.ExtraDate,
                //    extra1 = infoAdditionalCost.Extra1,
                //    voucherNo = infoAdditionalCost.VoucherNo,
                //    voucherTypeId = infoAdditionalCost.VoucherTypeId,
                //    credit = infoAdditionalCost.Credit,
                //    debit = infoAdditionalCost.Debit,
                //});
                //context.SaveChanges();
                //isSaved = true;
                //isSaved= spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                //....Ledger Posting Add...///
                //-------------------  Currency Conversion-----------------------------
                //decGrandTotal = input.TotalAdditionalCost;
                //decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(input.Currency);
                //decGrandTotal = decGrandTotal * decRate;
                ////---------------------------------------------------------------
                //infoLedgerPosting.Credit = decGrandTotal;
                //infoLedgerPosting.Debit = 0;
                //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);
                //infoLedgerPosting.DetailsId = 0;
                //infoLedgerPosting.InvoiceNo = strVoucherNo;
                //infoLedgerPosting.LedgerId = input.AdditionalCostCashOrBankId;
                //infoLedgerPosting.VoucherNo = strVoucherNo;
                //infoLedgerPosting.VoucherTypeId = decVoucherTypeId;
                //infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //infoLedgerPosting.ChequeDate = DateTime.Now;
                //infoLedgerPosting.ChequeNo = string.Empty;
                //infoLedgerPosting.Extra1 = string.Empty;
                //infoLedgerPosting.Extra2 = string.Empty;
                //infoLedgerPosting.ExtraDate = DateTime.Now;
                //if(spLedgerPosting.LedgerPostingAdd(infoLedgerPosting)>0)
                //{
                //    isSaved = true;
                //}
                //else
                //{
                //    isSaved = false;
                //}
                foreach (var row in input.AdditionalCostItems)
                {
                    /*-----------------------------------------Additional Cost Add----------------------------------------------------*/
                    infoAdditionalCost.Credit = 0;
                    infoAdditionalCost.Debit = row.Amount;
                    infoAdditionalCost.LedgerId = row.LedgerId;
                    infoAdditionalCost.VoucherNo = strVoucherNo;
                    infoAdditionalCost.VoucherTypeId = decVoucherTypeId;
                    infoAdditionalCost.StockJournalMasterId = decStockMasterId;
                    infoAdditionalCost.Extra1 = string.Empty;
                    infoAdditionalCost.Extra2 = string.Empty;
                    infoAdditionalCost.ExtraDate = DateTime.Now;
                    context.tbl_AdditionalCost.Add(new tbl_AdditionalCost()
                    {
                        ledgerId = row.LedgerId,
                        extra2 = "",
                        extraDate = DateTime.Now,
                        extra1 = "",
                        credit = 0,
                        debit = row.Amount,
                        voucherNo = strVoucherNo,
                        voucherTypeId = decVoucherTypeId,
                        projectId = row.ProjectId,
                        categoryId = row.CategoryId,
                        stockJornalMasterId = decStockMasterId
                    });
                    context.SaveChanges();
                    isSaved = true;
                    //isSaved= spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                    /*-----------------------------------------Additional Cost Ledger Posting----------------------------------------------------*/
                    decimal decTotal = 0;
                    //-------------------  Currency Conversion------------------------
                    decTotal = Convert.ToDecimal(infoAdditionalCost.Debit);
                    decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(input.Currency);
                    decTotal = decTotal * decRate;
                    //---------------------------------------------------------------
                    infoLedgerPosting.Credit = 0;
                    infoLedgerPosting.Debit = decTotal;
                    infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);
                    infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.InvoiceNo = strVoucherNo;
                    infoLedgerPosting.LedgerId = row.LedgerId;
                    infoLedgerPosting.VoucherNo = strVoucherNo;
                    infoLedgerPosting.VoucherTypeId = decVoucherTypeId;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    infoLedgerPosting.ExtraDate = DateTime.Now;
                    if (spLedgerPosting.LedgerPostingAdd(infoLedgerPosting) > 0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                }
            }
            //if (input.StockJournalMasterInfo.Extra1 == "Stock Transfer")
            //{
            //    DBMatConnection conn = new DBMatConnection();
            //    string queryStr = string.Format("UPDATE tbl_StockJournalMaster_Pending " +
            //                                    "SET status='{0}'" +
            //                                    "WHERE stockJournalMasterId={1} ", "Approved", input.StockJournalMasterInfo.Extra2);
            //    if (conn.customUpdateQuery(queryStr) > 0)
            //    {
            //        return true;
            //    }
            //}

            return isSaved;
        }

        private decimal getQuantityInStock(decimal productId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format(" SELECT  convert(decimal(18, 2), (ISNULL(SUM(s.inwardQty), 0) - ISNULL(SUM(s.outwardQty), 0))) AS Currentstock " +
                " FROM tbl_StockPosting s inner join tbl_product p on p.productId = s.productId " +
                " INNER JOIN tbl_Batch b on s.batchId = b.batchId " +
                " WHERE p.productId = '{0}' AND s.date <= '{1}' ", productId, MATFinancials.PublicVariables._dtToDate);
            return Convert.ToDecimal(conn.getSingleValue(queryStr));
        }

        public void SaveOrEdit(SockJournalModel obj)
        {
            bool Isexit = false;
            StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
            try
            {
                string strVoucherNo;
                // to take assign voucher number in case automatic voucher numbering is set to off urefe 20161206
                if (true)//(!isAutomatic && txtVoucherNo.Text.Trim() != string.Empty)
                {
                    strVoucherNo = ""; //txtVoucherNo.Text.Trim();
                }
                //dgvConsumption.ClearSelection();
                //dgvProduction.ClearSelection();
                //dgvAdditionalCost.ClearSelection();
                int inRowConsumption = obj.ConsumptionItems.Count;
                int inRowProduction = obj.SrcLineItems.Count;

                if (false)//(txtVoucherNo.Text.Trim() == string.Empty)
                {
                    //Messages.InformationMessage("Enter voucher number");
                    //txtVoucherNo.Focus();
                }
                else if (false) //()(spStockJournalMaster.StockJournalInvoiceNumberCheckExistence(txtVoucherNo.Text.Trim(), 0, decVoucherTypeId) == true && btnSave.Text == "Save")
                {
                    //Messages.InformationMessage("Invoice number already exist");
                    //txtVoucherNo.Focus();
                }
                else if (false)//(obj.TransDate.Trim() == string.Empty)
                {
                    //Messages.InformationMessage("Select a date in between financial year");
                    //txtDate.Focus();
                }
                else if (false)//(obj.Currency < 1)
                {
                    //Messages.InformationMessage("Select any currency");
                    //cmbCurrency.Focus();
                }
                else
                {
                    if (false)//(rbtnManufacturing.Checked)
                    {
                        //if (inRowConsumption - 1 == 0)
                        //{
                        //    Messages.InformationMessage("Can't save Stock Journal without atleast one product with complete details");
                        //    dgvConsumption.Focus();
                        //    goto Exit;
                        //}
                        //if (inRowProduction - 1 == 0)
                        //{
                        //    Messages.InformationMessage("Can't save Stock Journal without atleast one product with complete details");
                        //    dgvProduction.Focus();
                        //    goto Exit;
                        //}

                    }
                    if (true)//(rbtnTransfer.Checked)
                    {
                        int indgvRowsConsumption = obj.ConsumptionItems.Count;
                        int indgvRowsProduction = obj.SrcLineItems.Count;

                        if (inRowConsumption - 1 == 0)
                        {
                            //Messages.InformationMessage("Can't save Stock Journal without atleast one product with complete details");
                            //dgvConsumption.Focus();
                            //goto Exit;
                        }
                        if (inRowProduction - 1 == 0)
                        {
                            //Messages.InformationMessage("Can't save Stock Journal without atleast one product with complete details");
                            //dgvProduction.Focus();
                            //goto Exit;
                        }
                        if (indgvRowsConsumption != indgvRowsProduction)
                        {
                            //Messages.InformationMessage("Please Tranfer the details");
                            //goto Exit;
                        }
                        int indexCounter = 0;
                        foreach (var productionObj in obj.SrcLineItems)
                        {
                            decimal dcConsumption = 0;
                            decimal dcProduction = 0;
                            dcProduction = Convert.ToDecimal(productionObj.Qty);
                            dcConsumption = Convert.ToDecimal(obj.ConsumptionItems[indexCounter].Qty);
                            if (productionObj.Store < 1)
                            {
                                //Messages.InformationMessage("Rows Contains Invalid entries please fill the store Details");
                                //goto Exit;
                            }
                            if (productionObj.Store == obj.ConsumptionItems[indexCounter].Store)
                            {
                                //Messages.InformationMessage(" The Godown should be different");
                                //dgvProduction.Focus();
                                //Isexit = true;
                                //break;
                            }
                            indexCounter++;
                            if (dcConsumption != dcProduction)
                            {

                                //Messages.InformationMessage("The Quantity Should be Same");
                                //goto Exit;
                            }

                        }
                    }
                    if (false)//(rbtnStockOut.Checked)
                    {
                        //if (inRowConsumption - 1 == 0)
                        //{
                        //Messages.InformationMessage("Can't save Stock Journal without atleast one product with complete details");
                        //dgvConsumption.Focus();
                        //goto Exit;
                        //}
                    }
                    if (true)//(!Isexit)
                    {
                        if (true)//(RemoveIncompleteRowsFromConsumptionGrid())
                        {
                            if (true)//(!rbtnStockOut.Checked)
                            {
                                if (true)//(RemoveIncompleteRowsFromProductionGrid())
                                {
                                    if (obj.ConsumptionItems.Count < 1)
                                    {
                                        //MessageBox.Show("Can't save Stock Journal without atleast one product with complete details", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                        //dgvConsumption.ClearSelection();
                                        //dgvConsumption.Focus();
                                        //goto Exit;
                                    }
                                    else
                                    {
                                        //if (btnSave.Text == "Save")
                                        // {

                                        // if (Messages.SaveConfirmation())
                                        //{
                                        //grandTotalAmountCalculationConsumption();
                                        //grandTotalAmountCalculationProduction();
                                        Save(obj);
                                        // }

                                        //}
                                        //if (btnSave.Text == "Update")
                                        //{
                                        //    if (Messages.UpdateConfirmation())
                                        //    {
                                        //        grandTotalAmountCalculationConsumption();
                                        //        grandTotalAmountCalculationProduction();
                                        //        Save();
                                        //    }
                                        //}
                                    }
                                }
                            }
                            else
                            {
                                //if (dgvConsumption.Rows[0].Cells["dgvtxtConsumptionProductName"].Value == null || dgvConsumption.Rows[0].Cells["dgvtxtConsumptionProductName"].Value.ToString() == string.Empty && dgvConsumption.Rows[0].Cells["dgvtxtConsumptionQty"].Value == null)
                                //{
                                //    MessageBox.Show("Can't save Stock Journals without atleast one product with complete details", "MAT Financials", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                //    dgvConsumption.ClearSelection();
                                //    dgvConsumption.Focus();
                                //    goto Exit;
                                //}
                                //if (btnSave.Text == "Save")
                                //{
                                //    if (Messages.SaveConfirmation())
                                //    {
                                //        grandTotalAmountCalculationConsumption();
                                //        Save();
                                //    }
                                //}
                                //if (btnSave.Text == "Update")
                                //{
                                //    if (Messages.UpdateConfirmation())
                                //    {
                                //        grandTotalAmountCalculationConsumption();
                                //        Save();

                                //    }
                                //}

                            }
                        }
                    }
                }


            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "SJ40:" + ex.Message;
            }
        }

        public void Save(SockJournalModel obj)
        {
            try
            {

                StockJournalMasterInfo infoStockJournalMaster = new StockJournalMasterInfo();
                StockJournalMasterSP spStockJournalMaster = new StockJournalMasterSP();
                StockJournalDetailsInfo infoStockJournalDetails = new StockJournalDetailsInfo();
                StockJournalDetailsSP spStockJournalDetails = new StockJournalDetailsSP();
                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                StockPostingInfo infoStockPosting = new StockPostingInfo();
                var spStockPosting = new MatApi.Services.StockPostingSP();
                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
                AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
                decimal decStockMasterId;
                if (true)//(isAutomatic == true)
                {
                    // infoStockJournalMaster.SuffixPrefixId = decSuffixPrefixId;
                    //infoStockJournalMaster.VoucherNo = strVoucherNo;
                }
                else
                {
                    infoStockJournalMaster.SuffixPrefixId = 0;
                    infoStockJournalMaster.VoucherNo = "";// strVoucherNo;
                }
                infoStockJournalMaster.ExtraDate = DateTime.Now;
                infoStockJournalMaster.InvoiceNo = "";//txtVoucherNo.Text.Trim();
                infoStockJournalMaster.Date = Convert.ToDateTime(obj.TransDate);
                infoStockJournalMaster.AdditionalCost = Convert.ToDecimal(obj.AdditionalAmountTotal);
                infoStockJournalMaster.VoucherNo = ""; //strVoucherNo;
                infoStockJournalMaster.VoucherTypeId = 1;// decVoucherTypeId;
                infoStockJournalMaster.Narration = obj.Narration.Trim();
                infoStockJournalMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoStockJournalMaster.ExchangeRateId = Convert.ToDecimal(obj.Currency);
                if (false)//(rbtnManufacturing.Checked)
                {
                    infoStockJournalMaster.Extra1 = "Manufacturing";
                }
                if (true) //(rbtnTransfer.Checked)
                {
                    infoStockJournalMaster.Extra1 = "Stock Transfer";
                }
                if (false)// (rbtnStockOut.Checked)
                {
                    infoStockJournalMaster.Extra1 = "Stock Out";
                }
                infoStockJournalMaster.Extra2 = string.Empty;
                if (true)//(btnSave.Text == "Save")
                {
                    decStockMasterId = spStockJournalMaster.StockJournalMasterAdd(infoStockJournalMaster);
                }
                else
                {
                    //infoStockJournalMaster.StockJournalMasterId = decStockJournalMasterIdForEdit;
                    //spStockJournalMaster.StockJournalMasterEdit(infoStockJournalMaster);
                    //RemoveRowStockJournalConsumptionDetails();
                    //RemoveRowStockJournalProductionDetails();
                    //if (rbtnManufacturing.Checked)
                    //{
                    //    //if (cmbFinishedGoods.SelectedIndex != 0 && txtQty.Text != string.Empty)
                    //    //{
                    //    //    txtQty_Leave(sender,e);
                    //    //}
                    //}
                    //RemoveRowStockJournalAdditionalCostDetails();
                    //spStockPosting.DeleteStockPostingForStockJournalEdit(strVoucherNo, decVoucherTypeId);
                }

                if (obj.ConsumptionItems.Count > 0)
                {
                    int inCount = obj.ConsumptionItems.Count;
                    int indexConsumptionCounter = 0;
                    foreach (var consumptionObj in obj.ConsumptionItems)
                    {
                        if (true)//(btnSave.Text == "Save")
                        {
                            infoStockJournalDetails.StockJournalMasterId = decStockMasterId;
                        }
                        else
                        {
                            infoStockJournalMaster.StockJournalMasterId = 1;//decStockJournalMasterIdForEdit;
                        }
                        infoStockJournalDetails.Extra1 = string.Empty;
                        infoStockJournalDetails.Extra2 = string.Empty;
                        infoStockJournalDetails.ExtraDate = DateTime.Now;
                        infoStockJournalDetails.ProductId = Convert.ToDecimal(consumptionObj.ProductCode);
                        infoStockJournalDetails.Qty = Convert.ToDecimal(consumptionObj.Qty);
                        infoStockJournalDetails.Rate = Convert.ToDecimal(consumptionObj.Rate);
                        infoStockJournalDetails.UnitId = Convert.ToDecimal(consumptionObj.Unit);
                        infoStockJournalDetails.UnitConversionId = 1;//Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionunitConversionId"].Value);
                        if (false)//(btnSave.Text == "Update")
                        {
                            //infoStockJournalDetails.StockJournalMasterId = decStockJournalMasterIdForEdit;
                            //if (dgvConsumption.Rows[i].Cells["dgvtxtConsumptionStockJournalDetailsId"].Value == null || dgvConsumption.Rows[i].Cells["dgvtxtConsumptionStockJournalDetailsId"].Value.ToString() == string.Empty)
                            //{

                            //    if (dgvConsumption.Rows[i].Cells["dgvcmbConsumptionBatch"].Value != null && dgvConsumption.Rows[i].Cells["dgvcmbConsumptionBatch"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.BatchId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvcmbConsumptionBatch"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.BatchId = 0;
                            //    }
                            //    if (dgvConsumption.Rows[i].Cells["dgvcmbConsumptionGodown"].Value != null && dgvConsumption.Rows[i].Cells["dgvcmbConsumptionGodown"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.GodownId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvcmbConsumptionGodown"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.GodownId = 0;
                            //    }
                            //    if (dgvConsumption.Rows[i].Cells["dgvcmbConsumptionRack"].Value != null && dgvConsumption.Rows[i].Cells["dgvcmbConsumptionRack"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.RackId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvcmbConsumptionRack"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.RackId = 0;
                            //    }
                            //    infoStockJournalDetails.Amount = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionAmount"].Value);
                            //    infoStockJournalDetails.ConsumptionOrProduction = "Consumption";
                            //    infoStockJournalDetails.Slno = Convert.ToInt32(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionSlNo"].Value);
                            //    spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetails);
                            //}
                            //else
                            //{
                            //    infoStockJournalDetails.StockJournalDetailsId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionStockJournalDetailsId"].Value);
                            //    if (dgvConsumption.Rows[i].Cells["dgvcmbConsumptionBatch"].Value != null && dgvConsumption.Rows[i].Cells["dgvcmbConsumptionBatch"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.BatchId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvcmbConsumptionBatch"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.BatchId = 0;
                            //    }
                            //    if (dgvConsumption.Rows[i].Cells["dgvcmbConsumptionGodown"].Value != null && dgvConsumption.Rows[i].Cells["dgvcmbConsumptionGodown"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.GodownId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvcmbConsumptionGodown"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.GodownId = 0;
                            //    }
                            //    if (dgvConsumption.Rows[i].Cells["dgvcmbConsumptionRack"].Value != null && dgvConsumption.Rows[i].Cells["dgvcmbConsumptionRack"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.RackId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvcmbConsumptionRack"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.RackId = 0;
                            //    }
                            //    infoStockJournalDetails.Amount = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionAmount"].Value);
                            //    infoStockJournalDetails.ConsumptionOrProduction = "Consumption";
                            //    infoStockJournalDetails.Slno = Convert.ToInt32(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionSlNo"].Value);
                            //    spStockJournalDetails.StockJournalDetailsEdit(infoStockJournalDetails);
                            //}
                        }
                        else
                        {
                            infoStockJournalDetails.BatchId = Convert.ToDecimal(consumptionObj.Batch);
                            infoStockJournalDetails.GodownId = Convert.ToDecimal(consumptionObj.Store);
                            infoStockJournalDetails.RackId = Convert.ToDecimal(consumptionObj.Rack);
                            infoStockJournalDetails.Amount = Convert.ToDecimal(consumptionObj.Amount);
                            infoStockJournalDetails.ConsumptionOrProduction = "Consumption";
                            infoStockJournalDetails.Slno = Convert.ToInt32(consumptionObj.SiNo);
                            spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetails);
                        }
                        //Stock Posting Add
                        if (false)//(btnSave.Text == "Update")
                        {
                            //infoStockPosting.BatchId = infoStockJournalDetails.BatchId;
                            //infoStockPosting.Date = Convert.ToDateTime(txtDate.Text);
                            //infoStockPosting.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            //infoStockPosting.GodownId = infoStockJournalDetails.GodownId;
                            //infoStockPosting.InwardQty = 0;
                            //infoStockPosting.OutwardQty = infoStockJournalDetails.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(infoStockJournalDetails.UnitConversionId);
                            //infoStockPosting.ProductId = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionProductId"].Value);
                            //infoStockPosting.RackId = infoStockJournalDetails.RackId;
                            //infoStockPosting.Rate = Convert.ToDecimal(dgvConsumption.Rows[i].Cells["dgvtxtConsumptionRate"].Value);
                            //infoStockPosting.UnitId = infoStockJournalDetails.UnitId;
                            //infoStockPosting.InvoiceNo = txtVoucherNo.Text.Trim();
                            //infoStockPosting.VoucherNo = strVoucherNo;
                            //infoStockPosting.VoucherTypeId = decVoucherTypeId;
                            //infoStockPosting.AgainstVoucherTypeId = 0;
                            //infoStockPosting.AgainstInvoiceNo = "NA";
                            //infoStockPosting.AgainstVoucherNo = "NA";
                            //infoStockPosting.Extra1 = string.Empty;
                            //infoStockPosting.Extra2 = string.Empty;
                            //spStockPosting.StockPostingAdd(infoStockPosting);
                        }
                        else
                        {
                            infoStockPosting.BatchId = Convert.ToDecimal(consumptionObj.Batch);
                            infoStockPosting.Date = Convert.ToDateTime(obj.TransDate);
                            infoStockPosting.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            infoStockPosting.GodownId = Convert.ToDecimal(consumptionObj.Store);
                            infoStockPosting.InwardQty = 0;
                            infoStockPosting.OutwardQty = infoStockJournalDetails.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(infoStockJournalDetails.UnitConversionId);
                            infoStockPosting.ProductId = Convert.ToDecimal(consumptionObj.ProductCode);
                            infoStockPosting.RackId = Convert.ToDecimal(consumptionObj.Rack);
                            infoStockPosting.Rate = Convert.ToDecimal(consumptionObj.Rate);
                            infoStockPosting.UnitId = Convert.ToDecimal(consumptionObj.Unit);
                            infoStockPosting.InvoiceNo = ""; //txtVoucherNo.Text.Trim();
                            infoStockPosting.VoucherNo = "";// strVoucherNo;
                            infoStockPosting.VoucherTypeId = 1;//decVoucherTypeId;
                            infoStockPosting.AgainstVoucherTypeId = 0;
                            infoStockPosting.AgainstInvoiceNo = "NA";
                            infoStockPosting.AgainstVoucherNo = "NA";
                            infoStockPosting.Extra1 = string.Empty;
                            infoStockPosting.Extra2 = string.Empty;
                            spStockPosting.StockPostingAdd(infoStockPosting);
                        }
                    }

                }
                if (obj.SrcLineItems.Count > 0)
                {
                    int inCount = obj.SrcLineItems.Count;
                    int indexProductionCounter = 0;
                    foreach (var productionObj in obj.SrcLineItems)
                    {

                        if (false)//(btnSave.Text == "Update")
                        {
                            infoStockJournalMaster.StockJournalMasterId = 1;// decStockJournalMasterIdForEdit;
                        }
                        else
                        {
                            infoStockJournalDetails.StockJournalMasterId = decStockMasterId;
                        }
                        infoStockJournalDetails.Extra1 = string.Empty;
                        infoStockJournalDetails.Extra2 = string.Empty;
                        infoStockJournalDetails.ExtraDate = DateTime.Now;
                        infoStockJournalDetails.ProductId = Convert.ToDecimal(productionObj.ProductCode);
                        infoStockJournalDetails.Qty = Convert.ToDecimal(productionObj.Qty);
                        infoStockJournalDetails.Rate = Convert.ToDecimal(productionObj.Rate);
                        infoStockJournalDetails.UnitId = Convert.ToDecimal(productionObj.Unit);
                        infoStockJournalDetails.UnitConversionId = 1;// Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvtxtProductionunitConversionId"].Value);
                        if (false)//(btnSave.Text == "Update")
                        {
                            //infoStockJournalDetails.StockJournalMasterId = decStockJournalMasterIdForEdit;
                            //if (dgvProduction.Rows[i].Cells["dgvtxtProductionStockJournalDetailsId"].Value == null || dgvProduction.Rows[i].Cells["dgvtxtProductionStockJournalDetailsId"].Value.ToString() == string.Empty)
                            //{

                            //    if (dgvProduction.Rows[i].Cells["dgvcmbProductionBatch"].Value != null && dgvProduction.Rows[i].Cells["dgvcmbProductionBatch"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.BatchId = Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvcmbProductionBatch"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.BatchId = 0;
                            //    }
                            //    if (dgvProduction.Rows[i].Cells["dgvcmbProductionGodown"].Value != null && dgvProduction.Rows[i].Cells["dgvcmbProductionGodown"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.GodownId = Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvcmbProductionGodown"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.GodownId = 0;
                            //    }
                            //    if (dgvProduction.Rows[i].Cells["dgvcmbProductionRack"].Value != null && dgvProduction.Rows[i].Cells["dgvcmbProductionRack"].Value.ToString() != string.Empty)
                            //    {
                            //        infoStockJournalDetails.RackId = Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvcmbProductionRack"].Value);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.RackId = 0;
                            //    }
                            //    infoStockJournalDetails.Amount = Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvtxtProductionAmount"].Value);
                            //    infoStockJournalDetails.ConsumptionOrProduction = "Production";
                            //    infoStockJournalDetails.Slno = Convert.ToInt32(dgvProduction.Rows[i].Cells["dgvtxtProductionSlNo"].Value);
                            //    spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetails);
                            //}
                            //else
                            //{
                            // infoStockJournalDetails.StockJournalDetailsId = 1;//Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvtxtProductionStockJournalDetailsId"].Value);
                            //    if (productionObj.Batch > 0)
                            //    {
                            //        infoStockJournalDetails.BatchId = Convert.ToDecimal(productionObj.Batch);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.BatchId = 0;
                            //    }
                            //    if (productionObj.Store > 0)
                            //    {
                            //        infoStockJournalDetails.GodownId = Convert.ToDecimal(productionObj.Store);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.GodownId = 0;
                            //    }
                            //    if (productionObj.Rate > 0)
                            //    {
                            //        infoStockJournalDetails.RackId = Convert.ToDecimal(productionObj.Rack);
                            //    }
                            //    else
                            //    {
                            //        infoStockJournalDetails.RackId = 0;
                            //    }
                            //    infoStockJournalDetails.Amount = Convert.ToDecimal(productionObj.Amount);
                            //    infoStockJournalDetails.ConsumptionOrProduction = "Production";
                            //    infoStockJournalDetails.Slno = Convert.ToInt32(productionObj.SiNo);
                            //    spStockJournalDetails.StockJournalDetailsEdit(infoStockJournalDetails);
                            //}
                        }
                        else
                        {
                            infoStockJournalDetails.BatchId = Convert.ToDecimal(productionObj.Batch);
                            infoStockJournalDetails.GodownId = Convert.ToDecimal(productionObj.Store);
                            infoStockJournalDetails.RackId = Convert.ToDecimal(productionObj.Rack);
                            infoStockJournalDetails.Amount = Convert.ToDecimal(productionObj.Amount);
                            infoStockJournalDetails.ConsumptionOrProduction = "Production";
                            infoStockJournalDetails.Slno = Convert.ToInt32(productionObj.SiNo);
                            spStockJournalDetails.StockJournalDetailsAdd(infoStockJournalDetails);
                        }
                        //Stock Posting Add
                        if (true)//(btnSave.Text == "Save")
                        {
                            infoStockPosting.BatchId = Convert.ToDecimal(productionObj.Batch);
                            infoStockPosting.Date = Convert.ToDateTime(obj.TransDate);
                            infoStockPosting.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            infoStockPosting.GodownId = Convert.ToDecimal(productionObj.Store);
                            infoStockPosting.InwardQty = infoStockJournalDetails.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(infoStockJournalDetails.UnitConversionId);
                            infoStockPosting.OutwardQty = 0;
                            infoStockPosting.ProductId = Convert.ToDecimal(productionObj.ProductCode);
                            infoStockPosting.RackId = Convert.ToDecimal(productionObj.Rack);
                            infoStockPosting.Rate = Convert.ToDecimal(productionObj.Rate);
                            infoStockPosting.UnitId = Convert.ToDecimal(productionObj.Unit);
                            infoStockPosting.InvoiceNo = ""; //txtVoucherNo.Text.Trim();
                            infoStockPosting.VoucherNo = ""; //strVoucherNo;
                            infoStockPosting.VoucherTypeId = 1; //decVoucherTypeId;
                            infoStockPosting.AgainstVoucherTypeId = 0;
                            infoStockPosting.AgainstInvoiceNo = "NA";
                            infoStockPosting.AgainstVoucherNo = "NA";
                            infoStockPosting.Extra1 = string.Empty;
                            infoStockPosting.Extra2 = string.Empty;
                            spStockPosting.StockPostingAdd(infoStockPosting);
                        }
                        else
                        {
                            //infoStockPosting.BatchId = infoStockJournalDetails.BatchId;
                            //infoStockPosting.Date = Convert.ToDateTime(txtDate.Text);
                            //infoStockPosting.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            //infoStockPosting.GodownId = infoStockJournalDetails.GodownId;
                            //infoStockPosting.InwardQty = infoStockJournalDetails.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(infoStockJournalDetails.UnitConversionId);
                            //infoStockPosting.OutwardQty = 0;
                            //infoStockPosting.ProductId = Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvtxtProductionProductId"].Value);
                            //infoStockPosting.RackId = infoStockJournalDetails.RackId;
                            //infoStockPosting.Rate = Convert.ToDecimal(dgvProduction.Rows[i].Cells["dgvtxtProductionRate"].Value);
                            //infoStockPosting.UnitId = infoStockJournalDetails.UnitId;
                            //infoStockPosting.InvoiceNo = txtVoucherNo.Text.Trim();
                            //infoStockPosting.VoucherNo = strVoucherNo;
                            //infoStockPosting.VoucherTypeId = decVoucherTypeId;
                            //infoStockPosting.AgainstVoucherTypeId = 0;
                            //infoStockPosting.AgainstInvoiceNo = "NA";
                            //infoStockPosting.AgainstVoucherNo = "NA";
                            //infoStockPosting.Extra1 = string.Empty;
                            //infoStockPosting.Extra2 = string.Empty;
                            //spStockPosting.StockPostingAdd(infoStockPosting);

                        }
                    }

                }
                //....Additional Cost Add...////
                if (false)//(btnSave.Text == "Update")
                {
                    //spLedgerPosting.DeleteLedgerPostingForStockJournalEdit(strVoucherNo, decVoucherTypeId);//Delete
                    //spAdditionalCost.DeleteAdditionalCostForStockJournalEdit(strVoucherNo, decVoucherTypeId);//Delete
                }
                decimal decGrandTotal = 0;
                decimal decRate = 0;
                ExchangeRateSP spExchangeRate = new ExchangeRateSP();
                if (obj.AdditionalCostItems.Count > 1)
                {
                    infoAdditionalCost.Credit = Convert.ToDecimal(obj.AdditionalAmountTotal);
                    infoAdditionalCost.Debit = 0;
                    infoAdditionalCost.LedgerId = Convert.ToDecimal(obj.CashNBank);
                    infoAdditionalCost.VoucherNo = "";// strVoucherNo;
                    infoAdditionalCost.VoucherTypeId = 1;//decVoucherTypeId;
                    infoAdditionalCost.Extra1 = string.Empty;
                    infoAdditionalCost.Extra2 = string.Empty;
                    infoAdditionalCost.ExtraDate = DateTime.Now;
                    spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                    //....Ledger Posting Add...///
                    //-------------------  Currency Conversion-----------------------------
                    decGrandTotal = Convert.ToDecimal(obj.AdditionalAmountTotal);
                    decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(obj.Currency));
                    decGrandTotal = decGrandTotal * decRate;
                    //---------------------------------------------------------------
                    infoLedgerPosting.Credit = decGrandTotal;
                    infoLedgerPosting.Debit = 0;
                    infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);
                    infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.InvoiceNo = "";//txtVoucherNo.Text.Trim();
                    infoLedgerPosting.LedgerId = Convert.ToDecimal(obj.CashNBank);
                    infoLedgerPosting.VoucherNo = "";// strVoucherNo;
                    infoLedgerPosting.VoucherTypeId = 1;// decVoucherTypeId;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    if (true)//(btnSave.Text == "Save")
                    {
                        infoLedgerPosting.ExtraDate = DateTime.Now;
                    }
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                    foreach (var additiobnalCostObj in obj.AdditionalCostItems)
                    {
                        if (additiobnalCostObj.AccountLedger > 0)
                        {
                            try
                            {
                                /*-----------------------------------------Additional Cost Add----------------------------------------------------*/
                                infoAdditionalCost.Credit = 0;
                                infoAdditionalCost.Debit = Convert.ToDecimal(additiobnalCostObj.Amount);
                                infoAdditionalCost.LedgerId = Convert.ToDecimal(additiobnalCostObj.AccountLedger);
                                infoAdditionalCost.VoucherNo = "";//strVoucherNo;
                                infoAdditionalCost.VoucherTypeId = 1;// decVoucherTypeId;
                                infoAdditionalCost.Extra1 = string.Empty;
                                infoAdditionalCost.Extra2 = string.Empty;
                                infoAdditionalCost.ExtraDate = DateTime.Now;
                                spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                                /*-----------------------------------------Additional Cost Ledger Posting----------------------------------------------------*/
                                decimal decTotal = 0;
                                //-------------------  Currency Conversion------------------------
                                decTotal = Convert.ToDecimal(infoAdditionalCost.Debit);
                                decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(obj.Currency));
                                decTotal = decTotal * decRate;
                                //---------------------------------------------------------------
                                infoLedgerPosting.Credit = 0;
                                infoLedgerPosting.Debit = decTotal;
                                infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = "";//txtVoucherNo.Text.Trim();
                                infoLedgerPosting.LedgerId = Convert.ToDecimal(additiobnalCostObj.AccountLedger);
                                infoLedgerPosting.VoucherNo = "";//strVoucherNo;
                                infoLedgerPosting.VoucherTypeId = 1;// decVoucherTypeId;
                                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.ChequeDate = DateTime.Now;
                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                infoLedgerPosting.ExtraDate = DateTime.Now;

                                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                            }
                            catch (Exception ex)
                            {

                            }




                        }
                    }
                }
                if (true)//(btnSave.Text == "Save")
                {
                    //Messages.SavedMessage();
                    // if (cbxPrintAfterSave.Checked)
                    //{
                    //Print(decStockMasterId);
                    //}
                    //Clear();
                }
                else
                {
                    //Messages.UpdatedMessage();
                    //if (cbxPrintAfterSave.Checked)
                    //{
                    // Print(decStockJournalMasterIdForEdit);
                    // }
                    // this.Close();
                }

            }

            catch (Exception ex)
            {
                //  formMDI.infoError.ErrorString = "SJ36:" + ex.Message;
            }
        }

        [HttpPost]
        public bool EditStockTransfer(StockJournalVM obj)
        {
            DBMatConnection db = new DBMatConnection();
            string masterEditQuery = string.Empty;
            string detailsEditQuery = string.Empty;

            StockJournalMasterInfo newObj = new StockJournalMasterInfo();
            List<StockJournalDetailsInfo> newObjCons = new List<StockJournalDetailsInfo>();
            List<StockJournalDetailsInfo> newObjProd = new List<StockJournalDetailsInfo>();

            newObj = obj.StockJournalMasterInfo;
            newObjCons = obj.StockJournalDetailsInfoConsumption;
            newObjProd = obj.StockJournalDetailsInfoProduction;

            masterEditQuery = string.Format("UPDATE tbl_StockJournalMaster_Pending " +
                                             "SET narration = '{0}', date='{1}', status = 'Pending' " +
                                             "WHERE stockJournalMasterId={2} ", obj.StockJournalMasterInfo.Narration,
                                             obj.StockJournalMasterInfo.Date, obj.StockJournalMasterInfo.StockJournalMasterId);

            if (db.customUpdateQuery(masterEditQuery) > 0)
            {
                string GetQuery1 = string.Format("SELECT * FROM tbl_StockJournalDetails_Pending WHERE stockJournalMasterId = '{0}' AND consumptionOrProduction='Production'", newObj.StockJournalMasterId);
                string GetQuery2 = string.Format("SELECT * FROM tbl_StockJournalDetails_Pending WHERE stockJournalMasterId = '{0}' AND consumptionOrProduction='Consumption'", newObj.StockJournalMasterId);

                var production = db.customSelect(GetQuery1);
                var consumption = db.customSelect(GetQuery2);

                for (int i = 0; i < production.Rows.Count; i++)
                {
                    var exist = newObjProd.Find(p => p.StockJournalDetailsId == Convert.ToDecimal(production.Rows[i].ItemArray[0]));

                    if (exist == null)
                    {
                        string deleteQuery = string.Format("DELETE FROM tbl_StockJournalDetails_Pending WHERE stockJournalDetailsId = {0}", Convert.ToDecimal(production.Rows[i].ItemArray[0]));
                        db.ExecuteNonQuery2(deleteQuery);
                    }
                }
                for (int i = 0; i < consumption.Rows.Count; i++)
                {
                    var exist = newObjCons.Find(p => p.StockJournalDetailsId == Convert.ToDecimal(consumption.Rows[i].ItemArray[0]));

                    if (exist == null)
                    {
                        string deleteQuery = string.Format("DELETE FROM tbl_StockJournalDetails_Pending WHERE stockJournalDetailsId = {0}", Convert.ToDecimal(consumption.Rows[i].ItemArray[0]));
                        db.ExecuteNonQuery2(deleteQuery);
                    }
                }

                foreach (var o in newObjCons)
                {
                    if (o.StockJournalDetailsId > 0)
                    {
                        var amount = o.Rate * o.Qty;
                        detailsEditQuery = string.Format("UPDATE tbl_StockJournalDetails_Pending " +
                                                         "SET productId = {0}, qty = {1}, rate = {2}, godownId = {3}, amount = {4} " +
                                                         "WHERE stockJournalDetailsId = {5}", o.ProductId, o.Qty, o.Rate,
                                                         o.GodownId, amount, o.StockJournalDetailsId);
                        db.customUpdateQuery(detailsEditQuery);
                    }
                    if (o.StockJournalDetailsId == 0)
                    {
                        var amount = o.Rate * o.Qty;
                        string detailsQuery2 = string.Format("INSERT INTO tbl_StockJournalDetails_Pending " +
                                                    "VALUES({0},{1},{2},{3},{4},{5},{6},'{7}',{8},'{9}',{10})"
                                                    , newObj.StockJournalMasterId,
                                                    o.ProductId,
                                                    o.Qty,
                                                    o.Rate,
                                                    o.GodownId,
                                                    o.RackId,
                                                    amount,
                                                    "Consumption",
                                                    o.Slno,
                                                    "",
                                                    0);
                        db.ExecuteNonQuery2(detailsQuery2);
                        amount = 0;
                    }
                }
                foreach (var o in newObjProd)
                {
                    if (o.StockJournalDetailsId > 0)
                    {
                        var amount = o.Rate * o.Qty;
                        detailsEditQuery = string.Format("UPDATE tbl_StockJournalDetails_Pending " +
                                                         "SET productId = {0}, qty = {1}, rate = {2}, godownId = {3}, amount = {4} " +
                                                         "WHERE stockJournalDetailsId = {5}", o.ProductId, o.Qty, o.Rate,
                                                         o.GodownId, amount, o.StockJournalDetailsId);
                        db.customUpdateQuery(detailsEditQuery);
                    }
                    if (o.StockJournalDetailsId == 0)
                    {
                        var amount = o.Rate * o.Qty;
                        string detailsQuery2 = string.Format("INSERT INTO tbl_StockJournalDetails_Pending " +
                                                    "VALUES({0},{1},{2},{3},{4},{5},{6},'{7}',{8},'{9}',{10})"
                                                    , newObj.StockJournalMasterId,
                                                    o.ProductId,
                                                    o.Qty,
                                                    o.Rate,
                                                    o.GodownId,
                                                    o.RackId,
                                                    amount,
                                                    "Production",
                                                    o.Slno,
                                                    "",
                                                    0);
                        db.ExecuteNonQuery2(detailsQuery2);
                    }
                }
                return true;
            }
            return false;
        }

        [HttpGet]
        public decimal CancelStockTransfer(decimal id)
        {
            DBMatConnection db = new DBMatConnection();
            string query = string.Format("UPDATE tbl_stockJournalMaster_Pending SET status = 'Cancelled' WHERE stockJournalMasterId = {0}", id);

            return db.customUpdateQuery(query);
        }

        [HttpGet]
        public decimal DeleteStockTransfer(decimal id)
        {
            DBMatConnection db = new DBMatConnection();

            string query = string.Format("DELETE FROM tbl_stockJournalMaster_Pending WHERE stockJournalMasterId = {0}", id);
            if (db.ExecuteNonQuery2(query))
            {
                string detailsQuery = string.Format("DELETE FROM tbl_stockJournalDetails_Pending WHERE stockJournalMasterId = {0}", id);
                return 1;
            }
            return 0;
        }
    }
}
