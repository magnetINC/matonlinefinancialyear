﻿using System;
using System.Collections.Generic;
using MatApi.Models;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory.PriceList
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class StandardRateController : ApiController
    {
        public StandardRateController()
        {

        }

        [HttpGet]
        public MatResponse GetLookUps()
        {
            dynamic lookUps = new ExpandoObject();
            MatResponse response = new MatResponse();

            try
            {
                lookUps.ProductGroups = new ProductGroupSP().ProductGroupViewAll();
                lookUps.Products = new ProductSP().ProductViewAll();
                lookUps.Batches = new BatchSP().BatchViewAll();
                lookUps.Units = new UnitSP().UnitViewAll();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server Error";
                response.Response = e;
            }

            return response;
        }

        [HttpPost]
        public MatResponse GetProductsForStandardRate(standardRateSearchParam input)
        {
            MatResponse response = new MatResponse();
            DataTable standardRateList = new DataTable();

            try
            {
                standardRateList = new ProductGroupSP().ProductAndUnitViewAllForGridFill(input.productGroupId, input.productCode, input.productId);

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = standardRateList;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server Error";
                response.Response = e;
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetStandardRates(decimal productId)
        {
            DataTable details = new DataTable();
            MatResponse response = new MatResponse();

            try
            {
                details = new StandardRateSP().StandardRateGridFill(productId);

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = details;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server Error";
                response.Response = e;
            }

            return response;
        }

        [HttpPost]
        public MatResponse SaveStandardRate(StandardRateVM input)
        {
            MatResponse response = new MatResponse();

            try
            {
                StandardRateInfo infoStandardRate = new StandardRateInfo();
                StandardRateSP spStandardRate = new StandardRateSP();

                infoStandardRate.ApplicableFrom = input.applicableFrom;
                infoStandardRate.ApplicableTo = input.applicableTo;
                infoStandardRate.ProductId = input.productId;
                infoStandardRate.UnitId = input.unitId;
                infoStandardRate.BatchId = input.batchId;
                infoStandardRate.Rate = input.rate;
                infoStandardRate.Extra1 = string.Empty;
                infoStandardRate.Extra2 = string.Empty;
                if (spStandardRate.StandardrateCheckExistence(0, input.applicableFrom, input.applicableTo, input.productId, input.batchId) == false)
                {
                    spStandardRate.StandardRateAddParticularfields(infoStandardRate);

                    response.ResponseCode = 200;
                    response.ResponseMessage = "Success";
                }
                else
                {
                    response.ResponseCode = 403;
                    response.ResponseMessage = "Standard rate already exist for selected product,Batch and dates";
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server Error";
                response.Response = e;
            }
            return response;
        }

        [HttpPost]
        public MatResponse EditStandardRate(StandardRateVM input)
        {
            MatResponse response = new MatResponse();

            try
            {
                StandardRateInfo infoStandardRate = new StandardRateInfo();
                StandardRateSP spStandardRate = new StandardRateSP();

                infoStandardRate.StandardRateId = input.standardRateId;
                infoStandardRate.ApplicableFrom = input.applicableFrom;
                infoStandardRate.ApplicableTo = input.applicableTo;
                infoStandardRate.ProductId = input.productId;
                infoStandardRate.UnitId = input.unitId;
                infoStandardRate.BatchId = input.batchId;
                infoStandardRate.Rate = input.rate;
                infoStandardRate.Extra1 = string.Empty;
                infoStandardRate.Extra2 = string.Empty;
                if (spStandardRate.StandardrateCheckExistence(input.standardRateId, input.applicableFrom, input.applicableTo, input.productId, input.batchId) == false)
                {
                    spStandardRate.StandardRateEdit(infoStandardRate);

                    response.ResponseCode = 200;
                    response.ResponseMessage = "Success";
                }
                else
                {
                    response.ResponseCode = 403;
                    response.ResponseMessage = "Standard rate already exist for selected product and dates";
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server Error";
                response.Response = e;
            }
            return response;
        }

        [HttpGet]
        public MatResponse DeleteStandardRate(decimal standardRateId)
        {
            MatResponse response = new MatResponse();

            try
            {
                new StandardRateSP().StandardRateDelete(standardRateId);
                response.ResponseCode = 200;
                response.ResponseMessage = "Success";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server Error";
                response.Response = e;
            }

            return response;
        }

    }

    public class standardRateSearchParam
    {
        public decimal productGroupId { get; set; }
        public decimal productId { get; set; }
        public string productCode { get; set; }
    }
    public class StandardRateVM
    {
        public decimal productId { get; set; }
        public decimal standardRateId { get; set; }
        public decimal unitId { get; set; }
        public decimal batchId { get; set; }
        public decimal rate { get; set; }
        public DateTime applicableFrom { get; set; }
        public DateTime applicableTo { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
    }
}
