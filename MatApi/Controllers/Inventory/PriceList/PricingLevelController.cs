﻿using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory.PriceList
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PricingLevelController : ApiController
    {
        PricingLevelSP pricingLevelSp;
        public PricingLevelController()
        {
            pricingLevelSp = new PricingLevelSP();
        }

        [HttpGet]
        public DataTable GetPricingLevels()
        {
            DBMatConnection conn = new DBMatConnection();

            string query = string.Format("SELECT * FROM tbl_PricingLevel");
            var pricingLevels = conn.customSelect(query);
            
            return pricingLevels;
        }

        [HttpGet]
        public PricingLevelInfo GetPricingLevel(decimal pricingLevelId)
        {
            var pricingLevelDt = pricingLevelSp.PricingLevelView(pricingLevelId);
            PricingLevelInfo brand = new PricingLevelInfo
            {
                PricinglevelId = Convert.ToDecimal(pricingLevelDt.PricinglevelId),
                PricinglevelName = pricingLevelDt.PricinglevelName

            };
            return pricingLevelDt;
        }

        [HttpPost]
        public bool DeletePricingLevel(PricingLevelInfo pricingLevel)
        {
            if (pricingLevelSp.PricingLevelCheckReferenceAndDelete(pricingLevel.PricinglevelId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddPricingLevel(PricingLevelInfo pricingLevel)
        {
            pricingLevel.ExtraDate = DateTime.Now;
            pricingLevel.Extra1 = "";
            pricingLevel.Extra2 = "";
            if (pricingLevelSp.PricingLevelAddWithoutSamePricingLevel(pricingLevel) > 0)
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditPricingLevel(PricingLevelInfo pricingLevel)
        {
            pricingLevel.ExtraDate = DateTime.Now;
            pricingLevel.Extra1 = "";
            pricingLevel.Extra2 = "";
            return pricingLevelSp.PricingLevelEditParticularFields(pricingLevel);
        }
    }
}
