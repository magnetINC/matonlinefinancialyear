﻿using MatApi.Models;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory.PriceList
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PriceListController : ApiController
    {
        public PriceListController()
        {

        }

        [HttpGet]
        public MatResponse LookUps()
        {
            dynamic lookUps = new ExpandoObject();
            MatResponse response = new MatResponse();

            try
            {
                lookUps.ProductGroups = new ProductGroupSP().ProductGroupViewAll();
                lookUps.Products = new ProductSP().ProductViewAll();
                lookUps.PricingLevels = new PricingLevelSP().PricingLevelViewAll();
                lookUps.Batches = new BatchSP().BatchViewAll();
                lookUps.Units = new UnitSP().UnitViewAll();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server Error";
                response.Response = e;
            }

            return response;
        }

        [HttpPost]
        public MatResponse GetProductsForPriceList(PriceListSearchParam input)
        {
            DataTable priceList = new DataTable();
            MatResponse response = new MatResponse();

            try
            {
                priceList = new PriceListSP().ProductDetailsViewGridfill(input.productGroupId, input.productCode, input.productId, input.pricingLevelName);

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = priceList;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server Error";
                response.Response = e;
            }
           
            return response;
        }

        [HttpGet]
        public MatResponse GetPriceList(decimal pricingLevelId, decimal productId)
        {
            DataTable details = new DataTable();
            MatResponse response = new MatResponse();

            try
            {
                details = new PriceListSP().PriceListPopupGridFill(pricingLevelId, productId);

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = details;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server Error";
                response.Response = e;
            }

            return response;
        }

        [HttpPost]
        public MatResponse SavePriceList(PriceListVM input)
        {
            MatResponse response = new MatResponse();

            try
            {
                PriceListSP spPriceList = new PriceListSP();
                PriceListInfo infoPriceList = new PriceListInfo();
                infoPriceList.ProductId = input.productId;
                infoPriceList.PricinglevelId = input.pricingLevelId;
                infoPriceList.UnitId = input.unitId;
                infoPriceList.BatchId = input.batchId;
                infoPriceList.Rate = input.rate;
                infoPriceList.Extra1 = string.Empty;
                infoPriceList.Extra2 = string.Empty;
                if (spPriceList.PriceListCheckExistence(0, input.pricingLevelId, input.batchId, input.productId) == true)
                {
                    spPriceList.PriceListAdd(infoPriceList);

                    response.ResponseCode = 200;
                    response.ResponseMessage = "Success";
                }
                else
                {
                    response.ResponseCode = 403;
                    response.ResponseMessage = "Price List already exist for selected product and batches";
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server Error";
                response.Response = e;
            }
            return response;
        }

        [HttpPost]
        public MatResponse EditPriceList(PriceListVM input)
        {
            MatResponse response = new MatResponse();

            try
            {
                PriceListSP spPriceList = new PriceListSP();
                PriceListInfo infoPriceList = new PriceListInfo();

                infoPriceList.ProductId = input.productId;
                infoPriceList.PricelistId = input.pricingListId;
                infoPriceList.PricinglevelId = input.pricingLevelId;
                infoPriceList.UnitId = input.unitId;
                infoPriceList.BatchId = input.batchId;
                infoPriceList.Rate = input.rate;
                infoPriceList.Extra1 = string.Empty;
                infoPriceList.Extra2 = string.Empty;
                spPriceList.PriceListEdit(infoPriceList);

                response.ResponseCode = 200;
                response.ResponseMessage = "Success";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server Error";
                response.Response = e;
            }
            return response;
        }

        [HttpGet]
        public MatResponse DeletePriceList(decimal pricingLevelId)
        {
            MatResponse response = new MatResponse();

            try
            {
                if (new PriceListSP().PriceListCheckReferenceAndDelete(pricingLevelId) <= 0)
                {
                    response.ResponseCode = 403;
                    response.ResponseMessage = "Reference Exists.";
                }
                else
                {
                    response.ResponseCode = 200;
                    response.ResponseMessage = "Success";
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Server Error";
                response.Response = e;
            }

            return response;
        }
    }

    public class PriceListSearchParam
    {
        public decimal productGroupId { get; set; }
        public string pricingLevelName { get; set; }
        public decimal productId { get; set; }
        public string productCode { get; set; }
    }

    public class PriceListVM
    {
        public decimal pricingListId { get; set; }
        public decimal productId { get; set; }
        public decimal pricingLevelId { get; set; }
        public decimal unitId { get; set; }
        public decimal batchId { get; set; }
        public decimal rate { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
    }
}
