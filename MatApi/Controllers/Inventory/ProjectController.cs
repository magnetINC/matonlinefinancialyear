﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using MatApi.DBModel;
using EntityState = System.Data.Entity.EntityState;

namespace MatApi.Controllers.Inventory
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]

    public class ProjectController : ApiController
    {
        private DBMATAccounting_MagnetEntities1 db = new DBMATAccounting_MagnetEntities1();

        // GET: api/tbl_Project
        public IQueryable<tbl_Project> GetProject()
        {
            return db.tbl_Project;
        }

        // GET: api/tbl_Project/5
        [ResponseType(typeof(tbl_Project))]
        public IHttpActionResult GetProject(int id)
        {
            tbl_Project tbl_Project = db.tbl_Project.Find(id);
            if (tbl_Project == null)
            {
                return NotFound();
            }

            return Ok(tbl_Project);
        }

        // PUT: api/tbl_Project/5
        [ResponseType(typeof(void))]
        [HttpPost]
        public IHttpActionResult PutProject(int id, tbl_Project tbl_Project)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tbl_Project.ProjectId)
            {
                return BadRequest();
            }
            tbl_Project.ModifiedOn = DateTime.Now;
            db.Entry(tbl_Project).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!ProjectExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/tbl_Project
        [ResponseType(typeof(tbl_Project))]
        public IHttpActionResult CreateProject(tbl_Project tbl_Project)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            tbl_Project.CreatedOn = DateTime.UtcNow; 
            db.tbl_Project.Add(tbl_Project);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tbl_Project.ProjectId }, tbl_Project);
        }

        // DELETE: api/tbl_Project/5
        [ResponseType(typeof(tbl_Project))]
        [HttpPost]
        public IHttpActionResult DeleteProject(int id)
        {
            tbl_Project tbl_Project = db.tbl_Project.Find(id);
            if (tbl_Project == null)
            {
                return NotFound();
            }
            var checkRef = db.tbl_StockJournalDetails.Where(p => p.projectId == id);
            if (!checkRef.Any())
            {
                db.tbl_Project.Remove(tbl_Project);
                db.SaveChanges();
                return Ok();
            }
            return Ok(tbl_Project);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool ProjectExists(int id)
        {
            return db.tbl_Project.Count(e => e.ProjectId == id) > 0;
        }
    }
}