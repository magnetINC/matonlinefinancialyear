﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;

namespace MatApi.Controllers.Inventory.ItemComponent
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BrandController : ApiController
    {
        BrandSP brandSp;
        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public BrandController()
        {
            brandSp = new BrandSP();
        }

        public List<BrandInfo> GetBrands()
        {
            var brandsDt = brandSp.BrandViewAll();
            List<BrandInfo> brands = new List<BrandInfo>();
            foreach (DataRow row in brandsDt.Rows)
            {
                brands.Add(new BrandInfo
                {
                    BrandId = Convert.ToDecimal(row["BrandId"]),
                    BrandName = row["BrandName"].ToString(),
                    Manufacturer = row["Manufacturer"].ToString(),
                    Narration = row["Narration"].ToString()
                });
            }
            return brands;
        }

        public BrandInfo GetBrand(decimal brandId)
        {
            var brandDt = brandSp.BrandView(brandId);
            BrandInfo brand = new BrandInfo
            {
                BrandId = Convert.ToDecimal(brandDt.BrandId),
                BrandName = brandDt.BrandName,
                Manufacturer = brandDt.Manufacturer,
                Extra1 = brandDt.Extra1,
                Extra2 = brandDt.Extra1,
                ExtraDate = Convert.ToDateTime(brandDt.ExtraDate),
                Narration = brandDt.Narration

            };
            return brandDt;
        }

        [HttpPost]
        public bool DeleteBrand(BrandInfo brand)
        {
            if (brandSp.BrandDeleteCheckExistence(brand.BrandId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public decimal AddBrand(BrandInfo brand)
        {
            if (brandSp.BrandCheckIfExist(brand.BrandName, 0) == true)
            {
                return 2;
            }
            else
            {
                brand.ExtraDate = DateTime.Now;
                if (brandSp.BrandAdd(brand) > 0)
                {
                    return 1;
                }
            }
           
            return 0;
        }
        [HttpPost]
        public bool EditBrand(BrandInfo brand)
        {
            brand.ExtraDate = DateTime.Now;
            var item = context.tbl_Brand.Where(a => a.brandId == brand.BrandId).FirstOrDefault();
            if (item != null)
            {
                item.brandName = brand.BrandName;
                item.extra1 = brand.Extra1;
                item.extra2 = brand.Extra2;
                item.extraDate = brand.ExtraDate;
                item.manufacturer = brand.Manufacturer;
                item.narration = brand.Narration;
                context.SaveChanges();
                return true;
            }

            //var save = brandSp.BrandEdit(brand);
            return false;
        }
    }
}
