﻿using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;
using EntityState = System.Data.Entity.EntityState;

namespace MatApi.Controllers.Inventory.ItemComponent
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SizeController : ApiController
    {
        SizeSP sizeSp;
        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public SizeController()
        {
            sizeSp = new SizeSP();
        }

        public List<SizeInfo> GetSizes()
        {
            var sizesDt = sizeSp.SizeViewAll();
            List<SizeInfo> sizes = new List<SizeInfo>();
            foreach (DataRow row in sizesDt.Rows)
            {
                sizes.Add(new SizeInfo
                {
                    SizeId = Convert.ToDecimal(row["SizeId"]),
                    Size = row["Size"].ToString(),
                    Extra1 = row["Extra1"].ToString(),
                    Extra2 = row["Extra2"].ToString(),
                    Narration = row["Narration"].ToString()
                });
            }
            return sizes;
        }

        public SmartShopResponse SizeList()
        {
            var sizesDt = sizeSp.SizeViewAll();
            List<SizeInfo> sizes = new List<SizeInfo>();
            foreach (DataRow row in sizesDt.Rows)
            {
                sizes.Add(new SizeInfo
                {
                    SizeId = Convert.ToDecimal(row["SizeId"]),
                    Size = row["Size"].ToString(),
                    Extra1 = row["Extra1"].ToString(),
                    Extra2 = row["Extra2"].ToString(),
                    Narration = row["Narration"].ToString()
                });
            }
            SmartShopResponse resp = new SmartShopResponse();
            resp.data = sizes;
            return resp;
        }

        public SizeInfo GetSize(decimal sizeId)
        {
            var sizeDt = sizeSp.SizeViewing(sizeId);
            SizeInfo size = new SizeInfo
            {
                SizeId = Convert.ToDecimal(sizeDt.SizeId),
                Size = sizeDt.Size,
                Extra1 = sizeDt.Extra1,
                Extra2 = sizeDt.Extra1,
                Narration = sizeDt.Narration
            };
            return size;
        }

        [HttpPost]
        public bool DeleteSize(SizeInfo size)
        {
            var item = context.tbl_Size.Where(a => a.sizeId == size.SizeId).FirstOrDefault();
            if (item != null)
            {
                context.tbl_Size.Remove(item);
                context.SaveChanges();
                return true;
            }
           
            return false;
        }

        [HttpPost]
        public decimal AddSize(SizeInfo size)
        {
            if(sizeSp.SizeNameCheckExistence(size.Size,0) == true)
            {
                return 2;
            }
            else
            {
                size.Extra1 = "";
                size.Extra2 = "";
                size.Narration = "";
                size.ExtraDate = DateTime.Now;
                if (sizeSp.SizeAdding(size) > 0)
                {
                    return 1;
                }
            }        
            return 0;
        }
        [HttpPost]
        public bool EditSize(SizeInfo size)
        {
            var item = context.tbl_Size.Where(a => a.sizeId == size.SizeId).FirstOrDefault(); 
            
            size.Extra1 = "";
            size.Extra2 = "";
            size.Narration = "";

            size.ExtraDate = DateTime.Now;
            if (item != null)
            {
                item.extra1 = size.Extra1;
                item.extra2 = size.Extra2;
                item.narration = size.Narration;
                item.extraDate = size.ExtraDate;

                item.size = size.Size;
                context.Entry(item).State = EntityState.Modified;
                context.SaveChanges();
                return true;
            }

            return false;
        }
    }
}
