﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;

namespace MatApi.Controllers.Inventory.ItemComponent
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BatchController : ApiController
    {
        BatchSP batchSp;
        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public BatchController()
        {
            batchSp = new BatchSP();
        }

        public List<BatchInfo> GetBatches()
        {
            //var batchesDt = batchSp.BatchViewAll();
            List<BatchInfo> batches = new List<BatchInfo>();
            var batch = context.tbl_Batch.ToList();
            foreach (var i in batch)
            {
                batches.Add(new BatchInfo
                {
                    BatchId = i.batchId,
                    BatchNo = i.batchNo,
                    ProductId = Convert.ToInt32(i.productId),
                    ManufacturingDate = Convert.ToDateTime(i.manufacturingDate),
                    ExpiryDate = Convert.ToDateTime(i.expiryDate),
                    narration = i.narration
                });
            }
            return batches;
        }

        [HttpGet]
        public IHttpActionResult GetBatchFromProduct(int id)
        {
            var batch = context.tbl_Batch.Where(a => a.productId == id).ToList();
           
            return Ok(batch);
        }
        [HttpGet]
        public IHttpActionResult GetBatch()
        {
            var batch = context.tbl_Batch.ToList();
            return Ok(batch);
        }
        public IHttpActionResult GetBatch(decimal batchId)
        {
            //var batchDt = batchSp.BatchView(batchId); 
            var b = context.tbl_Batch.Find(batchId);
            if (b == null) return NotFound();
            BatchInfo batch = new BatchInfo
            {
                BatchId = Convert.ToDecimal(b.batchId),
                BatchNo = b.batchNo.ToString(),
                ProductId = Convert.ToDecimal(b.productId),
                ManufacturingDate = Convert.ToDateTime(b.manufacturingDate),
                ExpiryDate = Convert.ToDateTime(b.expiryDate)
            };
            return  Ok(batch);
        }

        [HttpPost]
        public bool DeleteBatch(BatchInfo batch)
        {
            if (batchSp.BatchDelete(batch.BatchId) > 0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddBatch(BatchInfo batch)
        {
            batch.Extra1 = "";
            batch.Extra2 = "";
           // batch.narration = "";
            batch.partNo = "";
            batch.barcode = "";
            batch.ExtraDate = DateTime.Now;
            if (batchSp.BatchAdd(batch) > 0)
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditBatch(BatchInfo batch)
        {
            batch.Extra1 = "";
            batch.Extra2 = "";
            //batch.BatchNo = "NA";
            batch.partNo = "";
            batch.barcode = "";
            batch.ExtraDate = DateTime.Now;
            return batchSp.BatchEdit(batch);
        }
    }
}
