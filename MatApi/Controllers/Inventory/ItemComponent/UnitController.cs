﻿using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;

namespace MatApi.Controllers.Inventory.ItemComponent
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class UnitController : ApiController
    {
        UnitSP unitSp;
        public UnitController()
        {
            unitSp = new UnitSP();
        }
        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();

        public List<UnitInfo> GetUnits()
        {
            var unitsDt = unitSp.UnitViewAll();
            List<UnitInfo> units = new List<UnitInfo>();
            foreach (DataRow row in unitsDt.Rows)
            {
                units.Add(new UnitInfo
                {
                    UnitId = Convert.ToDecimal(row["UnitId"]),
                    UnitName = row["UnitName"].ToString(),
                    //formalName = row["formalName"].ToString(),
                    noOfDecimalplaces = Convert.ToDecimal(row["noOfDecimalplaces"]),
                    //Extra1 = row["Extra1"].ToString(),
                    //Extra2 = row["Extra2"].ToString(),
                    Narration = row["Narration"].ToString()
                });
            }
            return units;
        }


        public List<UnitInfo> GetUnits1()
        {
            var unitsDt = unitSp.UnitViewAll12();
            List<UnitInfo> units = new List<UnitInfo>();
            foreach (var row in unitsDt)
            {
                units.Add(new UnitInfo
                {
                    UnitId = Convert.ToDecimal(row.unitId),
                    UnitName = row.unitName.ToString(),
                    //formalName = row["formalName"].ToString(),
                    noOfDecimalplaces = Convert.ToDecimal(row.noOfDecimalplaces),
                    //Extra1 = row["Extra1"].ToString(),
                    //Extra2 = row["Extra2"].ToString(),
                    Narration = row.narration.ToString()
                });
            }
            return units;
        }

        public SmartShopResponse UnitList12()
        {
            var unitsDt = unitSp.UnitViewAll12();
            List<UnitInfo> units = new List<UnitInfo>();
            foreach (var row in unitsDt)
            {
                units.Add(new UnitInfo
                {
                    UnitId = Convert.ToDecimal(row.unitId),
                    UnitName = row.unitName.ToString(),
                    //formalName = row["formalName"].ToString(),
                    noOfDecimalplaces = Convert.ToDecimal(row.noOfDecimalplaces),
                    //Extra1 = row["Extra1"].ToString(),
                    //Extra2 = row["Extra2"].ToString(),
                    Narration = row.narration.ToString()
                });
            }
            SmartShopResponse resp = new SmartShopResponse();
            resp.data = units;
            return resp;
        }

        public SmartShopResponse UnitList()
        {
            var unitsDt = unitSp.UnitViewAll();
            List<UnitInfo> units = new List<UnitInfo>();
            foreach (DataRow row in unitsDt.Rows)
            {
                units.Add(new UnitInfo
                {
                    UnitId = Convert.ToDecimal(row["UnitId"]),
                    UnitName = row["UnitName"].ToString(),
                    //formalName = row["formalName"].ToString(),
                    noOfDecimalplaces = Convert.ToDecimal(row["noOfDecimalplaces"]),
                    //Extra1 = row["Extra1"].ToString(),
                    //Extra2 = row["Extra2"].ToString(),
                    Narration = row["Narration"].ToString()
                });
            }
            SmartShopResponse resp = new SmartShopResponse();
            resp.data = units;
            return resp;
        }

        public UnitInfo GetUnit(decimal unitId)
        {
            var unitDt = unitSp.UnitView(unitId);
            UnitInfo unit = new UnitInfo
            {
                UnitId = Convert.ToDecimal(unitDt.UnitId),
                UnitName = unitDt.UnitName,
                //formalName = unitDt.formalName,
                noOfDecimalplaces = Convert.ToDecimal(unitDt.noOfDecimalplaces),
                //Extra1=unitDt.Extra1,
                //Extra2 = unitDt.Extra2,
                //ExtraDate = Convert.ToDateTime(unitDt.ExtraDate),
                Narration = unitDt.Narration

            };
            return unit;
        }

        [HttpPost]
        public bool DeleteUnit(UnitInfo unit)
        {
            if (!context.tbl_StockJournalDetails.Any(p => p.unitId == unit.UnitId) && !context.tbl_Product.Any(p => p.unitId == unit.UnitId))
            {
                var item = context.tbl_Unit.Where(a => a.unitId == unit.UnitId).FirstOrDefault();
                if (item != null)
                {
                    context.tbl_Unit.Remove(item);
                    context.SaveChanges();
                    return true;
                }
                return false;
            }
            else
            {
                return false;
            }
        }

        [HttpPost]
        public decimal AddUnit(UnitInfo unit)
        {
            if (unitSp.UnitCheckExistence(unit.UnitName, 0) == true)
            {
                return 2;
            }
            else
            {
                unit.Extra1 = "";
                unit.Extra2 = "";
                unit.Narration = "";
                unit.formalName = "";
                unit.ExtraDate = DateTime.Now;
                if (unitSp.UnitAdd(unit) > 0)
                {
                    return 1;
                }
            }

            return 0;
        }
        [HttpPost]
        public bool EditUnit(UnitInfo unit)
        {
            unit.Extra1 = "";
            unit.Extra2 = "";
            unit.Narration = "";
            unit.formalName = "";
            unit.ExtraDate = DateTime.Now;
            return unitSp.UnitEdit(unit);
        }
    }
}
