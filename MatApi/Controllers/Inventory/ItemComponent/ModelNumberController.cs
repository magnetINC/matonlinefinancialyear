﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Inventory.ItemComponent
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ModelNumberController : ApiController
    {
        ModelNoSP modelNoSp;
        public ModelNumberController()
        {
            modelNoSp = new ModelNoSP();
        }

        public List<ModelNoInfo> GetModelNos()
        {
            var modelNosDt = modelNoSp.ModelNoViewAll();
            List<ModelNoInfo> modelNos = new List<ModelNoInfo>();
            foreach (DataRow row in modelNosDt.Rows)
            {
                modelNos.Add(new ModelNoInfo
                {
                    ModelNoId = Convert.ToDecimal(row["ModelNoId"]),
                    ModelNo = row["ModelNo"].ToString()
                });
            }
            return modelNos;
        }

        public ModelNoInfo GetModelNo(decimal modelNoId)
        {
            var modelNoDt = modelNoSp.ModelNoView(modelNoId);
            ModelNoInfo modelno = new ModelNoInfo
            {
                ModelNoId = Convert.ToDecimal(modelNoDt.ModelNoId),
                ModelNo = modelNoDt.ModelNo

            };
            return modelno;
        }

        [HttpPost]
        public bool DeleteModelNo(ModelNoInfo modelNo)
        {
            if (modelNoSp.ModelNoCheckReferenceAndDelete(modelNo.ModelNoId)>0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public bool AddModelNo(ModelNoInfo modelNo)
        {
            modelNo.Extra1 = "";
            modelNo.Extra2 = "";
            modelNo.Narration = "";
            modelNo.ExtraDate = DateTime.Now;
            if (modelNoSp.ModelNoAddWithDifferentModelNo(modelNo) > 0)
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditModelNo(ModelNoInfo modelNo)
        {
            modelNo.Extra1 = "";
            modelNo.Extra2 = "";
            modelNo.Narration = "";
            modelNo.ExtraDate = DateTime.Now;
            return modelNoSp.ModelNoEdit(modelNo);
        }
    }
}
