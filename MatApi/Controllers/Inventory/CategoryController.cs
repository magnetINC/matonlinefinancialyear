﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Web.Http.Description;
using MatApi.DBModel;
using EntityState = System.Data.Entity.EntityState;

namespace MatApi.Controllers.Inventory
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CategoryController : ApiController
    {
        private DBMATAccounting_MagnetEntities1 db = new DBMATAccounting_MagnetEntities1();

        // GET: api/Category
        public IQueryable<tbl_Category> GetCategory()
        {
            return db.tbl_Category;
        }

        // GET: api/Category/5
        [ResponseType(typeof(tbl_Category))]
        public IHttpActionResult GetCategory(int id)
        {
            tbl_Category tbl_Category = db.tbl_Category.Find(id);
            if (tbl_Category == null)
            {
                return NotFound();
            }

            return Ok(tbl_Category);
        }

        // PUT: api/Category/5
        [ResponseType(typeof(void))]
        [HttpPost]
        public IHttpActionResult PutCategory(int id, tbl_Category tbl_Category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != tbl_Category.CategoryId)
            {
                return BadRequest();
            }

            tbl_Category.ModifiedOn = DateTime.Now;
            db.Entry(tbl_Category).State = EntityState.Modified;

            try
            {
                db.SaveChanges();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CategoryExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Category
        [ResponseType(typeof(tbl_Category))]
        public IHttpActionResult CreateCategory(tbl_Category tbl_Category)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }
            tbl_Category.CreatedOn = DateTime.Now;
            db.tbl_Category.Add(tbl_Category);
            db.SaveChanges();

            return CreatedAtRoute("DefaultApi", new { id = tbl_Category.CategoryId }, tbl_Category);
        }

        // DELETE: api/Category/5
        [ResponseType(typeof(tbl_Category))]
        [HttpPost]
        public IHttpActionResult DeleteCategory(int id)
        {
            tbl_Category tbl_Category = db.tbl_Category.Find(id);
            if (tbl_Category == null)
            {
                return NotFound();
            }

            var checkRef = db.tbl_StockJournalDetails.Where(p => p.categoryId == id);
            if (!checkRef.Any())
            {
                db.tbl_Category.Remove(tbl_Category);
                db.SaveChanges();
                return Ok();
            }

            return Ok(tbl_Category);
        }

        [HttpGet]
        public IHttpActionResult GetSubCategory(int id)
        {
             return Ok(db.tbl_Category.Find(id));
        } 
        [HttpGet]
        public IHttpActionResult GetSubCategoryRelation(int id)
        {
            var c = db.tbl_Category.Find(id);
            if (c == null) return NotFound();

            var cr = "";
            for (;;)
            {
                if (c.BelongsToCategoryId == null) break;
                
               c = GetCategoryItem((int) c.BelongsToCategoryId); 
               if (c == null) break;
               cr += c.CategoryName + ">";
            }
            cr= cr.TrimEnd('>');
            return Ok(cr);
        }

        public tbl_Category GetCategoryItem(int id)
        {
            return db.tbl_Category.Find(id);
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool CategoryExists(int id)
        {
            return db.tbl_Category.Count(e => e.CategoryId == id) > 0;
        }
    }
}