﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MATFinancials;
using MatApi.Models;
using MatApi.Models.Security;
using PublicVariables = MatApi.Models.PublicVariables;
using MatDal;
using MatApi.DBModel;

namespace MatApi.Controllers.Security
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class LoginController : ApiController
    {
        UserSP userSp;
        public LoginController()
        {
            userSp = new UserSP();
        }

        [HttpPost]
        public LoginResponseVM Login(LoginRequestModel request)
        {
            LoginResponseVM response = new LoginResponseVM();

            try
            {
                var newContext = new DBMATAccounting_MagnetEntities1();

                System.Web.HttpContext.Current.Session.Add("DefaultDatabaseConnection", newContext.Database.Connection.ConnectionString);

                // var encode1 = encodePassword(request.Password);

                // var Users = newContext.tbl_User.Where(p => p.userName == request.Username && p.password == encode1).FirstOrDefault();

                decimal userId = userSp.GetUserIdAfterLogin(request.Username, encodePassword(request.Password), newContext.Database.Connection.ConnectionString);

                UserInfo user = userSp.UserView(userId, newContext.Database.Connection.ConnectionString);

                GodownInfo store = new GodownSP().GodownView(Convert.ToDecimal(user.StoreId), newContext.Database.Connection.ConnectionString);

                RoleInfo role = new RoleSP().RoleView(user.RoleId, newContext.Database.Connection.ConnectionString);

                response.Password = request.Password;
                response.UserLogin = request.Username;
                response.UserId = user.UserId;
                response.Username = user.UserName;
                response.FirstName = user.FirstName;
                response.LastName = user.LastName;
                response.StoreId = Convert.ToDecimal(user.StoreId);
                response.StoreName = store.GodownName;
                response.RoleId = role.RoleId;
                response.RoleName = role.Role;
                response.IsActive = user.Active;
                response.DateCreated = user.ExtraDate;
               
                var Result = newContext.tbl_FinancialYear.Where(f=>f.IsFinanacialYearExpired.Value).ToList();
                if(Result != null && Result.Count > 0)
                {
                    var Result1 = Result[Result.Count - 1];
                   
                    if(Result1 != null)
                    {
                        response.FinancialYearDisplay = Result1.fromDate.Value.ToShortDateString() + "  -  " + Result1.toDate.Value.ToShortDateString();
                        response.FinancialYear = Result1.fromDate.Value.Year;

                        response.IsInCurrentFinancialYear = true;

                        response._dtToDate = Convert.ToDateTime(Result1.toDate.Value);
                        response._dtFromDate = Convert.ToDateTime(Result1.fromDate.Value);
                        response._decCurrentFinancialYearId = (int)Result1.financialYearId;

                        response.CurrentFinicialId = (int)Result1.financialYearId;
                        response.ToDate = Convert.ToDateTime(Result1.toDate.Value);
                        response.FromDate = Convert.ToDateTime(Result1.fromDate.Value);
                        response.CurrentFinicialYearId = (int)Result1.financialYearId;
                        response.userInfo = user;
                    }
                }

                PublicVariables.CurrentUserId = user.UserId;
                PublicVariables.CurrentUser = user;
            }
            catch(Exception ex)
            {

            }
            return response;
        }

        private string encodePassword(string password)
        {
            MATFinancials.Classes.Security sec = new MATFinancials.Classes.Security();
            return sec.base64Encode(password);
        }

        public UserInfo getUserDetails(decimal userId)
        {
            UserSP sp = new UserSP();
            UserInfo user =sp.UserView(userId);
            return user;
            //return new PersonInfo {
            //    FirstName="Halex",
            //    LastName="Thosyn",
            //    Username="mclux",
            //    Role="Developer",
            //    RoleId=1,
            //    UserId=1
            //};
        }
    }
}
