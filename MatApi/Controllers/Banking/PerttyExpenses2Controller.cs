﻿//using MatApi.Models;
//using MatApi.Models.Supplier;
//using MATFinancials;
//using MATFinancials.Classes.HelperClasses;
//using MATFinancials.DAL;
//using MATFinancials.Other;
//using System;
//using System.Collections.Generic;
//using System.Data;
//using System.Dynamic;
//using System.Linq;
//using System.Net;
//using System.Net.Http;
//using System.Web.Http;
//using System.Web.Http.Cors;

//namespace MatApi.Controllers.Banking
//{
//    [EnableCors(origins: "*", headers: "*", methods: "*")]

//    public class PettyExpensesController : ApiController
//    {
//        string strVoucherNo = string.Empty;//to save voucher no into tbl_payment master
//        string strInvoiceNo = string.Empty;//to save invoice no into tbl_payment master 
//        string tableName = "PettyExpenses";//to get the table name in voucher type selection
//        string strCashOrBank;//to get the selected value in cmbBankOrCash at teh time of ledger popup
//        string strPrefix = string.Empty;//to get the prefix string from frmvouchertypeselection
//        string strSuffix = string.Empty;//to get the suffix string from frmvouchertypeselection
//        decimal decPaymentVoucherTypeId = 10035;
//        decimal creditAmount;
//        decimal decDailySuffixPrefixId = 0;


//        [HttpGet]
//        public HttpResponseMessage LookUps()
//        {
//            dynamic response = new ExpandoObject();
//            try
//            {
//                var bankOrCash = new TransactionsGeneralFill().BankOrCashComboFill(false);
//                var suppliers = new AccountLedgerSP().AccountLedgerViewSuppliersOnly();
//                var otherExpenses = new AccountLedgerSP().AccountLedgerViewOtherExpenses();

//                response.BankOrCash = bankOrCash;
//                response.Suppliers = suppliers;
//                response.Expenses = otherExpenses;
//                response.VoucherTypes = new VoucherTypeSP().VoucherTypeViewAll();
//            }
//            catch (Exception ex)
//            {
//            }
//            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
//        }
//        [HttpGet]
//        public decimal GetCurrentBalance(decimal ledgerId)
//        {
//            decimal Balance = 0;
//            MATFinancials.DAL.DBMatConnection _db = new MATFinancials.DAL.DBMatConnection();
//            DataSet ds = _db.ExecuteQuery("select isnull(sum(debit-credit),0) as balance from tbl_LedgerPosting where ledgerId ='" + ledgerId + "'");
//            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
//            {
//                Balance = Convert.ToDecimal(ds.Tables[0].Rows[0]["balance"]);
//            }
//            return Balance;
//        }

//        [HttpGet]
//        public string getAutoVoucherNo()
//        {
//            return AutomaticVoucherGeneration();
//        }
//        private string AutomaticVoucherGeneration()
//        {
//            TransactionsGeneralFill obj = new TransactionsGeneralFill();
//            PaymentMasterSP SpPaymentMaster = new PaymentMasterSP();
//            if (strVoucherNo == string.Empty)
//            {
//                strVoucherNo = "0";
//            }
//            strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPaymentVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
//            if (Convert.ToDecimal(strVoucherNo) != SpPaymentMaster.PaymentMasterMax(decPaymentVoucherTypeId) + 1)
//            {
//                strVoucherNo = SpPaymentMaster.PaymentMasterMax(decPaymentVoucherTypeId).ToString();
//                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPaymentVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
//                if (SpPaymentMaster.PaymentMasterMax(decPaymentVoucherTypeId) == 0)
//                {
//                    strVoucherNo = "0";
//                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPaymentVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
//                }
//            }
//            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
//            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
//            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decPaymentVoucherTypeId, DateTime.Now);
//            strPrefix = infoSuffixPrefix.Prefix;
//            strSuffix = infoSuffixPrefix.Suffix;
//            strInvoiceNo = strPrefix + strVoucherNo + strSuffix;

//            return strInvoiceNo;
//        }
//        [HttpGet]
//        public HttpResponseMessage getMaxPendingMasterIdPlusOne()
//        {
//            string lastRowIdValue = getMaxPendingMasterIdPlus1();

//            return Request.CreateResponse(HttpStatusCode.OK, (object)lastRowIdValue);
//        }
//        public string getMaxPendingMasterIdPlus1()
//        {
//            PaymentMasterSP SpPaymentMaster = new PaymentMasterSP();
//            var masterResult = SpPaymentMaster.PendingPaymentMasterMax();
//            string lastRowIdValue = (masterResult + 1).ToString();
//            if (masterResult < 1)
//            {
//                lastRowIdValue = "1";
//            }
//            return lastRowIdValue;
//        }
//        [HttpGet]
//        public HttpResponseMessage getPartyBalanceComboFill(int id)
//        {
//            var response = new PartyBalanceSP().
//                PartyBalanceComboViewByLedgerId(id, "Dr", 0, "");

//            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
//        }

//        public void MasterLedgerPosting(decimal decPaymentMasterId, CreatePaymentMaster input)
//        {
//            try
//            {
//                LedgerPostingInfo InfoLedgerPosting = new LedgerPostingInfo();
//                LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
//                ExchangeRateSP SpExchangRate = new ExchangeRateSP();
//                InfoLedgerPosting.Credit = creditAmount; // Convert.ToDecimal(txtTotal.Text);
//                InfoLedgerPosting.Date = DateTime.Now;
//                InfoLedgerPosting.Debit = 0;
//                InfoLedgerPosting.DetailsId = decPaymentMasterId;
//                InfoLedgerPosting.Extra1 = string.Empty;
//                InfoLedgerPosting.Extra2 = string.Empty;
//                InfoLedgerPosting.InvoiceNo = input.invoiceNo;
//                InfoLedgerPosting.ChequeNo = input.ChequeNo;
//                InfoLedgerPosting.ChequeDate = input.ChequeDate;
//                InfoLedgerPosting.LedgerId = input.LedgerId;
//                InfoLedgerPosting.VoucherNo = input.VoucherNo;
//                InfoLedgerPosting.VoucherTypeId = decPaymentVoucherTypeId;
//                InfoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
//                SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
//            }
//            catch (Exception ex)
//            {
//            }
//        }
//        [HttpPost]
//        public void Save(CreatePaymentMaster input)
//        {
//            try
//            {
//                int inB = 0;
//                PaymentMasterInfo InfoPaymentMaster = new PaymentMasterInfo();
//                PaymentMasterSP SpPaymentMaster = new PaymentMasterSP();
//                PaymentDetailsInfo InfoPaymentDetails = new PaymentDetailsInfo();
//                PaymentDetailsSP SpPaymentDetails = new PaymentDetailsSP();
//                PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
//                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
//                DateValidation objVal = new DateValidation();
//                var decSelectedCurrencyRate = new ExchangeRateSP().GetExchangeRateByExchangeRateId(1);//Exchange rate of grid's row

//                int inTableRowCount = input.PartyBalances.Count();

//                InfoPaymentMaster.Date = input.Date;
//                InfoPaymentMaster.Extra1 = "";
//                InfoPaymentMaster.Extra2 = "";
//                InfoPaymentMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
//                InfoPaymentMaster.InvoiceNo = input.invoiceNo;
//                InfoPaymentMaster.LedgerId = input.LedgerId;
//                InfoPaymentMaster.Narration = input.Narration;
//                InfoPaymentMaster.SuffixPrefixId = decDailySuffixPrefixId;
//                //decimal decTotalAmount = TotalAmountCalculation();
//                InfoPaymentMaster.TotalAmount = (input.TotalAmount * decSelectedCurrencyRate);
//                InfoPaymentMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
//                InfoPaymentMaster.VoucherNo = input.VoucherNo;
//                InfoPaymentMaster.VoucherTypeId = decPaymentVoucherTypeId;
//                decimal decPaymentMasterId = SpPaymentMaster.PaymentMasterAdd(InfoPaymentMaster);
//                if (decPaymentMasterId != 0)
//                {

//                    creditAmount = input.TotalAmount;
//                    //if (txtPaymentAmt.Text != null && txtPaymentAmt.Text.ToString() != string.Empty && Convert.ToDecimal(txtPaymentAmt.Text) > 0)
//                    //{
//                    //    isWithholdingTaxApplicable = true;
//                    //    creditAmount = Convert.ToDecimal(txtPaymentAmt.Text);
//                    //}
//                    MasterLedgerPosting(decPaymentMasterId, input);
//                    //if (dgvPaymentVoucher.Rows[0].Cells["dgvCmbWithHoldingTax"].Value != null
//                    //    && dgvPaymentVoucher.Rows[0].Cells["dgvCmbWithHoldingTax"].Value.ToString().Trim() != string.Empty
//                    //    && txtWithHoldingTax.Text != string.Empty && Convert.ToDecimal(txtWithHoldingTax.Text) > 0)
//                    //{
//                    //    decimal taxId = Convert.ToDecimal(dgvPaymentVoucher.Rows[0].Cells["dgvCmbWithHoldingTax"].Value);
//                    //    decimal ledgerId = dtblWithholdingTax.AsEnumerable().Where(i => i.Field<decimal>("taxId") == taxId)
//                    //    .Select(i => i.Field<decimal>("ledgerId")).FirstOrDefault();
//                    //    postLedgerForWithholdingTax(ledgerId);
//                    //    postWithholdingTaxDetails(true, false, false);
//                    //}
//                }
//                for (int inI = 0; inI < input.PaymentDetails.Count; inI++)
//                {
//                    InfoPaymentDetails.Amount = input.PaymentDetails[inI].Amount;
//                    InfoPaymentDetails.grossAmount = 0;
//                    InfoPaymentDetails.ExchangeRateId = decimal.Parse(input.PaymentDetails[inI].ExchangeRateId.ToString());
//                    InfoPaymentDetails.Extra1 = string.Empty;
//                    InfoPaymentDetails.Extra2 = string.Empty;
//                    InfoPaymentDetails.ProjectId = input.PaymentDetails[inI].ProjectId;
//                    InfoPaymentDetails.CategoryId = input.PaymentDetails[inI].CategoryId;
//                    InfoPaymentDetails.LedgerId = input.PaymentDetails[inI].LedgerId;
//                    InfoPaymentDetails.PaymentMasterId = decPaymentMasterId;
//                    InfoPaymentDetails.ChequeNo = input.ChequeNo;
//                    InfoPaymentDetails.ChequeDate = input.ChequeDate;
//                    InfoPaymentDetails.Memo = input.PaymentDetails[inI].Memo;
//                    InfoPaymentDetails.withholdingTaxId = input.PaymentDetails[inI].TaxId;
//                    InfoPaymentDetails.withholdingTaxAmount = input.PaymentDetails[inI].TaxAmount;

//                    decimal decPaymentDetailsId = SpPaymentDetails.PaymentDetailsAdd(InfoPaymentDetails);
//                    if (decPaymentDetailsId != 0)
//                    {
//                        for (int inJ = 0; inJ < inTableRowCount; inJ++)
//                        {
//                            if (input.PaymentDetails[inI].LedgerId == Convert.ToDecimal(input.PartyBalances[inJ].LedgerId.ToString()))
//                            {
//                                PartyBalanceAddOrEdit(inJ, input);
//                            }
//                        }
//                        inB++;
//                        DetailsLedgerPosting(inI, decPaymentDetailsId, input);
//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//            }
//        }

//        [HttpPost]
//        public MatResponse Pend(CreatePaymentMaster input)
//        {
//            MatResponse response = new MatResponse();
//            try
//            {
//                int inB = 0;
//                PaymentMasterInfo InfoPaymentMaster = new PaymentMasterInfo();
//                PaymentMasterSP SpPaymentMaster = new PaymentMasterSP();
//                PaymentDetailsInfo InfoPaymentDetails = new PaymentDetailsInfo();
//                PaymentDetailsSP SpPaymentDetails = new PaymentDetailsSP();
//                PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
//                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
//                DateValidation objVal = new DateValidation();
//                string suffixResponse = "";
//                var decSelectedCurrencyRate = new ExchangeRateSP().GetExchangeRateByExchangeRateId(1);//Exchange rate of grid's row

//                if (SpPaymentMaster.PendingPaymentMasterCheckExistence(Convert.ToDecimal(input.invoiceNo)))
//                {
//                    //returnValue = -1;
//                    input.invoiceNo = getMaxPendingMasterIdPlus1();
//                    input.VoucherNo = input.invoiceNo;
//                    suffixResponse = " The form number was incremented to " + input.invoiceNo + ".";
//                }

//                int inTableRowCount = input.PartyBalances.Count();

//                InfoPaymentMaster.Date = input.Date;
//                InfoPaymentMaster.Extra1 = "";
//                InfoPaymentMaster.Extra2 = "";
//                InfoPaymentMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
//                InfoPaymentMaster.InvoiceNo = input.invoiceNo;
//                InfoPaymentMaster.LedgerId = input.LedgerId;
//                InfoPaymentMaster.Narration = input.Narration;
//                InfoPaymentMaster.SuffixPrefixId = decDailySuffixPrefixId;
//                //decimal decTotalAmount = TotalAmountCalculation();
//                InfoPaymentMaster.TotalAmount = (input.TotalAmount * decSelectedCurrencyRate);
//                InfoPaymentMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
//                InfoPaymentMaster.VoucherNo = input.VoucherNo;
//                InfoPaymentMaster.VoucherTypeId = decPaymentVoucherTypeId;
//                decimal decPaymentMasterId = SpPaymentMaster.PendingPaymentMasterAdd(InfoPaymentMaster);
//                if (decPaymentMasterId != 0)
//                {
//                    creditAmount = input.TotalAmount;
//                    MasterLedgerPosting(decPaymentMasterId, input);
//                }
//                for (int inI = 0; inI < input.PaymentDetails.Count; inI++)
//                {
//                    InfoPaymentDetails.Amount = input.PaymentDetails[inI].Amount;
//                    InfoPaymentDetails.grossAmount = 0;
//                    InfoPaymentDetails.ExchangeRateId = decimal.Parse(input.PaymentDetails[inI].ExchangeRateId.ToString());
//                    InfoPaymentDetails.Extra1 = string.Empty;
//                    InfoPaymentDetails.Extra2 = string.Empty;
//                    InfoPaymentDetails.ProjectId = 1;
//                    InfoPaymentDetails.CategoryId = 1;
//                    InfoPaymentDetails.LedgerId = input.PaymentDetails[inI].LedgerId;
//                    InfoPaymentDetails.PaymentMasterId = decPaymentMasterId;
//                    InfoPaymentDetails.ChequeNo = input.ChequeNo;
//                    InfoPaymentDetails.DoneBy = input.DoneBy;
//                    InfoPaymentDetails.ChequeDate = input.ChequeDate;
//                    InfoPaymentDetails.Memo = input.PaymentDetails[inI].Memo;
//                    InfoPaymentDetails.withholdingTaxId = input.PaymentDetails[inI].TaxId;
//                    InfoPaymentDetails.withholdingTaxAmount = input.PaymentDetails[inI].TaxAmount;

//                    decimal decPaymentDetailsId = SpPaymentDetails.PendingPaymentDetailsAdd(InfoPaymentDetails);

//                }
//                response.ResponseMessage = "Petty Expenses successfully saved!" + suffixResponse;
//            }
//            catch (Exception ex)
//            {
//            }
//            return response;
//        }
//        [HttpGet]
//        public HttpResponseMessage GetPendingPayments()
//        {
//            dynamic response = new ExpandoObject();
//            try
//            {
//                var spPendingPaymentMaster = new PaymentMasterSP();
//                var spPendingPaymentDetails = new PaymentDetailsSP();
//                var pendingPaymentMaster = spPendingPaymentMaster.PendingPaymentMasterViewAll();
//                var pendingPaymentDetails = spPendingPaymentDetails.PendingPaymentDetailsViewAll();
//                string currencyName = string.Empty;

//                pendingPaymentDetails.Columns.Add("CurrencyName", typeof(System.String));

//                foreach (DataRow item in pendingPaymentDetails.Rows)
//                {
//                    currencyName = spPendingPaymentDetails.GetCurrencyNameByExchangeRate(Convert.ToString(item["ExchangeRateId"]));
//                    item["CurrencyName"] = currencyName;
//                }
//                var user = spPendingPaymentMaster.GetUser(Convert.ToDecimal(pendingPaymentMaster.Rows[0]["UserId"]));

//                response.PendingPaymentMaster = pendingPaymentMaster;
//                response.User = user.Rows[0]["firstName"] + " " + user.Rows[0]["lastName"];
//                response.PendingPaymentDetails = pendingPaymentDetails;

//            }
//            catch (Exception ex)
//            {
//            }
//            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
//        }
//        [HttpGet]
//        public HttpResponseMessage GetOnePendingPayment(decimal pendingPaymentMasterIdFromUrl)
//        {
//            dynamic response = new ExpandoObject();
//            try
//            {
//                var spPendingPaymentMaster = new PaymentMasterSP();
//                var spPendingPaymentDetails = new PaymentDetailsSP();
//                var pendingPaymentMaster = spPendingPaymentMaster.PendingPaymentMasterViewBySalesMasterId(pendingPaymentMasterIdFromUrl);
//                var pendingPaymentDetails = spPendingPaymentDetails.PendingPaymentDetailsViewAll();

//                response.PendingPaymentMaster = pendingPaymentMaster;
//                response.PendingPaymentDetails = pendingPaymentDetails;

//            }
//            catch (Exception ex)
//            {
//            }
//            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
//        }
//        public MatResponse ApplyCredit(ApplyCreditVM input)
//        {
//            MatResponse response = new MatResponse();
//            try
//            {
//                #region apply credit against supplier
//                DebitNoteMasterSP spDebitnoteMaster = new DebitNoteMasterSP();
//                DebitNoteMasterInfo infoDebitNoteMaster = new DebitNoteMasterInfo();
//                DebitNoteDetailsSP spDebitNoteDetails = new DebitNoteDetailsSP();
//                DebitNoteDetailsInfo infoDebitNoteDetails = new DebitNoteDetailsInfo();
//                PaymentMasterInfo infoPaymentMaster = new PaymentMasterInfo();
//                PaymentDetailsInfo infoPaymentDetails = new PaymentDetailsInfo();
//                PaymentMasterSP spPaymentMaster = new PaymentMasterSP();
//                PaymentDetailsSP spPaymentDetails = new PaymentDetailsSP();
//                JournalMasterInfo infoJournalMaster = new JournalMasterInfo();
//                JournalDetailsInfo infoJournalDetails = new JournalDetailsInfo();
//                JournalMasterSP spJournalMaster = new JournalMasterSP();
//                JournalDetailsSP spJournalDetails = new JournalDetailsSP();
//                PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
//                PartyBalanceSP spPartyBalance = new PartyBalanceSP();
//                HelperClasses helperClasses = new HelperClasses();
//                ExchangeRateSP spExchangeRate = new ExchangeRateSP();
//                PostingsHelper PostingHelper = new PostingsHelper();
//                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
//                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
//                DataTable dt = new DataTable();
//                decimal decSelectedCurrencyRate = 1;
//                decimal existingLedgerId = 0;
//                bool isRowAffected = false;
//                decimal TotalAmount = 0;

//                #region Working region

//                bool createNewDebitNote = false;
//                DataTable dtAdvancePayments = new DataTable();
//                dtAdvancePayments.Columns.Add("voucherTypeId", typeof(string));
//                dtAdvancePayments.Columns.Add("display", typeof(string));
//                dtAdvancePayments.Columns.Add("voucherNo", typeof(string));
//                dtAdvancePayments.Columns.Add("balance", typeof(decimal));
//                dtAdvancePayments.Columns.Add("exchangeRateId", typeof(string));
//                dtAdvancePayments.Columns.Add("invoiceNo", typeof(string));
//                dtAdvancePayments.Columns.Add("AmountToApply", typeof(decimal));

//                foreach (var row in input.advancePayments)
//                {
//                    DataRow dr = dtAdvancePayments.NewRow();
//                    dr[0] = row.voucheerTypeId;
//                    dr[1] = row.voucherType;
//                    dr[2] = row.voucherNo;
//                    dr[3] = row.value;
//                    dr[4] = row.exchangeRateId;
//                    dr[5] = row.invoiceNo;
//                    dr[6] = row.amountToApply;
//                    dtAdvancePayments.Rows.Add(dr);
//                }

//                if (dtAdvancePayments.Rows.Count > 0)
//                {
//                    decimal totalAdvancePaymentToApply = (from t in dtAdvancePayments.AsEnumerable()
//                                                          select t.Field<decimal>("AmountToApply")).Sum();
//                    decimal totalInvoice = input.grandTotal;

//                    foreach (DataRow row in dtAdvancePayments.Rows)
//                    {
//                        createNewDebitNote = false;
//                        decimal voucherTypeId = Convert.ToDecimal(row["voucherTypeId"].ToString());
//                        string voucherNo = row["voucherNo"].ToString();
//                        decimal balance = Convert.ToDecimal(row["balance"].ToString());
//                        decimal amountToApply = Convert.ToDecimal(row["AmountToApply"].ToString());
//                        if (balance > amountToApply)
//                        {
//                            createNewDebitNote = true;
//                        }

//                        dt = spPartyBalance.PartyBalanceViewByVoucherNoAndVoucherType(voucherTypeId, voucherNo, DateTime.Now);
//                        //decimal decPartyBalanceId = Convert.ToDecimal(dt.Rows[0]["PartyBalanceId"].ToString());

//                        TotalAmount = balance > amountToApply ? amountToApply : balance;

//                        #region Used code
//                        if (voucherTypeId == 23)        //debit note
//                        {
//                            bool DebitMasterEdit = false;
//                            infoDebitNoteMaster.DebitNoteMasterId = helperClasses.DebitNoteMasterId(voucherNo.ToString());
//                            infoDebitNoteMaster = spDebitnoteMaster.DebitNoteMasterView(infoDebitNoteMaster.DebitNoteMasterId);

//                            infoDebitNoteMaster.VoucherNo = infoDebitNoteMaster.VoucherNo;
//                            infoDebitNoteMaster.InvoiceNo = infoDebitNoteMaster.InvoiceNo;
//                            infoDebitNoteMaster.SuffixPrefixId = infoDebitNoteMaster.SuffixPrefixId;
//                            infoDebitNoteMaster.Date = input.date;
//                            infoDebitNoteMaster.Narration = input.narration;
//                            infoDebitNoteMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
//                            infoDebitNoteMaster.VoucherTypeId = infoDebitNoteMaster.VoucherTypeId;
//                            infoDebitNoteMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
//                            infoDebitNoteMaster.ExtraDate = DateTime.Now;
//                            infoDebitNoteMaster.Extra1 = string.Empty;
//                            infoDebitNoteMaster.Extra2 = string.Empty;
//                            infoDebitNoteMaster.TotalAmount = TotalAmount;
//                            DebitMasterEdit = true;
//                            decimal noofRowsAffected = spDebitnoteMaster.DebitNoteMasterEdit(infoDebitNoteMaster);
//                            if (noofRowsAffected > 0)
//                            {
//                                isRowAffected = true;
//                            }
//                            if (isRowAffected == true)
//                            {
//                                dt = spDebitNoteDetails.DebitNoteDetailsViewByMasterId(infoDebitNoteMaster.DebitNoteMasterId);
//                                for (int i = 0; i < dt.Rows.Count; i++)
//                                {
//                                    infoDebitNoteDetails.ChequeDate = DateTime.Now;
//                                    infoDebitNoteDetails.ChequeNo = string.Empty;
//                                    if (Convert.ToDecimal(dt.Rows[i]["debit"].ToString()) == 0)
//                                    {
//                                        infoDebitNoteDetails.Credit = TotalAmount;
//                                        infoDebitNoteDetails.Debit = 0;
//                                        infoDebitNoteDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
//                                        existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
//                                    }
//                                    else
//                                    {
//                                        infoDebitNoteDetails.Credit = 0;
//                                        infoDebitNoteDetails.Debit = TotalAmount;
//                                        infoDebitNoteDetails.LedgerId = input.ledgerId;
//                                        //existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
//                                    }
//                                    infoDebitNoteDetails.DebitNoteDetailsId = Convert.ToDecimal(dt.Rows[i]["DebitNoteDetailsId"].ToString());
//                                    infoDebitNoteDetails.DebitNoteMasterId = infoDebitNoteMaster.DebitNoteMasterId;
//                                    infoDebitNoteDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
//                                    infoDebitNoteDetails.Extra1 = string.Empty;
//                                    infoDebitNoteDetails.Extra2 = string.Empty;
//                                    infoDebitNoteDetails.ExtraDate = DateTime.Now;
//                                    DebitMasterEdit = false;
//                                    //------------------Currency conversion------------------//
//                                    decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoDebitNoteDetails.ExchangeRateId);
//                                    PostingHelper.DebitNoteDetailsAddOrEdit(infoDebitNoteMaster, infoDebitNoteDetails, DebitMasterEdit, decSelectedCurrencyRate, input.voucherNo, input.vouchertypeId);
//                                }
//                            }

//                            #endregion

//                            #region Create new Debit note
//                            if (createNewDebitNote)
//                            {
//                                SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
//                                SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
//                                TransactionsGeneralFill obj = new TransactionsGeneralFill();
//                                spDebitnoteMaster = new DebitNoteMasterSP();
//                                string strNewDebitNoteVoucherNo = string.Empty;
//                                string strNewDebitNoteInvoiceNo = string.Empty;

//                                if (strNewDebitNoteVoucherNo == string.Empty)
//                                {
//                                    strNewDebitNoteVoucherNo = "0"; //strMax;
//                                }
//                                //===================================================================================================================//

//                                VoucherTypeSP spVoucherType = new VoucherTypeSP();
//                                bool isAutomaticForDebitNote = spVoucherType.CheckMethodOfVoucherNumbering(23);
//                                if (isAutomaticForDebitNote)
//                                {
//                                    strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), DateTime.Now, "DebitNoteMaster");
//                                    if (Convert.ToDecimal(strNewDebitNoteVoucherNo) != spDebitnoteMaster.DebitNoteMasterGetMaxPlusOne(23))
//                                    {
//                                        strNewDebitNoteVoucherNo = spDebitnoteMaster.DebitNoteMasterGetMax(23).ToString();
//                                        strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), DateTime.Now, "DebitNoteMaster");
//                                        if (spDebitnoteMaster.DebitNoteMasterGetMax(23).ToString() == "0")
//                                        {
//                                            strNewDebitNoteVoucherNo = "0";
//                                            strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), DateTime.Now, "DebitNoteMaster");
//                                        }
//                                    }
//                                    infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(23, DateTime.Now);
//                                    strPrefix = infoSuffixPrefix.Prefix;
//                                    strSuffix = infoSuffixPrefix.Suffix;
//                                    strNewDebitNoteInvoiceNo = strPrefix + strNewDebitNoteVoucherNo + strSuffix;
//                                    //txtVoucherNo.Text = strInvoiceNo;
//                                    //txtVoucherNo.ReadOnly = true;
//                                }
//                                else
//                                {
//                                    //txtVoucherNo.ReadOnly = false;
//                                    //txtVoucherNo.Text = string.Empty;
//                                    strNewDebitNoteVoucherNo = input.voucherNo + "_Dr";
//                                    strNewDebitNoteInvoiceNo = strNewDebitNoteVoucherNo;
//                                }
//                                // ============================ Debit note master add =========================== //
//                                infoDebitNoteMaster.VoucherNo = strNewDebitNoteVoucherNo;
//                                infoDebitNoteMaster.InvoiceNo = strNewDebitNoteVoucherNo;
//                                infoDebitNoteMaster.SuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
//                                infoDebitNoteMaster.Date = input.date;
//                                infoDebitNoteMaster.Narration = input.narration;
//                                infoDebitNoteMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
//                                infoDebitNoteMaster.VoucherTypeId = 23;
//                                infoDebitNoteMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
//                                infoDebitNoteMaster.Extra1 = string.Empty;
//                                infoDebitNoteMaster.Extra2 = string.Empty;
//                                infoDebitNoteMaster.TotalAmount = balance - amountToApply;
//                                decimal decDebitNoteMasterId = spDebitnoteMaster.DebitNoteMasterAdd(infoDebitNoteMaster);

//                                // ========================== DebitNote Details Add =============================== //
//                                spDebitNoteDetails = new DebitNoteDetailsSP();
//                                spLedgerPosting = new LedgerPostingSP();
//                                infoLedgerPosting = new LedgerPostingInfo();
//                                decimal decLedgerId = 0;
//                                decimal decDebit = 0;
//                                decimal decCredit = 0;
//                                try
//                                {
//                                    infoDebitNoteDetails.DebitNoteMasterId = decDebitNoteMasterId;
//                                    infoDebitNoteDetails.ExchangeRateId = 1;
//                                    infoDebitNoteDetails.ExtraDate = DateTime.Now;
//                                    infoDebitNoteDetails.Extra1 = string.Empty;
//                                    infoDebitNoteDetails.Extra2 = string.Empty;
//                                    infoDebitNoteDetails.LedgerId = input.ledgerId;
//                                    decLedgerId = infoDebitNoteDetails.LedgerId;
//                                    infoDebitNoteDetails.Debit = balance - amountToApply;
//                                    infoDebitNoteDetails.Credit = 0;
//                                    decDebit = infoDebitNoteDetails.Debit * decSelectedCurrencyRate;
//                                    decCredit = infoDebitNoteDetails.Credit * decSelectedCurrencyRate;
//                                    infoDebitNoteDetails.ChequeNo = string.Empty;
//                                    infoDebitNoteDetails.ChequeDate = DateTime.Now;
//                                    //-------------------- debit leg ---------------------------- //
//                                    decimal decDebitDetailsId = spDebitNoteDetails.DebitNoteDetailsAdd(infoDebitNoteDetails);

//                                    infoDebitNoteDetails.LedgerId = 1;
//                                    decLedgerId = infoDebitNoteDetails.LedgerId;
//                                    infoDebitNoteDetails.Debit = 0;
//                                    infoDebitNoteDetails.Credit = balance - amountToApply;
//                                    decDebit = infoDebitNoteDetails.Debit * decSelectedCurrencyRate;
//                                    decCredit = infoDebitNoteDetails.Credit * decSelectedCurrencyRate;

//                                    // -------------------- credit leg --------------------------- //
//                                    decimal decCreditDetailsId = spDebitNoteDetails.DebitNoteDetailsAdd(infoDebitNoteDetails);

//                                    // -------------------------- party balance add --------------------------- //
//                                    spPartyBalance = new PartyBalanceSP();
//                                    PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
//                                    InfopartyBalance.CreditPeriod = 0;//
//                                    InfopartyBalance.Date = input.date;
//                                    InfopartyBalance.LedgerId = input.ledgerId;
//                                    InfopartyBalance.ReferenceType = "OnAccount";
//                                    InfopartyBalance.AgainstInvoiceNo = "0";
//                                    InfopartyBalance.AgainstVoucherNo = "0";
//                                    InfopartyBalance.AgainstVoucherTypeId = 0;
//                                    InfopartyBalance.VoucherTypeId = 23;
//                                    InfopartyBalance.InvoiceNo = strNewDebitNoteVoucherNo;
//                                    InfopartyBalance.VoucherNo = strNewDebitNoteVoucherNo;
//                                    InfopartyBalance.Credit = 0;
//                                    InfopartyBalance.Debit = balance - amountToApply;
//                                    InfopartyBalance.ExchangeRateId = 1;
//                                    InfopartyBalance.Extra1 = string.Empty;
//                                    InfopartyBalance.Extra2 = string.Empty;
//                                    InfopartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
//                                    PostingHelper.PartyBalanceAdd(InfopartyBalance);

//                                    // =============================== ledger posting ==================================== //
//                                    infoLedgerPosting.ChequeDate = DateTime.Now;
//                                    infoLedgerPosting.ChequeNo = string.Empty;
//                                    infoLedgerPosting.Credit = 0;
//                                    infoLedgerPosting.Date = input.date;
//                                    infoLedgerPosting.Debit = balance - amountToApply;
//                                    infoLedgerPosting.DetailsId = decCreditDetailsId;
//                                    infoLedgerPosting.Extra1 = string.Empty;
//                                    infoLedgerPosting.Extra2 = string.Empty;
//                                    infoLedgerPosting.ExtraDate = DateTime.Now;
//                                    infoLedgerPosting.InvoiceNo = strNewDebitNoteVoucherNo;
//                                    infoLedgerPosting.LedgerId = input.ledgerId;
//                                    infoLedgerPosting.VoucherNo = strNewDebitNoteVoucherNo;
//                                    infoLedgerPosting.VoucherTypeId = 23;
//                                    infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
//                                    // --------------- debit leg -------------------------------------------------- //
//                                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

//                                    // --------------- credit leg --------------------------------------------------- //
//                                    infoLedgerPosting.Credit = balance - amountToApply;
//                                    infoLedgerPosting.Date = input.date;
//                                    infoLedgerPosting.Debit = 0;
//                                    infoLedgerPosting.DetailsId = decDebitDetailsId;
//                                    infoLedgerPosting.Extra1 = string.Empty;
//                                    infoLedgerPosting.LedgerId = existingLedgerId;
//                                    infoLedgerPosting.VoucherNo = strNewDebitNoteVoucherNo;
//                                    infoLedgerPosting.VoucherTypeId = 23;
//                                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
//                                }
//                                catch (Exception ex) { }
//                                createNewDebitNote = false;
//                                #endregion
//                            }
//                        }
//                        #region Used code
//                        if (voucherTypeId == 4) // Payment voucher
//                        {
//                            bool PaymentMasterEdit = false;
//                            string comStr = string.Format("SELECT paymentMasterId FROM tbl_PaymentMaster WHERE " +
//                                "voucherNo = '{0}' ", voucherNo.ToString());
//                            infoPaymentMaster.PaymentMasterId = helperClasses.GetMasterId(comStr);
//                            infoPaymentMaster = spPaymentMaster.PaymentMasterView(infoPaymentMaster.PaymentMasterId);

//                            infoPaymentMaster.VoucherNo = infoPaymentMaster.VoucherNo;
//                            infoPaymentMaster.InvoiceNo = infoPaymentMaster.InvoiceNo;
//                            infoPaymentMaster.SuffixPrefixId = infoPaymentMaster.SuffixPrefixId;
//                            infoPaymentMaster.Date = input.date;
//                            infoPaymentMaster.Narration = input.narration;
//                            infoPaymentMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
//                            infoPaymentMaster.VoucherTypeId = infoPaymentMaster.VoucherTypeId;
//                            infoPaymentMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
//                            infoPaymentMaster.ExtraDate = DateTime.Now;
//                            infoPaymentMaster.Extra1 = string.Empty;
//                            infoPaymentMaster.Extra2 = string.Empty;
//                            infoPaymentMaster.TotalAmount = TotalAmount;
//                            PaymentMasterEdit = true;
//                            //bool isRowAffected = PostingHelper.ReceiptMasterEdit(infoPaymentMaster, decSelectedCurrencyRate);
//                            isRowAffected = PostingHelper.PaymentMasterEdit(infoPaymentMaster, decSelectedCurrencyRate);

//                            if (isRowAffected == true)
//                            {
//                                dt = spPaymentDetails.PaymentDetailsViewByMasterId(infoPaymentMaster.PaymentMasterId);
//                                for (int i = 0; i < dt.Rows.Count; i++)
//                                {
//                                    infoPaymentDetails.Amount = amountToApply;
//                                    infoPaymentDetails.ChequeDate = DateTime.Now;
//                                    infoPaymentDetails.ChequeNo = string.Empty;
//                                    infoPaymentDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
//                                    existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
//                                    infoPaymentDetails.PaymentDetailsId = Convert.ToDecimal(dt.Rows[i]["paymentDetailsId"].ToString());
//                                    infoPaymentDetails.PaymentMasterId = infoPaymentMaster.PaymentMasterId;
//                                    infoPaymentDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["exchangeRateId"].ToString());
//                                    infoPaymentDetails.Extra1 = string.Empty;
//                                    infoPaymentDetails.Extra2 = string.Empty;
//                                    infoPaymentDetails.ExtraDate = DateTime.Now;
//                                    PaymentMasterEdit = false;
//                                    //------------------Currency conversion------------------//
//                                    decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoPaymentDetails.ExchangeRateId);
//                                    PostingHelper.PaymentDetailsEdit(infoPaymentMaster, infoPaymentDetails, PaymentMasterEdit, decSelectedCurrencyRate, input.voucherNo, input.vouchertypeId);
//                                }
//                            }
//                        }

//                        #endregion

//                        #region Create new credit note
//                        if (createNewDebitNote)
//                        {
//                            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
//                            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
//                            TransactionsGeneralFill obj = new TransactionsGeneralFill();
//                            DebitNoteMasterSP spDebitNoteMaster = new DebitNoteMasterSP();
//                            string strNewDebitNoteVoucherNo = string.Empty;
//                            string strNewDebitNoteInvoiceNo = string.Empty;

//                            if (strNewDebitNoteVoucherNo == string.Empty)
//                            {
//                                strNewDebitNoteVoucherNo = "0"; //strMax;
//                            }
//                            //===================================================================================================================//

//                            VoucherTypeSP spVoucherType = new VoucherTypeSP();
//                            bool isAutomaticForDebitNote = spVoucherType.CheckMethodOfVoucherNumbering(23);
//                            if (isAutomaticForDebitNote)
//                            {
//                                strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), DateTime.Now, "DebitNoteMaster");
//                                if (Convert.ToDecimal(strNewDebitNoteVoucherNo) != spDebitnoteMaster.DebitNoteMasterGetMaxPlusOne(23))
//                                {
//                                    strNewDebitNoteVoucherNo = spDebitnoteMaster.DebitNoteMasterGetMax(23).ToString();
//                                    strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), DateTime.Now, "DebitNoteMaster");
//                                    if (spDebitnoteMaster.DebitNoteMasterGetMax(23).ToString() == "0")
//                                    {
//                                        strNewDebitNoteVoucherNo = "0";
//                                        strNewDebitNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(23, Convert.ToDecimal(strNewDebitNoteVoucherNo), DateTime.Now, "DebitNoteMaster");
//                                    }
//                                }
//                                infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(23, DateTime.Now);
//                                strPrefix = infoSuffixPrefix.Prefix;
//                                strSuffix = infoSuffixPrefix.Suffix;
//                                strNewDebitNoteInvoiceNo = strPrefix + strNewDebitNoteVoucherNo + strSuffix;
//                            }
//                            else
//                            {
//                                strNewDebitNoteVoucherNo = input.voucherNo + "_Dr";
//                                strNewDebitNoteInvoiceNo = strNewDebitNoteVoucherNo;
//                            }
//                            // ============================ Debit note master add =========================== //
//                            infoDebitNoteMaster.VoucherNo = strNewDebitNoteVoucherNo;
//                            infoDebitNoteMaster.InvoiceNo = strNewDebitNoteVoucherNo;
//                            infoDebitNoteMaster.SuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
//                            infoDebitNoteMaster.Date = Convert.ToDateTime(input.date);
//                            infoDebitNoteMaster.Narration = input.narration;
//                            infoDebitNoteMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
//                            infoDebitNoteMaster.VoucherTypeId = 23;
//                            infoDebitNoteMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
//                            infoDebitNoteMaster.Extra1 = string.Empty;
//                            infoDebitNoteMaster.Extra2 = string.Empty;
//                            infoDebitNoteMaster.TotalAmount = balance - amountToApply;
//                            decimal decDebitNoteMasterId = spDebitnoteMaster.DebitNoteMasterAdd(infoDebitNoteMaster);

//                            // ========================== DebitNote Details Add =============================== //
//                            spDebitNoteDetails = new DebitNoteDetailsSP();
//                            spLedgerPosting = new LedgerPostingSP();
//                            infoLedgerPosting = new LedgerPostingInfo();
//                            decimal decLedgerId = 0;
//                            decimal decDebit = 0;
//                            decimal decCredit = 0;
//                            try
//                            {
//                                infoDebitNoteDetails.DebitNoteMasterId = decDebitNoteMasterId;
//                                infoDebitNoteDetails.ExchangeRateId = 1;
//                                infoDebitNoteDetails.ExtraDate = DateTime.Now;
//                                infoDebitNoteDetails.Extra1 = string.Empty;
//                                infoDebitNoteDetails.Extra2 = string.Empty;
//                                infoDebitNoteDetails.LedgerId = input.ledgerId;
//                                decLedgerId = infoDebitNoteDetails.LedgerId;
//                                infoDebitNoteDetails.Debit = 0;
//                                infoDebitNoteDetails.Credit = balance - amountToApply;
//                                decDebit = infoDebitNoteDetails.Debit * decSelectedCurrencyRate;
//                                decCredit = infoDebitNoteDetails.Credit * decSelectedCurrencyRate;
//                                infoDebitNoteDetails.ChequeNo = string.Empty;
//                                infoDebitNoteDetails.ChequeDate = input.date;
//                                //-------------------- credit leg ---------------------------- //
//                                decimal decCreditDetailsId = spDebitNoteDetails.DebitNoteDetailsAdd(infoDebitNoteDetails);

//                                infoDebitNoteDetails.LedgerId = 1;
//                                decLedgerId = infoDebitNoteDetails.LedgerId;
//                                infoDebitNoteDetails.Debit = balance - amountToApply;
//                                infoDebitNoteDetails.Credit = 0;
//                                decDebit = infoDebitNoteDetails.Debit * decSelectedCurrencyRate;
//                                decCredit = infoDebitNoteDetails.Credit * decSelectedCurrencyRate;

//                                // -------------------- debit leg --------------------------- //
//                                decimal decDebitDetailsId = spDebitNoteDetails.DebitNoteDetailsAdd(infoDebitNoteDetails);

//                                // -------------------------- party balance add --------------------------- //
//                                spPartyBalance = new PartyBalanceSP();
//                                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
//                                InfopartyBalance.CreditPeriod = 0;//
//                                InfopartyBalance.Date = input.date;
//                                InfopartyBalance.LedgerId = input.ledgerId;
//                                InfopartyBalance.ReferenceType = "OnAccount";
//                                InfopartyBalance.AgainstInvoiceNo = "0";
//                                InfopartyBalance.AgainstVoucherNo = "0";
//                                InfopartyBalance.AgainstVoucherTypeId = 0;
//                                InfopartyBalance.VoucherTypeId = 23;
//                                InfopartyBalance.InvoiceNo = strNewDebitNoteVoucherNo;
//                                InfopartyBalance.VoucherNo = strNewDebitNoteVoucherNo;
//                                InfopartyBalance.Debit = balance - amountToApply;
//                                InfopartyBalance.Credit = 0;
//                                InfopartyBalance.ExchangeRateId = 1;
//                                InfopartyBalance.Extra1 = string.Empty;
//                                InfopartyBalance.Extra2 = string.Empty;
//                                InfopartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
//                                PostingHelper.PartyBalanceAdd(InfopartyBalance);

//                                // =============================== ledger posting ==================================== //
//                                infoLedgerPosting.ChequeDate = DateTime.Now;
//                                infoLedgerPosting.ChequeNo = string.Empty;
//                                infoLedgerPosting.Credit = 0;
//                                infoLedgerPosting.Date = input.date;
//                                infoLedgerPosting.Debit = balance - amountToApply;
//                                infoLedgerPosting.DetailsId = decCreditDetailsId;
//                                infoLedgerPosting.Extra1 = string.Empty;
//                                infoLedgerPosting.Extra2 = string.Empty;
//                                infoLedgerPosting.ExtraDate = DateTime.Now;
//                                infoLedgerPosting.InvoiceNo = strNewDebitNoteVoucherNo;
//                                infoLedgerPosting.LedgerId = input.ledgerId;
//                                infoLedgerPosting.VoucherNo = strNewDebitNoteVoucherNo;
//                                infoLedgerPosting.VoucherTypeId = 23;
//                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
//                                // --------------- debit leg -------------------------------------------------- //
//                                //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

//                                // --------------- credit leg --------------------------------------------------- //
//                                infoLedgerPosting.Credit = balance - amountToApply;
//                                infoLedgerPosting.Date = input.date;
//                                infoLedgerPosting.Debit = 0;
//                                infoLedgerPosting.DetailsId = decDebitDetailsId;
//                                infoLedgerPosting.Extra1 = string.Empty;
//                                infoLedgerPosting.LedgerId = existingLedgerId;
//                                infoLedgerPosting.VoucherNo = strNewDebitNoteVoucherNo;
//                                infoLedgerPosting.VoucherTypeId = 23;
//                                //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
//                            }
//                            catch (Exception ex)
//                            {
//                                response.ResponseCode = 500;
//                                response.ResponseMessage = "something went wrong";
//                            }

//                            //infoPartyBalance.Date = input.date;
//                            //infoPartyBalance.LedgerId = input.ledgerId;
//                            //infoPartyBalance.VoucherNo = input.voucherNo;
//                            //infoPartyBalance.InvoiceNo = input.voucherNo;
//                            //infoPartyBalance.VoucherTypeId = 13;
//                            //infoPartyBalance.AgainstVoucherTypeId = 0;
//                            //infoPartyBalance.AgainstVoucherNo = "0";
//                            //infoPartyBalance.AgainstInvoiceNo = "0";
//                            //infoPartyBalance.ReferenceType = "New";
//                            //infoPartyBalance.Debit = input.grandTotal;
//                            //infoPartyBalance.Credit = 0;
//                            //infoPartyBalance.CreditPeriod = 0;
//                            //infoPartyBalance.ExchangeRateId = 1;
//                            //infoPartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
//                            //infoPartyBalance.ExtraDate = DateTime.Now;
//                            //infoPartyBalance.Extra1 = string.Empty;
//                            //infoPartyBalance.Extra2 = string.Empty;
//                            //spPartyBalance.PartyBalanceAdd(infoPartyBalance);

//                            response.ResponseCode = 200;
//                            response.ResponseMessage = "success";
//                            #endregion
//                        }
//                    }
//                }
//                #endregion
//                #endregion
//            }
//            catch (Exception e)
//            {
//                response.ResponseCode = 500;
//                response.ResponseMessage = "something went wrong";
//            }
//            return response;
//        }

//        public MatResponse GetInvoicesToApplyCredit(decimal ledgerId)
//        {
//            MatResponse response = new MatResponse();
//            DBMatConnection db = new DBMatConnection();
//            dynamic data = new ExpandoObject();
//            List<ApplyCreditInvoices> applyCreditInvoices = new List<ApplyCreditInvoices>();
//            try
//            {
//                var purchaseInvoices = new PurchaseMasterSP().PurchaseMasterViewAll();
//                var accruedExpenseInvoices = db.customSelect("SELECT * FROM tbl_accruedExpensesMaster");
//                var partyBalances = new PartyBalanceSP().PartyBalanceComboViewByLedgerId(ledgerId, "Dr", 0, "");
//                var payments = new PartyBalanceSP().PartyBalanceComboViewByLedgerId(ledgerId, "Cr", 0, "");
//                for (var pb = 0; pb < partyBalances.Rows.Count; pb++)
//                {
//                    if (Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"]) == 13)
//                    {
//                        for (var pi = 0; pi < purchaseInvoices.Rows.Count; pi++)
//                        {
//                            if (partyBalances.Rows[pb]["voucherNo"].ToString() == purchaseInvoices.Rows[pi]["voucherNo"].ToString())
//                            {
//                                applyCreditInvoices.Add(new ApplyCreditInvoices
//                                {
//                                    date = Convert.ToDateTime(purchaseInvoices.Rows[pi]["date"].ToString()),
//                                    grandTotal = Convert.ToDecimal(partyBalances.Rows[pb]["balance"].ToString()),
//                                    ledgerId = Convert.ToDecimal(purchaseInvoices.Rows[pi]["ledgerId"].ToString()),
//                                    narration = purchaseInvoices.Rows[pi]["narration"].ToString(),
//                                    voucherNo = purchaseInvoices.Rows[pi]["voucherNo"].ToString(),
//                                    voucherTypeId = Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"].ToString()),
//                                    //balance = Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"].ToString()),
//                                });
//                            }
//                        }
//                    }
//                    else if (Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"]) == 10032)
//                    {
//                        for (var aei = 0; aei < accruedExpenseInvoices.Rows.Count; aei++)
//                        {
//                            if (partyBalances.Rows[pb]["voucherNo"].ToString() == accruedExpenseInvoices.Rows[aei]["InvoiceNo"].ToString())
//                            {
//                                applyCreditInvoices.Add(new ApplyCreditInvoices
//                                {
//                                    date = Convert.ToDateTime(accruedExpenseInvoices.Rows[aei]["TransactionDate"].ToString()),
//                                    grandTotal = Convert.ToDecimal(partyBalances.Rows[pb]["balance"].ToString()),
//                                    ledgerId = Convert.ToDecimal(accruedExpenseInvoices.Rows[aei]["Supplier"].ToString()),
//                                    narration = "",
//                                    voucherNo = accruedExpenseInvoices.Rows[aei]["InvoiceNo"].ToString(),
//                                    voucherTypeId = Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"].ToString()),
//                                });
//                            }
//                        }
//                    }
//                }
//                data.invoices = applyCreditInvoices;
//                data.payments = payments;
//                response.ResponseCode = 200;
//                response.ResponseMessage = "success";
//                response.Response = data;
//            }
//            catch (Exception e)
//            {
//                response.ResponseCode = 500;
//                response.ResponseMessage = "something went wrong";
//            }
//            return response;
//        }
//        [HttpGet]
//        public HttpResponseMessage getMasterandDetails(decimal id)
//        {
//            dynamic response = new ExpandoObject();

//            response.master = new PaymentMasterSP().PaymentMasterView(id);
//            response.details = new PaymentDetailsSP().PaymentDetailsViewByMasterId(id);

//            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
//        }
//        public void PartyBalanceAddOrEdit(int inJ, CreatePaymentMaster input)
//        {
//            try
//            {
//                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
//                PartyBalanceSP spPartyBalance = new PartyBalanceSP();

//                InfopartyBalance.Credit = 0;
//                InfopartyBalance.CreditPeriod = 0;
//                InfopartyBalance.Date = DateTime.Now;
//                InfopartyBalance.Debit = Convert.ToDecimal(input.PartyBalances[inJ].Credit);
//                InfopartyBalance.ExchangeRateId = 1;
//                InfopartyBalance.Extra1 = string.Empty;
//                InfopartyBalance.Extra2 = string.Empty;
//                InfopartyBalance.ExtraDate = DateTime.Now;
//                InfopartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
//                InfopartyBalance.LedgerId = Convert.ToDecimal(input.PartyBalances[inJ].LedgerId);
//                InfopartyBalance.ReferenceType = input.PartyBalances[inJ].ReferenceType.ToString();
//                if (input.PartyBalances[inJ].ReferenceType.ToString() == "New" || input.PartyBalances[inJ].ReferenceType.ToString() == "OnAccount")
//                {
//                    InfopartyBalance.AgainstInvoiceNo = "0";
//                    InfopartyBalance.AgainstVoucherNo = "0";
//                    InfopartyBalance.AgainstVoucherTypeId = 0;
//                    InfopartyBalance.VoucherTypeId = decPaymentVoucherTypeId;
//                    InfopartyBalance.InvoiceNo = input.invoiceNo;
//                    InfopartyBalance.VoucherNo = input.VoucherNo;
//                }
//                else
//                {
//                    InfopartyBalance.ExchangeRateId = 1;
//                    InfopartyBalance.AgainstInvoiceNo = input.invoiceNo;
//                    InfopartyBalance.AgainstVoucherNo = input.VoucherNo;
//                    InfopartyBalance.AgainstVoucherTypeId = decPaymentVoucherTypeId;
//                    InfopartyBalance.VoucherTypeId = 29;//Convert.ToDecimal(input.PartyBalances[inJ].VoucherTypeId.ToString());
//                    InfopartyBalance.VoucherNo = input.PartyBalances[inJ].VoucherNo.ToString();
//                    InfopartyBalance.InvoiceNo = input.PartyBalances[inJ].InvoiceNo.ToString();
//                }
//                if (input.PartyBalances[inJ].PartyBalanceId.ToString() == "0")
//                {
//                    spPartyBalance.PartyBalanceAdd(InfopartyBalance);
//                }
//                else
//                {
//                    InfopartyBalance.PartyBalanceId = Convert.ToDecimal(input.PartyBalances[inJ].PartyBalanceId.ToString());
//                    spPartyBalance.PartyBalanceEdit(InfopartyBalance);
//                }
//            }
//            catch (Exception ex)
//            {
//            }
//        }

//        public void DetailsLedgerPosting(int inA, decimal decPaymentDetailsId, CreatePaymentMaster input)
//        {
//            LedgerPostingInfo InfoLedgerPosting = new LedgerPostingInfo();
//            LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
//            ExchangeRateSP SpExchangRate = new ExchangeRateSP();
//            PartyBalanceSP spPartyBalance = new PartyBalanceSP();


//            DataTable dtblPartyBalance = new DataTable();
//            dtblPartyBalance = spPartyBalance.PartyBalanceViewByVoucherNoAndVoucherType(decPaymentVoucherTypeId, input.VoucherNo, DateTime.Now);
//            int inTableRowCount = dtblPartyBalance.Rows.Count;

//            decimal decOldExchange = 0;
//            decimal decNewExchangeRate = 0;
//            decimal decNewExchangeRateId = 0;
//            decimal decOldExchangeId = 0;
//            decimal decSelectedCurrencyRate = 0;
//            decimal decAmount = 0;
//            decimal decConvertRate = 0;
//            try
//            {
//                if (input.PaymentDetails[inA].Status == "OnAccount")
//                {
//                    decimal d = input.PaymentDetails[inA].ExchangeRateId;
//                    decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(d);
//                    decAmount = input.PaymentDetails[inA].Amount;
//                    decConvertRate = decAmount * decSelectedCurrencyRate; // isWithholdingTaxApplicable == true ? Convert.ToDecimal(txtPaymentAmt.Text) * decSelectedCurrencyRate : decAmount * decSelectedCurrencyRate;
//                    InfoLedgerPosting.Credit = 0;
//                    InfoLedgerPosting.Date = input.Date;
//                    InfoLedgerPosting.Debit = decConvertRate;
//                    InfoLedgerPosting.DetailsId = decPaymentDetailsId;
//                    InfoLedgerPosting.Extra1 = string.Empty;
//                    InfoLedgerPosting.Extra2 = string.Empty;
//                    InfoLedgerPosting.InvoiceNo = input.invoiceNo;
//                    InfoLedgerPosting.ChequeNo = input.ChequeNo;
//                    InfoLedgerPosting.ChequeDate = input.ChequeDate;
//                    //InfoLedgerPosting.ChequeDate = Convert.ToDateTime(txtChqDate.Text.ToString());

//                    InfoLedgerPosting.LedgerId = input.PaymentDetails[inA].LedgerId;
//                    InfoLedgerPosting.VoucherNo = input.VoucherNo;
//                    InfoLedgerPosting.VoucherTypeId = decPaymentVoucherTypeId;
//                    InfoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
//                    SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
//                }
//                else
//                {
//                    InfoLedgerPosting.Date = input.Date;

//                    InfoLedgerPosting.Extra1 = string.Empty;
//                    InfoLedgerPosting.Extra2 = string.Empty;
//                    InfoLedgerPosting.InvoiceNo = input.invoiceNo;
//                    InfoLedgerPosting.VoucherTypeId = decPaymentVoucherTypeId;
//                    InfoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
//                    InfoLedgerPosting.Credit = 0;
//                    InfoLedgerPosting.LedgerId = input.PaymentDetails[inA].LedgerId;
//                    InfoLedgerPosting.VoucherNo = input.VoucherNo;
//                    InfoLedgerPosting.DetailsId = decPaymentDetailsId;
//                    InfoLedgerPosting.InvoiceNo = input.VoucherNo;
//                    InfoLedgerPosting.ChequeNo = input.ChequeNo;
//                    InfoLedgerPosting.ChequeDate = input.ChequeDate;
//                    foreach (var dr in input.PartyBalances)
//                    {
//                        if (InfoLedgerPosting.LedgerId == dr.LedgerId)
//                        {
//                            decOldExchange = 1;
//                            decNewExchangeRateId = 1;
//                            decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchange);
//                            decAmount = Convert.ToDecimal(dr.Amount);
//                            decConvertRate = (decAmount * decSelectedCurrencyRate);
//                        }
//                    }
//                    InfoLedgerPosting.Debit = decConvertRate; // isWithholdingTaxApplicable == true ? Convert.ToDecimal(txtPaymentAmt.Text) * decSelectedCurrencyRate : decConvertRate;
//                    SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);

//                    InfoLedgerPosting.LedgerId = 12;
//                    InfoLedgerPosting.DetailsId = 0;
//                    foreach (var dr in input.PartyBalances)
//                    {
//                        if (input.PaymentDetails[inA].LedgerId == Convert.ToDecimal(dr.LedgerId))
//                        {
//                            if (dr.ReferenceType.ToString() == "Against")
//                            {
//                                decNewExchangeRateId = 1;
//                                decNewExchangeRate = SpExchangRate.GetExchangeRateByExchangeRateId(decNewExchangeRateId);
//                                decOldExchangeId = 1;
//                                decOldExchange = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchangeId);
//                                decAmount = Convert.ToDecimal(dr.Amount);
//                                decimal decForexAmount = (decAmount * decNewExchangeRate) - (decAmount * decOldExchange);
//                                if (decForexAmount >= 0)
//                                {
//                                    InfoLedgerPosting.Debit = decForexAmount;
//                                    InfoLedgerPosting.Credit = 0;
//                                }
//                                else
//                                {
//                                    InfoLedgerPosting.Credit = -1 * decForexAmount;
//                                    InfoLedgerPosting.Debit = 0;
//                                }
//                                SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
//                            }
//                        }

//                    }
//                }
//            }
//            catch (Exception ex)
//            {
//            }
//        }


//    }

//    public class AdvancePaymentVM
//    {
//        public decimal voucheerTypeId { get; set; }
//        public string voucherType { get; set; }
//        public string voucherNo { get; set; }
//        public decimal value { get; set; }
//        public decimal exchangeRateId { get; set; }
//        public string invoiceNo { get; set; }
//        public decimal amountToApply { get; set; }
//    }
//    public class ApplyCreditVM
//    {
//        public decimal ledgerId { get; set; }
//        public DateTime date { get; set; }
//        public decimal grandTotal { get; set; }
//        public string voucherNo { get; set; }
//        public string narration { get; set; }
//        public decimal vouchertypeId { get; set; }
//        public List<AdvancePaymentVM> advancePayments { get; set; }
//    }
//    public class ApplyCreditInvoices
//    {
//        public decimal ledgerId { get; set; }
//        public DateTime date { get; set; }
//        public decimal grandTotal { get; set; }
//        public string voucherNo { get; set; }
//        public string narration { get; set; }
//        public decimal voucherTypeId { get; set; }
//        public decimal balance { get; set; }
//    }
//}

