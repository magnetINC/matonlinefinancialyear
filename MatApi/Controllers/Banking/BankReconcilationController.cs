﻿using System.Dynamic;
using MatApi.Models;
using MATFinancials;
using System.Data;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Banking
{
    [EnableCors(origins:"*", headers:"*", methods:"*")]
    public class BankReconcilationController : ApiController
    {
        public BankReconcilationController()
        {

        }
        [HttpGet]
        public MatResponse GetLookUps()
        {
            dynamic lookUps = new ExpandoObject();
            MatResponse response = new MatResponse();

            try
            {
                lookUps.accounts = new TransactionsGeneralFill().BankComboFill();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
                response.Response = e;
            }

            return response;
        }
        [HttpGet]
        public MatResponse GetReconciledOrUnreconciledToList(decimal lid, DateTime tdt, DateTime fdt, string st)
        {
            MatResponse response = new MatResponse();
            try
            {
                BankReconciliationInfo infoBankReconciliation = new BankReconciliationInfo();
                BankReconciliationSP spBankReconciliation = new BankReconciliationSP();

                DataTable dtblBank = new DataTable();
                if (st == "Reconciled")
                {
                    dtblBank = spBankReconciliation.BankReconciliationFillReconcile(lid, fdt, tdt);
                }
                else
                {
                    dtblBank = spBankReconciliation.BankReconciliationUnrecocile(lid, fdt, tdt, false);
                }

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = dtblBank;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
        [HttpPost]
        public MatResponse Save(List<BankReconciliationInfo> input)
        {
            MatResponse response = new MatResponse();
            try
            {
                BankReconciliationInfo infoBankReconciliation = new BankReconciliationInfo();
                BankReconciliationSP spBankReconciliation = new BankReconciliationSP();
                for (var i = 0; i < input.Count(); i++)
                {
                    infoBankReconciliation.LedgerPostingId = input[i].LedgerPostingId;
                    infoBankReconciliation.StatementDate = input[i].StatementDate;
                    infoBankReconciliation.Extra1 = string.Empty;
                    infoBankReconciliation.Extra2 = string.Empty;
                    infoBankReconciliation.ExtraDate = MATFinancials.PublicVariables._dtCurrentDate;
                    infoBankReconciliation.ReconStartDate = input[i].ReconStartDate;
                    infoBankReconciliation.ReconEndDate = input[i].ReconEndDate;
                    decimal decReconcileId = spBankReconciliation.BankReconciliationLedgerPostingId(input[i].LedgerPostingId);
                    if (decReconcileId != 0)
                    {
                        infoBankReconciliation.ReconcileId = decReconcileId;
                        spBankReconciliation.BankReconciliationEdit(infoBankReconciliation);

                        response.ResponseCode = 200;
                        response.ResponseMessage = "Edited Successfully";
                    }
                    else
                    {
                        spBankReconciliation.BankReconciliationAdd(infoBankReconciliation);
                        response.ResponseCode = 200;
                        response.ResponseMessage = "success";
                    }
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
    }
}
