﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Dynamic;
using MatApi.Models;
using MATFinancials;
using System.Data;
using MATFinancials.DAL;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Banking
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class BankRegisterController : ApiController
    {
        public BankRegisterController()
        {

        }

        [HttpGet]
        public  MatResponse GetlookUps()
        {
            dynamic lookUps = new ExpandoObject();
            MatResponse response = new MatResponse();

            try
            {
                lookUps.banks = new TransactionsGeneralFill().BankComboFill();
                lookUps.cash = new AccountLedgerSP().CashComboFill();
                lookUps.allLedgers = new AccountLedgerSP().AccountLedgerViewAllForComboBox();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
                response.Response = e;
            }

            return response;
        }

        [HttpGet]
        public MatResponse BankRegister(decimal lid, DateTime tdt, DateTime fdt, string lname)
        {
            dynamic lookUps = new ExpandoObject();
            MatResponse response = new MatResponse();
            try
            {
                FinancialStatementSP spFinance = new FinancialStatementSP();

                lookUps.register = spFinance.CashRegister(fdt, tdt, lid, "bank register".ToLower(), lname);
                if (lid != 0)
                {
                    lookUps.closingBalance = GetCurrentClosingBalance(fdt, lid);
                }
                else
                {
                    lookUps.closingBalance = GetCurrentClosingBalanceForBank(fdt);
                }

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
        [HttpGet]
        public MatResponse CashRegister(decimal lid, DateTime tdt, DateTime fdt, string lname)
        {
            MatResponse response = new MatResponse();
            dynamic lookUps = new ExpandoObject();
            try
            {
                FinancialStatementSP spFinance = new FinancialStatementSP();
                lookUps.registers = spFinance.CashRegister(fdt, tdt, lid, "cash register".ToLower(), lname);
                if (lid != 0)
                {
                    lookUps.closingBalance = GetCurrentClosingBalance(fdt, lid);
                }
                else
                {
                    lookUps.closingBalance = GetCurrentClosingBalanceForCash(fdt);
                }
                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }

        [HttpGet]
        public MatResponse OtherRegister(decimal lid, DateTime tdt, DateTime fdt)
        {
            dynamic lookUps = new ExpandoObject();
            DBMatConnection conn = new DBMatConnection();
            MatResponse response = new MatResponse();
            DataTable registers = new DataTable();
            List<OtherRegister> registerDtbl = new List<OtherRegister>();
            List<OtherRegistersDetails> registerDtbl2 = new List<OtherRegistersDetails>();

            string transactionType = "";
            decimal currentBalance = 0;
            decimal openingBalance = 0;
            decimal currentLedgerId = 0;
            decimal previousLedgerId = 0;
            int i = 1;
            try
            {
                AccountLedgerSP ledger = new AccountLedgerSP();

                registers = ledger.OtherRegister(fdt, tdt, lid);
                foreach (DataRow dr in registers.Rows)
                {
                    transactionType = dr["Ttype"].ToString();
                    currentLedgerId = Convert.ToDecimal(dr["ledgerId"]);
                    registerDtbl2.Add(new OtherRegistersDetails
                    {
                        Date = Convert.ToDateTime(dr["eDate"].ToString()),
                        LedgerName = dr["ledgerName"].ToString(),
                        Credit = Convert.ToDecimal(dr["Credit"].ToString()),
                        Debit = Convert.ToDecimal(dr["Debit"].ToString()),
                        InvoiceNo = dr["invoiceNo"].ToString(),
                        LedgerId = Convert.ToDecimal(dr["ledgerId"].ToString()),
                        Memo = dr["Memo"].ToString(),
                        TransactionType = dr["Ttype"].ToString(),
                        VoucherName = dr["voucherTypeName"].ToString(),
                        VoucherTypeId = Convert.ToDecimal(dr["voucherTypeId"].ToString()),
                    });
                    //if (currentLedgerId != previousLedgerId && previousLedgerId != 0 && i != 0)
                    //{
                    //    registerDtbl2.Add(new OtherRegistersDetails
                    //    {
                    //        LedgerName = dr["ledgerName"].ToString(),
                    //        Credit = Convert.ToDecimal(dr["Credit"].ToString()),
                    //        Debit = Convert.ToDecimal(dr["Debit"].ToString()),
                    //        InvoiceNo = dr["invoiceNo"].ToString(),
                    //        LedgerId = Convert.ToDecimal(dr["ledgerId"].ToString()),
                    //        Memo = dr["Memo"].ToString(),
                    //        TransactionType = dr["Ttype"].ToString(),
                    //        VoucherName = dr["voucherTypeName"].ToString(),
                    //        VoucherTypeId = Convert.ToDecimal(dr["voucherTypeId"].ToString()),
                    //    });
                    //    if (transactionType == "Dr")
                    //    {
                    //        currentBalance += Convert.ToDecimal(dr["Debit"].ToString()) - Convert.ToDecimal(dr["Credit"].ToString());   // Customer
                    //    }
                    //    else
                    //    {
                    //        currentBalance += Convert.ToDecimal(dr["Credit"].ToString()) - Convert.ToDecimal(dr["Debit"].ToString());   // supplier
                    //    }
                    //    registerDtbl.Add(new OtherRegister
                    //    {
                    //        LedgerName = dr["ledgerName"].ToString(),
                    //        ClosingBalance = currentBalance,
                    //        Details = registerDtbl2
                    //    });
                    //    i++;
                    //}
                    previousLedgerId = currentLedgerId;
                    i++;
                   
                }
                if (Convert.ToDecimal(lid) != 0)
                {
                    string otherLedgers = string.Format(" select al.ledgerId, al.ledgerName, al.crOrDr from tbl_AccountLedger al where al.ledgerId = '{0}' ", lid);
                    DataSet ds = conn.ExecuteQuery(otherLedgers);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        string queryStr = "";
                        DataTable dt = new DataTable(); dt = ds.Tables[0];
                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["crOrDr"].ToString() == "Cr")
                            {
                                queryStr = string.Format("SELECT ISNULL(SUM(credit - debit),0) as ClosingBalance from tbl_LedgerPosting WHERE tbl_LedgerPosting.ledgerId ='{0}' and Date <= '{1}'", lid, fdt);

                            }
                            else
                            {
                                queryStr = string.Format("SELECT ISNULL(SUM(debit - credit),0) as ClosingBalance from tbl_LedgerPosting WHERE tbl_LedgerPosting.ledgerId ='{0}' and Date <= '{1}'", lid, fdt);

                            }
                            decimal balance = Convert.ToDecimal(conn.getSingleValue(queryStr));
                            openingBalance += balance;
                        }
                    }
                }
                else
                {
                    string otherLedgers = " select al.ledgerId, al.ledgerName, al.crOrDr from tbl_AccountLedger al where al.accountGroupId NOT IN(27, 28) ";
                    DataSet ds = conn.ExecuteQuery(otherLedgers);
                    if (ds != null && ds.Tables.Count > 0)
                    {
                        DataTable dt = new DataTable(); dt = ds.Tables[0];
                        string queryStr = "";
                        foreach (DataRow row in dt.Rows)
                        {
                            if (row["crOrDr"].ToString() == "Cr")
                            {
                                queryStr = string.Format("SELECT ISNULL(SUM(credit - debit),0) as ClosingBalance from tbl_LedgerPosting WHERE tbl_LedgerPosting.ledgerId ='{0}' and Date <= '{1}'", row["ledgerId"], fdt);
                            }
                            else
                            {
                                queryStr = string.Format("SELECT ISNULL(SUM(debit - credit),0) as ClosingBalance from tbl_LedgerPosting WHERE tbl_LedgerPosting.ledgerId ='{0}' and Date <= '{1}'", row["ledgerId"], fdt);
                            }
                            decimal balance = Convert.ToDecimal(conn.getSingleValue(queryStr));
                            openingBalance += balance;
                        }
                    }
                }
                lookUps.register = registerDtbl2.GroupBy(p=>p.LedgerName);
                lookUps.balance = openingBalance;
                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
        public decimal GetCurrentClosingBalance(DateTime currentDate, decimal ledgerId)
        {
            MATFinancials.DAL.DBMatConnection _db = new MATFinancials.DAL.DBMatConnection();
            decimal ClosingBalance = 0;
            DataSet ds = _db.ExecuteQuery("SELECT ISNULL(SUM(debit-credit),0) as ClosingBalance from tbl_LedgerPosting WHERE tbl_LedgerPosting.ledgerId ='" + ledgerId + "' and Date <= '" + currentDate.AddDays(-1) +  /*+ DateTime.Now.AddDays(-1) +*/ "'");
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ClosingBalance = Convert.ToDecimal(ds.Tables[0].Rows[0]["ClosingBalance"]);
            }
            else
            {
                ClosingBalance = 0m;
            }
            return ClosingBalance;
        }
        public decimal GetCurrentClosingBalanceForCash(DateTime currentDate)
        {
            MATFinancials.DAL.DBMatConnection _db = new MATFinancials.DAL.DBMatConnection();
            decimal ClosingBalance = 0;
            DataSet ds = _db.ExecuteQuery("select ISNULL(SUM(debit-credit),0) as ClosingBalance from tbl_LedgerPosting inner join tbl_AccountLedger al on al.ledgerId = tbl_LedgerPosting.ledgerId where al.accountGroupId = 27 and Date <= '" + currentDate.AddDays(-1) +  /*+ DateTime.Now.AddDays(-1) +*/ "'");
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ClosingBalance = Convert.ToDecimal(ds.Tables[0].Rows[0]["ClosingBalance"]);
            }
            else
            {
                ClosingBalance = 0m;
            }
            return ClosingBalance;
        }
        public decimal GetCurrentClosingBalanceForBank(DateTime currentDate)
        {
            MATFinancials.DAL.DBMatConnection _db = new MATFinancials.DAL.DBMatConnection();
            decimal ClosingBalance = 0;
            DataSet ds = _db.ExecuteQuery("select ISNULL(SUM(debit-credit),0) as ClosingBalance from tbl_LedgerPosting inner join tbl_AccountLedger al on al.ledgerId = tbl_LedgerPosting.ledgerId where al.accountGroupId = 28 and Date <= '" + currentDate.AddDays(-1) +  /*+ DateTime.Now.AddDays(-1) +*/ "'");
            if (ds != null && ds.Tables.Count > 0 && ds.Tables[0].Rows.Count > 0)
            {
                ClosingBalance = Convert.ToDecimal(ds.Tables[0].Rows[0]["ClosingBalance"]);
            }
            else
            {
                ClosingBalance = 0m;
            }
            return ClosingBalance;
        }
    }
    public class OtherRegistersDetails
    {
        public DateTime Date { get; set; }
        public string LedgerName { get; set; }
        public string TransactionType { get; set; }
        public string Memo { get; set; }
        public string VoucherName { get; set; }
        public decimal Credit { get; set; }
        public decimal Debit { get; set; }
        public decimal LedgerId { get; set; }
        public decimal VoucherTypeId { get; set; }
        public string InvoiceNo { get; set; }
    }
    public class OtherRegister
    {
        public string LedgerName { get; set; }
        public List<OtherRegistersDetails> Details { get; set; }
        public decimal ClosingBalance { get; set; }
    }
}
