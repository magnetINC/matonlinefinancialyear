﻿using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PayElementController : ApiController
    {
        PayHeadSP payHeadSp;
        public PayElementController()
        {
            payHeadSp = new PayHeadSP();
        }

        public HttpResponseMessage GetPayElements()
        {
            var payElementsDt = payHeadSp.PayHeadViewAll();
            var ledgers = new AccountLedgerSP().AccountLedgerViewAll();
            PayElementViewModel response = new PayElementViewModel
            {
                AccountLedger = ledgers,
                PayElement = payElementsDt,
                AccountGroup = new AccountGroupSP().AccountGroupViewAll(),
            };
            return Request.CreateResponse(HttpStatusCode.OK, response);
        }

        public HttpResponseMessage GetPayElement(decimal payElementId)
        {
            var payElementDt = payHeadSp.PayHeadView(payElementId);            
            return Request.CreateResponse(HttpStatusCode.OK, payElementDt);
        }

        [HttpPost]
        public bool DeletePayElement(PayHeadInfo payElementInfo)
        {
            return payHeadSp.PayHeadDelete(payElementInfo.PayHeadId);
        }

        [HttpPost]
        public bool AddPayElement(PayHeadInfo payElementInfo)
        {
            payElementInfo.ExtraDate = DateTime.Now;
            payElementInfo.Extra1 = "";
            payElementInfo.Extra2 = "";
            payElementInfo.Narration = "";
            return payHeadSp.PayHeadAdd(payElementInfo);
        }
        [HttpPost]
        public bool EditPayElement(PayHeadInfo payElementInfo)
        {
            payElementInfo.ExtraDate = DateTime.Now;
            payElementInfo.Extra1 = "";
            payElementInfo.Extra2 = "";
            payElementInfo.Narration = "";
            return payHeadSp.PayHeadEdit(payElementInfo);
        }
    }
}
