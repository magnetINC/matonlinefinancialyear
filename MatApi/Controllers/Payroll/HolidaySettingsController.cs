﻿using MATFinancials;
using MatApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class HolidaySettingsController : ApiController
    {
        public HolidaySettingsController()
        {

        }

        [HttpGet]
        public MatResponse GetHolidays()
        {
            MatResponse response = new MatResponse();

            try
            {
                DataTable dtblHolidayRegister = new DataTable();
                dtblHolidayRegister = new HolidaySP().HolidayViewAll();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = dtblHolidayRegister;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = ex;
            }
            return response;
        }

        [HttpPost]
        public MatResponse SaveNewHoliday(HolidayInfo input)
        {
            MatResponse response = new MatResponse();

            try
            {
                new HolidaySP().HolidayAddWithIdentity(input);

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = ex;
            }
            return response;
        }

        [HttpGet]
        public MatResponse DeleteHoliday(decimal holidayId)
        {
            MatResponse response = new MatResponse();

            try
            {
                if(new HolidaySP().HolidayDelete(holidayId) > 0)
                {
                    response.ResponseCode = 200;
                    response.ResponseMessage = "success";
                }

            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = ex;
            }
            return response;
        }
    }
}
