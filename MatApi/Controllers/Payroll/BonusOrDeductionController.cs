﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using MATFinancials;
using MatApi.Models;
using System.Dynamic;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins:"*",headers:"*",methods:"*")]
    public class BonusOrDeductionController : ApiController
    {
        public BonusOrDeductionController()
        {

        }

        [HttpGet]
        public MatResponse GetLookUps()
        {
            dynamic lookUps = new ExpandoObject();
            MatResponse response = new MatResponse();

            try
            {
                lookUps.accounts = new AccountLedgerSP().AccountLedgerViewByAccountGroup(15);
                lookUps.employees = new EmployeeSP().EmployeeViewAll();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
                response.Response = e;
            }

            return response;
        }

        [HttpPost]
        public MatResponse SaveBonusOrDeduction(BonusDedutionInfo input)
        {
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            BonusDedutionSP spBonusDeduction = new BonusDedutionSP();

            MatResponse response = new MatResponse();
            try
            {
                if (spBonusDeduction.BonusDeductionAddIfNotExist(input))
                {
                    response.ResponseCode = 200;
                    response.ResponseMessage = "success";
                }
                else
                {
                    response.ResponseCode = 200;
                    response.ResponseMessage = "Employee bonus deduction already exist";
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 200;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
        
        [HttpGet]
        public MatResponse GetBonusesOrDeductionsForRegister(DateTime month)
        {
            MatResponse response = new MatResponse();
            try
            {
                DataTable dtblBonusDeduction = new DataTable();
                BonusDedutionSP spBonusDeduction = new BonusDedutionSP();
                dtblBonusDeduction = spBonusDeduction.BonusDeductionSearch("", month);

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = dtblBonusDeduction;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetBonusesOrDeductionsDetailsForRegister(decimal bonusOrdeductionid)
        {
            MatResponse response = new MatResponse();
            try
            {
                BonusDedutionInfo infoBonusDeduction = new BonusDedutionInfo();
                BonusDedutionSP spBonusDeduction = new BonusDedutionSP();
                infoBonusDeduction = spBonusDeduction.BonusDeductionViewForUpdate(bonusOrdeductionid);

            }
            catch (Exception ex)
            {

            }
            return response;
        }

        [HttpGet]
        public MatResponse DeleteBonusesOrDeductions(decimal bonusOrDeductionsId, DateTime month)
        {
            MatResponse response = new MatResponse();
            try
            {
                BonusDedutionSP spBonusDeduction = new BonusDedutionSP();
                if ((spBonusDeduction.BonusDeductionReferenceDelete(bonusOrDeductionsId, month)) == -1)
                {
                    response.ResponseCode = 203;
                    response.ResponseMessage = "Can't delete,reference exist";
                }
                else
                {
                    response.ResponseCode = 200;
                    response.ResponseMessage = "success";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
    }
}
