﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Dynamic;
using MatApi.Models;
using MATFinancials;
using System.Data;
using MATFinancials.DAL;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DailySalaryController : ApiController
    {

        decimal decDailyVoucherTypeId = 26;
        decimal decDailySuffixPrefixId = 0;
        decimal decMasterId = 0;
        decimal decMasterIdforEdit = 0;

        int inNarrationCount = 0;
        int dgvcell = 0;

        string strTableName = "DailySalaryVoucherMaster";
        string strVoucherNo = string.Empty;
        string strInvoiceNo = string.Empty;
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        string strEployeeNames = string.Empty;
        string strVoucherNoforEdit = "0";
        string strUpdatedVoucherNo = string.Empty;
        string strUpdatedInvoiceNo = string.Empty;
        string strledgerId;

        bool isAutomatic = false;
        bool @isEditmode = false;
        
        [HttpGet]
        public MatResponse GetLookUps()
        {
            dynamic lookUps = new ExpandoObject();
            MatResponse response = new MatResponse();

            try
            {
                lookUps.cashOrBank = new TransactionsGeneralFill().CashOrBankComboFill();
                lookUps.invoiceNo = VoucherNumberGeneration();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
                response.Response = e;
            }

            return response;
        }

        [HttpPost]
        public MatResponse SaveSalary(DailySalaryProcessingVM input)
        {
            MatResponse response = new MatResponse();
            var voucherMessage = "";
            try
            {

                DailySalaryVoucherMasterInfo infoMaster = new DailySalaryVoucherMasterInfo();
                DailySalaryVoucherMasterSP spMaster = new DailySalaryVoucherMasterSP();
                DailySalaryVoucherDetailsInfo infoDetails = new DailySalaryVoucherDetailsInfo();
                DailySalaryVoucherDetailsSP spDetails = new DailySalaryVoucherDetailsSP();

                var master = input.master;
                var details = input.details;

                //-------------In multi user case check whether salary is paying for the same persone--------------//
                int inCounts = details.Count();
                int incont = 0;
                decimal decVal = 0;
                for (int i = 0; i < inCounts; i++)
                {
                    decVal = details[i].EmployeeId;
                    if (spDetails.CheckWhetherDailySalaryAlreadyPaid(decVal, master.SalaryDate) != "0")
                    {
                        strEployeeNames = strEployeeNames + spDetails.CheckWhetherDailySalaryAlreadyPaid(decVal, master.SalaryDate) + ",";
                        foreach (char ch in strEployeeNames)
                        {
                            if (ch == ',')
                            {
                                incont++;
                            }
                        }
                        if (incont == 15)
                        {
                            incont = 0;
                            strEployeeNames = strEployeeNames + Environment.NewLine;
                        }

                    }
                }
                if (spDetails.CheckWhetherDailySalaryAlreadyPaid(decVal, master.SalaryDate) != "0")
                {
                    response.ResponseCode = 205;
                    response.ResponseMessage = " Salary already paid for - " + " " + strEployeeNames;
                    return response;
                }
                else
                {
                    infoMaster.VoucherNo = master.VoucherNo;
                    infoMaster.Date = master.Date;
                    infoMaster.SalaryDate = master.SalaryDate;
                    infoMaster.LedgerId = master.LedgerId;
                    infoMaster.Narration = master.Narration;
                    infoMaster.TotalAmount = master.TotalAmount;
                    infoMaster.Extra1 = string.Empty; // Fields not in design//
                    infoMaster.Extra2 = string.Empty; // Fields not in design//
                    infoMaster.InvoiceNo = master.InvoiceNo;
                    infoMaster.SuffixPrefixId = decDailySuffixPrefixId;
                    infoMaster.VoucherTypeId = decDailyVoucherTypeId;
                    infoMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;

                    int inval = 0;
                    int inCount = details.Count(); ;
                    for (int i = 0; i < inCount; i++)
                    {
                        if (details[i].Status == "Paid")
                        {
                            inval++;
                        }

                    }
                    if (inval >= 0)
                    {
                        //-------------checks Voucher No. repeating in Multi user case----------//
                        DataTable dtbl = new DataTable();
                        dtbl = spMaster.DailySalaryVoucherMasterAddWithIdentity(infoMaster, isAutomatic);
                        foreach (DataRow dr in dtbl.Rows)
                        {
                            decMasterId = Convert.ToDecimal(dr.ItemArray[0].ToString());
                            strUpdatedVoucherNo = dr.ItemArray[1].ToString();
                            strUpdatedInvoiceNo = dr.ItemArray[2].ToString();
                        }
                        if (Convert.ToDecimal(strUpdatedVoucherNo) != Convert.ToDecimal(master.VoucherNo))
                        {
                            strVoucherNo = strUpdatedVoucherNo.ToString();
                            strInvoiceNo = strUpdatedInvoiceNo;

                            voucherMessage = "Voucher number changed from  " + master.VoucherNo + "  to  " + strUpdatedInvoiceNo;


                        }
                        else
                        {
                            strVoucherNo = master.VoucherNo;
                            strInvoiceNo = master.VoucherNo;
                        }
                        //-------------------------------------//
                        LedgerPosting(master);

                        infoDetails.DailySalaryVocherMasterId = decMasterId;
                        infoDetails.Extra1 = string.Empty;// Fields not in design//
                        infoDetails.Extra2 = string.Empty;// Fields not in design//
                        int inRowCount = details.Count();
                        for (int i = 0; i < inRowCount; i++)
                        {
                            infoDetails.EmployeeId = details[i].EmployeeId;
                            infoDetails.Wage = details[i].Wage;
                            infoDetails.Status = details[i].Status;

                            if (details[i].Status == "Paid" && details[i].DailySalaryVocherMasterId == 0)
                            {
                                infoDetails.DailySalaryVocherMasterId = decMasterId;
                                spDetails.DailySalaryVoucherDetailsAdd(infoDetails);
                            }
                        }
                        response.ResponseCode = 200;
                        response.ResponseMessage = "Saved Successfully, But, " + voucherMessage;
                        return response;

                    }
                    else
                    {
                        response.ResponseCode = 205;
                        response.ResponseMessage = "Can't save without atleast one employee";
                    }
                }
            }
            catch(Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong.";
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetDailySalaryForProcessing(DateTime date)
        {
            MatResponse response = new MatResponse();
            try
            {
                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = new DailySalaryVoucherDetailsSP().DailySalaryVoucherDetailsGridViewAll(date.ToString(), false, 0.ToString()); ;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
                response.Response = e;
            }

            return response;
        }

        public string VoucherNumberGeneration()
        {
            try
            {
                TransactionsGeneralFill TransactionsGeneralFillobj = new TransactionsGeneralFill();
                DailySalaryVoucherMasterSP spmaster = new DailySalaryVoucherMasterSP();
                //--------------------------Automatic Generation------------------------//
                if (strVoucherNo == string.Empty)
                {


                    strVoucherNo = "0";
                }
                strVoucherNo = TransactionsGeneralFillobj.VoucherNumberAutomaicGeneration(decDailyVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strTableName);
                if (Convert.ToDecimal(strVoucherNo) != spmaster.SalaryVoucherMasterGetMaxPlusOne(decDailyVoucherTypeId))
                {
                    strVoucherNo = spmaster.SalaryVoucherMasterGetMaxPlusOne(decDailyVoucherTypeId).ToString();
                    strVoucherNo = TransactionsGeneralFillobj.VoucherNumberAutomaicGeneration(decDailyVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strTableName);
                    if (spmaster.DailySalaryVoucherMasterGetMax(decDailyVoucherTypeId) == "0")
                    {
                        strVoucherNo = "0";
                        strVoucherNo = TransactionsGeneralFillobj.VoucherNumberAutomaicGeneration(decDailyVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strTableName);
                    }
                }
                //---------------------------------------------------------------------------------//
                if (isAutomatic)
                {
                    SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                    SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                    infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decDailyVoucherTypeId, DateTime.Now);
                    strPrefix = infoSuffixPrefix.Prefix;
                    strSuffix = infoSuffixPrefix.Suffix;
                    strInvoiceNo = strPrefix + strVoucherNo + strSuffix;
                }
            }
            catch (Exception ex)
            {
            }
            return strVoucherNo;
        }
        public void LedgerPosting(DailySalaryVoucherMasterInfo input)
        {
            try
            {
                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();

                infoLedgerPosting.Debit = 0;
                infoLedgerPosting.Credit = input.TotalAmount;
                infoLedgerPosting.VoucherTypeId = decDailyVoucherTypeId;
                infoLedgerPosting.VoucherNo = strInvoiceNo;
                infoLedgerPosting.Date = input.Date;
                infoLedgerPosting.LedgerId = input.LedgerId;
                infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.InvoiceNo = strInvoiceNo;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;

                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                infoLedgerPosting.Debit = input.TotalAmount;
                infoLedgerPosting.Credit = 0;
                infoLedgerPosting.VoucherTypeId = decDailyVoucherTypeId;
                infoLedgerPosting.VoucherNo = strInvoiceNo;
                infoLedgerPosting.Date = MATFinancials.PublicVariables._dtCurrentDate;
                infoLedgerPosting.LedgerId = 4;
                infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.InvoiceNo = strInvoiceNo;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.YearId = MATFinancials.   PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

            }
            catch (Exception ex)
            {
            }
        }
    }
    public class DailySalaryProcessingVM
    {
        public DailySalaryVoucherMasterInfo master { get; set; }
        public List<DailySalaryVoucherDetailsInfo> details { get; set; }
    }
}
