﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Dynamic;
using MatApi.Models;
using MATFinancials;
using System.Data;
using MATFinancials.DAL;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins:"*", headers:"*", methods:"*")]
    public class AdvancePaymentController : ApiController
    {
        public AdvancePaymentController()
        {
        }
        decimal decPaymentVoucherTypeId = 10028;
        string str = string.Empty;
        string strFormName = "frmAdvancePayment";
        static string strPaymentVoucherTypeId = string.Empty;
        decimal decPaymentSuffixPrefixId = 0;
        decimal decAdvancePaymentsId;
        decimal decAdvancePaymentId;
        string strUpdatedVoucherNumber = string.Empty;
        string strSuffix = string.Empty;
        string strPrefix = string.Empty;
        string strUpdatedInvoiceNumber = string.Empty;
        string strVoucherNo = string.Empty;
        string strInvoiceNo = string.Empty;
        bool isLoad = false;
        string strEmployeeId;
        string strAdvancePayment = "AdvancePayment";
        bool isAutomatic = false;
        int inNarrationCount = 0;

        [HttpGet]
        public MatResponse GetLookUps()
        {
            MatResponse response = new MatResponse();
            dynamic lookUps = new ExpandoObject();

            try
            {
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                DataTable dtblAccountLedgers = new DataTable();
                dtblAccountLedgers = spAccountLedger.AccountLedgerViewAll();
                DataTable dt = new DataTable();
                dt.Columns.Add("ledgerId");
                dt.Columns.Add("ledgerName");
                DataRow row = null;
                var currentAssetsAccount = (from i in dtblAccountLedgers.AsEnumerable()
                                            where i.Field<decimal>("accountGroupId") == 6
                                            select new
                                            {
                                                accountGroupId = i.Field<decimal>("ledgerId"),
                                                accountGroupName = i.Field<string>("ledgerName")
                                            }).ToList();
                foreach (var rowObj in currentAssetsAccount)
                {
                    row = dt.NewRow();
                    dt.Rows.Add(rowObj.accountGroupId, rowObj.accountGroupName);
                }

                AdvancePaymentSP spAdvancePayment = new AdvancePaymentSP();
                DataTable dtblspAdvancePayment = new DataTable();
                dtblspAdvancePayment = spAdvancePayment.AdvancePaymentEmployeeComboFill();


                DBMatConnection db = new DBMatConnection();
                decimal formNo = 0;
                string query = string.Empty;
                string id = db.getSingleValue("SELECT top 1 voucherNo from tbl_AdvancePayment_Pending order by advancePaymentId desc");
                if (id == "")
                {
                    formNo = 1;
                }
                else
                {
                    formNo = Convert.ToDecimal(id) + 1;
                }

                lookUps.receivingLedger = dt;
                lookUps.employees = dtblspAdvancePayment;
                lookUps.EmployeesList = new EmployeeSP().EmployeeViewAll();
                lookUps.cashOrBank = new TransactionsGeneralFill().BankOrCashComboFill(false);
                lookUps.InvoiceNo = formNo;

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetAdvancePaymentsForListing(DateTime month)
        {
            MatResponse response = new MatResponse();
            dynamic lookUps = new ExpandoObject();

            DBMatConnection db = new DBMatConnection();
            string query = string.Empty;
            try
            {
                query = string.Format("SELECT * FROM tbl_AdvancePayment_Pending WHERE salaryMonth='{0}'", month);

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = db.customSelect(query);
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
        [HttpGet]
        public MatResponse GetAdvancePaymentDetailsForListing(decimal advancePaymentId)
        {
            MatResponse response = new MatResponse();
            dynamic lookUps = new ExpandoObject();

            DBMatConnection db = new DBMatConnection();
            string query = string.Empty;
            try
            {
                query = string.Format("SELECT * FROM tbl_AdvancePayment_Pending WHERE advancePaymentId='{0}'", advancePaymentId);

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = db.customSelect(query);
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }

        [HttpPost]
        public MatResponse SaveAdvnacePayment(AdvancePaymentInfo input)
        {
            MatResponse response = new MatResponse();
            var voucherMessage = "";
            try
            {
                AdvancePaymentSP spAdvancepayment = new AdvancePaymentSP();
                AdvancePaymentInfo infoAdvancepayment = new AdvancePaymentInfo();
                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                MonthlySalarySP spMonthlySalary = new MonthlySalarySP();

                if (CheckAdvanceAmount(input.EmployeeId, input.Amount))
                {
                    if (!spMonthlySalary.CheckSalaryAlreadyPaidOrNotForAdvancePayment(input.EmployeeId, input.SalaryMonth))
                    {
                        if (!spAdvancepayment.CheckSalaryAlreadyPaidOrNot(input.EmployeeId, input.SalaryMonth))
                        {
                            infoAdvancepayment.VoucherNo = input.VoucherNo;
                            infoAdvancepayment.EmployeeId = input.EmployeeId;
                            infoAdvancepayment.SalaryMonth = input.SalaryMonth;
                            infoAdvancepayment.Chequenumber = input.Chequenumber;
                            infoAdvancepayment.Date = input.Date;
                            infoAdvancepayment.Amount = input.Amount;
                            infoAdvancepayment.InvoiceNo = input.InvoiceNo;
                            infoAdvancepayment.LedgerId = input.LedgerId;
                            infoAdvancepayment.ChequeDate = input.ChequeDate;
                            infoAdvancepayment.Narration = input.Narration;
                            infoAdvancepayment.ExtraDate = Convert.ToDateTime(DateTime.Now.ToString());
                            infoAdvancepayment.Extra1 = string.Empty;
                            infoAdvancepayment.Extra2 = string.Empty;
                            infoAdvancepayment.VoucherTypeId = decPaymentVoucherTypeId;
                            infoAdvancepayment.SuffixPrefixId = 1;
                            infoAdvancepayment.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                            infoAdvancepayment.ReceivingLedgerId = input.ReceivingLedgerId;

                            DataTable dtbl = new DataTable();
                            dtbl = spAdvancepayment.AdvancePaymentAddWithIdentity(infoAdvancepayment, true);
                            foreach (DataRow dr in dtbl.Rows)
                            {
                                decAdvancePaymentId = Convert.ToDecimal(dr.ItemArray[0].ToString());
                                strUpdatedVoucherNumber = dr.ItemArray[1].ToString();
                                strUpdatedInvoiceNumber = dr.ItemArray[2].ToString();
                            }
                            if (Convert.ToDecimal(strUpdatedVoucherNumber) != Convert.ToDecimal(input.VoucherNo))
                            {
                                input.VoucherNo = strUpdatedVoucherNumber;
                                voucherMessage = "Voucher number changed to  " + strUpdatedInvoiceNumber;
                            }
                            LedgerPosting(input, decAdvancePaymentId);
                            DBMatConnection db = new DBMatConnection();
                            string query = string.Empty;

                            query = string.Format("UPDATE tbl_AdvancePayment_Pending " +
                                            "SET status='{0}', invoiceNo='{1}', voucherNo='{1}' " +
                                            "WHERE salaryMonth='{2}' AND employeeId={3}", "Approved", input.VoucherNo, input.SalaryMonth, input.EmployeeId);
                            db.customUpdateQuery(query);
                        }
                        else
                        {
                            response.ResponseCode = 205;
                            response.ResponseMessage = " Advance already paid for this month";
                            return response;
                        }
                    }
                    else
                    {
                        response.ResponseCode = 205;
                        response.ResponseMessage = "Cant pay advance for this month,Salary already paid";
                        return response;
                    }
                }
                else
                {
                    response.ResponseCode = 205;
                    response.ResponseMessage = "Advance of this month exceeds the amount set  for  the employee";
                    return response;
                }
                response.ResponseCode = 200;
                response.ResponseMessage = "Saved Successfully, " + voucherMessage;


            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
            }
            return response;
        }

        [HttpPost]
        public MatResponse SaveAdvancePaymentPending(AdvancePaymentInfo input)
        {
            MatResponse response = new MatResponse();
            try
            {
                DBMatConnection db = new DBMatConnection();
                string query = string.Empty;

                query = string.Format("INSERT INTO tbl_AdvancePayment_Pending VALUES({0},{1},'{2}','{3}','{4}',{5},'{6}'," + 
                                        "'{7}','{8}','{9}','{10}','{11}','{12}',{13},{14},{15},{16},'{17}')"
                                        ,input.EmployeeId,
                                        input.LedgerId,
                                        input.VoucherNo,
                                        input.InvoiceNo,
                                        input.Date,
                                        input.Amount,
                                        input.SalaryMonth,
                                        input.Chequenumber,
                                        input.ChequeDate,
                                        input.Narration,
                                        DateTime.Now,
                                        "",
                                        "",
                                        1,
                                        decPaymentVoucherTypeId,
                                        MATFinancials.PublicVariables._decCurrentFinancialYearId,
                                        input.ReceivingLedgerId,
                                        "Pending Approval");
                if(db.ExecuteNonQuery2(query))
                {
                    response.ResponseCode = 200;
                    response.ResponseMessage = "success";
                }
            }
            catch(Exception e)
            {

            }
            return response;
        }

        [HttpGet]
        public MatResponse DeleteAdvancePayment(decimal employeeId, decimal apId, DateTime month, string voucherNo)
        {
            MatResponse response = new MatResponse();
            try
            {
                MonthlySalarySP spMonthlySalary = new MonthlySalarySP();
                if (!spMonthlySalary.CheckSalaryStatusForAdvancePayment(employeeId, month))
                {
                    AdvancePaymentInfo infoAdvancepayment = new AdvancePaymentInfo();
                    AdvancePaymentSP spAdvancePayment = new AdvancePaymentSP();
                    LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                    spAdvancePayment.AdvancePaymentDelete(apId);
                    spLedgerPosting.LedgerPostDelete(voucherNo, decPaymentVoucherTypeId);

                    response.ResponseCode = 200;
                    response.ResponseMessage = "sucess";
                }
                else
                {
                    response.ResponseCode = 203;
                    response.ResponseMessage = "Salary has been processed already";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
            }
            return response;
        }

        public MatResponse getAdvancePaymentsForRegister(string month)
        {
            MatResponse response = new MatResponse();
            try
            {
                DataTable dtblAdvancePayment = new DataTable();
                DataTable dtblAdvancePaymentPending = new DataTable();
                AdvancePaymentSP spAdvanceRegister = new AdvancePaymentSP();
                List<AdvancePaymentRegister> registerResult = new List<AdvancePaymentRegister>();

                DBMatConnection db = new DBMatConnection();
                string query = string.Empty;
                string query2 = string.Empty;
                query = string.Format("SELECT * FROM tbl_AdvancePayment WHERE salaryMonth='{0}'", month);
                query2 = string.Format("SELECT * FROM tbl_AdvancePayment_Pending WHERE salaryMonth='{0}' AND status='Pending Approval'", month);

                dtblAdvancePayment = db.customSelect(query);
                dtblAdvancePaymentPending = db.customSelect(query2);

                for(var i = 0; i < dtblAdvancePayment.Rows.Count; i++)
                {
                    registerResult.Add(new AdvancePaymentRegister {
                        AdvancePaymentId = Convert.ToDecimal(dtblAdvancePayment.Rows[i]["advancePaymentId"]),
                        Amount = Convert.ToDecimal(dtblAdvancePayment.Rows[i]["amount"]),
                        ChequeDate = Convert.ToDateTime(dtblAdvancePayment.Rows[i]["chequeDate"]),
                        Chequenumber = dtblAdvancePayment.Rows[i]["chequenumber"].ToString(),
                        Date = Convert.ToDateTime(dtblAdvancePayment.Rows[i]["date"]),
                        EmployeeId = Convert.ToDecimal(dtblAdvancePayment.Rows[i]["employeeId"]),
                        Extra1 = dtblAdvancePayment.Rows[i]["extra1"].ToString(),
                        Extra2 = dtblAdvancePayment.Rows[i]["extra2"].ToString(),
                        ExtraDate = Convert.ToDateTime(dtblAdvancePayment.Rows[i]["extraDate"]),
                        FinancialYearId = Convert.ToDecimal(dtblAdvancePayment.Rows[i]["financialYearId"]),
                        InvoiceNo = dtblAdvancePayment.Rows[i]["invoiceNo"].ToString(),
                        LedgerId = Convert.ToDecimal(dtblAdvancePayment.Rows[i]["ledgerId"]),
                        Narration = dtblAdvancePayment.Rows[i]["narration"].ToString(),
                        ReceivingLedgerId = Convert.ToDecimal(dtblAdvancePayment.Rows[i]["ReceivingLedgerId"]),
                        SalaryMonth = Convert.ToDateTime(dtblAdvancePayment.Rows[i]["salaryMonth"]),
                        Status = "Approved",
                        SuffixPrefixId = Convert.ToDecimal(dtblAdvancePayment.Rows[i]["suffixPrefixId"]),
                        VoucherNo = dtblAdvancePayment.Rows[i]["voucherNo"].ToString(),
                        VoucherTypeId = Convert.ToDecimal(dtblAdvancePayment.Rows[i]["voucherTypeId"])
                    });
                }
                for (var i = 0; i < dtblAdvancePaymentPending.Rows.Count; i++)
                {
                    registerResult.Add(new AdvancePaymentRegister
                    {
                        AdvancePaymentId = Convert.ToDecimal(dtblAdvancePaymentPending.Rows[i]["advancePaymentId"]),
                        Amount = Convert.ToDecimal(dtblAdvancePaymentPending.Rows[i]["amount"]),
                        ChequeDate = Convert.ToDateTime(dtblAdvancePaymentPending.Rows[i]["chequeDate"]),
                        Chequenumber = dtblAdvancePaymentPending.Rows[i]["chequenumber"].ToString(),
                        Date = Convert.ToDateTime(dtblAdvancePaymentPending.Rows[i]["date"]),
                        EmployeeId = Convert.ToDecimal(dtblAdvancePaymentPending.Rows[i]["employeeId"]),
                        Extra1 = dtblAdvancePaymentPending.Rows[i]["extra1"].ToString(),
                        Extra2 = dtblAdvancePaymentPending.Rows[i]["extra2"].ToString(),
                        ExtraDate = Convert.ToDateTime(dtblAdvancePaymentPending.Rows[i]["extraDate"]),
                        FinancialYearId = Convert.ToDecimal(dtblAdvancePaymentPending.Rows[i]["financialYearId"]),
                        InvoiceNo = dtblAdvancePaymentPending.Rows[i]["invoiceNo"].ToString(),
                        LedgerId = Convert.ToDecimal(dtblAdvancePaymentPending.Rows[i]["ledgerId"]),
                        Narration = dtblAdvancePaymentPending.Rows[i]["narration"].ToString(),
                        ReceivingLedgerId = Convert.ToDecimal(dtblAdvancePaymentPending.Rows[i]["ReceivingLedgerId"]),
                        SalaryMonth = Convert.ToDateTime(dtblAdvancePaymentPending.Rows[i]["salaryMonth"]),
                        Status = "Pending Approval",
                        SuffixPrefixId = Convert.ToDecimal(dtblAdvancePaymentPending.Rows[i]["suffixPrefixId"]),
                        VoucherNo = dtblAdvancePaymentPending.Rows[i]["voucherNo"].ToString(),
                        VoucherTypeId = Convert.ToDecimal(dtblAdvancePaymentPending.Rows[i]["voucherTypeId"])
                    });
                }

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = registerResult.OrderByDescending(p => p.Date);
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
            }

            return response;
        }

        public MatResponse getAdvancePaymentsDetailsForRegister(decimal advancePaymentId)
        {
            MatResponse response = new MatResponse();
            try
            {
                AdvancePaymentSP spAdvanceRegister = new AdvancePaymentSP();
               
                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = spAdvanceRegister.AdvancePaymentView(advancePaymentId); ;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
            }

            return response;
        }

        public bool CheckAdvanceAmount(decimal employeeId, decimal amount)
        {
            bool cancel = true;

            MatResponse response = new MatResponse();
            try
            {
                AdvancePaymentSP spAdvancePayment = new AdvancePaymentSP();
                decimal decEmployeesalary = 0;

                decEmployeesalary = spAdvancePayment.AdvancePaymentAmountchecking(employeeId);

                if (amount > decEmployeesalary)
                {
                    cancel = false;
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
                response.Response = true;
            }
            return cancel;
        }
        public void LedgerPosting(AdvancePaymentInfo  input, decimal decAdvancePaymentId)
        {
            try
            {
                AdvancePaymentSP spAdvancePayment = new AdvancePaymentSP();
                AdvancePaymentInfo infoAdvancePayment = new AdvancePaymentInfo();
                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();

                infoLedgerPosting.VoucherTypeId = decPaymentVoucherTypeId;
                infoLedgerPosting.VoucherNo = input.VoucherNo;
                infoLedgerPosting.Date = input.Date;
                infoLedgerPosting.LedgerId = input.LedgerId;
                infoLedgerPosting.DetailsId = decAdvancePaymentId;
                infoLedgerPosting.InvoiceNo = input.InvoiceNo;
                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.Debit = 0;
                infoLedgerPosting.Credit = input.Amount;

                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = DateTime.Now;

                infoLedgerPosting.ExtraDate = DateTime.Now;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                infoLedgerPosting.VoucherTypeId = decPaymentVoucherTypeId;
                infoLedgerPosting.VoucherNo = input.VoucherNo;
                infoLedgerPosting.Date = input.Date;
                infoLedgerPosting.LedgerId = 3;
                infoLedgerPosting.DetailsId = decAdvancePaymentId;
                infoLedgerPosting.InvoiceNo = input.InvoiceNo;
                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.Debit = input.Amount;
                infoLedgerPosting.Credit = 0;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = DateTime.Now;

                infoLedgerPosting.ExtraDate = DateTime.Now;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);


            }
            catch (Exception ex)
            {
                
            }
        }
        public string VoucherNoGeneration()
        {
            try
            {
                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                AdvancePaymentSP spAdvancePayment = new AdvancePaymentSP();
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPaymentVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strAdvancePayment);
                if (Convert.ToDecimal(strVoucherNo) != spAdvancePayment.AdvancePaymentGetMaxPlusOne(decPaymentVoucherTypeId))
                {
                    strVoucherNo = spAdvancePayment.AdvancePaymentGetMax(decPaymentVoucherTypeId).ToString();
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPaymentVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strAdvancePayment);
                    if (spAdvancePayment.AdvancePaymentGetMax(decPaymentVoucherTypeId) == "0")
                    {
                        strVoucherNo = "0";
                        strVoucherNo = obj.VoucherNumberAutomaicGeneration(decPaymentVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strAdvancePayment);
                    }
                }
                if (isAutomatic)
                {
                    SuffixPrefixSP spSuffixPrefix = new SuffixPrefixSP();
                    SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                    infoSuffixPrefix = spSuffixPrefix.GetSuffixPrefixDetails(decPaymentVoucherTypeId, DateTime.Now);
                    strPrefix = infoSuffixPrefix.Prefix;
                    strSuffix = infoSuffixPrefix.Suffix;
                    strInvoiceNo = strPrefix + strVoucherNo + strSuffix;
                }
            }
            catch (Exception ex)
            {
            }
            return strVoucherNo;
        }
    }
    public class AdvancePaymentRegister
    {
        public decimal AdvancePaymentId { get; set; }
        public decimal EmployeeId { get; set; }

        public decimal LedgerId { get; set; }
        public string VoucherNo { get; set; }
        public string InvoiceNo { get; set; }
        public DateTime Date { get; set; }
        public decimal Amount { get; set; }
        public DateTime SalaryMonth { get; set; }
        public string Chequenumber { get; set; }
        public DateTime ChequeDate { get; set; }
        public string Narration { get; set; }
         public DateTime ExtraDate { get; set; }
        public string Extra1 { get; set; }
        public string Extra2 { get; set; }
        public decimal SuffixPrefixId { get; set; }
        public decimal VoucherTypeId { get; set; }
        public decimal FinancialYearId { get; set; }
        public decimal ReceivingLedgerId { get; set; }
        public string Status { get; set; }
    }
}
