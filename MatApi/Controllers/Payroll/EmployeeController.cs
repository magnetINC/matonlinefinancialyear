﻿using MatApi.DBModel;
using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins:"*", headers: "*", methods:"*")]
    public class EmployeeController : ApiController
    {
        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public EmployeeController()
        {

        }

        [HttpGet]
        public MatResponse GetLookUps()
        {
            MatResponse response = new MatResponse();
            dynamic lookUps = new ExpandoObject();

            lookUps.salaryPackages = new SalaryPackageSP().SalaryPackageViewAll();
            lookUps.designation = new DesignationSP().DesignationViewAll();
            lookUps.employeesList = new EmployeeSP().EmployeeViewAll();

            response.ResponseCode = 200;
            response.ResponseMessage = "success";
            response.Response = lookUps;

            return response;
        }

        [HttpPost]
        public MatResponse SaveEmployee(EmployeeInfo input)
        {
            MatResponse response = new MatResponse();
            EmployeeSP spEmployee = new EmployeeSP();
            try
            {
                if (spEmployee.EmployeeCodeCheckExistance(input.EmployeeCode, 0) == false)
                {
                    decimal decEmployeeId = spEmployee.EmployeeAddWithReturnIdentity(input);
                    if (decEmployeeId > 0)
                    {
                        response.ResponseCode = 200;
                        response.ResponseMessage = "success";
                    }
                }
                else
                {
                    response.ResponseCode = 403;
                    response.ResponseMessage = "Employee Code exists";
                }
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetListForRegister()
        {
            MatResponse response = new MatResponse();
            try
            {
                EmployeeInfo infoEmployee = new EmployeeInfo();
                EmployeeSP spEmployee = new EmployeeSP();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = spEmployee.EmployeeViewAll();
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
            }
            return response;
        }

        [HttpPost]
        public MatResponse UpdateEmployee(EmployeeInfo input)
        {
            MatResponse response = new MatResponse();
            EmployeeSP spEmployee = new EmployeeSP();
            try
            {
                tbl_Employee Employee = context.tbl_Employee.SingleOrDefault(p => p.employeeId == input.EmployeeId);
                decimal employeeUserId = Convert.ToDecimal(Employee.extra1);
                tbl_User SalesManUser = context.tbl_User.SingleOrDefault(p => p.userId == employeeUserId);
                if (SalesManUser != null)
                    UpdateSalesMan(input, SalesManUser);

                if (spEmployee.EmployeeEdit(input))
                {
                    response.ResponseCode = 200;
                    response.ResponseMessage = "success";
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }
            return response;
        }
        public void UpdateSalesMan(EmployeeInfo input, tbl_User SalesManUser)
        {
            SalesManUser.userName = input.EmployeeCode;
            SalesManUser.firstName = input.EmployeeName.Split(new char[] { ' ' })[0];
            SalesManUser.lastName = input.EmployeeName.Split(new char[] { ' ' }).Length > 1? input.EmployeeName.Split(new char[] { ' ' })[1]:"";
            SalesManUser.active = input.IsActive;
            SalesManUser.narration = input.Narration;
            context.SaveChanges();
        }

        [HttpGet]
        public MatResponse ViewEmployeeDetails(decimal employeeId)
        {
            MatResponse response = new MatResponse();
            try
            {
                EmployeeSP spEmployee = new EmployeeSP();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = spEmployee.EmployeeView(employeeId);
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }

        [HttpGet]
        public MatResponse DeleteEmployee(decimal employeeId)
        {
            MatResponse response = new MatResponse();
            try
            {
                EmployeeSP spEmployee = new EmployeeSP();
                if (spEmployee.EmployeeCheckReferences(employeeId) == -1)
                {
                    response.ResponseCode = 203;
                    response.ResponseMessage = "Reference exists for this employee";
                }
                else
                {
                    response.ResponseCode = 200;
                    response.ResponseMessage = "success";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
    }
}
