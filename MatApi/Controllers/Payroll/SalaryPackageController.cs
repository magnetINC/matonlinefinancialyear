﻿using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Data;
using MatApi.DBModel;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SalaryPackageController : ApiController
    {
        SalaryPackageSP salaryPackageSp;
        PayHeadSP payHeadSp;
        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public SalaryPackageController()
        {
            salaryPackageSp = new SalaryPackageSP();
            payHeadSp = new PayHeadSP();
        }

        [HttpGet]
        public MatResponse GetLookUps()
        {
            dynamic lookUps = new ExpandoObject();
            MatResponse response = new MatResponse();

            try
            {
                lookUps.PayElements = new PayHeadSP().PayHeadViewAll();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }

            return response;
        }

        [HttpGet]
        public MatResponse GetSalaryPackage()
        {
            MatResponse response = new MatResponse();
            DataTable dtbl = new DataTable();
            try
            {
                dtbl = new SalaryPackageSP().SalaryPackageViewAll();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = dtbl;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }

            return response;
        }

        [HttpGet]
        public MatResponse GetSalaryPackage(decimal salaryPackageId)
        {
            MatResponse response = new MatResponse();
            dynamic thisSalaryPackage = new ExpandoObject();
            try
            {
                thisSalaryPackage.master = new SalaryPackageSP().SalaryPackageView(salaryPackageId);
                thisSalaryPackage.details = new SalaryPackageDetailsSP().SalaryPackageDetailsViewWithSalaryPackageId(salaryPackageId);

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = thisSalaryPackage;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }

            return response;
        }

        [HttpGet]
        public MatResponse DeleteSalaryPackage(decimal salaryPackageId)
        {
            MatResponse response = new MatResponse();
            try
            {
                if (new SalaryPackageSP().SalaryPackageDeleteAll(salaryPackageId))
                {
                    response.ResponseCode = 200;
                    response.ResponseMessage = "success";
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }

            return response;
        }

        [HttpPost]
        public MatResponse AddSalaryPackage(SalaryPackageVM input)
        {

            MatResponse response = new MatResponse();
            decimal decSalaryPackageId = 0;
            SalaryPackageSP spSalaryPackage = new SalaryPackageSP();
            SalaryPackageDetailsSP spSalarypackageDetails = new SalaryPackageDetailsSP();

            try
            {
                if (!spSalaryPackage.SalaryPackageNameCheckExistance(input.master.SalaryPackageName))
                {
                    decSalaryPackageId = spSalaryPackage.SalaryPackageAdd(input.master);
                    if (decSalaryPackageId > 0)
                    {
                        foreach (var sp in input.details)
                        {
                            sp.SalaryPackageId = decSalaryPackageId;
                            spSalarypackageDetails.SalaryPackageDetailsAdd(sp);
                        }
                        response.ResponseCode = 200;
                        response.ResponseMessage = "Success";
                    }
                    else
                    {
                        response.ResponseCode = 500;
                        response.ResponseMessage = "Error Saving";
                    }
                }
                else
                {
                    response.ResponseCode = 403;
                    response.ResponseMessage = "Salary package Already Exists.";
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }
            return response;
        }

        [HttpPost]
        public MatResponse UpdateSalaryPackage(SalaryPackageVM input)
        {
            MatResponse response = new MatResponse();
            decimal decSalaryPackageId = 0;
            SalaryPackageSP spSalaryPackage = new SalaryPackageSP();
            SalaryPackageDetailsSP spSalarypackageDetails = new SalaryPackageDetailsSP();

            try
            {
                var master = context.tbl_SalaryPackage.Find(input.master.SalaryPackageId);
                if (master != null)
                {
                    spSalaryPackage.SalaryPackageEdit(input.master);
                    if (new SalaryPackageDetailsSP().SalaryPackageDetailsDeleteWithSalaryPackageId(input.master.SalaryPackageId) > 0)
                        {
                            foreach (var sp in input.details)
                            {
                                sp.SalaryPackageId = input.master.SalaryPackageId;
                                spSalarypackageDetails.SalaryPackageDetailsAdd(sp);
                            }
                           
                        }
                        else
                        {
                        foreach (var sp in input.details)
                        {
                            sp.SalaryPackageId = input.master.SalaryPackageId;
                            spSalarypackageDetails.SalaryPackageDetailsAdd(sp);
                        }
                    }
                    response.ResponseCode = 200;
                    response.ResponseMessage = "Success";
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }
            return response;

            
        }

        [HttpGet]
        public MatResponse EditPayElement(SalaryPackageVM input)
        {
            MatResponse response = new MatResponse();
            try
            {
                if (new SalaryPackageSP().SalaryPackageEdit(input.master) > 0)
                {
                    if (new SalaryPackageDetailsSP().SalaryPackageDetailsDeleteWithSalaryPackageId(input.master.SalaryPackageId) > 0)
                    {
                        foreach (var sp in input.details)
                        {
                            if (new SalaryPackageDetailsSP().SalaryPackageDetailsAdd(sp))
                            {
                                response.ResponseCode = 200;
                                response.ResponseMessage = "success";
                            }
                        }
                    }
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }

            return response;

        }

        [HttpGet]
        public MatResponse SalaryPackageRegister(string packageName, string status)
        {
            MatResponse response = new MatResponse();
            try
            {
                SalaryPackageSP spSalaryPackage = new SalaryPackageSP();
                DataTable dtblSalaryPackage = new DataTable();

                dtblSalaryPackage = spSalaryPackage.SalaryPackageregisterSearch(packageName, status);

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = dtblSalaryPackage;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Failed";
                response.Response = e;
            }
            return response;
        }
    }

    public class SalaryPackageVM
    {
        public SalaryPackageInfo master { get; set; }
        public List<SalaryPackageDetailsInfo> details { get; set; }
    }
}
