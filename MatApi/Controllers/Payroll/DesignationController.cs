﻿using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class DesignationController : ApiController
    {
        DesignationSP designationSp;
        public DesignationController()
        {
            designationSp = new DesignationSP();
        }

        public HttpResponseMessage GetDesignations()
        {
            var designationsDt = designationSp.DesignationViewAll();
            return Request.CreateResponse(HttpStatusCode.OK, designationsDt);
        }

        public HttpResponseMessage GetDesignation(decimal designationId)
        {
            var designationDt = designationSp.DesignationView(designationId);
            return Request.CreateResponse(HttpStatusCode.OK, designationDt);
        }

        [HttpPost]
        public MatResponse DeleteDesignation(DesignationInfo designationInfo)
        {
            MatResponse response = new MatResponse();
            if (designationSp.DesignationDelete(designationInfo.DesignationId) == true)
            {
                response.Response = true;
                response.ResponseCode = 200;
                response.ResponseMessage = "success";
            }
            else
            {
                response.Response = false;
                response.ResponseCode = 200;
                response.ResponseMessage = "success";
            }
            return response;
        }

        [HttpPost]
        public bool AddDesignation(DesignationInfo designationInfo)
        {
            designationInfo.ExtraDate = DateTime.Now;
            designationInfo.Extra1 = "";
            designationInfo.Extra2 = "";
            designationInfo.Narration = "";
            if (designationSp.DesignationAddWithReturnIdentity(designationInfo) > 0)
            {
                return true;
            }
            return false;
        }
        [HttpPost]
        public bool EditDesignation(DesignationInfo designationInfo)
        {
            designationInfo.ExtraDate = DateTime.Now;
            designationInfo.Extra1 = "";
            designationInfo.Extra2 = "";
            designationInfo.Narration = "";
            return designationSp.DesignationEdit(designationInfo);
        }
    }
}
