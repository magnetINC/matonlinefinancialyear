﻿using MatApi.Models;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using System.Dynamic;
using MATFinancials.DAL;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins:"*", headers:"*",methods:"*")]
    public class MonthlySalarySettingsController : ApiController
    {
        public MonthlySalarySettingsController()
        {

        }

        [HttpGet]
        public MatResponse GetLookUps()
        {
            MatResponse response = new MatResponse();
            dynamic lookUps = new ExpandoObject();
            
            try
            {
                lookUps.SalaryPackages = new SalaryPackageSP().SalaryPackageViewAllForMonthlySalarySettings();
                lookUps.Employees = new EmployeeSP().EmployeeViewAll();

                response.ResponseCode = 200;
                response.ResponseMessage = "Success";
                response.Response = lookUps;
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetMaster()
        {
            MatResponse response = new MatResponse();

            try
            {
                response.ResponseCode = 200;
                response.ResponseMessage = "Success";
                response.Response = new MonthlySalarySP().MonthlySalaryViewAll();
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }
            return response;
        }

        [HttpGet]
        public MatResponse GetDetails(decimal monthlySalaryId)
        {
            MatResponse response = new MatResponse();
            DBMatConnection conn = new DBMatConnection();
            try
            {
                string getQuery = string.Format("SELECT * FROM tbl_MonthlySalaryDetails where monthlySalaryId = {0}", monthlySalaryId);

                response.ResponseCode = 200;
                response.ResponseMessage = "Success";
                response.Response = conn.customSelect(getQuery);
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }
            return response;
        }

        [HttpPost]
        public MatResponse SaveMonthlySalary(MonthlySalaryVM input)
        {
            MatResponse response = new MatResponse();
            dynamic lookUps = new ExpandoObject();

            try
            {
                decimal  decMasterIdForEdit = new MonthlySalarySP().MonthlySalaryAddWithIdentity(input.master);
                if( decMasterIdForEdit > 0)
                {
                    foreach(var details in input.details)
                    {
                        details.MonthlySalaryId = decMasterIdForEdit;
                        new MonthlySalaryDetailsSP().MonthlySalaryDetailsAddWithMonthlySalaryId(details);
                        new EmployeeSP().EmployeePackageEdit(details.EmployeeId, details.SalaryPackageId);
                    }
                    response.ResponseCode = 200;
                    response.ResponseMessage = "Success";
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }
            return response;
        }

        [HttpPost]
        public MatResponse EditMonthlySalary(MonthlySalaryDetailsInfo input)
        {
            MatResponse response = new MatResponse();

            try
            {
                new EmployeeSP().EmployeePackageEdit(input.EmployeeId, input.SalaryPackageId);
                new MonthlySalaryDetailsSP().MonthlySalaryDetailsEditUsingMasterIdAndDetailsId(input);

                response.ResponseCode = 200;
                response.ResponseMessage = "Success";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }
            return response;
        }

        [HttpGet]
        public MatResponse DeleteMonthlySalary(decimal monthlySalaryId)
        {
            MatResponse response = new MatResponse();

            try
            {
                new MonthlySalarySP().MonthlySalaryDeleteAll(monthlySalaryId);

                response.ResponseCode = 200;
                response.ResponseMessage = "Success";
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }
            return response;
      
        }
    }

    public class MonthlySalaryVM
    {
        public MonthlySalaryInfo master { get; set; }
        public List<MonthlySalaryDetailsInfo> details { get; set; }
    }
}
