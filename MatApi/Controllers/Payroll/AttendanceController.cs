﻿using MatApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MATFinancials;
using System.Data;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class AttendanceController : ApiController
    {
        public AttendanceController()
        {
        }

        [HttpGet]
        public MatResponse GetAttendance(DateTime date)
        {
            MatResponse response = new MatResponse();

            try
            {
                DailyAttendanceDetailsInfo infoDailyAttendanceDetails = new DailyAttendanceDetailsInfo();
                DailyAttendanceDetailsSP spDailyAttendanceDetails = new DailyAttendanceDetailsSP();
                DailyAttendanceMasterInfo infoDailyAttendanceMaster = new DailyAttendanceMasterInfo();
                DailyAttendanceMasterSP spDailyAttendanceMaster = new DailyAttendanceMasterSP();

                if (spDailyAttendanceMaster.DailyAttendanceMasterMasterIdSearch(date.ToString()))
                {
                    DataTable dtblAttendance = new DataTable();
                    dtblAttendance = spDailyAttendanceDetails.DailyAttendanceDetailsSearchGridFill(date.ToString());

                    response.ResponseCode = 200;
                    response.ResponseMessage = "For already Taken attendance";
                    response.Response = dtblAttendance;
                }
                else
                {
                    DataTable dtblAttendance = new DataTable();
                    dtblAttendance = spDailyAttendanceDetails.DailyAttendanceDetailsSearchGridFill(date.ToString());

                    response.ResponseCode = 200;
                    response.ResponseMessage = "For new day";
                    response.Response = dtblAttendance;
                }

                
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }
            return response;
        }

        [HttpPost]
        public MatResponse SaveAttendance(AttendanceVM input)
        {
            MatResponse response = new MatResponse();

            try
            {
                decimal ID = new DailyAttendanceMasterSP().DailyAttendanceAddToMaster(input.master);
                if (ID > 0)
                {
                    foreach (var details in input.details)
                    {
                        details.DailyAttendanceMasterId = ID;
                        new DailyAttendanceDetailsSP().DailyAttendanceDetailsAddUsingMasterId(details);
                    }
                    response.ResponseCode = 200;
                    response.ResponseMessage = "Success";
                }
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "something went wrong";
                response.Response = e;
            }
            return response;
        }
    }

    public class AttendanceVM
    {
        public DailyAttendanceMasterInfo master { get; set; }
        public List<DailyAttendanceDetailsInfo> details { get; set; }
    }
}
