﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using MATFinancials;
using System.Data;
using System.Dynamic;
using MatApi.Models;
using MATFinancials.Classes.HelperClasses;
using MatApi.DBModel;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins:"*", headers:"*", methods:"*")]
    public class MonthlySalaryController : ApiController
    {
        decimal decMonthlyVoucherTypeId = 27;
        decimal decPaymentVoucherTypeId = 10028;
        decimal decMonthlySuffixPrefixId = 0;
        decimal decMasterId = 0;
        decimal decIsEditModeMasterId = 0;

        string strUpdatedVoucherNo = string.Empty;
        string strUpdatedInvoiceNo = string.Empty;
        string tableName = "SalaryVoucherMaster";
        string strVoucherNo = string.Empty;
        string strInvoiceNo = string.Empty;
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        string strEployeeNames = string.Empty;
        string strledgerId;
        string strVoucherNoforEdit = "0";

        int q = 0;
        int inNarrationCount = 0;

        bool isAutomatic = false;
        bool @isEditMode = false;
        bool isReverseSalary = false;
        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public MonthlySalaryController()
        {
            
        }

        [HttpGet]
        public MatResponse GetLookUps()
        {
            MatResponse response = new MatResponse();
            dynamic lookUps = new ExpandoObject();
            

            try
            {
                //lookUps.filteredLedgers = context.tbl_AccountLedger.Where(a => a.accountGroupId == 27 || a.accountGroupId == 28 || a.accountGroupId == 3).ToList();
                lookUps.cashOrBank = new TransactionsGeneralFill().BankOrCashComboFill(false);
                lookUps.invoiceNo = voucherNumberGeneration();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = lookUps;
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
                response.Response = e;
            }

            return response;
        }

        [HttpGet]
        public MatResponse GetMonthlySalaryForProcessing(DateTime date)
        {
            MatResponse response = new MatResponse();
            string strMonth = date.ToString("MMMMyyyy");
            string Month = strMonth.Substring(0, 3);
            string strmonthYear = Convert.ToDateTime(strMonth.ToString()).Year.ToString();
            string monthYear = Month + " " + strmonthYear;
            try
            {
                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = new SalaryVoucherDetailsSP().MonthlySalaryVoucherDetailsViewAll(strMonth, Month, monthYear, false, 0.ToString());
                 
            }
            catch (Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
                response.Response = e;
            }

            return response;
        }

        [HttpPost]
        public MatResponse SaveSalary(MonthlySalaryProcessingVM input)
            {
            MatResponse response = new MatResponse();
            try
            {
                SalaryVoucherMasterSP spMaster = new SalaryVoucherMasterSP();
                SalaryVoucherMasterInfo infoMaster = new SalaryVoucherMasterInfo();
                SalaryVoucherDetailsSP spDetails = new SalaryVoucherDetailsSP();
                SalaryVoucherDetailsInfo infoDetails = new SalaryVoucherDetailsInfo();
                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                PayHeadSP SalaryPayHeads = new PayHeadSP();
                GeneralQueries query = new GeneralQueries();

                var master = input.master;
                var details = input.details;

                //------------------------------- In the case of multi user check whether salary is paying for the sam person ----------------//
                int inCounts = details.Count();
                int incont = 0;
                decimal decVal = 0;
                for (int i = 0; i < inCounts; i++)
                {
                    decVal = details[i].EmployeeId;
                    if (spDetails.CheckWhetherSalaryAlreadyPaid(decVal, master.Month) != "0")
                    {
                        strEployeeNames = strEployeeNames + spDetails.CheckWhetherSalaryAlreadyPaid(decVal, master.Month) + ",";
                        foreach (char ch in strEployeeNames)
                        {
                            if (ch == ',')
                            {
                                incont++;
                            }
                        }
                        if (incont == 15)
                        {
                            incont = 0;
                            strEployeeNames = strEployeeNames + Environment.NewLine;
                        }

                    }
                }
                if (spDetails.CheckWhetherSalaryAlreadyPaid(decVal, master.Month) != "0")
                {
                    response.ResponseCode = 403;
                    response.ResponseMessage = "Salary already paid for -" + " " + strEployeeNames;
                }
                infoMaster.LedgerId = master.LedgerId;
                infoMaster.VoucherNo = master.VoucherNo;
                infoMaster.Month = master.Month;
                infoMaster.Date = master.Date;
                infoMaster.Narration = master.Narration;
                infoMaster.InvoiceNo = master.InvoiceNo;
                infoMaster.TotalAmount = master.TotalAmount;
                infoMaster.Extra1 = string.Empty; // Fields not in design//
                infoMaster.Extra2 = string.Empty; // Fields not in design//
                infoMaster.SuffixPrefixId = decMonthlySuffixPrefixId;
                infoMaster.VoucherTypeId = decMonthlyVoucherTypeId;
                infoMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;

                int inCount = details.Count();
                int inValue = 0;
                for (int i = 0; i < inCount; i++)
                {
                    if (details[i].Status == "Paid")
                    {
                        inValue++;
                    }
                }
                if (inValue > 0)
                {
                    //-------------------------In the case of Multi-User Check the VoucherNo. again (Max of VoucherNumber )---------------------//
                    DataTable dtbl = new DataTable();
                    dtbl = spMaster.MonthlySalaryVoucherMasterAddWithIdentity(infoMaster, isAutomatic);
                    foreach (DataRow dr in dtbl.Rows)
                    {
                        decMasterId = Convert.ToDecimal(dr.ItemArray[0].ToString());
                        strUpdatedVoucherNo = dr.ItemArray[1].ToString();
                        strUpdatedInvoiceNo = dr.ItemArray[2].ToString();
                    }
                    DataSet dsLedgerPostingElements = query.EmployeeReceivingLedgerDetails();
                    DataTable dtPayHeads = query.SalaryPackageWithPayHead();
                    DataTable dtLedgerPosting = SalaryHelper.LedgerPostingMirrorTable();
                    DataTable dtSalaryVoucherDetails = SalaryHelper.SalaryVoucherDetailsMirrorTable();
                    List<LedgerPostingInfo> ListofLedgerPosting = new List<LedgerPostingInfo>();
                    DataRow drLedgerPosting = dtLedgerPosting.NewRow();
                    decimal SalaryPackageId;

                    #region Post ledger for Bonus, Deduction, PAYE and Pension 
                    // these ledger posting only applies where there are values for the parameters listed above

                    for (int m = 0; m < details.Count(); m++)
                    {
                        #region post for bonus
                        //post ledger for bonus
                        if (details[m].Bonus != 0 || details[m].Bonus.ToString() != "")
                        {
                            Decimal BonusAmount = details[m].Bonus;
                            if (BonusAmount > 0)
                            {
                                //int ReceivingLedgerId = (from k in dsLedgerPostingElements.Tables[0].AsEnumerable()
                                //                         select k.Field<int>("ReceivingLedgerId")).FirstOrDefault();
                                decimal EmployeeId = details[m].EmployeeId;
                                int ReceivingLedgerId = context.tbl_BonusDeduction
                                    .Where(a => a.employeeId == EmployeeId && a.month == master.Month)
                                    .Select(b => b.ReceivingLedgerId).FirstOrDefault();

                                infoLedgerPosting = new LedgerPostingInfo();
                                infoLedgerPosting.Debit = 0;
                                infoLedgerPosting.Credit = BonusAmount;
                                infoLedgerPosting.VoucherTypeId = decMonthlyVoucherTypeId;
                                infoLedgerPosting.VoucherNo = master.VoucherNo;
                                infoLedgerPosting.Date = master.Date;
                                infoLedgerPosting.LedgerId = master.LedgerId;
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = master.InvoiceNo;
                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.ChequeDate = DateTime.Now;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                // do not credit the bank at the time, bulk credit of gross ammount per transaction will be done at th end of the posting 
                                // hence the comment below to skip the transaction from the list urefe 20160812
                                ListofLedgerPosting.Add(infoLedgerPosting);

                                infoLedgerPosting = new LedgerPostingInfo();
                                infoLedgerPosting.Debit = BonusAmount;
                                infoLedgerPosting.Credit = 0;
                                infoLedgerPosting.VoucherTypeId = decMonthlyVoucherTypeId;
                                infoLedgerPosting.VoucherNo = master.VoucherNo;
                                infoLedgerPosting.Date = DateTime.Now;
                                infoLedgerPosting.LedgerId = ReceivingLedgerId;
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = master.InvoiceNo;
                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.ChequeDate = DateTime.Now;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                ListofLedgerPosting.Add(infoLedgerPosting);
                            }
                        }
                        #endregion
                        #region post for salary advance
                        if (details[m].Advance != 0 || details[m].Advance.ToString() != "")
                        {
                            Decimal AdvanceAmount = details[m].Advance;
                            if(AdvanceAmount > 0)
                            {
                                infoLedgerPosting.VoucherTypeId = decPaymentVoucherTypeId;
                                infoLedgerPosting.VoucherNo = master.VoucherNo;
                                infoLedgerPosting.Date = master.Date;
                                infoLedgerPosting.LedgerId = details[m].AdvancePaymentLedgerId;
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = master.InvoiceNo;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.Debit = details[m].Advance;
                                infoLedgerPosting.Credit = 0;

                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.ChequeDate = DateTime.Now;

                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                ListofLedgerPosting.Add(infoLedgerPosting);
                            }
                        }
                        #endregion
                        #region post for deduction
                        if (details[m].Deduction != 0 || details[m].Deduction.ToString() != "")
                        {
                            Decimal DeductionAmount = details[m].Deduction;
                            if (DeductionAmount > 0)
                            {
                                //int ReceivingLedgerId = (from k in dsLedgerPostingElements.Tables[0].AsEnumerable()
                                //                         select k.Field<int>("ReceivingLedgerId")).FirstOrDefault();
                                decimal EmployeeId = details[m].EmployeeId;
                                int ReceivingLedgerId = context.tbl_BonusDeduction
                                    .Where(a => a.employeeId == EmployeeId && a.month == master.Month)
                                    .Select(b => b.ReceivingLedgerId).FirstOrDefault();

                                infoLedgerPosting = new LedgerPostingInfo();
                                infoLedgerPosting.Debit = DeductionAmount;
                                infoLedgerPosting.Credit = 0;
                                infoLedgerPosting.VoucherTypeId = decMonthlyVoucherTypeId;
                                infoLedgerPosting.VoucherNo = master.VoucherNo;
                                infoLedgerPosting.Date = master.Date;
                                infoLedgerPosting.LedgerId = master.LedgerId;
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = master.InvoiceNo;
                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.ChequeDate = DateTime.Now;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                // do not credit the bank at the time, bulk credit of gross ammount per transaction will be done at th end of the posting 
                                // hence the comment below to skip the transaction from the list urefe 20160812
                                ListofLedgerPosting.Add(infoLedgerPosting);

                                infoLedgerPosting = new LedgerPostingInfo();
                                infoLedgerPosting.Debit = 0;
                                infoLedgerPosting.Credit = DeductionAmount;
                                infoLedgerPosting.VoucherTypeId = decMonthlyVoucherTypeId;
                                infoLedgerPosting.VoucherNo = master.VoucherNo;
                                infoLedgerPosting.Date = MATFinancials.PublicVariables._dtCurrentDate;
                                infoLedgerPosting.LedgerId = ReceivingLedgerId;
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = master.InvoiceNo;
                                infoLedgerPosting.ChequeDate = DateTime.Now;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                ListofLedgerPosting.Add(infoLedgerPosting);
                            }
                        }
                        #endregion
                        #region post for PAYE
                        if (details[m].PAYE != 0 || details[m].PAYE.ToString() != "")
                        {
                            Decimal PAYEAmount = details[m].PAYE;
                            if (PAYEAmount > 0 && details[m].Status == "Paid" && details[m].SalaryVoucherMasterId == 0)
                            {
                                int ReceivingLedgerId = (from k in dsLedgerPostingElements.Tables[2].AsEnumerable()
                                                         select k.Field<int>("PAYEReceivingLedgerId")).FirstOrDefault();

                                infoLedgerPosting = new LedgerPostingInfo();
                                infoLedgerPosting.Debit = PAYEAmount;
                                infoLedgerPosting.Credit = 0;
                                infoLedgerPosting.VoucherTypeId = decMonthlyVoucherTypeId;
                                infoLedgerPosting.VoucherNo = master.VoucherNo;
                                infoLedgerPosting.Date = master.Date;
                                infoLedgerPosting.LedgerId = master.LedgerId;
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = master.InvoiceNo;
                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.ChequeDate = DateTime.Now;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                // do not credit the bank at the time, bulk credit of gross ammount per transaction will be done at th end of the posting 
                                // hence the comment below to skip the transaction from the list urefe 20160812
                                ListofLedgerPosting.Add(infoLedgerPosting);

                                infoLedgerPosting = new LedgerPostingInfo();
                                infoLedgerPosting.Debit = 0;
                                infoLedgerPosting.Credit = PAYEAmount;
                                infoLedgerPosting.VoucherTypeId = decMonthlyVoucherTypeId;
                                infoLedgerPosting.VoucherNo = master.VoucherNo;
                                infoLedgerPosting.Date = MATFinancials.PublicVariables._dtCurrentDate;
                                infoLedgerPosting.LedgerId = ReceivingLedgerId;
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = master.InvoiceNo;
                                infoLedgerPosting.ChequeDate = DateTime.Now;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                ListofLedgerPosting.Add(infoLedgerPosting);
                            }
                        }
                        #endregion
                        #region post for Pension
                        if (details[m].Pension != 0 || details[m].Pension.ToString() != "")
                        {
                            Decimal PensionAmount = details[m].Pension;
                            if (PensionAmount > 0 && details[m].Status == "Paid" && details[m].SalaryVoucherMasterId == 0)
                            {
                                int ReceivingLedgerId = (from k in dsLedgerPostingElements.Tables[2].AsEnumerable()
                                                         select k.Field<int>("PensionReceivingLedgerId")).FirstOrDefault();

                                infoLedgerPosting = new LedgerPostingInfo();
                                infoLedgerPosting.Debit = PensionAmount;
                                infoLedgerPosting.Credit = 0;
                                infoLedgerPosting.VoucherTypeId = decMonthlyVoucherTypeId;
                                infoLedgerPosting.VoucherNo = master.VoucherNo;
                                infoLedgerPosting.Date = master.Date;
                                infoLedgerPosting.LedgerId = master.LedgerId;
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = master.InvoiceNo;
                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.ChequeDate = DateTime.Now;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                // do not credit the bank at the time, bulk credit of gross ammount per transaction will be done at th end of the posting 
                                // hence the comment below to skip the transaction from the list urefe 20160812
                                ListofLedgerPosting.Add(infoLedgerPosting);

                                infoLedgerPosting = new LedgerPostingInfo();
                                infoLedgerPosting.Debit = 0;
                                infoLedgerPosting.Credit = PensionAmount;
                                infoLedgerPosting.VoucherTypeId = decMonthlyVoucherTypeId;
                                infoLedgerPosting.VoucherNo = master.VoucherNo;
                                infoLedgerPosting.Date = MATFinancials.PublicVariables._dtCurrentDate;
                                infoLedgerPosting.LedgerId = ReceivingLedgerId;
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = master.InvoiceNo;
                                infoLedgerPosting.ChequeDate = DateTime.Now;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                ListofLedgerPosting.Add(infoLedgerPosting);
                            }
                        }
                        #endregion
                    }
                    #endregion

                    // post ledger for salary pay elements (pay heads)
                    for (int j = 0; j < details.Count(); j++)
                    {
                        if (details[j].Status == "Paid" && details[j].SalaryVoucherMasterId == 0)
                        {
                            SalaryPackageId = Convert.ToDecimal(details[j].Extra1);
                            var PayElementsInPackage = (from k in dtPayHeads.AsEnumerable()
                                                        where k.Field<decimal>("salaryPackageId") == SalaryPackageId
                                                        select new { amount = k.Field<decimal>("amount"), ReceivingLedgerId = k.Field<int>("ReceivingLedgerId") }).ToList();
                            foreach (var PayElement in PayElementsInPackage)
                            {
                                infoLedgerPosting = new LedgerPostingInfo();
                                infoLedgerPosting.Debit = 0;
                                infoLedgerPosting.Credit = PayElement.amount;
                                infoLedgerPosting.VoucherTypeId = decMonthlyVoucherTypeId;
                                infoLedgerPosting.VoucherNo = master.VoucherNo;
                                infoLedgerPosting.Date = master.Date;
                                infoLedgerPosting.LedgerId = master.LedgerId;
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = master.InvoiceNo;
                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.ChequeDate = DateTime.Now;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                // spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                                // do not credit the bank at this time, bulk credit of gross ammount per transaction will be done at th end of the posting 
                                // hence the comment below to skip the transaction from the list urefe 20160812
                                ListofLedgerPosting.Add(infoLedgerPosting);

                                infoLedgerPosting = new LedgerPostingInfo();
                                infoLedgerPosting.Debit = PayElement.amount;
                                infoLedgerPosting.Credit = 0;
                                infoLedgerPosting.VoucherTypeId = decMonthlyVoucherTypeId;
                                infoLedgerPosting.VoucherNo = master.VoucherNo;
                                infoLedgerPosting.Date = MATFinancials.PublicVariables._dtCurrentDate;
                                // previous implementation change by Urefe to refelect the proper to post ot 20160808
                                // infoLedgerPosting.LedgerId = 4; //ledgerId of salarys
                                infoLedgerPosting.LedgerId = PayElement.ReceivingLedgerId;
                                infoLedgerPosting.DetailsId = 0;
                                infoLedgerPosting.InvoiceNo = master.InvoiceNo;
                                infoLedgerPosting.ChequeDate = DateTime.Now;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                                ListofLedgerPosting.Add(infoLedgerPosting);
                            }
                        }
                    }
                    // deduct the bulk payment per this transaction from bank but deduct the statutory deductions from the bulk payment first
                    if (master.TotalAmount > 0)
                    {
                        infoLedgerPosting = new LedgerPostingInfo();
                        infoLedgerPosting.Debit = 0;
                        infoLedgerPosting.Credit = master.TotalAmount;
                        infoLedgerPosting.VoucherTypeId = decMonthlyVoucherTypeId;
                        infoLedgerPosting.VoucherNo = master.VoucherNo;
                        infoLedgerPosting.Date = master.Date;
                        infoLedgerPosting.LedgerId = master.LedgerId;
                        infoLedgerPosting.DetailsId = 0;
                        infoLedgerPosting.InvoiceNo = master.InvoiceNo;
                        infoLedgerPosting.ChequeNo = string.Empty;
                        infoLedgerPosting.ChequeDate = DateTime.Now;
                        infoLedgerPosting.ExtraDate = DateTime.Now;
                        infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                        infoLedgerPosting.Extra1 = string.Empty;
                        infoLedgerPosting.Extra2 = string.Empty;
                        ListofLedgerPosting.Add(infoLedgerPosting);
                    }
                    if (ListofLedgerPosting.Any())
                    {
                        foreach (var ListItem in ListofLedgerPosting)
                        {
                            DataRow dr = dtLedgerPosting.NewRow();
                            dr[0] = ListItem.Date;
                            dr[1] = ListItem.VoucherTypeId;
                            dr[2] = ListItem.VoucherNo;
                            dr[3] = ListItem.LedgerId;
                            dr[4] = ListItem.Debit;
                            dr[5] = ListItem.Credit;
                            dr[6] = ListItem.DetailsId;
                            dr[7] = ListItem.YearId;
                            dr[8] = ListItem.InvoiceNo;
                            dr[9] = ListItem.ChequeNo;
                            dr[10] = ListItem.ChequeDate;
                            dr[11] = ListItem.ExtraDate;
                            dr[12] = ListItem.Extra1;
                            dr[13] = ListItem.Extra1;
                            dtLedgerPosting.Rows.Add(dr);
                        }

                    }

                    //LedgerPosting(Convert.ToDecimal(cmbCashOrBankAcc.SelectedValue.ToString()));
                    infoDetails.Extra1 = string.Empty;
                    infoDetails.Extra2 = string.Empty;
                    infoDetails.SalaryVoucherMasterId = decMasterId;

                    int inRowCount = details.Count();
                    for (int i = 0; i < inRowCount; i++)
                    {
                        DataRow dr = dtSalaryVoucherDetails.NewRow();

                        infoDetails.EmployeeId = details[i].EmployeeId;
                        infoDetails.Bonus = details[i].Bonus;
                        infoDetails.Deduction = details[i].Deduction;
                        infoDetails.Advance = details[i].Advance;
                        infoDetails.Lop = details[i].Lop;
                        infoDetails.PAYE = details[i].PAYE;
                        infoDetails.Pension = details[i].Pension;
                        infoDetails.Salary = details[i].Salary;
                        infoDetails.Status = details[i].Status;

                        if (details[i].Status == "Paid" && details[i].SalaryVoucherMasterId == 0)
                        {
                            dr[0] = infoDetails.SalaryVoucherMasterId;
                            dr[1] = infoDetails.EmployeeId;
                            dr[2] = infoDetails.Bonus;
                            dr[3] = infoDetails.Deduction;
                            dr[4] = infoDetails.Advance;
                            dr[5] = infoDetails.Lop;
                            dr[6] = infoDetails.PAYE;
                            dr[7] = infoDetails.Pension;
                            dr[8] = infoDetails.Salary;
                            dr[9] = infoDetails.Status;
                            dr[10] = DateTime.Now;
                            dr[11] = string.Empty;
                            dr[12] = null;
                            dtSalaryVoucherDetails.Rows.Add(dr);
                        }
                    }
                    query.BulkInsertSalaryPaymentDetails(dtSalaryVoucherDetails, dtLedgerPosting);

                    response.ResponseCode = 200;
                    response.ResponseMessage = "success";
                }
                else
                {
                    response.ResponseCode = 205;
                    response.ResponseMessage = "Cant Save without atleast one employee";
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "SOmething went wrong";
            }
            return response;
        }


        public string voucherNumberGeneration()
        {
            try
            {
                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                SalaryVoucherMasterSP spMaster = new SalaryVoucherMasterSP();
                //-----------------------------------------Voucher number Automatic generation ------------------------------------------------//
                if (strVoucherNo == string.Empty)
                {

                    strVoucherNo = "0";
                }
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decMonthlyVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                if (Convert.ToDecimal(strVoucherNo) != spMaster.SalaryVoucherMasterGetMaxPlusOne(decMonthlyVoucherTypeId))
                {
                    strVoucherNo = spMaster.SalaryVoucherMasterGetMax(decMonthlyVoucherTypeId).ToString();
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decMonthlyVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                    if (spMaster.SalaryVoucherMasterGetMax(decMonthlyVoucherTypeId) == "0")
                    {
                        strVoucherNo = "0";
                        strVoucherNo = obj.VoucherNumberAutomaicGeneration(decMonthlyVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                    }
                }
                if (isAutomatic)
                {
                    SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                    SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();

                    infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decMonthlyVoucherTypeId, DateTime.Now);
                    strPrefix = infoSuffixPrefix.Prefix;
                    strSuffix = infoSuffixPrefix.Suffix;
                    strInvoiceNo = strPrefix + strVoucherNo + strSuffix;
                }

            }
            catch (Exception ex)
            {
            }
            return strVoucherNo;
        }
    }

    public class MonthlySalaryProcessingVM
    {
        public SalaryVoucherMasterInfo master { get; set; }
        public List<SalaryVoucherDetailsInfo> details { get; set; }
    }
}
