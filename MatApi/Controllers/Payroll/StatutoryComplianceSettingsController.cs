﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Dynamic;
using MatApi.Models;
using MATFinancials;
using System.Data;
using MATFinancials.DAL;
using System.Web.Http.Cors;
using MatDal;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins:"*", headers:"*",methods:"*")]
    public class StatutoryComplianceSettingsController : ApiController
    {
        public StatutoryComplianceSettingsController()
        {

        }

        StatutoryComplianceSP SaveStatutoryComplianceSetup = new StatutoryComplianceSP();
        StatutoryComplianceInfo StatutoryComplianceInfo = new StatutoryComplianceInfo();
        SalaryPackageSP SalaryPackage = new SalaryPackageSP();
        SalaryPackageDetailsSP SalaryPackageDetails = new SalaryPackageDetailsSP();
        PayHeadSP PayHeads = new PayHeadSP();
        protected decimal Pension;

        DBMATAccounting_MagnetEntities context = new DBMATAccounting_MagnetEntities();

        [HttpGet]
        public MatResponse AccountGroupComboFill()
        {
            DataTable dt = new DataTable();
            MatResponse response = new MatResponse();
            try
            {
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                DataTable dtblAccountLedgers = new DataTable();
                dtblAccountLedgers = spAccountLedger.AccountLedgerViewAll();
                dt.Columns.Add("ledgerId");
                dt.Columns.Add("ledgerName");
                DataRow row = null;
                var IndirectExpenseAccountGroup = (from i in dtblAccountLedgers.AsEnumerable()
                                                   where i.Field<decimal>("accountGroupId") == 3
                                                   select new
                                                   {
                                                       accountGroupId = i.Field<decimal>("ledgerId"),
                                                       accountGroupName = i.Field<string>("ledgerName")
                                                   }).ToList();
                foreach (var rowObj in IndirectExpenseAccountGroup)
                {
                    row = dt.NewRow();
                    dt.Rows.Add(rowObj.accountGroupId, rowObj.accountGroupName);
                }

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = dt;

            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
        [HttpPost]
        public MatResponse SaveStatutoryComplianceSettings(StatutoryComplianceInfo input)
        {
            MatResponse response = new MatResponse();
            try
            {
                StatutoryComplianceSP spStatutoryCompliance = new StatutoryComplianceSP();
                StatutoryComplianceInfo infoStatutoryCompliance = new StatutoryComplianceInfo();
                SalaryPackageSP spSalaryPackage = new SalaryPackageSP();
                DataTable dtbl = new DataTable();
                var company = context.tbl_Company.FirstOrDefault();

                List<string> PensionPayElements = new List<string> { "Basic Salary", "Housing Allowance", "Transport Allowance" };
                if (company.mailingName.ToUpper().Contains("KASCO KANO"))
                {
                    PensionPayElements.Add("Meal");
                    PensionPayElements.Add("Utilities");
                }
                List<decimal> PayHeadIdsForPension = new List<decimal>();
                DataTable dtPayHeads = PayHeads.PayHeadViewAll();
                if (dtPayHeads != null && dtPayHeads.Rows.Count > 0)
                {
                    PayHeadIdsForPension = (from p in dtPayHeads.AsEnumerable()
                                            where PensionPayElements.Contains(p.Field<string>("payHeadName"))
                                            select p.Field<decimal>("payHeadId")
                                  ).ToList();
                }

                decimal spId = 0;
                dtbl = spSalaryPackage.SalaryPackageIdViewAll();
                foreach (DataRow dr in dtbl.Rows)
                {

                    spId = Convert.ToDecimal(dr["SalaryPackageId"]);
                    infoStatutoryCompliance.SalaryPackageID = Convert.ToInt32(spId);
                    infoStatutoryCompliance.IsPAYEApplicable = input.IsPAYEApplicable;
                    infoStatutoryCompliance.IsPensionApplicable = input.IsPensionApplicable;
                    infoStatutoryCompliance.PAYE = ComputePAYE(spId, PayHeadIdsForPension);
                    infoStatutoryCompliance.Pension = ComputePension(spId, PayHeadIdsForPension);
                    infoStatutoryCompliance.SalaryPackageID = Convert.ToInt32(spId);
                    infoStatutoryCompliance.PAYEReceivingLedgerId = input.PAYEReceivingLedgerId;
                    infoStatutoryCompliance.PensionReceivingLedgerId = input.PensionReceivingLedgerId;

                    DBMatConnection db = new DBMatConnection();
                    DataSet dss = new DataSet();
                    db.AddParameter("@SalaryPackageId", spId);
                    dss = db.getDataSet("GetStatutoryComplianceID");
                    if (dss != null && dss.Tables.Count > 0 && dss.Tables[0].Rows.Count > 0)
                    {
                        spStatutoryCompliance.StatutoryComplianceUpdate(infoStatutoryCompliance);

                        response.ResponseCode = 200;
                        response.ResponseMessage = "success";
                    }
                    else
                    {
                        spStatutoryCompliance.StatutoryComplianceAdd(infoStatutoryCompliance);
                        response.ResponseCode = 200;
                        response.ResponseMessage = "success";
                    }
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
                response.Response = ex;
            }
            return response;
        }


        protected decimal ComputePAYE(decimal SalaryPackageID, List<decimal> SelectedPayHeads)
        {
            DataSet ds = new DataSet();
            DataTable dt = new DataTable();
            decimal TaxableIncome; decimal AnnualGross = 1; decimal ConsolidatedRelief; decimal NHF = 0; decimal EarnedIncome;
            decimal FirstPortion = 300000, SecondPortion = 300000, ThirdPortion = 500000, FourthPortion = 500000, FifthPortion = 1600000, RemainingPortion;
            decimal PAYE = 0, MinimumTaxRate = 0;
            try
            {
                List<decimal> PayHeadIdsForGross = new List<decimal>();
                DataTable dtPayHeads = PayHeads.PayHeadViewAll();
                DataTable dtMonthlyGross = SalaryPackageDetails.SalaryPackageDetailsViewAll();

                Pension = (ComputePension(SalaryPackageID, SelectedPayHeads) * 12);

                decimal NHFElement = (from d in dtMonthlyGross.AsEnumerable()
                                      join e in dtPayHeads.AsEnumerable() on d.Field<decimal>("payHeadId") equals e.Field<decimal>("payHeadId")
                                      where d.Field<decimal>("salaryPackageId") == SalaryPackageID && e.Field<string>("payHeadName") == "Basic Salary"
                                      select d.Field<decimal>("amount")).Sum();
                //NHF = ((decimal)2.5 / 100) * (NHFElement * 12);

                if (dtMonthlyGross != null && dtMonthlyGross.Rows.Count > 0)
                {
                    AnnualGross = ((from k in dtMonthlyGross.AsEnumerable()
                                    join l in dtPayHeads.AsEnumerable() on k.Field<decimal>("payHeadId") equals l.Field<decimal>("payHeadId")
                                    where k.Field<decimal>("salaryPackageId") == SalaryPackageID && l.Field<string>("type") == "Addition"
                                    select k.Field<decimal>("amount")).Sum()) * (decimal)12;
                }
                EarnedIncome = AnnualGross;
                if ((200000 + ((decimal)0.20 * AnnualGross)) <= ((decimal)0.21 *AnnualGross))
                {
                    ConsolidatedRelief = ((decimal)0.21 * AnnualGross) + Pension;
                }
                else
                {
                    ConsolidatedRelief = (200000 + ((decimal)0.20 * AnnualGross)) + Pension;
                }

                TaxableIncome = AnnualGross - ConsolidatedRelief;
                if (TaxableIncome >= 300000)
                {
                    PAYE += (decimal)0.07 * FirstPortion;
                    TaxableIncome = TaxableIncome - FirstPortion;

                    if (TaxableIncome < 300000)
                    {
                        PAYE += (decimal)0.11 * TaxableIncome;
                        TaxableIncome = 0;
                    }
                    else if (TaxableIncome > 300000)
                    {
                        PAYE += (decimal)0.11 * SecondPortion;
                        TaxableIncome = TaxableIncome - SecondPortion;
                    }
                    if (TaxableIncome < 500000)
                    {
                        PAYE += (decimal)0.15 * TaxableIncome;
                        TaxableIncome = 0;
                    }
                    if (TaxableIncome > 500000)
                    {
                        PAYE += (decimal)0.15 * ThirdPortion;
                        TaxableIncome = TaxableIncome - ThirdPortion;
                    }
                    if (TaxableIncome < 500000)
                    {
                        PAYE += (decimal)0.19 * TaxableIncome;
                        TaxableIncome = 0;
                    }
                    else if (TaxableIncome > 500000)
                    {
                        PAYE += (decimal)0.19 * FourthPortion;
                        TaxableIncome = TaxableIncome - FourthPortion;
                    }
                    if (TaxableIncome < 1600000)
                    {
                        PAYE += (decimal)0.21 * TaxableIncome;
                        TaxableIncome = 0;
                    }
                    else if(TaxableIncome > 1600000)
                    {
                        PAYE += (decimal)0.21 * FifthPortion;
                        TaxableIncome = TaxableIncome - FifthPortion;
                    }
                    if (TaxableIncome < 3200000)
                    {
                        PAYE += (decimal)0.24 * TaxableIncome;
                        TaxableIncome = 0;
                    }
                    else if (TaxableIncome > 3200000)
                    {
                        PAYE += (decimal)0.24 * TaxableIncome;
                    }
                }
                else if (TaxableIncome < 300000)
                {
                    PAYE = (decimal)0.07 * TaxableIncome;
                }
                if (AnnualGross <= 300000)
                {
                    MinimumTaxRate = (decimal)0.01 * AnnualGross;
                }
                PAYE = PAYE > MinimumTaxRate ? PAYE : MinimumTaxRate;
                PAYE = PAYE / 12;

            }
            catch (Exception ex)
            {
            }
            return PAYE;
        }
        protected decimal ComputePension(decimal SalaryPackageID, List<decimal> SelectedPayHeads)
        {
            decimal PensionIncome;
            DataTable dtTaxableIncome = new DataTable();
            //dtTaxableIncome = SalaryPackage.SalaryPackageViewAll();
            dtTaxableIncome = SalaryPackageDetails.SalaryPackageDetailsViewAll();
            if (dtTaxableIncome != null && dtTaxableIncome.Rows.Count > 0)
            {
                PensionIncome = (from d in dtTaxableIncome.AsEnumerable()
                                 where d.Field<decimal>("salaryPackageId") == SalaryPackageID && SelectedPayHeads.Contains(d.Field<decimal>("payHeadId"))
                                 select d.Field<decimal>("amount")).Sum();
                Pension = ((decimal)8 / 100) * PensionIncome;
            }
            return Pension;
        }


        /// <summary>
        /// Precious Method for Calculating PAYE
        /// </summary>
        /// <param name="SalaryPackageId"></param>
        /// <returns></returns>
        //public decimal CalculatePAYE(decimal SalaryPackageId)
        //{
        //    decimal accumulatedTaxIncome = 0m, accumulatedDeductedIncome = 0m;
        //    decimal TaxableIncome = 0, earnedIncome = 0, Pension = 0, NHF = 0, PayeePerAnnum = 0, PayeePerMonth = 0;
        //    decimal taxFreeIncome = 0, persAllowed = 0;
        //    decimal first, second, third, fourth, fifth, sixth;
        //    decimal Basic = 0, Housing = 0, Transport = 0;

        //    SalaryPackageDetailsSP spSalaryPackage = new SalaryPackageDetailsSP();
        //    SalaryPackageSP SalaryPackage = new SalaryPackageSP();

        //    DataTable dtbl = spSalaryPackage.SalaryPackageForComputation(SalaryPackageId);
        //    earnedIncome = Convert.ToDecimal(dtbl.Rows[0]["totalAmount"]) * 12;

        //    Basic = (from d in dtbl.AsEnumerable()
        //             .Where(d => d.Field<string>("payHeadName").Contains("Basic") ||
        //             d.Field<string>("payHeadName").Contains("BASIC") ||
        //             d.Field<string>("payHeadName").Contains("basic"))
        //             select d.Field<decimal>("amount")).Sum();

        //    Transport = (from t in dtbl.AsEnumerable()
        //                 .Where(t => t.Field<string>("payHeadName").Contains("Transport") ||
        //                 t.Field<string>("payHeadName").Contains("TRANSPORT") ||
        //                 t.Field<string>("payHeadName").Contains("transport"))
        //                 select t.Field<decimal>("amount")).Sum();

        //    Housing = (from h in dtbl.AsEnumerable()
        //               .Where(h => h.Field<string>("payHeadName").Contains("Housing") ||
        //               h.Field<string>("payHeadName").Contains("HOUSING") ||
        //               h.Field<string>("payHeadName").Contains("housing"))
        //               select h.Field<decimal>("amount")).Sum();

        //    Pension = 0.08m * (Basic + Housing + Transport);  // 8% of B+H+T           

        //    persAllowed = (earnedIncome * 0.2m) + 200000 / 12; // gross * 20% + 200,000 i.e CRA. (Added Pension as suggested by JANE)
        //    NHF = 0.025m * Basic; // 2.5% of B           

        //    //if (chkAllowNHF.Checked == true)
        //    //{
        //    //    // if NHF is activated.
        //    //    taxFreeIncome = persAllowed + Pension + NHF;
        //    //}
        //    //else
        //    //{
        //        taxFreeIncome = persAllowed + Pension; // + NHF; If NHF is not allowed.
        //    //}
        //    TaxableIncome = earnedIncome - taxFreeIncome;
        //    accumulatedDeductedIncome = TaxableIncome;

        //    first = 300000 / 12;
        //    second = 300000 / 12;
        //    third = 500000 / 12;
        //    fourth = 500000 / 12;
        //    fifth = 1600000 / 12;
        //    sixth = 3200000 / 12;

        //    //if (TaxableIncome < first)
        //    //{
        //    //    PayeePerMonth = earnedIncome / 100;
        //    //    return PayeePerMonth;
        //    //}
        //    if (TaxableIncome < 0)
        //    {
        //        PayeePerMonth = earnedIncome / 1200;
        //        return PayeePerMonth;
        //    }
        //    //else if (earnedIncome < taxFreeIncome)
        //    //{
        //    //    PayeePerMonth = earnedIncome / 1200;
        //    //    return PayeePerMonth;
        //    //}
        //    else
        //    {

        //        accumulatedTaxIncome = TaxableIncome;
        //        int i = 1;
        //        do
        //        {
        //            if (i == 1)
        //            {
        //                if (first <= accumulatedTaxIncome)
        //                {
        //                    PayeePerAnnum += first * 7 / 100;
        //                    accumulatedTaxIncome -= first;
        //                }
        //                else
        //                {
        //                    PayeePerAnnum += accumulatedTaxIncome * 7 / 100;
        //                    return PAYEPerMONTH(PayeePerAnnum);
        //                }
        //            }
        //            else if (i == 2)
        //            {
        //                if (second <= accumulatedTaxIncome && i == 2)
        //                {
        //                    PayeePerAnnum += second * 11 / 100;
        //                    accumulatedTaxIncome -= second;
        //                }
        //                else
        //                {
        //                    PayeePerAnnum += accumulatedTaxIncome * 11 / 100;
        //                    return PAYEPerMONTH(PayeePerAnnum);
        //                }
        //            }
        //            else if (i == 3)
        //            {
        //                if (third <= accumulatedTaxIncome && i == 3)
        //                {
        //                    PayeePerAnnum += third * 15 / 100;
        //                    accumulatedTaxIncome -= third;
        //                }
        //                else
        //                {
        //                    PayeePerAnnum += accumulatedTaxIncome * 15 / 100;
        //                    return PAYEPerMONTH(PayeePerAnnum);
        //                }
        //            }
        //            else if (i == 4)
        //            {
        //                if (fourth <= accumulatedTaxIncome && i == 4)
        //                {
        //                    PayeePerAnnum += fourth * 19 / 100;
        //                    accumulatedTaxIncome -= fourth;
        //                }
        //                else
        //                {
        //                    PayeePerAnnum += accumulatedTaxIncome * 19 / 100;
        //                    return PAYEPerMONTH(PayeePerAnnum);
        //                }
        //            }
        //            else if (i == 5)
        //            {
        //                if (fifth <= accumulatedTaxIncome && i == 5)
        //                {
        //                    PayeePerAnnum += fifth * 21 / 100;
        //                    accumulatedTaxIncome -= fifth;
        //                }
        //                else
        //                {
        //                    PayeePerAnnum += accumulatedTaxIncome * 21 / 100;
        //                    return PAYEPerMONTH(PayeePerAnnum);
        //                }
        //            }
        //            else
        //            {
        //                if (sixth <= accumulatedTaxIncome && i == 6)
        //                {
        //                    PayeePerAnnum += sixth * 24 / 100;
        //                    accumulatedTaxIncome -= sixth;
        //                }
        //                else
        //                {
        //                    PayeePerAnnum += accumulatedTaxIncome * 24 / 100;
        //                    return PAYEPerMONTH(PayeePerAnnum);
        //                }
        //            }

        //            i++;
        //        } while (accumulatedTaxIncome < TaxableIncome && i <= 6);

        //        //first = 300000 * 7 / 100;
        //        //second = 300000 * 11 / 100;
        //        //third = 500000 * 15 / 100;
        //        //fourth = 500000 * 19 / 100;
        //        //fifth = 1600000 * 21 / 100;
        //        //sixth = 3200000 * 24 / 100;

        //        PayeePerMonth = PayeePerAnnum / 12;
        //        return PayeePerAnnum;
        //    }
        //}
        
        public decimal PAYEPerMONTH(decimal annualPayee)
        {
            return annualPayee / 1;
        }
        public decimal CalculatePENSION(decimal SalaryPackageId)
        {
            var company = context.tbl_Company.FirstOrDefault();
            decimal pensionPerMonth = 0;
            decimal earnedIncome = 0, Pension = 0;
            //decimal Basic = 0, Housing = 0, Transport = 0;
            decimal Basic = 0, Housing = 0, Transport = 0, Meal = 0, Utility = 0;

            SalaryPackageDetailsSP spSalaryPackage = new SalaryPackageDetailsSP();
            SalaryPackageSP SalaryPackage = new SalaryPackageSP();

            DataTable dtbl = spSalaryPackage.SalaryPackageForComputation(SalaryPackageId);
            earnedIncome = Convert.ToDecimal(dtbl.Rows[0]["totalAmount"]);

            Basic = (from d in dtbl.AsEnumerable()
                      .Where(d => d.Field<string>("payHeadName").Contains("Basic") ||
                      d.Field<string>("payHeadName").Contains("BASIC") ||
                      d.Field<string>("payHeadName").Contains("basic"))
                     select d.Field<decimal>("amount")).Sum();

            Transport = (from t in dtbl.AsEnumerable()
                         .Where(t => t.Field<string>("payHeadName").Contains("Transport") ||
                         t.Field<string>("payHeadName").Contains("TRANSPORT") ||
                         t.Field<string>("payHeadName").Contains("transport"))
                         select t.Field<decimal>("amount")).Sum();

            Housing = (from h in dtbl.AsEnumerable()
                       .Where(h => h.Field<string>("payHeadName").Contains("Housing") ||
                       h.Field<string>("payHeadName").Contains("HOUSING") ||
                       h.Field<string>("payHeadName").Contains("housing"))
                       select h.Field<decimal>("amount")).Sum();

            //Pension = 0.08m * (Basic + Housing + Transport);  // 8% of B+H+T
            if (company.mailingName.ToUpper().Contains("KASCO KANO"))
            {
                Meal = (from h in dtbl.AsEnumerable()
                       .Where(h => h.Field<string>("payHeadName").ToUpper().Contains("MEAL"))
                        select h.Field<decimal>("amount")).Sum();
                Utility = (from h in dtbl.AsEnumerable()
                       .Where(h => h.Field<string>("payHeadName").ToUpper().Contains("UTILITIES"))
                           select h.Field<decimal>("amount")).Sum();
            }

            Pension = 0.08m * (Basic + Housing + Transport + Meal + Utility);  // 8% of B+H+T, For Kasco (8% of B+H+T+M+U)           

            pensionPerMonth = Pension / 12;
            return Pension;

        }
    }
}
