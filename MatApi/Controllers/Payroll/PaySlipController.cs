﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Data;
using MATFinancials;
using System.Dynamic;
using MatApi.Models;
using MatDal;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Payroll
{
    [EnableCors(origins:"*", headers:"*", methods:"*")]
    public class PaySlipController : ApiController
    {
        public PaySlipController()
        {

        }
        [HttpGet]
        public MatResponse GetLookUps()
        {
            MatResponse response = new MatResponse();
            try
            {
                DataTable dtbl = new DataTable();
                EmployeeSP spEmployee = new EmployeeSP();
                dtbl = spEmployee.EmployeeViewForPaySlip();

                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = dtbl;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 200;
                response.ResponseMessage = "something went wrong";
                response.Response = ex;
            }
            return response;
        }
        [HttpGet]
        public MatResponse GetPaySlipForPrint(decimal employeeId, DateTime month)
        {
            MatResponse response = new MatResponse();

            try
            {
                DataSet dsPaySlip = new SalaryVoucherMasterSP().PaySlipPrinting(employeeId, month, 1);

               /* var companyInfo = dsPaySlip.Tables[0];
                var salaryInfo = dsPaySlip.Tables[1];
                var attendanceInfo = dsPaySlip.Tables[2];*/

                foreach (DataTable dtbl in dsPaySlip.Tables)
                {
                    if (dtbl.Rows.Count > 0)
                    {
                        response.ResponseCode = 200;
                        response.ResponseMessage = "success";
                        response.Response = dsPaySlip;
                    }
                    else
                    {
                        response.ResponseCode = 205;
                        response.ResponseMessage = "Salary not Paid";
                    }
                }
            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
                response.Response = e;
            }
            return response;
        }
    }
}
