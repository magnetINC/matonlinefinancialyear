﻿using MatApi.Models.Customer;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PendingSalesController : ApiController
    {
        SalesMasterSP spSalesMaster = new SalesMasterSP();
        SalesDetailsInfo InfoSalesDetails = new SalesDetailsInfo();
        SalesDetailsSP spSalesDetails = new SalesDetailsSP();
        public DataSet GetPendingSales()
        {
            DataSet ds = new DataSet();
            DBMatConnection connection = new DBMatConnection();
            try
            {
                ds = connection.getDataSet("PendingSalesRegisterGridfillAll");
            }
            catch (Exception EX)
            {

            }
            return ds;
        }
        [HttpGet]
        // public SalesInvoiceVM ViewSales(decimal pendingsalesMasterId, bool salesOrPending)
        public SalesInvoiceVM ViewSales(decimal pendingsalesMasterId, bool salesOrPending)
        {
            //DataTable response = new DataTable();
            SalesInvoiceVM response = new SalesInvoiceVM();
            response.SalesMasterInfo = new SalesMasterInfo();
            response.SalesDetailsInfo = new List<SalesDetailsInfo>();
            var master = spSalesMaster.POSSalesMasterViewBySalesMasterId(pendingsalesMasterId, true);
            var details = spSalesDetails.SalesDetailsViewBySalesMasterId(pendingsalesMasterId, true);

            //response.SalesMasterInfo = master.Rows[0]
            for (int i = 0; i < master.Rows.Count; i++)
            {
                response.SalesMasterInfo.SalesMasterId = decimal.Parse(master.Rows[i].ItemArray[0].ToString());//serves as pendindSalesMasterId or SalesMasterId depending on query
                response.SalesMasterInfo.VoucherNo = master.Rows[i].ItemArray[1].ToString();
                response.SalesMasterInfo.InvoiceNo = master.Rows[i].ItemArray[2].ToString();
                response.SalesMasterInfo.SuffixPrefixId = decimal.Parse(master.Rows[i].ItemArray[3].ToString());
                response.SalesMasterInfo.VoucherTypeId = decimal.Parse(master.Rows[i].ItemArray[4].ToString());
                response.SalesMasterInfo.Date = DateTime.Parse(master.Rows[i].ItemArray[5].ToString());
                response.SalesMasterInfo.CustomerName = master.Rows[i].ItemArray[6].ToString();
                response.SalesMasterInfo.PricinglevelId = decimal.Parse(master.Rows[i].ItemArray[7].ToString());
                response.SalesMasterInfo.EmployeeId = decimal.Parse(master.Rows[i].ItemArray[8].ToString());
                response.SalesMasterInfo.CreditPeriod = int.Parse(master.Rows[i].ItemArray[9].ToString());
                response.SalesMasterInfo.LedgerId = decimal.Parse(master.Rows[i].ItemArray[10].ToString());
                response.SalesMasterInfo.SalesAccount = decimal.Parse(master.Rows[i].ItemArray[11].ToString());
                response.SalesMasterInfo.QuotationMasterId = decimal.Parse(master.Rows[i].ItemArray[12].ToString());
                response.SalesMasterInfo.OrderMasterId = decimal.Parse(master.Rows[i].ItemArray[13].ToString());
                response.SalesMasterInfo.DeliveryNoteMasterId = decimal.Parse(master.Rows[i].ItemArray[14].ToString());
                response.SalesMasterInfo.Narration = master.Rows[i].ItemArray[15].ToString();
                response.SalesMasterInfo.ExchangeRateId = decimal.Parse(master.Rows[i].ItemArray[16].ToString());
                response.SalesMasterInfo.CounterId = decimal.Parse(master.Rows[i].ItemArray[17].ToString());
                response.SalesMasterInfo.AdditionalCost = decimal.Parse(master.Rows[i].ItemArray[18].ToString());
                response.SalesMasterInfo.TaxAmount = decimal.Parse(master.Rows[i].ItemArray[19].ToString());
                response.SalesMasterInfo.BillDiscount = decimal.Parse(master.Rows[i].ItemArray[20].ToString());
                response.SalesMasterInfo.TotalAmount = decimal.Parse(master.Rows[i].ItemArray[21].ToString());
                response.SalesMasterInfo.GrandTotal = decimal.Parse(master.Rows[i].ItemArray[22].ToString());
                response.SalesMasterInfo.UserId = decimal.Parse(master.Rows[i].ItemArray[23].ToString());
                response.SalesMasterInfo.LrNo = master.Rows[i].ItemArray[25].ToString();
                response.SalesMasterInfo.TransportationCompany = master.Rows[i].ItemArray[26].ToString();
                response.SalesMasterInfo.ExtraDate = DateTime.Parse(master.Rows[i].ItemArray[27].ToString());
                response.SalesMasterInfo.Extra1 = master.Rows[i].ItemArray[28].ToString();
                response.SalesMasterInfo.Extra2 = master.Rows[i].ItemArray[29].ToString();
                response.SalesMasterInfo.POS = bool.Parse(master.Rows[i].ItemArray[32].ToString());
                response.SalesMasterInfo.AmountTendered = decimal.Parse(master.Rows[i].ItemArray[33].ToString());
                response.SalesMasterInfo.ChangeGiven = decimal.Parse(master.Rows[i].ItemArray[34].ToString());
                response.SalesMasterInfo.POSTellerNo = master.Rows[i].ItemArray[35].ToString();
                response.SalesMasterInfo.cardAmount = decimal.Parse(master.Rows[i].ItemArray[36].ToString());
                

            }

            //for (int i = 0; i < details.Rows.Count; i++)
            //{
            //    response.SalesDetailsInfo.Add( new SalesDetailsInfo{
            //        SalesMasterId = decimal.Parse(details.Rows[i].ItemArray[0].ToString()),
            //        SalesDetailsId = decimal.Parse(details.Rows[i].ItemArray[1].ToString()),
            //        DeliveryNoteDetailsId = decimal.Parse(details.Rows[i].ItemArray[2].ToString()),
            //        OrderDetailsId = decimal.Parse(details.Rows[i].ItemArray[3].ToString()),
            //        QuotationDetailsId = decimal.Parse(details.Rows[i].ItemArray[4].ToString()),
            //        ProductId = decimal.Parse(details.Rows[i].ItemArray[5].ToString()),
            //        Qty = decimal.Parse(details.Rows[i].ItemArray[11].ToString()),
            //        Rate = decimal.Parse(details.Rows[i].ItemArray[12].ToString()),
            //        UnitId = decimal.Parse(master.Rows[i].ItemArray[13].ToString()),
            //        UnitConversionId = decimal.Parse(details.Rows[i].ItemArray[15].ToString()),
            //        TaxId = decimal.Parse(details.Rows[i].ItemArray[16].ToString()),
            //        TaxName = details.Rows[i].ItemArray[17].ToString(),
            //        BatchId = decimal.Parse(master.Rows[i].ItemArray[18].ToString()),
            //        GodownId = decimal.Parse(details.Rows[i].ItemArray[21].ToString()),
            //        RackId = decimal.Parse(details.Rows[i].ItemArray[23].ToString()),
            //        TaxAmount = decimal.Parse(details.Rows[i].ItemArray[26].ToString()),
            //        GrossAmount = decimal.Parse(details.Rows[i].ItemArray[27].ToString()),
            //        NetAmount = decimal.Parse(details.Rows[i].ItemArray[28].ToString()),
            //        Discount = decimal.Parse(details.Rows[i].ItemArray[29].ToString()),
            //        Amount = decimal.Parse(details.Rows[i].ItemArray[30].ToString()),
            //        SlNo = int.Parse(details.Rows[i].ItemArray[31].ToString()),
            //        ProjectId = int.Parse(details.Rows[i].ItemArray[32].ToString()),
            //        CategoryId = int.Parse(details.Rows[i].ItemArray[33].ToString()),
                  
            //    });

            //}

            foreach (DataRow item in details.Rows)
            {
                response.SalesDetailsInfo.Add(new SalesDetailsInfo
                {
                    SalesMasterId = decimal.Parse(item.ItemArray[0].ToString()),
                    SalesDetailsId = decimal.Parse(item.ItemArray[1].ToString()),
                    DeliveryNoteDetailsId = decimal.Parse(item.ItemArray[2].ToString()),
                    OrderDetailsId = decimal.Parse(item.ItemArray[3].ToString()),
                    QuotationDetailsId = decimal.Parse(item.ItemArray[4].ToString()),
                    ProductId = decimal.Parse(item.ItemArray[5].ToString()),
                    Qty = decimal.Parse(item.ItemArray[11].ToString()),
                    Rate = decimal.Parse(item.ItemArray[12].ToString()),
                    UnitId = decimal.Parse(item.ItemArray[13].ToString()),
                    UnitConversionId = decimal.Parse(item.ItemArray[15].ToString()),
                    TaxId = decimal.Parse(item.ItemArray[16].ToString()),
                    TaxName = item.ItemArray[17].ToString(),
                    BatchId = decimal.Parse(item.ItemArray[18].ToString()),
                    GodownId = decimal.Parse(item.ItemArray[21].ToString()),
                    RackId = decimal.Parse(item.ItemArray[23].ToString()),
                    TaxAmount = decimal.Parse(item.ItemArray[26].ToString()),
                    GrossAmount = decimal.Parse(item.ItemArray[27].ToString()),
                    NetAmount = decimal.Parse(item.ItemArray[28].ToString()),
                    Discount = decimal.Parse(item.ItemArray[29].ToString()),
                    Amount = decimal.Parse(item.ItemArray[30].ToString()),
                    SlNo = int.Parse(item.ItemArray[31].ToString()),
                    ProjectId = int.Parse(item.ItemArray[32].ToString()),
                    CategoryId = int.Parse(item.ItemArray[33].ToString()),

                });
            }
                   
            return response;
        }
    }
}
