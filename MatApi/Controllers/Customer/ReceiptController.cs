﻿using MatApi.Models;
using MatApi.Models.Customer;
using MATFinancials;
using MATFinancials.DAL;
using MATFinancials.Other;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Windows.Forms;
using MatApi.DBModel;
//using MatDal;
using MatApi.Models.Supplier;
using MatDal;
using System.Threading.Tasks;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class ReceiptController : ApiController
    {
        decimal decReceiptVoucherTypeId = 5;
        decimal decagainstVoucherTypeId = 5;
        decimal voucherTypeId = 28;
        DataTable dtblPartyBalance = new DataTable();//to store party balance entries while clicking btn_Save in Receipt voucher
        string tableName = "ReceiptMaster";//to get the table name in voucher type selection
        string strPrefix = string.Empty;//to get the prefix string from frmvouchertypeselection decimal decPaymentVoucherTypeId = 4;
        string strSuffix = string.Empty;//to get the suffix string from frmvouchertypeselection

        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public ReceiptController()
        {

        }

        [HttpGet]
        public HttpResponseMessage GetLookups()
        {
            dynamic response = new ExpandoObject();
            var bankOrCash = new TransactionsGeneralFill().BankOrCashComboFill(false);
            //var bankOrCash = new TransactionsGeneralFill().CashOrBankComboFill(false, 6, 0);
            var currency = new TransactionsGeneralFill().CurrencyComboFill();
            var customers = new AccountLedgerSP().AccountLedgerViewCustomerOnly();

            response.BankOrCash = bankOrCash;
            response.Currency = currency;
            response.Customers = customers;
            response.VoucherNo = generateVoucherNo();
            response.VoucherTypes = new VoucherTypeSP().VoucherTypeViewAll();
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage getMasterandDetailsForEdit(decimal id, string voucherNo)
        {
            dynamic response = new ExpandoObject();

            response.master = new ReceiptMasterSP().ReceiptMasterView(id);
            response.details = new ReceiptDetailsSP().ReceiptDetailsViewByMasterId(id);

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public DataTable GetCustomerInvoices(decimal ledgerId, string voucherNo)
        {

            var FinianlYears = context.tbl_FinancialYear.ToList();

            var BeginFinianlYear = FinianlYears.Min(dt => dt.fromDate);

            if(BeginFinianlYear==null || BeginFinianlYear >= MATFinancials.PublicVariables._dtToDate)
            {
                BeginFinianlYear = MATFinancials.PublicVariables._dtFromDate;
            }

            var SalesInvs = context.tbl_SalesMaster.Where(s => s.ledgerId == 160700 && s.ledgerId == 160695 && s.voucherTypeId == 28 && (s.voucherNo == "48" || s.voucherNo == "47" || s.voucherNo == "50" || s.voucherNo == "45")).ToList();

            var  SalesInvs1 = context.tbl_SalesMaster.Where(s => s.ledgerId == 160700 && s.ledgerId == 160695 && s.voucherTypeId == 28 && (s.voucherNo == "48" || s.voucherNo == "47" || s.voucherNo == "50" || s.voucherNo == "45")).ToList();

            SalesInvs.AddRange(SalesInvs1);
            foreach (var itm in SalesInvs)
            {

                var patBals = context.tbl_PartyBalance.Where(s => s.voucherTypeId == itm.voucherTypeId && (s.voucherNo == itm.voucherNo) && s.ledgerId == itm.ledgerId).Any();
               
                if(!patBals)
                {
                    var PartyBalance = new DBModel.tbl_PartyBalance()
                    {
                        debit = itm.grandTotal,
                        credit = 0,
                        date = itm.date,
                        extra1 = itm.extra1,
                        extra2 = itm.extra2,
                        extraDate = DateTime.Now,
                        financialYearId = itm.financialYearId,
                        invoiceNo = itm.invoiceNo,
                        voucherTypeId = itm.voucherTypeId,
                        voucherNo = itm.voucherNo,
                        ledgerId = itm.ledgerId,
                        creditPeriod = itm.creditPeriod,
                        exchangeRateId = itm.exchangeRateId,
                        againstVoucherTypeId = 0,
                        againstInvoiceNo = "0",
                        againstVoucherNo = "0",
                        referenceType = "New",
                    };

                    context.tbl_PartyBalance.Add(PartyBalance);
                    context.SaveChanges();
                }
            }



            //MATFinancials.PublicVariables._dtFromDate
            var ResultOPI = new PartyBalanceSP().PartyBalanceComboViewByLedgerIdUseInvoiceNo(BeginFinianlYear.Value, MATFinancials.PublicVariables._dtToDate, ledgerId, "Cr", decReceiptVoucherTypeId, voucherNo);

            ResultOPI = ResultOPI.AsEnumerable()
                             .Where(r => r.Field<string>("voucherTypeId") != "6")
                             .CopyToDataTable();

            foreach (DataRow dr in ResultOPI.Rows)
            {
                var Result = dr[0].ToString();
                var Result1 = dr[1].ToString();
                var Result2 = dr[2].ToString();
                var Result3 = dr[3].ToString();
                var Result4 = dr[4].ToString();
                var Result5 = dr[5].ToString();
                var Result6 = dr[6].ToString();
                //var Result7 = dr[7].ToString();
                //var Result8 = dr[8].ToString();
                //var Result9 = dr[9].ToString();

            }

             return ResultOPI;
            // return new PartyBalanceSP().PartyBalanceComboViewByLedgerId(ledgerId, "Cr", decReceiptVoucherTypeId, voucherNo);
            // return new PartyBalanceSP().PartyBalanceComboViewByLedgerId(ledgerId, "Cr", 28, voucherNo); 
        }

        public int SavePayment(ReceiptVM input)
        {
            bool isSaved = false;

            ReceiptMasterInfo InfoReceiptMaster = new ReceiptMasterInfo();
            ReceiptMasterSP SpReceiptMaster = new ReceiptMasterSP();
            ReceiptDetailsInfo InfoReceiptDetails = new ReceiptDetailsInfo();
            ReceiptDetailsSP SpReceiptDetails = new ReceiptDetailsSP();
            PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
            PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
            LedgerPostingInfo ledgerPostingInfo = new LedgerPostingInfo();
            int inTableRowCount = input.PartyBalanceInfo.Count();
            //To know the value of exchangeRate
            var decSelectedCurrencyRate = new ExchangeRateSP().GetExchangeRateByExchangeRateId(1);

            input.ReceiptMasterInfo.Extra1 = string.Empty;
            input.ReceiptMasterInfo.Extra2 = string.Empty;
            input.ReceiptMasterInfo.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
            input.ReceiptMasterInfo.SuffixPrefixId = 0;
            //get total amount 
            var totalAmount = 0.0;
            foreach (var i in input.ReceiptDetailsInfo)
            {
                DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
                if (i.InvoiceNo != "-")
                {
                    var result = context.tbl_PartyBalance.FirstOrDefault(x => x.invoiceNo == i.InvoiceNo && x.voucherTypeId == 28);
                    result.debit = result.debit - i.Amount;

                    //var current = context.tbl_PartyBalance.FirstOrDefault(x => x.debit == x.voucherTypeId);
                    //if (current != null)
                    //{
                    //    current.credit = current.credit - current.debit;
                    //    context.Entry(result).State = System.Data.Entity.EntityState.Modified;
                    //    context.SaveChanges();
                    //}
                    context.Entry(result).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges();
                }
                //else
                //{
                //    DBModel.tbl_PartyBalance tb = new DBModel.tbl_PartyBalance();
                //    tb.ledgerId = i.LedgerId;
                //    tb.voucherNo = i.VoucherNo;
                //    //tb.date = i.ChequeDate;
                //    //tb.credit = i.Amount;

                //    //context.tbl_PartyBalance.Add(tb);
                //    //context.SaveChanges();

                //}

                totalAmount += Convert.ToDouble(i.Amount);
            }


            input.ReceiptMasterInfo.TotalAmount = Convert.ToDecimal(totalAmount);

            input.ReceiptMasterInfo.VoucherTypeId = decReceiptVoucherTypeId;
            input.ReceiptMasterInfo.DoneBy = input.ReceiptMasterInfo.DoneBy;
            input.ReceiptMasterInfo.InvoiceNo = input.ReceiptMasterInfo.InvoiceNo;
            input.ReceiptMasterInfo.VoucherNo = input.ReceiptMasterInfo.VoucherNo;
            decimal decReceiptMasterId = SpReceiptMaster.ReceiptMasterAdd(input.ReceiptMasterInfo);
            if (decReceiptMasterId != 0)
            {
                isSaved = true;
                //if (dgvReceiptVoucher.Rows[0].Cells["dgvtxtChequeNo"].Value != null && dgvReceiptVoucher.Rows[0].Cells["dgvtxtChequeNo"].Value.ToString() != string.Empty && dgvReceiptVoucher.Rows.Count == 2)
                //{
                //    chequeNo = dgvReceiptVoucher.Rows[0].Cells["dgvtxtChequeNo"].Value.ToString();
                //}
                isSaved = MasterLedgerPosting(decReceiptMasterId, input.ReceiptMasterInfo);
                //if (dgvReceiptVoucher.Rows[0].Cells["dgvCmbWithHoldingTax"].Value != null
                //    && dgvReceiptVoucher.Rows[0].Cells["dgvCmbWithHoldingTax"].Value.ToString().Trim() != string.Empty
                //    && txtWithHoldingTax.Text != string.Empty && Convert.ToDecimal(txtWithHoldingTax.Text) > 0)
                //{
                //    decimal taxId = Convert.ToDecimal(dgvReceiptVoucher.Rows[0].Cells["dgvCmbWithHoldingTax"].Value);
                //    decimal ledgerId = dtblWithholdingTax.AsEnumerable().Where(i => i.Field<decimal>("taxId") == taxId)
                //    .Select(i => i.Field<decimal>("ledgerId")).FirstOrDefault();
                //    postLedgerForWithholdingTax(ledgerId);
                //    postWithholdingTaxDetails(true, false, false);
                //}
            }
            for (int i = 0; i < input.ReceiptDetailsInfo.Count; i++)
            {
                input.ReceiptDetailsInfo[i].Extra1 = string.Empty;
                input.ReceiptDetailsInfo[i].Extra2 = string.Empty;
                input.ReceiptDetailsInfo[i].ProjectId = 0;
                input.ReceiptDetailsInfo[i].CategoryId = 0;
                input.ReceiptDetailsInfo[i].ExchangeRateId = 1;
                //input.ReceiptDetailsInfo[i].VoucherNo = input.ReceiptDetailsInfo[i].VoucherNo;
                //input.ReceiptDetailsInfo[i].VoucherNo = "";
                input.ReceiptDetailsInfo[i].InvoiceNo = input.ReceiptDetailsInfo[i].InvoiceNo;
                input.ReceiptDetailsInfo[i].ReceiptMasterId = decReceiptMasterId;
                //input.ReceiptDetailsInfo[i].ChequeNo = input.ReceiptDetailsInfo[i].ChequeNo == null? "" : input.ReceiptDetailsInfo[i].ChequeNo;
                if (input.ReceiptDetailsInfo[i].ChequeNo == null || input.ReceiptDetailsInfo[i].ChequeNo == "")
                {
                    // input.ReceiptDetailsInfo[i].ChequeDate = DateTime.Now;
                    //changed the datetime from present date to an unrealistic date to reinforce the fact that no cheque number was saved
                    input.ReceiptDetailsInfo[i].ChequeDate = DateTime.Parse(input.ReceiptDetailsInfo[i].ChequeDate.ToString());
                    input.ReceiptDetailsInfo[i].ChequeNo = "";
                }

                decimal decReceiptDetailsId = SpReceiptDetails.ReceiptDetailsAdd(input.ReceiptDetailsInfo[i]);
                if (decReceiptDetailsId != 0)
                {
                    isSaved = true;
                    //for (int inJ = 0; inJ < inTableRowCount; inJ++)
                    //{
                    //    if (dgvReceiptVoucher.Rows[inI].Cells["dgvcmbAccountLedger"].Value.ToString() == dtblPartyBalance.Rows[inJ]["LedgerId"].ToString())
                    //        if (input.ReceiptDetailsInfo[inJ].LedgerId == Convert.ToDecimal(input.PartyBalanceInfo[inJ].LedgerId.ToString()))
                    //        {
                    //            PartyBalanceAddOrEdit(inJ, input);
                    //        }
                    //}

                    for (int k = 0; k < input.PartyBalanceInfo.Count; k++)
                    {
                        if (input.ReceiptDetailsInfo[i].LedgerId == input.PartyBalanceInfo[k].LedgerId)
                        {
                            isSaved = PartyBalanceAddOrEdit(k, input.PartyBalanceInfo, input.ReceiptMasterInfo);
                        }
                    }
                    //inB++;

                    isSaved = DetailsLedgerPosting(i, decReceiptDetailsId, input.ReceiptDetailsInfo[i].ChequeDate, input.ReceiptDetailsInfo[i].ChequeNo,
                        input.ReceiptMasterInfo, input.ReceiptDetailsInfo[i], input.PartyBalanceInfo);
                }
                else
                {
                    isSaved = false;
                }
            }
            if (isSaved) //if nothing failed, return the next voucher number
            {
                return Convert.ToInt32(generateVoucherNo());
            }

            return 0;
        }

        [HttpGet]
        public decimal deleteReceipt(decimal receiptMastertId)
        {
            int decResult1 = 0;
            ReceiptMasterSP rm = new ReceiptMasterSP();
            ReceiptDetailsSP rd = new ReceiptDetailsSP();
            var receiptmaster = new ReceiptMasterSP().ReceiptMasterView(receiptMastertId);
            string invoiceNo = receiptmaster.InvoiceNo;

            rm.ReceiptMasterDelete(receiptMastertId);

            var receipts = new ReceiptDetailsSP().ReceiptDetailsViewByMasterId(receiptMastertId);
            for (var r = 0; r < receipts.Rows.Count; r++)
            {
                rd.ReceiptDetailsDelete(Convert.ToDecimal(receipts.Rows[r].ItemArray[1]));
                decResult1 = 1;

                
                string originalInvoiceNo = receipts.Rows[r].ItemArray[12].ToString();
                if (receipts.Rows[r].ItemArray[12].ToString() != "-")
                {
                    var result = context.tbl_PartyBalance.FirstOrDefault(x => x.invoiceNo == originalInvoiceNo && x.voucherTypeId == 28);
                    result.debit = result.debit + decimal.Parse(receipts.Rows[r].ItemArray[2].ToString());


                    context.Entry(result).State = System.Data.Entity.EntityState.Modified;
                    context.SaveChanges(); 
                }

            }
            if (decResult1 > 0)
            {
                new PartyBalanceSP().PartyBalanceDeleteByVoucherTypeAndVoucherNo(voucherTypeId, invoiceNo, decagainstVoucherTypeId);
                new AccountLedgerSP().LedgerPostingDeleteByVoucherTypeAndVoucherNo(invoiceNo, decReceiptVoucherTypeId);
                decResult1 = 1;
            }
            return decResult1;
        }
        public bool MasterLedgerPostingEdit(decimal decReceiptMasterId, ReceiptMasterInfo input)
        {
            try
            {
                LedgerPostingInfo InfoLedgerPosting = new LedgerPostingInfo();
                LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
                ExchangeRateSP SpExchangRate = new ExchangeRateSP();
                InfoLedgerPosting.Debit = input.TotalAmount;
                InfoLedgerPosting.Date = input.Date;
                InfoLedgerPosting.Credit = 0;
                //InfoLedgerPosting.DetailsId = 0;
                InfoLedgerPosting.DetailsId = decReceiptMasterId;
                InfoLedgerPosting.Extra1 = string.Empty;
                InfoLedgerPosting.Extra2 = string.Empty;
                InfoLedgerPosting.InvoiceNo = input.InvoiceNo;
                InfoLedgerPosting.ChequeNo = "";
                InfoLedgerPosting.ChequeDate = DateTime.Now;
                InfoLedgerPosting.LedgerId = input.LedgerId;
                InfoLedgerPosting.VoucherNo = input.VoucherNo;
                InfoLedgerPosting.VoucherTypeId = decReceiptVoucherTypeId;
                InfoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                //SpLedgerPosting.LedgerPostingEditByVoucherTypeAndVoucherNo(InfoLedgerPosting);
                SpLedgerPosting.LedgerPostingEditByVoucherTypeAndVoucherNoAndLedgerId(InfoLedgerPosting);
            }
            catch (Exception e)
            {

            }
            return true;

        }
        public bool MasterLedgerPosting(decimal decReceiptMasterId, ReceiptMasterInfo input)
        {
            bool isSave = false;

            try
            {
                LedgerPostingInfo InfoLedgerPosting = new LedgerPostingInfo();
                LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
                ExchangeRateSP SpExchangRate = new ExchangeRateSP();
                InfoLedgerPosting.Debit = input.TotalAmount;
                InfoLedgerPosting.Credit = 0;
                //InfoLedgerPosting.DetailsId = 0;
                InfoLedgerPosting.Date = input.Date;
                InfoLedgerPosting.VoucherNo = input.VoucherNo;
                InfoLedgerPosting.VoucherTypeId = decReceiptVoucherTypeId;
                InfoLedgerPosting.InvoiceNo = input.InvoiceNo;
                InfoLedgerPosting.DetailsId = decReceiptMasterId;
                InfoLedgerPosting.Extra1 = string.Empty;
                InfoLedgerPosting.Extra2 = string.Empty;
                InfoLedgerPosting.ChequeNo = "";
                InfoLedgerPosting.LedgerId = input.LedgerId;
                InfoLedgerPosting.ChequeDate = DateTime.Now;
                InfoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                if (SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting) > 0)
                {
                    isSave = true;
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RV21:" + ex.Message;
            }
            return isSave;
        }

        [HttpGet]
        public IHttpActionResult GetReceiptDetail(int receiptMasterId)
        {
            try
            {
                var recieptMaster = context.tbl_ReceiptMaster
                    .Where(a => a.receiptMasterId == receiptMasterId).ToList();
                var recieptDetail = context.tbl_ReceiptDetails.Where(a => a.receiptMasterId == receiptMasterId)
                    .ToList();
                var ledger = context.tbl_AccountLedger.ToList();

                var receipt = recieptMaster.Join(recieptDetail,
                    rm => rm.receiptMasterId, rd => rd.receiptMasterId,
                    (recm, recd) => new
                    {
                        ReceiptMaster = recm,
                        ReceiptDetails = recd
                    }
                    ).Join(ledger, rmd => rmd.ReceiptMaster.ledgerId, l => l.ledgerId,
                        (rmd, l) => new
                        {
                            ReceiptMasterDetails = rmd,
                            Ledger = l
                        }
                        )
                    ;
                return Json(receipt);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
        }
        public bool PartyBalanceAddOrEdit(int inJ, List<PartyBalanceInfo> input, ReceiptMasterInfo receiptMaster)
        {
            bool isSave = false;

            try
            {
                int inTableRowCount = dtblPartyBalance.Rows.Count;
                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                PartyBalanceSP spPartyBalance = new PartyBalanceSP();
                InfopartyBalance.Debit = 0;
                InfopartyBalance.CreditPeriod = 0;
                InfopartyBalance.Date = receiptMaster.Date;
                InfopartyBalance.Credit = input[inJ].Credit;
                InfopartyBalance.ExchangeRateId = 1/*input[inJ].ExchangeRateId*/;
                InfopartyBalance.Extra1 = string.Empty;
                InfopartyBalance.Extra2 = string.Empty;
                InfopartyBalance.ExtraDate = DateTime.Now;
                InfopartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                InfopartyBalance.LedgerId = input[inJ].LedgerId;
                InfopartyBalance.ReferenceType = input[inJ].ReferenceType;
                if (input[inJ].ReferenceType == "New" || input[inJ].ReferenceType == "OnAccount")
                {
                    InfopartyBalance.AgainstInvoiceNo = "0";//dtblPartyBalance.Rows[inJ]["AgainstInvoiceNo"].ToString();
                    InfopartyBalance.AgainstVoucherNo = "0";//dtblPartyBalance.Rows[inJ]["AgainstVoucherNo"].ToString();
                    InfopartyBalance.AgainstVoucherTypeId = 0;// Convert.ToDecimal(dtblPartyBalance.Rows[inJ]["AgainstVoucherTypeId"].ToString());//decPaymentVoucherTypeId;
                    InfopartyBalance.VoucherTypeId = decReceiptVoucherTypeId;
                    InfopartyBalance.InvoiceNo = input[inJ].InvoiceNo;

                    InfopartyBalance.VoucherNo = input[inJ].VoucherNo;

                }
                else
                {
                    InfopartyBalance.ExchangeRateId = 1;
                    InfopartyBalance.AgainstInvoiceNo = input[inJ].AgainstInvoiceNo;

                    InfopartyBalance.AgainstVoucherNo = input[inJ].AgainstVoucherNo;

                    InfopartyBalance.AgainstVoucherTypeId = decReceiptVoucherTypeId;
                    InfopartyBalance.VoucherTypeId = input[inJ].VoucherTypeId;
                    InfopartyBalance.VoucherNo = input[inJ].VoucherNo;
                    InfopartyBalance.InvoiceNo = input[inJ].InvoiceNo;
                }
                if (input[inJ].PartyBalanceId.ToString() == "0")
                {
                    if (spPartyBalance.PartyBalanceAdd(InfopartyBalance) > 0)
                    {
                        isSave = true;
                    }
                }
                else
                {
                    InfopartyBalance.PartyBalanceId = input[inJ].PartyBalanceId;
                    if (spPartyBalance.PartyBalanceEdit(InfopartyBalance) > 0)
                    {
                        isSave = true;
                    }
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RV10:" + ex.Message;
            }
            return isSave;
        }

        public bool DetailsLedgerPosting(int inA, decimal decreceiptDetailsId, DateTime chequeDate, string chequeNo, ReceiptMasterInfo mast, ReceiptDetailsInfo det, List<PartyBalanceInfo> partyBalanceDetails)
        {
            bool isSaved = false;
            LedgerPostingInfo InfoLedgerPosting = new LedgerPostingInfo();
            LedgerPostingSP SpLedgerPosting = new LedgerPostingSP();
            ExchangeRateSP SpExchangRate = new ExchangeRateSP();
            SettingsSP spSettings = new SettingsSP();
            decimal decOldExchange = 0;
            decimal decNewExchangeRate = 0;
            decimal decNewExchangeRateId = 0;
            decimal decOldExchangeId = 0;
            //decimal  decConvertRate = 0;
            decimal decAmount = 0;
            //decimal decSelectedCurrencyRate = 0;
            try
            {
                //if (!dgvReceiptVoucher.Rows[inA].Cells["dgvtxtAmount"].ReadOnly)
                //{

                //decimal d = Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbCurrency"].Value.ToString());
                //InfoLedgerPosting.LedgerId = Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbAccountLedger"].Value.ToString());

                //decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbCurrency"].Value.ToString()));
                //decAmount = Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvtxtAmount"].Value.ToString());
                //decConvertRate = decAmount * decSelectedCurrencyRate;

                //InfoLedgerPosting.Date = input.Date;
                //InfoLedgerPosting.Debit = 0;
                //InfoLedgerPosting.Credit = input.Credit;
                //InfoLedgerPosting.DetailsId = decreceiptDetailsId;
                //InfoLedgerPosting.Extra1 = string.Empty;
                //InfoLedgerPosting.Extra2 = string.Empty;
                //InfoLedgerPosting.InvoiceNo = input.InvoiceNo;
                //if (dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value != null && dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value.ToString() != string.Empty)
                //{
                //    InfoLedgerPosting.ChequeNo = dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeNo"].Value.ToString();
                //    if (dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value != null && dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value.ToString() != string.Empty)
                //    {
                //        InfoLedgerPosting.ChequeDate = Convert.ToDateTime(dgvReceiptVoucher.Rows[inA].Cells["dgvtxtChequeDate"].Value.ToString());
                //    }
                //    else
                //        InfoLedgerPosting.ChequeDate = DateTime.Now;
                //}
                //else
                //{
                //    InfoLedgerPosting.ChequeNo = string.Empty;
                //    InfoLedgerPosting.ChequeDate = DateTime.Now;
                //}


                //InfoLedgerPosting.VoucherNo = strVoucherNo;

                //InfoLedgerPosting.VoucherTypeId = decReceiptVoucherTypeId;
                //InfoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
                //}
                //else
                //{

                //decConvertRate = decAmount * decSelectedCurrencyRate;
                InfoLedgerPosting.Date = mast.Date;
                InfoLedgerPosting.Extra1 = string.Empty;
                InfoLedgerPosting.Extra2 = string.Empty;
                InfoLedgerPosting.InvoiceNo = mast.InvoiceNo;
                InfoLedgerPosting.VoucherTypeId = decReceiptVoucherTypeId;
                InfoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                //InfoLedgerPosting.Debit = decConvertRate;
                InfoLedgerPosting.LedgerId = det.LedgerId;
                InfoLedgerPosting.VoucherNo = mast.VoucherNo;
                InfoLedgerPosting.DetailsId = decreceiptDetailsId;
               
                if (chequeDate != null && (chequeNo != "" || chequeNo != null))
                {
                    InfoLedgerPosting.ChequeDate = chequeDate;
                    InfoLedgerPosting.ChequeNo = chequeNo;
                }
                else
                {
                    InfoLedgerPosting.ChequeNo = "";
                    InfoLedgerPosting.ChequeDate = DateTime.Now;
                }

                //foreach (DataRow dr in dtblPartyBalance.Rows)
                //{
                //    if (InfoLedgerPosting.LedgerId == Convert.ToDecimal(dr["LedgerId"].ToString()))
                //    {
                //        decOldExchange = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                //        decNewExchangeRateId = Convert.ToDecimal(dr["CurrencyId"].ToString());
                //        decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchange);
                //        decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                //        //decConvertRate = decConvertRate + (decAmount * decSelectedCurrencyRate);      //old implementation takes continual sum of all rows rather than for each row 20161228
                //        decConvertRate = (decAmount * decSelectedCurrencyRate);
                //    }
                //}
                InfoLedgerPosting.Credit = partyBalanceDetails[inA].Credit;

                //if (InfoLedgerPosting.LedgerId == dr.LedgerId)
                //{
                //    decOldExchange = 1;
                //    decNewExchangeRateId = 1;
                //    decSelectedCurrencyRate = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchange);
                //    //decAmount = Convert.ToDecimal(dr.Amount.ToString());
                //    decConvertRate = (decAmount * decSelectedCurrencyRate);
                //}
                if (SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting) > 0)
                {
                    isSaved = true;
                }
                else
                {
                    isSaved = false;
                }

                InfoLedgerPosting.LedgerId = 12;
                InfoLedgerPosting.DetailsId = 0;
                foreach (DataRow dr in dtblPartyBalance.Rows)
                for (int v = 0; v < partyBalanceDetails.Count; v++)
                {
                    //if (Convert.ToDecimal(dgvReceiptVoucher.Rows[inA].Cells["dgvcmbAccountLedger"].Value.ToString()) == Convert.ToDecimal(dr["LedgerId"].ToString()))
                    if (partyBalanceDetails[v].ReferenceType == "Against")
                    {
                            //if (partyBalanceDetails[inA].Credit >= 0)
                            //{

                            //    InfoLedgerPosting.Credit = partyBalanceDetails[inA].Credit;
                            //    InfoLedgerPosting.Debit = 0;
                            //}
                            //else
                            //{
                            //    InfoLedgerPosting.Debit = -1 * partyBalanceDetails[inA].Credit;
                            //    InfoLedgerPosting.Credit = 0;
                            //}
                            //if (SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting) > 0)
                            //{
                            //    isSaved = true;
                            //}
                            //else
                            //{
                            //    isSaved = false;
                            //}

                            
                        if (dr["ReferenceType"].ToString() == "Against")
                        {
                            decNewExchangeRateId = Convert.ToDecimal(dr["CurrencyId"].ToString());
                            decNewExchangeRate = SpExchangRate.GetExchangeRateByExchangeRateId(decNewExchangeRateId);
                            decOldExchangeId = Convert.ToDecimal(dr["OldExchangeRate"].ToString());
                            decOldExchange = SpExchangRate.GetExchangeRateByExchangeRateId(decOldExchangeId);
                            decAmount = Convert.ToDecimal(dr["Amount"].ToString());
                            decimal decForexAmount = (decAmount * decNewExchangeRate) - (decAmount * decOldExchange);
                            if (decForexAmount >= 0)
                            {

                                InfoLedgerPosting.Credit = decForexAmount;
                                InfoLedgerPosting.Debit = 0;
                            }
                            else
                            {
                                InfoLedgerPosting.Debit = -1 * decForexAmount;
                                InfoLedgerPosting.Credit = 0;
                            }
                            SpLedgerPosting.LedgerPostingAdd(InfoLedgerPosting);
                        }
                    }

                }

                // }

            }
            catch (Exception ex)
            {
                // formMDI.infoError.ErrorString = "RV23:" + ex.Message;
            }
            return isSaved;
        }

        private string generateVoucherNo()
        {
            SalaryVoucherMasterSP spMaster = new SalaryVoucherMasterSP();
            ReceiptMasterSP SpReceiptMaster = new ReceiptMasterSP();
            TransactionsGeneralFill obj = new TransactionsGeneralFill();

            string strVoucherNo = "";
            if (strVoucherNo == string.Empty)
            {
                strVoucherNo = "0";
            }
            strVoucherNo = obj.VoucherNumberAutomaicGeneration(decReceiptVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
            if (Convert.ToDecimal(strVoucherNo) != SpReceiptMaster.ReceiptMasterGetMax(decReceiptVoucherTypeId) + 1)
            {
                strVoucherNo = SpReceiptMaster.ReceiptMasterGetMax(decReceiptVoucherTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decReceiptVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                if (SpReceiptMaster.ReceiptMasterGetMax(decReceiptVoucherTypeId) == 0)
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decReceiptVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, tableName);
                }
            }

            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decReceiptVoucherTypeId, DateTime.Now);
            string strPrefix = infoSuffixPrefix.Prefix;
            string strSuffix = infoSuffixPrefix.Suffix;
            string strInvoiceNo = strPrefix + strVoucherNo + strSuffix;
            return strInvoiceNo;
        }

        [HttpPost]
        public bool UpdateReceiptMaster(ReceiptVM input)
        {
            bool isSaved = false;

            input.ReceiptMasterInfo.Extra1 = string.Empty;
            input.ReceiptMasterInfo.Extra2 = string.Empty;
            input.ReceiptMasterInfo.ExtraDate = DateTime.Now;
            input.ReceiptMasterInfo.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
            input.ReceiptMasterInfo.SuffixPrefixId = 0;
            input.ReceiptMasterInfo.VoucherTypeId = decReceiptVoucherTypeId;
            decimal isSuccessful = new ReceiptMasterSP().ReceiptMasterEdit(input.ReceiptMasterInfo);

            if (isSuccessful > 0)
            {
                isSaved = MasterLedgerPostingEdit(input.ReceiptMasterInfo.ReceiptMasterId, input.ReceiptMasterInfo);

                if (input.ReceiptDetailsInfo.Count() > 0)
                {
                    for (int i = 0; i < input.ReceiptDetailsInfo.Count; i++)
                    {
                        input.ReceiptDetailsInfo[i].Extra1 = string.Empty;
                        input.ReceiptDetailsInfo[i].Extra2 = string.Empty;
                        input.ReceiptDetailsInfo[i].ProjectId = 0;
                        input.ReceiptDetailsInfo[i].CategoryId = 0;
                        input.ReceiptDetailsInfo[i].ExchangeRateId = 1;
                        input.ReceiptDetailsInfo[i].ReceiptMasterId = input.ReceiptMasterInfo.ReceiptMasterId;
                        //if (input.ReceiptDetailsInfo[i].ChequeNo == null || input.ReceiptDetailsInfo[i].ChequeNo == "")
                        //{
                        //    input.ReceiptDetailsInfo[i].ChequeDate = DateTime.Now;
                        //    input.ReceiptDetailsInfo[i].ChequeNo = "";
                        //}

                        decimal decReceiptDetailsId = new ReceiptDetailsSP().ReceiptDetailsAdd(input.ReceiptDetailsInfo[i]);
                        if (decReceiptDetailsId > 0)
                        {
                            isSaved = true;
                            for (int k = 0; k < input.PartyBalanceInfo.Count; k++)
                            {
                                if (input.ReceiptDetailsInfo[i].LedgerId == input.PartyBalanceInfo[k].LedgerId)
                                {
                                    isSaved = PartyBalanceAddOrEdit(k, input.PartyBalanceInfo, input.ReceiptMasterInfo);
                                }
                            }
                            isSaved = DetailsLedgerPosting(i, decReceiptDetailsId, input.ReceiptDetailsInfo[i].ChequeDate, input.ReceiptDetailsInfo[i].ChequeNo, input.ReceiptMasterInfo, input.ReceiptDetailsInfo[i], input.PartyBalanceInfo);
                        }
                    }
                }
            }
            else
            {
                isSaved = false;
            }
            return isSaved;
        }

        [HttpPost]
        public bool UpdateReceiptDetails(EditReceiptVM input)
        {
            var isSaved = false;
            LedgerPostingInfo InfoLedgerPosting = new LedgerPostingInfo();

            if (input.ReceiptDetailsInfo != null)
            {
                input.ReceiptDetailsInfo.Extra1 = string.Empty;
                input.ReceiptDetailsInfo.Extra2 = string.Empty;
                input.ReceiptDetailsInfo.ProjectId = 0;
                input.ReceiptDetailsInfo.CategoryId = 0;
                input.ReceiptDetailsInfo.ExchangeRateId = 1;
                // input.ReceiptDetailsInfo.ReceiptMasterId = input.receiptMasterId;
                //if (input.ReceiptDetailsInfo.ChequeNo == null || input.ReceiptDetailsInfo.ChequeNo == "")
                //{
                //    input.ReceiptDetailsInfo.ChequeDate = DateTime.Now;
                //    input.ReceiptDetailsInfo.ChequeNo = "";
                ////}

                decimal saved = new ReceiptDetailsSP().ReceiptDetailsEdit(input.ReceiptDetailsInfo);
                if (saved > 0)
                {
                    DBMatConnection conn = new DBMatConnection();
                    string editPartyBalanceQuery = string.Empty;
                    isSaved = true;
                    if (input.ReceiptDetailsInfo.LedgerId == input.PartyBalanceInfo.LedgerId)
                    {
                        editPartyBalanceQuery = string.Format("UPDATE tbl_PartyBalance SET  " +
                                        "date = '{0}', credit = {1}, ledgerId = {2} " +
                                        "WHERE voucherTypeId = 5 and voucherNo = '{3}' and ledgerId = {4}",
                                        input.PartyBalanceInfo.Date, input.PartyBalanceInfo.Credit,
                                        input.PartyBalanceInfo.LedgerId, input.PartyBalanceInfo.VoucherNo,
                                        input.PartyBalanceInfo.Extra1);
                        if (conn.ExecuteNonQuery2(editPartyBalanceQuery))
                        {

                            InfoLedgerPosting.Date = input.PartyBalanceInfo.Date;
                            InfoLedgerPosting.Extra1 = string.Empty;
                            InfoLedgerPosting.Extra2 = string.Empty;
                            InfoLedgerPosting.InvoiceNo = input.PartyBalanceInfo.InvoiceNo;
                            InfoLedgerPosting.VoucherTypeId = decReceiptVoucherTypeId;
                            InfoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                            InfoLedgerPosting.Debit = 0;
                            InfoLedgerPosting.LedgerId = input.PartyBalanceInfo.LedgerId;
                            InfoLedgerPosting.VoucherNo = input.PartyBalanceInfo.VoucherNo;
                            InfoLedgerPosting.DetailsId = input.ReceiptDetailsInfo.ReceiptDetailsId;
                            InfoLedgerPosting.ChequeDate = input.ReceiptDetailsInfo.ChequeDate;
                            InfoLedgerPosting.ChequeNo = input.ReceiptDetailsInfo.ChequeNo;
                            InfoLedgerPosting.Credit = input.PartyBalanceInfo.Credit;

                            new LedgerPostingSP().LedgerPostingEditByVoucherTypeAndVoucherNoAndLedgerId(InfoLedgerPosting);
                            isSaved = true;
                        }
                    }
                }
            }
            return isSaved;
        }

        [HttpGet]
        public bool DeleteReceiptDetails(string voucherNo, decimal ledgerId, decimal detailsId)
        {
            DBMatConnection conn = new DBMatConnection();

            string deleteReceiptQuery = string.Empty;
            string deleteLedgerPostingQuery = string.Empty;
            string deletePartyBalanceQuery = string.Empty;

            deleteReceiptQuery = string.Format("delete from tbl_ReceiptDetails where receiptDetailsId = {0}", detailsId);
            if (conn.ExecuteNonQuery2(deleteReceiptQuery))
            {
                deleteLedgerPostingQuery = string.Format("delete from tbl_ledgerPosting where detailsId = {0}", detailsId);
                if (conn.ExecuteNonQuery2(deleteLedgerPostingQuery))
                {
                    deletePartyBalanceQuery = string.Format("delete from tbl_PartyBalance where voucherTypeId = {0} and voucherNo = {1} and ledgerId = {2}",
                        decReceiptVoucherTypeId, voucherNo, ledgerId);
                    if (conn.ExecuteNonQuery2(deletePartyBalanceQuery))
                    {
                        return true;
                    }
                    else
                    {
                        return false;
                    }
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        //APPLY CREDIT PART
        public async Task<MatResponse> GetInvoicesToApplyCredit(decimal ledgerId)
        {
            MatResponse response = new MatResponse();
            DBMatConnection db = new DBMatConnection();
            dynamic data = new ExpandoObject();
            List<ApplyCreditInvoices> applyCreditInvoices = new List<ApplyCreditInvoices>();
            try
            {

                //await SwapVoucherNoAndAgainstVoucherNo();
                //await UpdatePartyBalanceWithZeroDebit();


                var salesInvoices = new SalesMasterSP().SalesMasterViewAll();

                var financialYearList = context.tbl_FinancialYear.OrderBy(x =>x.financialYearId);
                if (financialYearList.Count() < 1) throw new Exception($"Unable to fetch any financial year");
                var firstFinancialObj = financialYearList.FirstOrDefault();
                var lastFinancialObj = financialYearList.OrderByDescending(x =>x.financialYearId).FirstOrDefault();

                var partyBalances = new PartyBalanceSP().PartyBalanceComboViewByLedgerId(firstFinancialObj.fromDate.Value, lastFinancialObj.toDate.Value, ledgerId, "Cr", 0, ""); //debit invoices
                var payments = new PartyBalanceSP().PartyBalanceComboViewByLedgerId(firstFinancialObj.fromDate.Value, lastFinancialObj.toDate.Value, ledgerId, "Dr", 0, "");      //credit modal payments
                var journalAndPurchaseVoucherIds = new List<decimal>() { 6, 28, 5 };
                var ledgerJournalAndPurchases = context.tbl_PartyBalance.AsEnumerable()
                                                        .Where(x => x.ledgerId == ledgerId && journalAndPurchaseVoucherIds.Contains(x.voucherTypeId.Value))
                                                        .ToList();


                //for (var pb = 0; pb < partyBalances.Rows.Count; pb++)
                //{
                //    for (var pi = 0; pi < salesInvoices.Rows.Count; pi++) //for sales_invoice
                //    {
                //        if (partyBalances.Rows[pb]["voucherNo"].ToString() == salesInvoices.Rows[pi]["voucherNo"].ToString()
                //             && Convert.ToDecimal(salesInvoices.Rows[pi]["ledgerId"].ToString()) == ledgerId)
                //        {
                //            applyCreditInvoices.Add(new ApplyCreditInvoices
                //            {
                //                date = Convert.ToDateTime(salesInvoices.Rows[pi]["date"].ToString()),
                //                grandTotal = Convert.ToDecimal(partyBalances.Rows[pb]["balance"].ToString()),
                //                ledgerId = Convert.ToDecimal(salesInvoices.Rows[pi]["ledgerId"].ToString()),
                //                narration = salesInvoices.Rows[pi]["narration"].ToString(),
                //                voucherNo = salesInvoices.Rows[pi]["voucherNo"].ToString(),
                //                voucherTypeId = Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"].ToString()),
                //                //balance = Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"].ToString()),
                //            });
                //        }

                //        ////get journalPartyBalances with invoiceNo for this ledgerId
                //        //var journalApplyBalances = context.tbl_PartyBalance.AsEnumerable()
                //        //                                        .Where(x => x.ledgerId.Value == ledgerId && x.voucherNo == salesInvoices.Rows[pi]["voucherNo"].ToString() && x.voucherTypeId == 6)
                //        //                                        .ToList();
                //        //if(journalApplyBalances.Any())
                //        //{
                //        //    var debitJournalBalance = journalApplyBalances.FirstOrDefault(x => x.referenceType == "New" || x.referenceType == "NEW");
                //        //    if(debitJournalBalance != null)
                //        //    {
                //        //        //get credits same voucherNo as debitJournalBalance.voucherNo 
                //        //        var creditTotal = journalApplyBalances.Where(x => x.voucherNo == debitJournalBalance.voucherNo).Sum(x => x.credit);
                //        //        var journalApplyItem = journalApplyBalances.Where(x => x.referenceType == "New" || x.referenceType == "NEW")
                //        //                                    .Select(x => new ApplyCreditInvoices
                //        //                                    {
                //        //                                        date = x.date.Value,
                //        //                                        grandTotal = x.debit.Value - creditTotal.Value,
                //        //                                        ledgerId = x.ledgerId.Value,
                //        //                                        narration = String.Empty,
                //        //                                        voucherNo = x.voucherNo,
                //        //                                        voucherTypeId = x.voucherTypeId.Value
                //        //                                    })
                //        //                                    .FirstOrDefault();
                //        //        if(journalApplyItem != null)
                //        //        {
                //        //            journalApplyItem.balance = journalApplyItem.grandTotal;

                //        //            if (journalApplyItem != null && journalApplyItem.balance > 0)
                //        //            {
                //        //                var itemExist = applyCreditInvoices
                //        //                    .Where(x => x.voucherTypeId == journalApplyItem.voucherTypeId && x.balance == journalApplyItem.balance && x.voucherNo == journalApplyItem.voucherNo )
                //        //                    //.Where(x => x.date == journalApplyItem.date && x.voucherTypeId == journalApplyItem.voucherTypeId && x.balance == journalApplyItem.balance && x.voucherNo == journalApplyItem.voucherNo )
                //        //                    .FirstOrDefault();
                //        //                if(itemExist == null) applyCreditInvoices.Add(journalApplyItem);
                //        //            }

                //        //        }
                //        //    }

                //        //}



                //    }
                //}

                for (var pb = 0; pb < partyBalances.Rows.Count; pb++)
                {
                    if (Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"]) == 28)// sales invoice type
                    {
                        for (var pi = 0; pi < salesInvoices.Rows.Count; pi++)
                        {
                            if (partyBalances.Rows[pb]["voucherNo"].ToString() == salesInvoices.Rows[pi]["voucherNo"].ToString())
                            {
                                applyCreditInvoices.Add(new ApplyCreditInvoices
                                {
                                    date = Convert.ToDateTime(salesInvoices.Rows[pi]["date"].ToString()),
                                    grandTotal = Convert.ToDecimal(partyBalances.Rows[pb]["balance"].ToString()),
                                    ledgerId = Convert.ToDecimal(salesInvoices.Rows[pi]["ledgerId"].ToString()),
                                    narration = salesInvoices.Rows[pi]["narration"].ToString(),
                                    voucherNo = salesInvoices.Rows[pi]["voucherNo"].ToString(),
                                    voucherTypeId = Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"].ToString()),
                                });
                            }
                        }
                    }
                    else if (Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"]) == 6)//General_Journal
                    {
                        var newJournalItem = ledgerJournalAndPurchases.FirstOrDefault(x => x.referenceType == "NEW" && x.voucherTypeId.Value == 6 && x.voucherNo == partyBalances.Rows[pb]["InvoiceNo"].ToString());
                        applyCreditInvoices.Add(new ApplyCreditInvoices
                        {
                            date = newJournalItem.date.Value,
                            grandTotal = Convert.ToDecimal(partyBalances.Rows[pb]["balance"].ToString()),
                            ledgerId = ledgerId,
                            narration = "",
                            voucherNo = partyBalances.Rows[pb]["InvoiceNo"].ToString(),
                            voucherTypeId = Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"].ToString()),
                        });

                    }
                    else if (Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"]) == 28)//sales invoice
                    {
                        var newPurchaseItem = ledgerJournalAndPurchases.FirstOrDefault(x => x.referenceType == "NEW" && x.voucherTypeId.Value == 28 && x.voucherNo == partyBalances.Rows[pb]["InvoiceNo"].ToString());

                        applyCreditInvoices.Add(new ApplyCreditInvoices
                        {
                            date = newPurchaseItem.date.Value,
                            grandTotal = Convert.ToDecimal(partyBalances.Rows[pb]["balance"].ToString()),
                            ledgerId = ledgerId,
                            narration = "",
                            voucherNo = partyBalances.Rows[pb]["InvoiceNo"].ToString(),
                            voucherTypeId = Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"].ToString()),
                        });
                    }
                    else if (Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"]) == 5)//receipt voucher
                    {
                        var newPurchaseItem = ledgerJournalAndPurchases.FirstOrDefault(x => x.referenceType == "NEW" && x.voucherTypeId.Value == 5 && x.voucherNo == partyBalances.Rows[pb]["InvoiceNo"].ToString());

                        applyCreditInvoices.Add(new ApplyCreditInvoices
                        {
                            date = newPurchaseItem.date.Value,
                            grandTotal = Convert.ToDecimal(partyBalances.Rows[pb]["balance"].ToString()),
                            ledgerId = ledgerId,
                            narration = "",
                            voucherNo = partyBalances.Rows[pb]["InvoiceNo"].ToString(),
                            voucherTypeId = Convert.ToDecimal(partyBalances.Rows[pb]["voucherTypeId"].ToString()),
                        });
                    }


                }

                data.invoices = applyCreditInvoices;
                data.payments = payments;
                response.ResponseCode = 200;
                response.ResponseMessage = "success";
                response.Response = data;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = $"something went wrong. {ex.Message}";
            }
            return response;
        }
        public MatResponse ApplyCredit(ApplyCreditVM input)
        {
            MatResponse response = new MatResponse();
            try
            {
                CreditNoteMasterSP spCreditnoteMaster = new CreditNoteMasterSP();
                CreditNoteMasterInfo infoCreditNoteMaster = new CreditNoteMasterInfo();
                CreditNoteDetailsSP spCreditNoteDetails = new CreditNoteDetailsSP();
                CreditNoteDetailsInfo infoCreditNoteDetails = new CreditNoteDetailsInfo();
                ReceiptMasterInfo infoReceiptMaster = new ReceiptMasterInfo();
                ReceiptDetailsInfo infoReceiptDetails = new ReceiptDetailsInfo();
                ReceiptMasterSP spReceiptMaster = new ReceiptMasterSP();
                ReceiptDetailsSP spReceiptDetails = new ReceiptDetailsSP();
                JournalMasterInfo infoJournalMaster = new JournalMasterInfo();
                JournalDetailsInfo infoJournalDetails = new JournalDetailsInfo();
                JournalMasterSP spJournalMaster = new JournalMasterSP();
                JournalDetailsSP spJournalDetails = new JournalDetailsSP();
                PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
                PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
                HelperClasses helperClasses = new HelperClasses();
                ExchangeRateSP spExchangeRate = new ExchangeRateSP();
                PostingsHelper PostingHelper = new PostingsHelper();
                DataTable dt = new DataTable();
                decimal decSelectedCurrencyRate = 1;
                decimal existingLedgerId = 0;
                bool isRowAffected = false;
                decimal TotalAmount = 0;

                var inputTotal = input.grandTotal;
                var advancePaymentTotal = input.advancePayments.Sum(x => x.amountToApply);
                if (advancePaymentTotal > inputTotal) throw new Exception($"Amount to apply is more than Invoice Amount");


                #region Working region

                bool createNewCreditNote = false;
                DataTable dtAdvancePayments = new DataTable();
                dtAdvancePayments.Columns.Add("voucherTypeId", typeof(string));
                dtAdvancePayments.Columns.Add("display", typeof(string));
                dtAdvancePayments.Columns.Add("voucherNo", typeof(string));
                dtAdvancePayments.Columns.Add("balance", typeof(decimal));
                dtAdvancePayments.Columns.Add("exchangeRateId", typeof(string));
                dtAdvancePayments.Columns.Add("invoiceNo", typeof(string));
                dtAdvancePayments.Columns.Add("AmountToApply", typeof(decimal));

                foreach (var row in input.advancePayments)
                {
                    DataRow dr = dtAdvancePayments.NewRow();
                    dr[0] = row.voucheerTypeId;
                    dr[1] = row.voucherType;
                    dr[2] = row.voucherNo;
                    dr[3] = row.value;
                    dr[4] = row.exchangeRateId;
                    dr[5] = row.invoiceNo;
                    dr[6] = row.amountToApply;
                    dtAdvancePayments.Rows.Add(dr);
                }

                if (dtAdvancePayments.Rows.Count > 0)
                {
                    decimal totalAdvancePaymentToApply = (from t in dtAdvancePayments.AsEnumerable()
                                                          select t.Field<decimal>("AmountToApply")).Sum();
                    decimal totalInvoice = input.grandTotal;

                    foreach (DataRow row in dtAdvancePayments.Rows)
                    {
                        createNewCreditNote = false;
                        decimal voucherTypeId = Convert.ToDecimal(row["voucherTypeId"].ToString());
                        string voucherNo = row["voucherNo"].ToString();
                        decimal balance = Convert.ToDecimal(row["balance"].ToString());
                        decimal amountToApply = Convert.ToDecimal(row["AmountToApply"].ToString());
                        if (balance > amountToApply)
                        {
                            createNewCreditNote = true;
                        }

                        dt = SpPartyBalance.PartyBalanceViewByVoucherNoAndVoucherType(voucherTypeId, voucherNo, DateTime.Now);
                        //decimal decPartyBalanceId = Convert.ToDecimal(dt.Rows[0]["PartyBalanceId"].ToString());

                        TotalAmount = balance > amountToApply ? amountToApply : balance;

                        #region Credit Note
                        if (voucherTypeId == 22) // Credit Note
                        {
                            bool CreditMasterEdit = false;
                            //infoCreditNoteMaster.CreditNoteMasterId = Convert.ToDecimal(dgvCreditNote.SelectedValue.ToString());
                            //infoCreditNoteMaster.CreditNoteMasterId = helperClasses.CreditNoteMasterId(Convert.ToDecimal(dgvCreditNote.SelectedValue));
                            infoCreditNoteMaster.CreditNoteMasterId = helperClasses.CreditNoteMasterId(voucherNo.ToString());
                            infoCreditNoteMaster = spCreditnoteMaster.CreditNoteMasterView(infoCreditNoteMaster.CreditNoteMasterId);

                            infoCreditNoteMaster.VoucherNo = infoCreditNoteMaster.VoucherNo;
                            infoCreditNoteMaster.InvoiceNo = infoCreditNoteMaster.InvoiceNo;
                            infoCreditNoteMaster.SuffixPrefixId = infoCreditNoteMaster.SuffixPrefixId;
                            infoCreditNoteMaster.Date = input.date;
                            infoCreditNoteMaster.Narration = input.narration;
                            infoCreditNoteMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                            infoCreditNoteMaster.VoucherTypeId = infoCreditNoteMaster.VoucherTypeId;
                            infoCreditNoteMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
                            infoCreditNoteMaster.ExtraDate = DateTime.Now;
                            infoCreditNoteMaster.Extra1 = string.Empty;
                            infoCreditNoteMaster.Extra2 = string.Empty;
                            //decTotalDebit = Convert.ToDecimal(txtDebitTotal.Text.Trim());
                            //decTotalCredit = Convert.ToDecimal(txtCreditTotal.Text.Trim());
                            infoCreditNoteMaster.TotalAmount = TotalAmount;
                            CreditMasterEdit = true;
                            isRowAffected = PostingHelper.CreditNoteMasterAddOrEdit(infoCreditNoteMaster, infoCreditNoteDetails, CreditMasterEdit, decSelectedCurrencyRate);

                            if (isRowAffected == true)
                            {
                                dt = spCreditNoteDetails.CreditNoteDetailsViewByMasterId(infoCreditNoteMaster.CreditNoteMasterId);
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    infoCreditNoteDetails.ChequeDate = DateTime.Now;
                                    infoCreditNoteDetails.ChequeNo = string.Empty;
                                    if (Convert.ToDecimal(dt.Rows[i]["debit"].ToString()) == 0)
                                    {
                                        infoCreditNoteDetails.Credit = TotalAmount;
                                        infoCreditNoteDetails.Debit = 0;
                                        infoCreditNoteDetails.LedgerId = input.ledgerId;
                                    }
                                    else
                                    {
                                        infoCreditNoteDetails.Credit = 0;
                                        infoCreditNoteDetails.Debit = TotalAmount;
                                        infoCreditNoteDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                        existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                    }
                                    infoCreditNoteDetails.CreditNoteDetailsId = Convert.ToDecimal(dt.Rows[i]["CreditNoteDetailsId"].ToString());
                                    infoCreditNoteDetails.CreditNoteMasterId = infoCreditNoteMaster.CreditNoteMasterId;
                                    infoCreditNoteDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
                                    infoCreditNoteDetails.Extra1 = string.Empty;
                                    infoCreditNoteDetails.Extra2 = string.Empty;
                                    infoCreditNoteDetails.ExtraDate = DateTime.Now;
                                    CreditMasterEdit = false;
                                    //------------------Currency conversion------------------//
                                    decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoCreditNoteDetails.ExchangeRateId);
                                    PostingHelper.JournalCreditNoteDetailsAddOrEdit(infoCreditNoteMaster, infoCreditNoteDetails, CreditMasterEdit, decSelectedCurrencyRate, input.voucherNo, input.vouchertypeId);
                                }
                            }
                            #endregion

                            #region Create new credit note
                            if (createNewCreditNote)
                            {
                                SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                                SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                                TransactionsGeneralFill obj = new TransactionsGeneralFill();
                                CreditNoteMasterSP spCreditNoteMaster = new CreditNoteMasterSP();
                                string strNewCreditNoteVoucherNo = string.Empty;
                                string strNewCreditNoteInvoiceNo = string.Empty;

                                if (strNewCreditNoteVoucherNo == string.Empty)
                                {
                                    strNewCreditNoteVoucherNo = "0"; //strMax;
                                }
                                //===================================================================================================================//

                                VoucherTypeSP spVoucherType = new VoucherTypeSP();
                                bool isAutomaticForCreditNote = spVoucherType.CheckMethodOfVoucherNumbering(22);
                                if (isAutomaticForCreditNote)
                                {
                                    strNewCreditNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(22, Convert.ToDecimal(strNewCreditNoteVoucherNo), DateTime.Now, "CreditNoteMaster");
                                    if (Convert.ToDecimal(strNewCreditNoteVoucherNo) != spCreditNoteMaster.CreditNoteMasterGetMaxPlusOne(22))
                                    {
                                        strNewCreditNoteVoucherNo = spCreditNoteMaster.CreditNoteMasterGetMax(22).ToString();
                                        strNewCreditNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(22, Convert.ToDecimal(strNewCreditNoteVoucherNo), DateTime.Now, "CreditNoteMaster");
                                        if (spCreditNoteMaster.CreditNoteMasterGetMax(22).ToString() == "0")
                                        {
                                            strNewCreditNoteVoucherNo = "0";
                                            strNewCreditNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(22, Convert.ToDecimal(strNewCreditNoteVoucherNo), DateTime.Now, "CreditNoteMaster");
                                        }
                                    }
                                    infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(22, DateTime.Now);
                                    strPrefix = infoSuffixPrefix.Prefix;
                                    strSuffix = infoSuffixPrefix.Suffix;
                                    strNewCreditNoteInvoiceNo = strPrefix + strNewCreditNoteVoucherNo + strSuffix;
                                    //txtVoucherNo.Text = strInvoiceNo;
                                    //txtVoucherNo.ReadOnly = true;
                                }
                                else
                                {
                                    //txtVoucherNo.ReadOnly = false;
                                    //txtVoucherNo.Text = string.Empty;
                                    strNewCreditNoteVoucherNo = input.voucherNo + "_Cr";
                                    strNewCreditNoteInvoiceNo = strNewCreditNoteVoucherNo;
                                }
                                // ============================ credit note master add =========================== //
                                infoCreditNoteMaster.VoucherNo = strNewCreditNoteVoucherNo;
                                infoCreditNoteMaster.InvoiceNo = strNewCreditNoteVoucherNo;
                                infoCreditNoteMaster.SuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                                infoCreditNoteMaster.Date = input.date;
                                infoCreditNoteMaster.Narration = input.narration;
                                infoCreditNoteMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                                infoCreditNoteMaster.VoucherTypeId = 22;
                                infoCreditNoteMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
                                infoCreditNoteMaster.Extra1 = string.Empty;
                                infoCreditNoteMaster.Extra2 = string.Empty;
                                infoCreditNoteMaster.TotalAmount = balance - amountToApply;
                                decimal decCreditNoteMasterId = spCreditNoteMaster.CreditNoteMasterAdd(infoCreditNoteMaster);

                                // ========================== CreditNote Details Add =============================== //
                                spCreditNoteDetails = new CreditNoteDetailsSP();
                                LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                                decimal decCreditNoteDetailsId = 0;
                                decimal decLedgerId = 0;
                                decimal decDebit = 0;
                                decimal decCredit = 0;
                                try
                                {
                                    infoCreditNoteDetails.CreditNoteMasterId = decCreditNoteMasterId;
                                    infoCreditNoteDetails.ExchangeRateId = 1;
                                    infoCreditNoteDetails.ExtraDate = DateTime.Now;
                                    infoCreditNoteDetails.Extra1 = string.Empty;
                                    infoCreditNoteDetails.Extra2 = string.Empty;
                                    infoCreditNoteDetails.LedgerId = input.ledgerId;
                                    decLedgerId = infoCreditNoteDetails.LedgerId;
                                    infoCreditNoteDetails.Debit = 0;
                                    infoCreditNoteDetails.Credit = balance - amountToApply;
                                    decDebit = infoCreditNoteDetails.Debit * decSelectedCurrencyRate;
                                    decCredit = infoCreditNoteDetails.Credit * decSelectedCurrencyRate;
                                    infoCreditNoteDetails.ChequeNo = string.Empty;
                                    infoCreditNoteDetails.ChequeDate = DateTime.Now;
                                    //-------------------- credit leg ---------------------------- //
                                    decimal decDebitDetailsId = spCreditNoteDetails.CreditNoteDetailsAdd(infoCreditNoteDetails);

                                    infoCreditNoteDetails.LedgerId = 1;
                                    decLedgerId = infoCreditNoteDetails.LedgerId;
                                    infoCreditNoteDetails.Debit = balance - amountToApply;
                                    infoCreditNoteDetails.Credit = 0;
                                    decDebit = infoCreditNoteDetails.Debit * decSelectedCurrencyRate;
                                    decCredit = infoCreditNoteDetails.Credit * decSelectedCurrencyRate;

                                    // -------------------- debit leg --------------------------- //
                                    decimal decCreditDetailsId = spCreditNoteDetails.CreditNoteDetailsAdd(infoCreditNoteDetails);

                                    // -------------------------- party balance add --------------------------- //
                                    PartyBalanceSP spPartyBalance = new PartyBalanceSP();
                                    PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                                    InfopartyBalance.CreditPeriod = 0;//
                                    InfopartyBalance.Date = input.date;
                                    InfopartyBalance.LedgerId = input.ledgerId;
                                    InfopartyBalance.ReferenceType = "OnAccount";
                                    InfopartyBalance.AgainstInvoiceNo = "0";
                                    InfopartyBalance.AgainstVoucherNo = "0";
                                    InfopartyBalance.AgainstVoucherTypeId = 0;
                                    InfopartyBalance.VoucherTypeId = 22;
                                    InfopartyBalance.InvoiceNo = strNewCreditNoteVoucherNo;
                                    InfopartyBalance.VoucherNo = strNewCreditNoteVoucherNo;
                                    InfopartyBalance.Credit = balance - amountToApply;
                                    InfopartyBalance.Debit = 0;
                                    InfopartyBalance.ExchangeRateId = 1;
                                    InfopartyBalance.Extra1 = string.Empty;
                                    InfopartyBalance.Extra2 = string.Empty;
                                    InfopartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                    PostingHelper.PartyBalanceAdd(InfopartyBalance);

                                    // =============================== ledger posting ==================================== //
                                    infoLedgerPosting.ChequeDate = DateTime.Now;
                                    infoLedgerPosting.ChequeNo = string.Empty;
                                    infoLedgerPosting.Credit = balance - amountToApply;
                                    infoLedgerPosting.Date = input.date;
                                    infoLedgerPosting.Debit = 0;
                                    infoLedgerPosting.DetailsId = decCreditDetailsId;
                                    infoLedgerPosting.Extra1 = string.Empty;
                                    infoLedgerPosting.Extra2 = string.Empty;
                                    infoLedgerPosting.ExtraDate = DateTime.Now;
                                    infoLedgerPosting.InvoiceNo = strNewCreditNoteVoucherNo;
                                    infoLedgerPosting.LedgerId = input.ledgerId;
                                    infoLedgerPosting.VoucherNo = strNewCreditNoteVoucherNo;
                                    infoLedgerPosting.VoucherTypeId = 22;
                                    infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                    // --------------- credit leg -------------------------------------------------- //
                                    //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                                    // --------------- debit leg --------------------------------------------------- //
                                    infoLedgerPosting.Credit = 0;
                                    infoLedgerPosting.Date = input.date;
                                    infoLedgerPosting.Debit = balance - amountToApply;
                                    infoLedgerPosting.DetailsId = decDebitDetailsId;
                                    infoLedgerPosting.Extra1 = string.Empty;
                                    infoLedgerPosting.LedgerId = existingLedgerId;
                                    infoLedgerPosting.VoucherNo = strNewCreditNoteVoucherNo;
                                    infoLedgerPosting.VoucherTypeId = 22;
                                    //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                                }
                                catch (Exception ex) { }
                                createNewCreditNote = false;
                                #endregion
                            }
                        }
                        #region Receipt Voucher
                        if (voucherTypeId == 5) // Receipt voucher
                        {
                            bool ReceiptMasterEdit = false;
                            string comStr = string.Format("SELECT receiptMasterId FROM tbl_ReceiptMaster WHERE " +
                                "voucherNo = '{0}' ", voucherNo.ToString());
                            infoReceiptMaster.ReceiptMasterId = helperClasses.GetMasterId(comStr);     //.CreditNoteMasterId = helperClasses.CreditNoteMasterId(Convert.ToDecimal(voucherNo));
                            infoReceiptMaster = spReceiptMaster.ReceiptMasterView(infoReceiptMaster.ReceiptMasterId);   // ( spCreditnoteMaster.CreditNoteMasterView(infoCreditNoteMaster.CreditNoteMasterId);

                            //infoReceiptMaster.VoucherNo = infoReceiptMaster.VoucherNo;
                            infoReceiptMaster.VoucherNo = row["voucherNo"].ToString();
                            infoReceiptMaster.InvoiceNo = infoReceiptMaster.InvoiceNo;
                            infoReceiptMaster.SuffixPrefixId = infoReceiptMaster.SuffixPrefixId;
                            infoReceiptMaster.Date = input.date;
                            infoReceiptMaster.Narration = input.narration;
                            infoReceiptMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                            infoReceiptMaster.VoucherTypeId = infoReceiptMaster.VoucherTypeId;
                            infoReceiptMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
                            infoReceiptMaster.ExtraDate = DateTime.Now;
                            infoReceiptMaster.Extra1 = string.Empty;
                            infoReceiptMaster.Extra2 = string.Empty;
                            //decTotalDebit = Convert.ToDecimal(txtDebitTotal.Text.Trim());
                            //decTotalCredit = Convert.ToDecimal(txtCreditTotal.Text.Trim());
                            infoReceiptMaster.TotalAmount = TotalAmount;
                            ReceiptMasterEdit = true;
                            isRowAffected = PostingHelper.ReceiptMasterEdit(infoReceiptMaster, decSelectedCurrencyRate);

                            if (isRowAffected == true)
                            {
                                dt = spReceiptDetails.ReceiptDetailsViewByMasterId(infoReceiptMaster.ReceiptMasterId);  
                                //spCreditNoteDetails.CreditNoteDetailsViewByMasterId(infoCreditNoteMaster.CreditNoteMasterId);
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    infoReceiptDetails.Amount = amountToApply;
                                    infoReceiptDetails.ChequeDate = DateTime.Now;
                                    infoReceiptDetails.ChequeNo = string.Empty;
                                    infoReceiptDetails.VoucherNo = infoReceiptDetails.VoucherNo; //This is where the changes are made
                                    //infoReceiptDetails.LedgerId = Convert.ToDecimal(cmbCashOrParty.SelectedValue.ToString());
                                    infoReceiptDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                    existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                    infoReceiptDetails.ReceiptDetailsId = Convert.ToDecimal(dt.Rows[i]["ReceiptDetailsId"].ToString());
                                    infoReceiptDetails.ReceiptMasterId = infoReceiptMaster.ReceiptMasterId;
                                    infoReceiptDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
                                    infoReceiptDetails.Extra1 = string.Empty;
                                    infoReceiptDetails.Extra2 = string.Empty;
                                    infoReceiptDetails.ExtraDate = DateTime.Now;
                                    ReceiptMasterEdit = false;
                                    //------------------Currency conversion------------------//
                                    decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoReceiptDetails.ExchangeRateId);
                                    PostingHelper.ReceiptDetailsEdit(infoReceiptMaster, infoReceiptDetails, ReceiptMasterEdit, decSelectedCurrencyRate, input.voucherNo, input.vouchertypeId);
                                }
                            }
                        }
                        #endregion
                        #region Journal Voucher
                        if (voucherTypeId == 6) // Journal vouchers
                        {
                            bool JournalMasterEdit = false;
                            string comStr = string.Format("SELECT journalMasterId FROM tbl_JournalMaster WHERE voucherNo = '{0}' ", voucherNo.ToString());
                            infoJournalMaster.JournalMasterId = helperClasses.GetMasterId(comStr);
                            infoJournalMaster = spJournalMaster.JournalMasterView(infoJournalMaster.JournalMasterId);   
                            infoJournalMaster.Date = Convert.ToDateTime(input.date);
                            infoJournalMaster.Narration = input.narration;
                            infoJournalMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                            infoJournalMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
                            infoJournalMaster.ExtraDate = DateTime.Now;
                            infoJournalMaster.TotalAmount = TotalAmount;
                            JournalMasterEdit = true;
                            bool isRowEdited = PostingHelper.JournalMasterEdit(infoJournalMaster, decSelectedCurrencyRate);

                            if (isRowEdited == true)
                            {
                                dt = new DataTable();
                                dt = spJournalDetails.JournalDetailsViewByMasterId(infoJournalMaster.JournalMasterId);    // spCreditNoteDetails.CreditNoteDetailsViewByMasterId(infoCreditNoteMaster.CreditNoteMasterId);
                                for (int i = 0; i < dt.Rows.Count; i++)
                                {
                                    if (Convert.ToDecimal(dt.Rows[i]["debit"].ToString()) == 0)
                                    {
                                        infoJournalDetails.Credit = TotalAmount;
                                        infoJournalDetails.Debit = 0;
                                        infoJournalDetails.LedgerId = Convert.ToDecimal(input.ledgerId);
                                    }
                                    else
                                    {
                                        infoJournalDetails.Credit = 0;
                                        infoJournalDetails.Debit = TotalAmount;
                                        infoJournalDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                        existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                    }
                                    infoJournalDetails.ChequeDate = Convert.ToDateTime(input.date);
                                    infoJournalDetails.ChequeNo = string.Empty;
                                    infoJournalDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                    existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                    infoJournalDetails.JournalMasterId = infoJournalMaster.JournalMasterId;
                                    infoJournalDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
                                    infoJournalDetails.Extra1 = string.Empty;
                                    infoJournalDetails.Extra2 = string.Empty;
                                    infoJournalDetails.Memo = dt.Rows[i]["Memo"].ToString();
                                    infoJournalDetails.ExtraDate = DateTime.Now;
                                    JournalMasterEdit = false;
                                    //------------------Currency conversion------------------//
                                    decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoJournalDetails.ExchangeRateId);
                                    /*
                                     *  1. using ledgerId, get accountLedgerEntity
                                     *  2. fetch accountgroup for the ledger 
                                     *  3. if accountLedger.accountgroupid is not account payable id, set false, else set true || for 'Account Payables' | 'Account Receivables'
                                     */
                                    var accountLedgerEntity = context.tbl_AccountLedger.AsEnumerable().FirstOrDefault(x => x.ledgerId == input.ledgerId);
                                    if (accountLedgerEntity == null) throw new Exception($"Error fetching Account Ledger with ledger id: {input.ledgerId}");
                                    var ledgerAccountGroup = context.tbl_AccountGroup.AsEnumerable().FirstOrDefault(x => x.accountGroupId == accountLedgerEntity.accountGroupId);
                                    if (ledgerAccountGroup == null) throw new Exception($"Error fetching Account Group for  Ledger with ledger id: {input.ledgerId}");
                                    var isPayable = ledgerAccountGroup.accountGroupName.Trim() == "Account Payables" ? true : false;

                                    PostingHelper.JournalDetailsEdit(infoJournalMaster, infoJournalDetails, isPayable, decSelectedCurrencyRate, input.voucherNo, input.vouchertypeId);
                                }
                            }
                        }
                        #endregion
                        #region Create new credit note
                        if (createNewCreditNote)
                        {
                            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                            TransactionsGeneralFill obj = new TransactionsGeneralFill();
                            CreditNoteMasterSP spCreditNoteMaster = new CreditNoteMasterSP();
                            string strNewCreditNoteVoucherNo = string.Empty;
                            string strNewCreditNoteInvoiceNo = string.Empty;

                            if (strNewCreditNoteVoucherNo == string.Empty)
                            {
                                strNewCreditNoteVoucherNo = "0"; //strMax;
                            }
                            //===================================================================================================================//

                            VoucherTypeSP spVoucherType = new VoucherTypeSP();
                            bool isAutomaticForCreditNote = spVoucherType.CheckMethodOfVoucherNumbering(22);
                            if (isAutomaticForCreditNote)
                            {
                                strNewCreditNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(22, Convert.ToDecimal(strNewCreditNoteVoucherNo), DateTime.Now, "DebitNoteMaster");
                                if (Convert.ToDecimal(strNewCreditNoteVoucherNo) != spCreditNoteMaster.CreditNoteMasterGetMaxPlusOne(22))
                                {
                                    strNewCreditNoteVoucherNo = spCreditNoteMaster.CreditNoteMasterGetMax(22).ToString();
                                    strNewCreditNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(22, Convert.ToDecimal(strNewCreditNoteVoucherNo), DateTime.Now, "DebitNoteMaster");
                                    if (spCreditNoteMaster.CreditNoteMasterGetMax(22).ToString() == "0")
                                    {
                                        strNewCreditNoteVoucherNo = "0";
                                        strNewCreditNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(22, Convert.ToDecimal(strNewCreditNoteVoucherNo), DateTime.Now, "DebitNoteMaster");
                                    }
                                }
                                infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(22, DateTime.Now);
                                strPrefix = infoSuffixPrefix.Prefix;
                                strSuffix = infoSuffixPrefix.Suffix;
                                strNewCreditNoteInvoiceNo = strPrefix + strNewCreditNoteVoucherNo + strSuffix;
                                //txtVoucherNo.Text = strInvoiceNo;
                                //txtVoucherNo.ReadOnly = true;
                            }
                            else
                            {
                                //txtVoucherNo.ReadOnly = false;
                                //txtVoucherNo.Text = string.Empty;
                                strNewCreditNoteVoucherNo = input.voucherNo + "_Cr";
                                strNewCreditNoteInvoiceNo = strNewCreditNoteVoucherNo;
                            }
                            // ============================ credit note master add =========================== //
                            infoCreditNoteMaster.VoucherNo = strNewCreditNoteVoucherNo;
                            infoCreditNoteMaster.InvoiceNo = strNewCreditNoteVoucherNo;
                            infoCreditNoteMaster.SuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                            infoCreditNoteMaster.Date = Convert.ToDateTime(input.date);
                            infoCreditNoteMaster.Narration = input.narration;
                            infoCreditNoteMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                            infoCreditNoteMaster.VoucherTypeId = 22;
                            infoCreditNoteMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
                            infoCreditNoteMaster.Extra1 = string.Empty;
                            infoCreditNoteMaster.Extra2 = string.Empty;
                            infoCreditNoteMaster.TotalAmount = balance - amountToApply;
                            decimal decCreditNoteMasterId = spCreditNoteMaster.CreditNoteMasterAdd(infoCreditNoteMaster);

                            // ========================== CreditNote Details Add =============================== //
                            spCreditNoteDetails = new CreditNoteDetailsSP();
                            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                            decimal decCreditNoteDetailsId = 0;
                            decimal decLedgerId = 0;
                            decimal decDebit = 0;
                            decimal decCredit = 0;
                            try
                            {
                                infoCreditNoteDetails.CreditNoteMasterId = decCreditNoteMasterId;
                                infoCreditNoteDetails.ExchangeRateId = 1;
                                infoCreditNoteDetails.ExtraDate = DateTime.Now;
                                infoCreditNoteDetails.Extra1 = string.Empty;
                                infoCreditNoteDetails.Extra2 = string.Empty;
                                infoCreditNoteDetails.LedgerId = input.ledgerId;
                                decLedgerId = infoCreditNoteDetails.LedgerId;
                                infoCreditNoteDetails.Debit = 0;
                                infoCreditNoteDetails.Credit = balance - amountToApply;
                                decDebit = infoCreditNoteDetails.Debit * decSelectedCurrencyRate;
                                decCredit = infoCreditNoteDetails.Credit * decSelectedCurrencyRate;
                                infoCreditNoteDetails.ChequeNo = string.Empty;
                                infoCreditNoteDetails.ChequeDate = input.date;
                                //-------------------- credit leg ---------------------------- //
                                decimal decDebitDetailsId = spCreditNoteDetails.CreditNoteDetailsAdd(infoCreditNoteDetails);

                                infoCreditNoteDetails.LedgerId = 1;
                                decLedgerId = infoCreditNoteDetails.LedgerId;
                                infoCreditNoteDetails.Debit = balance - amountToApply;
                                infoCreditNoteDetails.Credit = 0;
                                decDebit = infoCreditNoteDetails.Debit * decSelectedCurrencyRate;
                                decCredit = infoCreditNoteDetails.Credit * decSelectedCurrencyRate;

                                // -------------------- debit leg --------------------------- //
                                decimal decCreditDetailsId = spCreditNoteDetails.CreditNoteDetailsAdd(infoCreditNoteDetails);

                                // -------------------------- party balance add --------------------------- //
                                PartyBalanceSP spPartyBalance = new PartyBalanceSP();
                                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                                InfopartyBalance.CreditPeriod = 0;//
                                InfopartyBalance.Date = input.date;
                                InfopartyBalance.LedgerId = input.ledgerId;
                                InfopartyBalance.ReferenceType = "OnAccount";
                                InfopartyBalance.AgainstInvoiceNo = "0";
                                InfopartyBalance.AgainstVoucherNo = "0";
                                InfopartyBalance.AgainstVoucherTypeId = 0;
                                InfopartyBalance.VoucherTypeId = 22;
                                InfopartyBalance.InvoiceNo = strNewCreditNoteVoucherNo;
                                InfopartyBalance.VoucherNo = strNewCreditNoteVoucherNo;
                                InfopartyBalance.Credit = balance - amountToApply;
                                InfopartyBalance.Debit = 0;
                                InfopartyBalance.ExchangeRateId = 1;
                                InfopartyBalance.Extra1 = string.Empty;
                                InfopartyBalance.Extra2 = string.Empty;
                                InfopartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                PostingHelper.PartyBalanceAdd(InfopartyBalance);

                                // =============================== ledger posting ==================================== //
                                infoLedgerPosting.ChequeDate = DateTime.Now;
                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.Credit = balance - amountToApply;
                                infoLedgerPosting.Date = input.date;
                                infoLedgerPosting.Debit = 0;
                                infoLedgerPosting.DetailsId = decCreditDetailsId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.InvoiceNo = strNewCreditNoteVoucherNo;
                                infoLedgerPosting.LedgerId = input.ledgerId;
                                infoLedgerPosting.VoucherNo = strNewCreditNoteVoucherNo;
                                infoLedgerPosting.VoucherTypeId = 22;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                // --------------- credit leg -------------------------------------------------- //
                                //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                                // --------------- debit leg --------------------------------------------------- //
                                infoLedgerPosting.Credit = 0;
                                infoLedgerPosting.Date = input.date;
                                infoLedgerPosting.Debit = balance - amountToApply;
                                infoLedgerPosting.DetailsId = decDebitDetailsId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.LedgerId = existingLedgerId;
                                infoLedgerPosting.VoucherNo = strNewCreditNoteVoucherNo;
                                infoLedgerPosting.VoucherTypeId = 22;
                                //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                            }
                            catch (Exception ex)
                            {
                                response.ResponseCode = 500;
                                response.ResponseMessage = "something went wrong";
                            }
                           
                        } 
                        #endregion
                    }

                    response.ResponseCode = 200;
                    response.ResponseMessage = "success";
                    #endregion
                }
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = $"something went wrong. {ex.Message}";
            }
            return response;
        }

        public async Task SwapVoucherNoAndAgainstVoucherNo()
        {
            try
            {
                /*
                 * 	1. from partybalance where 
		                    - referenceType = 'Against' and voucherTypeId = 28 as pb
	                    foreach item in pb 
		                    2. from receiptMaster where as rm
			                    - 	and pb.credit = rm.totalAmount and pb.voucherNo = rm.voucherNo;
		                    3. update pb 
			                    - var a = pb.voucherNo
			                    - var b = pb.againstVoucherNo
			                    - set pb.voucherNo and pb.invoiceNo to b 
			                    - set pb.againstVoucherNo and pb.againstInvoiceNo to a

                 */
                var updateList = new List<DBModel.tbl_PartyBalance>();
                var targetPartyBalanceIds = new List<decimal>() { 120796, 120804, 120810, 90541, 120818, 90539, 130953, 140991, 140999, 100575, 100581, 100585, 100588, 110695, 120777, 120786, 120790 };
                var partyBalanceList = context.tbl_PartyBalance.AsEnumerable()
                                                    .Where(x => x.referenceType == "Against" && x.voucherTypeId == 28)
                                                    //.Where(x => x.referenceType == "Against" && x.voucherTypeId == 28 && targetPartyBalanceIds.Contains(x.partyBalanceId))
                                                    .ToList();
                if(partyBalanceList.Any())
                {
                    foreach (var balanceItem in partyBalanceList)
                    {
                        //if (balanceItem.partyBalanceId != 100588) continue;
                        var receiptMasterItem = context.tbl_ReceiptMaster.AsEnumerable()
                                                       .Where(x => x.totalAmount == balanceItem.credit && x.voucherNo == balanceItem.voucherNo)
                                                       .FirstOrDefault();
                        var salesMasterItem = context.tbl_SalesMaster.AsEnumerable()
                                                       .Where(x => x.ledgerId == balanceItem.ledgerId && x.voucherNo == balanceItem.againstVoucherNo)
                                                       .FirstOrDefault();
                        if (receiptMasterItem != null && salesMasterItem != null)
                        {
                            var pbVoucherNo = balanceItem.voucherNo;
                            var pbAgainstVoucherNo = balanceItem.againstVoucherNo;

                            balanceItem.voucherNo = pbAgainstVoucherNo;
                            balanceItem.invoiceNo = pbAgainstVoucherNo;
                            balanceItem.againstVoucherNo = pbVoucherNo;
                            balanceItem.againstInvoiceNo = pbVoucherNo;
                            updateList.Add(balanceItem);
                            context.Entry<DBModel.tbl_PartyBalance>(balanceItem).State = (System.Data.Entity.EntityState)EntityState.Modified;
                            await context.SaveChangesAsync();
                        }
                    }
                }
                //updateList.Count() == -- 205 

            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
            }

        }
        public async Task UpdatePartyBalanceWithZeroDebit()
        {
            try
            {
                /*
	                1. from partybalance where 
		                - referenceType in ('New', 'NEW') and voucherTypeId = 28 and debit = 0 as pb
	                foreach item in pb 
		                2. from salesMaster where as sm
			                - 	pb.voucherNo = sm.voucherNo;
		                3. update pb 
			                - set set pb.debit = sm.grandTotal 

                 */
                var updateList = new List<DBModel.tbl_PartyBalance>();
                var referenceOptions = new List<string>() { "New", "NEW" };
                var zeroDebitPartyBalanceList = context.tbl_PartyBalance.AsEnumerable()
                                                    .Where(x => referenceOptions.Contains(x.referenceType) && x.voucherTypeId == 28 && x.debit == 0)
                                                    .ToList();
                if(zeroDebitPartyBalanceList.Any())
                {
                    foreach (var balanceItem in zeroDebitPartyBalanceList)
                    {
                        var salesMasterItem = context.tbl_SalesMaster.AsEnumerable()
                                                       .Where(x => x.voucherNo == balanceItem.voucherNo)
                                                       .FirstOrDefault();
                        if (salesMasterItem != null)
                        {

                            balanceItem.debit = salesMasterItem.grandTotal;
                            updateList.Add(balanceItem);
                            context.Entry<DBModel.tbl_PartyBalance>(balanceItem).State = (System.Data.Entity.EntityState)EntityState.Modified;
                            await context.SaveChangesAsync();
                        }
                    }
                }
                //updateList.Count() == -- 175

            }
            catch (Exception ex)
            {
                Console.WriteLine($"{ex.Message}");
            }

        }

    }
    public class AdvancePaymentVM
    {
        public decimal voucheerTypeId { get; set; }
        public string voucherType { get; set; }
        public string voucherNo { get; set; }
        public decimal value { get; set; }
        public decimal exchangeRateId { get; set; }
        public string invoiceNo { get; set; }
        public decimal amountToApply { get; set; }
    }
    public class ApplyCreditVM
    {
        public decimal ledgerId { get; set; }
        public DateTime date { get; set; }
        public decimal grandTotal { get; set; }
        public string voucherNo { get; set; }
        public string narration { get; set; }
        public decimal vouchertypeId { get; set; }
        public List<AdvancePaymentVM> advancePayments { get; set; }
    }
    public class ApplyCreditInvoices
    {
        public decimal ledgerId { get; set; }
        public DateTime date { get; set; }
        public decimal grandTotal { get; set; }
        public string voucherNo { get; set; }
        public string narration { get; set; }
        public decimal voucherTypeId { get; set; }
        public decimal balance { get; set; }
    }
}
