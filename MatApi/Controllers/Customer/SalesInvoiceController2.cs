﻿using MatApi.DBModel;
using MatApi.Models;
using MATFinancials;
using MATFinancials.DAL;
using MATFinancials.Other;
//using Microsoft.SqlServer.Management.Smo;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SalesInvoiceController2 : ApiController
    {
        decimal ProjectId;
        decimal CategoryId;
        decimal decSalesDetailsId;
        List<decimal> listofDetailsId = new List<decimal>();
        decimal TotalAmount;
        string strPrefix;
        string strSuffix;
        decimal ExchangeRateId = 1;
        string comStr = "";
        string strVoucherNo = string.Empty;
        string strCreditNoteMasterTableName = "CreditNoteMaster";

        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public SalesInvoiceController2()
        {
            
        }

        [HttpPost]
        public HttpResponseMessage CreateInvoice(CreateInvoiceVM input)
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            SalesMasterSP spSalesMaster1 = new SalesMasterSP();
            TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
            if (strVoucherNo == string.Empty)
            {
                strVoucherNo = "0";
            }
            strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(28, Convert.ToDecimal(strVoucherNo), input.Date, "SalesMaster");
            if (Convert.ToDecimal(strVoucherNo) != (spSalesMaster1.SalesMasterVoucherMax(28)))
            {
                strVoucherNo = spSalesMaster1.SalesMasterVoucherMax(28).ToString();
                strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(28, Convert.ToDecimal(strVoucherNo), input.Date, "SalesMaster");
                if (spSalesMaster1.SalesMasterVoucherMax(28) == 0)
                {
                    strVoucherNo = "0";
                    strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(28, Convert.ToDecimal(strVoucherNo), input.Date, "SalesMaster");
                }
            }

            decimal totalAdvance = 0;
            decimal totalInvoiceCreated = Convert.ToDecimal(input.GrandTotal);
            if (input.IsAdvancePayment)
            {
                foreach (var advancePay in input.AdvancePayments)
                {
                    if (advancePay.IsApply)
                    {
                        totalAdvance += Convert.ToDecimal(advancePay.Balance);
                    }
                }
                if (totalAdvance > totalInvoiceCreated)
                {
                    return Request.CreateResponse(HttpStatusCode.OK, "Applied amount is greater than bill amount");
                }
            }

            SalesMasterSP spSalesMaster = new SalesMasterSP();
            SalesDetailsSP spSalesDetails = new SalesDetailsSP();
            StockPostingInfo infoStockPosting = new StockPostingInfo();
            SalesMasterInfo InfoSalesMaster = new SalesMasterInfo();
            SalesDetailsInfo InfoSalesDetails = new SalesDetailsInfo();
            var spStockPosting = new MatApi.Services.StockPostingSP();
            AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            SalesBillTaxInfo infoSalesBillTax = new SalesBillTaxInfo();
            SalesBillTaxSP spSalesBillTax = new SalesBillTaxSP();
            UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
            HelperClasses helperClasses = new HelperClasses();
            try
            {
                InfoSalesMaster.AdditionalCost = Convert.ToDecimal(input.SalesMasterInfo.AdditionalCost);
                // Urefe checked if bill discount is convertible to decimal before attempting to convert it
                // InfoSalesMaster.BillDiscount = Convert.ToDecimal(txtBillDiscount.Text.Trim()); // old implementation      

                InfoSalesMaster.BillDiscount = input.SalesMasterInfo.BillDiscount;
                InfoSalesMaster.CreditPeriod = input.SalesMasterInfo.CreditPeriod;
                InfoSalesMaster.CustomerName = input.SalesMasterInfo.CustomerName;
                InfoSalesMaster.Date = Convert.ToDateTime(input.SalesMasterInfo.Date);
                InfoSalesMaster.ExchangeRateId = input.SalesMasterInfo.ExchangeRateId;
                InfoSalesMaster.EmployeeId = input.SalesMasterInfo.EmployeeId;
                InfoSalesMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                InfoSalesMaster.GrandTotal = input.SalesMasterInfo.GrandTotal;
                InfoSalesMaster.LedgerId = input.SalesMasterInfo.LedgerId;
                InfoSalesMaster.VoucherTypeId = 28; //change this later when voucher types are configured for online portal
                bool isAutomatic = true;
                decimal decSalseInvoiceSuffixPrefixId = 0;

                if (isAutomatic)
                {
                    InfoSalesMaster.InvoiceNo = input.SalesMasterInfo.InvoiceNo;
                    InfoSalesMaster.VoucherNo = input.InvoiceNumber;
                    InfoSalesMaster.SuffixPrefixId = decSalseInvoiceSuffixPrefixId;
                }
                else
                {
                    InfoSalesMaster.InvoiceNo = input.InvoiceNumber;
                    InfoSalesMaster.VoucherNo = input.InvoiceNumber;
                    InfoSalesMaster.SuffixPrefixId = 0;
                }
                if (input.SalesMode == "Against SalesOrder")
                {
                    InfoSalesMaster.OrderMasterId = Convert.ToDecimal(input.SalesModeOrderNo);
                }
                else
                {
                    InfoSalesMaster.OrderMasterId = 0;
                }
                if (input.SalesMode == "Against Delivery Note")
                {
                    InfoSalesMaster.DeliveryNoteMasterId = Convert.ToDecimal(input.SalesModeOrderNo);
                }
                else
                {
                    InfoSalesMaster.DeliveryNoteMasterId = 0;
                }
                if (input.SalesMode == "Against Quotation")
                {
                    InfoSalesMaster.QuotationMasterId = Convert.ToDecimal(input.SalesModeOrderNo);
                }
                else
                {
                    InfoSalesMaster.QuotationMasterId = 0;
                }
                InfoSalesMaster.Narration = input.SalesMasterInfo.Narration;
                InfoSalesMaster.PricinglevelId = Convert.ToDecimal(input.SalesMasterInfo.PricinglevelId);
                InfoSalesMaster.SalesAccount = 10;  //Convert.ToDecimal(cmbSalesAccount.SelectedValue.ToString());         Now reports to Sales Account, to disable the dropdownlist for SalesAccount// to be modified.........reassigned down below
                InfoSalesMaster.TotalAmount = Convert.ToDecimal(input.SalesMasterInfo.TotalAmount);
                if (input.IsTax)
                {
                    InfoSalesMaster.TaxAmount = Convert.ToDecimal(input.TotalTaxAmount);
                }
                else
                {
                    InfoSalesMaster.TaxAmount = 0;
                }
                InfoSalesMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                InfoSalesMaster.LrNo = "NIL";
                InfoSalesMaster.TransportationCompany = "NIL";
                InfoSalesMaster.POS = false;
                InfoSalesMaster.CounterId = 0;
                InfoSalesMaster.ExtraDate = DateTime.Now;
                InfoSalesMaster.Extra1 = string.Empty;
                InfoSalesMaster.Extra2 = string.Empty;
                InfoSalesMaster.POSTellerNo = string.Empty;
                InfoSalesMaster.transactionDay = helperClasses.GetCurrentDay(MATFinancials.PublicVariables._decCurrentUserId);
                decimal decSalesMasterId = spSalesMaster.SalesMasterAdd(InfoSalesMaster);           // updates sales master table
                
                InfoSalesDetails.SalesMasterId = decSalesMasterId;      // assigns SalesMasterDetailsID
                InfoSalesDetails.ExtraDate = DateTime.Now;
                InfoSalesDetails.Extra1 = string.Empty;
                InfoSalesDetails.Extra2 = string.Empty;
                string strAgainstInvoiceN0 = input.InvoiceNumber;
                //for (int inI = 0; inI <input.SalesDetailsInfo.Count(); inI++)
                int salesInvoiceSL = 1;
                foreach(var itemLine in input.ItemLines)
                {
                    if (itemLine.ProductId>0)
                    {
                        salesInvoiceSL++;
                        InfoSalesDetails.SalesAccount = Convert.ToDecimal(itemLine.SalesAccount);       // re-assigns value for salesAccount
                        if (itemLine.Qty>0)
                        {
                            if (input.SalesMode == "Against SalesOrder")
                            {
                                InfoSalesDetails.OrderDetailsId = Convert.ToDecimal(itemLine.OrderDetailsId);
                            }
                            else
                            {
                                InfoSalesDetails.OrderDetailsId = 0;
                            }
                            if (input.SalesMode == "Against Delivery Note")
                            {
                                InfoSalesDetails.DeliveryNoteDetailsId = Convert.ToDecimal(itemLine.DeliveryNoteDetailsId);
                            }
                            else
                            {
                                InfoSalesDetails.DeliveryNoteDetailsId = 0;
                            }
                            if (input.SalesMode == "Against Quotation")
                            {
                                InfoSalesDetails.QuotationDetailsId = Convert.ToDecimal(itemLine.QuotationDetailsId);
                            }
                            else
                            {
                                InfoSalesDetails.QuotationDetailsId = 0;
                            }
                            InfoSalesDetails.SlNo = salesInvoiceSL;
                            InfoSalesDetails.ProductId = Convert.ToDecimal(itemLine.ProductId);
                            InfoSalesDetails.Qty = Convert.ToDecimal(itemLine.Qty);
                            InfoSalesDetails.Rate = Convert.ToDecimal(itemLine.Rate);
                            InfoSalesDetails.UnitId = Convert.ToDecimal(itemLine.UnitId);
                            InfoSalesDetails.UnitConversionId = Convert.ToDecimal(itemLine.UnitConversionId);
                            InfoSalesDetails.Discount = Convert.ToDecimal(itemLine.Discount);
                            InfoSalesDetails.BatchId = Convert.ToDecimal(itemLine.BatchId);
                            InfoSalesDetails.GodownId = itemLine.GodownId > 0 ? itemLine.GodownId : 0;
                            InfoSalesDetails.RackId = itemLine.RackId > 0 ? itemLine.RackId : 0;
                            
                            if (input.IsTax)
                            {
                                InfoSalesDetails.TaxId = Convert.ToDecimal(itemLine.TaxId);
                                InfoSalesDetails.TaxAmount = Convert.ToDecimal(itemLine.TaxAmount);
                            }
                            else
                            {
                                InfoSalesDetails.TaxId = 1;
                                InfoSalesDetails.TaxAmount = 0;
                            }
                            InfoSalesDetails.GrossAmount = Convert.ToDecimal(itemLine.GrossAmount);
                            InfoSalesDetails.NetAmount = Convert.ToDecimal(itemLine.NetAmount);
                            InfoSalesDetails.Amount = Convert.ToDecimal(itemLine.Amount);
                            if (itemLine.ProjectId>0)
                            {
                                InfoSalesDetails.ProjectId = itemLine.ProjectId;
                                ProjectId = itemLine.ProjectId;
                            }
                            else
                            {
                                InfoSalesDetails.ProjectId = 0;
                            }
                            if (itemLine.CategoryId>0)
                            {
                                InfoSalesDetails.CategoryId = itemLine.CategoryId;
                                CategoryId = itemLine.CategoryId;
                            }
                            else
                            {
                                InfoSalesDetails.CategoryId = 0;
                            }
                            InfoSalesDetails.ItemDescription = itemLine.ItemDescription != ""?itemLine.ItemDescription:"";
                            decSalesDetailsId = spSalesDetails.SalesDetailsAdd(InfoSalesDetails);       // updates sales details table
                            listofDetailsId.Add(decSalesDetailsId);
                            infoStockPosting.Date = Convert.ToDateTime(input.StockPosting.Date);
                            infoStockPosting.ProductId = Convert.ToDecimal(itemLine.ProductId);
                            infoStockPosting.BatchId = Convert.ToDecimal(itemLine.BatchId);
                            infoStockPosting.UnitId = Convert.ToDecimal(itemLine.UnitId);
                            infoStockPosting.GodownId = itemLine.GodownId > 0 ? itemLine.GodownId : 0;
                            infoStockPosting.RackId = itemLine.RackId > 0 ? itemLine.RackId : 0;                            
                            infoStockPosting.Rate = Convert.ToDecimal(itemLine.Rate);
                            infoStockPosting.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                            infoStockPosting.ExtraDate = DateTime.Now;
                            infoStockPosting.Extra1 = string.Empty;
                            infoStockPosting.Extra2 = string.Empty;
                            if (itemLine.ProjectId > 0)
                            {
                                InfoSalesDetails.ProjectId = itemLine.ProjectId;
                                ProjectId = itemLine.ProjectId;
                            }
                            else
                            {
                                InfoSalesDetails.ProjectId = 0;
                            }
                            if (itemLine.CategoryId > 0)
                            {
                                InfoSalesDetails.CategoryId = itemLine.CategoryId;
                                CategoryId = itemLine.CategoryId;
                            }
                            else
                            {
                                InfoSalesDetails.CategoryId = 0;
                            }
                            if (itemLine.DeliveryNoteDetailsId>0)
                            {
                                infoStockPosting.InwardQty = InfoSalesDetails.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(InfoSalesDetails.UnitConversionId);
                                infoStockPosting.OutwardQty = 0;
                                /*****come back here*****/
                                //infoStockPosting.VoucherNo = strVoucherNoTostockPost;
                                //infoStockPosting.AgainstVoucherNo = strVoucherNo;
                                //infoStockPosting.InvoiceNo = strInvoiceNoTostockPost;
                                //infoStockPosting.AgainstInvoiceNo = strAgainstInvoiceN0;
                                //infoStockPosting.VoucherTypeId = decVouchertypeIdTostockPost;
                                //infoStockPosting.AgainstVoucherTypeId = DecSalesInvoiceVoucherTypeId;
                                spStockPosting.StockPostingAdd(infoStockPosting);
                            }
                            infoStockPosting.InwardQty = 0;
                            infoStockPosting.OutwardQty = InfoSalesDetails.Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(InfoSalesDetails.UnitConversionId);
                            infoStockPosting.VoucherNo = InfoSalesMaster.VoucherNo;
                            infoStockPosting.VoucherTypeId = 28; //hardcoded by alex
                            infoStockPosting.InvoiceNo = InfoSalesMaster.InvoiceNo;
                            infoStockPosting.AgainstInvoiceNo = "NA";
                            infoStockPosting.AgainstVoucherNo = "NA";
                            infoStockPosting.AgainstVoucherTypeId = 0;
                            infoStockPosting.Extra1 = string.Empty;
                            infoStockPosting.Extra2 = string.Empty;
                            spStockPosting.StockPostingAdd(infoStockPosting);
                        }
                    }
                }

                if (input.IsTax)
                {
                    infoSalesBillTax.SalesMasterId = decSalesMasterId;
                    infoSalesBillTax.ExtraDate = DateTime.Now;
                    infoSalesBillTax.Extra1 = string.Empty;
                    infoSalesBillTax.Extra2 = string.Empty;
                    foreach(var tax in input.SalesBillTaxInfo)
                    {
                        if (tax.TaxId>0)
                        {
                            decimal decAmount = Convert.ToDecimal(tax.TaxAmount);
                            if (decAmount > 0)
                            {
                                infoSalesBillTax.TaxId = Convert.ToInt32(tax.TaxId);
                                infoSalesBillTax.TaxAmount = Convert.ToDecimal(tax.TaxAmount);
                                spSalesBillTax.SalesBillTaxAdd(infoSalesBillTax);
                            }
                        }
                    }
                }
                
                /***additional cost commented by Alex(Junior) as instructed by Mr Alex(MD) that it's no longer needed***/
                //infoAdditionalCost.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                ////infoAdditionalCost.VoucherNo = strVoucherNo; /***come back here***/
                //infoAdditionalCost.ExtraDate = DateTime.Now;
                //infoAdditionalCost.Extra1 = string.Empty;
                //infoAdditionalCost.Extra2 = string.Empty;
                //for (int inI = 0; inI < inAddRowCount; inI++)
                //{
                //    if (dgvSalesInvoiceLedger.Rows[inI].Cells["dgvCmbAdditionalCostledgerName"].Value != null && dgvSalesInvoiceLedger.Rows[inI].Cells["dgvCmbAdditionalCostledgerName"].Value.ToString() != string.Empty)
                //    {
                //        if (dgvSalesInvoiceLedger.Rows[inI].Cells["dgvtxtAdditionalCoastledgerAmount"].Value != null && dgvSalesInvoiceLedger.Rows[inI].Cells["dgvtxtAdditionalCoastledgerAmount"].Value.ToString() != string.Empty)
                //        {
                //            infoAdditionalCost.LedgerId = Convert.ToInt32(dgvSalesInvoiceLedger.Rows[inI].Cells["dgvCmbAdditionalCostledgerName"].Value.ToString());
                //            if (!cmbCashOrbank.Visible)
                //            {
                //                infoAdditionalCost.Debit = 0;
                //                infoAdditionalCost.Credit = 100;//Convert.ToDecimal(dgvSalesInvoiceLedger.Rows[inI].Cells["dgvtxtAdditionalCoastledgerAmount"].Value.ToString());
                //            }
                //            else
                //            {
                //                infoAdditionalCost.Debit = Convert.ToDecimal(dgvSalesInvoiceLedger.Rows[inI].Cells["dgvtxtAdditionalCoastledgerAmount"].Value.ToString());
                //                infoAdditionalCost.Credit = 0;
                //            }
                //            spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                //        }
                //    }
                //}
                //if (!cmbCashOrbank.Visible)
                //{
                //    decimal decCAshOrBankId = 0;
                //    decCAshOrBankId = Convert.ToDecimal(cmbCashOrParty.SelectedValue.ToString());
                //    decimal decTotalAddAmount = Convert.ToDecimal(lblLedgerTotalAmount.Text.Trim().ToString());
                //    if (decTotalAddAmount > 0)
                //    {
                //        infoAdditionalCost.Debit = decTotalAddAmount;
                //        infoAdditionalCost.Credit = 0;
                //        infoAdditionalCost.LedgerId = decCAshOrBankId;
                //        infoAdditionalCost.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                //        infoAdditionalCost.VoucherNo = strVoucherNo;
                //        infoAdditionalCost.ExtraDate = DateTime.Now;
                //        infoAdditionalCost.Extra1 = string.Empty;
                //        infoAdditionalCost.Extra2 = string.Empty;
                //        spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                //    }
                //}
                //else
                //{
                //    if (cmbCashOrbank.Visible)
                //    {
                //        decimal decCAshOrBankId = 0;
                //        decCAshOrBankId = Convert.ToDecimal(cmbCashOrParty.SelectedValue.ToString());   // changed to cash or Party
                //        decimal decTotalAddAmount = Convert.ToDecimal(lblLedgerTotalAmount.Text.Trim().ToString());
                //        if (decTotalAddAmount > 0)
                //        {
                //            infoAdditionalCost.Debit = 0;
                //            infoAdditionalCost.Credit = decTotalAddAmount;
                //            infoAdditionalCost.LedgerId = decCAshOrBankId;
                //            infoAdditionalCost.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                //            infoAdditionalCost.VoucherNo = strVoucherNo;
                //            infoAdditionalCost.ExtraDate = DateTime.Now;
                //            infoAdditionalCost.Extra1 = string.Empty;
                //            infoAdditionalCost.Extra2 = string.Empty;
                //            spAdditionalCost.AdditionalCostAdd(infoAdditionalCost);
                //        }
                //    }
                //}

                applyCreditNote(input);

                ledgerPostingAdd(decSalesMasterId, decSalesDetailsId,input);     // calls a function to post to ledger
                if (spSalesMaster.SalesInvoiceInvoicePartyCheckEnableBillByBillOrNot(Convert.ToDecimal(input.Customer)))
                {
                    partyBalanceAdd(input);
                }
                if (input.IsSendMailAfterSave)
                {
                    //TO BE DONE
                    //emailInvoice(decSalesMasterId, InfoSalesMaster.OrderMasterId, InfoSalesMaster.DeliveryNoteMasterId, InfoSalesMaster.QuotationMasterId);
                }
                if (input.IsPrintAfterSave)
                {
                    SettingsSP spSettings = new SettingsSP();
                    if (spSettings.SettingsStatusCheck("Printer") == "Dot Matrix")
                    {
                        PrintForDotMatrix(decSalesMasterId);
                    }
                    else
                    {
                        Print(decSalesMasterId);
                    }
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK,ex.Message);
            }
            return Request.CreateResponse(HttpStatusCode.OK,"GENERAL_FAILURE");
        }

        private void applyCreditNote(CreateInvoiceVM input)
        {
            CreditNoteMasterSP spCreditnoteMaster = new CreditNoteMasterSP();
            CreditNoteMasterInfo infoCreditNoteMaster = new CreditNoteMasterInfo();
            CreditNoteDetailsSP spCreditNoteDetails = new CreditNoteDetailsSP();
            CreditNoteDetailsInfo infoCreditNoteDetails = new CreditNoteDetailsInfo();
            ReceiptMasterInfo infoReceiptMaster = new ReceiptMasterInfo();
            ReceiptDetailsInfo infoReceiptDetails = new ReceiptDetailsInfo();
            ReceiptMasterSP spReceiptMaster = new ReceiptMasterSP();
            ReceiptDetailsSP spReceiptDetails = new ReceiptDetailsSP();
            JournalMasterInfo infoJournalMaster = new JournalMasterInfo();
            JournalDetailsInfo infoJournalDetails = new JournalDetailsInfo();
            JournalMasterSP spJournalMaster = new JournalMasterSP();
            JournalDetailsSP spJournalDetails = new JournalDetailsSP();
            PartyBalanceSP SpPartyBalance = new PartyBalanceSP();
            PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
            HelperClasses helperClasses = new HelperClasses();
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            PostingsHelper PostingHelper = new PostingsHelper();
            DataTable dt = new DataTable();
            decimal decSelectedCurrencyRate = 1;
            decimal existingLedgerId = 0;

            #region Working region

            bool createNewCreditNote = false;
            DataTable dtAdvancePayments = new DataTable();
            dtAdvancePayments.Columns.Add("voucherTypeId", typeof(string));
            dtAdvancePayments.Columns.Add("display", typeof(string));
            dtAdvancePayments.Columns.Add("voucherNo", typeof(string));
            dtAdvancePayments.Columns.Add("balance", typeof(decimal));
            dtAdvancePayments.Columns.Add("exchangeRateId", typeof(string));
            dtAdvancePayments.Columns.Add("invoiceNo", typeof(string));
            dtAdvancePayments.Columns.Add("AmountToApply", typeof(decimal));

            foreach(var advancePayment in input.AdvancePayments)
            {
                if (advancePayment.IsApply)
                {
                    DataRow dr = dtAdvancePayments.NewRow();
                    dr[0] = advancePayment.VoucherTypeId.ToString();
                    dr[1] = advancePayment.Display.ToString();
                    dr[2] = advancePayment.VoucherNo.ToString();
                    dr[3] = advancePayment.Balance.ToString();
                    dr[4] = advancePayment.ExchangeRateId.ToString();
                    dr[5] = advancePayment.InvoiceNo.ToString();
                    dr[6] = advancePayment.Balance.ToString();
                    dtAdvancePayments.Rows.Add(dr);
                }
            }

            if (dtAdvancePayments.Rows.Count > 0)
            {
                decimal totalAdvancePaymentToApply = (from t in dtAdvancePayments.AsEnumerable()
                                                      select t.Field<decimal>(6)).Sum();
                decimal totalInvoice = Convert.ToDecimal(input.GrandTotal);

                foreach (DataRow row in dtAdvancePayments.Rows)
                {
                    createNewCreditNote = false;
                    decimal voucherTypeId = Convert.ToDecimal(row[0].ToString());
                    string voucherNo = row[2].ToString();
                    decimal balance = Convert.ToDecimal(row[3].ToString());
                    decimal amountToApply = Convert.ToDecimal(row[6].ToString());
                    if (balance > amountToApply)
                    {
                        createNewCreditNote = true;
                    }

                    dt = SpPartyBalance.PartyBalanceViewByVoucherNoAndVoucherType(voucherTypeId, voucherNo, DateTime.Now);
                    //decimal decPartyBalanceId = Convert.ToDecimal(dt.Rows[0]["PartyBalanceId"].ToString());

                    TotalAmount = balance > amountToApply ? amountToApply : balance;

                    #region Used code
                    if (voucherTypeId == 22)
                    {
                        bool CreditMasterEdit = false;
                        //infoCreditNoteMaster.CreditNoteMasterId = Convert.ToDecimal(dgvCreditNote.SelectedValue.ToString());
                        //infoCreditNoteMaster.CreditNoteMasterId = helperClasses.CreditNoteMasterId(Convert.ToDecimal(dgvCreditNote.SelectedValue));
                        infoCreditNoteMaster.CreditNoteMasterId = helperClasses.CreditNoteMasterId(voucherNo.ToString());
                        infoCreditNoteMaster = spCreditnoteMaster.CreditNoteMasterView(infoCreditNoteMaster.CreditNoteMasterId);

                        infoCreditNoteMaster.VoucherNo = infoCreditNoteMaster.VoucherNo;
                        infoCreditNoteMaster.InvoiceNo = infoCreditNoteMaster.InvoiceNo;
                        infoCreditNoteMaster.SuffixPrefixId = infoCreditNoteMaster.SuffixPrefixId;
                        infoCreditNoteMaster.Date = Convert.ToDateTime(input.Date);
                        infoCreditNoteMaster.Narration = input.Narration;
                        infoCreditNoteMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                        infoCreditNoteMaster.VoucherTypeId = infoCreditNoteMaster.VoucherTypeId;
                        infoCreditNoteMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
                        infoCreditNoteMaster.ExtraDate = DateTime.Now;
                        infoCreditNoteMaster.Extra1 = string.Empty;
                        infoCreditNoteMaster.Extra2 = string.Empty;
                        //decTotalDebit = Convert.ToDecimal(txtDebitTotal.Text.Trim());
                        //decTotalCredit = Convert.ToDecimal(txtCreditTotal.Text.Trim());
                        infoCreditNoteMaster.TotalAmount = TotalAmount;
                        CreditMasterEdit = true;
                        bool isRowAffected = PostingHelper.CreditNoteMasterAddOrEdit(infoCreditNoteMaster, infoCreditNoteDetails, CreditMasterEdit, decSelectedCurrencyRate);

                        if (isRowAffected == true)
                        {
                            dt = spCreditNoteDetails.CreditNoteDetailsViewByMasterId(infoCreditNoteMaster.CreditNoteMasterId);
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                infoCreditNoteDetails.ChequeDate = Convert.ToDateTime(input.Date);
                                infoCreditNoteDetails.ChequeNo = string.Empty;
                                if (Convert.ToDecimal(dt.Rows[i]["debit"].ToString()) == 0)
                                {
                                    infoCreditNoteDetails.Credit = TotalAmount;
                                    infoCreditNoteDetails.Debit = 0;
                                    infoCreditNoteDetails.LedgerId = Convert.ToDecimal(input.Customer);
                                }
                                else
                                {
                                    infoCreditNoteDetails.Credit = 0;
                                    infoCreditNoteDetails.Debit = TotalAmount;
                                    infoCreditNoteDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                    existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                }
                                infoCreditNoteDetails.CreditNoteDetailsId = Convert.ToDecimal(dt.Rows[i]["CreditNoteDetailsId"].ToString());
                                infoCreditNoteDetails.CreditNoteMasterId = infoCreditNoteMaster.CreditNoteMasterId;
                                infoCreditNoteDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
                                infoCreditNoteDetails.Extra1 = string.Empty;
                                infoCreditNoteDetails.Extra2 = string.Empty;
                                infoCreditNoteDetails.ExtraDate = DateTime.Now;
                                CreditMasterEdit = false;
                                //------------------Currency conversion------------------//
                                decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoCreditNoteDetails.ExchangeRateId);
                                PostingHelper.CreditNoteDetailsAddOrEdit(infoCreditNoteMaster, infoCreditNoteDetails, CreditMasterEdit, decSelectedCurrencyRate, input.InvoiceNumber, 28);
                            }
                        }

                        #endregion

                        #region Create new credit note
                        if (createNewCreditNote)
                        {
                            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                            TransactionsGeneralFill obj = new TransactionsGeneralFill();
                            CreditNoteMasterSP spCreditNoteMaster = new CreditNoteMasterSP();
                            string strNewCreditNoteVoucherNo = string.Empty;
                            string strNewCreditNoteInvoiceNo = string.Empty;

                            if (strNewCreditNoteVoucherNo == string.Empty)
                            {
                                strNewCreditNoteVoucherNo = "0"; //strMax;
                            }
                            //===================================================================================================================//

                            VoucherTypeSP spVoucherType = new VoucherTypeSP();
                            bool isAutomaticForCreditNote = spVoucherType.CheckMethodOfVoucherNumbering(22);
                            if (isAutomaticForCreditNote)
                            {
                                strNewCreditNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(22, Convert.ToDecimal(strNewCreditNoteVoucherNo), Convert.ToDateTime(input.Date), strCreditNoteMasterTableName);
                                if (Convert.ToDecimal(strNewCreditNoteVoucherNo) != spCreditNoteMaster.CreditNoteMasterGetMaxPlusOne(22))
                                {
                                    strNewCreditNoteVoucherNo = spCreditNoteMaster.CreditNoteMasterGetMax(22).ToString();
                                    strNewCreditNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(22, Convert.ToDecimal(strNewCreditNoteVoucherNo), Convert.ToDateTime(input.Date), strCreditNoteMasterTableName);
                                    if (spCreditNoteMaster.CreditNoteMasterGetMax(22).ToString() == "0")
                                    {
                                        strNewCreditNoteVoucherNo = "0";
                                        strNewCreditNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(22, Convert.ToDecimal(strNewCreditNoteVoucherNo), Convert.ToDateTime(input.Date), strCreditNoteMasterTableName);
                                    }
                                }
                                infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(22, Convert.ToDateTime(input.Date));
                                strPrefix = infoSuffixPrefix.Prefix;
                                strSuffix = infoSuffixPrefix.Suffix;
                                strNewCreditNoteInvoiceNo = strPrefix + strNewCreditNoteVoucherNo + strSuffix;
                                //txtVoucherNo.Text = strInvoiceNo;
                                //txtVoucherNo.ReadOnly = true;
                            }
                            else
                            {
                                //txtVoucherNo.ReadOnly = false;
                                //txtVoucherNo.Text = string.Empty;
                                strNewCreditNoteVoucherNo = input.InvoiceNumber + "_Cr";
                                strNewCreditNoteInvoiceNo = strNewCreditNoteVoucherNo;
                            }
                            // ============================ credit note master add =========================== //
                            infoCreditNoteMaster.VoucherNo = strNewCreditNoteVoucherNo;
                            infoCreditNoteMaster.InvoiceNo = strNewCreditNoteVoucherNo;
                            infoCreditNoteMaster.SuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                            infoCreditNoteMaster.Date = Convert.ToDateTime(input.Date);
                            infoCreditNoteMaster.Narration = input.Narration.Trim();
                            infoCreditNoteMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                            infoCreditNoteMaster.VoucherTypeId = 22;
                            infoCreditNoteMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
                            infoCreditNoteMaster.Extra1 = string.Empty;
                            infoCreditNoteMaster.Extra2 = string.Empty;
                            infoCreditNoteMaster.TotalAmount = balance - amountToApply;
                            decimal decCreditNoteMasterId = spCreditNoteMaster.CreditNoteMasterAdd(infoCreditNoteMaster);

                            // ========================== CreditNote Details Add =============================== //
                            spCreditNoteDetails = new CreditNoteDetailsSP();
                            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                            decimal decCreditNoteDetailsId = 0;
                            decimal decLedgerId = 0;
                            decimal decDebit = 0;
                            decimal decCredit = 0;
                            try
                            {
                                infoCreditNoteDetails.CreditNoteMasterId = decCreditNoteMasterId;
                                infoCreditNoteDetails.ExchangeRateId = ExchangeRateId;
                                infoCreditNoteDetails.ExtraDate = DateTime.Now;
                                infoCreditNoteDetails.Extra1 = string.Empty;
                                infoCreditNoteDetails.Extra2 = string.Empty;
                                infoCreditNoteDetails.LedgerId = Convert.ToDecimal(input.Customer);
                                decLedgerId = infoCreditNoteDetails.LedgerId;
                                infoCreditNoteDetails.Debit = 0;
                                infoCreditNoteDetails.Credit = balance - amountToApply;
                                decDebit = infoCreditNoteDetails.Debit * decSelectedCurrencyRate;
                                decCredit = infoCreditNoteDetails.Credit * decSelectedCurrencyRate;
                                infoCreditNoteDetails.ChequeNo = string.Empty;
                                infoCreditNoteDetails.ChequeDate = Convert.ToDateTime(input.Date);
                                //-------------------- credit leg ---------------------------- //
                                decimal decDebitDetailsId = spCreditNoteDetails.CreditNoteDetailsAdd(infoCreditNoteDetails);

                                infoCreditNoteDetails.LedgerId = 1;
                                decLedgerId = infoCreditNoteDetails.LedgerId;
                                infoCreditNoteDetails.Debit = balance - amountToApply;
                                infoCreditNoteDetails.Credit = 0;
                                decDebit = infoCreditNoteDetails.Debit * decSelectedCurrencyRate;
                                decCredit = infoCreditNoteDetails.Credit * decSelectedCurrencyRate;

                                // -------------------- debit leg --------------------------- //
                                decimal decCreditDetailsId = spCreditNoteDetails.CreditNoteDetailsAdd(infoCreditNoteDetails);

                                // -------------------------- party balance add --------------------------- //
                                PartyBalanceSP spPartyBalance = new PartyBalanceSP();
                                PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                                InfopartyBalance.CreditPeriod = 0;//
                                InfopartyBalance.Date = Convert.ToDateTime(input.Date);
                                InfopartyBalance.LedgerId = Convert.ToDecimal(input.Customer);
                                InfopartyBalance.ReferenceType = "OnAccount";
                                InfopartyBalance.AgainstInvoiceNo = "0";
                                InfopartyBalance.AgainstVoucherNo = "0";
                                InfopartyBalance.AgainstVoucherTypeId = 0;
                                InfopartyBalance.VoucherTypeId = 22;
                                InfopartyBalance.InvoiceNo = strNewCreditNoteVoucherNo;
                                InfopartyBalance.VoucherNo = strNewCreditNoteVoucherNo;
                                InfopartyBalance.Credit = balance - amountToApply;
                                InfopartyBalance.Debit = 0;
                                InfopartyBalance.ExchangeRateId = 1;
                                InfopartyBalance.Extra1 = string.Empty;
                                InfopartyBalance.Extra2 = string.Empty;
                                InfopartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                PostingHelper.PartyBalanceAdd(InfopartyBalance);

                                // =============================== ledger posting ==================================== //
                                infoLedgerPosting.ChequeDate = Convert.ToDateTime(input.Date);
                                infoLedgerPosting.ChequeNo = string.Empty;
                                infoLedgerPosting.Credit = balance - amountToApply;
                                infoLedgerPosting.Date = Convert.ToDateTime(input.Date);
                                infoLedgerPosting.Debit = 0;
                                infoLedgerPosting.DetailsId = decCreditDetailsId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.Extra2 = string.Empty;
                                infoLedgerPosting.ExtraDate = DateTime.Now;
                                infoLedgerPosting.InvoiceNo = strNewCreditNoteVoucherNo;
                                infoLedgerPosting.LedgerId = Convert.ToDecimal(input.Customer);
                                infoLedgerPosting.VoucherNo = strNewCreditNoteVoucherNo;
                                infoLedgerPosting.VoucherTypeId = 22;
                                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                                // --------------- credit leg -------------------------------------------------- //
                                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                                // --------------- debit leg --------------------------------------------------- //
                                infoLedgerPosting.Credit = 0;
                                infoLedgerPosting.Date = Convert.ToDateTime(input.Date);
                                infoLedgerPosting.Debit = balance - amountToApply;
                                infoLedgerPosting.DetailsId = decDebitDetailsId;
                                infoLedgerPosting.Extra1 = string.Empty;
                                infoLedgerPosting.LedgerId = existingLedgerId;
                                infoLedgerPosting.VoucherNo = strNewCreditNoteVoucherNo;
                                infoLedgerPosting.VoucherTypeId = 22;
                                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                            }
                            catch (Exception ex) { }
                            createNewCreditNote = false;
                            #endregion
                        }
                    }
                    #region /*Used code*/
                    if (voucherTypeId == 5) // Receipt voucher
                    {
                        bool ReceiptMasterEdit = false;
                        comStr = string.Format("SELECT receiptMasterId FROM tbl_ReceiptMaster WHERE " +
                            "voucherNo = '{0}' ", voucherNo.ToString());
                        infoReceiptMaster.ReceiptMasterId = helperClasses.GetMasterId(comStr);     //.CreditNoteMasterId = helperClasses.CreditNoteMasterId(Convert.ToDecimal(voucherNo));
                        infoReceiptMaster = spReceiptMaster.ReceiptMasterView(infoReceiptMaster.ReceiptMasterId);   // ( spCreditnoteMaster.CreditNoteMasterView(infoCreditNoteMaster.CreditNoteMasterId);

                        //infoReceiptMaster.VoucherNo = infoReceiptMaster.VoucherNo;
                        infoReceiptMaster.InvoiceNo = infoReceiptMaster.InvoiceNo;
                        infoReceiptMaster.SuffixPrefixId = infoReceiptMaster.SuffixPrefixId;
                        infoReceiptMaster.Date = Convert.ToDateTime(input.Date);
                        infoReceiptMaster.Narration = input.Narration.Trim();
                        infoReceiptMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                        infoReceiptMaster.VoucherTypeId = infoReceiptMaster.VoucherTypeId;
                        infoReceiptMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
                        infoReceiptMaster.ExtraDate = DateTime.Now;
                        infoReceiptMaster.Extra1 = string.Empty;
                        infoReceiptMaster.Extra2 = string.Empty;
                        //decTotalDebit = Convert.ToDecimal(txtDebitTotal.Text.Trim());
                        //decTotalCredit = Convert.ToDecimal(txtCreditTotal.Text.Trim());
                        infoReceiptMaster.TotalAmount = TotalAmount;
                        ReceiptMasterEdit = true;
                        bool isRowAffected = PostingHelper.ReceiptMasterEdit(infoReceiptMaster, decSelectedCurrencyRate);

                        if (isRowAffected == true)
                        {
                            //where i made changes
                            dt = spReceiptDetails.ReceiptDetailsViewByMasterId(infoReceiptMaster.ReceiptMasterId);    // spCreditNoteDetails.CreditNoteDetailsViewByMasterId(infoCreditNoteMaster.CreditNoteMasterId);
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                infoReceiptDetails.Amount = amountToApply;
                                infoReceiptDetails.ChequeDate = Convert.ToDateTime(input.Date);
                                infoReceiptDetails.ChequeNo = string.Empty;
                                //infoReceiptDetails.VoucherNo = infoReceiptDetails.VoucherNo; //This is where the invoiceNo is been called
                                //infoReceiptDetails.LedgerId = Convert.ToDecimal(cmbCashOrParty.SelectedValue.ToString());
                                infoReceiptDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                infoReceiptDetails.ReceiptDetailsId = Convert.ToDecimal(dt.Rows[i]["ReceiptDetailsId"].ToString());
                                infoReceiptDetails.ReceiptMasterId = infoReceiptMaster.ReceiptMasterId;
                                infoReceiptDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
                                infoReceiptDetails.Extra1 = string.Empty;
                                infoReceiptDetails.Extra2 = string.Empty;
                                infoReceiptDetails.ExtraDate = DateTime.Now;
                                ReceiptMasterEdit = false;
                                //------------------Currency conversion------------------//
                                decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoReceiptDetails.ExchangeRateId);
                                PostingHelper.ReceiptDetailsEdit(infoReceiptMaster, infoReceiptDetails, ReceiptMasterEdit, decSelectedCurrencyRate, input.InvoiceNumber, 28);
                            }
                        }
                    }
                    if (voucherTypeId == 6) // Journal voucher
                    {
                        bool JournalMasterEdit = false;
                        comStr = string.Format("SELECT journalMasterId FROM tbl_JournalMaster WHERE " +
                        "voucherNo = '{0}' ", voucherNo.ToString());
                        infoJournalMaster.JournalMasterId = helperClasses.GetMasterId(comStr);     //.CreditNoteMasterId = helperClasses.CreditNoteMasterId(Convert.ToDecimal(voucherNo));
                        infoJournalMaster = spJournalMaster.JournalMasterView(infoJournalMaster.JournalMasterId);   // ( spCreditnoteMaster.CreditNoteMasterView(infoCreditNoteMaster.CreditNoteMasterId);
                                                                                                                    //infoJournalMaster.VoucherNo = infoJournalMaster.VoucherNo;
                                                                                                                    //infoJournalMaster.InvoiceNo = infoJournalMaster.InvoiceNo;
                                                                                                                    //infoJournalMaster.SuffixPrefixId = infoJournalMaster.SuffixPrefixId;
                        infoJournalMaster.Date = Convert.ToDateTime(input.Date);
                        infoJournalMaster.Narration = input.Narration;
                        infoJournalMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                        //infoJournalMaster.VoucherTypeId = infoJournalMaster.VoucherTypeId;
                        infoJournalMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
                        infoJournalMaster.ExtraDate = DateTime.Now;
                        //infoJournalMaster.Extra1 = string.Empty;
                        //infoJournalMaster.Extra2 = string.Empty;
                        //decTotalDebit = Convert.ToDecimal(txtDebitTotal.Text.Trim());
                        //decTotalCredit = Convert.ToDecimal(txtCreditTotal.Text.Trim());
                        infoJournalMaster.TotalAmount = TotalAmount;
                        JournalMasterEdit = true;
                        bool isRowEdited = PostingHelper.JournalMasterEdit(infoJournalMaster, decSelectedCurrencyRate);

                        if (isRowEdited == true)
                        {
                            dt = new DataTable();
                            dt = spJournalDetails.JournalDetailsViewByMasterId(infoJournalMaster.JournalMasterId);    // spCreditNoteDetails.CreditNoteDetailsViewByMasterId(infoCreditNoteMaster.CreditNoteMasterId);
                            for (int i = 0; i < dt.Rows.Count; i++)
                            {
                                if (Convert.ToDecimal(dt.Rows[i]["debit"].ToString()) == 0)
                                {
                                    infoJournalDetails.Credit = TotalAmount;
                                    infoJournalDetails.Debit = 0;
                                    infoJournalDetails.LedgerId = Convert.ToDecimal(input.Customer);
                                }
                                else
                                {
                                    infoJournalDetails.Credit = 0;
                                    infoJournalDetails.Debit = TotalAmount;
                                    infoJournalDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                    existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                }
                                infoJournalDetails.ChequeDate = Convert.ToDateTime(input.Date);
                                infoJournalDetails.ChequeNo = string.Empty;
                                infoJournalDetails.LedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                existingLedgerId = Convert.ToDecimal(dt.Rows[i]["ledgerId"].ToString());
                                infoJournalDetails.JournalMasterId = infoJournalMaster.JournalMasterId;
                                infoJournalDetails.ExchangeRateId = Convert.ToDecimal(dt.Rows[i]["ExchangeRateId"].ToString());
                                infoJournalDetails.Extra1 = string.Empty;
                                infoJournalDetails.Extra2 = string.Empty;
                                infoJournalDetails.Memo = dt.Rows[i]["Memo"].ToString();
                                infoJournalDetails.ExtraDate = DateTime.Now;
                                JournalMasterEdit = false;
                                //------------------Currency conversion------------------//
                                decSelectedCurrencyRate = spExchangeRate.GetExchangeRateByExchangeRateId(infoJournalDetails.ExchangeRateId);

                                /*
                                 *  1. using ledgerId, get accountLedgerEntity
                                 *  2. fetch accountgroup for the ledger 
                                 *  3. if accountLedger.accountgroupid is not account payable id, set false, else set true || for 'Account Payables' | 'Account Receivables'
                                 */
                                var accountLedgerEntity = context.tbl_AccountLedger.AsEnumerable().FirstOrDefault(x => x.ledgerId == infoJournalDetails.LedgerId);
                                if (accountLedgerEntity == null) throw new Exception($"Error fetching Account Ledger with ledger id: {infoJournalDetails.LedgerId}");
                                var ledgerAccountGroup = context.tbl_AccountGroup.AsEnumerable().FirstOrDefault(x => x.accountGroupId == accountLedgerEntity.accountGroupId);
                                if (ledgerAccountGroup == null) throw new Exception($"Error fetching Account Group for  Ledger with ledger id: {infoJournalDetails.LedgerId}");
                                var isPayable = ledgerAccountGroup.accountGroupName.Trim() == "Account Payables" ? true : false;

                                PostingHelper.JournalDetailsEdit(infoJournalMaster, infoJournalDetails, isPayable, decSelectedCurrencyRate, input.InvoiceNumber, 28);
                            }
                        }
                    }
                    #endregion

                    #region Create new credit note
                    if (createNewCreditNote)
                    {
                        SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                        SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                        TransactionsGeneralFill obj = new TransactionsGeneralFill();
                        CreditNoteMasterSP spCreditNoteMaster = new CreditNoteMasterSP();
                        string strNewCreditNoteVoucherNo = string.Empty;
                        string strNewCreditNoteInvoiceNo = string.Empty;

                        if (strNewCreditNoteVoucherNo == string.Empty)
                        {
                            strNewCreditNoteVoucherNo = "0"; //strMax;
                        }
                        //===================================================================================================================//

                        VoucherTypeSP spVoucherType = new VoucherTypeSP();
                        bool isAutomaticForCreditNote = spVoucherType.CheckMethodOfVoucherNumbering(22);
                        if (isAutomaticForCreditNote)
                        {
                            strNewCreditNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(22, Convert.ToDecimal(strNewCreditNoteVoucherNo), Convert.ToDateTime(input.Date), strCreditNoteMasterTableName);
                            if (Convert.ToDecimal(strNewCreditNoteVoucherNo) != spCreditNoteMaster.CreditNoteMasterGetMaxPlusOne(22))
                            {
                                strNewCreditNoteVoucherNo = spCreditNoteMaster.CreditNoteMasterGetMax(22).ToString();
                                strNewCreditNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(22, Convert.ToDecimal(strNewCreditNoteVoucherNo), Convert.ToDateTime(input.Date), strCreditNoteMasterTableName);
                                if (spCreditNoteMaster.CreditNoteMasterGetMax(22).ToString() == "0")
                                {
                                    strNewCreditNoteVoucherNo = "0";
                                    strNewCreditNoteVoucherNo = obj.VoucherNumberAutomaicGeneration(22, Convert.ToDecimal(strNewCreditNoteVoucherNo), Convert.ToDateTime(input.Date), strCreditNoteMasterTableName);
                                }
                            }
                            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(22,input.Date);
                            strPrefix = infoSuffixPrefix.Prefix;
                            strSuffix = infoSuffixPrefix.Suffix;
                            strNewCreditNoteInvoiceNo = strPrefix + strNewCreditNoteVoucherNo + strSuffix;
                            //txtVoucherNo.Text = strInvoiceNo;
                            //txtVoucherNo.ReadOnly = true;
                        }
                        else
                        {
                            //txtVoucherNo.ReadOnly = false;
                            //txtVoucherNo.Text = string.Empty;
                            strNewCreditNoteVoucherNo = input.InvoiceNumber + "_Cr";
                            strNewCreditNoteInvoiceNo = strNewCreditNoteVoucherNo;
                        }
                        // ============================ credit note master add =========================== //
                        infoCreditNoteMaster.VoucherNo = strNewCreditNoteVoucherNo;
                        infoCreditNoteMaster.InvoiceNo = strNewCreditNoteVoucherNo;
                        infoCreditNoteMaster.SuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                        infoCreditNoteMaster.Date = Convert.ToDateTime(input.Date);
                        infoCreditNoteMaster.Narration = input.Narration.Trim();
                        infoCreditNoteMaster.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                        infoCreditNoteMaster.VoucherTypeId = 22;
                        infoCreditNoteMaster.FinancialYearId = Convert.ToDecimal(MATFinancials.PublicVariables._decCurrentFinancialYearId.ToString());
                        infoCreditNoteMaster.Extra1 = string.Empty;
                        infoCreditNoteMaster.Extra2 = string.Empty;
                        infoCreditNoteMaster.TotalAmount = balance - amountToApply;
                        decimal decCreditNoteMasterId = spCreditNoteMaster.CreditNoteMasterAdd(infoCreditNoteMaster);

                        // ========================== CreditNote Details Add =============================== //
                        spCreditNoteDetails = new CreditNoteDetailsSP();
                        LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
                        LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                        decimal decCreditNoteDetailsId = 0;
                        decimal decLedgerId = 0;
                        decimal decDebit = 0;
                        decimal decCredit = 0;
                        try
                        {
                            infoCreditNoteDetails.CreditNoteMasterId = decCreditNoteMasterId;
                            infoCreditNoteDetails.ExchangeRateId = ExchangeRateId;
                            infoCreditNoteDetails.ExtraDate = DateTime.Now;
                            infoCreditNoteDetails.Extra1 = string.Empty;
                            infoCreditNoteDetails.Extra2 = string.Empty;
                            infoCreditNoteDetails.LedgerId = Convert.ToDecimal(input.Customer);
                            decLedgerId = infoCreditNoteDetails.LedgerId;
                            infoCreditNoteDetails.Debit = 0;
                            infoCreditNoteDetails.Credit = balance - amountToApply;
                            decDebit = infoCreditNoteDetails.Debit * decSelectedCurrencyRate;
                            decCredit = infoCreditNoteDetails.Credit * decSelectedCurrencyRate;
                            infoCreditNoteDetails.ChequeNo = string.Empty;
                            infoCreditNoteDetails.ChequeDate = Convert.ToDateTime(input.Date);
                            //-------------------- credit leg ---------------------------- //
                            decimal decDebitDetailsId = spCreditNoteDetails.CreditNoteDetailsAdd(infoCreditNoteDetails);

                            infoCreditNoteDetails.LedgerId = 1;
                            decLedgerId = infoCreditNoteDetails.LedgerId;
                            infoCreditNoteDetails.Debit = balance - amountToApply;
                            infoCreditNoteDetails.Credit = 0;
                            decDebit = infoCreditNoteDetails.Debit * decSelectedCurrencyRate;
                            decCredit = infoCreditNoteDetails.Credit * decSelectedCurrencyRate;

                            // -------------------- debit leg --------------------------- //
                            decimal decCreditDetailsId = spCreditNoteDetails.CreditNoteDetailsAdd(infoCreditNoteDetails);

                            // -------------------------- party balance add --------------------------- //
                            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
                            PartyBalanceInfo InfopartyBalance = new PartyBalanceInfo();
                            InfopartyBalance.CreditPeriod = 0;//
                            InfopartyBalance.Date = Convert.ToDateTime(input.Date);
                            InfopartyBalance.LedgerId = Convert.ToDecimal(input.Customer);
                            InfopartyBalance.ReferenceType = "OnAccount";
                            InfopartyBalance.AgainstInvoiceNo = "0";
                            InfopartyBalance.AgainstVoucherNo = "0";
                            InfopartyBalance.AgainstVoucherTypeId = 0;
                            InfopartyBalance.VoucherTypeId = 22;
                            InfopartyBalance.InvoiceNo = strNewCreditNoteVoucherNo;
                            InfopartyBalance.VoucherNo = strNewCreditNoteVoucherNo;
                            InfopartyBalance.Credit = balance - amountToApply;
                            InfopartyBalance.Debit = 0;
                            InfopartyBalance.ExchangeRateId = 1;
                            InfopartyBalance.Extra1 = string.Empty;
                            InfopartyBalance.Extra2 = string.Empty;
                            InfopartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                            PostingHelper.PartyBalanceAdd(InfopartyBalance);

                            // =============================== ledger posting ==================================== //
                            infoLedgerPosting.ChequeDate = Convert.ToDateTime(input.Date);
                            infoLedgerPosting.ChequeNo = string.Empty;
                            infoLedgerPosting.Credit = balance - amountToApply;
                            infoLedgerPosting.Date = Convert.ToDateTime(input.Date);
                            infoLedgerPosting.Debit = 0;
                            infoLedgerPosting.DetailsId = decCreditDetailsId;
                            infoLedgerPosting.Extra1 = string.Empty;
                            infoLedgerPosting.Extra2 = string.Empty;
                            infoLedgerPosting.ExtraDate = DateTime.Now;
                            infoLedgerPosting.InvoiceNo = strNewCreditNoteVoucherNo;
                            infoLedgerPosting.LedgerId = Convert.ToDecimal(input.Customer);
                            infoLedgerPosting.VoucherNo = strNewCreditNoteVoucherNo;
                            infoLedgerPosting.VoucherTypeId = 22;
                            infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                            // --------------- credit leg -------------------------------------------------- //
                            //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);

                            // --------------- debit leg --------------------------------------------------- //
                            infoLedgerPosting.Credit = 0;
                            infoLedgerPosting.Date = Convert.ToDateTime(input.Date);
                            infoLedgerPosting.Debit = balance - amountToApply;
                            infoLedgerPosting.DetailsId = decDebitDetailsId;
                            infoLedgerPosting.Extra1 = string.Empty;
                            infoLedgerPosting.LedgerId = existingLedgerId;
                            infoLedgerPosting.VoucherNo = strNewCreditNoteVoucherNo;
                            infoLedgerPosting.VoucherTypeId = 22;
                            //spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                        }
                        catch (Exception ex) { }
                        #endregion
                    }
                }
                #endregion
            }
        }

        /// <summary>
        /// Ledger posting save function
        /// </summary>
        public string ledgerPostingAdd(decimal decSalesMasterId, decimal decSalesDetailsId,CreateInvoiceVM input)
        {
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            SalesMasterInfo InfoSalesMaster = new SalesMasterInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            decimal decRate = 0;
            decimal decimalGrantTotal = 0;
            decimal decTotalAmount = 0;
            int itemsinList = 0;

            try
            {
                decimalGrantTotal = Convert.ToDecimal(input.GrandTotal);
                decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(input.Currrency));
                decimalGrantTotal = decimalGrantTotal * decRate;
                infoLedgerPosting.Debit = decimalGrantTotal;
                infoLedgerPosting.Credit = 0;
                infoLedgerPosting.Date = Convert.ToDateTime(input.Date);
                infoLedgerPosting.VoucherTypeId = /*DecSalesInvoiceVoucherTypeId*/28;
                infoLedgerPosting.VoucherNo = strVoucherNo;
                infoLedgerPosting.InvoiceNo = input.InvoiceNumber;
                infoLedgerPosting.LedgerId = Convert.ToDecimal(input.Customer);
                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                //infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.DetailsId = decSalesMasterId;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);        // posts to debit side
                ///---------credit section
                ///
                #region new implementation
                foreach (var itemLine in input.ItemLines)
                {
                    LedgerPostingSP ledgerPostingSalesAccountId = new LedgerPostingSP();
                    if (itemLine.ProductId>0)
                    {
                        decTotalAmount = TotalNetAmountForLedgerPosting(input);
                        decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(input.Currrency));
                        decTotalAmount = decTotalAmount * decRate;
                        infoLedgerPosting.Debit = 0;
                        infoLedgerPosting.Credit=Convert.ToDecimal(itemLine.NetAmount);
                        infoLedgerPosting.Date = Convert.ToDateTime(input.Date);
                        infoLedgerPosting.VoucherTypeId = 28;
                        infoLedgerPosting.VoucherNo = strVoucherNo;
                        infoLedgerPosting.InvoiceNo = input.InvoiceNumber;
                        // Old implementation changed by Urefe because of Precious' work that removed/hide sales account selection from UI
                        // infoLedgerPosting.LedgerId = Convert.ToDecimal(row.Cells["dgvtxtsalesAccount"].Value.ToString());
                        var productCode = "";   //create a function dat gets product code using product id
                        infoLedgerPosting.LedgerId = ledgerPostingSalesAccountId.ProductSalesAccountId(productCode);
                        infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                        //infoLedgerPosting.DetailsId = 0;
                        infoLedgerPosting.DetailsId = listofDetailsId[itemsinList]; //decSalesDetailsId;
                        infoLedgerPosting.ChequeNo = string.Empty;
                        infoLedgerPosting.ChequeDate = DateTime.Now;
                        infoLedgerPosting.Extra1 = string.Empty;
                        infoLedgerPosting.Extra2 = string.Empty;
                        spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);    // post to credit side
                        itemsinList++;
                    }
                }
                #endregion
                #region old implementatin
                /*
                decTotalAmount = TotalNetAmountForLedgerPosting();
                decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(cmbCurrency.SelectedValue.ToString()));
                decTotalAmount = decTotalAmount * decRate;
                infoLedgerPosting.Debit = 0;
                infoLedgerPosting.Credit = decTotalAmount;  // to be gotten from grid
                infoLedgerPosting.Date = Convert.ToDateTime(txtDate.Text.ToString());
                infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                infoLedgerPosting.VoucherNo = strVoucherNo;
                infoLedgerPosting.InvoiceNo = txtInvoiceNo.Text.Trim();
                infoLedgerPosting.LedgerId = Convert.ToDecimal(cmbSalesAccount.SelectedValue.ToString());       // to be gotten from grid
                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);        /// posts to credit side
                */
                #endregion
                decimal decBillDis = 0;
                decBillDis = Convert.ToDecimal(input.BillDiscount);
                decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(input.Currrency));
                decBillDis = decBillDis * decRate;
                if (decBillDis > 0)
                {
                    infoLedgerPosting.Debit = decBillDis;
                    infoLedgerPosting.Credit = 0;
                    infoLedgerPosting.Date = Convert.ToDateTime(input.Date);
                    infoLedgerPosting.VoucherTypeId = 28;
                    infoLedgerPosting.VoucherNo = strVoucherNo;
                    infoLedgerPosting.InvoiceNo = input.InvoiceNumber;
                    infoLedgerPosting.LedgerId = 8;
                    infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
                foreach (var dgvrow in input.SalesInvoiceTax)
                {
                    if (dgvrow.TaxId>0)
                    {
                        decimal decTaxAmount = 0;
                        decTaxAmount = Convert.ToDecimal(dgvrow.TaxAmount);
                        decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(input.Currrency));
                        decTaxAmount = decTaxAmount * decRate;
                        if (decTaxAmount > 0)
                        {
                            infoLedgerPosting.Debit = 0;
                            infoLedgerPosting.Credit = decTaxAmount;
                            infoLedgerPosting.Date = Convert.ToDateTime(input.Date);
                            infoLedgerPosting.VoucherTypeId = 28;
                            infoLedgerPosting.VoucherNo = strVoucherNo;
                            infoLedgerPosting.InvoiceNo = input.InvoiceNumber;
                            infoLedgerPosting.LedgerId = Convert.ToDecimal(dgvrow.TaxLedgerId);
                            infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                            infoLedgerPosting.DetailsId = 0;
                            infoLedgerPosting.ChequeNo = string.Empty;
                            infoLedgerPosting.ChequeDate = DateTime.Now;
                            infoLedgerPosting.Extra1 = string.Empty;
                            infoLedgerPosting.Extra2 = string.Empty;
                            spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                        }
                    }
                }
                //if (input.IsCashOrBank)
                //{
                //    foreach (DataGridViewRow dgvrow in dgvSalesInvoiceLedger.Rows)
                //    {
                //        if (dgvrow.Cells["dgvCmbAdditionalCostledgerName"].Value != null && dgvrow.Cells["dgvCmbAdditionalCostledgerName"].Value.ToString() != string.Empty)
                //        {
                //            decimal decAmount = 0;
                //            decAmount = Convert.ToDecimal(dgvrow.Cells["dgvtxtAdditionalCoastledgerAmount"].Value.ToString());
                //            decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(cmbCurrency.SelectedValue.ToString()));
                //            decAmount = decAmount * decRate;
                //            if (decAmount > 0)
                //            {
                //                infoLedgerPosting.Debit = decAmount;
                //                infoLedgerPosting.Credit = 0;
                //                infoLedgerPosting.Date = Convert.ToDateTime(txtDate.Text.ToString());
                //                infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                //                infoLedgerPosting.VoucherNo = strVoucherNo;
                //                infoLedgerPosting.InvoiceNo = txtInvoiceNo.Text.Trim();
                //                infoLedgerPosting.LedgerId = Convert.ToDecimal(dgvrow.Cells["dgvCmbAdditionalCostledgerName"].Value.ToString());
                //                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //                infoLedgerPosting.DetailsId = 0;
                //                infoLedgerPosting.ChequeNo = string.Empty;
                //                infoLedgerPosting.ChequeDate = DateTime.Now;
                //                infoLedgerPosting.Extra1 = string.Empty;
                //                infoLedgerPosting.Extra2 = string.Empty;
                //                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                //            }
                //        }
                //    }
                //    decimal decBankOrCashId = 0;
                //    //decBankOrCashId = Convert.ToDecimal(cmbCashOrbank.SelectedValue.ToString());
                //    decBankOrCashId = Convert.ToDecimal(cmbCashOrParty.SelectedValue.ToString());
                //    decimal decAmountForCr = 0;
                //    decAmountForCr = Convert.ToDecimal(lblLedgerTotalAmount.Text.ToString());
                //    decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(cmbCurrency.SelectedValue.ToString()));
                //    decAmountForCr = decAmountForCr * decRate;
                //    if (decAmountForCr > 0)
                //    {
                //        infoLedgerPosting.Debit = 0;
                //        infoLedgerPosting.Credit = decAmountForCr;
                //        infoLedgerPosting.Date = Convert.ToDateTime(txtDate.Text.ToString());
                //        infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                //        infoLedgerPosting.VoucherNo = strVoucherNo;
                //        infoLedgerPosting.InvoiceNo = txtInvoiceNo.Text.Trim();
                //        infoLedgerPosting.LedgerId = decBankOrCashId;
                //        infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //        infoLedgerPosting.DetailsId = 0;
                //        infoLedgerPosting.ChequeNo = string.Empty;
                //        infoLedgerPosting.ChequeDate = DateTime.Now;
                //        infoLedgerPosting.Extra1 = "AddCash";
                //        infoLedgerPosting.Extra2 = string.Empty;
                //        spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                //    }
                //}
                //else
                //{
                //    foreach (DataGridViewRow dgvrow in dgvSalesInvoiceLedger.Rows)
                //    {
                //        if (dgvrow.Cells["dgvCmbAdditionalCostledgerName"].Value != null && dgvrow.Cells["dgvCmbAdditionalCostledgerName"].Value.ToString() != string.Empty)
                //        {
                //            decimal decAmount = 0;
                //            decAmount = Convert.ToDecimal(dgvrow.Cells["dgvtxtAdditionalCoastledgerAmount"].Value.ToString());
                //            decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(cmbCurrency.SelectedValue.ToString()));
                //            decAmount = decAmount * decRate;
                //            if (decAmount > 0)
                //            {
                //                infoLedgerPosting.Debit = 0;
                //                infoLedgerPosting.Credit = decAmount;
                //                infoLedgerPosting.Date = Convert.ToDateTime(txtDate.Text.ToString());
                //                infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                //                infoLedgerPosting.VoucherNo = strVoucherNo;
                //                infoLedgerPosting.InvoiceNo = txtInvoiceNo.Text.Trim();
                //                infoLedgerPosting.LedgerId = Convert.ToDecimal(dgvrow.Cells["dgvCmbAdditionalCostledgerName"].Value.ToString());
                //                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //                infoLedgerPosting.DetailsId = 0;
                //                infoLedgerPosting.ChequeNo = string.Empty;
                //                infoLedgerPosting.ChequeDate = DateTime.Now;
                //                infoLedgerPosting.Extra1 = string.Empty;
                //                infoLedgerPosting.Extra2 = string.Empty;
                //                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                //            }
                //        }
                //    }
                //}
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
            return "GENERAL_FAILURE";
        }
        /// <summary>
        /// Party balance save function
        /// </summary>
        public HttpResponseMessage partyBalanceAdd(CreateInvoiceVM input)
        {
            PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
            SalesMasterInfo InfoSalesMaster = new SalesMasterInfo();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            try
            {
                infoPartyBalance.Date = Convert.ToDateTime(input.Date);
                infoPartyBalance.LedgerId = Convert.ToDecimal(input.Customer);
                infoPartyBalance.VoucherNo = strVoucherNo;
                infoPartyBalance.InvoiceNo = input.InvoiceNumber;
                infoPartyBalance.VoucherTypeId = /*DecSalesInvoiceVoucherTypeId*/28;
                infoPartyBalance.AgainstVoucherTypeId = 0;
                infoPartyBalance.AgainstVoucherNo = "0";
                infoPartyBalance.AgainstInvoiceNo = "0";
                infoPartyBalance.ReferenceType = "New";
                infoPartyBalance.Debit = Convert.ToDecimal(input.GrandTotal);
                infoPartyBalance.Credit = 0;
                infoPartyBalance.CreditPeriod = Convert.ToInt32(input.CreditPeriod);
                infoPartyBalance.ExchangeRateId = Convert.ToDecimal(input.Currrency);
                infoPartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoPartyBalance.ExtraDate = DateTime.Now;
                infoPartyBalance.Extra1 = string.Empty;
                infoPartyBalance.Extra2 = string.Empty;
                if(spPartyBalance.PartyBalanceAdd(infoPartyBalance)>0)
                {
                    return Request.CreateResponse(HttpStatusCode.OK,"PARTY_BALANCE_SUCCESSFUL");
                }
            }
            catch (Exception ex)
            {
                return Request.CreateResponse(HttpStatusCode.OK, ex.Message.ToString());
            }
            return Request.CreateResponse(HttpStatusCode.OK, "GENERAL_FAILURE");
        }
        /// <summary>
        /// Print function in crystal report
        /// </summary>
        /// <param name="decSalesMasterId"></param>
        /// //TO BE DONE
        public void Print(decimal decSalesMasterId)
        {            
            //SalesMasterSP spSalesMaster = new SalesMasterSP();
            //SalesMasterInfo InfoSalesMaster = new SalesMasterInfo();
            //try
            //{
            //    DataSet dsSalesInvoice = spSalesMaster.salesInvoicePrintAfterSave(decSalesMasterId, PublicVariables._decCurrentCompanyId, InfoSalesMaster.OrderMasterId, InfoSalesMaster.DeliveryNoteMasterId, InfoSalesMaster.QuotationMasterId);
            //    frmReport frmReport = new frmReport();
            //    frmReport.MdiParent = formMDI.MDIObj;
            //    frmReport.SalesInvoicePrinting(dsSalesInvoice);
            //}
            //catch (Exception ex)
            //{
            //    formMDI.infoError.ErrorString = "SI75:" + ex.Message;
            //}
        }
        /// <summary>
        /// Print function for dotmatrix printer
        /// </summary>
        /// <param name="decSalesMasterId"></param>
        /// //TO BE DONE
        public void PrintForDotMatrix(decimal decSalesMasterId)
        {
            //try
            //{
            //    DataTable dtblOtherDetails = new DataTable();
            //    CompanySP spComapany = new CompanySP();
            //    dtblOtherDetails = spComapany.CompanyViewForDotMatrix();
            //    DataTable dtblGridDetails = new DataTable();
            //    dtblGridDetails.Columns.Add("SlNo");
            //    dtblGridDetails.Columns.Add("BarCode");
            //    dtblGridDetails.Columns.Add("ProductCode");
            //    dtblGridDetails.Columns.Add("ProductName");
            //    dtblGridDetails.Columns.Add("Qty");
            //    dtblGridDetails.Columns.Add("Unit");
            //    dtblGridDetails.Columns.Add("Godown");
            //    dtblGridDetails.Columns.Add("Brand");
            //    dtblGridDetails.Columns.Add("Tax");
            //    dtblGridDetails.Columns.Add("TaxAmount");
            //    dtblGridDetails.Columns.Add("NetAmount");
            //    dtblGridDetails.Columns.Add("DiscountAmount");
            //    dtblGridDetails.Columns.Add("DiscountPercentage");
            //    dtblGridDetails.Columns.Add("SalesRate");
            //    dtblGridDetails.Columns.Add("PurchaseRate");
            //    dtblGridDetails.Columns.Add("MRP");
            //    dtblGridDetails.Columns.Add("Rack");
            //    dtblGridDetails.Columns.Add("Batch");
            //    dtblGridDetails.Columns.Add("Rate");
            //    dtblGridDetails.Columns.Add("Amount");
            //    int inRowCount = 0;
            //    foreach (DataGridViewRow dRow in dgvSalesInvoice.Rows)
            //    {
            //        if (!dRow.IsNewRow)
            //        {
            //            DataRow dr = dtblGridDetails.NewRow();
            //            dr["SlNo"] = ++inRowCount;
            //            if (dRow.Cells["dgvtxtSalesInvoiceBarcode"].Value != null)
            //            {
            //                dr["BarCode"] = dRow.Cells["dgvtxtSalesInvoiceBarcode"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoiceProductCode"].Value != null)
            //            {
            //                dr["ProductCode"] = dRow.Cells["dgvtxtSalesInvoiceProductCode"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoiceProductName"].Value != null)
            //            {
            //                dr["ProductName"] = dRow.Cells["dgvtxtSalesInvoiceProductName"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoiceQty"].Value != null)
            //            {
            //                dr["Qty"] = dRow.Cells["dgvtxtSalesInvoiceQty"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoicembUnitName"].Value != null)
            //            {
            //                dr["Unit"] = dRow.Cells["dgvtxtSalesInvoicembUnitName"].FormattedValue.ToString();
            //            }
            //            if (dRow.Cells["dgvcmbSalesInvoiceGodown"].Value != null)
            //            {
            //                dr["Godown"] = dRow.Cells["dgvcmbSalesInvoiceGodown"].FormattedValue.ToString();
            //            }
            //            if (dRow.Cells["dgvcmbSalesInvoiceRack"].Value != null)
            //            {
            //                dr["Rack"] = dRow.Cells["dgvcmbSalesInvoiceRack"].FormattedValue.ToString();
            //            }
            //            if (dRow.Cells["dgvcmbSalesInvoiceBatch"].Value != null)
            //            {
            //                dr["Batch"] = dRow.Cells["dgvcmbSalesInvoiceBatch"].FormattedValue.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoiceRate"].Value != null)
            //            {
            //                dr["Rate"] = dRow.Cells["dgvtxtSalesInvoiceRate"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoiceAmount"].Value != null)
            //            {
            //                dr["Amount"] = dRow.Cells["dgvtxtSalesInvoiceAmount"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvcmbSalesInvoiceTaxName"].Value != null)
            //            {
            //                dr["Tax"] = dRow.Cells["dgvcmbSalesInvoiceTaxName"].FormattedValue.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoiceBrand"].Value != null)
            //            {
            //                dr["Brand"] = dRow.Cells["dgvtxtSalesInvoiceBrand"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoiceTaxAmount"].Value != null)
            //            {
            //                dr["TaxAmount"] = dRow.Cells["dgvtxtSalesInvoiceTaxAmount"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoiceNetAmount"].Value != null)
            //            {
            //                dr["NetAmount"] = dRow.Cells["dgvtxtSalesInvoiceNetAmount"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoiceDiscountAmount"].Value != null)
            //            {
            //                dr["DiscountAmount"] = dRow.Cells["dgvtxtSalesInvoiceDiscountAmount"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoiceDiscountPercentage"].Value != null)
            //            {
            //                dr["DiscountPercentage"] = dRow.Cells["dgvtxtSalesInvoiceDiscountPercentage"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoiceSalesRate"].Value != null)
            //            {
            //                dr["SalesRate"] = dRow.Cells["dgvtxtSalesInvoiceSalesRate"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoicePurchaseRate"].Value != null)
            //            {
            //                dr["PurchaseRate"] = dRow.Cells["dgvtxtSalesInvoicePurchaseRate"].Value.ToString();
            //            }
            //            if (dRow.Cells["dgvtxtSalesInvoiceMrp"].Value != null)
            //            {
            //                dr["MRP"] = dRow.Cells["dgvtxtSalesInvoiceMrp"].Value.ToString();
            //            }
            //            dtblGridDetails.Rows.Add(dr);
            //        }
            //    }
            //    dtblOtherDetails.Columns.Add("voucherNo");
            //    dtblOtherDetails.Columns.Add("date");
            //    dtblOtherDetails.Columns.Add("ledgerName");
            //    dtblOtherDetails.Columns.Add("SalesMode");
            //    dtblOtherDetails.Columns.Add("SalesAccount");
            //    dtblOtherDetails.Columns.Add("SalesMan");
            //    dtblOtherDetails.Columns.Add("CreditPeriod");
            //    dtblOtherDetails.Columns.Add("VoucherType");
            //    dtblOtherDetails.Columns.Add("PricingLevel");
            //    dtblOtherDetails.Columns.Add("Customer");
            //    dtblOtherDetails.Columns.Add("CustomerAddress");
            //    dtblOtherDetails.Columns.Add("CustomerTIN");
            //    dtblOtherDetails.Columns.Add("CustomerCST");
            //    dtblOtherDetails.Columns.Add("Narration");
            //    dtblOtherDetails.Columns.Add("Currency");
            //    dtblOtherDetails.Columns.Add("TotalAmount");
            //    dtblOtherDetails.Columns.Add("BillDiscount");
            //    dtblOtherDetails.Columns.Add("GrandTotal");
            //    dtblOtherDetails.Columns.Add("AmountInWords");
            //    dtblOtherDetails.Columns.Add("Declaration");
            //    dtblOtherDetails.Columns.Add("Heading1");
            //    dtblOtherDetails.Columns.Add("Heading2");
            //    dtblOtherDetails.Columns.Add("Heading3");
            //    dtblOtherDetails.Columns.Add("Heading4");
            //    DataRow dRowOther = dtblOtherDetails.Rows[0];
            //    dRowOther["voucherNo"] = txtInvoiceNo.Text;
            //    dRowOther["date"] = txtDate.Text;
            //    dRowOther["ledgerName"] = cmbCashOrParty.Text;
            //    dRowOther["Narration"] = txtNarration.Text;
            //    dRowOther["Currency"] = cmbCurrency.Text;
            //    dRowOther["SalesMode"] = cmbSalesMode.Text;
            //    //dRowOther["SalesAccount"] = cmbSalesAccount.Text;           // cmbSalesAccount is obsolete
            //    dRowOther["SalesMan"] = cmbSalesMan.SelectedText;
            //    dRowOther["CreditPeriod"] = (txtCreditPeriod.Text) + " Days";
            //    dRowOther["PricingLevel"] = cmbPricingLevel.Text;
            //    dRowOther["Customer"] = txtCustomer.Text;
            //    dRowOther["BillDiscount"] = txtBillDiscount.Text;
            //    dRowOther["GrandTotal"] = txtGrandTotal.Text;
            //    dRowOther["TotalAmount"] = txtTotalAmount.Text;
            //    dRowOther["VoucherType"] = cmbVoucherType.Text;
            //    dRowOther["address"] = (dtblOtherDetails.Rows[0]["address"].ToString().Replace("\n", ", ")).Replace("\r", "");
            //    AccountLedgerSP spAccountLedger = new AccountLedgerSP();
            //    AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
            //    infoAccountLedger = spAccountLedger.AccountLedgerView(Convert.ToDecimal(cmbCashOrParty.SelectedValue));
            //    dRowOther["CustomerAddress"] = (infoAccountLedger.Address.ToString().Replace("\n", ", ")).Replace("\r", "");
            //    dRowOther["CustomerTIN"] = infoAccountLedger.Tin;
            //    dRowOther["CustomerCST"] = infoAccountLedger.Cst;
            //    dRowOther["AmountInWords"] = new NumToText().AmountWords(Convert.ToDecimal(txtGrandTotal.Text), PublicVariables._decCurrencyId);
            //    VoucherTypeSP spVoucherType = new VoucherTypeSP();
            //    DataTable dtblDeclaration = spVoucherType.DeclarationAndHeadingGetByVoucherTypeId(DecSalesInvoiceVoucherTypeId);
            //    dRowOther["Declaration"] = dtblDeclaration.Rows[0]["Declaration"].ToString();
            //    dRowOther["Heading1"] = dtblDeclaration.Rows[0]["Heading1"].ToString();
            //    dRowOther["Heading2"] = dtblDeclaration.Rows[0]["Heading2"].ToString();
            //    dRowOther["Heading3"] = dtblDeclaration.Rows[0]["Heading3"].ToString();
            //    dRowOther["Heading4"] = dtblDeclaration.Rows[0]["Heading4"].ToString();
            //    int inFormId = spVoucherType.FormIdGetForPrinterSettings(Convert.ToInt32(dtblDeclaration.Rows[0]["masterId"].ToString()));
            //    DotMatrixPrint.PrintDesign(inFormId, dtblOtherDetails, dtblGridDetails, dtblOtherDetails);
            //}
            //catch (Exception ex)
            //{
            //    formMDI.infoError.ErrorString = "SI76:" + ex.Message;
            //}
        }

        /// <summary>
        /// To get Total Net Amount For LedgerPosting 
        /// </summary>
        /// <returns></returns>
        public decimal TotalNetAmountForLedgerPosting(CreateInvoiceVM input)
        {
            decimal decNetAmount = 0;
            try
            {
                foreach (var item in input.ItemLines)
                {
                    if (item.ProductId>0)
                    {
                        decNetAmount = decNetAmount + Convert.ToDecimal(item.NetAmount);
                    }
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "SI67:" + ex.Message;
            }
            return decNetAmount;
        }

        [HttpGet]
        public HttpResponseMessage InvoiceLookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var customers = transactionGeneralFillObj.CashOrPartyUnderSundryDrComboFill();
            var salesMen= transactionGeneralFillObj.SalesmanViewAllForComboFill();
            var pricingLevels= transactionGeneralFillObj.PricingLevelViewAll();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            var units = new UnitSP().UnitViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks = new RackSP().RackViewAll();
            var batches = new BatchSP().BatchViewAll();


            dynamic response = new ExpandoObject();
            response.Customers = customers;
            response.SalesMen = salesMen;
            response.PricingLevels = pricingLevels;
            response.Currencies = currencies;
            response.Units = units;
            response.Stores = stores;
            response.Racks = racks;
            response.Batches = batches;
            response.voucherNo = new TransactionsGeneralFill().VoucherNumberAutomaicGeneration(28, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");

            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }

        [HttpGet]
        public HttpResponseMessage SearchProduct(string filter,string searchBy)
        {
            SalesDetailsSP spSalesDetails = new SalesDetailsSP();
            UnitSP spUnit = new UnitSP();
            GodownSP spGodown = new GodownSP();
            BatchSP spBatch = new BatchSP();
            TaxSP spTax = new TaxSP();
            RackSP spRack = new RackSP();
            dynamic response = new ExpandoObject();
            

            if (searchBy == "ProductCode")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByProductCodeForSI(28, filter);
                var units = spUnit.UnitViewAll();
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackViewAll();
                var batches = spBatch.BatchViewAll();
                //var units= spUnit.UnitViewAllByProductId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[4].ToString()));
                //var stores = spGodown.GodownViewAll();
                //var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[11].ToString()));
                //var batches= spBatch.BatchNoViewByProductId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[4].ToString()));
                var taxes= spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Units = units;
                response.Stores = stores;
                response.Racks = racks;
                response.Batches = batches;
                response.Tax = taxes;
                response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productDetails.Rows[0].ItemArray[4].ToString()));
            }
            else if (searchBy == "ProductName")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByProductNameForSI(28, filter);
                var units = spUnit.UnitViewAll();
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackViewAll();
                var batches = spBatch.BatchViewAll();
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
                //var units = spUnit.UnitViewAllByProductId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[4].ToString()));
                //var stores = spGodown.GodownViewAll();
                //var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[11].ToString()));
                //var batches = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[4].ToString()));
                
                response.Product = productDetails;
                response.Units = units;
                response.Stores = stores;
                response.Racks = racks;
                response.Batches = batches;
                response.Tax = taxes;
                response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productDetails.Rows[0].ItemArray[4].ToString()));
            }
            else if (searchBy == "Barcode")
            {
                var productDetails = spSalesDetails.SalesInvoiceDetailsViewByBarcodeForSI(28, filter);
                var units = spUnit.UnitViewAll();
                var stores = spGodown.GodownViewAll();
                var racks = spRack.RackViewAll();
                var batches = spBatch.BatchViewAll();
                //var units = spUnit.UnitViewAllByProductId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[4].ToString()));
                //var stores = spGodown.GodownViewAll();
                //var racks = spRack.RackNamesCorrespondingToGodownId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[11].ToString()));
                //var batches = spBatch.BatchNoViewByProductId(Convert.ToDecimal(productDetails.Rows[0].ItemArray[4].ToString()));
                var taxes = spTax.TaxViewAllByVoucherTypeIdApplicaleForProduct(28);

                response.Product = productDetails;
                response.Units = units;
                response.Stores = stores;
                response.Racks = racks;
                response.Batches = batches;
                response.Tax = taxes;
                response.QuantityInStock = getQuantityInStock(Convert.ToDecimal(productDetails.Rows[0].ItemArray[4].ToString()));
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetSalesModeOrderNo(decimal ledgerId, decimal voucherTypeId, decimal salesInvoiceToEdit=0)
        {
            //salesquota = 15
            //delivery 17
            //salesorder 16
            SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
            DeliveryNoteMasterSP spDeliveryNoteMasterSp = new DeliveryNoteMasterSP();
            SalesQuotationMasterSP spSalesQuotationMasterSp = new SalesQuotationMasterSP();

            if (voucherTypeId==16)
            {
                var salesOrders = spSalesOrderMaster.GetSalesOrderNoIncludePendingCorrespondingtoLedgerforSI(ledgerId, salesInvoiceToEdit, voucherTypeId);
                return Request.CreateResponse(HttpStatusCode.OK, salesOrders);
            }
            if (voucherTypeId==17)
            {
                var deliveryNotes = spDeliveryNoteMasterSp.GetDeleveryNoteNoIncludePendingCorrespondingtoLedgerForSI(ledgerId, salesInvoiceToEdit, voucherTypeId);
                return Request.CreateResponse(HttpStatusCode.OK, deliveryNotes);
            }
            if (voucherTypeId==15)
            {
                var salesQuotations = spSalesQuotationMasterSp.GetSalesQuotationIncludePendingCorrespondingtoLedgerForSI(ledgerId, salesInvoiceToEdit, voucherTypeId);
                return Request.CreateResponse(HttpStatusCode.OK, salesQuotations);
            }
            
            return Request.CreateResponse(HttpStatusCode.OK, "SALES_MODE_FAILURE");
        }

        [HttpGet]
        public HttpResponseMessage GetCurrencies(DateTime input)
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();
            var dt = transactionGeneralFillObj.CurrencyComboByDate(input);
            return Request.CreateResponse(HttpStatusCode.OK, dt);
        }

        [HttpGet]
        public HttpResponseMessage ItemUnits(decimal decProductId)
        {
            try
            {
                DataTable dtbl = new DataTable();
                UnitSP spUnit = new UnitSP();
                dtbl = spUnit.UnitViewAllByProductId(decProductId);
                return Request.CreateResponse(HttpStatusCode.OK,dtbl);
            }
            catch (Exception ex)
            {
                
            }
            return Request.CreateResponse(HttpStatusCode.OK,"GENERAL_FAILURE");
        }
                
        [HttpGet]
        public HttpResponseMessage ItemStores()
        {
            try
            {
                GodownSP spGodown = new GodownSP();
                DataTable dtblGodown = new DataTable();
                dtblGodown = spGodown.GodownViewAll();
                return Request.CreateResponse(HttpStatusCode.OK, dtblGodown);
            }
            catch (Exception ex)
            {
                
            }
            return Request.CreateResponse(HttpStatusCode.OK, "GENERAL_FAILURE");
        }
        
        [HttpGet]
        public HttpResponseMessage ItemStoreRacks(decimal storeId)
        {
            try
            {
                DataTable dtbl = new DataTable();
                RackSP spRack = new RackSP();
                dtbl = spRack.RackNamesCorrespondingToGodownId(storeId);
                return Request.CreateResponse(HttpStatusCode.OK, dtbl);
            }
            catch (Exception ex)
            {
                
            }
            return Request.CreateResponse(HttpStatusCode.OK, "GENERAL_FAILURE");
        }
        
        [HttpGet]
        public HttpResponseMessage ItemBatch(decimal decProductId)
        {
            try
            {
                DataTable dtbl = new DataTable();
                BatchSP spBatch = new BatchSP();
                dtbl = spBatch.BatchNamesCorrespondingToProduct(decProductId);
                return Request.CreateResponse(HttpStatusCode.OK, dtbl);
            }
            catch (Exception ex)
            {
                
            }
            return Request.CreateResponse(HttpStatusCode.OK, "GENERAL_FAILURE");
        }
        
        private decimal getQuantityInStock(decimal productId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format(" SELECT  convert(decimal(18, 2), (ISNULL(SUM(s.inwardQty), 0) - ISNULL(SUM(s.outwardQty), 0))) AS Currentstock " +
                " FROM tbl_StockPosting s inner join tbl_product p on p.productId = s.productId " +
                " INNER JOIN tbl_Batch b on s.batchId = b.batchId " +
                " WHERE p.productId = '{0}' AND s.date <= '{1}' ", productId, MATFinancials.PublicVariables._dtToDate);
            return Convert.ToDecimal(conn.getSingleValue(queryStr));
        } 
    }
}
