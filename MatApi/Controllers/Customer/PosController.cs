﻿using MatApi.Models;
using MatApi.Models.Customer;
using MATFinancials;
using MATFinancials.Other;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using WebGrease.Css.Extensions;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class PosController : ApiController
    {
        decimal DecPosVoucherTypeId = 10034;
        bool isAutomatic = false;
        decimal decPOSSuffixPrefixId = 0;
        decimal decSalesDetailsId = 0;
        string strVoucherNo = string.Empty;
        string strTableName = "SalesMaster";
        string strPrefix = string.Empty;        //to get the prefix string from frmvouchertypeselection
        string strSuffix = string.Empty;
        List<decimal> listofDetailsId = new List<decimal>();
        TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
        SalesMasterSP spSalesMaster = new SalesMasterSP();
        SalesDetailsInfo InfoSalesDetails = new SalesDetailsInfo();
        SalesDetailsSP spSalesDetails = new SalesDetailsSP(); 
        private DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();

        public int EmployeeId { get; set; }
        public HttpResponseMessage GetLookups()
        {
            dynamic response = new ExpandoObject();
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var pricingLevels = transactionGeneralFillObj.PricingLevelViewAll();
            var salesPoints= new CounterSP().CounterOnlyViewAll();
            var printers = new Object();
            var products = new ProductSP().ProductViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks= new RackSP().RackViewAll();
            var tax= new TaxSP().TaxViewAll();//new TaxSP().TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
            var bankLedger=new AccountLedgerSP().BankledgerComboFill();
            var customerOnly = new AccountLedgerSP().AccountLedgerViewCustomerOnly();
            var cash = new AccountLedgerSP().CashComboFill();
            var units = new UnitSP().UnitViewAll();
            var batches = new BatchSP().BatchViewAll();
            var receiptNo = VoucherNumberGeneration();
            var allAccountLedger = new AccountLedgerSP().AccountLedgerViewAll();
            var salesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();

            response.PricingLevel = pricingLevels;
            response.SalesPoints = salesPoints;
            response.Printers = printers;
            response.Products = products;
            response.Stores = stores;
            response.Racks = racks;
            response.Taxes = tax;
            response.BankLedger = bankLedger;
            response.CustomerOnly = customerOnly;
            response.Cash = cash;
            response.Units = units;
            response.Batches = batches;
            response.ReceiptNo = receiptNo;
            response.AllAccountLedger = allAccountLedger;
            response.SalesMen = salesMen;
            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }

        [HttpPost]
        public HttpResponseMessage SearchProduct(decimal productId)
        {
            dynamic response = new ExpandoObject();
            
            var product = new ProductSP().ProductView(productId);
            var batches=new BatchSP().BatchNoViewByProductId(productId);
            var units=new UnitSP().UnitViewAllByProductId(productId);

            response.Product = product;
            response.Batches = batches;
            response.Units = units;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
        [HttpPost]
        public MatResponse SavePos(SalesInvoiceVM input)
        {
            bool isSavedSuccessfully = false;
            MatResponse response = new MatResponse();

            try
            {
                SalesMasterSP spSalesMaster = new SalesMasterSP();
                SalesDetailsSP spSalesDetails = new SalesDetailsSP();
                var spStockPosting = new MatApi.Services.StockPostingSP();
                AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
                SalesBillTaxInfo infoSalesBillTax = new SalesBillTaxInfo();
                SalesBillTaxSP spSalesBillTax = new SalesBillTaxSP();
                UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
                HelperClasses helperClasses = new HelperClasses();
                ExchangeRateSP spExchangeRate = new ExchangeRateSP();
                List<SalesBillTaxInfo> infoSalesBillTaxes = new List<SalesBillTaxInfo>();
              
                //if (spSalesMaster.SalesInvoiceInvoiceNumberCheckExistence(input.SalesMasterInfo.InvoiceNo, 0, DecPosVoucherTypeId))
                //{
                    //Messages.InformationMessage("Invoice number already exist");
                    //isSavedSuccessfully = false;
                    //return isSavedSuccessfully;
                    
                    response.ResponseCode = 302;
                   // response.ResponseMessage = "Receipt number already exists!";
                    //return response;
               // }

                input.SalesMasterInfo.VoucherTypeId = DecPosVoucherTypeId;
                input.SalesMasterInfo.SuffixPrefixId = 0;
                input.SalesMasterInfo.AdditionalCost = 0;
                input.SalesMasterInfo.SalesAccount = 10;  //Convert.ToDecimal(cmbSalesAccount.SelectedValue.ToString());

                //input.SalesMasterInfo.UserId = MATFinancials.PublicVariables._decCurrentUserId;/*PublicVariables._decCurrentUserId*/
                input.SalesMasterInfo.LrNo = "NIL";
                input.SalesMasterInfo.TransportationCompany = "NIL";
                //input.SalesMasterInfo.POS = false;
                //input.SalesMasterInfo.Narration = "";
                input.SalesMasterInfo.CustomerName = "";
                input.SalesMasterInfo.ExtraDate = DateTime.Now;
                input.SalesMasterInfo.Date = input.SalesMasterInfo.Date.Add(DateTime.Now.TimeOfDay);
                input.SalesMasterInfo.DeliveryNoteMasterId = 0;
                //input.SalesMasterInfo.Extra1 = string.Empty;
                //input.SalesMasterInfo.Extra2 = string.Empty;
                //input.SalesMasterInfo.POSTellerNo = string.Empty;
                input.SalesMasterInfo.transactionDay = helperClasses.GetCurrentDay(MATFinancials.PublicVariables._decCurrentUserId);

                //if (MATFinancials.PublicVariables._decCurrentUserId > 0)
                //{
                //    input.SalesMasterInfo.EmployeeId = EmployeeId;
                //}
                //else
                //{
                //    input.SalesMasterInfo.EmployeeId = -1;
                //}

                decimal decExachangeRateId = spExchangeRate.ExchangerateViewByCurrencyId(MATFinancials.PublicVariables._decCurrencyId);
                input.SalesMasterInfo.ExchangeRateId = decExachangeRateId;
                input.SalesMasterInfo.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                input.SalesMasterInfo.OrderMasterId = 0;
                input.SalesMasterInfo.POS = true;
                input.SalesMasterInfo.QuotationMasterId = 0;
                //input.SalesMasterInfo.SalesAccount = 10;
                //input.SalesMasterInfo.TransportationCompany = string.Empty;
                input.SalesMasterInfo.SuffixPrefixId = 0;
                //input.SalesMasterInfo.cardAmount = 0;


                decimal decSalesMasterId = spSalesMaster.SalesMasterAdd(input.SalesMasterInfo);           // updates sales master table
                if (decSalesMasterId <= 0)
                {
                    // isSavedSuccessfully = true;
                    response.ResponseCode = 400;
                    response.ResponseMessage = "Something went wrong!";
                    return response;
                }
                if (spSalesMaster.PendingSalesMasterCheckExistence(input.PendingSalesMasterId))
                {
                    spSalesMaster.PendingSalesMasterDelete(input.PendingSalesMasterId);
                    spSalesDetails.PendingSalesDetailsDeleteByPendingsalesMasterId(input.PendingSalesMasterId);
                }


                string strAgainstInvoiceN0 = input.SalesMasterInfo.InvoiceNo;

                for (int i = 0; i < input.SalesDetailsInfo.Count; i++)
                {
                    var unc = new UnitConvertionSP().UnitconversionIdViewByUnitIdAndProductId(input.SalesDetailsInfo[i].UnitId, input.SalesDetailsInfo[i].ProductId);
                    input.SalesDetailsInfo[i].UnitConversionId = unc;
                    input.SalesDetailsInfo[i].SalesMasterId = decSalesMasterId;      // assigns SalesMasterDetailsID
                    input.SalesDetailsInfo[i].ExtraDate = DateTime.Now;
                    input.SalesDetailsInfo[i].Extra1 = string.Empty;
                    input.SalesDetailsInfo[i].Extra2 = string.Empty;
                    decSalesDetailsId = spSalesDetails.SalesDetailsAdd(input.SalesDetailsInfo[i]);       // updates sales details table
                    if (decSalesDetailsId > 0)
                    {
                        isSavedSuccessfully = true;
                        response.ResponseCode = 200;
                        response.ResponseMessage = "POS saved successfully!";
                    }
                    else
                    {
                        //isSavedSuccessfully = false;
                        response.ResponseCode = 400;
                        response.ResponseMessage = "Something went wrong!";
                    }
                    listofDetailsId.Add(decSalesDetailsId);

                   
                    input.StockPostingInfo = new StockPostingInfo();
                    input.StockPostingInfo.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    input.StockPostingInfo.ExtraDate = DateTime.Now;
                    input.StockPostingInfo.Date = DateTime.Now;
                    input.StockPostingInfo.Extra1 = string.Empty;
                    input.StockPostingInfo.Extra2 = string.Empty;
                    input.StockPostingInfo.ProjectId = 0;
                    input.StockPostingInfo.CategoryId = 0;
                    input.StockPostingInfo.ProductId = input.SalesDetailsInfo[i].ProductId;
                    input.StockPostingInfo.BatchId = input.SalesDetailsInfo[i].BatchId;
                    input.StockPostingInfo.GodownId = input.SalesDetailsInfo[i].GodownId;
                    input.StockPostingInfo.UnitId = input.SalesDetailsInfo[i].UnitId;
                    input.StockPostingInfo.Rate = input.SalesDetailsInfo[i].Rate;//Convert.ToDecimal(product.purchaseRate) * input.SalesDetailsInfo[i].Qty; // input.SalesDetailsInfo[i].NetAmount;
                    input.StockPostingInfo.InwardQty = 0;
                    decimal c_rate = SPUnitConversion.UnitConversionRateByUnitConversionId(input.SalesDetailsInfo[i].UnitConversionId); //input.SalesDetailsInfo[i].Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(input.SalesDetailsInfo[i].UnitId);
                    input.StockPostingInfo.OutwardQty = input.SalesDetailsInfo[i].Qty / ((c_rate > 0) ? c_rate : 1); 
                    input.StockPostingInfo.VoucherNo = input.SalesMasterInfo.VoucherNo;
                    input.StockPostingInfo.VoucherTypeId = DecPosVoucherTypeId;
                    input.StockPostingInfo.InvoiceNo = input.SalesMasterInfo.InvoiceNo;
                    input.StockPostingInfo.AgainstInvoiceNo = "NA";
                    input.StockPostingInfo.AgainstVoucherNo = "NA";
                    input.StockPostingInfo.AgainstVoucherTypeId = 0;
                    input.StockPostingInfo.Extra1 = string.Empty;
                    input.StockPostingInfo.Extra2 = string.Empty;
                    spStockPosting.StockPostingAdd(input.StockPostingInfo);

                    // old implementation 20161201
                    //// Urefe added if statement so that sales invoice against delivery not will not affect stock
                    //if (InfoSalesMaster.DeliveryNoteMasterId == 0)
                    //{
                    //    spStockPosting.StockPostingAdd(infoStockPosting);
                    //}
                }
                //for(int i=0;i<input.SalesBillTaxInfo.Count;i++)
                for (int i = 0; i < 1; i++)   //1 is used since we'll be implementing tax
                {
                    infoSalesBillTax.SalesMasterId = decSalesMasterId;
                    infoSalesBillTax.TaxAmount = input.SalesMasterInfo.TaxAmount;
                    infoSalesBillTax.TaxId = 10012/*input.SalesDetailsInfo[0].TaxId*/;   //since same tax is maintained for all items hence pick first tax in item list
                    infoSalesBillTax.ExtraDate = DateTime.Now;
                    infoSalesBillTax.Extra1 = string.Empty;
                    infoSalesBillTax.Extra2 = string.Empty;
                    if (spSalesBillTax.SalesBillTaxAdd(infoSalesBillTax) > 0)
                    {
                         isSavedSuccessfully = true;
                        response.ResponseCode = 200;
                        response.ResponseMessage = "POS saved successfully!";
                    }
                    else
                    {
                        //isSavedSuccessfully = false;
                        response.ResponseCode = 400;
                        response.ResponseMessage = "Something went wrong!";
                    }
                }
                infoSalesBillTaxes.Add(infoSalesBillTax);
                //applyCreditNote();    //don't apply credit note now(recommended by Mr Alex), we'll create another window
                //to apply credit note

                isSavedSuccessfully = ledgerPostingAdd(decSalesMasterId, decSalesDetailsId, input.SalesMasterInfo, input.SalesDetailsInfo, infoSalesBillTaxes);     // calls a function to post to ledger
                if (spSalesMaster.SalesInvoiceInvoicePartyCheckEnableBillByBillOrNot(input.SalesMasterInfo.LedgerId))
                {
                    isSavedSuccessfully = partyBalanceAdd(input.SalesMasterInfo);
                }

                if (isSavedSuccessfully == true)
                {
                    response.ResponseCode = 200;
                    response.ResponseMessage = "POS saved successfully!";
                }
                else
                {
                    response.ResponseCode = 400;
                    response.ResponseMessage = "Something went wrong!";
                }

            }
            catch (Exception ex)
            {
                response.ResponseCode = 400;
                response.ResponseMessage = "Something went wrong!";
            }

            return response;
        }
        [HttpPost]
        public MatResponse HoldPos(SalesInvoiceVM input)
        {
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            HelperClasses helperClasses = new HelperClasses();
            MatResponse response = new MatResponse();
            try
            {
                if (spSalesMaster.SalesInvoiceInvoiceNumberCheckExistence(input.SalesMasterInfo.InvoiceNo, 0, DecPosVoucherTypeId))
                {
                    
                    response.ResponseCode = 302;
                    response.ResponseMessage = "Could not save. Receipt number already exists!";
                    return response;
                }
                if (spSalesMaster.PendingSalesMasterCheckExistenceByInvoiceNo(decimal.Parse(input.SalesMasterInfo.InvoiceNo)))
                {

                    response.ResponseCode = 302;
                    response.ResponseMessage = "POS details already on hold!";
                    return response;
                }
                
                input.SalesMasterInfo.AdditionalCost = 0;
                input.SalesMasterInfo.TransportationCompany = "NIL";               
                input.SalesMasterInfo.CreditPeriod = 0;
                input.SalesMasterInfo.CustomerName = string.Empty;
                input.SalesMasterInfo.Date = input.SalesMasterInfo.Date.AddHours(DateTime.Now.Hour).AddMinutes(DateTime.Now.Minute).AddSeconds(DateTime.Now.Second);
                input.SalesMasterInfo.DeliveryNoteMasterId = 0;
                if (MATFinancials.PublicVariables._decCurrentUserId > 0)
                {
                    input.SalesMasterInfo.EmployeeId = EmployeeId;
                }
                else
                {
                    input.SalesMasterInfo.EmployeeId = -1;
                }
                decimal decExachangeRateId = spExchangeRate.ExchangerateViewByCurrencyId(MATFinancials.PublicVariables._decCurrencyId);
                input.SalesMasterInfo.ExchangeRateId = decExachangeRateId;
                input.SalesMasterInfo.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                input.SalesMasterInfo.LrNo = string.Empty;
                input.SalesMasterInfo.OrderMasterId = 0;
                input.SalesMasterInfo.POS = true;
                //input.SalesMasterInfo.cardAmount = 0;
                input.SalesMasterInfo.QuotationMasterId = 0;
                input.SalesMasterInfo.SalesAccount = 10; 
                input.SalesMasterInfo.UserId = MATFinancials.PublicVariables._decCurrentUserId;
                input.SalesMasterInfo.VoucherTypeId = DecPosVoucherTypeId;
                input.SalesMasterInfo.SuffixPrefixId = 0;                
                input.SalesMasterInfo.ExtraDate = DateTime.Now;
                input.SalesMasterInfo.Date = DateTime.Now;
                //input.SalesMasterInfo.Extra2 = string.Empty;              
                //input.SalesMasterInfo.AmountTendered = 0;              
                input.SalesMasterInfo.transactionDay = helperClasses.GetCurrentDay(MATFinancials.PublicVariables._decCurrentUserId);         // added by Urefe because of day report
                decimal decSalesMasterId = spSalesMaster.PendingSalesMasterAdd(input.SalesMasterInfo);
                if (decSalesMasterId == 0)
                {
                    response.ResponseCode = 400;
                    response.ResponseMessage = "Something went wrong!";
                    return response;
                }
                int inRowCount = input.SalesDetailsInfo.Count;            
                InfoSalesDetails.SalesMasterId = decSalesMasterId;
                InfoSalesDetails.ExtraDate = DateTime.Now;
                InfoSalesDetails.Extra1 = string.Empty;
                InfoSalesDetails.Extra2 = string.Empty;

                decimal rowsAffected = inRowCount;
                for (int inI = 0; inI < inRowCount; inI++)
                {
                    if (input.SalesDetailsInfo[inI].ProductId.ToString() != string.Empty && input.SalesDetailsInfo[inI].Qty.ToString() != string.Empty)
                    {
                            InfoSalesDetails.SlNo = Convert.ToInt32(input.SalesDetailsInfo[inI].SlNo.ToString());
                            InfoSalesDetails.ProductId = Convert.ToDecimal(input.SalesDetailsInfo[inI].ProductId.ToString());
                            InfoSalesDetails.Qty = Convert.ToDecimal(input.SalesDetailsInfo[inI].Qty.ToString());
                            InfoSalesDetails.Rate = Convert.ToDecimal(input.SalesDetailsInfo[inI].Rate.ToString());
                            InfoSalesDetails.UnitId = Convert.ToDecimal(input.SalesDetailsInfo[inI].UnitId.ToString());
                            InfoSalesDetails.UnitConversionId = Convert.ToDecimal(input.SalesDetailsInfo[inI].UnitConversionId.ToString());
                            InfoSalesDetails.Discount = Convert.ToDecimal(input.SalesDetailsInfo[inI].Discount.ToString());
                            InfoSalesDetails.TaxId = Convert.ToDecimal(input.SalesDetailsInfo[inI].TaxId.ToString());
                            InfoSalesDetails.BatchId = Convert.ToDecimal(input.SalesDetailsInfo[inI].BatchId.ToString());
                            InfoSalesDetails.GodownId = Convert.ToDecimal(input.SalesDetailsInfo[inI].GodownId.ToString());
                            InfoSalesDetails.RackId = Convert.ToDecimal(input.SalesDetailsInfo[inI].RackId .ToString());
                            InfoSalesDetails.TaxAmount = Convert.ToDecimal(input.SalesDetailsInfo[inI].TaxAmount.ToString());
                            InfoSalesDetails.GrossAmount = Convert.ToDecimal(input.SalesDetailsInfo[inI].GrossAmount.ToString());
                            InfoSalesDetails.NetAmount = Convert.ToDecimal(input.SalesDetailsInfo[inI].NetAmount.ToString());
                            InfoSalesDetails.Amount = Convert.ToDecimal(input.SalesDetailsInfo[inI].Amount.ToString());
                            InfoSalesDetails.ProjectId = Convert.ToInt32(input.SalesDetailsInfo[inI].ProjectId.ToString());  // modified by Precious
                            InfoSalesDetails.CategoryId = Convert.ToInt32(input.SalesDetailsInfo[inI].CategoryId.ToString());    // modified by Precious
                            InfoSalesDetails.SalesAccount = Convert.ToDecimal(input.SalesDetailsInfo[inI].SalesAccount.ToString()); // modified by Precious
                            bool decSalesDetailsSaved = spSalesDetails.PendingSalesDetailsAdd(InfoSalesDetails);
                            if (decSalesDetailsSaved)
                            {                                                              
                                response.ResponseCode = 200;
                                response.ResponseMessage = "POS details on Hold!";                                                
                                rowsAffected--;
                            }
                            else
                            {                                
                                response.ResponseCode = 400;
                                response.ResponseMessage = "Something went wrong!";
                            }                       
                    }
                }
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "POS45:" + ex.Message;
            }
            return response;
        }
        [HttpGet]
        // public SalesInvoiceVM ViewSales(decimal pendingsalesMasterId, bool salesOrPending)
        public SalesInvoiceVM ViewSales(decimal pendingsalesMasterId, bool salesOrPending)
        {
           // DataTable response = new DataTable();
            SalesInvoiceVM response = new SalesInvoiceVM();
            var master = spSalesMaster.POSSalesMasterViewBySalesMasterId(pendingsalesMasterId, true);
            var details = spSalesDetails.SalesDetailsViewBySalesMasterId(pendingsalesMasterId, true);

            // response.SalesMasterInfo = master.Rows[0]
            for (int i = 0; i < master.Rows.Count; i++)
            {
                response.SalesMasterInfo.POSTellerNo = master.Rows[i].ItemArray[0].ToString();
                response.SalesMasterInfo.SalesMasterId = decimal.Parse(master.Rows[i].ItemArray[1].ToString());//serves as pendindSalesMasterId or SalesMasterId depending on query
                response.SalesMasterInfo.AdditionalCost = decimal.Parse(master.Rows[i].ItemArray[2].ToString());
                response.SalesMasterInfo.AmountTendered = decimal.Parse(master.Rows[i].ItemArray[3].ToString());
                response.SalesMasterInfo.BillDiscount = decimal.Parse(master.Rows[i].ItemArray[4].ToString());
                response.SalesMasterInfo.cardAmount = decimal.Parse(master.Rows[i].ItemArray[5].ToString());
                response.SalesMasterInfo.ChangeGiven = decimal.Parse(master.Rows[i].ItemArray[6].ToString());
                response.SalesMasterInfo.CounterId = decimal.Parse(master.Rows[i].ItemArray[7].ToString());
                response.SalesMasterInfo.CreditPeriod = int.Parse(master.Rows[i].ItemArray[8].ToString());
                response.SalesMasterInfo.CustomerName = master.Rows[i].ItemArray[9].ToString();
                response.SalesMasterInfo.Date = DateTime.Parse(master.Rows[i].ItemArray[10].ToString());
                response.SalesMasterInfo.DeliveryNoteMasterId = decimal.Parse(master.Rows[i].ItemArray[11].ToString());
                response.SalesMasterInfo.EmployeeId = decimal.Parse(master.Rows[i].ItemArray[12].ToString());
                response.SalesMasterInfo.ExchangeRateId = decimal.Parse(master.Rows[i].ItemArray[13].ToString());
                response.SalesMasterInfo.Extra1 = master.Rows[i].ItemArray[14].ToString();
                response.SalesMasterInfo.Extra2 = master.Rows[i].ItemArray[15].ToString();
                response.SalesMasterInfo.ExtraDate = DateTime.Parse(master.Rows[i].ItemArray[16].ToString());
                response.SalesMasterInfo.GrandTotal = decimal.Parse(master.Rows[i].ItemArray[17].ToString());
                response.SalesMasterInfo.InvoiceNo = master.Rows[i].ItemArray[18].ToString();
                response.SalesMasterInfo.LedgerId = decimal.Parse(master.Rows[i].ItemArray[19].ToString());
                response.SalesMasterInfo.LrNo = master.Rows[i].ItemArray[20].ToString();
                response.SalesMasterInfo.Narration = master.Rows[i].ItemArray[21].ToString();
                response.SalesMasterInfo.OrderMasterId = decimal.Parse(master.Rows[i].ItemArray[23].ToString());
                response.SalesMasterInfo.POS = bool.Parse(master.Rows[i].ItemArray[24].ToString());
                response.SalesMasterInfo.PricinglevelId = decimal.Parse(master.Rows[i].ItemArray[25].ToString());
                response.SalesMasterInfo.QuotationMasterId = decimal.Parse(master.Rows[i].ItemArray[26].ToString());
                response.SalesMasterInfo.SalesAccount = decimal.Parse(master.Rows[i].ItemArray[27].ToString());
                response.SalesMasterInfo.SuffixPrefixId = decimal.Parse(master.Rows[i].ItemArray[28].ToString());
                response.SalesMasterInfo.TaxAmount = decimal.Parse(master.Rows[i].ItemArray[29].ToString());
                response.SalesMasterInfo.TotalAmount = decimal.Parse(master.Rows[i].ItemArray[30].ToString());
                response.SalesMasterInfo.TransportationCompany = master.Rows[i].ItemArray[31].ToString();
                response.SalesMasterInfo.UserId = decimal.Parse(master.Rows[i].ItemArray[32].ToString());
                response.SalesMasterInfo.VoucherNo = master.Rows[i].ItemArray[33].ToString();
                response.SalesMasterInfo.VoucherTypeId = decimal.Parse(master.Rows[i].ItemArray[34].ToString());
            }
                
           // response = master;
            //var acctLedgersDt = new AccountLedgerSP().AccountLedgerViewAll();
            //var ledgerDetails = new LedgerPostingSP().LedgerPostingViewByLedgerID(decimal.Parse(acctLedgersDt.Rows[0].ItemArray[0].ToString()));
           // UserInfo user = new UserSP().UserView(master.UserId);
            //List<AccountLedgerInfo> ledgers = new List<AccountLedgerInfo>();
            //for (int i = 0; i < acctLedgersDt.Rows.Count; i++)
            //{
            //    ledgers.Add(new AccountLedgerInfo
            //    {
            //        LedgerId = Convert.ToDecimal(acctLedgersDt.Rows[i].ItemArray[0]),
            //        LedgerName = acctLedgersDt.Rows[i].ItemArray[2].ToString(),
            //    });
            //}


            //List<CustomJournalDetailsVM> custDetails = new List<CustomJournalDetailsVM>();
            //for (int i = 0; i < details.Rows.Count; i++)
            //{
            //    custDetails.Add(new CustomJournalDetailsVM
            //    {
            //        ChequeDate = Convert.ToDateTime(details.Rows[i].ItemArray[7]),
            //        ChequeNo = details.Rows[i].ItemArray[6].ToString(),
            //        Credit = Convert.ToDecimal(details.Rows[i].ItemArray[3]),
            //        Debit = Convert.ToDecimal(details.Rows[i].ItemArray[4]),
            //        JournalDetails = Convert.ToDecimal(details.Rows[i].ItemArray[0]),
            //        JournalMaster = Convert.ToDecimal(details.Rows[i].ItemArray[1]),
            //        LedgerId = Convert.ToDecimal(details.Rows[i].ItemArray[2]),
            //        LedgerName = ledgers.FirstOrDefault(p => p.LedgerId == Convert.ToDecimal(details.Rows[i].ItemArray[2])).LedgerName,
            //        Memo = details.Rows[i].ItemArray[9].ToString()
            //    });
            //}


            //JournalViewVM response = new JournalViewVM();
            //response.JournalMasterId = master.JournalMasterId;
            //response.JournalNo = master.InvoiceNo;
            //response.JournalDate = master.Date.ToString("dd/MM/yyyy");
            //response.UserId = master.UserId;
            //response.UserFullName = user.FirstName + " " + user.LastName;
            //response.TotalAmount = master.TotalAmount;
            //response.Details = custDetails;

            return response;
        }
        [HttpPost]
        public MatResponse saveInvoice()
        {
            MatResponse response = new MatResponse();
            try
            {

            }
            catch(Exception e)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = "Something went wrong";
            }
            return response;
        }
        public bool ledgerPostingAdd(decimal decSalesMasterId, decimal decSalesDetailsId, SalesMasterInfo salesMasterInfo, List<SalesDetailsInfo> salesDetailsInfo, List<SalesBillTaxInfo> salesTax)
        {
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            SalesMasterInfo InfoSalesMaster = new SalesMasterInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            decimal decRate = 0;
            decimal decimalGrantTotal = 0;
            decimal decTotalAmount = 0;
            int itemsinList = 0;
            bool isSaved = false;

            try
            {
                decimalGrantTotal = salesMasterInfo.GrandTotal;
                decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(salesMasterInfo.ExchangeRateId);
                decimalGrantTotal = decimalGrantTotal * decRate;
                infoLedgerPosting.Debit = decimalGrantTotal;
                infoLedgerPosting.Credit = 0;
                infoLedgerPosting.Date = salesMasterInfo.Date;
                infoLedgerPosting.VoucherTypeId = DecPosVoucherTypeId;
                infoLedgerPosting.VoucherNo = salesMasterInfo.InvoiceNo/*strVoucherNo*/;
                infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                infoLedgerPosting.LedgerId = salesMasterInfo.LedgerId;
                infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                //infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.DetailsId = decSalesMasterId;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = DateTime.Now;
                infoLedgerPosting.Date = DateTime.Now;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                if (spLedgerPosting.LedgerPostingAdd(infoLedgerPosting) > 0)        // posts to debit side
                {
                    isSaved = true;
                }
                ///---------credit section
                ///
                #region new implementation
                foreach (var row in salesDetailsInfo)
                {
                    LedgerPostingSP ledgerPostingSalesAccountId = new LedgerPostingSP();
                    decTotalAmount = row.NetAmount;
                    decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(salesMasterInfo.ExchangeRateId);
                    decTotalAmount = decTotalAmount * decRate;
                    infoLedgerPosting.Debit = 0;
                    infoLedgerPosting.Credit = row.NetAmount;
                    infoLedgerPosting.Date = salesMasterInfo.Date;
                    infoLedgerPosting.VoucherTypeId = DecPosVoucherTypeId;
                    infoLedgerPosting.VoucherNo = strVoucherNo;
                    infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                    // Old implementation changed by Urefe because of Precious' work that removed/hide sales account selection from UI
                    // infoLedgerPosting.LedgerId = Convert.ToDecimal(row.Cells["dgvtxtsalesAccount"].Value.ToString()); 
                    infoLedgerPosting.LedgerId = ledgerPostingSalesAccountId.ProductSalesAccountId(new ProductSP().ProductView(row.ProductId).ProductCode);
                    infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    //infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.DetailsId = listofDetailsId[itemsinList]; //decSalesDetailsId;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    if (spLedgerPosting.LedgerPostingAdd(infoLedgerPosting) > 0)    // post to credit side
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                    itemsinList++;
                }
                #endregion

                decimal decBillDis = 0;
                decBillDis = salesMasterInfo.BillDiscount;
                decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(salesMasterInfo.ExchangeRateId);
                decBillDis = decBillDis * decRate;
                if (decBillDis > 0)
                {
                    infoLedgerPosting.Debit = decBillDis;
                    infoLedgerPosting.Credit = 0;
                    infoLedgerPosting.Date = salesMasterInfo.Date;
                    infoLedgerPosting.VoucherTypeId = DecPosVoucherTypeId;
                    infoLedgerPosting.VoucherNo = strVoucherNo;
                    infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                    infoLedgerPosting.LedgerId = 8;
                    infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    if (spLedgerPosting.LedgerPostingAdd(infoLedgerPosting) > 0)
                    {
                        isSaved = true;
                    }
                    else
                    {
                        isSaved = false;
                    }
                }
                foreach (var row in salesTax)
                {
                    decimal decTaxAmount = 0;
                    decTaxAmount = row.TaxAmount;
                    decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(salesMasterInfo.ExchangeRateId);
                    decTaxAmount = decTaxAmount * decRate;
                    if (decTaxAmount > 0)
                    {
                        infoLedgerPosting.Debit = 0;
                        infoLedgerPosting.Credit = decTaxAmount;
                        infoLedgerPosting.Date = salesMasterInfo.Date;
                        infoLedgerPosting.VoucherTypeId = DecPosVoucherTypeId;
                        infoLedgerPosting.VoucherNo = strVoucherNo;
                        infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                        infoLedgerPosting.LedgerId = 59;//salesMasterInfo.LedgerId;
                        infoLedgerPosting.YearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                        infoLedgerPosting.DetailsId = 0;
                        infoLedgerPosting.ChequeNo = string.Empty;
                        infoLedgerPosting.ChequeDate = DateTime.Now;
                        infoLedgerPosting.Extra1 = string.Empty;
                        infoLedgerPosting.Extra2 = string.Empty;
                        if (spLedgerPosting.LedgerPostingAdd(infoLedgerPosting) > 0)
                        {
                            isSaved = true;
                        }
                        else
                        {
                            isSaved = false;
                        }
                    }
                }

            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "SI73:" + ex.Message;
            }
            return isSaved;
        }
        public bool partyBalanceAdd(SalesMasterInfo input)
        {
            PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
            SalesMasterInfo InfoSalesMaster = new SalesMasterInfo();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            bool isSaved = false;

            try
            {
                infoPartyBalance.Date = input.Date;
                infoPartyBalance.LedgerId = input.LedgerId;
                infoPartyBalance.VoucherNo = strVoucherNo;
                infoPartyBalance.InvoiceNo = input.InvoiceNo;
                infoPartyBalance.VoucherTypeId = DecPosVoucherTypeId;
                infoPartyBalance.AgainstVoucherTypeId = 0;
                infoPartyBalance.AgainstVoucherNo = "0";
                infoPartyBalance.AgainstInvoiceNo = "0";
                infoPartyBalance.ReferenceType = "New";
                infoPartyBalance.Debit = input.GrandTotal;
                infoPartyBalance.Credit = 0;
                infoPartyBalance.CreditPeriod = input.CreditPeriod;
                infoPartyBalance.ExchangeRateId = input.ExchangeRateId;
                infoPartyBalance.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoPartyBalance.ExtraDate = DateTime.Now;
                infoPartyBalance.Extra1 = string.Empty;
                infoPartyBalance.Extra2 = string.Empty;
                if (spPartyBalance.PartyBalanceAdd(infoPartyBalance) > 0)
                {
                    isSaved = true;
                }
            }
            catch (Exception ex)
            {
                // formMDI.infoError.ErrorString = "SI74:" + ex.Message;
            }
            return isSaved;
        }
        /// <summary>
        /// Function to generate Voucher number as per settings
        /// </summary>
       // [HttpPost]
        public string VoucherNumberGeneration()
        {
            dynamic response = new ExpandoObject();
      
            try
            {
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }
                DateTime dtpDate = DateTime.Now;
                strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(DecPosVoucherTypeId, Convert.ToDecimal(strVoucherNo), dtpDate, strTableName);
                if (Convert.ToDecimal(strVoucherNo) != (spSalesMaster.SalesMasterVoucherMax(DecPosVoucherTypeId)))
                {
                    strVoucherNo = spSalesMaster.SalesMasterVoucherMax(DecPosVoucherTypeId).ToString();
                    strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(DecPosVoucherTypeId, Convert.ToDecimal(strVoucherNo), dtpDate, strTableName);
                    if (spSalesMaster.SalesMasterVoucherMax(DecPosVoucherTypeId) == 0)
                    {
                        strVoucherNo = "0";
                        strVoucherNo = TransactionGeneralFillObj.VoucherNumberAutomaicGeneration(DecPosVoucherTypeId, Convert.ToDecimal(strVoucherNo), dtpDate, strTableName);
                    }
                }
              //  if (isAutomatic)
                //{
                    SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
                    SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                    infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(DecPosVoucherTypeId, dtpDate);
                    strPrefix = infoSuffixPrefix.Prefix;
                    strSuffix = infoSuffixPrefix.Suffix;
                    decPOSSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
                    response.txtVoucherNo = strPrefix + strVoucherNo + strSuffix;
                   // txtVoucherNo.ReadOnly = true;
               // }
                //else
                //{
                //    txtVoucherNo.ReadOnly = false;
                //    txtVoucherNo.Text = string.Empty;
                //    strVoucherNo = string.Empty;
                //}
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "POS5:" + ex.Message;
            }
            return response.txtVoucherNo;
            //return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public IHttpActionResult ActivityLog()
        {
            var sales = context.tbl_SalesMaster.Where(a => a.POS == true).ToList()
                    .Join(context.tbl_SalesDetails.ToList(), o => o.salesMasterId, p=>p.salesMasterId,
                        (o,p)=> new
                        {
                          SalesMaster = o,
                          SalesDetail = p
                        }).Select((a)=> new
                    {
                        product = context.tbl_Product.Where(p=>p.productId == a.SalesDetail.productId).SingleOrDefault(),
                        salesDetail = a.SalesDetail,
                        SalesMasterInfo = a.SalesMaster,
                        Ledger = context.tbl_AccountLedger.Where(b=>b.ledgerId == a.SalesMaster.ledgerId ).SingleOrDefault(),
                        User = context.tbl_User.Where(b=>b.userId == a.SalesMaster.userId).SingleOrDefault(),
                        Counter = context.tbl_Counter.Where(b=>b.counterId == a.SalesMaster.counterId).SingleOrDefault()
                    })   
                ;
        
            return Ok(sales);
        } 


        [HttpPost]
        public IHttpActionResult ActivityLog2()
        {
            //var sales = context.tbl_SalesMaster.Where(a => a.POS == true).ToList()
            //        .Join(context.tbl_SalesDetails.ToList(), o => o.salesMasterId, p=>p.salesMasterId,
            //            (o,p)=> new
            //            {
            //                SalesMaster = o,
            //                SalesDetail = p
            //            })
            //        .Select((a)=> new
            //        {
            //            product = context.tbl_Product.Where(p=>p.productId == a.SalesDetail.productId).SingleOrDefault(),
            //            salesDetail = a.SalesDetail,
            //            SalesMasterInfo = a.SalesMaster,
            //            Ledger = context.tbl_AccountLedger.Where(b=>b.ledgerId == a.SalesMaster.ledgerId ).SingleOrDefault(),
            //            User = context.tbl_User.Where(b=>b.userId == a.SalesMaster.userId).SingleOrDefault(),
            //            Counter = context.tbl_Counter.Where(b=>b.counterId == a.SalesMaster.counterId).SingleOrDefault()
            //        })   
            //    ;
            var sales = context.tbl_Counter.ToList().GroupJoin(
                context.tbl_SalesMaster.Where(a => a.POS == true).ToList(), outer => outer.counterId,
                inner => inner.counterId,
                (o, p) => new
                {
                    saleMaster = p,
                    counter = o,
                }).Select((a) => new
            {
                counter = new
                {
                    id= a.counter.counterId,
                    name = a.counter.counterName,
                    sale = a.saleMaster.Select(b=> new
                    {
                        saleMaster = a.saleMaster.SingleOrDefault(),
                        salesDetail = context.tbl_SalesDetails.Where(c=>c.salesMasterId == b.salesMasterId).ToList()
                            .Select(c => new
                            {
                                detail = c,
                                product = context.tbl_Product.Where(p=>p.productId == c.productId).SingleOrDefault()
                            }),
                        user = context.tbl_User.Where(c=>c.userId == b.userId).SingleOrDefault(),
                        ledger = context.tbl_AccountLedger.Where(c=>c.ledgerId == b.ledgerId).SingleOrDefault(),
                    }).SingleOrDefault(),
                },
            })  ;

            return Ok(sales);
        }

        [HttpGet]
        public HttpResponseMessage GetSalesMen()
        {
            dynamic response = new ExpandoObject();
            try
            {
                DataTable dtblSalesMenSearch = new DataTable();
                EmployeeSP spEmployee = new EmployeeSP();
                dtblSalesMenSearch = spEmployee.SalesmanViewAll();
                response.salesMen = dtblSalesMenSearch;
            }
            catch (Exception ex)
            {

                throw;
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
        [HttpPost]
        public HttpResponseMessage SaveSalesMan(SalesManInfo input)
        {
            dynamic response = new ExpandoObject();
            try
            {
                EmployeeInfo InfoEmployee = new EmployeeInfo();
                EmployeeSP SpEmployee = new EmployeeSP();
                UserInfo infoUser = new UserInfo();
                UserSP spUser = new UserSP();
                string responseSuffix = "";

                InfoEmployee.EmployeeCode = input.EmployeeInfo.EmployeeCode;
                InfoEmployee.DesignationId = Convert.ToDecimal(SpEmployee.SalesmanGetDesignationId());
                InfoEmployee.EmployeeName = input.EmployeeInfo.EmployeeName;
                InfoEmployee.Email = input.EmployeeInfo.Email;
                InfoEmployee.PhoneNumber = input.EmployeeInfo.PhoneNumber;
                InfoEmployee.MobileNumber = input.EmployeeInfo.MobileNumber;
                InfoEmployee.Address = input.EmployeeInfo.Address;
                InfoEmployee.Narration = input.EmployeeInfo.Narration;
                InfoEmployee.Dob = DateTime.Now;
                InfoEmployee.MaritalStatus = string.Empty;
                InfoEmployee.Gender = string.Empty;
                InfoEmployee.Qualification = string.Empty;
                InfoEmployee.BloodGroup = string.Empty;
                InfoEmployee.JoiningDate = MATFinancials.PublicVariables._dtCurrentDate;
                InfoEmployee.TerminationDate = DateTime.Now;
                InfoEmployee.IsActive = input.EmployeeInfo.IsActive;
                InfoEmployee.SalaryType = "Monthly";
                InfoEmployee.DefaultPackageId = 1;
                InfoEmployee.BankName = string.Empty;
                InfoEmployee.BankAccountNumber = string.Empty;
                InfoEmployee.BranchName = string.Empty;
                InfoEmployee.BranchCode = string.Empty;
                InfoEmployee.PanNumber = string.Empty;
                InfoEmployee.PfNumber = string.Empty;
                InfoEmployee.EsiNumber = string.Empty;
                InfoEmployee.PassportNo = string.Empty;
                InfoEmployee.PassportExpiryDate = DateTime.Now;
                InfoEmployee.VisaNumber = string.Empty;
                InfoEmployee.VisaExpiryDate = DateTime.Now;
                InfoEmployee.LabourCardNumber = string.Empty;
                InfoEmployee.LabourCardExpiryDate = DateTime.Now;
                InfoEmployee.Extra1 = string.Empty;
                InfoEmployee.Extra2 = string.Empty;

                infoUser.UserName = input.UserInfo.UserName;
                infoUser.FirstName = input.UserInfo.FirstName;
                infoUser.LastName = input.UserInfo.LastName;
                infoUser.RoleId = input.UserInfo.RoleId;
                infoUser.Active = input.UserInfo.Active;
                infoUser.Password = new MATFinancials.Classes.Security().base64Encode(input.UserInfo.Password);       
                infoUser.ExtraDate = DateTime.Now;
                infoUser.Narration = input.UserInfo.Narration;
                infoUser.Extra1 = string.Empty;
                infoUser.Extra2 = string.Empty;
                infoUser.StoreId = "0";
                if (SpEmployee.EmployeeCodeCheckExistance(input.EmployeeInfo.EmployeeCode, 0) == false)
                {
                    decimal decEmployeeId = SpEmployee.EmployeeAddWithReturnIdentity(InfoEmployee);
                    
                    if (decEmployeeId > 0)
                    {
                        response.ResponseCode = 200;
                        response.ResponseMessage = "Success!";
                    }
                    if (spUser.UserCreationCheckExistence(0, input.UserInfo.UserName) == false)
                    {
                        decimal decUserId = spUser.UserAddWithReturnIdentity(infoUser);
                        SpEmployee.SalesmanUpdate(decEmployeeId, decUserId);
                    }
                    else
                    {
                        decimal userId = context.tbl_User.Where(p => p.userName == input.UserInfo.UserName)
                            .Select(p => p.userId).FirstOrDefault();
                        SpEmployee.SalesmanUpdate(decEmployeeId, userId);
                        responseSuffix = " Use your user login details to log in.";
                    }
                    
                }
                else
                {
                    response.ResponseCode = 403;
                    response.ResponseMessage = "Salesman Code exists!";
                }
                response.ResponseMessage += responseSuffix;
            }
            catch (Exception ex)
            {

                throw;
            }
            
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
        [HttpPost]
        public HttpResponseMessage UpdateSalesMan(SalesManInfo input)
        {
            dynamic response = new ExpandoObject();
            try
            {
                //EmployeeInfo InfoEmployee = new EmployeeInfo();
                EmployeeSP SpEmployee = new EmployeeSP();
                //UserInfo infoUser = new UserInfo();
                //UserSP spUser = new UserSP();

                tbl_Employee SalesManEmployee = context.tbl_Employee.SingleOrDefault(p => p.employeeId == input.EmployeeInfo.EmployeeId);

                if (SalesManEmployee == null)
                {
                    response.ResponseMessage = "Something went wrong!";
                    response.ResponseCode = 403;
                    return Request.CreateResponse(HttpStatusCode.NotFound, (object)response);
                }

                SalesManEmployee.employeeCode = input.EmployeeInfo.EmployeeCode;
                SalesManEmployee.designationId = Convert.ToDecimal(SpEmployee.SalesmanGetDesignationId());
                SalesManEmployee.employeeName = input.EmployeeInfo.EmployeeName;
                SalesManEmployee.email = input.EmployeeInfo.Email;
                SalesManEmployee.phoneNumber = input.EmployeeInfo.PhoneNumber;
                SalesManEmployee.mobileNumber = input.EmployeeInfo.MobileNumber;
                SalesManEmployee.address = input.EmployeeInfo.Address;
                SalesManEmployee.narration = input.EmployeeInfo.Narration;
                SalesManEmployee.isActive = input.EmployeeInfo.IsActive;
                context.SaveChanges();


                decimal salesManEmployeeId = Convert.ToDecimal(SalesManEmployee.extra1);
                tbl_User SalesManUser = context.tbl_User.SingleOrDefault(p => p.userId == salesManEmployeeId);
                if (SalesManUser == null)
                {
                    response.ResponseMessage = "Something went wrong!";
                    response.ResponseCode = 403;
                    return Request.CreateResponse(HttpStatusCode.NotFound, (object)response);
                }
                SalesManUser.userName = input.UserInfo.UserName;
                SalesManUser.firstName = input.UserInfo.FirstName;
                SalesManUser.lastName = input.UserInfo.LastName;
                SalesManUser.roleId = input.UserInfo.RoleId;
                SalesManUser.active = input.UserInfo.Active;
                SalesManUser.narration = input.UserInfo.Narration;
                context.SaveChanges();

                response.ResponseMessage = "Sales Man Updated Successfully!";
                response.ResponseCode = 200;
            }
            catch (Exception ex)
            {

                throw;
            }

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }
    }
    public class SalesManInfo
    {
        public UserInfo UserInfo;
        public EmployeeInfo EmployeeInfo;
    }
}
