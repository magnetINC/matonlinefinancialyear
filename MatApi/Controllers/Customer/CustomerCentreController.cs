﻿using MatApi.Models;
using MatApi.Models.Customer;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Dynamic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Text;
using static ClosedXML.Excel.XLPredefinedFormat;
using System.Activities.Expressions;

//using MatDal;

//using MatDal;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class CustomerCentreController : ApiController
    {

        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        [HttpPost]
        public DataTable GetCustomerList(CustomerCentreSearchModel SearchParameters)
        {
            try
            {
                //decimal decAreaId = 0;
                //decimal decRouteId = 0;
                DataTable dtbl = new DataTable();
                AccountLedgerSP spAccountledger = new AccountLedgerSP();
                //if (SearchParameters.StateId == 0)
                //{
                //    decAreaId = 0;
                //}
                //else
                //{
                //    decAreaId = Convert.ToDecimal(SearchParameters.StateId.ToString());
                //}
                //if (SearchParameters.CityId == 0)
                //{
                //    decRouteId = 0;
                //}
                //else
                //{
                //    decRouteId = Convert.ToDecimal(SearchParameters.CityId.ToString());
                //}


                int AccountStatus = Convert.ToInt32(SearchParameters.Status);
                string CustomerName = SearchParameters.CustomerName;

                dtbl = spAccountledger.AccountLedgerSearchforCustomer(SearchParameters.StateId, SearchParameters.CityId, CustomerName.Trim(), MATFinancials.PublicVariables._dtToDate, AccountStatus);

                if (SearchParameters.OpenBalances == "Open Balances")
                {
                    dtbl = dtbl.AsEnumerable().Where(i => i.Field<decimal>("openingBalance") != 0).CopyToDataTable();
                }
                else
                {
                    dtbl = dtbl.AsEnumerable().CopyToDataTable();
                }

                return dtbl;
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "Cus16:" + ex.Message;
            }

            return null;
        }

        [HttpGet]
        public HttpResponseMessage GetLookUp()
        {
            dynamic resp = new ExpandoObject();
            //Switch Rack to city and Store to Location
            var cities = new RouteSP().RouteViewAll(); 
            //var cities = new RackSP().RackViewAll();
            var states = new AreaSP().AreaViewAll();
            //var states = new GodownSP().GodownViewAll();
            var pricingLevels = new PricingLevelSP().PricingLevelViewAll();

            resp.Cities = cities;
            resp.States = states;
            resp.PricingLevels = pricingLevels;

            return Request.CreateResponse(HttpStatusCode.OK,(object)resp);
        }

        [HttpPost]
        public MatResponse SaveNewCustomer(List<AccountLedgerInfo> input)
        {
            decimal decledgerid = 0;
            MatResponse matResponse = new MatResponse();
            try
            {
                foreach (AccountLedgerInfo item in input)
                {
                    AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
                    AccountLedgerSP spAccountLedger = new AccountLedgerSP();

                    infoAccountLedger.AccountGroupId = 26;
                    infoAccountLedger.LedgerName = item.LedgerName;
                   // infoAccountLedger.IsActive = item.IsActive;
                    infoAccountLedger.OpeningBalance = 0;
                    infoAccountLedger.CrOrDr = "Dr";
                    infoAccountLedger.BankAccountNumber = item.BankAccountNumber;
                    infoAccountLedger.BranchName = item.BranchName;
                    infoAccountLedger.BranchCode = item.BranchCode;
                    infoAccountLedger.Mobile = item.Mobile;
                    infoAccountLedger.Address = item.Address;
                    infoAccountLedger.CreditLimit = Convert.ToDecimal(item.CreditLimit);
                    infoAccountLedger.CreditPeriod = Convert.ToInt32(item.CreditPeriod);
                    infoAccountLedger.Cst = item.Cst;
                    infoAccountLedger.AreaId = Convert.ToDecimal(item.AreaId);
                    infoAccountLedger.RouteId = Convert.ToDecimal(item.RouteId);
                    infoAccountLedger.MailingName = item.MailingName;
                    infoAccountLedger.Phone = item.Phone;
                    infoAccountLedger.Email = item.Email;
                    infoAccountLedger.PricinglevelId = Convert.ToDecimal(item.PricinglevelId);
                    infoAccountLedger.Tin = item.Tin;
                    infoAccountLedger.Pan = "";
                    infoAccountLedger.Narration = "";
                    infoAccountLedger.IsDefault = false;
                    infoAccountLedger.Extra1 = spAccountLedger.AccountLedgerGetMax().ToString()/*input.Extra1*/;    // to save agent code
                    infoAccountLedger.Extra2 = string.Empty;
                    infoAccountLedger.ExtraDate = MATFinancials.PublicVariables._dtCurrentDate;
                    infoAccountLedger.IsActive = true;
                    infoAccountLedger.BillByBill = true;
                    if (spAccountLedger.AccountLedgerCheckExistenceForCustomer(item.LedgerName, 0) == false)
                    {
                        decledgerid = spAccountLedger.AccountLedgerAddForCustomer(infoAccountLedger);
                    }
                    else
                    {
                        string FailMessage = "Ledger name '" + item.LedgerName + "' already exists!";
                        matResponse.ResponseCode = 300;
                        matResponse.ResponseMessage = FailMessage;
                        return matResponse;
                    }
                }
               
            }
            catch (Exception ex)
            {

            }
            matResponse.ResponseCode = 200;
            matResponse.ResponseMessage = "Ledger(s) saved successfully!";
            return matResponse;
        }

        [HttpGet]
        public HttpResponseMessage GetLedgerDetails(System.DateTime DateFrom1, System.DateTime DateTo1, int ledgerId)
        {
            try
            {

                DataSet ledgerDetails = new DataSet();
                AccountLedgerSP SpAccountLedger = new AccountLedgerSP();
                dynamic resp = new ExpandoObject();
                ledgerDetails = SpAccountLedger.GetLedgerDetailsFromSelectedCustomer(DateFrom1, DateTo1, ledgerId);
                resp.LedgerDetails = ledgerDetails;
                resp.OpeningDate = DateFrom1;

                //var DateFrom = DateFrom1.Year + "-" + DateFrom1.Month + "-" + DateFrom1.Day  /*?? MatApi.Models.PublicVariables.FromDate.ToShortDateString()*/;
                //var DateTo = DateTo1.Year + "-" + DateTo1.Month + "-" + DateTo1.Day/*?? MatApi.Models.PublicVariables.ToDate.ToShortDateString()*/;

                //var FyearQuery = string.Format(" (lp.date between CAST('" + DateFrom + "' AS datetime) and CAST('" + DateTo + "' AS datetime)) and ");

                //var query = "select lp.credit, lp.debit, sum(lp.credit - lp.debit) as OpenBalance, lp.date, rcm.narration from tbl_LedgerPosting lp Right join tbl_AccountLedger rm on lp.ledgerId = rm.ledgerId left join tbl_ReceiptMaster rcm on rcm.voucherNo = lp.voucherNo  where "+ FyearQuery + " lp.ledgerId = 160688 group by lp.credit, lp.debit, lp.date, rcm.narration order by  lp.date";

                //DBMatConnection conn = new DBMatConnection();

                //var data = conn.customSelect(query);

                //var ListOpeningBalance = new List<OpeningBalance>();
                //double ListCount = 0;

                //if (data.Rows.Count > 0)
                //{
                //    foreach (DataRow row in data.Rows)
                //    {
                //        ListCount += double.Parse(row["OpenBalance"].ToString());
                //    }
                //}

                //var ListCount12 = ListCount;

                return Request.CreateResponse(HttpStatusCode.OK,(object)resp);
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpGet]
        public HttpResponseMessage GetCustomerDetails(int ledgerId)
        {
            try
            {
                AccountLedgerInfo infoAccountledger = new AccountLedgerInfo();
                AccountLedgerSP spAccountledger = new AccountLedgerSP();
                infoAccountledger = spAccountledger.AccountLedgerViewForCustomer(ledgerId);
                return Request.CreateResponse(HttpStatusCode.OK, (object)infoAccountledger);

            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpPost]
        public string UpdateLedgerDetails(AccountLedgerInfo input)
        {
            try
            {
                AccountLedgerInfo infoAccountLedger = new AccountLedgerInfo();
                AccountLedgerSP spAccountLedger = new AccountLedgerSP();

                infoAccountLedger.AccountGroupId = 26;
                infoAccountLedger.LedgerId = input.LedgerId;
                infoAccountLedger.LedgerName = input.LedgerName;
                infoAccountLedger.IsActive = input.IsActive;
                infoAccountLedger.OpeningBalance = 0;
                infoAccountLedger.CrOrDr = "Dr";
                infoAccountLedger.BankAccountNumber = input.BankAccountNumber;
                infoAccountLedger.BranchName = input.BranchName;
                infoAccountLedger.BranchCode = input.BranchCode;
                infoAccountLedger.Mobile = input.Mobile;
                infoAccountLedger.Address = input.Address;
                infoAccountLedger.CreditLimit = Convert.ToDecimal(input.CreditLimit);
                infoAccountLedger.CreditPeriod = Convert.ToInt32(input.CreditPeriod);
                infoAccountLedger.Cst = input.Cst;
                infoAccountLedger.AreaId = Convert.ToDecimal(input.AreaId);
                infoAccountLedger.RouteId = Convert.ToDecimal(input.RouteId);
                infoAccountLedger.MailingName = input.MailingName;
                infoAccountLedger.Phone = input.Phone;
                infoAccountLedger.Email = input.Email;
                infoAccountLedger.PricinglevelId = Convert.ToDecimal(input.PricinglevelId);
                infoAccountLedger.Tin = input.Tin;
                infoAccountLedger.Pan = "";
                infoAccountLedger.Narration = "";
                infoAccountLedger.IsDefault = false;
                infoAccountLedger.Extra1 = input.Extra1;    // to save agent code
                infoAccountLedger.Extra2 = string.Empty;
                infoAccountLedger.ExtraDate = MATFinancials.PublicVariables._dtCurrentDate;
                infoAccountLedger.IsActive = true;
                infoAccountLedger.BillByBill = true;
                spAccountLedger.AccountLedgerEditForCustomer(infoAccountLedger);
                return "Agent Updated Successfully";
            }
            catch (Exception ex)
            {
                return ex.ToString();
            }
        }

        [HttpGet]
        public string DeleteLedger(int ledgerId)
        {
            try
            {
                var msg = "";

                AccountLedgerSP spAccountLedger = new AccountLedgerSP();
                if (spAccountLedger.AccountLedgerCheckReferences(ledgerId) == -1)
                {
                    msg = "Ledger has an existing transaction";

                    return msg;
                }
                else
                {
                    spAccountLedger.PartyBalanceDeleteByVoucherTypeVoucherNoAndReferenceType(ledgerId.ToString(), 1);
                    spAccountLedger.LedgerPostingDeleteByVoucherTypeAndVoucherNo(ledgerId.ToString(), 1);

                    msg = "Ledger deleted successfully";

                    return msg;
                }
            }
            catch (Exception ex)
            {

            }
            return null;
        }

        [HttpGet]
        public double GetCustomerBalance(decimal ledgerId)
        {
            double balance = 0.0;
            double totalConfirmedOrder = 0.0;
            double totalJournal = 0.0;
            double totalPayments = 0.0;

            DBMatConnection conn = new DBMatConnection();
            string queryTotalOrderConfirmed = string.Format("select isnull(sum(totalAmount),0) from tbl_SalesOrderMaster where ledgerId={0} and voucherTypeId=19", ledgerId);
            string queryTotalJournal = string.Format("select isnull(sum(credit),0) from tbl_ledgerposting where ledgerId={0} and vouchertypeid=6", ledgerId);
            string queryTotalPayments = string.Format("select isnull(sum(credit),0) from tbl_ledgerposting where voucherTypeId!=6 and ledgerId={0}", ledgerId);
            totalConfirmedOrder = Convert.ToDouble(conn.getSingleValue(queryTotalOrderConfirmed)==null?"0": conn.getSingleValue(queryTotalOrderConfirmed));
            totalJournal = Convert.ToDouble(conn.getSingleValue(queryTotalJournal)==null?"0": conn.getSingleValue(queryTotalJournal));
            totalPayments = Convert.ToDouble(conn.getSingleValue(queryTotalPayments)==null?"0": conn.getSingleValue(queryTotalPayments));
            //balance = totalPayments - (totalConfirmedOrder + totalJournal);   //calculation changed based on suggestion from
            balance = (totalPayments + totalJournal) - totalConfirmedOrder;     //madam vic and toyin when wichtech was having balance issue
            return balance;
        }

        [HttpGet]
        public double GetCustomerBalance22(decimal ledgerId)
        {
            double balance = 0.0;
            DBMatConnection conn = new DBMatConnection();
            string query = string.Format("select ISNULL(convert(decimal(18,2),sum(isnull(debit,0) - isnull(credit,0))),0) as openingBalance "+
                                            "from tbl_LedgerPosting lp right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId where al.accountGroupId = 26 and lp.ledgerId = {0} "+
                                            "group by lp.ledgerId",ledgerId);
            balance = Convert.ToDouble(conn.getSingleValue(query));
            return balance;
        }

        [HttpGet]
        public List<OpeningBalance> GetAllAgentLedger()
        {
            DBMatConnection conn = new DBMatConnection();

            var DateFrom0 = MatApi.Models.PublicVariables.FromDate;
            var DateTo0 = MatApi.Models.PublicVariables.ToDate;
            var FYearId = MatApi.Models.PublicVariables.CurrentFinicialYearId;

            DateFrom0 = DateFrom0.AddDays(-1);

            var DateFrom = DateFrom0.Year + "-" + DateFrom0.Month + "-" + DateFrom0.Day  /*?? MatApi.Models.PublicVariables.FromDate.ToShortDateString()*/;
            var DateTo = DateTo0.Year + "-" + DateTo0.Month + "-" + DateTo0.Day/*?? MatApi.Models.PublicVariables.ToDate.ToShortDateString()*/;

            var FyearQuery = string.Format(" (lp.date between CAST('" + DateFrom + "' AS datetime) and CAST('" + DateTo + "' AS datetime)) and ");

            string query = string.Format("select ISNULL(lp.ledgerid, al.ledgerId) as ledgerId,  al.ledgerName, al.extra1, al.routeId, al.areaId, tbl_Route.routeName, tbl_Area.areaName, " +
                                         "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance, " +
                                         "al.isActive AS Active from tbl_LedgerPosting lp " +
                                         " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " +
                                         " left outer join tbl_Route on al.routeId = tbl_Route.routeId " +
                                         " left outer join tbl_Area on al.areaId = tbl_Area.areaId " +
                                         " where  "  + " al.accountGroupId = 26 " +
                                         "  group by ledgerName, al.ledgerId, lp.ledgerId, al.isActive, al.routeId, al.areaId, al.extra1,tbl_Route.routeName, tbl_Area.areaName ");




            //  var Result = context.tbl_LedgerPosting.Where(lp => lp.ledgerId)

            ///+ " lp.date BETWEEN " + DateFrom + " and " + DateTo

            /*string query = string.Format("select ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, al.extra1, al.routeId, al.areaId, tbl_Rack.RackName, tbl_Godown.godownName, " +
                                         "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance, " +
                                         "al.isActive AS Active from tbl_LedgerPosting lp " +
                                         "right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " +
                                         "left outer join tbl_Rack on al.routeId = tbl_Rack.RackId " +
                                         "left outer join tbl_Godown on al.areaId = tbl_Godown.godownId  " +
                                         "where al.accountGroupId = 26 " +
                                         "group by ledgerName, al.ledgerId, lp.ledgerId, al.isActive, al.routeId, al.areaId, al.extra1,tbl_Rack.RackName, tbl_Godown.godownName");*/

            var data = conn.customSelect(query);

            var ListOpeningBalance = new List<OpeningBalance>();

            if (data.Rows.Count > 0)
            {
                foreach (DataRow row in data.Rows)
                {
                    var newOpeningAmt = new OpeningBalance();
                    newOpeningAmt.Active = row["Active"];
                    newOpeningAmt.areaId = row["areaId"];
                    newOpeningAmt.areaName = row["areaName"];
                    newOpeningAmt.extra1 = row["extra1"]; 
                    newOpeningAmt.ledgerId = row["ledgerId"];
                    newOpeningAmt.ledgerName = row["ledgerName"];
                    newOpeningAmt.extra1 = row["extra1"]; 
                    newOpeningAmt.routeId = row["routeId"];
                    newOpeningAmt.routeName = row["routeName"];
                    try
                    {
                        var Amt = double.Parse(row["openingBalance"].ToString());
                        if(Amt != null && Amt != 0)
                        {
                            if(Amt.ToString().Contains("-"))
                            {
                                newOpeningAmt.balanceState = "Credit";
                            }
                            else
                            {
                                newOpeningAmt.balanceState = "Debit";
                            }
                          newOpeningAmt.openingBalance = Amt;

                        }
                        else
                        {
                            newOpeningAmt.openingBalance = 0;
                            newOpeningAmt.balanceState = "None";
                        }
                    }
                    catch (Exception ex)
                    {
                        newOpeningAmt.openingBalance = 0;
                    }


                    ListOpeningBalance.Add(newOpeningAmt);
                }
            }


            //query = string.Format("select ISNULL(lp.ledgerid, al.ledgerId) as ledgerId,  al.ledgerName, al.extra1, al.routeId, al.areaId, tbl_Route.routeName, tbl_Area.areaName, " +
            //                       "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance, " +
            //                       "al.isActive AS Active from tbl_LedgerPosting lp " +
            //                       " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " +
            //                       " left outer join tbl_Route on al.routeId = tbl_Route.routeId " +
            //                       " left outer join tbl_Area on al.areaId = tbl_Area.areaId " +
            //                       " where  " + FyearQuery + " al.accountGroupId = 26 "+
            //                       "  group by ledgerName, al.ledgerId, lp.ledgerId, al.isActive, al.routeId, al.areaId, al.extra1,tbl_Route.routeName, tbl_Area.areaName ");

            //var Tabledata = conn.customSelect(query);

            //var ListOpeningBalance1 = new List<OpeningBalance>();

            //if (Tabledata.Rows.Count > 0)
            //{
            //    foreach (DataRow row in Tabledata.Rows)
            //    {
            //        var newOpeningAmt = new OpeningBalance();
            //        newOpeningAmt.Active = row["Active"];
            //        newOpeningAmt.areaId = row["areaId"];
            //        newOpeningAmt.areaName = row["areaName"];
            //        newOpeningAmt.extra1 = row["extra1"];
            //        newOpeningAmt.ledgerId = row["ledgerId"];
            //        newOpeningAmt.ledgerName = row["ledgerName"];
            //        newOpeningAmt.extra1 = row["extra1"];
            //        newOpeningAmt.routeId = row["routeId"];
            //        newOpeningAmt.routeName = row["routeName"];

            //        try
            //        {
            //            var Amt = double.Parse(row["openingBalance"].ToString());
            //            if (Amt != null && (Amt > 0 || Amt.ToString().Contains("-")))
            //            {
            //                newOpeningAmt.openingBalance = Amt;
            //            }
            //            else
            //            {
            //                newOpeningAmt.openingBalance = 0;
            //            }
            //        }
            //        catch (Exception ex)
            //        {
            //            newOpeningAmt.openingBalance = 0;
            //        }

            //        ListOpeningBalance1.Add(newOpeningAmt);
            //    }


            //    for (var i = 0; i <= ListOpeningBalance.Count - 1; i++)
            //    {
            //        var LedgerId = ListOpeningBalance[i].ledgerId;
            //        var LedgerList = ListOpeningBalance1.Where(lb => lb.ledgerId == LedgerId).FirstOrDefault();
            //        if (LedgerList != null)
            //        {
            //            if (Convert.ToDouble(LedgerList.openingBalance) > 0 || LedgerList.openingBalance.ToString().Contains("-"))
            //            {
            //                ListOpeningBalance[i].openingBalance = Convert.ToDouble(LedgerList.openingBalance);
            //            }
            //            else
            //            {
            //                ListOpeningBalance[i].openingBalance = 0;
            //                //ListOpeningBalance[i].openingBalance -= Convert.ToDouble(LedgerList.openingBalance);
            //            }
            //        }
            //        else
            //        {
            //            ListOpeningBalance[i].openingBalance = 0;
            //        }
            //    }




            //    DateFrom = DateFrom0.Year + "-" + DateFrom0.Month + "-" + DateFrom0.Day  /*?? MatApi.Models.PublicVariables.FromDate.ToShortDateString()*/;
            //    DateTo = DateTo0.Year + "-" + DateTo0.Month + "-" + DateTo0.Day/*?? MatApi.Models.PublicVariables.ToDate.ToShortDateString()*/;

            //    var FinancialYear = context.tbl_FinancialYear.FirstOrDefault();

            //    if (FinancialYear != null)
            //    {
            //        DateTo0 = DateFrom0.AddDays(-1);

            //        DateFrom = FinancialYear.fromDate.Value.Year + "-" + FinancialYear.fromDate.Value.Month + "-" + FinancialYear.fromDate.Value.Day;
            //        DateTo = DateTo0.Year + "-" + DateTo0.Month + "-" + DateTo0.Day;

            //        FyearQuery = string.Format("  where (lp.date between CAST('" + DateFrom + "' AS datetime) and CAST('" + DateTo + "' AS datetime)) ");

            //        query = string.Format("select lp.ledgerId, convert(decimal(18, 2), sum(lp.debit)) as openDebit, convert(decimal(18, 2), sum(lp.credit)) as openCredit  from  tbl_LedgerPosting lp " + " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " + FyearQuery + "  and al.accountGroupId = 26 group by lp.ledgerId");

            //        var data1 = conn.customSelect(query);

            //        var ListOpenBalan = new List<OpenBalan>();

            //        if (data1.Rows.Count > 0)
            //        {
            //            foreach (DataRow row in data1.Rows)
            //            {
            //                var newOpeningAmt = new OpenBalan();
            //                newOpeningAmt.openCredit = row["openCredit"];
            //                newOpeningAmt.openDebit = row["openDebit"];
            //                newOpeningAmt.ledgerId = row["ledgerId"];
            //                if (string.IsNullOrEmpty(Convert.ToString(newOpeningAmt.openCredit)) || string.IsNullOrWhiteSpace(Convert.ToString(newOpeningAmt.openCredit)) || Convert.ToString(newOpeningAmt.openCredit) == "{}")
            //                {
            //                    newOpeningAmt.openCredit = 0;
            //                }
            //                if (string.IsNullOrEmpty(Convert.ToString(newOpeningAmt.openDebit)) || string.IsNullOrWhiteSpace(Convert.ToString(newOpeningAmt.openDebit)) || Convert.ToString(newOpeningAmt.openDebit) == "{}")
            //                {
            //                    newOpeningAmt.openDebit = 0;
            //                }
            //                ListOpenBalan.Add(newOpeningAmt);
            //            }

            //        }

            //        for (var i = 0; i <= ListOpeningBalance.Count - 1; i++)
            //        {
            //            var LedgerId = ListOpeningBalance[i].ledgerId;
            //            var LedgerList = ListOpenBalan.Where(lb => lb.ledgerId == LedgerId).FirstOrDefault();
            //            if (LedgerList != null)
            //            {
            //                var BalanceTop = Convert.ToDouble(LedgerList.openCredit) - Convert.ToDouble(LedgerList.openDebit);

            //                ListOpeningBalance[i].openingBalance = Convert.ToDouble(ListOpeningBalance[i].openingBalance) + BalanceTop;

            //                //if (Convert.ToDouble(ListOpeningBalance[i].openingBalance) > 0)
            //                //{

            //                //}
            //                //else
            //                //{
            //                //    ListOpeningBalance[i].openingBalance = 0;
            //                //}
            //            }
            //        }

            //    }


            //}
            return ListOpeningBalance;
        }

        [HttpGet]
        public List<OpeningBalance> GetAllAgentLedger1(System.DateTime DateFrom1, System.DateTime DateTo1, int FYearId =0)
        {
            DBMatConnection conn = new DBMatConnection();

            var DateFrom = DateFrom1.Year + "-" + DateFrom1.Month + "-"+ DateFrom1.Day  /*?? MatApi.Models.PublicVariables.FromDate.ToShortDateString()*/;
            var DateTo = DateTo1.Year + "-" + DateTo1.Month + "-" + DateTo1.Day/*?? MatApi.Models.PublicVariables.ToDate.ToShortDateString()*/;

            var FyearQuery = string.Format("  (lp.date between CAST('" + DateFrom + "' AS datetime) and CAST('"+ DateTo + "' AS datetime))  and  ");

            string query = string.Format("select ISNULL(lp.ledgerid, al.ledgerId) as ledgerId,  al.ledgerName, al.extra1, al.routeId, al.areaId, tbl_Route.routeName, tbl_Area.areaName, " +
                                         "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance, " +
                                         "al.isActive AS Active from tbl_LedgerPosting lp " +
                                         " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " +
                                         " left outer join tbl_Route on al.routeId = tbl_Route.routeId " +
                                         " left outer join tbl_Area on al.areaId = tbl_Area.areaId " +
                                         " where  " +  " al.accountGroupId = 26 " +
                                         "  group by ledgerName, al.ledgerId, lp.ledgerId, al.isActive, al.routeId, al.areaId, al.extra1,tbl_Route.routeName, tbl_Area.areaName ");

            //  var Result = context.tbl_LedgerPosting.Where(lp => lp.ledgerId)

            ///+ " lp.date BETWEEN " + DateFrom + " and " + DateTo

            /*string query = string.Format("select ISNULL(lp.ledgerid, al.ledgerId) as ledgerId, al.ledgerName, al.extra1, al.routeId, al.areaId, tbl_Rack.RackName, tbl_Godown.godownName, " +
                                         "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance, " +
                                         "al.isActive AS Active from tbl_LedgerPosting lp " +
                                         "right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " +
                                         "left outer join tbl_Rack on al.routeId = tbl_Rack.RackId " +
                                         "left outer join tbl_Godown on al.areaId = tbl_Godown.godownId  " +
                                         "where al.accountGroupId = 26 " +
                                         "group by ledgerName, al.ledgerId, lp.ledgerId, al.isActive, al.routeId, al.areaId, al.extra1,tbl_Rack.RackName, tbl_Godown.godownName");*/

            var data = conn.customSelect(query);

            var ListOpeningBalance = new List<OpeningBalance>();

            if (data.Rows.Count > 0)
            {
                foreach (DataRow row in data.Rows)
                {
                    var newOpeningAmt = new OpeningBalance();
                    newOpeningAmt.Active = row["Active"];
                    newOpeningAmt.areaId = row["areaId"];
                    newOpeningAmt.areaName = row["areaName"];
                    newOpeningAmt.extra1 = row["extra1"];
                    newOpeningAmt.ledgerId = row["ledgerId"];
                    newOpeningAmt.ledgerName = row["ledgerName"];
                    newOpeningAmt.extra1 = row["extra1"];
                    newOpeningAmt.routeId = row["routeId"];
                    newOpeningAmt.routeName = row["routeName"];
                    try
                    {
                        var Amt = double.Parse(row["openingBalance"].ToString());
                        if (Amt != null && (Amt > 0 || Amt.ToString().Contains("-")))
                        {
                            newOpeningAmt.openingBalance = 0;
                        }
                        else
                        {
                            newOpeningAmt.openingBalance = 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        newOpeningAmt.openingBalance = 0;
                    }

                    ListOpeningBalance.Add(newOpeningAmt);
                }
            }

            query = string.Format("select ISNULL(lp.ledgerid, al.ledgerId) as ledgerId,  al.ledgerName, al.extra1, al.routeId, al.areaId, tbl_Route.routeName, tbl_Area.areaName, " +
                                   "ISNULL(convert(decimal(18, 2), sum(isnull(debit, 0) - isnull(credit, 0))), 0) as openingBalance, " +
                                   "al.isActive AS Active from tbl_LedgerPosting lp " +
                                   " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId " +
                                   " left outer join tbl_Route on al.routeId = tbl_Route.routeId " +
                                   " left outer join tbl_Area on al.areaId = tbl_Area.areaId " +
                                   " where  " + FyearQuery + " al.accountGroupId = 26 " +
                                   "  group by ledgerName, al.ledgerId, lp.ledgerId, al.isActive, al.routeId, al.areaId, al.extra1,tbl_Route.routeName, tbl_Area.areaName ");

            var Tabledata = conn.customSelect(query);

            var ListOpeningBalance1 = new List<OpeningBalance>();

            if (Tabledata.Rows.Count > 0)
            {
                foreach (DataRow row in Tabledata.Rows)
                {
                    var newOpeningAmt = new OpeningBalance();
                    newOpeningAmt.Active = row["Active"];
                    newOpeningAmt.areaId = row["areaId"];
                    newOpeningAmt.areaName = row["areaName"];
                    newOpeningAmt.extra1 = row["extra1"];
                    newOpeningAmt.ledgerId = row["ledgerId"];
                    newOpeningAmt.ledgerName = row["ledgerName"];
                    newOpeningAmt.extra1 = row["extra1"];
                    newOpeningAmt.routeId = row["routeId"];
                    newOpeningAmt.routeName = row["routeName"];

                    try
                    {
                        var Amt = double.Parse(row["openingBalance"].ToString());
                        if (Amt != null && (Amt > 0 || Amt.ToString().Contains("-")))
                        {
                            newOpeningAmt.openingBalance = Amt;
                        }
                        else
                        {
                            newOpeningAmt.openingBalance = 0;
                        }
                    }
                    catch (Exception ex)
                    {
                        newOpeningAmt.openingBalance = 0;
                    }

                    ListOpeningBalance1.Add(newOpeningAmt);
                }


                for (var i = 0; i <= ListOpeningBalance.Count-1; i++)
                {
                    var LedgerId = ListOpeningBalance[i].ledgerId;
                    var LedgerList = ListOpeningBalance1.Where(lb => lb.ledgerId == LedgerId).FirstOrDefault();
                    if (LedgerList != null)
                    {
                        if (Convert.ToDouble(LedgerList.openingBalance) > 0 || LedgerList.openingBalance.ToString().Contains("-"))
                        {
                            ListOpeningBalance[i].openingBalance = Convert.ToDouble(LedgerList.openingBalance);
                        }
                        else
                        {
                            ListOpeningBalance[i].openingBalance = 0;
                        }
                    }
                    else
                    {
                        ListOpeningBalance[i].openingBalance = 0;
                    }
                }

                DateFrom = DateFrom1.Year + "-" + DateFrom1.Month + "-" + DateFrom1.Day  /*?? MatApi.Models.PublicVariables.FromDate.ToShortDateString()*/;
                DateTo = DateTo1.Year + "-" + DateTo1.Month + "-" + DateTo1.Day/*?? MatApi.Models.PublicVariables.ToDate.ToShortDateString()*/;

                var FinancialYear = context.tbl_FinancialYear.FirstOrDefault();

                if (FinancialYear != null)
                {
                    DateTo1 = DateFrom1.AddDays(-1);

                    DateFrom = FinancialYear.fromDate.Value.Year + "-" + FinancialYear.fromDate.Value.Month + "-" + FinancialYear.fromDate.Value.Day;
                    DateTo = DateTo1.Year + "-" + DateTo1.Month + "-" + DateTo1.Day;

                     FyearQuery = string.Format("  where (lp.date between CAST('" + DateFrom + "' AS datetime) and CAST('" + DateTo + "' AS datetime)) ");

                     query = string.Format("select lp.ledgerId, convert(decimal(18, 2), sum(lp.debit)) as openDebit, convert(decimal(18, 2), sum(lp.credit)) as openCredit  from  tbl_LedgerPosting lp " + " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId "  + FyearQuery + "  and al.accountGroupId = 26 group by lp.ledgerId");

                    var data1 = conn.customSelect(query);

                    var ListOpenBalan = new List<OpenBalan>();
                   
                    if (data1.Rows.Count > 0)
                    {
                        foreach (DataRow row in data1.Rows)
                        {
                            var newOpeningAmt = new OpenBalan();
                            newOpeningAmt.openCredit = row["openCredit"];
                            newOpeningAmt.openDebit = row["openDebit"];
                            newOpeningAmt.ledgerId = row["ledgerId"];
                            if (string.IsNullOrEmpty(Convert.ToString(newOpeningAmt.openCredit)) || string.IsNullOrWhiteSpace(Convert.ToString(newOpeningAmt.openCredit)) || Convert.ToString(newOpeningAmt.openCredit) == "{}")
                            {
                                newOpeningAmt.openCredit = 0;
                            }
                            if (string.IsNullOrEmpty(Convert.ToString(newOpeningAmt.openDebit)) || string.IsNullOrWhiteSpace(Convert.ToString(newOpeningAmt.openDebit)) || Convert.ToString(newOpeningAmt.openDebit) == "{}")
                            {
                                newOpeningAmt.openDebit = 0;
                            }
                            ListOpenBalan.Add(newOpeningAmt);
                        }
                      
                    }


                    for (var i = 0; i <= ListOpeningBalance.Count - 1; i++)
                    {
                        var LedgerId = ListOpeningBalance[i].ledgerId;
                        var LedgerList = ListOpenBalan.Where(lb => lb.ledgerId == LedgerId).FirstOrDefault();
                        if (LedgerList != null)
                        {
                            var BalanceTop = Convert.ToDouble(LedgerList.openCredit) - Convert.ToDouble(LedgerList.openDebit);

                            ListOpeningBalance[i].openingBalance = Convert.ToDouble(ListOpeningBalance[i].openingBalance) + BalanceTop;

                            //if (Convert.ToDouble(ListOpeningBalance[i].openingBalance) > 0)
                            //{
                               
                            //}
                            //else
                            //{
                            //    ListOpeningBalance[i].openingBalance = 0;
                            //}
                        }
                    }
                }
            }

            return ListOpeningBalance;
        }

        [HttpGet]
        public OpenBalan GetLedgerOpeningBalance(System.DateTime DateFrom1, System.DateTime DateTo1, int ledgerId = 0)
        {
            DBMatConnection conn = new DBMatConnection();

            var DateFrom = DateFrom1.Year + "-" + DateFrom1.Month + "-" + DateFrom1.Day  /*?? MatApi.Models.PublicVariables.FromDate.ToShortDateString()*/;
            var DateTo = DateTo1.Year + "-" + DateTo1.Month + "-" + DateTo1.Day/*?? MatApi.Models.PublicVariables.ToDate.ToShortDateString()*/;

            var FinancialYear = context.tbl_FinancialYear.FirstOrDefault();

            if (FinancialYear != null)
            {
                DateTo1 = DateFrom1.AddDays(-1);

                DateFrom = FinancialYear.fromDate.Value.Year + "-" + FinancialYear.fromDate.Value.Month + "-" + FinancialYear.fromDate.Value.Day;
                DateTo = DateTo1.Year + "-" + DateTo1.Month + "-" + DateTo1.Day;

                var FyearQuery = string.Format("  where (lp.date between CAST('" + DateFrom + "' AS datetime) and CAST('" + DateTo + "' AS datetime)) and lp.ledgerId=" + ledgerId);

                var query = string.Format("select convert(decimal(18, 2), sum(lp.debit)) as openDebit, convert(decimal(18, 2), sum(lp.credit)) as openCredit  from  tbl_LedgerPosting lp " + " right join tbl_AccountLedger al on lp.ledgerId = al.ledgerId and al.ledgerId=" + ledgerId  + FyearQuery + "  and al.accountGroupId = 26 ");

                var data1 = conn.customSelect(query);

                var ListOpeningBalance = new List<OpenBalan>();
                var newOpeningAmt = new OpenBalan();
                if (data1.Rows.Count > 0)
                {
                    foreach (DataRow row in data1.Rows)
                    {
                        newOpeningAmt.openCredit = row["openCredit"];
                        newOpeningAmt.openDebit = row["openDebit"];
                        if(string.IsNullOrEmpty(Convert.ToString(newOpeningAmt.openCredit)) || string.IsNullOrWhiteSpace(Convert.ToString(newOpeningAmt.openCredit)) || Convert.ToString(newOpeningAmt.openCredit) == "{}")
                        {
                            newOpeningAmt.openCredit = 0;
                        }
                        if (string.IsNullOrEmpty(Convert.ToString(newOpeningAmt.openDebit)) || string.IsNullOrWhiteSpace(Convert.ToString(newOpeningAmt.openDebit)) || Convert.ToString(newOpeningAmt.openDebit) == "{}")
                        {
                            newOpeningAmt.openDebit = 0;
                        }
                    }
                }

                return newOpeningAmt;
            }

            return null;
        }
        public class OpenBalan
        {
            public dynamic openDebit { get; set; }
            public dynamic openCredit { get; set; }

            public dynamic ledgerId { get; set; }
        }
        public class OpeningBalance
        {
            public System.DateTime date { get; set; }
            public dynamic ledgerId { get; set; }
            public dynamic YearId { get; set; }
            public dynamic ledgerName { get; set; }
            public dynamic extra1 { get; set; }
            public dynamic routeId { get; set; }
            public dynamic areaId { get; set; }
            public dynamic routeName { get; set; }
            public dynamic areaName { get; set; }
            public dynamic mailingName { get; set; }
            
            public dynamic openingBalance { get; set; }
            public dynamic Active { get; set; }
            public dynamic balanceState { get; set; }
        }
        public class OpeningBalanceDTO
        {
            public System.DateTime date { get; set; }
            public dynamic ledgerId { get; set; }
            public dynamic YearId { get; set; }
            public dynamic ledgerName { get; set; }
            public dynamic extra1 { get; set; }
            public dynamic routeId { get; set; }
            public dynamic areaId { get; set; }
            public dynamic routeName { get; set; }
            public dynamic areaName { get; set; }
            public dynamic mailingName { get; set; }

            public dynamic openingBalance { get; set; }
            public dynamic Active { get; set; }
            public dynamic balanceState { get; set; }
        }

        [HttpGet]
        public MatResponse GetCustomerTransactions()
        {
            MatResponse response = new MatResponse();
            try
            {
                DBMatConnection con = new DBMatConnection();
                List<CustomerTransactionResponseWrapperVM> output = new List<CustomerTransactionResponseWrapperVM>();

                var salesOrders = "";
                var receipts = "";
                var deliveryNotes = "";
                var invoices = "";
                var salesReturns = "";

                string salesOrderQuery = string.Format("select so.salesOrderMasterId,so.voucherNo,so.date," +
                                                       "so.totalAmount,al.ledgerId,al.ledgerName,us.userId," +
                                                       "concat(us.firstName,' ',us.lastName)as fullname " +
                                                       "from tbl_salesordermaster as so " +
                                                       "inner join tbl_AccountLedger as al on so.ledgerId = al.ledgerId " +
                                                       "inner join tbl_user as us on so.userId = us.userId");
                DataTable salesOrderDt = con.customSelect(salesOrderQuery);
                List<CustomerTransactionResponseVM> salesOrderVM = new List<CustomerTransactionResponseVM>();
                foreach (DataRow row in salesOrderDt.Rows)
                {
                    var additionalDetails = "Invoice No: "+ row["voucherNo"].ToString();
                    salesOrderVM.Add(new CustomerTransactionResponseVM
                    {
                        DoneByUserId = row["userid"].ToString(),
                        DoneByUserName = row["fullname"].ToString(),
                        LedgerId = row["ledgerid"].ToString(),
                        LedgerName = row["ledgername"].ToString(),
                        AdditionalDetails = additionalDetails,
                        TransactionAmount = row["totalAmount"].ToString(),
                        TransactionDate = row["date"].ToString(),
                        TransactionId = row["salesordermasterid"].ToString(),
                        TransactionNumber = row["voucherno"].ToString(),
                        TransactionType = "SALES_ORDER"
                    });
                }
                output.Add(new CustomerTransactionResponseWrapperVM
                {
                    Transactions = salesOrderVM,
                    TransactionType = "SALES_ORDER"
                });

                string receiptQuery = string.Format("select rm.receiptMasterId,rm.voucherNo,rm.date,rm.totalAmount," +
                                                    "al.ledgerId,al.ledgerName,us.userId,concat(us.firstName,' ',us.lastName)as fullname " +
                                                     "from tbl_receiptmaster as rm " +
                                                     "inner join tbl_AccountLedger as al on rm.ledgerId = al.ledgerId " +
                                                     "inner join tbl_User as us on rm.userId = us.userId");
                DataTable receiptDt = con.customSelect(receiptQuery);
                List<CustomerTransactionResponseVM> receiptVM = new List<CustomerTransactionResponseVM>();
                foreach (DataRow row in receiptDt.Rows)
                {
                    salesOrderVM.Add(new CustomerTransactionResponseVM
                    {
                        DoneByUserId = row["userid"].ToString(),
                        DoneByUserName = row["fullname"].ToString(),
                        LedgerId = row["ledgerid"].ToString(),
                        LedgerName = row["ledgername"].ToString(),
                        TransactionAmount = row["totalAmount"].ToString(),
                        TransactionDate = row["date"].ToString(),
                        TransactionId = row["receiptMasterId"].ToString(),
                        TransactionNumber = row["voucherno"].ToString(),
                        TransactionType = "RECEIPT"
                    });
                }
                output.Add(new CustomerTransactionResponseWrapperVM
                {
                    Transactions = receiptVM,
                    TransactionType = "RECEIPT"
                });

                string deliveryNoteQuery = string.Format("select dn.deliveryNoteMasterId,dn.voucherNo,dn.date," +
                                                         "dn.totalAmount,al.ledgerId,al.ledgerName,us.userId,concat(us.firstName,' ',us.lastName)as fullname " +
                                                         "from tbl_DeliveryNoteMaster as dn " +
                                                         "inner join tbl_AccountLedger as al on dn.ledgerId = al.ledgerId " +
                                                         "inner join tbl_User as us on dn.userId = us.userId");
                DataTable deliveryNoteDt = con.customSelect(deliveryNoteQuery);
                List<CustomerTransactionResponseVM> deliveryNoteVM = new List<CustomerTransactionResponseVM>();
                foreach (DataRow row in deliveryNoteDt.Rows)
                {
                    deliveryNoteVM.Add(new CustomerTransactionResponseVM
                    {
                        DoneByUserId = row["userid"].ToString(),
                        DoneByUserName = row["fullname"].ToString(),
                        LedgerId = row["ledgerid"].ToString(),
                        LedgerName = row["ledgername"].ToString(),
                        TransactionAmount = row["totalAmount"].ToString(),
                        TransactionDate = row["date"].ToString(),
                        TransactionId = row["deliverynoteMasterId"].ToString(),
                        TransactionNumber = row["voucherno"].ToString(),
                        TransactionType = "DELIVERY_NOTE"
                    });
                }
                output.Add(new CustomerTransactionResponseWrapperVM
                {
                    Transactions = deliveryNoteVM,
                    TransactionType = "DELIVERY_NOTE"
                });

                string invoiceQuery = string.Format("select si.salesMasterId,si.voucherNo,si.date,si.grandTotal," +
                                                    "al.ledgerId,al.ledgerName,us.userId,concat(us.firstName,' ',us.lastName)as fullname " +
                                                    "from tbl_salesmaster as si " +
                                                    "inner join tbl_AccountLedger as al on si.ledgerId = al.ledgerId " +
                                                    "inner join tbl_User as us on si.userId = us.userId");
                DataTable invoiceDt = con.customSelect(invoiceQuery);
                List<CustomerTransactionResponseVM> salesInvoiceVM = new List<CustomerTransactionResponseVM>();
                foreach (DataRow row in invoiceDt.Rows)
                {
                    salesInvoiceVM.Add(new CustomerTransactionResponseVM
                    {
                        DoneByUserId = row["userid"].ToString(),
                        DoneByUserName = row["fullname"].ToString(),
                        LedgerId = row["ledgerid"].ToString(),
                        LedgerName = row["ledgername"].ToString(),
                        TransactionAmount = row["grandTotal"].ToString(),
                        TransactionDate = row["date"].ToString(),
                        TransactionId = row["salesMasterId"].ToString(),
                        TransactionNumber = row["voucherno"].ToString(),
                        TransactionType = "SALES_INVOICE"
                    });
                }
                output.Add(new CustomerTransactionResponseWrapperVM
                {
                    Transactions = salesInvoiceVM,
                    TransactionType = "SALES_INVOICE"
                });

                string salesReturnQuery = string.Format("Select * from tbl_SalesReturnMaster");
                DataTable salesReturnDt = con.customSelect(salesReturnQuery);
                var users = context.tbl_User.ToList();
                var ledgers = context.tbl_AccountLedger.ToList();
                var rejections = context.tbl_RejectionInMaster.ToList()
                    .Join(ledgers,
                        r => r.ledgerId, l => l.ledgerId,
                        (rejectionMaster, ledger) => new
                        {
                            RejectionMaster = rejectionMaster, 
                            Ledger = ledger 
                        }).Join(users,rl=>rl.RejectionMaster.userId, u=>u.userId, 
                        (rl, u)=> new
                        {
                            RejectionMasterLedger = rl ,
                            User = u
                        })
                    ;
                List<CustomerTransactionResponseVM> rejectionVm = new List<CustomerTransactionResponseVM>();

                foreach (var i in rejections)
                {
                    rejectionVm.Add(new CustomerTransactionResponseVM()
                    {
                        DoneByUserId = i.User.userId.ToString(CultureInfo.InvariantCulture),
                        DoneByUserName = i.User.userName,
                        LedgerId = i.RejectionMasterLedger.Ledger.ledgerId.ToString(CultureInfo.InvariantCulture),
                        LedgerName =  i.RejectionMasterLedger.Ledger.ledgerName,
                        TransactionAmount = i.RejectionMasterLedger.RejectionMaster.totalAmount.ToString(),
                        TransactionDate = Convert.ToDateTime(i.RejectionMasterLedger.RejectionMaster.date).ToString(""),
                        TransactionId = i.RejectionMasterLedger.RejectionMaster.rejectionInMasterId.ToString(),
                        TransactionNumber = i.RejectionMasterLedger.RejectionMaster.voucherNo,
                        TransactionType = "Rejection In"
                        
                    });
                }
                output.Add(new CustomerTransactionResponseWrapperVM
                {
                    Transactions = rejectionVm,
                    TransactionType = "Rejection In"
                });


                    

                // sales return 
                var salesReturnMaster = context.tbl_SalesReturnMaster.ToList();
                var returns = salesReturnMaster.Join(ledgers, sr => sr.ledgerId, l => l.ledgerId,
                        (sr, l) => new
                        {
                            SalesReturnMaster = sr,
                            Ledger = l
                        })
                    .Join(users, srl => srl.SalesReturnMaster.userId, u => u.userId,
                        (srl, u) => new
                        {
                            SalesReturnMasterLedger = srl,
                            User = u
                        }
                    );

                List<CustomerTransactionResponseVM> returnsVm = new List<CustomerTransactionResponseVM>();

                foreach (var i in returns)
                {
                    rejectionVm.Add(new CustomerTransactionResponseVM()
                    {
                        DoneByUserId = i.User.userId.ToString(CultureInfo.InvariantCulture),
                        DoneByUserName = i.User.userName,
                        LedgerId = i.SalesReturnMasterLedger.Ledger.ledgerId.ToString(CultureInfo.InvariantCulture),
                        LedgerName =  i.SalesReturnMasterLedger.Ledger.ledgerName,
                        TransactionAmount = i.SalesReturnMasterLedger.SalesReturnMaster.totalAmount.ToString(),
                        TransactionDate = Convert.ToDateTime(i.SalesReturnMasterLedger.SalesReturnMaster.date).ToString(""),
                        TransactionId = i.SalesReturnMasterLedger.SalesReturnMaster.salesReturnMasterId.ToString(),
                        TransactionNumber = i.SalesReturnMasterLedger.SalesReturnMaster.voucherNo,
                        TransactionType = "Sales Returned"
                        
                    });
                }
                output.Add(new CustomerTransactionResponseWrapperVM
                {
                    Transactions = returnsVm,
                    TransactionType = "Sales Returned"
                });

                response.ResponseCode = 200;
                response.Response = output;
            }
            catch(Exception ex)
            {
                response.ResponseCode = 500;
            }

            return response;
        }
    }

    public class CustomerTransactionResponseWrapperVM
    {
        public List<CustomerTransactionResponseVM> Transactions { get; set; }
        public string TransactionType { get; set; }
    }

    public class CustomerTransactionResponseVM
    {
        public string TransactionId { get; set; }
        public string TransactionNumber { get; set; }
        public string LedgerId { get; set; }
        public string LedgerName { get; set; }
        public string DoneByUserId { get; set; }
        public string DoneByUserName { get; set; }
        public string AdditionalDetails { get; set; }
        public string TransactionAmount { get; set; }
        public string TransactionDate { get; set; }
        public string TransactionType { get; set; }
    }
}
