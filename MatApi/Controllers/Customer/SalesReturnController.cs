﻿using MATFinancials;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using MatApi.DBModel;
using MatApi.Dto;
using MatApi.Enums;
using MatApi.Models;
using Microsoft.Ajax.Utilities;
using PublicVariables = MATFinancials.PublicVariables;
using EntityState = System.Data.Entity.EntityState;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SalesReturnController : ApiController
    {
        decimal decinvoiceno = 0;
        decimal decSalesReturnVoucherTypeId = 20;
        decimal decSalesReturnSuffixPrefixId = 0;
        bool isSalesReturnFormActive = false;
        bool isAutomatic = false;
        static bool isEnterIntoComboSelectn = false;
        string strVoucherNo = string.Empty;
        string strCashOrPartyAccount = string.Empty;
        string strPricinglevel;
        string strSalesMan;
        string strSalesAccount;
        string strSalesReturnIdToEdit = string.Empty;
        string strProductCode = string.Empty;
        decimal salesReturnMasterId = 0;
        decimal decQty = 0;
        decimal decRate = 0;
        bool isFromSalesReturnRegister = false;
        bool isFromSalesReturnReport = false;
        decimal decTotalAmounForSaveCheck = 0;
        decimal decTotal = 0;
        decimal decProductId = 0;
        decimal decExchangeRate = 0;
        decimal decAgainstVoucherTypeId = 0;
        decimal decTotalBillTaxAmount = 0;
        decimal decTotalCessTaxamount = 0;
        string strAgainstVoucherNo = string.Empty;
        string strAgainstInvoiceNo = string.Empty;
        decimal oldQuantity = 0;
        decimal oldBillDiscount = 0;
        bool isPOSDiscount = false;
        TransactionsGeneralFill TransactionGeneralFillObj = new TransactionsGeneralFill();
        VoucherTypeSP spVoucherType = new VoucherTypeSP();
        SettingsSP spSettings = new SettingsSP();
        ArrayList lstArrOfRemove = new ArrayList();
        string strTaxRate = string.Empty;
        ProductSP spProduct = new ProductSP();
        ProductInfo infoProduct = new ProductInfo();
        SalesMasterInfo infoSalesMaster = new SalesMasterInfo();
        SalesDetailsSP spSalesDetails = new SalesDetailsSP();
        SalesMasterSP spSalesMaster = new SalesMasterSP();
        TransactionsGeneralFill Obj = new TransactionsGeneralFill();
        int inIndex = 0;
        decimal decSalesReturnMasterId = 0;
        decimal decSalesReturnDetailId = 0;
        bool isInvoiceFill = false;
        bool isFromOther = false;
        bool isFromSalesAccountCombo = false;
        bool isFromCashOrPartyCombo = false;
        bool isCheckForVoucherTypeFill = false;
        bool isFiilCheck = false;
        TransactionsGeneralFill objTransactionGenerationFill = new TransactionsGeneralFill();
        decimal decTotalAmountForEditCheck = 0;
        decimal decTotalNetAmount = 0;
        DateValidation objDateValidation = new DateValidation();
        ArrayList arrlstMasterId = new ArrayList();
        int inNarrationCount = 0;
        public decimal decCurrentConversionRate = 0;
        public decimal decCurrentRate = 0;
        decimal DecSalesReturnVoucherTypeId = 0;
        string ManualReturnNo = string.Empty;
        decimal salesMasterId = 0;
        bool isByTypedInvoiceNo = false;

        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();
        public SalesReturnController()
        {

        }

        [HttpGet]
        public HttpResponseMessage GetLookUps()
        {
            dynamic response = new ExpandoObject();
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();
            response.ReturnNo = AutoInvoiceNo();
            response.VoucherTypes = GetVoucherTypes();
            response.Products = new ProductSP().ProductViewAll();
            response.Unit = new UnitSP().UnitViewAll();
            response.Batch = new BatchSP().BatchViewAll();
            response.Stores = new GodownSP().GodownViewAll();
            response.Ledgers = new AccountLedgerSP().AccountLedgerViewCustomerOnly();
            response.Users = new UserSP().UserViewAll();
            response.Currency = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            response.Tax = new TaxSP().TaxViewAll();
            response.Rack = new RackSP().RackViewAll();
            response.SalesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        public DataTable GetVoucherTypes()
        {
            DataTable dtbl = new DataTable();
            SalesReturnMasterSP spmaster = new SalesReturnMasterSP();
            dtbl = spmaster.vouchertypecompofill();

            return dtbl;
        }

        private string AutomaticVoucherNoGeneration()
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            SalesReturnMasterSP spSalesReturn = new SalesReturnMasterSP();
            string strPrefix = string.Empty;
            string strSuffix = string.Empty;
            string strReturnNo = string.Empty;
            string tableName = "SalesReturnMaster";

            strVoucherNo = obj.VoucherNumberAutomaicGeneration(decSalesReturnVoucherTypeId, 0, DateTime.Now, tableName);
            if (Convert.ToDecimal(strVoucherNo) != spSalesReturn.SalesReturnMasterGetMaxPlusOne(decSalesReturnVoucherTypeId))
            {
                strVoucherNo = spSalesReturn.SalesReturnMasterGetMax(decSalesReturnVoucherTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(decSalesReturnVoucherTypeId, 0, DateTime.Now, tableName);
                if (spSalesReturn.SalesReturnMasterGetMax(decSalesReturnVoucherTypeId) == "0")
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(decSalesReturnVoucherTypeId, 0, DateTime.Now, tableName);
                }
            }
            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(decSalesReturnVoucherTypeId, DateTime.Now);
            strPrefix = infoSuffixPrefix.Prefix;
            strSuffix = infoSuffixPrefix.Suffix;
            decSalesReturnSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
            strReturnNo = strPrefix + strVoucherNo + strSuffix;

            return strReturnNo;
        }

        [HttpGet]
        public DataTable GetInvoiceNo(decimal ledgerId, decimal voucherTypeId)
        {
            SalesReturnMasterSP salesReturnMasterSP = new SalesReturnMasterSP();
            DataTable dtbl = new DataTable();

            if (ledgerId == 1)
            {
                return dtbl = salesReturnMasterSP.SalesReturnInvoiceNoComboFill(voucherTypeId, 0, 0);
            }
            else
            {
                return dtbl = salesReturnMasterSP.SalesReturnInvoiceNoComboFill(voucherTypeId, 0, ledgerId);
            }
        }

        [HttpGet]
        public HttpResponseMessage GetInvoiceDetails(decimal invoiceNo)
        {
            DataTable master = new SalesMasterSP().SalesMasterViewByInvoiceNoForComboSelection(invoiceNo);
            DataTable detail = spSalesDetails.SalesDetailsViewForSalesReturnGrideFill(invoiceNo, salesReturnMasterId);

            dynamic response = new ExpandoObject();

            response.master = master;
            response.details = detail;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpPost]
        public bool Save(CreateSalesReturnVM input)
        {
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            SalesReturnMasterSP spSalesReturnMaster = new SalesReturnMasterSP();
            SalesReturnDetailsSP spSalesReturnDetails = new SalesReturnDetailsSP();
            var spStockPosting = new MatApi.Services.StockPostingSP();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();

            SalesReturnMasterInfo infoSalesReturnMaster = new SalesReturnMasterInfo();

            infoSalesReturnMaster.VoucherNo = input.VoucherNo;
            infoSalesReturnMaster.InvoiceNo = input.ReturnNo.Trim();
            infoSalesReturnMaster.VoucherTypeId = decSalesReturnVoucherTypeId;
            infoSalesReturnMaster.SuffixPrefixId = decSalesReturnSuffixPrefixId;
            infoSalesReturnMaster.LedgerId = input.LedgerId;
            infoSalesReturnMaster.SalesMasterId = input.SalesMasterId;
            infoSalesReturnMaster.PricinglevelId = input.CurrencyId;
            infoSalesReturnMaster.EmployeeId = input.SalesManId;
            infoSalesReturnMaster.ExchangeRateId = input.CurrencyId;
            decExchangeRate = spExchangeRate.ExchangeRateViewByExchangeRateId(input.CurrencyId);
            infoSalesReturnMaster.SalesAccount = input.SalesAccount;
            infoSalesReturnMaster.Narration = input.Narration;
            infoSalesReturnMaster.UserId = input.SalesManId;
            infoSalesReturnMaster.LrNo = input.LrNo;
            infoSalesReturnMaster.TransportationCompany = input.TransportationCompany;
            infoSalesReturnMaster.Date = input.Date;
            infoSalesReturnMaster.TotalAmount = input.TotalAmount;
            infoSalesReturnMaster.grandTotal = input.GrandTotal;
            infoSalesReturnMaster.TaxAmount = input.TaxAmount;
            infoSalesReturnMaster.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
            infoSalesReturnMaster.Extra1 = string.Empty;
            infoSalesReturnMaster.Extra2 = string.Empty;
            infoSalesReturnMaster.Discount = 0;

            decSalesReturnMasterId = spSalesReturnMaster.SalesReturnMasterAdd(infoSalesReturnMaster);

            SalesReturnDetailsInfo infoSalesReturnDetailsInfo = new SalesReturnDetailsInfo();
            infoSalesReturnDetailsInfo.Extra1 = string.Empty;
            infoSalesReturnDetailsInfo.Extra2 = string.Empty;
            if (input.LineItems != null && decSalesReturnMasterId > 0)
            {
                foreach (var lineitem in input.LineItems)
                {
                    //to save to salesDetails
                    infoSalesReturnDetailsInfo.SalesReturnMasterId = decSalesReturnMasterId;
                    infoSalesReturnDetailsInfo.ProductId = lineitem.ProductId;
                    infoSalesReturnDetailsInfo.Qty = lineitem.Quantity;
                    infoSalesReturnDetailsInfo.Rate = lineitem.Rate;
                    infoSalesReturnDetailsInfo.UnitId = lineitem.UnitId;
                    infoSalesReturnDetailsInfo.UnitConversionId = lineitem.UnitConversionId;
                    infoSalesReturnDetailsInfo.Discount = lineitem.Discount;
                    infoSalesReturnDetailsInfo.TaxId = lineitem.TaxId;
                    infoSalesReturnDetailsInfo.BatchId = lineitem.BatchId;
                    infoSalesReturnDetailsInfo.GodownId = lineitem.GodownId;
                    infoSalesReturnDetailsInfo.RackId = lineitem.RackId;
                    infoSalesReturnDetailsInfo.TaxAmount = lineitem.taxAmount;
                    infoSalesReturnDetailsInfo.GrossAmount = lineitem.GrossAmount;
                    infoSalesReturnDetailsInfo.NetAmount = lineitem.NetAmount;
                    infoSalesReturnDetailsInfo.Amount = lineitem.Amount;
                    infoSalesReturnDetailsInfo.SlNo = lineitem.SL;
                    infoSalesReturnDetailsInfo.SalesDetailsId = lineitem.SalesDetailsId;
                    infoSalesReturnDetailsInfo.ProjectId = 0;
                    infoSalesReturnDetailsInfo.CategoryId = 0;
                    infoSalesReturnDetailsInfo.itemDescription = lineitem.Description;

                    decSalesReturnDetailId = spSalesReturnDetails.SalesReturnDetailsAdd(infoSalesReturnDetailsInfo);

                    //to save to stock posting
                    StockPostingInfo infoStockPosting = new StockPostingInfo();
                    infoStockPosting.Date = infoSalesReturnMaster.Date;
                    if (input.VoucherTypeId != 0)
                    {
                        infoStockPosting.VoucherTypeId = input.VoucherTypeId;
                        decAgainstVoucherTypeId = infoStockPosting.VoucherTypeId;
                        infoStockPosting.AgainstVoucherTypeId = decSalesReturnVoucherTypeId;
                    }
                    else
                    {
                        infoStockPosting.VoucherTypeId = decSalesReturnVoucherTypeId;
                        infoStockPosting.AgainstVoucherTypeId = 0;
                    }
                    if (input.VoucherNo != "")
                    {
                        infoStockPosting.VoucherNo = input.ReturnNo;
                        strAgainstVoucherNo = infoStockPosting.VoucherNo;
                        infoStockPosting.AgainstVoucherNo = input.ReturnNo;
                    }
                    else
                    {
                        infoStockPosting.VoucherNo = input.ReturnNo;
                        infoStockPosting.AgainstVoucherNo = "NA";
                    }
                    if (input.VoucherNo != "")
                    {
                        infoStockPosting.InvoiceNo = input.ReturnNo;
                        strAgainstInvoiceNo = infoStockPosting.InvoiceNo;
                        infoStockPosting.AgainstInvoiceNo = input.ReturnNo;
                    }
                    else
                    {
                        infoStockPosting.InvoiceNo = input.ReturnNo;
                        infoStockPosting.AgainstInvoiceNo = "NA";
                    }
                    infoStockPosting.ProductId = infoSalesReturnDetailsInfo.ProductId;
                    infoStockPosting.BatchId = infoSalesReturnDetailsInfo.BatchId;
                    infoStockPosting.UnitId = infoSalesReturnDetailsInfo.UnitId;
                    infoStockPosting.GodownId = infoSalesReturnDetailsInfo.GodownId;
                    infoStockPosting.RackId = infoSalesReturnDetailsInfo.RackId;
                    if (infoSalesReturnDetailsInfo.ProductId != 0 && infoSalesReturnDetailsInfo.UnitId != 0)
                    {
                        decimal decUnitConvertionRate = 0;
                        infoProduct = spProduct.ProductView(infoSalesReturnDetailsInfo.ProductId);
                        DataTable dtbl = new UnitConvertionSP().DGVUnitConvertionRateByUnitId(infoSalesReturnDetailsInfo.UnitId, infoProduct.ProductName);
                        foreach (DataRow drowDetails in dtbl.Rows)
                        {
                            decUnitConvertionRate = Convert.ToDecimal(drowDetails["conversionRate"].ToString());
                        }
                        string strQuantities = new UnitSP().UnitConversionCheck(infoSalesReturnDetailsInfo.UnitId, infoSalesReturnDetailsInfo.ProductId);
                        if (strQuantities != string.Empty)
                        {
                            infoStockPosting.InwardQty = infoSalesReturnDetailsInfo.Qty / decUnitConvertionRate;
                        }
                        else
                        {
                            infoStockPosting.InwardQty = infoSalesReturnDetailsInfo.Qty;
                        }
                    }
                    infoStockPosting.OutwardQty = 0;
                    infoStockPosting.Rate = infoSalesReturnDetailsInfo.Rate;
                    infoStockPosting.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                    infoStockPosting.Extra1 = string.Empty;
                    infoStockPosting.Extra2 = string.Empty;

                    spStockPosting.StockPostingAdd(infoStockPosting);


                    //cost of goods posting
                    var product = context.tbl_Product.Find(lineitem.ProductId);
                    if (product != null)
                    {
                        var ledgerExpence = context.tbl_AccountLedger.Find(product.expenseAccount);
                        LedgerPosting(new LedgerPostingInfo
                        {
                            InvoiceNo = infoSalesReturnMaster.InvoiceNo,
                            Debit = 0,
                            Date = infoSalesReturnMaster.Date,
                            ExtraDate = DateTime.UtcNow,
                            LedgerId = ledgerExpence.ledgerId,
                            VoucherTypeId = infoSalesReturnMaster.VoucherTypeId,
                            Credit = Convert.ToDecimal(product.purchaseRate * lineitem.Quantity),
                            Extra2 = "",
                            Extra1 = "",
                            VoucherNo = infoSalesReturnMaster.InvoiceNo,
                            ChequeDate = DateTime.UtcNow,
                            ChequeNo = "",
                            DetailsId = decSalesReturnDetailId,
                            YearId = Models.PublicVariables.CurrentFinicialYearId
                        });

                        var amt = infoStockPosting.InwardQty * infoSalesReturnDetailsInfo.Rate;

                        LedgerPosting(new LedgerPostingInfo
                        {
                            InvoiceNo = infoSalesReturnMaster.InvoiceNo,
                            Debit = amt,
                            Date = infoSalesReturnMaster.Date,
                            ExtraDate = DateTime.UtcNow,
                            LedgerId = Convert.ToDecimal(product.salesAccount),
                            VoucherTypeId = infoSalesReturnMaster.VoucherTypeId,
                            Credit = 0,
                            Extra2 = "",
                            Extra1 = "",
                            VoucherNo = infoSalesReturnMaster.InvoiceNo,
                            ChequeDate = DateTime.UtcNow,
                            ChequeNo = "",
                            DetailsId = decSalesReturnDetailId,
                            YearId = Models.PublicVariables.CurrentFinicialYearId
                        });

                    }
                }
                decimal decGrandTotal = input.GrandTotal;
                decimal decNetTotal = input.NetAmount;
                LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
                infoLedgerPosting.Date = infoSalesReturnMaster.Date;
                infoLedgerPosting.ChequeDate = infoSalesReturnMaster.Date;
                infoLedgerPosting.ChequeNo = String.Empty;
                infoLedgerPosting.VoucherTypeId = infoSalesReturnMaster.VoucherTypeId;
                infoLedgerPosting.VoucherNo = infoSalesReturnMaster.VoucherNo;
                infoLedgerPosting.LedgerId = infoSalesReturnMaster.LedgerId;
                infoLedgerPosting.Debit = 0;
                infoLedgerPosting.Credit = (input.GrandTotal * decExchangeRate);
                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                infoLedgerPosting.InvoiceNo = infoSalesReturnMaster.InvoiceNo;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                //account receivable posting
                spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);




                if (input.DiscountAmount > 0)
                {
                    infoLedgerPosting.LedgerId = LedgerConstants.Discount_Received;
                    infoLedgerPosting.Debit = 0;
                    infoLedgerPosting.Credit = (input.DiscountAmount);
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
                if (input.TaxAmount > 0)
                {
                    infoLedgerPosting.LedgerId = LedgerConstants.Vat;
                    infoLedgerPosting.Debit = input.TaxAmount * decExchangeRate;
                    infoLedgerPosting.Credit = 0;
                    spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                }
                //to save to party balance
                PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
                infoPartyBalance.Date = infoSalesReturnMaster.Date;
                infoPartyBalance.LedgerId = infoSalesReturnMaster.LedgerId;
                if (decAgainstVoucherTypeId != 0)
                {
                    infoPartyBalance.VoucherTypeId = decAgainstVoucherTypeId;
                    infoPartyBalance.VoucherNo = strAgainstVoucherNo;
                    infoPartyBalance.InvoiceNo = strAgainstInvoiceNo;
                    infoPartyBalance.AgainstVoucherTypeId = infoSalesReturnMaster.VoucherTypeId;
                    infoPartyBalance.AgainstVoucherNo = infoSalesReturnMaster.VoucherNo;
                    infoPartyBalance.AgainstInvoiceNo = infoSalesReturnMaster.InvoiceNo;
                    infoPartyBalance.ReferenceType = "Against";
                }
                else
                {
                    infoPartyBalance.VoucherTypeId = infoSalesReturnMaster.VoucherTypeId;
                    infoPartyBalance.VoucherNo = infoSalesReturnMaster.VoucherNo;
                    infoPartyBalance.InvoiceNo = infoSalesReturnMaster.InvoiceNo;
                    infoPartyBalance.AgainstVoucherTypeId = 0;
                    infoPartyBalance.AgainstVoucherNo = "NA";
                    infoPartyBalance.AgainstInvoiceNo = "NA";
                    infoPartyBalance.ReferenceType = "New";
                }

                infoPartyBalance.Credit = infoSalesReturnMaster.TotalAmount;
                infoPartyBalance.Debit = 0;
                infoPartyBalance.CreditPeriod = 0;
                infoPartyBalance.ExchangeRateId = infoSalesReturnMaster.ExchangeRateId;
                infoPartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoPartyBalance.Extra1 = string.Empty;
                infoPartyBalance.Extra2 = string.Empty;
                if (infoPartyBalance.LedgerId != 1)
                {
                    spPartyBalance.PartyBalanceAdd(infoPartyBalance);
                }
                SalesReturnBillTaxInfo infoSalesReturnBillTax = new SalesReturnBillTaxInfo();
                infoSalesReturnBillTax.SalesReturnMasterId = decSalesReturnMasterId;
                infoSalesReturnBillTax.TaxId = 10012;
                infoSalesReturnBillTax.TaxAmount = input.TaxAmount;
                infoSalesReturnBillTax.Extra1 = string.Empty;
                infoSalesReturnBillTax.Extra2 = string.Empty;
                new SalesReturnBillTaxSP().SalesReturnBillTaxAdd(infoSalesReturnBillTax);

                MATFinancials.DAL.DBMatConnection conn = new MATFinancials.DAL.DBMatConnection();
                string updateQuery = string.Format("UPDATE tbl_SalesReturnMaster_Pending SET status='Approved' WHERE returnNo={0}", input.ReturnNo);
                if (conn.customUpdateQuery(updateQuery) > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
            else
            {
                return false;
            }
        }

        public bool LedgerPosting(LedgerPostingInfo infoLedgerPosting)
        {
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            var post = spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
            if (post > 0) return true;

            return false;
        }


        [HttpPost]
        public decimal SavePending(CreateSalesReturnVM input)
        {
            decimal result = 0;

            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            //string masterQuery = string.Format("INSERT INTO tbl_SalesReturnMaster_Pending " +
            //                                        "VALUES({0},{1},{2},{3},{4},'{5}',{6},{7},{8},'{9}','{10}','{11}',{12},{13},{14},'{15}','{16}')"
            //                                        , input.SalesMasterId,
            //                                        input.LedgerId,
            //                                        input.SalesAccount,
            //                                        input.CurrencyId,
            //                                        input.SalesManId,
            //                                        input.Narration,
            //                                        input.CurrencyId,
            //                                        input.TaxAmount,
            //                                        input.SalesManId,
            //                                        input.LrNo,
            //                                        input.TransportationCompany,
            //                                        input.Date,
            //                                        input.TotalAmount,
            //                                        input.DiscountAmount,
            //                                        input.GrandTotal,
            //                                        "Pending",
            //                                        input.ReturnNo);
            //if (db.ExecuteNonQuery2(masterQuery))
            //{
            try
            {
                int salesReturnId = 0;
                string id = db.getSingleValue("SELECT top 1 salesReturnMasterId from tbl_SalesReturnMaster_Pending order by salesReturnMasterId desc");
                if (id.IsNullOrWhiteSpace())
                {
                    salesReturnId = 0 + 1;
                }

                if (!id.IsNullOrWhiteSpace())
                {
                    salesReturnId = Convert.ToInt32(id) + 1;
                }

                decimal amount = 0;
                decimal discountAmount = 0;
                decimal netAmount = 0;
                decimal grossAmount = 0;
                decimal taxAmount = 0;
                int deleteAll = 0;
                foreach (var row in input.LineItems)
                {
                    if (row.Quantity > 0)
                    {
                        //var amount = row.Rate * row.Qty; 
                        string detailsQuery1 = string.Format("INSERT INTO tbl_SalesReturnDetails_Pending " +
                                                    "VALUES({0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},{11},{12},{13},{14},{15},{16},'{17}')"
                                                    , salesReturnId,
                                                    row.ProductId,
                                                    row.Quantity,
                                                    row.Rate,
                                                    row.UnitId,
                                                    row.UnitConversionId,
                                                    row.Discount,
                                                    row.TaxId,
                                                    row.BatchId,
                                                    row.GodownId,
                                                    row.RackId,
                                                    row.taxAmount,
                                                    row.GrossAmount,
                                                    row.NetAmount,
                                                    row.Amount,
                                                    row.SalesDetailsId,
                                                    row.SL,
                                                    row.Description);
                        if (db.ExecuteNonQuery2(detailsQuery1))
                        {
                            result = 1;
                        }
                        else
                        {
                            result = 0;
                        }

                        //Updating or deleting salesDetails depending on the quatity to be returned
                        //string salesDetailsQuantity = db.getSingleValue(string.Format("SELECT qty from tbl_SalesDetails WHERE salesDetailsId = {0}", row.SalesDetailsId));
                        var salesDetailsQuantity = context.tbl_SalesDetails.FirstOrDefault(a => a.salesDetailsId == row.SalesDetailsId).qty;
                        if (Convert.ToDecimal(salesDetailsQuantity) == row.Quantity)
                        {
                            deleteAll++;
                            db.ExecuteNonQuery2(string.Format("DELETE FROM tbl_SalesDetails WHERE salesDetailsId = {0}", row.SalesDetailsId));
                        }
                        else
                        {
                            int saleDq = Convert.ToInt32(salesDetailsQuantity);
                            int rqty = Convert.ToInt32(row.Quantity);
                            int newQuantity = saleDq - rqty;
                            netAmount = row.Rate * newQuantity;
                            grossAmount = row.Rate * newQuantity;
                            taxAmount = row.taxAmount * newQuantity / saleDq;
                            discountAmount = row.Discount * newQuantity / saleDq;
                            amount = row.NetAmount + row.taxAmount;
                            db.ExecuteNonQuery2(string.Format("UPDATE tbl_SalesDetails SET qty = {0}, netAmount = {1}, grossAmount = {2}, taxAmount = {3}, discount = {4}, amount = {5} WHERE salesDetailsId = {6}", newQuantity, netAmount, grossAmount, taxAmount, discountAmount, amount, row.SalesDetailsId));
                        }
                    }
                    else
                    {
                        amount += row.Amount;
                        discountAmount += row.Discount;
                        netAmount += row.NetAmount;
                        grossAmount += row.GrossAmount;
                        taxAmount += row.taxAmount;
                    }
                }
                //}
                string salesReturnMasterQuery = string.Format("INSERT INTO tbl_SalesReturnMaster_Pending " +
                                                        "VALUES({0},{1},{2},{3},{4},'{5}',{6},{7},{8},'{9}','{10}','{11}',{12},{13},{14},'{15}','{16}')"
                                                        , input.SalesMasterId,
                                                        input.LedgerId,
                                                        input.SalesAccount,
                                                        input.CurrencyId,
                                                        input.SalesManId,
                                                        input.Narration,
                                                        input.CurrencyId,
                                                        taxAmount,//input.TaxAmount,
                                                        input.SalesManId,
                                                        input.LrNo,
                                                        input.TransportationCompany,
                                                        input.Date,
                                                        amount,//input.TotalAmount,
                                                        grossAmount,//input.DiscountAmount,
                                                        grossAmount,//input.GrandTotal,
                                                        "Pending",
                                                        input.ReturnNo);

                if (db.ExecuteNonQuery2(salesReturnMasterQuery))
                {
                    result = 1;
                }

                string salesMasterQuery = string.Format("UPDATE tbl_SalesMaster SET taxAmount = {0}, billDiscount = {1}, grandTotal = {2}, totalAmount = {3} WHERE salesMasterId = {4}"
                                                        , taxAmount,
                                                        discountAmount,
                                                        grossAmount,
                                                        amount,
                                                        input.SalesMasterId
                                                        );

                if (db.ExecuteNonQuery2(salesMasterQuery))
                {
                    result = 1;
                }

                //note: there might still be a need to update the amountTendered and changeGiven fields in the SalesMaster table 
                int lineCount = Convert.ToInt32(input.LineItems.Count());
                if (deleteAll == lineCount)
                {
                    string deleteSalesMaster = string.Format("DELETE FROM tbl_SalesMaster WHERE salesMasterId = {0}", input.SalesMasterId);
                    if (db.ExecuteNonQuery2(deleteSalesMaster))
                    {
                        result = 1;
                    }
                }
            }
            catch (Exception e)
            {
                result = 0;
            }

            return result;
        }

        [HttpGet]
        public DataTable GetPendingToList(DateTime fromDate, DateTime toDate, string Status)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string getQuery = string.Empty;
            dynamic response = new ExpandoObject();

            if (Status == "" || Status == null)
            {
                getQuery = string.Format("SELECT * FROM tbl_SalesReturnMaster_Pending WHERE date BETWEEN '{0}' AND '{1}' ORDER BY salesReturnMasterId DESC", fromDate, toDate);

            }
            else
            {
                getQuery = string.Format("SELECT * FROM tbl_SalesReturnMaster_Pending WHERE date BETWEEN '{0}' AND '{1}' AND status='{2}' ORDER BY SalesReturnMasterId DESC", fromDate, toDate, Status);
            }

            return db.customSelect(getQuery);
        }

        [HttpGet]
        public DataTable GetMasterToList(DateTime fromDate, DateTime toDate, string Status)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string getQuery = string.Empty;
            dynamic response = new ExpandoObject();

            if (Status == "" || Status == null)
            {
                getQuery = string.Format("SELECT * FROM tbl_SalesReturnMaster WHERE date BETWEEN '{0}' AND '{1}' ORDER BY salesReturnMasterId DESC", fromDate, toDate);

            }
            else
            {
                getQuery = string.Format("SELECT * FROM tbl_SalesReturnMaster WHERE date BETWEEN '{0}' AND '{1}' AND status='{2}' ORDER BY SalesReturnMasterId DESC", fromDate, toDate, Status);
            }

            return db.customSelect(getQuery);
        }


        [HttpGet]
        public DataTable GetPendingDetails(decimal id)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string getQuery = string.Empty;

            getQuery = string.Format("SELECT * FROM tbl_SalesReturnDetails_Pending WHERE salesReturnMasterId = {0}", id);

            return db.customSelect(getQuery);
        }


        [HttpGet]
        public IHttpActionResult GetSalesReturnDetails(int id)
        {
            context = new DBMATAccounting_MagnetEntities1();
            context.Configuration.LazyLoadingEnabled = false;
            var details = context.tbl_SalesReturnDetails
               .Where(a => a.salesReturnMasterId == id)
               .Include(a => a.tbl_Product)
               .Include(a => a.tbl_SalesReturnMaster)
               .Include(a => a.tbl_SalesReturnMaster.tbl_AccountLedger)
               .Include(a => a.tbl_SalesDetails)
               .ToList();

            var response = new
            {
                records = details.Select(a => new
                {
                    detail = a,
                    product = a.tbl_Product,
                    initialDetail = a.tbl_SalesDetails
                }),
                master = details.Select(a => new { a.tbl_SalesReturnMaster }).FirstOrDefault()?.tbl_SalesReturnMaster,
                ledger = details.Select(a => new { a.tbl_SalesReturnMaster.tbl_AccountLedger }).FirstOrDefault()
            };
            return Ok(response);
        }

        [HttpPost]
        public IHttpActionResult Edit(EditSalesReturnDto edit)
        {
            if (edit == null) return BadRequest("No Data Found");
            var auditLogs = new List<TransactionAuditLog>();

            var salesMaster = context.tbl_SalesReturnMaster.Find(edit.SalesReturnMaster.salesReturnMasterId);
            if (salesMaster != null)
            {
                //check for sales return 
                auditLogs.Add(new TransactionAuditLog()
                {
                    TransId = salesMaster.salesReturnMasterId.ToString(),
                    DateTime = DateTime.UtcNow,
                    Amount = salesMaster.totalAmount,
                    VochureTypeId = Constants.SalesReturn_VoucherTypeId,
                    VochureName = TransactionsEnum.SalesReturnMaster.ToString(),
                    AdditionalInformation = "Edited Sales Return Master",
                    UserId = PublicVariables._decCurrentUserId,
                    Status = TransactionOperationEnums.Edit.ToString()
                });

                salesMaster.narration = edit.SalesReturnMaster.narration;
                salesMaster.totalAmount = edit.SalesReturnMaster.totalAmount;

                salesMaster.grandTotal = edit.SalesReturnMaster.grandTotal;

                context.Entry(salesMaster).State = EntityState.Modified;


                //edit line items
                var details = salesMaster.tbl_SalesReturnDetails;

                var detailIds = details.Select(a => Convert.ToDecimal(a.salesDetailsId)).ToList();
                var updatedIds = new List<decimal>();


                foreach (var i in edit.SalesReturnDetails)
                {
                    //get item to update

                    var item = details.FirstOrDefault(a => a.salesReturnDetailsId == i.salesReturnDetailsId);

                    if (item != null)
                    {
                        auditLogs.Add(new TransactionAuditLog()
                        {
                            TransId = item.salesReturnDetailsId.ToString(),
                            DateTime = DateTime.UtcNow,
                            Amount = item.amount,
                            Quantity = item.qty,
                            VochureTypeId = Constants.SalesReturn_VoucherTypeId,
                            VochureName = TransactionsEnum.SalesReturnDetail.ToString(),
                            AdditionalInformation = "Edited Sales  Return Record",
                            UserId = PublicVariables._decCurrentUserId,
                            Status = TransactionOperationEnums.Edit.ToString()
                        });
                        item.discount = i.discount;
                        item.qty = i.qty;
                        item.amount = item.rate * i.qty;
                        context.Entry(item).State = EntityState.Modified;
                        updatedIds.Add(item.salesReturnDetailsId);
                    }
                    if (i.salesDetailsId == 0)
                    {
                        i.extraDate = DateTime.UtcNow;
                        context.tbl_SalesReturnDetails.Add(i);
                    }
                }

                var itemToDeleteIds = detailIds.Except(updatedIds);
                foreach (var i in itemToDeleteIds)
                {
                    var item = details.FirstOrDefault(a => a.salesReturnDetailsId == i);
                    if (item != null)
                    {
                        context.tbl_SalesReturnDetails.Remove(item);
                    }
                }
                context.SaveChanges();

                var stockPosting = context.tbl_StockPosting.Where(a =>
                    a.voucherTypeId == Constants.SalesInvoice_VoucherTypeId && a.voucherNo == salesMaster.voucherNo).ToList();
                // delete stock posting 
                if (stockPosting.Any())
                {
                    context.tbl_StockPosting.RemoveRange(stockPosting);
                }
                //get new updated items 
                var updatedItems = context.tbl_SalesDetails
                    .Where(a => a.salesMasterId == salesMaster.salesMasterId)
                    .ToList();

                // add new stock posting to update record
                foreach (var i in updatedItems)
                {
                    var product = context.tbl_Product.Find(i.productId);
                    context.tbl_StockPosting.Add(new DBModel.tbl_StockPosting
                    {
                        //set items here 
                        batchId = i.batchId,
                        CategoryId = i.CategoryId,
                        productId = i.productId,
                        ProjectId = i.ProjectId,
                        extra2 = i.extra2,
                        voucherTypeId = Constants.SalesReturn_VoucherTypeId,
                        date = DateTime.UtcNow,
                        extraDate = DateTime.UtcNow,
                        financialYearId = PublicVariables._decCurrentFinancialYearId,
                        extra1 = "",
                        rackId = i.rackId,
                        voucherNo = salesMaster.voucherNo,
                        invoiceNo = salesMaster.invoiceNo,
                        unitId = i.unitId,
                        rate = product.purchaseRate,
                        godownId = i.godownId,
                        outwardQty = 0,
                        inwardQty = i.qty,
                        againstInvoiceNo = "",
                        againstVoucherNo = "",
                        againstVoucherTypeId = 0
                    });
                }
                context.SaveChanges();
                //get all ledger posting base on the vocheur id from the master, then delete them and create a new one 
                var oldLedgerPosting = context.tbl_LedgerPosting
                    .Where(a => a.voucherNo == salesMaster.voucherNo && a.voucherTypeId == Constants.SalesReturn_VoucherTypeId)
                    .ToList();
                if (oldLedgerPosting.Any())
                {
                    context.tbl_LedgerPosting.RemoveRange(oldLedgerPosting);
                }
                var goodInTransitLedger = LedgerConstants.GoodsInTransit;
                foreach (var i in updatedItems)
                {
                    var product = context.tbl_Product.Find(i.productId);

                    //cost of sale
                    context.tbl_LedgerPosting.Add(new DBModel.tbl_LedgerPosting()
                    {
                        voucherTypeId = Constants.SalesReturn_VoucherTypeId,
                        date = DateTime.UtcNow,
                        ledgerId = product.expenseAccount,
                        extra2 = "",
                        extraDate = DateTime.UtcNow,
                        voucherNo = salesMaster.voucherNo,
                        extra1 = "",
                        credit = i.qty * i.rate,
                        debit = 0,
                        invoiceNo = salesMaster.invoiceNo,
                        chequeDate = DateTime.UtcNow,
                        yearId = PublicVariables._decCurrentFinancialYearId,
                        detailsId = i.salesDetailsId,
                        chequeNo = ""
                    });
                    //income account
                    context.tbl_LedgerPosting.Add(new DBModel.tbl_LedgerPosting()
                    {
                        voucherTypeId = Constants.SalesReturn_VoucherTypeId,
                        date = DateTime.UtcNow,
                        ledgerId = product.salesAccount,
                        extra2 = "",
                        extraDate = DateTime.UtcNow,
                        voucherNo = salesMaster.voucherNo,
                        extra1 = "",
                        credit = 0,
                        debit = i.qty * i.rate,
                        invoiceNo = salesMaster.invoiceNo,
                        chequeDate = DateTime.UtcNow,
                        yearId = PublicVariables._decCurrentFinancialYearId,
                        detailsId = i.salesDetailsId,
                        chequeNo = ""
                    });

                    //Account Recieveable 
                    context.tbl_LedgerPosting.Add(new DBModel.tbl_LedgerPosting()
                    {
                        voucherTypeId = Constants.SalesReturn_VoucherTypeId,
                        date = DateTime.UtcNow,
                        ledgerId = salesMaster.ledgerId,
                        extra2 = "",
                        extraDate = DateTime.UtcNow,
                        voucherNo = salesMaster.voucherNo,
                        extra1 = "",
                        credit = i.qty * i.rate,
                        debit = 0,
                        invoiceNo = salesMaster.invoiceNo,
                        chequeDate = DateTime.UtcNow,
                        yearId = PublicVariables._decCurrentFinancialYearId,
                        detailsId = i.salesDetailsId,
                        chequeNo = ""
                    });
                    context.SaveChanges();
                }
                if (auditLogs.Any())
                {
                    new Services.TransactionAuditService().CreateMany(auditLogs);
                }
                return Ok(new { status = 200, message = "Edit Made Successful" });

            }

            return InternalServerError();
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var audits = new List<TransactionAuditLog>();
            var master = context.tbl_SalesReturnMaster.Find(id);

            if (master == null) return NotFound();

            var vochureId = Constants.SalesReturn_VoucherTypeId;

            //find ledgerPosting and Delete  

            var ledgerPosting = context.tbl_LedgerPosting.Where(a =>
                a.voucherTypeId == vochureId
                && a.voucherNo == master.voucherNo);
            if (ledgerPosting.Any())
            {
                context.tbl_LedgerPosting.RemoveRange(ledgerPosting);
                //audits.AddRange( ledgerPosting.Select(a=> new TransactionAuditLog
                //{

                //}));
            }
            //find all stock posting and delete 
            var stocks = context.tbl_StockPosting.Where(a => a.voucherTypeId == vochureId &&
                                                 a.voucherNo == master.voucherNo).ToList();
            if (stocks.Any())
            {
                context.tbl_StockPosting.RemoveRange(stocks);
            }
            audits.AddRange(master.tbl_SalesReturnDetails.Select(a => new TransactionAuditLog
            {
                Quantity = a.qty,
                Amount = a.amount * a.qty,
                UserId = PublicVariables._decCurrentUserId,
                VochureName = Enums.TransactionsEnum.SalesReturnDetail.ToString(),
                DateTime = DateTime.Now,
                Status = Enums.TransactionOperationEnums.Delete.ToString(),
                AdditionalInformation = "Delete Items From Sales Return Details",
                VochureTypeId = Constants.SalesInvoice_VoucherTypeId,
                TransId = a.salesDetailsId.ToString(),
            }));
            audits.Add(new TransactionAuditLog()
            {

                Amount = master.totalAmount,
                UserId = PublicVariables._decCurrentUserId,
                VochureName = Enums.TransactionsEnum.SalesMaster.ToString(),
                DateTime = DateTime.Now,
                Status = Enums.TransactionOperationEnums.Delete.ToString(),
                AdditionalInformation = "Delete Items From Sales Return Master",
                VochureTypeId = Constants.SalesInvoice_VoucherTypeId,
                TransId = master.salesMasterId.ToString(),
            });

            //delete master and detail in 
            context.tbl_SalesReturnMaster.Remove(master);
            context.SaveChanges();
            new Services.TransactionAuditService().CreateMany(audits);


            return Ok(new { status = 200, message = "Delete Made Successful" });
        }


        [HttpGet]
        public decimal CancelReturn(decimal id)
        {

            MATFinancials.DAL.DBMatConnection conn = new MATFinancials.DAL.DBMatConnection();
            string updateQuery = string.Format("UPDATE tbl_SalesReturnMaster_Pending SET status='Cancelled' WHERE salesReturnMasterId={0}", id);

            return conn.customUpdateQuery(updateQuery);
        }

        private string AutoInvoiceNo()
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();

            decimal nextNo = 0;
            string id = db.getSingleValue("SELECT top 1 salesReturnMasterId from tbl_SalesReturnMaster_Pending order by salesReturnMasterId desc");
            if (id == "")
            {
                nextNo = 0;
            }
            else
            {
                nextNo = Convert.ToDecimal(id);
            }
            decimal nextInvoice = nextNo + 1;

            return nextInvoice.ToString();
        }
    }

    public class CreateSalesReturnVM
    {
        public string ReturnNo { get; set; }
        public DateTime Date { get; set; }
        public decimal LedgerId { get; set; }
        public string Narration { get; set; }
        public decimal SalesAccount { get; set; }
        public decimal TotalAmount { get; set; }
        public string TransportationCompany { get; set; }
        public string LrNo { get; set; }
        public decimal SalesManId { get; set; }
        public decimal CurrencyId { get; set; }
        public decimal SalesMasterId { get; set; }
        public decimal DiscountAmount { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal NetAmount { get; set; }
        public decimal GrandTotal { get; set; }
        public decimal VoucherTypeId { get; set; }
        public string VoucherNo { get; set; }
        public string InvoiceNo { get; set; }
        public List<SalesReturnLineItems> LineItems { get; set; }
    }

    public class SalesReturnLineItems
    {
        public int SL { get; set; }
        public decimal ProductId { get; set; }
        public decimal ProductBarcode { get; set; }
        public decimal ProductCode { get; set; }
        public string ProductName { get; set; }
        public int CategoryId { get; set; }
        public int Projectid { get; set; }
        public decimal Discount { get; set; }
        public decimal GodownId { get; set; }
        public decimal GrossAmount { get; set; }
        public decimal NetAmount { get; set; }
        public decimal SalesDetailsId { get; set; }
        public string Description { get; set; }
        public decimal Quantity { get; set; }
        public decimal UnitId { get; set; }
        public decimal UnitConversionId { get; set; }
        public decimal RackId { get; set; }
        public decimal BatchId { get; set; }
        public decimal Rate { get; set; }
        public decimal TaxId { get; set; }
        public decimal taxAmount { get; set; }
        public decimal Amount { get; set; }
    }
}
