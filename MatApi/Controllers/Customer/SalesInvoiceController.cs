﻿using MatApi.Models.Customer;
using MATFinancials;
using MATFinancials.DAL;
using MATFinancials.Other;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;
using MatApi.Dto;
using MatApi.Enums;
using MatApi.Models;
using MatDal;
using MatDal.Repository;
using Microsoft.Office.Interop.Excel;
using Newtonsoft.Json;
using OfficeOpenXml.FormulaParsing.Excel.Functions.Math;
using Constants = MatApi.Models.Constants;
using DataTable = System.Data.DataTable;
using PublicVariables = MATFinancials.PublicVariables;
using tbl_Product = MatApi.DBModel.tbl_Product;
using System.Threading.Tasks;
using MatApi.Models.Reports.OtherReports;
using EntityState = System.Data.Entity.EntityState;
using TransactionAuditLog = MatApi.DBModel.TransactionAuditLog;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SalesInvoiceController : ApiController
    {
        decimal DecSalesInvoiceVoucherTypeId = 28;
        decimal decSalseInvoiceSuffixPrefixId = 0;
        string strVoucherNo = string.Empty;
        string strInvoiceNo = string.Empty;
        string strPrefix = string.Empty;
        string strSuffix = string.Empty;
        decimal decSalesDetailsId = 0;
        decimal godownId = 0;
        List<decimal> listofDetailsId = new List<decimal>();
        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();

        private string AutoGenerateFormNo()
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            SalesMasterSP spSalesMaster = new SalesMasterSP();

            strVoucherNo = "0";
            strVoucherNo = spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId).ToString();
            if (strVoucherNo == string.Empty)
            {
                strVoucherNo = "0";
            }
            strVoucherNo = obj.VoucherNumberAutomaicGeneration(DecSalesInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");
            if (Convert.ToDecimal(strVoucherNo) != (spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId)))
            {
                strVoucherNo = spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(DecSalesInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");
                if (spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId) == 0)
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(DecSalesInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");
                }
            }

            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(DecSalesInvoiceVoucherTypeId, DateTime.Now);
            strPrefix = infoSuffixPrefix.Prefix;
            strSuffix = infoSuffixPrefix.Suffix;
            var decPurchaseInvoiceSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
            var FormNo = strPrefix + strVoucherNo + strSuffix;

            return FormNo;
        }

        [HttpGet]
        public IHttpActionResult GetAutoGenerateFormNo()
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            SalesMasterSP spSalesMaster = new SalesMasterSP();

            strVoucherNo = "0";
            strVoucherNo = spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId).ToString();
            if (strVoucherNo == string.Empty)
            {
                strVoucherNo = "0";
            }
            strVoucherNo = obj.VoucherNumberAutomaicGeneration(DecSalesInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");
            if (Convert.ToDecimal(strVoucherNo) != (spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId)))
            {
                strVoucherNo = spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(DecSalesInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");
                if (spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId) == 0)
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(DecSalesInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");
                }
            }

            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(DecSalesInvoiceVoucherTypeId, DateTime.Now);
            strPrefix = infoSuffixPrefix.Prefix;
            strSuffix = infoSuffixPrefix.Suffix;
            var decPurchaseInvoiceSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
            var FormNo = strPrefix + strVoucherNo + strSuffix;

            return Json(FormNo);
        }

        [HttpGet]
        public HttpResponseMessage InvoiceLookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var customers = transactionGeneralFillObj.CashOrPartyUnderSundryDrComboFill();
            var salesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();
            var pricingLevels = transactionGeneralFillObj.PricingLevelViewAll();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            var units = new UnitSP().UnitViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks = new RackSP().RackViewAll();
            var batches = new BatchSP().BatchViewAll();
            var tax = new TaxSP().TaxViewAll();


            dynamic response = new ExpandoObject();
            response.Customers = customers;
            response.SalesMen = salesMen;
            response.PricingLevels = pricingLevels;
            response.Currencies = currencies;
            response.Units = units;
            response.Stores = stores;
            response.Racks = racks;
            response.Batches = batches;
            response.Taxes = tax;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetInvoiceMax()
        {
            dynamic response = new ExpandoObject();
            try
            {
                SalesMasterSP spSalesMaster = new SalesMasterSP();
                response.InvoiceNo = spSalesMaster.SalesMasterGetInvoiceMax();
            }
            catch (Exception e)
            {
                Console.Write(e.Message); 
            }
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetSalesModeOrderNo(decimal ledgerId, decimal voucherTypeId, decimal salesInvoiceToEdit = 0)
        {
            //salesquota = 15
            //delivery 17
            //salesorder 16
            SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
            DeliveryNoteMasterSP spDeliveryNoteMasterSp = new DeliveryNoteMasterSP();
            SalesQuotationMasterSP spSalesQuotationMasterSp = new SalesQuotationMasterSP();

            DBMatConnection conn = new DBMatConnection();
            dynamic response = new ExpandoObject();
            //var salesOrderNoList = context.tbl_SalesOrderMaster
            //   .Where(a => a.ledgerId == ledgerId).ToList()
            //   .Join(context.tbl_SalesMaster.ToList(), b => b.salesOrderMasterId, c => c.orderMasterId,
            //    (b, c) => new { });
            if (voucherTypeId == 10030)
            {
                string queryStr = string.Format(" SELECT invoiceNo,salesOrderMasterId FROM tbl_SalesOrderMaster WHERE ledgerId = {0} AND salesOrderMasterId NOT IN " +
                                    "(SELECT tbl_SalesOrderMaster.salesOrderMasterId FROM tbl_SalesOrderMaster " +
                                    "INNER JOIN tbl_SalesMaster ON tbl_SalesOrderMaster.ledgerId = {0} " +
                                    " AND tbl_SalesMaster.ledgerId = {0} AND tbl_SalesOrderMaster.salesOrderMasterId = tbl_SalesMaster.orderMasterId) ", ledgerId);
                var salesOrders = conn.GetDataSet(queryStr);
                response.salesOrderNo = salesOrders;
                //var salesOrders = spSalesOrderMaster.GetSalesOrderNoIncludePendingCorrespondingtoLedgerforSI(ledgerId, salesInvoiceToEdit, voucherTypeId);
                return Request.CreateResponse(HttpStatusCode.OK, (object)response);
            }
            if (voucherTypeId == 17)
            {
                var deliveryNotes = spDeliveryNoteMasterSp.GetDeleveryNoteNoIncludePendingCorrespondingtoLedgerForSI(ledgerId, salesInvoiceToEdit, voucherTypeId);
                return Request.CreateResponse(HttpStatusCode.OK, deliveryNotes);
            }
            if (voucherTypeId == 10031)
            {
                var salesQuotations = spSalesQuotationMasterSp.GetSalesQuotationIncludePendingCorrespondingtoLedgerForSI(ledgerId, salesInvoiceToEdit, voucherTypeId);
                return Request.CreateResponse(HttpStatusCode.OK, salesQuotations);
            }

            return Request.CreateResponse(HttpStatusCode.OK, "SALES_MODE_FAILURE");
        }

        [HttpGet]
        public HttpResponseMessage GetSalesModeItems(decimal orderNo, string salesMode)
        {
            dynamic response = new ExpandoObject();
            if (salesMode == "17") {
                DeliveryNoteMasterSP SPDeliveryNoteMaster = new DeliveryNoteMasterSP();
                DeliveryNoteDetailsSP SPDeliveryNoteDetails = new DeliveryNoteDetailsSP();

                DataTable dtblMaster = SPDeliveryNoteMaster.SalesInvoiceGridfillAgainestDeliveryNote(orderNo);
                //var dtblDetails = SPDeliveryNoteDetails.SalesInvoiceGridfillAgainestDeliveryNoteUsingDeliveryNoteDetails(orderNo, 0, DecSalesInvoiceVoucherTypeId);
                DataTable dtblDetails = SPDeliveryNoteDetails.SalesInvoiceGridfillAgainestDeliveryNoteUsingDeliveryNoteDetails(orderNo, 0, DecSalesInvoiceVoucherTypeId);

                //dynamic response = new ExpandoObject();
                response.Master = dtblMaster;
                response.Details = dtblDetails;
            }
            else if (salesMode == "10030")
            {
                DataTable dtblMaster = new SalesOrderMasterSP().SalesOrderMasterViewBySalesOrderMasterId(orderNo);
                var a = dtblMaster.Rows[0];
                DataTable dtblDetails = new SalesOrderDetailsSP().SalesOrderDetailsViewBySalesOrderMasterIdWithRemaining(orderNo, 0);
                foreach (DataRow row in dtblDetails.Rows)
                {
                    row["extra2"] = getSalesOrderQuantityRequested(Convert.ToDecimal(row["salesOrderDetailsId"]));
                }
                
                response.Master = dtblMaster;
                response.Details = dtblDetails;
                response.Units = new UnitSP().UnitViewAll();
                response.Stores = new GodownSP().GodownViewAll();
                response.Racks = new RackSP().RackViewAll();
                response.Batches = new BatchSP().BatchViewAll();
                response.Taxes = new TaxSP().TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
            }
            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }

        //public HttpResponseMessage GetSalesModeItems1(decimal orderNo, string salesMode)
        //{
        //    dynamic response = new ExpandoObject();
        //    if (salesMode == "17")
        //    {
        //        DeliveryNoteMasterSP SPDeliveryNoteMaster = new DeliveryNoteMasterSP();
        //        DeliveryNoteDetailsSP SPDeliveryNoteDetails = new DeliveryNoteDetailsSP();

        //        DataTable dtblMaster = SPDeliveryNoteMaster.SalesInvoiceGridfillAgainestDeliveryNote(orderNo);
        //        var dtblDetails = SPDeliveryNoteDetails.SalesInvoiceGridfillAgainestDeliveryNoteUsingDeliveryNoteDetails1(orderNo, 0, DecSalesInvoiceVoucherTypeId);

        //        //dynamic response = new ExpandoObject();
        //        response.Master = dtblMaster;
        //        response.Details = dtblDetails;
        //    }
        //    else if (salesMode == "10030")
        //    {
        //        DataTable dtblMaster = new SalesOrderMasterSP().SalesOrderMasterViewBySalesOrderMasterId(orderNo);
        //        if (dtblMaster.Rows.Count > 0)
        //        {
        //            var a = dtblMaster.Rows[0]; 
        //        }
        //        DataTable dtblDetails = new SalesOrderDetailsSP().SalesOrderDetailsViewBySalesOrderMasterIdWithRemaining(orderNo, 0);
        //        foreach (DataRow row in dtblDetails.Rows)
        //        {
        //            row["extra2"] = getSalesOrderQuantityRequested(Convert.ToDecimal(row["salesOrderDetailsId"]));
        //        }

        //        response.Master = dtblMaster;
        //        response.Details = dtblDetails;
        //        response.Units = new UnitSP().UnitViewAll();
        //        response.Stores = new GodownSP().GodownViewAll();
        //        response.Racks = new RackSP().RackViewAll();
        //        response.Batches = new BatchSP().BatchViewAll();
        //        response.Taxes = new TaxSP().TaxViewAllByVoucherTypeIdApplicaleForProduct(28);
        //    }
        //    return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        //}

        [HttpPost]
        public IHttpActionResult SaveSalesInvoice(SalesInvoiceVM input)
        {
            bool isSavedSuccessfully = false;
            decimal decSalesMasterId = 0.0m;
            SalesMasterSP spSalesMaster = new SalesMasterSP();
            SalesDetailsSP spSalesDetails = new SalesDetailsSP();
            //StockPostingInfo infoStockPosting = new StockPostingInfo();
            // SalesMasterInfo InfoSalesMaster = new SalesMasterInfo();
            //SalesDetailsInfo InfoSalesDetails = new SalesDetailsInfo();
            var spStockPosting = new MatApi.Services.StockPostingSP();
            //AdditionalCostInfo infoAdditionalCost = new AdditionalCostInfo();
            AdditionalCostSP spAdditionalCost = new AdditionalCostSP();
            SalesBillTaxInfo infoSalesBillTax = new SalesBillTaxInfo();
            List<SalesBillTaxInfo> infoSalesBillTaxes = new List<SalesBillTaxInfo>();
            SalesBillTaxSP spSalesBillTax = new SalesBillTaxSP();
            UnitConvertionSP SPUnitConversion = new UnitConvertionSP();
            HelperClasses helperClasses = new HelperClasses();
            var invoiceNo = AutoGenerateFormNo();
            input.SalesMasterInfo.InvoiceNo = invoiceNo;
            input.SalesMasterInfo.VoucherNo = invoiceNo;
            try
            {
                input.SalesMasterInfo.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                input.SalesMasterInfo.SuffixPrefixId = 0;
                input.SalesMasterInfo.SalesAccount = 10; // LedgerConstants.SalesAccount;  //Convert.ToDecimal(cmbSalesAccount.SelectedValue.ToString());
                //input.SalesMasterInfo.UserId = PublicVariables._decCurrentUserId;  /*PublicVariables._decCurrentUserId*/
                input.SalesMasterInfo.LrNo = "NIL";
                input.SalesMasterInfo.TransportationCompany = "NIL";
                input.SalesMasterInfo.POS = false;
                input.SalesMasterInfo.CounterId = 0;
                input.SalesMasterInfo.Narration = "";
                input.SalesMasterInfo.CustomerName = new AccountLedgerSP().AccountLedgerView(input.SalesMasterInfo.LedgerId).LedgerName;
                input.SalesMasterInfo.ExtraDate = DateTime.Now;
                input.SalesMasterInfo.Extra1 = string.Empty;
                input.SalesMasterInfo.Extra2 = string.Empty;
                input.SalesMasterInfo.POSTellerNo = string.Empty;
                input.SalesMasterInfo.transactionDay = helperClasses.GetCurrentDay(input.SalesMasterInfo.UserId /*PublicVariables._decCurrentUserId*/);
                decSalesMasterId = spSalesMaster.SalesMasterAdd(input.SalesMasterInfo);           // updates sales master table       
                if(decSalesMasterId>0)
                {

                    isSavedSuccessfully = true;
                }
                else
                {
                    isSavedSuccessfully = false; 
                }

                string strAgainstInvoiceN0 = input.SalesMasterInfo.InvoiceNo;

                for(int i=0;i<input.SalesDetailsInfo.Count;i++)
                {
                    var product = context.tbl_Product.Find(input.SalesDetailsInfo[i].ProductId);

                    if(product != null)
                    {
                        var unc = new UnitConvertionSP().UnitconversionIdViewByUnitIdAndProductId(input.SalesDetailsInfo[i].UnitId, input.SalesDetailsInfo[i].ProductId);
                        input.SalesDetailsInfo[i].UnitConversionId = unc;
                        input.SalesDetailsInfo[i].SalesMasterId = decSalesMasterId;      // assigns SalesMasterDetailsID
                        input.SalesDetailsInfo[i].ExtraDate = DateTime.Now;
                        input.SalesDetailsInfo[i].Extra1 = string.Empty;
                        input.SalesDetailsInfo[i].Extra2 = string.Empty;
                        decSalesDetailsId = spSalesDetails.SalesDetailsAdd(input.SalesDetailsInfo[i]);       // updates sales details table
                        if (decSalesDetailsId > 0)
                        {
                            isSavedSuccessfully = true;


                            //post goods inTranist ledger if from material receipt
                            if (Convert.ToInt32(input.SalesMode) == Constants.SalesMode_Against_Delivery_Note)
                            {
                                LedgerPosting(new LedgerPostingInfo
                                {
                                    InvoiceNo = invoiceNo,
                                    VoucherNo = invoiceNo,
                                    VoucherTypeId = DecSalesInvoiceVoucherTypeId,
                                    Debit = 0,
                                    //Credit = Convert.ToInt32(product.purchaseRate) * input.SalesDetailsInfo[i].Qty,
                                    Credit = Convert.ToInt32(input.SalesDetailsInfo[i].Rate) * input.SalesDetailsInfo[i].Qty,
                                    LedgerId = LedgerConstants.GoodsInTransit,
                                    YearId = PublicVariables._decCurrentFinancialYearId,
                                    //Date = DateTime.Now,
                                   
                                    ExtraDate = DateTime.Now,
                                    Extra2 = "",
                                    Extra1 = "",
                                    Date = input.SalesMasterInfo.Date,
                                    ChequeDate = input.SalesMasterInfo.Date,
                                    ChequeNo = "",
                                    DetailsId = decSalesDetailsId,

                                });
                            }
                            //if (Convert.ToInt32(input.SalesMode) != Constants.SalesMode_Against_Delivery_Note)
                            //{
                            //record cost of sales ledger transaction  

                            var ledger = context.tbl_AccountLedger.Find(product.expenseAccount);

                            var ProductStockStatusResponse = GetStockStatusValue();

                            List<StockSummary> ProductStockStatus = ProductStockStatusResponse.StockSummary;

                            var Avgcost = ProductStockStatus.Where(a => Convert.ToDecimal(a.ProductId) == product.productId).FirstOrDefault();

                            //Avgcost.AvgCostValue

                            if (Avgcost != null)
                            {
                                LedgerPosting(new LedgerPostingInfo
                                {
                                    InvoiceNo = invoiceNo,
                                    VoucherNo = invoiceNo,
                                    VoucherTypeId = DecSalesInvoiceVoucherTypeId,
                                    //Debit = Convert.ToInt32(product.purchaseRate) * input.SalesDetailsInfo[i].Qty,
                                    Debit = Convert.ToDecimal(Avgcost.AvgCostValue) * input.SalesDetailsInfo[i].Qty,
                                    Credit = 0,
                                    LedgerId = ledger.ledgerId,
                                    YearId = PublicVariables._decCurrentFinancialYearId,
                                    //Date = DateTime.Now,
                                    ExtraDate = DateTime.Now,
                                    Extra2 = "",
                                    Extra1 = "",
                                    //ChequeDate = DateTime.Now,
                                    ChequeNo = "",
                                    DetailsId = decSalesDetailsId,
                                    Date = input.SalesMasterInfo.Date,
                                    ChequeDate = input.SalesMasterInfo.Date,
                                });
                                //}
                                // create income sales
                                var sledger = context.tbl_AccountLedger.Find(product.salesAccount);
                                LedgerPosting(new LedgerPostingInfo
                                {
                                    InvoiceNo = invoiceNo,
                                    VoucherNo = invoiceNo,
                                    VoucherTypeId = DecSalesInvoiceVoucherTypeId,
                                    Debit = 0,
                                    Credit = input.SalesDetailsInfo[i].Amount,
                                    LedgerId = sledger.ledgerId,
                                    YearId = PublicVariables._decCurrentFinancialYearId,
                                    //Date = DateTime.Now,
                                    ExtraDate = DateTime.Now,
                                    Extra2 = "",
                                    Extra1 = "",
                                    //ChequeDate = DateTime.Now,
                                    ChequeNo = "",
                                    DetailsId = decSalesDetailsId,
                                    Date = input.SalesMasterInfo.Date,
                                    ChequeDate = input.SalesMasterInfo.Date,
                                });
                            }

                        }
                        else
                        {
                            isSavedSuccessfully = false;

                            return Ok(new
                            {
                                isSaved = isSavedSuccessfully,
                                id = decSalesMasterId
                            });
                        }

                        listofDetailsId.Add(decSalesDetailsId);

                        //if its not a delivery not dont impact the stock
                        if (Convert.ToInt32(input.SalesMode) != Constants.SalesMode_Against_Delivery_Note)
                        {
                            input.StockPostingInfo = new StockPostingInfo();
                            input.StockPostingInfo.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                            input.StockPostingInfo.ExtraDate = DateTime.Now;
                            input.StockPostingInfo.Date = input.SalesMasterInfo.Date;
                            input.StockPostingInfo.Extra1 = string.Empty;
                            input.StockPostingInfo.Extra2 = string.Empty;
                            input.StockPostingInfo.ProjectId = input.SalesDetailsInfo[i].ProjectId;
                            input.StockPostingInfo.CategoryId = input.SalesDetailsInfo[i].CategoryId;
                            input.StockPostingInfo.ProductId = input.SalesDetailsInfo[i].ProductId;
                            input.StockPostingInfo.BatchId = input.SalesDetailsInfo[i].BatchId;
                            input.StockPostingInfo.GodownId = input.SalesDetailsInfo[i].GodownId;
                            input.StockPostingInfo.UnitId = input.SalesDetailsInfo[i].UnitId;
                            input.StockPostingInfo.Rate = input.SalesDetailsInfo[i].Rate;//Convert.ToDecimal(product.purchaseRate) * input.SalesDetailsInfo[i].Qty; // input.SalesDetailsInfo[i].NetAmount;
                            input.StockPostingInfo.InwardQty = 0;

                            var spUnitConversionId = input.SalesDetailsInfo[i].UnitConversionId;
                            var spUnitConversionValue = spUnitConversionId > 0 ? SPUnitConversion.UnitConversionRateByUnitConversionId(spUnitConversionId) : 1;
                            input.StockPostingInfo.OutwardQty = input.SalesDetailsInfo[i].Qty / spUnitConversionValue;
                            //input.StockPostingInfo.OutwardQty = input.SalesDetailsInfo[i].Qty / SPUnitConversion.UnitConversionRateByUnitConversionId(input.SalesDetailsInfo[i].UnitConversionId);


                            input.StockPostingInfo.VoucherNo = input.SalesMasterInfo.VoucherNo;
                            input.StockPostingInfo.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                            input.StockPostingInfo.InvoiceNo = input.SalesMasterInfo.InvoiceNo;

                            input.StockPostingInfo.AgainstVoucherNo = "NA";
                            input.StockPostingInfo.AgainstVoucherTypeId = 0;
                            input.StockPostingInfo.Extra1 = string.Empty;
                            input.StockPostingInfo.Extra2 = string.Empty;
                            var stockId = spStockPosting.StockPostingAdd(input.StockPostingInfo);
                        }


                    }

                }
                //for(int i=0;i<input.SalesBillTaxInfo.Count;i++)
                for (int i = 0; i < 1; i++)   //1 is used since we'll be implementing tax
                {
                    infoSalesBillTax.SalesMasterId = decSalesMasterId;
                    infoSalesBillTax.TaxAmount = input.SalesDetailsInfo[i].TaxAmount;
                    infoSalesBillTax.TaxId = /*10012*/input.SalesDetailsInfo[i].TaxId;   //since same tax is maintained for all items hence pick first tax in item list
                    infoSalesBillTax.ExtraDate = DateTime.Now;
                    infoSalesBillTax.Extra1 = string.Empty;
                    infoSalesBillTax.Extra2 = string.Empty;
                    if (spSalesBillTax.SalesBillTaxAdd(infoSalesBillTax) > 0)
                    {
                        isSavedSuccessfully = true;
                        infoSalesBillTaxes.Add(infoSalesBillTax);
                    }
                    else
                    {
                        isSavedSuccessfully = false;
                    }

                    LedgerPosting(new LedgerPostingInfo
                    {
                        InvoiceNo = invoiceNo,
                        VoucherNo = invoiceNo,
                        VoucherTypeId = DecSalesInvoiceVoucherTypeId,
                        Credit = input.SalesDetailsInfo[i].TaxAmount,
                        Debit = 0,
                        LedgerId = 59,//LedgerConstants.Vat
                        YearId = PublicVariables._decCurrentFinancialYearId,
                        //Date = DateTime.Now,
                        ExtraDate = DateTime.Now,
                        Extra2 = "",
                        Extra1 = "",
                        //ChequeDate = DateTime.Now,
                        ChequeNo = "",
                        DetailsId = decSalesDetailsId,
                        Date = input.SalesMasterInfo.Date,
                        ChequeDate = input.SalesMasterInfo.Date,
                    });

                }

                if (input.SalesMasterInfo.BillDiscount > 0)
                {
                    LedgerPosting(new LedgerPostingInfo
                    {
                        InvoiceNo = invoiceNo,
                        VoucherNo = invoiceNo,
                        VoucherTypeId = DecSalesInvoiceVoucherTypeId,
                        Debit = input.SalesMasterInfo.BillDiscount,
                        Credit = 0,
                        LedgerId = LedgerConstants.Discount_Allowed,
                        YearId = PublicVariables._decCurrentFinancialYearId,
                        //Date = DateTime.Now,
                        ExtraDate = DateTime.Now,
                        Extra2 = "",
                        Extra1 = "",
                        //ChequeDate = DateTime.Now,
                        ChequeNo = "",
                        DetailsId = decSalesDetailsId,
                        Date = input.SalesMasterInfo.Date,
                        ChequeDate = input.SalesMasterInfo.Date,
                    });
                }

                //applyCreditNote();    //don't apply credit note now(recommended by Mr Alex), we'll create another window
                //to apply credit no

                isSavedSuccessfully = ledgerPostingAdd(decSalesMasterId, decSalesDetailsId,input.SalesMasterInfo,input.SalesDetailsInfo/*,infoSalesBillTaxes*/);     // calls a function to post to ledger
                if (spSalesMaster.SalesInvoiceInvoicePartyCheckEnableBillByBillOrNot(input.SalesMasterInfo.LedgerId))
                {
                    isSavedSuccessfully= partyBalanceAdd(input.SalesMasterInfo);
                }
                
            }
            catch (Exception ex)
            {
            }

            var res = new
            {
                isSaved = isSavedSuccessfully,
                id = decSalesMasterId
            };
            return Ok(res);
        }

        [HttpPost]
        public IHttpActionResult Edit(EditSalesInvoiceDto edit)
        {
            if (edit == null) return BadRequest("No Data Found");
            var auditLogs = new List<TransactionAuditLog>();

            var salesMaster = context.tbl_SalesMaster.Find(edit.SalesMaster.salesMasterId);
            if (salesMaster != null)
            {
                //check for sales return 
                auditLogs.Add(new TransactionAuditLog()
                {
                    TransId = salesMaster.salesMasterId.ToString(),
                    DateTime = DateTime.UtcNow,
                    Amount = salesMaster.totalAmount,
                    VochureTypeId = Constants.SalesInvoice_VoucherTypeId,
                    VochureName = TransactionsEnum.SalesMaster.ToString(),
                    AdditionalInformation = "Edited Sales Master",
                    UserId = PublicVariables._decCurrentUserId,
                    Status = TransactionOperationEnums.Edit.ToString()
                });

                salesMaster.narration = edit.SalesMaster.narration;
                salesMaster.totalAmount = edit.SalesMaster.totalAmount;
                salesMaster.taxAmount = edit.SalesMaster.taxAmount;
                salesMaster.billDiscount = edit.SalesMaster.billDiscount;
                salesMaster.grandTotal = edit.SalesMaster.grandTotal;

                context.Entry(salesMaster).State = EntityState.Modified;


                //edit line items
                var details = context.tbl_SalesDetails.ToList().Where(a => a.salesMasterId == edit.SalesMaster.salesMasterId);

                var detailIds = details.Select(a =>  Convert.ToDecimal(a.salesDetailsId)).ToList();
                var updatedIds = new List<decimal>();


                foreach (var i in edit.SalesDetails)
                {
                    //get item to update

                    var item = details.FirstOrDefault(a => a.salesDetailsId == i.salesDetailsId);
                  
                    if (item != null)
                    {
                        auditLogs.Add(new TransactionAuditLog()
                        {
                            TransId = item.salesDetailsId.ToString(),
                            DateTime = DateTime.UtcNow,
                            Amount = item.amount,
                            Quantity = item.qty,
                            VochureTypeId = Constants.SalesInvoice_VoucherTypeId,
                            VochureName = TransactionsEnum.SalesDetail.ToString(),
                            AdditionalInformation = "Edited Delivery Line  detail Record",
                            UserId = PublicVariables._decCurrentUserId,
                            Status = TransactionOperationEnums.Edit.ToString()
                        });
                        item.discount = i.discount;
                        item.qty = i.qty;
                        item.amount = item.rate * i.qty;
                        context.Entry(item).State = EntityState.Modified;
                        updatedIds.Add(item.salesDetailsId);
                    }
                    if (i.salesDetailsId == 0)
                    {
                        i.extraDate = DateTime.UtcNow;
                        context.tbl_SalesDetails.Add(i);
                    }
                }
               
                var itemToDeleteIds = detailIds.Except(updatedIds);
                foreach (var i in itemToDeleteIds)
                {
                    var item = details.FirstOrDefault(a => a.salesDetailsId == i);
                    if (item != null)
                    {
                        context.tbl_SalesDetails.Remove(item);
                    }
                }
                context.SaveChanges();

                var stockPosting = context.tbl_StockPosting.Where(a =>
                    a.voucherTypeId == Constants.SalesInvoice_VoucherTypeId && a.voucherNo == salesMaster.voucherNo).ToList();
                // delete stock posting 
                if (stockPosting.Any())
                {
                    context.tbl_StockPosting.RemoveRange(stockPosting);
                    context.SaveChanges();
                }
                //get new updated items 
                var updatedItems = context.tbl_SalesDetails
                    .Where(a => a.salesMasterId == salesMaster.salesMasterId)
                    .ToList();

                // add new stock posting to update record
                foreach (var i in updatedItems)
                {
                    var product = context.tbl_Product.Find(i.productId);
                    context.tbl_StockPosting.Add(new DBModel.tbl_StockPosting
                    {
                        //set items here 
                        batchId = i.batchId,
                        CategoryId = i.CategoryId,
                        productId = i.productId,
                        ProjectId = i.ProjectId,
                        extra2 = i.extra2,
                        voucherTypeId = Constants.SalesInvoice_VoucherTypeId,
                        date = DateTime.UtcNow,
                        extraDate = DateTime.UtcNow,
                        financialYearId = PublicVariables._decCurrentFinancialYearId,
                        extra1 = "",
                        rackId = (i.rackId != null)? i.rackId : 0,
                        voucherNo = salesMaster.voucherNo,
                        invoiceNo = salesMaster.invoiceNo,
                        unitId = i.unitId,
                        rate = product.purchaseRate,
                        godownId = i.godownId,
                        outwardQty = 0,
                        inwardQty = i.qty,
                        againstInvoiceNo = "",
                        againstVoucherNo = "",
                        againstVoucherTypeId = 0
                    });
                }
                context.SaveChanges();
                //get all ledger posting base on the vocheur id from the master, then delete them and create a new one 
                var oldLedgerPosting = context.tbl_LedgerPosting
                    .Where(a => a.voucherNo == salesMaster.voucherNo && a.voucherTypeId == Constants.SalesInvoice_VoucherTypeId)
                    .ToList();
                if (oldLedgerPosting.Any())
                {
                    context.tbl_LedgerPosting.RemoveRange(oldLedgerPosting);
                }

                //LedgerPosting Account Recieveable (Customer)
                context.tbl_LedgerPosting.Add(new DBModel.tbl_LedgerPosting()
                {
                    voucherTypeId = Constants.SalesInvoice_VoucherTypeId,
                    date = DateTime.UtcNow,
                    ledgerId = salesMaster.ledgerId,
                    extra2 = "",
                    extraDate = DateTime.UtcNow,
                    voucherNo = salesMaster.voucherNo,
                    extra1 = "",
                    credit = 0,
                    debit = edit.SalesMaster.grandTotal,
                    invoiceNo = salesMaster.invoiceNo,
                    chequeDate = DateTime.UtcNow,
                    yearId = PublicVariables._decCurrentFinancialYearId,
                    detailsId = edit.SalesMaster.salesMasterId,
                    chequeNo = ""
                });

                var goodInTransitLedger = LedgerConstants.GoodsInTransit;
                foreach (var i in updatedItems)
                {
                    var product = context.tbl_Product.Find(i.productId);
                    if (salesMaster.deliveryNoteMasterId > 0)
                    {
                        context.tbl_LedgerPosting.Add(new DBModel.tbl_LedgerPosting()
                        {
                            voucherTypeId = Constants.SalesInvoice_VoucherTypeId,
                            date = DateTime.UtcNow,
                            ledgerId = goodInTransitLedger,
                            extra2 = "",
                            extraDate = DateTime.UtcNow,
                            voucherNo = salesMaster.voucherNo,
                            extra1 = "",
                            credit = i.qty * i.rate,
                            debit = 0,
                            invoiceNo = salesMaster.invoiceNo,
                            chequeDate = DateTime.UtcNow,
                            yearId = PublicVariables._decCurrentFinancialYearId,
                            detailsId = i.salesDetailsId,
                            chequeNo = ""
                        });
                    }


                    //Get AvgCost
                    var ProductStockStatusResponse = GetStockStatusValue();

                    List<StockSummary> ProductStockStatus = ProductStockStatusResponse.StockSummary;

                    var Avgcost = ProductStockStatus.Where(a => Convert.ToDecimal(a.ProductId) == product.productId).FirstOrDefault().AvgCostValue;

                    //cost of sale
                    context.tbl_LedgerPosting.Add(new DBModel.tbl_LedgerPosting()
                    {
                        voucherTypeId = Constants.SalesInvoice_VoucherTypeId,
                        date = DateTime.UtcNow,
                        ledgerId = product.expenseAccount,
                        extra2 = "",
                        extraDate = DateTime.UtcNow,
                        voucherNo = salesMaster.voucherNo,
                        extra1 = "",
                        credit = 0,
                        //debit = i.qty * i.rate,
                        debit = i.qty * Convert.ToDecimal(Avgcost),
                        invoiceNo = salesMaster.invoiceNo,
                        chequeDate = DateTime.UtcNow,
                        yearId = PublicVariables._decCurrentFinancialYearId,
                        detailsId = i.salesDetailsId,
                        chequeNo = ""
                    });

                    //income account
                    context.tbl_LedgerPosting.Add(new DBModel.tbl_LedgerPosting()
                    {
                        voucherTypeId = Constants.SalesInvoice_VoucherTypeId,
                        date = DateTime.UtcNow,
                        ledgerId = product.salesAccount,
                        extra2 = "",
                        extraDate = DateTime.UtcNow,
                        voucherNo = salesMaster.voucherNo,
                        extra1 = "",
                        credit = i.qty * i.rate,
                        debit =0,
                        invoiceNo = salesMaster.invoiceNo,
                        chequeDate = DateTime.UtcNow,
                        yearId = PublicVariables._decCurrentFinancialYearId,
                        detailsId = i.salesDetailsId,
                        chequeNo = ""
                    });

                    context.SaveChanges();
                }
                if (auditLogs.Any())
                {
                    new Services.TransactionAuditService().CreateMany(auditLogs);
                }
                return Ok(new { status = 200, message = "Edit Made Successful" });

            }

            return InternalServerError();
        }

        [HttpGet]
        public IHttpActionResult Delete(int id)
        {
            var audits = new List<TransactionAuditLog>();
            var master = context.tbl_SalesMaster.Find(id);
            if (master == null) return NotFound();

            //check if it has deliveryNote 
            var delivery = context.tbl_DeliveryNoteMaster.Find(master.deliveryNoteMasterId);
            if (delivery != null)
                return Ok(new {status = 200, message = "cant delete this item because it has a reference to delivery"});
            var order = context.tbl_SalesOrderMaster.Find(master.orderMasterId);
            if (order != null) return Ok(new { status = 200, message = "cant delete this item because it has a reference to order" });
            //sales invoice delete from partybalance
            var partyBalance = context.tbl_PartyBalance.Where(a =>a.voucherTypeId == Constants.SalesInvoice_VoucherTypeId && a.voucherNo == master.voucherNo);
            if (partyBalance.Any())
            {
                context.tbl_PartyBalance.RemoveRange(partyBalance);
            }
            else
            {
                //To restrict partyBalances been deleted when payment is currently going on it.
                if (partyBalance.Any()) return Ok(new { status = 200, message = "cant delete this salesInvoice because it has a reference to a receipt" });
            }
            //var receipt = context.tbl_PartyBalance.Where(a => a.voucherTypeId == Constants.SalesInvoice_VoucherTypeId
            //    && a.voucherNo == master.voucherNo && a.againstVoucherTypeId != 0);
            //if (receipt.Any()) return Ok(new { status = 200, message = "cant delete this salesInvoice because it has a reference to a receipt" });


            //find ledgerPosting and Delete  

            var ledgerPosting = context.tbl_LedgerPosting.Where(a =>
                a.voucherTypeId == Constants.SalesInvoice_VoucherTypeId
                && a.voucherNo == master.voucherNo);
            if (ledgerPosting.Any())
            {
                context.tbl_LedgerPosting.RemoveRange(ledgerPosting);
                //audits.AddRange( ledgerPosting.Select(a=> new TransactionAuditLog
                //{

                //}));
            }
            //find all stock posting and delete 
        var stocks =    context.tbl_StockPosting.Where(a => a.voucherTypeId == Constants.SalesInvoice_VoucherTypeId &&
                                                a.voucherNo == master.voucherNo).ToList();
        if (stocks.Any())
        {
            context.tbl_StockPosting.RemoveRange(stocks);
        }
            audits.AddRange(  master.tbl_SalesDetails.Select(a => new TransactionAuditLog
            {
                Quantity = a.qty,
                Amount = a.amount * a.qty,
                UserId = PublicVariables._decCurrentUserId,
                VochureName = Enums.TransactionsEnum.SalesDetail.ToString(),
                DateTime = DateTime.Now,
                Status = Enums.TransactionOperationEnums.Delete.ToString(),
                AdditionalInformation = "Delete Items From Sales Details",
                VochureTypeId = Constants.SalesInvoice_VoucherTypeId,
                TransId = a.salesDetailsId.ToString(),
            }));
            audits.Add(new TransactionAuditLog()
            {
                
                Amount = master.totalAmount,
                UserId = PublicVariables._decCurrentUserId,
                VochureName = Enums.TransactionsEnum.SalesMaster.ToString(),
                DateTime = DateTime.Now,
                Status = Enums.TransactionOperationEnums.Delete.ToString(),
                AdditionalInformation = "Delete Items From Sales Master",
                VochureTypeId = Constants.SalesInvoice_VoucherTypeId,
                TransId = master.salesMasterId.ToString(),
            });


            //delete sales details 
            var details = context.tbl_SalesDetails.Where(a =>  a.salesMasterId == master.salesMasterId).ToList();
            if (details.Any())
            {
                context.tbl_SalesDetails.RemoveRange(details);
            }

            //delete master and detail in 
            context.tbl_SalesMaster.Remove(master);
            context.SaveChanges();
            new Services.TransactionAuditService().CreateMany(audits);


            return Ok(new { status = 200, message = "Delete Made Successful" });
        }
        public bool LedgerPosting(LedgerPostingInfo infoLedgerPosting)
        {
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            var post =      spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
            if (post > 0) return true;
          
            return false;
        }
        public bool partyBalanceAdd(SalesMasterInfo input)
        {
            PartyBalanceInfo infoPartyBalance = new PartyBalanceInfo();
            SalesMasterInfo InfoSalesMaster = new SalesMasterInfo();
            PartyBalanceSP spPartyBalance = new PartyBalanceSP();
            bool isSaved = false;

            try
            {
                infoPartyBalance.Date = input.Date;
                infoPartyBalance.LedgerId = input.LedgerId;
                infoPartyBalance.VoucherNo = strVoucherNo;
                infoPartyBalance.InvoiceNo = input.InvoiceNo;
                infoPartyBalance.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                infoPartyBalance.AgainstVoucherTypeId = 0;
                infoPartyBalance.AgainstVoucherNo = "0";
                infoPartyBalance.AgainstInvoiceNo = "0";
                infoPartyBalance.ReferenceType = "New";
                infoPartyBalance.Debit = input.GrandTotal;
                infoPartyBalance.Credit = 0;
                infoPartyBalance.CreditPeriod = input.CreditPeriod;
                infoPartyBalance.ExchangeRateId = input.ExchangeRateId;
                infoPartyBalance.FinancialYearId = PublicVariables._decCurrentFinancialYearId;
                infoPartyBalance.ExtraDate = DateTime.Now;
                infoPartyBalance.Extra1 = string.Empty;
                infoPartyBalance.Extra2 = string.Empty;
                if(spPartyBalance.PartyBalanceAdd(infoPartyBalance)>0)
                {
                    isSaved = true;
                }
            }
            catch (Exception ex)
            {
                
               // formMDI.infoError.ErrorString = "SI74:" + ex.Message;
            }
            return isSaved;
        }
        private decimal getSalesOrderQuantityRequested(decimal salesOrderDetailsId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("SELECT qty from tbl_salesorderdetails WHERE salesorderdetailsid={0}", salesOrderDetailsId);
            return Convert.ToDecimal(conn.getSingleValue(queryStr));
        }
      
        public bool ledgerPostingAdd(decimal decSalesMasterId, decimal decSalesDetailsId,SalesMasterInfo salesMasterInfo,List<SalesDetailsInfo> salesDetailsInfo/*, List<SalesBillTaxInfo> salesTax*/)
        {
            LedgerPostingInfo infoLedgerPosting = new LedgerPostingInfo();
            SalesMasterInfo InfoSalesMaster = new SalesMasterInfo();
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            ExchangeRateSP spExchangeRate = new ExchangeRateSP();
            decimal decRate = 0;
            decimal decimalGrantTotal = 0;
            decimal decTotalAmount = 0;
            int itemsinList = 0;
            bool isSaved = false;

            try
            {
                //account recieveable
                decimalGrantTotal = salesMasterInfo.GrandTotal;
                decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(salesMasterInfo.ExchangeRateId);

                decimalGrantTotal = decimalGrantTotal * decRate;

                infoLedgerPosting.Debit = decimalGrantTotal;
                infoLedgerPosting.Credit = 0;
                infoLedgerPosting.Date = salesMasterInfo.Date;
                infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                infoLedgerPosting.VoucherNo = salesMasterInfo.InvoiceNo/*strVoucherNo*/;
                infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                infoLedgerPosting.LedgerId = salesMasterInfo.LedgerId;
                infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //infoLedgerPosting.DetailsId = 0;
                infoLedgerPosting.DetailsId = decSalesMasterId;
                infoLedgerPosting.ChequeNo = string.Empty;
                infoLedgerPosting.ChequeDate = salesMasterInfo.Date;
                infoLedgerPosting.Date = salesMasterInfo.Date;
                infoLedgerPosting.Extra1 = string.Empty;
                infoLedgerPosting.Extra2 = string.Empty;
                if(spLedgerPosting.LedgerPostingAdd(infoLedgerPosting)>0)        // posts to debit side
                {
                    isSaved = true;
                }
                ///---------credit section
                ///
                //#region new implementation
                //foreach (var row in salesDetailsInfo)
                //{
                //    LedgerPostingSP ledgerPostingSalesAccountId = new LedgerPostingSP();
                //    decTotalAmount = row.NetAmount;
                //    decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(salesMasterInfo.ExchangeRateId);
                //    decTotalAmount = decTotalAmount * decRate;
                //    infoLedgerPosting.Debit = 0;
                //    infoLedgerPosting.Credit = row.NetAmount;
                //    infoLedgerPosting.Date = salesMasterInfo.Date;
                //    infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                //    infoLedgerPosting.VoucherNo = strVoucherNo;
                //    infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                //    // Old implementation changed by Urefe because of Precious' work that removed/hide sales account selection from UI
                //    // infoLedgerPosting.LedgerId = Convert.ToDecimal(row.Cells["dgvtxtsalesAccount"].Value.ToString()); 
                //    infoLedgerPosting.LedgerId = ledgerPostingSalesAccountId.ProductSalesAccountId(new ProductSP().ProductView(row.ProductId).ProductCode);
                //    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //    //infoLedgerPosting.DetailsId = 0;
                //    infoLedgerPosting.DetailsId = listofDetailsId[itemsinList]; //decSalesDetailsId;
                //    infoLedgerPosting.ChequeNo = string.Empty;
                //    infoLedgerPosting.ChequeDate = DateTime.Now;
                //    infoLedgerPosting.Extra1 = string.Empty;
                //    infoLedgerPosting.Extra2 = string.Empty;
                //    if(spLedgerPosting.LedgerPostingAdd(infoLedgerPosting)>0)    // post to credit side
                //    {
                //        isSaved = true;
                //        // post to Debit side
                //        infoLedgerPosting.Credit = 0;
                //        infoLedgerPosting.Debit =  context.tbl_Product.Find(row.ProductId) != null ?
                //            (decimal)context.tbl_Product.Find(row.ProductId).purchaseRate * row.Qty
                //                : 0.0m;   //TotalNetAmount(); //* spExchangeRate.ExchangeRateViewByExchangeRateId(Convert.ToDecimal(cmbCurrency.SelectedValue.ToString()));
                //                                                 //infoLedgerPosting.Date = Convert.ToDateTime(PublicVariables._dtCurrentDate);     // ---------- old implementation uses system current date ---------- //
                //         infoLedgerPosting.Date = DateTime.UtcNow;
                //        //infoLedgerPosting.DetailsId = 0;
                //        infoLedgerPosting.DetailsId = listofDetailsId[itemsinList];
                //        infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                //        //infoLedgerPosting.LedgerId = infoPurchaseMaster.PurchaseAccount;//ledger posting of purchase account  dgvrow.Cells["dgvcmbTax"].Value.ToString
                //        // ----------------- Old implementation changed by Precious and then Urefe 20160919 ------------------- //
                //        //infoLedgerPosting.LedgerId = Convert.ToDecimal(row.Cells["dgvtxtExpenseAccount"].Value.ToString()); 
                //        infoLedgerPosting.LedgerId = ledgerPostingSalesAccountId.ProductExpenseAccountId(new ProductSP().ProductView(row.ProductId).ProductCode);
                     
                //        infoLedgerPosting.VoucherNo = strVoucherNo;
                //        infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                //        infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //        infoLedgerPosting.ChequeDate = DateTime.Now;
                //        infoLedgerPosting.ChequeNo = string.Empty;
                //        infoLedgerPosting.Extra1 = string.Empty;
                //        infoLedgerPosting.Extra2 = string.Empty;
                //        infoLedgerPosting.ExtraDate = DateTime.Now;
                //        spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                //    }
                //    else
                //    {
                //        isSaved = false;
                //    }
                //    itemsinList++;
                //}
                //#endregion
                
                //decimal decBillDis = 0;
                //decBillDis = salesMasterInfo.BillDiscount;
                //decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(salesMasterInfo.ExchangeRateId);
                //decBillDis = decBillDis * decRate;
                //if (decBillDis > 0)
                //{
                //    infoLedgerPosting.Debit = decBillDis;
                //    infoLedgerPosting.Credit = 0;
                //    infoLedgerPosting.Date = salesMasterInfo.Date;
                //    infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                //    infoLedgerPosting.VoucherNo = strVoucherNo;
                //    infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                //    infoLedgerPosting.LedgerId = 8;
                //    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //    infoLedgerPosting.DetailsId = 0;
                //    infoLedgerPosting.ChequeNo = string.Empty;
                //    infoLedgerPosting.ChequeDate = DateTime.Now;
                //    infoLedgerPosting.Extra1 = string.Empty;
                //    infoLedgerPosting.Extra2 = string.Empty;
                //    if(spLedgerPosting.LedgerPostingAdd(infoLedgerPosting)>0)
                //    {
                //        isSaved = true;
                //    }
                //    else
                //    {
                //        isSaved = false;
                //    }
                //}

                foreach (var i in salesDetailsInfo)
                {
                    infoLedgerPosting.Debit = 0;
                    infoLedgerPosting.Credit = i.TaxAmount;
                    infoLedgerPosting.Date = salesMasterInfo.Date;
                    infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                    infoLedgerPosting.VoucherNo = strVoucherNo;
                    infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                    infoLedgerPosting.LedgerId = i.TaxId;//salesMasterInfo.LedgerId;
                    infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                    infoLedgerPosting.DetailsId = 0;
                    infoLedgerPosting.ChequeNo = string.Empty;
                    infoLedgerPosting.ChequeDate = DateTime.Now;
                    infoLedgerPosting.Extra1 = string.Empty;
                    infoLedgerPosting.Extra2 = string.Empty;
                    
                    if(i.TaxId != 2)
                    {
                       // spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
                    }
                }
                //foreach (var row in salesTax)
                //{
                //    decimal decTaxAmount = 0;
                //    decTaxAmount = row.TaxAmount;
                //    decRate = spExchangeRate.ExchangeRateViewByExchangeRateId(salesMasterInfo.ExchangeRateId);
                //    decTaxAmount = decTaxAmount * decRate;
                //    if (decTaxAmount > 0)
                //    {
                //        infoLedgerPosting.Debit = 0;
                //        infoLedgerPosting.Credit = decTaxAmount;
                //        infoLedgerPosting.Date = salesMasterInfo.Date;
                //        infoLedgerPosting.VoucherTypeId = DecSalesInvoiceVoucherTypeId;
                //        infoLedgerPosting.VoucherNo = strVoucherNo;
                //        infoLedgerPosting.InvoiceNo = salesMasterInfo.InvoiceNo;
                //        infoLedgerPosting.LedgerId = 59;//salesMasterInfo.LedgerId;
                //        infoLedgerPosting.YearId = PublicVariables._decCurrentFinancialYearId;
                //        infoLedgerPosting.DetailsId = 0;
                //        infoLedgerPosting.ChequeNo = string.Empty;
                //        infoLedgerPosting.ChequeDate = DateTime.Now;
                //        infoLedgerPosting.Extra1 = string.Empty;
                //        infoLedgerPosting.Extra2 = string.Empty;
                //        if(spLedgerPosting.LedgerPostingAdd(infoLedgerPosting)>0)
                //        {
                //            isSaved = true;
                //        }
                //        else
                //        {
                //            isSaved = false;
                //        }
                //    }
                //}
                
            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "SI73:" + ex.Message;
            }
            return isSaved;
        }

        private void suffixPrefix()
        {
            TransactionsGeneralFill obj = new TransactionsGeneralFill();
            SalesMasterSP spSalesMaster = new SalesMasterSP();
            if (strVoucherNo == string.Empty)
            {
                strVoucherNo = "0";
            }
            strVoucherNo = obj.VoucherNumberAutomaicGeneration(DecSalesInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");
            if (Convert.ToDecimal(strVoucherNo) != (spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId)))
            {
                strVoucherNo = spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId).ToString();
                strVoucherNo = obj.VoucherNumberAutomaicGeneration(DecSalesInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");
                if (spSalesMaster.SalesMasterVoucherMax(DecSalesInvoiceVoucherTypeId) == 0)
                {
                    strVoucherNo = "0";
                    strVoucherNo = obj.VoucherNumberAutomaicGeneration(DecSalesInvoiceVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, "SalesMaster");
                }
            }
            SuffixPrefixSP spSuffisprefix = new SuffixPrefixSP();
            SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
            infoSuffixPrefix = spSuffisprefix.GetSuffixPrefixDetails(DecSalesInvoiceVoucherTypeId, DateTime.Now);
            strPrefix = infoSuffixPrefix.Prefix;
            strSuffix = infoSuffixPrefix.Suffix;
            strInvoiceNo = strPrefix + strVoucherNo + strSuffix;
            decSalseInvoiceSuffixPrefixId = infoSuffixPrefix.SuffixprefixId;
        }
        
        [HttpPost]
        public async Task<IHttpActionResult> GetSaleInvoiceFromMaster(DateTime? fromDate = null, DateTime? toDate =null )
        {
            var from = PublicVariables._dtFromDate;
            var to = PublicVariables._dtToDate;
            if (fromDate != null)
            {
                from = (DateTime) fromDate;
            }

            if (toDate != null)
            {
                to =(DateTime) toDate;
                to = to.AddDays(+1).AddSeconds(-1);
            }
            try
            {
                // get invoice record base on finicial date by default 

                context = new DBMATAccounting_MagnetEntities1();
                context.Configuration.LazyLoadingEnabled = false;

                var invoices = await context.tbl_SalesMaster.Where(a => a.date >= from && a.date <= to)
                    .Include(a => a.tbl_AccountLedger)
                    .Include(a => a.tbl_SalesDetails)
                    .Select(c => new
                    {
                        SaleMaster = c,
                        SaleDetail = c.tbl_SalesDetails.ToList(),
                        Ledger = c.tbl_AccountLedger,
                        User = c.tbl_User
                    })
                    .ToListAsync();
            

                //return Content<object>(HttpStatusCode.OK, invoices);

                return Ok(invoices);

            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                throw;
            }
            return null;
        }
         
        [HttpGet]
        public IHttpActionResult GetSalesOrderAndDeliverFromMaster(int saleMasterId)
        {
            try
            {
                context = new DBMATAccounting_MagnetEntities1();
                context.Configuration.LazyLoadingEnabled = false;

                var invoices = context.tbl_SalesMaster.Where(a => a.salesMasterId == saleMasterId)
                    .Include(a=>a.tbl_SalesDetails)
                    .Include(a=>a.tbl_AccountLedger)
                    .Include(a=>a.tbl_User)
                    .Include(a=>a.tbl_SalesDetails.Select(b=>b.tbl_Product))
                    .ToList()
                    .Select(a=>new
                    {
                        SaleMaster = a,
                        SaleDetail = a.tbl_SalesDetails.ToList(),
                        Ledger = a.tbl_AccountLedger,
                        User =a.tbl_User,
                        Prduct =  a.tbl_SalesDetails.Select(c => c.tbl_Product).ToList(),
                        ProductFromSale = a.tbl_SalesDetails.Select(c => c.tbl_Product).ToList()
                    }).ToList();
                    //.Select( o =>new
            //    {
            //        SaleMaster = o.SaleMaster,
            //        SaleDetail = o.SaleDetail,
            //        Ledger = o.Ledger,
            //        User = o.User,
            //        Deliver = o.DeliverMasterDetail.Select(a=>a.DeliveryDetail),
            //        Order = o.SalesOrderDetailProduct.Select(a=> new {a.SaleOrder}),
            //        Prduct = o.SalesOrderDetailProduct.Select(a=> new {a.Product}),
            //        ProductFromSale = context.tbl_Product.Where(a=>a.productId == o.SaleDetail.productId).ToList()
            //    });
            return Ok(invoices);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
               
            }
            return null;
        }

        public IHttpActionResult GetInvoiceDetailsFromMaster(int id)
        {
            context = new DBMATAccounting_MagnetEntities1();
            context.Configuration.LazyLoadingEnabled = false;
            var details = context.tbl_SalesDetails.Where(a => a.salesMasterId == id)
                .Include(a => a.tbl_Product)
                .Include(a => a.tbl_SalesMaster)
                .Include(a=>a.tbl_SalesMaster.tbl_AccountLedger)
                .ToList();
            var data = new
            {
                
                details= details.Select(a => new
                {
                    detail = a,
                    product = a.tbl_Product,
                    ledger = a.tbl_SalesMaster.tbl_AccountLedger,
                    master = a.tbl_SalesMaster,
                })
            };
            return Ok(data);
        }
        [HttpGet]
        public IHttpActionResult GetInvoiceDetailsFromLegderPosting(int salesMasterId, int ledgerId)
        {
            context.Configuration.LazyLoadingEnabled = false;
            var salesD = context.tbl_SalesDetails.Where(a => a.salesMasterId == salesMasterId).Select(b => b.salesDetailsId).ToList();
            List<DBModel.tbl_LedgerPosting> ledgerPostingDetails = new List<DBModel.tbl_LedgerPosting>();
            foreach (var item in salesD)
            {
                var a = context.tbl_LedgerPosting.Where(b => b.ledgerId == ledgerId && b.detailsId == item).FirstOrDefault();
                if (a != null)
                {
                    ledgerPostingDetails.Add(a);
                }
            }

            return Ok(ledgerPostingDetails);
        }
        public List<tbl_Product> GetProducts()
        {
            return null;
        }
        public dynamic GetStockStatusValue()
        {
            List<StockSummary> stockSummary = new List<StockSummary>();
            dynamic response = new ExpandoObject();

            decimal returnValue = 0;
            try
            {
                DBMatConnection conn = new DBMatConnection();
                StockPostingSP spstock = new StockPostingSP();
                DataTable dtbl = new DataTable();
                decimal productId = 0;
                string pCode = "";
                decimal storeId = 0;
                string batch = "All";

                string queryStr = "select top 1 f.fromDate from tbl_FinancialYear f";
                DateTime lastFinYearStart = Convert.ToDateTime(conn.getSingleValue(queryStr));

                //dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime("04/30/2017") /*PublicVariables._dtToDate*/, pCode, "");
               
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, PublicVariables._dtToDate, pCode, "", 0, 0);
                 
                decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                decimal prevProductId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                 string productName = "", storeName = "", productCode = "";
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal totalAssetValue = 0, qtyBalance = 0;
                decimal value1 = 0;
                int i = 0;
                bool isAgainstVoucher = false;

                foreach (DataRow dr in dtbl.Rows)
                {
                    currentProductId = Convert.ToDecimal(dr["productId"]);
                    currentGodownID = Convert.ToDecimal(dr["godownId"]);
                    productCode = dr["productCode"].ToString();
                    string voucherType = "", refNo = "";

                    decimal inwardQty = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == prevProductId
                                         select p.Field<decimal>("inwardQty")).Sum();
                    decimal outwardQty = (from p in dtbl.AsEnumerable()
                                          where p.Field<decimal>("productId") == prevProductId
                                          select p.Field<decimal>("outwardQty")).Sum();

                    inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                    outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                    decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                    string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                    rate2 = Convert.ToDecimal(dr["rate"]);
                    if (previousRunningAverage != 0 && currentProductId == prevProductId)
                    {
                        purchaseRate = previousRunningAverage;
                    }
                    if (currentProductId == prevProductId)
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        prevProductId = Convert.ToDecimal(dr["productId"]);
                        productCode = dr["productCode"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    else
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    //======================== 2017-02-27 =============================== //
                    //if (againstVoucherType != "NA")
                    //{
                    //    voucherType = againstVoucherType;
                    //}
                    //else
                    //{
                    //    voucherType = dr["typeOfVoucher"].ToString();
                    //}
                    refNo = dr["refNo"].ToString();
                    if (againstVoucherType != "NA")
                    {
                        refNo = dr["againstVoucherNo"].ToString();
                        isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                        againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                        //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                        string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                        string typeOfVoucher = conn.getSingleValue(queryString);
                        if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                        if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                    }
                    voucherType = dr["typeOfVoucher"].ToString();
                    //---------  COMMENT END ----------- //
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if (outwardQty2 > 0)
                    {
                        if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = purchaseRate;
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                        {
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue -= outwardQty2 * rate2;
                        }

                        else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Sales Order")
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                    }
                    else if (voucherType == "Sales Return" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Rejection In" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;

                        }
                        else
                        {
                            computedAverageRate = previousRunningAverage; // (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                        // voucherType = "Sales Return";
                    }
                    else if (voucherType == "Delivery Note")
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                    }
                    else
                    {
                        // hndles other transactions such as purchase, opening balance, etc
                        qtyBal += inwardQty2 - outwardQty2;
                        stockValue += (inwardQty2 * rate2);
                    }
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        //qtyBal = Convert.ToDecimal(dr["inwardQty"]);
                        //stockValue = inwardQty2 * rate2;
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        if (voucherType == "Sales Invoice")
                        {
                            stockValue = qtyBal * purchaseRate;
                        }
                        else
                        {
                            stockValue = qtyBal * rate2;
                        }
                        //totalAssetValue += Math.Round(value1, 2);
                        totalAssetValue += value1;
                        //i++;
                        returnValue = totalAssetValue;
                    }
                    // -----------------added 2017-02-21 -------------------- //
                    if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        if (voucherType == "Sales Invoice")
                        {
                            computedAverageRate = purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = rate2;
                        }
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        stockValue = qtyBal * computedAverageRate;
                    }
                    // ------------------------------------------------------ //

                    previousRunningAverage = computedAverageRate;

                    value1 = stockValue;
                    prevProductId = currentProductId;
                    prevGodownID = currentGodownID;

                    if (i == dtbl.Rows.Count - 1)
                    {
                        var avgCost = "";
                        if (value1 != 0 && (inwardqt - outwardqt) != 0)
                        {
                            avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                        }
                        else
                        {
                            avgCost = "0.0";
                        }

                        stockSummary.Add(new StockSummary
                        {
                            ProductId = dr["productId"].ToString(),
                            StoreId = dr["godownId"].ToString(),
                            ProductName = dr["productName"].ToString(),
                            StoreName = dr["godownName"].ToString(),
                            PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                            SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                            QtyBalance = (inwardqt - outwardqt).ToString(),
                            AvgCostValue = avgCost,
                            StockValue = value1.ToString("N2"),
                            ProductCode = dr["productCode"].ToString(),
                            UnitName = string.IsNullOrEmpty(dr["unitName"].ToString()) ? "" : dr["unitName"].ToString()
                        });
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                    {
                        var avgCost = "";
                        if (value1 != 0 && (inwardqt - outwardqt) != 0)
                        {
                            avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                        }
                        else
                        {
                            avgCost = "0.0";
                        }

                        stockSummary.Add(new StockSummary
                        {
                            ProductId = dr["productId"].ToString(),
                            StoreId = dr["godownId"].ToString(),
                            ProductName = dr["productName"].ToString(),
                            StoreName = dr["godownName"].ToString(),
                            PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                            SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                            QtyBalance = (inwardqt - outwardqt).ToString(),
                            AvgCostValue = avgCost,
                            StockValue = value1.ToString("N2"),
                            ProductCode = dr["productCode"].ToString(),
                            UnitName = string.IsNullOrEmpty(dr["unitName"].ToString()) ? "" : dr["unitName"].ToString()
                        });
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    i++;
                }
                decimal totalStockValue = 0;
                foreach (var row in stockSummary)
                {
                    totalStockValue += Convert.ToDecimal(row.StockValue);
                }

                stockSummary.Add(new StockSummary
                {
                    ProductName = "",
                    StoreName = "Total Stock Value:",
                    PurchaseRate = "",
                    SalesRate = "",
                    QtyBalance = qtyBalance.ToString("N2"),
                    AvgCostValue = (totalStockValue / qtyBalance).ToString("N2"),
                    StockValue = totalStockValue.ToString("N2"),
                    ProductCode = ""
                });

                response.TotalStockValue = totalStockValue.ToString("N2");
                response.StockSummary = stockSummary;
            }
            catch (Exception ex)
            {
                
            }

            return response;
        }
    }  
}
