﻿using MatApi.Models;
using MATFinancials;
using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Dynamic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using MatApi.DBModel;
using MatApi.Dto;
using MatApi.Enums;
using MatDal;
using PublicVariables = MatApi.Models.PublicVariables;
using EntityState = System.Data.Entity.EntityState;
using TransactionAuditLog = MatApi.DBModel.TransactionAuditLog;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class RejectionInController : ApiController
    {
        private DBMATAccounting_MagnetEntities1 context;
        public RejectionInController()
        {
            context = new DBMATAccounting_MagnetEntities1();
        }

        decimal decRejectionInVoucherTypeId = 18;
        decimal decRejectionInSuffixPrefixId = 0;
        decimal decRejectionInIdToEdit = 0;
        decimal decCurrentConversionRate = 0;
        decimal decCurrentRate = 0;
        string strVoucherNo = string.Empty;
        string strSalesman = string.Empty;
        string strCashorParty = string.Empty;
        string strRejectionInVoucherNo = string.Empty;
        TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();
        RejectionInMasterSP spRejectionInMaster = new RejectionInMasterSP();

        [HttpGet]
        public HttpResponseMessage LookUps()
        {
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var customers = transactionGeneralFillObj.CashOrPartyUnderSundryDrComboFill();
            var salesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();
            var pricingLevels = transactionGeneralFillObj.PricingLevelViewAll();
            var voucherTypes = transactionGeneralFillObj.VoucherTypeComboFill("Delivery Note", false);
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(MATFinancials.PublicVariables._dtCurrentDate);
            dynamic response = new ExpandoObject();
            response.Customers = customers;
            response.SalesMen = salesMen;
            response.PricingLevels = pricingLevels;
            response.Currencies = currencies;
            response.VoucherTypes = voucherTypes;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetDeliveryNoteNo(decimal customerId, decimal voucherType)
        {
            DeliveryNoteMasterSP spdeliverynotemaster = new DeliveryNoteMasterSP();
            var deliveryNotesNo = spdeliverynotemaster.DeliveryNoteNoCorrespondingToLedger(customerId, 0, voucherType);
            return Request.CreateResponse(HttpStatusCode.OK, deliveryNotesNo);
        }

        [HttpGet]
        public List<RejectionInLineItems> FillGridCorrespondingToDeliveryNoteNo(decimal deliveryNoteNo)
        {
            DeliveryNoteDetailsSP SPDeliveryNoteDetails = new DeliveryNoteDetailsSP();
            List<RejectionInLineItems> response = new List<RejectionInLineItems>();
            try
            {
                var notes = SPDeliveryNoteDetails.DeliveryNoteDetailsViewByDeliveryNoteMasterIdWithPending(deliveryNoteNo, 0);
                for (int i = 0; i < notes.Rows.Count; i++)
                {
                    var note = notes.Rows[i].ItemArray;
                    var prod = new ProductSP().ProductView(Convert.ToDecimal(note[2]));
                    response.Add(new RejectionInLineItems
                    {
                        DeliveryNoteDetailsId = Convert.ToDecimal(note[0]),
                        Amount = Convert.ToDecimal(note[10]),
                        Barcode = note[11].ToString(),
                        Batch = note[9].ToString(),
                        BatchId = Convert.ToDecimal(note[9]),
                        ProductId = prod.ProductId,
                        ProductCode = prod.ProductCode,
                        ProductName = prod.ProductName,
                        Quantity = Convert.ToDecimal(note[3]),
                        Rack = new RackSP().RackView(Convert.ToDecimal(note[8])).RackName,
                        RackId = Convert.ToDecimal(note[8]),
                        Rate = Convert.ToDecimal(note[4]),
                        Store = new GodownSP().GodownView(Convert.ToDecimal(note[7])).GodownName,
                        StoreId = Convert.ToDecimal(note[7]),
                        Unit = new UnitSP().UnitView(Convert.ToDecimal(note[5])).UnitName,
                        UnitId = Convert.ToDecimal(note[5]),
                        UnitConversionId = Convert.ToDecimal(note[6])
                    });
                }

            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "RI12:" + ex.Message;
            }
            return response;
        }

        [HttpGet]
        public IHttpActionResult GetRejectionInItems(int id)
        {
            var record = context.tbl_RejectionInDetails.Where(a => a.rejectionInMasterId == id).ToList();
            List<RejectionInLineItems> response = new List<RejectionInLineItems>();

            foreach (var i in record)
            {
                response.Add(new RejectionInLineItems
                {
                    DeliveryNoteDetailsId = Convert.ToDecimal(i.deliveryNoteDetailsId),
                    Amount = Convert.ToDecimal(i.amount),
                    RejectionInDetailsId = i.rejectionInDetailsId,
                    Barcode = i.tbl_Product.productCode,
                    Batch = "",
                    BatchId = Convert.ToDecimal(i.batchId),
                    ProductId = i.tbl_Product.productId,
                    ProductCode = i.tbl_Product.productCode,
                    ProductName = i.tbl_Product.productName,
                    Quantity = Convert.ToDecimal(i.tbl_DeliveryNoteDetails.qty),
                    Rack = new RackSP().RackView(Convert.ToDecimal(i.rackId)).RackName,
                    RackId = Convert.ToDecimal(i.rackId),
                    Rate = Convert.ToDecimal(i.rate),
                    Store = new GodownSP().GodownView(Convert.ToDecimal(i.godownId)).GodownName,
                    StoreId = Convert.ToDecimal(i.godownId),
                    Unit = new UnitSP().UnitView(Convert.ToDecimal(i.unitId)).UnitName,
                    UnitId = Convert.ToDecimal(i.unitId),
                    UnitConversionId = Convert.ToDecimal(i.unitConversionId),
                    QtyToReturn = Convert.ToInt32(i.qty)
                });
            }
            return Ok(response);
        }
        //[HttpPost]
        //public bool savePendingRejectionIn(CreateRejectionInVM input)
        //{
        //    try
        //    {
        //        MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
        //        input.Date = DateTime.Now;
        //        var status = "Pending";
        //        string masterQuery = string.Format("INSERT INTO tbl_RejectionInMaster_Pending " +
        //                                            "VALUES('{0}','{1}',{2},{3},'{4}',{5},'{6}',{7},'{8}',{9},{10},{11},'{12}',{13},'{14}',{15},'{16}','{17}','{18}', '{19}');"
        //                                            , input.ReceiptNo,
        //                                            input.ReceiptNo,
        //                                            1,
        //                                            decMaterialReceiptVoucherTypeId,
        //                                            input.Date,
        //                                            input.SupplierId,
        //                                            "",
        //                                            input.OrderMasterId,
        //                                            input.Narration,
        //                                            input.TotalAmount,
        //                                            1,
        //                                            MATFinancials.PublicVariables._decCurrentUserId,
        //                                            "",
        //                                            1,
        //                                            "",
        //                                            MATFinancials.PublicVariables._decCurrentFinancialYearId,
        //                                            DateTime.Now,
        //                                            "",
        //                                            "",
        //                                            approved);

        //        string detailsQuery = string.Format("");
        //        if (db.ExecuteNonQuery2(masterQuery))
        //        {
        //            string id = db.getSingleValue("SELECT top 1 materialReceiptMasterId from tbl_MaterialReceiptMaster_Pending order by materialReceiptMasterId desc");
        //            foreach (var row in input.LineItems)
        //            {
        //                var amount = row.Rate * row.Quantity;
        //                detailsQuery = string.Format("INSERT INTO tbl_MaterialReceiptDetails_Pending " +
        //                                            "VALUES({17},{0},{1},{2},{3},{4},{5},{6},{7},{8},{9},{10},'{11}','{12}','{13}',{14},{15},'{16}')"
        //                                            , row.ProductId,
        //                                            row.OrderDetailsId,
        //                                            row.Quantity,
        //                                            row.Rate,
        //                                            row.UnitId,
        //                                            row.UnitConversionId,
        //                                            row.BatchId,
        //                                            row.StoreId,
        //                                            row.RackId,
        //                                            amount,
        //                                            row.SL,
        //                                            DateTime.Now,
        //                                            "",
        //                                            "",
        //                                            0,
        //                                            0,
        //                                            row.Description,
        //                                            id);
        //                db.ExecuteNonQuery2(detailsQuery);
        //                amount = 0;
        //            }
        //            if (input.OrderDetailsId > 0)
        //            {
        //                string status = "Approved";
        //                string updateQuery = string.Format("");
        //                updateQuery = string.Format("UPDATE tbl_PurchaseOrderMaster " +
        //                                            "SET orderStatus='{0}' " +
        //                                            "WHERE purchaseOrderMasterId={1} ", status, input.OrderMasterId);
        //                if (db.customUpdateQuery(updateQuery) > 0)
        //                {
        //                    return true;
        //                }
        //            }
        //            else
        //            {
        //                return true;
        //            }
        //        }
        //        //db.CloseConnection();
        //    }
        //    catch (Exception e)
        //    {

        //    }

        //    return false;
        //}
        [HttpGet]
        public HttpResponseMessage getPendingRejectionIn(DateTime fromDate, DateTime toDate, string approved)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            dynamic response = new ExpandoObject();

            string GetQuery = string.Format("");
            if (approved == "All" || approved == "" || approved == null)
            {
                GetQuery = string.Format("");
                GetQuery = string.Format("SELECT * FROM tbl_RejectionInMaster_Pending WHERE date BETWEEN '{0}' AND '{1}' ORDER BY rejectionInMasterId DESC", fromDate, toDate);
            }
            else
            {
                GetQuery = string.Format("");
                GetQuery = string.Format("SELECT * FROM tbl_RejectionInMaster_Pending WHERE date BETWEEN '{0}' AND '{1}' AND status='{2}' ORDER BY rejectionInMasterId DESC", fromDate, toDate, approved);
            }
            response.pendingRejectionIns = db.GetDataSet(GetQuery);
            response.Products = new ProductSP().ProductViewAll();
            response.unit = new UnitSP().UnitViewAll();
            response.batch = new BatchSP().BatchViewAll();
            response.stores = new GodownSP().GodownViewAll();
            response.ledgers = new AccountLedgerSP().AccountLedgerViewCustomerOnly();
            response.users = new UserSP().UserViewAll();
            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        public IHttpActionResult GetRejectionIn(DateTime fromDate, DateTime toDate, string approved)
        {
            context = new DBMATAccounting_MagnetEntities1();
            context.Configuration.LazyLoadingEnabled = false;
            var res = context.tbl_RejectionInMaster
                .Where(a => a.date >= fromDate && a.date <= toDate)
                .ToList();
            var response = new
            {
                rejectionIns = res,
                products = new ProductSP().ProductViewAll(),
                unit = new UnitSP().UnitViewAll(),
                batch = new BatchSP().BatchViewAll(),
                stores = new GodownSP().GodownViewAll(),
                ledgers = new AccountLedgerSP().AccountLedgerViewCustomerOnly(),
                users = new UserSP().UserViewAll(),
            };
            return Ok(response);
        }

        [HttpGet]
        public HttpResponseMessage getPendingRejectionInDetails(decimal id)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            dynamic response = new ExpandoObject();

            string getQuery = "";
            getQuery = string.Format("SELECT * FROM tbl_RejectionInDetails_Pending WHERE rejectionInMasterId = {0}", id);
            response.details = db.GetDataSet(getQuery);

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetRejectionInDetails(decimal id)
        {
            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            dynamic response = new ExpandoObject();

            string getQuery = "";
            getQuery = string.Format("SELECT * FROM tbl_RejectionInDetails WHERE rejectionInMasterId = {0}", id);
            response.details = db.GetDataSet(getQuery);

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpPost]
        public string savePendingRejectionIn(CreateRejectionInVM input)
        {
            var result = "";

            MATFinancials.DAL.DBMatConnection db = new MATFinancials.DAL.DBMatConnection();
            string masterQuery = string.Format("INSERT INTO tbl_RejectionInMaster_Pending " +
                                                    "VALUES('{0}',{1},{2},{3},'{4}',{5},{6},'{7}','{8}',{9},'{10}')"
                                                    , input.Date,
                                                    input.CustomerId,
                                                    input.PricingLevelId,
                                                    input.SalesManId,
                                                    input.Narration,
                                                    input.TotalAmount,
                                                    input.UserId,
                                                    input.LrNo,
                                                    input.TransportationCompany,
                                                    input.DeliveryNoteMasterId,
                                                    "Pending");
            if (db.ExecuteNonQuery2(masterQuery))
            {
                string id = db.getSingleValue("SELECT top 1 rejectionInMasterId from tbl_RejectionInMaster_Pending order by rejectionInMasterId desc");
                foreach (var row in input.LineItems)
                {
                    //var amount = row.Rate * row.Qty;
                    string detailsQuery1 = string.Format("INSERT INTO tbl_RejectionInDetails_Pending " +
                                                "VALUES({0},{1},{2},{3},{4},{5},{6},'{7}',{8},'{9}',{10},{11},'{12}','{13}','{14}',{15},{16})"
                                                , id,
                                                row.DeliveryNoteDetailsId,
                                                row.ProductId,
                                                row.QtyToReturn,
                                                row.Rate,
                                                row.UnitId,
                                                row.UnitConversionId,
                                                row.BatchId,
                                                row.StoreId,
                                                row.RackId,
                                                row.Amount,
                                                row.SL,
                                                DateTime.Now,
                                                "",
                                                "",
                                                1,
                                                1);
                    if (db.ExecuteNonQuery2(detailsQuery1))
                    {
                        result = "success";
                    }
                    else
                    {
                        result = "failed";
                    }
                }
            }

            return result;
        }


        [HttpPost]
        public string saveRejectionIn(CreateRejectionInVM input)
        {
            try
            {
                VoucherNoGeneration();

                decimal decIdentity = 0;
                DeliveryNoteMasterSP SpDeliveryNoteMaster = new DeliveryNoteMasterSP();
                DeliveryNoteMasterInfo InfoDeliveryNoteMaster = new DeliveryNoteMasterInfo();
                InfoDeliveryNoteMaster = SpDeliveryNoteMaster.DeliveryNoteMasterView(input.DeliveryNoteMasterId);
                DeliveryNoteDetailsInfo infoDeliveryNoteDetails = new DeliveryNoteDetailsInfo();
                var details = new DeliveryNoteDetailsSP().DeliveryNoteDetailsViewByDeliveryNoteMasterId(input.DeliveryNoteMasterId);
                RejectionInMasterInfo InfoRejectionInMaster = new RejectionInMasterInfo();
                RejectionInDetailsInfo InfoRejectionInDetails = new RejectionInDetailsInfo();
                RejectionInDetailsSP SpRejectionInDetails = new RejectionInDetailsSP();
                StockPostingInfo InfoStockPosting = new StockPostingInfo();
                var spStockPosting = new MatApi.Services.StockPostingSP();
                var totalAmount = 0.0m;

                InfoRejectionInMaster.VoucherNo = input.VoucherNo;
                InfoRejectionInMaster.InvoiceNo = strVoucherNo;
                InfoRejectionInMaster.VoucherTypeId = decRejectionInVoucherTypeId;
                InfoRejectionInMaster.SuffixPrefixId = decRejectionInSuffixPrefixId;
                InfoRejectionInMaster.Date = input.Date;
                InfoRejectionInMaster.LedgerId = input.CustomerId;
                InfoRejectionInMaster.PricinglevelId = input.PricingLevelId;
                InfoRejectionInMaster.EmployeeId = input.UserId;
                InfoRejectionInMaster.Narration = input.Narration;
                InfoRejectionInMaster.ExchangeRateId = 1;

                foreach (var i in input.LineItems)
                {
                    i.Quantity = i.QtyToReturn;
                    totalAmount = (i.Quantity * i.Rate);
                }

                InfoRejectionInMaster.TotalAmount = input.TotalAmount;
                InfoRejectionInMaster.UserId = input.UserId;
                InfoRejectionInMaster.LrNo = input.LrNo;
                InfoRejectionInMaster.TransportationCompany = input.TransportationCompany;
                InfoRejectionInMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                InfoRejectionInMaster.Extra1 = string.Empty;
                InfoRejectionInMaster.Extra2 = string.Empty;
                InfoRejectionInMaster.DeliveryNoteMasterId = input.DeliveryNoteMasterId;
                decIdentity = spRejectionInMaster.RejectionInMasterAdd(InfoRejectionInMaster);
                InfoRejectionInDetails.RejectionInMasterId = decIdentity;
                foreach (var lineItem in input.LineItems)
                {
                    InfoRejectionInDetails.DeliveryNoteDetailsId = lineItem.DeliveryNoteDetailsId;
                    InfoRejectionInDetails.ProductId = lineItem.ProductId;
                    InfoRejectionInDetails.Qty = lineItem.Quantity;
                    InfoRejectionInDetails.Rate = lineItem.Rate;
                    InfoRejectionInDetails.UnitId = lineItem.UnitId;
                    InfoRejectionInDetails.UnitConversionId = lineItem.UnitConversionId;
                    InfoRejectionInDetails.BatchId = lineItem.BatchId;
                    InfoRejectionInDetails.GodownId = lineItem.StoreId;
                    InfoRejectionInDetails.RackId = lineItem.RackId;
                    InfoRejectionInDetails.Amount = lineItem.Amount;
                    InfoRejectionInDetails.SlNo = Convert.ToInt32(lineItem.SL);
                    InfoRejectionInDetails.Extra1 = string.Empty;
                    InfoRejectionInDetails.Extra2 = string.Empty;
                    InfoRejectionInDetails.ProjectId = 1;
                    InfoRejectionInDetails.CategoryId = 1;
                    SpRejectionInDetails.RejectionInDetailsAdd(InfoRejectionInDetails);

                    InfoStockPosting.Date = input.Date;
                    InfoStockPosting.VoucherTypeId = InfoDeliveryNoteMaster.VoucherTypeId;
                    InfoStockPosting.VoucherNo = InfoDeliveryNoteMaster.VoucherNo;
                    InfoStockPosting.InvoiceNo = InfoDeliveryNoteMaster.InvoiceNo;
                    InfoStockPosting.ProductId = lineItem.ProductId;
                    InfoStockPosting.BatchId = lineItem.BatchId;
                    InfoStockPosting.UnitId = lineItem.UnitId;
                    InfoStockPosting.GodownId = lineItem.StoreId;
                    InfoStockPosting.RackId = lineItem.RackId;
                    InfoStockPosting.ProjectId = 0;
                    InfoStockPosting.CategoryId = 0;
                    InfoStockPosting.AgainstVoucherTypeId = decRejectionInVoucherTypeId;
                    InfoStockPosting.AgainstInvoiceNo = strVoucherNo;
                    InfoStockPosting.AgainstVoucherNo = strVoucherNo;
                    InfoStockPosting.InwardQty = lineItem.Quantity;
                    InfoStockPosting.OutwardQty = 0;
                    InfoStockPosting.Rate = lineItem.Rate;
                    InfoStockPosting.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                    InfoStockPosting.Extra1 = string.Empty;
                    InfoStockPosting.Extra2 = string.Empty;
                    var spStockPosting1 = new MatApi.Services.StockPostingSP();
                    spStockPosting1.StockPostingAdd(InfoStockPosting);


                    var product = context.tbl_Product.Find(lineItem.ProductId);
                    if (product != null)
                    {
                        // Rejection in GoodinTransit Ledger Posting 
                        var ledgerExpense = context.tbl_LedgerPosting
                            .FirstOrDefault(a => a.ledgerId == product.expenseAccount);
                        LedgerPosting(new LedgerPostingInfo()
                        {
                            InvoiceNo = InfoRejectionInMaster.InvoiceNo,
                            Credit = Convert.ToDecimal(product.purchaseRate * lineItem.Quantity),
                            Date = InfoRejectionInMaster.Date,
                            ExtraDate = DateTime.UtcNow,
                            LedgerId = LedgerConstants.GoodsInTransit,
                            VoucherTypeId = InfoRejectionInMaster.VoucherTypeId,
                            Debit = 0,
                            Extra2 = "",
                            Extra1 = "",
                            VoucherNo = InfoRejectionInMaster.InvoiceNo,
                            ChequeDate = DateTime.UtcNow,
                            ChequeNo = "",
                            DetailsId = decIdentity,
                            YearId = PublicVariables.CurrentFinicialYearId

                        });
                    }
                }
                DBMatConnection conn = new DBMatConnection();
                string queryStr = string.Format("UPDATE tbl_RejectionInMaster_Pending " +
                                                "SET status='{0}'" +
                                                "WHERE deliveryNoteMasterId={1} ", "Approved", input.DeliveryNoteMasterId);
                if (conn.customUpdateQuery(queryStr) > 0)
                {
                    return "Rejection Successfully Saved";
                }
                return "Rejection not Successfully Saved";
            }
            catch (Exception ex)
            {

            }
            return "Rejection Successfully Saved"; ;
        }

        [HttpGet]
        public string GetAutoFormNo1()
        {
            var VoucherNo = VoucherNoGeneration1();
           
            return VoucherNo;
        }

        
        [HttpGet]
        public string GetAutoFormNo()
        {
            return VoucherNoGeneration();
        }

        public bool LedgerPosting(LedgerPostingInfo infoLedgerPosting)
        {
            LedgerPostingSP spLedgerPosting = new LedgerPostingSP();
            var post =      spLedgerPosting.LedgerPostingAdd(infoLedgerPosting);
            if (post > 0) return true;
          
            return false;
        }

        [HttpPost]
        public IHttpActionResult EditRejectionIn([FromBody] EditRejectionInDto record)
        {
            var auditLogs = new List<TransactionAuditLog>();
            var master = context.tbl_RejectionInMaster.Find(record.RejectionInMaster.rejectionInMasterId);
            if (master == null) return NotFound();

            // edit master    
            //create old audit log value 
            auditLogs.Add(new TransactionAuditLog()
            {
                TransId = master.rejectionInMasterId.ToString(),
                DateTime = DateTime.UtcNow,
                Amount = master.totalAmount,
                VochureTypeId = Constants.Rejection_In_VoucherTypeId,
                VochureName =  TransactionsEnum.RejectionInMaster.ToString(),
                AdditionalInformation = "Edited Rejection In Master",
                UserId = PublicVariables.CurrentUserId,
                Status = TransactionOperationEnums.Edit.ToString()
            });
            //master.ledgerId = record.RejectionInMaster.ledgerId;
            master.totalAmount = record.RejectionInMaster.totalAmount;
            master.narration = record.RejectionInMaster.narration;
            master.transportationCompany = record.RejectionInMaster.transportationCompany;
            context.Entry(master).State = EntityState.Modified;
           

            //edit master deliveryNote 
            var deliveryMaster = context.tbl_DeliveryNoteMaster
                .FirstOrDefault(a => a.deliveryNoteMasterId == master.deliveryNoteMasterId);

            if (deliveryMaster != null)
            {//record old value
                auditLogs.Add(new TransactionAuditLog()
                {
                    TransId = deliveryMaster.deliveryNoteMasterId.ToString(),
                    DateTime = DateTime.UtcNow,
                    Amount = deliveryMaster.totalAmount,
                    VochureTypeId = Constants.Delivery_NoteVoucherTypeId,
                    VochureName = TransactionsEnum.DeliveryNoteMaster.ToString(),
                    AdditionalInformation = "Edited DeliveryNoteMaster Record",
                    UserId = PublicVariables.CurrentUserId,
                    Status = TransactionOperationEnums.Edit.ToString()

                });
                deliveryMaster.totalAmount = deliveryMaster.totalAmount - master.totalAmount;
                context.Entry(deliveryMaster).State = EntityState.Modified;
            }
            var currentDetails =
                context.tbl_RejectionInDetails.Where(a => a.rejectionInMasterId == master.rejectionInMasterId).ToList();
            var detailIds = currentDetails.Select(a => a.rejectionInDetailsId).ToList();
            var updatedIds = new List<decimal>();
            foreach (var i in record.RejectionInDetails)
            {
                //get item to update

                var item = currentDetails.Find(a => a.rejectionInDetailsId == i.rejectionInDetailsId);
                var deliveryDetails =
                    context.tbl_DeliveryNoteDetails.FirstOrDefault(a =>
                        a.deliveryNoteDetailsId == i.deliveryNoteDetailsId);
                if (deliveryDetails != null)
                {
                    //record old value 
                    auditLogs.Add(new TransactionAuditLog()
                    {
                        TransId = deliveryDetails.deliveryNoteDetailsId.ToString(),
                        DateTime = DateTime.UtcNow,
                        Amount = deliveryDetails.amount,
                        Quantity = deliveryDetails.qty,
                        VochureTypeId = Constants.Delivery_NoteVoucherTypeId,
                        VochureName = TransactionsEnum.DeliverNoteDetail.ToString(),
                        AdditionalInformation = "Edited Delivery note detail Record",
                        UserId = PublicVariables.CurrentUserId,
                        Status = TransactionOperationEnums.Edit.ToString()

                    });

                    var qty = deliveryDetails.qty - i.qty;
                    deliveryDetails.qty = qty;
                    deliveryDetails.amount = deliveryDetails.rate * qty;
                    context.Entry(deliveryDetails).State = EntityState.Modified;
                }
                if (item != null)
                {
                    auditLogs.Add(new TransactionAuditLog()
                    {
                        TransId = item.rejectionInDetailsId.ToString(),
                        DateTime = DateTime.UtcNow,
                        Amount = item.amount,
                        Quantity = item.qty,
                        VochureTypeId = Constants.Rejection_In_VoucherTypeId,
                        VochureName = TransactionsEnum.RejectionInDetail.ToString(),
                        AdditionalInformation = "Edited RejectionIn  detail Record",
                        UserId = PublicVariables.CurrentUserId,
                        Status = TransactionOperationEnums.Edit.ToString()

                    });
                    item.qty = i.qty;
                    item.amount = item.rate * i.qty;
                    context.Entry(item).State = EntityState.Modified;
                    updatedIds.Add(item.rejectionInDetailsId);
                }
                if (i.rejectionInDetailsId == 0)
                {
                    i.extraDate = DateTime.UtcNow;
                    context.tbl_RejectionInDetails.Add(i);
                }
            }
            var itemToDeleteIds = detailIds.Except(updatedIds);
            foreach (var i in itemToDeleteIds)
            {
                var item = currentDetails.FirstOrDefault(a => a.rejectionInDetailsId == i);
                if (item != null)
                {
                    context.tbl_RejectionInDetails.Remove(item);
                }
            }

            context.SaveChanges();

            //update stock posting  
            //get stock posting base on the  deliveryNoteVochureType Id, and its Vochure number that can be found in masterRecord
            var stockPosting = context.tbl_StockPosting.Where(a =>
                a.voucherTypeId == Constants.Rejection_In_VoucherTypeId && a.voucherNo == master.voucherNo).ToList();
            // delete stock posting 
            if (stockPosting.Any())
            {
                context.tbl_StockPosting.RemoveRange(stockPosting);
            }
            //get new updated items 
            var updatedItems = context.tbl_RejectionInDetails
                .Where(a => a.rejectionInMasterId == master.rejectionInMasterId)
                .ToList();

            // add new stock posting to update record
            foreach (var i in updatedItems)
            {
                var product = context.tbl_Product.Find(i.productId);
                context.tbl_StockPosting.Add(new DBModel.tbl_StockPosting
                {
                    //set items here 
                    batchId = i.batchId,
                    CategoryId = i.CategoryId,
                    productId = i.productId,
                    ProjectId = i.ProjectId,
                    extra2 = i.extra2,
                    voucherTypeId = Constants.Rejection_In_VoucherTypeId,
                    date = DateTime.UtcNow,
                    extraDate = DateTime.UtcNow,
                    financialYearId = PublicVariables.CurrentFinicialYearId,
                    extra1 = "",
                    rackId = i.rackId,
                    voucherNo = master.voucherNo,
                    invoiceNo = master.invoiceNo,
                    unitId = i.unitId,
                    rate = product.purchaseRate,
                    godownId = i.godownId,
                    outwardQty = 0,
                    inwardQty = i.qty,
                    againstInvoiceNo = "",
                    againstVoucherNo = "",
                    againstVoucherTypeId = 0
                }); 

                context.SaveChanges();
            }

            //get all ledger posting base on the vocheur id from the master, then delete them and create a new one 
            var oldLedgerPosting = context.tbl_LedgerPosting
                .Where(a => a.voucherNo == master.voucherNo && a.voucherTypeId == Constants.Rejection_In_VoucherTypeId)
                .ToList();
            if (oldLedgerPosting.Any())
            {
                context.tbl_LedgerPosting.RemoveRange(oldLedgerPosting);
            }
            //add new ledger base on the updated item and debit the good in transit ledger; 
            var goodInTransitLedger = LedgerConstants.GoodsInTransit;

            foreach (var i in updatedItems)
            {
                var product = context.tbl_Product.Find(i.productId);
                context.tbl_LedgerPosting.Add(new DBModel.tbl_LedgerPosting()
                {
                    voucherTypeId = Constants.Rejection_In_VoucherTypeId,
                    date = DateTime.UtcNow,
                    ledgerId = goodInTransitLedger,
                    extra2 = "",
                    extraDate = DateTime.UtcNow,
                    voucherNo = master.voucherNo,
                    extra1 = "",
                    credit = i.qty * i.rate,
                    debit = 0,
                    invoiceNo = master.invoiceNo,
                    chequeDate = DateTime.UtcNow,
                    yearId = PublicVariables.CurrentFinicialYearId,
                    detailsId = i.rejectionInDetailsId,
                    chequeNo = ""
                });
                context.SaveChanges();
            }
            if (auditLogs.Any())
            {
                new Services.TransactionAuditService().CreateMany(auditLogs);
            }
            return Ok(new { status = 200, message = "Edit Made Successful" });


            return Ok();
        }

        public string VoucherNoGeneration()
        {
            string strRejectionIn = "RejectionInMaster";
            string strSuffix = string.Empty;
            string strPrefix = string.Empty;
            string strInvoiceNo = string.Empty;
            try
            {
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }
                strVoucherNo = transactionGeneralFillObj.VoucherNumberAutomaicGeneration(decRejectionInVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strRejectionIn);

                SuffixPrefixSP spSuffixPrefix = new SuffixPrefixSP();
                SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                infoSuffixPrefix = spSuffixPrefix.GetSuffixPrefixDetails(decRejectionInVoucherTypeId, DateTime.Now);
                strPrefix = infoSuffixPrefix.Prefix;
                strSuffix = infoSuffixPrefix.Suffix;
                strInvoiceNo = strPrefix + strVoucherNo + strSuffix;

              
            }
            catch (Exception ex)
            {
            }
            return strVoucherNo;
        }


        public string VoucherNoGeneration1()
        {
            string strRejectionIn = "RejectionInMaster";
            string strSuffix = string.Empty;
            string strPrefix = string.Empty;
            string strInvoiceNo = string.Empty;
            try
            {
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }
                strVoucherNo = transactionGeneralFillObj.VoucherNumberAutomaicGeneration(decRejectionInVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strRejectionIn);

                SuffixPrefixSP spSuffixPrefix = new SuffixPrefixSP();
                SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                infoSuffixPrefix = spSuffixPrefix.GetSuffixPrefixDetails(decRejectionInVoucherTypeId, DateTime.Now);
                strPrefix = infoSuffixPrefix.Prefix;
                strSuffix = infoSuffixPrefix.Suffix;
                strInvoiceNo = strPrefix + strVoucherNo + strSuffix;

                if (!string.IsNullOrEmpty(strVoucherNo) && !string.IsNullOrWhiteSpace(strVoucherNo))
                {
                    var master = context.tbl_LedgerPosting.Where(u => u.voucherNo == strVoucherNo).Any();

                    var master1 = context.tbl_StockPosting.Where(u => u.voucherNo == strVoucherNo).Any();

                    if (master1 || master)
                    {
                        return VoucherNoGeneration1();
                    }
                }
            }
            catch (Exception ex)
            {
            }
            return strVoucherNo;
        }

        public string ProductVoucherNoGeneration()
        {
            string strRejectionIn = "RejectionInMaster";
            string strSuffix = string.Empty;
            string strPrefix = string.Empty;
            string strInvoiceNo = string.Empty;
            try
            {
                if (strVoucherNo == string.Empty)
                {
                    strVoucherNo = "0";
                }
                strVoucherNo = transactionGeneralFillObj.VoucherNumberAutomaicGeneration(decRejectionInVoucherTypeId, Convert.ToDecimal(strVoucherNo), DateTime.Now, strRejectionIn);

                SuffixPrefixSP spSuffixPrefix = new SuffixPrefixSP();
                SuffixPrefixInfo infoSuffixPrefix = new SuffixPrefixInfo();
                infoSuffixPrefix = spSuffixPrefix.GetSuffixPrefixDetails(decRejectionInVoucherTypeId, DateTime.Now);
                strPrefix = infoSuffixPrefix.Prefix;
                strSuffix = infoSuffixPrefix.Suffix;
                strInvoiceNo = strPrefix + strVoucherNo + strSuffix;

                //var context = new DBModel.DBMATAccounting_MagnetEntities1();
                //var IsExitVoucherSL = context.tbl_LedgerPosting.Where(u => u.voucherNo == strVoucherNo).Any();
                //var IsExitVoucherSP = context.tbl_StockPosting.Where(u => u.voucherNo == strVoucherNo).Any();

                //if (IsExitVoucherSL || IsExitVoucherSP)
                //{
                //    return VoucherNoGeneration();
                //}

                return strVoucherNo;
            }
            catch (Exception ex)
            {
            }
            return strVoucherNo;
        }

        [HttpGet]
        public IHttpActionResult GetRejectionInFromMaster(int rejectionMasterId)
        {
            var rejections = context.tbl_RejectionInMaster.Where(a=>a.rejectionInMasterId== rejectionMasterId).ToList()
                .Join(context.tbl_RejectionInDetails.Where(a=>a.rejectionInMasterId== rejectionMasterId).ToList(), rm => rm.rejectionInMasterId, rd => rd.rejectionInMasterId,
                    (m, d) => new
                    {
                        RejectionMaster = m,
                        RejectionDetail = m,
                    }).Join(context.tbl_AccountLedger.ToList(), o => o.RejectionMaster.ledgerId, l => l.ledgerId,

                    (o, l) => new
                    {
                        RejectionMasterDetail = o,
                        Ledger = l
                    })
                .Join(context.tbl_User.ToList(), o=>o.RejectionMasterDetail.RejectionMaster.userId, u=>u.userId,
                    (o, u)=>new
                    {
                        RejectionMasterDetailLedger = o,
                        User = u
                    })
           
                ;
            var deliveryOrdersProduct = context.tbl_DeliveryNoteMaster.ToList()
                .Join(context.tbl_SalesOrderMaster,  o=>o.orderMasterId , s=>s.salesOrderMasterId,
                    (dm, om)=>new
                    {
                        DeliverMaster = dm,
                        OrderMaster = om
                    }
                    )
                .Join(context.tbl_SalesOrderDetails.ToList(), dom =>dom.DeliverMaster.orderMasterId , od =>od.salesOrderMasterId ,
                    (deliveryOrderMaster, orderDetail) => new
                    {
                        DeliveryOrderMaster = deliveryOrderMaster,
                        OrderDetail = orderDetail
                    }
                    )
                .Join(context.tbl_Product.ToList(), o =>o.OrderDetail.productId , p =>p.productId, 
                    (o,p) =>
                    new
                    {
                        DeliveryOrderMasterDetail = o,
                        Product = p
                    }
                    ).Join(context.tbl_Godown.ToList(), o=>o.DeliveryOrderMasterDetail.OrderDetail.extra1 , 
                    g => g.godownId.ToString(), 
                (o, g) => new
                {
                    DeliveryOrderMasterDetail = o.DeliveryOrderMasterDetail,
                   Product= o.Product,
                    Store = g
                }
                );
            var enumerable = rejections.ToList();
            var @join = enumerable.Join(deliveryOrdersProduct,
                o => o.RejectionMasterDetailLedger.RejectionMasterDetail.RejectionMaster.deliveryNoteMasterId,
                d => d.DeliveryOrderMasterDetail.DeliveryOrderMaster.DeliverMaster.deliveryNoteMasterId,
                (rejection, delivery) => new
                {
                    Rejection = rejection,
                    Delivery = delivery
                })
                //.Join(context.tbl_Godown.ToList(), o=>o.Delivery.DeliveryOrderMasterDetail.OrderDetail.extra1 , 
                //    g =>g.godownId, 
                //    (o, g)=>new
                //    {
                //        Rejection = o.Rejection,
                //        Delivery = o.Delivery,
                //        Store = g
                //    }
                //    )
                .Select( m=> new
            {
                RejectionMaster = m.Rejection.RejectionMasterDetailLedger.RejectionMasterDetail.RejectionMaster,
                RejectionDetail =  m.Rejection.RejectionMasterDetailLedger.RejectionMasterDetail.RejectionDetail,
                Ledger = m.Rejection.RejectionMasterDetailLedger.Ledger, 
                User = m.Rejection.User, 
                DeliveryMaster = m.Delivery.DeliveryOrderMasterDetail.DeliveryOrderMaster.DeliverMaster,
                OrderMaster = m.Delivery.DeliveryOrderMasterDetail.DeliveryOrderMaster.OrderMaster,
                OrderDetail = m.Delivery.DeliveryOrderMasterDetail.OrderDetail,
                Product = m.Delivery.Product,
                Store = m.Delivery.Store
            });

            return Json(@join);
        }

        [HttpDelete]
        public IHttpActionResult Delete(int id)
        {
            var rejectionItems = context.tbl_RejectionInMaster.Find(id); 

            if (rejectionItems != null)
            {
                //check if sales has rejection items deliveryNote id
                var sales = context.tbl_SalesMaster
                    .Where(a => a.deliveryNoteMasterId == rejectionItems.deliveryNoteMasterId)
                    .ToList();
                if (sales.Any())
                    return Ok(new
                    {
                        status = 200, message = "Cannot Delete Item now, Since record now since record has an invoice"
                    });

                //delete ledger transaction 
                var ledgers = context.tbl_LedgerPosting
                    .Where(a => a.voucherNo == rejectionItems.voucherNo &&
                                a.voucherTypeId == Constants.Rejection_In_VoucherTypeId)
                    .ToList();
                if (ledgers.Any())
                {
                    context.tbl_LedgerPosting.RemoveRange(ledgers);
                }
                //delete stock posting
                var stock = context.tbl_StockPosting
                    .Where(a => a.voucherNo == rejectionItems.voucherNo &&
                                              a.voucherTypeId == Constants.Rejection_In_VoucherTypeId)
                    .ToList();
                if (stock.Any())
                {
                    context.tbl_StockPosting.RemoveRange(stock);
                }
               
                var deliveryMaster = rejectionItems.tbl_DeliveryNoteMaster;
                var rejectedItems = rejectionItems.tbl_RejectionInDetails;
                // update deliveryMasterItem  
                deliveryMaster.totalAmount = deliveryMaster.totalAmount + rejectionItems.totalAmount;
                context.Entry(deliveryMaster).State = EntityState.Modified;

                //update deliveryDetail Items 
                foreach (var i in rejectedItems)
                {
                    var deliverItem = deliveryMaster.tbl_DeliveryNoteDetails
                        .FirstOrDefault(a => a.deliveryNoteDetailsId == i.deliveryNoteDetailsId);
                    deliverItem.qty = deliverItem.qty + i.qty;
                    deliverItem.amount = deliverItem.rate * i.qty;
                    context.Entry(deliverItem).State = EntityState.Modified;
                }
                


                //get record before delete and add to  
                List<TransactionAuditLog> itemBefore = rejectionItems.tbl_RejectionInDetails.Select(a => new TransactionAuditLog()
                {
                    
                    Quantity = a.qty,
                    Amount = a.qty * a.amount,
                    UserId = PublicVariables.CurrentUserId,
                    VochureName = Enums.TransactionsEnum.RejectionInDetail.ToString(),
                    VochureTypeId = Constants.Rejection_In_VoucherTypeId,
                    Status = Enums.TransactionOperationEnums.Delete.ToString(),
                    DateTime = DateTime.UtcNow,
                    AdditionalInformation = "Rejection items deleted",
                    TransId = a.rejectionInDetailsId.ToString(),
                }).ToList();
                itemBefore.Add(new TransactionAuditLog()
                {
                    TransId = rejectionItems.rejectionInMasterId.ToString(),
                    Amount = rejectionItems.totalAmount,
                    UserId = PublicVariables.CurrentUserId,
                    VochureName = Enums.TransactionsEnum.RejectionInMaster.ToString(),
                    VochureTypeId = Constants.Rejection_In_VoucherTypeId,
                    Status = Enums.TransactionOperationEnums.Delete.ToString(),
                    DateTime = DateTime.UtcNow,
                    AdditionalInformation = "Rejection Master Item deleted",
                });

                //remove master and detail (details are automatically removed because of cascade enforce in the reletionship

                context.tbl_RejectionInMaster.Remove(rejectionItems);
                if (context.SaveChanges() > 0)
                {
                    new Services.TransactionAuditService().CreateMany(itemBefore);
                    return Ok(new { status= 200, message = "Record Deleted Successfully"});
                }
                else
                {
                    return Ok(new { status = 200, message = "Something went wrong unable to delete items now" });
                }
            }
            return BadRequest( "Something went wrong");
        }
    }
}