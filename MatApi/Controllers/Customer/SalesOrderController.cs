﻿using MatApi.Models.Customer;
using MATFinancials;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Cors;
using System.Dynamic;
using System.Data;
using MatApi.DBModel;
using MATFinancials.DAL;
using MatApi.Models;
using MatDal;

namespace MatApi.Controllers.Customer
{
    [EnableCors(origins: "*", headers: "*", methods: "*")]
    public class SalesOrderController : ApiController
    {
        string strVoucherNo = "";
        decimal decSalesOrderTypeId = 10030;
        decimal decSalesOrderMasterIdentity = 0;
         
        DBMATAccounting_MagnetEntities1 context = new DBMATAccounting_MagnetEntities1();

        [HttpPost]
        public MatResponse SaveSalesOrder(SalesOrderVM input)
        {
            strVoucherNo = "";
            MatResponse response = new MatResponse();
            
            try
            {


                decimal decSalesOrderSuffixPrefixId = 0;
                SalesOrderDetailsInfo infoSalesOrderDetails = new SalesOrderDetailsInfo();
                SalesOrderMasterInfo infoSalesOrderMaster = new SalesOrderMasterInfo();
                SalesOrderDetailsSP spSalesOrderDetails = new SalesOrderDetailsSP();
                SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
                //string invNumber = spSalesOrderMaster.VoucherNoMax(decSalesOrderTypeId);
                decimal newSalesOrderMasterId = spSalesOrderMaster.SalesOrderMasterGetMaxPlusOne(decSalesOrderTypeId);
                string changedOrderNo = "";

                var masterResult = spSalesOrderMaster.SalesOrderMasterViewLastRow();
                int result;
                string lastRowValue = (Convert.ToInt32(masterResult.InvoiceNo) + 1).ToString();
                if (!int.TryParse(masterResult.InvoiceNo, out result) || (int.Parse(masterResult.InvoiceNo) < 0001))
                {
                    //note, the default value of lastRowValue, ie the first row if no row has been inserted, should be adjusted according to the business rule
                    lastRowValue = "0001";
                }

                //if (spSalesOrderMaster.SalesOrderCheckVoucherNoExistence(decimal.Parse(input.infoSalesOrderMaster.InvoiceNo)) > 0)
                //{
                //    changedOrderNo = " Your Order Number was incremented to " + lastRowValue;
                //    //return false;
                //}
                infoSalesOrderMaster.Cancelled = input.infoSalesOrderMaster.Cancelled;
                infoSalesOrderMaster.Date = Convert.ToDateTime(input.infoSalesOrderMaster.Date);
                infoSalesOrderMaster.DueDate = Convert.ToDateTime(input.infoSalesOrderMaster.DueDate);
                infoSalesOrderMaster.LedgerId = Convert.ToDecimal(input.infoSalesOrderMaster.LedgerId);
                infoSalesOrderMaster.SuffixPrefixId = 0;
                infoSalesOrderMaster.VoucherNo = lastRowValue;//input.infoSalesOrderMaster.InvoiceNo;
                infoSalesOrderMaster.VoucherTypeId = decSalesOrderTypeId;
                infoSalesOrderMaster.InvoiceNo = lastRowValue; //input.infoSalesOrderMaster.InvoiceNo;
                infoSalesOrderMaster.UserId = input.infoSalesOrderMaster.UserId;
                infoSalesOrderMaster.SalesOrderMasterId = input.infoSalesOrderMaster.SalesOrderMasterId;
                infoSalesOrderMaster.EmployeeId = input.infoSalesOrderMaster.EmployeeId;
                infoSalesOrderMaster.FinancialYearId = MATFinancials.PublicVariables._decCurrentFinancialYearId;
                infoSalesOrderMaster.PricinglevelId = input.infoSalesOrderMaster.PricinglevelId;
                infoSalesOrderMaster.Narration = input.infoSalesOrderMaster.Narration;
                infoSalesOrderMaster.QuotationMasterId = input.infoSalesOrderMaster.QuotationMasterId;
                infoSalesOrderMaster.ExchangeRateId = input.infoSalesOrderMaster.ExchangeRateId;
                var totalAmount = 0.0m;
                var totalTax = 0.0m;
                foreach (var i in input.infoSalesOrderDetails)
                {
                    //totalAmount += i.Amount + i.taxAmount; Emeka was here
                    totalAmount += i.Amount;
                    totalTax += i.taxAmount;
                }

                input.infoSalesOrderMaster.TotalAmount = totalAmount;
                infoSalesOrderMaster.TotalAmount = input.infoSalesOrderMaster.TotalAmount;
                infoSalesOrderMaster.taxAmount = totalTax;
                infoSalesOrderMaster.Extra1 = string.Empty;
                infoSalesOrderMaster.Extra2 = string.Empty;
                decSalesOrderMasterIdentity = Convert.ToDecimal(spSalesOrderMaster.SalesOrderMasterAdd(infoSalesOrderMaster));
                if(decSalesOrderMasterIdentity>0)
                {
                    response.ResponseMessage = "Order Saved!" + changedOrderNo;
                    response.ResponseCode = 200;
                }
                else
                {
                    response.ResponseMessage = "Something went wrong!";
                    response.ResponseCode = 400;
                }

                foreach(var detail in input.infoSalesOrderDetails)
                {
                    infoSalesOrderDetails.SalesOrderMasterId = decSalesOrderMasterIdentity;
                    infoSalesOrderDetails.ProductId = detail.ProductId;
                    infoSalesOrderDetails.Qty = detail.Qty;
                    infoSalesOrderDetails.UnitId = detail.UnitId;
                    infoSalesOrderDetails.UnitConversionId = new UnitConvertionSP().UnitconversionIdViewByUnitIdAndProductId(detail.UnitId,detail.ProductId);
                    infoSalesOrderDetails.BatchId = new BatchSP().BatchIdViewByProductId(detail.ProductId);
                    if(detail.QuotationDetailsId==0)
                    {
                        infoSalesOrderDetails.QuotationDetailsId = 0;
                    }                    
                    else
                    {
                        infoSalesOrderDetails.QuotationDetailsId = detail.QuotationDetailsId;
                    }
                    infoSalesOrderDetails.Rate = detail.Rate;
                    infoSalesOrderDetails.Amount = detail.Amount;
                    infoSalesOrderDetails.taxAmount = detail.taxAmount;
                    infoSalesOrderDetails.taxId = detail.taxId;
                    infoSalesOrderDetails.SlNo = detail.SlNo;
                    //infoSalesOrderDetails.UnitConversionId = new UnitConvertionSP().UnitViewAllByProductId(detail.ProductId).UnitconvertionId;
                    infoSalesOrderDetails.Extra1 = detail.Extra1;
                    infoSalesOrderDetails.Extra2 = string.Empty;
                    infoSalesOrderDetails.ProjectId = 0;
                    infoSalesOrderDetails.CategoryId = 0;
                    infoSalesOrderDetails.itemDescription = detail.itemDescription;
                    if (spSalesOrderDetails.SalesOrderDetailsAdd(infoSalesOrderDetails)>0)
                    {
                        response.ResponseMessage = "Order Saved!" + changedOrderNo;
                        response.ResponseCode = 200;
                    }
                    else
                    {
                        response.ResponseMessage = "Something went wrong!";
                        response.ResponseCode = 400;
                    }
                }
                //updateQuotationStatus(input.infoSalesOrderMaster.QuotationMasterId, "Approved");
            }
            catch (Exception ex)
            {
                response.ResponseMessage = "Something went wrong!";
                response.ResponseCode = 400;
            }
            return response;
        }
        
        [HttpGet]
        public bool updateQuotationStatus(decimal quotationMasterId,string status)
        {
            string query = string.Format("UPDATE tbl_salesquotationmaster SET approved='{0}' WHERE quotationmasterid={1}",status,quotationMasterId);
            DBMatConnection db = new DBMatConnection();
            if(db.customUpdateQuery(query)>0)
            {
                return true;
            }
            return false;
        }

        [HttpPost]
        public HttpResponseMessage GetLookups()
        {
            dynamic response = new ExpandoObject();
            TransactionsGeneralFill transactionGeneralFillObj = new TransactionsGeneralFill();

            var customers = transactionGeneralFillObj.CashOrPartyUnderSundryDrComboFill(false);
            var salesMen = transactionGeneralFillObj.SalesmanViewAllForComboFill();
            var applyOn = new SalesOrderMasterSP().VoucherTypesBasedOnTypeOfVouchers("Sales Quotation");
            var pricingLevel = transactionGeneralFillObj.PricingLevelViewAll();
            var currencies = transactionGeneralFillObj.CurrencyComboByDate(DateTime.Now);
            var units = new UnitSP().UnitViewAll();
            var stores = new GodownSP().GodownViewAll();
            var racks = new RackSP().RackViewAll();
            var batches = new BatchSP().BatchViewAll();
            //var tax = new TaxSP().TaxView(2);
            var tax = new TaxSP().TaxViewAll();
            var allTaxes = new TaxSP().TaxViewAll();

            response.Customers = customers;
            response.SalesMen = salesMen;
            response.ApplyOn = applyOn;
            response.PricingLevel = pricingLevel;
            response.Currencies = currencies;
            response.Units = units;
            response.Stores = stores;
            response.Racks = racks;
            response.Batches = batches;
            response.Tax = tax;
            response.AllTaxes = allTaxes;

            return Request.CreateResponse(HttpStatusCode.OK, (object)response);
        }

        [HttpGet]
        public DataTable GetQuotationNumbers(decimal customerId,decimal applyOn,decimal currentUserId)
        {
            DataTable resp = new DataTable();
            resp.Columns.Add("quotationMasterId", typeof(decimal));
            resp.Columns.Add("invoiceNo", typeof(string));

            var quotationNumbers = new SalesQuotationMasterSP().GetSalesQuotationNumberCorrespondingToLedgerForSO(customerId, applyOn, 0);
            foreach(DataRow dt in quotationNumbers.Rows)
            {
                var quotationDetails = new SalesQuotationMasterSP().SalesQuotationMasterView(Convert.ToDecimal(dt.ItemArray[0]));
                var quotationUser = new UserSP().UserView(quotationDetails.userId);
                var currentUser = new UserSP().UserView(currentUserId);
                //resp.Rows.Add(new Object[] {
                //        dt.ItemArray[0],
                //        dt.ItemArray[1]
                //    });
                string query = "SELECT RoleId FROM tbl_CycleActionPriviledge WHERE CycleAction='General Manager'";
                var roleId = new DBMatConnection().getSingleValue(query);
                if(roleId!=null)
                {
                    if (quotationUser.StoreId == currentUser.StoreId && Convert.ToDecimal(roleId) ==currentUser.RoleId)
                    {
                        resp.Rows.Add(new Object[] {
                            dt.ItemArray[0],
                            dt.ItemArray[1]
                        });
                    }
                }                
            }
            return resp;
        }

        [HttpGet]
        public HttpResponseMessage GetOrderDetails(decimal quotationNumber, decimal salesOrderMasterId)
        {
            DataTable dtblMaster = new SalesQuotationMasterSP().QuotationMasterViewByQuotationMasterId(quotationNumber);
            DataTable dtblDetails = new SalesQuotationDetailsSP().SalesQuotationDetailsViewByquotationMasterIdWithRemainingBySO(quotationNumber, salesOrderMasterId);

            dynamic response = new ExpandoObject();
            response.Master = dtblMaster;
            response.Details = dtblDetails;
            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }

        [HttpGet]
        public HttpResponseMessage GetSalesOrderMasterDetails(int id)
        {
            SalesOrderMasterSP sqSalesOrderMaster = new SalesOrderMasterSP();
            var masterResult = sqSalesOrderMaster.SalesOrderMasterView(id);

            return Request.CreateResponse(HttpStatusCode.OK, (object)masterResult);
        }
        [HttpPost]
        public HttpResponseMessage GetSalesOrderMasterLastRowDetails()
        {
            SalesOrderMasterSP sqSalesOrderMaster = new SalesOrderMasterSP();
            var masterResult = sqSalesOrderMaster.SalesOrderMasterViewLastRow();
            int result;
           // string lastRowValue = (int.Parse(masterResult.InvoiceNo) + 1).ToString(); 

           string lastRowValue = (Convert.ToInt32(masterResult.InvoiceNo) + 1).ToString();
            if (!int.TryParse(masterResult.InvoiceNo, out result) || (int.Parse(masterResult.InvoiceNo) < 0001))
            {
                //note, the default value of lastRowValue, ie the first row if no row has been inserted, should be adjusted according to the business rule
                lastRowValue = "001";
            }
           
            return Request.CreateResponse(HttpStatusCode.OK, (object)lastRowValue);
        }

        [HttpGet]
        public HttpResponseMessage GetSalesOrderDetails(int id)
        {
            SalesOrderDetailsSP sqSalesOrderDetails = new SalesOrderDetailsSP();
            var Result = sqSalesOrderDetails.SalesOrderDetailsViewByMasterId(id);
            
            return Request.CreateResponse(HttpStatusCode.OK, (object)Result);
        }

        [HttpPost]
        public string EditSalesOrderdetails(SalesOrderVM obj)
        {
            SalesOrderMasterInfo newObj = new SalesOrderMasterInfo();
            List<SalesOrderDetailsInfo> newObj2 = new List<SalesOrderDetailsInfo>();
            newObj = obj.infoSalesOrderMaster;
            newObj2 = obj.infoSalesOrderDetails;

            SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
            SalesOrderDetailsSP spSalesOrderDetails = new SalesOrderDetailsSP();

            spSalesOrderMaster.SalesOrderMasterEdit(newObj);
            var dbOrderDetails = spSalesOrderDetails.SalesOrderDetailsViewByMasterId(newObj.SalesOrderMasterId);
            for (int i = 0; i < dbOrderDetails.Rows.Count; i++)
            {
                var exist = newObj2.Find(p => p.SalesOrderDetailsId == Convert.ToDecimal(dbOrderDetails.Rows[i].ItemArray[0]));

                if (exist == null)
                {
                    spSalesOrderDetails.SalesOrderDetailsDelete(Convert.ToDecimal(dbOrderDetails.Rows[i].ItemArray[0]));
                }
            }

            foreach (var o in newObj2)
            {
                o.Extra2 = "";
                o.ExtraDate = DateTime.Now.Date;
                o.UnitConversionId = o.ProductId;
                o.SalesOrderMasterId = newObj.SalesOrderMasterId;

                if (o.SalesOrderDetailsId > 0)
                {
                    spSalesOrderDetails.SalesOrderDetailsEdit(o);
                }
                else if (o.SalesOrderDetailsId == 0)
                {
                    spSalesOrderDetails.SalesOrderDetailsAdd(o);
                }
            }

            return "Changes Made Successfully";
        }

        [HttpGet]
        public string DeleteSalesOrderDetails(int id)
        {
            SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
            SalesOrderDetailsSP spSalesOrderDetails = new SalesOrderDetailsSP();

            spSalesOrderMaster.SalesOrderMasterDelete(id);
            var salesOrder = spSalesOrderDetails.SalesOrderDetailsViewByMasterId(id);

            for (var so = 0; so < salesOrder.Rows.Count; so++)
            {
                var orderToDelete = new SalesOrderDetailsSP().SalesOrderDetailsDelete
                    (Convert.ToDecimal(salesOrder.Rows[so].ItemArray[1]));

                if (orderToDelete == true)
                {
                    return "Order Deleted Successfully.";
                }
            }
            return "";
        }
        [HttpGet]
        public IHttpActionResult MakeDeleteSalesOrderDetails(int id)
        {
            SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
            SalesOrderDetailsSP spSalesOrderDetails = new SalesOrderDetailsSP();

            spSalesOrderMaster.SalesOrderMasterDelete(id);
            var salesOrder = spSalesOrderDetails.SalesOrderDetailsViewByMasterId(id);
            // delete details  
            //fetch details 
            var orderDetails = context.tbl_SalesOrderDetails.Where(a => a.salesOrderMasterId == id).ToList();
            if (orderDetails.Any())
            {
                context.tbl_SalesOrderDetails.RemoveRange(orderDetails);
                context.SaveChanges();
                 return Json( new {message = "Delete was successful", status = 200});;
            }
            //for (var so = 0; so < salesOrder.Rows.Count; so++)
            //{
            //    var orderToDelete = new SalesOrderDetailsSP().SalesOrderDetailsDelete
            //        (Convert.ToDecimal(salesOrder.Rows[so].ItemArray[1]));

            //    if (orderToDelete == true)
            //    {
            //        return Json( new {message = "Delete was successful", status = 200});
            //    }
            //}
             return Json( new {message = "Failed - Delete didnt Complete", status = 500});

        }
        [HttpGet]
        public HttpResponseMessage GetConfirmedOrders(decimal salesOrderMasterId)
        {
            SalesOrderMasterSP sqSalesOrderMaster = new SalesOrderMasterSP();
            var masterResult = sqSalesOrderMaster.SalesOrderMasterView(salesOrderMasterId);

            SalesOrderDetailsSP sqSalesOrderDetails = new SalesOrderDetailsSP();
            var detailsResult = sqSalesOrderDetails.SalesOrderDetailsViewByMasterId(salesOrderMasterId);

            SalesOrderMasterInfoCustom master = new SalesOrderMasterInfoCustom {
                CustomerName=new UserSP().UserView(masterResult.LedgerId).FirstName + " "+ new UserSP().UserView(masterResult.LedgerId).LastName,
                LedgerId=masterResult.LedgerId,
                Date=masterResult.Date,
                InvoiceNo=masterResult.InvoiceNo,
                OrderMasterId=masterResult.SalesOrderMasterId,
                TaxAmount=masterResult.taxAmount,
                TotalAmount=masterResult.TotalAmount,
                VoucherNo=masterResult.InvoiceNo,
                VoucherTypeId=masterResult.VoucherTypeId,
                VoucherType=new VoucherTypeSP().VoucherTypeView(masterResult.VoucherTypeId).VoucherTypeName,
                QuotationMasterId=masterResult.QuotationMasterId
            };
            List<SalesOrderDetailsInfoCustom> details = new List<SalesOrderDetailsInfoCustom>();
            foreach(DataRow row in detailsResult.Rows)
            {
                var prod = new ProductSP().ProductView(Convert.ToDecimal(row[2].ToString()));

                details.Add(new SalesOrderDetailsInfoCustom {
                    amount = Convert.ToDecimal(row[14].ToString()),
                    batchId = Convert.ToDecimal(row[15].ToString()),
                    Category = 0,
                    extra1 = new GodownSP().GodownView(Convert.ToDecimal(row[21].ToString())).GodownName,
                    extra2 = "",
                    itemDescription = row[18].ToString(),
                    product = prod.ProductName,
                    productcode = prod.ProductCode,
                    barcode = prod.barcode,
                    productId = Convert.ToDecimal(row[2].ToString()),
                    Project = 0,
                    qty = Convert.ToDecimal(row[7].ToString()),
                    quotationDetailsId = Convert.ToDecimal(row[17].ToString()),
                    rate = Convert.ToDecimal(row[8].ToString()),
                    slno = 0,
                    tax = "",
                    unitId = Convert.ToDecimal(row[11].ToString()),
                    unitConversionId = 1,
                    unit = new UnitSP().UnitView(Convert.ToDecimal(row[11].ToString())).UnitName,
                    taxId = Convert.ToDecimal(row[20].ToString()),
                    taxAmount = Convert.ToDecimal(row[19].ToString())
                });
            }
            

            dynamic response = new ExpandoObject();
            response.Master = master;
            response.Details = details;
            return Request.CreateResponse(HttpStatusCode.OK,(object)response);
        }

        [HttpGet]
        public MatResponse GetOderDetails(decimal orderMasterId)
        {
            MatResponse response = new MatResponse();
            try
            {
                var masterQuery = string.Format("select * from tbl_SalesOrderMaster where salesordermasterid={0}",orderMasterId);
                DataTable master = new DBMatConnection().customSelect(masterQuery);

                var detailsQuery = string.Format("select * from tbl_SalesOrderDetails where salesordermasterid={0}", orderMasterId);
                DataTable details = new DBMatConnection().customSelect(detailsQuery);

                dynamic obj = new ExpandoObject();
                obj.master = master;
                obj.details = details;

                response.ResponseCode = 200;
                response.Response = obj;
            }
            catch (Exception ex)
            {
                response.ResponseCode = 500;
                response.ResponseMessage = ex.Message.ToString();
            }
            return response;
        }

        [HttpGet]
        public IHttpActionResult GetSaleOrderFromMaster(int salesOrderMasterId)
        {
                try
            {
               
        var salesOrdersProduct = context.tbl_SalesOrderMaster.Where(a=>a.salesOrderMasterId == salesOrderMasterId).ToList()
            
                .Join(context.tbl_SalesOrderDetails.ToList(), o =>o.salesOrderMasterId , od =>od.salesOrderMasterId ,
                    (saleOrderMaster, orderDetail) => new
                    {
                        SaleOrderMaster = saleOrderMaster,
                        OrderDetail = orderDetail
                    }
                    )
                .Join(context.tbl_Product.ToList(), o =>o.OrderDetail.productId , p =>p.productId, 
                    (o,p) =>
                    new
                    {
                        SaleOrderMasterDetail = o,
                        Product = p
                    }
                    ).Join(context.tbl_Godown.ToList(), o=>o.SaleOrderMasterDetail.OrderDetail.extra1 , 
                    g => g.godownId.ToString(), 
                (o, g) => new
                {
                    SaleOrderMasterDetail = o.SaleOrderMasterDetail,
                   Product= o.Product,
                    Store = g
                }
                ).Join(context.tbl_AccountLedger.ToList(), 
                    o=>o.SaleOrderMasterDetail.SaleOrderMaster.ledgerId , l=>l.ledgerId, 
                    (o,l)=>new
                    {
                        SaleOrderMasterDetail = o.SaleOrderMasterDetail,
                        Product= o.Product,
                        Store = o.Store,
                        Ledger = l
                    }).
                Join(context.tbl_User.ToList(), o=>o.SaleOrderMasterDetail.SaleOrderMaster.userId, u=>u.userId,
                    (o, u) => new
                    {
                        SaleOrderMasterDetail = o.SaleOrderMasterDetail,
                        Product= o.Product,
                        Store = o.Store,
                        Ledger = o.Ledger,
                        User = u
                    }
                    )
                .Select( m=> new
            {

                    Ledger = m.Ledger,
                ////User = m.Rejection.User, 
      
                SaleOrderMaster = m.SaleOrderMasterDetail.SaleOrderMaster,
                OrderDetail = m.SaleOrderMasterDetail.OrderDetail,
                Product = m.Product,
                Store = m.Store,
                User = m.User
            });
        return Ok(salesOrdersProduct);
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
            }
            return null;

        }
    }

    public class SalesOrderMasterInfoCustom
    {
        public DateTime Date { get; set; }
        public decimal EmployeeId { get; set; }
        public decimal ExchangeRateId { get; set; }
        public string InvoiceNo { get; set; }
        public decimal LedgerId { get; set; }
        public string CustomerName { get; set; }
        public decimal QuotationMasterId { get; set; }
        public decimal OrderMasterId { get; set; }
        public decimal TaxAmount { get; set; }
        public decimal VoucherTypeId { get; set; }
        public string VoucherType { get; set; }
        public decimal TotalAmount { get; set; }
        public decimal UserId { get; set; }
        public string User { get; set; }
        public string VoucherNo { get; set; }
    }

    public class SalesOrderDetailsInfoCustom
    {
        public decimal quotationDetailsId { get; set; }
        public decimal quotationMasterId { get; set; }
        public decimal salesOrderMasterId { get; set; }
        public decimal productId { get; set; }
        public string product { get; set; }
        public string productcode { get; set; }
        public string barcode { get; set; }
        public decimal unitId { get; set; }
        public string unit { get; set; }
        public decimal unitConversionId { get; set; }
        public decimal qty { get; set; }
        public decimal rate { get; set; }
        public decimal amount { get; set; }
        public decimal batchId { get; set; }
        public int slno { get; set; }
        public DateTime extraDate { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public int Project { get; set; }
        public int Category { get; set; }
        public string itemDescription { get; set; }
        public decimal taxAmount { get; set; }
        public decimal taxId { get; set; }
        public string tax { get; set; }
    }
}
