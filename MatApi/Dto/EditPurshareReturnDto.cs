﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MatApi.DBModel;

namespace MatApi.Dto
{
    public class EditPurchaseReturnDto
    {
        public  tbl_PurchaseReturnMaster PurchaseReturnMaster { get; set; }

        public  List<tbl_PurchaseReturnDetails> PurchaseReturnDetails { get; set; }
    }
}