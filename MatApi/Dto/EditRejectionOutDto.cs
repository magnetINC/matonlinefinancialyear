﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MatApi.DBModel;

namespace MatApi.Dto
{
    public class EditRejectionOutDto
    {
        public tbl_RejectionOutMaster RejectionOutMaster { get; set; }
        public List<tbl_RejectionOutDetails> RejectionOutDetails { get; set; }
    }
}