﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MatApi.DBModel;

namespace MatApi.Dto
{
    public class EditSalesInvoiceDto
    { 
        public tbl_SalesMaster SalesMaster { get; set; }
        public List<tbl_SalesDetails> SalesDetails { get; set; }
    }
}