﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatApi.Dto
{
    public class ProfitLossDto
    { 
        public string AccountHeadLine { get; set; }

        public List<AccountDetailDto> AccountDetails { get; set; }
    }
    public class BalanceSheetDto 
    {
        public string AccountHeadLine { get; set; }

        public List<AccountBalanceSheetDto> AccountDetails { get; set; }
    }

    public class AccountDetailDto
    {
        public string AccountName { get; set; } 
        public  decimal Value { get; set; } 
        public  string Type { get; set; } 
        public  bool isSubTotal { get; set; }

        public AccountDetailDto()
        {
            isSubTotal = false;
        }
    }

    public class AccountBalanceSheetDto : AccountDetailDto
    {
        public decimal Cost { get; set; }
        public decimal AccDep { get; set; }
        //Net Book Value 
        public decimal Nbv { get; set; }
    }
}