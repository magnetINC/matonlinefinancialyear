﻿using System;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MatApi.DBModel;

namespace MatApi.Dto
{
    public class EditDeliveryNoteDto
    {
        public  tbl_DeliveryNoteMaster DeliveryNoteMaster { get; set; }

        public  List<tbl_DeliveryNoteDetails> DeliveryNoteDetails { get; set; }
    }
}