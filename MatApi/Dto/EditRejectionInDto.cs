﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MatApi.DBModel;

namespace MatApi.Dto
{
    public class EditRejectionInDto
    {
        public tbl_RejectionInMaster RejectionInMaster { get; set; }
        public List<tbl_RejectionInDetails> RejectionInDetails { get; set; }
    }
}