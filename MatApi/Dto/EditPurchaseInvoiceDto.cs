﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MatApi.DBModel;

namespace MatApi.Dto
{
    public class EditPurchaseInvoiceDto
    { 
        public tbl_PurchaseMaster PurchaseMaster { get; set; }
        public List<tbl_PurchaseDetails> PurchaseDetails { get; set; }
    }
}