﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using MatApi.DBModel;

namespace MatApi.Dto
{
    public class EditSalesReturnDto
    { 
        public tbl_SalesReturnMaster SalesReturnMaster { get; set; }
        public List<tbl_SalesReturnDetails> SalesReturnDetails { get; set; }
    }
}