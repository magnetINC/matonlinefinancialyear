﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace PdfTest.Controllers
{
    public class PdfController : Controller
    {

        public void DownloadPDF()
        {
            //string HTMLContent = "Hello <b>World</b>";
            string HTMLContent = new PdfCont2().html();
            Response.Clear();
            Response.ContentType = "application/pdf";
            Response.AddHeader("content-disposition", "attachment;filename=" + "PDFfile.pdf");
            Response.Cache.SetCacheability(HttpCacheability.NoCache);
            Response.BinaryWrite(GetPDF(HTMLContent));
            Response.End();
        }

        public byte[] GetPDF(string pHTML)
        {
            byte[] bPDF = null;

            MemoryStream ms = new MemoryStream();
            TextReader txtReader = new StringReader(pHTML);

            // 1: create object of a itextsharp document class  
            Document doc = new Document(PageSize.A4, 25, 25, 25, 25);

            // 2: we create a itextsharp pdfwriter that listens to the document and directs a XML-stream to a file  
            PdfWriter oPdfWriter = PdfWriter.GetInstance(doc, ms);

            // 3: we create a worker parse the document  
            HTMLWorker htmlWorker = new HTMLWorker(doc);

            // 4: we open document and start the worker on the document  
            doc.Open();
            htmlWorker.StartDocument();


            // 5: parse the html into the document  
            htmlWorker.Parse(txtReader);

            // 6: close the document and the worker  
            htmlWorker.EndDocument();
            htmlWorker.Close();
            doc.Close();

            bPDF = ms.ToArray();

            return bPDF;
        }
    }

    public class PdfCont2
    {
        public string html()
        {
            return "<html>"+
                    "<head></head>" +
                    "<body>" +
                        "<table style = \"width:100%\" >"+ 
                           "<tr>"+ 
                            " <th> Firstname </th>"+ 
                            " <th> Lastname </th>"+ 
                             "<th> Age </th>"+
                             "<th> Pix </th>" +
                           "</tr>" + 
                           "<tr>"+ 
                           "  <td> Jill </td>"+ 
                            " <td> Smith </td>"+ 
                             "<td> 50 </td>"+
                             "<td> <img src=\"https://www.w3schools.com/w3css/img_avatar3.png\" /> </td>" +
                           "</tr>" +  
                     "</table>"+
                     "</body>" +
                    "</html>";
        }
    }
}