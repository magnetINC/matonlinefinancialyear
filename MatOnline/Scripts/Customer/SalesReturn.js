﻿//array declarations
var ledgers = [];
var products = [];
var units = [];
var stores = [];
var batches = [];
var users = [];
var voucherTypes = []
var currencies = [];
var details = [];;
var rack = [];
var salesMen = [];
//var detailsToReturn = [];

//object declaraions
var master = {};
var tax = [];

//other variables
var returnNo = "";

var totalBillDiscountAmount = 0;
var totalTaxAmount = 0;
var grandTotal = 0;
var totalAmount = 0;

function autoFill() {
    var ledgerId = $("#ledgerId").val();
    var ledgerName = $("#ledgerName").val();
    var voucherId = $("#voucherId").val();
    var voucherTypeId = $("#voucherTypeId").val();

    var txtId = $("#txtId").val();
    if (txtId === '' || txtId === null || value === undefined) {
        return;
    }
    if (txtId != null || txtId !== '' || txtId !== undefined) {
        $("#customers").html();
        //set customer  
        var html = `<option type="${ledgerId}" selected="selected"> ${ledgerName}</option>`;
        $("#customers").html(html);
        //fill invoice number 
        var fillInvoiceNumber = $.ajax({
            url: API_BASE_URL + "/SalesReturn/GetInvoiceNo?ledgerId=" + ledgerId + "&voucherTypeId=" + voucherTypeId,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                var salesReturnHtml = "";
                salesReturnHtml += '<option value=""></option>';
                $.each(data, function (count, record) {
                    salesReturnHtml += '<option value="' + record.salesMasterId + '">' + record.invoiceNo + '</option>';
                });
                $("#invoiceNo").html(salesReturnHtml);
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            }
        });
        //when complete 
        $.when(fillInvoiceNumber).then(function (data) {
            console.log(data);
            var salesReturnHtml = "";
            salesReturnHtml += '<option value=""></option>';
            $.each(data,
                function (count, record) {
                    salesReturnHtml += '<option value="' + record.salesMasterId + '">' + record.invoiceNo + '</option>';
                });
            $("#invoiceNo").html(salesReturnHtml);

            //set active recorded  
            $("#invoiceNo").val(txtId);

            // set vochure Type 
            $("#applyOn").val(voucherTypeId);

            // load invoice record  
            var loadInvoice = $.ajax({
                url: API_BASE_URL + "/SalesReturn/GetInvoiceDetails?invoiceNo=" + $("#invoiceNo").val(),
                type: "GET",
                contentType: "application/json",
                success: function (data) {
                    console.log(data);
                    master = data.master[0];
                    details = data.details;

                    renderDetails();

                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.Loader.Hide();
                    Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                }
            });
            $.when(loadInvoice).then(function (data) {
                console.log(data);
                master = data.master[0];
                details = data.details;

                renderDetails();

                Utilities.Loader.Hide();
            });
            Utilities.Loader.Hide();
        });
    }
}

$(function () {
    getLookUpData();

    $("#applyOn").change(function () {
        fetchInvoiceNumbers($("#customers").val());
    });

    $("#invoiceNo").change(function () {
        var val = $("#invoiceNo").val();
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/SalesReturn/GetInvoiceDetails?invoiceNo=" + val,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                master = data.master[0];
                details = data.details;

                renderDetails();

                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            }
        });
    });

    //$("#customers").click(function () {
    //    $("#agentsModal").modal("show");
    //});
    $("#applyDiscount").click(function () {
        var productObjectToApplyDiscount = details.find(p => p.productId == $("#productIdToApplyDiscount").val());
        console.log(productObjectToApplyDiscount);
        var type = $("#discountType").val();
        var computedDiscount = 0.0;
        if (type == "") {
            $("#errorMsg").html("Please select discount type!");
            return;
        } else {
            $("#errorMsg").html("");
            if (type == "percentage") {
                computedDiscount =
                    (productObjectToApplyDiscount.grossAmount + (productObjectToApplyDiscount.grossAmount * 0.01)) *
                    (parseFloat($("#discountValue").val()) / 100.0);
            } else if (type == "value") {
                computedDiscount = $("#discountValue").val();
            }
            productObjectToApplyDiscount.discount = parseFloat(computedDiscount);
            productObjectToApplyDiscount.netAmount =
                productObjectToApplyDiscount.grossAmount - productObjectToApplyDiscount.discount;
            renderDetails();
            $("#discountValue").val(0.0);
            $("#discountType")[0].selectedIndex = 0;
            $("#discountModal").modal("toggle");
        }

    });
    autoFill();
});

function fetchInvoiceNumbers(ledgerId) {
    Utilities.Loader.Show();
    val2 = $("#applyOn").val();
    $.ajax({
        url: API_BASE_URL + "/SalesReturn/GetInvoiceNo?ledgerId=" + ledgerId + "&voucherTypeId=" + val2,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var salesReturnHtml = "";
            salesReturnHtml += '<option value=""></option>';
            $.each(data, function (count, record) {
                salesReturnHtml += '<option value="' + record.salesMasterId + '">' + record.invoiceNo + '</option>';
            });
            $("#invoiceNo").html(salesReturnHtml);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
        }
    });
}

function getLookUpData() {
    Utilities.Loader.Show();
    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/SalesReturn/GetLookUps",
        type: "GET",
        contentType: "application/json",
    });
    $.when(lookUpAjax)
        .done(function (dataLookUp) {
            console.log("lookUp", dataLookUp);

            ledgers = dataLookUp.Ledgers;
            units = dataLookUp.Unit;
            users = dataLookUp.Users;
            stores = dataLookUp.Stores;
            products = dataLookUp.Products;
            batches = dataLookUp.Batch;
            tax = dataLookUp.Tax;
            voucherTypes = dataLookUp.VoucherTypes.filter(p => p.voucherTypeName == "Sales Invoice");
            currencies = dataLookUp.Currency;
            rack = dataLookUp.Rack;
            returnNo = dataLookUp.ReturnNo;
            salesMen = dataLookUp.SalesMen;
           /* debugger;*/
            renderDataToControls();
            $("#customers").kendoDropDownList({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                optionLabel: "Please Select...",
                dataSource: {
                    data: ledgers
                },
                filter: "contains"
            });
            Utilities.Loader.Hide();
        });

}
$("#customers").change(function () 
{

    var ledgerId = $(this).val();
    fetchInvoiceNumbers(ledgerId);

    //$("#customers").html('<option value="' + selectedAgent.ledgerId + '">' + selectedAgent.ledgerName + "  :  " + selectedAgent.extra1 + "</option>");
    //$("#agentsModal").modal("hide");
});
function renderDataToControls() {
    $("#returnNo").val(returnNo);

    var voucherTypeHtml = "";
    voucherTypes.splice(1, 1);
    $.each(voucherTypes, function (count, record) {
        voucherTypeHtml += '<option value="' + record.voucherTypeId + '">' + record.voucherTypeName + '</option>';
    });
    $("#applyOn").html(voucherTypeHtml);

    var currencyHtml = "";
    $.each(currencies, function (count, record) {
        currencyHtml += '<option value="' + record.exchangeRateId + '">' + record.currencyName + '</option>';
    });
    $("#currency").html(currencyHtml);

    //Restrict Sales Man
    var salesMan = salesMen.find(p => p.userId == matUserInfo.UserId);
    if (salesMan != undefined) {
        $("#salesMan").html(Utilities.PopulateDropDownFromArray([salesMan], 0, 1));
    } else {
        $("#salesMan").html("<option value=\"0\">--NA--</option>" + Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
    }

    $("#salesMan").chosen({ width: "100%", margin: "1px" });
    //to render customer with code datatable and dropdown
    // renderDataToCustomerTable();
}

function renderDataToCustomerTable() {
    var customersWithCode = [];
    for (i = 0; i < ledgers.length; i++) {
        var ext = (ledgers[i].extra1 == "") ? "NA" : ledgers[i].extra1;
        //customersWithCode.push([customers[i].ledgerName + "  ____  " + ext,customers[i].ledgerId]);
        customersWithCode.push([(i + 1), ledgers[i].extra1, ledgers[i].ledgerName]);
    }
    var table = "";

    table = $('#agentsTable').DataTable({
        data: customersWithCode,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

    $('.dataTable').on('click', 'tbody tr', function () {
        console.log('API row values : ', table.row(this).data());
        var rowData = table.row(this).data();
        var selectedAgent = ledgers.find(p => p.extra1 == rowData[1]);
        console.log(selectedAgent);

        fetchInvoiceNumbers(selectedAgent.ledgerId);

        $("#customers").html('<option value="' + selectedAgent.ledgerId + '">' + selectedAgent.ledgerName + "  :  " + selectedAgent.extra1 + "</option>");
        $("#agentsModal").modal("hide");
    })
}

function renderDetails() {
    var output = "";
    totalTaxAmount = 0.0;
    totalBillDiscountAmount = 0.0;
    totalAmount = 0.0;

    $.each(details, function (count, row) {
        if (row.QtyToReturn == 0 || row.QtyToReturn == undefined || row.QtyToReturn == null) {
            row.QtyToReturn = 0;
        }
        output += '<tr>\
                                <td style="white-space: nowrap;"><button onclick="showDiscountModal(' + row.productId + ')" class="btn btn-sm btn-primary"><i class="fa fa-check"></i></button></>\
                                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                                <td style="white-space: nowrap;">'+ findProduct(row.productId).productName + '</td>\
                                <td style="white-space: nowrap;">' + findProduct(row.productId).narration + '</td>\
                                <td style="white-space: nowrap;">' + row.qty + '</td>\
                                <td style="white-space: nowrap;">'+ row.QtyToReturn + '</td>\
                                <td style="white-space: nowrap;">' + findStore(row.godownId).godownName + '</td>\
                                <td style="white-space: nowrap;">' + (findRack(row.rackId).rackName == undefined ? "NA" : findRack(row.rackId).rackName) + '</td>\
                                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.grossAmount) + '</td>\
                                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.discount) + '</td>\
                                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.netAmount) + '</td>\
                                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.taxAmount) + '</td>\
                                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                                <td><button class="btn btn-sm btn-info" onclick="changeQuantity(' + row.productId + ')"><i class="fa fa-edit"></i></button></td>\
                            </tr>\
                            ';
        totalTaxAmount = totalTaxAmount + row.taxAmount;
        totalBillDiscountAmount = totalBillDiscountAmount + row.discount;
        //totalAmount = totalAmount + row.Amount;   //not using this logic again due to customization for wichtech
        totalAmount = totalAmount + parseFloat(row.amount);
    });
    grandTotal = totalAmount - totalBillDiscountAmount;
    $("#totalAmount").val(Utilities.FormatCurrency(totalAmount));
    $("#billDiscount").val(Utilities.FormatCurrency(totalBillDiscountAmount));
    $("#grandTotal").val(Utilities.FormatCurrency(grandTotal));
    $("#transportationCompany").val(master.transportationCompany);
    $("#lrNo").val(master.lrNo);
    $("#narration").val(master.narration);
    $("#salesReturnListTbody").html(output);
}

function showDiscountModal(productId) {
    $("#productIdToApplyDiscount").val(productId);
    $("#discountModal").modal("toggle");
}

function changeQuantity(id) {
    $("#quantityModal").modal("show");
    productToEdit = id;
}
function findTax(taxId) {
    var output = {};
    for (let i = 0; i < tax.length; i++) {
        if (tax[i].taxId == taxId) {
            output = tax[i];
            break;
        }
    }
    return output;
}
function applyQuantityOnObject() {
    console.log(productToEdit);
    for (let i = 0; i < details.length; i++) {
        var currentDetail = details[i];
        var gross = currentDetail.rate * $("#quantity").val();
        currentDetail.grossAmount = gross;
        var discount = currentDetail.discount; //discount percent 

        var netAmount = gross - discount;
        currentDetail.netAmount = netAmount;
        var tax = findTax(currentDetail.taxId);
        if (tax == '' || tax == undefined || tax == null) {
            tax.rate = 0;
        }
        var taxAmount = netAmount * (tax.rate / 100.0);
        currentDetail.taxAmount = parseFloat(taxAmount);
        var amount = parseFloat(netAmount) + parseFloat(taxAmount);
        if (currentDetail.productId == productToEdit) {
            if ($("#quantity").val() <= currentDetail.qty) {
                currentDetail.QtyToReturn = parseInt($("#quantity").val());
                currentDetail.amount = amount;
                currentDetail.netAmount = netAmount;
                currentDetail.taxAmount = taxAmount;
                currentDetail.grossAmount = gross;
                details[i] = currentDetail;
            }
            else {
                Utilities.ErrorNotification("Quantity to Return cannot be Higher than Quantity Ordered");
            }
        }

    }
    $("#quantity").val("");
    $("#quantityModal").modal("hide");
    renderDetails();
}

function savePending() {
    var toSave = {};
    var lineItems = [];

    $.each(details, function (count, row) {
        lineItems.push({
            SL: (count + 1),
            ProductId: row.productId,
            CategoryId: 1,
            Projectid: 1,
            Discount: row.discount,
            GodownId: row.godownId,
            GrossAmount: row.grossAmount,
            NetAmount: row.netAmount,
            SalesDetailsId: row.salesDetailsId,
            Description: "",
            Quantity: row.QtyToReturn,
            UnitId: row.unitId,
            UnitConversionId: row.unitConversionId,
            RackId: row.rackId,
            BatchId: row.batchId,
            Rate: row.rate,
            TaxId: 10012,
            taxAmount: row.taxAmount,
            Amount: row.amount
        });
    });

    toSave.ReturnNo = $("#returnNo").val();
    toSave.Date = $("#returnDate").val();
    toSave.LedgerId = $("#customers").val();
    toSave.Narration = $("#narration").val();
    toSave.TotalAmount = totalAmount;
    toSave.TransportationCompany = $("#transportationCompany").val();
    toSave.LrNo = 1;
    toSave.SalesManId = $("#salesMan").val();//matUserInfo.UserId;
    toSave.CurrencyId = $("#currency").val();
    toSave.SalesMasterId = $("#invoiceNo").val();
    toSave.DiscountAmount = totalBillDiscountAmount;
    toSave.TaxAmount = totalTaxAmount;
    //toSave.NetAmount = $("#netAmount").val();
    toSave.GrandTotal = grandTotal;
    toSave.VoucherNo = $("#returnNo").val();
    toSave.SalesAccount = master.salesAccount;
    toSave.LineItems = lineItems;

    console.log(toSave);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesReturn/SavePending",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data == 1) {
                saveReturn();
                Utilities.Loader.Hide();
                Utilities.SuccessNotification("Sales Return saved");
                window.location = "/Customer/SalesReturn/Index";
            }
            else {
                Utilities.Loader.Hide();
                Utilities.ErrorNotification("could not save Sales Return");
            }
        },
        error: function (err) {
            Utilities.Loader.Hide();
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
        }
    });
}
function saveReturn() {
    var toSave = {};
    var lineItems = [];

    $.each(details, function (count, row) {
        lineItems.push({
            SL: (count + 1),
            ProductId: row.productId,
            CategoryId: 1,
            Projectid: 1,
            Discount: row.discount,
            GodownId: row.godownId,
            GrossAmount: row.grossAmount,
            NetAmount: row.netAmount,
            SalesDetailsId: row.salesDetailsId,
            Description: "",
            Quantity: row.QtyToReturn,
            UnitId: row.unitId,
            UnitConversionId: row.unitConversionId,
            RackId: row.rackId,
            BatchId: row.batchId,
            Rate: row.rate,
            TaxId: 10012,
            taxAmount: row.taxAmount,
            Amount: row.amount
        });
    });

    toSave.ReturnNo = $("#returnNo").val();
    toSave.Date = $("#returnDate").val();
    toSave.LedgerId = $("#customers").val();
    toSave.Narration = $("#narration").val();
    toSave.TotalAmount = totalAmount;
    toSave.TransportationCompany = $("#transportationCompany").val();
    toSave.LrNo = 1;
    toSave.SalesManId = matUserInfo.UserId;
    toSave.CurrencyId = $("#currency").val();
    toSave.SalesMasterId = $("#invoiceNo").val();
    toSave.DiscountAmount = totalBillDiscountAmount;
    toSave.TaxAmount = totalTaxAmount;
    //toSave.NetAmount = $("#netAmount").val();
    toSave.GrandTotal = grandTotal;
    toSave.VoucherNo = $("#returnNo").val();
    toSave.SalesAccount = master.salesAccount;
    toSave.LineItems = lineItems;

    console.log(toSave);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesReturn/Save",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data == 1) {
                Utilities.Loader.Hide();
                Utilities.SuccessNotification("Sales Return saved");
                window.location = "/Customer/SalesReturn/Index";
            }
            else {
                Utilities.Loader.Hide();
                Utilities.ErrorNotification("could not save Sales Return");
            }
        },
        error: function (err) {
            Utilities.Loader.Hide();
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
        }
    });
}


function findCustomer(ledgerId) {
    var output = {};
    for (i = 0; i < ledgers.length; i++) {
        if (ledgers[i].ledgerId == ledgerId) {
            output = ledgers[i];
            break;
        }
    }
    return output;
}
function findUser(userId) {
    var output = {};
    for (i = 0; i < users.length; i++) {
        if (users[i].userId == userId) {
            output = users[i];
            break;
        }
    }
    return output;
}
function findProduct(id) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].productId == id) {
            output = products[i];
            break;
        }
    }
    return output;
}
function findUnit(id) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == id) {
            output = units[i];
            break;
        }
    }
    return output;
}
function findStore(id) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == id) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findRack(id) {
    var output = {};
    for (i = 0; i < rack.length; i++) {
        if (rack[i].rackId == id) {
            output = rack[i];
            break;
        }
    }
    return output;
}