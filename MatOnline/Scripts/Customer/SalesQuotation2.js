﻿var stores = [];

$(function () {
    getAccountGroup();
    function getAccountGroup() {
      //  showLoader();
        $.ajax({
            url: API_BASE_URL + "/SalesQuotation/GetCustomers",
            type: 'GET',
            contentType: "application/json",
            success: function (data) {             
                var str = "";
                $.each(data, function (i, record) {             
                    //str +="<option value = '" + record.AccountGroupId + "'>" + record.AccountGroupName + "</option>";  
                    str +="<option value = '" + record.ledgerId + "'>" + record.ledgerName + "</option>";  
                });
                $("#Customer").html(str);
                
            },
            error: function (request, error) {
               // hideLoader();
            }
        });
    }
    getSalesMan()
    function getSalesMan() {
        //  showLoader();
        $.ajax({
            url: API_BASE_URL + "/Dropdown/SalesManDropdown",
            type: 'GET',
            contentType: "application/json",
            success: function (data) {
                var str = "";
                $.each(data, function (i, record) {
                    str += "<option value = '" + record.employeeId + "'>" + record.employeeName + "</option>";
                });
                $("#SalesMan").html(str);

            },
            error: function (request, error) {
                // hideLoader();
            }
        });
    }
    getPricingLevel()
    function getPricingLevel() {
        //  showLoader();
        $.ajax({
            url: API_BASE_URL + "/Dropdown/PricingLevelDropdown",
            type: 'GET',
            contentType: "application/json",
            success: function (data) {
                var str = "";
                $.each(data, function (i, record) {
                    str += "<option value = '" + record.pricinglevelId + "'>" + record.pricinglevelName + "</option>";
                });
                $("#PricingLevel").html(str);

            },
            error: function (request, error) {
                // hideLoader();
            }
        });
    }
    getCurrency()
    function getCurrency() {
        $.ajax({
            url: API_BASE_URL + "/Dropdown/CurrencyDropdown",
            type: 'GET',
            contentType: "application/json",
            success: function (data) {                
                var str = "";
                $.each(data, function (i, record) {
                    str += "<option value = '" + record.exchangeRateId + "'>" + record.currencyName + "</option>";
                });
                $("#Currency").html(str);
            },
            error: function (request, error) {
            }
        });
    }

    $("#store").change(function () {
        if($("#store").val()!="")
        {
            $("#Qty").removeAttr("disabled");
        }
        else
        {
            $("#Qty").prop("disabled","disabled");
        }
    });
});
var ProductList = [];
function getProductAll() {
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            var str = "<option value = ''></option>";
            $.each(data, function (i, record) {
                str += "<option value = '" + record.ProductCode + "'>" + record.ProductCode + "</option>";
            });
            $("#ProductCode").html(str);
            $("#ProductCode").chosen({ width: "100%", margin: "1px" });

            var str = "<option value = ''></option>";
            $.each(data, function (i, record) {
                str += "<option value = '" + record.ProductName + "'>" + record.ProductName + "</option>";
            });
            $("#ProductName").html(str);
            $("#ProductName").chosen({ width: "100%" });

            var str = "<option value = ''></option>";
            $.each(data, function (i, record) {
                str += "<option value = '" + record.barcode + "'>" + record.barcode + "</option>";
            });
            $("#Barcode").html(str);
            $("#Barcode").chosen({ width: "100%" });
        },
        error: function (request, error) {
        }
    });
}
function searchProduct(searchBy, filter) {
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/SearchProduct",
        type: 'GET',
        data: { searchBy: searchBy, filter: filter },
        contentType: "application/json",
        success: function (data) {
            ProductList = data;
            stores = data.Stores;
            $("#store").html("<option value=\"\">--Select--</option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
            console.log(stores);
            console.log(data);
            fillProductForm(data)
        },
        error: function (request, error) {
        }
    });
}
function getNSetTaxAmount(taxId, amount) {
    $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetTaxAmount",
        type: 'GET',
        data: { taxId: taxId, amount: amount},
        contentType: "application/json",
        success: function (data) {
            $("#TaxAmount").val(data);
        },
        error: function (request, error) {
        }
    });
}
//=================== Line item ===================
var lineItemList = [];
var lineItemSiNo = 1;
var columns = ["SI No", "Barcode", "Product Code", "Product Name", "Description", "Qty", "Unit", "Store", "Batch", "RateAmount", "Amount", "Tax", "Tax Amount"];
lineItemTable(columns, lineItemList);//initialize table

//create a table for the list of line item data
function lineItemTable(cols, data) {
    str = "<table class='table table-striped' id='SalesQuotationLineItmTb' style=\"overflow-x:auto !important;\">";
    if (cols.length > 0) {
        str += "<thead><tr>";
        str += "<th>#</th>";
        str += "<th>Stock Card</th>";
        for (i = 0 ; cols.length > i; i++) {
            str += "<th>" + cols[i] + "</th>";
        }
        str += "</tr></thead>";
    }
    if (data.length > 0) {
        str += "<tbody>";
        for (i = 0 ; i<data.length; i++) {// rows array [row]; 
            var dataArr = $.map(data[i], function (value, index) { // convert object to array
                return [value];
            });
            //console.log(data);
            str += "<tr>";
            str += "<td><button class='btn btn-danger' onclick='lineItemRemove(" + dataArr[0] + ")'><i class='fa fa-times'></i></button></td>";
            str += "<td><button class='btn btn-primary add-tooltip' data-original-title=\"View Stock Card\" onclick='showStockCard(" + data[i].ProductId + ")'><i class='fa fa-info'></i></button></td>";
            for (j = 0 ; dataArr.length > j; j++) {//columns in the row array [row] 

                if (j == 7)
                {
                    str += "<td>" + findStore(dataArr[j]).godownName + "</td>";
                }
                else
                {
                    str += "<td>" + dataArr[j] + "</td>";
                }
            }
            str += "</tr>";
            console.log("table row", str);
        }
        str += "</tbody>";

    }
    str += "</table>";
    console.log("table", str);
    $("#QuotationLineItmTb").html(str);
}
// add a new line item
function lineItemAdd(lineItm) {
    lineItemList.push(lineItm);
    lineItemTable(columns, lineItemList);
}

//add a row
function AddLineItemToList() {
    lineItem = {
        SiNo: lineItemSiNo++, Barcode: $("#Barcode").val(), ProductCode: $("#ProductCode").val(), ProductName: $("#ProductName").val(),
        Description: $("#Description").val(), Qty: $("#Qty").val(), Unit: $("#Unit").val(),
        StoreId: $("#store").val(), Batch: $("#Batch").val(), Rate: $("#Rate").val(),
        Amount: $("#Amount").val(), Tax: $("#Tax").val(), TaxAmount: $("#TaxAmount").val(),
        ProductId: $("#productId").val()
    };
    lineItemAdd(lineItem);
    id = "bill";
    $("#TotalAmount").val(calculateTotal(lineItemList).TotalAmount);
    $("#TotalTax").val(calculateTotal(lineItemList).TotalTax);
    $("#GrandTotal").val(calculateTotal(lineItemList).GrandTotal);
    //clear form
    clearLineItemForm()

}

//remove a line item
function lineItemRemove(rowId) {
    if (rowId > 0) {
        var indexOfObjectToRemove = lineItemList.findIndex(p=>p.SiNo == rowId);
        lineItemList.splice(indexOfObjectToRemove, 1);
        lineItemTable(columns, lineItemList);
        $("#TotalAmount").val(calculateTotal(lineItemList).TotalAmount);
        $("#TotalTax").val(calculateTotal(lineItemList).TotalTax);
        $("#GrandTotal").val(calculateTotal(lineItemList).GrandTotal);
    }  
}

function calculateTotal(data) {
    allTotals = {
        TotalAmount: 0,
        TotalTax: 0,
        GrandTotal: 0,
    }
    var qty = 0;
    var rate = 0;
    var amount = 0;
    var tax = 0;
    if (data.length > 0){
    $.each(data, function (i, rec) {
        
        qty = parseFloat(qty) + parseFloat(rec.Qty);      
        rate = parseFloat(rate) + parseFloat(rec.Rate);     
        amount = parseFloat(amount) + parseFloat(rec.Amount);     
        tax = parseFloat(tax) + parseFloat(rec.TaxAmount);
    })
    allTotals.TotalAmount = amount;
    allTotals.TotalTax = tax;
    allTotals.GrandTotal = amount + tax;
}
    return allTotals
}
// fill product fill or line item base on product code or product name or barcode
function fillProductForm(obj) {
    $("#productId").val(obj.Product[0].productId);
    $("#Barcode").val(obj.Product[0].barcode);
    $("#Barcode").trigger("chosen:updated");
    $("#ProductCode").val(obj.Product[0].productCode);
    $("#ProductCode").trigger("chosen:updated");
    $("#ProductName").val(obj.Product[0].productName);
    $("#ProductName").trigger("chosen:updated");
    $("#Description").val(obj.Product[0].narration);
    $("#Qty").val("");
    $("#Batch").val("");
    $("#Rate").val(obj.Product[0].salesRate);
    setUnit(obj.Unit);
    setBatch(obj.Batch);
    setTax(obj.Taxs);
    //$("#Amount").val("");
    //$("#Tax").val(obj.TaxId);
    //$("#TaxAmount").val("");
    //getUnitByProduct(obj.ProductId);
    //getTaxByQuotationType(28);//not completed
}
function clearLineItemForm() {
    $("#Barcode").val("");
    $("#Barcode").trigger("chosen:updated");
    $("#ProductCode").val("");
    $("#ProductCode").trigger("chosen:updated");
    $("#ProductName").val("");
    $("#ProductName").trigger("chosen:updated");
    $("#Description").val("");
    $("#Qty").val("");
    $("#Unit").val("");
    $("#Batch").val("");
    $("#Rate").val("");
    $("#Amount").val("");
    $("#Tax").val("");
    $("#TaxAmount").val("");
}
function clearForm() {
    lineItemList = [];
    lineItemTable(columns, lineItemList);
    $("#Naration").val("");
    $("#TotalAmount").val("");
    $("#TotalTax").val("");
    $("#GrandTotal").val("");
}
function setUnit(data) {
    var str = "";
    $.each(data, function (i, record) {
        str += "<option value = '" + record.unitId + "'>" + record.unitName + "</option>";
    });
    $("#Unit").html(str);
}
function setBatch(data) {
    var str = "";
    $.each(data, function (i, record) {
        str += "<option value = '" + record.batchId + "'>" + record.batchNo + "</option>";
    });
    $("#Batch").html(str);           
}
function setTax(data) {
    str = "<option value = ''>Select Tax</option>";
    $.each(data, function (i, record) {
        str += "<option value = '" + record.taxId + "'>" + record.taxName + "</option>";
    });
    $("#Tax").html(str);
}

$("#btnLineItemModal").click(function () {
    clearLineItemForm();
    getProductAll()
    $("#LineItemModal").modal("toggle");
});
$("#Barcode").change(function () {
    searchProduct("Barcode", $("#Barcode").val());
    //var obj = $.grep(ProductList, function (v) {
    //    return v.barcode === $("#Barcode").val();
    //});
    //fillProductForm(obj[0])
});
$("#ProductCode").change(function () {
    searchProduct("ProductCode", $("#ProductCode").val());
});
$("#ProductName").change(function () {
    searchProduct("ProductName", $("#ProductName").val());
});
$("#Qty").keyup(function () {
    $("#Tax").val(""); $("#TaxAmount").val("");
    if(($(this).val() > 0) && (parseFloat($("#Rate").val())>0)) {
        $("#Amount").val(parseFloat($(this).val()) * parseFloat($("#Rate").val()))
    } else {
        $("#Amount").val("0")
    }
});
$("#Rate").keyup(function () {
    $("#Tax").val(""); $("#TaxAmount").val("");
    if (($(this).val() > 0) && (parseFloat($("#Qty").val()) > 0)) {
        $("#Amount").val(parseFloat($(this).val()) * parseFloat($("#Qty").val()))
    } else {
        $("#Amount").val("0")
    }
});
$("#Amount").keyup(function () {
    $("#Tax").val(""); $("#TaxAmount").val("");
});
$("#Tax").change(function () {
    if ($(this).val() != "") {
        getNSetTaxAmount($(this).val(), $("#Amount").val());     
    }

});
//============= Save Sales Quote==================
$("#btnSave").click(function () {

    if ($("#QuotationNo").val() == "")
    {
        Utilities.ErrorNotification("Quotation number is required!");
    }
    else if ($("#TransDate").val() == "")
    {
        Utilities.ErrorNotification("Transaction date is required!");
    }
    else if (lineItemList.length<1)
    {
        Utilities.ErrorNotification("Line item contains no record!");
    }
    else
    {
        SalesQuotationObj =
        {
        QuotationNo: $("#QuotationNo").val(),
        Customer: $("#Customer").val(),
        SalesMan: $("#SalesMan").val(),
        TransDate: $("#TransDate").val(),
        PricingLevel: $("#PricingLevel").val(),
        Currency: $("#Currency").val(),
        Naration: $("#Naration").val(),
        TotalAmount: $("#TotalAmount").val(),
        TotalTax: $("#TotalTax").val(),
        GrandTotal: $("#GrandTotal").val(),
        LineItems: lineItemList

    };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/SalesQuotation/SaveSalesQuotation",
            type: 'POST',
            data: JSON.stringify(SalesQuotationObj),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                Utilities.SuccessNotification("Sales Quotation saved!");
                //clearForm();
                hideLoader();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    }
    
});

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function showStockCard(productId)
{
    //alert(productId);
    Utilities.ShowStockCardDialog(productId);
}

