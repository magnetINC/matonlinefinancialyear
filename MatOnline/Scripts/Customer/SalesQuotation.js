﻿var products = [];
var lookups = [];
var units = [];
var stores = [];
var racks = [];
var batches = [];
var tax = {taxId:0,taxName:"NA",rate:0};
var allTaxes = [];
var customers = [];
var salesMen = [];
var currencies = [];
var pricingLevel = [];
var searchResult = {};
var lineItems = [];
var sl = 1;
var salesRate = 0;
var sizes = [];
var voucherNumber = "";

$(function () {
    loadLookups();
    $('#itemLineModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
    $("#searchByProductName").on("change", function () {
        searchProduct($("#searchByProductName").val(), "ProductName");
    });
    $("#searchByProductCode").on("change", function () {
        searchProduct($("#searchByProductCode").val(), "ProductCode");
    });
    $("#searchByBarcode").on("change", function () {
        //searchProduct($("#searchByBarcode").val(), "Barcode");    barcode not working as products use productcode as barcode
        searchProduct($("#searchByBarcode").val(), "ProductCode");
    });

    $("#quantity").change(function () {
        calculateAmountOfItemToAdd();
    });
    $("#rate").change(function () {
        salesRate = $("#rate").val();   //recommended by madam vic to make rate editable as client may decide to change rate at selling point
        calculateAmountOfItemToAdd();
    });
    $("#tax").change(function () {
        var isFound = false;
        for (i = 0; i < allTaxes.length;i++)
        {
            if(allTaxes[i].taxId==$("#tax").val())
            {
                tax = allTaxes[i];
                isFound = true;
                break;
            }
        }
        if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
        {
            tax = { taxId: 0, taxName: "NA", rate: 0 };
        }
//        console.log(tax);
        calculateAmountOfItemToAdd();
    });
    $("#addLineItem").click(function () {
        
        if (searchResult.productId != null)
        {
            var lineQty = $("#quantity").val();
            var lineAmount = lineQty * salesRate;
            var lineTaxAmt = (tax.rate / 100.0) * lineAmount;
            var selectedStoreId = $("#store").val();
            var selectedStoreText = $("#store :selected").text();

            lineItems.push({
                Slno: sl,
                ProductId: searchResult.productId,
                Qty: lineQty,
                UnitId: $("#unit").val(),
                BatchId: $("#batch").val(),
                Rate: salesRate,
                Amount: lineAmount,
                taxAmount: lineAmount*0.01,
                taxId: 10012,
                Extra1: $("#store").val()
            });
            sl = sl + 1;

            clearLineItemForm();
            searchResult = {};
            renderLineItem();

            //reset store to selected store after clearing form
            $("#store").html('<option value="' + selectedStoreId + '">' + selectedStoreText + '</option>');


            if (lineItems.length > 0)   //means item has been added to line item so prevent user from changing store since it won't make sense
            {                           //to pick items from different stores for one quotation
                $("#store").attr("disabled", "disabled");
            }
            else {
                $("#store").removeAttr("disabled");
            }
        }
        else
        {
            Utilities.Alert("Please select item!");
        }
    });

    $("#saveQuotation").click(function () {
        if ($("#quotationNo").val() == "") {
            Utilities.ErrorNotification("Order number is required!");
            return;
        }
        else if ($("#date").val() == "") {
            Utilities.ErrorNotification("Transaction date is required!");
            return;
        }
        else if ($("#customer").val()=="N/A") {
            Utilities.ErrorNotification("Agent is required!");
            return;
        }
        else if (lineItems.length < 1) {
            Utilities.ErrorNotification("Line item contains no record!");
            return;
        }
        else
        {
            var toSave = {
                Master: {
                    Date: $("#date").val(),
                    PricinglevelId: $("#pricingLevel").val(),
                    LedgerId: $("#customer").val(),
                    EmployeeId: $("#salesMan").val(),
                    VoucherNo: $("#quotationNo").val(),
                    InvoiceNo: $("#quotationNo").val(),
                    TotalAmount: getTotalAmount(),
                    Narration: $("#narration").val() == "" ? "" : $("#narration").val(),
                    ExchangeRateId: $("#currency").val(),
                    taxAmount: getTotalTax(),
                    Approved: $("#approved").prop('checked'),
                    userId: matUserInfo.UserId
                },
                LineItems: lineItems
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/SalesQuotation/SaveSalesQuotation",
                type: 'POST',
                data: JSON.stringify(toSave),
                contentType: "application/json",
                success: function (data) {
                    console.log(data);
                    if (data > 0)
                    {
                        clearMasterForm();
                        $("#store").removeAttr("disabled");
                        Utilities.SuccessNotification("Sales Order saved!");
                        window.location = "/Customer/SalesQuotation/Index"; //short fix: page reloads so that
                                                                                        //next order number is generated
                    }
                    else if(data==-1)
                    {
                        Utilities.ErrorNotification("Order number already exist!");
                    }
                    else
                    {
                        Utilities.ErrorNotification("Sales Order could not save!");
                    }
                    //clearForm();
                    hideLoader();
                },
                error: function (request, error) {
                    hideLoader();
                    Utilities.ErrorNotification("Sorry something went wrong!");
                }
            });
        }
    });

    $('#itemLineModal').on('show.bs.modal', function (e) {
        
        
        if (lineItems.length > 0)   //means item has been added to line item so prevent user from changing store since it won't make sense
        {                           //to pick items from different stores for one quotation
            $("#store").attr("disabled", "disabled");
        }
        else
        {
            $("#store").removeAttr("disabled");
        }
    });

    $("#customer").click(function () {
        $("#agentsModal").modal("show");
    });
});

function clearLineItemForm()
{
    //$("#searchByBarcode").val("");
    //$("#searchByBarcode").trigger("chosen:updated");
    //$("#searchByProductCode").val("");
    //$("#searchByProductCode").trigger("chosen:updated");
    //$("#searchByProductName").val("");
    //$("#searchByProductName").trigger("chosen:updated");
    var dropdownlistBarcode = $("#searchByBarcode").data("kendoDropDownList");
    var dropdownlistProductCode = $("#searchByProductCode").data("kendoDropDownList");
    var dropdownlistProductName = $("#searchByProductName").data("kendoDropDownList");

    dropdownlistBarcode.value("Please select");
    dropdownlistProductCode.value("Please select");
    dropdownlistProductName.value("Please select");
    $("#description").val("");
    $("#quantity").val("0");
    $("#rate").val("");
    $("#amount").val("");
    $("#taxAmount").val("");
    $("#quantityInStock").val("");
    $("select#unit")[0].selectedIndex = 0;
    $("select#batch")[0].selectedIndex = 0;
    $("select#store")[0].selectedIndex = 0;
}

function clearMasterForm() {
    lineItems = [];
    renderLineItem();
    $("#quotationNo").val("");
    $("#date").val("");
    $("select#customer")[0].selectedIndex = 0;
    $("select#pricingLevel")[0].selectedIndex = 0;
    $("select#salesMan")[0].selectedIndex = 0;
    //$('#approved').prop("checked", false);
}

function loadLookups()
{
    Utilities.Loader.Show();
    var productsAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: "Get",
        contentType: "application/json"
    });
    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetLookups",
        type: "Get",
        contentType: "application/json"
    });

    var voucherNoAjax = $.ajax({
        url: API_BASE_URL + "/SalesQuotation/voucherNo",
        type: "Get",
        contentType: "application/json"
    });

    $.when(productsAjax, lookUpAjax, voucherNoAjax)
    .done(function (dataProducts, dataLookUps,dataVoucherNo) {
        products = dataProducts[2].responseJSON;
        units = dataLookUps[2].responseJSON.Units;
        stores = dataLookUps[2].responseJSON.Stores;
        racks = dataLookUps[2].responseJSON.Racks;
        batches = dataLookUps[2].responseJSON.Batches;
        allTaxes = dataLookUps[2].responseJSON.Tax;
        customers = dataLookUps[2].responseJSON.Customers;
        salesMen = dataLookUps[2].responseJSON.SalesMen;
        currencies = dataLookUps[2].responseJSON.Currencies;
        pricingLevel = dataLookUps[2].responseJSON.PricingLevels;
        voucherNumber = dataVoucherNo[2].responseJSON;
        var customersWithCode = [];
        for (i = 0; i < customers.length; i++)
        {
            var ext = (customers[i].extra1 == "") ? "NA" : customers[i].extra1;
            //customersWithCode.push([customers[i].ledgerName + "  ____________  " + ext,customers[i].ledgerId]);
            customersWithCode.push([(i + 1), customers[i].extra1, customers[i].ledgerName]);
        }
        var table = "";
        
        table=$('#agentsTable').DataTable({
            data: customersWithCode,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });

        $('.dataTable').on('click', 'tbody tr', function () {
            console.log('API row values : ', table.row(this).data());
            var rowData=table.row(this).data();
            var selectedAgent = customers.find(p=>p.extra1 == rowData[1]);
            console.log(selectedAgent);            
            $("#customer").html('<option value="'+selectedAgent.ledgerId+'">' + selectedAgent.ledgerName + "  :  " + selectedAgent.extra1 + "</option>");
            $("#agentsModal").modal("hide");
        })

        //showing autogen order number
        $("#quotationNo").val(voucherNumber);

        //rendering dropdowns
        $("#unit").html(Utilities.PopulateDropDownFromArray(units, 1, 2));
        $("#batch").html(Utilities.PopulateDropDownFromArray(batches, 0,1));
        $("#store").html(Utilities.PopulateDropDownFromArray(stores, 0, 1));
        //$("#customer").html(Utilities.PopulateDropDownFromArray(customersWithCode, 1, 0));
        $("#customer").html("<option value=\"N/A\">Select Agent</option>");
        $("#currency").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));
        $("#salesMan").html(Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
        $("#pricingLevel").html(Utilities.PopulateDropDownFromArray(pricingLevel, 0, 1));
        console.log(allTaxes);
        var filteredTax = {};
        for (i = 0; i < allTaxes.length; i++)
        {
            if(allTaxes[i].taxName=="Management Fee")
            {
                filteredTax = allTaxes[i];
                break;
            }
        }
        $("#tax").html("<option value=\"" + filteredTax.taxId + "\">" + filteredTax.taxName + "</option>");

        //$("#searchByProductName").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 3, 3));
        //$("#searchByBarcode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 0, 0));
        //$("#searchByProductCode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 2, 2));
        ////$("#searchByProductName").chosen({ width: "100%", margin: "1px" });
        //$("#searchByBarcode").chosen({ width: "100%", margin: "1px" });
        //$("#searchByProductCode").chosen({ width: "100%", margin: "1px" });

        $("#searchByBarcode").kendoDropDownList({
            filter: "contains",
            optionLabel: "Please Select...",
            dataTextField: "ProductCode",
            dataValueField: "ProductCode",
            dataSource: products
        });
        $("#searchByProductCode").kendoDropDownList({
            filter: "contains",
            optionLabel: "Please Select...",
            dataTextField: "ProductCode",
            dataValueField: "ProductCode",
            dataSource: products
        });
        $("#searchByProductName").kendoDropDownList({
            filter: "contains",
            optionLabel: "Please Select...",
            dataTextField: "ProductName",
            dataValueField: "ProductName",
            dataSource: products,
            //change : onChange
        });

        Utilities.Loader.Hide();
    });

}

function searchProduct(filter, searchBy) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/SearchProduct?filter=" + encodeURIComponent(filter) + "&searchBy=" + searchBy,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            //console.log(tax); //return;
            //var productDetails = data.Product[0];
            searchResult = data.Product[0];
            sizes = data.Size;
            salesRate = searchResult.salesRate;
            //console.log(searchResult);
            //console.log(sizes.find(p=>p.SizeId == searchResult.sizeId));
            $("#rate").val(salesRate);
            $("#manufacturer").val(searchResult.extra2);
            $("#size").val(sizes.find(p=>p.sizeId == searchResult.sizeId).size);
            $("#color").val(searchResult.brandName);
            //$("#amount").val((searchResult.purchaseRate) * $("#quantityToAdd").val());
            $("#description").val(searchResult.narration);
            //$("#searchByBarcode").val(searchResult.productCode);
            //$("#searchByBarcode").trigger("chosen:updated");
            //$("#searchByProductCode").val(searchResult.productCode);
            //$("#searchByProductCode").trigger("chosen:updated");
            //$("#searchByProductName").val(searchResult.productName);
            //$("#searchByProductName").trigger("chosen:updated");

            var dropdownlistBarcode = $("#searchByBarcode").data("kendoDropDownList");
            var dropdownlistProductCode = $("#searchByProductCode").data("kendoDropDownList");
            var dropdownlistProductName = $("#searchByProductName").data("kendoDropDownList");

            dropdownlistBarcode.value(searchResult.productCode);
            dropdownlistProductCode.value(searchResult.productCode);
            dropdownlistProductName.value(searchResult.productName);

            if (data.QuantityInStock > 0)
            {
                $("#quantityInStock").css("color", "black");
                $("#quantityInStock").val(Utilities.FormatQuantity(data.QuantityInStock));
            }
            else
            {
                $("#quantityInStock").css("color", "red");
                $("#quantityInStock").val("OUT OF STOCK!");
            }
            Utilities.Loader.Hide();
        },
        error: function (err)
        {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function renderLineItem()
{
    //Utilities.ShowStockCardDialog($("#stockCardProducts").val());
    var output = "";
    $.each(lineItems,function (count,row) {
        var prod = row;
        output += '<tr>\
                    <td style="white-space: nowrap;"><button onclick="removeLineItem(' + prod.ProductId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>\
                    <td style="white-space: nowrap;"><button onclick="Utilities.ShowStockCardDialog(' + prod.ProductId + ')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></button></td>\
                    <td>'+ prod.Slno + '</td>\
                    <td>' + findProduct(prod.ProductId).barcode + '</td>\
                    <td>' + findProduct(prod.ProductId).ProductCode + '</td>\
                    <td>' + findProduct(prod.ProductId).ProductName + '</td>\
                    <td>' + findStore(prod.Extra1).godownName + '</td>\
                    <td>' + prod.Qty + '</td>\
                    <td>' + findUnit(prod.UnitId).unitName + '</td>\
                    <td>' + findBatch(prod.BatchId).batchNo + '</td>\
                    <td>' + Utilities.FormatCurrency(prod.Rate) + '</td>\
                    <td>' + Utilities.FormatCurrency(prod.Amount) + '</td>\
                    <td>' + tax.taxName + '</td>\
                    <td>' + Utilities.FormatCurrency(prod.taxAmount) + '</td>\
                  </tr>';
    });

    $("#quotationLineItemTbody").html(output);
    $("#totalAmount").val(Utilities.FormatCurrency(getTotalAmount()));
    $("#totalTax").val(Utilities.FormatCurrency(getTotalTax()));
    $("#grandTotal").val(Utilities.FormatCurrency(getTotalAmount()+getTotalTax()));
}

function getTotalAmount()
{
    var amt = 0.0;
    for(i=0;i<lineItems.length;i++)
    {
        amt = amt + lineItems[i].Amount;
    }
    return amt;
}

function getTotalTax()
{
    var amt = 0.0;
    for (i = 0; i < lineItems.length; i++) {
        amt = amt + lineItems[i].taxAmount;
    }
    return amt;
}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}

function findProduct(productId) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].ProductId == productId) {
            output = products[i];
            break;
        }
    }
    return output;
}

function findSalesMan(employeeId) {
    var output = {};
    for (i = 0; i < salesMen.length; i++) {
        if (salesMen[i].employeeId == employeeId) {
            output = salesMen[i];
            break;
        }
    }
    return output;
}

function findTax(taxId)
{
    var output = {};
    for (i = 0; i < tax.length; i++) {
        if (tax[i].TaxId == taxId) {
            output = tax[i];
            break;
        }
    }
    return output;
}

function removeLineItem(productId) {
    if (confirm("Remove this item?")) {
        var indexOfObjectToRemove = lineItems.findIndex(p=>p.ProductId == productId);
        lineItems.splice(indexOfObjectToRemove, 1);
        renderLineItem();
    }
}

function calculateAmountOfItemToAdd()
{
    var amount = salesRate * $("#quantity").val();
    //var taxAmount = amount * (tax.rate / 100.0);
    var taxAmount = amount * 0.01;  //0.01 is 1% for management fee
    $("#amount").val(Utilities.FormatCurrency(amount));
    $("#taxAmount").val(Utilities.FormatCurrency(taxAmount));
}