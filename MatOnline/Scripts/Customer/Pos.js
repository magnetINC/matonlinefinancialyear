﻿var pricingLevels = [];
var printers = [];
var products = [];
var racks = [];
var salesPoints = [];
var stores = [];
var tax = [];
var bankLedger = [];
var cash = [];
var customersOnly = [];
var salesInvoiceLineItems = [];
var units = [];
var printData = [];
var selectedTax = {};
var allLedgers = [];
printData.lineItems = [];
var salesMasterIdFromUrl = 0;
var salesRate = 0;
var resetBatch = true;
var productBatches = [];
var salesMen = [];

var lookUpAjax = $.ajax({
    url: API_BASE_URL + "/Pos/GetLookups",
    type: "GET",
    contentType: "application/json",
});

var productLookupAjax = $.ajax({
    url: API_BASE_URL + "/ProductCreation/GetProducts",
    type: "GET",
    contentType: "application/json",
});

$(function () {
    
    Utilities.Loader.Show();
   
    $.when(lookUpAjax, productLookupAjax)
        .done(function (dataLookUp, dataProduct) {
            console.log(dataLookUp);

            allProducts = dataProduct[2].responseJSON;
            pricingLevels = dataLookUp[0].PricingLevel;
            printers = dataLookUp[0].Printers;
            products = dataLookUp[0].Products;
            racks = dataLookUp[0].Racks;
            salesPoints = dataLookUp[0].SalesPoints;
            stores = dataLookUp[0].Stores;
            tax = dataLookUp[0].Taxes;
            receiptNo = dataLookUp[0].ReceiptNo;
            bankLedger = dataLookUp[0].BankLedger;
            cash = dataLookUp[0].Cash;
            units = dataLookUp[0].Units;
            batches = dataLookUp[0].Batches;
            customersOnly = dataLookUp[0].CustomerOnly;
            allLedgers = dataLookUp[0].AllAccountLedger;
            salesMen = dataLookUp[0].SalesMen;
            renderLookupControls();

            if (salesMasterIdFromUrl != 0) {

                viewDetailsClick(salesMasterIdFromUrl);
            }

            $("#tax").html(Utilities.PopulateDropDownFromArray(tax, 0, 1));

            //Restrict Location
            var nStore = stores.find(p => p.godownId == matUserInfo.StoreId);
            if (nStore != undefined && matUserInfo.RoleId != 1) {
                $("#store").html(Utilities.PopulateDropDownFromArray([nStore], 0, 1));
            } else {
                $("#store").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
            }

            $("#unit").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(units, 1, 2));
            $("#batch").html(Utilities.PopulateDropDownFromArray(batches, 0, 1));
            $("#searchByProductName").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allProducts, 3, 3));
            $("#searchByBarcode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allProducts, 0, 0));
            $("#searchByProductCode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allProducts, 2, 2));
            $("#tax").chosen({ width: "100%", margin: "1px" });
            $("#searchByProductName").chosen({ width: "100%", margin: "1px" });
            $("#searchByBarcode").chosen({ width: "100%", margin: "1px" });
            $("#searchByProductCode").chosen({ width: "100%", margin: "1px" });
            $("#store").chosen({ width: "100%", margin: "1px" });
            $("#receiptNo").val(receiptNo);

            Utilities.Loader.Hide();
        });

    $("#searchByProductName").on("change", function () {
        resetBatch = true;
        searchProduct($("#searchByProductName").val(), "ProductName");
        calculateTax();
    });
    $("#searchByProductCode").on("change", function () {
        resetBatch = true;
        searchProduct($("#searchByProductCode").val(), "ProductCode");
        calculateTax();
    });
    $("#searchByBarcode").on("change", function () {
        //searchProduct($("#searchByBarcode").val(), "Barcode");    barcode not working as products use productcode as barcode
        resetBatch = true;
        searchProduct($("#searchByBarcode").val(), "ProductCode");
        calculateTax();
    });

    $("#paymentMode").change(function () {
        var selectedMode = $("#paymentMode").val();
        switch (selectedMode) {
            case "Cash":
                $("#cashDropdown").html("");
                $("#cashDropdown").html(Utilities.PopulateDropDownFromArray(cash, 1, 0));
                $("#bankDropdownParent").attr("hidden", "hidden");
                $("#cardAmt").attr("hidden", "hidden");
                $("#tellerNoDiv").attr("hidden", "hidden");
                $("#amtPaid").removeAttr("hidden");
                break;
            case "Bank":
                $("#cashDropdown").html("");
                $("#cashDropdown").html(Utilities.PopulateDropDownFromArray(bankLedger, 1, 0));
                $("#bankDropdownParent").attr("hidden", "hidden");
                $("#cardAmt").attr("hidden", "hidden");
                $("#tellerNoDiv").removeAttr("hidden");
                $("#amtPaid").removeAttr("hidden");
                break;
            case "Cheque":
                $("#cashDropdown").html("");
                $("#cashDropdown").html(Utilities.PopulateDropDownFromArray(bankLedger, 1, 0));
                $("#bankDropdownParent").attr("hidden", "hidden");
                $("#cardAmt").attr("hidden", "hidden");
                $("#amtPaid").removeAttr("hidden");
                $("#tellerNoDiv").removeAttr("hidden");
                break;
            case "Both":
                $("#cashDropdown").html("");
                $("#cashDropdown").html(Utilities.PopulateDropDownFromArray(cash, 1, 0));
                $("#bankDropdown").html("");
                $("#bankDropdown").html(Utilities.PopulateDropDownFromArray(bankLedger, 1, 0));
                $("#cardAmt").removeAttr("hidden");
                $("#amtPaid").removeAttr("hidden");
                $("#tellerNoDiv").removeAttr("hidden");
                $("#bankDropdownParent").removeAttr("hidden");
                break;
        }
    });
});

$("#batch").change(function () {
    if ($("#searchByProductName").val() == "") {
        return false;
    }
    searchProduct($("#searchByProductName").val(), "ProductName");
    resetBatch = false;
    //resetTaxVal = false;
});

//$(function () {
//    if (salesMasterIdFromUrl != 0) {
//        viewDetailsClick(salesMasterIdFromUrl);
//    }
//})

$("#save").click(saveOrHold.bind(this, "/Pos/SavePos"));

$("#hold").click(saveOrHold.bind(this, "/Pos/HoldPos"));


function saveOrHold(connectionString) {

    if ($("#receiptNo").val() == "" || $("#receiptNo").val() == "0") {
        Utilities.ErrorNotification("Please Enter Receipt No.");
        return;
    }
    if ((($("#amountPaid").val() == "" || $("#amountPaid").val() == "0") && ($("#cardAmount").val() == "" || $("#cardAmount").val() == "0")) && connectionString == "/Pos/SavePos") {
        Utilities.ErrorNotification("Please Enter Amount Paid.");
        return;
    }
    //else if (parseFloat($("#amountPaid").val()) < parseFloat($("#grandTotal").val()) && connectionString == "/Pos/SavePos") {
    //    Utilities.ErrorNotification("Amount paid is less than amount purchased, check and try again.");
    //    return;
    //}
    else if ((parseFloat($("#amountPaid").val()) + parseFloat($("#cardAmount").val())) < parseFloat($("#grandTotal").val()) && connectionString == "/Pos/SavePos") {
        Utilities.ErrorNotification("Amount paid is less than amount purchased, check and try again.");
        return;
    }
    else if ($("#paymentMode").val() == "") {
        Utilities.ErrorNotification("Please Select Payment Mode.");
        return;
    }
    else if ((($("#paymentMode").val() == "Bank") || ($("#paymentMode").val() == "Cheque") || ($("#paymentMode").val() == "Both")) && ($("#tellerNo").val() == "")) {
        Utilities.ErrorNotification("Enter Teller Number.");
        return;
    }
    //else if ($("#invoiceNo").val() == "") {
    //    Utilities.ErrorNotification("Please enter invoice number.");
    //    return;
    //}
    else if (salesInvoiceLineItems.length < 1) {
        Utilities.ErrorNotification("Transaction has no item added.");
        return;
    }
    else {
        var extra1 = "";
        var extra2 = "";
        var amountTendered = $("#amountPaid").val();
        var cardAmount = $("#cardAmount").val();
        if ($("#paymentMode").val() == "Cheque") {
            extra1 = "cheque payment";
        }
        else if ($("#paymentMode").val() == "Bank") {
            extra1 = "bank payment";
        }
        else if ($("#paymentMode").val() == "Cash") {
            extra1 = "cash payment";
        }
        else if ($("#paymentMode").val() == "Both") {
            extra1 = "both cash and bank payment";
        }
        if ($("#paymentMode").val() == "Cash") {
            cardAmount = 0;
        }
        //if ($("#paymentMode").val() == "Bank") {
        //    amountTendered = 0;
        //}
      

        var ledger = findCust($("#cashDropdown").val()).replace(" and ","");
        var ledger1 = findCust($("#bankDropdown").val()).replace(" and ", "");
        var test = ledger;
        var test1 = ledger1;
        var toSave =
        {
            //SalesMode: $("#salesMode").val(),
            SalesMasterInfo: {
                Date: $("#transactionDate").val(),
                VoucherNo: $("#receiptNo").val(),
                InvoiceNo: $("#receiptNo").val(),
                Balance: $("#balance").val(),
                ChangeGiven: $("#balance").val(),
                LedgerId: $("#cashDropdown").val(),//ledgerId,
                PricinglevelId: $("#pricingLevel").val(),
                Extra1: extra1,
                Extra2: ledger,
                AmountTendered: amountTendered,
                cardAmount: cardAmount,
                TaxAmount: $("#totalTaxAmount").val(),
                BillDiscount: $("#billDiscount").val(),
                GrandTotal: $("#grandTotal").val(),
                TotalAmount: $("#totalAmount").val(),
                Narration: $("#narration").val(),
                EmployeeId: $("#salesMan").val(),
                //ExchangeRateId: $("#currency").val(),
                //TaxAmount: totalTaxAmount,
                //BillDiscount: totalBillDiscountAmount,
                //GrandTotal: grandTotal,
                //TotalAmount: totalAmount,
                LrNo: "",
                POSTellerNo: $("#tellerNo").val(),
                CounterId: $("#salesPoint").val(),
                //TransportationCompany: "",
                Discount: $("#percentDiscount").val(),
                UserId: matUserInfo.UserId
            },
            SalesDetailsInfo: salesInvoiceLineItems,
            PendingSalesMasterId: $("#pendingSalesMasterId").val()
        };
        printData = toSave.SalesMasterInfo;
        printData.lineItems = toSave.SalesDetailsInfo;



        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + connectionString,
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(toSave),
            success: function (data) {

                if (data.ResponseCode == 200) {
                    if ($("#print").is(':checked')) {
                        print();

                    }
                    Utilities.SuccessNotification(data.ResponseMessage);

                    //$("#transactionDate").val("");
                    //$("#invoiceNo").val("");
                    clear();

                    //$("#customer")[0].selectedIndex = 0;
                    //$("#pricingLevel").val("");
                    var newReceiptNo = 0;
                    if (connectionString == "/Pos/SavePos") {
                        newReceiptNo = parseInt($("#receiptNo").val()) + 1;
                        $("#receiptNo").val(newReceiptNo);
                    }
                   
                    // $("#pricingLevel")[0].selectedIndex = 0;
                    //$("#salesMan")[0].selectedIndex = 0;
                    salesInvoiceLineItems = [];
                    totalBillDiscountAmount = 0;
                    totalTaxAmount = 0;
                    grandTotal = 0;
                    totalAmount = 0;
                    renderSalesInvoiceLineItems();
                    Utilities.Loader.Hide();
                    console.log(data);
                    window.top.location = window.top.location;
                    if (connectionString == "/Pos/HoldPos") {
                        newReceiptNo = parseInt($("#receiptNo").val()) + 1;
                        $("#receiptNo").val(newReceiptNo);
                        window.stop();
                    }
                    
                }
                else if (data.ResponseCode == 302) {
                    Utilities.ErrorNotification(data.ResponseMessage);
                    Utilities.Loader.Hide();
                }
                else {
                    Utilities.ErrorNotification("Sorry, Someting went wrong");
                    Utilities.Loader.Hide();
                }

            },
            error: function () {
                Utilities.Loader.Hide();
            }
        });
    }

}

function renderLookupControls() {
    $("#pricingLevel").html(Utilities.PopulateDropDownFromArray(pricingLevels, 0, 1));
    $("#salesPoint").html(Utilities.PopulateDropDownFromArray(salesPoints, 1, 2));
    var salesMan = salesMen.find(p => p.userId == matUserInfo.UserId);
    if (salesMan != undefined) {
        $("#salesMan").html(Utilities.PopulateDropDownFromArray([salesMan], 0, 1));
    } else {
        $("#salesMan").html("<option value=\"0\">--NA--</option>" + Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
    }
}

function searchProduct(filter, searchBy) {
    var storeId = $("#store").val() == "" ? 0 : parseInt($("#store").val());
    var batchId = $("#batch").val() == "" ? 0 : parseInt($("#batch").val());
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/SearchProduct?filter=" + filter + "&searchBy=" + searchBy + "&storeId=" + storeId + "&batchId=" + batchId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            searchResult = data.Product[0];
            salesRate = searchResult.salesRate;
            //console.log(data);
            selectedTax = tax.find(p => p.taxId == searchResult.taxId);
            productBatches = batches.filter(p => p.productId == parseInt(searchResult.productId));

            if (resetBatch == true) {
                populateBatch();
            }

            $("#rate").val(salesRate);
            $("#unit").val(searchResult.unitId);
            $("#description").val(searchResult.narration);
            $("#tax").val(searchResult.taxId);
            $("#tax").trigger("chosen:updated");
            $("#searchByBarcode").val(searchResult.productCode);
            $("#searchByBarcode").trigger("chosen:updated");
            $("#searchByProductCode").val(searchResult.productCode);
            $("#searchByProductCode").trigger("chosen:updated");
            $("#searchByProductName").val(searchResult.productName);
            $("#searchByProductName").trigger("chosen:updated");

            if (data.QuantityInStock > 0) {
                $("#quantityInStock").css("color", "black");
                $("#quantityInStock").val(Utilities.FormatQuantity(data.QuantityInStock));
            }
            else if ($("#batch").val() != "") {
                $("#quantityInStock").css("color", "red");
                $("#quantityInStock").val("OUT OF STOCK! " + data.QuantityInStock + "STOCK");
            }

            if (data.StoreQuantityInStock > 0) {
                $("#storeQuantityInStock").css("color", "black");
                $("#storeQuantityInStock").val(data.StoreQuantityInStock);
            }
            else if ($("#batch").val() != "" && $("#store").val() != "") {
                $("#storeQuantityInStock").css("color", "red");
                $("#storeQuantityInStock").val("OUT OF STOCK! " + data.StoreQuantityInStock + " STOCK");
            }

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

var sl = 0;
$("#addLineItem").click(function () {

    sl += 1
    //console.log("searchResult", searchResult);
    if ($("#quantity").val() == 0 || "") {
        Utilities.ErrorNotification("You need to fill the quantity field.");
        return false;
    }

    if ($("#unit").val() == "") {
        Utilities.ErrorNotification("The unit field cannot be empty.");
        return false;
    }

    if ($("#store").val() == "") {
        Utilities.ErrorNotification("You need to add a store.");
        return false;
    }

    console.log(searchResult);    
    var lineTaxAmt = isNaN((parseFloat(tax.rate) / 100.0) * parseFloat(salesRate)) ? 0 : (parseFloat(tax.rate) / 100.0) * parseFloat(salesRate);
    //searchResult.salesRate use the inserted rate
    var netAmount = parseFloat(salesRate) * parseFloat($("#quantity").val());
    var grossAmount = parseFloat(salesRate) * parseFloat($("#quantity").val());
    var amount = parseFloat(salesRate) * parseFloat($("#quantity").val()); //parseFloat(searchResult.salesRate) + lineTaxAmt;
    var salesMode = $("#salesMode").val();
    var taxAmount = parseFloat($("#taxAmount").val().replace(/,/g, ''));
    var taxName = $("#tax option:selected").text();

    salesInvoiceLineItems.push({
        DeliveryNoteDetailsId: salesMode == "Against Delivery Note" ? deliveryNoteDetailsId : 0,
        OrderDetailsId: salesMode == "Against SalesOrder" ? orderDetailsId : 0,
        QuotationDetailsId: salesMode == "Against Quotation" ? quotationDetailsId : 0,
        ProductId: searchResult.productId,
        Qty: $("#quantity").val(),
        Rate: salesRate,
        UnitId: $("#unit").val(),
        //UnitConversionId : 1,
        Discount: 0,
        TaxId: selectedTax.taxId,
        Narration: $("#narration").val(),
        BatchId: $("#batch").val(),
        GodownId: $("#store").val(),
        RackId: 0,
        TaxAmount: taxAmount,
        TaxName: taxName,
        //GrossAmount: grossAmount,
        NetAmount: netAmount,
        Amount: amount,
        SlNo: sl,
        ItemDescription: "",
        ProductBarcode: findProduct(searchResult.productId).barcode,
        ProductCode: findProduct(searchResult.productId).ProductCode,
        ProductName: findProduct(searchResult.productId).ProductName,
        StoreName: findStore($("#store").val()).godownName,
        Unit: findUnit($("#unit").val()).unitName,
        Batch: findBatch($("#batch").val()).batchNo
    });
    console.log(salesInvoiceLineItems);
    renderSalesInvoiceLineItems();

    $("#searchByBarcode").val("");
    $("#searchByBarcode").trigger("chosen:updated");
    $("#searchByProductCode").val("");
    $("#searchByProductCode").trigger("chosen:updated");
    $("#searchByProductName").val("");
    $("#searchByProductName").trigger("chosen:updated");
    $("#store").val("");
    $("#store").trigger("chosen:updated");
    $("#unit").val("");
    $("#unit").trigger("chosen:updated");
    $("#quantity").val(0);
    $("#amount").val("");
    //$("#tax").val("0");
    $("#taxAmount").val("");
    // $("#unit").val("50007");
    $("#quantityInStock").val("");
    $("#rate").val("");
});

function populateBatch() {
    $("#batch").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(productBatches, 0, 1));
    $("#batch").trigger("chosen:updated");
}

$("#clear").click(function () {
    salesInvoiceLineItems = [];
    clear();
    renderSalesInvoiceLineItems();
})


function renderSalesInvoiceLineItems() {
    var output = "";
    var balance = 0.0;
    var amountPaid = parseFloat($("#amountPaid").val());
    var cardAmount = parseFloat($("#cardAmount").val());
    var totalTaxAmount = 0.0;
    var totalAmount = 0.0;
    var totalBillDiscountAmount = parseFloat($("#billDiscount").val());

    // var totalTaxAmount = parseFloat($("#totalTaxAmount").val());
    //var totalAmount = parseFloat($("#totalAmount").val());
    //totalBillDiscountAmount = 0.0;
    // totalAmount = 0.0;
    tax.taxName = tax.taxName == undefined ? "NA" : tax.taxName;
    $.each(salesInvoiceLineItems, function (count, row) {
        output +=
            '<tr>\
                <td style="white-space: nowrap;"><button onclick="removeLineItem('+ row.ProductId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                <td style="white-space: nowrap;">' + findProduct(row.ProductId).barcode + '</td>\
                <td style="white-space: nowrap;">' + findProduct(row.ProductId).ProductCode + '</td>\
                <td style="white-space: nowrap;">' + findProduct(row.ProductId).ProductName + '</td>\
                <td style="white-space: nowrap;">' + findStore(+row.GodownId).godownName + '</td>\
                <td style="white-space: nowrap;">' + row.Qty + '</td>\
                <td style="white-space: nowrap;">' + findUnit(row.UnitId).unitName + '</td>\
                <td style="white-space: nowrap;">' + findBatch(row.BatchId).batchNo + '</td>\
                 <td style="white-space: nowrap;">' + row.Rate + /*'</td>\
                <td style="white-space: nowrap;">' + row.GrossAmount + '</td>\
                <td style="white-space: nowrap;">' + row.Discount + '</td>\
                <td style="white-space: nowrap;">'+ row.NetAmount + */ '</td>\
                <td style="white-space: nowrap;">' + row.TaxName + '</td>\
                <td style="white-space: nowrap;">'+ row.TaxAmount + '</td>\
                <td style="white-space: nowrap;">'+ row.Amount + '</td>\
            </tr>\
            ';
        totalTaxAmount = totalTaxAmount + parseFloat(row.TaxAmount);
        //totalBillDiscountAmount = totalBillDiscountAmount + row.Discount;
        totalAmount = totalAmount + parseFloat(row.Amount);
    });
    var percentDiscountNum = (totalBillDiscountAmount * 100) / totalAmount;
    var subTotal = totalAmount - totalBillDiscountAmount;
    grandTotal = subTotal + totalTaxAmount;//- totalBillDiscountAmount;
    balance = 0 - grandTotal + amountPaid + cardAmount;
    $("#totalAmount").val(totalAmount);
    $("#grandTotal").val(grandTotal);
    $("#posLineItemTbody").html(output);
    $("#balance").val(balance);
    $("#totalTaxAmount").val(totalTaxAmount);
    $("#percentDiscount").val(percentDiscountNum.toFixed(2));
    $("#subTotal").val(subTotal);

}

$("#amountPaid").change(function () {
    //var grandTotalNum = parseFloat($("#grandTotal").val());
    //var amountPaidNum = parseFloat($("#amountPaid").val());
    //var discountNum = parseFloat($("#billDiscount").val());
    //var balanceNum = amountPaidNum - grandTotalNum - discountNum;
    //$("#balance").val(balanceNum);
    resetBalanceAndSubTotal();
});
$("#cardAmount").change(function () {
    //var grandTotalNum = parseFloat($("#grandTotal").val());
    //var amountPaidNum = parseFloat($("#amountPaid").val());
    //var discountNum = parseFloat($("#billDiscount").val());
    //var balanceNum = amountPaidNum - grandTotalNum - discountNum;
    //$("#balance").val(balanceNum);
    resetBalanceAndSubTotal();
});

$("#billDiscount").change(function () {
    var totalAmountNum = parseFloat($("#totalAmount").val());
    var billDiscountNum = $("#billDiscount").val() != "" ? parseFloat($("#billDiscount").val()) : 0;
    var percentDiscountNum = $("#percentDiscount").val() != "" ? parseFloat($("#percentDiscount").val()) : 0;
    // var subTotalNum = totalAmountNum - billDiscountNum ;
    percentDiscountNum = (billDiscountNum * 100) / totalAmountNum;
    $("#percentDiscount").val(percentDiscountNum.toFixed(2));
    // $("#subTotal").val(subTotalNum);
    resetBalanceAndSubTotal();
});

$("#percentDiscount").change(function () {
    var totalAmountNum = parseFloat($("#totalAmount").val());
    var billDiscountNum = $("#billDiscount").val() != "" ? parseFloat($("#billDiscount").val()) : 0;
    var percentDiscountNum = $("#percentDiscount").val() != "" ? parseFloat($("#percentDiscount").val()) : 0;
    billDiscountNum = percentDiscountNum.toFixed(2) / 100 * totalAmountNum; //((amountPaidNum - billDiscountNum) * 100) / amountPaidNum;
    //var subTotalNum = totalAmountNum - billDiscountNum;
    $("#billDiscount").val(billDiscountNum);
    // $("#subTotal").val(subTotalNum);
    resetBalanceAndSubTotal();
});

function resetBalanceAndSubTotal() {
    var grandTotalNum = parseFloat($("#grandTotal").val());
    var amountPaidNum = $("#amountPaid").val() != "" ? parseFloat($("#amountPaid").val()) : 0;
    var cardAmountNum = $("#cardAmount").val() != "" ? parseFloat($("#cardAmount").val()) : 0;
    var billDiscountNum = $("#billDiscount").val() != "" ? parseFloat($("#billDiscount").val()) : 0;
    var totalAmountNum = parseFloat($("#totalAmount").val());
    var balanceNum = 0;//amountPaidNum - grandTotalNum + billDiscountNum; // $("#totalTaxAmount").val(totalTaxAmount);
    var subTotalNum = totalAmountNum - billDiscountNum;
    var totalTaxAmountNum = $("#totalTaxAmount").val() != "" ? parseFloat($("#totalTaxAmount").val()) : 0;
    grandTotalNum = subTotalNum + totalTaxAmountNum;
    balanceNum = amountPaidNum + cardAmountNum - grandTotalNum;
    $("#subTotal").val(subTotalNum);
    $("#grandTotal").val(grandTotalNum);
    $("#balance").val(balanceNum);
}

$("#quantity").change(function () {
    calculateTax();
    calculateAmountOfItemToAdd();
});
$("#rate").change(function () {
    salesRate = $("#rate").val();   //recommended by madam vic to make rate editable as client may decide to change rate at selling point
    calculateTax();
    calculateAmountOfItemToAdd();
});
$("#tax").change(function () {
    calculateTax();
});

function calculateTax() {
    var isFound = false;
    for (i = 0; i < tax.length; i++) {
        if (tax[i].taxId == $("#tax").val()) {
            selectedTax = tax[i];
            isFound = true;
            break;
        }
    }
    if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
    {
        selectedTax = { taxId: 0, taxName: "NA", rate: 0 };
    }
    //        console.log(tax);
    calculateAmountOfItemToAdd();
}

//$("#tax").change(function () {
//    var isFound = false;
//    for (i = 0; i < taxe.length; i++) {
//        if (taxes[i].taxId == $("#tax").val()) {
//            tax = taxes[i];
//            isFound = true;
//            break;
//        }
//    }
//    if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
//    {
//        tax = { taxId: 0, taxName: "NA", rate: 0 };
//    }
//    //        console.log(tax);
//    calculateAmountOfItemToAdd();
//});

function removeLineItem(productId) {
    if (confirm("Remove this item?")) {

        var indexOfObjectToRemove = salesInvoiceLineItems.findIndex(p => p.ProductId == productId);
        salesInvoiceLineItems.splice(indexOfObjectToRemove, 1);
        //console.log(salesInvoiceLineItems);
        renderSalesInvoiceLineItems();
    }
}

function findProduct(productId) {
    var output = {};
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].ProductId == productId) {
            output = allProducts[i];
            break;
        }
    }
    return output;
}

function findCust(ledgerId) {

    var selectedMode = $("#paymentMode").val();
    var output = "";
    switch (selectedMode) {
        case "Cash":
            //for (i = 0; i < cash.length; i++) {
            //    if (cash[i].ledgerId == ledgerId) {
            //        output = cash[i].ledgername;
            //        break;
            //    }
            //}
            output = findCustValue(cash, ledgerId);
            break;
        case "Bank":
            //for (i = 0; i < bankLedger.length; i++) {
            //    if (bankLedger[i].ledgerId == ledgerId) {
            //        output = bankLedger[i].ledgername;
            //        break;
            //    }
            //}
            output = findCustValue(bankLedger, ledgerId);
            break;
        case "Cheque":
            //for (i = 0; i < bankLedger.length; i++) {
            //    if (bankLedger[i].ledgerId == ledgerId) {
            //        output = bankLedger[i].ledgername;
            //        break;
            //    }
            //}
            output = findCustValue(bankLedger, ledgerId);
            break;
        case "Both":
            output = findCustValue(cash, ledgerId) + " and " + findCustValue(bankLedger, ledgerId);
            break;
    }


    return output;
}

function findCustValue(selectedObj, ledgerId) {
    var output = "";
    for (i = 0; i < selectedObj.length; i++) {
        if (selectedObj[i].ledgerId == ledgerId) {
            output = selectedObj[i].ledgername;
            break;
        }
    }
    return output;
}


//function findCust(ledgerId) {
//    var output = {};
//    for (i = 0; i < customersOnly.length; i++) {
//        if (customersOnly[i].ledgerId == ledgerId) {
//            output = customersOnly[i];
//            break;
//        }
//    }
//    return output;
//}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}

function calculateAmountOfItemToAdd() {
    var amount = salesRate * $("#quantity").val();
    if (isNaN(parseFloat(selectedTax.rate))) {
        var taxAmount = 0.0;
    }
    else {
        var taxAmount = amount * (selectedTax.rate / 100.0);
    }
    $("#amount").val(Utilities.FormatCurrency(amount));
    $("#taxAmount").val(Utilities.FormatCurrency(taxAmount));
}

function print() {
    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();

    columns = [
        { title: "SN", dataKey: "SlNo" },
        { title: "Product Name", dataKey: "ProductName" },
        { title: "Qty", dataKey: "Qty" },
        { title: "Unit", dataKey: "Unit" },
        { title: "Rate", dataKey: "Rate" },
        //{ title: "Gross Value", dataKey: "GrossAmount" },
        { title: "Tax Amount", dataKey: "TaxAmount" },
        { title: "Amount", dataKey: "Amount" },
    ];

    //var doc = new jsPDF('portrait');
    var doc = new jsPDF('portrait', 'pt', 'c7');
    doc.setFontSize(6);
    doc.text(companyName, 55, 24);
    doc.setFontSize(4);
    doc.text(companyAddress, 69, 28.5);
    doc.setFontSize(3);
    doc.text('THANKS FOR YOUR PATRONAGE', 82, 200);
    doc.setDrawColor(0);
    doc.setFontSize(2);
    //doc.setFillColor(26, 189, 156);
    //doc.rect(166, 18.6, 1, 1, 'F');
    //doc.rect(166, 23.5, 1, 1, 'F');
    //doc.rect(166, 28, 1, 1, 'F');
    ////doc.rect(86, doc.autoTable.previous.finalY + 25, 103, 1, 'F');
    //doc.text(' ' + 'IT ADVISORY' + '\n\n ' + 'CONSULTING' + '\n\n '
    //    + 'FINANCE', 168, 20);
    doc.autoTable(columns, printData.lineItems, {
        startY: 95, theme: 'grid', styles: {
            fontSize: 3,
        },
    });
    var totalAmountForPrint = Utilities.FormatCurrency(parseFloat(printData.TotalAmount));
    var billDiscountForPrint = Utilities.FormatCurrency(parseFloat(printData.BillDiscount));
    var subTotalForPrint = Utilities.FormatCurrency(parseFloat(printData.TotalAmount) - parseFloat(printData.BillDiscount));
    var totalTaxAmountForPrint = Utilities.FormatCurrency(parseFloat(printData.TaxAmount));
    var grandTotalForPrint = Utilities.FormatCurrency(parseFloat(printData.GrandTotal));
    var amountTenderedForPrint = parseFloat(printData.AmountTendered);
    var amountCardForPrint = parseFloat(printData.cardAmount);
    var amountForPrint = Utilities.FormatCurrency(parseFloat(amountTenderedForPrint) + parseFloat(amountCardForPrint));
    var balanceForPrint = Utilities.FormatCurrency(parseFloat(printData.Balance));
    var salesPointObj = salesPoints.find(p => p.counterId == printData.CounterId);
    var salesPoint = salesPointObj == undefined ? "" : salesPointObj.counterName;
    var account = allLedgers.find(p => p.ledgerId == printData.LedgerId).ledgerName;
    var soldBy = $("#nameSpan").html();
    doc.setFontStyle('bold');
    doc.setFontSize(2);
    doc.text('Total Amount             NGN ' + totalAmountForPrint + '\n\n Discount                       NGN ' + billDiscountForPrint + '\n\n Sub Total                    NGN ' + subTotalForPrint + '\n\n\  Grand Total                NGN ' + grandTotalForPrint, 90, doc.autoTable.previous.finalY + 8);
    doc.text('Teller Number                ' + printData.POSTellerNo + '\n\n Discount %                            ' + printData.Discount + '\n\n Tax Amount                NGN ' + totalTaxAmountForPrint + '\n\n Amount Paid               NGN ' + amountForPrint, 150, doc.autoTable.previous.finalY + 8);
    doc.setFontSize(6);
    doc.text('INVOICE', 95, 57);
    doc.setDrawColor(207, 207, 207);
    doc.rect(86.5, doc.autoTable.previous.finalY, 103.2, 36);
    doc.setFontSize(2);
    doc.text('\nSales Point:\t' + salesPoint + '\nAccount:\t     ' + account + '\nSold By:\t      ' + soldBy + '\nInvoice No:\t ' + printData.InvoiceNo + '\nTrans Date:\t '
        + printData.Date + '\nStore:\t           ' + printData.lineItems[0].StoreName, 43, 67);
    doc.rect(40, 65, 66, 18);
    doc.setFontSize(3);
    doc.text('Balance: NGN ' + balanceForPrint, 125, doc.autoTable.previous.finalY + 33);
    doc.autoPrint();
    doc.save('POS' + printData.InvoiceNo + '.pdf');
    //window.open(doc.output('bloburl'));
    window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
}

//function print() {
//    var columns = [
//        { title: "SN", dataKey: "SlNo" },
//        { title: "Barcode", dataKey: "ProductBarcode" },
//        { title: "Product Code", dataKey: "ProductCode" },
//        { title: "Product Name", dataKey: "ProductName" },
//        { title: "Store", dataKey: "StoreName" },
//        { title: "Qty", dataKey: "Qty" },
//        { title: "Unit", dataKey: "Unit" },
//        { title: "Batch", dataKey: "BatchId" },
//        { title: "Rate", dataKey: "Rate" },
//        { title: "Gross Value", dataKey: "GrossAmount" },
//        { title: "Tax", dataKey: "TaxName" },
//        { title: "Tax Amount", dataKey: "TaxAmount" },
//        { title: "Amount", dataKey: "Amount" },

//    ];
//    var doc = new jsPDF('landscape');
//    doc.setFontSize(30);
//    doc.text('MAGNET CONSULTING ASSOCIATES', 15, 30);
//    doc.setFontSize(8);
//    doc.text('248, Ikorodu Road, Obanikoro Bus Stop', 15, 35);
//    doc.text('_________\n\n Signature & Date', 35, 190);
//    doc.text('_________\n\n Signature & Date', 227, 190);
//    doc.text('Agent Name: ' + findCust(printData.LedgerId) + '\n\nOrder No: ' + printData.InvoiceNo + '\n\nOrder Date: '
//        + printData.Date , 240, 20);

//    doc.autoTable(columns, printData.lineItems, {
//        startY: 45, theme: 'grid', styles: {
//            fontSize: 8
//        }
//    });
//    doc.save('POS' + printData.InvoiceNo + '.pdf');
//    //window.open(doc.output('bloburl'));
//    window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
//}

function viewDetailsClick(salesMasterIdFromUrl) {

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PendingSales/ViewSales?pendingsalesMasterId=" + salesMasterIdFromUrl + "&salesOrPending=" + true,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var details = data.SalesDetailsInfo;
            var output = "";
            var positionOfT = data.SalesMasterInfo.Date.indexOf('T');
            var transactionDate = data.SalesMasterInfo.Date.slice(0, positionOfT);
            var paymentMode = "";
            var otherDrop = "";
            var cashDrop = "";

            if (data.SalesMasterInfo.Extra1 == "cheque payment") {
                paymentMode = "Cheque";
                //cashDrop = findCust(data.SalesMasterInfo.LedgerId).replace(" and ", "");
            }
            else if (data.SalesMasterInfo.Extra1 == "bank payment") {
                paymentMode = "Bank";
                $("#tellerNoDiv").removeAttr("hidden");
                //$("#bankDropdownParent").removeAttr("hidden");
                //cashDrop = findCust(data.SalesMasterInfo.LedgerId).replace(" and ", "");
            }
            else if (data.SalesMasterInfo.Extra1 == "cash payment") {
                paymentMode = "Cash";
                //cashDrop = $("#paymentMode").val();
;            }
            else if (data.SalesMasterInfo.Extra1 == "both cash and bank payment") {
                paymentMode = "Both";
                $("#tellerNoDiv").removeAttr("hidden");
                $("#bankDropdownParent").removeAttr("hidden");
                cashDrop = findCust(data.SalesMasterInfo.LedgerId).replace(" and ", "");
                otherDrop = findCust(data.SalesMasterInfo.Extra2).replace(" and ", "");
            }

            //$("#journalNoDiv").html(data.JournalNo);
            $("#orderNo").html(data.SalesMasterInfo.VoucherNo);
            $("#agentName").html(findCust(data.SalesMasterInfo.LedgerId, data.SalesMasterInfo.Extra1));
            $("#orderDate").html(data.SalesMasterInfo.ExtraDate);
            $("#narration").val(data.SalesMasterInfo.Narration);
            $("#amountPaid").val(data.SalesMasterInfo.AmountTendered);
            $("#cardAmount").val(data.SalesMasterInfo.cardAmount);
            $("#totalTaxAmount").val(data.SalesMasterInfo.TaxAmount);
            $("#balance").val(data.SalesMasterInfo.ChangeGiven);
            $("#tellerNo").val(data.SalesMasterInfo.POSTellerNo);
            $("#totalAmount").val(data.SalesMasterInfo.TotalAmount);
            $("#percentDiscount").val(data.SalesMasterInfo.Discount);
            $("#billDiscount").val(data.SalesMasterInfo.BillDiscount);
            $("#grandTotal").val(data.SalesMasterInfo.GrandTotal);
            // $("#receiptNo").val(data.SalesMasterInfo.InvoiceNo);
            $("#transactionDate").val(transactionDate);
            $("#pricingLevel").val(data.SalesMasterInfo.PricinglevelId);
            $("#salesPoint").val(data.SalesMasterInfo.CounterId);
            $("#paymentMode").val(paymentMode);
            // $("#pendingSalesMasterId").val(salesMasterIdFromUrl); pendingSalesMasterId
            $("#pendingSalesMasterId").val(salesMasterIdFromUrl);
            $("#cashDropdown").val(data.SalesMasterInfo.Extra2);
            //$("#bankDropdown").val(data.SalesMasterInfo.Extra2);
            //findCust(ledgerId)
           
            salesMasterId = data.SalesMasterInfo.SalesMasterId;
            salesInvoiceLineItems = details;
            //$("#journalMasterId").val(data.JournalMasterId);
            //$("#debitAmtTxt").val(Utilities.FormatCurrency(data.TotalAmount));
            //$("#creditAmtTxt").val(Utilities.FormatCurrency(data.TotalAmount));
            //var output = "";
            for (var i = 0; i < details.length; i++) {
                var unitName = findUnit(details[i].UnitId).unitName == undefined ? "N/A" : findUnit(details[i].UnitId).unitName;
                var batchNo = findStore(+details[i].GodownId).godownName = undefined ? "N/A" : findStore(+details[i].GodownId).godownName;
                output +=
                    '<tr>\
                <td style="white-space: nowrap;"><button onclick="removeLineItem(' + details[i].ProductId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
                <td style="white-space: nowrap;">'+ (i + 1) + '</td>\
                <td style="white-space: nowrap;">' + findProduct(details[i].ProductId).barcode + '</td>\
                <td style="white-space: nowrap;">' + findProduct(details[i].ProductId).ProductCode + '</td>\
                <td style="white-space: nowrap;">' + findProduct(details[i].ProductId).ProductName + '</td>\
                <td style="white-space: nowrap;">' + findStore(+details[i].GodownId).godownName + '</td>\
                <td style="white-space: nowrap;">' + details[i].Qty + '</td>\
                <td style="white-space: nowrap;">' + unitName + '</td>\
                <td style="white-space: nowrap;">' + batchNo + '</td>\
                <td style="white-space: nowrap;">' + details[i].Rate + /*'</td>\
                <td style="white-space: nowrap;">' + details[i].GrossAmount + '</td>\
                <td style="white-space: nowrap;">' + row.Discount + '</td>\
                <td style="white-space: nowrap;">'+ row.NetAmount + */ '</td>\
                <td style="white-space: nowrap;">' + details[i].TaxName + '</td>\
                <td style="white-space: nowrap;">' + details[i].TaxAmount + '</td>\
                <td style="white-space: nowrap;">' + details[i].Amount + '</td>\
            </tr>\
            ';
            }
            $("#posLineItemTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry, could not load pending details.");
            Utilities.Loader.Hide();
        }
    });
}

function clear() {
    $("#amountPaid").val("0");
    $("#cardAmount").val("0");
    $("#totalTaxAmount").val("0");
    $("#balance").val("");
    $("#tellerNo").val("");
    $("#totalAmount").val("0");
    $("#percentDiscount").val(0);
    $("#billDiscount").val(0);
    $("#grandTotal").val("0");
    $("#paymentMode").val("");
    $("#salesPoint").val("");
}