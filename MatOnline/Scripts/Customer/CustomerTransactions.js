﻿$(function () {
    PriviledgeSettings.ApplyUserPriviledge();
    
    loadTransactions();
    
    $("#filterBtn").click(function () {
        $("#filterModal").modal("show");
    });
});

function  GetReceiptDetailFromMaster(receiptMasterId) { 

    $.ajax(API_BASE_URL + "/Receipt/GetReceiptDetail/?receiptMasterId="+receiptMasterId,
        {
            type:'get',
            beforeSend: function() {
                Utilities.Loader.Show();

            },
            complete: function() {
                Utilities.Loader.Hide();

            },
            success: function(data) {
                if (data != null) {
                    var values = [];
                    console.log(data);
                    $("#detailsModal").modal();
                    var count = 1;
                    var totalAmount = 0.0;
                    for (var i of data) {
                        values.push({
                            "sn": count,
                            "chequeNo": i.ReceiptMasterDetails.ReceiptDetails.chequeNo,
                            "chequeDate": i.ReceiptMasterDetails.ReceiptDetails.chequeDate,
                            "amount": i.ReceiptMasterDetails.ReceiptDetails.amount,
                            "ledgerName": i.Ledger.ledgerName
                        });
                        totalAmount += i.ReceiptMasterDetails.ReceiptDetails.amount;
                        count++;
                        $("#receiptNoDiv").text(i.ReceiptMasterDetails.ReceiptMaster.invoiceNo);
                        $("#receiptDateDiv").text(i.ReceiptMasterDetails.ReceiptMaster.date);
                      
                    } 
                    $('#invoicetb').DataTable({
                        data: values,
                        columns: [
                            { data: 'sn' },
                            { data: 'chequeNo' },
                            { data: 'chequeDate' },
                            { data: 'amount' },
                            { data: 'ledgerName' }

                        ]
                    });
                    $("#amountTxt").val(totalAmount);
                }
            }
        });
}
function loadTransactions()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerCentre/GetCustomerTransactions",
        type: "Get",
        contentType: "application/json",
        success: function (data) {            
            var transactionList = [];
            var count = 1;
            console.log("transactions",data);
            for (i = 0; i < data.Response.length; i++)
            {
                var transactionsWrapper = data.Response[i];
                if (transactionsWrapper.Transactions != null && transactionsWrapper.Transactions != "undefined")
                {
                    for (k = 0; k < transactionsWrapper.Transactions.length; k++)
                    {
                        var row = transactionsWrapper.Transactions[k];
                        transactionList.push([
                            count,
                            row.TransactionNumber,
                            row.TransactionDate.replace("0:00:00", ""),
                            row.TransactionType,
                            row.LedgerName,

                            Utilities.FormatCurrency(parseFloat(row.TransactionAmount)),
                            '<label class="label label-default">Open<label>',
                            row.DoneByUserName,
                            row.AdditionalDetails,
                            renderAction(row.TransactionType, row.TransactionId)
                        ]);
                        count++;
                    }
                }                
            }
            var rowSelection = $('#transactionList').DataTable({
                data: transactionList,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "responsive": true
            });

            $('.action').hide();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(hideUnauthorisedAction, 500);

            $('#transactionList').on('click', 'tr', function () {
                if ($(this).hasClass('selected')) {
                    $(this).removeClass('selected');
                }
                else {
                    rowSelection.$('tr.selected').removeClass('selected');
                    $(this).addClass('selected');
                }
            });


            Utilities.Loader.Hide();
        },
        error: function (e) {
            Utilities.Loader.Hide();
        }
    });
}

function renderAction(transactionType,transactionId)
{
    var output = "";
    if(transactionType=="SALES_ORDER")
    {
        output = '<div class="btn-group btn-group-sm">\
                        <div class="dropdown">\
                            <button class="hide salesOrderAction btn-sm btn-success btn-active-white dropdown-toggle" data-toggle="dropdown" type="button">\
                                Action <i class="dropdown-caret"></i>\
                            </button>\
                            <ul class="dropdown-menu">\
                            <!--    <li><a href="javascript:void(0);" onclick="showInvoiceModal('+transactionId+')" >Create Invoice</a></li>  -->\
                                         <li class="invoice-save hide"><a href="/Customer/SalesInvoice/Index?txtId=' + transactionId + '" " >Create Invoice</a> </li> \
                                <li class="deliveryNote-save hide"><a href="/Customer/DeliveryNote/Index?txtId=' + transactionId + '">Raise Delivery Note</a></li>\
                                <li class="salesOrder-view hide"><a onclick="GetSaleOrderFromMaster(' + transactionId + ')">View</a></li>\
                            </ul>\
                        </div>\
                    </div>\
                 ';
    } 
    //GetRejectionInFromMaster
    else if (transactionType == "SALES_INVOICE")
    {
        output = '<div class="btn-group btn-group-sm">\
                        <div class="dropdown">\
                            <button class="salesInvoiceAction hide btn btn-sm btn-success btn-active-white dropdown-toggle" data-toggle="dropdown" type="button">\
                                Action <i class="dropdown-caret"></i>\
                            </button>\
                            <ul class="dropdown-menu">\
                                <li class="receipt-save hide"><a href="/Customer/Receipt/receipt?invoiceTxnId=' + transactionId + '">Apply Payment</a></li>\
                                <li class="salesReturn-save hide"><a href="/Customer/SalesReturn/Index?invoicetxtId=' + transactionId + '">Return Sale</a></li>\
                            </ul>\
                        </div>\
                    </div>\
                 ';
    }
    else if (transactionType == "DELIVERY_NOTE")
    {
        output = '<div class="btn-group btn-group-sm">\
                        <div class="dropdown">\
                            <button class="deliveryNoteAction hide btn btn-sm btn-success btn-active-white dropdown-toggle" data-toggle="dropdown" type="button">\
                                Action <i class="dropdown-caret"></i>\
                            </button>\
                            <ul class="dropdown-menu">\
                           <li class="invoice-save hide"><a href="/Customer/SalesInvoice/Index?deliveryTxtId=' + transactionId + '" " >Create Invoice</a> </li> \
                    <li class="rejectionIn-save hide"><a href="/Customer/RejectionIn/Index?deliveryTxtId='+transactionId+'" " >Reject</a> </li> \
                    <li class="deliveryNote-view hide"><button onclick="GetDeliveryNoteFromMaster(' + transactionId + ')" class="btn btn-info">View</button> </li> \
                         </ul>\
                        </div>\
                    </div>\
                 ';
        //GetDeliveryNoteFromMaster
    }
    else if (transactionType == "RECEIPT") {

        output = '<button onclick="GetReceiptDetailFromMaster(' + transactionId + ')" class="receipt-view hide btn btn-xs btn-info"><i class="fa fa-eye"></i>View</button>';
                 ;
    }
    else if (transactionType == "Rejection In") {

        output = '<button onclick="GetRejectionInFromMaster(' + transactionId + ')" class="rejectionIn-view hide btn btn-xs btn-info"><i class="fa fa-eye"></i>View</button>';
        ;
    }
    else if (transactionType == "Sales Returned") {

        output = '<button onclick="GetReceiptDetailFromMaster(' + transactionId + ')" class="salesReturn-view hide btn btn-xs btn-info"><i class="fa fa-eye"></i>View</button>';
        ;
    }

    return output;
}

document.addEventListener("click", function () {
    hideUnauthorisedAction();
});

function hideUnauthorisedAction() {
    if (checkPriviledge("frmSalesInvoice", "Save") !== false) {
        $(".deliveryNoteAction").removeClass("hide");
        $(".deliveryNoteAction").show();
        $(".salesOrderAction").removeClass("hide");
        $(".salesOrderAction").show();
        $(".invoice-save").removeClass("hide");
        $(".invoice-save").show();
    }
    if (checkPriviledge("frmDeliveryNote", "Save") !== false) {
        $(".salesOrderAction").removeClass("hide");
        $(".salesOrderAction").show();
        $(".deliveryNote-save").removeClass("hide");
        $(".deliveryNote-save").show();
    }
    if (checkPriviledge("frmSalesOrder", "View") !== false) {
        $(".salesOrderAction").removeClass("hide");
        $(".salesOrderAction").show();
        $(".salesOrder-view").removeClass("hide");
        $(".salesOrder-view").show();
    }
    if (checkPriviledge("frmReceipts", "Save") !== false) {
        $(".salesInvoiceAction").removeClass("hide");
        $(".salesInvoiceAction").show();
        $(".receipt-save").removeClass("hide");
        $(".receipt-save").show();
    }
    if (checkPriviledge("frmSalesReturn", "Save") !== false) {
        $(".salesInvoiceAction").removeClass("hide");
        $(".salesInvoiceAction").show();
        $(".salesReturn-save").removeClass("hide");
        $(".salesReturn-save").show();
    }

    if (checkPriviledge("frmRejectionIn", "Save") !== false) {
        $(".deliveryNoteAction").removeClass("hide");
        $(".deliveryNoteAction").show();
        $(".rejectionIn-save").removeClass("hide");
        $(".rejectionIn-save").show();
    }
    if (checkPriviledge("frmDeliveryNote", "View") !== false) {
        $(".deliveryNoteAction").removeClass("hide");
        $(".deliveryNoteAction").show();
        $(".deliveryNote-view").removeClass("hide");
        $(".deliveryNote-view").show();
    }
    if (checkPriviledge("frmReceipts", "View") !== false) {
        $(".receipt-view").removeClass("hide");
        $(".receipt-view").show();
    }
    if (checkPriviledge("frmRejectionIn", "View") !== false) {
        $(".rejectionIn-view").removeClass("hide");
        $(".rejectionIn-view").show();
    }
    if (checkPriviledge("frmSalesReturn", "View") !== false) {
        $(".salesReturn-view").removeClass("hide");
        $(".salesReturn-view").show();
    }
}

function clearLineItemForm() {
    var dropdownlistBarcode = $("#searchByBarcodeInvoice").data("kendoDropDownList");
    var dropdownlistProductCode = $("#searchByProductCodeInvoice").data("kendoDropDownList");
    var dropdownlistProductName = $("#searchByProductNameInvoice").data("kendoDropDownList");

    dropdownlistBarcode.value("Please select");
    dropdownlistProductCode.value("Please select");
    dropdownlistProductName.value("Please select");
    $("#descriptionInvoice").val("");
    $("#quantityInvoice").val("0");
    $("#rateInvoice").val("");
    $("#amountInvoice").val("");
    $("#taxAmountInvoice").val("");
    $("#quantityInStockInvoice").val("");
    $("select#unitInvoice")[0].selectedIndex = 0;
    $("select#storeInvoice")[0].selectedIndex = 0;
}
