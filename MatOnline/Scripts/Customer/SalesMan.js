﻿var table = "";
var roles = [];
var salesMen = [];

getSalesMen();
getLookUps();

function getSalesMen() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/Pos/GetSalesMen",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            hideLoader();
            salesMen = data.salesMen;
            var objToShow = [];
            console.log(data);
            salesMen.forEach(function (item, count) {
                var isActiveLabel = item.isActive == "1" ? '<center><label class="label label-primary">Yes<label></center>' : '<center><label class="label label-default">No<label><center>'
                objToShow.push([
                    (count + 1),
                    item.employeeCode,
                    item.employeeName,
                    item.phoneNumber,
                    isActiveLabel,
                    '<button type="button" onclick="getSalesManDetails(' + item.employeeId + ')" class="btnEditModal btn btn-primary text-left" ><i class="fa fa-eye"></i> View</button>'
            //        '<div class="btn-group btn-group-sm employeeAction">\
            //    <div class="dropdown">\
            //        <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button>\
            //        <ul class="dropdown-menu">\
            //            <li class="viewEmployee" onclick="getSalesManDetails(' + item.employeeId + ')"><a>View Details</a></li>\
            //        </ul>\
            //    </div>\
            //</div>'
                ])
            }); 
            table = $('#salesMenTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}


function getLookUps() {
    var rolesAjax = $.ajax({
        url: API_BASE_URL + "/Role/GetRolesForPriviledges",
        type: "GET",
        contentType: "application/json"
    });

    $.when(rolesAjax)
        .done(function (dataRoles) {
            roles = dataRoles;
            $("#role").html('<option value="">-Select Role-</option>' + Utilities.PopulateDropDownFromArray(roles, 0, 1));
        });
}

$("#save").click(
    function () {
        if(validateSalesman()){
            var salesManToSave = {
                UserInfo: {
                    FirstName: $("#firstName").val(),
                    LastName: $("#lastName").val(),
                    UserName: $("#salesmanCode").val(),
                    RoleId: $("#role").val(),
                    PhoneNumber: $("#phoneNo").val(),
                    MobileNumber: $("#mobileNo").val(),
                    Active: $('#isActive').is(":checked"),
                    Address: $("#address").val(),
                    Password: $("#password").val(),
                    Narration: $("#narration").val()
                },
                EmployeeInfo: {
                    EmployeeName: $("#firstName").val() + " " + $("#lastName").val(),
                    EmployeeCode: $("#salesmanCode").val(),
                    Email: $("#email").val(),
                    RoleId: $("#role").val(),
                    PhoneNumber: $("#phoneNo").val(),
                    MobileNumber: $("#mobileNo").val(),
                    IsActive: $('#isActive').is(":checked"),
                    Address: $("#address").val(),
                    Narration: $("#narration").val()
                }
            }
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Pos/SaveSalesman",
                type: 'POST',
                data: JSON.stringify(salesManToSave),
                contentType: "application/json",
                success: function (data) {
                    hideLoader();
                    if (data.ResponseCode == 200) {
                        table.destroy();
                        clearModal();
                        getSalesMen();
                        Utilities.SuccessNotification(data.ResponseMessage);
                        window.location.href = window.location.href;
                    }
                    else if(data.ResponseCode == 403) {
                        Utilities.ErrorNotification(data.ResponseMessage);
                    }
                },
                error: function (request, error) {
                    hideLoader();
                    Utilities.ErrorNotification("Error Saving");
                }
            });
        };
    }
)

function clearModal() {
    $("#firstName").val() = "";
    $("#lastName").val() = "";
    $("#salesmanCode").val() = "";
    $("#role").val() = "";
    $("#phoneNo").val() = "";
    $("#mobileNo").val() = "";
    $('#isActive').prop("checked", true);
    $("#address").val() = "";
    $("#password").val() = "";
    $("#narration").val() = "";
}
function validateSalesman() {
    if ($("#firstName").val() == "") {
        Utilities.ErrorNotification("First Name cannot be empty!");
        return false;
    }
    else if ($("#lastName").val() == "") {
        Utilities.ErrorNotification("Last Name cannot be empty!");
        return false;
    }
    else if ($("#salesmanCode").val() == "") {
        Utilities.ErrorNotification("Salesman code cannot be empty!");
        return false;
    }
    else if ($("#email").val() == "") {
        Utilities.ErrorNotification("Email cannot be empty!");
        return false;
    }
    else if ($("#role").val() == "") {
        Utilities.ErrorNotification("Role cannot be empty!");
        return false;
    }
    else if ($("#password").val() == "") {
        Utilities.ErrorNotification("Password cannot be empty!");
        return false;
    }
    else if ($("#password").val() !== $("#confirmPassword").val()) {
        Utilities.ErrorNotification("Password does not match!");
        return false;
    }
    return true;
}

function getSalesManDetails(salesManId) {
    $("#updateSalesMan").hide();
    $("#detailsModal").modal("show");
    var salesMan = salesMen.find(p => p.employeeId == salesManId);
    //var roleObj = roles.find(p => p.roleId == salesMan.roleId);
    //var roleName = roleObj == undefined ? "" : roleObj.role;
    if (salesMan.isActive == "1") {
        $("#isActive2").prop("checked", true);
    }
    else {
        $("#isActive2").prop("checked", false);
    }
    $("#employeeId").val(salesManId);
    $("#firstName2").val(salesMan.firstName);
    $("#lastName2").val(salesMan.lastName);
    $("#salesmanCode2").val(salesMan.employeeCode);
    $("#email2").val(salesMan.email);
    $("#mobileNo2").val(salesMan.mobileNumber);
    $("#phoneNo2").val(salesMan.phoneNumber);
    $("#address2").val(salesMan.address);
    $("#narration2").val(salesMan.narration);
    //$("#role2").html('<option value="">' + roleName + '</option>');
    $("#role2").html('<option value="">-Select Role-</option>' + Utilities.PopulateDropDownFromArray(roles, 0, 1));
    $("#role2").chosen({ width: "100%", margin: "1px" });
    $("#role2").val(salesMan.roleId);
    $("#role2").trigger("chosen:updated");
}

function editSalesMan() {
    $('#detailsModal').find(':input').prop('readonly', false);
    $("#editSalesMan").hide();
    $("#updateSalesMan").show();
}

$("#updateSalesMan").click(
    function () {
        if (validateSalesmanForUpdate()) {
            var salesManToSave = {
                UserInfo: {
                    FirstName: $("#firstName2").val(),
                    LastName: $("#lastName2").val(),
                    UserName: $("#salesmanCode2").val(),
                    RoleId: $("#role2").val(),
                    PhoneNumber: $("#phoneNo2").val(),
                    MobileNumber: $("#mobileNo2").val(),
                    Active: $('#isActive2').is(":checked"),
                    Address: $("#address2").val(),
                    Narration: $("#narration2").val()
                },
                EmployeeInfo: {
                    EmployeeId: $("#employeeId").val(),
                    EmployeeName: $("#firstName2").val() + " " + $("#lastName2").val(),
                    EmployeeCode: $("#salesmanCode2").val(),
                    Email: $("#email2").val(),
                    RoleId: $("#role2").val(),
                    PhoneNumber: $("#phoneNo2").val(),
                    MobileNumber: $("#mobileNo2").val(),
                    IsActive: $('#isActive2').is(":checked"),
                    Address: $("#address2").val(),
                    Narration: $("#narration2").val()
                }
            }
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Pos/UpdateSalesman",
                type: 'POST',
                data: JSON.stringify(salesManToSave),
                contentType: "application/json",
                success: function (data) {
                    hideLoader();
                    if (data.ResponseCode == 200) {
                        table.destroy();
                        getSalesMen();
                        Utilities.SuccessNotification(data.ResponseMessage);
                    }
                    else if (data.ResponseCode == 403) {
                        Utilities.ErrorNotification(data.ResponseMessage);
                    }
                },
                error: function (request, error) {
                    hideLoader();
                    Utilities.ErrorNotification("Error Saving");
                }
            });
        };
    }
)

function validateSalesmanForUpdate() {
    if ($("#firstName2").val() == "") {
        Utilities.ErrorNotification("First Name cannot be empty!");
        return false;
    }
    else if ($("#lastName2").val() == "") {
        Utilities.ErrorNotification("Last Name cannot be empty!");
        return false;
    }
    else if ($("#salesmanCode2").val() == "") {
        Utilities.ErrorNotification("Salesman code cannot be empty!");
        return false;
    }
    else if ($("#email2").val() == "") {
        Utilities.ErrorNotification("Email cannot be empty!");
        return false;
    }
    else if ($("#role2").val() == "") {
        Utilities.ErrorNotification("Role cannot be empty!");
        return false;
    }
    return true;
}