﻿var rejectionInList = [];
var customer = [];
var User = [];
var products = [];
var units = [];
var stores = [];
var thisRejectionIn = {};
var details = [];

$(function () {
    $("#formNo").hide();
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
});

function getRejectionInListing(fromDate, todate, approved)
{
    var newToDate = todate + " 23:59:59 PM";
    console.log(newToDate);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/RejectionIn/getPendingRejectionIn?fromDate=" + fromDate + "&toDate=" + newToDate + "&approved=" + approved,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            rejectionInList = data.pendingRejectionIns.Table;
            units = data.unit;
            customer = data.ledgers;
            User = data.users;
            stores = data.stores;
            products = data.Products;

            var output = "";
            var objToShow = [];
            var statusOutput = "";
            $.each(rejectionInList, function (count, row) {
                if (row.status == "Pending") {
                    statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending </span>';
                }
                else if (row.status == "Approved") {
                    statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Approved</span>';
                }
                else if (row.status == "Cancelled") {
                    statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
                }
                objToShow.push([
                    count + 1,
                    Utilities.FormatJsonDate(row.date),
                    findCustomer(row.ledgerId).ledgerName,
                    '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
                    findUser(row.userId).userName,
                    statusOutput,
                    '<button type="button" class="btn btn-primary btn-sm" onclick="getRejectionInDetails(' + row.rejectionInMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                ]);
            });
            table = $('#rejectionInListTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            //$("#salesQuotationListTbody").html(output);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getRejectionInDetails(id) {
    Utilities.Loader.Show();
    console.log(rejectionInList);
    for (var i = 0; i < rejectionInList.length; i++)
    {
        if (rejectionInList[i].rejectionInMasterId == id)
        {
            thisRejectionIn = rejectionInList[i];
        }
    }
    console.log(thisRejectionIn);
    $.ajax({
        url: API_BASE_URL + "/RejectionIn/getPendingRejectionInDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            details = data.details.Table;
            console.log(details);
            if (thisRejectionIn.status == "Pending") {
                $("#approveOrder").show();
               // $("#cancelOrder").show();
                statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending Approval</span>';
            }
            else if (thisRejectionIn.status == "Approved") {
                $("#approveOrder").hide();
                //$("#cancelOrder").hide();
                statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Approved</span>';
            }
            else if (thisRejectionIn.status == "Cancelled") {
                $("#approveOrder").hide();
                //$("#cancelOrder").hide();
                statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
            }


            //$("#orderNoDiv").html(row.invoiceNo);
            $("#orderDateDiv").html(Utilities.FormatJsonDate(thisRejectionIn.date));
            //$("#orderNoDiv").html(row.invoiceNo);
            $("#raisedByDiv").html(findUser(thisRejectionIn.userId).userName);
            $("#raisedForDiv").html(findCustomer(thisRejectionIn.ledgerId).ledgerName);
            $("#statusDiv").html(statusOutput);
            //$("#orderAmountTxt").val(Utilities.FormatCurrency(row.totalAmount));
            ////$("#taxAmountTxt").val(Utilities.FormatCurrency(tAmount));
            //$("#grandTotalTxt").val(Utilities.FormatCurrency(row.totalAmount));

            var output = "";
            $.each(details, function (count, row) {
                output += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProduct(row.productId).productCode + '</td>\
                            <td>' + findProduct(row.productId).productCode + '</td>\
                            <td>' + findProduct(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>' + findUnit(row.unitId).unitName + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                        </tr>';
            });


            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function approveRejectionIn()
{
    Utilities.Loader.Show();
    var toSave = {
        Date: thisRejectionIn.date,
        CustomerId: thisRejectionIn.ledgerId,
        Narration: thisRejectionIn.narration,
        TotalAmount: thisRejectionIn.totalAmount,
        TransportationCompany: thisRejectionIn.transportationCompany,
        LrNo: thisRejectionIn.lrNo,
        PricingLevelId: thisRejectionIn.pricinglevelId,
        DeliveryNoteMasterId: thisRejectionIn.deliveryNoteMasterId,
        UserId: matUserInfo.UserId,
        LineItems: details,
    }

    console.log(toSave);
    $.ajax({
        url: API_BASE_URL + "/RejectionIn/saveRejectionIn",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            Utilities.Loader.Hide();
            if (data == "Rejection Successfully Saved") {
                Utilities.Loader.Hide();
                Utilities.SuccessNotification("Rejection-In saved");
                window.location = "/Customer/RejectionIn/RejectionInListing";
            }
            else {
                Utilities.ErrorNotification("Rejection-In Failed");
            }
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function findCustomer(ledgerId) {
    var output = {};
    for (i = 0; i < customer.length; i++) {
        if (customer[i].ledgerId == ledgerId) {
            output = customer[i];
            break;
        }
    }
    return output;
}

function findUser(userId) {
    var output = {};
    for (i = 0; i < User.length; i++) {
        if (User[i].userId == userId) {
            output = User[i];
            break;
        }
    }
    return output;
}

function findProduct(id) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].productId == id) {
            output = products[i];
            break;
        }
    }
    return output;
}
function findUnit(id) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == id) {
            output = units[i];
            break;
        }
    }
    return output;
}
function findStore(id) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == id) {
            output = stores[i];
            break;
        }
    }
    return output;
}