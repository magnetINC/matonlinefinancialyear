﻿var rejectionInList = [];
var customer = [];
var User = [];
var products = [];
var units = [];
var thisRejectionIn = {};
var details = [];


$(function () {
    $("#formNo").hide();
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
});

function getRejectionInListing(fromDate, todate, approved) {
    var newToDate = todate + " 23:59:59 PM";
    console.log(newToDate);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/RejectionIn/GetRejectionIn/?fromDate=" + fromDate + "&toDate=" + newToDate + "&approved=" + approved,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log( "rejections", data);
            rejectionInList = data.rejectionIns;
            units = data.unit;
            customer = data.ledgers;
            User = data.users;
            products = data.products;

            var output = "";
            var objToShow = [];
            var statusOutput = "";
            $.each(rejectionInList, function (count, row) {
                //if (row.status == "Pending") {
                //    statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending </span>';
                //}
                //else if (row.status == "Approved") {
                //    statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Approved</span>';
                //}
                //else if (row.status == "Cancelled") {
                //    statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
                //}
                objToShow.push([
                    count + 1,
                    row.rejectionInMasterId,
                    Utilities.FormatJsonDate(row.date),
                    (findCustomer(row.ledgerId).ledgerName == "")? " N/A" :  findCustomer(row.ledgerId).ledgerName ,
                    Utilities.FormatCurrency(row.totalAmount),
                    findUser(row.userId).userName,
                    '<button type="button" class="btn btn-info btn-xs" onclick="getRejectionInDetails(' + row.rejectionInMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                ]);
            });
            table = $('#rejectionInListTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            //$("#salesQuotationListTbody").html(output);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getRejectionInDetails(id) {
    Utilities.Loader.Show();
    
    var thisRejectionIn = rejectionInList.find(x => x.rejectionInMasterId == id);
    var doneBy = findUser(thisRejectionIn.userId).userName;
    
    $.ajax({
        url: API_BASE_URL + "/RejectionIn/GetRejectionInDetails/?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            details = data.details.Table;
            console.log(details);
            //if (thisRejectionIn.status == "Pending") {
            //    $("#ApproveOrder").show();
            //    // $("#cancelOrder").show();
            //    statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending Approval</span>';
            //}
            //else if (thisRejectionIn.status == "Approved") {
            //    //$("#ApproveOrder").hide();
            //    //$("#cancelOrder").hide();
            //    statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Approved</span>';
            //}
            //else if (thisRejectionIn.status == "Cancelled") {
            //    //$("#ApproveOrder").hide();
            //    //$("#cancelOrder").hide();
            //    statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
            //}


            //$("#orderNoDiv").html(row.invoiceNo);
            $("#orderDateDiv").html(Utilities.FormatJsonDate(thisRejectionIn.date));
            //$("#orderNoDiv").html(row.invoiceNo);
            $("#raisedByDiv").html(doneBy);
            $("#raisedForDiv").html(findCustomer(thisRejectionIn.ledgerId).ledgerName);
            //$("#statusDiv").html(statusOutput);
            //$("#orderAmountTxt").val(Utilities.FormatCurrency(row.totalAmount));
            ////$("#taxAmountTxt").val(Utilities.FormatCurrency(tAmount));
            //$("#grandTotalTxt").val(Utilities.FormatCurrency(row.totalAmount));

            var output = "";
            $.each(details, function (count, row) {
                output += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProduct(row.productId).productCode + '</td>\
                            <td>' + findProduct(row.productId).productCode + '</td>\
                            <td>' + findProduct(row.productId).productName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>' + findUnit(row.unitId).unitName + '</td>\
                            <td>'+ Utilities.FormatCurrency(row.rate) + '</td>\
                            <td>' + Utilities.FormatCurrency(row.amount) + '</td>\
                        </tr>';
            });


            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            $("#edit").attr("data-id", id);
            $("#delete").attr("data-id", id);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
$(function () {
    $("#edit").click(function() {
        var id = $(this).attr("data-id");
        //redirect to edit  

        location.href = "/customer/rejectionIn/edit/" + id;
    });

    $("#delete").click(function () {
        var id = $(this).attr("data-id");
        //process delete request
        Utilities.ConfirmFancy("Are You Sure you want to delete!",
            function() {
                $.ajax(API_BASE_URL + "/RejectionIn/Delete/" + id,
                    {
                        type: "delete",
                        success: function(res) {
                            if (res.status == 200) {
                                Utilities.SuccessNotification(res.message);
                                setTimeout(function() {
                                        location.href = "/Customer/RejectionIn/RejectionInRegister";
                                    },
                                    4000);
                            }

                        }
               
                    });
            });
    });
})

function findCustomer(ledgerId) {
    var output = {};
    for (i = 0; i < customer.length; i++) {
        if (customer[i].ledgerId == ledgerId) {
            output = customer[i];
            break;
        }
    }
    return output;
}

function findUser(userId) {
    var output = {};
    for (i = 0; i < User.length; i++) {
        if (User[i].userId == userId) {
            output = User[i];
            break;
        }
    }
    return output;
}

function findProduct(id) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].productId == id) {
            output = products[i];
            break;
        }
    }
    return output;
}
function findUnit(id) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == id) {
            output = units[i];
            break;
        }
    }
    return output;
}