﻿var thisSalesReturn = {};
var salesReturnList = [];
var salesReturnDetails = [];
var returnDetails = [];
var lineItems = [];

var ledgers = [];
var products = [];
var units = [];
var stores = [];
var batches = [];
var users = [];
var voucherTypes = []

var table = "";

$(function () {
    $("#formNo").hide();
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
    getLookUps();
});

function getLookUps() {
    Utilities.Loader.Show();
    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/SalesReturn/GetLookUps",
        type: "GET",
        contentType: "application/json",
    });
    $.when(lookUpAjax)
    .done(function (dataLookUp) {
        ledgers = dataLookUp.Ledgers;
        units = dataLookUp.Unit;
        users = dataLookUp.Users;
        stores = dataLookUp.Stores;
        products = dataLookUp.Products;
        batches = dataLookUp.Batch;
        tax = dataLookUp.tax;
        voucherTypes = dataLookUp.VoucherTypes;

        GetPendingList("2017-01-01", $("#toDate").val(), "");
    });
}
function GetPendingList(fromDate, todate, approved) {
    var newToDate = todate + " 23:59:59 PM";
    console.log(newToDate);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesReturn/GetPendingToList?fromDate=" + fromDate + "&toDate=" + newToDate + "&Status=" + approved,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            salesReturnList = data;

            var output = "";
            var objToShow = [];
            var statusOutput = "";
            $.each(salesReturnList, function (count, row) {
                if (row.status == "Pending") {
                    statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending </span>';
                }
                else if (row.status == "Approved") {
                    statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Approved</span>';
                }
                else if (row.status == "Cancelled") {
                    statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
                }
                objToShow.push([
                    count + 1,
                    Utilities.FormatJsonDate(row.date),
                    findCustomer(row.ledgerId).ledgerName,
                    '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
                    findUser(row.userId).userName,
                    statusOutput,
                    '<button type="button" class="btn btn-primary btn-sm" onclick="getSalesReturnDetails(' + row.salesReturnMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                ]);
            });
            table = $('#salesReturnListTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            //$("#salesQuotationListTbody").html(output);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getSalesReturnDetails(id) {
    Utilities.Loader.Show();
    for (var i = 0; i < salesReturnList.length; i++) {
        if (salesReturnList[i].salesReturnMasterId == id) {
            thisSalesReturn = salesReturnList[i];
        }
    }
    console.log(thisSalesReturn);
    $.ajax({
        url: API_BASE_URL + "/SalesReturn/GetPendingDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            returnDetails = data;
            console.log(data);
            if (thisSalesReturn.status == "Pending") {
                $("#approveReturn").show();
                $("#cancelReturn").show();
                statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending Approval</span>';
            }
            else if (thisSalesReturn.status == "Approved") {
                $("#approveReturn").hide();
                $("#cancelReturn").hide();
                statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Approved</span>';
            }
            else if (thisSalesReturn.status == "Cancelled") {
                $("#approveReturn").hide();
                $("#cancelReturn").hide();
                statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
            }


            //$("#orderNoDiv").html(row.invoiceNo);
            $("#orderDateDiv").html(Utilities.FormatJsonDate(thisSalesReturn.date));
            //$("#orderNoDiv").html(row.invoiceNo);
            $("#raisedByDiv").html(findUser(thisSalesReturn.userId).userName);
            $("#raisedForDiv").html(findCustomer(thisSalesReturn.ledgerId).ledgerName);
            $("#statusDiv").html(statusOutput);

            $("#totalAmount").val(Utilities.FormatCurrency(thisSalesReturn.totalAmount));
            $("#billDiscount").val(Utilities.FormatCurrency(thisSalesReturn.discount));
            $("#grandTotal").val(Utilities.FormatCurrency(thisSalesReturn.grandTotal));

            var output = "";
            $.each(data, function (count, row) {
                output += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProduct(row.productId).productCode + '</td>\
                            <td>' + findProduct(row.productId).productCode + '</td>\
                            <td>' + findProduct(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>' + findUnit(row.unitId).unitName + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.grossAmount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.discount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.netAmount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.taxAmount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                        </tr>';
            });


            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function cancelReturn() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesReturn/CancelReturn?id=" + thisSalesReturn.salesReturnMasterId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            Utilities.Loader.Hide();
            if (data > 0) {
                Utilities.Loader.Hide();
                Utilities.SuccessNotification("Sales Return Cancelled");
                $("#detailsModal").modal("hide");
                if (table != "") {
                    table.destroy();
                }
                GetPendingList("2017-01-01", $("#toDate").val(), "");
                //window.location = "/Customer/RejectionIn/RejectionInListing";
            }
            else {
                Utilities.Loader.Hide();
                Utilities.ErrorNotification("Sales Return Cancellation Failed");
            }
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function findCustomer(ledgerId) {
    var output = {};
    for (i = 0; i < ledgers.length; i++) {
        if (ledgers[i].ledgerId == ledgerId) {
            output = ledgers[i];
            break;
        }
    }
    return output;
}
function findUser(userId) {
    var output = {};
    for (i = 0; i < users.length; i++) {
        if (users[i].userId == userId) {
            output = users[i];
            break;
        }
    }
    return output;
}
function findProduct(id) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].productId == id) {
            output = products[i];
            break;
        }
    }
    return output;
}
function findUnit(id) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == id) {
            output = units[i];
            break;
        }
    }
    return output;
}
function findStore(id) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == id) {
            output = stores[i];
            break;
        }
    }
    return output;
}