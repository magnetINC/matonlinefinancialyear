﻿var table = "";
var receipts = [];
var thisReceipt = {};
var customers = [];
var receipt = 0;

$(function () {
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
    //getReceiptList("2017-01-01", $("#toDate").val(), $("#cashOrBank").val());
    getLookups();
});
$(function () {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Register/cashOrBankComboFill",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            //console.log(data);
            $("#cashOrBank").html("<option value=\"\">All</option>" + Utilities.PopulateDropDownFromArray(data, 0, 2));
            //Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
});

//to load all look up data
function getLookups() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Receipt/GetLookups",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            customers = data.Customers;
            getReceiptList("2017-01-01", $("#toDate").val(), $("#cashOrBank").val());
            //Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

//setTimeout(() => { getReceiptList("2017-01-01", $("#toDate").val(), $("#cashOrBank").val());}, 5000);

 function  GetReceiptDetail(receiptMasterId) {
            $.ajax(API_BASE_URL + "/Receipt/GetReceiptDetail/?receiptMasterId=60384",
                {
                    type:'get',
                    beforeSend: function() {
                        Utilities.Loader.Show();

                    },
                    complete: function() {
                        Utilities.Loader.Hide();

                    },
                    success: function(data) {
                        if (data != null) {
                            $("#detailsModal").modal();
                            var values = [];
                            console.log(data);
                           
                            var count = 1;
                            for (var i of data) {
                                values.push({
                                    "sn": count,
                                    "chequeNo": i.ReceiptMasterDetails.ReceiptDetails.chequeNo,
                                    "chequeDate": i.ReceiptMasterDetails.ReceiptDetails.chequeDate,
                                    "amount": i.ReceiptMasterDetails.ReceiptDetails.amount,
                                    "ledgerName": i.Ledger.ledgerName
                                });
                                count++;
                            } 
                            $('#invoicetb').DataTable({
                                data: data,
                                columns: [
                                    { data: 'sn' },
                                    { data: 'chequeNo' },
                                    { data: 'chequeDate' },
                                    { data: 'amount' },
                                    { data: 'ledgerName' }

                                ]
                            });
                        }
                    }
                });
        }
function getReceiptList(fromDate, toDate, ledgerId) {
    receiptearchParam = {
        fromDate: fromDate,
        toDate: toDate,
        ledgerId: ledgerId,
        formNo: "",
        userId: matUserInfo.UserId
    };

    console.log(receiptearchParam);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Register/Receipt",
        type: "Post",
        data: JSON.stringify(receiptearchParam),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 1000, data);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function onSuccess(data) {
    receipts = data;
    var output = "";
    var objToShow = [];
    console.log(customers);
    $.each(data, function (count, row) {
        var customer = customers.find(p => p.ledgerId == row.ledgerId);
        var customerName = customer == null ? '-' : customer.ledgerName;
        var doneBy = row.doneBy == null ? "Admin" : row.doneBy; 
        objToShow.push([
            count + 1,
            row.invoiceNo,
            customerName,
            row.date,
            row.totalAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ","),
            //row.invoiceNo,
            row.narration,
            row.ledgerName,
            doneBy,
        '<button onclick="getReceiptDetails(' + row.receiptMasterId + ')" class="btn btn-info btn-sm"><i class="fa fa-eye"></i> View</button>'
        ]);
    });
    if (checkPriviledge("frmReceipts", "View") === false) {
        objToShow.forEach(function (item) {
            item.splice(6, 1);
        });
    }
    console.log("receipt stuff",objToShow);
    table = $('#receiptListTbody').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    Utilities.Loader.Hide();
    //$("#deliveryNoteListTbody").html(output);
}

//getReceiptList("2017-01-01", $("#toDate").val(), $("#cashOrBank").val());

function GetThisReceipt(id) {
    for (i = 0; i < receipts.length; i++) {
        if (receipts[i].receiptMasterId == id) {
            thisReceipt = receipts[i];
        }
    }
}

function getReceiptDetails(id) {
    Utilities.Loader.Show();
    GetThisReceipt(id)
    receipt = id;
    $.ajax({
        url: API_BASE_URL + "/Register/ReceiptDetails?receiptMasterId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log('test', data);
            $("#receiptNoDiv").html(thisReceipt.invoiceNo);
            $("#receiptDateDiv").html(thisReceipt.date);
            $("#raisedForDiv").html(thisReceipt.ledgerName);
            $("#amountTxt").val(thisReceipt.totalAmount);
            var output = "";
            $.each(data, function (count, row) {
            //    row.totalAmount.toString().replace(/\B(?=(\d{3})+(?!\d))/g;
                var invoiceNo = row.invoiceNo == null ? '-' : row.invoiceNo;
                output += '<tr>\
                            <td>' + (count + 1) + '</td>\
                            <td>' + findCustomer(row.ledgerId).ledgerName + '</td>\
                            <td>' + row.chequeNo + '</td>\
                            <td>' + Utilities.FormatCurrency(row.amount) + '</td>\
                            <td>' + invoiceNo + '</td>\
                            <td>' + Utilities.FormatJsonDate(row.chequeDate) + '</td>\
                        </tr>';
            });
            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            thisReceipt = {};
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function EditReceipt()
{
    window.location = "/Customer/Receipt/EditReceipt/" + receipt;
}

function deleteReceipt() {
    if (confirm('Delete this Receipt?')) {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/Receipt/deleteReceipt?receiptMastertId=" + receipt,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data > 0) {
                    Utilities.SuccessNotification("Receipt Deleted successfully");
                    $("#detailsModal").modal("hide");
                    window.location = "/Customer/Receipt/Index";
                }
                else {
                    Utilities.ErrorNotification("Oops! Sorry, couldn't delete");
                }
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                Utilities.Loader.Hide();
            }
        });
    }
}
///$("#orderNo").val() + "&salesMode=" + $("#salesMode").val(),
function findCustomer(ledgerId) {
    var output = {};
    for (i = 0; i < customers.length; i++) {
        if (customers[i].ledgerId == ledgerId) {
            output = customers[i];
            break;
        }
    }
    return output;
}