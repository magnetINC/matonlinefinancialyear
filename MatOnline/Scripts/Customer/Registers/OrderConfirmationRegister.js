﻿var table = "";
var orderConfirmationSearchParameters = {};
var currentViewedOrderMasterId = "";
var orderListForSearch = [];
var allUsers = [];
$(function () {

    $.ajax({
        url: API_BASE_URL + "/UserCreation/GetAllUses",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            //console.log('all users', data.Users)
            allUsers = data.Users;
        }
    });



    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
});


function orderConfirmationListTable(fromDate, toDate, condition) {

    Utilities.Loader.Show();

    orderConfirmationSearchParameters = {
        fromDate: fromDate,
        toDate: toDate,
        ledgerId: 0,
        gmUserId: matUserInfo.UserId,
        salesOrderNo: "",
        condition: condition
    };

    $.ajax({
        url: API_BASE_URL + "/Register/SalesOrder",
        type: "Post",
        data: JSON.stringify(orderConfirmationSearchParameters),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            onSuccess(data);     
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function onSuccess(data){
    orderListForSearch = data;
    var output = "";
    var objToShow = [];
    $.each(data, function (count, row) {
        var statusOP = "";
        if (row.AuthorizationStatus == "Pending") {
            statusOP = '<span class="label label-warning">Pending</span>';
        }
        else if (row.AuthorizationStatus == "Cancelled") {
            statusOP = '<span class="label label-danger">Cancelled</span>';
        }
        else if (row.AuthorizationStatus == "Processed") {
            statusOP = '<span class="label label-mint"> Processed</span>';
        }
        else {
            statusOP = '<span class="label label-success"> Authorized</span>';
        }
       
        objToShow.push([
            count + 1,
            row.invoiceNo,
            row.ledgerName,
            row.date,
            Utilities.FormatCurrency(row.totalAmount),
            row.dueDate,
            row.userName,
            //statusOP,
            '<button type="button" class="btn btn-info btn-xs" onclick="getOrderConfirmationTransaction(' + row.salesOrderMasterId + ')"><i class="fa fa-eye"></i> View</button>',
            '<button type="button" class="btn btn-primary btn-xs" onclick="printOrderConfirmationTransaction(' + row.salesOrderMasterId + ')"><i class="fa fa-print"></i> Print</button>'
        ]);
    });
    if (checkPriviledge("frmSalesOrderRegister", "View") === false) {
        objToShow.forEach(function (item) {
            item.splice(7, 1);
        });
    }
    
    if (checkPriviledge("frmSalesOrder", "Update") === false) {
        $('.editOrder').hide();
    }
    if (checkPriviledge("frmSalesOrder", "Delete") === false) {
        $('.deleteOrder').hide();
    }

    if (table != null && table != undefined && table != "") {

        table.destroy();
    }

    table = $('#orderConfirmationListTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    Utilities.Loader.Hide();
}

function getOrderConfirmationTransaction(salesOrderMasterId) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetSalesOrderForAuthorization?salesOrderMasterId=" + salesOrderMasterId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            var doneByUserName = orderListForSearch.find(x => x.salesOrderMasterId == salesOrderMasterId).userName;
            var doneByFinder = allUsers.find(x => x.userName == doneByUserName);
            var doneByFullName = doneByFinder.firstName + ' ' + doneByFinder.lastName;
            var doneBy = doneByFullName == undefined ? 'admin' : doneByFullName;

            master = data.Master;
            details = data.Details;
            var statusOutput = "";
            salesOrderMasterIdToAuthorize = salesOrderMasterId;
            currentViewedOrderMasterId = salesOrderMasterId;
            if (master.AuthorizationStatus == "Pending") {
                $("#authorizeOrder").show();
                //$("#cancelOrder").show();
                statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending</span>';
            }
            else if (master.AuthorizationStatus == "Authorized") {
                $("#authorizeOrder").hide();
                // $("#cancelOrder").hide();
                statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Authorized</span>';
            }
            else if (master.AuthorizationStatus == "Cancelled") {
                $("#authorizeOrder").hide();
                // $("#cancelOrder").hide();
                statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
            }
            else if (master.AuthorizationStatus == "Processed") {
                statusOutput = '<span class="label label-mint"><i class="fa fa-check"></i> Processed</span>';
            }
            //$("#orderNoDiv").html(master.InvoiceNo);
            $("#orderDateDiv").html(Utilities.FormatJsonDate(master.Date));
            $("#orderNoDiv").html(master.InvoiceNo);
            $("#raisedByDiv").html(doneBy);
            $("#raisedForDiv").html(master.CustomerName);
            $("#statusDiv").html(statusOutput);
            $("#orderAmountTxt").val('N'+Utilities.FormatCurrency(master.TotalAmount));
            $("#taxAmountTxt").val('N'+Utilities.FormatCurrency(master.TaxAmount));
            $("#grandTotalTxt").val('N'+Utilities.FormatCurrency(master.TotalAmount + master.TaxAmount));
            var output = "";
            for (i = 0; i < details.length; i++) {
                output += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td><button onclick="getStoreQuantity('+ details[i].productId + ',\'' + details[i].extra1 + '\')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></button></td>\
                            <td>' + details[i].barcode + '</td>\
                            <td>' + details[i].productcode + '</td>\
                            <td>' + details[i].product + '</td>\
                            <td>' + (details[i].extra1 == "" ? "NA" : details[i].extra1) + '</td>\
                            <td>' + details[i].qty + '</td>\
                            <td>' + details[i].unit + '</td>\
                            <td>' +Utilities.FormatCurrency(details[i].rate) + '</td>\
                            <td>' + Utilities.FormatCurrency(details[i].amount) + '</td>\
                            <td>' + Utilities.FormatCurrency(details[i].taxAmount) + '</td>\
                        </tr>';
            }
            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;

//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}



function printOrderConfirmationTransaction(salesOrderMasterId) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetSalesOrderForAuthorization?salesOrderMasterId=" + salesOrderMasterId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            var doneByUserName = orderListForSearch.find(x => x.salesOrderMasterId == salesOrderMasterId).userName;
            var doneByFinder = allUsers.find(x => x.userName == doneByUserName);
            var doneByFullName = doneByFinder.firstName + ' ' + doneByFinder.lastName;
            var doneBy = doneByFullName == undefined ? 'admin' : doneByFullName;
            
            master = data.Master;
            details = data.Details;
            /*salesOrderMasterIdToAuthorize = salesOrderMasterId;
            currentViewedOrderMasterId = salesOrderMasterId;*/
            var companyName = $("#companyName").val();
            var companyAddress = $("#companyAddress").val();
            var thisEmail = $("#companyEmail").val();
            var thisPhone = $("#companyPhone").val();
            var thisVoucherNo = master.InvoiceNo;

            //get the value selected from options
            /*var domNode = document.getElementById("bankorcash");
            var value = domNode.selectedIndex;
            var thisAccount = domNode.options[value].text;*/

            var thisCustomer = master.CustomerName;
            var thisDate = Utilities.FormatJsonDate(master.Date);
            
            //$("#raisedByDiv").html(master.userName);
            console.log(details);
            
            var objToPrint = [];
            $.each(details, function (count, row) {
                objToPrint.push({
                    "SlNo": count + 1,
                    "Barcode": row.barcode,
                    "ProductCode": row.productcode,
                    "Product": row.product,
                    "WareHouse": (row.extra1 == "" ? "N/A" : row.extra1) ,
                    "Quantity": row.qty,
                    "Unit": row.unit,
                    "Rate": 'N'+Utilities.FormatCurrency(row.rate),
                    "Amount": Utilities.FormatCurrency(row.amount),
                    "Tax": 'N' + Utilities.FormatCurrency(row.taxAmount),
                });
            });

            columns = [
                { title: "S/N", dataKey: "SlNo" },
                { title: "Barcode", dataKey: "Barcode" },
                { title: "Product Code", dataKey: "ProductCode" },
                { title: "Product", dataKey: "Product" },
                { title: "WareHouse", dataKey: "WareHouse" },
                { title: "Quantity", dataKey: "Quantity" },
                { title: "Unit", dataKey: "Unit" },
                { title: "Rate", dataKey: "Rate" },
                { title: "Amount", dataKey: "Amount" },
                { title: "Tax", dataKey: "Tax" },
            ];

            var doc = new jsPDF('portrait', 'pt', 'letter');
            doc.setDrawColor(0);

            //start drawing
            doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);


            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(companyName, 320, 80,'center');
            doc.setFontSize(9);
            doc.setFontType("normal");
            doc.text(companyAddress, 320, 90, 'center');

            doc.setFontSize(9);
            doc.setFontType("bold");
            doc.text('Email Id: ', 400, 150);
            doc.setFontType("normal");
            doc.text(thisEmail, 460, 150);
            doc.setFontType("bold");
            doc.text('Phone: ', 400, 165);
            doc.setFontType("normal");
            doc.text(thisPhone, 460, 165);

            doc.line(40, 170, 570, 170);

            doc.setFontType("bold");
            doc.setFontSize(9);
            doc.text('Customer:', 40, 185);
            doc.setFontType("normal");
            doc.text(thisCustomer, 85, 185);

            doc.setFontType("bold");
            doc.text('Order No: ', 400, 185);
            doc.setFontType("normal");
            doc.text(thisVoucherNo, 460, 185);
            doc.setFontType("bold");
            doc.text('Date: ', 400, 205);
            doc.setFontType("normal");
            doc.text(thisDate, 460, 205);

            doc.line(40, 220, 570, 220);

            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text('Sales Order', 250, 240);

            //doc.line(120, 20, 120, 60); horizontal line
            doc.setFontSize(8);
            doc.autoTable(columns, objToPrint, {
                startY: 255,
                theme: 'striped',
                styles: {
                    fontSize: 8,
                },
                columnStyles: {
                    SlNo: { columnWidth: 30, },
                    Amount: { columnWidth: 80, halign: 'right' },
                    Memo: { columnWidth: 100 },
                    PayTo: { columnWidth: 110 },
                    Currency: { columnWidth: 100 }

                },
            });

            doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

            doc.setFontSize(9);
            doc.setFontType("bold"); //master.TotalAmount + master.TaxAmount
            doc.text('Order Amount: ' + Utilities.FormatCurrency(parseInt(master.TotalAmount)), 555, doc.autoTable.previous.finalY + 20, "right");
            doc.text('Tax Amount: ' + Utilities.FormatCurrency(parseInt(master.TaxAmount)), 555, doc.autoTable.previous.finalY + 35, "right");
            doc.text('Grand Total: ' + Utilities.FormatCurrency(parseInt(master.TotalAmount + master.TaxAmount)), 555, doc.autoTable.previous.finalY + 50, "right");

            doc.line(40, doc.autoTable.previous.finalY + 65, 570, doc.autoTable.previous.finalY + 65);

            doc.setFontType("bold");
            doc.text('Grand Total in Words: ', 40, doc.autoTable.previous.finalY + 80);
            doc.setFontType("normal");

            //remove commas
            //var tempTotal = totalAmt.replace(/,/g, "");
            //convert to number
            var amtToBeConverted = parseInt(master.TotalAmount + master.TaxAmount);

            //capitalize Each Word
            var wordFormat = Utilities.NumberToWords(amtToBeConverted);
            const words = wordFormat.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());

            doc.text(words + ' Naira ONLY.', 140, doc.autoTable.previous.finalY + 80);

            doc.setFontType("bold");
            doc.text('Payment approved by:  _________________________________', 40, doc.autoTable.previous.finalY + 120);

            doc.text('Done by:  ' + doneBy, 350, doc.autoTable.previous.finalY + 120);
            doc.text('_________________', 390, doc.autoTable.previous.finalY + 123);
            doc.autoPrint();
            doc.save('Sales_Order.pdf');
            //window.open(doc.output('bloburl'));
            window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function editOrder() {
    window.location.href = "/Customer/SalesOrder/SalesOrderRegisterDetails/" + currentViewedOrderMasterId;
}

function MakeDeleteOrder(id) {
    $.ajax(API_BASE_URL + "/SalesOrder/MakeDeleteSalesOrderDetails/" + id,
        {
            type: "get",
            beforeSend: () => {
                Utilities.Loader.Show();
            },
            complete: () => {
                Utilities.Loader.Hide();
            },
            success: (data) => {
                console.log(data); 
                if (data.status == 200) {
                    Utilities.Alert(data.message);
                    Utilities.SuccessNotification(data.message); 
                  location.reload();

                } else {
                    Utilities.Alert(data.message);
                }
            }
        });
} 

function deleteOrder()
{    //var resp = false; 
    $("#detailsModal").modal('hide');
    Swal.fire({
        title: 'Warning',
        text: "Are you sure you want to delete this record",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, I am Sure'
    }).then((result) => { 
        if (result.value) {
            MakeDeleteOrder(currentViewedOrderMasterId);
        }       
    });
   
    //if (confirm('Delete this Ledger?')) {
    //    Utilities.Loader.Show();
    //    $.ajax({
    //        url: API_BASE_URL + "/SalesQuotation/DeleteSalesQuotationDetails?id=" + orderId,
    //        type: "GET",
    //        contentType: "application/json",
    //        success: function (data) {
    //            console.log(data);
    //            if (data > 0) {
    //                Utilities.SuccessNotification("Order Deleted successfully");
    //                $("#detailsModal").modal("hide");
    //                window.location = "/Customer/SalesQuotation/SalesQuotationRegister"
    //            }
    //            else {
    //                Utilities.ErrorNotification("Oops! Sorry, couldn't delete");
    //            }
    //            Utilities.Loader.Hide();
    //        },
    //        error: function (err) {
    //            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
    //            Utilities.Loader.Hide();
    //        }
    //    });
    //}
}