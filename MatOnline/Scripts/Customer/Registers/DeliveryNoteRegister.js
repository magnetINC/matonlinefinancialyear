﻿var table = "";
var deliveryNotes = [];
var users = [];
var ledgers = [];
var thisDeliveryNote = {};
var stores = [];



var lookUpAjax = $.ajax({
    url: API_BASE_URL + "/SalesInvoice/InvoiceLookUps",
    type: "GET",
    contentType: "application/json",
});

var lookUpAjaxUsers = $.ajax({
    url: API_BASE_URL + "/UserCreation/GetAllUses",
    type: "Get",
    contentType: "application/json",
});

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}


$(function () {
    //Utilities.Loader.Show();
    $.when(lookUpAjax)
        .done(function (dataLookUp) {
            //console.log(dataLookUp);
            stores = dataLookUp.Stores;
        });

    $.when(lookUpAjaxUsers)
        .done(function (data) {
            allUsers = data.Users;
        });
});

$(function () {
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
});

function getDeliveryNoteList(fromDate, toDate) {
    deliveyNoteSearchParameters = {
        FromDate: fromDate,
        ToDate: toDate,
        Status: ""
    };

    console.log(deliveyNoteSearchParameters);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/DeliveryNote/GetDeliveryNotesForRegister",
        type: "Post",
        data: JSON.stringify(deliveyNoteSearchParameters),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 500, data);

           
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function onSuccess(data) {
    deliveryNotes = data.deliveryNotes;
    users = data.userss
    ledgers = data.ledgers;

    var output = "";
    var objToShow = [];
    $.each(deliveryNotes, function (count, row) {
        var statusOP = "";
        if (row.Status == "Unprocessed") {
            statusOP = '<span class="label label-warning">Unprocessed</span>';
        }
        else if (row.Status == "Processed") {
            statusOP = '<span class="label label-mint">Processed</span>';
        }
        var thisUser = allUsers.find(x => x.userId == row.UserId);
        var thisUserName = thisUser == undefined ? 'admin' : thisUser.userName;

        objToShow.push([
        count + 1,
        row.OrderNo,
        row.Date,
        row.LedgerName,
        Utilities.FormatCurrency(row.Amount),
        thisUserName,
        //statusOP,
        '<button onclick="getDeliveryNoteDetails(' + row.DeliverynoteMasterId + ')" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> View</button>',
        '<button onclick="printDeliveryNoteDetails(' + row.DeliverynoteMasterId + ')" class="btn btn-primary btn-xs"><i class="fa fa-print"></i> Print</button>'
        ]);
    });
    if (checkPriviledge("frmDeliveryNote", "View") === false) {
        objToShow.forEach(function (item) {
            item.splice(5, 1);
        });
    }
    table = $('#deliveryNoteListTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    Utilities.Loader.Hide();
    //$("#deliveryNoteListTbody").html(output);
}

//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;

//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}

function printDeliveryNoteDetails(id) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/DeliveryNote/deliveryNoteDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            var thisLineItems = data;
            GetThisDeliveryNote(id);
            thisDeliveryNote;

            var companyName = $("#companyName").val();
            var companyAddress = $("#companyAddress").val();
            var thisEmail = $("#companyEmail").val();
            var thisPhone = $("#companyPhone").val();
            var thisVoucherNo = thisDeliveryNote.OrderNo;

            var thisCustomer = thisDeliveryNote.LedgerName;
            var thisDate = thisDeliveryNote.Date;
            var Narration = "";
            
            var objToPrint = [];
            $.each(thisLineItems, function (count, row) {
                var thisStore = row.godownId == null ? "N/A" : findStore(row.godownId).godownName;
                objToPrint.push({
                    "SL": row.slNo,
                    "ProductBarcode": row.barcode,
                    "ProductCode": row.productCode,
                    "ProductName": row.productName,
                    "Store": thisStore,
                    "Description": row.itemDescription,
                    "Quantity": row.qty,
                    "Unit": (row.unitName == undefined || "" ? "NA" : row.unitName),
                });
            });

            var columns = [
                { title: "SL", dataKey: "SL" },
                { title: "Barcode", dataKey: "ProductBarcode" },
                { title: "Product Code", dataKey: "ProductCode" },
                { title: "Product Name", dataKey: "ProductName" },
                { title: "Store", dataKey: "Store" },
                { title: "Description", dataKey: "Description" },
                { title: "Qty Delivered", dataKey: "Quantity" },
                { title: "Unit", dataKey: "Unit" },
            ];

            var doc = new jsPDF('portrait', 'pt', 'letter');
            doc.setDrawColor(0);

            //start drawing
            doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);

            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(companyName, 320, 80, 'center');
            doc.setFontSize(9);
            doc.setFontType("normal");
            doc.text(companyAddress, 320, 90, 'center');

            doc.setFontSize(9);
            doc.setFontType("bold");
            doc.text('Email Id: ', 400, 150);
            doc.setFontType("normal");
            doc.text(thisEmail, 460, 150);
            doc.setFontType("bold");
            doc.text('Phone: ', 400, 165);
            doc.setFontType("normal");
            doc.text(thisPhone, 460, 165);

            doc.line(40, 170, 570, 170);

            doc.setFontType("bold");
            doc.setFontSize(9);
            doc.text('Customer:', 40, 185);
            doc.setFontType("normal");
            doc.text(thisCustomer, 85, 185);
            doc.setFontType("bold");
            doc.text('Narration:', 40, 205);
            doc.setFontType("normal");
            doc.text(Narration, 85, 205);

            doc.setFontType("bold");
            doc.text('Order No: ', 400, 185);
            doc.setFontType("normal");
            doc.text(thisVoucherNo.toString(), 460, 185);
            doc.setFontType("bold");
            doc.text('Order Date: ', 400, 205);
            doc.setFontType("normal");
            doc.text(thisDate, 460, 205);

            doc.line(40, 220, 570, 220);

            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text('Delivery Note', 250, 240);

            //doc.line(120, 20, 120, 60); horizontal line
            //doc.setFontSize(8);
            doc.autoTable(columns, objToPrint, {
                startY: 255,
                theme: 'striped',
                styles: { "overflow": "linebreak", "cellWidth": "wrap", "rowPageBreak": "avoid", "fontSize": "7", "lineColor": "100", "lineWidth": ".25"},
                //halign: 'right'
                columnStyles: {
                    ProductBarcode: { columnWidth: 50 },
                    Quantity: { columnWidth: 50, halign: 'center' },
                    
                }
            });

            doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

            doc.setFontType("bold");
            doc.text('Authorized by:  _________________', 40, doc.autoTable.previous.finalY + 30);

            doc.text('Done by: ' + thisDeliveryNote.User , 350, doc.autoTable.previous.finalY + 30);
            doc.text('_________________', 390, doc.autoTable.previous.finalY + 33);

            doc.autoPrint();
            doc.save('DeliveryNote' + thisDeliveryNote.OrderNo + '.pdf');
            //window.open(doc.output('bloburl'));
            window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
            Utilities.Loader.Hide();
        }
    })
}

function GetThisDeliveryNote(id)
{
    for (i = 0; i < deliveryNotes.length; i++)
    {
        if (deliveryNotes[i].DeliverynoteMasterId == id)
        {
            thisDeliveryNote = deliveryNotes[i];
        }
    }
}

function getDeliveryNoteDetails(id)
{
    Utilities.Loader.Show();
    GetThisDeliveryNote(id);
    console.log(stores);
    $.ajax({
        url: API_BASE_URL + "/DeliveryNote/deliveryNoteDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            //master = data.Master;
            //details = data.Details;
            var deliverId = data.length > 0 ? data[0].deliveryNoteMasterId : id;
            var statusOutput = "";
            if (thisDeliveryNote.Status == "Unprocessed") {
                statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Unprocessed</span>';
            }
            else if (thisDeliveryNote.Status == "Processed") {
                statusOutput = '<span class="label label-mint"><i class="fa fa-check"></i> Processed</span>';
            }
            $("#orderNoDiv").html(thisDeliveryNote.OrderNo);
            $("#orderDateDiv").html(thisDeliveryNote.Date);
            $("#raisedByDiv").html(thisDeliveryNote.User);
            $("#raisedForDiv").html(thisDeliveryNote.LedgerName);
            $("#statusDiv").html(statusOutput);
            $("#amountTxt").val('N' + Utilities.FormatCurrency(thisDeliveryNote.Amount));
            $("#edit").attr("data-id", deliverId);
            $("#delete").attr("data-id", deliverId);
            var output = "";
            $.each(data, function (count, row) {
                var thisStore = row.godownId == null ? "N/A" : findStore(row.godownId).godownName;
                    //< td > ' + (row.extra1 == "" ? "NA" : row.extra1) + '</td>\
                output += '<tr>\
                            <td>' + (count + 1) + '</td>\
                            <td><button onclick="getStoreQuantity(' + row.productId + ',\'' + row.extra1 + '\')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></button></td>\
                            <td>' + row.barcode + '</td>\
                            <td>' + row.productCode + '</td>\
                            <td>' + row.productName + '</td>\
                            <td>' + thisStore + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>' + (row.unitName == undefined || "" ? "NA" : row.unitName) + '</td>\
                            <td style="text-align:right">' + Utilities.FormatCurrency(row.rate) + '</td>\
                            <td style="text-align:right">' + Utilities.FormatCurrency(row.amount) + '</td>\
                        </tr>';
            });
            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");

            thisDeliveryNote = {};
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

$(function() {
    //delete edit 
    $("#edit").click(function() {
        var id = $(this).attr("data-id");
        location.href = "/Customer/DeliveryNote/EditDeliveryNote/" + id;
    });

    //delete

    $("#delete").click(function() {
        var id = $(this).attr("data-id");
        Utilities.ConfirmFancy("Are Sure you want to delete? you cant reverse this",
            function() {
                $.ajax(API_BASE_URL + "/DeliveryNote/DeleteDeliveryNote/" + id,
                    {
                        type: "delete",
                        success: function(res) {
                            if (res.status == 200) {
                                Utilities.SuccessNotification(res.message);
                                setTimeout(function() {
                                        location.href = "/Customer/DeliveryNote/DeliveryNoteRegister";
                                    },
                                    3000);
                            }
                        }
                    });
            });

        //send a delete request
        //location.href = "/Customer/DeliveryNote/EditDeliveryNote/" + id;
    });
})