﻿var saleQuotationSearchParameters = {};
var table = "";
var cust = [];
var users = [];
var orderId = 0;

$(function () {
    $("#deleteSalesOrder").hide();
    loadLookUps();
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
});

function loadLookUps() {
    Utilities.Loader.Show();
    var lookupsAjax = $.ajax({
        url: API_BASE_URL + "/Register/LookUps",
        type: "GET",
        contentType: "application/json",
    });
    $.when(lookupsAjax)
    .done(function (lookups) {
        console.log(lookups);
        cust = lookups.cust;
        users = lookups.users;

        getSalesQuotationsList("2017-01-01", $("#toDate").val(), "");
    });
    //$.ajax({
    //    url: API_BASE_URL + "/Register/LookUps",
    //    type: "GET",
    //    contentType: "application/json",
    //    success: function (data) {
    //        console.log(data);
    //        users = data.users;
    //        cust = data.cust;
    //    },
    //    error: function (err) {
    //        Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
    //        Utilities.Loader.Hide();
    //    }
    //});
}


function getSalesQuotationsList(fromDate, toDate, condition) {
    Utilities.Loader.Show();
    saleQuotationSearchParameters = {
        fromDate: fromDate,
        toDate: toDate,
        ledgerId: 0,
        gmUserId: matUserInfo.UserId,
        qoutationNo: "",
        condition: condition
    };
    
    $.ajax({
        url: API_BASE_URL + "/Register/salesQuotation",
        type: "Post",
        data: JSON.stringify(saleQuotationSearchParameters),
        contentType: "application/json",
        success: function (data) {
            console.log("salesQuotation", data);
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors.
            setTimeout(onSuccess, 500, data);

           
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function onSuccess(data) {
    orderListForSearch = data;
    var output = "";
    var objToShow = [];
    $.each(data, function (count, row) {
        var statusOP = "";
        var agentName = "";
        var userName = "";
        var generateButton = `<a href="/Customer/SalesInvoice/Index?ledgerId=${row.ledgerId}"
                            class="btn btn-success btn-xs"
                            onclick="getQuotationTransaction(' + row.quotationMasterId + ')">
                            <i class="fa fa-upload"></i> Generate Invoice</a>`;


        if (row.approved == "Pending") {
            statusOP = '<span class="label label-warning">Pending</span>';
            $("#deleteSalesOrder").show();
        }
        else if (row.approved == "Cancelled") {
            statusOP = '<span class="label label-danger">Cancelled</span>';
        }
        else if (row.approved == "Processed") {
            statusOP = '<span class="label label-mint"> Processed</span>';
        }
        else {
            statusOP = '<span class="label label-success"> Approved</span>';
        }
        if (findSuppliers(row.ledgerId).ledgerName == undefined ||
            findSuppliers(row.ledgerId).ledgerName == "" ||
            findSuppliers(row.ledgerId).ledgerName == null) {
            agentName = "NA";
        }
        else {
            agentName = findSuppliers(row.ledgerId).ledgerName;
        }
        if (findUser(row.userId).userName == undefined ||
        findUser(row.userId).userName == "" ||
        findUser(row.userId).userName == null) {
            userName = "NA";
        }
        else {
            userName = findUser(row.userId).userName;
        }
        objToShow.push([
            count + 1,
            row.invoiceNo,
            Utilities.FormatJsonDate(row.date),
            agentName,
            '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
            userName,
            statusOP,
            `  <div class="dropdown">
                        <button class="btn btn-success btn-xs btn-active-purple dropdown-toggle" data-toggle="dropdown" type="button">
                            Action <i class="dropdown-caret"></i>
                        </button>
                         <ul class="dropdown-menu">
                            <li>${generateButton}</li>
                            <li><button type="button" class="btn btn-primary btn-xs" onclick="getQuotationTransaction(' + row.quotationMasterId + ')"><i class="fa fa-eye"></i> View</button> </li>

                        </ul>
                    </div>`


        ]);
    });
    //if (checkPriviledge("frmSalesOrder", "View") === false) {
    //    objToShow.forEach(function (item) {
    //        item.splice(7, 1);
    //    });
    //}
    //if (checkPriviledge("frmSalesOrder", "Update") === false) {
    //    $('.editOrder').hide();
    //}
    //if (checkPriviledge("frmSalesOrder", "Delete") === false) {
    //    $('.deleteOrder').hide();
    //}
    table = $('#salesQuotationListTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    Utilities.Loader.Hide();
}

function getQuotationTransaction(quotationMasterId) {
    orderId = quotationMasterId
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetSalesQuotationForConfirmation?salesQuotationMasterId=" + quotationMasterId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            master = data.Master;
            details = data.Details;
            var statusOutput = "";
            if (master.Approved == "Pending") {
                $("#deleteSalesOrder").show();
                $("#editSalesOrder").show();
                statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending</span>';
            }
            else if (master.Approved == "Approved") {
                statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Approved</span>';
                $("#deleteSalesOrder").hide();
                $("#editSalesOrder").hide();
            }
            else if (master.Approved == "Cancelled") {
                statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
                $("#deleteSalesOrder").hide();
                $("#editSalesOrder").hide();
            }
            $("#orderNoDiv").html(master.InvoiceNo);
            $("#orderDateDiv").html(Utilities.FormatJsonDate(master.Date));
            $("#orderNoDiv").html(master.InvoiceNo);
            $("#raisedByDiv").html(master.User);
            $("#raisedForDiv").html(master.CustomerName);
            $("#statusDiv").html(statusOutput);
            $("#orderAmountTxt").val(Utilities.FormatCurrency(master.TotalAmount));
            $("#taxAmountTxt").val(Utilities.FormatCurrency(master.TaxAmount));
            $("#grandTotalTxt").val(Utilities.FormatCurrency(master.TotalAmount + master.TaxAmount));
            var output = "";
            for (i = 0; i < details.length; i++) {
                output += '<tr>\
                            <td width="20">'+ (i + 1) + '</td>\
                            <td width="20"><button onclick="getStoreQuantityByStoreId(' + details[i].productId + ',\'' + details[i].extra1 + '\')" class="btn btn-xs btn-primary"><i class="fa fa-info"></i></button></td>\
                            <td>' + details[i].barcode + '</td>\
                            <td>' + details[i].productcode + '</td>\
                            <td>' + details[i].product + '</td>\
                            <td>' + details[i].storeName + '</td>\
                            <td>' + details[i].qty + '</td>\
                            <td>' + details[i].unit + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].amount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].taxAmount) + '</td>\
                        </tr>';
            }
            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function deleteOrder()
{
    if (confirm('Delete this Ledger?')) {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/SalesQuotation/DeleteSalesQuotationDetails?id=" + orderId,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data > 0) {
                    Utilities.SuccessNotification("Order Deleted successfully");
                    $("#detailsModal").modal("hide");
                    window.location = "/Customer/SalesQuotation/SalesQuotationRegister"
                }
                else {
                    Utilities.ErrorNotification("Oops! Sorry, couldn't delete");
                }
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                Utilities.Loader.Hide();
            }
        });
    }
}

function EditOrder() {
    window.location = "/Customer/SalesQuotation/SalesQuotationRegisterDetails/" + orderId;
}

function findSuppliers(id) {
    var output = {};
    for (i = 0; i < cust.length; i++) {
        if (cust[i].ledgerId == id) {
            output = cust[i];
            break;
        }
    }
    return output;
}

function findUser(id) {
    var output = {};
    for (i = 0; i < users.length; i++) {
        if (users[i].userId == id) {
            output = users[i];
            break;
        }
    }
    return output;
}