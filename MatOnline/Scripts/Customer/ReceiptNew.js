﻿var receiptToSave = [];
var bankOrCash = [];
var customers = [];
var selectedCustomer = 0;
var customerInvoices = [];
var unprocessedInvoicePayments = [];
var partyBalances = [];
var receiptDetails = [];

function autoFill() {
    //get page varables 
    var ledgerId = $("#ledgerId").val();
    var ledgerName = $("#ledgerName").val();
    var voucherId = $("#voucherId").val();
    var amount = $("#txtAmount").val();
    var txtId = $("#txtId").val();
    //check if transaction is not null, which means it is a route 
    if (txtId != null || txtId != undefined) {
        $("#agentList").val(ledgerId);
        $("#amount").val(amount);
        $("#totalInvoiceAmt").val(amount);
        //openModal();
    }
}

//
$("#agentList").change(function () {
    autoApplyCheck();
})

$(function () {
    Utilities.Loader.Show();
    getLookups();
    renderDataToControls();
    $("#partyBalanceTable").hide();


    $('#partyBalanceTbody').on('change', '.invAmt', function () {
        var indexOfObject = unprocessedInvoicePayments.findIndex(p => p.InvoiceNo == $(this).attr("id"));
        if (indexOfObject >= 0) //object exists, just modify
        {
            unprocessedInvoicePayments[indexOfObject].Amount = $(this).val();
        }
        else    //object does not exist, insert new record
        {
            unprocessedInvoicePayments.push({
                InvoiceNo: $(this).attr("id"),
                Amount: $(this).val()
            });
        }
        var amt = 0.0;
        for (i = 0; i < unprocessedInvoicePayments.length; i++) {
            var chk = unprocessedInvoicePayments[i].Amount == undefined ? 0 : parseFloat(unprocessedInvoicePayments[i].Amount)
            amt = amt + chk;
        }

        $("#amount").val(amt);
        $("#totalInvoiceAmt").val(amt);
        //console.log(customersRow);
    });
    autoFill();
});

function getTotalInvoiceAmount() {
    var amt = 0.0;
    for (i = 0; i < unprocessedInvoicePayments.length; i++) {
        var chk = unprocessedInvoicePayments[i].Amount == undefined ? 0 : parseFloat(unprocessedInvoicePayments[i].Amount)
        amt = amt + chk;
    }
    return amt;
}

function changeAmount(selectObject) {
    var newAmount = parseFloat(selectObject.value);
    $("#totalInvoiceAmt").val(Utilities.FormatCurrency(newAmount));
}


//to load all look up data

//to render controls
function renderDataToControls()
{

}

function autoApplyCheck()
{
    
    var val = $("#apply").val();

    if (val == "Against")
    {
        Utilities.Loader.Show();

        $.ajax({
            url: API_BASE_URL + "/Receipt/GetCustomerInvoices?ledgerId=" + $("#agentList").val() + "&voucherNo=" + $("#voucherNo").val(),
            type: "GET",
            contentType: "application/json",
            success: function (data)
            {
                debugger;
                document.getElementById("amount").disabled = true;
                customerInvoices = data;
                renderCustomerInvoices();
                Utilities.Loader.Hide();
            },
            error: function (err)
            {
                Utilities.Loader.Hide();
            }
        });
    }
    else if (val == "On Account")
    {
        document.getElementById("amount").disabled = false;
        $("#partyBalanceTable").hide();
    }
    else
    {
        $("#partyBalanceTable").hide();
    }
}

//to load invoices
function applyCheck(selectObject)
{

    if ($("#agentList").val() == "")
    {
        Utilities.ErrorNotification("Select Customer First");
        return;
    }

    
    var val = selectObject.value;
    if (val == "Against")
    {
        Utilities.Loader.Show();

        $.ajax({
            url: API_BASE_URL + "/Receipt/GetCustomerInvoices?ledgerId=" + $("#agentList").val() + "&voucherNo=" + $("#voucherNo").val(),
            type: "GET",
            contentType: "application/json",
            success: function (data)
            {
                document.getElementById("amount").disabled = true;
                customerInvoices = data;
                renderCustomerInvoices();
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
    else if (val == "On Account") {
        document.getElementById("amount").disabled = false;
        $("#partyBalanceTable").hide();
    }
}
//to render invoices to table
function renderCustomerInvoices()
{
    console.log(customerInvoices);
    var output = "";
    for (i = 0; i < customerInvoices.length; i++) {
        var balance = customerInvoices[i].balance;
        var textField = '<input class="form-control invAmt" id="' + customerInvoices[i].invoiceNo + '" type="text"/>';

        if (receiptDetails.length > 0) {
            $.each(receiptDetails, function (index, objects) {
                if (customerInvoices[i].invoiceNo == objects.InvoiceNo) {
                    balance = customerInvoices[i].balance - objects.Amount;
                    textField = '<input class="form-control" type="text" disabled/>';
                }
            });
        }

        output +=
            '<tr>\
            <td>'+ (i + 1) + '</td>\
            <td>'+ customerInvoices[i].display + '</td>\
            <td>'+ customerInvoices[i].invoiceNo + '</td>\
            <td>&#8358;' + Utilities.FormatCurrency(balance) + '</td>\
            <td>'+ textField + '</td>\
            <td>Cr</td>\
        </tr>';
    }
    $("#partyBalanceTable").show();
    $("#partyBalanceTbody").html(output);

}

function openModal()
{

    $("#receiptModal").modal("show");
    $('#receiptModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
    autoApplyCheck();
}

$('#partyBalanceTbody').on('change', '.invAmt', function ()
{
    for (var j = 0; j < customerInvoices.length; j++)
    {
        if ($(this).attr('id') == customerInvoices[j].invoiceNo)
        {
            if ($(this).val() > customerInvoices[j].balance)
            {
                Utilities.ErrorNotification('Entry is more than the balance');
                //return;
                $(this).css("border", "1px solid red");
            }
            else {
                $(this).css("border", "none");
            }
        }
    }
});

function addReceiptToObject()
{
    var arrayOfInvoiceNos = [];
    var inputs = $(".invAmt");
    if ($("#agentList").val() == "") {
        $("span[aria-owns='agentList_listbox'] span.k-state-default").css("border", "1px solid red");
        return;
    }

    else {
        $("span[aria-owns='agentList_listbox'] span.k-state-default").css("border", "none");
        if ($("#apply").val() == "Against") {

            //make sure user enters only one customer
            if (receiptDetails.length > 0) {
                if (receiptDetails[0].LedgerId != $("#agentList").val()) {
                    Utilities.ErrorNotification("Cannot add a different Customer");
                    return;
                }
            }

            //user must not enter value more than the balance
            for (var i = 0; i < inputs.length; i++) {
                for (var j = 0; j < customerInvoices.length; j++) {
                    if ($(inputs[i]).attr('id') == customerInvoices[j].invoiceNo) {
                        if ($(inputs[i]).val() > customerInvoices[j].balance) {
                            Utilities.ErrorNotification('Excess input than balance on Invoice number: ' + customerInvoices[j].invoiceNo);
                            return;
                        }
                    }
                }

            }

            //populate the entered amounts into a new array
            for (var i = 0; i < inputs.length; i++) {
                if ($(inputs[i]).val() != "") {
                    arrayOfInvoiceNos.push({
                        InvoiceNo: $(inputs[i]).attr('id'),
                        Amount: $(inputs[i]).val()
                    });

                }

            }

            //find details of this value from the main invoices
            for (i = 0; i < arrayOfInvoiceNos.length; i++) {
                for (j = 0; j < customerInvoices.length; j++) {
                    if (arrayOfInvoiceNos[i].InvoiceNo == customerInvoices[j].invoiceNo) {
                        partyBalances.push({
                            Date: Utilities.YyMmDdDate(),
                            Credit: arrayOfInvoiceNos[i].Amount,
                            LedgerId: $("#agentList").val(),
                            ReferenceType: "Against",
                            AgainstInvoiceNo: customerInvoices[j].invoiceNo,
                            AgainstVoucherNo: customerInvoices[j].voucherNo,
                            VoucherTypeId: customerInvoices[j].voucherTypeId,
                            VoucherNo: $("#voucherNo").val(),
                            InvoiceNo: $("#voucherNo").val()
                        });
                    }
                }

            }

            $.each(arrayOfInvoiceNos, function (index, row) {
                receiptDetails.push({
                    ChequeDate: $("#effectiveDate").val(),
                    //EffectiveDate: $("#effectiveDate").val(),
                    LedgerId: $("#agentList").val(), //this is where to get the customer 
                    Amount: row.Amount,
                    InvoiceNo: row.InvoiceNo,
                    ExchangeRateId: $("#currency").val(),
                    grossAmount: 0,
                    ApplyType: "Against",
                    //ChequeOrRefNo: $("#chequeRefNo").val(),
                    ChequeNo: $("#chequeRefNo").val()
                });

            })
            $("#totalReceiptDetailsAmt").val(getTotalReceiptDetailsAmount());
        }

        else if ($("#apply").val() == "On Account") {

            if (receiptDetails.length > 0) {
                if (receiptDetails[0].LedgerId != $("#agentList").val()) {
                    Utilities.ErrorNotification("Cannot add a different Customer");
                    return;
                }
            }

            partyBalances.push({
                Date: Utilities.YyMmDdDate(),
                Credit: $("#amount").val(),
                LedgerId: $("#agentList").val(),
                //Currency: $("#currency").val(),
                ExchangeRateId: $("#currency").val(),
                ReferenceType: "OnAccount",
                VoucherNo: $("#voucherNo").val(),
                InvoiceNo: $("#voucherNo").val()
            });
            //always empty array to populate with updated data
            receiptDetails.push({
                ChequeDate: $("#effectiveDate").val(),
                LedgerId: $("#agentList").val(),
                Amount: $("#amount").val(),
                ExchangeRateId: $("#currency").val(),
                InvoiceNo: '-',
                grossAmount: 0,
                ApplyType: "On Account",
                ChequeNo: $("#chequeRefNo").val()
            });
            //console.log(receiptDetails);
            $("#totalReceiptDetailsAmt").val(getTotalReceiptDetailsAmount());

            $("#applyOnAccountAmount").val(0);

        }
        renderReceipts();
    }

}

function renderReceipts() {
    var output = "";
    $.each(receiptDetails, function (count, row) {
        var newAmount = isNaN(parseFloat(row.Amount)) ? 0 : parseFloat(row.Amount);
        output += '<tr>\
                    <td>'+ (count + 1) + '</td>\
                    <td>' + findCustomer(row.LedgerId).ledgerName + '</td>\
                    <td> ' + row.ApplyType + '</td>\
                    <td> ' + row.InvoiceNo + '</td>\
                    <td>Naira | NGN </td>\
                    <td>&#8358;' + Utilities.FormatCurrency(newAmount) + '</td>\
                    <td>' + (row.ChequeNo == undefined ? "" : row.ChequeNo) + '</td>\
                    <td>' + (row.ChequeDate == undefined ? "" : row.ChequeDate) + '</td>\
                    <td><button class="btn btn-sm btn-danger" onclick="removePayment(' + row.LedgerId + ')"><i class="fa fa-times"></i></button></td>\
                   </tr>';
    });
    $("#customersRowTbody").html(output);
    $("#totalReceiptDetailsAmt").val(Utilities.FormatCurrency(getTotalReceiptDetailsAmount()));

    $("#amount").val(0);
    $("#chequeRefNo").val("");
    $("#totalInvoiceAmt").val(0);
    $("#partyBalanceTable").hide();
    $("#totalReceiptDetailsAmt").val();
    autoApplyCheck();
}

function getLookups() {
   
    $.ajax({
        url: API_BASE_URL + "/Receipt/GetLookups",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            bankOrCash = data.BankOrCash;
            customers = data.Customers;
            console.log(customers);

            //render data to controls
            //$("#bankOrCash").html(Utilities.PopulateDropDownFromArray(bankOrCash, 1, 0));
            $("#voucherNo").val(data.VoucherNo);
            $("#agentList").kendoDropDownList({
                filter: "contains",
                optionLabel: "Please Select...",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: customers
            });
            $("#bankOrCash").kendoDropDownList({
                filter: "contains",
                optionLabel: "Please Select...",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: bankOrCash
            });

            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function savePayment() {
    if ($("#voucherNo").val() == "") {
        Utilities.ErrorNotification("Voucher number required!");
        return;
    }
    else if ($("#date").val() == "") {
        Utilities.ErrorNotification("Payment Date required!");
        return;
    }
    else if ($("#bankOrCash").val() == "" || $("#bankOrCash").val() < 1) {
        Utilities.ErrorNotification("please Select a bank");
        return;
    }
    else {
        var receiptInfo = {
            VoucherNo: $("#voucherNo").val(),
            InvoiceNo: $("#voucherNo").val(),
            LedgerId: $("#bankOrCash").val(),
            Narration: $("#narration").val(),
            UserId: 1,
            //UserId: $("#agentList").val(),
            Date: $("#date").val(),
            DoneBy: $("#UserId").html(),
            TotalAmount: $("#totalReceiptDetailsAmt").val()
        };

        var toSave = {
            totalPaymentAmount: 0,
            ReceiptMasterInfo: receiptInfo,
            ReceiptDetailsInfo: receiptDetails,
            PartyBalanceInfo: partyBalances
        };
        

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/Receipt/SavePayment",
            data: JSON.stringify(toSave),
            type: "POST",
            contentType: "application/json",
            success: function (data) {
                if (data > 0) {
                    Utilities.SuccessNotification("Payment saved!");
                    Utilities.Loader.Hide();
                    $("#voucherNo").val(data);
                    window.location = "/Customer/Receipt/receipt"
                }
                else {
                    Utilities.ErrorNotification("Oops! Payment not saved!");
                    Utilities.Loader.Hide();
                }
            },
            error: function (err) {
                Utilities.ErrorNotification("Oops! Something went wrong!");
                Utilities.Loader.Hide();
            }
        });
    }

}

function getTotalReceiptDetailsAmount() {
    var totalReceiptDetailsAmt = 0.0;
    $.each(receiptDetails, function (count, row) {
        var chk = row.Amount == undefined ? 0 : row.Amount;
        totalReceiptDetailsAmt = totalReceiptDetailsAmt + parseFloat(chk);
    });
    return totalReceiptDetailsAmt;
}

function clearForm() {
    receiptDetails = [];
    partyBalances = [];
    customerInvoices = [];
    $("select#bankOrCash")[0];
    $("#narration").val("");
}

function removePayment(ledgerId) {
    //console.log("i C U!!");
    //party balance,customer rows,receiptDetails
    if (confirm("Remove this Line Item?")) {
        var ind2 = receiptDetails.findIndex(p => p.LedgerId == ledgerId);
        receiptDetails.splice(ind2, 1);
        var ind3 = partyBalances.findIndex(p => p.LedgerId == ledgerId);
        partyBalances.splice(ind3, 1);

        renderReceipts()
    }
}

function findCustomer(ledgerId) {
    var output = {};
    for (i = 0; i < customers.length; i++) {
        if (customers[i].ledgerId == ledgerId) {
            output = customers[i];
            break;
        }
    }
    return output;
}