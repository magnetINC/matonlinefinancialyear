﻿var allProducts = [];
var salesInvoiceLineItems = [];
var searchResult = {};
var customers = [];
var salesMen = [];
var pricingLevels = [];
var currencies = [];
var units = [];
var batches = [];
var stores = [];
var racks = [];
var taxes = [];
var tax = { taxId: 0, taxName: "NA", rate: 0 };
var salesRate = 0;
var orderDetailsId = 0;
var deliveryNoteDetailsId = 0;
var quotationDetailsId = 0;
var totalBillDiscountAmount = 0;
var totalTaxAmount = 0;
var grandTotal = 0;
var totalAmount = 0;
var toSaveFromApply = {};   //object to save when invoice is generated from "apply on"
var deleteIndex;
var grossValue = 0;
var netValue = 0;
var taxAmount = 0;
var amount = 0;
var itemToEdit;
var itemToEditIndex;
var productBatches = [];
var resetBatch = true;
var resetTaxVal = true;
var allProjects = [];
var allCategories = [];



Utilities.Loader.Show();
var lookUpAjax = $.ajax({
    url: API_BASE_URL + "/SalesInvoice/InvoiceLookUps",
    type: "GET",
    contentType: "application/json",
});

var productLookupAjax = $.ajax({
    url: API_BASE_URL + "/ProductCreation/GetProducts",
    type: "GET",
    contentType: "application/json",
});

var lookUpProjects = $.ajax({
    url: API_BASE_URL + "/Project/GetProject",
    type: "GET",
    contentType: "application/json",
});

var lookUpCategories = $.ajax({
    url: API_BASE_URL + "/Category/GetCategory",
    type: "GET",
    contentType: "application/json",
});
function AutoFillForm() {
    var ledgerId = $("#ledgerId").val();
   
    if (ledgerId != null || ledgerId != undefined) { 
        var dropdownlist = $("#customer").data("kendoDropDownList");

        dropdownlist.value(ledgerId);

        //auto fill saleMode entry
        var salesMode = $("#salesMode").val();
        if (salesMode == "17") {
            $("#applyOn").val("17");
            $("#applyOn").html('<option value="17">Delivery Note</option>');
            getOrderNumbers();
        }
        else if (salesMode == "10030") {
            $("#applyOn").val("10030");
            $("#applyOn").html('<option value="10030">Sales Order</option>');
            getOrderNumbers();
       
        }
        else {
            $("#applyOn").val("");
            $("#applyOn").html('');
        }
       
        }
    var loadOrder =  $.ajax({
        url: API_BASE_URL + "/SalesInvoice/GetSalesModeOrderNo?ledgerId=" + $("#customer").val() + "&voucherTypeId=" + $("#applyOn").val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            $("#orderNo").html('<option value="">-Select-</option>' + Utilities.PopulateDropDownFromArray(data, 0, 1));
          
         
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
    $.when(loadOrder).then(function(data) {
        var txtId = $("#txtId").val();
        var vochureId =   $("#invoiceNo").val();; 
        
        if (txtId != null || txtId != undefined) {
            $("#orderNo").val(txtId);
          //  $("#invoiceNo").val(vochureId);
            $("#invoiceNo").val();
              $.ajax({
                  url: API_BASE_URL + "/SalesInvoice/GetInvoiceDetailsFromMaster/" + $("#txtId").val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                //var master = data.Master[0];
                //var details = data.Details;
                             
               // $("#invoiceNo").val(master.invoiceNo);
               var sn = 0;
                for (var i of data.details) {
                    sn += 1;
                   var taxAmt = i.detail.taxAmount;
                   
                    salesInvoiceLineItems.push({
                        SalesDetailsId: i.detail.salesDetailsId,
                        DeliveryNoteDetailsId: i.detail.deliveryNoteDetailsId,
                        OrderDetailsId: i.detail.orderDetailsId,
                        QuotationDetailsId: i.detail.quotationDetailsId,
                        ProductId: i.product.productId,
                        Qty: i.detail.qty,
                        Rate: i.detail.rate,
                        UnitId: i.detail.unitId,
                        UnitConversionId: i.detail.unitConversionId,
                        Discount: i.detail.discount,
                        TaxId: i.detail.taxId,
                        BatchId: i.detail.batchId,
                        GodownId: i.detail.godownId,
                        CategoryId: i.detail.CategoryId,
                        ProjectId: i.detail.ProjectId,
                        RackId: i.detail.rackId,
                        TaxAmount: i.detail.taxAmount,
                        GrossAmount: i.detail.amount, //same as amount since discount does not apply
                        NetAmount: i.detail.netAmount,
                        Amount: i.detail.amount,
                        SlNo: sn,
                        ItemDescription: i.detail.itemDescription
                        
                    });
                }
                renderSalesInvoiceLineItems();
                Utilities.Loader.Hide();
            },
            error: function () {
                Utilities.Loader.Hide();
            }
        });
        }
    });
        //$("#salesMode").data("kendoDropDownList").value("10030");
        $("#balance").val(0);
}

function getSalesMasterInvoiceMax() {
    $.ajax({
        url: API_BASE_URL + "/SalesInvoice/GetInvoiceMax",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data); //return;
            $("#invoiceNo").val(data.InvoiceNo);

        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

$(function () {
    //getAllProducts();
    Utilities.Loader.Show();
    getSalesMasterInvoiceMax()
    $.when(lookUpAjax, productLookupAjax, lookUpCategories, lookUpProjects)
    .done(function (dataLookUp, dataProduct, dataCategories, dataProjects ) {
        console.log("general lookup", dataLookUp);
        console.log("products", dataProduct);
        console.log("categories", dataCategories);
        console.log("projects", dataProjects);
        allProjects = dataProjects[0];
        allCategories = dataCategories[0];
        allProducts = dataProduct[2].responseJSON;
        customers = dataLookUp[2].responseJSON.Customers;
        salesMen = dataLookUp[2].responseJSON.SalesMen;
        pricingLevels = dataLookUp[2].responseJSON.PricingLevels;
        currencies = dataLookUp[2].responseJSON.Currencies;
        stores = dataLookUp[2].responseJSON.Stores;
        racks = dataLookUp[2].responseJSON.Racks;
        units = dataLookUp[2].responseJSON.Units;
        taxes = dataLookUp[2].responseJSON.Taxes;
        batches = dataLookUp[2].responseJSON.Batches;

        //rendering dropdowns
        $("#unit").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(units, 1, 2));
        $("#store").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
        $("#store").chosen({ width: "100%", margin: "1px" });
        $("#project").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allProjects, 0, 1));
        $("#project").chosen({ width: "100%", margin: "1px" });
        $("#category").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allCategories, 0, 1));
        $("#category").chosen({ width: "100%", margin: "1px" });
        // $("#customer").html(Utilities.PopulateDropDownFromArray(customers, 1, 0));
        //$("#batch").html(Utilities.PopulateDropDownFromArray(batches, 0, 1));
        $("#currency").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));
        $("#salesMan").html(Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
        $("#pricingLevel").html(Utilities.PopulateDropDownFromArray(pricingLevels, 0, 1));
        $("#tax").html(Utilities.PopulateDropDownFromArray(taxes, 0, 1));

        $("#searchByProductName").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allProducts, 3, 3));
        $("#searchByBarcode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allProducts, 0, 0));
        $("#searchByProductCode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allProducts, 2, 2));
        $("#searchByProductName").chosen({ width: "100%", margin: "1px" });
        $("#searchByBarcode").chosen({ width: "100%", margin: "1px" });
        $("#searchByProductCode").chosen({ width: "100%", margin: "1px" });

        $("#customer").kendoDropDownList({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: {
                data: customers
            },
            filter: "contains",
        });
        AutoFillForm();
        Utilities.Loader.Hide();
    });

    $("#searchByProductName").on("change", function () {
        searchProduct($("#searchByProductName").val(), "ProductName");
    });
    $("#searchByProductCode").on("change", function () {
        searchProduct($("#searchByProductCode").val(), "ProductCode");
    });
    $("#searchByBarcode").on("change", function () {
        //searchProduct($("#searchByBarcode").val(), "Barcode");    barcode not working as products use productcode as barcode
        searchProduct($("#searchByBarcode").val(), "ProductCode");
    });

    $("#quantity").change(function () {
        resetTax();
        calculateAmountOfItemToAdd();
    });
    $("#rate").change(function () {
        salesRate = $("#rate").val();   //recommended by madam vic to make rate editable as client may decide to change rate at selling point
        resetTax();
        calculateAmountOfItemToAdd();
    });


   
    $("#salesMode").change(function () {
        var salesMode = $(this).val(); 
        if (salesMode == "17") {
            $("#applyOn").val("17");
            $("#applyOn").html('<option value="17">Release Form</option>');
            getOrderNumbers();
        }
        else if (salesMode == "10030") {
            $("#applyOn").val("10030");
            $("#applyOn").html('<option value="10030">Sales Order</option>');
            getOrderNumbers();
        }
        else {
            $("#applyOn").val("");
            $("#applyOn").html('');
        }
    });

    var sl = 1;
    $("#addLineItem").click(function () {
        //salesInvoiceLineItems.push({
        //    ProductId: searchResult.productId,
        //    BarCode: searchResult.barcode,
        //    ProductCode: searchResult.productCode,
        //    ProductName: searchResult.productName,
        //    Description: searchResult.narration,
        //    Brand: searchResult.brandName,
        //    Quantity: $("#quantityToAdd").val(),
        //    Unit: searchResult.unitId,
        //    Store: searchResult.godownId,
        //    Rack: searchResult.rackId,
        //    Batch: searchResult.batchId,
        //    PurchaseRate: searchResult.purchaseRate,
        //    MRP: searchResult.Mrp,
        //    Rate: searchResult.salesRate,
        //    GrossValue: (searchResult.salesRate * $("#quantityToAdd").val()).toFixed(2),
        //    DiscountAmout: $("#discountAmount").val(),
        //    NetAmount: "",
        //    Tax: "",
        //    TaxAmount: "0.00",
        //    Amount:""
        //});
        //console.log("searchResult", searchResult);
        console.log(searchResult);
        //var lineTaxAmt = (tax.rate / 100.0) * searchResult.salesRate; 
        //var netAmount = searchResult.salesRate * $("#quantity").val();
        //var grossAmount = searchResult.salesRate * $("#quantity").val();
        //var amount = searchResult.salesRate + lineTaxAmt;
        var rate = Number($("#rate").val());
        //var lineTaxAmt = (tax.rate / 100.0) * rate;
        //var amount = rate + lineTaxAmt;
        var salesMode = $("#salesMode").val();

        if ($("#quantity").val() == 0 || "") {
            Utilities.ErrorNotification("You need to fill the quantity field.");
            return false;
        }

        if ($("#store").val() == "") {
            Utilities.ErrorNotification("You need to add a store.");
            return false;
        }
        if ($("#batch").val() == "") {
            Utilities.ErrorNotification("Select a Batch");
            $("#batch").css("boder", "1px solid red");
            Utilities.Alert("Select a Batch");
            return false;
        }
        salesInvoiceLineItems.push({
            DeliveryNoteDetailsId: salesMode == "Against Delivery Note" ? deliveryNoteDetailsId : 0,
            OrderDetailsId: salesMode == "Against SalesOrder" ? orderDetailsId : 0,
            QuotationDetailsId: salesMode == "Against Quotation" ? quotationDetailsId : 0,
            ProductId: searchResult.productId,
            Qty: $("#quantity").val(),
            Rate: rate,
            UnitId: $("#unit").val(),
            //UnitConversionId : 1,
            Discount: $("#discount").val() != ""? $("#discount").val() : 0,
            PercentDiscount: $("#percentDiscount").val() != "" ? $("#percentDiscount").val() : 0,
            TaxId: $("#tax").val(),
            BatchId: $("#batch").val(),
            GodownId: $("#store").val(),
            ProjectId: $("#project").val(),
            CategoryId: $("#category").val(),
            RackId: 0,
            TaxAmount: parseFloat($("#taxAmount").val()),
            GrossAmount: grossValue,
            NetAmount: netValue,
            Amount: amount,
            SlNo: sl,
            ItemDescription: ""

            
        });
       
        tax = { taxId: 0, taxName: "NA", rate: 0 };

        console.log("salesinvitems",salesInvoiceLineItems);
        $("#searchByBarcode").val("");
        $("#searchByBarcode").trigger("chosen:updated");
        $("#searchByProductCode").val("");
        $("#searchByProductCode").trigger("chosen:updated");
        $("#searchByProductName").val("");
        $("#searchByProductName").trigger("chosen:updated");
        $("#project").val("");
        $("#project").trigger("chosen:updated");
        $("#category").val("");
        $("#category").trigger("chosen:updated");
        $("#unit").val("");
        $("#store").val("");
        $("#store").trigger("chosen:updated");
        //$("#tax").val("0"); 
        $("#taxAmount").val("0");
        $("#quantity").val(""); 
        $("#quantityInStock").val("");
        $("#storeQuantityInStock").val("");
        $("#amount").val(0);
        $("#rate").val(0);
        $("#description").val("");
        $("#quantityToAdd").val(0);
        $("#discount").val(0);
        $("#percentDiscount").val(0);
        $("#grossValue").val("0");
        $("#netValue").val("0");
        renderSalesInvoiceLineItems();
    });

    $("#customer").change(function () {
        getOrderNumbers();
    });

    $("#orderNo").change(function () {
        $.ajax({
            url: API_BASE_URL + "/SalesInvoice/GetSalesModeItems?orderNo=" + $("#orderNo").val() + "&salesMode=" + $("#salesMode").val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                var master = data.Master[0];
                var details = data.Details;
                             

               // $("#invoiceNo").val(master.invoiceNo);
                for (i = 0; i < details.length; i++) {
                    var taxAmt = details[i].taxAmount == undefined ? 0 : details[i].taxAmount;
                    details[i].qty = details[i].qty == undefined ? details[i].Qty : details[i].qty;
                    details[i].rate = details[i].rate == undefined ? details[i].Rate : details[i].rate;
                    details[i].amount = details[i].amount == undefined ? details[i].Amount : details[i].amount;
                    details[i].godownId = details[i].godownId == undefined || details[i].godownId == 0 ? details[i].extra1 : details[i].godownId;

                    salesInvoiceLineItems.push({
                        DeliveryNoteDetailsId: details[i].deliveryNoteDetailsId,
                        OrderDetailsId: 0,
                        QuotationDetailsId: 0,
                        ProductId: details[i].productId,
                        Qty: details[i].qty,
                        Rate: details[i].rate,
                        UnitId: details[i].unitId,
                        UnitConversionId: details[i].unitConversionId,
                        Discount: 0,
                        PercentDiscount: 0,
                        TaxId: details[i].taxId == undefined ? 0 : details[i].taxId,
                        BatchId: details[i].batchId,
                        GodownId: details[i].godownId,
                        ProjectId: details[i].projectId,
                        CategoryId: details[i].categoryId,
                        RackId: details[i].rackId,
                        TaxAmount: taxAmt,
                        GrossAmount: details[i].amount, //same as amount since discount does not apply
                        NetAmount: parseFloat(details[i].amount),
                        Amount: parseFloat(details[i].amount) + taxAmt,
                        SlNo: (i + 1),
                        ItemDescription: details[i].itemDescription
                    });
                }
                renderSalesInvoiceLineItems();
                Utilities.Loader.Hide();
            },
            error: function () {
                Utilities.Loader.Hide();
            }
        });
    });

    $("#save").click(function () {
        var customer = $("#customer").data("kendoDropDownList").value();
        if (customer == "") {
            Utilities.ErrorNotification("Please select a customer .");
            return;
        }
        if ($("#transactionDate").val() == "") {
            Utilities.ErrorNotification("Please select transaction date.");
            return;
        }
        else if ($("#invoiceNo").val() == "") {
            Utilities.ErrorNotification("Please enter invoice number.");
            return;
        }
        else if ($("#balance").val() == "") {
            Utilities.ErrorNotification("Please enter credit period.");
            return;
        }
        else if (salesInvoiceLineItems.length < 1) {
            Utilities.ErrorNotification("Transaction has no item added.");
            return;
        }
        else { 
            //get orderId or Deliverynote Id 
            var mode = $("#salesMode").val();
            var deliveryId = null;
            var orderId = null; 
            //DeliveryNote Mode
            if (mode == 17) {
                deliveryId = $("#orderNo").val();
            }
            else if (mode == 10030) {
                orderId = $("#orderNo").val();
            }
            var toSave =
            {
                SalesMode: $("#salesMode").val(),
                SalesMasterInfo: {
                    Date: $("#transactionDate").val(),
                    VoucherNo: $("#invoiceNo").val(),
                    InvoiceNo: $("#invoiceNo").val(),
                    CreditPeriod: $("#balance").val(),
                    LedgerId: customer,
                    PricinglevelId: $("#pricingLevel").val(),
                    EmployeeId: $("#salesMan").val(),
                    ExchangeRateId: $("#currency").val(),
                    TaxAmount: parseFloat(totalTaxAmount),
                    BillDiscount:  $("#billDiscount").val() != ""?  $("#billDiscount").val(): 0,
                    GrandTotal: grandTotal,
                    TotalAmount: totalAmount,
                    LrNo: "",
                    TransportationCompany: "",
                    Discount: $("#percentBillDiscount").val() != "" ? $("#percentBillDiscount").val() : 0,
                    OrderMasterId: orderId,
                    DeliveryNoteMasterId: deliveryId
                },
                SalesDetailsInfo: salesInvoiceLineItems
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/SalesInvoice/SaveSalesInvoice",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(toSave),
                success: function (data) {
                    Utilities.SuccessNotification("Invoice saved!");
                    getSalesMasterInvoiceMax();
                    $("#transactionDate").val("");
                    $("#balance").val("");
                    $("#customer")[0].selectedIndex = 0;
                    $("#pricingLevel")[0].selectedIndex = 0;
                    $("#salesMan")[0].selectedIndex = 0;
                    salesInvoiceLineItems = [];
                    totalBillDiscountAmount = 0;
                    totalTaxAmount = 0;
                    grandTotal = 0;
                    totalAmount = 0;
                    renderSalesInvoiceLineItems();
                    Utilities.Loader.Hide();
                    console.log(data);
                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }

    });

    $("#save-changes").click(function() {
        var details = [];
        var master = {
            "salesMasterId": $("#txtId").val(),
            "billDiscount": $("#billDiscount").val(),
            "grandTotal": $("#grandTotal").val(),
            "totalAmount": $("#totalAmount").val(),
            "taxAmount": $("#totalTaxAmount").val(),
            "narration": $("#narration").val()
        }
        for (var i of salesInvoiceLineItems) {
            var detail = {
                "salesDetailsId": i.SalesDetailsId,
                "salesMasterId": $("#txtId").val(),
                "deliveryNoteDetailsId": i.DeliveryNoteDetailsId,
                "orderDetailsId": i.OrderDetailsId,
                "quotationDetailsId": i.QuotationDetailsId,
                "productId": i.ProductId,
                "qty": i.Qty,
                "rate": i.Rate,
                "unitId": i.UnitId,
                "unitConversionId": i.UnitConversionId,
                "discount": i.Discount,
                "taxId": i.TaxId,
                "batchId": i.BatchId,
                "godownId": i.GodownId,
                "projectId": i.ProjectId,
                "categoryId": i.CategoryId,
                "taxAmount": i.TaxAmount,
                "grossAmount": i.GrossAmount,
                "netAmount": i.NetAmount,
                "amount": i.Amount
            }
            details.push(detail);
        }
        var record =
                {
                  "SalesMaster": master,
                  "SalesDetails": details
        }
        $.ajax(API_BASE_URL + "/SalesInvoice/Edit",
            {
                data: record,
                type: "post",
                success: function(res) {
                    if (res.status == 200) {
                        Utilities.Alert(res.message);
                        setTimeout(function() {
                            location.href = "/Customer/SalesInvoice/List";
                        }, 3000);
                    }
                }
    });

    });
});

function getOrderNumbers() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesInvoice/GetSalesModeOrderNo?ledgerId=" + $("#customer").val() + "&voucherTypeId=" + $("#applyOn").val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            $("#orderNo").html('<option value="">-Select-</option>' + Utilities.PopulateDropDownFromArray(data, 0, 1));
            var voucherId = $("#txtId").val();
            if (voucherId != null || voucherId != undefined) {
                $("#orderNo").val(voucherId);

            }
         
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

$("#batch").change(function () {
    resetBatch = false;
    resetTaxVal = false;
    searchProduct($("#searchByProductName").val(), "ProductName");
});

$("#store").change(function () {
    resetBatch = false;
    resetTaxVal = false;
    searchProduct($("#searchByProductName").val(), "ProductName");
});

$("#tax").on("change", function () {
    resetTax();
    calculateAmountOfItemToAdd();
});

function searchProduct(filter, searchBy) {
    var storeId = $("#store").val() == "" ? 0 : parseInt($("#store").val());
    var batchId = $("#batch").val() == "" ? 0 : parseInt($("#batch").val());
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/SearchProduct?filter=" + filter + "&searchBy=" + searchBy + "&storeId=" + storeId + "&batchId=" + batchId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            searchResult = data.Product[0];
            salesRate = searchResult.salesRate;
            //console.log(data);
            productBatches = batches.filter(p => p.productId == parseInt(searchResult.productId));
            

            if (resetBatch == true) {
                populateBatch();
            }

            if (resetTaxVal == true) {
                tax = taxes.find(p => p.taxId == parseInt(searchResult.taxId));
                $("#tax").val(searchResult.taxId);
                $("#tax").trigger("chosen:updated");
            }

            $("#quantity").val(0);
            $("#unit").val(searchResult.unitId);
            $("#rate").val(salesRate);
            $("#description").val(searchResult.narration);
            $("#searchByBarcode").val(searchResult.productCode);
            $("#searchByBarcode").trigger("chosen:updated");
            $("#searchByProductCode").val(searchResult.productCode);
            $("#searchByProductCode").trigger("chosen:updated");
            $("#searchByProductName").val(searchResult.productName);
            $("#searchByProductName").trigger("chosen:updated");

            if (data.QuantityInStock > 0) {
                $("#quantityInStock").css("color", "black");
                $("#quantityInStock").val(Utilities.FormatQuantity(data.QuantityInStock));
            }
            else if ($("#batch").val() != "") {
                $("#quantityInStock").css("color", "red");
                $("#quantityInStock").val("OUT OF STOCK! " + data.QuantityInStock + "STOCK");
            }
            if (data.StoreQuantityInStock > 0) {
                $("#storeQuantityInStock").css("color", "black");
                $("#storeQuantityInStock").val(data.StoreQuantityInStock);
            }
            else if ($("#batch").val() != "" && $("#store").val() != "") {
                $("#storeQuantityInStock").css("color", "red");
                $("#storeQuantityInStock").val("OUT OF STOCK! " + data.StoreQuantityInStock + " STOCK");
            }
                        
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function populateBatch() {
    $("#batch").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(productBatches, 0, 1));
    $("#batch").trigger("chosen:updated");
}

function populateTax() {
    //$("#tax").html(Utilities.PopulateDropDownFromArray([tax], 0, 1) + "<option value=1>NA</option>");
    //$("#tax").trigger("chosen:updated");
}


function getAllProducts() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            allProducts = data;
            console.log(allProducts);

            var productNameHtml = "";
            var productCodeHtml = "";
            var barCodeHtml = "";

            $.each(allProducts, function (count, record) {
                productNameHtml += '<option value="' + record.ProductName + '">' + record.ProductName + '</option>';
                productCodeHtml += '<option value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                barCodeHtml += '<option value="' + record.barcode + '">' + record.barcode + '</option>';
            });
            $("#searchByProductName").html(productNameHtml);
            $("#searchByProductCode").html(productCodeHtml);
            $("#searchByBarcode").html(barCodeHtml);

            $("#searchByProductName").chosen({ width: "100%" });
            $("#searchByProductCode").chosen({ width: "100%" });
            $("#searchByBarcode").chosen({ width: "100%" });
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function renderSalesInvoiceLineItems() {
    var output = "";
    totalTaxAmount = 0.0;
    totalBillDiscountAmount = 0.0;
    totalAmount = 0.0;
    var taxName = "";
    console.log("sa",salesInvoiceLineItems);
    $.each(salesInvoiceLineItems, function (count, row) {
        taxName = taxes.find(p => p.taxId == parseInt(row.TaxId)) == undefined ? "NA" : taxes.find(p => p.taxId == parseInt(row.TaxId)).taxName;
        var thisCategory = row.CategoryId == null ? "N/A" : getCategoryName(row.CategoryId);
        var thisProject = row.ProjectId == null ? "N/A" : getProjectName(row.ProjectId);
        //var projectObj = allProjects.find(p => p.ProjectId == row.ProjectId);
        //var projectName = projectObj == undefined ? "NA" : projectObj.ProjectName;
        //var categoryObj = allCategories.find(p => p.CategoryId == row.CategoryId);
        //var categoryName = categoryObj == undefined ? "NA" : categoryObj.CategoryName;
        output +=
            '<tr>\
                <td style="white-space: nowrap;"><button onclick="confirmDelete(' + count + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
                <td style="white-space: nowrap;"><button onclick="applyDiscount(' + count + ' )" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></button></td>\
                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                <td style="white-space: nowrap;">' + findProduct(row.ProductId).barcode + '</td>\
                <td style="white-space: nowrap;">' + findProduct(row.ProductId).ProductCode + '</td>\
                <td style="white-space: nowrap;">' + findProduct(row.ProductId).ProductName + '</td>\
                <td style="white-space: nowrap;">' + findStore(+row.GodownId).godownName + '</td>\
                <td style="white-space: nowrap;">' + row.Qty + '</td>\
                <td style="white-space: nowrap;">' + findUnit(row.UnitId).unitName + '</td>\
                <td style="white-space: nowrap;">' + findBatch(row.BatchId).batchNo + '</td>\
                <td style="white-space: nowrap;">' + row.Rate + '</td>\
                <td style="white-space: nowrap;">' + thisProject + '</td>\
                <td style="white-space: nowrap;">' + thisCategory + '</td>\
                <td style="white-space: nowrap;">' + row.GrossAmount + '</td>\
                <td style="white-space: nowrap;">' + row.Discount + '</td>\
                <td style="white-space: nowrap;">' + row.PercentDiscount + '</td>\
                <td style = "white-space: nowrap;" > ' + row.Amount + '</td >\
                <!--< td style = "white-space: nowrap;" > ' + row.NetAmount + '</td> -->\
                <td style="white-space: nowrap;">' + taxName + '</td>\
                <td style="white-space: nowrap;">'+ row.TaxAmount + '</td>\
            </tr>\
            ';
        console.log("Ct array =", allCategories);
        console.log("Ct array =", getCategoryName(12));
       
        totalTaxAmount = totalTaxAmount + row.TaxAmount;
        //totalBillDiscountAmount = totalBillDiscountAmount + row.Discount;
        totalAmount = totalAmount + row.Amount;
    });
    var discount = $("#billDiscount").val();
    billDiscountChange();
    grandTotal = (totalAmount + totalTaxAmount) - discount;
    $("#totalAmount").val(totalAmount);
    $("#totalTaxAmount").val(totalTaxAmount);
    //$("#billDiscount").val(totalBillDiscountAmount);
    $("#grandTotal").val(grandTotal);
    $("#salesInvoiceLineItemTbody").html(output);
}

var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the positive lable as key
    //...
};

function confirmDelete(index) {
    deleteIndex = index;
    eModal.confirm(options).then(removeLineItem);
}

function removeLineItem() {
    salesInvoiceLineItems.splice(deleteIndex, 1);
    renderSalesInvoiceLineItems();
}

function resetTax() {
    var isFound = false;
    for (i = 0; i < taxes.length; i++) {
        if (taxes[i].taxId == $("#tax").val()) {
            tax = taxes[i];
            isFound = true;
            break;
        }
    }
    if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
    {
        tax = { taxId: 0, taxName: "NA", rate: 0 };
    }
    //        console.log(tax);
}


function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}

function findProduct(productId) {
    var output = {};
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].ProductId == productId) {
            output = allProducts[i];
            break;
        }
    }
    return output;
}


function getCategoryName(id) {
    var CategoryName;
    for (i = 0; i < allCategories.length; i++) {
        if (allCategories[i].CategoryId == id) {
            CategoryName = allCategories[i].CategoryName;
            break;
        }
    }
    return CategoryName;
}

function getProjectName(id) {
    var ProjectName = {};
    for (i = 0; i < allProjects.length; i++) {
        if (allProjects[i].ProjectId == id) {
            ProjectName = allProjects[i].ProjectName;
            break;
        }
    }
    return ProjectName;
}

function calculateAmountOfItemToAdd() {
    grossValue = salesRate * $("#quantity").val();
    netValue = grossValue - parseFloat($("#discount").val());
    taxAmount = netValue * (tax.rate / 100.0);
    amount = netValue; //netValue + taxAmount;
    discountChange();
    $("#grossValue").val(Utilities.FormatCurrency(grossValue));
    $("#netValue").val(Utilities.FormatCurrency(netValue));
    $("#amount").val(Utilities.FormatCurrency(amount));
    $("#taxAmount").val(Utilities.FormatCurrency(taxAmount));
}

$("#discount").change(function () {
    discountChange();
    calculateAmountOfItemToAdd();
});

function discountChange() {
    var discount = $("#discount").val() != "" ? parseFloat($("#discount").val()) : 0;
    var percentDiscount = (discount * 100) / grossValue;
    $("#percentDiscount").val(percentDiscount.toFixed(2));
}

$("#percentDiscount").change(function () {
    var percentDiscount = $("#percentDiscount").val() != "" ? parseFloat($("#percentDiscount").val()) : 0;
    var discount = percentDiscount / 100 * grossValue; 
    $("#discount").val(discount.toFixed(2));
    calculateAmountOfItemToAdd();
});

///s

$("#billDiscount").change(function () {
    billDiscountChange();
});

$("#percentBillDiscount").change(function () {
    var percentBillDiscount = $("#percentBillDiscount").val() != "" ? parseFloat($("#percentBillDiscount").val()) : 0;
    var billDiscount = percentBillDiscount / 100 * totalAmount;
    grandTotal = totalAmount - billDiscount;
    $("#billDiscount").val(billDiscount);
    $("#grandTotal").val(grandTotal);
});

function billDiscountChange() {
    var billDiscount = $("#billDiscount").val() != "" ? parseFloat($("#billDiscount").val()) : 0;
    var percentBillDiscount = (billDiscount * 100) / totalAmount;
    grandTotal = totalAmount - billDiscount;
    $("#percentBillDiscount").val(percentBillDiscount.toFixed(2));
    $("#grandTotal").val(grandTotal);
}

function applyDiscount(id) {
    itemToEditIndex = id;
    itemToEdit = salesInvoiceLineItems[id];
    $("#applyDiscountModal").modal("show");
    $("#applyDiscountAmount").val(itemToEdit.Discount);
    $("#applyPercentDiscount").val(itemToEdit.PercentDiscount);
}

$("#applyDiscountAmount").change(function () {
    var discount = $("#applyDiscountAmount").val() != "" ? parseFloat($("#applyDiscountAmount").val()) : 0;
    var percentDiscount = (discount * 100) / itemToEdit.GrossAmount;
    $("#applyPercentDiscount").val(percentDiscount.toFixed(2));
    //calculateAmountOfItemToAdd();
});

$("#applyPercentDiscount").change(function () {
    var percentDiscount = $("#applyPercentDiscount").val() != "" ? parseFloat($("#applyPercentDiscount").val()) : 0;
    var discount = percentDiscount / 100 * itemToEdit.GrossAmount;
    $("#applyDiscountAmount").val(discount);
    //calculateAmountOfItemToAdd();
});

function applyDiscountOnObject() {
    var grossValue = itemToEdit.Rate * itemToEdit.Qty;
    var netValue = grossValue - parseFloat($("#applyDiscountAmount").val());
    var tax = taxes.find(p => p.taxId == itemToEdit.TaxId);
    var taxRate = tax != undefined ? tax.rate : 0;
    var taxAmount = netValue * (taxRate / 100.0);
    var amount = netValue; //netValue + taxAmount;

    itemToEdit.Discount = $("#applyDiscountAmount").val();
    itemToEdit.PercentDiscount = $("#applyPercentDiscount").val();
    itemToEdit.GrossAmount = grossValue;
    itemToEdit.NetAmount = netValue;
    itemToEdit.TaxAmount = taxAmount;
    itemToEdit.Amount = amount;
    salesInvoiceLineItems[itemToEditIndex] = itemToEdit;
    renderSalesInvoiceLineItems();
}