﻿var allProducts = [];
var salesInvoiceLineItems = [];
var searchResult = {};
var customers = [];
var salesMen = [];
var pricingLevels = [];
var currencies = [];
var units = [];
var batches = [];
var stores = [];
var racks = [];
var taxes = [];
var voucherTypes = [];
var lineItems = [];
var productToEdit = 0;
var totalAmountG = 0.0;
var lookUpAjax = $.ajax({
    url: API_BASE_URL + "/RejectionIn/LookUps",
    type: "GET",
    contentType: "application/json",
});

var productLookupAjax = $.ajax({
    url: API_BASE_URL + "/ProductCreation/GetProducts",
    type: "GET",
    contentType: "application/json"
});


var voucherNoLookupAjax = $.ajax({
    url: API_BASE_URL + "/RejectionIn/GetAutoFormNo",
    type: "GET",
    contentType: "application/json",
});



function autoFill() {

    //get page varables 
    var ledgerId = $("#ledgerId").val();
    var ledgerName = $("#ledgerName").val();
    var voucherId = $("#voucherId").val();
    var txtId = $("#txtId").val();  
    if (txtId == '') {
        return;
    }
    //check if transaction is not null, which means it is a route 
    if (txtId != null || txtId != undefined) {
        $("#customer").html(); 
        //set customer 
        var html = `<option type="${ledgerId}"> ${ledgerName}</option>`;
        $("#customer").html(html); 
        var fillReturn =    $.ajax({
            url: API_BASE_URL + "/RejectionIn/GetDeliveryNoteNo?customerId=" + ledgerId + "&voucherType=" + $("#applyOn").val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                var deliveryNotesHtml = "";
                deliveryNotesHtml += '<option value=""></option>';
                $.each(data, function (count, record) {
                    deliveryNotesHtml += '<option value="' + record.deliveryNoteMasterId + '">' + record.invoiceNo + '</option>';
                });
                $("#deliveryNoteNo").html(deliveryNotesHtml);
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                Utilities.Loader.Hide();
            }
        });
        $.when(fillReturn).then(function(data) {
            console.log(data);
            var deliveryNotesHtml = "";
            deliveryNotesHtml += '<option value=""></option>';
            $.each(data, function (count, record) {
                deliveryNotesHtml += '<option value="' + record.deliveryNoteMasterId + '">' + record.invoiceNo + '</option>';
            });
            $("#deliveryNoteNo").html(deliveryNotesHtml);
            $("#deliveryNoteNo").val(txtId); 

            Utilities.Loader.Hide(); 
            Utilities.Loader.Show();
            var val = $("#deliveryNoteNo").val();
            $.ajax({
                url: API_BASE_URL + "/RejectionIn/FillGridCorrespondingToDeliveryNoteNo?deliveryNoteNo=" + val,
                type: "GET",
                contentType: "application/json",
                success: function (data) {
                    for (i = 0; i < data.length; i++) {
                        data[i].QtyToReturn = 0;
                    }
                    lineItems = data;
                    console.log(data);
                    renderLineItems(data);
                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                    Utilities.Loader.Hide();
                }
            });
        });
    }
  



}
$(function () {
    //getAllProducts();
    Utilities.Loader.Show();
   
    $.when(lookUpAjax, productLookupAjax, voucherNoLookupAjax)
    .done(function (dataLookUp, dataProduct, dataVoucherNo) {
        console.log("general lookup", dataLookUp);
        console.log("products", dataProduct);
        console.log("voucherNo", dataVoucherNo[0]);
        $("#voucherNo").val(dataVoucherNo[0]);
        allProducts = dataProduct[2].responseJSON;
        customers = dataLookUp[0].Customers;
        salesMen = dataLookUp[2].responseJSON.SalesMen;
        pricingLevels = dataLookUp[2].responseJSON.PricingLevels;
        currencies = dataLookUp[2].responseJSON.Currencies;
        voucherTypes = dataLookUp[2].responseJSON.VoucherTypes;

        var productNameHtml = "";
        var productCodeHtml = "";
        var barCodeHtml = "";

        $.each(allProducts, function (count, record) {
            productNameHtml += '<option value="' + record.ProductName + '">' + record.ProductName + '</option>';
            productCodeHtml += '<option value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
            barCodeHtml += '<option value="' + record.barcode + '">' + record.barcode + '</option>';
        });
        $("#searchByProductName").html(productNameHtml);
        $("#searchByProductCode").html(productCodeHtml);
        $("#searchByBarcode").html(barCodeHtml);

        //var customersHtml = "";
        //$.each(customers, function (count, record) {
        //    customersHtml += '<option value="' + record.ledgerId + '">' + record.ledgerName + " - " + record.extra1 + '</option>';
        //});
        //$("#customers").kendoDropDownList({
        //    filter: "contains",
        //    dataTextField: "ledgerName" + "extra1",
        //    dataValueField: "ledgerId",
        //    dataSource: customers
        //});
        // $("#customers").html(customersHtml); 
        $("#customer").kendoDropDownList({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: {
                data: customers
            },
            filter: "contains"
        });
        var customersWithCode = [];
        //for (i = 0; i < customers.length; i++) {
        //    var ext = (customers[i].extra1 == "") ? "NA" : customers[i].extra1;
        //    //customersWithCode.push([customers[i].ledgerName + "  ____  " + ext,customers[i].ledgerId]);
        //    customersWithCode.push([(i + 1), customers[i].extra1, customers[i].ledgerName]);
        //}
        //var table = "";

        //table = $('#agentsTable').DataTable({
        //    data: customersWithCode,
        //    "paging": true,
        //    "lengthChange": true,
        //    "searching": true,
        //    "ordering": true,
        //    "info": true,
        //    "autoWidth": true
        //});
        $("#customer").change(function() {
            var ledgerId = $(this).val();
            console.log("selected ledger Id ", ledgerId );
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/RejectionIn/GetDeliveryNoteNo?customerId=" + ledgerId + "&voucherType=" + $("#applyOn").val(),
                type: "GET",
                contentType: "application/json",
                success: function (data) {
                    console.log(data);
                    var deliveryNotesHtml = "";
                    deliveryNotesHtml += '<option value=""></option>';
                    $.each(data, function (count, record) {
                        deliveryNotesHtml += '<option value="' + record.deliveryNoteMasterId + '">' + record.invoiceNo + '</option>';
                    });
                    $("#deliveryNoteNo").html(deliveryNotesHtml);
                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                    Utilities.Loader.Hide();
                }
            });
        });
        $('.dataTable').on('click', 'tbody tr', function () {
            console.log('API row values : ', table.row(this).data());
            var rowData = table.row(this).data();
            var selectedAgent = customers.find(p=>p.extra1 == rowData[1]);
            console.log(selectedAgent);
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/RejectionIn/GetDeliveryNoteNo?customerId=" + selectedAgent.ledgerId + "&voucherType=" + $("#applyOn").val(),
                type: "GET",
                contentType: "application/json",
                success: function (data) {
                    console.log(data);
                    var deliveryNotesHtml = "";
                    deliveryNotesHtml += '<option value=""></option>';
                    $.each(data, function (count, record) {
                        deliveryNotesHtml += '<option value="' + record.deliveryNoteMasterId + '">' + record.invoiceNo + '</option>';
                    });
                    $("#deliveryNoteNo").html(deliveryNotesHtml);
                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                    Utilities.Loader.Hide();
                }
            });
            $("#customer").html('<option value="' + selectedAgent.ledgerId + '">' + selectedAgent.ledgerName + "  :  " + selectedAgent.extra1 + "</option>");
            $("#agentsModal").modal("hide");
        })

        //$("#customer").click(function () {
        //    $("#agentsModal").modal("show");
        //});
        //var salesMenHtml = "";
        //$.each(salesMen, function (count, record) {
        //    salesMenHtml += '<option value="' + record.employeeId + '">' + record.employeeName + '</option>';
        //});
        //$("#salesMan").html(salesMenHtml);

        //Restrict Sales Man
        var salesMan = salesMen.find(p => p.userId == matUserInfo.UserId);
        if (salesMan != undefined) {
            $("#salesMan").html(Utilities.PopulateDropDownFromArray([salesMan], 0, 1));
        } else {
            $("#salesMan").html("<option value=\"0\">--NA--</option>" + Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
        }


        $("#salesMan").chosen({ width: "100%" });

        var pricingLevelHtml = "";
        $.each(pricingLevels, function (count, record) {
            pricingLevelHtml += '<option value="' + record.pricinglevelId + '">' + record.pricinglevelName + '</option>';
        });
        $("#pricingLevel").html(pricingLevelHtml);

        var currencyHtml = "";
        $.each(currencies, function (count, record) {
            currencyHtml += '<option value="' + record.exchangeRateId + '">' + record.currencyName + '</option>';
        });
        $("#currency").html(currencyHtml);

        var voucherTypeHtml = "";
        $.each(voucherTypes, function (count, record) {
            voucherTypeHtml += '<option value="' + record.voucherTypeId + '">' + record.voucherTypeName + '</option>';
        });
        $("#applyOn").html(voucherTypeHtml);
        autoFill();
        Utilities.Loader.Hide();
    });

    $("#addSalesInvoiceLineItem").click(function () {
        salesInvoiceLineItems.push({
            ProductId: searchResult.productId,
            BarCode: searchResult.barcode,
            ProductCode: searchResult.productCode,
            ProductName: searchResult.productName,
            Description: searchResult.narration,
            Brand: searchResult.brandName,
            Quantity: $("#quantityToAdd").val(),
            Unit: searchResult.unitId,
            Store: searchResult.godownId,
            Rack: searchResult.rackId,
            Batch: searchResult.batchId,
            PurchaseRate: searchResult.purchaseRate,
            MRP: searchResult.Mrp,
            Rate: searchResult.salesRate,
            GrossValue: (searchResult.salesRate * $("#quantityToAdd").val()).toFixed(2),
            DiscountAmout: $("#discountAmount").val(),
            NetAmount: "",
            Tax: "",
            TaxAmount: "0.00",
            Amount: (searchResult.salesRate * $("#quantityToAdd").val()).toFixed(2) -  Number($("#discountAmount").val())
        });
        console.log("searchResult", searchResult);
        renderSalesInvoiceLineItems();
    });

    //$("#customers").change(function () {
    //    var val = $("#customers").val();
    //    Utilities.Loader.Show();
    //    $.ajax({
    //        url: API_BASE_URL + "/RejectionIn/GetDeliveryNoteNo?customerId=" + $("#customers").val() + "&voucherType=" + $("#applyOn").val(),
    //        type: "GET",
    //        contentType: "application/json",
    //        success:function(data)
    //        {
    //            console.log(data);
    //            var deliveryNotesHtml = "";
    //            deliveryNotesHtml += '<option value=""></option>';
    //            $.each(data, function (count, record) {
    //                deliveryNotesHtml += '<option value="' + record.deliveryNoteMasterId + '">' + record.invoiceNo + '</option>';
    //            });
    //            $("#deliveryNoteNo").html(deliveryNotesHtml);
    //            Utilities.Loader.Hide();
    //        },
    //        error: function (err)
    //        {
    //            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
    //            Utilities.Loader.Hide();
    //        }
    //    });
    //});
    function getRejectionInItems(id) {
        $.ajax({
            url: API_BASE_URL + "/RejectionIn/FillGridCorrespondingToDeliveryNoteNo?deliveryNoteNo=" + val,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                for (i = 0; i < data.length; i++) {
                    data[i].QtyToReturn = 0;
                }
                lineItems = data;
                var sl = 0;
                for (var i of lineItems) {
                    sl += 1;
                    i.SL = sl;
                }
                console.log(data);
                renderLineItems(data);
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                Utilities.Loader.Hide();
            }
        });
    }
    $("#deliveryNoteNo").change(function () {
        Utilities.Loader.Show();
        var val = $("#deliveryNoteNo").val();
        $.ajax({
            url: API_BASE_URL + "/RejectionIn/FillGridCorrespondingToDeliveryNoteNo?deliveryNoteNo=" + val,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                for (i = 0; i < data.length; i++) {
                    data[i].QtyToReturn = 0;
                }
                lineItems = data;
                var sl = 0;
                for (var i of lineItems) {
                    sl += 1;
                    i.SL = sl;
                }
                console.log(data);
                renderLineItems(data);
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                Utilities.Loader.Hide();
            }
        });
    });

    $("#saveRejectionIn").click(function () {
        saveRejectionIn();
    });
});

function searchProduct(filter, searchBy) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesInvoice/SearchProduct?filter=" + filter + "&searchBy=" + searchBy,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var productDetails = data.Product[0];
            searchResult = productDetails;

            $("#searchResultName").val(productDetails.productName);

            if (data.QuantityInStock > 0) {
                $("#quantityInStock").css("color", "black");
                $("#quantityInStock").html(data.QuantityInStock);
            }
            else {
                $("#quantityInStock").css("color", "red");
                $("#quantityInStock").html("OUT OF STOCK!");
            }

            var storesHtml = '';
            $.each(data.Stores, function (count, row) {
                storesHtml += '<option value="' + row.godownId + '">' + row.godownName + '</option>';
            });
            $("#searchResultStore").html(storesHtml);

            var racksHtml = '';
            $.each(data.Racks, function (count, row) {
                racksHtml += '<option value="' + row.rackId + '">' + row.rackName + '</option>';
            });
            $("#searchResultRack").html(racksHtml);

            batches = data.Batches;
            units = data.Units;
            stores = data.Stores;
            racks = data.Racks;
            taxes = data.Tax;

            $("#searchResultPanel").show();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function getAllProducts() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            allProducts = data;
            console.log(allProducts);

            var productNameHtml = "";
            var productCodeHtml = "";
            var barCodeHtml = "";

            $.each(allProducts, function (count, record) {
                productNameHtml += '<option value="' + record.ProductName + '">' + record.ProductName + '</option>';
                productCodeHtml += '<option value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                barCodeHtml += '<option value="' + record.barcode + '">' + record.barcode + '</option>';
            });
            $("#searchByProductName").html(productNameHtml);
            $("#searchByProductCode").html(productCodeHtml);
            $("#searchByBarcode").html(barCodeHtml);

            $("#searchByProductName").chosen({ width: "100%" });
            $("#searchByProductCode").chosen({ width: "100%" });
            $("#searchByBarcode").chosen({ width: "100%" });
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}


function renderLineItems() {
    var output = "";
    var totalAmount = 0.0;
    $.each(lineItems, function (count, row) {
        output +=
            '<tr>\
                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                <td style="white-space: nowrap;">' + row.Barcode + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductCode + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductName + '</td>\
                <td style="white-space: nowrap;">'+ row.Quantity + '</td>\
                <td style="white-space: nowrap;">'+ row.QtyToReturn + '</td>\
                <td style="white-space: nowrap;">' + row.Unit + '</td>\
                <td style="white-space: nowrap;">'+ row.Store + '</td>\
                <td style="white-space: nowrap;">' + row.Rack + '</td>\
                <td style="white-space: nowrap;">' + row.Batch + '</td>\
                <td><button class="btn btn-sm btn-info" onclick="changeQuantity(' + row.SL + ')"><i class="fa fa-edit"></i></button></td>\
            </tr>\
            ';
        totalAmount = totalAmount + row.Amount;
        totalAmountG += totalAmount;
    }); //parseFloat((Math.round(num * 100) / 100)).toFixed(2).toLocaleString();
    $("#totalAmount").val(Utilities.FormatCurrency(totalAmount));
    $("#lineItemTbody").html(output);
}

function changeQuantity(id) {
    $("#quantityModal").modal("show");
    productToEdit = id;
}

function applyQuantityOnObject() {
    console.log(productToEdit);
    for (i = 0; i < lineItems.length; i++) {
        if (lineItems[i].SL == productToEdit) {
            if ($("#quantity").val() <= lineItems[i].Quantity) {
                lineItems[i].QtyToReturn = Number($("#quantity").val());
                lineItems[i].Amount = lineItems[i].Rate * Number($("#quantity").val());
            }
            else {
                Utilities.ErrorNotification("Quantity to Return cannot be Higher than Quantity Ordered");
            }
        }

    }
    $("#quantity").val("");
    $("#quantityModal").modal("hide");
    renderLineItems();
}

function saveRejectionIn() {
    Utilities.Loader.Show();
    var totalAmt = 0.0;
    for (i = 0; i < lineItems.length; i++) {
        lineItems[i].Quantity = lineItems[i].QtyToReturn;
        totalAmt += Number(lineItems[i].Amount);
    }
    
    var toSave = {
        Date: $("#transactionDate").val(),
        CustomerId: $("#customer").val(),
        Narration: $("#narration").val(),
        TotalAmount: totalAmt,
        TransportationCompany: $("#transportationCompany").val(),
        LrNo: 1,
        PricingLevelId: $("#pricingLevel").val(),
        DeliveryNoteMasterId: $("#deliveryNoteNo").val(),
        UserId: matUserInfo.UserId,
        LineItems: lineItems,
    }
    console.log(toSave);
    $.ajax({
        url: API_BASE_URL + "/RejectionIn/savePendingRejectionIn",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            Utilities.Loader.Hide();
            if (data == "success")
            {
                Utilities.SuccessNotification("Rejection-In saved");
                window.location = "/Customer/RejectionIn/Index";
            }
            else {
                Utilities.ErrorNotification("Rejection-In Failed");
            }
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}


function saveRejectionInComplete() {
    Utilities.Loader.Show()
    var totalAmt = 0.0;
    for (i = 0; i < lineItems.length; i++) {
        lineItems[i].Quantity = lineItems[i].QtyToReturn;
        totalAmt += Number(lineItems[i].Amount);

    }
    var toSave = {
        Date: $("#transactionDate").val(),
        VoucherNo: $("#voucherNo").val(),
        CustomerId: $("#customer").val(),
        Narration: $("#narration").val(),
        TotalAmount: totalAmt,
        TransportationCompany: $("#transportationCompany").val(),
        LrNo: 1,
        PricingLevelId: $("#pricingLevel").val(),
        DeliveryNoteMasterId: $("#deliveryNoteNo").val(),
        SalesManId: $("#salesMan").val(),
        UserId: matUserInfo.UserId,
        LineItems: lineItems,
    }
    console.log(toSave);
    $.ajax({
        url: API_BASE_URL + "/RejectionIn/savePendingRejectionIn",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            Utilities.Loader.Hide();
            if (data == "success")
            {
                //Utilities.SuccessNotification("Rejection-In saved"); 
                //effect quantity 
                $.ajax({
                    url: API_BASE_URL + "/RejectionIn/saveRejectionIn",
                    type: "POST",
                    data: JSON.stringify(toSave),
                    contentType: "application/json",
                    success: function (data) {
                        console.log(data);
                        Utilities.Loader.Hide();
                        if (data == "Rejection Successfully Saved") {
                            Utilities.Loader.Hide();
                            Utilities.SuccessNotification("Rejection-In saved");
                            //window.location = "/Customer/RejectionIn/RejectionInListing";
                            window.location.reload();
                        }
                        else {
                            Utilities.ErrorNotification("Rejection-In Failed");
                        }
                    },
                    error: function (err) {
                        Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                        Utilities.Loader.Hide();
                    }
                });

            }
            else {
                Utilities.ErrorNotification("Rejection-In Failed");
            }
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}