﻿var count = 0;
//var lineItemCount = 0;
//var additionalCost = [];
//var lineItems = [];
var totalLineItemAmount = 0.0;
var totalTaxAmount = 0.0;


//var suppliers = [];
//var accountLedgers = [];
//var currencies = [];
//var orderNumbers = [];
//var receiptNumbers = [];
//var purchaseOrderVoucherType = [{1:10, 2:"Purchase Order"}];
//var materialReceiptVoucherType =[ {1:11, 2:"Material Receipt"}];
//var purchaseInvoiceLineItems = [];
//var units = [];
//var batches = [];
//var stores = [];
//var racks = [];
//var tax = {};
//var totalAdditionalCost = 0.0;

var storeId = 0;
var products = [];
var searchResult = {};
var salesOrderLineItems = [];
var customers = [];
var salesMen = [];
var applyOn = [];
var pricingLevel = [];
var currencies = [];
var units = [];
var batches = [];
var stores = [];
var racks = [];
var allTaxes = [];
var quotationMasterId = 0;
var quotationDetailsId = 0;
var tax = [];
function updateAmount() {
    var rate = $("#rate").val();
    var qty = $("#quantityToAdd").val();
    var amount = rate * qty;
    $("#amount").val(amount);
}
$(function () {

    getLookUps(SalesOrderMasterId);

    $("#dueDays").attr("disabled", "disabled");
    $("#quotationNo").attr("disabled", "disabled");
    $("#rate").change(function() {
        updateAmount();
    });
    $("#applyOn").change(function () {
        if ($("#applyOn").val() != "NA") {
            getQuotationNumbers($("#customers").val(), $("#applyOn").val());
            $("#quotationNo").removeAttr("disabled");
            $("#btnAddLineItemModal").attr("disabled", "disabled");
        }
        else {
            $("#quotationNo").attr("disabled", "disabled");
            $("#btnAddLineItemModal").removeAttr("disabled");
        }
    });
    $("#customers").change(function ()
    {
        if ($("#applyOn").val() != "NA") 
        {
            getQuotationNumbers($("#customers").val(), $("#applyOn").val());
        }
    });
    $("#searchByProductName").on("change", function () {
        searchProduct($("#searchByProductName").val(), storeId, "ProductName");
    });
    $("#searchByProductCode").on("change", function () {
        searchProduct($("#searchByProductCode").val(), storeId, "ProductCode");
    });
    $("#searchByBarcode").on("change", function () {
        searchProduct($("#searchByBarcode").val(), storeId, "Barcode");
    });
    $("#quantityToAdd").on("change", function () {
        var rate = $("#rate").val();
        var qty = $("#quantityToAdd").val();
        var amount = rate * qty;
        $("#amount").val(amount);
    });
    $("#store").on("change", function () {
        if ($("#store").val() != "") {
            $("#quantityToAdd").removeAttr("disabled");
        }
        else {
            $("#quantityToAdd").prop("disabled", "disabled");
        }
    });

    var sl = 1;
    $("#addLineItem").click(function () {
        var qty = $("#quantityToAdd").val();
        var rate = $("#rate").val(); //  searchResult.salesRate;
        var gross = (qty * rate);
        var discount = 0; //discount percent
        var netAmount = gross - (discount / 100.00);
        var taxAmount = searchResult.taxId == tax.TaxId ? (netAmount * 0.05).toFixed(2) : 0;
        var amount = parseFloat(gross) + parseFloat(taxAmount);

        salesOrderLineItems.push({
            SlNo: sl,
            productId: searchResult.productId,
            barcode: searchResult.productCode,
            productCode: searchResult.productCode,
            productName: searchResult.productName,
            itemDescription: searchResult.narration,
            brand: searchResult.brandName,
            qty: qty,
            unitId: searchResult.unitId,
            unitConversionId: searchResult.unitConversionId,
            extra1: searchResult.godownId, //searchResult.godownId,
            rackId: searchResult.rackId,
            batchId: searchResult.batchId,
            taxId: tax.TaxId,
            tax: searchResult.taxId == tax.TaxId ? tax.TaxName : "NA",
            taxAmount: taxAmount,
            rate: rate,
            amount: amount
        });
        sl += 1;
        console.log(salesOrderLineItems);
        renderLineItems();
    });

    $("#update").click(function () {
        update();
    });

    $("#delete").click(function () {
        if (confirm("Remove this item?")) {
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/SalesOrder/DeleteSalesOrderDetails?id=" + SalesOrderMasterId,
                type: "Get",
                contentType: "application/json",
                success: function (data) {
                    console.log(data);
                    var output = "";
                    Utilities.SuccessNotification(data);
                    Utilities.Loader.Hide();
                    window.location = "/Customer/SalesOrder/SalesOrderRegister";
                },
                error: function (err) {
                    Utilities.Loader.Hide();
                }
            });
        }
    });

});  

function AutoFill() {
    var customerId = $("#customerId").val();
    var customerName = $("#customerName").val(); 
    if (customerId != null || customerId != '') {
        $("#customers").val(customerId); 
    }
}
function getLookUps(id) {
    Utilities.Loader.Show();
    Utilities.Loader.Show();
    var productsAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: "Get",
        contentType: "application/json"
    });
    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetLookups",
        type: "Post",
        contentType: "application/json"
    });
    var detailsAjax = $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetSalesOrderDetails?id=" + id,
        type: "Get",
        contentType: "application/json"
    });
    var masterDetailAjax = $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetSalesOrderMasterDetails?id=" + id,
        type: "Get",
        contentType: "application/json"
    });
    $.when(productsAjax, lookUpAjax, detailsAjax, masterDetailAjax)
    .done(function (dataProducts, dataLookUps, dataDetails, dataMaster)
    {
        console.log(dataLookUps);
        products = dataProducts[2].responseJSON;
        salesOrderLineItems = dataDetails[2].responseJSON;
        master = dataMaster[2].responseJSON; console.log(master);
        applyOn = dataLookUps[2].responseJSON.ApplyOn.splice(1, 1);
        units = dataLookUps[2].responseJSON.Units;
        stores = dataLookUps[2].responseJSON.Stores; console.log("stores",stores);
        racks = dataLookUps[2].responseJSON.Racks;
        batches = dataLookUps[2].responseJSON.Batches;
        allTaxes = dataLookUps[2].responseJSON.AllTaxes; console.log(allTaxes);
        customers = dataLookUps[2].responseJSON.Customers;
        salesMen = dataLookUps[2].responseJSON.SalesMen;
        currencies = dataLookUps[2].responseJSON.Currencies;
        pricingLevel = dataLookUps[2].responseJSON.PricingLevel;
        tax = dataLookUps[2].responseJSON.Tax;

        populateOrderMasterDetails();
        renderLineItems();
        AutoFill();
        Utilities.Loader.Hide();
    });
}

function getQuotationNumbers(customerId, applyOn) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetQuotationNumbers?customerId=" + customerId + "&applyOn=" + applyOn,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            $("#quotationNo").html("<option value=\"NA\">-Select-</option>" + Utilities.PopulateDropDownFromArray(data, 0, 1));
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function populateOrderMasterDetails() {

    $("#customers").html(Utilities.PopulateDropDownFromArray(customers, 1, 0));
    $("#currency").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));
    $("#salesMan").html("<option value=\"-1\">--Select--</option>" + Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
    $("#store").html("<option value=\"\">--Select--</option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
    $("#applyOn").html("<option value=\"NA\">NA</option>" + Utilities.PopulateDropDownFromArray(applyOn, 0, 1));
    $("#pricingLevel").html("<option value=\"-1\">--Select--</option>" + Utilities.PopulateDropDownFromArray(pricingLevel, 0, 1));
    $("#searchByProductName").html("<option value=\"\">--Select--</option>" + Utilities.PopulateDropDownFromArray(products, 3, 3));
    $("#searchByBarcode").html("<option value=\"\">--Select--</option>" + Utilities.PopulateDropDownFromArray(products, 0, 0));
    $("#searchByProductCode").html("<option value=\"\">--Select--</option>" + Utilities.PopulateDropDownFromArray(products, 2, 2));
    $("#searchByProductName").chosen({ width: "100%", margin: "1px" });
    $("#searchByBarcode").chosen({ width: "100%", margin: "1px" });
    $("#searchByProductCode").chosen({ width: "100%", margin: "1px" });

    if (master.extra1 == null) {
        master.extra1 = 1;
    }

    document.getElementById("orderNo").value = master.InvoiceNo;
    document.getElementById("transactionDate").value = Utilities.FormatJsonDate(master.Date);
    document.getElementById("dueDate").value = Utilities.FormatJsonDate(master.DueDate);

    $("#customer").val(getAccountLedger(master.LedgerId).ledgerId);
    $("#customer :selected").text(getAccountLedger(master.LedgerId).ledgerName);

    $("#salesMan").val(findSalesMan(master.EmployeeId).employeeId);
    $("#salesMan :selected").text(findSalesMan(master.EmployeeId).enployeeName);

    $("#pricingLevel").val(findPricingLevel(master.PricinglevelId).pricinglevelId);
    $("#pricingLevel :selected").text(findPricingLevel(master.PricinglevelId).pricinglevelName);

    $("#store").val(findStore(master.extra1).godownId);
    $("#store :selected").text(findStore(master.extra1).godownName);

    $("#dueDate").change(function () {
        var transDate = new Date(master.Date);
        var dueDate = new Date(master.DueDate);
        var timeDiff = Math.abs(dueDate.getTime() - transDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        $("#dueDays").val(diffDays);
    });
}
function searchProduct(filter, storeId, searchBy) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/searchProduct?filter=" + filter + "&searchBy=" + searchBy + "&storeId=" + storeId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data); //return;
            var productDetails = data.Product[0];
            searchResult = productDetails;

            $("#rate").val(productDetails.salesRate);
            $("#amount").val(($("#rate").val() * $("#quantityToAdd").val()));
            $("#description").val(productDetails.narration);

            if (data.QuantityInStock > 0) {
                $("#quantityInStock").css("color", "black");
                $("#quantityInStock").val(data.QuantityInStock);
            }
            else {
                $("#quantityInStock").css("color", "red");
                $("#quantityInStock").val("OUT OF STOCK!");
            }

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}
function renderLineItems() {
    totalLineItemAmount = 0.0;
    var output = "";
    console.log(salesOrderLineItems);
    $.each(salesOrderLineItems, function (count, row) {
        //console.log("it is", row.Extra1);
        if (row.taxAmount == null)
        {
            row.taxAmount = 0.00
        }
        if (row.extra1 == undefined) {
            row.extra1 = 1;
        }
        output +=
            '<tr>\
                <td style="white-space: nowrap;"><button onclick="removeLineItem('+ row.productId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
                <td style="white-space: nowrap;"><button onclick="Utilities.ShowStockCardDialog(' + row.productId + ')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></button></td>\
                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                <td style="white-space: nowrap;" class="Settings_Barcode">' + row.barcode + '</td>\
                <td style="white-space: nowrap;">'+ row.productCode + '</td>\
                <td style="white-space: nowrap;">'+ row.productName + '</td>\
                <td style="white-space: nowrap;">'+ row.itemDescription + '</td>\
                <td style="white-space: nowrap;">'+ row.qty + '</td>\
                <td style="white-space: nowrap;">' + findUnit(row.unitId).unitName + '</td>\
                <td style="white-space: nowrap;">'+ ((findStore(row.extra1).extra1 == undefined) ? "NA" : findStore(row.extra1).extra1) + '</td>\
                <td style="white-space: nowrap;">' + ((findRack(row.rackId).rackName == undefined) ? "NA" : findRack(row.rackId).rackName) + '</td>\
                <td style="white-space: nowrap;">' + ((findBatch(row.BatchId).batchNo == undefined) ? "NA" : findBatch(row.BatchId).batchNo) + '</td>\
                <td style="white-space: nowrap;">' + ((findTax(row.taxId).taxName == undefined) ? "NA" : findTax(row.taxId).taxName) + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency1(row.taxAmount) + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency1(row.rate) + '</td>\
                <td style="white-space: nowrap;">'+ Utilities.FormatCurrency1(row.amount) + '</td>\
            </tr>\
            ';
        totalLineItemAmount = (totalLineItemAmount + row.amount);
        //totalTaxAmount = (totalTaxAmount + parseFloat(row.TaxAmount));
    });
    $("#billAmount").val(Utilities.FormatCurrency(totalLineItemAmount));
    $("#grandTotal").val(Utilities.FormatCurrency(totalLineItemAmount + getTotalTaxAmount()));
    $("#salesOrderLineItemTbody").html(output);
    $("#taxAmountHtml").val(Utilities.FormatCurrency(getTotalTaxAmount()));
    settings.ApplySettings();
}

function removeLineItem(productId) {
    if (confirm("Remove this item?")) {
        var indexOfObjectToRemove = salesOrderLineItems.findIndex(p=>p.ProductId == productId);
        salesOrderLineItems.splice(indexOfObjectToRemove, 1);
        renderLineItems();
    }
}
function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}
function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}
function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    storeId = output.godownId;
    return output;
    
}
function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}
function findProduct(productId) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].ProductId == productId) {
            output = products[i];
            break;
        }
    }
    return output;
}
function findTax(taxId) {
    var output = {};
    for (i = 0; i < allTaxes.length; i++) {
        if (allTaxes[i].taxId == taxId) {
            output = allTaxes[i];
            break;
        }
    }
    return output;
}
function findSalesMan(employeeId) {
    var output = {};
    for (i = 0; i < salesMen.length; i++) {
        if (salesMen[i].employeeId == employeeId) {
            output = salesMen[i];
            break;
        }
    }
    return output;
}
function findPricingLevel(pricingLevelId) {
    var output = {};
    for (i = 0; i < pricingLevel.length; i++) {
        if (pricingLevel[i].pricinglevelId == pricingLevelId) {
            output = pricingLevel[i];
            break;
        }
    }
    return output;
}
function accountLedgerDropDownEditor(container, options) {
    $('<input required name="' + options.field + '" data-text-field="ledgerName" data-value-field="ledgerId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: {
                data: accountLedgers
            },
            template: '<span>#: ledgerName #</span>',
            filter: "contains",
        });
}
function getAccountLedger(ledgerId) {
    for (i = 0; i < customers.length; i++) {
        if (customers[i].ledgerId == ledgerId) {
            return customers[i].ledgerName;
        }
    }
    return "";
}
function getTotalTaxAmount() {
    var totalTax = 0.00;
    for (i = 0; i < salesOrderLineItems.length; i++) {
        totalTax = totalTax + parseFloat(salesOrderLineItems[i].taxAmount);
    }
    return totalTax;
}

function getTotalAdditionalCost(obj) {
    var output = 0.0;
    for (i = 0; i < obj.length; i++) {
        output = output + parseFloat(obj[i].amount);
    }
    totalAdditionalCost = output;
    renderLineItems();
}
function getKendoLineItemIndex(datasource, objectToFindIndex)    //function was created to remove lineitem from row wen its removed from
{                                                               //kendo grid
    for (i = 0; i < datasource.length; i++) {
        if (objectToFindIndex.id == datasource[i].id) {
            return i;
        }
    }
    return -1;
}
function update() {
    //perform validation in another boolean function which calls save() if it returns true
    if ($("#orderNo").val() == "") {
        Utilities.ErrorNotification("Order number required!");
        return;
    }
    else if ($("#transactionDate").val() == "") {
        Utilities.ErrorNotification("Transaction date required!");
        return;
    }
    else if ($("#dueDate").val() == "") {
        Utilities.ErrorNotification("Due date required!");
        return;
    }
    else if ($("#salesMan").val() == "-1") {
        Utilities.ErrorNotification("Select sales person!");
        return;
    }
    else if ($("#store").val() == "") {
        Utilities.ErrorNotification("Select store!");
        return;
    }
    else if (salesOrderLineItems.length < 1) {
        Utilities.ErrorNotification("Line item contains no record!");
        return;
    }
    else {
        var infoSalesOrderMaster = {
            Date: $("#transactionDate").val(),
            DueDate: $("#dueDate").val(),
            LedgerId: $("#customers").val(),
            InvoiceNo: $("#orderNo").val(),
            VoucherNo: $("#orderNo").val(),
            Cancelled: $("#cancelled").prop('checked'),
            //SalesOrderMasterId: 0,
            EmployeeId: $("#salesMan").val(),
            Narration: $("#narration").val(),
            ExchangeRateId: $("#currency").val(),
            TotalAmount: $("#billAmount").val(),
            taxAmount: $("#taxAmountHtml").val(),
            PricinglevelId: $("#pricingLevel").val(),
            QuotationMasterId: quotationMasterId,
            extra1: searchResult.godownId,
            extra2: "",
            salesOrderMasterId: SalesOrderMasterId
        };
        var infoSalesOrderDetails = salesOrderLineItems;

        //populate main object
        var orderToSave = {
            infoSalesOrderDetails: infoSalesOrderDetails,
            infoSalesOrderMaster: infoSalesOrderMaster
        };

        console.log(orderToSave);
        //console.log(orderToSave); //return;
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/SalesOrder/EditSalesOrderdetails",
            type: "Post",
            contentType: "application/json",
            data: JSON.stringify(orderToSave),
            success: function (e) {
                Utilities.Loader.Hide();
                Utilities.SuccessNotification("Order Updated!");
                $("#orderNo").val("");
                $("#transactionDate").val("");
                $("#dueDate").val("");
                $("#dueDate").val("0");
                $("#customers").text("");
                $("#salesMan").text("");
                salesOrderLineItems = [];
                renderLineItems();
                $("#narration").val("");

                window.location = "/Customer/SalesOrder/SalesOrderRegister";
            },
            error: function (e) {
                Utilities.Loader.Hide();
            }
        });
    }
} 

function clear() {
    $("#voucherNo").val("");
    $("#invoiceNo").val("");
    $("#suppliers").val("");
    $("#transactionDate").val();
    $("#suppliers").val();
    $("#invoiceNo").val();
    $("#invoiceDate").val();
    $("#creditPeriod").val();
    $("#currency").val();
    $("#narration").val();
    $("#grandTotal").val("");
    $("#billAmount").val("");
    $("#taxAmountHtml").val("");
}
