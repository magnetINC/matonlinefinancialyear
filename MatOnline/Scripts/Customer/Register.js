﻿var id = 0;
var saleQuotationSearchParameters = {};
var saleOrderSearchParameters = {};
var deliveyNoteSearchParameters = {};
var rejectionInSearchParameters = {};
var salesInvoiceSearchParameters = {};
var PDCReceivableSearchParameters = {};
var receiptSearchParameters = {};
var creditNoteSearchParameters = {};

var orderListForSearch = [];
var table = "";
var table2 = "";
var master = {};
var details = [];
var salesOrderMasterIdToAuthorize = "";
var myLocationOrders = [];
var otherLocationOrders = [];

$(function () {
    //$("#agentNameFilter").change(function () {
    //    filterOrders($("#agentNameFilter").val());
    //});

    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
    $("#backDays2").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
    $("#searchOrders").click(function () {
        //var today = new Date();
        //today = new Date().setDate(today.getDate() + 7);
        //console.log((today.getMonth()) + '/' + (today.getDate()) + '/' + (today.getFullYear())); return;
        //var date = in_a_week;
        //var k = (in_a_week.getMonth()) + '/' + (in_a_week.getDate()) + '/' + (in_a_week.getFullYear());
        //console.log(k);
        //var k = new Date("2019-05-04");
        //return;
        table.destroy();
        if ($("#backDays").val() == "custom") {
            //$(".dtHide").show();
            getSalesQuotationsList($("#fromDate").val(), $("#toDate").val(), $("#orderStatus").val());
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            //$(".dtHide").hide();
            getSalesQuotationsList("2017-01-01", $("#toDate").val(), $("#orderStatus").val());
        }

    });
});

$("#formType").change(function () {
    id = $("#formType").val();
    populateSalesType();
});

function filterOrders(agentName) {
    var output = "";
    for (i = 0; i < orderListForSearch.length; i++) {
        //console.log(agentName + " " + orderListForSearch[i].ledgerName + " " + orderListForSearch[i].ledgerName.toLowerCase().indexOf(agentName));
        if (orderListForSearch[i].ledgerName.toLowerCase().indexOf(agentName.toLowerCase()) >= 0) {
            //output += '<tr>\
            //                <td>'+ (i + 1) + '</td>\
            //                <td></td>\
            //                <td>' + orderListForSearch[i].barcode + '</td>\
            //                <td>' + orderListForSearch[i].productcode + '</td>\
            //                <td>' + orderListForSearch[i].product + '</td>\
            //                <td>' + orderListForSearch[i].extra1 + '</td>\
            //                <td>' + orderListForSearch[i].qty + '</td>\
            //                <td>' + orderListForSearch[i].unit + '</td>\
            //                <td>' + orderListForSearch[i].rate + '</td>\
            //                <td>' + orderListForSearch[i].amount + '</td>\
            //                <td>' + orderListForSearch[i].taxAmount + '</td>\
            //            </tr>';
        }

    }

    //$("#detailsTbody").html(output);
}

function cancelOrder() {
    if (myLocationOrders.findIndex(p=>p.QuotationMasterId == master.QuotationMasterId) >= 0) {
        //if order is in my location, approve directly
        cancelOrderAction();
    }
    else if (otherLocationOrders.findIndex(p=>p.QuotationMasterId == master.QuotationMasterId) >= 0) {
        if (otherLocationOrders.find(p=>p.QuotationMasterId == master.QuotationMasterId).crossChecked == "Pending")    //means order has
        {                                                                                                           //first gm approval
            cancelOrderAction();
        }
        else {
            updateOrderCrossCheckStatus(master.QuotationMasterId, "Cancelled");
        }
    }
}

function cancelOrderAction()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesOrder/updateQuotationStatus?quotationMasterId=" + master.QuotationMasterId + "&status=Cancelled",
        type: "Get",                                                                    //Cancelled is not in quotes cz the parameter will be
        contentType: "application/json",                                                //wrapped in quotes in the API
        success: function (e) {
            Utilities.Loader.Hide();
            Utilities.SuccessNotification("Order cancelled!");

            //$("#detailsModal").modal("hide");

            //if ($("#orderStatus").val() == "custom") {
            //    getSalesQuotationsList($("#fromDate").val(), $("#toDate").val(), $("#orderStatus").val());
            //}
            //else {
            //    getSalesQuotationsList("2017-01-01", $("#toDate").val(), $("#orderStatus").val());
            //}
            window.location = "/Customer/SalesOrder/OrderConfirmationListing";
        },
        error: function (e) {
            Utilities.Loader.Hide();
        }
    });
}

function approveOrder() {
    
    if (myLocationOrders.findIndex(p=>p.QuotationMasterId == master.QuotationMasterId)>=0)
    {
        //if order is in my location, approve directly
        approveOrderAction();
    }
    else if (otherLocationOrders.findIndex(p=>p.QuotationMasterId == master.QuotationMasterId) >= 0)
    {
        if(otherLocationOrders.find(p=>p.QuotationMasterId == master.QuotationMasterId).crossChecked=="Pending")    //means order has
        {                                                                                                           //first gm approval
            approveOrderAction();
        }
        else
        {
            updateOrderCrossCheckStatus(master.QuotationMasterId,"Pending");
        }
    }
}

function approveOrderAction()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerCentre/GetCustomerBalance?ledgerId=" + master.LedgerId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            //var balance = -1 * data;    //-1 to negate it and change the value to +ve
            var totalLineItemAmount = master.TotalAmount + master.TaxAmount;
            var balance = data;

            if (balance >= totalLineItemAmount) {
                var infoSalesOrderMaster = {
                    Date: "",
                    DueDate: "",
                    LedgerId: master.LedgerId,
                    InvoiceNo: master.InvoiceNo,
                    VoucherNo: master.InvoiceNo,
                    UserId: master.UserId,
                    Cancelled: 0,
                    //SalesOrderMasterId: 0,
                    EmployeeId: master.EmployeeId,
                    Narration: "",
                    ExchangeRateId: 1,
                    QuotationMasterId: 0,
                    TotalAmount: master.TotalAmount,
                    taxAmount: master.TaxAmount,
                    PricinglevelId: 0,
                    QuotationMasterId: master.QuotationMasterId,
                };
                var infoSalesOrderDetails = details;

                //populate main object
                var orderToSave = {
                    infoSalesOrderDetails: infoSalesOrderDetails,
                    infoSalesOrderMaster: infoSalesOrderMaster
                };
                //console.log(orderToSave); return;

                $.ajax({
                    url: API_BASE_URL + "/SalesOrder/SaveSalesOrder",
                    type: "Post",
                    contentType: "application/json",
                    data: JSON.stringify(orderToSave),
                    success: function (e) {
                        Utilities.Loader.Hide();
                        Utilities.SuccessNotification("Order approved!");
                        //if ($("#orderStatus").val() == "custom") {
                        //    getSalesQuotationsList($("#fromDate").val(), $("#toDate").val(), $("#orderStatus").val());
                        //}
                        //else {
                        //    getSalesQuotationsList("2017-01-01", $("#toDate").val(), $("#orderStatus").val());
                        //}
                        window.location = "/Customer/SalesOrder/OrderConfirmationListing";
                    },
                    error: function (e) {
                        Utilities.Loader.Hide();
                    }
                });
            }
            else {
                Utilities.Loader.Hide();
                Utilities.ErrorNotification("Insufficient agent balance.<br/>Agent Balance: " + balance);
            }
            $("#detailsModal").modal("hide");
            table.destroy();

        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry,could not get agent balance at this time.");
        }
    });
}

function updateOrderCrossCheckStatus(orderMasterId,status)
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesQuotation/UpdateOrderCrossCheckStatus?orderId="+orderMasterId+"&status="+status,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            if (data == true)
            {
                if (status == "Pending" || status == "Approved")
                {
                    Utilities.SuccessNotification("Order approved!");
                }
                else
                {
                    Utilities.SuccessNotification("Order cancelled!");
                }
                window.location = "/Customer/SalesOrder/OrderConfirmationListing";
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function authorizeOrder(salesOrderMasterId) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesQuotation/AuthorizeOrder?salesOrderMasterId=" + salesOrderMasterId,
        type: "Get",
        contentType: "application/json",
        success: function (e) {
            Utilities.Loader.Hide();
            $("#detailsModal").modal("hide");
            Utilities.SuccessNotification("Order authorized!");
            Utilities.Loader.Show();
            if(table != "")
            {
                table.destroy();
            }
            getAuthorizationListing($("#fromDate").val(), $("#toDate").val(), $("#orderStatus").val());
            //table.destroy();
            //if ($(this).val() == "custom") {
            //    getSalesQuotationsList($("#fromDate").val(), $("#toDate").val(), $("#orderStatus").val());
            //}
            //else {
            //    getSalesQuotationsList("2017-01-01", $("#toDate").val(), $("#orderStatus").val());
            //}
        },
        error: function (e) {
            Utilities.Loader.Hide();
        }
    });
}

$(function () {
    $.ajax({
        url: API_BASE_URL + "/Register/CashOrPartyComboFill",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            //console.log(data);
            $("#customers").html(Utilities.PopulateDropDownFromArray(data, 1, 0));
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
});
$(function () {
    $.ajax({
        url: API_BASE_URL + "/Register/voucherTypeComboFill",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            //console.log(data);
            $("#voucherType").html(Utilities.PopulateDropDownFromArray(data, 0, 1));
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
});
$(function () {
    $.ajax({
        url: API_BASE_URL + "/Register/voucherTypeComboFill",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            //console.log(data);
            $("#formType").html(Utilities.PopulateDropDownFromArray(data, 0, 1));
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
});
$(function () {
    $.ajax({
        url: API_BASE_URL + "/Register/AccountLedgerComboFill",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            //console.log(data);
            $("#accountLedger").html(Utilities.PopulateDropDownFromArray(data, 2, 2));
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
});
$(function () {
    $.ajax({
        url: API_BASE_URL + "/Register/cashOrBankComboFill",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            //console.log(data);
            $("#cashOrBank").html(Utilities.PopulateDropDownFromArray(data, 0, 2));
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
});

function populateSalesType() {
    $.ajax({
        url: API_BASE_URL + "/Register/SalesModeComboFill?id=" + id,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            //$("#cashOrBank").html(Utilities.PopulateDropDownFromArray(data, 0, 2));
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getQuotationTransaction(quotationMasterId) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetSalesQuotationForConfirmation?salesQuotationMasterId=" + quotationMasterId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var userObj = localStorage.getItem("MAT_USER_INFO");

            master = data.Master;
            details = data.Details;
            var statusOutput = "";
            //console.log("master",master);
            if (master.Approved == "Pending") {
                //$("#approveOrder").show();
                //$("#cancelOrder").show();
                statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending</span>';
            }
            else if (master.Approved == "Approved") {
                //$("#approveOrder").hide();
                //$("#cancelOrder").hide();
                statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Approved</span>';
            }
            else if (master.Approved == "Cancelled") {
                //$("#approveOrder").hide();
                //$("#cancelOrder").hide();
                statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
            }
            
            var myLoc = myLocationOrders.find(p=>p.QuotationMasterId == quotationMasterId);
            var otherLoc = otherLocationOrders.find(p=>p.QuotationMasterId == quotationMasterId);
            if (myLoc != null)
            {
                if (master.Approved == "Pending") {
                    $("#approveOrder").show();
                    $("#cancelOrder").show();
                }
                else if (master.Approved == "Approved") {
                    $("#approveOrder").hide();
                    $("#cancelOrder").hide();
                }
                else if (master.Approved == "Cancelled") {
                    $("#approveOrder").hide();
                    $("#cancelOrder").hide();
                }
            }
            else if (otherLoc != null)
            {
                if (master.Approved == "Pending")
                {                    
                    if (JSON.parse(userObj).StoreId == details[0].extra1 && otherLoc.CrossChecked == "")
                    {
                        $("#approveOrder").show();
                        $("#cancelOrder").show();
                    }
                    else if (JSON.parse(userObj).StoreId == details[0].extra1 && otherLoc.CrossChecked == "Pending")
                    {
                        $("#approveOrder").show();
                        $("#cancelOrder").show();
                    }
                    else if (JSON.parse(userObj).StoreId != details[0].extra1 && otherLoc.CrossChecked == "")
                    {
                        $("#approveOrder").show();
                        $("#cancelOrder").show();
                    }
                    else if (JSON.parse(userObj).StoreId != details[0].extra1 && otherLoc.CrossChecked == "Pending")
                    {
                        $("#approveOrder").hide();
                        $("#cancelOrder").hide();
                    }
                }
                else if (master.Approved == "Approved") {
                    $("#approveOrder").hide();
                    $("#cancelOrder").hide();
                }
                else if (master.Approved == "Cancelled") {
                    $("#approveOrder").hide();
                    $("#cancelOrder").hide();
                }
            }

            //var myLocationCheck = myLocationOrders.find(p=>p.QuotationMasterId == quotationMasterId);
          
            //if (JSON.parse(userObj).StoreId != details[0].extra1)
            //{
            //    var otherLocationChk = otherLocationOrders.find(p=>p.QuotationMasterId == quotationMasterId);
            //    //var myLocationChk = myLocationOrders.find(p=>p.QuotationMasterId == quotationMasterId);
            //    console.log("other",otherLocationChk);
            //    //console.log("my",myLocationChk);
            //    if ((otherLocationChk != undefined && otherLocationChk.crossChecked == "Pending") /**|| (myLocationChk!=undefined && myLocationChk.crossChecked == "Pending")**/)
            //    {
            //        $("#approveOrder").hide();
            //        $("#cancelOrder").hide();
            //    }
            //    else
            //    {
            //        $("#approveOrder").show();
            //        $("#cancelOrder").show();
            //    }
            //}
            //else if (JSON.parse(userObj).StoreId == details[0].extra1) {
            //    //var otherLocationChk = otherLocationOrders.find(p=>p.QuotationMasterId == quotationMasterId);
            //    var myLocationChk = myLocationOrders.find(p=>p.QuotationMasterId == quotationMasterId);
            //    console.log("other", myLocationChk); return;
            //    console.log("my", myLocationChk != undefined);
            //    console.log("my", myLocationChk.approved == "Pending");
            //    if ((myLocationChk != undefined && myLocationChk.approved == "Pending") /**|| (myLocationChk!=undefined && myLocationChk.crossChecked == "Pending")**/) {
            //        $("#approveOrder").show();
            //        $("#cancelOrder").show();
            //    }
            //    else {
            //        $("#approveOrder").hide();
            //        $("#cancelOrder").hide();
            //    }
            //}
            $("#orderNoDiv").html(master.InvoiceNo);
            $("#orderDateDiv").html(Utilities.FormatJsonDate(master.Date));
            $("#orderNoDiv").html(master.InvoiceNo);
            $("#raisedByDiv").html(master.User);
            $("#raisedForDiv").html(master.CustomerName);
            $("#statusDiv").html(statusOutput);
            $("#orderAmountTxt").val(Utilities.FormatCurrency(master.TotalAmount));
            $("#taxAmountTxt").val(Utilities.FormatCurrency(master.TaxAmount));
            $("#grandTotalTxt").val(Utilities.FormatCurrency(master.TotalAmount + master.TaxAmount));
            var output = "";
            for (i = 0; i < details.length; i++) {
                output += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td><button onclick="getStoreQuantityByStoreId(' + details[i].productId + ',\'' + details[i].extra1 + '\')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></button></td>\
                            <td>' + details[i].barcode + '</td>\
                            <td>' + details[i].productcode + '</td>\
                            <td>' + details[i].product + '</td>\
                            <td>' + details[i].storeName + '</td>\
                            <td>' + details[i].qty + '</td>\
                            <td>' + details[i].unit + '</td>\
                            <td>' + details[i].rate + '</td>\
                            <td>' + details[i].amount + '</td>\
                            <td>' + details[i].taxAmount + '</td>\
                        </tr>';
            }
            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getStoreQuantity(productId, storeName) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/ProductStockCard?productId=" + productId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            //availableQuantityDiv
            var stkCard = data.StockCardLocations;
            var storeIndex = stkCard.findIndex(p=>p.StoreName == storeName);
            console.log(stkCard[storeIndex]);
            $("#availableQuantityDiv").html("<b>" + stkCard[storeIndex].QuantityOnHand + "</b> units available in <b>" + stkCard[storeIndex].StoreName + "</b>");
            Utilities.Loader.Hide();
        },
        error: function (error) {
            Utilities.Loader.Hide();
        }
    });
}

function getStoreQuantityByStoreId(productId, storeId) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/ProductStockCard?productId=" + productId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            //availableQuantityDiv
            var stkCard = data.StockCardLocations;
            var storeIndex = stkCard.findIndex(p=>p.StoreId == storeId);
            console.log(stkCard[storeIndex]);
            $("#availableQuantityDiv").html("<b>" + stkCard[storeIndex].QuantityOnHand + "</b> units available in <b>" + stkCard[storeIndex].StoreName + "</b>");
            Utilities.Loader.Hide();
        },
        error: function (error) {
            Utilities.Loader.Hide();
        }
    });
}

function getSalesQuotationsList(fromDate, toDate, condition) {
    Utilities.Loader.Show();
    saleQuotationSearchParameters = {
        fromDate: fromDate,
        toDate: toDate,
        ledgerId: 0,
        gmUserId: matUserInfo.UserId,
        qoutationNo: $("#quotationNo").val(),
        condition: condition
    };

    var myLocationOrdersAjax = $.ajax({
            url: API_BASE_URL + "/Register/OrderConfirmationListing",
            type: "Post",
            data: JSON.stringify(saleQuotationSearchParameters),
            contentType: "application/json"
        });

    var otherLocationOrdersAjax = $.ajax({
        url: API_BASE_URL + "/Register/OtherLocationOrderConfirmationListing",
            type: "Post",
            data: JSON.stringify(saleQuotationSearchParameters),
            contentType: "application/json"
    });

    $.when(myLocationOrdersAjax, otherLocationOrdersAjax)
    .done(function (dataMyLocation,dataOtherLocation) {
        orderListForSearch = dataMyLocation[2].responseJSON;
        myLocationOrders = dataMyLocation[2].responseJSON;
        otherLocationOrders = dataOtherLocation[2].responseJSON;
                var output = "";
                var objToShow = [];
                var objToShow2 = [];

                $("#myLocationCount").html(dataMyLocation[2].responseJSON.length);
                $("#otherLocationCount").html(dataOtherLocation[2].responseJSON.length);         

                $.each(dataMyLocation[2].responseJSON, function (count, row) {

                    var status = "";
                    if (row.Approved == "Pending")
                    {
                        status = '<label class="label label-warning">Pending</label>';
                    }
                    else if (row.Approved == "Approved") {
                        status = '<label class="label label-success">Approved</label>';
                    }
                    else if (row.Approved == "Cancelled") {
                        status = '<label class="label label-danger">Cancelled</label>';
                    }
                    objToShow.push([
                        count + 1,
                        row.InvoiceNo,
                        row.Date,
                        row.LedgerName,
                        '&#8358;' + Utilities.FormatCurrency(parseFloat((row.TotalAmount) + parseFloat(row.TaxAmount))),
                        row.FirstName+" "+row.LastName,
                        status,
                        '<button type="button" class="btn btn-primary btn-sm" onclick="getQuotationTransaction(' + row.QuotationMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                    ]);
                });

                $.each(dataOtherLocation[2].responseJSON, function (count, row) {

                    var status = "";
                    if (row.CrossChecked == "") {
                        status = '<label class="label label-warning">Pending Confirmation</label>';
                    }
                    if (row.CrossChecked == "Pending") {
                        status = '<label class="label label-warning">Pending Approval</label>';
                    }
                    else if (row.CrossChecked == "Approved") {
                        status = '<label class="label label-success">Approved</label>';
                    }
                    else if (row.CrossChecked == "Cancelled") {
                        status = '<label class="label label-danger">Cancelled</label>';
                    }
                    objToShow2.push([
                        count + 1,
                        row.InvoiceNo,
                        row.Date,
                        row.LedgerName,
                        '&#8358;' + Utilities.FormatCurrency(parseFloat((row.TotalAmount) + parseFloat(row.TaxAmount))),
                        row.FirstName + " " + row.LastName,
                        status,
                        '<button type="button" class="btn btn-primary btn-sm" onclick="getQuotationTransaction(' + row.QuotationMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                    ]);
                });

                //if (table != "")
                //{
                //    table.destroy();
                //}
                table = $('#salesQuotationListTable').DataTable({
                    data: objToShow,
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });

                //if (table2 != "") {
                //    table2.destroy();
                //}
                table2 = $('#salesQuotationListTable2').DataTable({
                    data: objToShow2,
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });               
                Utilities.Loader.Hide();
    });

    //$.ajax({
    //    url: API_BASE_URL + "/Register/OrderConfirmationListing",
    //    type: "Post",
    //    data: JSON.stringify(saleQuotationSearchParameters),
    //    contentType: "application/json",
    //    success: function (data) {
    //        console.log(data);
    //        orderListForSearch = data;
    //        var output = "";
    //        var objToShow = [];
    //        $.each(data, function (count, row) {
               
    //            var status = "";
    //            if (row.approved == "Pending")
    //            {
    //                status = '<label class="label label-warning">Pending</label>';
    //            }
    //            else if (row.approved == "Approved") {
    //                status = '<label class="label label-success">Approved</label>';
    //            }
    //            else if (row.approved == "Cancelled") {
    //                status = '<label class="label label-danger">Approved</label>';
    //            }
    //            objToShow.push([
    //                count + 1,
    //                row.invoiceNo,
    //                row.date,
    //                row.ledgerName,
    //                '&#8358;' + Utilities.FormatCurrency(parseFloat(row.totalAmount)),
    //                row.userName,
    //                status,
    //                '<button type="button" class="btn btn-primary btn-sm" onclick="getQuotationTransaction(' + row.QuotationMasterId + ')"><i class="fa fa-eye"></i> View</button>'
    //            ]);
    //        });
    //        if (table != "")
    //        {
    //            table.destroy();
    //        }
    //        table = $('#salesQuotationListTable').DataTable({
    //            data: objToShow,
    //            "paging": true,
    //            "lengthChange": true,
    //            "searching": true,
    //            "ordering": true,
    //            "info": true,
    //            "autoWidth": true
    //        });
    //        //$("#salesQuotationListTbody").html(output);
    //        Utilities.Loader.Hide();
    //    },
    //    error: function (err) {
    //        Utilities.Loader.Hide();
    //    }
    //});
}

function getSalesQuotationsList2(fromDate, toDate, condition) {
    Utilities.Loader.Show();
    saleQuotationSearchParameters = {
        fromDate: fromDate,
        toDate: toDate,
        ledgerId: 0,
        gmUserId: matUserInfo.UserId,
        qoutationNo: $("#quotationNo").val(),
        condition: condition
    };

    var myLocationOrdersAjax = $.ajax({
        url: API_BASE_URL + "/Register/OrderConfirmationListing",
        type: "Post",
        data: JSON.stringify(saleQuotationSearchParameters),
        contentType: "application/json"
    });

    var otherLocationOrdersAjax = $.ajax({
        url: API_BASE_URL + "/Register/OtherLocationOrderConfirmationListing",
        type: "Post",
        data: JSON.stringify(saleQuotationSearchParameters),
        contentType: "application/json"
    });

    $.when(myLocationOrdersAjax, otherLocationOrdersAjax)
    .done(function (dataMyLocation, dataOtherLocation) {
        orderListForSearch = dataMyLocation[2].responseJSON;
        var output = "";
        var objToShow = [];
        $.each(dataMyLocation[2].responseJSON, function (count, row) {

            var status = "";
            if (row.approved == "Pending") {
                status = '<label class="label label-warning">Pending</label>';
            }
            else if (row.approved == "Approved") {
                status = '<label class="label label-success">Approved</label>';
            }
            else if (row.approved == "Cancelled") {
                status = '<label class="label label-danger">Cancelled</label>';
            }
            objToShow.push([
                count + 1,
                row.invoiceNo,
                row.date,
                row.ledgerName,
                '&#8358;' + Utilities.FormatCurrency(parseFloat(row.totalAmount)),
                row.userName,
                status,
                '<button type="button" class="btn btn-primary btn-sm" onclick="getQuotationTransaction(' + row.QuotationMasterId + ')"><i class="fa fa-eye"></i> View</button>'
            ]);
        });
        if (table != "") {
            table.destroy();
        }
        table = $('#salesQuotationListTable').DataTable({
            data: objToShow,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
        //$("#salesQuotationListTbody").html(output);
        Utilities.Loader.Hide();
    });

    //$.ajax({
    //    url: API_BASE_URL + "/Register/OrderConfirmationListing",
    //    type: "Post",
    //    data: JSON.stringify(saleQuotationSearchParameters),
    //    contentType: "application/json",
    //    success: function (data) {
    //        console.log(data);
    //        orderListForSearch = data;
    //        var output = "";
    //        var objToShow = [];
    //        $.each(data, function (count, row) {

    //            var status = "";
    //            if (row.approved == "Pending")
    //            {
    //                status = '<label class="label label-warning">Pending</label>';
    //            }
    //            else if (row.approved == "Approved") {
    //                status = '<label class="label label-success">Approved</label>';
    //            }
    //            else if (row.approved == "Cancelled") {
    //                status = '<label class="label label-danger">Approved</label>';
    //            }
    //            objToShow.push([
    //                count + 1,
    //                row.invoiceNo,
    //                row.date,
    //                row.ledgerName,
    //                '&#8358;' + Utilities.FormatCurrency(parseFloat(row.totalAmount)),
    //                row.userName,
    //                status,
    //                '<button type="button" class="btn btn-primary btn-sm" onclick="getQuotationTransaction(' + row.QuotationMasterId + ')"><i class="fa fa-eye"></i> View</button>'
    //            ]);
    //        });
    //        if (table != "")
    //        {
    //            table.destroy();
    //        }
    //        table = $('#salesQuotationListTable').DataTable({
    //            data: objToShow,
    //            "paging": true,
    //            "lengthChange": true,
    //            "searching": true,
    //            "ordering": true,
    //            "info": true,
    //            "autoWidth": true
    //        });
    //        //$("#salesQuotationListTbody").html(output);
    //        Utilities.Loader.Hide();
    //    },
    //    error: function (err) {
    //        Utilities.Loader.Hide();
    //    }
    //});
}

function getSalesOrderList() {
    saleOrderSearchParameters = {
        fromDate: $("#fromDate").val(),
        toDate: $("#toDate").val(),
        gmUserId: matUserInfo.UserId,
        ledgerId: $("#customers").val(),
        salesOrderNo: $("#salesOrderNo").val(),
        condition: $("#condition").val()
    };

    $.ajax({
        url: API_BASE_URL + "/Register/SalesOrder",
        type: "Post",
        data: JSON.stringify(saleOrderSearchParameters),
        contentType: "application/json",
        success: function (data) {
            console.log(data);

            var output = "";
            $.each(data, function (count, row) {
                output +=
                  '<tr>\
                        <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                        <td style="white-space: nowrap;">' + row.invoiceNo + '</td>\
                        <td style="white-space: nowrap;">' + row.voucherTypeName + '</td>\
                        <td style="white-space: nowrap;">' + row.date + '</td>\
                        <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.totalAmount) + '</td>\
                        <td style="white-space: nowrap;">' + row.narration + '</td>\
                        <td style="white-space: nowrap;">' + row.dueDate + '</td>\
                        <td style="white-space: nowrap;">' + row.currencyName + '</td>\
                        <td style="white-space: nowrap;">' + row.userName + '</td>\
                        <td style="white-space: nowrap;"><a type="button" class="btn btn-primary btn-labeled" \
                         href="/Customer/SalesOrder/SalesOrderRegisterDetails/' + row.salesOrderMasterId + '">View Transactions</a></td>\
                    </tr>\
                    ';
            });
            $("#salesOrderListTbody").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getAuthorizationListing(fromDate, toDate, status) {
    Utilities.Loader.Show();
    saleOrderSearchParameters = {
        fromDate: fromDate,
        toDate: toDate,
        ledgerId: -1,
        gmUserId: matUserInfo.UserId,
        salesOrderNo: "All",
        condition: "Pending"    //in db stored procedure, pending means order is not cancelled. i.e cancelled=false
    };
    $.ajax({
        url: API_BASE_URL + "/Register/SalesOrder",
        type: "Post",
        data: JSON.stringify(saleOrderSearchParameters),
        contentType: "application/json",
        success: function (data) {
            // console.log(data);
            var output = "";
            var objToShow = [];
            //var table = "";
            if (data!=null)
            {
                $.each(data, function (count, row) {
                    var statusOP = "";
                    if (row.AuthorizationStatus == "Pending") {
                        statusOP = '<span class="label label-warning">Pending</span>';
                    }
                    else if (row.AuthorizationStatus == "Cancelled") {
                        statusOP = '<span class="label label-danger">Cancelled</span>';
                    }
                    else if (row.AuthorizationStatus == "Processed") {
                        statusOP = '<span class="label label-mint"> Processed</span>';
                    }
                    else {
                        statusOP = '<span class="label label-success"> Authorized</span>';
                    }
                    if (status == row.AuthorizationStatus) {
                        objToShow.push([
                        (count + 1),
                        row.QuotationNo,
                        row.ledgerName,
                        row.date,
                        '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
                        row.userName,
                        statusOP,
                        '<button type="button" class="btn btn-primary btn-sm" onclick="getOrderAuthorizationDetails(' + row.salesOrderMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                        ]);
                    }
                    else if (status == "") {
                        objToShow.push([
                        (count + 1),
                        row.QuotationNo,
                        row.ledgerName,
                        row.date,
                        //'&#8358;' + Utilities.FormatCurrency(row.totalAmount),
                        row.userName,
                        statusOP,
                        '<button type="button" class="btn btn-primary btn-sm" onclick="getOrderAuthorizationDetails(' + row.salesOrderMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                        ]);
                    }

                });
            }
            console.log(objToShow);
            //if (table != "")
            //{
            //    table.destroy();
            //}
            
            table = $('#authTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            console.log(objToShow);
            Utilities.Loader.Hide();
            //$("#av").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getReleaseFormListing(fromDate, toDate,status) {
    Utilities.Loader.Show();
    saleOrderSearchParameters = {
        fromDate: fromDate+" 00:00:00",
        toDate: toDate + " 23:59:00",
        UserId: matUserInfo.UserId,
        //ledgerId: -1,
        //salesOrderNo: "All",
        condition: status
        //condition: "Pending"    //in db stored procedure, pending means order is not cancelled. i.e cancelled=false
    };

    $.ajax({
        url: API_BASE_URL + "/Register/WaybillListing",
        type: "Post",
        data: JSON.stringify(saleOrderSearchParameters),
        contentType: "application/json",
        success: function (data) {
            // console.log(data);
            var output = "";
            var objToShow = [];
            $.each(data, function (count, row) {
                var statusText = "";
                var actionButton = "";

                //Alex logic: consultants and clients want to c pending but in d db, what they want as pending is actually authorized
                //            but yet-to-be-processed orders so i treate backend 'Authorized' as 'Pending' on frontend

                //if (row.AuthorizationStatus == "Pending")
                //{
                //    statusText = '<span class="label label-warning">Pending</span>';
                //    actionButton = '<span class="label label-warning">Awaiting Authorizaton</span>';
                //}
                if(row.Status == "Authorized")
                {
                    statusText = '<span style="color:#e79824;">Pending</span>';
                    actionButton = '<a class="btn btn-warning" \
                         href="/Customer/DeliveryNote/Index?deliveryMode=Against Order&orderNo=' + row.SalesOrderMasterId + '">Process&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a>'
                }
                else if (row.Status == "Processed") {
                    statusText = '<span style="color:green;">Processed</span>';
                    actionButton = '<button class="btn btn-success">Completed</button>';
                }
                if (status == "Authorized")
                {
                    if (row.Status == 'Authorized') {
                        objToShow.push([
                        (count + 1),
                        row.AgentName,
                        row.VoucherNo,
                        row.OrderDate,
                        row.AuthorizationDate,
                        statusText,
                        actionButton
                        ]);
                    }
                }
                else if (status == "Processed")
                {
                    if (row.Status == 'Processed')
                    {
                        objToShow.push([
                        (count + 1),
                        row.AgentName,
                        row.VoucherNo,
                        row.OrderDate,
                        row.AuthorizationDate,
                        statusText,
                        actionButton
                        ]);
                    }
                }
                else if (status == "All")
                {
                    objToShow.push([
                        (count + 1),
                        row.AgentName,
                        row.VoucherNo,
                        row.OrderDate,
                        row.AuthorizationDate,
                        statusText,
                        actionButton
                    ]);
                }
                
            });
            //if (table != "")
            //{
            //    table.destroy();
            //}
            table = $('#releaseFormListingTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            Utilities.Loader.Hide();
            //$("#av").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getConfirmedSalesOrders(fromDate, toDate, condition, filter) {
    saleOrderSearchParameters = {
        fromDate: fromDate,
        toDate: toDate,
        ledgerId: -1,
        salesOrderNo: "All",
        condition: condition
    };

    deliveyNoteSearchParameters = {
        fromDate: fromDate,
        toDate: toDate,
        ledgerId: 0,
        deliveryNoteNo: "",
        userId: matUserInfo.UserId
    };
    Utilities.Loader.Show();
    var orderAjax = $.ajax({
        url: API_BASE_URL + "/Register/SalesOrder",
        type: "Post",
        data: JSON.stringify(saleOrderSearchParameters),
        contentType: "application/json"
    });

    var deliveryNoteAjax = $.ajax({
        url: API_BASE_URL + "/Register/DeliveryNoteForProcessing",
        type: "Post",
        data: JSON.stringify(deliveyNoteSearchParameters),
        contentType: "application/json"
    });

    $.when(orderAjax, deliveryNoteAjax)
    .done(function (dataOrder, dataDeliveryNote) {
        console.log(dataOrder);
        console.log(dataDeliveryNote);
        var objToShow = [];
        //salesOrder list
        var orders = dataOrder[0];

        //processed and unprocessed 
        var deliveries = dataDeliveryNote[0];

        if (filter == "SO") {
            objToShow = [];
            $.each(orders, function (count, row) {
                objToShow.push([
                    count + 1,
                    row.invoiceNo,
                    row.date,
                    row.ledgerName,
                    '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
                    row.userName,
                    '<button type="button" class="btn btn-primary btn-sm" onclick="redirectToReleaseForm(' + row.salesOrderMasterId + ')"><i class="fa fa-eye"></i> Deliver</button>'
                ]);
            });
        }
        else if (filter == "DN") {
            var actionButton = "";

            objToShow = [];
            $.each(deliveries, function (count, row) {
                if (row.status == "Processed") {
                    actionButton = 'Processed';
                }
                else if (row.status == "unprocessed") {
                    actionButton = '<button type="button" class="btn btn-primary btn-sm" onclick="redirectToReleaseForm(' + row.deliveryNoteMasterId + ')"><i class="fa fa-eye"></i> Authorize</button>';
                }
                objToShow.push([
                    (count + 1),    //so numbering can continue from where orders stopped
                    row.invoiceNo,
                    row.Date,
                    row.CashOrParty,
                    '&#8358;' + row.Amount,
                    row.userName,
                    actionButton
                ]);
            });
        }
        Utilities.Loader.Hide();
        if (table != "") {
            table.destroy();
        }
        table = $('#salesOrderConfirmationTable').DataTable({
            data: objToShow,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });

    });


    //$.ajax({
    //    url: API_BASE_URL + "/Register/SalesOrder",
    //    type: "Post",
    //    data: JSON.stringify(saleOrderSearchParameters),
    //    contentType: "application/json",
    //    success: function (data) {
    //        //console.log(data);
    //        //console.log("yaaaga");
    //        //return;
    //        var output = "";
    //        var objToShow = [];
    //        $.each(data, function (count, row) {
    //            objToShow.push([
    //                count + 1,
    //                row.invoiceNo,
    //                row.date,
    //                row.ledgerName,
    //                '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
    //                row.userName,
    //                '<button type="button" class="btn btn-primary btn-sm" onclick="redirectToReleaseForm(' + row.salesOrderMasterId + ')"><i class="fa fa-eye"></i> Deliver</button>'
    //            ]);
    //        });
    //        table = $('#salesOrderConfirmationTable').DataTable({
    //            data: objToShow,
    //            "paging": true,
    //            "lengthChange": true,
    //            "searching": true,
    //            "ordering": true,
    //            "info": true,
    //            "autoWidth": true
    //        });
    //        Utilities.Loader.Hide();
    //    },
    //    error: function (err) {
    //        Utilities.Loader.Hide();
    //    }
    //});
}

function redirectToReleaseForm(salesOrderMasterId) {
    window.location = "/Customer/DeliveryNote/Index?orderNo=" + salesOrderMasterId;
}

function getDeliveryNoteList(fromDate, toDate) {
    deliveyNoteSearchParameters = {
        fromDate: fromDate,
        toDate: toDate,
        ledgerId: 0,
        deliveryNoteNo: "",
        userId: matUserInfo.UserId
    };

    console.log(deliveyNoteSearchParameters);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Register/DeliveryNote",
        type: "Post",
        data: JSON.stringify(deliveyNoteSearchParameters),
        contentType: "application/json",
        success: function (data) {
            console.log(data);

            var output = "";
            var objToShow = [];
            $.each(data, function (count, row) {
                //output +=
                //  '<tr>\
                //    <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                //    <td style="white-space: nowrap;">' + row.invoiceNo + '</td>\
                //    <td style="white-space: nowrap;">' + row.Date + '</td>\
                //    <td style="white-space: nowrap;">' + row.CashOrParty + '</td>\
                //    <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.Amount) + '</td>\
                //    <td style="white-space: nowrap;">' + row.narration + '</td>\
                //    <td style="white-space: nowrap;">' + row.currencyName + '</td>\
                //    <td style="white-space: nowrap;">' + row.userName + '</td>\
                //    <td style="white-space: nowrap;">' + row.OrderNoOrQuotationNo + '</td>\
                //    <td style="white-space: nowrap;"><a href="/Customer/DeliveryNote/GenerateWayBill?rid=' + row.deliveryNoteMasterId + '" class="btn btn-primary">Confirm/Generate Way Bill</a></td>\
                //</tr>\
                //';
                objToShow.push([
                count + 1,
                row.OrderNoOrQuotationNo,
                row.Date,
                row.CashOrParty,
                row.userName,
                '<a href="/Customer/DeliveryNote/GenerateWayBill?rid=' + row.deliveryNoteMasterId + '" class="btn btn-primary">Confirm/Generate Way Bill</a>'
                ]);
            });
            if (table != "") {
                table.destroy();
            }
            table = $('#deliveryNoteListTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            Utilities.Loader.Hide();
            //$("#deliveryNoteListTbody").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getWaybillGenerationListing(fromDate, toDate, status) {
    var param = {
        FromDate: fromDate,
        ToDate: toDate,
        Status: status
    };

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/DeliveryNote/GetDeliveryNotes",
        type: "Post",
        data: JSON.stringify(param),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var output = "";
            var objToShow = [];
            $.each(data, function (count, row) {                
                
                if(row.Status=="Processed")
                {
                    objToShow.push([
                        count + 1,
                        row.OrderNo,
                        row.Date,
                        row.LedgerName,
                        row.User,
                        '<button onclick="showWaybillModal(' + row.DeliverynoteMasterId + ')" class="btn btn-success">Print Way Bill</button>'
                    ]);
                }
                else if(row.Status=="Unprocessed")
                {
                    objToShow.push([
                        count + 1,
                        row.OrderNo,
                        row.Date,
                        row.LedgerName,
                        row.User,
                        '<a href="/Customer/DeliveryNote/GenerateWayBill?rid=' + row.DeliverynoteMasterId + '" class="btn btn-primary">Confirm/Generate Way Bill</a>'
                    ]);
                }
            });
            //if (table != "") {
            //    table.destroy();
            //}
            table = $('#deliveryNoteListTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            Utilities.Loader.Hide();
            //$("#deliveryNoteListTbody").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getDeliveryNoteListForProcessing() {
    deliveyNoteSearchParameters = {
        fromDate: $("#fromDate").val(),
        toDate: $("#toDate").val(),
        ledgerId: $("#customers").val(),
        deliveryNoteNo: "",
        userId: matUserInfo.UserId
    };

    $.ajax({
        url: API_BASE_URL + "/Register/DeliveryNoteForProcessing",
        type: "Post",
        data: JSON.stringify(deliveyNoteSearchParameters),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var status = "";

            var output = "";
            if (data != null && data.length > 0) {
                $.each(data, function (count, row) {
                    if (row.status == 'Processed') {
                        status = '<span class="label label-success"><i class="fa fa-check-circle"></i> Approved</span>';
                    }
                    else {
                        status = '<span class="label label-warning"><i class="fa fa-question-circle"></i> Pending</span>';
                    }
                    output +=
                      '<tr>\
                        <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                        <td style="white-space: nowrap;">' + row.invoiceNo + '</td>\
                        <td style="white-space: nowrap;">' + row.Date + '</td>\
                        <td style="white-space: nowrap;">' + row.CashOrParty + '</td>\
                        <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.Amount) + '</td>\
                        <td style="white-space: nowrap;">' + row.narration + '</td>\
                        <td style="white-space: nowrap;">' + row.currencyName + '</td>\
                        <td style="white-space: nowrap;">' + row.userName + '</td>\
                        <td style="white-space: nowrap;">' + row.OrderNoOrQuotationNo + '</td>\
                        <td style="white-space: nowrap;">' + status + '</td>\
                        <td style="white-space: nowrap;"><a href="/Customer/DeliveryNote/ProcessReleaseFormDetails?rid=' + row.deliveryNoteMasterId + '" class="btn btn-primary">View Details</a></td>\
                    </tr>\
                    ';
                });
            }
            else {
                output =
                      '<tr>\
                        <td colspan="9">No record found.</td>\
                      </tr>\
                    ';
            }
            $("#deliveryNoteListTbody").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getRejectionInList() {
    rejectionInSearchParameters = {
        fromDate: $("#fromDate").val(),
        toDate: $("#toDate").val(),
        ledgerId: $("#customers").val(),
        rejectionInNo: $("#rejectionInNo").val(),
        voucherTypeId: $("#voucherType").val()

    };
    
    $.ajax({
        url: API_BASE_URL + "/Register/RejectionIn",
        type: "Post",
        data: JSON.stringify(rejectionInSearchParameters),
        contentType: "application/json",
        success: function (data) {
            console.log(data);

            var output = "";
            $.each(data, function (count, row) {
                output +=
                  '<tr>\
                        <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                        <td style="white-space: nowrap;">' + row.voucherNo + '</td>\
                        <td style="white-space: nowrap;">' + row.voucherTypeName + '</td>\
                        <td style="white-space: nowrap;">' + row.date + '</td>\
                        <td style="white-space: nowrap;">' + row.ledgerName + '</td>\
                        <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.totalAmount) + '</td>\
                        <td style="white-space: nowrap;">' + row.narration + '</td>\
                        <td style="white-space: nowrap;">' + row.userName + '</td>\
                    </tr>\
                    ';
            });
            $("#rejectionInListTbody").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getSalesInvoiceList() {
    salesInvoiceSearchParameters = {
        fromDate: $("#fromDate").val(),
        toDate: $("#toDate").val(),
        ledgerId: $("#customers").val(),
        formNo: $("#formNo").val(),
        formType: $("#formType").val(),
        salesMode: $("#salesMode").val()
    };

    $.ajax({
        url: API_BASE_URL + "/Register/RejectionIn",
        type: "Post",
        data: JSON.stringify(salesInvoiceSearchParameters),
        contentType: "application/json",
        success: function (data) {
            console.log(data);

            var output = "";
            $.each(data, function (count, row) {
                output +=
                  '<tr>\
                        <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                        <td style="white-space: nowrap;">' + row.voucherNo + '</td>\
                        <td style="white-space: nowrap;">' + row.voucherTypeName + '</td>\
                        <td style="white-space: nowrap;">' + row.date + '</td>\
                        <td style="white-space: nowrap;">' + row.ledgerName + '</td>\
                        <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.totalAmount) + '</td>\
                        <td style="white-space: nowrap;">' + row.narration + '</td>\
                        <td style="white-space: nowrap;">' + row.userName + '</td>\
                    </tr>\
                    ';
            });
            $("#rejectionInListTbody").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getPDCReceivableList() {

    PDCReceivableSearchParameters = {
        fromDate: $("#fromDate").val(),
        toDate: $("#toDate").val(),
        formNo: $("#formNo").val(),
        ledgerName: $("#accountLedger").val()
    };

    $.ajax({
        url: API_BASE_URL + "/Register/PDCReceivable",
        type: "Post",
        data: JSON.stringify(PDCReceivableSearchParameters),
        contentType: "application/json",
        success: function (data) {
            console.log(data);

            //var output = "";
            //$.each(data, function (count, row) {
            //    output +=
            //      '<tr>\
            //            <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
            //            <td style="white-space: nowrap;">' + row.voucherNo + '</td>\
            //            <td style="white-space: nowrap;">' + row.voucherTypeName + '</td>\
            //            <td style="white-space: nowrap;">' + row.date + '</td>\
            //            <td style="white-space: nowrap;">' + row.ledgerName + '</td>\
            //            <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.totalAmount) + '</td>\
            //            <td style="white-space: nowrap;">' + row.narration + '</td>\
            //        </tr>\
            //        ';
            //});
            //$("#creditNoteListTbody").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getReceiptList_old() {

    receiptSearchParameters = {
        fromDate: $("#fromDate").val(),
        toDate: $("#toDate").val(),
        formNo: $("#formNo").val(),
        ledgerId: $("#cashOrBank").val()
    };

    $.ajax({
        url: API_BASE_URL + "/Register/Receipt",
        type: "Post",
        data: JSON.stringify(receiptSearchParameters),
        contentType: "application/json",
        success: function (data) {
            console.log(data);

            var output = "";
            $.each(data, function (count, row) {
                output +=
                  '<tr>\
                        <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                        <td style="white-space: nowrap;">' + row.voucherTypeId + '</td>\
                        <td style="white-space: nowrap;">' + row.voucherTypeName + '</td>\
                        <td style="white-space: nowrap;">' + row.date + '</td>\
                        <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.totalAmount) + '</td>\
                        <td style="white-space: nowrap;">' + row.narration + '</td>\
                        <td style="white-space: nowrap;">' + row.ledgerName + '</td>\
                    </tr>\
                    ';
            });
            $("#receiptListTbody").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getReceiptList() {
    Utilities.Loader.Show();
    if ($("#backDays").val() == "custom") {
        //$(".dtHide").show();
        receiptSearchParameters = {
            fromDate: $("#fromDate").val(),
            toDate: $("#toDate").val(),
        };
    }
    else {
        //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
        //$(".dtHide").hide();
        receiptSearchParameters = {
            fromDate: "2017-01-01",
            toDate: $("#toDate").val(),
        };
    }

    receiptSearchParameters = {
        fromDate: $("#fromDate").val(),
        toDate: $("#toDate").val(),
        formNo: $("#formNo").val(),
        ledgerId: $("#cashOrBank").val()
    };

    $.ajax({
        url: API_BASE_URL + "/Register/Receipt",
        type: "Post",
        data: JSON.stringify(receiptSearchParameters),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var output = [];
            $.each(data, function (count, row) {
                output.push([
                    count + 1,
                    row.voucherTypeId,
                    row.voucherTypeName,
                    row.date,
                    '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
                    row.narration,
                    row.ledgerName,
                ]);
            });
            if (table != "") {
                table.destroy();
            }
            table = $('#receiptListTbody').DataTable({
                data: output,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getCreditNoteList() {

    creditNoteSearchParameters = {
        fromDate: $("#fromDate").val(),
        toDate: $("#toDate").val(),
        formNo: $("#formNo").val(),
    };

    $.ajax({
        url: API_BASE_URL + "/Register/CreditNote",
        type: "Post",
        data: JSON.stringify(creditNoteSearchParameters),
        contentType: "application/json",
        success: function (data) {
            console.log(data);

            var output = "";
            $.each(data, function (count, row) {
                output +=
                  '<tr>\
                        <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                        <td style="white-space: nowrap;">' + row.voucherNo + '</td>\
                        <td style="white-space: nowrap;">' + row.voucherTypeName + '</td>\
                        <td style="white-space: nowrap;">' + row.date + '</td>\
                        <td style="white-space: nowrap;">' + row.ledgerName + '</td>\
                        <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.totalAmount) + '</td>\
                        <td style="white-space: nowrap;">' + row.narration + '</td>\
                    </tr>\
                    ';
            });
            $("#creditNoteListTbody").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getOrderAuthorizationDetails(salesOrderMasterId) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetSalesOrderForAuthorization?salesOrderMasterId=" + salesOrderMasterId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            master = data.Master;
            details = data.Details;
            var statusOutput = "";
            salesOrderMasterIdToAuthorize = salesOrderMasterId;
            if (master.AuthorizationStatus == "Pending") {
                $("#authorizeOrder").show();
                //$("#cancelOrder").show();
                statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending</span>';
            }
            else if (master.AuthorizationStatus == "Authorized") {
                $("#authorizeOrder").hide();
                // $("#cancelOrder").hide();
                statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Authorized</span>';
            }
            else if (master.AuthorizationStatus == "Cancelled") {
                $("#authorizeOrder").hide();
                // $("#cancelOrder").hide();
                statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
            }
            //$("#orderNoDiv").html(master.InvoiceNo);
            $("#orderDateDiv").html(Utilities.FormatJsonDate(master.Date));
            $("#orderNoDiv").html(master.InvoiceNo);
            $("#raisedByDiv").html(master.User);
            $("#raisedForDiv").html(master.CustomerName);
            $("#statusDiv").html(statusOutput);
            $("#orderAmountTxt").val(Utilities.FormatCurrency(master.TotalAmount));
            $("#taxAmountTxt").val(Utilities.FormatCurrency(master.TaxAmount));
            $("#grandTotalTxt").val(Utilities.FormatCurrency(master.TotalAmount + master.TaxAmount));
            var output = "";
            for (i = 0; i < details.length; i++) {
                output += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td><button onclick="getStoreQuantity('+ details[i].productId + ',\'' + details[i].extra1 + '\')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></button></td>\
                            <td>' + details[i].barcode + '</td>\
                            <td>' + details[i].productcode + '</td>\
                            <td>' + details[i].product + '</td>\
                            <td>' + (details[i].extra1 == "" ? "NA" : details[i].extra1) + '</td>\
                            <td>' + details[i].qty + '</td>\
                            <td>' + details[i].unit + '</td>\
                        </tr>';
            }
            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}