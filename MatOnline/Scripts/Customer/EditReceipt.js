﻿var receipt = [];
var currencies = [];
var voucherTypes = [];
var LineItems = [];
var totalLineItemAmount = 0;
var units = [];
var batches = [];
var stores = [];
var racks = [];
var taxes = [];
var allProd = [];

$(function () {
    loadLookups();
    loadRecord();
    $("#suppliers").change(function () {
        var supplierId = $("#suppliers").val();
        var applyOn = $("#applyOn").val();
        if (supplierId > 0) {
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/Receipt/MaterialReceiptNumbers?supplierId=" + supplierId + "&applyOn=" + applyOn,
                type: "GET",
                contentType: "application/json",
                success: function (data) {

                    var materialReceiptNoHtml = "<option></option>";
                    $.each(data, function (count, record) {
                        materialReceiptNoHtml += '<option value="' + record.materialReceiptMasterId + '">' + record.invoiceNo + '</option>';
                    });
                    $("#materialReceiptNo").html(materialReceiptNoHtml);
                    console.log(data);
                    Utilities.Loader.Hide();
                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }
    });

    $("#materialReceiptNo").change(function () {
        var mNo = $("#materialReceiptNo").val();
        if (mNo > 0) {
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/Receipt/GetMaterialReceiptDetails?materialReceiptNo=" + mNo,
                type: "GET",
                contentType: "application/json",
                success: function (data) {
                    for (i = 0; i < data.length; i++) {
                        data[i].QtyToReturn = 0;
                    }
                    LineItems = data;
                    console.log(data);

                    renderLineItems();
                    Utilities.Loader.Hide();
                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }
    });
    $("#applyOn").on("change", function () {
        var supplierId = $("#suppliers").val();
        var applyOn = $("#applyOn").val();
        if (supplierId > 0) {
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/Receipt/MaterialReceiptNumbers?supplierId=" + supplierId + "&applyOn=" + applyOn,
                type: "GET",
                contentType: "application/json",
                success: function (data) {

                    var materialReceiptNoHtml = "<option></option>";
                    $.each(data, function (count, record) {
                        materialReceiptNoHtml += '<option value="' + record.materialReceiptMasterId + '">' + record.invoiceNo + '</option>';
                    });
                    $("#materialReceiptNo").html(materialReceiptNoHtml);
                    console.log(data);
                    Utilities.Loader.Hide();
                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }
    });

    $("#save").click(function () {
        Save();
    });
});

function loadLookups() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Receipt/RejectionOutLookups",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            suppliers = data.Suppliers;
            currencies = data.Currencies;
            voucherTypes = data.VoucherTypes;
            batches = data.Batches;
            units = data.Unit;
            stores = data.stores;
            racks = data.racks;
            taxes = data.taxes;
            allProd = data.allproducts;

            console.log(data);
            populateLookupsControls();
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function populateLookupsControls() {
    var currencyHtml = "";
    $.each(currencies, function (count, record) {
        currencyHtml += '<option value="' + record.exchangeRateId + '">' + record.currencyName + '</option>';
    });
    $("#currency").html(currencyHtml);

    //var suppliersHtml = "<option></option>";
    //$.each(suppliers, function (count, record) {
    //    suppliersHtml += '<option value="' + record.ledgerId + '">' + record.ledgerName + '</option>';
    //});
    //$("#suppliers").html(suppliersHtml);
    $("#suppliers").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: suppliers,
        optionLabel: "please Select..."
    });

    var ledgerId = $("#ledgerId").val();
    $("#suppliers").data('kendoDropDownList').value(ledgerId);

    var applyOnHtml = "";
    applyOnHtml += '<option></option>';
    $.each(voucherTypes, function (count, record) {
        applyOnHtml += '<option value="' + record.voucherTypeId + '">' + record.voucherTypeName + '</option>';
    });
    $("#applyOn").html(applyOnHtml);

}

function loadRecord() {

    var id = $("#masterId").val();
    $.ajax(API_BASE_URL + "/Receipt/GetRejectionDetails/" + id,
        {
            type: "get",
            success: function (data) {
                LineItems = data;
                renderLineItems();
            }
        });
}

function renderLineItems() {
    var output = "";
    var totalAmount = 0.0;
    $.each(LineItems, function (count, row) {
        count += 1;
        row.sn = count;
        output +=
            '\
            <tr>\
                 <td style="white-space: nowrap;">' + (count) + '</td>\
                <td style="white-space: nowrap;">' + row.Product.code + '</td>\
                <td style="white-space: nowrap;">' + row.Product.code + '</td>\
                <td style="white-space: nowrap;">'+ row.Product.productName + '</td>\
                <td style="white-space: nowrap;">'+ row.Initial.qty + '</td>\
                <td style="white-space: nowrap;">'+ row.RejectionDetails.qty + '</td>\
                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.RejectionDetails.rate) + '</td>\
                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.RejectionDetails.amount) + '</td>\
                <td style="white-space: nowrap;">' + findUnit(row.RejectionDetails.unitId).unitName + '</td>\
                <td style="white-space: nowrap;">' + findStore(row.RejectionDetails.godownId).godownName + '</td>\
                <td><button class="btn btn-sm btn-info" onclick="changeQuantity(' + count + ')"><i class="fa fa-edit"></i></button></td>\
            </tr>\
            ';
        totalAmount = (totalAmount + row.amount);
        totalLineItemAmount = totalAmount;

        console.log(totalAmount);
    });
    $("#lineItemTbody").html(output);
    $("#totalAmount").val(Utilities.FormatCurrency(totalAmount));
}

function Save() {
    Utilities.Loader.Show();
    for (i = 0; i < LineItems.length; i++) {
        LineItems[i].Quantity = LineItems[i].QtyToReturn;
        LineItems[i].StoreId = LineItems[i].godownId;
    }
    newRejectionOut = {
        Date: $("#transactionDate").val(),
        SupplierId: $("#suppliers").val(),
        MaterialReceiptNo: $("#materialReceiptNo").val(),
        Narration: $("#narration").val(),
        TotalAmount: totalLineItemAmount,
        TransportCompany: $("#transportationCompany").val(),
        LrNo: $("#lrNo").val(),
        CurrencyId: $("#currency").val(),
        LineItems: LineItems,
    };
    console.log(newRejectionOut);
    $.ajax({
        url: API_BASE_URL + "/Receipt/SaveOrEditFunction",
        type: "POST",
        data: JSON.stringify(newRejectionOut),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            Utilities.SuccessNotification("Rejection Saved successfully");
            window.location = "/Supplier/RejectionOut/Index";
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function changeQuantity(productId) {
    $("#quantityModal").modal("show");
    productToEdit = productId;
}

function applyQuantityOnObject() {
    console.log(productToEdit);
    for (i = 0; i < LineItems.length; i++) {
        if (LineItems[i].sn == productToEdit) {
            if ($("#quantity").val() <= LineItems[i].Initial.qty) {
                LineItems[i].QtyToReturn = $("#quantity").val();
                LineItems[i].RejectionDetails.qty = $("#quantity").val();
                LineItems[i].RejectionDetails.amount = LineItems[i].RejectionDetails.rate * $("#quantity").val();
            }
            else {
                Utilities.ErrorNotification("Quantity to Return cannot be Higher than Quantity Ordered");
            }
        }
    }
    $("#quantity").val("");
    $("#quantityModal").modal("hide");
    renderLineItems();
}

function SaveChanges() {

    delete LineItems[0].RejectionMaster.tbl_AccountLedger;
    delete LineItems[0].RejectionMaster.tbl_RejectionOutDetails;

    var master = LineItems[0].RejectionMaster;
    master.transportationCompany = $("#transportationCompany").val();
    master.lrNo = $("#lrNo").val();
    master.narration = $("#narration").val();
    master.totalAmount = $("#totalAmount").val();

    var items = [];
    for (var i of LineItems) {
        delete i.RejectionDetails.tbl_MaterialReceiptDetails;
        delete i.RejectionDetails.tbl_RejectionOutMaster;
        delete i.RejectionDetails.tbl_Product;

        items.push(i.RejectionDetails);
    }
    var data = {
        RejectionOutMaster: master,
        RejectionOutDetails: items
    };
    $.ajax(API_BASE_URL + "/Receipt/Receipt/Edit",
        {
            data: data,
            type: "post",
            success: function (res) {
                Utilities.InfoNotification(res.message);
                location.href = "//Receipt/Receipt/Register";
            }
        });
}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}
function findProduct(productId) {
    var output = {};
    for (i = 0; i < allProd.length; i++) {
        if (allProd[i].productId == productId) {
            output = allProd[i];
            break;
        }
    }
    return output;
}