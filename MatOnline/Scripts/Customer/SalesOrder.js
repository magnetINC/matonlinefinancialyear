﻿var count = 0;
var totalLineItemAmount = 0.0;
var totalTaxAmount = 0.0;
var products = [];
var searchResult = {};
var salesOrderLineItems = [];
var customers = [];
var salesMen = [];
var applyOn = [];
var pricingLevel = [];
var currencies = [];
var units = [];
var batches = [];
var stores = [];
var racks = [];
var tax = [];
var taxItem = { taxId: 0, taxName: "NA", rate: 0 };
var quotationMasterId = 0;
var quotationDetailsId = 0;
var allTaxes = [];
var today = new Date();
var taxAmt = 0;
var deleteIndex = 0;
var resetBatch = true;
var productBatches = [];
var resetTaxVal = true;
var printData = {};
printData.lineItems = [];
printData.master = {};

$(function () {

    getLookUps();
    getSalesOrderMasterLastRowDetails()
    
    $("#dueDays").attr("disabled", "disabled");
    $("#quotationNo").attr("disabled", "disabled");
    $("#orderNo").attr("disabled", "disabled");
    

    $("#dueDate").change(function () {
        var transDate = new Date($("#transactionDate").val());
        var dueDate = new Date($("#dueDate").val());
        var timeDiff = Math.abs(dueDate.getTime() - transDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        $("#dueDays").val(diffDays);
    });

    $("#applyOn").change(function ()
    {
        if ($("#applyOn").val() != "NA")
        {
            getQuotationNumbers($("#customers").val(), $("#applyOn").val());
            $("#quotationNo").removeAttr("disabled");
            $("#btnAddLineItemModal").attr("disabled", "disabled");
        }
        else
        {
            $("#quotationNo").attr("disabled", "disabled");
            $("#btnAddLineItemModal").removeAttr("disabled");
        }
    });
    $("#customers").change(function ()
    {
        if ($("#applyOn").val() != "NA")
        {
            getQuotationNumbers($("#customers").val(), $("#applyOn").val());
        }
    });
    $("#searchByProductName").on("change", function () {
        resetBatch = true;
        searchProduct($("#searchByProductName").val(), "ProductName");
    });
    $("#searchByProductCode").on("change", function () {
        resetBatch = true;
        searchProduct($("#searchByProductCode").val(), "ProductCode");
    });
    $("#searchByBarcode").on("change", function () {
        resetBatch = true;
        searchProduct($("#searchByBarcode").val(),"Barcode");
    });
    $("#quantityToAdd").on("change", function () {
        var rate = $("#rate").val();
        var qty = $("#quantityToAdd").val();
        var amount = rate * qty;
        $("#amount").val(amount);
        taxReset();
    });
    $("#rate").on("change", function () {
        var rate = $("#rate").val();
        var qty = $("#quantityToAdd").val();
        var amount = rate * qty;
        $("#amount").val(amount);
        taxReset();
    });
    $("#quotationNo").on("change", function () {
        if ($("#quotationNo").val() != "NA")
        {
            getOrderDetails($("#quotationNo").val(), 0);
        }        
    });
    $("#store").on("change", function () {
        if ($("#store").val() != "") {
            $("#quantityToAdd").removeAttr("disabled");
        }
        else
        {
            $("#quantityToAdd").prop("disabled","disabled");
        }
    });

    //$('#itemLineModal').on('show.bs.modal',
    //    function(e) {
    //        if ($("#store").val() == "") {
    //            Utilities.ErrorNotification("Please select store to pick item from.");
    //            e.preventDefault();
    //        }
    //    });

    var sl = 1;
    $("#addLineItem").click(function () {
        var qty = $("#quantityToAdd").val();
        //var rate = searchResult.salesRate;
        var rate = $("#rate").val();
        var gross = (qty * rate);
        var discount = 0; //discount percent
        var netAmount = gross - (discount / 100.00);
        var taxAmount = netAmount * tax.find(p => p.taxId == parseInt($("#tax").val())).rate/100;//(netAmount * 0.05).toFixed(2);//searchResult.taxId == tax.TaxId ? (netAmount * 0.05).toFixed(2) : 0;
        var amount = parseFloat(gross) + parseFloat(taxAmount);

        if (qty == 0 || "") {
            Utilities.ErrorNotification("You need to fill the quantity field.")
            return false;
        }
        
        salesOrderLineItems.push({
            SlNo: sl,
            ProductId: searchResult.productId,
            ProductBarcode: searchResult.productCode,
            ProductCode: searchResult.productCode,
            ProductName: searchResult.productName,
            //itemDescription: searchResult.narration,
            itemDescription: $("#description").val(),
            Brand: searchResult.brandName,
            Qty: qty,
            UnitId: searchResult.unitId,
            UnitConversionId: searchResult.unitConversionId,
            Extra1:$("#store").val(), //searchResult.godownId,
            RackId: searchResult.rackId,
            BatchId: searchResult.batchId,
            TaxId: $("#tax").val(),//tax.TaxId,
            Tax: searchResult.taxId == tax.TaxId ? tax.TaxName : "NA",
            TaxAmount: taxAmount,
            Rate: rate,
            Amount: amount
        });
        sl += 1;
        renderLineItems();
        $("#searchByBarcode").val("");
        $("#searchByBarcode").trigger("chosen:updated");
        $("#searchByProductCode").val("");
        $("#searchByProductCode").trigger("chosen:updated");
        $("#searchByProductName").val("");
        $("#searchByProductName").trigger("chosen:updated");
        $("#rate").val("");
        $("#quantityInStock").val(""); 
        $("#storeQuantityInStock").val(""); 
        $("#amount").val(""); 
        $("#description").val("");
        $("#quantityToAdd").val(0);
        
    });

    

});


function taxReset() {
    var qty = $("#quantityToAdd").val();
    var rate = $("#rate").val();
    var gross = (qty * rate);
    var discount = 0; //discount percent
    var netAmount = gross - (discount / 100.00);
    //var taxAmount = netAmount * tax.find(p => p.taxId == parseInt($("#tax").val())).rate / 100;
    var taxAmount = netAmount * taxItem.rate / 100;
    $("#taxAmount").val(taxAmount);
}

function getLookUps()
{
    Utilities.Loader.Show();
    var productsAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts1",
        type: "Get",
        contentType:"application/json"
    });

    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetLookups",
        type: "Post",
        contentType: "application/json"
    });

    $.when(productsAjax, lookUpAjax)
    .done(function (dataProducts, dataLookUps) {
        products = dataProducts[2].responseJSON;
        customers = dataLookUps[2].responseJSON.Customers;
        salesMen = dataLookUps[2].responseJSON.SalesMen;
        applyOn = dataLookUps[2].responseJSON.ApplyOn.splice(1, 1);
        pricingLevel = dataLookUps[2].responseJSON.PricingLevel;
        currencies = dataLookUps[2].responseJSON.Currencies;
        units = dataLookUps[2].responseJSON.Units;
        batches = dataLookUps[2].responseJSON.Batches;
        stores = dataLookUps[2].responseJSON.Stores;
        racks = dataLookUps[2].responseJSON.Racks;
        tax = dataLookUps[2].responseJSON.Tax;

        //remove sales quotation from apply on
        //var indexOfObjectToRemove = applyOn.findIndex(p=>p.voucherTypeId == 15);
        //applyOn.splice(0, 1);

        //$("#customers").html(Utilities.PopulateDropDownFromArray(customers,1,0));
        $("#customers").kendoDropDownList({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: {
                data: customers
            },
            filter: "contains",
        });
        $("#currency").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));

        //Restrict Sales Man
        var salesMan = salesMen.find(p => p.userId == matUserInfo.UserId);
        if (salesMan != undefined) {
            $("#salesMan").html(Utilities.PopulateDropDownFromArray([salesMan], 0, 1));
        } else {
            $("#salesMan").html("<option value=\"0\">--NA--</option>" + Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
        }

        //Restrict Location
        var nStore = stores.find(p => p.godownId == matUserInfo.StoreId);
        if (nStore != undefined && matUserInfo.RoleId != 1) {
            $("#store").html(Utilities.PopulateDropDownFromArray([nStore], 0, 1));
        } else {
            $("#store").html("<option value=\"\">--Select--</option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
        }

        
        $("#applyOn").html("<option value=\"NA\">NA</option>" + Utilities.PopulateDropDownFromArray(applyOn, 0, 1));
        $("#pricingLevel").html("<option value=\"-1\">--Select--</option>" + Utilities.PopulateDropDownFromArray(pricingLevel, 0, 1));
        $("#tax").html(Utilities.PopulateDropDownFromArray(tax, 0, 1));
        //$("#searchByProductName").html(Utilities.PopulateDropDownFromArray(products, 3, 3));
        //$("#searchByBarcode").html(Utilities.PopulateDropDownFromArray(products, 0, 0));
        //$("#searchByProductCode").html(Utilities.PopulateDropDownFromArray(products, 2, 2));


        if (products.length > 0) {


            var productNameHtml = "";
            var productCodeHtml = "";
            var barCodeHtml = "";

            $.each(products, function (count, record)
            {
                if (record.IsInRightYear == true) {
                    productNameHtml += '<option value="' + record.ProductName + '">' + record.ProductName + '</option>';
                    productCodeHtml += '<option value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                    barCodeHtml += '<option value="' + record.barcode + '">' + record.barcode + '</option>';
                }
                else {
                    productNameHtml += '<option disabled="disabled" value="' + record.ProductName + '">' + record.ProductName + '( This product is not active because it was created in higher financial year )</option>';
                    productCodeHtml += '<option disabled="disabled" value="' + record.ProductCode + '">' + record.ProductCode + '( This product is not active because it was created in higher financial year )</option>';
                    barCodeHtml += '<option disabled="disabled" value="' + record.barcode + '">' + record.barcode + '( This product is not active because it was created in higher financial year )</option>';
                }
            });

            $("#searchByProductName").html(productNameHtml);
            $("#searchByProductCode").html(productCodeHtml);
            $("#searchByBarcode").html(barCodeHtml);

            $("#searchByProductName").chosen({ width: "100%", margin: "1px" });
            $("#searchByBarcode").chosen({ width: "100%", margin: "1px" });
            $("#searchByProductCode").chosen({ width: "100%", margin: "1px" });
        }

        //$("#searchByProductName").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 3, 3));
        //$("#searchByBarcode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 0, 0));
        //$("#searchByProductCode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 2, 2));

        $("#store").chosen({ width: "100%", margin: "1px" });
        $("#pricingLevel").chosen({ width: "100%", margin: "1px" });
        $("#salesMan").chosen({ width: "100%", margin: "1px" });
        $("#searchByProductName").chosen({ width: "100%", margin: "1px" });
        $("#searchByBarcode").chosen({ width: "100%", margin: "1px" });
        $("#searchByProductCode").chosen({ width: "100%", margin: "1px" });

        Utilities.Loader.Hide();        
    });
}

function getQuotationNumbers(customerId, applyOn) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetQuotationNumbers?customerId=" + customerId + "&applyOn=" + applyOn + "&currentUserId=" + matUserInfo.UserId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            $("#quotationNo").html("<option value=\"NA\">-Select-</option>"+Utilities.PopulateDropDownFromArray(data, 0, 1));
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getOrderDetails(quotationNumber,salesOrderMasterId) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetOrderDetails?quotationNumber=" + quotationNumber + "&salesOrderMasterId=" + salesOrderMasterId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
//            console.log(data);
            quotationMasterId = data.Master[0].quotationMasterId;
            var details = data.Details;
            console.log("details",details);
            populateOrderMasterDetails(data.Master[0]);
            salesOrderLineItems = [];

            var sl = 1;
            var globalStoreId = details[0].extra1;
            var globalStore = findStore(globalStoreId);
            $("#store").val(globalStoreId);
            $("#store :selected").text(globalStore.godownName);
            //console.log(globalStore);
            //for (i = 0; i < details.length; i++)
            $.each(details, function (count, row) {
                var quotationDetailsId = row.quotationDetailsId;
                var prodId = row.productId;
                var prodQty = row.qty;
                var storeId = row.extra1;
                //console.log(storeId);
                var productDetails = findProduct(prodId);
                searchResult = productDetails;
                var rate = searchResult.SalesRate * prodQty;
                var taxAmount = rate * tax.find(p => p.taxId == searchResult.TaxId).rate / 100;

               // var taxAmount = searchResult.TaxId == tax.TaxId ? (rate * 0.05).toFixed(2) : 0;
                var amount = parseFloat(rate) + parseFloat(taxAmount);
                salesOrderLineItems.push({
                    SlNo: sl,
                    ProductId: searchResult.ProductId,
                    ProductBarcode: searchResult.ProductCode,
                    ProductCode: searchResult.ProductCode,
                    ProductName: searchResult.ProductName,
                    //itemDescription: searchResult.narration,
                    itemDescription: $("#description").val(),
                    Brand: searchResult.BrandId,
                    Qty: prodQty,
                    UnitId: searchResult.UnitId,
                    UnitConversionId: searchResult.unitConversionId,
                    Extra1: storeId,
                    RackId: searchResult.RackId,
                    BatchId: searchResult.BatchId,
                    TaxId: searchResult.taxId,
                    Tax: tax.TaxName,
                    TaxAmount: taxAmount,
                    Rate: Utilities.FormatCurrency(searchResult.SalesRate),
                    Amount: amount,
                    QuotationDetailsId: quotationDetailsId
                    //quotationDetailsId
                });
                sl = sl + 1;
            });
            //console.log(salesOrderLineItems);
            renderLineItems();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getSalesOrderMasterLastRowDetails() {
    console.log(4658);
   // Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetSalesOrderMasterLastRowDetails",
        type: "POST",
        contentType: "application/json",
        success: function (data) {
            console.log(data); //return;
            $("#orderNo").val(data);
            
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong getting new order number.");
            Utilities.Loader.Hide();
        }
    });
}

function populateOrderMasterDetails(orderMaster)
{
    $("#orderNo").val(orderMaster.invoiceNo);
    $("#salesMan").val(orderMaster.employeeId).change();
    $("#pricingLevel").val(orderMaster.pricinglevelId).change();
}

function populateOrderNumbersOrReceiptNumbers(purchaseMode)
{
    if (purchaseMode == "NA")
    {
        $("#applyOn").attr("disabled", "disabled");
        $("#orderNo").attr("disabled", "disabled");
    }
    else if (purchaseMode == "Against Purchase Order")
    {
        $("#applyOn").removeAttr("disabled");
        $("#orderNo").removeAttr("disabled");

        $("#applyOn").html(Utilities.PopulateDropDownFromArray(purchaseOrderVoucherType, 0, 1));
        getOrderNumbers($("#suppliers").val(), 10);
    }
    else if (purchaseMode == "Against Material Receipt")
    {
        $("#applyOn").removeAttr("disabled");
        $("#orderNo").removeAttr("disabled");

        $("#applyOn").html(Utilities.PopulateDropDownFromArray(materialReceiptVoucherType, 0, 1));
        getReceiptNumbers($("#suppliers").val(), 11);
    }
}

$("#batch").change(function () {
    resetBatch = false;
    resetTaxVal = false;
    searchProduct($("#searchByProductName").val(), "ProductName");
});

$("#store").change(function () {
    resetBatch = false;
    resetTaxVal = false;
    searchProduct($("#searchByProductName").val(), "ProductName");
});

$("#tax").on("change", function () {
    resetTaxRate();
    taxReset();
   // calculateAmountOfItemToAdd();
});

function searchProduct(filter, searchBy) {
    var storeId = $("#store").val() == "" ? 0 : parseInt($("#store").val());
    var batchId = $("#batch").val() == "" ? 0 : parseInt($("#batch").val());
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/SearchProduct?filter=" + filter + "&searchBy=" + searchBy + "&storeId=" + storeId + "&batchId=" + batchId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            //console.log(tax); //return;
            var productDetails = data.Product[0];
            searchResult = productDetails;
            productBatches = batches.filter(p => p.productId == parseInt(searchResult.productId));
           

            if (resetBatch == true) {
                populateBatch();
            }
            if (resetTaxVal == true) {
                taxItem = tax.find(p => p.taxId == parseInt(searchResult.taxId));
                $("#tax").val(searchResult.taxId);
                $("#tax").trigger("chosen:updated");
                taxReset();
            }


            $("#searchByBarcode").val(searchResult.productCode);
            $("#searchByBarcode").trigger("chosen:updated");
            $("#searchByProductCode").val(searchResult.productCode);
            $("#searchByProductCode").trigger("chosen:updated");
            $("#searchByProductName").val(searchResult.productName);
            $("#searchByProductName").trigger("chosen:updated");
           // $("#searchByProductName").data('kendoDropDownList').value(filter);
           // $("#searchByProductName").val(filter);
           // $("#searchByProductCode").val(filter);
           // $("#searchByBarcode").val(filter);
            $("#rate").val(productDetails.salesRate);
            $("#description").val(data.Narration);
            $("#amount").val(($("#rate").val() * $("#quantityToAdd").val()));
           // $("#description").val(productDetails.narration);

            if (data.QuantityInStock > 0) {
                $("#quantityInStock").css("color", "black");
                $("#quantityInStock").val(Utilities.FormatQuantity(data.QuantityInStock));
            }
            else if ($("#batch").val() != "") {
                $("#quantityInStock").css("color", "red");
                $("#quantityInStock").val("OUT OF STOCK! " + data.QuantityInStock + "STOCK");
            }
            if (data.StoreQuantityInStock > 0) {
                $("#storeQuantityInStock").css("color", "black");
                $("#storeQuantityInStock").val(data.StoreQuantityInStock);
            }
            else if ($("#batch").val() != "" && $("#store").val() != "") {
                $("#storeQuantityInStock").css("color", "red");
                $("#storeQuantityInStock").val("OUT OF STOCK! " + data.StoreQuantityInStock + " STOCK");
            }

           
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function populateBatch() {
    $("#batch").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(productBatches, 0, 1));
    $("#batch").trigger("chosen:updated");
}

function populateTax() {
    $("#tax").html(Utilities.PopulateDropDownFromArray([taxItem], 0, 1) + "<option value=1>NA</option>");
    $("#tax").trigger("chosen:updated");
}

function renderLineItems() {
    totalLineItemAmount = 0.0;
    var output = "";
    var billTotal = 0.0;
    var taxTotal = 0.0;
    var grandTotal = 0.0;
    $.each(salesOrderLineItems, function (count, row) {
        //console.log("it is", row.Extra1); 
        billTotal += row.Rate * row.Qty;
        taxTotal += Number(row.TaxAmount);
        output +=
            '<tr>\
                <td style="white-space: nowrap;"><button onclick="confirmDelete(' + count + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
                <td style="white-space: nowrap;"><button onclick="Utilities.ShowStockCardDialog(' + row.ProductId + ')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></button></td>\
                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                <td style="white-space: nowrap;">' + row.ProductBarcode + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductCode + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductName + '</td>\
                <td style="white-space: nowrap;">' + ((row.itemDescription == undefined) ? "NA" : row.itemDescription) + '</td>\
                <td style="white-space: nowrap;">'+ row.Qty + '</td>\
                <td style="white-space: nowrap;">' + findUnit(row.UnitId).unitName + '</td>\
                <td style="white-space: nowrap;">'+ findStore(row.Extra1).godownName + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.TaxAmount) + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.Rate) + '</td>\
                <td style="white-space: nowrap;">'+ Utilities.FormatCurrency(row.Amount) + '</td>\
            </tr>\
        ';
        /*< td style = "white-space: nowrap;" > ' + Utilities.FormatCurrency(billTotal) + '</td >\
        <td style="white-space: nowrap;">' + Utilities.FormatCurrency( (row.Rate * row.Qty)+ Number(row.TaxAmount)) + '</td>\*/

        totalLineItemAmount = (totalLineItemAmount + row.Amount);
        //totalTaxAmount = (totalTaxAmount + parseFloat(row.TaxAmount));
    });
    $("#billAmount").val(Utilities.FormatCurrency(billTotal));
    $("#grandTotal").val(Utilities.FormatCurrency(billTotal + taxTotal));
    $("#salesOrderLineItemTbody").html(output);
    $("#taxAmountHtml").val(Utilities.FormatCurrency(taxTotal));
    settings.ApplySettings();
}

var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the possitive lable as key
    //...
};

function confirmDelete(index) {
    deleteIndex = index;
    eModal.confirm(options).then(removeLineItem);
}

function removeLineItem() {
    salesOrderLineItems.splice(deleteIndex, 1);
    renderLineItems();
}

function resetTaxRate() {
    var isFound = false;
    for (i = 0; i < tax.length; i++) {
        if (tax[i].taxId == $("#tax").val()) {
            taxItem = tax[i];
            isFound = true;
            break;
        }
    }
    if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
    {
        taxItem = { taxId: 0, taxName: "NA", rate: 0 };
    }
    //        console.log(tax);
}

//function removeLineItem(productId) {
//    if (confirm("Remove this item?"))
//    {
//        var indexOfObjectToRemove = salesOrderLineItems.findIndex(p=>p.ProductId == productId);
//        salesOrderLineItems.splice(indexOfObjectToRemove, 1);
//        renderLineItems();
//    }
//}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}

function findProduct(productId)
{
    var output = {};
    for(i=0;i<products.length;i++)
    {
        if(products[i].ProductId==productId)
        {
            output = products[i];
            break;
        }
    }
    return output;
}

function findSalesMan(employeeId)
{
    var output = {};
    for(i=0;i<salesMen.length;i++)
    {
        if(salesMen[i].employeeId==employeeId)
        {
            output = salesMen[i];
            break;
        }
    }
    return output;
}

function accountLedgerDropDownEditor(container, options) {
    $('<input required name="' + options.field + '" data-text-field="ledgerName" data-value-field="ledgerId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: {
                data: accountLedgers
            },
            template: '<span>#: ledgerName #</span>',
            filter: "contains",
        });
}

function getAccountLedger(ledgerId) {
    for (i = 0; i < accountLedgers.length; i++) {
        if (accountLedgers[i].ledgerId == ledgerId) {
            return accountLedgers[i].ledgerName;
        }
    }
    return "";
}

function getTotalTaxAmount()
{
    var totalTax = 0.00;
    for(i=0;i<salesOrderLineItems.length;i++)
    {
        totalTax = totalTax + parseFloat(salesOrderLineItems[i].TaxAmount);
    }
    return totalTax;
}

function getTotalAdditionalCost(obj)
{
    var output = 0.0;
    for(i=0;i<obj.length;i++)
    {
        output = output + parseFloat(obj[i].amount);
    }
    totalAdditionalCost = output;
    renderLineItems();
}

function getKendoLineItemIndex(datasource,objectToFindIndex)    //function was created to remove lineitem from row wen its removed from
{                                                               //kendo grid
    for(i=0;i<datasource.length;i++)
    {
        if(objectToFindIndex.id==datasource[i].id)
        {
            return i;
        }
    }
    return -1;
}

$("#save, #saveprint").click(save);
function save()
{
    var myId = $(this).attr('id');
    //perform validation in another boolean function which calls save() if it returns true
    if ($("#orderNo").val() == "")
    {
        Utilities.ErrorNotification("Order number required!");
        return;
    }
    else if ($("#transactionDate").val() == "")
    {
        Utilities.ErrorNotification("Transaction date required!");
        return;
    }
    else if ($("#dueDate").val() == "")
    {
        Utilities.ErrorNotification("Due date required!");
        return;
    }
    else if ($("#store").val() == "")
    {
        Utilities.ErrorNotification("Select store!");
        return;
    }
    else if (salesOrderLineItems.length < 1)
    {
        Utilities.ErrorNotification("Line item contains no record!");
        return;
    }
    else
    {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/CustomerCentre/GetCustomerBalance?ledgerId="+$("#customers").val(),
            type: "GET",
            contentType: "application/json",
            success:function(data)
            {
                //var balance = -1 * data;    //-1 to negate it and change the value to +ve
                console.log(data);
                var balance = data;
                //if (balance >= totalLineItemAmount)
                //{
                    var infoSalesOrderMaster = {
                        Date: $("#transactionDate").val(),
                        DueDate: $("#dueDate").val(),
                        LedgerId: $("#customers").val(),
                        InvoiceNo: $("#orderNo").val(),
                        VoucherNo: $("#orderNo").val(),
                        UserId: matUserInfo.UserId,
                        Cancelled: $("#cancelled").prop('checked'),
                        //SalesOrderMasterId: 0,
                        EmployeeId: $("#salesMan").val(),
                        Narration: $("#narration").val(),
                        ExchangeRateId: $("#currency").val(),
                        QuotationMasterId: 0,
                        TotalAmount: totalLineItemAmount,//$("#billAmount").val(),
                        taxAmount: getTotalTaxAmount(),//$("#taxAmountHtml").val(),
                        PricinglevelId: $("#pricingLevel").val(),
                        QuotationMasterId: quotationMasterId
                };
                printData.master = {
                    Date: $("#transactionDate").val(),
                    DueDate: $("#dueDate").val(),
                    LedgerName: $("#customers option:selected").html(),
                    InvoiceNo: $("#orderNo").val(),
                    VoucherNo: $("#orderNo").val(),
                    UserId: $("#UserId").val(),
                    Cancelled: $("#cancelled").prop('checked'),
                    EmployeeName: $("#salesMan option:selected").html(),
                    Narration: $("#narration").val(),
                    ExchangeRateId: $("#currency").val(),
                    TotalAmount: totalLineItemAmount,
                    taxAmount: getTotalTaxAmount(),
                    PricinglevelName: $("#pricingLevel option:selected").html(),
                    QuotationMasterId: quotationMasterId
                };
                var infoSalesOrderDetails = salesOrderLineItems;
                printData.lineItems = salesOrderLineItems;

                    //populate main object
                    var orderToSave = {
                        infoSalesOrderDetails: infoSalesOrderDetails,
                        infoSalesOrderMaster: infoSalesOrderMaster
                    };
                    //console.log(orderToSave); //return;
                    $.ajax({
                        url: API_BASE_URL + "/SalesOrder/SaveSalesOrder",
                        type: "Post",
                        contentType: "application/json",
                        data: JSON.stringify(orderToSave),
                        success: function (e) {
                             Utilities.Loader.Hide();
                             if (e.ResponseCode == 200) {
                                var presentDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                                Utilities.SuccessNotification(e.ResponseMessage);
                                 getSalesOrderMasterLastRowDetails();
                                 if (myId == "saveprint") {
                                     printOrder(printData);
                                 }
                                //$("#transactionDate").val(presentDate);
                                 $("#dueDate").val(presentDate);
                                 //print();
                                //$("#dueDate").val("0");
                                //$("#customers").text("");
                                $("#salesMan").val("0");
                                $("#narration").val("");
                                $("#billAmount").val("");
                                $("#taxAmountHtml").val("");
                                $("#grandTotal").val("");
                                salesOrderLineItems = [];
                                 renderLineItems();
                                 Utilities.Loader.Hide();
                            }
                             else if (e.ResponseCode == 400) {
                                 Utilities.Loader.Hide();
                                Utilities.ErrorNotification(e.ResponseMessage);
                            }
                            
                        },
                        error: function (e) {
                            Utilities.Loader.Hide();
                        }
                    });
               // }
                //else
                //{
                //    Utilities.Loader.Hide();
                //    Utilities.ErrorNotification("Insufficient customer balance.<br/>Customer Balance: "+balance);
                //}
                //window.location.href = window.location.href;
            },
            error:function(err)
            {
                Utilities.ErrorNotification("Sorry,could not get customer balance at this time.");
            }
        });
        
    }   
    
}

function clear()
{
    $("#voucherNo").val("");
    $("#invoiceNo").val("");
    $("#suppliers").val("");
    $("#transactionDate").val();
    $("#suppliers").val();
    $("#invoiceNo").val();
    $("#invoiceDate").val();
    $("#creditPeriod").val();
    $("#currency").val();
    $("#narration").val();
    $("#grandTotal").val("");
    $("#billAmount").val("");
    $("#taxAmountHtml").val("");
}



//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;


//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}




function printOrder(printData) {
    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();
    var thisEmail = $("#companyEmail").val();
    var thisPhone = $("#companyPhone").val();
    var thisCustomer = printData.master.LedgerName;
    var thisDate = Utilities.FormatJsonDate(printData.master.Date);
    var thisVoucherNo = printData.master.InvoiceNo;
    var thisNarration = printData.master.Narration == "" ? "N/A" : printData.master.Narration;

    var objToPrint = [];
    $.each(printData.lineItems, function (count, row) {
        objToPrint.push({
            "SlNo": count + 1,
            "Barcode": row.ProductBarcode,
            "ProductCode": row.ProductCode,
            "Product": row.ProductName,
            //"WareHouse": (row.extra1 == "" ? "N/A" : row.extra1),
            "Quantity": row.Qty,
            "Description": row.itemDescription,
            "Rate": 'N' + Utilities.FormatCurrency(parseInt(row.Rate)),
            "Amount": Utilities.FormatCurrency(row.Amount),
            "Tax": 'N' + Utilities.FormatCurrency(row.TaxAmount),
        });
    });

    columns = [
        { title: "S/N", dataKey: "SlNo" },
        { title: "Barcode", dataKey: "Barcode" },
        { title: "Product Code", dataKey: "ProductCode" },
        { title: "Product", dataKey: "Product" },
        //{ title: "WareHouse", dataKey: "WareHouse" },
        { title: "Quantity", dataKey: "Quantity" },
        { title: "Description", dataKey: "Description" },
        { title: "Rate", dataKey: "Rate" },
        { title: "Amount", dataKey: "Amount" },
        { title: "Tax", dataKey: "Tax" },
    ];

    var doc = new jsPDF('portrait', 'pt', 'letter');
    doc.setDrawColor(0);

    //start drawing

    doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text(companyName, 320, 80, 'center');
    doc.setFontSize(9);
    doc.setFontType("normal");
    doc.text(companyAddress, 320, 90, 'center');

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Email Id: ', 400, 150);
    doc.setFontType("normal");
    doc.text(thisEmail, 460, 150);
    doc.setFontType("bold");
    doc.text('Phone: ', 400, 165);
    doc.setFontType("normal");
    doc.text(thisPhone, 460, 165);

    doc.line(40, 170, 570, 170);

    doc.setFontType("bold");
    doc.setFontSize(9);
    doc.text('Customer:', 40, 185);
    doc.setFontType("normal");
    doc.text(thisCustomer, 95, 185);
    doc.setFontType("bold");
    doc.text('Narration:', 40, 205);
    doc.setFontType("normal");
    doc.text(thisNarration, 95, 205);


    doc.setFontType("bold");
    doc.text('Order No: ', 400, 185);
    doc.setFontType("normal");
    doc.text(thisVoucherNo, 460, 185);
    doc.setFontType("bold");
    doc.text('Date: ', 400, 205);
    doc.setFontType("normal");
    doc.text(thisDate, 460, 205);

    doc.line(40, 220, 570, 220);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text('Sales Order', 250, 240);

    //doc.line(120, 20, 120, 60); horizontal line
    doc.setFontSize(8);
    doc.autoTable(columns, objToPrint, {
        startY: 255,
        theme: 'striped',
        styles: {
            fontSize: 8,
        },
        columnStyles: {
            SlNo: { columnWidth: 30, },
            Amount: { columnWidth: 80, halign: 'right' },
            Memo: { columnWidth: 100 },
            PayTo: { columnWidth: 110 },
            Currency: { columnWidth: 100 }

        },
    });

    doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);
    var OrderAmt = parseInt(printData.master.TotalAmount) - parseInt(printData.master.taxAmount);

    doc.setFontSize(9);
    doc.setFontType("bold"); //master.TotalAmount + master.TaxAmount
    doc.text('Order Amount: ' + Utilities.FormatCurrency(parseInt(OrderAmt)), 555, doc.autoTable.previous.finalY + 20, "right");
    doc.text('Tax Amount: ' + Utilities.FormatCurrency(parseInt(printData.master.taxAmount)), 555, doc.autoTable.previous.finalY + 35, "right");
    doc.text('Grand Total: ' + Utilities.FormatCurrency(parseInt(printData.master.TotalAmount)), 555, doc.autoTable.previous.finalY + 50, "right");

    doc.line(40, doc.autoTable.previous.finalY + 65, 570, doc.autoTable.previous.finalY + 65);

    doc.setFontType("bold");
    doc.text('Grand Total in Words: ', 40, doc.autoTable.previous.finalY + 80);
    doc.setFontType("normal");

    //remove commas
    //var tempTotal = totalAmt.replace(/,/g, "");
    //convert to number
    var amtToBeConverted = parseInt(printData.master.TotalAmount);

    //capitalize Each Word
    var wordFormat = Utilities.NumberToWords(amtToBeConverted);
    const words = wordFormat.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());

    doc.text(words + ' Naira ONLY.', 140, doc.autoTable.previous.finalY + 80);

    doc.setFontType("bold");
    doc.text('Payment approved by:  _________________________________', 40, doc.autoTable.previous.finalY + 120);

    doc.text('Done by:  ' + $("#nameSpan").html(), 350, doc.autoTable.previous.finalY + 120);
    doc.text('_________________', 390, doc.autoTable.previous.finalY + 123);
    doc.autoPrint();
    doc.save('Sales_Order.pdf');
    //window.open(doc.output('bloburl'));
    window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
}

function print() {
    var columns = [
        { title: "SN", dataKey: "SlNo" },
        { title: "Product Code", dataKey: "ProductCode" },
        { title: "Product Name", dataKey: "ProductName" },
        { title: "Qty", dataKey: "Quantity" },
        { title: "Category", dataKey: "CategoryName" },
        //{ title: "Memo", dataKey: "Memo" },
        { title: "Dr", dataKey: "Debit" },
        { title: "Cr", dataKey: "Credit" },
    ];

    var doc = new jsPDF('portrait');
    doc.setFontSize(15);
    doc.text('[Company Name]', 50, 30);
    if (base64Img) {
        doc.addImage(base64Img, 'JPEG', 15, 25, 30, 25);
    }
    doc.setFontSize(10);
    var dateOfPrint = new Date().toDateString();
    doc.text(dateOfPrint, 150, 25);
    //doc.text('THANKS FOR YOUR PATRONAGE', 70, 200);
    doc.setDrawColor(0);
    //doc.setFontSize(8);
    //doc.setFillColor(26, 189, 156);
    //doc.rect(150, 18, 2, 2, 'F');
    //doc.rect(150, 24, 2, 2, 'F');
    //doc.rect(150, 31, 2, 2, 'F');
    doc.rect(73, doc.autoTable.previous.finalY + 32, 123, 1, 'F');
    //doc.text(' ' + 'IT ADVISORY' + '\n\n ' + 'CONSULTING' + '\n\n '
    //   + 'FINANCE', 154, 20);
    doc.autoTable(columns, printData.lineItems, {
        startY: 105, theme: 'grid', styles: {
            fontSize: 9
        }
    });
    var totalCredit = Utilities.FormatCurrency(parseFloat(printData.totalCredit));
    var totalDebit = Utilities.FormatCurrency(parseFloat(printData.totalDebit));
    //var totalAmountForPrint = Utilities.FormatCurrency(parseFloat(printData.TotalAmount));
    //var billDiscountForPrint = Utilities.FormatCurrency(parseFloat(printData.BillDiscount));
    //var subTotalForPrint = Utilities.FormatCurrency(parseFloat(printData.TotalAmount) - parseFloat(printData.BillDiscount));
    //var totalTaxAmountForPrint = Utilities.FormatCurrency(parseFloat(printData.TaxAmount));
    //var grandTotalForPrint = Utilities.FormatCurrency(parseFloat(printData.GrandTotal));
    //var amountTenderedForPrint = Utilities.FormatCurrency(parseFloat(printData.AmountTendered));
    //var balanceForPrint = Utilities.FormatCurrency(parseFloat(printData.Balance));
    //var salesPointObj = salesPoints.find(p => p.counterId == printData.CounterId);
    //var salesPoint = salesPointObj == undefined ? "" : salesPointObj.counterName;
    //var account = allLedgers.find(p => p.ledgerId == printData.LedgerId).ledgerName;
    //var soldBy = $("#nameSpan").html();
    doc.setFontStyle('bold');
    doc.setFontSize(12);
    // doc.text(' Total Amount             NGN ' + totalAmountForPrint + '\n\n Dicount                       NGN ' + billDiscountForPrint + '\n\n Sub Total                    NGN ' + subTotalForPrint + '\n\n Grand Total                NGN ' + grandTotalForPrint, 80, doc.autoTable.previous.finalY + 8);
    // doc.text(' Teller Number                ' + printData.POSTellerNo + '\n\n Dicount %                            ' + printData.Discount + '\n\n Tax Amount                NGN ' + totalTaxAmountForPrint + '\n\n Amount Paid               NGN ' + amountTenderedForPrint, 140, doc.autoTable.previous.finalY + 8);
    doc.text(' Total Credit         ' + totalCredit + '\n\n Total Debit          ' + totalDebit, 120, doc.autoTable.previous.finalY + 8);
    doc.setFontSize(15);
    doc.text('Journal Voucher', 90, 80);
    doc.setFontSize(12);
    var journalNo = "Journal No: " + printData.journalNo;
    var date = "Date: " + new Date(printData.date).toDateString();
    doc.text(journalNo, 20, 100);
    doc.text(date, 150, 100);
    doc.setDrawColor(207, 207, 207);
    doc.rect(73, doc.autoTable.previous.finalY, 123, 23);
    var totalCreditInWords = "TOTAL CREDIT: " + (Utilities.NumberToWords(printData.totalCredit)).toUpperCase() + " NAIRA ONLY";
    var totalDebitInWords = "TOTAL DEBIT: " + (Utilities.NumberToWords(printData.totalDebit)).toUpperCase() + " NAIRA ONLY";
    doc.setFontSize(10);
    doc.text(totalCreditInWords, 20, doc.autoTable.previous.finalY + 40);
    doc.text(totalDebitInWords, 20, doc.autoTable.previous.finalY + 50);
    doc.text("JOURNAL APPROVED BY ____________________", 10, doc.autoTable.previous.finalY + 70);
    doc.text("JOURNAL RECEIVED BY ____________________", 110, doc.autoTable.previous.finalY + 70);
    //doc.text('\nSales Point: ' + salesPoint + '\nAccount: ' + account + '\nSold By: ' + soldBy + '\nInvoice No: ' + printData.InvoiceNo + '\nTrans Date: '
    //+ printData.Date + '\nStore: ' + printData.lineItems[0].StoreName, 28, 65);
    //doc.rect(23, 65, 85, 27);
    doc.setFontSize(15);
    doc.setLineWidth(1.0);
    doc.setDrawColor(0, 0, 0);
    doc.line(10, 70, 200, 70);
    //doc.text('Balance: NGN ' + balanceForPrint, 125, doc.autoTable.previous.finalY + 40);
    doc.save('GeneralJournal' + printData.journalNo + '.pdf');
    //window.open(doc.output('bloburl'));
    window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
}