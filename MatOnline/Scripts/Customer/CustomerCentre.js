﻿var CustomerSearchParam = {};
var NewLedgerList = [];
var NewLedger = {};
var cities = [];
var states = [];
var pricingLevels = [];
var cityStates1 = [];
var lId = 0;
var ledgerIdForEdit = 0;
var ledger = {};
var table = "";
var table1 = "";
var excelArrayData = [];
var tableToUpload = "";


$("#updateLedger").hide();
$(function () {
    $("#createAgent").click(function () {
        $("#newLedger").show();
        $("#updateLedger").hide();
        clearAllTextboxValues();
        $("#AgentDetails").modal("show");
    });
});

$("#state").change(function () {
    var val = $("#state").val();
    cityStates = [];
    for (i = 0; i < cities.length; i++) {
        if (cities[i].areaId == val) {
            cityStates.push(cities[i]);
        }
    }

    $("#city").html(Utilities.PopulateDropDownFromArray(cityStates, 0, 1));
});
$("#state1").change(function () {
    var val = $("#state1").val();
    cityStates1 = [];
    for (i = 0; i < cities.length; i++) {
        if (cities[i].areaId == val) {
            cityStates1.push(cities[i]);
        }
    }

    $("#city1").html(Utilities.PopulateDropDownFromArray(cityStates1, 0, 1));
});

function clearAllTextboxValues() {
    $("input[type='text'], textarea, input[type='password']").val('');
}

function getLookUp() {
    $.ajax({
        url: API_BASE_URL + "/CustomerCentre/GetLookUp",
        type: "Get",
        contentType: "application/json",
        success: function (data) {

            cities = data.Cities;
            states = data.States;
            pricingLevels = data.PricingLevels;

            populateLookUp();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function populateLookUp() {
    $("#state").html('<option>-Select State-</options>' + Utilities.PopulateDropDownFromArray(states, 0, 1));
    $("#state1").html('<option>-Select State-</options>' + Utilities.PopulateDropDownFromArray(states, 0, 1));
    $("#pricingLevel").html(Utilities.PopulateDropDownFromArray(pricingLevels, 0, 1));

}

$(function () {
    getLookUp();
    searchCustomerDetails();
});

function searchCustomerDetails() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerCentre/GetAllAgentLedger",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors.
            setTimeout(onSuccessSearchCustomerDetails, 500, data);

            
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}



function onSuccessSearchCustomerDetails(data) {
    Utilities.Loader.Hide();
    ledger = data;
    console.log({ data });
    var objToShow = [];
    for (i = 0; i < data.length; i++) {
        var openingBalanceRow = data[i].balanceState === 'Credit' ? '<span style="color:red">&#8358;' + Utilities.FormatCurrency(data[i].openingBalance) + '</span>'
            : '&#8358;' + Utilities.FormatCurrency(data[i].openingBalance);
        objToShow.push([
                        (i + 1),
                        data[i].ledgerName,
                        '<center>' + ((data[i].extra1 == "") ? "-" : (data[i].extra1)) + '</center>',
                        '<center>' + ((data[i].areaName == null) ? "-" : (data[i].areaName)) + '</center>',
                        openingBalanceRow ,
            '<button class="btn btn-sm btn-info" onclick="getCustomerDetails(' + data[i].ledgerId + ')"><i class="fa fa-info"></i> Edit </button>&nbsp;' +
            '<button class="btn btn-sm btn-info" onclick="GetAllOpeningBalance(' + data[i].ledgerId + ')"><i class="fa fa-eye"></i> Transaction Details </button>&nbsp;',
                        '<center><button class="btn btn-sm btn-danger" onclick="deleteLedger(' + data[i].ledgerId + ')"><i class="fa fa-trash"></i> Delete</button></center>'
        ]);
    }

    var count = 0;
    if (checkPriviledge("frmCustomer", "View") === false) {
        objToShow.forEach(function (item) {
            item.splice(5, 1);
        });
        count++;
    }

    if (checkPriviledge("frmCustomer", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 6 - count;
            item.splice(recalculateIndex, 1);
        });
    }
    if (checkPriviledge("frmCustomer", "Update") === false) {
        var inputArray = document.getElementById('AgentDetails').querySelectorAll("input");
        inputArray.forEach(p => p.setAttribute("readonly", "readonly"));
    }
    if (table != "") {
        table.destroy();
    }
    table = $('#customerAccountListTbody').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    Utilities.Loader.Hide();
}

function searchCustomerDetailsByDate(FromDate, ToDate, YearId) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerCentre/GetAllAgentLedger1?DateFrom1=" + FromDate + "&DateTo1=" + ToDate + "&FYearId=" + YearId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors.
            setTimeout(onSuccessSearchCustomerDetailsByDate, 500, data);

        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}


function onSuccessSearchCustomerDetailsByDate(data) {
    Utilities.Loader.Hide();
    ledger = data;
    var objToShow = [];
    for (i = 0; i < data.length; i++) {
        objToShow.push([
            (i + 1),
            data[i].ledgerName,
            '<center>' + ((data[i].extra1 == "") ? "-" : (data[i].extra1)) + '</center>',
            '<center>' + ((data[i].areaName == null) ? "-" : (data[i].areaName)) + '</center>',
            '&#8358;' + Utilities.FormatCurrency(data[i].openingBalance),
            '<button class="btn btn-sm btn-info" onclick="getCustomerDetails(' + data[i].ledgerId + ')"><i class="fa fa-info"></i> Edit </button>&nbsp;' +
            '<button class="btn btn-sm btn-info" onclick="GetAllOpeningBalance(' + data[i].ledgerId + ')"><i class="fa fa-eye"></i> Transaction Details </button>&nbsp;',
            '<center><button class="btn btn-sm btn-danger" onclick="deleteLedger(' + data[i].ledgerId + ')"><i class="fa fa-trash"></i> Delete</button></center>'
        ]);
    }

    var count = 0;
    if (checkPriviledge("frmCustomer", "View") === false) {
        objToShow.forEach(function (item) {
            item.splice(5, 1);
        });
        count++;
    }

    if (checkPriviledge("frmCustomer", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 6 - count;
            item.splice(recalculateIndex, 1);
        });
    }
    if (checkPriviledge("frmCustomer", "Update") === false) {
        var inputArray = document.getElementById('AgentDetails').querySelectorAll("input");
        inputArray.forEach(p => p.setAttribute("readonly", "readonly"));
    }
    if (table != "") {
        table.destroy();
    }
    table = $('#customerAccountListTbody').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    Utilities.Loader.Hide();
}

function addNewLedger() {
    Utilities.Loader.Show();

    var checkActive = false;
    if ($("#active").prop('checked') == true) {
        checkActive = true;
    }

    if ($('#AgentDetails').hasClass('in'))
    {
        NewLedgerList.push({
            LedgerName: $("#newCustomerName").val(),
            IsActive: checkActive,
            BankAccountNumber: $("#accountNumber").val(),
            BranchName: $("#branchName").val(),
            BranchCode: $("#branchCode").val(),
            Address: $("#address").val(),
            CreditLimit: $("#creditLimit").val(),
            CreditPeriod: $("#creditPeriod").val(),
            Cst: $("#position").val(),
            AreaId: $("#state1").val(),
            RouteId: $("#city1").val(),
            MailingName: $("#companyName").val(),
            Phone: $("#phoneNumber").val(),
            Email: $("#emailAddress").val(),
            PricinglevelId: $("#pricingLevel").val(),
            Tin: $("#TIN").val(),
            Extra1: $("#agentCode").val(),
            //Narration: $("#customerName").val()
            Mobile: $("#mobileNumber").val(),
        });
    }
    else {
        NewLedgerList = excelArrayData;
    }

    $.ajax({
        url: API_BASE_URL + "/CustomerCentre/SaveNewCustomer",
        type: "Post",
        data: JSON.stringify(NewLedgerList),
        contentType: "application/json",
        success: function (data) {
            var msg = data;

            Utilities.Loader.Hide();

            if (msg.ResponseCode == 300) {
                Utilities.ErrorNotification(msg.ResponseMessage);
            }
            else if (msg.ResponseCode == 200) {
                $("#AgentDetails").modal("hide");
                Utilities.SuccessNotification(msg.ResponseMessage);
                searchCustomerDetails();
                clearExcelArrayTable();
            }
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

var openingBalance = 0;
var objToShow = [];

function GetAllOpeningBalance(ledgerId)
{
    Utilities.Loader.Show();
    lId = ledgerId

    var openDateFrom1 = $("#fromDate").val();
    var openDateTo1 = $("#toDate").val();

    //get list of transactions
    $.ajax({
        url: API_BASE_URL + "/CustomerCentre/GetLedgerOpeningBalance?DateFrom1=" + openDateFrom1 + "&DateTo1=" + openDateTo1 +"&ledgerId=" + lId,
        type: "Get",
        contentType: "application/json",
        success: function (data)
        {
            Utilities.Loader.Hide();

            objToShow = [];
            openingBalance = 0;
            
            //if (parseFloat(data.openDebit) > parseFloat(data.openCredit))
            //{
            //    openingBalance += (data.openDebit - data.openCredit);
            //}
            //else {
            //    openingBalance += (data.openCredit - data.openDebit);
            //}

            openingBalance += (parseFloat(data.openDebit) - parseFloat(data.openCredit));

            objToShow.push([
                (1),
                openDateFrom1,
                "Opening Balance",
                "-",
                "-",
                '&#8358;' + Utilities.FormatCurrency(data.openDebit),
                '&#8358;' + Utilities.FormatCurrency(data.openCredit),
                '&#8358;' + Utilities.FormatCurrency(openingBalance)
            ]);

            Utilities.Loader.Hide();
            allLedgerDetails(lId);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });




}


function allLedgerDetails(ledgerId) {
    Utilities.Loader.Show();
    lId = ledgerId;
    var openDateFrom1 = $("#fromDate").val();
    var openDateTo1 = $("#toDate").val();
    //get list of transactions
    $.ajax({
        url: API_BASE_URL + "/CustomerCentre/GetLedgerDetails?DateFrom1=" + openDateFrom1 + "&DateTo1=" + openDateTo1 + "&ledgerId=" + lId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
         
            var transactions = [];
            transactions = data.LedgerDetails.Table1;
            console.log(transactions);
           
            var sl = 0;
            var SN = transactions.length + 1;

            for (var i = transactions.length - 1; i >= 0; i--)
            {
                var openingBalance1 = (transactions[i].debit - transactions[i].credit);

                openingBalance += openingBalance1;
               
                objToShow.push([
                                    (SN - i),
                                    Utilities.FormatJsonDate(transactions[i].date),
                                    transactions[i].voucherTypeName,
                                    transactions[i].invoiceNo,
                                    transactions[i].Memo,
                                    '&#8358;' +  Utilities.FormatCurrency(transactions[i].debit),
                                    '&#8358;' +  Utilities.FormatCurrency(transactions[i].credit),
                    '&#8358;' + Utilities.FormatCurrency(openingBalance)
                ]);
            }

            if (table1 != "")
            {
                table1.destroy();
            }

            table1 = $('#LedgerTransactionListTbody').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });

            $("#totalBalance").val(Utilities.FormatCurrency(openingBalance));
            $("#panelTitle").html("<b>Transactions for "+ledger.find(p=>p.ledgerId==ledgerId).ledgerName+"</b>");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });

}

function getCustomerDetails(lId) {
    Utilities.Loader.Show();
    //get ledger details for update/delete
    $.ajax({
        url: API_BASE_URL + "/CustomerCentre/GetCustomerDetails?ledgerId=" + lId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            $("#updateLedger").show();
            $("#newLedger").hide();
            console.log(data);
            var output = "";
            $("#newCustomerName").val(data.LedgerName);
            $("#companyName").val(data.MailingName);
            $("#agentCode").val(data.Extra1);
            $("#branchCode").val(data.BranchCode);
            $("#branchName").val(data.BranchName);
            $("#accountNumber").val(data.BankAccountNumber);
            $("#phoneNumber").val(data.Phone);
            $("#mobileNumber").val(data.Mobile);
            $("#address").val(data.Address);
            $("#emailAddress").val(data.Email);
            $("#creditLimit").val(data.CreditLimit);
            $("#position").val(data.Cst);
            $("#TIN").val(data.Tin);
            $("#creditPeriod").val(data.CreditPeriod);
            $("#state1").val(data.AreaId);
            $("#city1").val(data.RouteId);
            $("#pricingLevel").val(data.PricinglevelId);

            ledgerIdForEdit = data.LedgerId

            $("#AgentDetails").modal("show");

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}



function updateLedgerDetails() {
    Utilities.Loader.Show();

    var checkActive = false;
    if ($("#active").prop('checked') == true) {
        checkActive = true;
    }

    NewLedger = {
        LedgerId: ledgerIdForEdit,
        LedgerName: $("#newCustomerName").val(),
        IsActive: checkActive,
        BankAccountNumber: $("#accountNumber").val(),
        BranchName: $("#branchName").val(),
        BranchCode: $("#branchCode").val(),
        Mobile: $("#mobileNumber").val(),
        Address: $("#address").val(),
        CreditLimit: $("#creditLimit").val(),
        CreditPeriod: $("#creditPeriod").val(),
        Cst: $("#position").val(),
        AreaId: $("#state1").val(),
        RouteId: $("#city1").val(),
        MailingName: $("#companyName").val(),
        Phone: $("#phoneNumber").val(),
        Email: $("#emailAddress").val(),
        PricinglevelId: $("#pricingLevel").val(),
        Tin: $("#TIN").val(),
        Extra1: $("#agentCode").val()
        //Pan: $("#customerName").val(),
        //Narration: $("#customerName").val()
    },
    console.log(NewLedger);
    $.ajax({
        url: API_BASE_URL + "/CustomerCentre/UpdateLedgerDetails",
        type: "Post",
        data: JSON.stringify(NewLedger),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var msg = data.toString();

            Utilities.Loader.Hide();

            if (msg == "Agent Updated Successfully") {
                $("#AgentDetails").modal("hide");
                Utilities.SuccessNotification(msg);
                searchCustomerDetails();
            }
            else {
                Utilities.ErrorNotification("Agent update Failed");
            }
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

$("#deleteLedger").click(function () {
    //delete ledger
    deleteLedger(lId);
});

function deleteLedger(id)
{
    if (confirm('Delete this Ledger?'))
    {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/CustomerCentre/DeleteLedger?ledgerId=" + id,
            type: "Get",
            contentType: "application/json",
            success: function (data) {
                console.log(data);

                Utilities.Loader.Hide();

                if (data.toString() == "Ledger name has an existing transaction") {
                    Utilities.ErrorNotification(data);
                }
                else {
                    Utilities.SuccessNotification(data);
                    clearAllTextboxValues();
                    searchCustomerDetails();
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}

function uploadModal() {
    excelArrayData = [];
    $("#uploadCustomermodal").modal("show");
}

function ExportToTable() {
    var regex = /^([a-zA-Z0-9()\s_\\.\-:])+(.xlsx|.xls)$/;
    /*Checks whether the file is a valid excel file*/
    if (regex.test($("#excelfile").val().toLowerCase())) {
        var xlsxflag = false; /*Flag for checking whether excel is .xls format or .xlsx format*/
        if ($("#excelfile").val().toLowerCase().indexOf(".xlsx") > 0) {
            xlsxflag = true;
        }
        /*Checks whether the browser supports HTML5*/
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                var data = e.target.result;
                /*Converts the excel data in to object*/
                if (xlsxflag) {
                    var workbook = XLSX.read(data, { type: 'binary' });
                }
                else {
                    var workbook = XLS.read(data, { type: 'binary' });
                }
                /*Gets all the sheetnames of excel in to a variable*/
                var sheet_name_list = workbook.SheetNames;

                var cnt = 0; /*This is used for restricting the script to consider only first sheet of excel*/
                sheet_name_list.forEach(function (y) { /*Iterate through all sheets*/
                    /*Convert the cell value to Json*/
                    if (xlsxflag) {
                        var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
                        console.log("exceljson:", exceljson);
                    }
                    else {
                        var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);
                        console.log("exceljson:", exceljson);
                    }

                    if (exceljson.length > 0 && cnt == 0) {
                        //BindTable(exceljson, '#exceltable');  
                        console.log(exceljson);
                        for (i = 0; i < exceljson.length; i++) {
                            console.log(exceljson[i].Email);
                            exceljson[i].LedgerName = exceljson[i]["Customer Name"] == undefined ? "" : exceljson[i]["Customer Name"];
                            exceljson[i].CustomerCode = exceljson[i]["Customer Code"] == undefined ? "" : exceljson[i]["Customer Code"];
                            exceljson[i].BranchCode = exceljson[i]["Branch Code"] == undefined ? "" : exceljson[i]["Branch Code"];
                            exceljson[i].BranchName = exceljson[i]["Branch Name"] == undefined ? "" : exceljson[i]["Branch Name"];
                            exceljson[i].BankAccountNumber = exceljson[i]["Account No"] == undefined ? "" : exceljson[i]["Account No"];
                            exceljson[i].Phone = exceljson[i]["Phone"] == undefined ? "" : exceljson[i]["Phone"];
                            exceljson[i].Mobile = exceljson[i]["Mobile"] == undefined ? "" : exceljson[i]["Mobile"];
                            exceljson[i].Address = exceljson[i]["Address"] == undefined ? "" : exceljson[i]["Address"];
                            exceljson[i].Email = exceljson[i]["Email"] == undefined ? "" : exceljson[i]["Email"];
                            exceljson[i]["Bill by Bill"] = exceljson[i]["Bill by Bill"] == undefined ? "" : exceljson[i]["Bill by Bill"];
                            exceljson[i].CreditLimit = exceljson[i]["Credit Limit"] == undefined ? "" : exceljson[i]["Credit Limit"];
                            exceljson[i].Cst = exceljson[i]["Position"] == undefined ? "" : exceljson[i]["Position"];
                            exceljson[i].Tin = exceljson[i]["TIN"] == undefined ? "" : exceljson[i]["TIN"];
                            exceljson[i].CreditPeriod = exceljson[i]["Credit Period"] == undefined ? "" : exceljson[i]["Credit Period"];
                            exceljson[i].OpeningBalance = exceljson[i]["Opening Balance"] == undefined ? "" : exceljson[i]["Opening Balance"];
                            exceljson[i].MailingName = exceljson[i]["Mailing Name"] == undefined ? "" : exceljson[i]["Mailing Name"];
                            exceljson[i].Narration = exceljson[i]["Narration"] == undefined ? "" : exceljson[i]["Narration"];
                            exceljson[i]["State"] = exceljson[i]["State"] == undefined ? "" : exceljson[i]["State"];
                            exceljson[i]["City"] = exceljson[i]["City"] == undefined ? "" : exceljson[i]["City"];
                            exceljson[i]["Pricing Level"] = exceljson[i]["Pricing Level"] == undefined ? "" : exceljson[i]["Pricing Level"];

                            var state = states.find(p => p.areaName.toUpperCase().trim() == exceljson[i]["State"].toUpperCase().trim());
                            exceljson[i].AreaId = state == undefined ? "" : state.areaId;
                            var city = cities.find(p => p.routeName.toUpperCase().trim() == exceljson[i]["City"].toUpperCase().trim() && p.areaId == exceljson[i].AreaId);
                            exceljson[i].RouteId = city == undefined ? "" : city.routeId;
                            var pricingLevel = pricingLevels.find(p => p.pricinglevelName.toUpperCase().trim() == exceljson[i]["Pricing Level"].toUpperCase().trim());
                            exceljson[i].PricinglevelId = pricingLevel == undefined? "" : pricingLevel.pricinglevelId;
                           
                            excelArrayData.push(exceljson[i])
                            console.log(exceljson[i].Email);
                        }
                        cnt++;
                        renderExcelDataToTable();
                        console.log(excelArrayData);
                    }
                });
                //$('#exceltable').show();  
            }
            if (xlsxflag) {/*If excel file is .xlsx extension than creates a Array Buffer from excel*/
                reader.readAsArrayBuffer($("#excelfile")[0].files[0]);
            }
            else {
                reader.readAsBinaryString($("#excelfile")[0].files[0]);
            }
        }
        else {
            alert("Sorry! Your browser does not support HTML5!");
        }
    }
    else {
        alert("Please upload a valid Excel file!");
    }
}


function renderExcelDataToTable() {
    var objToShow = [];
    for (i = 0; i < excelArrayData.length; i++) {
        objToShow.push([
            (i + 1),
            excelArrayData[i].LedgerName,
            excelArrayData[i].CustomerCode,
            excelArrayData[i].State,
            excelArrayData[i].OpeningBalance,
            excelArrayData[i].City,
            excelArrayData[i].Narration,
            excelArrayData[i].MailingName,
            excelArrayData[i].Address,
            excelArrayData[i].Phone,
            excelArrayData[i].Mobile,
            excelArrayData[i].Email,
            excelArrayData[i].CreditPeriod,
            excelArrayData[i].CreditLimit,
            excelArrayData[i].BankAccountNumber,
            excelArrayData[i].BranchName,
            excelArrayData[i].BranchCode,
            '<button class="btn btn-sm btn-danger" onclick="removeFromExcelArray(' + i + ')"><i class="fa fa-trash"></i></button>'
        ]);
    }
    if (tableToUpload != "") {
        tableToUpload.destroy();
    }
    tableToUpload = $('#customerListToUpload').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "responsive": true
    });
   // $("#customerName").val(""),
    //$("#email").val(""),
    //$("#address").val(""),
    //$("#phoneNumber").val("")
}

function removeFromExcelArray(index) {
    excelArrayData.splice(index, 1);
    renderExcelDataToTable();
}

function clearExcelArrayTable() {
    excelArrayData = [];
    renderExcelDataToTable();
}

//restrict phone inputs to numbers:
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

//validate email
$(function () {
    var regExp = /^([\w\.\+]{1,})([^\W])(@)([\w]{1,})(\.[\w]{1,})+$/;
    $('[type="email"]').on('keyup', function () {
        regExp.test($(this).val()) ? $('#invalidEmail').hide() : $('#invalidEmail').show();
    });
});