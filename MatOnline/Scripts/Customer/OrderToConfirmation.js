﻿$(function () {

});


function getDetails(quotationMasterId)
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetSalesQuotationForConfirmation?salesQuotationMasterId="+quotationMasterId,
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            Utilities.Loader.Hide();
        },
        error:function(err)
        {
            Utilities.Loader.Hide();
        }
    });
}