﻿var pendingSales = [];
var stores = [];
var tax = [];
var bankLedger = [];
var cash = [];
var customersOnly = [];
var salesInvoiceLineItems = [];
var units = [];
var selectedTax = {};
var salesMasterId;

var lookUpAjax = $.ajax({
    url: API_BASE_URL + "/Pos/GetLookups",
    type: "GET",
    contentType: "application/json",
});

var productLookupAjax = $.ajax({
    url: API_BASE_URL + "/ProductCreation/GetProducts",
    type: "GET",
    contentType: "application/json",
});

$(function () {

    Utilities.Loader.Show();
    $.when(lookUpAjax, productLookupAjax)
    .done(function (dataLookUp, dataProduct) {
        console.log(dataLookUp);

        allProducts = dataProduct[2].responseJSON;
        pricingLevels = dataLookUp[0].PricingLevel;
        printers = dataLookUp[0].Printers;
        products = dataLookUp[0].Products;
        racks = dataLookUp[0].Racks;
        salesPoints = dataLookUp[0].SalesPoints;
        stores = dataLookUp[0].Stores;
        tax = dataLookUp[0].Tax;
        receiptNo = dataLookUp[0].ReceiptNo;
        bankLedger = dataLookUp[0].BankLedger;
        cash = dataLookUp[0].Cash;
        units = dataLookUp[0].Units;
        batches = dataLookUp[0].Batches;
        customersOnly = dataLookUp[0].CustomerOnly;
    });


})

$(function () {    
    
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PendingSales/GetPendingSales",
        type: "Get",
        contentType:"application/json",
        success:function(data)
        {
            console.log(data);
            //pendingSales = data.Table;
            data.Table.forEach(function (item) {
                item.date = Utilities.FormatJsonDate(item.date);
                pendingSales.push(item);
            });
            renderGrid();
            Utilities.Loader.Hide();
        },
        error:function(err)
        {
            Utilities.Loader.Hide();
        }
    });
});


function renderGrid()
{       
    $("#pendingSalesGrid").kendoGrid({
        dataSource:  pendingSales,
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 10,
            pageSizes: [10, 25, 50, 100, 1000],
            previousNext: true,
            buttonCount: 5,
        },
        columns: [
            { field: "PendingsalesMasterId", title: "Transaction Id" },
            { field: "date", title: "Date" },
            { field: "narration", title: "Narration" },
            {
                command: [{ name: 'viewDetails', text: 'View Details', click: viewDetailsClick }]
            }],
        //editable: "popup"
    });

}

//function renderSalesInvoiceLineItems() {
//    var output = "";
//    var balance = 0.0;
//    var amountPaid = parseFloat($("#amountPaid").val());
//    var totalTaxAmount = 0.0;
//    var totalAmount = 0.0;
//    var totalBillDiscountAmount = parseFloat($("#billDiscount").val());

//    // var totalTaxAmount = parseFloat($("#totalTaxAmount").val());
//    //var totalAmount = parseFloat($("#totalAmount").val());
//    //totalBillDiscountAmount = 0.0;
//    // totalAmount = 0.0;
//    tax.taxName = tax.taxName == undefined ? "NA" : tax.taxName;
//    $.each(salesInvoiceLineItems, function (count, row) {
//        output +=
//            '<tr>\
//                <td style="white-space: nowrap;"><button onclick="removeLineItem('+ row.ProductId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
//                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
//                <td style="white-space: nowrap;">' + findProduct(row.ProductId).barcode + '</td>\
//                <td style="white-space: nowrap;">' + findProduct(row.ProductId).ProductCode + '</td>\
//                <td style="white-space: nowrap;">' + findProduct(row.ProductId).ProductName + '</td>\
//                <td style="white-space: nowrap;">' + findStore(+row.GodownId).godownName + '</td>\
//                <td style="white-space: nowrap;">' + row.Qty + '</td>\
//                <td style="white-space: nowrap;">' + findUnit(row.UnitId).unitName + '</td>\
//                <td style="white-space: nowrap;">' + findBatch(row.BatchId).batchNo + '</td>\
//                <td style="white-space: nowrap;">' + row.Rate + /*'</td>\
//                <td style="white-space: nowrap;">' + row.GrossAmount + '</td>\
//                <td style="white-space: nowrap;">' + row.Discount + '</td>\
//                <td style="white-space: nowrap;">'+ row.NetAmount + */ '</td>\
//                <td style="white-space: nowrap;">' + row.TaxName + '</td>\
//                <td style="white-space: nowrap;">'+ row.TaxAmount + '</td>\
//                <td style="white-space: nowrap;">'+ row.Amount + '</td>\
//            </tr>\
//            ';
//        totalTaxAmount = totalTaxAmount + parseFloat(row.TaxAmount);
//        //totalBillDiscountAmount = totalBillDiscountAmount + row.Discount;
//        totalAmount = totalAmount + parseFloat(row.Amount);
//    });
//    var percentDiscountNum = (totalBillDiscountAmount * 100) / totalAmount;
//    var subTotal = totalAmount - totalBillDiscountAmount;
//    grandTotal = subTotal + totalTaxAmount;//- totalBillDiscountAmount;
//    balance = 0 - grandTotal + amountPaid;
//    $("#totalAmount").val(totalAmount);
//    $("#grandTotal").val(grandTotal);
//    $("#posLineItemTbody").html(output);
//    $("#balance").val(balance);
//    $("#totalTaxAmount").val(totalTaxAmount);
//    $("#percentDiscount").val(percentDiscountNum);
//    $("#subTotal").val(subTotal);

//}


$("#pushPOS").click(function () {
   
    window.location = "/Customer/Pos?salesMasterId=" + salesMasterId;
})

//function viewDetailsClick(e) {
//    var tr = $(e.target).closest("tr"); // get the current table row (tr)
//    // get the data bound to the current table row
//    var data = this.dataItem(tr);//userid
//    console.log(data);
//}

function viewDetailsClick(e) {
    var tr = $(e.target).closest("tr");
    var dataItem = this.dataItem(tr);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PendingSales/ViewSales?pendingsalesMasterId=" + dataItem.PendingsalesMasterId + "&salesOrPending=" + true,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var details = data.SalesDetailsInfo;
            var output = "";
           
            //$("#journalNoDiv").html(data.JournalNo);
            $("#orderNo").html(data.SalesMasterInfo.VoucherNo); 
            $("#agentName").html(findCust(data.SalesMasterInfo.LedgerId, data.SalesMasterInfo.Extra1));
            $("#orderDate").html(data.SalesMasterInfo.ExtraDate.split('T')[0]);
            $("#narration").val(data.SalesMasterInfo.Narration);
            $("#amountPaid").val(data.SalesMasterInfo.AmountTendered);
            $("#totalTaxAmount").val(data.SalesMasterInfo.TaxAmount);
            $("#balance").val(data.SalesMasterInfo.ChangeGiven);
            $("#tellerNo").val(data.SalesMasterInfo.POSTellerNo);
            $("#totalAmount").val(data.SalesMasterInfo.TotalAmount);
            $("#percentDiscount").val(data.SalesMasterInfo.Discount);
            $("#billDiscount").val(data.SalesMasterInfo.BillDiscount);
            $("#grandTotal").val(data.SalesMasterInfo.GrandTotal);

            salesMasterId = data.SalesMasterInfo.SalesMasterId;

            //$("#journalMasterId").val(data.JournalMasterId);
            //$("#debitAmtTxt").val(Utilities.FormatCurrency(data.TotalAmount));
            //$("#creditAmtTxt").val(Utilities.FormatCurrency(data.TotalAmount));
            //var output = "";
            for (var i = 0; i < details.length; i++) {
                var unitName = findUnit(details[i].UnitId).unitName == undefined ? "N/A": findUnit(details[i].UnitId).unitName;
                var batchNo = findStore(+details[i].GodownId).godownName = undefined ? "N/A" : findStore(+details[i].GodownId).godownName;
                output +=
           '<tr>\
                <td style="white-space: nowrap;">'+ (i + 1) +  '</td>\
                <td style="white-space: nowrap;">' + findProduct(details[i].ProductId).ProductCode + '</td>\
                <td style="white-space: nowrap;">' + findProduct(details[i].ProductId).ProductName + '</td>\
                <td style="white-space: nowrap;">' + findStore(+details[i].GodownId).godownName + '</td>\
                <td style="white-space: nowrap;">' + details[i].Qty + '</td>\
                <td style="white-space: nowrap;">' + unitName + '</td>\
                <td style="white-space: nowrap;">' + batchNo + '</td>\
                <td style="white-space: nowrap;">' + details[i].Rate + /*'</td>\
                <td style="white-space: nowrap;">' + details[i].GrossAmount + '</td>\
                <td style="white-space: nowrap;">' + row.Discount + '</td>\
                <td style="white-space: nowrap;">'+ row.NetAmount + */ '</td>\
                <td style="white-space: nowrap;">' + details[i].TaxName + '</td>\
                <td style="white-space: nowrap;">' + details[i].TaxAmount + '</td>\
                <td style="white-space: nowrap;">' + details[i].Amount + '</td>\
            </tr>\
            ';
            }
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry, could not load pending details.");
            Utilities.Loader.Hide();
        }
    });
}

function findProduct(productId) {
    var output = {};
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].ProductId == productId) {
            output = allProducts[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}


function findCust(ledgerId, selectedMode) {

    var output = "";
    switch (selectedMode) {
        case "cash payment":
            //for (i = 0; i < cash.length; i++) {
            //    if (cash[i].ledgerId == ledgerId) {
            //        output = cash[i].ledgername;
            //        break;
            //    }
            //}
            output = findCustValue(cash, ledgerId);
            break;
        case "bank payment":
            //for (i = 0; i < bankLedger.length; i++) {
            //    if (bankLedger[i].ledgerId == ledgerId) {
            //        output = bankLedger[i].ledgername;
            //        break;
            //    }
            //}
            output = findCustValue(bankLedger, ledgerId);
            break;
        case "cheque payment":
            //for (i = 0; i < bankLedger.length; i++) {
            //    if (bankLedger[i].ledgerId == ledgerId) {
            //        output = bankLedger[i].ledgername;
            //        break;
            //    }
            //}
            output = findCustValue(bankLedger, ledgerId);
            break;
        case "both cash and bank payment":
            output = findCustValue(cash, ledgerId) + " and " + findCustValue(bankLedger, ledgerId);
            break;
    }


    return output;
}

function findCustValue(selectedObj, ledgerId) {
    var output = "";
    for (i = 0; i < selectedObj.length; i++) {
        if (selectedObj[i].ledgerId == ledgerId) {
            output = selectedObj[i].ledgername;
            break;
        }
    }
    return output;
}