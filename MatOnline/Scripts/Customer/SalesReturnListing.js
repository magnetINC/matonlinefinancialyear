﻿var thisSalesReturn = {};
var salesReturnList = [];
var salesReturnDetails = [];
var returnDetails = [];
var lineItems = [];

var ledgers = [];
var products = [];
var units = [];
var stores = [];
var batches = [];
var users = [];
var voucherTypes = [];

var table = "";

$(function () {
    $("#formNo").hide();
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
    getLookUps();
});

function getLookUps()
{
    Utilities.Loader.Show();
    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/SalesReturn/GetLookUps",
        type: "GET",
        contentType: "application/json",
    });
    $.when(lookUpAjax)
    .done(function (dataLookUp) {
        ledgers = dataLookUp.Ledgers;
        units = dataLookUp.Unit;
        users = dataLookUp.Users;
        stores = dataLookUp.Stores;
        products = dataLookUp.Products;
        batches = dataLookUp.Batch;
        tax = dataLookUp.tax;
        voucherTypes = dataLookUp.VoucherTypes;

        GetPendingList("2017-01-01", $("#toDate").val(), "");
    });
}
function GetPendingList(fromDate, todate, approved)
{
    var newToDate = todate + " 23:59:59 PM";
    console.log(newToDate);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesReturn/GetMasterToList?fromDate=" + fromDate + "&toDate=" + newToDate + "&Status=" + approved,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log("sales Return", data);

            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 500, data);

        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function onSuccess(data) {
    salesReturnList = data;

    var output = "";
    var objToShow = [];
    var statusOutput = "";
    $.each(salesReturnList, function (count, row) {
        if (row.status == "Pending") {
            statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending </span>';
        }
        else if (row.status == "Approved") {
            statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Approved</span>';
        }
        else if (row.status == "Cancelled") {
            statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
        }
        objToShow.push([
            count + 1,
            row.salesReturnMasterId,
            Utilities.FormatJsonDate(row.date),
            findCustomer(row.ledgerId).ledgerName,
            '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
            findUser(row.userId).userName,
            //statusOutput,
            '<button type="button" class="btn btn-primary btn-sm" onclick="getSalesReturnDetails(' + row.salesReturnMasterId + ')"><i class="fa fa-eye"></i> View</button>'
        ]);
    });
    if (checkPriviledge("frmSalesReturn", "View") === false) {
        objToShow.forEach(function (item) {
            item.splice(6, 1);
        });
    }
    
    table = $('#salesReturnListTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    //$("#salesQuotationListTbody").html(output);
    Utilities.Loader.Hide();
}

function getSalesReturnDetails(id) {
    Utilities.Loader.Show();
    for (var i = 0; i < salesReturnList.length; i++) {
        if (salesReturnList[i].salesReturnMasterId == id) {
            thisSalesReturn = salesReturnList[i];
        }
    }
    console.log(thisSalesReturn);
    $.ajax({
        url: API_BASE_URL + "/SalesReturn/GetSalesReturnDetails/" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            returnDetails = data;
            console.log(data);
            //if (thisSalesReturn.status == "Pending") {
            //    $("#approveReturn").show();
            //    statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending Approval</span>';
            //}
            //else if (thisSalesReturn.status == "Approved") {
            //    $("#approveReturn").hide();
            //    statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Approved</span>';
            //}
            //else if (thisSalesReturn.status == "Cancelled") {
            //    $("#approveReturn").hide();
            //    statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
            //}


            //$("#orderNoDiv").html(row.invoiceNo);
            $("#orderDateDiv").html(Utilities.FormatJsonDate(data.master.date));
            //$("#orderNoDiv").html(row.invoiceNo);
            $("#raisedByDiv").html(findUser(data.master.userId).userName);
            $("#raisedForDiv").html(findCustomer(data.ledger.ledgerId).ledgerName);
            //$("#statusDiv").html(statusOutput);

            $("#totalAmount").val(Utilities.FormatCurrency(data.master.totalAmount));
            $("#billDiscount").val(Utilities.FormatCurrency(data.master.discount));
            $("#grandTotal").val(Utilities.FormatCurrency(data.master.grandTotal));
            
            var output = "";
            $.each(data.records, function (count, row) {
                output += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + row.product.productCode + '</td>\
                            <td>' + row.product.productId + '</td>\
                            <td>' + row.product.productName + '</td>\
                            <td>' + findStore(row.detail.godownId).godownName + '</td>\
                            <td>' + row.detail.qty + '</td>\
                            <td>' + findUnit(row.detail.unitId).unitName + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency1(row.detail.rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency1(row.detail.grossAmount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency1(row.detail.discount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency1(row.detail.netAmount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency1(row.detail.taxAmount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency1(row.detail.amount) + '</td>\
                        </tr>';
            });


            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
            var masterId = data.master.salesReturnMasterId;
            $("#edit").attr("data-id", masterId);
            $("#delete").attr("data-id", masterId);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

$("#edit").click(function() {
    var id = $(this).attr("data-id");
    //goto edit view 
    location.href = "/customer/SalesReturn/edit/" + id;

});


$("#delete").click(function () {
    var id = $(this).attr("data-id");
    //perform delete task 
    Utilities.ConfirmFancy("Are You sure you want to delete",
        function() {
            $.ajax(API_BASE_URL + "/SalesReturn/Delete/" + id,
                {
                    type: "delete",
                    success: function(data) {
                        Utilities.InfoNotification(data.message);
                        location.href = "/Customer/SalesReturn/SalesReturnListing";

                    }
                });
        });

});
function approveReturn()
{
    Utilities.Loader.Show();
    var toSave = {};
    $.each(returnDetails, function (count, row) {
        lineItems.push({
            SL: (count + 1),
            ProductId: row.productId,
            CategoryId: 1,
            Projectid: 1,
            Discount: row.discount,
            GodownId: row.godownId,
            GrossAmount: row.grossAmount,
            NetAmount: row.netAmount,
            SalesDetailsId: row.salesDetailsId,
            Description: "",
            Quantity: row.qty,
            UnitId: row.unitId,
            UnitConversionId: row.unitConversionId,
            RackId: row.rackId,
            BatchId: row.batchId,
            Rate: row.rate,
            TaxId: 10012,
            taxAmount: row.taxAmount,
            Amount: row.amount
        });
    });
    toSave.ReturnNo = thisSalesReturn.returnNo;
    toSave.Date = thisSalesReturn.date;
    toSave.LedgerId = thisSalesReturn.ledgerId;
    toSave.Narration = thisSalesReturn.narration;
    toSave.NetAmount = thisSalesReturn.totalAmount;
    toSave.TransportationCompany = thisSalesReturn.transportationCompany;
    toSave.LrNo = 1;
    toSave.SalesManId = thisSalesReturn.userId;
    toSave.CurrencyId = thisSalesReturn.pricinglevelId;
    toSave.SalesMasterId = thisSalesReturn.salesMasterId;
    toSave.DiscountAmount = thisSalesReturn.discount;
    toSave.TaxAmount = thisSalesReturn.taxAmount;
    //toSave.NetAmount = ((this.) - thisSalesReturn.discount);
    toSave.GrandTotal = thisSalesReturn.grandTotal;
    toSave.VoucherNo = thisSalesReturn.returnNo;
    toSave.SalesAccount = thisSalesReturn.salesAccount;
    toSave.LineItems = lineItems;

    console.log(toSave);
    $.ajax({
        url: API_BASE_URL + "/SalesReturn/Save",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            Utilities.Loader.Hide();
            if (data == true) {
                Utilities.Loader.Hide();
                Utilities.SuccessNotification("Sales Return saved");
                $("#detailsModal").modal("hide");
                if (table != "") {
                    table.destroy();
                }
                GetPendingList("2017-01-01", $("#toDate").val(), "");
                //window.location = "/Customer/RejectionIn/RejectionInListing";
            }
            else {
                Utilities.Loader.Hide();
                Utilities.ErrorNotification("Sales Return Failed");
            }
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}
//function cancelReturn()
//{
//    Utilities.Loader.Show();
//    $.ajax({
//        url: API_BASE_URL + "/SalesReturn/CancelReturn?id=" + thisSalesReturn.salesReturnMasterId,
//        type: "Get",
//        contentType: "application/json",
//        success: function (data) {
//            console.log(data);
//            Utilities.Loader.Hide();
//            if (data > 0) {
//                Utilities.Loader.Hide();
//                Utilities.SuccessNotification("Sales Return Cancelled");
//                $("#detailsModal").modal("hide");
//                if(table != "")
//                {
//                    table.destroy();
//                }
//                GetPendingList("2017-01-01", $("#toDate").val(), "");
//                //window.location = "/Customer/RejectionIn/RejectionInListing";
//            }
//            else {
//                Utilities.Loader.Hide();
//                Utilities.ErrorNotification("Sales Return Cancellation Failed");
//            }
//        },
//        error: function (err) {
//            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
//            Utilities.Loader.Hide();
//        }
//    });
//}

function findCustomer(ledgerId) {
    var output = {};
    for (i = 0; i < ledgers.length; i++) {
        if (ledgers[i].ledgerId == ledgerId) {
            output = ledgers[i];
            break;
        }
    }
    return output;
}
function findUser(userId) {
    var output = {};
    for (i = 0; i < users.length; i++) {
        if (users[i].userId == userId) {
            output = users[i];
            break;
        }
    }
    return output;
}
function findProduct(id) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].productId == id) {
            output = products[i];
            break;
        }
    }
    return output;
}
function findUnit(id) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == id) {
            output = units[i];
            break;
        }
    }
    return output;
}
function findStore(id) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == id) {
            output = stores[i];
            break;
        }
    }
    return output;
}