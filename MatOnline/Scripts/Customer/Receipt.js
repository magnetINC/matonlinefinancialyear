﻿var customersRow = [];
var bankOrCash = [];
var customers = [];
var selectedCustomer = 0;
var customerInvoices = [];
var unprocessedInvoicePayments = [];
var referenceType = "OnAccount";
var partyBalances = [];
var receiptDetails = [];
var fetchedCustomerInvoices = [];   //keeps a copy of invoices fetched for a customer to prevent 
                                    //going over the network when 'apply' action is used on a customer again

$(function () {

    Utilities.Loader.Show();
    getLookups();

    $('#customersRowTbody').on('change', '.cust', function ()
    {
        //console.log(this.value);
        var id = this.value;
        if (id != "0")   //means first item is not selected
        {
            var indexOfObject = customersRow.findIndex(p=>p.CustomerId == id);
            selectedCustomer = id;
            if (indexOfObject >= 0) //means customer name already in object
            {
                //so don't add new row, just update customer id
            }
            else
            {
                customersRow.push({
                    CustomerId: $(this).val(),
                    Apply:"NA"
                });
                renderCustomerRowItems();
            }

        }
        //console.log(customersRow);
    });

    $('#customersRowTbody').on('change', '.dateCtrl2', function () {
       // console.log(this.value);
        var val = this.value;
        var custId = $(this).attr("id").replace("date","");
        var indexToFind = customersRow.findIndex(p=>p.CustomerId == custId);
        if(indexToFind>=0)
        {
            customersRow[indexToFind].EffectiveDate = val;
        }
    });

    $('#customersRowTbody').on('blur', '.chequeOrRefNo', function () {
        //console.log(this.value);
        var val = this.value;
        var custId = $(this).attr("id").replace("chequeOrRefNo", "");
        var indexToFind = customersRow.findIndex(p=>p.CustomerId == custId);
        if (indexToFind >= 0)
        {
            customersRow[indexToFind].ChequeOrRefNo = val;
        }
    });

    $('#applyOnAccountAmount').change(function () {
        var custId = selectedCustomer;
        var indexToFind = customersRow.findIndex(p=>p.CustomerId == custId);
        if (indexToFind >= 0)
        {
            customersRow[indexToFind].Amount = $('#applyOnAccountAmount').val();
            customersRow[indexToFind].grossAmount = 0;
            customersRow[indexToFind].EffectiveDate = "";
        }
        else
        {
            customersRow.push({
                EffectiveDate: "",
                CustomerId: custId,
                Amount: $('#applyOnAccountAmount').val(),
                grossAmount: 0
            });
        }
    });
    
    $('#customersRowTbody').on('change', '.apply', function ()
    {
        
        var val = this.value;
        var customerToApplyOn = "";
        if (val != "")
        {
            var custId = selectedCustomer;
            var indexToFind = customersRow.findIndex(p=>p.CustomerId == custId);
            if (indexToFind >= 0) {
                customersRow[indexToFind].Apply = val;
            }
           // customerToApplyOn = $(this).attr("id").replace("apply", "");
        }
        //var chk = fetchedCustomerInvoices.findIndex(p=>p.LedgerId == customerToApplyOn);

        //if (chk >= 0)   //means invoices have been fetched for the customer before, so use the in-memory value
        //{
        //    customerInvoices = fetchedCustomerInvoices[chk];
        //    renderCustomerInvoices();
        //    $("#applyModal").modal("show");
        //}
        //else    //means invoice has not been loaded so fetch from db
        //{
        if (val == "Against")
        {
                Utilities.Loader.Show();
                $.ajax({
                    url: API_BASE_URL + "/Receipt/GetCustomerInvoices?ledgerId=" + selectedCustomer + "&voucherNo=" + $("#voucherNo").val(),
                    type: "GET",
                    contentType: "application/json",
                    success: function (data) {
                        customerInvoices = data;
                        //fetchedCustomerInvoices.push({ LedgerId: customerToApplyOn, Invoices: customerInvoices });
                        //console.log(fetchedCustomerInvoices);
                        renderCustomerInvoices();
                        $("#applyModal").modal("show");
                        //console.log(data);
                        Utilities.Loader.Hide();
                    },
                    error: function (err) {
                        Utilities.Loader.Hide();
                    }
                });
        }
        else if(val=="On Account")
        {
            $("#applyOnAccount").modal("show");
        }
       // }
        
    });

    $('#partyBalanceTbody').on('change', '.invAmt', function () {
        var indexOfObject = unprocessedInvoicePayments.findIndex(p=>p.InvoiceNo == $(this).attr("id"));
        if (indexOfObject >= 0) //object exists, just modify
        {
            unprocessedInvoicePayments[indexOfObject].Amount = $(this).val();
        }
        else    //object does not exist, insert new record
        {
            unprocessedInvoicePayments.push({
                InvoiceNo: $(this).attr("id"),
                Amount: $(this).val()
            });
        }
        customersRow.find(p=>p.CustomerId == selectedCustomer).Amount = getTotalInvoiceAmount();    //set total amount for receipt details line object
        $("#amt" + selectedCustomer).val(getTotalInvoiceAmount());
        $("#totalInvoiceAmt").val(getTotalInvoiceAmount());
        //console.log(customersRow);
    });

    $("#savePartyBalance").click(function () {
        var isInvoiceExists = partyBalances.findIndex(p=>p.LedgerId == selectedCustomer);
        if(isInvoiceExists>=0)  //record exists, fetch and edit record
        {
            for (i = 0; i < customerInvoices.length; i++)   //loop through all the invoices, and update amount based on invoice number
            {                                               //with what's in unprocessedInvoicePayments array

                var amtIndex = unprocessedInvoicePayments.findIndex(p=>p.InvoiceNo == customerInvoices[i].invoiceNo);
                if (amtIndex >= 0) //means amount's invoice number is in customerinvoices array
                {
                    partyBalances[isInvoiceExists].Credit = unprocessedInvoicePayments[amtIndex].Amount;
                }
            }
        }
        else    //record not found, so create new object
        {
            for (i = 0; i < customerInvoices.length; i++)   //loop through all the invoices, and update amount based on invoice number
            {                                               //with what's in unprocessedInvoicePayments array

                var amtIndex = unprocessedInvoicePayments.findIndex(p=>p.InvoiceNo == customerInvoices[i].invoiceNo);
                if (amtIndex >= 0 && unprocessedInvoicePayments[amtIndex].Amount>0) //means amount's invoice number is in customerinvoices array
                {
                    partyBalances.push({
                        Date: Utilities.YyMmDdDate(),
                        Credit: unprocessedInvoicePayments[amtIndex].Amount,
                        LedgerId: selectedCustomer,
                        ReferenceType: "Against",
                        AgainstInvoiceNo: $("#voucherNo").val(),
                        AgainstVoucherNo: $("#voucherNo").val(),
                        VoucherTypeId: customerInvoices[i].voucherTypeId,
                        VoucherNo: customerInvoices[i].voucherNo,
                        InvoiceNo: customerInvoices[i].invoiceNo
                    });
                   // console.log(partyBalances);
                }
            }
        }

        //populating receipt details array
        receiptDetails = [];    //always empty array to populate with updated data
        for(i=0;i<customersRow.length;i++)
        {
            receiptDetails.push({
                ChequeDate: customersRow[i].EffectiveDate,
                LedgerId: customersRow[i].CustomerId,
                Amount: customersRow[i].Amount,
                grossAmount: 0,
                ApplyType:"Against"
            });
        }
        $("#totalReceiptDetailsAmt").val(getTotalReceiptDetailsAmount());
    });

    $("#savePayment").click(function () {
        savePayment();
    });

    $("#saveAmountApplyOnAccount").click(function () {

        $("#amt" + selectedCustomer).val($("#applyOnAccountAmount").val());

        partyBalances.push({
            Date: Utilities.YyMmDdDate(),
            Credit: $("#applyOnAccountAmount").val(),
            LedgerId: selectedCustomer,
            ReferenceType: "OnAccount",
            VoucherNo: $("#voucherNo").val(),
            InvoiceNo: $("#voucherNo").val()
        });
        
        var indexOfObject = customersRow.findIndex(p=>p.CustomerId == selectedCustomer);
        console.log(customersRow);
        //if (indexOfObject >= 0) //object exists, just modify
        //{
        //    customersRow[indexOfObject].ChequeDate = "";
        //    customersRow[indexOfObject].Amount = $("#applyOnAccountAmount").val();
        //    customersRow[indexOfObject].grossAmount = $("#applyOnAccountAmount").val();
        //}

        //populating receipt details array
        receiptDetails = [];    //always empty array to populate with updated data
        for (i = 0; i < customersRow.length; i++) {
            receiptDetails.push({
                ChequeDate: customersRow[i].EffectiveDate,
                LedgerId: customersRow[i].CustomerId,
                Amount: customersRow[i].Amount,
                grossAmount: 0,
                ApplyType: "On Account"
            });
        }
        //console.log(receiptDetails);
        $("#totalReceiptDetailsAmt").val(getTotalReceiptDetailsAmount());

        $("#applyOnAccountAmount").val(0);
        $("#applyOnAccount").modal("hide");
    });
});

function renderCustomerRowItems()
{
    var output = "";    
    console.log("------",receiptDetails);
    $.each(customersRow, function (count, row)
    {
        var cid = row.CustomerId;
        var applyAction = "";
        //if (receiptDetails.length>1)
        //{
        //    if (receiptDetails.find(p=>p.CustomerId == row.CustomerId).Apply == "Against") {
        //        applyAction = '<option value="">-Select-</option>\
        //                    <option>On Account</option>\
        //                    <option selected="selected">Against</option>';
        //    }
        //    else if (receiptDetails.find(p=>p.CustomerId == row.CustomerId).Apply == "On Account") {
        //        applyAction = '<option value="">-Select-</option>\
        //                    <option selected="selected">On Account</option>\
        //                    <option>Against</option>';
        //    }
        //}
        if(row.Apply == "NA" || "")
        {
            output += '<tr>\
                    <td>'+ (count + 1) + '</td>\
                    <td><select class="form-control cust">' + '<option value="0">-Select-</option>' + renderSelectedCustomerDropdown(cid) + '</select></td>\
                    <td><select class="form-control apply"><option>--Select--</option><option>On Account</option><option>Against</option></select></td>\
                    <td><input class="form-control" value="' + (row.Amount == undefined ? 0 : row.Amount) + '" id="amt' + cid + '" type="text" readonly="readonly"/></td>\
                    <td><select class="form-control"><option>Naira | NGN</option></select></td>\
                    <td><input class="form-control chequeOrRefNo" id="chequeOrRefNo' + cid + '" value="' + (row.ChequeOrRefNo == undefined ? "" : row.ChequeOrRefNo) + '" type="text"/></td>\
                    <td><input class="form-control dateCtrl2" id="date' + cid + '" value="' + (row.EffectiveDate == undefined ? "" : row.EffectiveDate) + '" readonly="readonly" placeholder="Select Date"/></td>\
                    <td><button class="btn btn-sm btn-danger" onclick="removePayment(' + cid + ')"><i class="fa fa-times"></i></button></td>\
                   </tr>';
        }
        else
        {
            output += '<tr>\
                    <td>'+ (count + 1) + '</td>\
                    <td><select class="form-control cust">' + '<option value="0">-Select-</option>' + renderSelectedCustomerDropdown(cid) + '</select></td>\
                    <td><p>' + row.Apply + '</p></td>\
                    <td><input class="form-control" value="' + (row.Amount == undefined ? 0 : row.Amount) + '" id="amt' + cid + '" type="text" readonly="readonly"/></td>\
                    <td><select class="form-control"><option>Naira | NGN</option></select></td>\
                    <td><input class="form-control chequeOrRefNo" id="chequeOrRefNo' + cid + '" value="' + (row.ChequeOrRefNo == undefined ? "" : row.ChequeOrRefNo) + '" type="text"/></td>\
                    <td><input class="form-control dateCtrl2" id="date' + cid + '" value="' + (row.EffectiveDate == undefined ? "" : row.EffectiveDate) + '" readonly="readonly" placeholder="Select Date"/></td>\
                    <td><button class="btn btn-sm btn-danger" onclick="removePayment(' + cid + ')"><i class="fa fa-times"></i></button></td>\
                   </tr>';
           
        }
  
        //var rowAmt = (row.Amount == undefined) ? 0 : row.Amount;
        //console.log(row.Amount==undefined);
        //totalReceiptDetailsAmt = totalReceiptDetailsAmt + rowAmt;
    });
    
    output += '<tr>\
                    <td>'+(customersRow.length+1)+'</td>\
                    <td><select class="form-control cust">'+'<option value="0">-Select-</option>' + Utilities.PopulateDropDownFromArray(customers, 0, 2) + '</select></td>\
                    <td><select class="form-control apply"><option>--Select--</option><option>On Account</option><option>Against</option></select></td>\
                    <td><input class="form-control" type="text" readonly="readonly"/></td>\
                    <td><select class="form-control"><option>Naira | NGN</option></select></td>\
                    <td><input class="form-control" type="text"/></td>\
                    <td><input class="form-control dateCtrl2" readonly="readonly" placeholder="Select Date"/></td>\
                   </tr>';
    //console.log(output);
    $("#customersRowTbody").html(output);
    $("#totalReceiptDetailsAmt").val(getTotalReceiptDetailsAmount());
    $(".dateCtrl2").datepicker({
        clearBtn: true,
        daysOfWeekHighlighted: "0",
        autoclose: true,
        todayHighlight: true,
        format: "yyyy-mm-dd",
        //defaultViewDate:"today"
    });

}

function getTotalInvoiceAmount()
{
    var amt = 0.0;
    for(i=0;i<unprocessedInvoicePayments.length;i++)
    {
        var chk=unprocessedInvoicePayments[i].Amount==undefined ? 0 :parseFloat(unprocessedInvoicePayments[i].Amount)
        amt = amt + chk;
    }
    return amt;
}

function getTotalReceiptDetailsAmount()
{
    var totalReceiptDetailsAmt = 0.0;
    $.each(customersRow, function (count, row) {
        var chk=row.Amount==undefined ? 0 :row.Amount;
        totalReceiptDetailsAmt = totalReceiptDetailsAmt + parseFloat(chk);
    });
    return totalReceiptDetailsAmt;
}

function getLookups()
{
    //Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Receipt/GetLookups",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            bankOrCash = data.BankOrCash;
            customers = data.Customers;
            //console.log(Utilities.PopulateDropDownFromArray(customers, 0, 2));
            $("#bankOrCash").html(Utilities.PopulateDropDownFromArray(bankOrCash, 1, 0));
            $("#voucherNo").val(data.VoucherNo);
            renderCustomerRowItems();
            Utilities.Loader.Hide();
        },
        error:function()
        {
            Utilities.Loader.Hide();
        }
    });
}

function savePayment()
{
    if ($("#voucherNo").val() == "")
    {
        Utilities.ErrorNotification("Voucher number required!");
        return;
    }
    else if ($("#date").val() == "")
    {
        Utilities.ErrorNotification("Payment Date required!");
        return;
    }
    else
    {
        var receiptInfo = {
            VoucherNo: $("#voucherNo").val(),
            InvoiceNo: $("#voucherNo").val(),
            LedgerId: $("#bankOrCash").val(),
            Narration: "",
            UserId: 1,
            Date: $("#date").val(),
            TotalAmount: $("#totalReceiptDetailsAmt").val()
        };

        var toSave = {
            ReceiptMasterInfo: receiptInfo,
            ReceiptDetailsInfo: receiptDetails,
            PartyBalanceInfo: partyBalances
        };

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/Receipt/SavePayment",
            data: JSON.stringify(toSave),
            type: "POST",
            contentType: "application/json",
            success: function (data) {
                if (data > 0)
                {
                    Utilities.SuccessNotification("Payment saved!");
                    Utilities.Loader.Hide();
                    $("#voucherNo").val(data);
                    clearForm();
                }
                else
                {
                    Utilities.SuccessNotification("Oops! Payment not saved!");
                    Utilities.Loader.Hide();
                }
            },
            error: function (err) {
                Utilities.SuccessNotification("Oops! Something went wrong!");
                Utilities.Loader.Hide();
            }
        });
    }
    
}

function renderCustomerInvoices()
{
    var output = "";
    for (i = 0; i < customerInvoices.length; i++)
    {
        output +=
           '<tr>\
            <td>'+(i+1)+'</td>\
            <td>'+customerInvoices[i].display+' -- '+customerInvoices[i].balance+'</td>\
            <td>'+customerInvoices[i].invoiceNo+'</td>\
            <td>'+customerInvoices[i].balance+'</td>\
            <td><input class="form-control invAmt" id="' + customerInvoices[i].invoiceNo + '" type="text"/></td>\
            <td>Cr</td>\
        </tr>';
    }
    $("#partyBalanceTbody").html(output);
}

function renderSelectedCustomerDropdown(selectedValue)
{
    var output = "";
    for(i=0;i<customers.length;i++)
    {
        if(customers[i].ledgerId==selectedValue)
        {
            output += '<option selected value="' + customers[i].ledgerId + '">' + customers[i].ledgerName + '</option>';
        }
        else
        {
            output += '<option value="' + customers[i].ledgerId + '">' + customers[i].ledgerName + '</option>';
        }
    }
    return output;
}

function removePayment(ledgerId)
{
    console.log("i C U!!");
    //party balance,customer rows,receiptDetails
    if (confirm("Remove this record?")) {
        var ind1 = customersRow.findIndex(p=>p.CustomerId == ledgerId);
        customersRow.splice(ind1, 1);
        var ind2 = receiptDetails.findIndex(p=>p.LedgerId == ledgerId);
        receiptDetails.splice(ind2, 1);
        var ind3 = partyBalances.findIndex(p=>p.LedgerId == ledgerId);
        partyBalances.splice(ind3, 1);

        renderCustomerRowItems();
    }
}

function clearForm()
{
    customersRow = [];
    receiptDetails = [];
    partyBalances = [];
    customerInvoices = [];
    fetchedCustomerInvoices = [];
    $("select#bankOrCash")[0].selectedIndex = 0;
    $("#date").val("");
    renderCustomerInvoices();
    renderCustomerRowItems();
}
