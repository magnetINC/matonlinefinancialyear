﻿var products = [];
var lineItems = [];
var units = [];
var stores = [];
var racks = [];
var batches = [];
var tax = { taxId: 0, taxName: "NA", rate: 0 };
var allTaxes = [];
var salesMen = [];
var currencies = [];
var pricingLevel = [];
var master = [];
var sl = 1;
var SalesQuotationMasterObj = [];
var searchResult = {};

$(function () {
    $("#customer").click(function () {
        $("#agentsModal").modal("show");
    });
    getLookUps(SalesQuotationMasterId);
    // getSalesQuotationDetailsForEdit(SalesQuotationMasterId);
    $('#itemLineModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });

    $("#searchByProductName").on("change", function () {
        searchProduct($("#searchByProductName").val(), "ProductName");
    });
    $("#searchByProductCode").on("change", function () {
        searchProduct($("#searchByProductCode").val(), "ProductCode");
    });
    $("#searchByBarcode").on("change", function () {
        //searchProduct($("#searchByBarcode").val(), "Barcode");    barcode not working as products use productcode as barcode
        searchProduct($("#searchByBarcode").val(), "ProductCode");
    });

    //$("#searchByProductNameEdit").on("change", function () {
    //    searchProduct($("#searchByProductNameEdit").val(), "ProductName");
    //});
    //$("#searchByProductCodeEdit").on("change", function () {
    //    searchProduct($("#searchByProductCodeEdit").val(), "ProductCode");
    //});
    //$("#searchByBarcodeEdit").on("change", function () {
    //    //searchProduct($("#searchByBarcode").val(), "Barcode");    barcode not working as products use productcode as barcode
    //    searchProduct($("#searchByBarcodeEdit").val(), "ProductCode");
    //});

    $("#quantity").change(function () {
        calculateAmountOfItemToAdd();
    });
    $("#rate").change(function () {
        salesRate = $("#rate").val();   //recommended by madam vic to make rate editable as client may decide to change rate at selling point
        calculateAmountOfItemToAdd();
    });
    $("#tax").change(function () {
        var isFound = false;
        for (i = 0; i < allTaxes.length; i++) {
            if (allTaxes[i].taxId == $("#tax").val()) {
                tax = allTaxes[i];
                isFound = true;
                break;
            }
        }
        if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
        {
            tax = { taxId: 0, taxName: "NA", rate: 0 };
        }
        //        console.log(tax);
        calculateAmountOfItemToAdd();
    });
    $("#delete").click(function () {
        if (confirm("Remove this item?")) {
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/SalesQuotation/DeleteSalesQuotationDetails?id=" + SalesQuotationMasterId,
                type: "Get",
                contentType: "application/json",
                success: function (data) {
                    console.log(data);
                    var output = "";
                    Utilities.SuccessNotification(data);
                    Utilities.Loader.Hide();
                    window.location = "/Customer/SalesQuotation/SalesQuotationRegister";
                },
                error: function (err) {
                    Utilities.Loader.Hide();
                }
            });
        }
    });
    $("#addLineItem").click(function () {

        var lineQty = $("#quantity").val();
        var lineAmount = lineQty * salesRate;
        var lineTaxAmt = (tax.rate / 100.0) * lineAmount;
        var selectedStoreId = $("#store").val();
        var selectedStoreText = $("#store :selected").text();

        lineItems.push({
            slno: sl,
            productId: searchResult.productId,
            qty: lineQty,
            unitId: $("#unit").val(),
            batchId: $("#batch").val(),
            rate: salesRate,
            amount: lineAmount,
            taxAmount: lineAmount * 0.01,
            taxId: tax.TaxId,
            extra1: $("#store").val()
        });
        sl = sl + 1;

        clearLineItemForm();
        renderLineItem();

        //reset store to selected store after clearing form
        $("#store").html('<option value="' + selectedStoreId + '">' + selectedStoreText + '</option>');


        if (lineItems.length > 0)   //means item has been added to line item so prevent user from changing store since it won't make sense
        {                           //to pick items from different stores for one quotation
            $("#store").attr("disabled", "disabled");
        }
        else {
            $("#store").removeAttr("disabled");
        }
    });
    $("#saveQuotation").click(function () {
        Utilities.Loader.Show();

        var data = [];
        SalesQuotationMasterObj =
        {
            QuotationMasterId: SalesQuotationMasterId,
            InvoiceNo: $("#quotationNo").val(),
            LedgerId: $("#customer").val(),
            EmployeeId: $("#salesMan").val(),
            Date: $("#date").val(),
            PricinglevelId: $("#pricingLevel").val(),
            Narration: $("#narration").val(),
            TotalAmount: $("#totalAmount").val(),
            taxAmount: $("#totalTax").val(),
            VoucherTypeId: master.VoucherTypeId,
            Approved: master.Approved,
            ExchangeRateId: master.ExchangeRateId,
            Extra1: master.Extra1,
            Extra2: master.Extra2,
            ExtraDate: master.ExtraDate,
            FinancialYearId: master.FinancialYearId,
            SuffixPrefixId: master.SuffixPrefixId,
            VoucherNo: master.VoucherNo,
            userId: master.userId

        }
        data = { Master: SalesQuotationMasterObj, LineItems: lineItems }

        console.log(data);
        $.ajax({
            url: API_BASE_URL + "/SalesQuotation/EditSalesQuotationDetails",
            type: "Post",
            data: JSON.stringify(data),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                var output = "";

                Utilities.SuccessNotification(data);
                Utilities.Loader.Hide();
                window.location = "/Customer/SalesQuotation/SalesQuotationRegister";

            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    });
});

function getLookUps(id)
{
    Utilities.Loader.Show();
    var productsAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: "Get",
        contentType: "application/json"
    });
    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetLookups",
        type: "Get",
        contentType: "application/json"
    });
    var detailsAjax = $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetSalesQuotationDetails?id=" + id,
        type: "Get",
        contentType: "application/json"
    });
    var masterDetailAjax = $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetSalesQuotationMasterDetails?id=" + id,
        type: "Get",
        contentType: "application/json"
    });
    $.when(productsAjax, lookUpAjax, detailsAjax, masterDetailAjax)
    .done(function (dataProducts, dataLookUps, dataDetails, dataMaster) {
        
        products = dataProducts[2].responseJSON;
        lineItems = dataDetails[2].responseJSON;
        master = dataMaster[2].responseJSON;
        units = dataLookUps[2].responseJSON.Units;
        stores = dataLookUps[2].responseJSON.Stores;
        racks = dataLookUps[2].responseJSON.Racks;
        batches = dataLookUps[2].responseJSON.Batches; 
        allTaxes = dataLookUps[2].responseJSON.Tax;
        var filteredTax = {};
        for (i = 0; i < allTaxes.length; i++) {
            if (allTaxes[i].taxName == "Management Fee") {
                filteredTax = allTaxes[i];
                break;
            }
        }
        customers = dataLookUps[2].responseJSON.Customers; 
        salesMen = dataLookUps[2].responseJSON.SalesMen;
        currencies = dataLookUps[2].responseJSON.Currencies;
        pricingLevel = dataLookUps[2].responseJSON.PricingLevels; console.log(stores);

        //rendering dropdowns
        //$("#customer").html(Utilities.PopulateDropDownFromArray(customers, 1, 0));
        var customersWithCode = [];
        for (i = 0; i < customers.length; i++) {
            var ext = (customers[i].extra1 == "") ? "NA" : customers[i].extra1;
            //customersWithCode.push([customers[i].ledgerName + "  ____________  " + ext,customers[i].ledgerId]);
            customersWithCode.push([(i + 1), customers[i].extra1, customers[i].ledgerName]);
        }
        var table = "";

        table = $('#agentsTable').DataTable({
            data: customersWithCode,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });

        $('.dataTable').on('click', 'tbody tr', function () {
            console.log('API row values : ', table.row(this).data());
            var rowData = table.row(this).data();
            var selectedAgent = customers.find(p=>p.extra1 == rowData[1]);
            console.log(selectedAgent);
            $("#customer").html('<option value="' + selectedAgent.ledgerId + '">' + selectedAgent.ledgerName + "  :  " + selectedAgent.extra1 + "</option>");
            $("#agentsModal").modal("hide");
        })
        $("#customer").html('<option value="' + master.LedgerId + '">' + findLedger(master.LedgerId).ledgerName + "  :  " + findLedger(master.LedgerId).extra1 + "</option>");

        $("#customer").val(findLedger(master.LedgerId).ledgerId);
        $("#customer :selected").text(findLedger(master.LedgerId).ledgerName + "  :  " + findLedger(master.LedgerId).extra1);

        $("#salesMan").html(Utilities.PopulateDropDownFromArray(salesMen, 2, 1));
        $("#currency").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));
        $("#pricingLevel").html(Utilities.PopulateDropDownFromArray(pricingLevel, 0, 1));

        $("#unit").html(Utilities.PopulateDropDownFromArray(units, 1, 2));
        $("#batch").html(Utilities.PopulateDropDownFromArray(batches, 0, 1));
        $("#store").html(Utilities.PopulateDropDownFromArray(stores, 0, 1));
        $("#unitEdit").html(Utilities.PopulateDropDownFromArray(units, 1, 2));
        $("#batchEdit").html(Utilities.PopulateDropDownFromArray(batches, 0, 1));
        $("#storeEdit").html(Utilities.PopulateDropDownFromArray(stores, 0, 1));
        //$("#customer").html(Utilities.PopulateDropDownFromArray(customers, 1, 0));
        $("#currency").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));
        $("#salesMan").html(Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
        $("#pricingLevel").html(Utilities.PopulateDropDownFromArray(pricingLevel, 0, 1));
        $("#tax").html("<option value=\"" + filteredTax.taxId + "\">" + filteredTax.taxName + "</option>");
        $("#taxEdit").html("<option value=\"" + filteredTax.taxId + "\">" + filteredTax.taxName + "</option>");

        $("#searchByBarcode").kendoDropDownList({
            filter: "contains",
            optionLabel: "Please Select...",
            dataTextField: "ProductCode",
            dataValueField: "ProductCode",
            dataSource: products
        });
        $("#searchByProductCode").kendoDropDownList({
            filter: "contains",
            optionLabel: "Please Select...",
            dataTextField: "ProductCode",
            dataValueField: "ProductCode",
            dataSource: products
        });
        $("#searchByProductName").kendoDropDownList({
            filter: "contains",
            optionLabel: "Please Select...",
            dataTextField: "ProductName",
            dataValueField: "ProductName",
            dataSource: products,
            //change : onChange
        });

        document.getElementById("quotationNo").value = master.InvoiceNo;
        document.getElementById("date").value = Utilities.FormatJsonDate(master.Date);

       

        $("#salesMan").val(findSalesMan(master.EmployeeId).employeeId);
        $("#salesMan :selected").text(findSalesMan(master.EmployeeId).enployeeName);

        $("#pricingLevel").val(findPricingLevel(master.PricinglevelId).pricinglevelId);
        $("#pricingLevel :selected").text(findPricingLevel(master.PricinglevelId).pricinglevelName);

        //$("#currency").val(findCurrency(master.CurrencyId).currencyId);
        //$("#currency :selected").text(findCurrency(master.CurrencyId).currency);

        renderLineItem();
        Utilities.Loader.Hide();
    });
    
}
function searchProduct(filter, searchBy) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/SearchProduct?filter=" + filter + "&searchBy=" + searchBy,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
             //return;
            //var productDetails = data.Product[0];
            searchResult = data.Product[0];
            console.log(data);
            sizes = data.Size;
            salesRate = searchResult.salesRate;
            //console.log(searchResult);
            //console.log(sizes.find(p=>p.SizeId == searchResult.sizeId));
            $("#rate").val(salesRate);
            $("#manufacturer").val(searchResult.manufacturer);
            // $("#size").val(sizes.find(p=>p.sizeId == searchResult.sizeId).size);
            var sz = "";
            if (sizes.find(p=>p.sizeId == searchResult.sizeId) == undefined || sizes.find(p=>p.sizeId == searchResult.sizeId) == "") {
                sz = "N/A";
            }
            else {
                sz = sizes.find(p=>p.sizeId == searchResult.sizeId).size
            }
            $("#size").val(sz);
            $("#color").val(searchResult.brandName);
            //$("#amount").val((searchResult.purchaseRate) * $("#quantityToAdd").val());
            $("#description").val(searchResult.narration);
            //$("#searchByBarcode").val(searchResult.productCode);
            //$("#searchByBarcode").trigger("chosen:updated");
            //$("#searchByProductCode").val(searchResult.productCode);
            //$("#searchByProductCode").trigger("chosen:updated");
            //$("#searchByProductName").val(searchResult.productName);
            //$("#searchByProductName").trigger("chosen:updated");

            var dropdownlistBarcode = $("#searchByBarcode").data("kendoDropDownList");
            var dropdownlistProductCode = $("#searchByProductCode").data("kendoDropDownList");
            var dropdownlistProductName = $("#searchByProductName").data("kendoDropDownList");

            dropdownlistBarcode.value(searchResult.productCode);
            dropdownlistProductCode.value(searchResult.productCode);
            dropdownlistProductName.value(searchResult.productName);

            if (data.QuantityInStock > 0) {
                $("#quantityInStock").css("color", "black");
                $("#quantityInStock").val(Utilities.FormatQuantity(data.QuantityInStock));
            }
            else {
                $("#quantityInStock").css("color", "red");
                $("#quantityInStock").val("OUT OF STOCK!");
            }

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}
function clearLineItemForm() {
    $("#searchByBarcode").val("");
    $("#searchByBarcode").trigger("chosen:updated");
    $("#searchByProductCode").val("");
    $("#searchByProductCode").trigger("chosen:updated");
    $("#searchByProductName").val("");
    $("#searchByProductName").trigger("chosen:updated");
    $("#description").val("");
    $("#quantity").val("0");
    $("#rate").val("");
    $("#amount").val("");
    $("#taxAmount").val("");
    $("#quantityInStock").val("");
    $("select#unit")[0].selectedIndex = 0;
    $("select#batch")[0].selectedIndex = 0;
    $("select#store")[0].selectedIndex = 0;
}

function renderLineItem() {
    console.log(lineItems);
    //Utilities.ShowStockCardDialog($("#stockCardProducts").val());
    var output = "";
    $.each(lineItems, function (count, row) {
        var prod = row;
        //prod.Extra1 = prod.extra1;
        console.log(prod);
        if(prod.taxAmount == null)
        {
            prod.taxAmount = 0;
        }
        if (prod.extra1 == "")
        {
            prod.extra1 = 1;
        }
        output += '<tr>\
                    <td style="white-space: nowrap;"><button onclick="removeLineItem(' + prod.productId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>\
                    <td style="white-space: nowrap;"><button onclick="Utilities.ShowStockCardDialog(' + prod.productId + ')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></button></td>\
                    <td>'+ (count + 1) + '</td>\
                    <td>' + findProduct(prod.productId).barcode + '</td>\
                    <td>' + findProduct(prod.productId).ProductCode + '</td>\
                    <td>' + findProduct(prod.productId).ProductName + '</td>\
                    <td>' + findStore(prod.extra1).godownName + '</td>\
                    <td>' + prod.qty + '</td>\
                    <td>' + findUnit(prod.unitId).unitName + '</td>\
                    <td>' + findBatch(prod.batchId).batchNo + '</td>\
                    <td>&#8358;' + Utilities.FormatCurrency(prod.rate) + '</td>\
                    <td>&#8358;' + Utilities.FormatCurrency(prod.amount) + '</td>\
                    <td>&#8358;' + Utilities.FormatCurrency(prod.taxAmount) + '</td>\
                  </tr>';
    });

    $("#quotationLineItemTbody").html(output);
    $("#totalAmount").val(Utilities.FormatCurrency(getTotalAmount()));
    $("#totalTax").val(Utilities.FormatCurrency(getTotalTax()));
    $("#grandTotal").val(Utilities.FormatCurrency(getTotalAmount() + getTotalTax()));
}
function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}
function findCurrency(currencyId) {
    var output = {};
    for (i = 0; i < currencies.length; i++) {
        if (currencies[i].currencyId == currencyId) {
            output = currencies[i];
            break;
        }
    }
    return output;
}
function findPricingLevel(pricingLevelId) {
    var output = {};
    for (i = 0; i < pricingLevel.length; i++) {
        if (pricingLevel[i].pricinglevelId == pricingLevelId) {
            output = pricingLevel[i];
            break;
        }
    }
    return output;
}
function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}
function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}
function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}
function findProduct(productId) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].ProductId == productId) {
            output = products[i];
            break;
        }
    }
    return output;
}
function findLedger(ledgerId) {
    var output = {};
    for (i = 0; i < customers.length; i++) {
        if (customers[i].ledgerId == ledgerId) {
            output = customers[i];
            break;
        }
    }
    return output;
}
function findSalesMan(employeeId) {
    var output = {};
    for (i = 0; i < salesMen.length; i++) {
        if (salesMen[i].employeeId == employeeId) {
            output = salesMen[i];
            break;
        }
    }
    return output;
}
function findTax(taxId) {
    var output = {};
    for (i = 0; i < tax.length; i++) {
        if (tax[i].TaxId == taxId) {
            output = tax[i];
            break;
        }
    }
    return output;
}
function getTotalAmount() {
    var amt = 0.0;
    for (i = 0; i < lineItems.length; i++) {
        amt = amt + lineItems[i].amount;
    }
    return amt;
}
function getTotalTax() {
    var amt = 0.0;
    for (i = 0; i < lineItems.length; i++) {
        amt = amt + lineItems[i].taxAmount;
    }
    return amt;
}
function removeLineItem(productId) {
    console.log(lineItems);
    if (confirm("Remove this item?")) {
        var indexOfObjectToRemove = lineItems.findIndex(p=>p.productId == productId);
        lineItems.splice(indexOfObjectToRemove, 1);
        renderLineItem();
    }
}
function calculateAmountOfItemToAdd() {
    var amount = salesRate * $("#quantity").val();
    //var taxAmount = amount * (tax.rate / 100.0);
    var taxAmount = amount * 0.01;  //0.01 is 1% for management fee
    $("#amount").val(Utilities.FormatCurrency(amount));
    $("#taxAmount").val(Utilities.FormatCurrency(taxAmount));
}

//function EditLineItem(productId) {
//    var indexOfObjectToEdit = lineItems.findIndex(p=>p.productId == productId);
//    var lineItemtoEdit = lineItems[indexOfObjectToEdit];
//    console.log(lineItemtoEdit);

//    $("#searchByProductNameEdit").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 3, 3));
//    $("#searchByBarcodeEdit").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 0, 0));
//    $("#searchByProductCodeEdit").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 2, 2));

//    //$("#searchByBarcodeEdit").val(lineItemtoEdit.Barcode);
//    //$("#searchByProductCodeEdit").val(lineItemtoEdit.productCode);
//    //$("#searchByProductNameEdit").val(lineItemtoEdit.productName);
//    //$("#rateEdit").val(lineItemtoEdit.rate);
//    //$("#quantityEdit").val(lineItemtoEdit.qty);
//    //$("#taxEdit").val(lineItemtoEdit.taxId);
//    //$("#unitEdit").val(lineItemtoEdit.unitId);
//    //$("#batchEdit").val(lineItemtoEdit.batchId);
//    //$("#amountEdit").val(lineItemtoEdit.amount);
//    //$("#taxAmountEdit").val(lineItemtoEdit.taxAmount);
//    //$("#quantityInStockEdit").val(searchResult.QuantityInStock);
//    //$("#storeEdit").val(lineItemtoEdit.extra1);

//    $("#quantityEdit").change(function () {
//        var amount = $("#rateEdit").val() * $("#quantityEdit").val();
//        var taxAmount = amount * (tax.rate / 100.0);
//        $("#amountEdit").val(Utilities.FormatCurrency(amount));
//        $("#taxAmountEdit").val(Utilities.FormatCurrency(taxAmount));
//    });
//    $("#rateEdit").change(function () {
//        var amount = $("#rateEdit").val() * $("#quantityEdit").val();
//        var taxAmount = amount * (tax.rate / 100.0);
//        $("#amountEdit").val(Utilities.FormatCurrency(amount));
//        $("#taxAmountEdit").val(Utilities.FormatCurrency(taxAmount));
//    });
//    $("#taxEdit").change(function () {
//        var isFound = false;
//        for (i = 0; i < allTaxes.length; i++) {
//            if (allTaxes[i].taxId == $("#taxEdit").val()) {
//                tax = allTaxes[i];
//                isFound = true;
//                break;
//            }
//        }
//        if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
//        {
//            tax = { taxId: 0, taxName: "NA", rate: 0 };
//        }
//        //        console.log(tax);
//        var amount = $("#rateEdit").val() * $("#quantity").val();
//        var taxAmount = amount * (tax.rate / 100.0);
//        $("#amount").val(Utilities.FormatCurrency(amount));
//        $("#taxAmount").val(Utilities.FormatCurrency(taxAmount));
//    });

//    console.log(lineItems);
//    $("#replaceLineItem").click(function () {
//        var str = $("#amountEdit").val();
//        var amt = str.replace(",", "");
//        var str2 = $("#taxAmountEdit").val();
//        var taxAmt = str.replace(",", "");

//        lineItems[indexOfObjectToEdit].Barcode = $("#searchByBarcodeEdit").val();
//        lineItems[indexOfObjectToEdit].productCode = $("#searchByProductCodeEdit").val();
//        lineItems[indexOfObjectToEdit].productName = $("#searchByProductNameEdit").val();
//        lineItems[indexOfObjectToEdit].rate = $("#rateEdit").val();
//        lineItems[indexOfObjectToEdit].qty = $("#quantityEdit").val();
//        lineItems[indexOfObjectToEdit].taxId = $("#taxEdit").val();
//        lineItems[indexOfObjectToEdit].unitId = $("#unitEdit").val();
//        lineItems[indexOfObjectToEdit].batchId = $("#batchEdit").val();
//        lineItems[indexOfObjectToEdit].amount = parseFloat(amt);
//        lineItems[indexOfObjectToEdit].taxAmount = parseFloat(taxAmt);
//        lineItems[indexOfObjectToEdit].QuantityInStock = $("#quantityInStockEdit").val();
//        lineItems[indexOfObjectToEdit].extra1 = $("#storeEdit").val();
//        lineItems[indexOfObjectToEdit].CategoryId = lineItemtoEdit.CategoryId;
//        lineItems[indexOfObjectToEdit].ProjectId = lineItemtoEdit.ProjectId;
//        lineItems[indexOfObjectToEdit].quotationDetailsId = lineItemtoEdit.quotationDetailsId;
//        lineItems[indexOfObjectToEdit].productId = lineItemtoEdit.productId;
//        lineItems[indexOfObjectToEdit].slno = lineItemtoEdit.slno;

//        renderLineItem();
//        //console.log(lineItems);
//    });
//}


