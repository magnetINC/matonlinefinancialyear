﻿var invoices = [];
var balances = [];
var customers = [];
var voucherTypes = [];

var thisInvoice = {};

var table = "";
var partyBalanceIndex = 0;
var invoiceIndex = 0;


$(function () {
    getLookUps();

    $("#search").click(function () {
        getInvoices($("#customers").val());
    });
});


//======================== API CALLS ===========================
function getLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Receipt/GetLookups",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            customers = data.Customers;
            voucherTypes = data.VoucherTypes;
            RenderControlData();

            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function getInvoices(id) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Receipt/GetInvoicesToApplyCredit?ledgerId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data.ResponseCode >= 400) {
                Utilities.Loader.Hide();
                Utilities.ErrorNotification(data.ResponseMessage);
                return;
            }
            invoices = data.Response.invoices;
            balances = data.Response.payments;
            $.each(balances, function (count, row) {
                row.amountToPay = 0;
            });
            RenderInvoicesToControls();

            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function save()
{
    Utilities.Loader.Show();
    console.log(thisInvoice);

    var newPayments = [];
    $.each(balances, function (count, row) {
        if(row.amountToPay > 0)
        {
            newPayments.push({
                ledgerId: 0,
                date: new Date(),
                amountToApply: row.amountToPay,
                invoiceNo: row.invoiceNo,
                voucherNo: row.voucherNo,
                voucheerTypeId: row.voucherTypeId,
                value: row.balance,
                exchangeRateId: 1,
                voucherType: findVoucherTypes(row.voucheerTypeId).voucherTypeName
            });
        }
    });
    var toSave = {};
    toSave.ledgerId = $("#customers").val();
    toSave.date = thisInvoice.date;
    toSave.grandTotal = thisInvoice.grandTotal;
    toSave.voucherNo = thisInvoice.voucherNo;
    toSave.narration = thisInvoice.narration;
    toSave.vouchertypeId = thisInvoice.voucherTypeId;
    toSave.advancePayments = newPayments;

    //debugger;
    let advancePaymentsGrandTotal = newPayments.map(x => x.amountToApply).reduce((p, c) => p + c);
    if (advancePaymentsGrandTotal > toSave.grandTotal) {
        Utilities.ErrorNotification("Amount to apply is more than Invoice Amount");
        return;
    }


    console.log(toSave);
    $.ajax({
        url: API_BASE_URL + "/Receipt/ApplyCredit",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data.ResponseCode == 200) {
                Utilities.SuccessNotification(data.ResponseMessage);
                //getAvancePaymentsForListing($("#month").val())
                Utilities.Loader.Hide();
                window.location = "/Customer/Receipt/applycredit"
            }
            else {
                Utilities.ErrorNotification("Something went wrong");
                Utilities.Loader.Hide();
            }
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}


//======================== RENDER DATA TO CONTROLS =============================
function RenderControlData() {
    $("#customers").kendoDropDownList({
        filter: "contains",
        optionLabel: "Please Select...",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: customers
    });
}

function RenderInvoicesToControls()
{
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }
    $.each(invoices, function (count, row) {
        objToShow.push([
            count + 1,
            Utilities.FormatJsonDate(row.date),
            findVoucherTypes(row.voucherTypeId).voucherTypeName,
            row.voucherNo,
            row.narration,
             '&#8358;' + Utilities.FormatCurrency(row.grandTotal),
            '<a class="btn btn-primary" onclick="renderPartyBalances(' + count + ')">Apply Credit</a>'
        ]);
    });
    table = $('#invoiceListTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}
function renderPartyBalances(index) {
    var output = "";
    invoiceIndex = index;
    $.each(balances, function (count, row) {
        output += '<tr>\
                    <td>'+ (count + 1) + '</td>\
                    <td>' + row.display + '</td>\
                    <td>' + row.invoiceNo + '</td>\
                    <td>&#8358;' + Utilities.FormatCurrency(row.balance) + '</td>\
                    <td>&#8358;' + Utilities.FormatCurrency(row.amountToPay) + '</td>\
                    <td>Dr</td>\
                    <td><button class="btn btn-primary" onclick="makePayment(' + count + ')">Apply</button></td>\
                   </tr>';
    });
    console.log(invoices[index]);
    thisInvoice = invoices[index];
    $("#partyBalanceTbody").html(output);
    $("#applyModal").modal("show");
    $("#invoiceNoApply").val(invoices[index].voucherNo);
    $("#voucherTypeApply").val(findVoucherTypes(invoices[index].voucherTypeId).voucherTypeName);
    $("#amountApply").val(invoices[index].grandTotal);
}
function makePayment(indexOfObject) {
    $("#applyAgainst").modal("show");
    partyBalanceIndex = indexOfObject;

}
function applyPaymentToInvoice() {
    balances[partyBalanceIndex].amountToPay = parseFloat($("#amountToApplyToInvoice").val());
    $("#applyAgainst").modal("hide");
    $("#amountToApplyToInvoice").val(0);

    var amt = 0
    $.each(balances, function (count, row) {
        amt = amt + row.amountToPay;
    });
    renderPartyBalances(invoiceIndex);
    $("#totalInvoiceAmt").val(amt);
}

function findVoucherTypes(id) {
    var output = {};
    for (i = 0; i < voucherTypes.length; i++) {
        if (voucherTypes[i].voucherTypeId == id) {
            output = voucherTypes[i];
            break;
        }
    }
    return output;
}