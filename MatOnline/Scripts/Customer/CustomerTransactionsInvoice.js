﻿var products = [];
var lookups = [];
var units = [];
var stores = [];
var racks = [];
var batches = [];
var tax = { taxId: 0, taxName: "NA", rate: 0 };
var allTaxes = [];
var customers = [];
var salesMen = [];
var currencies = [];
var pricingLevel = [];
var searchResult = {};
var lineItems = [];
var sl = 1;
var salesRate = 0;
var sizes = [];
var voucherNumber = "";

$(function () {
    $('#salesInvoiceModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
    $('#salesInvoiceLineItemModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
    loadLookups();
    $("#searchByProductNameInvoice").on("change", function () {
        searchProduct($("#searchByProductNameInvoice").val(), "ProductName");
    });
    $("#searchByProductCodeInvoice").on("change", function () {
        searchProduct($("#searchByProductCodeInvoice").val(), "ProductCode");
    });
    $("#searchByBarcodeInvoice").on("change", function () {
        searchProduct($("#searchByBarcodeInvoice").val(), "ProductCode");
    });
    $("#quantityInvoice").change(function () {
        calculateAmountOfItemToAdd();
    });
    $("#rateInvoice").change(function () {
        salesRate = $("#rate").val();   //recommended by madam vic to make rate editable as client may decide to change rate at selling point
        calculateAmountOfItemToAdd();
    });
    $("#taxInvoice").change(function () {
        var isFound = false;
        for (i = 0; i < allTaxes.length; i++) {
            if (allTaxes[i].taxId == $("#taxInvoice").val()) {
                tax = allTaxes[i];
                isFound = true;
                break;
            }
        }
        if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
        {
            tax = { taxId: 0, taxName: "NA", rate: 0 };
        }
        //        console.log(tax);
        calculateAmountOfItemToAdd();
    });
    $("#quantityToAddInvoice").on("change", function () {
        var rate = $("#rateInvoice").val();
        var qty = $("#quantityToAddInvoice").val();
        var amount = rate * qty;
        $("#amountInvoice").val(amount);
    });
    $("#addLineItemInvoice").click(function () {

        if (searchResult.productId != null) {
            var lineQty = $("#quantityInvoice").val();
            var lineAmount = lineQty * salesRate;
            var lineTaxAmt = (tax.rate / 100.0) * lineAmount;
            var selectedStoreId = $("#storeInvoice").val();
            var selectedStoreText = $("#storeInvoice :selected").text();

            lineItems.push({
                Slno: sl,
                ProductId: searchResult.productId,
                Qty: lineQty,
                UnitId: $("#unitInvoice").val(),
                BatchId: $("#batchInvoice").val(),
                Rate: salesRate,
                Amount: lineAmount,
                taxAmount: lineAmount * 0.01,
                taxId: 10012,
                Extra1: $("#storeInvoice").val()
            });
            sl = sl + 1;

            clearLineItemForm();
            searchResult = {};
            renderLineItem();

            //reset store to selected store after clearing form
            $("#storeInvoice").html('<option value="' + selectedStoreId + '">' + selectedStoreText + '</option>');


            if (lineItems.length > 0)   //means item has been added to line item so prevent user from changing store since it won't make sense
            {                           //to pick items from different stores for one quotation
                $("#storeInvoice").attr("disabled", "disabled");
            }
            else
            {
                $("#storeInvoice").removeAttr("disabled");
            }
        }
        else
        {
            Utilities.ErrorNotification("Please select item!");
        }
    });
});


function loadLookups()
{
    Utilities.Loader.Show();
    var productsAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: "Get",
        contentType: "application/json"
    });
    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetLookups",
        type: "Get",
        contentType: "application/json"
    });

    var voucherNoAjax = $.ajax({
        url: API_BASE_URL + "/SalesQuotation/voucherNo",
        type: "Get",
        contentType: "application/json"
    });

    $.when(productsAjax, lookUpAjax, voucherNoAjax)
    .done(function (dataProducts, dataLookUps, dataVoucherNo) {
        products = dataProducts[2].responseJSON;
        units = dataLookUps[2].responseJSON.Units;
        stores = dataLookUps[2].responseJSON.Stores;
        racks = dataLookUps[2].responseJSON.Racks;
        batches = dataLookUps[2].responseJSON.Batches;
        allTaxes = dataLookUps[2].responseJSON.Tax;
        customers = dataLookUps[2].responseJSON.Customers;
        salesMen = dataLookUps[2].responseJSON.SalesMen;
        currencies = dataLookUps[2].responseJSON.Currencies;
        pricingLevel = dataLookUps[2].responseJSON.PricingLevels;
        voucherNumber = dataVoucherNo[2].responseJSON;
        
        //showing autogen order number
        //$("#quotationNo").val(voucherNumber);

        //rendering dropdowns
        $("#unitInvoice").html(Utilities.PopulateDropDownFromArray(units, 1, 2));
        $("#batchInvoice").html(Utilities.PopulateDropDownFromArray(batches, 0, 1));
        $("#storeInvoice").html(Utilities.PopulateDropDownFromArray(stores, 0, 1));
        $("#taxInvoice").html(Utilities.PopulateDropDownFromArray(allTaxes, 0, 1));
       // $("#currencyInvoice").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));
        //$("#salesManInvoice").html(Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
        //$("#pricingLevelInvoice").html(Utilities.PopulateDropDownFromArray(pricingLevel, 0, 1));
        //console.log(allTaxes);
        //var filteredTax = {};
        //for (i = 0; i < allTaxes.length; i++) {
        //    if (allTaxes[i].taxName == "Management Fee") {
        //        filteredTax = allTaxes[i];
        //        break;
        //    }
        //}
        //$("#taxInvoice").html("<option value=\"" + filteredTax.taxId + "\">" + filteredTax.taxName + "</option>");

        $("#invoiceCustomerName").kendoDropDownList({
            filter: "contains",
            optionLabel: "Please Select...",
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: customers
        });
        $("#searchByBarcodeInvoice").kendoDropDownList({
            filter: "contains",
            optionLabel: "Please Select...",
            dataTextField: "ProductCode",
            dataValueField: "ProductCode",
            dataSource: products
        });
        $("#searchByProductCodeInvoice").kendoDropDownList({
            filter: "contains",
            optionLabel: "Please Select...",
            dataTextField: "ProductCode",
            dataValueField: "ProductCode",
            dataSource: products
        });
        $("#searchByProductNameInvoice").kendoDropDownList({
            filter: "contains",
            optionLabel: "Please Select...",
            dataTextField: "ProductName",
            dataValueField: "ProductName",
            dataSource: products
        });

        Utilities.Loader.Hide();
    });

}

function searchProduct(filter, searchBy) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/SearchProduct?filter=" + encodeURIComponent(filter) + "&searchBy=" + searchBy,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            //console.log(tax); //return;
            //var productDetails = data.Product[0];
            searchResult = data.Product[0];
            sizes = data.Size;
            salesRate = searchResult.salesRate;
            //console.log(searchResult);
            //console.log(sizes.find(p=>p.SizeId == searchResult.sizeId));
            $("#rateInvoice").val(salesRate);
            $("#manufacturerInvoice").val(searchResult.extra2);
            $("#sizeInvoice").val(sizes.find(p=>p.sizeId == searchResult.sizeId).size);
            $("#colorInvoice").val(searchResult.brandName);
            $("#descriptionInvoice").val(searchResult.narration);

            var dropdownlistBarcode = $("#searchByBarcodeInvoice").data("kendoDropDownList");
            var dropdownlistProductCode = $("#searchByProductCodeInvoice").data("kendoDropDownList");
            var dropdownlistProductName = $("#searchByProductNameInvoice").data("kendoDropDownList");

            dropdownlistBarcode.value(searchResult.productCode);
            dropdownlistProductCode.value(searchResult.productCode);
            dropdownlistProductName.value(searchResult.productName);

            if (data.QuantityInStock > 0) {
                $("#quantityInStockInvoice").css("color", "black");
                $("#quantityInStockInvoice").val(Utilities.FormatQuantity(data.QuantityInStock));
            }
            else {
                $("#quantityInStockInvoice").css("color", "red");
                $("#quantityInStockInvoice").val("OUT OF STOCK!");
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function calculateAmountOfItemToAdd() {
    var amount = salesRate * $("#quantityInvoice").val();
    //var taxAmount = amount * (tax.rate / 100.0);
    var taxAmount = amount * 0.01;  //0.01 is 1% for management fee
    $("#amountInvoice").val(Utilities.FormatCurrency(amount));
    $("#taxAmountInvoice").val(Utilities.FormatCurrency(taxAmount));
}

function renderLineItem() {
    //Utilities.ShowStockCardDialog($("#stockCardProducts").val());
    var output = "";
    $.each(lineItems, function (count, row) {
        var prod = row;
        output += '<tr>\
                    <td style="white-space: nowrap;"><button onclick="removeLineItem(' + prod.ProductId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>\
                    <td>'+ (count + 1) + '</td>\
                    <td style="white-space: nowrap;"><button onclick="Utilities.ShowStockCardDialog(' + prod.ProductId + ')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></button></td>\
                    <td>' + findProduct(prod.ProductId).ProductCode + '</td>\
                    <td>' + findProduct(prod.ProductId).ProductCode + '</td>\
                    <td>' + findProduct(prod.ProductId).ProductName + '</td>\
                    <td>' + findStore(prod.Extra1).godownName + '</td>\
                    <td>' + prod.Qty + '</td>\
                    <td>' + findUnit(prod.UnitId).unitName + '</td>\
                    <td>' + Utilities.FormatCurrency(prod.Rate) + '</td>\
                    <td>' + Utilities.FormatCurrency(prod.Amount) + '</td>\
                    <td>' + Utilities.FormatCurrency(prod.taxAmount) + '</td>\
                    <td>' + Utilities.FormatCurrency(prod.Amount+prod.taxAmount) + '</td>\
                  </tr>';
    });

    $("#salesInvoiceLineItemTbody").html(output);
    $("#totalAmountInvoice").val(Utilities.FormatCurrency(getTotalAmount()));
    $("#totalTaxInvoice").val(Utilities.FormatCurrency(getTotalTax()));
    $("#grandTotalInvoice").val(Utilities.FormatCurrency(getTotalAmount() + getTotalTax()));
}

function showInvoiceModal(orderId)
{
    alert(orderId);
    $("#salesInvoiceModal").modal("show");
}

function getTotalAmount() {
    var amt = 0.0;
    for (i = 0; i < lineItems.length; i++) {
        amt = amt + lineItems[i].Amount;
    }
    return amt;
}

function getTotalTax() {
    var amt = 0.0;
    for (i = 0; i < lineItems.length; i++) {
        amt = amt + lineItems[i].taxAmount;
    }
    return amt;
}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}


function findProduct(productId) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].ProductId == productId) {
            output = products[i];
            break;
        }
    }
    return output;
}

function findTax(taxId) {
    var output = {};
    for (i = 0; i < tax.length; i++) {
        if (tax[i].TaxId == taxId) {
            output = tax[i];
            break;
        }
    }
    return output;
}

function removeLineItem(productId) {
    if (confirm("Remove this item?")) {
        var indexOfObjectToRemove = lineItems.findIndex(p=>p.ProductId == productId);
        lineItems.splice(indexOfObjectToRemove, 1);
        renderLineItem();
    }
}