﻿var master = {};
var details = [];
var allProducts = [];
var customers = [];
var salesMen = [];
var pricingLevels = [];
var currencies = [];
var stores = [];
var racks = [];
var units = [];
var taxes = [];
var batches = [];
var master = {};
var details = [];

var lookUpAjax = $.ajax({
    url: API_BASE_URL + "/SalesInvoice/InvoiceLookUps",
    type: "GET",
    contentType: "application/json",
});

var productLookupAjax = $.ajax({
    url: API_BASE_URL + "/ProductCreation/GetProducts",
    type: "GET",
    contentType: "application/json",
});

var transactionAjax = $.ajax({
    url: API_BASE_URL + "/DeliveryNote/GetReleaseFormTransaction?deliveryNoteMasterId=" + releaseNoteId,
    type: "GET",
    contentType: "application/json",
});

$(function () {
    //$.ajax({
    //    url: API_BASE_URL + "/DeliveryNote/GetReleaseFormTransaction?deliveryNoteMasterId=" + releaseNoteId,
    //    type: "GET",
    //    contentType: "application/json",
    //    success:function(data)
    //    {
    //        console.log(data);
    //        var master = data.Master;
    //        var details = data.Details;

    //        $("#invoiceNo").val(master.InvoiceNo);
    //        $("#transactionDate").val(master.Date);
    //        $("#customer").val(master.LedgerId);
    //        $("#deliveryMode").val(master.VoucherTypeId);
    //        $("#pricingLevel").val(master.PricinglevelId);
    //        $("#currency").val(master.ExchangeRateId);
    //        $("#salesPerson").val(master.EmployeeId);
    //        $("#orderNo").val(master.InvoiceNo);
    //        renderLineItems(details);
    //    },
    //    error:function(error)
    //    {

    //    }
    //});

    Utilities.Loader.Show();
    $.when(lookUpAjax, productLookupAjax,transactionAjax)
    .done(function (dataLookUp, dataProduct,dataTransaction) {

        master = dataTransaction[2].responseJSON.Master;
        details = dataTransaction[2].responseJSON.Details;
        allProducts = dataProduct[2].responseJSON;
        customers = dataLookUp[2].responseJSON.Customers;
        salesMen = dataLookUp[2].responseJSON.SalesMen;
        pricingLevels = dataLookUp[2].responseJSON.PricingLevels;
        currencies = dataLookUp[2].responseJSON.Currencies;
        stores = dataLookUp[2].responseJSON.Stores;
        racks = dataLookUp[2].responseJSON.Racks;
        units = dataLookUp[2].responseJSON.Units;
        taxes = dataLookUp[2].responseJSON.Tax;
        batches = dataLookUp[2].responseJSON.Batches;

        //var emp = (salesMen.length < 1) ? "N/A" : (salesMen.find(p=>p.employeeId == master.EmployeeId).employeeName);
        $("#invoiceNo").val(master.InvoiceNo);
        $("#transactionDate").val(Utilities.FormatJsonDate(master.Date));
        $("#customer").val(customers.find(p=>p.ledgerId == master.LedgerId).ledgerName);
        // $("#pricingLevel").val(pricingLevels.find(p=>p.pricinglevelId == master.PricinglevelId).pricinglevelName);
        $("#pricingLevel").val(1);
        $("#currency").val(currencies.find(p=>p.exchangeRateId == master.ExchangeRateId).currencyName);
        $("#salesPerson").val("NA");
        $("#orderNo").val(master.InvoiceNo);
        $("#transportationCompany").val(master.TransportationCompany);
        $("#lrNo").val(master.LrNo);
        renderLineItems(details);

        Utilities.Loader.Hide();
    });
});

function renderLineItems(lineItems)
{
    var output = "";
    $.each(lineItems, function (count, row) {
        var dlvd = (row.extra2 == "") ? "0" : row.extra2;
        var batchNo = (batches.find(p=>p.batchId == row.batchId) == null) ? "NA" : batches.find(p=>p.batchId == row.batchId).batchNo;
        output +=
            '<tr>\
                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                <td style="white-space: nowrap;" class="Settings_Barcode">' + row.productCode + '</td>\
                <td style="white-space: nowrap;">' + row.productCode + '</td>\
                <td style="white-space: nowrap;">' + row.productName + '</td>\
                <td style="white-space: nowrap;">' + row.itemDescription + '</td>\
                <td style="white-space: nowrap;">'+  row.qty + '</td>\
                <td style="white-space: nowrap;">' +dlvd + '</td>\
                <td style="white-space: nowrap;">' + (row.qty-dlvd) + '</td>\
                <td style="white-space: nowrap;">' + row.unitName + '</td>\
                <td style="white-space: nowrap;">' + stores.find(p=>p.godownId == row.godownId).godownName + '</td>\
            </tr>\
            ';
    });
    $("#lineItems").html(output);
}