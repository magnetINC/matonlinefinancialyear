﻿var allProducts = [];
var salesInvoiceLineItems = [];
var searchResult = {};
var customers = [];
var salesMen = [];
var pricingLevels = [];
var currencies = [];
var units = [];
var batches = [];
var stores = [];
var racks = [];
var taxes = [];
var tax = { taxId: 0, taxName: "NA", rate: 0 };
var salesRate = 0;
var orderDetailsId = 0;
var deliveryNoteDetailsId = 0;
var quotationDetailsId = 0;
var totalBillDiscountAmount = 0;
var totalTaxAmount = 0;
var grandTotal = 0;
var totalAmount = 0;
var toSaveFromApply = {};   //object to save when invoice is generated from "apply on"
var deleteIndex;
var grossValue = 0;
var netValue = 0;
var taxAmount = 0;
var amount = 0;
var itemToEdit;
var itemToEditIndex;
var productBatches = [];
var projects = [];
var categories = [];
var resetBatch = true;
var resetTaxVal = true;


Utilities.Loader.Show();
var lookUpAjax = $.ajax({
    url: API_BASE_URL + "/SalesInvoice/InvoiceLookUps",
    type: "GET",
    contentType: "application/json",
});

var productLookupAjax = $.ajax({
    url: API_BASE_URL + "/ProductCreation/GetProducts1",
    type: "GET",
    contentType: "application/json",
});

var projectLookupAjax = $.ajax({
    url: API_BASE_URL + "/Project/GetProject",
    type: "GET",
    contentType: "application/json",
});

var categoryLookupAjax = $.ajax({
    url: API_BASE_URL + "/Category/GetCategory",
    type: "GET",
    contentType: "application/json",
});
function AutoFillForm() {
    var ledgerId = $("#ledgerId").val();

    if (ledgerId != null || ledgerId != undefined) {
        var dropdownlist = $("#customer").data("kendoDropDownList");

        dropdownlist.value(ledgerId);

        //auto fill saleMode entry
        var salesMode = $("#salesMode").val();
        if (salesMode == "17") {
            $("#applyOn").val("17");
            $("#applyOn").html('<option value="17">Delivery Note</option>');
            getOrderNumbers();
        }
        else if (salesMode == "10030") {
            $("#applyOn").val("10030");
            $("#applyOn").html('<option value="10030">Sales Order</option>');
            getOrderNumbers();

        }
        else {
            $("#applyOn").val("");
            $("#applyOn").html('');
        }

    }
    var loadOrder = $.ajax({
        url: API_BASE_URL + "/SalesInvoice/GetSalesModeOrderNo?ledgerId=" + $("#customer").val() + "&voucherTypeId=" + $("#applyOn").val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            $("#orderNo").html('<option value="">-Select-</option>' + Utilities.PopulateDropDownFromArray(data, 0, 1));


            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
    $.when(loadOrder).then(function (data) {
        var txtId = $("#txtId").val();
        var vochureId = $("#invoiceNo").val();;

        if (txtId != null || txtId != undefined) {
            $("#orderNo").val(txtId);
            //  $("#invoiceNo").val(vochureId);
            $("#invoiceNo").val();
            $.ajax({
                url: API_BASE_URL + "/SalesInvoice/GetSalesModeItems?orderNo=" + $("#orderNo").val() + "&salesMode=" + $("#salesMode").val(),
                type: "GET",
                contentType: "application/json",
                success: function (data) {
                    console.log(data);
                    var master = data.Master[0];
                    var details = data.Details;


                    // $("#invoiceNo").val(master.invoiceNo);
                    for (i = 0; i < details.length; i++) {
                        var taxAmt = (details[i].taxId == 0) ? 0 : (details[i].amount * 0.05);
                        details[i].qty = details[i].qty == undefined ? details[i].Qty : details[i].qty;
                        details[i].rate = details[i].rate == undefined ? details[i].Rate : details[i].rate;
                        details[i].amount = details[i].amount == undefined ? details[i].Amount : details[i].amount;
                        details[i].godownId = details[i].extra1;

                        salesInvoiceLineItems.push({
                            DeliveryNoteDetailsId: details[i].deliveryNoteDetailsId,
                            OrderDetailsId: 0,
                            QuotationDetailsId: 0,
                            ProductId: details[i].productId,
                            Qty: details[i].qty,
                            Rate: details[i].rate,
                            UnitId: details[i].unitId,
                            UnitConversionId: details[i].unitConversionId,
                            Discount: 0,
                            TaxId: details[i].taxId,
                            BatchId: details[i].batchId,
                            GodownId: details[i].godownId,
                            RackId: details[i].rackId,
                            TaxAmount: taxAmt,
                            GrossAmount: details[i].amount, //same as amount since discount does not apply
                            NetAmount: parseFloat(details[i].amount) + taxAmt,
                            Amount: details[i].amount,
                            SlNo: (i + 1),
                            ItemDescription: details[i].itemDescription
                        });
                    }
                    renderSalesInvoiceLineItems();
                    Utilities.Loader.Hide();
                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }
    });
    //$("#salesMode").data("kendoDropDownList").value("10030");
    $("#balance").val(0);
}

function getSalesMasterInvoiceMax() {
    $.ajax({
        url: API_BASE_URL + "/SalesInvoice/GetInvoiceMax",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data); //return;
            $("#invoiceNo").val(data.InvoiceNo);

        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

$(function () {
    //getAllProducts();
    Utilities.Loader.Show();
    getSalesMasterInvoiceMax()
    $.when(lookUpAjax, productLookupAjax, projectLookupAjax, categoryLookupAjax)
        .done(function (dataLookUp, dataProduct, dataProject, dataCategory) {
            console.log("general lookup", dataLookUp);
            console.log("products", dataProduct);
            console.log("projects", dataProject);
            console.log("categories", dataCategory);
            allProducts = dataProduct[2].responseJSON;
            customers = dataLookUp[2].responseJSON.Customers;
            salesMen = dataLookUp[2].responseJSON.SalesMen;
            pricingLevels = dataLookUp[2].responseJSON.PricingLevels;
            currencies = dataLookUp[2].responseJSON.Currencies;
            stores = dataLookUp[2].responseJSON.Stores;
            racks = dataLookUp[2].responseJSON.Racks;
            units = dataLookUp[2].responseJSON.Units;
            taxes = dataLookUp[2].responseJSON.Taxes;
            batches = dataLookUp[2].responseJSON.Batches;
            projects = dataProject[0];
            categories = dataCategory[0];

            //rendering dropdowns
            $("#unit").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(units, 1, 2));

            //Restrict Location
            var nStore = stores.find(p => p.godownId == matUserInfo.StoreId);
            //if (nStore != undefined && matUserInfo.RoleId != 1) {
            //    $("#store").html(Utilities.PopulateDropDownFromArray([nStore], 0, 1));
            //} else {
            //    $("#store").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
            //}
            if (nStore == undefined && matUserInfo.RoleId > 0) {
                $("#store").html(Utilities.PopulateDropDownFromArray([nStore], 0, 1));
            } else {
                $("#store").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
            }

            $("#store").chosen({ width: "100%", margin: "1px" });
            $("#project").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(projects, 0, 1));
            $("#project").chosen({ width: "100%", margin: "1px" });
            $("#category").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(categories, 0, 1));
            $("#category").chosen({ width: "100%", margin: "1px" });
            // $("#customer").html(Utilities.PopulateDropDownFromArray(customers, 1, 0));
            //$("#batch").html(Utilities.PopulateDropDownFromArray(batches, 0, 1));
            $("#currency").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));

            //Restrict Sales Man
            var salesMan = salesMen.find(p => p.userId == matUserInfo.UserId);
            if (salesMan != undefined) {
                $("#salesMan").html(Utilities.PopulateDropDownFromArray([salesMan], 0, 1));
            } else {
                $("#salesMan").html("<option value=\"0\">--NA--</option>" + Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
            }

            $("#salesMan").chosen({ width: "100%", margin: "1px" });
            $("#pricingLevel").html(Utilities.PopulateDropDownFromArray(pricingLevels, 0, 1));
            $("#tax").html(Utilities.PopulateDropDownFromArray(taxes, 0, 1));
            console.log('all prods', allProducts);


            if (allProducts.length > 0)
            {


                var productNameHtml = "";
                var productCodeHtml = "";
                var barCodeHtml = "";

                $.each(allProducts, function (count, record) {
                    if (record.IsInRightYear == true) {
                        productNameHtml += '<option value="' + record.ProductName + '">' + record.ProductName + '</option>';
                        productCodeHtml += '<option value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                        barCodeHtml += '<option value="' + record.barcode + '">' + record.barcode + '</option>';
                    }
                    else {
                        productNameHtml += '<option disabled="disabled" value="' + record.ProductName + '">' + record.ProductName + '( This product is not active because it was created in higher financial year )</option>';
                        productCodeHtml += '<option disabled="disabled" value="' + record.ProductCode + '">' + record.ProductCode + '( This product is not active because it was created in higher financial year )</option>';
                        barCodeHtml += '<option disabled="disabled" value="' + record.barcode + '">' + record.barcode + '( This product is not active because it was created in higher financial year )</option>';
                    }
                });

                $("#searchByProductName").html(productNameHtml);
                $("#searchByProductCode").html(productCodeHtml);
                $("#searchByBarcode").html(barCodeHtml);


                $("#searchByProductName").chosen({ width: "100%", margin: "1px" });
                $("#searchByBarcode").chosen({ width: "100%", margin: "1px" });
                $("#searchByProductCode").chosen({ width: "100%", margin: "1px" });
            }

            //$("#searchByProductName").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allProducts, 3, 3));
            //$("#searchByBarcode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allProducts, 0, 0));
            //$("#searchByProductCode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allProducts, 2, 2));


            $("#customer").kendoDropDownList({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: {
                    data: customers
                },
                filter: "contains",
            });
            AutoFillForm();
            Utilities.Loader.Hide();
        });

    $("#searchByProductName").on("change", function () {
        searchProduct($("#searchByProductName").val(), "ProductName");
    });
    $("#searchByProductCode").on("change", function () {
        searchProduct($("#searchByProductCode").val(), "ProductCode");
    });
    $("#searchByBarcode").on("change", function () {
        //searchProduct($("#searchByBarcode").val(), "Barcode");    barcode not working as products use productcode as barcode
        searchProduct($("#searchByBarcode").val(), "ProductCode");
    });

    $("#quantity").change(function () {
        resetTax();
        calculateAmountOfItemToAdd();
    });
    $("#rate").change(function () {
        salesRate = $("#rate").val();   //recommended by madam vic to make rate editable as client may decide to change rate at selling point
        resetTax();
        calculateAmountOfItemToAdd();
    });



    $("#salesMode").change(function () {
        var salesMode = $(this).val();
        salesInvoiceLineItems = [];
        renderSalesInvoiceLineItems();
        if (salesMode == "17") {
            $("#applyOn").val("17");
            $("#applyOn").html('<option value="17">Release Form</option>');
            getOrderNumbers();
        }
        else if (salesMode == "10030") {
            $("#applyOn").val("10030");
            $("#applyOn").html('<option value="10030">Sales Order</option>');
            getOrderNumbers();
        }
        else {
            $("#applyOn").val("");
            $("#applyOn").html('');
        }
    });

    var sl = 1;
    $("#addLineItem").click(function () {
        //salesInvoiceLineItems.push({
        //    ProductId: searchResult.productId,
        //    BarCode: searchResult.barcode,
        //    ProductCode: searchResult.productCode,
        //    ProductName: searchResult.productName,
        //    Description: searchResult.narration,
        //    Brand: searchResult.brandName,
        //    Quantity: $("#quantityToAdd").val(),
        //    Unit: searchResult.unitId,
        //    Store: searchResult.godownId,
        //    Rack: searchResult.rackId,
        //    Batch: searchResult.batchId,
        //    PurchaseRate: searchResult.purchaseRate,
        //    MRP: searchResult.Mrp,
        //    Rate: searchResult.salesRate,
        //    GrossValue: (searchResult.salesRate * $("#quantityToAdd").val()).toFixed(2),
        //    DiscountAmout: $("#discountAmount").val(),
        //    NetAmount: "",
        //    Tax: "",
        //    TaxAmount: "0.00",
        //    Amount:""
        //});
        //console.log("searchResult", searchResult);
        console.log(searchResult);
        //var lineTaxAmt = (tax.rate / 100.0) * searchResult.salesRate; 
        //var netAmount = searchResult.salesRate * $("#quantity").val();
        //var grossAmount = searchResult.salesRate * $("#quantity").val();
        //var amount = searchResult.salesRate + lineTaxAmt;
        var rate = Number($("#rate").val());
        //var lineTaxAmt = (tax.rate / 100.0) * rate;
        //var amount = rate + lineTaxAmt;
        var salesMode = $("#salesMode").val();

        if ($("#quantity").val() == 0 || "") {
            Utilities.ErrorNotification("You need to fill the quantity field.");
            return false;
        }

        if ($("#store").val() == "") {
            Utilities.ErrorNotification("You need to add a store.");
            return false;
        }
        if ($("#batch").val() == "") {
            Utilities.ErrorNotification("Select a Batch");
            $("#batch").css("boder", "1px solid red");
            Utilities.Alert("Select a Batch");
            return false;
        }
        salesInvoiceLineItems.push({
            DeliveryNoteDetailsId: salesMode == "Against Delivery Note" ? deliveryNoteDetailsId : 0,
            OrderDetailsId: salesMode == "Against SalesOrder" ? orderDetailsId : 0,
            QuotationDetailsId: salesMode == "Against Quotation" ? quotationDetailsId : 0,
            ProductId: searchResult.productId,
            Qty: $("#quantity").val(),
            Rate: rate,
            UnitId: $("#unit").val(),
            //UnitConversionId : 1,
            Discount: $("#discount").val() != "" ? $("#discount").val() : 0,
            PercentDiscount: $("#percentDiscount").val() != "" ? $("#percentDiscount").val() : 0,
            TaxId: $("#tax").val(),
            BatchId: $("#batch").val(),
            GodownId: $("#store").val(),
            ProjectId: $("#project").val(),
            CategoryId: $("#category").val(),
            RackId: 0,
            TaxAmount: parseFloat($("#taxAmount").val().replace(',', '')), //parseFloat($("#taxAmount").val()),
            GrossAmount: grossValue,
            NetAmount: netValue,
            Amount: amount,
            SlNo: sl,
            ItemDescription: ""
        });

        tax = { taxId: 0, taxName: "NA", rate: 0 };

        console.log(salesInvoiceLineItems);
        $("#searchByBarcode").val("");
        $("#searchByBarcode").trigger("chosen:updated");
        $("#searchByProductCode").val("");
        $("#searchByProductCode").trigger("chosen:updated");
        $("#searchByProductName").val("");
        $("#searchByProductName").trigger("chosen:updated");
        $("#project").val("");
        $("#project").trigger("chosen:updated");
        $("#category").val("");
        $("#category").trigger("chosen:updated");
        $("#store").val("");
        $("#store").trigger("chosen:updated");
        $("#unit").val("");
        //$("#tax").val("0"); 
        $("#taxAmount").val("0");
        $("#quantity").val("");
        $("#quantityInStock").val("");
        $("#storeQuantityInStock").val("");
        $("#amount").val(0);
        $("#rate").val(0);
        $("#description").val("");
        $("#quantityToAdd").val(0);
        $("#discount").val(0);
        $("#percentDiscount").val(0);
        $("#grossValue").val("0");
        $("#netValue").val("0");
        renderSalesInvoiceLineItems();
    });

    $("#customer").change(function () {
        if ($("#salesMode").val() != "0") {
            salesInvoiceLineItems = [];
            renderSalesInvoiceLineItems();
        }
        getOrderNumbers();
    });

    $("#orderNo").change(function () {
        salesInvoiceLineItems = [];
        $.ajax({
            url: API_BASE_URL + "/SalesInvoice/GetSalesModeItems?orderNo=" + $("#orderNo").val() + "&salesMode=" + $("#salesMode").val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                var master = data.Master[0];
                var details = data.Details;


                // $("#invoiceNo").val(master.invoiceNo);
                for (i = 0; i < details.length; i++) {
                    var taxAmt = details[i].taxAmount == undefined ? 0 : details[i].taxAmount;
                    details[i].qty = details[i].qty == undefined ? details[i].Qty : details[i].qty;
                    details[i].rate = details[i].rate == undefined ? details[i].Rate : details[i].rate;
                    details[i].amount = details[i].amount == undefined ? details[i].Amount : details[i].amount;
                    details[i].godownId = details[i].godownId;
                    
                    salesInvoiceLineItems.push({
                        DeliveryNoteDetailsId: details[i].deliveryNoteDetailsId, 
                        OrderDetailsId: 0,
                        QuotationDetailsId: 0,
                        ProductId: details[i].productId,
                        Qty: details[i].qty,
                        Rate: details[i].rate,
                        UnitId: details[i].unitId,
                        UnitConversionId: details[i].unitConversionId,
                        Discount: 0,
                        PercentDiscount: 0,
                        TaxId: details[i].taxId == undefined ? 0 : details[i].taxId,
                        BatchId: details[i].batchId,
                        GodownId: details[i].godownId,
                        RackId: details[i].rackId,
                        TaxAmount: taxAmt,
                        GrossAmount: details[i].amount, //same as amount since discount does not apply
                        NetAmount: parseFloat(details[i].amount),
                        Amount: parseFloat(details[i].amount) + taxAmt,
                        SlNo: (i + 1),
                        ItemDescription: details[i].itemDescription
                    });
                }
                renderSalesInvoiceLineItems();
                Utilities.Loader.Hide();
            },
            error: function () {
                Utilities.Loader.Hide();
            }
        });
    });

    $("#save, #savePrint, #saveReceipt").click(function () {
        myid = $(this).attr('id');
        var customer = $("#customer").data("kendoDropDownList").value();
        if (customer == "") {
            Utilities.ErrorNotification("Please select a customer .");
            return;
        }
        if ($("#transactionDate").val() == "") {
            Utilities.ErrorNotification("Please select transaction date.");
            return;
        }
        else if ($("#invoiceNo").val() == "") {
            Utilities.ErrorNotification("Please enter invoice number.");
            return;
        }
        else if ($("#balance").val() == "") {
            Utilities.ErrorNotification("Please enter credit period.");
            return;
        }
        else if (salesInvoiceLineItems.length < 1) {
            Utilities.ErrorNotification("Transaction has no item added.");
            return;
        }
        else {
            //get orderId or Deliverynote Id 
            var mode = $("#salesMode").val();
            var deliveryId = null;
            var orderId = null;
            //DeliveryNote Mode
            if (mode == 17) {
                deliveryId = $("#orderNo").val();
            }
            else if (mode == 10030) {
                orderId = $("#orderNo").val();
            }
            var toSave =
            {
                SalesMode: $("#salesMode").val(),
                SalesMasterInfo: {
                    Date: $("#transactionDate").val(),
                    VoucherNo: $("#invoiceNo").val(),
                    InvoiceNo: $("#invoiceNo").val(),
                    CreditPeriod: $("#balance").val(),
                    LedgerId: customer,
                    PricinglevelId: $("#pricingLevel").val(),
                    EmployeeId: $("#salesMan").val(),
                    ExchangeRateId: $("#currency").val(),
                    TaxAmount: parseFloat(totalTaxAmount),
                    BillDiscount: $("#billDiscount").val() != "" ? $("#billDiscount").val() : 0,
                    GrandTotal: grandTotal,
                    TotalAmount: totalAmount,
                    LrNo: "",
                    SalesManId: $("#salesMan").val(),
                    TransportationCompany: "",
                    Discount: $("#percentBillDiscount").val() != "" ? $("#percentBillDiscount").val() : 0,
                    OrderMasterId: orderId,
                    DeliveryNoteMasterId: deliveryId,
                    UserId: matUserInfo.UserId
                },
                SalesDetailsInfo: salesInvoiceLineItems
            };

            if (myid == "savePrint") {
                print(salesInvoiceLineItems, toSave.SalesMasterInfo);
            }

            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/SalesInvoice/SaveSalesInvoice",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(toSave),
                success: function (data) {
                    Utilities.SuccessNotification("Invoice saved!");
                    getSalesMasterInvoiceMax();
                    $("#transactionDate").val("");
                    $("#balance").val("");
                    $("#customer")[0].selectedIndex = 0;
                    $("#pricingLevel")[0].selectedIndex = 0;
                    $("#salesMan")[0].selectedIndex = 0;
                    salesInvoiceLineItems = [];
                    totalBillDiscountAmount = 0;
                    totalTaxAmount = 0;
                    grandTotal = 0;
                    totalAmount = 0;
                    renderSalesInvoiceLineItems();
                    $("#narration").val("");
                    $("#totalAmount").val("");
                    $("#billDiscount").val("");
                    $("#percentBillDiscount").val("");
                    $("#grandTotal").val("");
                    Utilities.Loader.Hide();
                    console.log(data);
                    /*if (data.isSaved && myid == "savePrint") {
                        //var id = data.id;
                        print(salesInvoiceLineItems, toSave.SalesMasterInfo);
                        // go to report builder ; 
                        //location.href = `/Report/ReportBuilder/Index/?id=${id}&reportType=${reportType.salesInvoice}`;
                    }
                    else*/
                    if (data.isSaved && myid == "saveReceipt") {
                        Utilities.SuccessNotification("Redirecting to Receive Payment");
                        location.href = `/Customer/Receipt/Receipt`;
                    }
                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }

    });

    /*$("#savePrint").click(function () {
        var customer = $("#customer").data("kendoDropDownList").value();
        if (customer == "") {
            Utilities.ErrorNotification("Please select a customer .");
            return;
        }
        if ($("#transactionDate").val() == "") {
            Utilities.ErrorNotification("Please select transaction date.");
            return;
        }
        else if ($("#invoiceNo").val() == "") {
            Utilities.ErrorNotification("Please enter invoice number.");
            return;
        }
        else if ($("#balance").val() == "") {
            Utilities.ErrorNotification("Please enter credit period.");
            return;
        }
        else if (salesInvoiceLineItems.length < 1) {
            Utilities.ErrorNotification("Transaction has no item added.");
            return;
        }
        else {
            //get orderId or Deliverynote Id 
            var mode = $("#salesMode").val();
            var deliveryId = null;
            var orderId = null;
            //DeliveryNote Mode
            if (mode == 17) {
                deliveryId = $("#orderNo").val();
            }
            else if (mode == 10030) {
                orderId = $("#orderNo").val();
            }
            var toSave =
            {
                SalesMode: $("#salesMode").val(),
                SalesMasterInfo: {
                    Date: $("#transactionDate").val(),
                    VoucherNo: $("#invoiceNo").val(),
                    InvoiceNo: $("#invoiceNo").val(),
                    CreditPeriod: $("#balance").val(),
                    LedgerId: customer,
                    PricinglevelId: $("#pricingLevel").val(),
                    EmployeeId: $("#salesMan").val(),
                    ExchangeRateId: $("#currency").val(),
                    TaxAmount: parseFloat(totalTaxAmount),
                    BillDiscount: $("#billDiscount").val() != "" ? $("#billDiscount").val() : 0,
                    GrandTotal: grandTotal,
                    TotalAmount: totalAmount,
                    LrNo: "",
                    SalesManId: $("#salesMan").val(),
                    TransportationCompany: "",
                    Discount: $("#percentBillDiscount").val() != "" ? $("#percentBillDiscount").val() : 0,
                    OrderMasterId: orderId,
                    DeliveryNoteMasterId: deliveryId
                },
                SalesDetailsInfo: salesInvoiceLineItems
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/SalesInvoice/SaveSalesInvoice",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(toSave),
                success: function (data) {
                    Utilities.SuccessNotification("Invoice saved!");
                    getSalesMasterInvoiceMax();
                    $("#transactionDate").val("");
                    $("#balance").val("");
                    $("#customer")[0].selectedIndex = 0;
                    $("#pricingLevel")[0].selectedIndex = 0;
                    $("#salesMan")[0].selectedIndex = 0;
                    salesInvoiceLineItems = [];
                    totalBillDiscountAmount = 0;
                    totalTaxAmount = 0;
                    grandTotal = 0;
                    totalAmount = 0;
                    renderSalesInvoiceLineItems();
                    Utilities.Loader.Hide();
                    console.log(data); 
                    if (data.isSaved) {
                        var id = data.id; 
                        // go to report builder ; 
                        location.href = `/Report/ReportBuilder/Index/?id=${id }&reportType=${reportType.salesInvoice}`;
                    }
                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }

    });*/
});


//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;

//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}

//get customers with ID supplied
function getCustomerName(id) {
    var customerName = {};
    for (i = 0; i < customers.length; i++) {
        if (customers[i].ledgerId == id) {
            customerName = customers[i].ledgerName;
            break;
        }
    }
    return customerName;
}

function print(salesInvoiceLineItems, SalesMasterInfo) {
    master = SalesMasterInfo;
    details = salesInvoiceLineItems;

    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();
    var thisEmail = $("#companyEmail").val();
    var thisPhone = $("#companyPhone").val();
    var thisVoucherNo = master.InvoiceNo;

    var thisCustomer = getCustomerName(parseInt(master.LedgerId));
    var thisDate = Utilities.FormatJsonDate(master.Date);

    var objToPrint = [];
    $.each(details, function (count, row) {
        /*var taxObj = taxes.find(p => p.taxId == parseInt(row.TaxId));
        var taxName = taxObj == undefined ? "N/A" : taxObj.taxName;*/
        var projectObj = projects.find(p => p.ProjectId == row.ProjectId);
        var projectName = projectObj == undefined ? "N/A" : projectObj.ProjectName;
        var categoryObj = categories.find(p => p.CategoryId == row.CategoryId);
        var categoryName = categoryObj == undefined ? "N/A" : categoryObj.CategoryName;
        objToPrint.push({
            "SlNo": count + 1,
            "Barcode": findProduct(row.ProductId).barcode == undefined ? '-' : findProduct(row.ProductId).barcode ,
            "ProductCode": findProduct(row.ProductId).ProductCode == undefined ? '-' : findProduct(row.ProductId).ProductCode,
            "Product": findProduct(row.ProductId).ProductName == undefined ? '-' : findProduct(row.ProductId).ProductName,
            "WareHouse": findStore(row.GodownId).godownName == undefined ? '-' : findStore(row.GodownId).godownName,
            "Project": projectName,
            "Department": categoryName,
            "Quantity": row.Qty,
            "Unit": findUnit(row.UnitId).unitName == undefined ? '-' : findUnit(row.UnitId).unitName,
            "Rate": 'N' + Utilities.FormatCurrency(row.Rate),
            "TradeDiscount": 'N' + Utilities.FormatCurrency(parseInt(row.Discount)),
            "Amount": 'N' + Utilities.FormatCurrency(row.Amount),
            "Tax": 'N' + Utilities.FormatCurrency(row.TaxAmount),
        });
    });


    columns = [
        { title: "S/N", dataKey: "SlNo" },
        { title: "Barcode", dataKey: "Barcode" },
        { title: "Product Code", dataKey: "ProductCode" },
        { title: "Product", dataKey: "Product" },
        { title: "WareHouse", dataKey: "WareHouse" },
        { title: "Project", dataKey: "Project" },
        { title: "Department", dataKey: "Department" },
        { title: "Quantity", dataKey: "Quantity" },
        { title: "Unit", dataKey: "Unit" },
        { title: "Rate", dataKey: "Rate" },
        { title: "Trade Discount", dataKey: "TradeDiscount" },
        { title: "Amount", dataKey: "Amount" },
        { title: "Tax", dataKey: "Tax" },
    ];

    var doc = new jsPDF('portrait', 'pt', 'letter');
    doc.setDrawColor(0);

    //start drawing
    if (imageUrl != undefined) {
        doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);
    }

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text(companyName, 320, 80, 'center');
    doc.setFontSize(9);
    doc.setFontType("normal");
    doc.text(companyAddress, 320, 90, 'center');

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Email Id: ', 400, 150);
    doc.setFontType("normal");
    doc.text(thisEmail, 460, 150);
    doc.setFontType("bold");
    doc.text('Phone: ', 400, 165);
    doc.setFontType("normal");
    doc.text(thisPhone, 460, 165);

    doc.line(40, 170, 570, 170);

    doc.setFontType("bold");
    doc.setFontSize(9);
    doc.text('Customer:', 40, 185);
    doc.setFontType("normal");
    doc.text(thisCustomer, 85, 185);

    doc.setFontType("bold");
    doc.text('Invoice No: ', 400, 185);
    doc.setFontType("normal");
    doc.text(thisVoucherNo, 460, 185);
    doc.setFontType("bold");
    doc.text('Date: ', 400, 205);
    doc.setFontType("normal");
    doc.text(thisDate, 460, 205);

    doc.line(40, 220, 570, 220);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text('Sales Invoice', 250, 240);

    //doc.line(120, 20, 120, 60); horizontal line
    //doc.setFontSize(8);
    doc.autoTable(columns, objToPrint, {
        startY: 255,
        theme: 'striped',
        styles: {
            fontSize: 5,
        },
        columnStyles: {
            SlNo: { columnWidth: 30, },
            Amount: { columnWidth: 50, halign: 'right' },
        },
    });

    doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

    doc.setFontSize(9);
    doc.setFontType("bold"); //master.TotalAmount + master.TaxAmount
    doc.text('Order Amount: ' + Utilities.FormatCurrency(parseInt(master.TotalAmount)), 555, doc.autoTable.previous.finalY + 20, "right");
    doc.text('Tax Amount: ' + Utilities.FormatCurrency(parseInt(master.TaxAmount)), 555, doc.autoTable.previous.finalY + 35, "right");
    doc.text('Bill Discount: ' + Utilities.FormatCurrency(parseInt(master.BillDiscount)), 555, doc.autoTable.previous.finalY + 50, "right");
    doc.text('Grand Total: ' + Utilities.FormatCurrency(parseInt(master.GrandTotal)), 555, doc.autoTable.previous.finalY + 65, "right");

    doc.line(40, doc.autoTable.previous.finalY + 80, 570, doc.autoTable.previous.finalY + 80);

    doc.setFontType("bold");
    doc.text('Grand Total in Words: ', 40, doc.autoTable.previous.finalY + 95);
    doc.setFontType("normal");

    //remove commas
    //var tempTotal = totalAmt.replace(/,/g, "");
    //convert to number
    var amtToBeConverted = parseInt(master.GrandTotal);

    //capitalize Each Word
    var wordFormat = Utilities.NumberToWords(amtToBeConverted);
    const words = wordFormat.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());

    doc.text(words + ' Naira ONLY.', 140, doc.autoTable.previous.finalY + 95);

    doc.setFontType("bold");
    doc.text('Invoice approved by:  _________________________________', 40, doc.autoTable.previous.finalY + 130);

    doc.text('Done by: ' + $("#nameSpan").html(), 350, doc.autoTable.previous.finalY + 130);
    doc.text('_________________', 390, doc.autoTable.previous.finalY + 133);
    doc.autoPrint();
    doc.save('Sale Invoice' + master.Date + '.pdf');
    //window.open(doc.output('bloburl'));
    window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
}

function getOrderNumbers() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesInvoice/GetSalesModeOrderNo?ledgerId=" + $("#customer").val() + "&voucherTypeId=" + $("#applyOn").val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            //$("#orderNo").html('<option value="">-Select-</option>' + Utilities.PopulateDropDownFromArray(data, 0, 1));
            if ($("#applyOn").val() == 17) {
                $("#orderNo").html('<option value="">-Select-</option>' + Utilities.PopulateDropDownFromArray(data, 0, 0));
            }
            else {
                $("#orderNo").html('<option value="">-Select-</option>' + Utilities.PopulateDropDownFromArray(data.salesOrderNo.Table, 1, 0));
            }

            var voucherId = $("#txtId").val();
            if (voucherId != null || voucherId != undefined) {
                $("#orderNo").val(voucherId);

            }

            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

$("#batch").change(function () {
    resetBatch = false;
    resetTaxVal = false;
    searchProduct($("#searchByProductName").val(), "ProductName");
});

$("#store").change(function () {
    resetBatch = false;
    resetTaxVal = false;
    searchProduct($("#searchByProductName").val(), "ProductName");
});

$("#tax").on("change", function () {
    resetTax();
    calculateAmountOfItemToAdd();
});

function searchProduct(filter, searchBy) {
    var storeId = $("#store").val() == "" ? 0 : parseInt($("#store").val());
    var batchId = $("#batch").val() == "" ? 0 : parseInt($("#batch").val());
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/SearchProduct?filter=" + filter + "&searchBy=" + searchBy + "&storeId=" + storeId + "&batchId=" + batchId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            searchResult = data.Product[0];
            salesRate = searchResult.salesRate;
            //console.log(data);
            productBatches = batches.filter(p => p.productId == parseInt(searchResult.productId));


            if (resetBatch == true) {
                populateBatch();
            }

            if (resetTaxVal == true) {
                tax = taxes.find(p => p.taxId == parseInt(searchResult.taxId));
                $("#tax").val(searchResult.taxId);
                $("#tax").trigger("chosen:updated");
                $("#quantity").val(0);
            }


            $("#unit").val(searchResult.unitId);
            $("#rate").val(salesRate);
            $("#description").val(searchResult.narration);
            $("#searchByBarcode").val(searchResult.productCode);
            $("#searchByBarcode").trigger("chosen:updated");
            $("#searchByProductCode").val(searchResult.productCode);
            $("#searchByProductCode").trigger("chosen:updated");
            $("#searchByProductName").val(searchResult.productName);
            $("#searchByProductName").trigger("chosen:updated");

            if (data.QuantityInStock > 0) {
                $("#quantityInStock").css("color", "black");
                $("#quantityInStock").val(Utilities.FormatQuantity(data.QuantityInStock));
            }
            else if ($("#batch").val() != "") {
                $("#quantityInStock").css("color", "red");
                $("#quantityInStock").val("OUT OF STOCK! " + data.QuantityInStock + "STOCK");
            }
            if (data.StoreQuantityInStock > 0) {
                $("#storeQuantityInStock").css("color", "black");
                $("#storeQuantityInStock").val(data.StoreQuantityInStock);
            }
            else if ($("#batch").val() != "" && $("#store").val() != "") {
                $("#storeQuantityInStock").css("color", "red");
                $("#storeQuantityInStock").val("OUT OF STOCK! " + data.StoreQuantityInStock + " STOCK");
            }

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function populateBatch() {
    $("#batch").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(productBatches, 0, 1));
    $("#batch").trigger("chosen:updated");
}

function populateTax() {
    //$("#tax").html(Utilities.PopulateDropDownFromArray([tax], 0, 1) + "<option value=1>NA</option>");
    //$("#tax").trigger("chosen:updated");
}




function getAllProducts() {
    Utilities.Loader.Show();

    $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            allProducts = data;
            console.log(allProducts);

            var productNameHtml = "";
            var productCodeHtml = "";
            var barCodeHtml = "";

            $.each(allProducts, function (count, record)
            {
                if (record.IsInRightYear == true)
                {
                    productNameHtml += '<option value="' + record.ProductName + '">' + record.ProductName + '</option>';
                    productCodeHtml += '<option value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                    barCodeHtml += '<option value="' + record.barcode + '">' + record.barcode + '</option>';
                }
                else
                {
                    productNameHtml += '<option disabled="disabled" value="' + record.ProductName + '">' + record.ProductName + '( This product is not active because it was created in higher financial year )</option>';
                    productCodeHtml += '<option disabled="disabled" value="' + record.ProductCode + '">' + record.ProductCode + '( This product is not active because it was created in higher financial year )</option>';
                    barCodeHtml += '<option disabled="disabled" value="' + record.barcode + '">' + record.barcode + '( This product is not active because it was created in higher financial year )</option>';
                }
            });

            $("#searchByProductName").html(productNameHtml);
            $("#searchByProductCode").html(productCodeHtml);
            $("#searchByBarcode").html(barCodeHtml);

            $("#searchByProductName").chosen({ width: "100%" });
            $("#searchByProductCode").chosen({ width: "100%" });
            $("#searchByBarcode").chosen({ width: "100%" });
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function renderSalesInvoiceLineItems() {
    var output = "";
    totalTaxAmount = 0.0;
    totalBillDiscountAmount = 0.0;
    totalAmount = 0.0;
    $.each(salesInvoiceLineItems, function (count, row) {
        var taxObj = taxes.find(p => p.taxId == parseInt(row.TaxId));
        var taxName = taxObj == undefined ? "NA" : taxObj.taxName;
        var projectObj = projects.find(p => p.ProjectId == row.ProjectId);
        var projectName = projectObj == undefined ? "NA" : projectObj.ProjectName;
        var categoryObj = categories.find(p => p.CategoryId == row.CategoryId);
        var categoryName = categoryObj == undefined ? "NA" : categoryObj.CategoryName;
        output +=
            '<tr>\
                <td style="white-space: nowrap;"><button onclick="confirmDelete(' + count + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
                <td style="white-space: nowrap;"><button onclick="applyDiscount(' + count + ' )" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></button></td>\
                <td style="white-space: nowrap;">' + (count + 1) + '</td>\
                <td style="white-space: nowrap;">' + findProduct(row.ProductId).barcode + '</td>\
                <td style="white-space: nowrap;">' + findProduct(row.ProductId).ProductCode + '</td>\
                <td style="white-space: nowrap;">' + findProduct(row.ProductId).ProductName + '</td>\
                <td style="white-space: nowrap;">' + findStore(row.GodownId).godownName + '</td>\
                <td style="white-space: nowrap;">' + projectName + '</td>\
                <td style="white-space: nowrap;">' + categoryName + '</td>\
                <td style="white-space: nowrap;">' + row.Qty + '</td>\
                <td style="white-space: nowrap;">' + findUnit(row.UnitId).unitName + '</td>\
                <td style="white-space: nowrap;">' + findBatch(row.BatchId).batchNo + '</td>\
                <td style="white-space: nowrap;">' + row.Rate + '</td>\
                <td style="white-space: nowrap;">' + row.GrossAmount + '</td>\
                <td style="white-space: nowrap;">' + row.Discount + '</td>\
                <td style="white-space: nowrap;">' + row.PercentDiscount + '</td>\
                <td style="white-space: nowrap;">' + row.Amount + '</td>\
                <!--<td style="white-space: nowrap;">' + row.NetAmount + '</td>-->\
                <td style="white-space: nowrap;">' + taxName + '</td>\
                <td style="white-space: nowrap;">' + row.TaxAmount + '</td>\
            </tr>\
            ';
        totalTaxAmount = totalTaxAmount + row.TaxAmount;
        //totalBillDiscountAmount = totalBillDiscountAmount + row.Discount;
        totalAmount = totalAmount + row.Amount;
    });
    var discount = $("#billDiscount").val();
    billDiscountChange();
    grandTotal = (totalAmount + totalTaxAmount) - discount;
    $("#totalAmount").val(totalAmount);
    $("#totalTaxAmount").val(totalTaxAmount);
    //$("#billDiscount").val(totalBillDiscountAmount);
    $("#grandTotal").val(grandTotal);
    $("#salesInvoiceLineItemTbody").html(output);
}

var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the positive lable as key
    //...
};

function confirmDelete(index) {
    deleteIndex = index;
    eModal.confirm(options).then(removeLineItem);
}

function removeLineItem() {
    salesInvoiceLineItems.splice(deleteIndex, 1);
    renderSalesInvoiceLineItems();
}

function resetTax() {
    var isFound = false;
    for (i = 0; i < taxes.length; i++) {
        if (taxes[i].taxId == $("#tax").val()) {
            tax = taxes[i];
            isFound = true;
            break;
        }
    }
    if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
    {
        tax = { taxId: 0, taxName: "NA", rate: 0 };
    }
    //        console.log(tax);
}


function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}

function findProduct(productId) {
    var output = {};
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].ProductId == productId) {
            output = allProducts[i];
            break;
        }
    }
    return output;
}

function calculateAmountOfItemToAdd() {
    grossValue = salesRate * $("#quantity").val();
    netValue = grossValue - parseFloat($("#discount").val());
    taxAmount = netValue * (tax.rate / 100.0);
    amount = netValue; //netValue + taxAmount;
    discountChange();
    $("#grossValue").val(Utilities.FormatCurrency(grossValue));
    $("#netValue").val(Utilities.FormatCurrency(netValue));
    $("#amount").val(Utilities.FormatCurrency(amount));
    $("#taxAmount").val(Utilities.FormatCurrency(taxAmount));
}

$("#discount").change(function () {
    discountChange();
    calculateAmountOfItemToAdd();
});

function discountChange() {
    var discount = $("#discount").val() != "" ? parseFloat($("#discount").val()) : 0;
    var percentDiscount = (discount * 100) / grossValue;
    $("#percentDiscount").val(percentDiscount.toFixed(2));
}

$("#percentDiscount").change(function () {
    var percentDiscount = $("#percentDiscount").val() != "" ? parseFloat($("#percentDiscount").val()) : 0;
    var discount = percentDiscount / 100 * grossValue;
    $("#discount").val(discount.toFixed(2));
    calculateAmountOfItemToAdd();
});


$("#billDiscount").change(function () {
    billDiscountChange();
});

$("#percentBillDiscount").change(function () {
    var percentBillDiscount = $("#percentBillDiscount").val() != "" ? parseFloat($("#percentBillDiscount").val()) : 0;
    var billDiscount = percentBillDiscount / 100 * totalAmount;
    grandTotal = (totalAmount - billDiscount) + totalTaxAmount;
    $("#billDiscount").val(billDiscount);
    $("#grandTotal").val(grandTotal);
});

function billDiscountChange() {
    var billDiscount = $("#billDiscount").val() != "" ? parseFloat($("#billDiscount").val()) : 0;
    var percentBillDiscount = (billDiscount * 100) / totalAmount;
    grandTotal = (totalAmount - billDiscount) + totalTaxAmount;
    $("#percentBillDiscount").val(percentBillDiscount.toFixed(2));
    $("#grandTotal").val(grandTotal);
}

function applyDiscount(id) {
    itemToEditIndex = id;
    itemToEdit = salesInvoiceLineItems[id];
    $("#applyDiscountModal").modal("show");
    $("#applyDiscountAmount").val(itemToEdit.Discount);
    $("#applyPercentDiscount").val(itemToEdit.PercentDiscount);
}

$("#applyDiscountAmount").change(function () {
    var discount = $("#applyDiscountAmount").val() != "" ? parseFloat($("#applyDiscountAmount").val()) : 0;
    var percentDiscount = (discount * 100) / itemToEdit.GrossAmount;
    $("#applyPercentDiscount").val(percentDiscount.toFixed(2));
    //calculateAmountOfItemToAdd();
});

$("#applyPercentDiscount").change(function () {
    var percentDiscount = $("#applyPercentDiscount").val() != "" ? parseFloat($("#applyPercentDiscount").val()) : 0;
    var discount = percentDiscount / 100 * itemToEdit.GrossAmount;
    $("#applyDiscountAmount").val(discount);
    //calculateAmountOfItemToAdd();
});

function applyDiscountOnObject() {
    var grossValue = itemToEdit.Rate * itemToEdit.Qty;
    var netValue = grossValue - parseFloat($("#applyDiscountAmount").val());
    var tax = taxes.find(p => p.taxId == itemToEdit.TaxId);
    var taxRate = tax != undefined ? tax.rate : 0;
    var taxAmount = netValue * (taxRate / 100.0);
    var amount = netValue; //netValue + taxAmount;

    itemToEdit.Discount = $("#applyDiscountAmount").val();
    itemToEdit.PercentDiscount = $("#applyPercentDiscount").val();
    itemToEdit.GrossAmount = grossValue;
    itemToEdit.NetAmount = netValue;
    itemToEdit.TaxAmount = taxAmount;
    itemToEdit.Amount = amount;
    salesInvoiceLineItems[itemToEditIndex] = itemToEdit;
    renderSalesInvoiceLineItems();
}