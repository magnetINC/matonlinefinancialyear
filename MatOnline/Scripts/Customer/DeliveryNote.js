﻿var allProducts = [];
var deliveryNoteLineItems = [];
var searchResult = {};
var customers = [];
var salesMen = [];
var pricingLevels = [];
var currencies = [];
var units = [];
var batches = [];
var stores = [];
var racks = [];
var taxes = [];
var totalLineItemAmount = 0;
var master = {};
var printData = [];
printData.lineItems = [];
var deleteIndex = 0;
var resetBatch = true;
var productBatches = [];

var newDeliveryNote = {

};

var lookUpAjax = $.ajax({
    url: API_BASE_URL + "/SalesInvoice/InvoiceLookUps",
    type: "GET",
    contentType: "application/json",
});

var productLookupAjax = $.ajax({
    url: API_BASE_URL + "/ProductCreation/GetProducts1",
    type: "GET",
    contentType: "application/json",
});

function autoFillData() {
    var txtId = $("#txtId").val();
    if (txtId != null) {
        //get customer 
        var ledgerId = $("#ledgerId").val();


        $("#customers").val(ledgerId);
        $('#customers').trigger('change');
        //set deliver mode 
        $("#deliveryMode").val('Against Order');
        deliveryNoteLineItems = []; //clear array
        //loadOrderNumbers();
        //show order input
        $("#orderNoPanel").show();
        //LOAD ORDER numbers  
        var loadOrderNo = $.ajax({
            url: API_BASE_URL + "/DeliveryNote/LineItemByDeliveryMode?deliveryMode=" + $("#deliveryMode").val() + "&customerId=" + $("#customers").val() + "&currentUserId=" + matUserInfo.UserId,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                var orderNoHtml = '<option></option>';
                if (deliveryMode == "Against Order") {
                    $.each(data, function (count, row) {
                        orderNoHtml += '<option value="' + row.salesOrderMasterId + '">' + row.invoiceNo + '</option>';
                    });
                }
                else if (deliveryMode == "Against Quotation") {
                    $.each(data, function (count, row) {
                        orderNoHtml += '<option value="' + row.quotationMasterId + '">' + row.invoiceNo + '</option>';
                    });
                }
                $("#orderNo").html(orderNoHtml);
                Utilities.Loader.Hide();
            },
            error: function () {
                Utilities.Loader.Hide();
            }
        });

        $.when(loadOrderNo).then(function (data) {
            var orderNoHtml = '<option></option>';


            $.each(data, function (count, row) {
                orderNoHtml += '<option value="' + row.salesOrderMasterId + '">' + row.invoiceNo + '</option>';
            });


            $("#orderNo").html(orderNoHtml);
            Utilities.Loader.Hide();
            $("#orderNo").val(txtId);
            console.log($("#orderNo").val(txtId));
            //LOAD DETAILS ORDER
            var orderNo = txtId;

            Utilities.Loader.Show();
            //  GetOrderDetails(orderNo);

        });

    }
}

function editLineItemQty(sln, strInput) {
    var $input = $(strInput);
    for (var i of deliveryNoteLineItems) {
        if (i.SL == 1) {
            i.Quantity = Number($input.val());
            console.log(i.Quantity);
        }
    }
    console.log(deliveryNoteLineItems);
}
$(function () {
    $(document).keypress(
        function (event) {
            if (event.which == '13') {
                event.preventDefault();
            }
        });

    getDeliveryNoteMasterLastRowDetails();
    //getAllProducts();
    Utilities.Loader.Show();
    $.when(lookUpAjax, productLookupAjax)
        .done(function (dataLookUp, dataProduct) {
            console.log("general lookup", dataLookUp);
            console.log("products", dataProduct);
            allProducts = dataProduct[2].responseJSON;
            customers = dataLookUp[2].responseJSON.Customers;
            salesMen = dataLookUp[2].responseJSON.SalesMen;
            pricingLevels = dataLookUp[2].responseJSON.PricingLevels;
            currencies = dataLookUp[2].responseJSON.Currencies;
            stores = dataLookUp[2].responseJSON.Stores;
            racks = dataLookUp[2].responseJSON.Racks;
            units = dataLookUp[2].responseJSON.Units;
            taxes = dataLookUp[2].responseJSON.Tax;
            batches = dataLookUp[2].responseJSON.Batches;

            var productNameHtml = "";
            var productCodeHtml = "";
            var barCodeHtml = "";
            var storeHtml = "";
            var customersHtml = "";
            var batchHtml = "";

            productNameHtml += '<option value="Select">Select</option>';
            productCodeHtml += '<option  value="Select">Select</option>';
            barCodeHtml += '<option  value="Select">Select</option>';

            $.each(allProducts, function (count, record) {

                if (record.IsInRightYear == true)
                {
                    productNameHtml += '<option value="' + record.ProductName + '">' + record.ProductName + '</option>';
                    productCodeHtml += '<option value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                    barCodeHtml += '<option value="' + record.barcode + '">' + record.barcode + '</option>';
                }
                else
                {

                    productNameHtml += '<option disabled="disabled" value="' + record.ProductName + '">' + record.ProductName + '</option>';
                    productCodeHtml += '<option disabled="disabled" value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                    barCodeHtml += '<option disabled="disabled" value="' + record.barcode + '">' + record.barcode + '</option>';
                }
               
            });

            //Restrict Location
            var nStore = stores.find(p => p.godownId == matUserInfo.StoreId);
            //if (nStore != undefined && matUserInfo.RoleId != 1) {
            //    $.each([nStore], function (count, record) {
            //        storeHtml += '<option value="' + record.godownId + '">' + record.godownName + '</option>';
            //    });
            //} else {
            //    $.each(stores, function (count, record) {
            //        storeHtml += '<option value="' + record.godownId + '">' + record.godownName + '</option>';
            //    });
            //}
            if (nStore == undefined && matUserInfo.RoleId > 0) {
                $.each([nStore], function (count, record) {
                    storeHtml += '<option value="' + record.godownId + '">' + record.godownName + '</option>';
                });
            } else {
                $.each(stores, function (count, record) {
                    storeHtml += '<option value="' + record.godownId + '">' + record.godownName + '</option>';
                });
            }


            $.each(customers, function (count, record) {
                customersHtml += '<option value="' + record.ledgerId + '">' + record.ledgerName + '</option>';
            });

            $.each(batches,
                function (count, record)
                {
                    batchHtml += '<option value="' + record.batchId + '">' + record.batchNo + '</option>';
                });
            $("#batch").html(batchHtml);
            $("#searchByProductName").html("<option value=\"\"></option>" + productNameHtml);
            $("#searchByProductCode").html("<option value=\"\"></option>" + productCodeHtml);
            $("#searchByBarcode").html("<option value=\"\"></option>" + barCodeHtml);


            $("#unit").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(units, 1, 2));
            $("#store").html("<option value=\"\"></option>" + storeHtml);
            $("#customers").html("<option value=\"\"></option>" + customersHtml);

            //Restrict Sales Man
            var salesMan = salesMen.find(p => p.userId == matUserInfo.UserId);
            if (salesMan != undefined) {
                $("#salesMan").html(Utilities.PopulateDropDownFromArray([salesMan], 0, 1));
            } else {
                $("#salesMan").html("<option value=\"0\">--NA--</option>" + Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
            }

            $("#searchByProductName").chosen({ width: "100%" });
            $("#searchByProductCode").chosen({ width: "100%" });
            $("#searchByBarcode").chosen({ width: "100%" });
            $("#store").chosen({ width: "100%" });
            $("#salesMan").chosen({ width: "100%" });
            //$("#customers").chosen({ width: "100%" });
            $("#customers").select2();;


            //$("#store").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
            //$("#unit").html("<option value=\"\"></option>" + units);


            //$("#customers").kendoDropDownList({
            //    autoBind: true,
            //    highlightFirst: true,
            //    suggest: true,
            //    dataTextField: "ledgerName",
            //    dataValueField: "ledgerId",
            //    dataSource: {
            //        data: customers
            //    },
            //    filter: "contains",
            //});


            //var salesMenHtml = "";
            //$.each(salesMen, function (count, record) {
            //    salesMenHtml += '<option value="' + record.employeeId + '">' + record.employeeName + '</option>';
            //});
            // $("#salesMan").html(salesMenHtml);

            var pricingLevelHtml = "";
            $.each(pricingLevels, function (count, record) {
                pricingLevelHtml += '<option value="' + record.pricinglevelId + '">' + record.pricinglevelName + '</option>';
            });
            $("#pricingLevel").html(pricingLevelHtml);

            var currencyHtml = "";
            $.each(currencies, function (count, record) {
                currencyHtml += '<option value="' + record.exchangeRateId + '">' + record.currencyName + '</option>';
            });
            $("#currency").html(currencyHtml);


            //loadDetails(); 
            autoFillData();
            Utilities.Loader.Hide();
        });

    $("#searchResultPanel").hide();

    $("#searchByProductName").on("change", function () {
        resetBatch = true;
        searchProduct($("#searchByProductName").val(), "ProductName");
    });
    $("#searchByProductCode").on("change", function () {
        resetBatch = true;
        searchProduct($("#searchByProductCode").val(), "ProductCode");
    });
    $("#searchByBarcode").on("change", function () {
        resetBatch = true;
        searchProduct($("#searchByBarcode").val(), "Barcode");
    });
    var sl = 1;
    $("#addDeliveryNoteLineItem").click(function () {
        if ($("#quantityToAdd").val() == "" || $("#quantityToAdd").val() == "0") {
            Utilities.ErrorNotification("You have to fill the quantity field!");
            return false;
        }
        if ($("#store").val() == "") {
            Utilities.ErrorNotification("You have to select store");
            return false;
        }

        if ($("#batch").val() == "") {
            Utilities.ErrorNotification("You have to select Batch");
            return false;
        }

       // debugger;
        deliveryNoteLineItems.push({
            SL: sl,
            ProductId: searchResult.productId,
            ProductBarcode: searchResult.barcode,
            ProductCode: searchResult.productCode,
            ProductName: searchResult.productName,
            Description: searchResult.narration,
            Brand: searchResult.brandName,
            Quantity: $("#quantityToAdd").val(),
            UnitId: searchResult.unitId,
            StoreId: $("#store").val(),//searchResult.godownId,
            RackId: searchResult.rackId,
            BatchId: $("#batch").val(),
            Rate: $("#rate").val(),// searchResult.salesRate,
            Amount: parseFloat($("#quantityToAdd").val()) * parseFloat($("#rate").val()) //searchResult.salesRate
        });
        sl = sl + 1;
        console.log("searchResult", searchResult);
        $("#searchByBarcode").val("");
        $("#searchByBarcode").trigger("chosen:updated");
        $("#searchByProductCode").val("");
        $("#searchByProductCode").trigger("chosen:updated");
        $("#searchByProductName").val("");
        $("#searchByProductName").trigger("chosen:updated");
        $("#rate").val("");
        $("#unit").val("");
        $("#amount").val("");
        $("#quantityToAdd").val("");
        $("#description").val("");
        $("#quantityInStock").val("");
        $("#storeQuantityInStock").val("");
        renderDeliveryNoteLineItems();
        RefreashForm();
    });


    var options2 = {
        message: "Are you sure you want to proceed with saving?",
        title: 'Confirm Save',
        size: 'sm',
        subtitle: 'smaller text header',
        label: "Yes"   // use the possitive lable as key
        //...
    };

    function RefreashForm()
    {

        //$(document).keypress(
        //    function (event) {
        //        if (event.which == '13') {
        //            event.preventDefault();
        //        }
        //    });

        //getDeliveryNoteMasterLastRowDetails();
        //getAllProducts();
        Utilities.Loader.Show();
        $.when(lookUpAjax, productLookupAjax)
            .done(function (dataLookUp, dataProduct) {
                //console.log("general lookup", dataLookUp);
                //console.log("products", dataProduct);
                allProducts = dataProduct[2].responseJSON;
               // customers = dataLookUp[2].responseJSON.Customers;
              //  salesMen = dataLookUp[2].responseJSON.SalesMen;
                pricingLevels = dataLookUp[2].responseJSON.PricingLevels;
                currencies = dataLookUp[2].responseJSON.Currencies;
                stores = dataLookUp[2].responseJSON.Stores;
                racks = dataLookUp[2].responseJSON.Racks;
                units = dataLookUp[2].responseJSON.Units;
                taxes = dataLookUp[2].responseJSON.Tax;
                batches = dataLookUp[2].responseJSON.Batches;

                var productNameHtml = "";
                var productCodeHtml = "";
                var barCodeHtml = "";
                var storeHtml = "";
                var customersHtml = "";
                var batchHtml = "";

                productNameHtml += '<option value="Select">Select</option>';
                productCodeHtml += '<option  value="Select">Select</option>';
                barCodeHtml += '<option  value="Select">Select</option>';
                $.each(allProducts, function (count, record) {

                    if (record.IsInRightYear == true) {
                        productNameHtml += '<option value="' + record.ProductName + '">' + record.ProductName + '</option>';
                        productCodeHtml += '<option value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                        barCodeHtml += '<option value="' + record.barcode + '">' + record.barcode + '</option>';
                    }
                    else {

                        productNameHtml += '<option disabled="disabled" value="' + record.ProductName + '">' + record.ProductName + '</option>';
                        productCodeHtml += '<option disabled="disabled" value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                        barCodeHtml += '<option disabled="disabled" value="' + record.barcode + '">' + record.barcode + '</option>';
                    }

                });

                //Restrict Location
                var nStore = stores.find(p => p.godownId == matUserInfo.StoreId);
                //if (nStore != undefined && matUserInfo.RoleId != 1) {
                //    $.each([nStore], function (count, record) {
                //        storeHtml += '<option value="' + record.godownId + '">' + record.godownName + '</option>';
                //    });
                //} else {
                //    $.each(stores, function (count, record) {
                //        storeHtml += '<option value="' + record.godownId + '">' + record.godownName + '</option>';
                //    });
                //}
                if (nStore == undefined && matUserInfo.RoleId > 0) {
                    $.each([nStore], function (count, record) {
                        storeHtml += '<option value="' + record.godownId + '">' + record.godownName + '</option>';
                    });
                } else {
                    $.each(stores, function (count, record) {
                        storeHtml += '<option value="' + record.godownId + '">' + record.godownName + '</option>';
                    });
                }


                //$.each(customers, function (count, record) {
                //    customersHtml += '<option value="' + record.ledgerId + '">' + record.ledgerName + '</option>';
                //});

                $.each(batches,
                    function (count, record) {
                        batchHtml += '<option value="' + record.batchId + '">' + record.batchNo + '</option>';
                    });
                $("#batch").html(batchHtml);
                $("#searchByProductName").html("<option value=\"\"></option>" + productNameHtml);
                $("#searchByProductCode").html("<option value=\"\"></option>" + productCodeHtml);
                $("#searchByBarcode").html("<option value=\"\"></option>" + barCodeHtml);


                $("#unit").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(units, 1, 2));
                $("#store").html("<option value=\"\"></option>" + storeHtml);

                //$("#customers").html("<option value=\"\"></option>" + customersHtml);

                //Restrict Sales Man
                //var salesMan = salesMen.find(p => p.userId == matUserInfo.UserId);
                //if (salesMan != undefined) {
                //    $("#salesMan").html(Utilities.PopulateDropDownFromArray([salesMan], 0, 1));
                //} else {
                //    $("#salesMan").html("<option value=\"0\">--NA--</option>" + Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
                //}

                $("#searchByProductName").chosen({ width: "100%" });
                $("#searchByProductCode").chosen({ width: "100%" });
                $("#searchByBarcode").chosen({ width: "100%" });
                $("#store").chosen({ width: "100%" });
                $("#salesMan").chosen({ width: "100%" });
                //$("#customers").chosen({ width: "100%" });
                $("#customers").select2();;


                //$("#store").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
                //$("#unit").html("<option value=\"\"></option>" + units);


                //$("#customers").kendoDropDownList({
                //    autoBind: true,
                //    highlightFirst: true,
                //    suggest: true,
                //    dataTextField: "ledgerName",
                //    dataValueField: "ledgerId",
                //    dataSource: {
                //        data: customers
                //    },
                //    filter: "contains",
                //});


                //var salesMenHtml = "";
                //$.each(salesMen, function (count, record) {
                //    salesMenHtml += '<option value="' + record.employeeId + '">' + record.employeeName + '</option>';
                //});
                // $("#salesMan").html(salesMenHtml);

                var pricingLevelHtml = "";
                $.each(pricingLevels, function (count, record) {
                    pricingLevelHtml += '<option value="' + record.pricinglevelId + '">' + record.pricinglevelName + '</option>';
                });
                $("#pricingLevel").html(pricingLevelHtml);

                var currencyHtml = "";
                $.each(currencies, function (count, record) {
                    currencyHtml += '<option value="' + record.exchangeRateId + '">' + record.currencyName + '</option>';
                });
                $("#currency").html(currencyHtml);


                //loadDetails(); 
                ///autoFillData();
                Utilities.Loader.Hide();
            });
    }


    function saveDeliveryNoteDetails() {
        var validItem = validateQtyToDeliver();
        if (validItem == false) {
            // Utilities.Alert("Quantity to delivery must be above 0");
            return;
        }
        if (deliveryNoteLineItems.length < 1) {
            Utilities.ErrorNotification("Line item is Empty.");
            return;
        }
        if ($("#print").is(':checked')) {
            print();
        }

        //var orderNoToSave = master.hasOwnProperty() ? "" : master.invoiceNo;
        newDeliveryNote = {
            DeliveryNoteNo: $("#invoiceNo").val(),//master.invoiceNo,
            Date: $("#transactionDate").val(),
            CustomerId: $("#customers").val(),//master.ledgerId,
            DeliveryMode: $("#deliveryMode").val(),
            Narration: $("#narration").val(),
            TotalAmount: totalLineItemAmount,
            TransportaionCompany: $("#transportationCompany").val() == "" ? "NULL" : $("#transportationCompany").val(),
            LrNo: $("#lrNo").val() == "" ? "0" : $("#lrNo").val(),
            OrderNo: master.invoiceNo,
            PricingLevelId: 1,
            SalesManId: $("#salesMan").val(),
            SalesOrderMasterId: master.salesOrderMasterId,
            CurrencyId: $("#currency").val(),
            //OrderDetailsId: 1,
            UserId: matUserInfo.UserId,
            LineItems: deliveryNoteLineItems,
        };
        //console.log(newDeliveryNote); return;
        $.ajax({
            url: API_BASE_URL + "/DeliveryNote/SaveOrEditFunction",
            type: "POST",
            data: JSON.stringify(newDeliveryNote),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification(data.ResponseMessage);
                    Utilities.Loader.Hide();
                    //window.location = "/Customer/SalesOrder/ReleaseFormListing";
                    $("#invoiceNo").val("");
                    //$("#transactionDate").val("");
                    $("#narration").val("");
                    totalLineItemAmount = 0.0;
                    $("#transportationCompany").val("");
                    $("#lrNo").val("");
                    $("#orderNo").val("");

                    $("#customers").val("");
                    $("#customers").trigger("chosen:updated");
                    $("#store").val("");
                    $("#store").trigger("chosen:updated");

                    deliveryNoteLineItems = [];
                    getDeliveryNoteMasterLastRowDetails();
                    renderDeliveryNoteLineItems();

                    //$("#invoiceNo").val("");
                    //$("#transactionDate").val("");
                    //$("#narration").val("");
                    //totalLineItemAmount = 0.0;
                    //$("#transportationCompany").val("");
                    //$("#lrNo").val("");
                    //$("#orderNo").val("");
                    //deliveryNoteLineItems = [];
                    //renderDeliveryNoteLineItems();
                }
                else if (data.ResponseCode == 400) {
                    Utilities.ErrorNotification(data.ResponseMessage);
                }
                //else if (data == -1) {
                //    Utilities.ErrorNotification("Order number already exist!");
                //}
                else {
                    Utilities.ErrorNotification("Delivery note could not save!");
                }
            },
            error: function (err) {
                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                Utilities.Loader.Hide();
            }
        });
    }


    $("#saveDeliveryNote").click(function () {
        eModal.confirm(options2).then(saveDeliveryNoteDetails);
    });

    $("#savePrint").click(function () {
        Utilities.Loader.Show();
        var validItem = validateQtyToDeliver();
        if (validItem == false) {
            // Utilities.Alert("Quantity to delivery must be above 0");
            return;
        }

        if (deliveryNoteLineItems.length < 1) {
            Utilities.ErrorNotification("Line item is Empty.");
            return;
        }

        printData = {
            ledgerId: $("#customers").val(),
            invoiceNo: $("#invoiceNo").val(),
            extraDate: todayDate(),
            date: todayDate()
        };

        printData.lineItems = deliveryNoteLineItems;

        //var orderNoToSave = master.hasOwnProperty() ? "" : master.invoiceNo;
        newDeliveryNote = {
            DeliveryNoteNo: $("#invoiceNo").val(),//master.invoiceNo,
            Date: todayDate(),
            CustomerId: $("#customers").val(),//master.ledgerId,
            DeliveryMode: $("#deliveryMode").val(),
            Narration: $("#narration").val(),
            TotalAmount: totalLineItemAmount,
            TransportaionCompany: $("#transportationCompany").val() == "" ? "NULL" : $("#transportationCompany").val(),
            LrNo: $("#lrNo").val() == "" ? "0" : $("#lrNo").val(),
            OrderNo: master.invoiceNo,
            PricingLevelId: 1,
            SalesManId: $("#salesMan").val(),
            SalesOrderMasterId: master.salesOrderMasterId,
            CurrencyId: $("#currency").val(),
            //OrderDetailsId: 1,
            UserId: matUserInfo.UserId,
            LineItems: deliveryNoteLineItems,
        };
        //console.log(newDeliveryNote); return;
        $.ajax({
            url: API_BASE_URL + "/DeliveryNote/SaveOrEditFunction",
            type: "POST",
            data: JSON.stringify(newDeliveryNote),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification(data.ResponseMessage);
                    Utilities.Loader.Hide();
                    //window.location = "/Customer/SalesOrder/ReleaseFormListing";
                    $("#invoiceNo").val("");
                    //$("#transactionDate").val("");
                    $("#narration").val("");
                    totalLineItemAmount = 0.0;
                    $("#transportationCompany").val("");
                    $("#lrNo").val("");
                    $("#orderNo").val("");

                    $("#customers").val("");
                    $("#customers").trigger("chosen:updated");
                    $("#store").val("");
                    $("#store").trigger("chosen:updated");

                    deliveryNoteLineItems = [];
                    getDeliveryNoteMasterLastRowDetails();
                    renderDeliveryNoteLineItems();
                    print();
                    Utilities.Loader.Hide();
                    //$("#invoiceNo").val("");
                    //$("#transactionDate").val("");
                    //$("#narration").val("");
                    //totalLineItemAmount = 0.0;
                    //$("#transportationCompany").val("");
                    //$("#lrNo").val("");
                    //$("#orderNo").val("");
                    //deliveryNoteLineItems = [];
                    //renderDeliveryNoteLineItems();
                }
                else if (data.ResponseCode == 400) {
                    Utilities.ErrorNotification(data.ResponseMessage);
                    Utilities.Loader.Hide();
                }
                //else if (data == -1) {
                //    Utilities.ErrorNotification("Order number already exist!");
                //}
                else {
                    Utilities.ErrorNotification("Delivery note could not save!");
                    Utilities.Loader.Hide();
                }
            },
            error: function (err) {
                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                Utilities.Loader.Hide();
            }
        });     

    });

    $("#orderNoPanel").hide();
    $("#deliveryMode").change(function () {
        deliveryNoteLineItems = []; //clear array
        loadOrderNumbers();
    });
    $("#customers").change(function () {
        deliveryNoteLineItems = []; //clear array
        loadOrderNumbers();
    });
    $("#rate").change(function () {
        calculateAmount();
    });
    $("#quantityToAdd").change(function () {
        calculateAmount();
    });
    $("#orderNo").change(function () {
        var orderNo = $("#orderNo").val();
        if (orderNo != "") {
            Utilities.Loader.Show();
            GetOrderDetails(orderNo);
            $.ajax({
                url: API_BASE_URL + "/DeliveryNote/PopulateLineItems?deliveryMode=" + $("#deliveryMode").val() + "&orderNo=" + orderNo,
                type: "GET",
                contentType: "application/json",
                success: function (data) {
                    var sl = 1;
                    master = data.Master[0];
                    var details = data.Details;
                    printData = master;

                    //$.each(details, function (count,row) {

                    //});
                    //batches = data.Batch;
                    //units = data.Units;
                    //stores = data.Stores;
                    //racks = data.Racks;
                    //taxes = data.Taxes;
                    console.log("details",details);
                    deliveryNoteLineItems = []; //clear array
                    //deliveryNoteLineItems = details;
                    for (i = 0; i < details.length; i++)
                    {
                        deliveryNoteLineItems.push({
                            SL: sl,
                            ProductId: details[i].productId,
                            ProductBarcode: details[i].productCode,
                            ProductCode: details[i].productCode,
                            ProductName: details[i].productName,
                            Description: details[i].narration,
                            //Brand: 0,
                            SalesOrderDetailsId: details[i].salesOrderDetailsId,
                            Quantity: details[i].Qty,
                            UnitId: details[i].unitId,
                            StoreId: details[i].extra1/*.replace('.00000', '')*/,
                            RackId: details[i].rackId,
                            BatchId: 1,//details[i].batchId,
                            Rate: details[i].Rate,
                            Extra2: parseInt(details[i].extra2),
                            Amount: details[i].Rate * details[i].Qty //details.Amount    [not using the amount from backend since we ar not splitting quantity delivered
                            //from quantity ordered, amount should be rate * quantity delivered]
                        });
                        sl = sl + 1;
                        //printData.lineItems = deliveryNoteLineItems;
                    }
                    //console.log(deliveryNoteLineItems);
                    renderDeliveryNoteLineItems();


                    Utilities.Loader.Hide();
                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }

    });
});

function GetOrderDetails(orderMasterNo) {
    $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetSaleOrderFromMaster/?salesOrderMasterId=" + orderMasterNo,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            var sl = 1;
            if (data == null) return;
            master = data[0].SaleOrderMaster;
            var details = data;
            printData = master;

            console.log("details", details);
            deliveryNoteLineItems = []; //clear array
            //deliveryNoteLineItems = details;
            for (i = 0; i < details.length; i++) {
                deliveryNoteLineItems.push({
                    SL: sl,
                    ProductId: details[i].Product.productId,
                    ProductBarcode: details[i].Product.productCode,
                    ProductCode: details[i].Product.productCode,
                    ProductName: details[i].Product.productName,
                    Description: details[i].Product.narration,
                    //Brand: 0,
                    SalesOrderDetailsId: details[i].OrderDetail.salesOrderDetailsId,
                    Quantity: details[i].OrderDetail.qty,
                    UnitId: details[i].OrderDetail.unitId,
                    StoreId: details[i].OrderDetail.extra1/*.replace('.00000', '')*/,
                    RackId: details[i].OrderDetail.rackId,
                    BatchId: details[i].OrderDetail.batchId,//details[i].batchId,
                    Rate: details[i].OrderDetail.rate,
                    Extra2: parseInt(details[i].OrderDetail.extra1),
                    Amount: details[i].OrderDetail.rate * details[i].OrderDetail.qty //details.Amount    [not using the amount from backend since we ar not splitting quantity delivered
                    //from quantity ordered, amount should be rate * quantity delivered]
                });
                sl = sl + 1;
                //printData.lineItems = deliveryNoteLineItems;
            }
            //console.log(deliveryNoteLineItems);
            renderDeliveryNoteLineItems();


            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}


//$('#itemLineModal').on('show.bs.modal', function (e) { 
//    if ($("#store").val() == "") {
//        Utilities.ErrorNotification("Please select store to pick item from.");
//        e.preventDefault();
//    }
//})



//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;

//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}

//to print
function print() {
    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();
    var thisEmail = $("#companyEmail").val();
    var thisPhone = $("#companyPhone").val();
    var thisVoucherNo = printData.invoiceNo;

    var thisCustomer = findCust(printData.ledgerId).ledgerName;
    var thisDate = Utilities.FormatJsonDate(printData.date);
    var Narration = $("#narration").val()

    //(findStore(+row.StoreId).godownName == undefined ? "NA" : findStore(+row.StoreId).godownName)


    var objToPrint = [];
    $.each(printData.lineItems, function (count, row) {
        var thisStore = (findStore(+row.StoreId).godownName == undefined ? "N/A" : findStore(+row.StoreId).godownName)
        objToPrint.push({
            "SL": row.SL,
            "ProductBarcode": row.ProductBarcode,
            "ProductCode": row.ProductCode,
            "ProductName": row.ProductName,
            "Store": thisStore,
            "Description": row.Description,  //findUnit(row.UnitId).unitName
            "Quantity": row.Quantity,
            "Unit": findUnit(row.UnitId).unitName,
        });
    });

    var columns = [
        { title: "SL", dataKey: "SL" },
        { title: "Barcode", dataKey: "ProductBarcode" },
        { title: "Product Code", dataKey: "ProductCode" },
        { title: "Product Name", dataKey: "ProductName" },
        { title: "Store", dataKey: "Store" },
        { title: "Description", dataKey: "Description" },
        { title: "Qty Delivered", dataKey: "Quantity" },
        { title: "Unit", dataKey: "Unit" },
    ];

    var doc = new jsPDF('portrait', 'pt', 'letter');
    doc.setDrawColor(0);

    //start drawing
    doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text(companyName, 320, 80, 'center');
    doc.setFontSize(9);
    doc.setFontType("normal");
    doc.text(companyAddress, 320, 90, 'center');

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Email Id: ', 400, 150);
    doc.setFontType("normal");
    doc.text(thisEmail, 460, 150);
    doc.setFontType("bold");
    doc.text('Phone: ', 400, 165);
    doc.setFontType("normal");
    doc.text(thisPhone, 460, 165);

    doc.line(40, 170, 570, 170);

    doc.setFontType("bold");
    doc.setFontSize(9);
    doc.text('Customer:', 40, 185);
    doc.setFontType("normal");
    doc.text(thisCustomer, 85, 185);
    doc.setFontType("bold");
    doc.text('Narration:', 40, 205);
    doc.setFontType("normal");
    doc.text(Narration, 85, 205);

    doc.setFontType("bold");
    doc.text('Order No: ', 400, 185);
    doc.setFontType("normal");
    doc.text(thisVoucherNo, 460, 185);
    doc.setFontType("bold");
    doc.text('Order Date: ', 400, 205);
    doc.setFontType("normal");
    doc.text(thisDate, 460, 205);

    doc.line(40, 220, 570, 220);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text('Delivery Note', 250, 240);

    //doc.line(120, 20, 120, 60); horizontal line
    //doc.setFontSize(8);
    doc.autoTable(columns, objToPrint, {
        startY: 255,
        theme: 'striped',
        styles: {
            fontSize: 8,
        },
    });

    doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

    doc.setFontType("bold");
    doc.text('Authorized by:  _________________', 40, doc.autoTable.previous.finalY + 30);

    doc.text('Done by: ' + $("#nameSpan").html(), 350, doc.autoTable.previous.finalY + 30);
    doc.text('_________________', 390, doc.autoTable.previous.finalY + 33);

    doc.autoPrint();
    doc.save('DeliveryNote' + printData.invoiceNo + '.pdf');
    //window.open(doc.output('bloburl'));
    window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
}



function print_() {
    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();
    var thisEmail = $("#companyEmail").val();
    var thisPhone = $("#companyPhone").val();

    var columns = [
        { title: "SL", dataKey: "SL" },
        { title: "Barcode", dataKey: "ProductBarcode" },
        { title: "Product Code", dataKey: "ProductCode" },
        { title: "Product Name", dataKey: "ProductName" },
        { title: "Description", dataKey: "Description" },
        //{ title: "Qty Ordered", dataKey: "Quantity" },
        { title: "Qty Delivered", dataKey: "Quantity" },
        //{ title: "Qty Remaining", dataKey: "" },

    ];
    var doc = new jsPDF('landscape');
    doc.setFontSize(30);
    doc.text(companyName, 15, 30);
    doc.setFontSize(8);
    doc.text(companyAddress, 15, 35);
    doc.text('_________\n\n Signature & Date', 35, 190);
    doc.text('_________\n\n Signature & Date', 227, 190);
    doc.text('Agent Name: ' + findCust(printData.ledgerId).ledgerName + '\n\nOrder No: ' + printData.invoiceNo + '\n\nOrder Date: '
        + printData.date + '\n\nAuthorized Date: ' + Utilities.FormatJsonDate(printData.extraDate), 240, 20);

    doc.autoTable(columns, printData.lineItems, {
        startY: 45, theme: 'grid', styles: {
            fontSize: 8
        }
    });
    doc.save('DeliveryNote' + printData.invoiceNo + '.pdf');
    window.open(doc.output('bloburl'));
}

function findCust(ledgerId) {
    var output = {};
    for (i = 0; i < customers.length; i++) {
        if (customers[i].ledgerId == ledgerId) {
            output = customers[i];
            break;
        }
    }
    return output;
}

function loadOrderNumbers() {
    var deliveryMode = $("#deliveryMode").val();
    if (deliveryMode != "NA") {
        $("#orderNoPanel").show();
        Utilities.Loader.Show();
        var val = $("#deliveryMode").val();
        $.ajax({
            url: API_BASE_URL + "/DeliveryNote/LineItemByDeliveryMode?deliveryMode=" + deliveryMode + "&customerId=" + $("#customers").val() + "&currentUserId=" + matUserInfo.UserId,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                var orderNoHtml = '<option></option>';
                if (deliveryMode == "Against Order") {
                    $.each(data, function (count, row) {
                        orderNoHtml += '<option value="' + row.salesOrderMasterId + '">' + row.invoiceNo + '</option>';
                    });
                }
                else if (deliveryMode == "Against Quotation") {
                    $.each(data, function (count, row) {
                        orderNoHtml += '<option value="' + row.quotationMasterId + '">' + row.invoiceNo + '</option>';
                    });
                }
                $("#orderNo").html(orderNoHtml);
                Utilities.Loader.Hide();
            },
            error: function () {
                Utilities.Loader.Hide();
            }
        });
    }
    else {
        $("#orderNoPanel").hide();
    }
}

$("#batch").change(function () {
    resetBatch = false;
    searchProduct($("#searchByProductName").val(), "ProductName");
});

$("#store").change(function () {
    resetBatch = false;
    searchProduct($("#searchByProductName").val(), "ProductName");
});

function searchProduct(filter, searchBy) {
    var storeId = $("#store").val() == "" ? 0 : parseInt($("#store").val());
    var batchId = $("#batch").val() == "" ? 0 : parseInt($("#batch").val());
    Utilities.Loader.Show();
    $.ajax({
        //url: API_BASE_URL + "/ProductCreation/SearchProduct?filter="+filter+"&searchBy="+searchBy,
        url: API_BASE_URL + "/ProductCreation/SearchProduct?filter=" + filter + "&searchBy=" + searchBy + "&storeId=" + storeId + "&batchId=" + batchId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data.Unit);
            var productDetails = data.Product[0];
            searchResult = productDetails;
            productBatches = batches.filter(p => p.productId == parseInt(searchResult.productId));

            if (resetBatch == true) {
                populateBatch();
            }

            batches = data.Batch;
            units = data.Unit;
            stores = data.Stores;
            racks = data.Racks;
            taxes = data.Tax;
            console.log("batches", batches);
            //$("#searchResultName").val(productDetails.productName);
            $("#rate").val(productDetails.salesRate);
            $("#unit").val(productDetails.unitId);
            $("#amount").val(calculateAmount());
            $("#searchByBarcode").val(searchResult.productCode);
            $("#searchByBarcode").trigger("chosen:updated");
            $("#searchByProductCode").val(searchResult.productCode);
            $("#searchByProductCode").trigger("chosen:updated");
            $("#searchByProductName").val(searchResult.productName);
            $("#searchByProductName").trigger("chosen:updated");

            if (data.QuantityInStock > 0) {
                $("#quantityInStock").css("color", "black");
                $("#quantityInStock").val(Utilities.FormatQuantity(data.QuantityInStock));
            }
            else if ($("#quantityInStock").val() != "") {
                $("#quantityInStock").css("color", "red");
                $("#quantityInStock").val("OUT OF STOCK! " + data.QuantityInStock + "STOCK");
            }
            if (data.StoreQuantityInStock > 0) {
                $("#storeQuantityInStock").css("color", "black");
                $("#storeQuantityInStock").val(data.StoreQuantityInStock);
            }
            else if ($("#storeQuantityInStock").val() != "") {
                $("#storeQuantityInStock").css("color", "red");
                $("#storeQuantityInStock").val("OUT OF STOCK! " + data.StoreQuantityInStock + " STOCK");
            }

            var storesHtml = '';
            $.each(data.Stores, function (count, row) {
                storesHtml += '<option value="' + row.godownId + '">' + row.godownName + '</option>';
            });
            $("#searchResultStore").html(storesHtml);

            var racksHtml = '';
            $.each(data.Racks, function (count, row) {
                racksHtml += '<option value="' + row.rackId + '">' + row.rackName + '</option>';
            });
            $("#searchResultRack").html(racksHtml);

            $("#searchResultPanel").show();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function populateBatch() {
    $("#batch").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(productBatches, 0, 1));
    $("#batch").trigger("chosen:updated");
}

function calculateAmount() {
    var quantity = parseFloat($("#rate").val());
    var rate = parseFloat($("#quantityToAdd").val());
    var amount = isNaN(quantity * rate) ? 0 : quantity * rate;
    $("#amount").val(amount);
    return amount;
}

function getAllProducts() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts1",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            allProducts = data;
            console.log(allProducts);

            var productNameHtml = "";
            var productCodeHtml = "";
            var barCodeHtml = "";

            $.each(allProducts, function (count, record) {

                if (record.IsInRightYear == true) {

                    productNameHtml += '<option value="' + record.ProductName + '">' + record.ProductName + '</option>';
                    productCodeHtml += '<option value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                    barCodeHtml += '<option value="' + record.barcode + '">' + record.barcode + '</option>';

                }
                else {


                    productNameHtml += '<option disabled="disabled" value="' + record.ProductName + '">' + record.ProductName + '</option>';
                    productCodeHtml += '<option disabled="disabled" value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                    barCodeHtml += '<option disabled="disabled" value="' + record.barcode + '">' + record.barcode + '</option>';


                }
              

            });
            $("#searchByProductName").html("<option value=\"\"></option>" + productNameHtml);
            $("#searchByProductCode").html("<option value=\"\"></option>" + productCodeHtml);
            $("#searchByBarcode").html("<option value=\"\"></option>" + barCodeHtml);

            $("#searchByProductName").chosen({ width: "100%" });
            $("#searchByProductCode").chosen({ width: "100%" });
            $("#searchByBarcode").chosen({ width: "100%" });
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function loadDetails() {
    Utilities.Loader.Show();
    $.ajax({
        // url: API_BASE_URL + "/DeliveryNote/PopulateLineItems?deliveryMode=Against Order&orderNo=" + orderNoFromUrl,
        url: API_BASE_URL + "/DeliveryNote/PopulateLineItems?deliveryMode=Against Order&orderNo=" + 120170,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            var sl = 1;
            master = data.Master[0];
            var details = data.Details;
            console.log("details", master);
            deliveryNoteLineItems = []; //clear array
            //deliveryNoteLineItems = details;
            for (i = 0; i < details.length; i++) {
                deliveryNoteLineItems.push({
                    SL: sl,
                    ProductId: details[i].productId,
                    ProductBarcode: details[i].productCode,
                    ProductCode: details[i].productCode,
                    ProductName: details[i].productName,
                    Description: details[i].narration,
                    //Brand: 0,
                    SalesOrderDetailsId: details[i].salesOrderDetailsId,
                    Quantity: details[i].Qty,
                    UnitId: details[i].unitId,
                    StoreId: details[i].extra1/*.replace('.00000', '')*/,
                    RackId: details[i].rackId,
                    BatchId: 1,//details[i].batchId,
                    Rate: details[i].Rate,
                    Extra2: parseInt(details[i].extra2),
                    Amount: details[i].Rate * details[i].Qty //details.Amount    [not using the amount from backend since we ar not splitting quantity delivered
                    //from quantity ordered, amount should be rate * quantity delivered]
                });
                printData = master;
                //printData.lineItems = deliveryNoteLineItems;
                sl = sl + 1;
            }
            //$("#invoiceNo").val(master.invoiceNo);
            //$("#customers").val(master.ledgerId);

            //console.log(deliveryNoteLineItems);
            renderDeliveryNoteLineItems();


            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

//function renderDeliveryNoteLineItems()
//{
//    totalLineItemAmount = 0;
//    var output = "";
//    console.log("b4 rendering", deliveryNoteLineItems);
//    $.each(deliveryNoteLineItems, function (count, row) {
//        var img = row.ProductId+".jpg";
//        output +=
//            '<tr>\
//                <td style="white-space: nowrap;">'+(count+1)+'</td>\
//                <td style="white-space: nowrap;"><img style="width:40px;height:40px;" class="img-md img-circle" src="/Content/ProductImages/' + img + '" onError="this.src =\'/Content/ProductImages/default.jpg\'" alt="Sample Image"></td>\
//                <td style="white-space: nowrap;">'+row.ProductBarcode+'</td>\
//                <td style="white-space: nowrap;">'+row.ProductCode+'</td>\
//                <td style="white-space: nowrap;">'+row.ProductName+'</td>\
//                <td style="white-space: nowrap;">'+row.Description+'</td>\
//                <td style="white-space: nowrap;">' + (row.Quantity) + '</td>\
//                <td style="white-space: nowrap;"><input type="number" max="'+row.Quantity+'" onfocus="onFocusQty()" onblur="onLeaveQty(' + row.ProductId + ')" style="width:50px;" id="qty' + row.ProductId + '" value="0"/></td>\
//                <td style="white-space: nowrap;">' + findUnit(row.UnitId).unitName + '</td>\
//                <td style="white-space: nowrap;">'+ findStore(+row.StoreId).godownName+'</td>\
//                <td style="white-space: nowrap;">' + findRack(row.RackId).rackName == undefined ? "NA" : findRack(row.RackId).rackName + '</td>\
//                <td style="white-space: nowrap;">' + findBatch(row.BatchId).batchNo == undefined ? "NA" : findBatch(row.BatchId).batchNo + '</td>\
//                <td style="white-space: nowrap; display:none;">' + row.Rate + '</td>\
//                <td style="white-space: nowrap; display:none;">' + row.Amount + '</td>\
//                <td style="white-space: nowrap; display:none;"><button onclick="removeLineItem(' + row.ProductId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>\
//            </tr>\
//            ';
//        totalLineItemAmount += (row.Quantity * row.Amount);

//    });
//    //$("#totalAmount").html(totalLineItemAmount.toFixed(2));
//    console.log(output);
//    $("#lineItemTbody").html(output);
//}

function renderDeliveryNoteLineItems() {
    totalLineItemAmount = 0;
    var output = "";
    //console.log("b4 rendering", deliveryNoteLineItems);
    $.each(deliveryNoteLineItems, function (count, row) {
        var img = row.ProductId + ".jpg";
        var inputQty = ".itemQty_" + row.SL;
        var functionStr =
            output +=
            '<tr>\
                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                <td style="white-space: nowrap;"><button onclick="confirmDelete(' + count + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
                <td style="white-space: nowrap;"><img style="width:40px;height:40px;" class="img-md img-circle" src="/Content/ProductImages/' + img + '" onError="this.src =\'/Content/ProductImages/default.jpg\'" alt="Sample Image"></td>\
                <td style="white-space: nowrap;">'+ row.ProductBarcode + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductCode + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductName + '</td>\
                <td style="white-space: nowrap;">'+ row.Description + '</td>\
                <td style="white-space: nowrap;">' + (row.Quantity) + '</td>\
                <td style="white-space: nowrap;"><input type="number" max="'+ row.Quantity + '" onfocus="onFocusQty()" class="itemQty_' + row.SL + ' itemDel" ' + `onchange="editLineItemQty(${row.SL}, '${inputQty}')"` + '  onblur="onLeaveQty(' + row.ProductId + ')" C style="width:50px;" id="qty' + row.ProductId + '"value="' + row.Quantity + '"/></td>\
                <td style="white-space: nowrap;"><span id="remaining'+ row.ProductId + '"></span></td>\
                <td style="white-space: nowrap;">' + findUnit(row.UnitId).unitName + '</td>\
                <td style="white-space: nowrap;">' + (findStore(+row.StoreId).godownName == undefined ? "NA" : findStore(+row.StoreId).godownName) + '</td>\
                <td style="white-space: nowrap; display:none;">' + row.Rate + '</td>\
                <td style="white-space: nowrap; display:none;">' + row.Amount + '</td>\
            </tr>\
            ';

        totalLineItemAmount += (row.Quantity * row.Rate);

    });
    $("#totalAmount").html(totalLineItemAmount.toFixed(2));
    $("#lineItemTbody").html(output);
}

var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the positive lable as key
    //...
};

function confirmDelete(index) {
    deleteIndex = index;
    eModal.confirm(options).then(removeLineItem);
}

function removeLineItem() {
    deliveryNoteLineItems.splice(deleteIndex, 1);
    renderDeliveryNoteLineItems();
}

function validateQtyToDeliver() {
    var inputs = $.find(".itemDel");

    for (var i of inputs) {
        var value = $(i).val();
        if (value <= 0) {
            Utilities.Alert("Quantity to delivery must be above 0");
            return false;
        }
    }

    if ($("#customers").val() == "") {
        Utilities.Alert("Select a Customer!");
        return false;
    }
    return true;
}
function onLeaveQty(productId) {
    //console.log("b4 update", deliveryNoteLineItems);
    $.each(deliveryNoteLineItems, function (count, row) {
        //console.log("prod", $("#qty" + productId).val());
        //console.log("extra1",row.Extra1);
        if (row.ProductId == productId) {
            if ($("#qty" + productId).val() > row.Quantity) //prevent delivering more than what was ordered
            {
                $("#qty" + productId).val(0);
                $("#remaining" + row.ProductId).html("");
                Utilities.ErrorNotification("You can't deliver more than quantity ordered!");
                return;
            }
            else {
                //row.Quantity = parseFloat($("#qty" + productId).val());
                row.Extra2 = (row.Quantity - parseFloat($("#qty" + productId).val()));
                $("#remaining" + row.ProductId).html(row.Extra2);
                return;
            }
        }
    });
    //console.log("after update", deliveryNoteLineItems);
}

function updateLineItemsQuantity() {
    $.each(deliveryNoteLineItems, function (count, row) {
        row.Quantity = parseFloat($("#qty" + row.ProductId).val());
    });
    //console.log("lakaaa", deliveryNoteLineItems);
}

function onFocusQty() {
    // console.log("on focus", deliveryNoteLineItems);
}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}

function getDeliveryNoteMasterLastRowDetails() {
    console.log(4658);
    // Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/DeliveryNote/GetDeliveryNoteMasterLastRowIndex",
        type: "POST",
        contentType: "application/json",
        success: function (data) {
            console.log(data); //return;
            $("#invoiceNo").val(data);

        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}