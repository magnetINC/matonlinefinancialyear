﻿var userPriviledges = [];
var companyCount = 0;
var generalJournalCount = 0;
var mastersCount = 0;
var accountCount = 0;
var settingsCount = 0;
var agentCycleCount = 0;
var agentCycleRegisterCount = 0;
var supplierCycleCount = 0;
var supplierCycleRegisterCount = 0;
var inventoryCount = 0;
var itemComponentCount = 0;
var reportsCount = 0;
var OtherReportsCount = 0;
var inventoryReportCount = 0;

$(function () {
    $(".hide").hide();  //hide all controls with 'hide' class and show neccessary controls based on usage
   // $("#mainnav-menu").hide();
});

var PriviledgeSettings =
    {
        ApplyUserPriviledge:function()
        {
            if( userRoleId == 1)
            {
                $(".hide").removeClass("hide");
                $(".hide").show();
                //$("#mainnav-menu").show();
                //var k = $('#agentCycleChilds > li:visible').length;
                //alert(k);
            }
            else
            {
                $.ajax({
                    url: API_BASE_URL + "/RolePriviledgeSettings/LoadSettings?RoleId="+userRoleId,
                    type: "GET",
                    contentType: "application/json",
                    success: function (data) {

                        var priviledges = data;
                        userPriviledges = [];

                        $.each(priviledges, function (count, row) {
                            if (row.roleId == userRoleId) {
                                userPriviledges.push(row);
                            }
                        });

                        console.log(userPriviledges);
                        var specialFormNames = ["frmSalesOrder", "frmDeliveryNote", "frmSalesInvoice", "frmSalesReturn", "frmReceipts", "frmPos", "frmPurchaseOrder", "frmMaterialReceipt",
                                                "frmRejectionOut", "frmPurchaseInvoice", "frmPurchaseReturn", "frmSalaryPackageCreation", "frmEmployeeCreation", "frmAdvancePayment",
                                                "frmBonusDeduction", "frmMonthlySalaryVoucher", "frmProductCreation", "frmStandardRate", "frmStockJournal", "frmBankTransfer"];
                        $.each(userPriviledges, function (count, row) {
                            
                            //$("." + row.formName + "-" + row.action).removeClass("hide");
                            //$("." + row.formName + "-" + row.action).show();
                            if (specialFormNames.includes(row.formName)) {
                                if (row.action === "Save") {
                                    $("#" + row.formName).removeClass("hide");
                                    $("#" + row.formName).show();
                                }
                            }
                            else {
                                $("#" + row.formName).removeClass("hide");
                                $("#" + row.formName).show();
                            }
                           
                            //if (row.formName + "-" + row.action == "frmGeneralJournal-View")
                            //{
                            //    generalJournalCount = generalJournalCount + 1;
                            //}
                            //if (row.formName + "-" + row.action == "frmAccountGroup-View" || row.formName + "-" + row.action == "frmAccountLedger-View")
                            //{
                            //    accountCount = accountCount + 1;
                            //}
                            //if (row.formName + "-" + row.action == "frmCity-View" || row.formName + "-" + row.action == "frmState-View")
                            //{
                            //    mastersCount = mastersCount + 1;
                            //}
                            //if (row.formName + "-" + row.action == "frmUserCreation-View" || row.formName + "-" + row.action == "frmSettings-View" + "-" + row.action || row.formName + "-" + row.action == "frmRole-View" || row.formName + "-" + row.action == "frmRolePriviledgeSettings-View")
                            //{
                            //    settingsCount = settingsCount + 1;
                            //}
                            //agent cycle
                            //if (row.formName == "frmSalesQuotationRegister" || row.formName == "frmSalesOrderRegister" || row.formName == "frmDeliveryNoteRegister"
                            //    || row.formName == "frmRejectionInReport" || row.formName == "frmPdcReceivableReport" || row.formName == "frmReceiptReport"
                            //    || row.formName == "frmCreditNoteReport" )
                            //{
                            //    agentCycleRegisterCount = agentCycleRegisterCount + 1;
                            //}
                            if (row.formName == "frmGeneralJournal" || row.formName == "frmStockJournalRegister" || row.formName == "frmAccountGroup" || row.formName == "frmAccountLedger" ||
                               row.formName == "frmState" || row.formName == "frmCity" || row.formName == "frmBudgetSettings" || row.formName == "frmBudgetVariance" ||
                               row.formName == "frmUserCreation" || row.formName == "frmSettings" || row.formName == "frmRole" || row.formName == "frmRolePriviledgeSettings" ||
                                row.formName == "frmRole" || row.formName == "frmFinancialYearSettings" || row.formName == "frmUserCreation" || row.formName == "frmRole" || row.formName == "frmRolePriviledgeSettings")
                            {
                                $("#companyParent").removeClass("hide");
                                $("#companyParent").show();
                            }
                            if (row.formName == "frmGeneralJournal" || row.formName == "frmJournalRegister")
                            {
                                $("#generalJournalParent").removeClass("hide");
                                $("#generalJournalParent").show();
                            }
                            if (row.formName == "frmAccountGroup" || row.formName == "frmAccountLedger" || row.formName == "frmState" ||
                                row.formName == "frmCity" || row.formName == "frmBudgetSettings" || row.formName == "frmBudgetVariance")
                            {
                                $("#mastersParent").removeClass("hide");
                                $("#mastersParent").show();
                            }
                            if (row.formName == "frmAccountGroup" || row.formName == "frmAccountLedger")
                            {
                                $("#accountParent").removeClass("hide");
                                $("#accountParent").show();
                            }
                            if (row.formName == "frmUserCreation" || row.formName == "frmRole" || row.formName == "frmRolePriviledgeSettings") {
                                $("#settingsParent").removeClass("hide");
                                $("#settingsParent").show();
                            }
                            if (row.formName == "frmBudget" || row.formName == "frmBudgetVariance")
                            {
                                $("#budgetParent").removeClass("hide");
                                $("#budgetParent").show();
                            }
                            if(row.formName == "frmCustomer" || row.formName == "frmTransactions" || row.formName == "frmDeliveryNote" || row.formName == "frmRejectionIn" ||
                                row.formName == "frmSalesInvoice" || row.formName == "frmSalesReturn" || row.formName == "frmReceipt" || row.formName == "frmCustomerRegister" ||
                                row.formName == "frmSalesOrderRegister" || row.formName == "frmDeliveryNoteRegister" || row.formName == "frmRejectionInRegister" ||
                                row.formName == "frmSalesInvoiceRegister" || row.formName == "frmBankSalesReturnRegister" || row.formName == "frmReceiptRegister")
                            {
                                $("#customerCycleParent").removeClass("hide");
                                $("#customerCycleParent").show();
                            }
                            if (row.formName == "frmSalesOrderRegister" || row.formName == "frmDeliveryNoteRegister" || row.formName == "frmRejectionInRegister" ||
                                row.formName == "frmSalesInvoiceRegister" || row.formName == "frmBankSalesReturnRegister" || row.formName == "frmReceiptRegister")
                            {
                                $("#customerCycleRegister").removeClass("hide");
                                $("#customerCycleRegister").show();
                            }
                            if (row.formName == "frmPos" || row.formName == "frmPendingSales")
                            {
                                $("#posParent").removeClass("hide");
                                $("#posParent").show();
                            }
                            if (row.formName == "frmSupplier" || row.formName == "frmPurchaseOrder" || row.formName == "frmMaterialReceipt" || row.formName == "frmRejectionOut" ||
                                row.formName == "frmPurchaseInvoice" || row.formName == "frmPurchaseReturn" || row.formName == "frmPayment" || row.formName == "frmAccruedExpenses" ||
                                row.formName == "frmPurchaseOrderRegister" || row.formName == "frmMaterialReceiptRegister" || row.formName == "frmRejectionOutRegister" ||
                                row.formName == "frmPurchaseInvoiceRegister" || row.formName == "frmPurchaseReturnRegister" || row.formName == "frmPaymentRegister") {
                                $("#supplierCycleParent").removeClass("hide");
                                $("#supplierCycleParent").show();
                            }
                            if (row.formName == "frmPurchaseOrderRegister" || row.formName == "frmMaterialReceiptRegister" || row.formName == "frmRejectionOutRegister" ||
                                row.formName == "frmPurchaseInvoiceRegister" || row.formName == "frmPurchaseReturnRegister" || row.formName == "frmPaymentRegister")
                            {
                                $("#supplierCycleRegisterParent").removeClass("hide");
                                $("#supplierCycleRegisterParent").show();
                            }
                            if (row.formName == "frmDesignation" || row.formName == "frmPayElement" || row.formName == "frmSalaryPackageCreation" || row.formName == "frmEmployeeCreation" ||
                                row.formName == "frmHolidaySettings" || row.formName == "frmMonthlySalarySettings" || row.formName == "frmAttendance" || row.formName == "frmAdvancePayment" ||
                                row.formName == "frmBonusDeduction" || row.formName == "frmMonthlySalarySettings" || row.formName == "frmPaySlip" ||
                                row.formName == "frmSalaryPackageRegister" || row.formName == "frmEmployeeRegister" || row.formName == "frmAdvanceRegister" || 
                                row.formName == "frmBonusDeductionRegister" || row.formName == "frmMonthlySalaryRegister" || row.formName == "frmDailySalaryRegister")
                            {
                                $("#payrollParent").removeClass("hide");
                                $("#payrollParent").show();
                            }
                            if (row.formName == "frmSalaryPackageRegister" || row.formName == "frmEmployeeRegister" || row.formName == "frmAdvanceRegister" ||
                                row.formName == "frmBonusDeductionRegister" || row.formName == "frmMonthlySalaryRegister" || row.formName == "frmDailySalaryRegister")
                            {
                                $("#payrollRegisterParent").removeClass("hide");
                                $("#payrollRegisterParent").show();
                            }
                            if (row.formName == "frmStockCard" || row.formName == "frmProductGroup" || row.formName == "frmProductCreation" || row.formName == "frmProductBatch" ||
                               row.formName == "frmBrand" || row.formName == "frmSize" || row.formName == "frmUnit" || row.formName == "frmStore" || row.formName == "frmRack" ||
                                row.formName == "frmPriceLevel" || row.formName == "frmPriceList" || row.formName == "frmStandardRate" || row.formName == "frmStockJournal" ||
                                row.formName == "frmStockJournalConfirmation" || row.formName == "frmStockTransferIn" || row.formName == "frmStockJournalRegister")
                            {
                                $("#inventoryParent").removeClass("hide");
                                $("#inventoryParent").show();
                            }
                            if (row.formName == "frmProductBatch" || row.formName == "frmBrand" || row.formName == "frmSize" ||
                                row.formName == "frmUnit" || row.formName == "frmStore" || row.formName == "frmRack")
                            {
                                $("#inventoryComponentParent").removeClass("hide");
                                $("#inventoryComponentParent").show();
                            }
                            if (row.formName == "frmPricingLevel" || row.formName == "frmPriceList" || row.formName == "frmStandardRate")
                            {
                                $("#pricingListComponentParent").removeClass("hide");
                                $("#pricingListComponentParent").show();
                            }
                            if (row.formName == "frmStockJournal" || row.formName == "frmStockJournalConfirmation" || row.formName == "frmStockTransferIn" ||
                                row.formName == "frmStockJournalRegister" || row.formName == "frmFarmActivityReport")
                            {
                                $("#stockTransferComponentParent").removeClass("hide");
                                $("#stockTransferComponentParent").show();
                            }
                            if (row.formName == "frmBankTransfer" || row.formName == "frmBankReconcilliation")
                            {
                                $("#bankingParent").removeClass("hide");
                                $("#bankingParent").show();
                            }
                            if (row.formName == "frmTaxReport" || row.formName == "frmVatReturnsReport" || row.formName == "frmChequeOrCashDetailsReport" || row.formName == "frmCashorBankDetailsReport" ||
                                row.formName == "frmBankTransferDetailsReport" || row.formName == "frmInventoryStatisticsReport" || row.formName == "frmStockVarianceReport" || row.formName == "frmInventoryMovementReport" ||
                                row.formName == "frmPhysicalStockReport" || row.formName == "frmStockSummaryReport" || row.formName == "frmStockDetailsReport" || row.formName == "frmCustomerBalanceSummaryReport" ||
                                row.formName == "frmARAgeingSummaryReport" || row.formName == "frmSalesInvoiceReport" || row.formName == "frmSalesQuotationReport" || row.formName == "frmSalesOrderReport" ||
                                row.formName == "frmSalesReturnReport" || row.formName == "frmDeliveryNoteReport" || row.formName == "frmRejectionInReport" || row.formName == "frmReceiptReport" ||
                                row.formName == "frmPurchaseInvoiceReport" || row.formName == "frmAgeingReport" || row.formName == "frmSupplierBalanceSummaryReport" || row.formName == "frmPurchaseOrderReport" ||
                                row.formName == "frmMaterialReceiptReport" || row.formName == "frmRejectionOutReport" || row.formName == "frmPurchaseReturnReport" || row.formName == "frmPaymentReport" ||
                                row.formName == "frmBankRegisterReport" || row.formName == "frmAgentBalanceSummaryReport" || row.formName == "frmAgentBalanceSummaryReport" || row.formName == "frmAccountLedgerReport" ||
                                row.formName == "frmAccountGroupReport" || row.formName == "frmAccountListReport" || row.formName == "frmAdvancePaymentReport" || row.formName == "frmBonusDeductionReport" ||
                                row.formName == "frmDailyAttendanceReport" || row.formName == "frmDailySalaryReport" || row.formName == "frmEmployeeReport" || row.formName == "frmPayHeadReport" ||
                                row.formName == "frmSalaryPackageDetailsReport" || row.formName == "frmSalaryPackageReport" || row.formName == "frmMonthlySalaryReport" || row.formName == "frmEmployeeAddressReport" ||
                                row.formName == "frmMonthlyAttendanceReport" || row.formName == "frmCustomerBalanceSummaryReport" || row.formName == "frmSupplierBalanceSummaryReport" ||
                                row.formName == "frmFinancialStatementTrialBalance" || row.formName == "frmFinancialStatementBalanceSheet" || row.formName == "frmFinancialStatementProfitAndLoss")
                            {
                                $("#reportParent").removeClass("hide");
                                $("#reportParent").show();
                            }
                            if (row.formName == "frmTaxReport" || row.formName == "frmVatReturnsReport")
                            {
                                $("#generalReportParent").removeClass("hide");
                                $("#generalReportParent").show();
                            }
                            if (row.formName == "frmChequeOrCashDetailsReport" || row.formName == "frmCashorBankDetailsReport" || row.formName == "frmBankTransferDetailsReport")
                            {
                                $("#bankReportParent").removeClass("hide");
                                $("#bankReportParent").show();
                            }
                            if (row.formName == "frmInventoryStatisticsReport" || row.formName == "frmStockVarianceReport" || row.formName == "frmInventoryMovementReport" ||
                                row.formName == "frmPhysicalStockReport" || row.formName == "frmStockSummaryReport" || row.formName == "frmStockDetailsReport" || 
                                row.formName == "frmCategory" || row.formName == "frmProject" || row.formName == "frmFarmBuild" || row.formName == "frmFarmActivityReport") {
                                $("#inventoryReportParent").removeClass("hide");
                                $("#inventoryReportParent").show();
                            }
                            if (row.formName == "frmCustomerBalanceSummaryReport" || row.formName == "frmARAgeingSummaryReport" || row.formName == "frmSalesInvoiceReport" ||
                                row.formName == "frmSalesQuotationReport" || row.formName == "frmSalesOrderReport" || row.formName == "frmSalesReturnReport" ||
                                row.formName == "frmDeliveryNoteReport" || row.formName == "frmRejectionInReport" || row.formName == "frmReceiptReport"
                                || row.formName == "frmCustomerBalanceSummaryReport") {
                                $("#customerReportParent").removeClass("hide");
                                $("#customerReportParent").show();
                            }
                            if (row.formName == "frmSalesQuotationReport" || row.formName == "frmSalesOrderReport" || row.formName == "frmSalesReturnReport" || 
                                row.formName == "frmDeliveryNoteReport" || row.formName == "frmRejectionInReport" || row.formName == "frmReceiptReport") {
                                $("#customerTransactionParent").removeClass("hide");
                                $("#customerTransactionParent").show();
                            }
                            if (row.formName == "frmPurchaseInvoiceReport" || row.formName == "frmAgeingReport" || row.formName == "frmSupplierBalanceSummaryReport" || row.formName == "frmPurchaseOrderReport" ||
                                row.formName == "frmMaterialReceiptReport" || row.formName == "frmRejectionOutReport" || row.formName == "frmPurchaseReturnReport" || row.formName == "frmPaymentReport" ||
                                row.formName == "frmSupplierBalanceSummaryReport") {
                                $("#supplierReportParent").removeClass("hide");
                                $("#supplierReportParent").show();
                            }
                            if (row.formName == "frmPurchaseOrderReport" || row.formName == "frmMaterialReceiptReport" || row.formName == "frmRejectionOutReport" ||
                                row.formName == "frmPurchaseReturnReport" || row.formName == "frmPaymentReport")
                            {
                                $("#supplierTransactionParent").removeClass("hide");
                                $("#supplierTransactionParent").show();
                            }
                            if (row.formName == "frmFinancialStatementTrialBalance" || row.formName == "frmFinancialStatementBalanceSheet" || row.formName == "frmFinancialStatementProfitAndLoss")
                             {
                                 $("#financialStatementParent").removeClass("hide");
                                 $("#financialStatementParent").show();
                                }
                            if (row.formName == "frmAccountLedgerReport" || row.formName == "frmAccountGroupReport")
                            {
                                $("#accountReportParent").removeClass("hide");
                                $("#accountReportParent").show();
                            }
                            if (row.formName == "frmAccountListReport")
                            {
                                $("#listReportParent").removeClass("hide");
                                $("#listReportParent").show();
                            }
                            if (row.formName == "frmAdvancePaymentReport" || row.formName == "frmBonusDeductionReport" || row.formName == "frmDailyAttendanceReport" || row.formName == "frmDailySalaryReport" ||
                                row.formName == "frmEmployeeReport" || row.formName == "frmPayHeadReport" || row.formName == "frmSalaryPackageDetailsReport" || row.formName == "frmSalaryPackageReport" ||
                                row.formName == "frmMonthlySalaryReport" || row.formName == "frmEmployeeAddressReport" || row.formName == "frmMonthlyAttendanceReport")
                            {
                                $("#payrollReportParent").removeClass("hide");
                                $("#payrollReportParent").show();
                            }

                            $("#frmRolePriviledgeSettings").addClass("hide");
                            $("#frmRolePriviledgeSettings").hide();
                            
                            
                            //if (row.formName == "frmCustomer" || row.formName == "frmSalesQuotation" || row.formName == "frmSalesOrder"
                            //     || row.formName == "frmSalesOrderAuthorization" || row.formName == "frmReleaseForm" || row.formName == "frmSalesOrder"
                            //    || row.formName == "frmRejectionIn" || row.formName == "frmSalesInvoice" || row.formName == "frmReceipts")
                            //{
                            //    agentCycleCount = agentCycleCount + 1;
                            //}

                            //if (row.formName == "frmSupplier" || row.formName == "frmPurchaseOrder" || row.formName == "frmPurchaseOrderListing"
                            //    || row.formName == "frmMaterialReceiptApproval" || row.formName == "frmRejectionOut" || row.formName == "frmPurchaseInvoiceListing"
                            //    || row.formName == "frmPurchaseInvoiceApproval")
                            //{
                            //    supplierCycleCount = supplierCycleCount + 1;
                            //}

                            //if(row.formName=="frmPurchaseOrderRegister"|| row.formName=="frmMaterialReceiptRegister" || row.formName=="frmRejectionOutRegister"
                            //    || row.formName=="")
                            //{
                            //    supplierCycleRegisterCount = supplierCycleRegisterCount + 1;
                            //}

                            //if (row.formName == "frmProductBatch" || row.formName == "frmBrand" || row.formName == "frmModalNo"
                            //    || row.formName == "frmSize" || row.formName == "frmUnit" || row.formName == "frmStore"
                            //    || row.formName == "frmRack")
                            //{
                            //    itemComponentCount = itemComponentCount + 1;
                            //}

                            //if (row.formName == "frmProductGroup" || row.formName == "frmProductCreation" || row.formName == "frmProductBatch"
                            //    || row.formName == "frmBrand" || row.formName == "frmModalNo" || row.formName == "frmSize"
                            //    || row.formName == "frmUnit" || row.formName == "frmStore" || row.formName == "frmRack"
                            //    || row.formName == "frmPhysicalStock" || row.formName == "frmStockJournal")
                            //{
                            //    inventoryCount = inventoryCount + 1;
                            //}

                            //if(row.formName=="frmStockJournalReport" || row.formName=="frmStockVarianceReport" || row.formName=="frmInventoryMovementReport"
                            //    ||row.formName=="frmPhysicalStockReport" || row.formName=="frmStockSummaryReport" || row.formName=="frmStockDetailsReport")
                            //{
                            //    reportsCount = reportsCount + 1;
                            //}
                        });

                        //$(".hide").hide();
                        //var k = $('#agentCycleChilds > li:visible').length;
                        //alert(k);
                        //if(generalJournalCount<1)
                        //{
                        //    $("#generalJournalParent").hide();
                        //}
                        //if(accountCount<1)
                        //{
                        //    $("#accountParent").hide();
                        //}
                        //if(mastersCount<1 && accountCount<1)
                        //{
                        //    $("#mastersParent").hide();
                        //}
                        //if(settingsCount<1)
                        //{
                        //    $("#settingsParent").hide();
                        //}
                        //if(generalJournalCount<1 && accountCount<1 && mastersCount<1 && settingsCount<1)
                        //{
                        //    $("#companyParent").hide();
                        //}
                        //if(agentCycleRegisterCount<1)
                        //{
                        //    $("#agentCycleRegisterParent").hide();
                        //}
                        //if(agentCycleCount<1 && agentCycleRegisterCount<1)
                        //{
                        //    $("#agentCycleParent").hide();
                        //}
                        //if(supplierCycleCount<1)
                        //{
                        //    $("#supplierCycleRegisterParent").hide();
                        //}
                        //if (supplierCycleCount < 1 && supplierCycleRegisterCount<1)
                        //{
                        //    $("#supplierCycleParent").hide();
                        //}
                        //if(itemComponentCount<1)
                        //{
                        //    $("#inventoryParent").hide();
                        //}
                        //if(itemComponentCount<1 && inventoryCount<1)
                        //{
                        //    $("#inventoryComponentParent").hide();
                        //}
                        //if(reportsCount<1)
                        //{
                        //    $("#reportParent").hide();
                        //}

                       // $("#mainnav-menu").show();
                    },
                    error: function (err) {

                    }
                });
            }

            //console.log(userPriviledges);
            //this.LoadPriviledge();
            
        }
    };

function checkPriviledge(formName, action) {
    var checkPriviledge = userPriviledges.find(p => p.formName == formName && p.action == action);
    if (checkPriviledge == undefined && userRoleId != 1) {
        return false;
    }
    else {
        return true;
    }
}