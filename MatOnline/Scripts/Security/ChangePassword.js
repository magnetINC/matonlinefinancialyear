﻿$(function () {
    $("#changePassword").click(function () {
        var pwd = $("#newPassword").val();
        var confirmPwd = $("#confirmPassword").val();

        if(pwd=="")
        {
            Utilities.ErrorNotification("Please enter password!");
            return;
        }

        if (confirmPwd == "")
        {
            Utilities.ErrorNotification("Please confirm password!");
            return;
        }

        if(pwd!=confirmPwd)
        {
            Utilities.ErrorNotification("Password confirmation mismatch!");
            return;
        }
        else
        {
            Utilities.Loader.Show();
            var userInfo = {
                Password: pwd,
                UserId: matUserInfo.UserId
            };
            $.ajax({
                url: API_BASE_URL + "/UserCreation/ChangePassword",
                type: 'POST',
                data: JSON.stringify(userInfo),
                contentType: "application/json",
                success: function (data) {
                    if(data==true)
                    {
                        Utilities.SuccessNotification("Password changed!");
                    }
                    else
                    {
                        Utilities.ErrorNotification("Please try again!");
                    }
                    $("#newPassword").val("");
                    $("#confirmPassword").val("");
                    Utilities.Loader.Hide();
                },
                error: function (request, error) {
                    Utilities.Loader.Hide();
                }
            });
        }
    });
});