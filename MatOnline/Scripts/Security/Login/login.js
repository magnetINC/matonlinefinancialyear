﻿var loginResponse = {};
var reqObj = {
    username: "",
    password:""
            };

$(function () {
    $("#btnSignIn").click(function () {
        login();
    });
    
    $("#frmLogin").submit(function (e) {        
        e.preventDefault();
    });
});

function keyPress(e) {
    if (e.keyCode === 13) {
        login();
    }
}

function login()
{
    Utilities.Loader.Show();
    $("#btnSignIn").html('Please wait...');
    $("#btnSignIn").prop("disabled", "disabled");

    reqObj.username = $("#username").val();
    reqObj.password = $("#password").val();

    $.ajax({
        url: API_BASE_URL + "/Login/Login",
        type: 'POST',
        data: JSON.stringify(reqObj),
        dataType: 'json',
        contentType:"application/json",
        success: function (data)
        {
            if(data != null)
            {
                if(data.UserId > 0)
                {
                    if (data.IsActive == false)
                    {
                        $("#btnSignIn").html('Authenticate');
                        $("#btnSignIn").prop("disabled", "");
                        $("#msg").html(
                            '<div class="alert alert-danger alert-dismissible">\
                            <h5><i class="icon fa fa-times-circle"></i>Sorry,your account is inactive.<br/>Kindly contact your system administrator.</h5>\
                        </div>\
                        ');
                        Utilities.Loader.Hide();
                    }
                    else
                    {
                        localStorage.setItem("MAT_USER_INFO", JSON.stringify(data));
                        $("#userId").val(data.UserId);  //sets control of id to be passed to controller for further processing
                       // $("#frmLogin").unbind('submit').submit();   //submits the form so user id is passed to the mvc controller to enable cookie creation 
                        var host = location.origin;

                        var dataH = JSON.stringify(data);
                        var obj = { "data": dataH };
                        $.ajax(host + "/Security/Login/SetFinancialYearSession",
                            {
                                type: 'post',
                                data: obj,
                                success: function (Datares) {

                                    if (Datares == "True")
                                    {
                                        $.ajax(host + "/Security/Login/ProcessLogin",
                                            {
                                                type: 'post',
                                                data: { userId: data.UserId },
                                                success: function (res)
                                                {
                                                    $.ajax(host + "/Security/Login/CheckCurrentFinancialYear",
                                                        {
                                                            type: 'post',
                                                            data: obj,
                                                            success: function (dataq) {

                                                                Utilities.Loader.Hide();

                                                                if (dataq.IsCurrenty)
                                                                {
                                                                    Swal.fire({
                                                                        title: 'Financial Year Login',
                                                                        text: "You're not logging into the current financial year. Do you want to continue with this year " + dataq.FinancialDate,
                                                                        type: 'warning',
                                                                        showCancelButton: true,
                                                                        confirmButtonColor: '#3085d6',
                                                                        cancelButtonColor: '#d33',
                                                                        confirmButtonText: 'Continue'
                                                                    }).then((result) => {

                                                                        if (result.value) {

                                                                            var req = host + res;
                                                                            console.log(req);
                                                                            location.href = req;

                                                                        }
                                                                        else {
                                                                            location.reload();
                                                                        }
                                                                    });
                                                                }
                                                                else {
                                                                    var req = host + res;
                                                                    console.log(req);
                                                                    location.href = req;
                                                                }
                                                            }
                                                        });
                                                  
                                                }
                                            });
                                    }
                                    else
                                    {
                                        $("#btnSignIn").html('Authenticate');
                                        $("#btnSignIn").prop("disabled", "");
                                        $("#msg").html('<div class="alert alert-danger alert-dismissible">\
                            <i class="icon fa fa-times-circle"></i> Invalid login credentials.\
                        </div>\
');
                                        Utilities.Loader.Hide();
                                    }
                                }
                            });

                    }                    
                }
                else
                {
                    $("#btnSignIn").html('Authenticate');
                    $("#btnSignIn").prop("disabled", "");
                    $("#msg").html(
                        '<div class="alert alert-danger alert-dismissible">\
                            <i class="icon fa fa-times-circle"></i> Invalid login credentials.\
                        </div>\
                        ');
                    Utilities.Loader.Hide();
                }
            }

        },
        error: function (request, error) {
            $("#btnSignIn").html('Authenticate');
            $("#btnSignIn").prop("disabled", "");
            //alert("Request: " + JSON.stringify(request));
            Utilities.Loader.Hide();
        }
    });
}