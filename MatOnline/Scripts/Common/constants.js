﻿$(function () {
});

/* Localhost uri */
var API_BASE_URL = "http://localhost:30170";

/* test deployment uri */
//var API_BASE_URL = "https://testapi.matonlineapp.com"; //testapi.matonlineapp.com

/* atorj deployment uri */
//var API_BASE_URL = "https://atorjapi.matonlineapp.com";

/* kasco deployment uri */
//var API_BASE_URL = "https://kascoapi.matonlineapp.com";
//var API_BASE_URL = "https://161.97.160.238:3789"

/* hisfarm deployment uri */
//var API_BASE_URL = "https://hisfarmapi.matonlineapp.com";

/* nordica deployment uri */
//var API_BASE_URL = "https://nordicaapi.matonlineapp.com";

// var API_BASE_URL = "https://45.40.135.165:3789";

var k = {
    "ProductInfo":
        {
            "ProductName":"test object3",
            "ProductCode":"140090",
            "PurchaseRate":"2000",
            "SalesRate":"4000",
            "Mrp":"NULL",
            "MaximumStock":"60",
            "MinimumStock":"20",
            "ReorderLevel":"10",
            "TaxId":"10011",
            "UnitId":"30006",
            "GroupId":"10047",
            "ProductType":"Product",
            "SalesAccount":"10",
            "EffectiveDate":"2018-01-21",
            "ExpenseAccount":"58",
            "TaxapplicableOn":"MRP",
            "BrandId":"12",
            "SizeId":"6",
            "ModelNoId":"null",
            "GodownId":"8",
            "RackId":"10009",
            "IsallowBatch":"False",
            "IsBom":"False",
            "PartNo":"0",
            "Isopeningstock":"True",
            "Ismultipleunit":"False",
            "IsActive":false,
            "Extra1":"",
            "Extra2":"manufac",
            "IsshowRemember":false,
            "Narration":"desc",
            "barcode":""
        },
    "NewStores":
        [
            {
                "StoreId":"8",
                "RackId":"10009",
                "Quantity":"20",
                "Rate":"2000",
                "UnitId":"30006",
                "Amount":40000
            },
            {
                "StoreId":"7",
                "RackId":"10008",
                "Quantity":"15",
                "Rate":"2000",
                "UnitId":"30006",
                "Amount":30000
            }
        ],        
    "AutoBarcode":false,
    "IsSaveBomCheck":false,
    "IsSaveMultipleUnitCheck":false,
    "IsOpeningStock":true,
    "IsBatch": false

} 

var reportType =
{
    'salesInvoice': 1, 
    'salesOrder': 2,
    'deliveryNote': 3,
    'rejectionIn': 4,
    'receipt': 5,
    'generalJournal': 6,
    'pettyExpenses':7
}  

