﻿var settings =
    {
        LoadSettings:function()
        {
            $.ajax({
                url: API_BASE_URL + "/Settings/LoadSettings",
                type: "GET",
                contentType: "application/json",
                success: function (data) {
                    settings.SettingsObject = data;
                    settings.ApplySettings();
                },
                error: function (err) {
                }
            });
        },
        SettingsObject: {},
        ApplySettings:function()
        {
            console.log("applying settings...");
            var st = settings.SettingsObject;
            if(st[9].status=="Yes")
            {
                $(".Settings_Barcode").show();
            }
            else
            {
                $(".Settings_Barcode").hide();
            }
            if (st[10].status == "Yes") {
                $(".Settings_AllowBatch").show();
            }
            else {
                $(".Settings_AllowBatch").hide();
            }
            if (st[11].status == "Yes") {
                $(".Settings_AllowSize").show();
            }
            else {
                $(".Settings_AllowSize").hide();
            }
            if (st[12].status == "Yes") {
                $(".Settings_AllowModelNo").show();
            }
            else {
                $(".Settings_AllowModelNo").hide();
            }
            if (st[15].status == "Yes") {
                $(".Settings_ShowSalesRate").show();
            }
            else {
                $(".Settings_ShowSalesRate").hide();
            }
            if (st[16].status == "Yes") {
                $(".Settings_ShowMRP").show();
            }
            else {
                $(".Settings_ShowMRP").hide();
            }
            if (st[35].status == "Yes") {
                $(".Settings_ShowPurchaseRate").show();
            }
            else {
                $(".Settings_ShowPurchaseRate").hide();
            }
            if (st[17].status == "Yes") {
                $(".Settings_ShowUnit").show();
            }
            else {
                $(".Settings_ShowUnit").hide();
            }
            if (st[18].status == "Yes") {
                $(".Settings_ShowSize").show();
            }
            else {
                $(".Settings_ShowSize").hide();
            }
            if (st[19].status == "Yes") {
                $(".Settings_ShowModelNo").show();
            }
            else {
                $(".Settings_ShowModelNo").hide();
            }
            if (st[20].status == "Yes") {
                $(".Settings_ShowDiscountAmount").show();
            }
            else {
                $(".Settings_ShowDiscountAmount").hide();
            }
            if (st[21].status == "Yes") {
                $(".Settings_ShowProductCode").show();
            }
            else {
                $(".Settings_ShowProductCode").hide();
            }
            if (st[22].status == "Yes") {
                $(".Settings_ShowBrand").show();
            }
            else {
                $(".Settings_ShowBrand").hide();
            }
            if (st[23].status == "Yes") {
                $(".Settings_ShowDiscountPercent").show();
            }
            else {
                $(".Settings_ShowDiscountPercent").hide();
            }
            if (st[37].status == "Yes") {
                $(".Settings_Category").show();
            }
            else {
                $(".Settings_Category").hide();
            }
            if (st[38].status == "Yes") {
                $(".Settings_ShowWithholdingTax").show();
            }
            else {
                $(".Settings_ShowWithholdingTax").hide();
            }
            
        }
    };
settings.LoadSettings();