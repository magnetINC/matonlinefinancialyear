﻿
function showLoader() {
    $('#page-content').loader('show');
}

function hideLoader() {
    $('#page-content').loader('hide');
}

function countDown(container,startSeconds)
{
    var start = startSeconds+1;
    var x = setInterval(function () {
        --start;
        $("#"+container).html(start);
        if(start==0)
        {
            clearInterval(x);
            $("#successModal").modal("toggle");
        }
        return start;
    }, 1000);

    
}
function validateImage() {
    imgurl = '../../Assets/mat/img/MatOnlineLogo.png';
    imgToBase64(imgurl, function (base64) {
        base64Img = base64;
    });
}

function imgToBase64(src, callback) {
    var outputFormat = src.substr(-3) === 'png' ? 'image/png' : 'image/jpeg';
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function () {
        console.log("sherlock's portriat");
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var dataURL;
        canvas.height = this.naturalHeight;
        canvas.width = this.naturalWidth;
        ctx.drawImage(this, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        callback(dataURL);
        imgurl = '../../Assets/mat/img/MatOnlineLogo.png';
    };
    img.src = src;
    if (img.complete || img.complete === undefined) {
        img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
        img.src = src;
    }
}
var Utilities =
    {
        ErrorNotification: function (msg)
        {
            var autoClose = true;
            var content = '<div class="alert alert-danger media fade in">\
                                <div class="media-left">\
									<span class="icon-wrap icon-wrap-xs icon-circle alert-icon">\
										<i class="fa fa-times fa-lg"></i>\
									</span>\
								</div>\
								<div class="media-body">\
									<h4 class="alert-title">Oops!</h4>\
									<p class="alert-message">' + msg + '</p>\
								</div>\
            				</div>';
            //var content = '<div class="alert alert-danger" role="alert">\
            //                  <h4 class="alert-heading" style="padding:0; margin:0;">Well done!</h4>\
            //                  <span style="word-wrap:break-word;">Aww yeah, you successfully read this important alert messagejdfkdfgkshdgklhsdjgkhasldbg;asdjg;asdig asdgaisdgnasdkgn asdginasdg asdginasdghasidgh asdgiahshdhglkasld ga8sdgha;skdgha sdghasdgkjashduashetw89ehsg asdg88hawep8tphgsad iuasd fjgasdufglasjf ahb8asdgfjasdgofiua <span>\
            //               </div>';

            $.niftyNoty({
                type: "btn-danger",
                container: 'floating',
                html: content,
                timer: autoClose ? 10000 : 0
            });
        },
        SuccessNotification: function (msg) {
            var autoClose = true;
            var content = '<div class="alert alert-success media fade in">\
								<div class="media-left">\
									<span class="icon-wrap icon-wrap-xs icon-circle alert-icon">\
										<i class="fa fa-check fa-lg"></i>\
									</span>\
								</div>\
								<div class="media-body">\
                                    <h4 class="alert-title"></h4>\
									<p class="alert-message">'+ msg + '</p>\
								</div>\
							</div>';

            $.niftyNoty({
                type: "btn-success",
                container: 'floating',
                html: content,
                timer: autoClose ? 3000 : 0
            });
        }, 
        InfoNotification: function (msg) {
            var autoClose = true;
            var content = '<div class="alert alert-info media fade in">\
								<div class="media-left">\
									<span class="icon-wrap icon-wrap-xs icon-circle alert-icon">\
										<i class="fa fa-check fa-lg"></i>\
									</span>\
								</div>\
								<div class="media-body">\
                                    <h4 class="alert-title"></h4>\
									<p class="alert-message">'+ msg + '</p>\
								</div>\
							</div>';

            $.niftyNoty({
                type: "btn-success",
                container: 'floating',
                html: content,
                timer: autoClose ? 3000 : 0
            });
        }, 
          FormatDate: function(date) {
             return Date.parse(date);
          },
        Loader:
            {
                Show:function()
                {
                    $('#page-content').loader('show');
                },
                Hide:function()
                {
                    $('#page-content').loader('hide');
                }
            },
        CountDown:function()
        {
            var start = startSeconds + 1;
            var x = setInterval(function () {
                --start;
                $("#" + container).html(start);
                if (start == 0) {
                    clearInterval(x);
                    $("#successModal").modal("toggle");
                }
                return start;
            }, 1000);
        },
        Alert:function(msg)
        {
           // alert(msg);  
           Swal.fire({
               type: 'info',
               title: 'Notification',
               text: msg
               //footer: '<a href>Why do I have this issue?</a>'
           });
        },
        Confirm: function (msg)
        {
         return confirm(msg); 
          //var resp = false; 
          //Swal.fire({
          //    title: 'Warning',
          //    text: msg,
          //    type: 'warning',
          //    showCancelButton: true,
          //    confirmButtonColor: '#3085d6',
          //    cancelButtonColor: '#d33',
          //    confirmButtonText: 'Continue'
          //}).then((result) => { 
          //    resp = result.value;

          //    if (result.value) {
          //      //  console.log(result.value);
          //       // resp = result.value;
          //        //Swal.fire(
          //        //    "Complete"
          //        //);
          //    }
          //});
          //return resp;
        }, 
    ConfirmFancy: function(msg, confirmFunction, cancelFunction) {
        var resp = false;
        Swal.fire({
            title: 'Warning',
            text: msg,
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Continue'
        }).then((result) => { 
            resp = result.value;
            if (result.value) {
                confirmFunction();
            } else {
                cancelFunction();
            }
        });
        return resp;
    },
        IsValueEmptyOrNull:function(val)
        {
            if(val!="" && val!=null)
            {
                return true;
            }
            return false;
        },
        MonthString: function (num) {
            if (num == "01" || num == "1")
                return "Jan";
            if (num == "02" || num == "2")
                return "Feb";
            if (num == "03" || num == "3")
                return "Mar";
            if (num == "04" || num == "4")
                return "Apr";
            if (num == "05" || num == "5")
                return "May";
            if (num == "06" || num == "6")
                return "Jun";
            if (num == "07" || num == "7")
                return "Jul";
            if (num == "08" || num == "8")
                return "Aug";
            if (num == "09" || num == "9")
                return "Sep";
            if (num == "10")
                return "Oct";
            if (num == "11")
                return "Nov";
            if (num == "12")
                return "Dec";
        },
        FormatJsonDate: function (jsonDate) {
            var datePt = jsonDate.split("T");
            var dt = datePt[0].split("-");
            return dt[2] + "-" + this.MonthString(dt[1]) + "-" + dt[0];
        },
        PopulateDropDownFromArray: function (dataSource, valueIndex, displayIndex)
        {
            var objectOutput = [];
            var output = "";
            var htmlOutput = "";
            $.each(dataSource, function (count, record) {
                var array = $.map(record, function (value, index) {
                    return [value];
                });
                objectOutput.push(array);
            });
            //console.log(objectOutput);
            for (i = 0; i < objectOutput.length; i++) {
                output += '<option value="' + objectOutput[i][valueIndex] + '">' + objectOutput[i][displayIndex] + '</option>';
            }
            return output;
        },
        ShowStockCardDialog:function(productId)
        {
            var products = [];
            //if (productId < 1)
            //{
                
            //}
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/ProductCreation/GetProducts",
                type: "Get",
                contentType: "application/json",
                success: function (data) {
                    // $("#stockCardProducts").html('<option value="0">--Select Product--</option>' + Utilities.PopulateDropDownFromArray(data, 1, 3));
                    $("#stockCardProducts").kendoDropDownList({
                        width:"100",
                        filter: "contains",
                        dataTextField: "ProductName",
                        dataValueField: "ProductId",
                        optionLabel: "Select item...",
                        dataSource: data,
                        change: function (e) {
                            var selectedItem = this.dataItem(e.item);
                            Utilities.ShowStockCardDialog(selectedItem.ProductId);
                        }
                    });

                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.Loader.Hide();
                }
            });

            $("#itemNameSpan").html("NA");
            $("#itemCodeSpan").html("NA");
            $("#itemDescriptionSpan").html("NA");
            $("#purchaseOrderSpan").html("NA");

            $("#stockCardTbody").html('\
                                            <tr>\
                                                <td>NA</td>\
                                                <td>NA</td>\
                                                <td>NA</td>\
                                                <td>NA</td>\
                                                <td>NA</td>\
                                            </tr>\
                                        ');

            $("#stockCardModal").modal("show");
            if (productId > 0)
            {
                Utilities.Loader.Show();
                $.ajax({
                url: API_BASE_URL + "/ProductCreation/ProductStockCard?productId="+productId,
                type: "Get",
                contentType: "application/json",
                success:function(data)
                {
                    $("#itemNameSpan").html(data.ProductName);
                    $("#itemCodeSpan").html(data.ProductCode);
                    $("#itemDescriptionSpan").html(data.ProductDescription);
                    $("#purchaseOrderSpan").html(data.PurchaseOrder);
                    var locations = data.StockCardLocations;
                    var locationsHtml = "";
                    var totalQtyOnHand = 0;
                    var totalSalesOrder = 0;
                    for (i = 0; i < locations.length; i++)
                    {
                        console.log("loc", locations[i]);
                        totalQtyOnHand = totalQtyOnHand + parseFloat(locations[i].QuantityOnHand);
                        totalSalesOrder = totalSalesOrder + parseFloat(locations[i].SalesOrderQuantity);
                        locationsHtml += '<tr>\
                                                <td>' + locations[i].StoreName + '</td>\
                                                <td>' + parseFloat(locations[i].SalesOrderQuantity) + '</td>\
                                                <td>' + parseFloat(locations[i].QuantityOnHand) + '</td>\
                                                <td>' + (parseFloat(locations[i].QuantityOnHand) - parseFloat(locations[i].SalesOrderQuantity)) + '</td>\
                                                <td>' + locations[i].UnitName + '</td>\
                                            </tr>\
                                        ';
                    }
                    locationsHtml += '\
                                            <tr>\
                                                <td><b>All Stores</b></td>\
                                                <td><b>' + totalSalesOrder + '</b></td>\
                                                <td><b>' + totalQtyOnHand + '</b></td>\
                                                <td><b>' + (totalQtyOnHand - totalSalesOrder) + '</b></td>\
                                                 <td></td>\
                                            </tr>\
                                        ';
                    $("#stockCardTbody").html(locationsHtml);
                    console.log(data);
                    Utilities.Loader.Hide();
                },
                error:function(err)
                {
                    Utilities.Loader.Hide();
                }
            });
            }
            $('#stockCardModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
        },
        FormatCurrency:function(amount)
        {
            var val= amount.toLocaleString('en-US', {
                style: 'currency',
                currency: 'NGN',
            });
            return val.replace("NGN","");
        }, 
        FormatCurrency1:function(amount)
        {
            const formatter1 = new Intl.NumberFormat('en-US', {
                style: 'currency',
                currency: 'NGN',
                minimumFractionDigits: 2
            });
            return formatter1.format(amount);
    },

    FormatCurrency2: function (amount) {
        const formatter = new Intl.NumberFormat('en-US', {
            style: 'currency',
            currency: 'NGN',
            minimumFractionDigits: 2
        });
        return formatter.format(amount);
    },

        FormatToNumber: function (amount) {
            return amount.replace(/[^0-9.-]+/g, "");
            //return amount.replace(/,/g, "");
        },
        FormatQuantity: function (qty) {
            var val = qty.toLocaleString('en-US');
            return val;
        },
        LogOut:function()
        {
            localStorage.removeItem("MAT_USER_INFO");   //clear user data in localstorage
            window.location = "/Security/Login/Logout";     //and redirect to logout controller to do backend logout
        },
        YyMmDdDate:function()
        {
            var year = new Date().getFullYear();
            var month = (parseInt(new Date().getMonth() + 1) < 10) ? ("0" + (parseInt(new Date().getMonth() + 1))) : (parseInt(new Date().getMonth() + 1));
            var day = new Date().getDate() < 10 ? ("0" + new Date().getDate()) : new Date().getDate();
            return year + "-" + month + "-" + day;
        },
        ApplyRolePriviledge:function()
        {
            //PriviledgeSettings.ApplyUserPriviledge();
        },
        SetDate:function(oldDate,interval)
        {
            var yourDate = new Date(oldDate);
            var newdate = "";
            if (interval > 0)
            {
               newdate= new Date(yourDate.setDate(yourDate.getDate() +interval));
            }
            else if (interval < 0)
            {
                newdate = new Date(yourDate.setDate(yourDate.getDate() - (interval*-1)));
            }
            return newdate.getFullYear() + "-" + (newdate.getMonth() + 1) + "-" + newdate.getDate();
        },
        NumberToWords: function (n) {

            var string = n.toString(), units, tens, scales, start, end, chunks, chunksLen, chunk, ints, i, word, words, and = 'and';

            /* Remove spaces and commas */
            string = string.replace(/[, ]/g, "");

            /* Is number zero? */
            if (parseInt(string) === 0) {
                return 'zero';
            }

            /* Array of units as words */
            units = ['', 'one', 'two', 'three', 'four', 'five', 'six', 'seven', 'eight', 'nine', 'ten', 'eleven', 'twelve', 'thirteen', 'fourteen', 'fifteen', 'sixteen', 'seventeen', 'eighteen', 'nineteen'];

            /* Array of tens as words */
            tens = ['', '', 'twenty', 'thirty', 'forty', 'fifty', 'sixty', 'seventy', 'eighty', 'ninety'];

            /* Array of scales as words */
            scales = ['', 'thousand', 'million', 'billion', 'trillion', 'quadrillion', 'quintillion', 'sextillion', 'septillion', 'octillion', 'nonillion', 'decillion', 'undecillion', 'duodecillion', 'tredecillion', 'quatttuor-decillion', 'quindecillion', 'sexdecillion', 'septen-decillion', 'octodecillion', 'novemdecillion', 'vigintillion', 'centillion'];

            /* Split user arguemnt into 3 digit chunks from right to left */
            start = string.length;
            chunks = [];
            while (start > 0) {
                end = start;
                chunks.push(string.slice((start = Math.max(0, start - 3)), end));
            }

            /* Check if function has enough scale words to be able to stringify the user argument */
            chunksLen = chunks.length;
            if (chunksLen > scales.length) {
                return '';
            }

            /* Stringify each integer in each chunk */
            words = [];
            for (i = 0; i < chunksLen; i++) {

                chunk = parseInt(chunks[i]);

                if (chunk) {

                    /* Split chunk into array of individual integers */
                    ints = chunks[i].split('').reverse().map(parseFloat);

                    /* If tens integer is 1, i.e. 10, then add 10 to units integer */
                    if (ints[1] === 1) {
                        ints[0] += 10;
                    }

                    /* Add scale word if chunk is not zero and array item exists */
                    if ((word = scales[i])) {
                        words.push(word);
                    }

                    /* Add unit word if array item exists */
                    if ((word = units[ints[0]])) {
                        words.push(word);
                    }

                    /* Add tens word if array item exists */
                    if ((word = tens[ints[1]])) {
                        words.push(word);
                    }

                    /* Add 'and' string after units or tens integer if: */
                    if (ints[0] || ints[1]) {

                        /* Chunk has a hundreds integer or chunk is the first of multiple chunks */
                        if (ints[2] || !i && chunksLen) {
                            words.push(and);
                        }

                    }

                    /* Add hundreds word if array item exists */
                    if ((word = units[ints[2]])) {
                        words.push(word + ' hundred');
                    }

                }

            }

            return words.reverse().join(' ');

    },
        OnlyUnique: function(value, index, self) {
            return self.indexOf(value) === index;
        }

    };
