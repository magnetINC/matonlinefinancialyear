﻿var CustomerSearchParam = {};
var SupplierSearchParam = {};
var table = "";

$(function () {
    GetCounterValues();
    getDashboardLdegers();
    getStockBalance();
});

function GetCounterValues() {
    Utilities.Loader.Show();
    var tosearch = {};
    tosearch.condition = "Pending";
    tosearch.fromDate = "2017-01-01";
    tosearch.gmUserId = matUserInfo.UserId;
    tosearch.ledgerId = -1;
    tosearch.toDate = todayDate();

    var tosearch1 = {};
    tosearch1.condition = "Pending";
    tosearch1.fromDate = "2017-01-01";
    tosearch1.gmUserId = matUserInfo.UserId;
    tosearch1.ledgerId = -1;
    tosearch1.toDate = todayDate();
    tosearch1.salesOrderNo = "All";

    var myLocationOrdersAjax = $.ajax({
        url: API_BASE_URL + "/Register/OrderConfirmationListing",
        type: "Post",
        data: JSON.stringify(tosearch),
        contentType: "application/json"
    });

    var otherLocationOrdersAjax = $.ajax({
        url: API_BASE_URL + "/Register/OtherLocationOrderConfirmationListing",
        type: "Post",
        data: JSON.stringify(tosearch),
        contentType: "application/json"
    });
    var orderAuthorizationCounterAjax = $.ajax({
        url: API_BASE_URL + "/Register/SalesOrder",
        type: "Post",
        data: JSON.stringify(tosearch1),
        contentType: "application/json"
    });
    var releaseFormCounterAjax = $.ajax({
        url: API_BASE_URL + "/Register/ReleaseFormCount?userId=" + matUserInfo.UserId,
        type: "GET",
        contentType: "application/json",
    });
    $.when(myLocationOrdersAjax, otherLocationOrdersAjax, orderAuthorizationCounterAjax, releaseFormCounterAjax)
    .done(function (dataMyLocation, dataOtherLocation, authorizationCount, releaseFormCount) {
        //console.log(confirmationCount[0]);

        var myloc = dataMyLocation[2].responseJSON.length;
        var otherloc = dataOtherLocation[2].responseJSON.length;
        var orderconfirmationCount = myloc + otherloc;

        $("#OrderConfirmationCount").html(orderconfirmationCount + " NEW!");
        $("#OrderAuthorisationCount").html(authorizationCount[2].responseJSON.length + " NEW!");
        $("#ReleaseFormCount").html(releaseFormCount[0] + " NEW!");
        Utilities.Loader.Hide();
    });

    //Utilities.Loader.Show();
    //$.ajax({
    //    url: API_BASE_URL + "/PurchaseOrder/PurchaseOrderListingCount",
    //    type: "Get",
    //    contentType: "application/json",
    //    success: function (data) {
    //        $("#PurchaseOrderListingCount").html(data + " NEW!");
    //        Utilities.Loader.Hide();
    //    },
    //    error: function (err) {
    //        Utilities.Loader.Hide();
    //    }
    //});

    //Utilities.Loader.Show();
    //$.ajax({
    //    url: API_BASE_URL + "/MaterialReceipt/MaterialReceiptCount",
    //    type: "Get",
    //    contentType: "application/json",
    //    success: function (data) {
    //        $("#MaterialReceiptApprovalCount").html(data + " NEW!");
    //        Utilities.Loader.Hide();
    //    },
    //    error: function (err) {
    //        Utilities.Loader.Hide();
    //    }
    //});

}

function getStockBalance() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Dashboard/GetStockBalance",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            //var outputARL = "";
            //var outputASO = "";
            //var outputOOS = "";
            var objToShow = [];
            for (i = 0; i < data.length; i++) {
                var percent = (data[i].ReOrderLevel / data[i].StockBalance) * 100;
                if (percent >= 80 && percent <= 100) {

                    objToShow.push([
                    data[i].ProductName,
                    data[i].StockBalance,
                    '<span class="label label-info">Approaching Re-Order Level</span>'
                    ]);
                }
                else if (percent > 100) {
                    objToShow.push([
                    data[i].ProductName,
                    data[i].StockBalance,
                    '<span class="label label-warning">Approaching Stock-Out</span>'
                    ]);
                }
                else if (data[i].StockBalance <= 0) {
                    objToShow.push([
                    data[i].ProductName,
                    data[i].StockBalance,
                    '<span class="label label-danger">Out of Stock</span>'
                    ]);
                }
            }
            console.log(objToShow);
            table = $('#stockBalanceTBody').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
                "createdRow": function (row, data, dataIndex) {
                    //if (percent >= 80 && percent <= 100) {
                    //    $(row).css("background-color", "blue");
                    //}
                    //else if (percent > 100) {
                    //    $(row).css("background-color", "yellow");
                    //}
                },
            });
        },
        error: function (err) {
        }
    });
    Utilities.Loader.Hide();
}

function getDashboardLdegers() {
    Utilities.Loader.Show();

    $.ajax({
        url: API_BASE_URL + "/Dashboard/GetBankLedgers",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            var output = "";
            $.each(data, function (i, record) {
                if (record.Balance > 0) {
                    output += '<tr>\
                            <td style="white-space: nowrap;">' + record.ledgerName + '</td>\
                            <td class="text-semibold" style="white-space: nowrap; color:green;">&#8358;' + Utilities.FormatCurrency(record.Balance) + '</td>\
                        </tr>\
                        ';
                }
                else if (record.Balance < 0) {
                    output += '<tr>\
                            <td style="white-space: nowrap;">' + record.ledgerName + '</td>\
                            <td class="text-semibold" style="white-space: nowrap; color:red;">&#8358;' + Utilities.FormatCurrency(record.Balance) + '</td>\
                        </tr>\
                        ';
                }
                else {
                    output += '<tr>\
                            <td style="white-space: nowrap;">' + record.ledgerName + '</td>\
                            <td class="text-semibold" style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(record.Balance) + '</td>\
                        </tr>\
                        ';
                }

                Utilities.Loader.Hide();
            });
            $("#customerAccountListTbody").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getTotalAmount() {
    $.ajax({
        url: API_BASE_URL + "/Dashboard/GetTotalSales",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var output = "";
            $.each(data, function (i, record) {

                Utilities.Loader.Hide();
            });
            $("#customerAccountListTbody").html(output);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
