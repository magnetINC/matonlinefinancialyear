﻿var banks = [];
var cash = [];
var allLedgers = [];
var bankRegisters = [];
var cashRegisters = [];
var otherRegisters = [];

var bankTable = "";
var cashTable = "";
var closingBalanceForBank = 0;

//========== LOOK UPS ==========
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BankRegister/GetlookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            banks = data.Response.banks;
            cash = data.Response.cash;
            allLedgers = data.Response.allLedgers;

            renderLookUpDataToControls();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function renderLookUpDataToControls() {
    $("#bank").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: banks,
        optionLabel: "All"
    });
    $("#cash").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgername",
        dataValueField: "ledgerId",
        dataSource: cash,
        optionLabel: "All"
    });
    $("#ledgers").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: allLedgers,
        optionLabel: "All"
    });
}

//========== BANK REGISTER ==========
function getBankRegisters(lid,tdt,fdt,lname)
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BankRegister/BankRegister?lid=" + lid + "&tdt=" + tdt + "&fdt=" + fdt + "&lname=" + lname,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            bankRegisters = data.Response.register;
            closingBalanceForBank = data.Response.closingBalance; 

            renderBankRegisterToTable();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function renderBankRegisterToTable() {
    var output = "";
    var objToShow = [];
    var currentBalance = 0;
    currentBalance += closingBalanceForBank;

    if (bankTable != "") {
        bankTable.destroy();
    }
    objToShow.push([
       "",
       "",
       "",
       "",
       "",
       "",
       "",
       "<h6>Opening Balance</h6>",
       '<b>&#8358;' + Utilities.FormatCurrency(closingBalanceForBank) + '</b>'
    ]);
    $.each(bankRegisters, function (count, row) {
        if (row.Debit != 0) {
            currentBalance += row.Debit - row.Credit;
        }
        else {
            currentBalance -= row.Credit - row.Debit;
        }
        objToShow.push([
            count + 1,
            row.Ledger,
             Utilities.FormatJsonDate(row.Date),
            row.narration,
            row.typeOfVoucher,
            row.voucherNo,
            '&#8358;' + Utilities.FormatCurrency(row.Credit),
            '&#8358;' + Utilities.FormatCurrency(row.Debit),
            '&#8358;' + Utilities.FormatCurrency(currentBalance)
        ]);
    });

    $("#totalBankValue").val(Utilities.FormatCurrency(currentBalance));

    bankTable = $('#BankRegisterTable').DataTable({
        data: objToShow,
        "responsive": false,
        "paging": false,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}

//========== CASH REGISTER ==========
function getCashRegisters(lid, tdt, fdt, lname) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BankRegister/CashRegister?lid=" + lid + "&tdt=" + tdt + "&fdt=" + fdt + "&lname=" + lname,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            cashRegisters = data.Response.registers;
            closingBalanceForBank = data.Response.closingBalance;

            renderCashRegisterToTable();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function renderCashRegisterToTable() {
    var output = "";
    var objToShow = [];
    var currentBalance = 0;
    currentBalance += closingBalanceForBank;

    if (cashTable != "") {
        cashTable.destroy();
    }
    objToShow.push([
       "",
       "",
       "",
       "",
       "",
       "",
       "",
       "<h6>Opening Balance</h6>",
       '<b>&#8358;' + Utilities.FormatCurrency(closingBalanceForBank) + '</b>'
    ]);
    $.each(cashRegisters, function (count, row) {
        if (row.Debit != 0) {
            currentBalance += row.Debit - row.Credit;
        }
        else {
            currentBalance -= row.Credit - row.Debit;
        }
        objToShow.push([
            count + 1,
            row.Ledger,
             Utilities.FormatJsonDate(row.Date),
            row.narration,
            row.typeOfVoucher,
            row.voucherNo,
            '&#8358;' + Utilities.FormatCurrency(row.Credit),
            '&#8358;' + Utilities.FormatCurrency(row.Debit),
            '&#8358;' + Utilities.FormatCurrency(currentBalance)
        ]);
    });

    $("#currentBalance").val(Utilities.FormatCurrency(currentBalance));
    cashTable = $('#cashRegisterTable').DataTable({
        //scrollY: "500px",
        //scrollX: true,
        data: objToShow,
        "responsive": false,
        "paging": false,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}

//========== OTHER REGISTER ==========
function getOtherRegisters(lid, tdt, fdt) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BankRegister/OtherRegister?lid=" + lid + "&tdt=" + tdt + "&fdt=" + fdt,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            otherRegisters = data.Response.register;
            closingBalanceForBank = data.Response.balance;

            renderOtherRegisterToTable();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function renderOtherRegisterToTable() {
    var output = "";
    var objToShow = [];
    var iniCurrentBalance = 0;
    iniCurrentBalance += closingBalanceForBank;

    //if (cashTable != "") {
    //    cashTable.destroy();
    //}
    output += '<tr>\
                            <td colspan="9"><b>Opening Banlance:          &#8358;' + Utilities.FormatCurrency(iniCurrentBalance) + '</b></td>\
                        </tr>'
    for (i = 0; i < otherRegisters.length; i++) {
        var currentBalance = 0;

        output += '<tr>\
                            <td colspan="9"><b>' + otherRegisters[i][0].LedgerName + '</b></td>\
                        </tr>';
        for (o = 0; o < otherRegisters[i].length; o++) {
            if (otherRegisters[i][o].Debit != 0) {
                currentBalance += otherRegisters[i][o].Debit - otherRegisters[i][o].Credit;
                iniCurrentBalance += otherRegisters[i][o].Debit - otherRegisters[i][o].Credit;
            }
            else {
                currentBalance -= otherRegisters[i][o].Credit - otherRegisters[i][o].Debit;
                iniCurrentBalance -= otherRegisters[i][o].Credit - otherRegisters[i][o].Debit;

            }
            output += '<tr>\
                            <td>'+ (o + 1) + '</td>\
                            <td>' + Utilities.FormatJsonDate(otherRegisters[i][o].Date)  + '</td>\
                            <td>' + otherRegisters[i][o].Memo + '</td>\
                            <td>' + otherRegisters[i][o].VoucherName + '</td>\
                            <td>' + otherRegisters[i][o].InvoiceNo + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(otherRegisters[i][o].Credit) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(otherRegisters[i][o].Debit) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(currentBalance) + '</td>\
                        </tr>';
        }
        output += '<tr>\
                            <td colspan="9"><b> Sub-Total:          &#8358;' + Utilities.FormatCurrency(currentBalance) + '</b></td>\
                        </tr>';
    }

    $("#totalAmount").val(Utilities.FormatCurrency(iniCurrentBalance));
    $('#otherRegisterTable').html(output);
    //cashTable = $('#otherRegisterTable').DataTable({
    //    //scrollY: "500px",
    //    //scrollX: true,
    //    data: objToShow,
    //    "responsive": false,
    //    "paging": false,
    //    "lengthChange": true,
    //    "searching": true,
    //    "ordering": true,
    //    "info": true,
    //    "autoWidth": true
    //});
}
