﻿var bankOrCash = [];
var suppliers = [];
var expenses = [];
var accountsSource = [];
var supplierInvoices = [];
var paymentSource = [];
var partyBalances = [];
var voucherNo = "";
var supplierToApplyOn = 0;
var transactionDetails = {};
var partyBalanceDetails = [];
var toSave = {};
//var typeItem = {};
var partyBalanceRenderDetails = [];
var totalAmount = 0;
var itemAmountList = [];
var details = [];
var deleteIndex;
var table = "";
var projects = [];
var categories = [];
var deleteIndex = -1;

getProjectAndCategoryLookUps();

$(function () {
    $("#pend").click(function () {
        pendPayment();
    });
    $("#currentBalance").click(function () {
        GetCurrentBalance();
    });
    $("#typeParent").hide();
    $("#balanceText").hide();
    getFormNo();
    LoadLookUps();
});

/* ======= API CALLS ======= */
function getFormNo() {
    Utilities.Loader.Show();
    $.ajax({
        //url: API_BASE_URL + "/PettyExpenses/getMaxPendingMasterIdPlusOne",
        url: API_BASE_URL + "/PettyExpenses/getAutoVoucherNo",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            $("#voucherNo").val(data);
            console.log("voucher number:", data);
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}
function LoadLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PettyExpenses/LookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            bankOrCash = data.BankOrCash;
            expenses = data.Expenses;
            voucherNo = data.voucherNo;
            voucherTypes = data.VoucherTypes;
            RenderControlData();

            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function getProjectAndCategoryLookUps() {
    var projectLookupAjax = $.ajax({
        url: API_BASE_URL + "/Project/GetProject",
        type: "GET",
        contentType: "application/json",
    });

    var categoryLookupAjax = $.ajax({
        url: API_BASE_URL + "/Category/GetCategory",
        type: "GET",
        contentType: "application/json",
    });

    $.when(projectLookupAjax, categoryLookupAjax)
        .done(function (dataProject, dataCategory) {
            projects = dataProject[0];
            categories = dataCategory[0];
            console.log("projects", dataProject);
            console.log("categories", dataCategory);
            $("#project").html("<option value=\"0\"></option>" + Utilities.PopulateDropDownFromArray(projects, 0, 1));
            $("#project").chosen({ width: "100%", margin: "1px" });
            $("#category").html("<option value=\"0\"></option>" + Utilities.PopulateDropDownFromArray(categories, 0, 1));
            $("#category").chosen({ width: "100%", margin: "1px" });
        });
}
function pendPayment() {
    if ($("#voucherNo").val() == "") {
        Utilities.ErrorNotification("Voucher number required!");
        return;
    }
    else if ($("#transactionDate").val() == "") {
        Utilities.ErrorNotification("Payment Date required!");
        return;
    }
    else if ($("#chequeDate").val() == "") {
        Utilities.ErrorNotification("Cheque Date required!");
        return;
    }
    else if ($("#bankorcash").val() == 0 || $("#bankorcash").val() == null || $("#bankorcash").val() == undefined) {
        Utilities.ErrorNotification("Please select a bank");
        return;
    }
    else {
        console.log(this.value);
        // var id = supplierToApplyOn;
        //var indexToFind = paymentSource.findIndex(p=>p.LedgerId == id);
        //if (indexToFind >= 0) {
        //    paymentSource[indexToFind].Amount = $("#amt" + supplierToApplyOn).val();
        //}
        var paymentMaster = {
            VoucherNo: $("#voucherNo").val(),
            InvoiceNo: $("#voucherNo").val(),
            LedgerId: $("#bankorcash").val(),
            Narration: "",
            UserId: 1,
            Date: $("#transactionDate").val(),
            TotalAmount: getTotalPaymentDetailsAmount(),
            ChequeNo: $("#chqrefNo").val(),
            ChequeDate: $("#chequeDate").val(),
            //Details: paymentSource,
            PaymentDetails: paymentSource,
            PartyBalances: partyBalances
        };
        console.log(paymentMaster);
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PettyExpenses/Pend",
            data: JSON.stringify(paymentMaster),
            type: "POST",
            contentType: "application/json",
            success: function (data) {
                Utilities.SuccessNotification(data.ResponseMessage);
                Utilities.Loader.Hide();
                getFormNo();
                clear();
                window.location = "/supplier/PettyExpenses/index";
            },
            error: function (err) {
                Utilities.SuccessNotification("Oops! Something went wrong!");
                Utilities.Loader.Hide();
            }
        });
    }

}
function GetCurrentBalance() {
    var ledgerId = $("#bankorcash").val();
    if (ledgerId == 0 || ledgerId == null || ledgerId == undefined) {
        Utilities.ErrorNotification("Please select a Bank");
    }
    else {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PettyExpenses/GetCurrentBalance?ledgerId=" + ledgerId,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                var currentBalance = "₦" + Utilities.FormatCurrency(data);
                $("#balanceText").val(currentBalance);
                $("#balanceText").show();
                Utilities.Loader.Hide();
            },
            error: function () {
                Utilities.Loader.Hide();
            }
        });
    }
}

/* ======= RENDER DATA TO CONTROLS ======= */
function RenderControlData() {
    //$("#bankorcash").kendoDropDownList({
    //    filter: "contains",
    //    optionLabel: "Please Select...",
    //    dataTextField: "ledgerName",
    //    dataValueField: "ledgerId",
    //    dataSource: bankOrCash
    //});
    $("#bankorcash").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(bankOrCash, 1, 0));
    $("#bankorcash").chosen({ width: "100%", margin: "1px" });
}
function renderPayments() {
    var output = "";
    console.log(paymentSource);
    $.each(paymentSource, function (count, row) {
        var newAmount = parseFloat(row.Amount);
        output += '<tr>\
                    <td>'+ (count + 1) + '</td>\
                    <td>' + findSuppliers(row.LedgerId).ledgerName + '</td>\
                    <td>&#8358;' + Utilities.FormatCurrency(newAmount) + '</td>\
                    <td>Naira | NGN </td>\
                    <td>' + row.Memo + '</td>\
                    <td><button class="btn btn-sm btn-danger" onclick="confirmDelete(' + count + ')"><i class="fa fa-times"></i></button></td>\
                   </tr>';
    });
    $("#suppliersRowTbody").html(output);
    $("#totalAmount").val(Utilities.FormatCurrency(getTotalPaymentDetailsAmount()));

    $("#amount").val(0);
    $("#memo").val("");
    $("#totalPaymentAmt").val(0);
}

var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the positive lable as key
    //...
};

function confirmDelete(index) {
    deleteIndex = index;
    eModal.confirm(options).then(removeLineItem);
}

function removeLineItem() {
    paymentSource.splice(deleteIndex, 1);
    renderPayments();
}

function changeAmount(selectObject) {
    var newAmount = parseFloat(selectObject.value);
    $("#totalPaymentAmt").val(Utilities.FormatCurrency(newAmount));
}
function getTotalInvoiceAmount() {
    var amt = 0.0;
    for (i = 0; i < unprocessedInvoicePayments.length; i++) {
        var chk = unprocessedInvoicePayments[i].Amount == undefined ? 0 : parseFloat(unprocessedInvoicePayments[i].Amount)
        amt = amt + chk;
    }
    return amt;
}
function addPaymentToObject() {
    for (var i = 0; i < partyBalanceRenderDetails.length; i++) {
        partyBalances.push({
            Date: Utilities.YyMmDdDate(),
            Credit: itemAmountList[i],
            LedgerId: $("#suppliers").val(),
            ReferenceType: partyBalanceRenderDetails[i].reference,
            VoucherNo: partyBalanceRenderDetails[i].voucherNo,
            InvoiceNo: partyBalanceRenderDetails[i].invoiceNo
        });
    }
    //partyBalances.push({
    //    Date: Utilities.YyMmDdDate(),
    //    Credit: $("#totalPaymentAmount").val(),
    //    LedgerId: $("#suppliers").val(),
    //    ReferenceType: "OnAccount",
    //    VoucherNo: $("#voucherNo").val(),
    //    InvoiceNo: $("#voucherNo").val()
    //});
    console.log("Party balances " + partyBalances);
    //always empty array to populate with updated data
    paymentSource.push({
        Memo: $("#memo").val(),
        LedgerId: $("#suppliers").val(),
        Amount: $('#totalPaymentAmount').val(),
        grossAmount: 0,
        Status: "OnAccount",
        ExchangeRateId: $("#currency").val()
    });
    console.log("Payments " + paymentSource);
    renderPayments();
    $("#suppliers").val("");
    $("#suppliers").trigger("chosen:updated");
    $("#memo").val("");
    $("#reference").val("");
    $("#reference").trigger("chosen:updated");
    partyBalanceRenderDetails = [];
    itemAmountList = [];
    $("#totalPaymentAmount").val(0);
    renderPartyBalanceDetails();
}

function openModal() {
    if ($("#bankorcash").val() == "") {
        Utilities.ErrorNotification("You need to fill the Bank/Cash field!");
        return false;
    }

    $("#newExpenseModal").modal("show");
    $('#newExpenseModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });

    if ($("#accountType").val() == 1) {
        accountsSource = [];
        accountsSource = suppliers;
    }
    else if ($("#accountType").val() == 2) {
        accountsSource = [];
        accountsSource = expenses;
    }

    $("#expenses").kendoDropDownList({
        filter: "contains",
        optionLabel: "Please Select...",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: accountsSource
    });
}

//$("#suppliers").change(function () {
//        var ledgerId = parseInt($("#suppliers").val());
//        Utilities.Loader.Show();
//        $.ajax({
//            url: API_BASE_URL + "/SupplierCentre/GetLedgerDetails?ledgerId=" + ledgerId,
//            type: "GET",
//            contentType: "application/json",
//            success: function (data) {
//                transactionDetails = data;
//                renderTransactionDetails();
//                Utilities.Loader.Hide();
//            },
//            error: function (err) {
//                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
//                Utilities.Loader.Hide();
//            }
//        });
//});



$("#suppliers").change(function () {
    $("#reference").val("");
    $("#typeParent").hide();
    var ledgerId = parseInt($("#suppliers").val());
    suppliers.forEach(function (item) {
        if (item.ledgerId == ledgerId) {
            toSave.LedgerId = ledgerId;
            toSave.VoucherTypeId = voucherTypes.find(p => p.typeOfVoucher == "Payments").voucherTypeId;
            toSave.AccountGroupId = item.accountGroupId;
            return true;
        }
    });
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SupplierCentre/GetPartyBalanceDetails",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data) {
            partyBalanceDetails = data;
            console.log("party balance", data);
            // renderPartyBalanceDetails()
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
});

$("#reference").change(function () {
    if ($("#reference").val() == "Against") {
        $("#typeParent").show();
        $("#type").kendoDropDownList({
            filter: "contains",
            optionLabel: "Please Select...",
            dataTextField: "display",
            dataValueField: "invoiceNo",
            dataSource: partyBalanceDetails.PartyDetails
        });
    }
    else if ($("#reference").val() == "OnAccount") {
        var typeItem = {};
        typeItem.balance = "";
        typeItem.display = "";
        typeItem.invoiceNo = "";
        typeItem.voucherNo = "";
        typeItem.reference = "On Account";
        partyBalanceRenderDetails.push(typeItem);
        renderPartyBalanceDetails();
        $("#typeParent").hide();
    }

    else {
        $("#typeParent").hide();
    }
});

$("#type").change(function () {
    if ($("#type").val() != "") {
        var typeItem = {};
        typeItem = partyBalanceDetails.PartyDetails.find(p => p.invoiceNo == $("#type").val());
        typeItem.reference = "Against";
        partyBalanceRenderDetails.push(typeItem);
        renderPartyBalanceDetails();
    }
});

$(".lineItemAmt").change(
    calculateTotalAmount
);

function getTotalPaymentDetailsAmount() {
    var totalPaymentDetailsAmt = 0.0;
    $.each(details, function (count, row) {
        var chk = row.Amount == undefined ? 0 : row.Amount;
        totalPaymentDetailsAmt = totalPaymentDetailsAmt + parseFloat(chk);
    });
    return totalPaymentDetailsAmt;
}

function findSuppliers(ledgerId) {
    var output = {};
    for (i = 0; i < suppliers.length; i++) {
        if (suppliers[i].ledgerId == ledgerId) {
            output = suppliers[i];
            break;
        }
    }
    for (i = 0; i < expenses.length; i++) {
        if (expenses[i].ledgerId == ledgerId) {
            output = expenses[i];
            break;
        }
    }
    return output;
}


function renderPartyBalanceDetails() {
    var output = "";//"<thead><th>S/N</th><th>Date</th><th>Type</th><th>Num</th><th>Memo</th><th>Debit</th><th>Credit</th><th>Balance</th></thead><tbody>";
    var counter = 0;

    $.each(partyBalanceRenderDetails, function (count, row) {
        counter++;
        //overallBalance += row.OpeningCredit - row.OpeningDebit;
        output +=
            '<tr>\
                <td style="white-space: nowrap;">'+ counter + '</td>\
                <td style="white-space: nowrap;">' + row.reference + '</td>\
                <td style="white-space: nowrap;">' + row.display + '</td>\
                <td style="white-space: nowrap;">' + row.invoiceNo + '</td>\
                <td style="white-space: nowrap;" class="outstandingItem">' + row.balance + '</td>\
                <td style="white-space: nowrap;">' + '<input onkeyup="calculateTotalAmount()" class="lineItemAmt" value=' + itemAmountList[count] + ' type="number"></input>' + '</td>\
                <td style="white-space: nowrap;">' + 'Dr' + '</td>\
            </tr>\
            ';
    });
    $("#partyBalanceTbody").html(output);
    if ($("#reference").val === "OnAccount") {
        $(".outstandingTablehead").hide();
        $(".outstandingItem").hide()
    }

}


function calculateTotalAmount() {
    itemAmountList = [];
    var amtList = document.getElementsByClassName('lineItemAmt');
    var totalAmount = 0;
    for (var i = 0; i < amtList.length; i++) {
        if (amtList[i].value.trim() == "") {
            amtList[i].value = 0;
        }
        totalAmount += parseFloat(amtList[i].value);
        itemAmountList.push(amtList[i].value);
    }

    $("#totalPaymentAmount").val(totalAmount);
}

function clear() {
    $("#chqrefNo").val(totalAmount);
    $("#bankorcash").val("");
    $("#bankorcash").trigger("chosen:updated");
    paymentSource = [];
    renderPayments();
}

function addToObject() {
    var index = details.findIndex(p => p.LedgerId == $("#expenses").val());
    if (index >= 0) {
        Utilities.ErrorNotification("This Expense has been selected already.");
    }
    else if ($("#expenses").val() == "" || $("#expenses").val() == undefined || $("#expenses").val() == null) {
        Utilities.ErrorNotification("Please select an expense account");
    }
    //else if ($("#project").val() == "" || $("#project").val() == undefined || $("#expenses").val() == null) {
    //    Utilities.ErrorNotification("Please select a project");
    //}
    //else if ($("#category").val() == "" || $("#category").val() == undefined || $("#category").val() == null) {
    //    Utilities.ErrorNotification("Please select a category");
    //}
    else if (parseFloat($("#amount").val()) <= 0 || $("#amount").val() == "") {
        Utilities.ErrorNotification("Please enter a valid amount");
    }
    else {
        details.push({
            LedgerId: $("#expenses").val(),
            Amount: parseFloat($("#amount").val()),
            grossAmount: 0,
            Status: "OnAccount",
            Memo: $("#memo").val(),
            ExchangeRateId: $("#currency").val(),
            ProjectId: $("#project").val(),
            CategoryId: $("#category").val()
        });
        partyBalances.push({
            Date: Utilities.YyMmDdDate(),
            Credit: parseFloat($("#amount").val()),
            LedgerId: $("#suppliers").val(),
            ReferenceType: "OnAccount",
            VoucherNo: $("#voucherNo").val(),
            InvoiceNo: $("#voucherNo").val(),
        });
        renderExpenseDateToTable();
        $("#amount").val(0);
        $("#memo").val("");
    }
}

function renderExpenseDateToTable() {
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }

    $.each(details, function (count, row) {
        var projectObj = projects.find(p => p.ProjectId == row.ProjectId);
        var projectName = projectObj == undefined ? "" : projectObj.ProjectName;
        var categoryObj = categories.find(p => p.CategoryId == row.CategoryId);
        var categoryName = categoryObj == undefined ? "" : categoryObj.CategoryName;
        objToShow.push([
            count + 1,
            findSuppliers(row.LedgerId).ledgerName,
            '&#8358;' + Utilities.FormatCurrency(row.Amount),
            projectName,
            categoryName,
            row.Memo,
            `<td><button class="btn btn-sm btn-danger" onclick="confirmDelete(` + count + `)"><i class="fa fa-times"></i></button></td>`
        ]);
    });

    $("#totalAmount").val(Utilities.FormatCurrency(calculateTotalAmount()));
    table = $('#expensesTable').DataTable({
        data: objToShow,
        "responsive": false,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}

function calculateTotalAmount() {
    var totalAmount = 0;
    $.each(details, function (count, row) {
        totalAmount = totalAmount + row.Amount;
    });
    return totalAmount;
}

$("#save , #savePrint").click(savePayment);

function savePayment() {
    var myId = $(this).attr('id');
    console.log(myId);
    if ($("#voucherNo").val() == "") {
        Utilities.ErrorNotification("Voucher number required!");
        return;
    }
    else if ($("#transactionDate").val() == "") {
        Utilities.ErrorNotification("Payment Date required!");
        return;
    }
    else if ($("#chequeDate").val() == "") {
        Utilities.ErrorNotification("Cheque Date required!");
        return;
    }
    else if ($("#bankorcash").val() == 0 || $("#bankorcash").val() == null || $("#bankorcash").val() == undefined) {
        Utilities.ErrorNotification("Please select a bank");
        return;
    }
    else {
        console.log(this.value);
        var id = supplierToApplyOn;
        var indexToFind = paymentSource.findIndex(p => p.LedgerId == id);
        if (indexToFind >= 0) {
            paymentSource[indexToFind].Amount = $("#amt" + supplierToApplyOn).val();
        }
        var paymentMaster = {
            VoucherNo: $("#voucherNo").val(),
            InvoiceNo: $("#voucherNo").val(),
            LedgerId: $("#bankorcash").val(),
            Narration: "",
            UserId: 1,
            Date: $("#transactionDate").val(),
            TotalAmount: getTotalPaymentDetailsAmount(),
            ChequeNo: $("#chqrefNo").val(),
            ChequeDate: $("#chequeDate").val(),
            DoneBy: $("#UserId").html(),
            //Details: paymentSource,
            PaymentDetails: details,
            PartyBalances: partyBalances
        };

        console.log(paymentMaster);
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PettyExpenses/Save",
            data: JSON.stringify(paymentMaster),
            type: "POST",
            contentType: "application/json",
            success: function (data) {
                //Utilities.SuccessNotification(data.ResponseMessage);
                Utilities.SuccessNotification("saved successfully!");
                if (myId == "savePrint") {
                    printPayment();
                }
                Utilities.Loader.Hide();
                getFormNo();
                clear();

                window.location = "/Banking/PettyExpenses/Index";
            },
            error: function (err) {
                Utilities.SuccessNotification("Oops! Something went wrong!");
                Utilities.Loader.Hide();
            }
        });
    }

}



var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the positive lable as key
    //...
};

function confirmDelete(index) {
    deleteIndex = index;
    eModal.confirm(options).then(removeLineItem);
}

function removeLineItem() {
    details.splice(deleteIndex, 1);
    renderExpenseDateToTable();
}

//getImage
var imgsrc = $("#companyImage").attr('src');
var imageUrl;

//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}




function printPayment() {

    var objToPrint = [];

    $.each(details, function (count, row) {
        var projectObj = projects.find(p => p.ProjectId == row.ProjectId);
        var projectName = projectObj == undefined ? "" : projectObj.ProjectName;
        var categoryObj = categories.find(p => p.CategoryId == row.CategoryId);
        var categoryName = categoryObj == undefined ? "" : categoryObj.CategoryName;
        objToPrint.push({
            "SlNo": count + 1,
            "Expenses": findSuppliers(row.LedgerId).ledgerName,
            "Memo": row.Memo,
            "Projects": projectName,
            "Department": categoryName,
            "Amount": 'N' + Utilities.FormatCurrency(row.Amount),
        });
    });

    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();
    var thisEmail = $("#companyEmail").val();
    var thisVoucherNo = $("#voucherNo").val();

    //get the value selected from options
    var domNode = document.getElementById("bankorcash");
    var value = domNode.selectedIndex;
    var thisAccount = domNode.options[value].text;

    var thisDate = $("#transactionDate").val();
    var thisPhone = $("#companyPhone").val();
    var totalAmt = $("#totalAmount").val();

    columns = [
        { title: "S/N", dataKey: "SlNo" },
        { title: "Ledgers", dataKey: "Expenses" },
        { title: "Memo", dataKey: "Memo" },
        { title: "Projects", dataKey: "Projects" },
        { title: "Department", dataKey: "Department" },
        { title: "Amount", dataKey: "Amount" },
    ];

    var doc = new jsPDF('portrait', 'pt', 'letter');
    doc.setDrawColor(0);

    //start drawing

    doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text(companyName, 320, 80, 'center');
    doc.setFontSize(9);
    doc.setFontType("normal");
    doc.text(companyAddress, 320, 90, 'center');

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Email Id: ', 400, 150);
    doc.setFontType("normal");
    doc.text(thisEmail, 460, 150);
    doc.setFontType("bold");
    doc.text('Phone: ', 400, 165);
    doc.setFontType("normal");
    doc.text(thisPhone, 460, 165);

    doc.line(40, 170, 570, 170);

    doc.setFontType("bold");
    doc.setFontSize(9);
    doc.text('Account:', 40, 185);
    doc.setFontType("normal");
    doc.text(thisAccount, 85, 185);

    doc.setFontType("bold");
    doc.text('Voucher No: ', 400, 185);
    doc.setFontType("normal");
    doc.text(thisVoucherNo, 460, 185);
    doc.setFontType("bold");
    doc.text('Date: ', 400, 205);
    doc.setFontType("normal");
    doc.text(thisDate, 460, 205);

    doc.line(40, 220, 570, 220);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text('Petty Expenses', 250, 240);

    //doc.line(120, 20, 120, 60); horizontal line
    doc.setFontSize(8);
    doc.autoTable(columns, objToPrint, {
        startY: 255,
        theme: 'striped',
        styles: { "overflow": "linebreak", "cellWidth": "wrap", "rowPageBreak": "avoid", "fontSize": "7", "lineColor": "100", "lineWidth": ".25" },
        columnStyles: {
            SlNo: { columnWidth: 30, },
            Amount: { columnWidth: 80, halign: 'right' },
            Memo: { columnWidth: 100 },
            Expenses: { columnWidth: 110 },
            Department: { columnWidth: 100 },
            Projects: { columnWidth: 100 }

        },
    });

    doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Total Amount: NGN' + totalAmt, 555, doc.autoTable.previous.finalY + 20, "right");

    doc.line(40, doc.autoTable.previous.finalY + 30, 570, doc.autoTable.previous.finalY + 30);

    doc.setFontType("bold");
    doc.text('Amount in Words: ', 40, doc.autoTable.previous.finalY + 60);
    doc.setFontType("normal");

    //remove commas
    var tempTotal = totalAmt.replace(/,/g, "");
    //convert to number
    var amtToBeConverted = parseInt(tempTotal);

    //capitalize Each Word
    var wordFormat = Utilities.NumberToWords(amtToBeConverted);
    const words = wordFormat.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());

    var strArr = doc.splitTextToSize(words + ' Naira ONLY.', 500);
    doc.text(strArr, 120, doc.autoTable.previous.finalY + 60);

    doc.setFontType("bold");
    doc.text('Payment approved by:  _________________________________', 40, doc.autoTable.previous.finalY + 100);

    doc.text('Done by:  ' + $("#nameSpan").html(), 350, doc.autoTable.previous.finalY + 100);
    doc.text('_________________', 390, doc.autoTable.previous.finalY + 103);
    doc.autoPrint();
    doc.save('petty-Expenses.pdf');
    //window.open(doc.output('bloburl'));
    window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
}