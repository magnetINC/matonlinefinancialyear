﻿var banks = [];
var bankStatements = [];

var table = ""

$(function () {
    getLookUps();

    $(".dt").change(function () {
        var date1 = new Date('December 17, 1995 03:24:00');
        // Sun Dec 17 1995 03:24:00 GMT...

        var date2 = new Date('1995-12-17T03:24:00');


    });
});

//========== API CALLS ==========
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BankReconcilation/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            banks = data.Response.accounts;
            renderLookUpDataToControls();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getBankStatements()
{
    if ($("#bank").val() == 0 || $("#bank").val() == "" || $("#bank").val() == null)
    {
        Utilities.ErrorNotification("Please Select a bank");
    }
    else
    {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/BankReconcilation/GetReconciledOrUnreconciledToList?lid=" + $("#bank").val() + "&tdt=" + $("#toDate").val() + "&fdt=" + $("#fromDate").val() + "&st=" + $("#status").val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                bankStatements = data.Response;
                renderDataToTable();
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}
function save()
{
    if(confirm("Are you sure you wawnt to save?"))
    {
        Utilities.Loader.Show();
        var toSave = [];
        for (i = 0; i < bankStatements.length; i++) {
            if (bankStatements[i].statementDate != null) {
                toSave.push({
                    LedgerPostingId: bankStatements[i].ledgerPostingId,
                    StatementDate: bankStatements[i].statementDate,
                    ReconStartDate: $("#fromDate").val(),
                    ReconEndDate: $("#toDate").val(),
                });
            }
        }
        $.ajax({
            url: API_BASE_URL + "/BankReconcilation/Save",
            type: "POST",
            data: JSON.stringify(toSave),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification(data.ResponseMessage);
                    window.location = "/Banking/BankReconcilation/Index";
                    Utilities.Loader.Hide();
                }
                else {
                    Utilities.ErrorNotification(data.ResponseMessage);
                    Utilities.Loader.Hide();
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}



//=========== RENDER DATA TO CONTROLS ==========
function renderLookUpDataToControls() {
    $("#bank").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: banks,
        optionLabel: "Please Select..."
    });
}
function renderDataToTable() {
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }
    $.each(bankStatements, function (count, row) {
        if (row.statementDate == null)
        {
            objToShow.push([
                 count + 1,
                 row.date,
                 row.ledgerName,
                 row.voucherTypeName,
                 row.voucherNo,
                 row.chequeNo,
                 row.chequeDate,
                 '&#8358;' + Utilities.FormatCurrency(row.debit),
                 '&#8358;' + Utilities.FormatCurrency(row.credit),
                 '<input type="text"  class="form-control dt" onchange="changeDate(' + row.ledgerPostingId + ')" id="stDate' + row.ledgerPostingId + '" readonly="readonly"/>'
            ]);
        }
        else
        {
            objToShow.push([
                 count + 1,
                 row.date,
                 row.ledgerName,
                 row.voucherTypeName,
                 row.voucherNo,
                 row.chequeNo,
                 row.chequeDate,
                 '&#8358;' + Utilities.FormatCurrency(row.debit),
                 '&#8358;' + Utilities.FormatCurrency(row.credit),
                row.statementDate
            ]);
        }
     
    });
    table = $('#bankStatementsTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    $(".dt").datepicker();
}
function changeDate(id)
{
    var date1 = new Date($("#stDate"+id).val());
    var date2 = new Date($("#toDate").val());
    if (date1 > date2)
    {
        Utilities.ErrorNotification("Statement date must be within the date range");
        $("#stDate" + id).val("");
    }
    else {
        var indexOfObject = bankStatements.findIndex(p=>p.ledgerPostingId == id);
        bankStatements[indexOfObject].statementDate = $("#stDate" + id).val();
        console.log(bankStatements);

        $("#cbd").html('&#8358;' + Utilities.FormatCurrency(calculateAllTheAmounts().companyBalanceDeposit));
        $("#cbw").html('&#8358;' + Utilities.FormatCurrency(calculateAllTheAmounts().companyBalanceWithdrawal));
        $("#bd").html('&#8358;' + Utilities.FormatCurrency(calculateAllTheAmounts().bankBalanceDeposit));
        $("#bw").html('&#8358;' + Utilities.FormatCurrency(calculateAllTheAmounts().bankBalanceWithdrawal));
        $("#dd").html('&#8358;' + Utilities.FormatCurrency(calculateAllTheAmounts().differenceDeposit));
        $("#dw").html('&#8358;' + Utilities.FormatCurrency(calculateAllTheAmounts().differenceWithdrawal));
    }
}
function calculateAllTheAmounts()
{
    var companyBalanceDeposit = 0
    var companyBalanceWithdrawal = 0;
    var bankBalanceDeposit = 0;
    var bankBalanceWithdrawal = 0;
    var differenceDeposit = 0;
    var differenceWithdrawal = 0;
    $.each(bankStatements, function (count, row) {
        companyBalanceWithdrawal = companyBalanceWithdrawal + row.credit;
        companyBalanceDeposit = companyBalanceDeposit + row.debit;
        if(row.statementDate != null)
        {
            bankBalanceWithdrawal = bankBalanceWithdrawal + row.credit;
            bankBalanceDeposit = bankBalanceDeposit + row.debit;
        }
    });
    differenceWithdrawal = companyBalanceWithdrawal - bankBalanceWithdrawal;
    differenceDeposit = companyBalanceDeposit - bankBalanceDeposit;

    return {
        companyBalanceWithdrawal: companyBalanceWithdrawal,
        companyBalanceDeposit: companyBalanceDeposit,
        bankBalanceWithdrawal: bankBalanceWithdrawal,
        bankBalanceDeposit: bankBalanceDeposit,
        differenceWithdrawal: differenceWithdrawal,
        differenceDeposit: differenceDeposit
    }
}
