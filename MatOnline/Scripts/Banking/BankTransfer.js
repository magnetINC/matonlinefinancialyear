﻿var cashOrBank = [];
var accounts = [];
var transfer = [];
var currency = [];
var bankTransferDetails = [];
var bankTransferMaster = {};
var totalAmount = 0.0;
var formNo = "";
var deleteIndex;

var table = "";

$(function () {
    GetLookUps();
    $("#account").change(function () {
        RenderTransferGrid();
    });
    $("#transferType").change(function () {
        if($("#transferType").val() == "Deposit")
        {
            $("#accountLabel").html("Deposit Into");
            $("#panelLabel").html("Withdraw From");            
        }
        else
        {
            $("#accountLabel").html("Withdraw From");
            $("#panelLabel").html("Deposit Into");
        }
    });
    if ($("#transferType").val() == "Deposit") {
        $("#accountLabel").html("Deposit Into");
        $("#panelLabel").html("Withdraw From");
    }
    else {
        $("#accountLabel").html("Withdraw From");
        $("#panelLabel").html("Deposit Into");
    }
    $("#save").click(function () {
        Save();
    });
});

//To load look Up data
function GetLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BankTransfer/LoadLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            cashOrBank = data.CahsOrBank;
            accounts = data.CahsOrBank;
            currency = data.Currency;
            formNo = data.FormNo;
            RenderControlData();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.SuccessNotification("Oops! Something went wrong!");
            Utilities.Loader.Hide();
        }
    });
}
//to render control data
function RenderControlData() {
    $("#account").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: cashOrBank,
        optionLabel: "Please Select..."
    });
    $("#bank").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: cashOrBank,
        optionLabel: "Please Select..."
    });
    //$("#bank").html("<option value=\"\">--Select--</option>" + Utilities.PopulateDropDownFromArray(cashOrBank, 1, 0));
    $("#currency").kendoDropDownList({
        filter: "contains",
        dataTextField: "currencyName",
        dataValueField: "exchangeRateId",
        dataSource: currency,
    });
    $("#formNo").val(formNo);
}

function Save()
{
    if ($("#formNo").val() == "") {
        Utilities.ErrorNotification("Voucher number required!");
        return;
    }
    else if ($("#date").val() == "") {
        Utilities.ErrorNotification("Payment Date required!");
        return;
    }

    bankTransferMaster = {};
    bankTransferMaster.ledgerId = $("#account").val(),
    bankTransferMaster.voucherNo = $("#formNo").val(),
    bankTransferMaster.date = $("#date").val(),
    bankTransferMaster.transactionType = $("#transferType").val(),
    bankTransferMaster.totalAmount = totalAmount,
    bankTransferMaster.narration = "",
    bankTransferMaster.lineTransfers = bankTransferDetails

    console.log(bankTransferMaster);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BankTransfer/SaveBankTransfer",
        type: "Post",
        contentType: "application/json",
        data: JSON.stringify(bankTransferMaster),
        success: function (data) {
            if(data != null)
            {
                Utilities.Loader.Hide();
                //Utilities.SuccessNotification("Transfer Made successfully");
                Utilities.SuccessNotification(data.ResponseMessage);
                window.location = "/Banking/BankTransfer/Index";
            }
            else {
                Utilities.Loader.Hide();
                Utilities.ErrorNotification("Transfer not Successful");
            }
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry something went wrong!");
        }
    });
}


function addDataToObject()
{
    var indexOfObject = bankTransferDetails.findIndex(p=>p.ledgerId == $("#bank").val());
    if ($("#bank").val() == "" || $("#bank").val() == 0)
    {
        Utilities.ErrorNotification("Select a Bank/Cash Account");
    }
    else if($("#account").val() == $("#bank").val())
    {
        Utilities.ErrorNotification("you can't withdraw/deposit from/into the same account");
    }
    else if ($("#amount").val() == 0 || $("#amount").val() == "") {
        Utilities.ErrorNotification("enter valid amount");
    }
    else if (indexOfObject >= 0) {
        Utilities.ErrorNotification("This Ledger Account has been Selected already");
    }
    else
    {
        bankTransferDetails.push({
            ledgerId: $("#bank").val(),
            amount: parseFloat($("#amount").val()),
            memo: $("#memo").val(),
            currency: $("#currency").val(),
            chequeNo: $("#chequeNo").val(),
            chequeDate: $("#chequeDate").val()
        });
        var presentDate = new Date();
        $("#amount").val("");
        $("#bank").val("");
        $("#bank").trigger("chosen:updated");
        $("#memo").val("");
        $("#chequeNo").val("");
        //$("#chequeDate").val(presentDate.getFullYear() + "-" + (presentDate.getMonth() + 1) + "-" + presentDate.getDate());
    }
    renderTransfersToTable();

}

function addBank()
{
    if ($("#account").val() == "" || $("#account").val() == 0)
    {
        Utilities.ErrorNotification("Please select a Bank or cash account");
        return;
    }
    else {

        $("#addBankModal").modal("show");
        $('#addBankModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
    }
}

function renderTransfersToTable() {
    var output = "";
    var objToShow = [];
    var statusOutput = "";

    if (table != "") {
        table.destroy();
    }
    totalAmount = 0;
    $.each(bankTransferDetails, function (count, row) {
        totalAmount = totalAmount + row.amount
        objToShow.push([
            count + 1,
            getAccountLedger(row.ledgerId),
            '&#8358;' + Utilities.FormatCurrency(row.amount),
            getCurrency(row.currency),
            row.chequeNo,
            row.chequeDate,
            row.memo,
            '<a class="btn btn-danger" onclick="confirmDelete(' + count + ');"><i class="fa fa-trash"></i></a>'
            //'<a class="btn btn-danger" onclick="removeFromBudgetDetails(' + count + ');"><i class="fa fa-trash"></i></a>' 
        ]);
    });
    $("#totalAmount").val(Utilities.FormatCurrency(totalAmount));
    table = $('#newBanksTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}

var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the possitive lable as key
    //...
};

function confirmDelete(index) {
    deleteIndex = index;
    eModal.confirm(options).then(removeFromBudgetDetails);
}


function removeFromBudgetDetails() {
    var index = parseInt(deleteIndex);
    bankTransferDetails.splice(index, 1);
    renderTransfersToTable();
}



//to render the transfer grid
function RenderTransferGrid() {
    $("#BankTransferKendoGrid").kendoGrid({
        dataSource: {
            transport: {
                read: function (entries) {
                    entries.success(transfer);
                },
                create: function (entries) {
                    entries.success(entries.data);
                },
                update: function (entries) {
                    entries.success();
                },
                destroy: function (entries) {
                    entries.success();
                },
                parameterMap: function (data) { return JSON.stringify(data); }
            },
            schema: {
                model: {
                    id: "ledgerId",
                    fields: {
                        ledgerId: { editable: true, validation: { required: true } },
                        amount: { editable: true, validation: { required: true, type: "number" } },
                        exchangeRateId: { editable: true, validation: { required: true } },
                        chequeNo: { editable: true, validation: { required: false, type: "text" } },
                        chequeDate: { editable: true, validation: { required: false, type: "text" } },
                        memo: { editable: true, validation: { required: true, type: "text" } }
                    }
                }
            },
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 5,
            pageSizes: [5, 10, 20, 50, 100],
            previousNext: true,
            buttonCount: 5,
        },
        toolbar: [{ name: 'create', text: 'New Transfer' }],
        columns: [
            //{title:"#",template:"#= ++count #"},
            { field: "ledgerId", title: "Ac/ Ledger", editor: accountLedgerDropDownEditor, template: "#= getAccountLedger(ledgerId) #" },
            { field: "amount", title: "Amount" },
            { field: "exchangeRateId", title: "Currency", editor: currencyDropDownEditor, template: "#= getCurrency(exchangeRateId) #" },
            { field: "chequeNo", title: "Cheque No." },
            { field: "chequeDate", title: "Cheque Date" },
            { field: "memo", title: "Memo" },
            {
                command: [{ name: 'edit', text: { edit: "", update: "", cancel: "" } }, { name: 'destroy', text: '' }]
            }],
        editable: "incell",
        save: function (e) {
            var datasource = $("#BankTransferKendoGrid").data().kendoGrid.dataSource.view();
            getTotalAmount(datasource);
            //console.log(datasource);
        },
        remove: function (e) {
            var datasource = $("#BankTransferKendoGrid").data().kendoGrid.dataSource.view();
            var indexToRemove = getKendoLineItemIndex(datasource, e.model);
            datasource.splice(indexToRemove, 1);
            getTotalAmount(datasource);
        }
    });
}
function accountLedgerDropDownEditor(container, options) {
    $('<input required name="' + options.field + '" data-text-field="ledgerName" data-value-field="ledgerId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: {
                data: accounts
            },
            template: '<span>#: ledgerName #</span>',
            filter: "contains",
        });
}
function currencyDropDownEditor(container, options) {
    $('<input required name="' + options.field + '" data-text-field="currencyName" data-value-field="exchangeRateId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "currencyName",
            dataValueField: "exchangeRateId",
            dataSource: {
                data: currency
            },
            template: '<span>#: currencyName #</span>',
            filter: "contains",
        });
}


function getAccountLedger(ledgerId) {
    for (i = 0; i < accounts.length; i++) {
        if (accounts[i].ledgerId == ledgerId) {
            return accounts[i].ledgerName;
        }
    }
    return "";
}
function getCurrency(exchangeRateId) {
    for (i = 0; i < currency.length; i++) {
        if (currency[i].exchangeRateId == exchangeRateId) {
            return currency[i].currencyName;
        }
    }
    return "";
}

function getTotalAmount(obj) {
    var output = 0.0;
    for (i = 0; i < obj.length; i++) {
        output = output + parseFloat(obj[i].amount);
    }
    totalAmount = output
    $("#totalAmount").val(Utilities.FormatCurrency(totalAmount));
}
function getKendoLineItemIndex(datasource, objectToFindIndex)    //function was created to remove lineitem from row wen its removed from
{                                                               //kendo grid
    for (i = 0; i < datasource.length; i++) {
        if (objectToFindIndex.id == datasource[i].id) {
            return i;
        }
    }
    return -1;
}