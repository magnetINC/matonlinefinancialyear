﻿var bankTransferList = [];
var bankOrCash = [];

var table = "";

$(function () {
    getLookUps();

    $("#search").click(function () {
        var ledgerName = "";
        if($("#ledgers").data("kendoDropDownList").text() == "" || $("#ledgers").data("kendoDropDownList").text() == null)
        {
            ledgerName == "All";
        }
        else
        {
            ledgerName = $("#ledgers").data("kendoDropDownList").text();
        }
        getBankRegisters($("#fromDate").val(), $("#toDate").val(), "", ledgerName, $("#type").val());
    });
});

//=============== API CALLS ==============
function getLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BankTransfer/LoadLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            bankOrCash = data.CahsOrBank;

            renderLookUpDataToControls();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getBankRegisters(fdt, tdt, formNo, lname, type) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BankTransfer/GetListForRegister?fromDate=" + fdt + "&toDate=" + tdt + "&ledgerName=" + lname + "&formNo=" + formNo + "&type=" + type,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            bankTransferList = data.Response

            renderBankTransferRegisterToTable();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

//================ RENDER DATA TO CONTROLS ===========
function renderLookUpDataToControls()
{
    $("#ledgers").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: bankOrCash,
        optionLabel: "All"
    });
}
function renderBankTransferRegisterToTable() {
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }
    $.each(bankTransferList, function (count, row) {
        var stText = "";
        if (row.type == "Deposit")
        {
            stText = '<span class="label label-success">Deposit</span>';
        }
        else
        {
            stText = '<span class="label label-danger">Withdrawal</span>';
        }
        objToShow.push([
            count + 1,
            row.ledgerName,
            row.invoiceNo,
            row.date,
            '&#8358;' + Utilities.FormatCurrency(row.amount),
            stText,
            row.narration
        ]);
    });

    table = $('#BankTransferRegisterTable').DataTable({
        //scrollY: "500px",
        //scrollX: true,
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true 
    });
}
