﻿var currencies = [];
var ledgers = [];
var currencyDate = "";

$(function () {        
    getTransferDetails();
});

function getTransferDetails() {
    //bankTransferParam is defined and populated on the EditBankTransfer.cshtml view
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AccountLedgerTransactionDetails/GetBankTransferDetails",
        type: "POST",
        data: JSON.stringify(bankTransferParam),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            currencyDate = data.infoContraMaster.Date;
            getDropdownData(data);
            
        },
        error: function (err) {
            Utilities.Alert("Sorry, details could not be loaded.\nYou may want to check your internet connection.");
            Utilities.Loader.Hide();
        }
    });
}

function getDropdownData(data)
{
    var currencyAjax = $.ajax({
        url: API_BASE_URL + "/BankTransfer/GetCurrencyByDate?date=" + currencyDate,
        type: "GET",
        contentType: "application/json"
    });

    var ledgerAjax = $.ajax({
        url: API_BASE_URL + "/AccountLedger/GetAccountLedgers",
        type: "GET",
        contentType: "application/json"
    });

    $.when(currencyAjax, ledgerAjax)
    .done(function (dataCurrency, dataLedger) {
        currencies = dataCurrency[2].responseJSON;
        ledgers = dataLedger[2].responseJSON;
        console.log(currencies);
        console.log(ledgers);

        var currencyHtml = "";
        $.each(currencies, function (count, record) {
            currencyHtml += '<option value="'+record.exchangeRateId+'">'+record.currencyName+'</option>';
        });
        $("#currency").html(currencyHtml);

        var ledgersHtml = "";
        $.each(ledgers, function (count, record) {
            ledgersHtml += '<option value="' + record.ledgerId + '">' + record.ledgerName + '</option>';
        });
        $("#source").html(ledgersHtml);

        //
        $("#transactionType").val(data.infoContraMaster.Type);
        $("#transactionDate").val(Utilities.FormatJsonDate(data.infoContraMaster.Date));
        $("#source :selected").val(data.dtbl[0].ledgerId);
        $("#source :selected").text(findLedger(data.dtbl[0].ledgerId));
        $("#balance").val(data.balance.toFixed(2));
        //$("#currency").val();
        $("#amount").val(data.dtbl[0].amount.toFixed(2));
        $("#chequeNo").val(data.dtbl[0].chequeNo);
        $("#chequeDate").val((data.dtbl[0].chequeDate == "01 Jan 1753") ? "" : data.dtbl[0].chequeDate);
        $("#memo").val(data.dtbl[0].Memo);
    });
    Utilities.Loader.Hide();
}

function findLedger(ledgerId)
{
    var output = "";
    $.each(ledgers, function (count, record) {
        if(record.ledgerId==ledgerId)
        {
            output = record.ledgerName;
        }
    });
    return output;
}