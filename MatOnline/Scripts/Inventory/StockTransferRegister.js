﻿var accountLedgers = [];
var currencies = [];
var finishedGoods = [];
var allProducts = [];
var allStores = [];
var allRacks = [];
var allUnits = [];
var allBatches = [];
var cashOrBanks = [];
var pj = {};
var prod = [];
var cons = [];
var addCost = [];

$(function () {
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });

    getLookup();
});

function getLookup() {
    Utilities.Loader.Show();
    //var lookUpAjax = $.ajax({
    //    url: API_BASE_URL + "/StockJournal/GetLookups",
    //    type: "Get",
    //    contentType: "application/json",
    //});
    //$.when(lookUpAjax)
    //.done(function (dataLookUp) {
    //    orderListForSearch = dataMyLocation[2].responseJSON;
    //});
    var lookupsAjax = $.ajax({
        url: API_BASE_URL + "/StockJournal/GetLookups",
        type: "GET",
        contentType: "application/json",
    });
    $.when(lookupsAjax)
    .done(function (lookups) {
        console.log(lookups);
        finishedGoods = lookups.FinishedGoods;
        currencies = lookups.Currencies;
        allProducts = lookups.AllProducts;
        allStores = lookups.AllStores;
        allUnits = lookups.AllUnits;
        allRacks = lookups.AllRacks;
        allBatches = lookups.AllBatches;
        cashOrBanks = lookups.CashOrBanks;
        accountLedgers = lookups.AccountLedgers;
        allUsers = lookups.users;

        getStockTransfer("2017-01-01", $("#toDate").val(), "");

        $("#finishedGoods").html('<option value=""></option>' + Utilities.PopulateDropDownFromArray(finishedGoods, 1, 0));
        $("#currency").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));
        $("#bankOrCash").html(Utilities.PopulateDropDownFromArray(cashOrBanks, 1, 0));

        renderAdditionalCostGrid();
        renderTransferSourceGrid();
        renderStockAdjustmentGrid();

        //Utilities.Loader.Hide();
    });
}
function getStockTransfer(fromDate, toDate, status) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetStockJournals?fromDate=" + fromDate + "&toDate=" + toDate + "&status=" + status,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            pendingJournals = data.Table;
            var objToShow = [];
            var statusOutput = "";

            $.each(pendingJournals, function (count, row) {
                var statusOutput = "";
                if (row.status == "Pending") {
                    statusOutput = '<span class="label label-warning"> Pending</span>';
                }
                else if (row.status == "In-Transit") {
                    statusOutput = '<span class="label label-primary"> In-Transit</span>';
                }
                else if (row.status == "Delivered") {
                    statusOutput = '<span class="label label-success"> Delivered</span>';
                }
                else if (row.status == "Cancelled") {
                    statusOutput = '<span class="label label-danger"> Cancelled</span>';
                }
                objToShow.push([
                    count + 1,
                    row.formNo,
                    Utilities.FormatJsonDate(row.date),
                    row.narration == "" ? "NA" : row.narration,
                    ((findUsers(row.userId).userName) == "" ? "NA" : (findUsers(row.userId).userName)),
                    statusOutput,
                    '<button type="button" class="btn btn-primary btn-sm" onclick="getStockTransferDetails(' + row.stockJournalMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                ]);
            });
            table = $('#stockJournalListTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}
function getStockTransferDetails(id) {
    Utilities.Loader.Show();
    for (i = 0; i < pendingJournals.length; i++) {
        if (pendingJournals[i].stockJournalMasterId == id) {
            pj = pendingJournals[i];
        }
    }
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetStockJournalDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            prod = data.production;
            cons = data.consumption;
            addCost = data.addCost;
            ledgers = data.ledger;
            var amount = 0;
            console.log(cons);
            var statusOutput = "";
            if (pj.status == "Pending") {
                $("#editTransfer").show();
                $("#deleteTransfer").hide();
                statusOutput = '<span class="label label-warning"> Pending</span>';
            }
            else if (pj.status == "In-Transit") {
                $("#editTransfer").hide();
                $("#deleteTransfer").hide();
                statusOutput = '<span class="label label-primary"> In-Transit</span>';
            }
            else if (pj.status == "Delivered") {
                $("#editTransfer").hide();
                $("#deleteTransfer").hide();
                statusOutput = '<span class="label label-success"> Delivered</span>';
            }
            else if (pj.status == "Cancelled") {
                $("#editTransfer").show();
                $("#deleteTransfer").hide();
                statusOutput = '<span class="label label-danger"> Cancelled</span>';
            }
            $("#narrationDiv").html(pj.narration);
            $("#dateDiv").html(Utilities.FormatJsonDate(pj.date));
            $("#statusDiv").html(statusOutput);

            var outputCons = "";
            var outputProd = "";
            var outputAddCost = "";
            $.each(cons, function (count, row) {
                amount = amount + row.amount;
                outputCons += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                        </tr>';
            });
            $.each(prod, function (count, row) {
                outputProd += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                        </tr>';
            });
            //$.each(addCost, function (count, row) {
            //    outputAddCost += '<tr>\
            //                <td>'+ (count + 1) + '</td>\
            //                <td>' + findLedger(row.ledgerId).ledgerName + '</td>\
            //                <td>&#8358;' + Utilities.FormatCurrency(row.debit) + '</td>\
            //                <td>&#8358;' + Utilities.FormatCurrency(row.credit) + '</td>\
            //            </tr>';
            //});

            //$("#AmountTxt").val(Utilities.FormatCurrency(amount));
            //$("#addCostTxt").val(Utilities.FormatCurrency(pj.additionalCost));
            $("#grandTotalTxt").val(Utilities.FormatCurrency(amount + pj.additionalCost));

            $("#availableQuantityDiv").html("");
            $("#detailsTbodyConsumption").html(outputCons);
            $("#detailsTbodyProduction").html(outputProd);
            $("#detailsTbodyAdditionalCost").html(outputAddCost);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getStockTransferPend(fromDate, toDate, status) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetPendingStockJournals?fromDate=" + fromDate + "&toDate=" + toDate + "&status=" + status,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            pendingJournals = data.Table;
            var objToShow = [];
            var statusOutput = "";

            $.each(pendingJournals, function (count, row) {
                var statusOutput = "";
                if (row.status == "Pending") {
                    statusOutput = '<span class="label label-warning"> Pending</span>';
                }
                else if (row.status == "In-Transit") {
                    statusOutput = '<span class="label label-primary"> In-Transit</span>';
                }
                else if (row.status == "Delivered") {
                    statusOutput = '<span class="label label-success"> Delivered</span>';
                }
                else if (row.status == "Cancelled") {
                    statusOutput = '<span class="label label-danger"> Cancelled</span>';
                }
                objToShow.push([
                    count + 1,
                    row.formNo,
                    Utilities.FormatJsonDate(row.date),
                    row.narration == "" ? "NA" : row.narration,
                    ((findUsers(row.userId).userName) == "" ? "NA" : (findUsers(row.userId).userName)),
                    statusOutput,
                    '<button type="button" class="btn btn-primary btn-sm" onclick="getStockTransferDetails(' + row.stockJournalMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                ]);
            });
            table = $('#stockJournalListTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}
function getStockTransferDetailsPend(id) {
    Utilities.Loader.Show();
    for (i = 0; i < pendingJournals.length; i++) {
        if (pendingJournals[i].stockJournalMasterId == id) {
            pj = pendingJournals[i];
        }
    }
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetPendingStockJournalDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            prod = data.production;
            cons = data.consumption;
            addCost = data.addCost;
            ledgers = data.ledger;
            var amount = 0;
            console.log(cons);
            var statusOutput = "";
            if (pj.status == "Pending") {
                $("#editTransfer").show();
                $("#deleteTransfer").hide();
                statusOutput = '<span class="label label-warning"> Pending</span>';
            }
            else if (pj.status == "In-Transit") {
                $("#editTransfer").hide();
                $("#deleteTransfer").hide();
                statusOutput = '<span class="label label-primary"> In-Transit</span>';
            }
            else if (pj.status == "Delivered") {
                $("#editTransfer").hide();
                $("#deleteTransfer").hide();
                statusOutput = '<span class="label label-success"> Delivered</span>';
            }
            else if (pj.status == "Cancelled") {
                $("#editTransfer").show();
                $("#deleteTransfer").hide();
                statusOutput = '<span class="label label-danger"> Cancelled</span>';
            }
            $("#narrationDiv").html(pj.narration);
            $("#dateDiv").html(Utilities.FormatJsonDate(pj.date));
            $("#statusDiv").html(statusOutput);

            var outputCons = "";
            var outputProd = "";
            var outputAddCost = "";
            $.each(cons, function (count, row) {
                amount = amount + row.amount;
                outputCons += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                        </tr>';
            });
            $.each(prod, function (count, row) {
                outputProd += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                        </tr>';
            });
            //$.each(addCost, function (count, row) {
            //    outputAddCost += '<tr>\
            //                <td>'+ (count + 1) + '</td>\
            //                <td>' + findLedger(row.ledgerId).ledgerName + '</td>\
            //                <td>&#8358;' + Utilities.FormatCurrency(row.debit) + '</td>\
            //                <td>&#8358;' + Utilities.FormatCurrency(row.credit) + '</td>\
            //            </tr>';
            //});

            //$("#AmountTxt").val(Utilities.FormatCurrency(amount));
            //$("#addCostTxt").val(Utilities.FormatCurrency(pj.additionalCost));
            $("#grandTotalTxt").val(Utilities.FormatCurrency(amount + pj.additionalCost));

            $("#availableQuantityDiv").html("");
            $("#detailsTbodyConsumption").html(outputCons);
            $("#detailsTbodyProduction").html(outputProd);
            $("#detailsTbodyAdditionalCost").html(outputAddCost);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function editTransfer()
{
    window.location = "/Inventory/StockJournal/StockTransferEdit/" + pj.stockJournalMasterId;
}
function deleteTransfer()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/DeleteStockTransfer?id=" + pj.stockJournalMasterId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            if (data > 0) {
                Utilities.SuccessNotification("Stock Transfer Deleted Successfully!");  //clear controls
                Utilities.Loader.Hide();
                $("#detailsModal").modal("hide");
                getPendingStockJournal("2017-01-01", $("#toDate").val(), "");
            }
            else {
                Utilities.ErrorNotification("Stock Transfer couldn't delete");  //clear controls
                Utilities.Loader.Hide();
            }
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function findProductByProductId(productId) {
    var output = "N/A";
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productId == productId) {
            output = allProducts[i];
        }
    }
    return output;
}
function findStore(storeId) {
    var output = {};
    for (i = 0; i < allStores.length; i++) {
        if (allStores[i].godownId == storeId) {
            output = allStores[i];
            break;
        }
    }
    return output;
}
function findLedger(ledgerId) {
    var output = "N/A";
    for (i = 0; i < ledgers.length; i++) {
        if (ledgers[i].ledgerId == ledgerId) {
            output = ledgers[i];
        }
    }
    return output;
}
function findUsers(userId) {
    var output = {};;
    for (i = 0; i < allUsers.length; i++) {
        if (allUsers[i].userId == userId) {
            output = allUsers[i];
            break;
        }
    }
    return output;
}
