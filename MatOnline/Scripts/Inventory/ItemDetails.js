﻿var product = {};
var productIdToDelete = 0;
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created

$(function () {
    Utilities.Loader.Show();
    getProduct();
    quantityOnHand();
});

function getProduct() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProduct?productId="+productId,
        type: 'POST',
        contentType: "application/json",
        success: function (data) {
            product = data;
            console.log(product);

            $("#page-title").hide();
            $("#serviceType").html(product.ProductType);
            $("#productName").html(product.ProductName);
            $("#productTitle").html("<b>Item Name - </b>" + product.ProductName);
            $("#minimumStock").html(product.MinStock);
            $("#brand").html(product.Brand);

            $("#productCode").html(product.ProductCode);
            $("#maxStock").html(product.MaxStock);
            $("#size").html(product.Size);
            $("#modelNo").html(product.ModelNo);

            $("#purchaseRate").html(Utilities.FormatCurrency(product.PurchaseRate));
            $("#reorderLevel").html(product.ReorderLevel);
            $("#mrp").html(product.MRP);

            $("#salesRate").html(Utilities.FormatCurrency(product.SalesRate));
            $("#defaultRack").html(product.Rack);
            $("#bom").html(product.Bom);

            $("#unit").html(product.Unit);
            $("#store").html(product.Store);
            $("#multipleUnit").html(product.MultipleUnit);

            $("#expAcct").html(product.ExpenseAccount);
            $("#tax").html(product.Tax);
            $("#allowBatch").html(product.IsBatch);

            $("#salesAcct").html(product.SalesAccount);
            $("#taxApplicable").html(product.ApplicableOn);
            $("#group").html(product.Group);

            $("#openingStock").html(product.Isopeningstock);
            $("#effectiveDate").html(product.EffectiveDate);
            $("#barcode").html(product.Barcode);
            $("#narration").html(product.Narration==""?"N/A" :product.Narration);
            
            Utilities.Loader.Hide();
        },
        error: function (request, error) {
            Utilities.Loader.Hide();
        }
    });
}
function quantityOnHand()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/ProductStockCard?productId=" + productId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            var locations = data.StockCardLocations;
            var quantityOnHandHtml = "";
            var totalQtyOnHand = 0;
            var totalSalesOrder = 0;
            for (i = 0; i < locations.length; i++) {
                totalQtyOnHand = totalQtyOnHand + parseFloat(locations[i].QuantityOnHand);
            }
            $("#quantityInStock").html("<p>Quantity On Hand - <b>" + Utilities.FormatCurrency(totalQtyOnHand) + " " + data.StockCardLocations[0].UnitName + "</b></p>");
            console.log(data);
            debugger
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function findProduct(id) {
    var product = "";
    for (i = 0; i < products.length; i++) {
        if (products[i].ProductId == id) {
            product = products[i];
            break;
        }
    }
    return product;
}