﻿var allProjects = [];
var allCategories = [];
var allRawMaterials = [];
var allUnitConversionItems = [];
var filteredBatches = [];
var toogleSearchProduct = false;

var batch = [];
var racks = [];
var stores = [];

$("#PanelForTransferFormNo").hide();

$(function () {

    //fill category
    GetLookUps();
    getGetAllRawMaterialLookup();
    getFarmBuildItemsLookUp();
    renderSimpleTransferSourceGrid();
    renderSimpleStockAdjustmentGrid();

});

function GetLookUps()
{
    Utilities.Loader.Show();

    var lookUpsAjax = $.ajax({
        url: API_BASE_URL + "/EditItem/GetLooks",
        type: "Get",
        contentType: "application/json",
    });

    $.when(lookUpsAjax)
        .done(function (dataLookUps) {

           
            stores = dataLookUps.Stores;
            racks = dataLookUps.Racks;
            batch = dataLookUps.Batch;
           
            Utilities.Loader.Hide();
        });
}



function getGetAllRawMaterialLookup() {

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetAllRawMaterial",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            // console.log(data);
            allRawMaterials = data[0];
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function setHarvestSettings() {
    stockJournalAction = "Manufacturing";
    $("#additionalCostAmtDiv").show();
    $("#additionalCostAmountGrid").show();
    $("#additionalCostDiv").show();
    $("#additionalCostTitle").show();
    $("#PanelForTransferFormNo").hide();
    $("#formNoTransfer").hide();
    $("#PanelForFormNo").show();
    $("#formNo").show();
    $("#itemDiv").show();
    $("#finishedGoods").show();
}

function setTransferSettings() {
    stockJournalAction = "Stock Transfer";
    $("#additionalCostAmtDiv").show();
    $("#additionalCostAmountGrid").show();
    $("#additionalCostDiv").show();
    $("#additionalCostTitle").show();
    $("#itemDiv").hide();
    $("#finishedGoods").hide();
    $("#PanelForFormNo").hide();
    $("#formNo").hide();
    $("#PanelForTransferFormNo").show();
    $("#formNoTransfer").show();
}

function setStockAdjustmentSettings() {
    stockJournalAction = "Stock Out";
    $("#additionalCostAmtDiv").hide();
    $("#additionalCostAmountGrid").hide();
    $("#additionalCostDiv").hide();
    $("#additionalCostTitle").hide();
    $("#bankOrCashDiv").hide();
    $("#PanelForTransferFormNo").hide();
    $("#formNoTransfer").hide();
    $("#PanelForFormNo").show();
    $("#formNo").show();
    $("#itemDiv").hide();
    $("#finishedGoods").hide();
}

$("#finishedGoods").select2();

var setWaitInterval = setInterval(waitTillAllBatchesLoad, 100);

function waitTillAllBatchesLoad() {
    if (allBatches.length != 0) {
        filteredBatches = allBatches;
        clearInterval(setWaitInterval);
    }
}

GetLookUps();
getGetAllRawMaterialLookup();
getFarmBuildItemsLookUp();
renderSimpleTransferSourceGrid();
renderSimpleStockAdjustmentGrid();

function getFarmBuildItemsLookUp()
{

   /* Utilities.Loader.Show();*/
    var projectLookupAjax = $.ajax({
        url: API_BASE_URL + "/Project/GetProject",
        type: "GET",
        contentType: "application/json",
    });

    var categoryLookupAjax = $.ajax({
        url: API_BASE_URL + "/Category/GetCategory",
        type: "GET",
        contentType: "application/json",
    });

    var rawMaterialLookupAjax = $.ajax({
        url: API_BASE_URL + "/StockJournal/GetAllRawMaterial",
        type: "GET",
        contentType: "application/json",
    });

    var unitConversionLookupAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetUnitConversionItems",
        type: "GET",
        contentType: "application/json",
    });

    $.when(projectLookupAjax, categoryLookupAjax, rawMaterialLookupAjax, unitConversionLookupAjax)
        .done(function (dataProject, dataCategory, dataRawMaterial, dataUnitConversion)
        {
            allProjects = dataProject[0];
            allCategories = dataCategory[0];
            allRawMaterials = dataRawMaterial[0];
            allUnitConversionItems = dataUnitConversion[0].UnitCoversionItems;
          
        });
}

function renderSimpleTransferSourceGrid() {

    $("#simpleTransferSourceGrid").kendoGrid({
        dataSource: {
            transport: {
                read: function (entries) {
                    entries.success(transferSource);
                },
                create: function (entries) {
                    entries.success(entries.data);
                },
                update: function (entries) {
                    entries.success();
                },
                destroy: function (entries) {
                    entries.success();
                },
                parameterMap: function (data) { return JSON.stringify(data); }
            },
            schema: {
                model: {
                    id: "productId",
                    fields: {
                        productName: { editable: true, validation: { required: false } },
                        productCode: { editable: true, validation: { required: false } },
                        qty: { editable: true, validation: { required: false, type: "number" } },
                        unitId: { editable: true, validation: { required: false } },
                        godownId: { editable: true, validation: { required: false } },
                        qtyInStore: { editable: true, validation: { required: false, type: "number" } },
                        qtyAvailable: { editable: true, validation: { required: false, type: "number" } },
                        batchId: { editable: true, validation: { required: true, type: "number" } },
                        projectId: { editable: true, validation: { required: true, type: "number" } },
                        categoryId: { editable: true, validation: { required: true, type: "number" } },
                    }
                }
            },
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 15,
            pageSizes: [5, 10, 20, 50, 100],
            previousNext: true,
            buttonCount: 5,
        },
        toolbar: [{ name: 'create', text: 'Add Item' }],
        columns: [
            { field: "productName", title: "Select Product Name", editor: simpleProductNameDropDownEditor, template: "#= findProductByProductName(productName) #" },
            { field: "productCode", title: "Select Product Code", editor: simpleProductCodeDropDownEditor, template: "#= findProductByProductCode(productCode) #" },
            { field: "unitId", title: "Unit Of Measure" },
            { field: "godownId", title: "Select Location__", editor: storeDropDownEditor, template: "#= showStore(godownId) #" },
            { field: "batchId", title: "Select Batch Number", editor: batchDropDownEditor, template: "#= showBatch(batchId) #" },
            { field: "qty", title: "Enter Quantity", editor: simpleNumericEditor },
            { field: "qtyAvailable", title: "Quantity Available" },
            { field: "qtyInStore", title: "Quantity Available in Store" },
            { field: "projectId", title: "Select Project__", editor: projectDropDownEditor, template: "#= showProject(projectId) #" },
            { field: "categoryId", title: "Select Department", editor: categoryDropDownEditor, template: "#= showCategory(categoryId) #" },
            {
                command: [{ name: 'destroy', text: '' }]
            }],
        editable: "incell",
        save: function (data) {

            toogleSearchProduct = true;
            var datasource = $("#simpleTransferSourceGrid").data().kendoGrid.dataSource.view();
            //console.log(datasource);
            //getTotalSourceTransferCost(datasource);

            //console.log(datasource);
        },
        remove: function (e) {

            toogleSearchProduct = false;
            var datasource = $("#simpleTransferSourceGrid").data().kendoGrid.dataSource.view();
            var indexToRemove = getKendoLineItemIndex(datasource, e.model);
            datasource.splice(indexToRemove, 1);
            getTotalSourceTransferCost(datasource);
        }
    });

}

function renderSimpleTransferDestinationGrid() {

    $("#simpleTransferDestinationGrid").kendoGrid({
        dataSource: {
            transport: {
                read: function (entries) {
                    entries.success(transferDestination);
                    getTotalDestinationTransferCost(transferDestination);
                },
                create: function (entries) {
                    entries.success(entries.data);
                },
                update: function (entries) {
                    entries.success();
                },
                destroy: function (entries) {
                    entries.success();
                },
                parameterMap: function (data) { return JSON.stringify(data); }
            },
            schema: {
                model: {
                    id: "productId",
                    fields: {
                        productCode: { editable: true, validation: { required: false } },
                        productName: { editable: true, validation: { required: false } },
                        qty: { editable: true, validation: { required: false, type: "number" } },
                        unitId: { editable: true, validation: { required: false } },
                        godownId: { editable: true, validation: { required: false } },
                        batchId: { editable: true, validation: { required: true, type: "number" } },
                        projectId: { editable: true, validation: { required: true, type: "number" } },
                        categoryId: { editable: true, validation: { required: true, type: "number" } },
                    }
                }
            },
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 15,
            pageSizes: [5, 10, 20, 50, 100],
            previousNext: true,
            buttonCount: 5,
        },
        columns: [
            { field: "productCode", title: "Product Code", editor: simpleProductCodeDropDownEditor, template: "#= findProductByProductCode(productCode) #" },
            { field: "productName", title: "Product Name", editor: simpleProductNameDropDownEditor, template: "#= findProductByProductName(productName) #" },
            { field: "qty", title: "Quantity", editor: simpleNumericEditor },
            { field: "godownId", title: "Select Location__", editor: storeDropDownEditor, template: "#= showStore(godownId) #" },
            { field: "projectId", title: "Select Project", editor: projectDropDownEditor, template: "#= showProject(projectId) #" },
            { field: "categoryId", title: "Select Department", editor: categoryDropDownEditor, template: "#= showCategory(categoryId) #" },
            { field: "batchId", title: "Batch", editor: batchDropDownEditor, template: "#= showBatch(batchId) #" },
            {
                command: [{ name: 'destroy', text: '' }]
            }],
        editable: "incell",
        save: function (data) {
            toogleSearchProduct = false;
            var datasource = $("#simpleTransferDestinationGrid").data().kendoGrid.dataSource.view();
            getTotalDestinationTransferCost(datasource);
            transferDestination = $("#simpleTransferDestinationGrid").data().kendoGrid.dataSource.view();
            //getTotalAdditionalCost(datasource);
            console.log(transferDestination);
            finishedGoodsRawMaterial = [];
            $.each(transferDestination, function (count, row) {
                finishedGoodsRawMaterial.push({
                    productId: findProductIdByProductCode(row.productCode),
                    Qty: row.qty,
                    salesRate: row.rate,
                    unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
                    //UnitConversionId: row.unitconversionId,
                    batchId: row.batchId,
                    godownId: row.godownId,
                    rackId: row.rackId,
                    amount: (row.Qty * row.rate),
                    Slno: (count + 1)
                });
            });
        },
        remove: function (e) {
            toogleSearchProduct = false;
            var datasource = $("#simpleTransferDestinationGrid").data().kendoGrid.dataSource.view();
            var indexToRemove = getKendoLineItemIndex(datasource, e.model);
            datasource.splice(indexToRemove, 1);
            getTotalDestinationTransferCost(datasource);

            finishedGoodsRawMaterial = [];
            $.each(transferDestination, function (count, row) {
                finishedGoodsRawMaterial.push({
                    productId: findProductIdByProductCode(row.productCode),
                    Qty: row.qty,
                    salesRate: row.rate,
                    unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
                    //UnitConversionId: row.unitconversionId,
                    batchId: row.batchId,
                    godownId: row.godownId,
                    rackId: row.rackId,
                    amount: (row.Qty * row.rate),
                    Slno: (count + 1)
                });
            });
        }
    });

}

function searchProduct(filter, searchBy, model) {
    if (toogleSearchProduct) {
        var product1 = null;
       /* debugger*/
        var storeId = model.godownId == "" ? 0 : model.godownId;
        var batchId = model.batchId == "" ? -1 : model.batchId;

        if (model.godownId == undefined || model.godownId == "0" || model.godownId == "") {

            if (searchBy == "ProductCode") {
                product1 = allProducts.find(p => p.productId == model.productId && p.productName == model.productName && p.productCode == model.productCode && p.godownId != "" && p.godownId != "0" && p.productCode == filter);
            }

            if (searchBy == "ProductName") {
                product1 = allProducts.find(p => p.productId == model.productId && p.productName == model.productName && p.productCode == model.productCode && p.godownId != "" && p.godownId != "0" && p.productName == filter);
            }

            if (searchBy == "Barcode") {
                product1 = allProducts.find(p => p.productId == model.productId && p.productName == model.productName && p.productCode == model.productCode && p.godownId != "" && p.godownId != "0" && p.barCode == filter);
            }
        }
        else {

            storeId = model.godownId;
        }

        if (storeId == "" || storeId == undefined || storeId == "0") {

            if (product1 == undefined) {

                Utilities.ErrorNotification("Product not found try again!");
                return;
            }

            if (product1.godownId == undefined || product1.godownId == "0" || product1.godownId == "") {

                Utilities.ErrorNotification("This product does not have store location, try again!");
                return;
            }

            storeId = product1.godownId;
        }

        Utilities.Loader.Show();

        $.ajax({
            url: API_BASE_URL + "/ProductCreation/GetSearchProduct?filter=" + filter + "&searchBy=" + searchBy + "&storeId=" + storeId + "&batchId=" + batchId,
            type: "GET",
            contentType: "application/json",
            success: function (data) {

                if (storeId == 0 && batchId == -1) {
                    model.set("qtyAvailable", Utilities.FormatQuantity(data.QuantityInStock));
                    model.set("qtyInStore", Utilities.FormatQuantity(data.StoreQuantityInStock));
                }

                if (data.QuantityInStock > 0)
                {
                    model.set("qtyAvailable", Utilities.FormatQuantity(data.QuantityInStock));
                }
                else
                {
                    model.set("qtyAvailable", data.QuantityInStock);
                }

                if (data.StoreQuantityInStock > 0)
                {
                    model.set("qtyInStore", Utilities.FormatQuantity(data.StoreQuantityInStock));
                }
                else
                {
                    model.set("qtyInStore", data.StoreQuantityInStock);
                }

                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                Utilities.Loader.Hide();
            }
        });
    }
}

function projectDropDownEditor(container, options) {
    $('<input name="' + options.field + '" class="tst" data-text-field="ProjectName" data-value-field="ProjectId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ProjectName",
            dataValueField: "ProjectId",
            dataSource: {
                data: allProjects
            },
            template: '<span>#: ProjectName #</span>',
            filter: "contains",
            change: function (data) {

                var grid = $("#simpleTransferSourceGrid").data("kendoGrid");
                console.log("model", this);

            }
        });
}

function showProject(projectId) {
    var output = "N/A";
    for (i = 0; i < allProjects.length; i++) {
        if (allProjects[i].ProjectId == projectId) {
            output = allProjects[i].ProjectName;
        }
    }
    return output;
}

function categoryDropDownEditor(container, options) {
    $('<input name="' + options.field + '" class="tst" data-text-field="CategoryName" data-value-field="CategoryId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "CategoryName",
            dataValueField: "CategoryId",
            dataSource: {
                data: allCategories
            },
            template: '<span>#: CategoryName #</span>',
            filter: "contains",
            change: function (data) {

                var grid = $("#simpleTransferSourceGrid").data("kendoGrid");
                console.log("model", this);

            }
        });
}

function showCategory(categoryId) {
    var output = "N/A";
    for (i = 0; i < allCategories.length; i++) {
        if (allCategories[i].CategoryId == categoryId) {
            output = allCategories[i].CategoryName;
        }
    }
    return output;
}

function batchDropDownEditor(container, options) {
    $('<input name="' + options.field + '" class="tst" data-text-field="batchNo" data-value-field="batchId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "batchNo",
            dataValueField: "batchId",
            dataSource: {
                data: filteredBatches
            },
            template: '<span>#: batchNo #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#simpleTransferSourceGrid").data("kendoGrid");
                model = grid.dataItem(this.element.closest("tr"));
                model.set("productId", findSimpleProductByParam("batchId", model.batchId).productId);
                model.set("productCode", findSimpleProductByParam("batchId", model.batchId).productCode);
                model.set("productName", findSimpleProductByParam("batchId", model.batchId).productName);
                model.set("rate", findSimpleProductByParam("batchId", model.batchId).salesRate);
                console.log("model", this);

                searchProduct(model.productCode, "ProductCode", model);

            }
        });
}

function findSimpleProductByParam(paramType, param) {
    var output = {};
    for (i = 0; i < allProducts.length; i++) {
        if (paramType == "productcode") {
            if (allProducts[i].productCode == param) {
                output = allProducts[i];
                break;
            }
        }
        else if (paramType == "barcode") {
            if (allProducts[i].barcode == param) {
                output = allProducts[i];
                break;
            }
        }
        else if (paramType == "productname") {
            if (allProducts[i].productName == param) {
                output = allProducts[i];
                break;
            }
        }
        else if (paramType == "batchId") {
            var batch = filteredBatches.find(p => p.batchId == param);
            var productId = batch != undefined ? batch.productId : 0;
            if (allProducts[i].productId == productId) {
                output = allProducts[i];
                break;
            }
        }
    }
    filteredBatches = allBatches.filter(p => p.productId == output.productId);
    return output;
}

function getFirstBatchOfProduct(paramType, param) {
    var output = {};
    for (i = 0; i < allProducts.length; i++) {
        if (paramType == "productcode") {
            if (allProducts[i].productCode == param) {
                output = allProducts[i];
                break;
            }
        }
        else if (paramType == "barcode") {
            if (allProducts[i].barcode == param) {
                output = allProducts[i];
                break;
            }
        }
        else if (paramType == "productname") {
            if (allProducts[i].productName == param) {
                output = allProducts[i];
                break;
            }
        }
    }
    filteredBatch = allBatches.find(p => p.productId == output.productId).batchId;
    return filteredBatch;
}

function showBatch(batchId) {
    var output = "N/A";
    for (i = 0; i < filteredBatches.length; i++) {
        if (filteredBatches[i].batchId == batchId) {
            output = filteredBatches[i].batchNo;
        }
    }
    return output;
}

function simpleProductCodeDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="productCode" data-value-field="productCode" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productCode",
            dataValueField: "productCode",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productCode #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#simpleTransferSourceGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                model.set("productId", findProductByParam("productcode", model.productCode).productId);
                model.set("barCode", findProductByParam("productcode", model.productCode).productCode);
                model.set("productName", findProductByParam("productcode", model.productCode).productName);
                model.set("rate", findProductByParam("productcode", model.productCode).salesRate);
                model.set("batchId", getFirstBatchOfProduct("productcode", model.productCode));
                var unitID = findProductByParam("productcode", model.productCode).unitId;
                model.set("unitId", allUnits.find(x => x.unitId == unitID).unitName);
                searchProduct(model.productCode, "ProductCode", model);
            }
        });
}

function simpleProductNameDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="productName" data-value-field="productName" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productName",
            dataValueField: "productName",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productName #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#simpleTransferSourceGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                model.set("productId", findSimpleProductByParam("productname", model.productName).productId);
                model.set("barCode", findSimpleProductByParam("productname", model.productName).productCode);
                model.set("productCode", findSimpleProductByParam("productname", model.productName).productCode);
                model.set("rate", findSimpleProductByParam("productname", model.productName).salesRate);
                model.set("batchId", getFirstBatchOfProduct("productcode", model.productCode));
                var unitID = findSimpleProductByParam("productname", model.productName).unitId;
                model.set("unitId", allUnits.find(x => x.unitId == unitID).unitName);
                searchProduct(model.productName, "ProductName", model);
                // model.set("batch", findSimpleProductByParam("productName", model.productName).batchId);
            }
        });
}

function simpleStoreDropDownEditor(container, options) {
    $('<input name="' + options.field + '" class="tst" data-text-field="godownName" data-value-field="godownId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "godownName",
            dataValueField: "godownId",
            dataSource: {
                data: allStores
            },
            template: '<span>#: godownName #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#simpleTransferSourceGrid").data("kendoGrid");
                console.log("model", this);
            }
        });
}

function simpleNumericEditor(container, options) {
    $('<input value="0" name="' + options.field + '" data-text-field="" data-value-field="" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoNumericTextBox({
            min: 0,
            change: function (data) {
                var grid = $("#simpleTransferSourceGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                model.set("amount", model.qty * model.rate);
                var datasource = $("#simpleTransferSourceGrid").data().kendoGrid.dataSource.view();
                getTotalSourceTransferCost(datasource);
            }
        });
}

$(function () {

    $("#simpleTransferGoods").click(function () {
        stockJournalDetailsInfoConsumption = [];
        stockJournalDetailsInfoProduction = [];
        additionalCostItems = [];
        stockJournalMasterInfo = {};
        additionalCostInfo = {};
        totalAdditionalCost = 0.0;
        finishedGoodsRawMaterial = [];
        transferDestination = [];
        finishedGoodsItems = [];
        var src = $("#simpleTransferSourceGrid").data().kendoGrid.dataSource.view();
        //console.log(src);
        $.each(src, function (count, row) {
            var product = allProducts.find(p => p.productCode == row.productCode);
            finishedGoodsRawMaterial.push({
                productId: findProductIdByProductCode(row.productCode),
                Qty: row.qty,
                salesRate: product.salesRate,
                unitId: product.unitId,
                //UnitConversionId: row.unitconversionId,
                batchId: row.batchId,
                godownId: row.godownId,
                rackId: product.rackId,
                projectId: row.projectId,
                categoryId: row.categoryId,
                amount: (row.qty * product.salesRate),
                Slno: (count + 1)
            });
            transferDestination.push({
                amount: row.qty * product.salesRate,
                barCode: product.barCode,
                godownId: row.godownId,
                productCode: row.productCode,
                productId: row.productId,
                productName: row.productName,
                projectId: row.projectId,
                categoryId: row.categoryId,
                batchId: row.batchId,
                qty: row.qty,
                rackId: product.rackId,
                rate: product.salesRate,
                unitId: product.unitId
            });
        });
        //transferDestination = src;

        //stockJournalDetailsInfoConsumption = [];//empty the array b4 populating
        $.each(src, function (count, row) {
            var product = allProducts.find(p => p.productCode == row.productCode);
            finishedGoodsItems.push({
                productId: findProductIdByProductCode(row.productCode),
                Qty: row.qty,
                salesRate: row.rate,
                unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
                //UnitConversionId: row.unitconversionId,
                projectId: row.projectId,
                categoryId: row.categoryId,
                batchId: row.batchId,
                godownId: row.godownId,
                rackId: product.rackId,
                amount: (row.Qty * product.salesRate),
                Slno: (count + 1)
            });
        });
        renderSimpleTransferDestinationGrid();

        //$("#transferDestinationGrid").data("kendoGrid").setDataSource(src);
        //console.log(src);


        var datasource = $("#simpleTransferDestinationGrid").data().kendoGrid.dataSource.view();
        getTotalDestinationTransferCost(datasource);
    });

});

function renderSimpleStockAdjustmentGrid() {

    $("#simpleStockAdjustmentGrid").kendoGrid({
        dataSource: {
            transport: {
                read: function (entries) {
                    entries.success(transferSource);
                },
                create: function (entries) {
                    entries.success(entries.data);
                },
                update: function (entries) {
                    entries.success();
                },
                destroy: function (entries) {
                    entries.success();
                },
                parameterMap: function (data) { return JSON.stringify(data); }
            },
            schema: {
                model: {
                    id: "productId",
                    fields: {
                        productCode: { editable: true, validation: { required: false } },
                        productName: { editable: true, validation: { required: false } },
                        qty: { editable: true, validation: { required: false, type: "number" } },
                        unitId: { editable: true, validation: { required: false } },
                        godownId: { editable: true, validation: { required: false } },
                        qtyInStore: { editable: true, validation: { required: false, type: "number" } },
                        qtyAvailable: { editable: true, validation: { required: false, type: "number" } },
                        batchId: { editable: true, validation: { required: true, type: "number" } },
                        projectId: { editable: true, validation: { required: true, type: "number" } },
                        categoryId: { editable: true, validation: { required: true, type: "number" } },
                    }
                }
            },
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 15,
            pageSizes: [5, 10, 20, 50, 100],
            previousNext: true,
            buttonCount: 5,
        },

        toolbar: [{ name: 'create', text: 'Add Item' }],
        columns: [
            { field: "productName", title: "Select Product Name", editor: simpleProductNameDropDownEditor2, template: "#= findProductByProductName(productName) #" },
            { field: "productCode", title: "Select Product Code", editor: simpleProductCodeDropDownEditor2, template: "#= findProductByProductCode(productCode) #" },
            { field: "unitId", title: "Unit Of Measure" },
            { field: "godownId", title: "Select Location__", editor: simpleStoreDropDownEditor2, template: "#= showStore(godownId) #" },
            { field: "batchId", title: "Select Batch Number", editor: batchDropDownEditor2, template: "#= showBatch(batchId) #" },
            { field: "qty", title: "Enter the Quantity", editor: simpleNumericEditor2 },
            { field: "qtyAvailable", title: "Quantity Available" },
            { field: "qtyInStore", title: "Quantity Available in Store" },
            { field: "projectId", title: "Select Project__", editor: projectDropDownEditor2, template: "#= showProject(projectId) #" },
            { field: "categoryId", title: "Select Department", editor: categoryDropDownEditor2, template: "#= showCategory(categoryId) #" },
            {
                command: [{ name: 'destroy', text: '' }]
            }],
        editable: "incell",
        save: function (data) {
            toogleSearchProduct = true;
            var datasource = $("#simpleStockAdjustmentGrid").data().kendoGrid.dataSource.view();
            //console.log(datasource);
            //getTotalSourceTransferCost(datasource);

            //console.log(datasource);
        },
        remove: function (e) {
            var datasource = $("#simpleStockAdjustmentGrid").data().kendoGrid.dataSource.view();
            var indexToRemove = getKendoLineItemIndex(datasource, e.model);
            datasource.splice(indexToRemove, 1);
            getTotalSourceTransferCost(datasource);
        }
    });

}

function projectDropDownEditor2(container, options) {
    $('<input name="' + options.field + '" class="tst" data-text-field="ProjectName" data-value-field="ProjectId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ProjectName",
            dataValueField: "ProjectId",
            dataSource: {
                data: allProjects
            },
            template: '<span>#: ProjectName #</span>',
            filter: "contains",
            change: function (data) {

                var grid = $("#simpleStockAdjustmentSourceGrid").data("kendoGrid");
                console.log("model", this);

            }
        });
}

function categoryDropDownEditor2(container, options) {
    $('<input name="' + options.field + '" class="tst" data-text-field="CategoryName" data-value-field="CategoryId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "CategoryName",
            dataValueField: "CategoryId",
            dataSource: {
                data: allCategories
            },
            template: '<span>#: CategoryName #</span>',
            filter: "contains",
            change: function (data) {

                var grid = $("#simpleStockAdjustmentGrid").data("kendoGrid");
                console.log("model", this);

            }
        });
}

function batchDropDownEditor2(container, options) {
    $('<input name="' + options.field + '" class="tst" data-text-field="batchNo" data-value-field="batchId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "batchNo",
            dataValueField: "batchId",
            dataSource: {
                data: filteredBatches
            },
            template: '<span>#: batchNo #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#simpleStockAdjustmentGrid").data("kendoGrid");
                model = grid.dataItem(this.element.closest("tr"));
                model.set("productId", findSimpleProductByParam("batchId", model.batchId).productId);
                model.set("productCode", findSimpleProductByParam("batchId", model.batchId).productCode);
                model.set("productName", findSimpleProductByParam("batchId", model.batchId).productName);
                model.set("rate", findSimpleProductByParam("batchId", model.batchId).salesRate);
                searchProduct(model.productCode, "ProductCode", model);
                console.log("model", this);
            }
        });
}

function simpleProductCodeDropDownEditor2(container, options) {
    $('<input name="' + options.field + '" data-text-field="productCode" data-value-field="productCode" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productCode",
            dataValueField: "productCode",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productCode #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#simpleStockAdjustmentGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                model.set("productId", findProductByParam("productcode", model.productCode).productId);
                model.set("barCode", findProductByParam("productcode", model.productCode).productCode);
                model.set("productName", findProductByParam("productcode", model.productCode).productName);
                model.set("rate", findProductByParam("productcode", model.productCode).salesRate);
                model.set("batchId", getFirstBatchOfProduct("productcode", model.productCode));
                var unitID = findProductByParam("productcode", model.productCode).unitId;
                model.set("unitId", allUnits.find(x => x.unitId == unitID).unitName);
                searchProduct(model.productCode, "ProductCode", model);
            }
        });
}

function simpleProductNameDropDownEditor2(container, options) {
    $('<input name="' + options.field + '" data-text-field="productName" data-value-field="productName" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productName",
            dataValueField: "productName",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productName #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#simpleStockAdjustmentGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                model.set("productId", findSimpleProductByParam("productname", model.productName).productId);
                model.set("barCode", findSimpleProductByParam("productname", model.productName).productCode);
                model.set("productCode", findSimpleProductByParam("productname", model.productName).productCode);
                model.set("rate", findSimpleProductByParam("productname", model.productName).salesRate);
                model.set("batchId", getFirstBatchOfProduct("productcode", model.productCode));
                var unitID = findSimpleProductByParam("productname", model.productName).unitId;
                model.set("unitId", allUnits.find(x => x.unitId == unitID).unitName);
                searchProduct(model.productName, "ProductName", model);
                // model.set("batch", findSimpleProductByParam("productName", model.productName).batchId);
            }
        });
}

function simpleStoreDropDownEditor2(container, options) {
    $('<input name="' + options.field + '" class="tst" data-text-field="godownName" data-value-field="godownId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "godownName",
            dataValueField: "godownId",
            dataSource: {
                data: allStores
            },
            template: '<span>#: godownName #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#simpleStockAdjustmentGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                console.log("model", this);
                searchProduct(model.productCode, "ProductCode", model);
            }
        });
}

function simpleNumericEditor2(container, options) {
    $('<input value="0" name="' + options.field + '" data-text-field="" data-value-field="" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoNumericTextBox({
            min: 0,
            change: function (data) {
                var grid = $("#simpleStockAdjustmentGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                model.set("amount", model.qty * model.rate);
                var datasource = $("#simpleStockAdjustmentGrid").data().kendoGrid.dataSource.view();
                getTotalSourceTransferCost(datasource);
            }
        });
}

function setItemsForStockAdjustment()
{

    try {


        stockJournalDetailsInfoConsumption = [];
        stockJournalDetailsInfoProduction = [];
        additionalCostItems = [];
        stockJournalMasterInfo = {};
        additionalCostInfo = {};
        totalAdditionalCost = 0.0;
        finishedGoodsRawMaterial = [];
        transferDestination = [];
        finishedGoodsItems = [];
        var src = $("#simpleStockAdjustmentGrid").data().kendoGrid.dataSource.view();
        //console.log(src);
        $.each(src, function (count, row)
        {
           /* debugger;*/
            var product = allProducts.find(p => p.productCode == row.productCode);
            var isRawMaterial = allRawMaterials.find(p => p.Raw.productId == product.productId);

            if (row.qty == "" || row.qty == undefined) {
                row.qty = "0";
            }

            if (row.godownId == "" || row.godownId == undefined)
            {
                row.godownId = product.godownId;
            }

            var productq = allProducts.find(p => p.godownId == row.godownId);

            if (productq == undefined)
            {
                
                var Sracks = racks.find(p => p.godownId == row.godownId);
                if (Sracks != null && Sracks != undefined) {
                    productq = [];
                    productq.rackId = Sracks.rackId;
                }
                else {
                    Utilities.ErrorNotification("Store location is invaild!");
                    return false;
                }
            }

            if (isRawMaterial == undefined)
            {
                if (stockJournalAction == "Stock Out")
                {
                    finishedGoodsRawMaterial.push({
                        productId: findProductIdByProductCode(row.productCode),
                        Qty: row.qty,
                        QtyAvailable: row.qtyInStore,
                        salesRate: product.salesRate,
                        unitId: product.unitId,
                        //UnitConversionId: row.unitconversionId,
                        batchId: row.batchId,
                        godownId: row.godownId,
                        rackId: productq.rackId,
                        projectId: row.projectId,
                        categoryId: row.categoryId,
                        amount: (row.qty * product.salesRate),
                        Slno: (count + 1),
                        newBatchName: row.batchId,
                        stockJournalAction: "Stock Out",
                    });
                }
            }
            else
            {
                finishedGoodsRawMaterial.push({
                    productId: findProductIdByProductCode(row.productCode),
                    Qty: row.qty,
                    QtyAvailable: row.qtyInStore,
                    salesRate: product.salesRate,
                    unitId: product.unitId,
                    //UnitConversionId: row.unitconversionId,
                    batchId: row.batchId,
                    godownId: row.godownId,
                    rackId: productq.rackId,
                    projectId: row.projectId,
                    categoryId: row.categoryId,
                    amount: (row.qty * product.salesRate),
                    Slno: (count + 1),
                    newBatchName: row.batchId,
                    stockJournalAction: "Stock Out",
                });
            }
            //s s

            $.each(finishedGoodsRawMaterial,
                function (count, row) {
                    var product = allProducts.find(p => p.productId == row.productId);
                    var conversionObj = allUnitConversionItems.find(p => p.productId == product.productId && p.unitId == row.unitId);
                    var conversionRate = conversionObj.conversionRate;
                    var purchaseRate = product.purchaseRate / conversionRate;
                    var amount = purchaseRate * row.Qty;
                    stockJournalDetailsInfoConsumption.push({
                        ProductId: row.productId,
                        Qty: row.Qty,
                        QtyAvailable: row.QtyAvailable,
                        Rate: purchaseRate,
                        UnitId: row.unitId,
                        UnitConversionId: conversionObj.unitconversionId,//row.unitconversionId,
                        BatchId: row.batchId,
                        GodownId: row.godownId,
                        RackId: row.rackId,
                        Amount: amount, //(row.Qty * row.salesRate),
                        Slno: (count + 1),
                        ProjectId: row.ProjectId == undefined ? row.projectId : row.ProjectId,
                        CategoryId: row.CategoryId == undefined ? row.categoryId : row.CategoryId,
                        stockJournalAction: row.stockJournalAction, HasBatchSelected: true
                    });
                });
            //stockJournalDetailsInfoProduction = []; //empty the array b4 populating
            $.each(finishedGoodsItems,
                function (count, row) {
                    // var qt = (stockJournalAction == "Manufacturing" ? $("#quantityToManufacture").val() : row.Qty);
                    var qty = row.qty == undefined ? row.Qty : row.qty;
                    var product = allProducts.find(p => p.productId == row.productId);
                    var conversionObj = allUnitConversionItems.find(p => p.productId == product.productId && p.unitId == row.unitId);
                    var conversionRate = conversionObj.conversionRate;
                    var purchaseRate = product.purchaseRate / conversionRate;
                    var amount = purchaseRate * qty;
                    stockJournalDetailsInfoProduction.push({
                        ProductId: row.productId,
                        Qty: qty,
                        QtyAvailable: row.QtyAvailable,
                        Rate: purchaseRate,
                        UnitId: row.unitId,
                        UnitConversionId: conversionObj.unitconversionId,//row.unitconversionId,
                        BatchId: row.batchId,
                        GodownId: row.godownId,
                        RackId: row.rackId,
                        NewBatchName: row.newBatchName,
                        ProjectId: row.ProjectId == undefined ? row.projectId : row.ProjectId,
                        CategoryId: row.CategoryId == undefined ? row.categoryId : row.CategoryId,
                        Amount: amount,//(row.salesRate * qty),
                        Slno: (count + 1),
                        stockJournalAction: row.stockJournalAction, HasBatchSelected: true
                    });
                });
            //e s
            //transferDestination.push({
            //    amount: row.qty * product.salesRate,
            //    barCode: product.barCode,
            //    godownId: row.godownId,
            //    productCode: row.productCode,
            //    productId: row.productId,
            //    productName: row.productName,
            //    projectId: row.projectId,
            //    categoryId: row.categoryId,
            //    batchId: row.batchId,
            //    qty: row.qty,
            //    rackId: product.rackId,
            //    rate: product.salesRate,
            //    unitId: product.unitId
            //});
        });
   

        return true;

    }
    catch (error) {

        return false;
    }

}

function setItemsForStockTransfer()
{

    var isLocationUnique = true;

    try {

        stockJournalDetailsInfoConsumption = [];
        stockJournalDetailsInfoProduction = [];
        additionalCostItems = [];
        stockJournalMasterInfo = {};
        additionalCostInfo = {};
        totalAdditionalCost = 0.0;
        finishedGoodsRawMaterial = [];
        transferDestination = [];
        finishedGoodsItems = [];
        var srcTransferFrom = $("#simpleTransferSourceGrid").data().kendoGrid.dataSource.view();

        $.each(srcTransferFrom, function (count, row) {
            var product = allProducts.find(p => p.productCode == row.productCode);

            finishedGoodsItems.push({
                productId: findProductIdByProductCode(row.productCode),
                Qty: row.qty,
                salesRate: product.salesRate,
                unitId: product.unitId,
                //UnitConversionId: row.unitconversionId,
                batchId: row.batchId,
                godownId: row.godownId,
                rackId: product.rackId,
                projectId: row.projectId,
                categoryId: row.categoryId,
                amount: (row.qty * product.salesRate),
                Slno: (count + 1)
            });

        });

        //stockJournalDetailsInfoConsumption = [];//empty the array b4 populating
        //$.each(src, function (count, row) {
        //    var product = allProducts.find(p => p.productCode == row.productCode);
        //    finishedGoodsItems.push({
        //        productId: findProductIdByProductCode(row.productCode),
        //        Qty: row.qty,
        //        salesRate: row.rate,
        //        unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
        //        //UnitConversionId: row.unitconversionId,
        //        projectId: row.projectId,
        //        categoryId: row.categoryId,
        //        batchId: row.batchId,
        //        godownId: row.godownId,
        //        rackId: product.rackId,
        //        amount: (row.Qty * product.salesRate),
        //        Slno: (count + 1)
        //    });
        //});
        var srcTransferTo = $("#simpleTransferDestinationGrid").data().kendoGrid.dataSource.view();
        $.each(srcTransferTo, function (count, row) {
            var product = allProducts.find(p => p.productCode == row.productCode);
            finishedGoodsRawMaterial.push({
                productId: findProductIdByProductCode(row.productCode),
                Qty: row.qty,
                salesRate: product.salesRate,
                unitId: product.unitId,
                //UnitConversionId: row.unitconversionId,
                batchId: row.batchId,
                godownId: row.godownId,
                rackId: product.rackId,
                projectId: row.projectId,
                categoryId: row.categoryId,
                amount: (row.qty * product.salesRate),
                Slno: (count + 1)
            });
        });

        //s s
        $.each(finishedGoodsRawMaterial,
            function (count, row) {
                var product = allProducts.find(p => p.productId == row.productId);
                var conversionObj = allUnitConversionItems.find(p => p.productId == product.productId && p.unitId == row.unitId);
                var conversionRate = conversionObj.conversionRate;
                var purchaseRate = product.purchaseRate / conversionRate;
                var amount = purchaseRate * row.Qty;
                stockJournalDetailsInfoProduction.push({
                    ProductId: row.productId,
                    Qty: row.Qty,
                    Rate: purchaseRate,
                    UnitId: row.unitId,
                    UnitConversionId: conversionObj.unitconversionId,//row.unitconversionId,
                    BatchId: row.batchId,
                    GodownId: row.godownId,
                    RackId: row.rackId,
                    Amount: amount, //(row.Qty * row.salesRate),
                    Slno: (count + 1),
                    ProjectId: row.ProjectId == undefined ? row.projectId : row.ProjectId,
                    CategoryId: row.CategoryId == undefined ? row.categoryId : row.CategoryId,
                    stockJournalAction: "Stock Transfer"
                });
            });

        $.each(finishedGoodsItems,
            function (count, row) {
                // var qt = (stockJournalAction == "Manufacturing" ? $("#quantityToManufacture").val() : row.Qty);
                var qty = row.qty == undefined ? row.Qty : row.qty;
                var product = allProducts.find(p => p.productId == row.productId);
                var conversionObj = allUnitConversionItems.find(p => p.productId == product.productId && p.unitId == row.unitId);
                var conversionRate = conversionObj.conversionRate;
                var purchaseRate = product.purchaseRate / conversionRate;
                var amount = purchaseRate * qty;
                stockJournalDetailsInfoConsumption.push({
                    ProductId: row.productId,
                    Qty: qty,
                    Rate: purchaseRate,
                    UnitId: row.unitId,
                    UnitConversionId: conversionObj.unitconversionId,//row.unitconversionId,
                    BatchId: row.batchId,
                    GodownId: row.godownId,
                    RackId: row.rackId,
                    NewBatchName: row.newBatchName,
                    ProjectId: row.ProjectId == undefined ? row.projectId : row.ProjectId,
                    CategoryId: row.CategoryId == undefined ? row.categoryId : row.CategoryId,
                    Amount: amount,//(row.salesRate * qty),
                    Slno: (count + 1),
                    stockJournalAction: "Stock Transfer"
                });
            });
        //e s

        finishedGoodsItems.forEach(function (item) {
            var matchingTransferToItem = finishedGoodsRawMaterial.find(p => p.productId == item.productId && p.godownId == item.godownId);
            if (matchingTransferToItem != undefined) {
                isLocationUnique = false;
            }
        });

        return isLocationUnique;
    }
    catch (error) {

        return isLocationUnique;
    }

   
}