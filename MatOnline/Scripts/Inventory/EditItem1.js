﻿var itemToEdit = {};
var bomForProduct = [];
var productDetail = [];
var units = [];
var stores = [];

var Newstores = [];

var brands = [];
var racks = [];
var taxes = [];
var sizes = [];
var productGroups = [];
var modelNos = [];
var salesAccounts = [];
var expenseAccounts = [];
var bom = [];
var products = [];
var allUnitConversionItems = [];
var storeId = 1;
var stocksReadyDeleting = [];
var stocks = [];
var stocks = [];
var filteredRacks = [];
var stockDelete = [];
var batch = [];
var condition = [{ val: true, txt: "Yes" }, { val: false, txt: "No" }];

var imageName = "";

itemToEdit.ProductSp = {};
itemToEdit.ProductInfo = {};
itemToEdit.UnitConvertion = {};
itemToEdit.UnitConvertionInfo = {};
itemToEdit.NewStores = [];
itemToEdit.NewBoms = [];
itemToEdit.NewMultipleUnits = [];
itemToEdit.NewStockPostings = [];
itemToEdit.AutoBarcode = false;
itemToEdit.IsSaveBomCheck = false;
itemToEdit.IsSaveMultipleUnitCheck = false;
itemToEdit.IsOpeningStock = false;
itemToEdit.IsBatch = false;
var toatlAmt = 0;
var currentTab = "Main";

//on page load
$(function () {

    GetProductDetails(productId);
    // getBatches();
    GetLookUps();

    pageLoadProcesses();
});


function pageLoadProcesses() {

    var imageName = "";
    document.getElementById('productImage').onchange = function () {
        //alert('Selected file: ' + this.value.replace(/C:\\fakepath\\/i, ''));
        imageName = this.value.replace(/C:\\fakepath\\/i, '');
    };
    $("#btnPrevious").attr("disabled", "disabled");
    $("#btnSaveItem").hide();

    $("#productImage").change(function () {
        //alert("dd");
        readURL(this);
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        currentTab = e.target.text;
        if (currentTab == "Main") {
            $("#btnPrevious").attr("disabled", "disabled");
            $("#btnNext").show();
            $("#btnSaveItem").hide();
        }
        else if (currentTab == "Secondary") {
            $("#btnPrevious").removeAttr("disabled");
            $("#btnNext").show();
            $("#btnSaveItem").hide();
        }
        else if (currentTab == "Others") {
            $("#btnPrevious").removeAttr("disabled");
            $("#btnNext").hide();
            $("#btnSaveItem").show();
        }
    })
    $("#btnNext").click(function () {

        if (currentTab == "Main") {
            $('.nav-tabs a[href="#tab2' + '"]').tab('show');
        }
        else if (currentTab == "Secondary") {
            $('.nav-tabs a[href="#tab3' + '"]').tab('show');
        }

    });
    $("#btnPrevious").click(function () {

        if (currentTab == "Others") {
            $('.nav-tabs a[href="#tab2' + '"]').tab('show');
        }
        else if (currentTab == "Secondary") {
            $('.nav-tabs a[href="#tab1' + '"]').tab('show');
        }

    });

    $('#store').on('change', function () {
        var filteredRacks2 = []; //empty the array each time function is called
        var chk = $(this).val();
        $.each(racks, function (i, record) {
            if (record.GodownId == chk) {
                filteredRacks2.push(record);
            }
        });
        var rackHtml2 = "";
        $.each(filteredRacks2, function (i, record) {
            rackHtml2 += '<option value="' + record.RackId + '">' + record.RackName + '</option>';
        });
        $("#rack").html(rackHtml2);
    });

    $('#itemType').on('change', function () {
        var val = this.value;

        switch (val) {
            case "Product":
                //$("#openingStock").attr('disabled', 'disabled');
                $("#lblSalesRate").html("Sales Rate");
                $("#openingStock").removeAttr('disabled');
                $("#addModal").removeAttr('disabled');
                $("#minimumStock").removeAttr('disabled');
                $("#maximumStock").removeAttr('disabled');
                $("#reorderLevel").removeAttr('disabled');
                $("#store").removeAttr('disabled');
                $("#rack").removeAttr('disabled');
                $("#mrp").removeAttr('disabled');
                $("#brand").removeAttr('disabled');
                $("#bom").removeAttr('disabled');
                // $("#allowBatch").removeAttr('disabled');
                $("#multipleUnit").removeAttr('disabled');
                break;
            case "Service":
                $("#lblSalesRate").html("Service Rate");
                $("#openingStock").attr('disabled', 'disabled');
                $("#addModal").attr('disabled', 'disabled');
                $("#minimumStock").attr('disabled', 'disabled');
                $("#maximumStock").attr('disabled', 'disabled');
                $("#reorderLevel").attr('disabled', 'disabled');
                $("#store").attr('disabled', 'disabled');
                $("#rack").attr('disabled', 'disabled');
                $("#brand").attr('disabled', 'disabled');
                $("#mrp").attr('disabled', 'disabled');
                $("#bom").attr('disabled', 'disabled');
                // $("#allowBatch").attr('disabled', 'disabled');
                $("#multipleUnit").attr('disabled', 'disabled');
                break;
        }
    });

    $('#chkActive').change(function () {
        if (this.checked) {
            isActive = true;
        }
        else {
            isActive = false;
        }
    });

    $('#chkReminder').change(function () {
        if (this.checked) {
            isShowReminder = true;
        }
        else {
            isShowReminder = false;
        }
    });

    $("#btnAddBomItem").click(function () {
        var r = $("#bomRawMaterial").val();
        var q = $("#bomQuantity").val();
        var u = $("#bomUnit").val();
        bom.push({
            bomId: 0,
            extra1: "",
            extra2: "",
            //extraDate: new Date(),
            rowmaterialId: parseFloat(r),
            unitId: u,
            quantity: parseFloat(q),
            productId: productId
        });
        console.log(bom);
        //reset control
        $("#bomQuantity").val(0);
        renderNewBomItems();
    });

    $("#productImage").change(function () {
        //alert("dd");
        readURL(this);
    });

    $("#btnUpdateItem").click(function () {
        UpdateProductDetails();
    });

    if (productDetail.Isopeningstock == false) {
        $("#openingStockStorePanel").css("display", "none");
    }
    else {

    }

    document.getElementById('productImage').onchange = function () {
        //alert('Selected file: ' + this.value.replace(/C:\\fakepath\\/i, ''));
        imageName = this.value.replace(/C:\\fakepath\\/i, '');
    };
}

//get all look up data
function GetLookUps() {

    var ItemLoading = document.getElementById("ItemLoading");
    doncurrentTab = "Main";
    Utilities.Loader.Show();
    var lookUpsAjax = $.ajax({
        url: API_BASE_URL + "/EditItem/GetLookups12?id=" + productId,
        type: "Get",
        contentType: "application/json",
    });


    var groupAjax = $.ajax({
        url: API_BASE_URL + '/ItemGroup/GetProductGroups12',
        type: 'Get',
        contentType: "application/json",
    });

    var salesAccountAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetSalesAccounts',
        type: 'Get'
    });

    var expenseAccountAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetExpenseAccount',
        type: 'Get'
    });

    $.when(lookUpsAjax, groupAjax, salesAccountAjax, expenseAccountAjax)
        .done(function (dataLookUps, groupdata, dataSalesAccount, dataExpenseAccount) {
            sizes = dataLookUps[0].Sizes;
            units = dataLookUps[0].Units;
            stores = dataLookUps[0].Stores;
            racks = dataLookUps[0].Racks;
            brands = dataLookUps[0].Brands;
            taxes = dataLookUps[0].Taxes;
            productGroups = groupdata[0];
            modelNos = dataLookUps[0].ModelNos;
            salesAccounts = dataSalesAccount[2].responseJSON;
            expenseAccounts = dataExpenseAccount[2].responseJSON;
            bom = dataLookUps[0].Bom;
            products = dataLookUps[0].Products;
            stocks = dataLookUps[0].Stocks;
            stocksReadyDeleting = dataLookUps[0].Stocks;
            batch = dataLookUps[0].Batch;
            batch = dataLookUps[0].batchs;
            modelNos = dataLookUps[0].ModelNos;
            console.log(dataLookUps);
            FindVoucher();
            PopulateControls();
            populateDropDownLists();
            FillStores();
            ItemLoading.style.display = 'none';
            /*Utilities.Loader.Hide();*/
        });

  
   /* FillStores();*/
    Utilities.Loader.Show();
    var itemsAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetProducts1',
        type: 'Get'
    });

    var unitConversionLookupAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetUnitConversionItems",
        type: "GET",
        contentType: "application/json",
    });

    $.when(itemsAjax, unitConversionLookupAjax)
        .done(function (dataItems, dataUnitConversion) {
            items = dataItems[2].responseJSON;
            //units = dataUnit[2].responseJSON;
            allUnitConversionItems = dataUnitConversion[0].UnitCoversionItems;
            PopulateControls();
            Utilities.Loader.Hide();
        }
        );

}

//get product Details per Product
function GetProductDetails(productId) {

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/EditItem/GetProductDetails?id=" + productId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            productDetail = data;
            populateDropDownLists();
            Utilities.Loader.Hide();
            console.log(productDetail);
        },
        error: function (e) {
            Utilities.Loader.Hide();
        }
    });
}
//bind lookup data to html Controls
function PopulateControls() {

    $("#blah").attr("src", '/Content/ProductImages/' + productDetail.ProductId + '.jpg');
    $("#productCode").val(productDetail.ProductCode);
    $("#productName").val(productDetail.ProductName);
    $("#purchaseRate").val(productDetail.PurchaseRate);
    $("#salesRate").val(productDetail.SalesRate);
    $("#minimumStock").val(productDetail.MinimumStock);
    $("#maximumStock").val(productDetail.MaximumStock);
    $("#reorderLevel").val(productDetail.ReorderLevel);
    $("#mrp").val(productDetail.Mrp);
    $("#partNumber").val(productDetail.PartNo);
    $("#txtAutoBarcode").val(productDetail.barcode);
    $("#effectiveDate").val(productDetail.EffectiveDate);
    $("#description").val(productDetail.Narration);
}

$("#bomRawMaterial").change(function () {
    var productId = $("#bomRawMaterial").val();
    var unitHtml = "";
    var unitsItemsOfProduct = allUnitConversionItems.filter(p => p.productId == productId);
    console.log("unitsItemsOfProduct", unitsItemsOfProduct)
    for (var i of unitsItemsOfProduct) {
        var unitName = units.find(p => p.UnitId == i.unitId).UnitName
        unitHtml += `<option value="${i.unitId}">${unitName}</option>`;
    }
    // alert(unitName);
    $("#bomUnit").html(unitHtml);
});
//populate dropdownlists
function populateDropDownLists() {


    var salesAccountHtml = "";
    console.log(salesAccounts);
    $.each(salesAccounts, function (i, record) {
        var selected = "";
        if (record.LedgerId == productDetail.SalesAccount) {
            selected = "selected";
        }
        salesAccountHtml += '<option ' + selected + ' value="' + record.LedgerId + '">' + record.LedgerName + '</option>';

    });
    $("#salesAccount").html(salesAccountHtml);

    var expenseAccountHtml = "";
    $.each(expenseAccounts, function (i, record) {
        var selected = "";
        if (record.LedgerId == productDetail.ExpenseAccount) {
            selected = "selected";
        }
        expenseAccountHtml += '<option ' + selected + ' value="' + record.LedgerId + '">' + record.LedgerName + '</option>';

    });

    if (productDetail.Isopeningstock == false) {

        document.getElementById("openingStock").selectedIndex = "0";
        /*alert(productDetail.SalesAccount);*/
    }
    else {
        document.getElementById("openingStock").selectedIndex = "1";
    }

    if (productDetail.IsallowBatch == false) {

        document.getElementById("allowBatchs").selectedIndex = "0";
        /*alert(productDetail.SalesAccount);*/
    }
    else {
        document.getElementById("allowBatchs").selectedIndex = "1";
        ShowBatchField();

    }

    var html = "";

    $("#expenseAccount").html(expenseAccountHtml);

    //  $("#store").html(Utilities.PopulateDropDownFromArray(stores, 0, 1));
    //  $("#rack").html(Utilities.PopulateDropDownFromArray(racks, 0, 1));
    $("#tax").html(Utilities.PopulateDropDownFromArray(taxes, 0, 1));

    html = "";
    $.each(brands, function (i, record) {
        html += '<option value="' + record.brandId + '">' + "" + record.brandName + "" + '</option>';
    });
    $("#brand").html(html);

    ///$("#brand").html(Utilities.PopulateDropDownFromArray(brands, 1, 2));

    $("#group").html(Utilities.PopulateDropDownFromArray(productGroups, 1, 2));
    // $("#size").html(Utilities.PopulateDropDownFromArray(sizes, 0, 1));

    html = "";
    $.each(sizes, function (i, record) {
        html += '<option value="' + record.sizeId + '">' + "" + record.size + "" + '</option>';
    });
    $("#size").html(html);


    // $("#unit").html(Utilities.PopulateDropDownFromArray(units, 1, 2));

    html = "";
    $.each(units, function (i, record) {
        html += '<option value="' + record.unitId + '">' + "" + record.unitName + "" + '</option>';
    });
    $("#unit").html(html);


    html = "";
    $.each(products, function (i, record) {
        html += '<option value="' + record.productId + '">' + "" + record.productName + "" + '</option>';
    });
    $("#bomRawMaterial").html(html);

    // $("#bomRawMaterial").html(Utilities.PopulateDropDownFromArray(products, 0, 2));

    html = "";

    $.each(units, function (i, record) {
        html += '<option value="' + record.unitId + '">' + "" + record.unitName + "" + '</option>';
    });
    $("#bomUnit").html(html);
    //  $("#bomUnit").html(Utilities.PopulateDropDownFromArray(units, 1, 2));
    //$("#allowBatch").html(Utilities.PopulateDropDownFromArray(condition, 0, 1));
    //$("#multipleUnit").html(Utilities.PopulateDropDownFromArray(condition, 0, 1));
    // $("#Isopeningstock").html(Utilities.PopulateDropDownFromArray(condition, 0, 1));



    //if (productDetail.Isopeningstock == true)
    //{
    //    var e = document.getElementById("openingStock");
    //    e.selectedIndex = 1;
    //    e.disabled = true;
    //}
    //else
    //{
    //    var e = document.getElementById("openingStock");
    //    e.selectedIndex = 0;
    //    e.disabled = false;
    //}

    $("#modelNo").html(Utilities.PopulateDropDownFromArray(modelNos, 0, 1));
    //$("#bom").html(Utilities.PopulateDropDownFromArray(condition, 0, 1));

    /*$("#bomRawMaterial").select2();*/
    //$("#bomRawMaterial").selectize();
    var filteredGroupObj = [];
    $.each(productGroups, function (i, record) {
        var upperLevelGroup = "";
        var currentGroup = {};
        for (i = 0; i < productGroups.length; i++) {
            if (productGroups[i].GroupId == record.GroupUnder) {
                currentGroup = productGroups[i];
                break;
            }
        }
        if (currentGroup.GroupUnder == 0) {
            upperLevelGroup = "Primary";
        }
        else {
            upperLevelGroup = currentGroup.GroupName;
        }
        if (record.Extra1 == "Sub Category") {
            //groupHtml += '<option value="' + record.GroupId + '">' + record.GroupName + ' : ' + upperLevelGroup + '</option>';
            var lv3 = findUpperLevelByGroupLevel(productGroups, upperLevelGroup);
            var lv2 = findUpperLevelByGroupLevel(productGroups, lv3);
            console.log(lv2);
            filteredGroupObj.push({
                GroupId: record.GroupId,
                GroupName: record.GroupName,
                CustomDisplay: record.GroupName + " : " + upperLevelGroup + " : " + lv3 + " : " + lv2
            });
        }
    });
    //$("#group").html(groupHtml);
    console.log(filteredGroupObj);
    $("#group").kendoDropDownList({
        filter: "contains",
        dataTextField: "CustomDisplay",
        dataValueField: "GroupId",
        dataSource: filteredGroupObj
    });

    $("#bom").kendoDropDownList({
        dataTextField: "txt",
        dataValueField: "val",
        dataSource: condition,
        select: function (e) {
            var item = this.dataItem(e.item);
            if (item.val == true) {
                $("#bomModal").modal("toggle");
            }
        }
    });
    $("#allowBatch").kendoDropDownList({
        dataTextField: "txt",
        dataValueField: "val",
        dataSource: condition
    });
    $("#multipleUnit").kendoDropDownList({
        dataTextField: "txt",
        dataValueField: "val",
        dataSource: condition
    });
    $("#modelNo").kendoDropDownList({
        dataTextField: "modelNo",
        dataValueField: "modelNoId",
        dataSource: modelNos
    });
    //$("#Isopeningstock").kendoDropDownList({
    //    dataTextField: "txt",
    //    dataValueField: "val",
    //    dataSource: condition
    //});

    //$("#salesAccount").val(findSalesAccount(productDetail.SalesAccount).ledgerId);
    //$("#salesAccount :selected").text(findSalesAccount(productDetail.SalesAccount).ledgerName);

    //$("#expenseAccount").val(findExpenseAccount(productDetail.ExpenseAccount).ledgerId);
    //$("#expenseAccount :selected").text(findExpenseAccount(productDetail.ExpenseAccount).ledgerName);

    $("#store").val(findStore(productDetail.GodownId).godownId);
    $("#store :selected").text(findStore(productDetail.GodownId).godownName);

    $("#rack").val(findRack(productDetail.RackId).rackId);
    $("#rack :selected").text(findRack(productDetail.RackId).rackName);

    $("#tax").val(findTax(productDetail.TaxId).taxId);
    $("#tax :selected").text(findTax(productDetail.TaxId).taxName);

    $("#brand").val(findBrand(productDetail.BrandId).brandId);
    $("#brand :selected").text(findBrand(productDetail.BrandId).brandName);

    $("#group").val(findProductGroup(productDetail.GroupId).groupId);
    $("#group :selected").text(findProductGroup(productDetail.GroupId).groupName);

    $("#size").val(findSizes(productDetail.SizeId).sizeId);
    $("#size :selected").text(findSizes(productDetail.SizeId).SizeName);

    $("#unit").val(findUnit(productDetail.UnitId).unitId);
    $("#unit :selected").text(findUnit(productDetail.UnitId).unitName);

    //$("#modelNo").val(findModelNo(productDetail.ModelNoId).modelNoId);
    //$("#modelNo :selected").text(findModelNo(productDetail.ModelNoId).modelNo);

    if (productDetail.IsBom == true) {
        $("#bom").data("kendoDropDownList").value(true);
    }
    else if (productDetail.IsBom == false) {
        $("#bom").data("kendoDropDownList").value(false);
    }
    if (productDetail.Ismultipleunit == true) {
        $("#multipleUnit").data("kendoDropDownList").value(true);
    }
    else if (productDetail.Ismultipleunit == false) {
        $("#multipleUnit").data("kendoDropDownList").value(false);
    }
    //if (productDetail.IsallowBatch == true) {
    //    $("#allowBatch").data("kendoDropDownList").value(true);
    //}
    //else if (productDetail.IsallowBatch == false) {
    //    $("#allowBatch").data("kendoDropDownList").value(false);
    //}

    if (productDetail != null && productDetail.Isopeningstock == true) {
        document.getElementById("openingStockStorePanel").style.display = "block";
    }
    else {
        document.getElementById("openingStockStorePanel").style.display = "none";
    }

    if (productDetail.IsallowBatch == false) {

        //document.getElementById("hidTap1").style.display = "none";
        //document.getElementById("hidTap2").style.display = "none";
        //document.getElementById("hidTap3").style.display = "none";

        hideBatchColumns();

        document.getElementById("hideBatchField").style.display = "none";
        document.getElementById("hideBatchMDate").style.display = "none";
        document.getElementById("hideBatchExpireDate").style.display = "none";
    }

    if (productDetail.IsBom == true) {
        renderNewBomItems();
    }
    renderStoreItems();
    // renderStoresToAddItemsTo();
}
//to render bom table 
function renderNewBomItems() {
    var newBomItemsHtml = "";
    $.each(bom, function (i, record) {
        newBomItemsHtml += '<tr>\
                            <td>' + (i + 1) + '</td>\
                            <td>' + (findProduct(record.rowmaterialId).productName) + '</td>\
                            <td>' + record.quantity + '</td>\
                            <td>' + findUnit(record.unitId).unitName + '</td>\
                            <td>' + Utilities.FormatCurrency(findProduct(record.rowmaterialId).purchaseRate) + '</td>\
                            <td><button class="btn btn-danger" onclick="removeBomItem(' + record.id + ')"><i class="fa fa-times"></i></button></td>\
                          </tr>';
    });
    $("#bomTbody").html(newBomItemsHtml);
    $("#bomTotalInventoryAmount").val(Utilities.FormatCurrency(getBomTotalInventoryValue()));
}
//to remove bom object from array
function removeBomItem(id) {
    if (confirm("Remove this item?")) {
        var indexOfObjectToRemove = bom.findIndex(p => p.id == id);
        bom.splice(indexOfObjectToRemove, 1);
        renderNewBomItems();
    }
}
function getBomTotalInventoryValue() {
    var total = 0.00;
    $.each(bom, function (i, record) {
        total += (findProduct(record.rowmaterialId).purchaseRate) * record.quantity;
    });
    return total;
}
//to render stock to table when batch is true

//function getBatches() {
//    showLoader();
//    $.ajax({
//        url: API_BASE_URL + "/Batch/GetBatches",
//        type: 'GET',
//        contentType: "application/json",
//        success: function (data) {
//            PriviledgeSettings.ApplyUserPriviledge();
//            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
//            setTimeout(onSuccess, 500, data);
//        },
//        error: function (request, error) {
//            hideLoader();
//        }
//    });
//}

//function onSuccess(data) {

//    console.log(data); //return;

//    $.each(data, function (i, record) {  
//        batch.push([
//            record.BatchId,
//            record.BatchNo,
//            record.ProductId,
//            record.ManufacturingDate,
//            record.ExpiryDate,
//            record.narration
//        ]);
//    });
//}

$("#addStore").click(function () {

    var RackId = $("#newRack").val();
    var StoreID = $("#newStore").val();
    var allRacks = FindRacks(StoreID);
   
  
    if (productDetail.IsallowBatch == true) {

        var BatchName = document.getElementById("newBatch").value;
        //if (BatchName == null) {

        //    Swal.fire('Enter batch name and try again.', '', 'warning');
        //    return;
        //}

        if (BatchName.toString() == "") {

            Swal.fire('Enter batch name and try again.', '', 'warning');
            return;
        }




        var parsedMFDate = Date.parse($("#newMf").val());
        var parsedExDate = Date.parse($("#newExp").val());
        if (parsedMFDate > parsedExDate) {
            Swal.fire('Manufacture date cant be greater than expire date', '', 'warning');
            return;
        }

        if (parsedMFDate.toString() == parsedExDate.toString()) {

            Swal.fire('Manufacture date cant be equal to expire date', '', 'warning');
            return;
        }

      
    }

    var Itemquantity = document.getElementById("newQuantity").value;
    var itemRate = document.getElementById("newRate").value;



    if (StoreID == "") {

        Swal.fire('Select store and try again.', '', 'warning');
        return;
    }

     var united = $("#newUnit").val();

    if (united == "") {

        Swal.fire('Select unit or refresh the page and try again.', '', 'warning');
        return;
    }

    //if (Itemquantity == null) {

    //    Swal.fire('Enter item quantity and try again.', '', 'warning');
    //    return;
    //}


    if (Itemquantity == "") {

        Swal.fire('Enter item quantity and try again.', '', 'warning');
        return;
    }

    if (parseInt(Itemquantity) <= 0) {

        Swal.fire('Enter item quantity and try again.', '', 'warning');
        return;
    }


    //if (itemRate == null) {

    //    Swal.fire('Enter item rate and try again.', '', 'warning');
    //    return;
    //}

    if (itemRate == "") {

        Swal.fire('Enter item rate and try again.', '', 'warning');
        return;
    }

    if (parseInt(itemRate) <= 0) {

        Swal.fire('Enter item rate and try again.', '', 'warning');
        return;
    }


    if (RackId != null && RackId > 0 && RackId != "" && allRacks.length > 0)
    {
        FillByRackOpenStockItems();
    }
    else {
        FillWithOutRackOpenStockItems();
    }

});

function FillWithOutRackOpenStockItems()
{
    var amount = $("#newRate").val() * $("#newQuantity").val();
    var RackId = $("#newRack").val();
    var StoreID = $("#newStore").val();

    var allRacks = FindRacks(StoreID);

    var oldstocks = stocks.findIndex(p => p.godownId == StoreID);
    if ((oldstocks != null && oldstocks != "-1"))
    {
        Swal.fire('You have already entered an item with this same location location', '', 'warning');
        return;
    }

    if (allRacks != null && allRacks.length <= 0) {

        var newstocks = Newstores.findIndex(p => p.storeId == StoreID);
      
        if (Newstores != null && Newstores.length > 0 && ((newstocks != null && newstocks != "-1")))
        {
            Swal.fire('You have already entered an item with this same location location', '', 'warning');
        }
        else {

            Newstores.push({
                id: (storeId++),
                storeId: $("#newStore").val(),
                store: $("#newStore option:selected").text(),
                rackId: $("#newRack").val(),
                rack: $("#newRack option:selected").text(),
                batch: $("#newBatch").val(),
                mfD: $("#newMf").val(),
                expD: $("#newExp").val(),
                quantity: $("#newQuantity").val(),
                rate: $("#newRate").val(),
                unit: $("#newUnit option:selected").text(),
                unitId: $("#newUnit").val(),
                amount: amount
            });

            $("#amount").val(amount);

            //reset control
            $("#newMf").val("");
            $("#newExp").val("");
            $("#newQuantity").val("0");
            $("#newRate").val("0");
            $("#amount").val("0");
            renderStoreItems();
            console.log(Newstocks);
        }
    }

}

function FillByRackOpenStockItems() {

    var amount = $("#newRate").val() * $("#newQuantity").val();
    var RackId = $("#newRack").val();
    var StoreID = $("#newStore").val();

    var oldstocks = stocks.findIndex(p => /*p.rackId == RackId &&*/ p.godownId == StoreID);
    if ((oldstocks != null && oldstocks != "-1")) {
        Swal.fire('You have already entered an item with this same location location', '', 'warning');
        return;
    }
   
    //|| (oldstocks != null && oldstocks != "-1")
    if (Newstores != null && Newstores.length > 0) {

        var newstocks = Newstores.findIndex(p => p.rackId == RackId && p.storeId == StoreID);
       
        if ((newstocks != null && newstocks != "-1")) {

            Swal.fire('You have already entered an item with this same location', '', 'warning');

            $("#newMf").val("");
            $("#newExp").val("");
            $("#newQuantity").val("0");
            $("#newRate").val("0");
            $("#amount").val("0");
            return;
        }
        else {

            Newstores.push({
                id: (storeId++),
                storeId: $("#newStore").val(),
                store: $("#newStore option:selected").text(),
                rackId: $("#newRack").val(),
                rack: $("#newRack option:selected").text(),
                batch: $("#newBatch").val(),
                mfD: $("#newMf").val(),
                expD: $("#newExp").val(),
                quantity: $("#newQuantity").val(),
                rate: $("#newRate").val(),
                unit: $("#newUnit option:selected").text(),
                unitId: $("#newUnit").val(),
                amount: amount
            });

            $("#amount").val(amount);
            //reset control
            $("#newMf").val("");
            $("#newExp").val("");
            $("#newQuantity").val("0");
            $("#newRate").val("0");
            $("#amount").val("0");
            renderStoreItems();
            console.log(Newstocks);

        }
    }
    else {


        Newstores.push({
            id: (storeId++),
            storeId: $("#newStore").val(),
            store: $("#newStore option:selected").text(),
            rackId: $("#newRack").val(),
            rack: $("#newRack option:selected").text(),
            batch: $("#newBatch").val(),
            mfD: $("#newMf").val(),
            expD: $("#newExp").val(),
            quantity: $("#newQuantity").val(),
            rate: $("#newRate").val(),
            unit: $("#newUnit option:selected").text(),
            unitId: $("#newUnit").val(),
            amount: amount
        });

        $("#amount").val(amount);

        //reset control
        $("#newMf").val("");
        $("#newExp").val("");
        $("#newQuantity").val("0");
        $("#newRate").val("0");
        $("#amount").val("0");
        renderStoreItems();
        console.log(Newstocks);
    }
}

function shrinkDate(input) {
    var val = input.split("/");
    var year = val[2].substring(2, 4);
    return val[1] + "/" + val[0] + "/" + year;
}

function CalculeteRate(obj) {

    var stockRate = document.getElementById("newRate")

    var stockQty = document.getElementById("newQuantity")

    var totAmount = parseInt(stockRate.value) * parseInt(stockQty.value);

    document.getElementById("amount").value = totAmount;
}

function FillStores() {

    Utilities.Loader.Show();
    var effectiveDate = document.getElementById("effectiveDate");
    if (productDetail.Isopeningstock == true) {

        showLoader();
        $.ajax({
            url: API_BASE_URL + "/ProductCreation/GetCompanyFinancialYearFrom",
            type: 'GET',
            contentType: "application/json",
            success: function (data) {
                //var today = new Date();
                //today = Date.parse(data);
                console.log(""); // 9/17/2016
              //  effectiveDate.value = Utilities.FormatJsonDate(data);
                effectiveDate.readonly = true;

                hideLoader();
            },
            error: function (request, error) {

                effectiveDate.disabled = false;
                hideLoader();
            }
        });

    }
    else
    {

        effectiveDate.readonly = true;
    }

    var storeHtml = "";
    $.each(stores, function (i, record) {
        storeHtml += '<option value="' + record.godownId + '">' + "" + record.godownName + "" + '</option>';
    });

    $("#newStore").html(storeHtml);
    $("#store").html(storeHtml);

    var Sstores = findStore(productDetail.GodownId);
    var el = document.getElementById("newStore");
    for (var i = 0; i < el.options.length; i++) {
        if (el.options[i].text == Sstores.godownName && el.options[i].value == Sstores.godownId) {
            el.selectedIndex = i;
            findStoreRacks(Sstores.godownId);
            break;
        }
    }

    Utilities.Loader.Show();

    $.ajax({
        url: API_BASE_URL + "/Unit/GetUnits1",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {

            var unitHtml = "";
            $.each(data, function (i, record) {
                unitHtml += '<option value="' + record.UnitId + '">' + record.UnitName + '</option>';
            });

            Utilities.Loader.Hide();

            $("#newUnit").html(unitHtml);

            document.getElementById("newUnit").selectedIndex = document.getElementById("unit").selectedIndex;
            document.getElementById("newUnit").disabled = true;


        },
        error: function (request, error) {
            Utilities.Loader.Hide();
        }
    });
    //$("#rack").val(findRack(productDetail.RackId).rackId);
    //$("#rack :selected").text(findRack(productDetail.RackId).rackName);
}

function FindRacks(storeId) {

    var filteredRacksq = []; //empty the array each time function is called
    $.each(racks, function (i, record) {
        if (record.godownId == storeId) {
            filteredRacksq.push(record);
        }
    });

    return filteredRacksq;
}

function findStoreRacks(storeId) {

    filteredRacks = []; //empty the array each time function is called
    $.each(racks, function (i, record) {
        if (record.godownId == storeId) {
            filteredRacks.push(record);
        }
    });

    var newRackHtml = "";
    $.each(filteredRacks, function (i, record) {
        newRackHtml += '<option value="' + record.rackId + '">' + record.rackName + '</option>';
    });

    $("#newRack").html("");
    $("#newRack").html(newRackHtml);

    $("#rack").html("");
    $("#rack").html(newRackHtml);
}

$('#store').on('change', function () {
    findStoreRacks($(this).val());
});

$('#newStore').on('change', function () {
    findStoreRacks($(this).val());
});

var voucherIdOld = "";
var IsNewvoucherId = false;
function FindVoucher() {

    $.each(stocks, function (i, record)
    {
        if (record.productId == productId) {
            voucherIdOld = record.voucherNo;     
        }
    });

    if (voucherIdOld == null || voucherIdOld == "")
    {
        IsNewvoucherId = true;
        GenerateVoucherNO();
    }
}

function GenerateVoucherNO() {

    try {
        Utilities.Loader.Show();

        $.ajax({
            url: API_BASE_URL + "/RejectionIn/GetAutoFormNo1",
            type: "Get",
            contentType: "application/json",
            success: function (data) {
                IsNewvoucherId = true;
                voucherIdOld = data;
                Utilities.Loader.Hide();
                console.log(data);
            },
            error: function (e) {
                Utilities.Loader.Hide();
            }
        });
    }
    catch (error) {

    }

}

var stockersShow = [];


function renderStoreItems() {

    var itemCount = 0;
    stockersShow = [];
    toatlAmt = 0;
    var storesHtml = "";
    var NewTock = [];

    $.each(stocks, function (i, record) {

        if (record.productId == productId) {
            NewTock.push(record);
        }

    });

    stocks = [];

    stocks = NewTock;

    var stockToShow = [];

    for (i = 0; i < stocks.length; i++) {
        if (stocks[i].productId == productId)
        {
            stockToShow.push(stocks[i]);
        }
    }

    console.log(stockToShow);

    $.each(stockToShow, function (i, record) {
        if (productDetail.IsallowBatch == true) {
            stockersShow.push(record);
            itemCount++;
            var rackName = "";
            var Rach = findRack(record.rackId).rackName;
            if (Rach != "-1" && Rach != null && Rach != "undefined") {
                rackName = findRack(record.rackId).rackName;
            }
            storesHtml += '<tr>\
                            <td>' + (itemCount) + '</td>\
                            <td>' + (findStore(record.godownId).godownName) + '</td>\
                            <td>' + rackName + '</td>\
                            <td class="hideTab">' + (findBatch(record.batchId).BatchNo) + '</td>\
                            <td class="hideTab">' + (findBatch(record.batchId).ManufacturingDate1) + '</td>\
                            <td class="hideTab">' + (findBatch(record.batchId).ExpiryDate1) + '</td>\
                            <td>' + record.inwardQty + '</td>\
                            <td>' + record.rate + '</td>\
                            <td>' + (findUnit(record.unitId).unitName) + '</td>\
                            <td>'  + Utilities.FormatCurrency(parseInt(record.rate) * parseInt(record.inwardQty)) + '</td>\ <td><button class="btn btn-danger" onclick="removeStore(' + record.StockProductID + ')"><i class="fa fa-times"></i></button></td>\
                       </tr>';
            toatlAmt = toatlAmt + (parseInt(record.rate) * parseInt(record.inwardQty));
           
        }
        else if (productDetail.IsallowBatch == false) {
            stockersShow.push(record);
            itemCount++;
            var rackName = "";
            var Rach = findRack(record.rackId).rackName;
            if (Rach != "-1" && Rach != null && Rach != "undefined") {
                rackName = findRack(record.rackId).rackName;
            }
            storesHtml += '<tr>\
                           <td class="col-md-1">' + (itemCount) + '</td>\
                            <td class="col-md-1">' + (findStore(record.godownId).godownName) + '</td>\
                            <td class="col-md-1">' + rackName + '</td>\
                            <td class="col-md-1">' + record.inwardQty + '</td>\
                            <td class="col-md-1">' + record.rate + '</td>\
                            <td class="col-md-1">' + (findUnit(record.unitId).unitName) + '</td>\
                            <td class="col-md-1">' + Utilities.FormatCurrency(parseInt(record.rate) * parseInt(record.inwardQty)) + '</td>\ <td><button class="btn btn-danger" onclick="removeStore(' + record.StockProductID + ')"><i class="fa fa-times"></i></button></td>\
                       </tr>';

            toatlAmt = toatlAmt + (parseInt(record.rate) * parseInt(record.inwardQty));
        }
    });

    $.each(Newstores, function (i, record) {
        if (productDetail.IsallowBatch == true) {
            itemCount++;
            storesHtml += `<tr>
                                    <td><b>${(itemCount)}</b></td>
                                    <td><b>${record.store}</b></td>
                                    <td><b>${record.rack}</b></td>
                                    <td class="hideTab"><b>${record.batch}</b></td>
                                    <td class="hideTab"><b>${shrinkDate(record.mfD)}</b></td>
                                    <td  class="hideTab"><b>${shrinkDate(record.expD)}</b></td>
                                    <td><b>${record.quantity}</b></td>
                                    <td><b>${record.rate}</b></td>
                                    <td><b>${record.unit}</b></td>
                                    <td><b>${Utilities.FormatCurrency(parseInt(record.quantity) * parseInt(record.rate))}</b></td>
                                    <td><button class="btn btn-danger" onclick="removeStore12(${record.id})"><i class="fa fa-times"></i></button></td>
                                </tr>
                            `;

            toatlAmt = toatlAmt + (parseInt(record.quantity) * parseInt(record.rate));
        }
        else if (productDetail.IsallowBatch == false) {
            itemCount++;
            storesHtml += ` <tr>
                                    <td class=""><b>${(itemCount)}</b></td>
                                    <td class=""><b>${record.store}</b></td>
                                    <td class=""><b>${record.rack}</b></td>

                                    <td class=""><b>${record.quantity}</b></td>
                                    <td class=""><b>${record.rate}</b></td>
                                    <td class=""><b>${record.unit}</b></td>
                                    <td class=""><b>${Utilities.FormatCurrency(parseInt(record.quantity) * parseInt(record.rate))}</b></td>
                                    <td class=""><button class="btn btn-danger" onclick="removeStore12(${record.id})"><i class="fa fa-times"></i></button></td>
                                </tr>
                            `;
            toatlAmt = toatlAmt + (parseInt(record.quantity) * parseInt(record.rate));
        }

    });

    document.getElementById("stocktbody").innerHTML = storesHtml;
    document.getElementById("totalInventoryValue").value = "₦ " + Utilities.FormatCurrency(parseInt(toatlAmt));
}

function getTotalInventoryValue() {

    var total = 0.00;
    FillStores();

    $.each(Newstores, function (i, record) {
        total += parseInt(record.amount);
    });

    $.each(stocks, function (i, record) {
        total += parseInt(record.amount);
    });

    return total;

    //$.each(stores, function (i, record) {
    //    total += record.amount;
    //});
    //return total;
}

function removeStore(StockProductID) {

    Swal.fire({
        title: 'Warning',
        text: "Are you sure that you want to delete this item location",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, I am Sure'
    }).then((result) => {

        if (result.value) {

            try {
                var newstocksk = stocks.findIndex(p => p.StockProductID == StockProductID);
                if (newstocksk != null && newstocksk != "-1") {
                    stocks.splice(newstocksk, 1);

                    var newstocksOO = stockDelete.findIndex(p => p.id == StockProductID);
                    if (newstocksOO != null && newstocksOO == "-1") {
                        stockDelete.push({ id: StockProductID });
                    }
                }
            }
            catch (error) {

            }

            try {
                var newstocksq = stockersShow.findIndex(p => p.StockProductID == StockProductID);
                if (newstocksq != null && newstocksq != "-1") {
                    stockersShow.splice(newstocksq, 1);

                    var newstocksOO = stockDelete.findIndex(p => p.id == StockProductID);
                    if (newstocksOO != null && newstocksOO == "-1") {
                        stockDelete.push({ id: StockProductID });
                    }

                }
            }
            catch (error) {

            }


            //DeleteProduct(StockProductID)
            renderStoreItems();
            console.log(Newstores);
            console.log(stockersShow);
            console.log(stocks);

        }


    });




}

function removeStore12(id) {

    Swal.fire({
        title: 'Warning',
        text: "Are you sure that you want to delete this item location",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, I am Sure'
    }).then((result) => {

        if (result.value) {
            try {
                var newstocks = Newstores.findIndex(p => p.id == id);
                if (newstocks != null && newstocks != "-1") {
                    Newstores.splice(newstocks, 1);
                    // stockDelete.push({ id: id });
                }

                renderStoreItems();
                console.log(Newstores);
            }
            catch (error) {

            }

        }


    });

}

function DeleteProduct(StockProductID) {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/DelectStockProduct?productId=" + StockProductID,
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function ShowBatchField() {

    if (productDetail.IsallowBatch == true) {

        //document.getElementById("hidTap1").style.display = "inline-block";
        //document.getElementById("hidTap2").style.display = "inline-block";
        //document.getElementById("hidTap3").style.display = "inline-block";

        showBatchColumns();

        document.getElementById("allowBatchs").selectedIndex = 1;

        document.getElementById("hideBatchField").style.display = "block";
        document.getElementById("hideBatchMDate").style.display = "block";
        document.getElementById("hideBatchExpireDate").style.display = "block";
    }
}

function CheckIfItemHasBatch() {

    var IsHasStock = false;
    const newstocks = Newstores.findIndex(p => p.batch != "");

    if (newstocks != "-1") {
        IsHasStock = true;
    }

    const newstocksk = stocks.findIndex(p => p.batchId > 0 || p.BatchNo != "");
    if (newstocksk != "-1") {
        IsHasStock = true;
    }

    const newstocksq = stockersShow.findIndex(p => p.batchId > 0 || p.BatchNo != "");
    if (newstocksq != "-1") {
        IsHasStock = true;
    }

    return IsHasStock;
}

function CheckIfItemDontHasBatch() {

    var IsHasStock = true;
    const newstocks = Newstores.findIndex(p => p.batch == "" || p.batch == null);

    if (newstocks == "0") {
        IsHasStock = false;
    }

    if (stocks.length > 0) {
        const newstocksk = stocks.findIndex(p => p.batchId <= 0 || p.BatchNo == "" || p.batchId == null || p.BatchNo == null);
        if (newstocksk == "0") {
            IsHasStock = false;
        }
    }



    if (stockersShow.length > 0) {
        const newstocksq = stockersShow.findIndex(p => p.batchId <= 0 || p.BatchNo == "" || p.batchId == null || p.BatchNo == null);
        if (newstocksq == "0") {
            IsHasStock = false;
        }
    }


    return IsHasStock;
}

function hideBatchColumns() {
    $(".hideTab").hide();
    //var elems = document.getElementsByClassName("hideTab");
    //for (var i = 0; i < elems.length; i++) {
    //    elems[i].style.display = "none";
    //}
}

function showBatchColumns() {
    $(".hideTab").show();
    //var elems = document.getElementsByClassName("hideTab");
    //for (var i = 0; i < elems.length; i++) {
    //    elems[i].style.display = "grid";
    //}
}

function OnopeningStockChange(obj) {

    var effectiveDate = document.getElementById("effectiveDate");
    if (obj.value == "False") {
        effectiveDate.disabled = false;
        if (stockersShow.length >= 1) {

            obj.selectedIndex = 1;

            Swal.fire('Please delete all opening stock items.', '', 'warning');
            obj.selectedIndex = 1;
            obj.selectedIndex = 1;
        }
        else {


            /*  document.getElementById("hidTap1").style.display = "none";
              document.getElementById("hidTap2").style.display = "none";
              document.getElementById("hidTap3").style.display = "none";*/

            hideBatchColumns();

            document.getElementById("allowBatchs").selectedIndex = 0;

            document.getElementById("hideBatchField").style.display = "none";
            document.getElementById("hideBatchMDate").style.display = "none";
            document.getElementById("hideBatchExpireDate").style.display = "none";

            document.getElementById("openingStockStorePanel").style.display = "none";
            if (productDetail != null && productDetail != "+1") {
                productDetail.Isopeningstock = false;
            }

        }
    }
    else {


        showLoader();
        $.ajax({
            url: API_BASE_URL + "/ProductCreation/GetCompanyFinancialYearFrom",
            type: 'GET',
            contentType: "application/json",
            success: function (data) {
                //var today = new Date();
                //today = Date.parse(data);
               /* console.log(""); // 9/17/2016*/
               // effectiveDate.value = Utilities.FormatJsonDate(data);
                effectiveDate.readonly = true;

                hideLoader();
            },
            error: function (request, error) {

                effectiveDate.disabled = false;
                hideLoader();
            }
        });
        //document.getElementById("hidTap1").style.display = "none";
        //document.getElementById("hidTap2").style.display = "none";
        //document.getElementById("hidTap3").style.display = "none";

        //document.getElementById("allowBatch").selectedIndex = 0;
        document.getElementById("openingStockStorePanel").style.display = "block";
        if (productDetail != null && productDetail != "+1") {
            productDetail.Isopeningstock = true;
        }
        ////document.getElementById("hideBatchField").style.display = "none";
        ////document.getElementById("hideBatchMDate").style.display = "none";
        ////document.getElementById("hideBatchExpireDate").style.display = "none";
    }
}

function OnallowBatchChange(obj) {

    if (obj.value == "False") {

        var duu = CheckIfItemHasBatch();
        if (duu) {

            obj.selectedIndex = 1;
            Swal.fire('Please delete all stock items that has a batch and try again', '', 'warning');

            obj.selectedIndex = 1;
            obj.selectedIndex = 1;

        }
        else {

            if (productDetail != null && productDetail != "-1") {
                productDetail.IsallowBatch = false;
            }

            $("#newBatch").val("");
            $("#newMf").val("");
            $("#newExp").val("");
            $("#newQuantity").val("0");
            $("#newRate").val("0");

            hideBatchColumns();

            document.getElementById("hideBatchField").style.display = "none";
            document.getElementById("hideBatchMDate").style.display = "none";
            document.getElementById("hideBatchExpireDate").style.display = "none";
        }
    }
    else {

        var dontHasBatch = CheckIfItemDontHasBatch();

        if (dontHasBatch == false) {

            obj.selectedIndex = 0;
            Swal.fire('Please delete all stock items that dont has a batch and try again', '', 'warning');

            obj.selectedIndex = 0;
            obj.selectedIndex = 0;

        }
        else {

            showBatchColumns();
            //document.getElementById("hidTap1").style.display = "inline-block";
            //document.getElementById("hidTap2").style.display = "inline-block";
            //document.getElementById("hidTap3").style.display = "inline-block";

            document.getElementById("hideBatchField").style.display = "block";
            document.getElementById("hideBatchExpireDate").style.display = "block";
            document.getElementById("hideBatchMDate").style.display = "block";

            if (productDetail != null && productDetail != "-1") {
                productDetail.IsallowBatch = true;
            }
        }
    }
}

function ReplaceUnit(unitMan)
{
    //var unitMan = document.getElementById("unit");
    if (unitMan.selectedIndex > 0) {
        document.getElementById("newUnit").selectedIndex = unitMan.selectedIndex;
        document.getElementById("newUnit").disabled = true;
    } 
}

//function removeStore(id)
//{
//   // alert("Test");
//    //var indexOfObjectToRemove = stocks.findIndex(p => p.id == id);
//    //stocks.splice(indexOfObjectToRemove, 1);
//    //console.log(stocks);
//    //renderStoreItems();
//}

//update product details
function UpdateProductDetails() {

    if (voucherIdOld == "") {
        GenerateVoucherNO();
        Swal.fire('Invalid voucher Number, try again', '', 'warning')
        return;
    }

    var productCode =  $("#productCode").val();
    var unitType = $("#unit").val();
    var productName =  $("#productName").val();

    if (productName == "") {
        Swal.fire('Product name is required', '', 'warning')
    }

    if (unitType == "")
    {
        Swal.fire('Product unit not found, Please refresh the web page and try again', '', 'warning')
    }

    if (productId == "" || productCode == "") {
       
        Swal.fire('Product identification not found, Please refresh the web page and try again', '', 'warning')
        return;
    }

    
    if (IsNewvoucherId == true) {

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/EditItem/VoucherValidating?VoucherId=" + voucherIdOld,
            type: "Get",
            contentType: "application/json",
            success: function (data) {
                IsNewvoucherId = true;
                if (data.ResponseCode == 900) {

                    Utilities.Loader.Hide();
                    GenerateVoucherNO();
                    Utilities.ErrorNotification(data.ResponseMessage);
                    return;
                }
                else if (data.ResponseCode == 200) {
                    Utilities.Loader.Hide();
                    PushUpadteProduct();
                }
            },
            error: function (e) {
                Utilities.Loader.Hide();
                Swal.fire('An error has occur, Check Internet Connection and try again', '', 'warning');
                return;
            }
        });
    }
    else {
        PushUpadteProduct();
    }

}

function PushUpadteProduct() {

  
        Utilities.Loader.Show();

        itemToEdit.ProductInfo =
        {
            ProductId: productId,
            VoucherId: voucherIdOld,
            ProductName: $("#productName").val(),
            ProductCode: $("#productCode").val(),
            PurchaseRate: $("#purchaseRate").val(),
            SalesRate: $("#salesRate").val(),
            Mrp: $("#mrp").val() == "" ? "NULL" : $("#mrp").val(),
            MaximumStock: $("#maximumStock").val(),
            MinimumStock: $("#minimumStock").val(),
            ReorderLevel: $("#reorderLevel").val(),
            TaxId: $("#tax").val(),
            UnitId: $("#unit").val(),
            GroupId: $("#group").val(),
            ProductType: $("#itemType").val(),
            SalesAccount: $("#salesAccount").val(),
            EffectiveDate: $("#effectiveDate").val(),
            ExpenseAccount: $("#expenseAccount").val(),
            TaxapplicableOn: $("#taxApplicable").val(),
            BrandId: $("#brand").val(),
            SizeId: $("#size").val(),
            ModelNoId: $("#modelNo").val(),
            GodownId: $("#store").val(),
            RackId: $("#rack").val(),
            IsallowBatch: $("#allowBatchs").val(),
            IsBom: $("#bom").val(),
            PartNo: $("#partNumber").val() == "" ? "0" : $("#partNumber").val(),
            Isopeningstock: productDetail.Isopeningstock,
            Ismultipleunit: $("#multipleUnit").val(),
            IsActive: productDetail.IsActive,
            Extra1: imageName,
            Extra2: "",
            IsshowRemember: productDetail.IsshowRemember,   //isShowReminder
            Narration: $("#description").val()
        };


        itemToEdit.NewStores = stocks;
        itemToEdit.StockPosting = [];
        itemToEdit.StockPosting = Newstores;
        itemToEdit.stockDelete = [];
        itemToEdit.stockDelete = stockDelete;

        if (itemToEdit.NewStores.length <= 0 && itemToEdit.StockPosting.length <= 0) {
            productDetail.Isopeningstock = false;
            var irms = $("#openingStock").val();
            if (irms != "False") {
                Utilities.Loader.Hide();
                Swal.fire('Please disenabled opening stock items and try again', '', 'warning');
                return;
            }
        }

        for (i = 0; i < bom.length; i++) {
            itemToEdit.NewBoms.push(
                {
                    Extra1: bom[i].extra1,
                    Extra2: bom[i].extra2,
                    Quantity: bom[i].quantity,
                    RawMaterialId: bom[i].rowmaterialId,
                    UnitId: bom[i].unitId,
                    ProductId: bom[i].productId,
                    BomId: bom[i].bomId
                });
        }

        $.ajax({
            url: API_BASE_URL + "/EditItem/UpdateProductDetails1",
            type: "Post",
            contentType: "application/json",
            data: JSON.stringify(itemToEdit),
            success: function (e) {

                if (e == "HasTransactions")
                {
                    //$("#returnedProductId").val(itemToEdit.ProductInfo.ProductId);
                    //$("#frmProductImage").submit();
                    Utilities.Loader.Hide();
                    Swal.fire('This product can not be edit because it has out stock or in stock posted!', '', 'warning');

                }
                else
                {
                    $("#returnedProductId").val(itemToEdit.ProductInfo.ProductId);
                    $("#frmProductImage").submit();
                    Utilities.Loader.Hide();
                    Utilities.SuccessNotification("Product Updated!");
                    setTimeout(function () {
                        window.location = "/Inventory/ItemCreation/Index";;
                    }, 2000);
                }
               
            },
            error: function (e) {
                Utilities.Loader.Hide();
            }
        });

        console.log(itemToEdit); 
}

//functions to find the exact value in dropdownlist
function findProduct(productId) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].productId == productId) {
            output = products[i];
            break;
        }
    }
    return output;
}
function findTax(taxId) {
    var output = {};
    for (i = 0; i < taxes.length; i++) {
        if (taxes[i].taxId == taxId) {
            output = taxes[i];
            break;
        }
    }
    return output;
}
function findExpenseAccount(ledgerId) {
    var output = {};
    for (i = 0; i < expenseAccounts.length; i++) {
        if (expenseAccounts[i].ledgerId == ledgerId) {
            output = expenseAccounts[i];
            break;
        }
    }
    return output;
}
function findSalesAccount(ledgerId) {
    var output = {};
    for (i = 0; i < salesAccounts.length; i++) {
        if (salesAccounts[i].ledgerId == ledgerId) {
            output = salesAccounts[i];
            break;
        }
    }
    return output;
}
function findBrand(brandId) {
    var output = {};
    for (i = 0; i < brands.length; i++) {
        if (brands[i].brandId == brandId) {
            output = brands[i];
            break;
        }
    }
    return output;
}
function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batch.length; i++) {
        if (batch[i].BatchId == batchId) {
            output = batch[i];
            break;
        }
    }
    return output;
}
function findProductGroup(PGId) {
    var output = {};
    for (i = 0; i < productGroups.length; i++) {
        if (productGroups[i].groupId == PGId) {
            output = productGroups[i];
            break;
        }
    }
    return output;
}
function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}
function findSizes(sizeId) {
    var output = {};
    for (i = 0; i < sizes.length; i++) {
        if (sizes[i].sizeId == sizeId) {
            output = sizes[i];
            break;
        }
    }
    return output;
}
function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    storeId = output.godownId;
    return output;
}
//function findModelNo(modelNoId) {
//    var output = {};
//    for (i = 0; i < modelNos.length; i++) {
//        if (modelNos[i].modelNoId == modelNoId) {
//            output = modelNos[i];
//            break;
//        }
//    }
//    return output;
//}
function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}
function findCondition(cond) {
    var output = {};
    for (i = 0; i < condition.length; i++) {
        if (condition[i].val == cond) {
            output = condition[i];
            break;
        }
    }
    return output;
}
function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}
function findUpperLevelByGroupLevel(groups, groupNameToFind) {
    var obj = groups.find(p => p.GroupName == groupNameToFind);
    var groupUnderName = groups.find(p => p.GroupId == obj.GroupUnder);
    if (groupUnderName == null || groupUnderName == "") {
        return "N/A";
    }
    return groupUnderName.GroupName;
}
