﻿var accountLedgers = [];
var currencies = [];
var finishedGoods = [];
var allProducts = [];
var allStores = [];
var allRacks = [];
var allUnits = [];
var allBatches = [];
var cashOrBanks = [];
var ledgers = [];
var allUsers = [];
var totalamount = 0;

var table = "";
var pendingJournals = [];


//========MAKER-CHECKER PART===========
$(function () {
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
});
function savePending() {
    var src = $("#transferSourceGrid").data().kendoGrid.dataSource.view();
    stockJournalDetailsInfoConsumption = [];
    $.each(src, function (count, row) {
        stockJournalDetailsInfoConsumption.push({
            ProductId: row.productId,
            Qty: row.qty,
            Rate: row.rate,
            UnitId: "",
            UnitConversionId: "" /*row.unitconversionId*/,
            BatchId: "",
            GodownId: row.godownId,
            RackId: row.rackId,
            Amount: (row.qty * row.rate),
            Slno: (count + 1)
        });
    });

    var destination = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
    stockJournalDetailsInfoProduction = [];
    $.each(destination, function (count, row) {
        stockJournalDetailsInfoProduction.push({
            ProductId: row.productId,
            Qty: row.qty,
            Rate: row.rate,
            UnitId: "",
            UnitConversionId: "",
            BatchId: "",
            GodownId: row.godownId,
            RackId: row.rackId,
            Amount: (row.rate * row.qty),
            Slno: (count + 1)
        });
    });

    stockJournalMasterInfo = {
        Narration: $("#narration").val(),
        InvoiceNo: $("#formNoTransfer").val(),
        Extra1: stockJournalAction,
        Extra2: matUserInfo.UserId
    };

    additionalCostItems = [];
    //var kendoAdditionalCostSource = $("#additionalCostGrid").data().kendoGrid.dataSource.view();
    //$.each(kendoAdditionalCostSource, function (count, row) {
    //    additionalCostItems.push({
    //        LedgerId: row.ledgerId,
    //        Amount: row.amount
    //    });
    //});
    var toSave = {
        StockJournalMasterInfo: stockJournalMasterInfo,
        StockJournalDetailsInfoConsumption: stockJournalDetailsInfoConsumption,
        StockJournalDetailsInfoProduction: stockJournalDetailsInfoProduction,
        //LedgerPostingInfo: ledgerPostingInfo,
        AdditionalCostInfo: additionalCostInfo,
        TotalAdditionalCost: totalAdditionalCost,
        Date: $("#transactionDate").val(),
        Currency: $("#currency").val(),
        AdditionalCostCashOrBankId: $("#bankOrCash").val(),
        AdditionalCostItems: additionalCostItems
    };
    console.log(toSave);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/SavePending",
        type: "Post",
        contentType: "application/json",
        data: JSON.stringify(toSave),
        success: function (data) {
            if (data == true) {
                stockJournalDetailsInfoConsumption = [];
                stockJournalDetailsInfoProduction = [];

                additionalCostItems = [];
                stockJournalMasterInfo = {};
                additionalCostInfo = {};
                totalAdditionalCost = 0.0;
                $('#manufacturing').prop("checked", "checked");
                $("#quantityToManufacture").val(0);
                $("#narration").val("");
                $("#additionalCostTitle").html("Additional Cost / Narration");
                $("#stockAdjustmentGrid").data('kendoGrid').dataSource.data([]);
                //$("#transferDestinationGrid").data('kendoGrid').dataSource.data([]);
                //$("#transferSourceGrid").data('kendoGrid').dataSource.data([]);
                $("#totalSourceTransferCost").val(0.0);
                $("#totalDestinationTransferCost").val(0.0);

                stockJournalAction = "Manufacturing";
                renderFinishedGoodsItems();
                renderRawMaterialForFinishedGoods();
                getAutoVoucherNo();
                Utilities.SuccessNotification("Stock journal saved!");  //clear controls
                window.location = "/Inventory/StockJournal/Index";
            }
            else {
                Utilities.Loader.Hide();
                Utilities.ErrorNotification("Could not save journal!");
            }
        },
        error: function (err) {
            Utilities.Loader.Hide();
            Utilities.ErrorNotification("Sorry something went wrong!");
        }
    });
}
function getPendingStockJournal(fromDate, toDate, status) {
    Utilities.Loader.Show();
    var lookupsAjax = $.ajax({
        url: API_BASE_URL + "/StockJournal/GetLookups",
        type: "GET",
        contentType: "application/json",
    });
    var pendingListAjax = $.ajax({
        url: API_BASE_URL + "/StockJournal/GetPendingStockJournals?fromDate=" + fromDate + "&toDate=" + toDate + "&status=" + status,
        type: "Get",
        contentType: "application/json",
    });
    $.when(lookupsAjax, pendingListAjax)
    .done(function (lookups, pendingList) {
        console.log(lookups);
        console.log(pendingList);

        allUsers = lookups[0].users;
        currencies = lookups[0].Currencies;
        allProducts = lookups[0].AllProducts;
        allStores = lookups[0].AllStores;
        allUnits = lookups[0].AllUnits;
        allRacks = lookups[0].AllRacks;
        allBatches = lookups[0].AllBatches;

        pendingJournals = pendingList[0].Table;
        var objToShow = [];
        $.each(pendingJournals, function (count, row) {
            var statusOutput = "";
            if (row.status == "Pending") {
                statusOutput = '<span class="label label-warning"> Pending</span>';
            }
            else if (row.status == "In-Transit") {
                statusOutput = '<span class="label label-primary"> In-Transit</span>';
            }
            else if (row.status == "Delivered") {
                statusOutput = '<span class="label label-success"> Delivered</span>';
            }
            else if (row.status == "Cancelled") {
                statusOutput = '<span class="label label-danger"> Cancelled</span>';
            }
            objToShow.push([
                count + 1,
                row.formNo,
                Utilities.FormatJsonDate(row.date),
                row.narration == "" ? "NA" : row.narration,
                (findUsers(row.userId).userName == "" ? "NA" : findUsers(row.userId).userName),
                statusOutput,
                '<button type="button" class="btn btn-primary btn-sm" onclick="getPendingStockJournalDetails(' + row.stockJournalMasterId + ')"><i class="fa fa-eye"></i> View</button>'
            ]);
        });
        table = $('#stockJournalListTable').DataTable({
            data: objToShow,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });

        Utilities.Loader.Hide();
    });
}

var pj = {};
var prod = [];
var cons = [];
var addCost = [];
function getPendingStockJournalDetails(id) {
    Utilities.Loader.Show();
    for (i = 0; i < pendingJournals.length; i++) {
        if (pendingJournals[i].stockJournalMasterId == id) {
            pj = pendingJournals[i];
        }
    }
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetPendingStockJournalDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            prod = data.production;
            cons = data.consumption;
            addCost = data.addCost;
            ledgers = data.ledger;
            console.log(pj);
            var statusOutput = "";
            if (pj.status == "Pending") {
                $("#ApproveTransfer").show();
                $("#cancelOrder").show();
                statusOutput = '<span class="label label-warning"> Pending</span>';
            }
            else if (pj.status == "In-Transit") {
                $("#ApproveTransfer").hide();
                $("#cancelOrder").hide();
                statusOutput = '<span class="label label-primary"> In-Transit</span>';
            }
            else if (pj.status == "Delivered") {
                $("#ApproveTransfer").hide();
                $("#cancelOrder").hide();
                statusOutput = '<span class="label label-success"> Delivered</span>';
            }
            else if (pj.status == "Cancelled") {
                $("#ApproveTransfer").hide();
                $("#cancelOrder").hide();
                statusOutput = '<span class="label label-danger"> Cancelled</span>';
            }
            $("#transferNoDiv").html(pj.formNo);
            $("#narrationDiv").html(pj.narration);
            $("#dateDiv").html(Utilities.FormatJsonDate(pj.date));
            $("#statusDiv").html(statusOutput);
            $("#transferredByDiv").html(findUsers(pj.userId).userName);

            renderTransferLineItems();

            //$("#detailsTbodyAdditionalCost").html(outputAddCost);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function renderTransferLineItems() {
    var outputCons = "";
    var outputProd = "";
    var outputAddCost = "";
    $.each(cons, function (count, row) {
        totalamount = totalamount + row.amount;
        if (pj.status == "Pending") {
            outputCons += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td><button class="btn btn-sm btn-info" onclick="changeQuantity(' + row.ProductId + ',' + count + ')"><i class="fa fa-edit"></i></button></td>\
                        </tr>';
        }
        else {
            outputCons += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                        </tr>';
        }

    });
    $.each(prod, function (count, row) {
        //var lineItemStatus = "";
        //if (row.status == "In-Transit") {
        //    lineItemStatus = '<span class="label label-primary"> In-Transit </span>';
        //}
        //else if (row.status == "Delivered") {
        //    lineItemStatus = '<span class="label label-success"> Delivered </span>';
        //    buttonStatus = '';
        //}
        //else if (row.status == "Pending") {
        //    lineItemStatus = '<span class="label label-warning"> Pending Approval </span>';
        //}
        outputProd += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                        </tr>';
    });
    //$.each(addCost, function (count, row) {
    //    outputAddCost += '<tr>\
    //                <td>'+ (count + 1) + '</td>\
    //                <td>' + findLedger(row.ledgerId).ledgerName + '</td>\
    //                <td>&#8358;' + Utilities.FormatCurrency(row.debit) + '</td>\
    //                <td>&#8358;' + Utilities.FormatCurrency(row.credit) + '</td>\
    //            </tr>';
    //});

    $("#AmountTxt").val(Utilities.FormatCurrency(totalamount));
    $("#addCostTxt").val(Utilities.FormatCurrency(pj.additionalCost));
    $("#grandTotalTxt").val(Utilities.FormatCurrency(totalamount + pj.additionalCost));

    $("#availableQuantityDiv").html("");
    $("#detailsTbodyConsumption").html(outputCons);
    $("#detailsTbodyProduction").html(outputProd);
}
function changeQuantity(id, count) {
    Utilities.Loader.Show();
    var quantity = prompt("Please enter new Quantity:", "Enter New Quantity");
    if (quantity == null || quantity == "") {
        Utilities.ErrorNotification("Quantity cannot be Empty");
    } else {
        cons[count].qty = quantity;
        cons[count].amount = cons[count].rate * quantity;

        prod[count].qty = quantity;
        prod[count].amount = prod[count].rate * quantity;
    }
    $.ajax({
        url: API_BASE_URL + "/StockJournal/updateQuantity?idprod=" + prod[count].stockJournalDetailsId + "&idcons=" + cons[count].stockJournalDetailsId + "&quantity=" + quantity + "&amount=" + cons[count].amount,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            if (data == true) {
                renderTransferLineItems();
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function saveStockJournalforTransfer() {
    stockJournalDetailsInfoConsumption = [];
    $.each(cons, function (count, row) {
        stockJournalDetailsInfoConsumption.push({
            ProductId: row.productId,
            Qty: row.qty,
            Rate: row.rate,
            UnitId: "",
            UnitConversionId: "" /*row.unitconversionId*/,
            BatchId: "",
            GodownId: row.godownId,
            RackId: row.rackId,
            Amount: (row.qty * row.rate),
            Slno: (count + 1)
        });
    });

    stockJournalDetailsInfoProduction = [];
    $.each(prod, function (count, row) {
        stockJournalDetailsInfoProduction.push({
            ProductId: row.productId,
            Qty: row.qty,
            Rate: row.rate,
            UnitId: "",
            UnitConversionId: "",
            BatchId: "",
            GodownId: row.godownId,
            RackId: row.rackId,
            Amount: (row.rate * row.qty),
            Slno: (count + 1)
        });
    });

    stockJournalMasterInfo = {
        Narration: pj.narration,
        Extra1: "Stock Transfer",
        InvoiceNo: pj.formNo
    };

    additionalCostItems = [];
    //$.each(addCost, function (count, row) {
    //    additionalCostItems.push({
    //        LedgerId: row.ledgerId,
    //        Amount: row.debit
    //    });
    //});
    var toSave = {
        StockJournalMasterInfo: stockJournalMasterInfo,
        StockJournalDetailsInfoConsumption: stockJournalDetailsInfoConsumption,
        StockJournalDetailsInfoProduction: stockJournalDetailsInfoProduction,
        //LedgerPostingInfo: ledgerPostingInfo,
        AdditionalCostInfo: [],
        TotalAdditionalCost: pj.additionalCost,
        Date: pj.date,
        pendingId: pj.stockJournalMasterId,
        Currency: pj.exchangeRateId,
        AdditionalCostCashOrBankId: pj.ledgerId,
        AdditionalCostItems: additionalCostItems
    };
    console.log(toSave);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/saveStockJournalInTransit",
        type: "Post",
        contentType: "application/json",
        data: JSON.stringify(toSave),
        success: function (data) {
            $.each(prod, function (count, row) {
                row.ProductName = findProduct(row.productId).productName;
                row.Barcode = findProduct(row.productId).productCode;
                row.ProductCode = findProduct(row.productId).productCode;
                row.Store = findStore(row.godownId).godownName;
                row.Amount = Utilities.FormatCurrency(row.amount);
                row.Rate = Utilities.FormatCurrency(row.rate);
            });
            $.each(cons, function (count, row) {
                row.ProductName = findProduct(row.productId).productName;
                row.Barcode = findProduct(row.productId).productCode;
                row.ProductCode = findProduct(row.productId).productCode;
                row.Store = findStore(row.godownId).godownName;
                row.Amount = Utilities.FormatCurrency(row.amount);
                row.Rate = Utilities.FormatCurrency(row.rate);
            });
            if (data == true) {
                var columns = [
                    { title: "SL", dataKey: "slno" },
                    { title: "Barcode", dataKey: "Barcode" },
                    { title: "Product Code", dataKey: "ProductCode" },
                    { title: "Product Name", dataKey: "ProductName" },
                    { title: "Warehouse", dataKey: "Store" },
                    { title: "Quanity", dataKey: "qty" },
                ];
                var columns1 = [
                    { title: "SL", dataKey: "slno" },
                    { title: "Barcode", dataKey: "Barcode" },
                    { title: "Product Code", dataKey: "ProductCode" },
                    { title: "Product Name", dataKey: "ProductName" },
                    { title: "Warehouse", dataKey: "Store" },
                    { title: "Quanity", dataKey: "qty" },
                ];

                var doc = new jsPDF('landscape');
                doc.setFontSize(12);
                doc.text('Stock Journal Instruction Manual', 110, 25);
                doc.setFontSize(30);
                doc.text('WICHTECH GROUP', 15, 45);
                doc.setFontSize(8);
                doc.text('5, Wema Terrace, Osborne Estate, 2-9 Udi St, Ikoyi, Lagos, Nigeria', 15, 50);
                doc.text('Narration: ' + pj.narration + '\n\nTransfer Date: ' + Utilities.FormatJsonDate(pj.date) +
                   '\n\nTransfered By: ' + findUsers(pj.userId).userName, 240, 45);
                doc.text('Transfer(s) From', 15, 60);
                doc.autoTable(columns, cons,
                {
                    startY: doc.autoTableEndPosY() + 68, theme: 'grid', styles: {
                        fontSize: 8
                    }
                });
                doc.text('Transfer(s) To', 14, doc.autoTable.previous.finalY + 10);
                doc.autoTable(columns1, prod,
                {
                    startY: doc.autoTable.previous.finalY + 14, theme: 'grid', styles: {
                        fontSize: 8
                    }
                });
                doc.text('____________________\n\n Signature & Date\n\n Confirmed By: ' + findUsers(matUserInfo.UserId).userName, 14, doc.autoTable.previous.finalY + 20);
                doc.save('StockTransfer' + Utilities.FormatJsonDate(pj.date) + '.pdf');

                stockJournalDetailsInfoConsumption = [];
                stockJournalDetailsInfoProduction = [];

                additionalCostItems = [];
                stockJournalMasterInfo = {};
                additionalCostInfo = {};
                totalAdditionalCost = 0.0;

                Utilities.SuccessNotification("Stock Transfer Successful");  //clear controls
                window.location = "/Inventory/StockJournal/stockJournalConfirmation";
            }
            else {
                Utilities.ErrorNotification("Could not save journal!");
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry something went wrong!");
        }
    });
}

function cancelTransfer() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/CancelStockTransfer?id=" + pj.stockJournalMasterId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            if (data > 0) {
                Utilities.SuccessNotification("Stock Transfer cancelled Successfully!");  //clear controls
                Utilities.Loader.Hide();
                $("#detailsModal").modal("hide");
                if (table != "") {
                    table.destroy();
                }
                getPendingStockJournal("2017-01-01", $("#toDate").val(), "");
            }
            else {
                Utilities.ErrorNotification("Stock Transfer couldn't cancel");  //clear controls
                Utilities.Loader.Hide();
            }
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function findLedger(ledgerId) {
    var output = "N/A";
    for (i = 0; i < ledgers.length; i++) {
        if (ledgers[i].ledgerId == ledgerId) {
            output = ledgers[i];
        }
    }
    return output;
}


function findUnit(unitId) {
    var output = {};
    for (i = 0; i < allUnits.length; i++) {
        if (allUnits[i].unitId == unitId) {
            output = allUnits[i];
            break;
        }
    }
    //for (i = 0; i < allUnits.length; i++)
    //{
    //    if (allUnits[i].unitId == unitId) {
    //        output = allUnits[i];
    //        break;
    //    }
    //}
    return output;
}

function findBatch(batchId) {
    var output = {};;
    for (i = 0; i < allBatches.length; i++) {
        if (allBatches[i].batchId == batchId) {
            output = allBatches[i];
            break;
        }
    }
    return output;
}

function findUsers(userId) {
    var output = {};;
    for (i = 0; i < allUsers.length; i++) {
        if (allUsers[i].userId == userId) {
            output = allUsers[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < allStores.length; i++) {
        if (allStores[i].godownId == storeId) {
            output = allStores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < allRacks.length; i++) {
        if (allRacks[i].rackId == rackId) {
            output = allRacks[i];
            break;
        }
    }
    return output;
}

function findProduct(productId) {
    var output = {};
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productId == productId) {
            output = allProducts[i];
            break;
        }
    }
    return output;
}

function findProductByParam(paramType, param) {
    var output = {};
    for (i = 0; i < allProducts.length; i++) {
        if (paramType == "productcode") {
            if (allProducts[i].productCode == param) {
                output = allProducts[i];
                break;
            }
        }
        else if (paramType == "barcode") {
            if (allProducts[i].barcode == param) {
                output = allProducts[i];
                break;
            }
        }
        else if (paramType == "productname") {
            if (allProducts[i].productName == param) {
                output = allProducts[i];
                break;
            }
        }
    }
    return output;
}

function findProductByProductCode(productCode) {
    var output = "N/A";
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productCode == productCode) {
            output = allProducts[i].productCode;
        }
    }
    return output;
}

function findProductByBarCode(barcode) {
    var output = "N/A";
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productCode == barcode) {
            output = allProducts[i].productCode;
        }
    }
    return output;
}

function findProductIdByProductCode(productCode) {
    var output = "N/A";
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productCode == productCode) {
            output = allProducts[i].productId;
        }
    }
    return output;
}

function findProductByProductName(productName) {
    var output = "N/A";
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productName == productName) {
            output = allProducts[i].productName;
        }
    }
    return output;
}

function findProductByProductId(productId) {
    var output = "N/A";
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productId == productId) {
            output = allProducts[i];
        }
    }
    return output;
}