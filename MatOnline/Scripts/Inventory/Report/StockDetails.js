﻿var ReportData = [];
var batches = [];

getStockDetailsReport()
function getStockDetailsReport() {
    obj = {
        FromDate: "default",
        ToDate: "default",
        ProductCode: "default",
        Store: 1,
        Batch: 1,
        ProductId: 1
    };   
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/StockDetails/GetStockDetails",
        type: 'POST',
        data: JSON.stringify(obj),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var objToShow = [];
            $.each(data, function (i, record) {
                
                var prod = ""; //variable is quoted in string cz of an error with string type in the datatable.js lib
                objToShow.push([
                    record.Store,
                    record.ProductName,
                    record.ProductCode,
                    record.RefNo,
                    record.VoucherTypeName,
                    record.Batch,
                    record.Date,
                    record.Rate,
                    record.QtyIn,
                    record.QtyOut,
                    record.QtyBal,
                    record.AvgCost,
                    record.StockVal,
                    
                ]);
 
            });
            table = $('#StockDetailsRptTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

getAllProducts()
function getAllProducts() {
    $.ajax({
        url: API_BASE_URL + "/StockDetails/GetAllProducts",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            console.log(data)
            var str = "";
            str += "<option value = '0'>All</option>";
            $.each(data, function (i, record) {
                str += "<option value = '" + record.productId + "'>" + record.productName + "</option>";
            });
            $("#Product").html(str);
            $("#Product").chosen({ width: "100%" });
        },
        error: function (request, error) {
        }
    });
}

getAllStores()
function getAllStores() {
    $.ajax({
        url: API_BASE_URL + "/StockDetails/GetAllStores",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            var str = "";
            str += "<option value = '0'>All</option>";
            $.each(data, function (i, record) {
                str += "<option value = '" + record.godownId + "'>" + record.godownName + "</option>";
            });
            $("#Store").html(str);
            $("#Store").chosen({ width: "100%" });
        },
        error: function (request, error) {
        }
    });
}

getAllBatches()
function getAllBatches() {
    $.ajax({
        url: API_BASE_URL + "/StockDetails/GetAllBatches",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            var str = "";
            str += "<option value = '0'>All</option>";
            $.each(data, function (i, record) {
                str += "<option value = '" + record.batchId + "'>" + record.batchNo + "</option>";
            });
            $("#Batch").html(str);
            $("#Batch").chosen({ width: "100%" });
        },
        error: function (request, error) {
        }
    });
}

$("#btnSearch").click(function () {
    obj = {
        FromDate: $("#FromDate").val(),
        ToDate: $("#ToDate").val(),
        ProductCode: $("#ProductCode").val(),
        Store: $("#Store").val(),
        Batch: $("#Batch").val(),
        ProductId: $("#ProductId").val()
    };
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/StockDetails/GetStockDetails",
        type: 'POST',
        data: JSON.stringify(obj),
        contentType: "application/json",
        success: function (data) {
            $('#StockDetailsRptTable').html("")
            var objToShow = [];
            $.each(data, function (i, record) {

                var prod = ""; //variable is quoted in string cz of an error with string type in the datatable.js lib
                objToShow.push([
                    record.Store,
                    record.ProductName,
                    record.ProductCode,
                    record.RefNo,
                    record.VoucherTypeName,
                    record.Batch,
                    record.Date,
                    record.Rate,
                    record.QtyIn,
                    record.QtyOut,
                    record.QtyBal,
                    record.AvgCost,
                    record.StockVal,

                ]);

            });

            table = $('#StockDetailsRptTable').DataTable({
                destroy : true,
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
})
