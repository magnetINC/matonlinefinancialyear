﻿var additionalCost = [];
var totalSourceTransferCost = 0.0;
var totalDestinationTransferCost = 0.0;
var totalStockAdjustmentCost = 0.0;
var accountLedgers = [];
var currencies = [];
var finishedGoods = [];
var allProducts = [];
var allProductsFarmBuild = [];
var allStores = [];
var allRacks = [];
var allUnits = [];
var allBatches = [];
var cashOrBanks = [];
var finishedGoodsRawMaterial = [];
var totalAdditionalCost = 0.0;
var finishedGoodsItems = [];
var stockJournalAction = "Manufacturing";  //to store transaction type i.e either manufacturing or transfer or stock adjustment
var transferSource = [];
var transferDestination = [];
var tempRacks = [];
var ledgers = [];
var allUsers = [];
var totalamount = 0;
var unitName = null;

var pendingJournals = [];

var stockJournalMasterInfo = {};
var stockJournalDetailsInfoConsumption = [];
var stockJournalDetailsInfoProduction = [];
//var ledgerPostingInfo = [];
var additionalCostInfo = {};
var additionalCostItems = [];
var stockAdjustment = [];

var table = "";

$(function () {

    $("#formNoTransfer").hide();

    getLookup();
    GetLookupsFarmBuild();

    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });

    //$("#manufacturingPanel").hide#transferPanel
    $("#transferPanel").hide();
    $("#stockAdjustmentPanel").hide();

    $('#manufacturing').change(function () {
        var isChecked = $("#manufacturing").prop("checked");
        if (isChecked) {
            $("#manufacturingPanel").show();
            $("#transferPanel").hide();
            $("#additionalCostAmtDiv").show();
            $("#stockAdjustmentPanel").hide();
            $("#bankOrCashDiv").show();
            $("#additionalCostGrid").show();
            $("#additionalCostAmountGrid").show();
            $("#additionalCostTitle").html("Additional Cost / Narration");
            stockJournalAction = "Manufacturing";
            $("#formNoTransfer").hide();
            $("#PanelForFormNo").show();
        }
    });
    $('#transfer').change(function () {
        var isChecked = $("#transfer").prop("checked");
        if (isChecked) {
            $("#manufacturingPanel").hide();
            $("#transferPanel").show();
            $("#bankOrCashDiv").show();
            $("#additionalCostAmtDiv").show();
            $("#stockAdjustmentPanel").hide();
            $("#additionalCostAmountGrid").show();
            $("#additionalCostGrid").show();
            $("#additionalCostTitle").html("Additional Cost / Narration");
            stockJournalAction = "Stock Transfer";
            $("#formNoTransfer").show();
            $("#PanelForFormNo").hide();
        }
    });

    $('#stockAdjustment').change(function () {
        var isChecked = $("#stockAdjustment").prop("checked");
        if (isChecked) {
            $("#stockAdjustmentPanel").show();
            $("#manufacturingPanel").hide();
            $("#transferPanel").hide();
            $("#bankOrCashDiv").hide();
            $("#additionalCostAmtDiv").hide();
            $("#additionalCostAmountGrid").hide();
            $("#additionalCostGrid").hide();
            $("#additionalCostTitle").html("Narration");
            stockJournalAction = "Stock Out";
            $("#formNoTransfer").hide();
            $("#PanelForFormNo").show();
        }
    });
    $("#addFinishedGoods").click(function () {

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/StockJournal/GetRawMaterialForProduct?productId=" + $("#finishedGoods").val() + "&quantity=" + $("#quantityToManufacture").val(),
            type: "Get",
            contentType: "application/json",
            success: function (data) {
                Utilities.Loader.Hide();
                finishedGoodsItems = [];
                finishedGoodsRawMaterial = data;
                renderRawMaterialForFinishedGoods();
                finishedGoodsItems.push(findProduct($("#finishedGoods").val()));
                console.log("finished item", finishedGoodsItems);
                console.log("finishedGoodsRawMaterial", finishedGoodsRawMaterial);
                renderFinishedGoodsItems();
            },
            error: function () {
                Utilities.Loader.Hide();
            }
        });
    });
    $("#saveStockJournal").click(function () {
        //console.log(stockAdjustment);
        //console.log($("#stockAdjustmentGrid").data().kendoGrid.dataSource.view()); 
        //console.log($("#transferSourceGrid").data().kendoGrid.dataSource.view());
        //console.log($("#transferDestinationGrid").data().kendoGrid.dataSource.view());
        //return;
        var isChecked = $("#transfer").prop("checked");
        if (isChecked) {
           // savePending();
           saveStockJournal();

        }
        else {
            saveStockJournal();
        }
    }); 
    $("#transferGoods").click(function () {
        var src = $("#transferSourceGrid").data().kendoGrid.dataSource.view();
        //console.log(src);
        $.each(src, function (count, row) {
            finishedGoodsRawMaterial.push({
                productId: findProductIdByProductCode(row.productCode),
                Qty: row.qty,
                salesRate: row.rate,
                unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
                //UnitConversionId: row.unitconversionId,
                batchId: row.batchId,
                godownId: row.godownId,
                rackId: row.rackId,
                amount: (row.Qty * row.rate),
                Slno: (count + 1)
            });
            transferDestination.push({
                amount: row.amount,
                barCode: row.barCode,
                godownId: row.godownId,
                productCode: row.productCode,
                productId: row.productId,
                productName: row.productName,
                qty: row.qty,
                rackId: row.rackId,
                rate: row.rate,
                unitId: row.unitId
            });
        });
        //transferDestination = src;

        //stockJournalDetailsInfoConsumption = [];//empty the array b4 populating
        $.each(src, function (count, row) {
            finishedGoodsItems.push({
                productId: findProductIdByProductCode(row.productCode),
                Qty: row.qty,
                salesRate: row.rate,
                unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
                //UnitConversionId: row.unitconversionId,
                batchId: row.batchId,
                godownId: row.godownId,
                rackId: row.rackId,
                amount: (row.Qty * row.rate),
                Slno: (count + 1)
            });
        });
        renderTransferDestinationGrid();

        //$("#transferDestinationGrid").data("kendoGrid").setDataSource(src);
        //console.log(src);


        var datasource = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
        getTotalDestinationTransferCost(datasource);
    });

    getAutoVoucherNo();
    getAutoVoucherNoForTransfer();

});

function renderRawMaterialForFinishedGoods() {
    var output = "";
    var sl = 1;
    var amount = 0.0;
    var totalRawMaterialAmount = 0.0;
    var rate = 0.0;
    $.each(finishedGoodsRawMaterial, function (count, row) {

        rate = row.salesRate;
        amount = (rate * row.Qty);
        totalRawMaterialAmount = totalRawMaterialAmount + amount;
        output += '<tr>\
                    <td>'+ sl + '</td>\
                    <td>' + row.barcode + '</td>\
                    <td>' + row.productCode + '</td>\
                    <td>' + row.productName + '</td>\
                    <td>' + row.Qty + '</td>\
                    <td>' + findUnit(row.unitId).unitName + '</td>\
                    <td>' + findStore(row.godownId).godownName + '</td>\
                    <td>' + findBatch(row.batchId).batchNo + '</td>\
                    <td>' + findRack(row.rackId).rackName + '</td>\
                    <td>' + Utilities.FormatCurrency(rate) + '</td>\
                    <td>' + Utilities.FormatCurrency(amount) + '</td>\
                  </tr>';
        sl = sl + 1;
    });
    $("#manufacturingLineItem").html(output);
    $("#totalRawMaterialAmt").val(Utilities.FormatCurrency(totalRawMaterialAmount));
}

function renderFinishedGoodsItems() {
    var output = "";
    var sl = 1;
    var amount = 0.0;
    var totalFinishedGoodsAmount = 0.0;
    var rate = 0.0;
    $.each(finishedGoodsItems, function (count, row) {
        rate = row.salesRate;
        amount = (rate * $("#quantityToManufacture").val());
        totalFinishedGoodsAmount = totalFinishedGoodsAmount + amount;
        output += '<tr>\
                    <td>'+ sl + '</td>\
                    <td>' + row.productCode + '</td>\
                    <td>' + row.productCode + '</td>\
                    <td>' + row.productName + '</td>\
                    <td>' + $("#quantityToManufacture").val() + '</td>\
                    <td>' + findUnit(row.unitId).unitName + '</td>\
                    <td>' + findStore(row.godownId).godownName + '</td>\
                    <td>' + row.batchNo + '</td>\
                    <td>' + findRack(row.rackId).rackName + '</td>\
                    <td>' + Utilities.FormatCurrency(rate) + '</td>\
                    <td>' + Utilities.FormatCurrency(amount) + '</td>\
                  </tr>';
        sl = sl + 1;
    });
    $("#finishedGoodsLineItem").html(output);
    $("#finishedGoodsGrandTotal").val(Utilities.FormatCurrency(totalFinishedGoodsAmount));
}

function renderAdditionalCostGrid() {
    $("#additionalCostGrid").kendoGrid({
        dataSource: {
            transport: {
                read: function (entries) {
                    entries.success(additionalCost);
                },
                create: function (entries) {
                    entries.success(entries.data);
                },
                update: function (entries) {
                    entries.success();
                },
                destroy: function (entries) {
                    entries.success();
                },
                parameterMap: function (data) { return JSON.stringify(data); }
            },
            schema: {
                model: {
                    id: "ledgerId",
                    fields: {
                        ledgerId: { editable: true, validation: { required: true } },
                        amount: { editable: true, validation: { required: true, type: "number" } },
                    }
                }
            },
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 5,
            pageSizes: [5, 10, 20, 50, 100],
            previousNext: true,
            buttonCount: 5,
        },
        toolbar: [{ name: 'create', text: 'Add Cost' }],
        columns: [
            //{title:"#",template:"#= ++count #"},
            { field: "ledgerId", title: "Ac/ Ledger", editor: accountLedgerDropDownEditor, template: "#= getAccountLedger(ledgerId) #" },
            { field: "amount", title: "Amount" },
            {
                command: [{ name: 'edit', text: { edit: "", update: "", cancel: "" } }, { name: 'destroy', text: '' }]
            }],
        editable: "inline",
        save: function (e) {
            var datasource = $("#additionalCostGrid").data().kendoGrid.dataSource.view();
            getTotalAdditionalCost(datasource);
            //console.log(datasource);
        },
        remove: function (e) {
            var datasource = $("#additionalCostGrid").data().kendoGrid.dataSource.view();
            var indexToRemove = getKendoLineItemIndex(datasource, e.model);
            datasource.splice(indexToRemove, 1);
            getTotalAdditionalCost(datasource);
        }
    });

}

function renderTransferSourceGrid() {
    $("#transferSourceGrid").kendoGrid({
        dataSource: {
            transport: {
                read: function (entries) {
                    entries.success(transferSource);
                },
                create: function (entries) {
                    entries.success(entries.data);
                },
                update: function (entries) {
                    entries.success();
                },
                destroy: function (entries) {
                    entries.success();
                },
                parameterMap: function (data) { return JSON.stringify(data); }
            },
            schema: {
                model: {
                    id: "productId",
                    fields: {
                        productCode: { editable: true, validation: { required: false } },
                        barCode: { editable: true, validation: { required: false } },
                        productName: { editable: true, validation: { required: false } },
                        qty: { editable: true, validation: { required: false, type: "number" } },
                        unitId: { editable: true, validation: { required: false } },
                        godownId: { editable: true, validation: { required: false } },
                        rackId: { editable: true, validation: { required: false } },
                        //batchId: { editable: true, validation: { required: true } },
                        rate: { editable: true, validation: { required: false, type: "number" } },
                        amount: { editable: true, validation: { required: false, type: "number" } },
                    }
                }
            },
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 15,
            pageSizes: [5, 10, 20, 50, 100],
            previousNext: true,
            buttonCount: 5,
        },
        toolbar: [{ name: 'create', text: 'Add Item' }],
        columns: [
            { field: "productCode", title: "Product Code", editor: productCodeDropDownEditor, template: "#= findProductByProductCode(productCode) #" },
            { field: "barCode", title: "BarCode", editor: barCodeDropDownEditor, template: "#= findProductByBarCode(barCode) #" },
            { field: "productName", title: "Product Name", editor: productNameDropDownEditor, template: "#= findProductByProductName(productName) #" },
            { field: "qty", title: "Quantity", editor: numericEditor },
            { field: "godownId", title: "Wareouse", editor: storeDropDownEditor, template: "#= showStore(godownId) #" },
            { field: "rackId", title: "Rack", editor: rackDropDownEditor, template: "#= showRack(rackId) #" },
            { field: "rate", title: "Rate" },
            {
                field: "amount", title: "Amount",
                editor: function (cont, options) {
                    $("<span>" + options.model.amount + "</span>").appendTo(cont);
                }
            },
            {
                command: [{ name: 'destroy', text: '' }]
            }],
        editable: "incell",
        save: function (data) {

            var datasource = $("#transferSourceGrid").data().kendoGrid.dataSource.view();
            //console.log(datasource);
            //getTotalSourceTransferCost(datasource);

            //console.log(datasource);
        },
        remove: function (e) {
            var datasource = $("#transferSourceGrid").data().kendoGrid.dataSource.view();
            var indexToRemove = getKendoLineItemIndex(datasource, e.model);
            datasource.splice(indexToRemove, 1);
            getTotalSourceTransferCost(datasource);
        }
    });

}

function renderTransferDestinationGrid() {
    $("#transferDestinationGrid").kendoGrid({
        dataSource: {
            transport: {
                read: function (entries) {
                    entries.success(transferDestination);
                    //console.log("entries",entries);
                    getTotalDestinationTransferCost(transferDestination);
                },
                create: function (entries) {
                    entries.success(entries.data);
                },
                update: function (entries) {
                    entries.success();
                },
                destroy: function (entries) {
                    entries.success();
                },
                parameterMap: function (data) { return JSON.stringify(data); }
            },
            schema: {
                model: {
                    id: "productId",
                    fields: {
                        productId: { editable: false, validation: { required: true } },
                        productCode: { editable: false, validation: { required: true } },
                        barCode: { editable: false, validation: { required: true } },
                        productName: { editable: false, validation: { required: true } },
                        qty: { editable: false, validation: { required: true, type: "number" } },
                        unitId: { editable: false, validation: { required: true } },
                        godownId: { editable: true, validation: { required: true } },
                        rackId: { editable: true, validation: { required: true } },
                        //batchId: { editable: true, validation: { required: true } },
                        rate: { editable: false, validation: { required: true, type: "number" } },
                        amount: { editable: false, validation: { required: true, type: "number" } },
                    }
                }
            },
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 15,
            pageSizes: [5, 10, 20, 50, 100],
            previousNext: true,
            buttonCount: 5,
        },
        //toolbar: [{ name: 'create', text: 'Add Goods' }], removed on recommendation by toyin based on what is in the windows form
        columns: [
            //{title:"#",template:"#= ++count #"},
            //{ field: "ledgerId", title: "Ac/ Ledger", editor: accountLedgerDropDownEditor, template: "#= getAccountLedger(ledgerId) #" },
            //{ field: "productCode", title: "Product Code", editor: productCodeDestinationDropDownEditor, template: "#= findProductByProductCode(productCode) #" },
            { field: "barCode", title: "BarCode", editor: barCodeDestinationDropDownEditor, template: "#= findProductByBarCode(barCode) #" },
            { field: "productName", title: "Product Name", editor: productNameDestinationDropDownEditor, template: "#= findProductByProductName(productName) #" },
            { field: "qty", title: "Quantity", editor: destinationTransferNumericEditor },
            { field: "godownId", title: "Warehouse", editor: storeDestinationDropDownEditor, template: "#= showStore(godownId) #" },
            { field: "rackId", title: "Rack", editor: rackDestinationDropDownEditor, template: "#= showRack(rackId) #" },
            //{ field: "batchId", title: "Batch" },
            { field: "rate", title: "Rate" },
            { field: "amount", title: "Amount" },
            {
                command: [{ name: 'destroy', text: '' }]
            }],
        editable: "incell",
        edit: function () {
            // console.log("edit");
        },
        beforeEdit: function (e) {
            var columnIndex = this.cellIndex(e.container);
            var fieldName = this.thead.find("th").eq(columnIndex).data("productCode");

            if (!isEditable(fieldName, e.model)) {
                e.preventDefault();
            }
        },
        save: function (e) {
            var datasource = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
            getTotalDestinationTransferCost(datasource);
            transferDestination = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
            //getTotalAdditionalCost(datasource);
            console.log(transferDestination);
            finishedGoodsRawMaterial = [];
            $.each(transferDestination, function (count, row) {
                finishedGoodsRawMaterial.push({
                    productId: findProductIdByProductCode(row.productCode),
                    Qty: row.qty,
                    salesRate: row.rate,
                    unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
                    //UnitConversionId: row.unitconversionId,
                    batchId: row.batchId,
                    godownId: row.godownId,
                    rackId: row.rackId,
                    amount: (row.Qty * row.rate),
                    Slno: (count + 1)
                });
            });
        },
        remove: function (e) {
            var datasource = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
            var indexToRemove = getKendoLineItemIndex(datasource, e.model);
            datasource.splice(indexToRemove, 1);
            getTotalDestinationTransferCost(datasource);

            finishedGoodsRawMaterial = [];
            $.each(transferDestination, function (count, row) {
                finishedGoodsRawMaterial.push({
                    productId: findProductIdByProductCode(row.productCode),
                    Qty: row.qty,
                    salesRate: row.rate,
                    unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
                    //UnitConversionId: row.unitconversionId,
                    batchId: row.batchId,
                    godownId: row.godownId,
                    rackId: row.rackId,
                    amount: (row.Qty * row.rate),
                    Slno: (count + 1)
                });
            });
            //var datasource = $("#additionalCostGrid").data().kendoGrid.dataSource.view();
            //var indexToRemove = getKendoLineItemIndex(datasource, e.model);
            //datasource.splice(indexToRemove, 1);
            //getTotalAdditionalCost(datasource);
        }
    });

}

function renderStockAdjustmentGrid() {
    $("#stockAdjustmentGrid").kendoGrid({
        dataSource: {
            transport: {
                read: function (entries) {
                    entries.success(stockAdjustment);
                },
                create: function (entries) {
                    entries.success(entries.data);
                },
                update: function (entries) {
                    entries.success();
                },
                destroy: function (entries) {
                    sentries.success();
                },
                parameterMap: function (data) { return JSON.stringify(data); }
            },
            schema: {
                model: {
                    id: "productId",
                    fields: {
                        productId: { editable: true },
                        productCode: { editable: true, validation: { required: false } },
                        barCode: { editable: true, validation: { required: false } },
                        productName: { editable: true, validation: { required: false } },
                        qty: { editable: true, validation: { required: false, type: "number" } },
                        unitId: { editable: true, validation: { required: false } },
                        godownId: { editable: true, validation: { required: false } },
                        rackId: { editable: true, validation: { required: false } },
                        //batchId: { editable: true, validation: { required: true } },
                        rate: { editable: true, validation: { required: false, type: "number" } },
                        amount: { editable: true, validation: { required: false, type: "number" } },
                    }
                }
            },
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 5,
            pageSizes: [5, 10, 20, 50, 100],
            previousNext: true,
            buttonCount: 5,
        },
        toolbar: [{ name: 'create', text: 'Add Item' }],
        columns: [
            //{title:"#",template:"#= ++count #"},
            //{ field: "ledgerId", title: "Ac/ Ledger", editor: accountLedgerDropDownEditor, template: "#= getAccountLedger(ledgerId) #" },
            { field: "productCode", title: "Product Code", editor: productCodeStockAdjustmentDropDownEditor, template: "#= findProductByProductCode(productCode) #" },
            { field: "barCode", title: "BarCode", editor: barCodeStockAdjustmentDropDownEditor, template: "#= findProductByProductCode(barCode) #" },
            { field: "productName", title: "Product Name", editor: productNameStockAdjustmentDropDownEditor, template: "#= findProductByProductName(productName) #" },
            { field: "qty", title: "Quantity", editor: numericStockAdjustmentEditor },
            { field: "godownId", title: "Warehouse", editor: storeStockAdjustmentDropDownEditor, template: "#= showStore(godownId) #" },
            { field: "rackId", title: "Rack", editor: rackStockAdjustmentDropDownEditor, template: "#= showRack(rackId) #" },
            //{ field: "batchId", title: "Batch" },
            { field: "rate", title: "Rate" },
            { field: "amount", title: "Amount", editor: amountStockAdjustmentNumericEditor },
            {
                command: [{ name: 'destroy', text: '' }]
            }
        ],
        editable: "incell",
        save: function (e) {
            //var datasource = $("#stockAdjustmentGrid").data().kendoGrid.dataSource.view();
            ////console.log(e.model);
            //getTotalStockAdjustmentCost(datasource);
            //$.each(datasource, function (count, row) {
            //    finishedGoodsRawMaterial.push({
            //        productId: findProductIdByProductCode(row.productCode),
            //        Qty: row.qty,
            //        salesRate: row.rate,
            //        unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
            //        //UnitConversionId: row.unitconversionId,
            //        batchId: row.batchId,
            //        godownId: row.godownId,
            //        rackId: row.rackId,
            //        amount: (row.Qty * row.rate),
            //        Slno: (count + 1)
            //    });
            //});
            //console.log(finishedGoodsRawMaterial);
        },
        remove: function (e) {
            var datasource = $("#stockAdjustmentGrid").data().kendoGrid.dataSource.view();
            var indexToRemove = getKendoLineItemIndex(datasource, e.model);
            datasource.splice(indexToRemove, 1);
            getTotalStockAdjustmentCost(datasource);
            $.each(datasource, function (count, row) {
                finishedGoodsRawMaterial.push({
                    productId: findProductIdByProductCode(row.productCode),
                    Qty: row.qty,
                    salesRate: row.rate,
                    unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
                    //UnitConversionId: row.unitconversionId,
                    batchId: row.batchId,
                    godownId: row.godownId,
                    rackId: row.rackId,
                    amount: (row.Qty * row.rate),
                    Slno: (count + 1)
                });
            });
        }
    });

}

/********Editors for source transfer**********/
function accountLedgerDropDownEditor(container, options) {
    $('<input required name="' + options.field + '" data-text-field="ledgerName" data-value-field="ledgerId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: {
                data: accountLedgers
            },
            template: '<span>#: ledgerName #</span>',
            filter: "contains",
        });
}

function productCodeDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="productCode" data-value-field="productCode" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productCode",
            dataValueField: "productCode",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productCode #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#transferSourceGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                model.set("productId", findProductByParam("productcode", model.productCode).productId);
                model.set("barCode", findProductByParam("productcode", model.productCode).productCode);
                model.set("productName", findProductByParam("productcode", model.productCode).productName);
                model.set("rate", findProductByParam("productcode", model.productCode).salesRate);
            }
        });
}

function barCodeDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="productCode" data-value-field="productCode" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productCode",
            dataValueField: "productCode",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productCode #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#transferSourceGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                model.set("productId", findProductByParam("productcode", model.barCode).productId);
                model.set("productCode", findProductByParam("productcode", model.barCode).productCode);
                model.set("productName", findProductByParam("productcode", model.barCode).productName);
                model.set("rate", findProductByParam("productcode", model.barCode).salesRate);
            }
        });
}

function productNameDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="productName" data-value-field="productName" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productName",
            dataValueField: "productName",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productName #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#transferSourceGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                model.set("productId", findProductByParam("productname", model.productName).productId);
                model.set("barCode", findProductByParam("productname", model.productName).productCode);
                model.set("productCode", findProductByParam("productname", model.productName).productCode);
                model.set("rate", findProductByParam("productname", model.productName).salesRate);
            }
        });
}

function numericEditor(container, options) {
    $('<input value="0" name="' + options.field + '" data-text-field="" data-value-field="" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoNumericTextBox({
            min: 0,
            change: function (data) {
                var grid = $("#transferSourceGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                //model.set("barCode", findProductByParam("productname", model.productName).productCode);
                //model.set("productName", findProductByParam("productname", model.productName).productName);
                //model.set("rate", findProductByParam("productname", model.productName).salesRate);
                model.set("amount", model.qty * model.rate);
                var datasource = $("#transferSourceGrid").data().kendoGrid.dataSource.view();
                getTotalSourceTransferCost(datasource);
            }
        });
}

function destinationTransferNumericEditor(container, options) {
    $('<input value="0" name="' + options.field + '" data-text-field="" data-value-field="" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoNumericTextBox({
            min: 0,
            change: function (data) {
                var grid = $("#transferDestinationGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                //model.set("barCode", findProductByParam("productname", model.productName).productCode);
                //model.set("productName", findProductByParam("productname", model.productName).productName);
                //model.set("rate", findProductByParam("productname", model.productName).salesRate);
                model.set("amount", model.qty * model.rate);
                var datasource = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
                getTotalDestinationTransferCost(datasource);
            }
        });
}

function amountStockAdjustmentNumericEditor(container, options) {
    $('<input readonly name="' + options.field + '" data-text-field="" data-value-field="" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoNumericTextBox({
            min: 0
        });
}
function storeDropDownEditor(container, options) {
    $('<input name="' + options.field + '" class="tst" data-text-field="godownName" data-value-field="godownId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "godownName",
            dataValueField: "godownId",
            dataSource: {
                data: allStores
            },
            template: '<span>#: godownName #</span>',
            filter: "contains",
            change: function (data) {
                //tempRacks = allRacks;
                var grid = $("#simpleTransferSourceGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                //console.log($("#transferSourceGrid").data("kendoGrid").dataSource.data()[0]);
                //model = grid.dataItem(this.element.closest("tr"));
                console.log("model", this);
                //console.log("element", this.element);
                //model.set("barCode", findProductByParam("productname", model.productName).productCode);
                //model.set("productName", findProductByParam("productname", model.productName).productName);
                //model.set("rate", findProductByParam("productname", model.productName).salesRate);
                // model.set("amount", model.qty * model.rate);
                searchProduct(model.productCode, "ProductCode", model);
            }
        });
}

function rackDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="rackName" data-value-field="rackId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "rackName",
            dataValueField: "rackId",
            dataSource: {
                data: tempRacks
            },
            template: '<span>#: rackName #</span>',
            filter: "contains",
            open: function () {
                var grid = $("#transferSourceGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                var st = model.godownId;
                var filtered = [];
                model.set("rackId", "");     //empty what was selected
                $.each(allRacks, function (count, row) {
                    if (row.godownId == st) {
                        filtered.push(row);
                    }
                });
                this.setDataSource(filtered);
                //console.log(allRacks);
            }
        });
}
/*********end of source transfer editors*********************/

/********Editors for destination transfer**********/
function productCodeDestinationDropDownEditor(container, options) {
    $('<input required name="' + options.field + '" data-text-field="productCode" data-value-field="productCode" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productCode",
            dataValueField: "productCode",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productCode #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#transferDestinationGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                model.set("barCode", findProductByParam("productcode", model.productCode).productCode);
                model.set("productName", findProductByParam("productcode", model.productCode).productName);
                model.set("rate", findProductByParam("productcode", model.productCode).salesRate);
            }
        });
}

function barCodeDestinationDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="productCode" data-value-field="productCode" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productCode",
            dataValueField: "productCode",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productCode #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#transferSourceGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                model.set("barCode", findProductByParam("barcode", model.barCode).productCode);
                model.set("productName", findProductByParam("barcode", model.barCode).productName);
                model.set("rate", findProductByParam("barcode", model.barCode).salesRate);
            }
        });
}

function productNameDestinationDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="productName" data-value-field="productName" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productName",
            dataValueField: "productName",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productName #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#transferDestinationGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                model.set("barCode", findProductByParam("productname", model.productName).productCode);
                model.set("productName", findProductByParam("productname", model.productName).productName);
                model.set("rate", findProductByParam("productname", model.productName).salesRate);
            }
        });
}

function numericDestinationEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="" data-value-field="" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoNumericTextBox({
            min: 0,
            change: function (data) {
                var grid = $("#transferDestinationGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                //model.set("barCode", findProductByParam("productname", model.productName).productCode);
                //model.set("productName", findProductByParam("productname", model.productName).productName);
                //model.set("rate", findProductByParam("productname", model.productName).salesRate);
                model.set("amount", model.qty * model.rate);
            }
        });
}

function storeDestinationDropDownEditor(container, options) {
    $('<input name="' + options.field + '" class="tst" data-text-field="godownName" data-value-field="godownId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "godownName",
            dataValueField: "godownId",
            dataSource: {
                data: allStores
            },
            template: '<span>#: godownName #</span>',
            filter: "contains",
            change: function (data) {
                //tempRacks = allRacks;
                var grid = $("#transferDestinationGrid").data("kendoGrid");
                //console.log($("#transferSourceGrid").data("kendoGrid").dataSource.data()[0]);
                //model = grid.dataItem(this.element.closest("tr"));
                console.log("model", grid);
                //console.log("element", this.element);
                //model.set("barCode", findProductByParam("productname", model.productName).productCode);
                //model.set("productName", findProductByParam("productname", model.productName).productName);
                //model.set("rate", findProductByParam("productname", model.productName).salesRate);
                // model.set("amount", model.qty * model.rate);
            }
        });
}

function rackDestinationDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="rackName" data-value-field="rackId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "rackName",
            dataValueField: "rackId",
            dataSource: {
                data: tempRacks
            },
            template: '<span>#: rackName #</span>',
            filter: "contains",
            open: function () {
                var grid = $("#transferDestinationGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                var st = model.godownId;
                var filtered = [];
                model.set("rackId", "");     //empty what was selected
                $.each(allRacks, function (count, row) {
                    if (row.godownId == st) {
                        filtered.push(row);
                    }
                });
                this.setDataSource(filtered);
                //console.log(allRacks);
            }
        });
}
/*********end of manufacturing editors*********************/

/********Editors for stock adjustment**********/
function productCodeStockAdjustmentDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="productCode" data-value-field="productCode" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productCode",
            dataValueField: "productCode",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productCode #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#stockAdjustmentGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                model.set("productId", findProductByParam("productcode", model.productCode).productId);
                model.set("barCode", findProductByParam("productcode", model.productCode).productCode);
                model.set("productName", findProductByParam("productcode", model.productCode).productName);
                model.set("rate", findProductByParam("productcode", model.productCode).salesRate);
            }
        });
}

function barCodeStockAdjustmentDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="productCode" data-value-field="productCode" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productCode",
            dataValueField: "productCode",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productCode #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#stockAdjustmentGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                model.set("productId", findProductByParam("productcode", model.barCode).productId);
                model.set("productCode", findProductByParam("productcode", model.barCode).productCode);
                model.set("productName", findProductByParam("productcode", model.barCode).productName);
                model.set("rate", findProductByParam("productcode", model.barCode).salesRate);
            }
        });
}

function productNameStockAdjustmentDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="productName" data-value-field="productName" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "productName",
            dataValueField: "productName",
            dataSource: {
                data: allProducts
            },
            template: '<span>#: productName #</span>',
            filter: "contains",
            change: function (data) {
                var grid = $("#stockAdjustmentGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                model.set("productId", findProductByParam("productname", model.productCode).productId);
                model.set("barCode", findProductByParam("productname", model.productName).productCode);
                model.set("productName", findProductByParam("productname", model.productName).productName);
                model.set("rate", findProductByParam("productname", model.productName).salesRate);
            }
        });
}

function numericStockAdjustmentEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="" data-value-field="" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoNumericTextBox({
            min: 0,
            change: function (data) {
                var grid = $("#stockAdjustmentGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                //model.set("barCode", findProductByParam("productname", model.productName).productCode);
                //model.set("productName", findProductByParam("productname", model.productName).productName);
                //model.set("rate", findProductByParam("productname", model.productName).salesRate);
                model.set("amount", model.qty * model.rate);
            }
        });
}

function storeStockAdjustmentDropDownEditor(container, options) {
    $('<input name="' + options.field + '" class="tst" data-text-field="godownName" data-value-field="godownId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "godownName",
            dataValueField: "godownId",
            dataSource: {
                data: allStores
            },
            template: '<span>#: godownName #</span>',
            filter: "contains",
            change: function (data) {
                //tempRacks = allRacks;
                var grid = $("#stockAdjustmentGrid").data("kendoGrid");
                //console.log($("#transferSourceGrid").data("kendoGrid").dataSource.data()[0]);
                //model = grid.dataItem(this.element.closest("tr"));
                console.log("model", this);
                //console.log("element", this.element);
                //model.set("barCode", findProductByParam("productname", model.productName).productCode);
                //model.set("productName", findProductByParam("productname", model.productName).productName);
                //model.set("rate", findProductByParam("productname", model.productName).salesRate);
                // model.set("amount", model.qty * model.rate);
            }
        });
}

function rackStockAdjustmentDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="rackName" data-value-field="rackId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "rackName",
            dataValueField: "rackId",
            dataSource: {
                data: tempRacks
            },
            template: '<span>#: rackName #</span>',
            filter: "contains",
            open: function () {
                var grid = $("#stockAdjustmentGrid").data("kendoGrid"),
                model = grid.dataItem(this.element.closest("tr"));
                var st = model.godownId;
                var filtered = [];
                model.set("rackId", "");     //empty what was selected
                $.each(allRacks, function (count, row) {
                    if (row.godownId == st) {
                        filtered.push(row);
                    }
                });
                this.setDataSource(filtered);
                //console.log(allRacks);
            }
        });
}
/*********end of stock adjustment editors*********************/

function getAccountLedger(ledgerId) {
    for (i = 0; i < accountLedgers.length; i++) {
        if (accountLedgers[i].ledgerId == ledgerId) {
            return accountLedgers[i].ledgerName;
        }
    }
    return "";
}

function getTotalAdditionalCost(obj) {
    var output = 0.0;
    for (i = 0; i < obj.length; i++) {
        output = output + parseFloat(obj[i].amount);
    }
    totalAdditionalCost = output;
    $("#additionalCost").val(Utilities.FormatCurrency(totalAdditionalCost));
}

function getTotalSourceTransferCost(obj) {
    var output = 0.0;
    for (i = 0; i < obj.length; i++) {
        output = output + parseFloat(obj[i].amount);
    }
    totalSourceTransferCost = output;
    $("#totalSourceTransferCost").val(Utilities.FormatCurrency(totalSourceTransferCost));
}

function getTotalDestinationTransferCost(obj) {
    var output = 0.0;
    for (i = 0; i < obj.length; i++) {
        output = output + parseFloat(obj[i].amount);
    }
    totalDestinationTransferCost = output;
    $("#totalDestinationTransferCost").val(Utilities.FormatCurrency(totalDestinationTransferCost));
}

function getTotalStockAdjustmentCost(obj) {
    var output = 0.0;
    for (i = 0; i < obj.length; i++) {
        output = output + parseFloat(obj[i].amount);
    }
    totalStockAdjustmentCost = output;
    $("#stockAdjustmentTotalCost").val(Utilities.FormatCurrency(totalStockAdjustmentCost));
}

function GetLookupsFarmBuild()
{
    Utilities.Loader.Show();
  
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetLookupsFarmBuild",
        type: "Get",
        contentType: "application/json",
        success: function (data)
        {
            console.log(data);
         
            allProductsFarmBuild = data.AllProducts;
           
            Utilities.Loader.Hide();
        },
        error: function ()
        {
            Utilities.Loader.Hide();
        }
    });
}

function getLookup() {

    Utilities.Loader.Show();
    //var lookUpAjax = $.ajax({
    //    url: API_BASE_URL + "/StockJournal/GetLookups",
    //    type: "Get",
    //    contentType: "application/json",
    //});
    //$.when(lookUpAjax)
    //.done(function (dataLookUp) {
    //    orderListForSearch = dataMyLocation[2].responseJSON;
    //});

    

    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetLookups",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            finishedGoods = data.FinishedGoods;
            currencies = data.Currencies;
            allProducts = data.AllProducts;
            allStores = data.AllStores;
            allUnits = data.AllUnits;
            allRacks = data.AllRacks;
            allBatches = data.AllBatches;
            cashOrBanks = data.CashOrBanks;
            accountLedgers = data.AccountLedgers;
            allUsers = data.users;
            var today = new Date();
            getPendingStockJournal("2017-01-01", today.toLocaleDateString());
            $("#finishedGoods").html('<option value=""></option>' + Utilities.PopulateDropDownFromArray(finishedGoods, 1, 0));
            $("#currency").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));
            $("#bankOrCash").html(Utilities.PopulateDropDownFromArray(cashOrBanks, 1, 0));

            renderAdditionalCostGrid();
            renderTransferSourceGrid();
            renderStockAdjustmentGrid();

            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < allUnits.length; i++) {
        if (allUnits[i].unitId == unitId) {
            output = allUnits[i];
            break;
        }
    }
    //for (i = 0; i < allUnits.length; i++)
    //{
    //    if (allUnits[i].unitId == unitId) {
    //        output = allUnits[i];
    //        break;
    //    }
    //}
    return output;
}

function findBatch(batchId) {
    var output = {};;
    for (i = 0; i < allBatches.length; i++) {
        if (allBatches[i].batchId == batchId) {
            output = allBatches[i];
            break;
        }
    }
    return output;
}

function findUsers(userId) {
    var output = {};;
    for (i = 0; i < allUsers.length; i++) {
        if (allUsers[i].userId == userId) {
            output = allUsers[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < allStores.length; i++) {
        if (allStores[i].godownId == storeId) {
            output = allStores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < allRacks.length; i++) {
        if (allRacks[i].rackId == rackId) {
            output = allRacks[i];
            break;
        }
    }
    return output;
}

function findProduct(productId) {
    var output = {};
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productId == productId) {
            output = allProducts[i];
            break;
        }
    }
    return output;
}

function findProductByParam(paramType, param) {
    var output = {};
    for (i = 0; i < allProducts.length; i++) {
        if (paramType == "productcode") {
            if (allProducts[i].productCode == param) {
                output = allProducts[i];
                break;
            }
        }
        else if (paramType == "barcode") {
            if (allProducts[i].barcode == param) {
                output = allProducts[i];
                break;
            }
        }
        else if (paramType == "productname") {
            if (allProducts[i].productName == param) {
                output = allProducts[i];
                break;
            }
        }
    }
    return output;
}

function getKendoLineItemIndex(datasource, objectToFindIndex)    //function was created to remove lineitem from row wen its removed from
{                                                               //kendo grid
    for (i = 0; i < datasource.length; i++) {
        if (objectToFindIndex.id == datasource[i].id) {
            return i;
        }
    }
    return -1;
}

    function saveStockJournal() {
        if ($("#transactionDate").val() == "") {
            Utilities.ErrorNotification("Please select transaction date!");
            return;
        }
        else {
            if (stockJournalAction == "Stock Out") {
                var datasource = $("#stockAdjustmentGrid").data().kendoGrid.dataSource.view();
                //console.log(e.model);
                getTotalStockAdjustmentCost(datasource);
                $.each(datasource, function (count, row) {
                    finishedGoodsRawMaterial.push({
                        productId: findProductIdByProductCode(row.productCode),
                        Qty: row.qty,
                        salesRate: row.rate,
                        unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
                        //UnitConversionId: row.unitconversionId,
                        batchId: row.batchId,
                        godownId: row.godownId,
                        rackId: row.rackId,
                        amount: (row.Qty * row.rate),
                        Slno: (count + 1)
                    });
                });
                $.each(finishedGoodsRawMaterial, function (count, row) {
                    stockJournalDetailsInfoConsumption.push({
                        ProductId: row.productId,
                        Qty: row.Qty,
                        Rate: row.salesRate,
                        UnitId: row.unitId,
                        UnitConversionId: row.unitconversionId,
                        BatchId: row.batchId,
                        GodownId: row.godownId,
                        RackId: row.rackId,
                        Amount: (row.Qty * row.salesRate),
                        Slno: (count + 1)
                    });
                });
            }
            else if (stockJournalAction == "Stock Transfer") {
                var src = $("#transferSourceGrid").data().kendoGrid.dataSource.view();
                stockJournalDetailsInfoConsumption = [];
                $.each(src, function (count, row) {
                    stockJournalDetailsInfoConsumption.push({
                        ProductId: row.productId,
                        Qty: row.qty,
                        Rate: row.rate,
                        UnitId: "",
                        UnitConversionId: "" /*row.unitconversionId*/,
                        BatchId: "",
                        GodownId: row.godownId,
                        RackId: row.rackId,
                        Amount: (row.qty * row.rate),
                        Slno: (count + 1)
                    });
                });

                var destination = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
                stockJournalDetailsInfoProduction = [];
                $.each(destination, function (count, row) {
                    stockJournalDetailsInfoProduction.push({
                        ProductId: row.productId,
                        Qty: row.qty,
                        Rate: row.rate,
                        UnitId: "",
                        UnitConversionId: "",
                        BatchId: "",
                        GodownId: row.godownId,
                        RackId: row.rackId,
                        Amount: (row.rate * row.qty),
                        Slno: (count + 1)
                    });
                });
            }
            else if (stockJournalAction == "Manufacturing") {
                $.each(finishedGoodsRawMaterial, function (count, row) {
                    stockJournalDetailsInfoConsumption.push({
                        ProductId: row.productId,
                        Qty: row.Qty,
                        Rate: row.salesRate,
                        UnitId: row.unitId,
                        UnitConversionId: row.unitconversionId,
                        BatchId: row.batchId,
                        GodownId: row.godownId,
                        RackId: row.rackId,
                        Amount: (row.Qty * row.salesRate),
                        Slno: (count + 1)
                    });
                });
                //stockJournalDetailsInfoProduction = []; //empty the array b4 populating
                $.each(finishedGoodsItems, function (count, row) {
                    var qt = (stockJournalAction == "Manufacturing" ? $("#quantityToManufacture").val() : row.Qty);
                    stockJournalDetailsInfoProduction.push({
                        ProductId: row.productId,
                        Qty: qt,
                        Rate: row.salesRate,
                        UnitId: row.unitId,
                        UnitConversionId: row.unitconversionId,
                        BatchId: 1,
                        GodownId: row.godownId,
                        RackId: row.rackId,
                        Amount: (row.salesRate * qt),
                        Slno: (count + 1)
                    });
                });
            }

            stockJournalMasterInfo = {
                Narration: $("#narration").val(),
                Extra1: stockJournalAction,
                InvoiceNo: $("#formNo").val()
            };

            additionalCostItems = [];
            var kendoAdditionalCostSource = $("#additionalCostGrid").data().kendoGrid.dataSource.view();
            $.each(kendoAdditionalCostSource, function (count, row) {
                additionalCostItems.push({
                    LedgerId: row.ledgerId,
                    Amount: row.amount
                });
            });
            var toSave = {
                StockJournalMasterInfo: stockJournalMasterInfo,
                StockJournalDetailsInfoConsumption: stockJournalDetailsInfoConsumption,
                StockJournalDetailsInfoProduction: stockJournalDetailsInfoProduction,
                //LedgerPostingInfo: ledgerPostingInfo,
                AdditionalCostInfo: additionalCostInfo,
                TotalAdditionalCost: totalAdditionalCost,
                Date: $("#transactionDate").val(),
                Currency: $("#currency").val(),
                AdditionalCostCashOrBankId: $("#bankOrCash").val(),
                AdditionalCostItems: additionalCostItems
            };
            console.log(toSave);
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/StockJournal/SaveStockJournal",
                type: "Post",
                contentType: "application/json",
                data: JSON.stringify(toSave),
                success: function (data) {
                    if (data == true) {
                        stockJournalDetailsInfoConsumption = [];
                        stockJournalDetailsInfoProduction = [];
                        additionalCostItems = [];
                        stockJournalMasterInfo = {};
                        additionalCostInfo = {};
                        totalAdditionalCost = 0.0;
                        $('#manufacturing').prop("checked", "checked");
                        $("#quantityToManufacture").val(0);
                        $("#narration").val("");
                        $("#additionalCostTitle").html("Additional Cost / Narration");
                        $("#stockAdjustmentGrid").data('kendoGrid').dataSource.data([]);
                        //$("#transferDestinationGrid").data('kendoGrid').dataSource.data([]);
                        //$("#transferSourceGrid").data('kendoGrid').dataSource.data([]);
                        $("#totalSourceTransferCost").val(0.0);
                        $("#totalDestinationTransferCost").val(0.0);

                        stockJournalAction = "Manufacturing";
                        renderFinishedGoodsItems();
                        renderRawMaterialForFinishedGoods();
                        getAutoVoucherNo();
                        getAutoVoucherNoForTransfer();
                        Utilities.SuccessNotification("Stock journal saved!");  //clear controls
                        window.location = "/Inventory/StockJournal/Index";
                    }
                    else {
                        Utilities.ErrorNotification("Could not save journal!");
                    }
                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.ErrorNotification("Sorry something went wrong!");
                }
            });
        }
    }

function findProductByProductCode(productCode) {
    var output = "N/A";
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productCode == productCode) {
            output = allProducts[i].productCode;
        }
    }
    return output;
}

function findProductByBarCode(barcode) {
    var output = "N/A";
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productCode == barcode) {
            output = allProducts[i].productCode;
        }
    }
    return output;
}

function findProductIdByProductCode(productCode) {
    var output = "N/A";
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productCode == productCode) {
            output = allProducts[i].productId;
        }
    }
    return output;
}

function findProductByProductName(productName) {
    var output = "N/A";
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productName == productName) {
            output = allProducts[i].productName;
        }
    }
    return output;
}

function findProductByProductId(productId) {
    var output = "N/A";
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productId == productId) {
            output = allProducts[i];
        }
    }
    return output;
}

function showStore(storeId) {
    var output = "N/A";
    for (i = 0; i < allStores.length; i++) {
        if (allStores[i].godownId == storeId) {
            output = allStores[i].godownName;
        }
    }
    return output;
}

function showRack(rackId) {
    var output = "N/A";
    for (i = 0; i < allRacks.length; i++) {
        if (allRacks[i].rackId == rackId) {
            output = allRacks[i].rackName;
        }
    }
    return output;
}

function getAutoVoucherNo() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetAutoVoucherNo",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            $("#formNo").val(data);
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}
function getAutoVoucherNoForTransfer() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetFormNoForTransfer",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            $("#formNoTransfer").val(data);
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

//========MAKER-CHECKER PART===========
function savePending() {
    var src = $("#transferSourceGrid").data().kendoGrid.dataSource.view();
    stockJournalDetailsInfoConsumption = [];
    $.each(src, function (count, row) {
        stockJournalDetailsInfoConsumption.push({
            ProductId: row.productId,
            Qty: row.qty,
            Rate: row.rate,
            UnitId: "",
            UnitConversionId: "" /*row.unitconversionId*/,
            BatchId: "",
            GodownId: row.godownId,
            RackId: row.rackId,
            Amount: (row.qty * row.rate),
            Slno: (count + 1)
        });
    });

    var destination = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
    stockJournalDetailsInfoProduction = [];
    $.each(destination, function (count, row) {
        stockJournalDetailsInfoProduction.push({
            ProductId: row.productId,
            Qty: row.qty,
            Rate: row.rate,
            UnitId: "",
            UnitConversionId: "",
            BatchId: "",
            GodownId: row.godownId,
            RackId: row.rackId,
            Amount: (row.rate * row.qty),
            Slno: (count + 1)
        });
    });

    stockJournalMasterInfo = {
        Narration: $("#narration").val(),
        Extra1: stockJournalAction,
        Extra2: matUserInfo.UserId,
        InvoiceNo: $("#formNoTransfer").val()
    };

    additionalCostItems = [];
    var kendoAdditionalCostSource = $("#additionalCostGrid").data().kendoGrid.dataSource.view();
    $.each(kendoAdditionalCostSource, function (count, row) {
        additionalCostItems.push({
            LedgerId: row.ledgerId,
            Amount: row.amount
        });
    });
    var toSave = {
        StockJournalMasterInfo: stockJournalMasterInfo,
        StockJournalDetailsInfoConsumption: stockJournalDetailsInfoConsumption,
        StockJournalDetailsInfoProduction: stockJournalDetailsInfoProduction,
        //LedgerPostingInfo: ledgerPostingInfo,
        AdditionalCostInfo: additionalCostInfo,
        TotalAdditionalCost: totalAdditionalCost,
        Date: $("#transactionDate").val(),
        Currency: $("#currency").val(),
        AdditionalCostCashOrBankId: $("#bankOrCash").val(),
        AdditionalCostItems: additionalCostItems
    };
    console.log(toSave);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/SavePending",
        type: "Post",
        contentType: "application/json",
        data: JSON.stringify(toSave),
        success: function (data) {
            if (data == true) {
                stockJournalDetailsInfoConsumption = [];
                stockJournalDetailsInfoProduction = [];

                additionalCostItems = [];
                stockJournalMasterInfo = {};
                additionalCostInfo = {};
                totalAdditionalCost = 0.0;
                $('#manufacturing').prop("checked", "checked");
                $("#quantityToManufacture").val(0);
                $("#narration").val("");
                $("#additionalCostTitle").html("Additional Cost / Narration");
                $("#stockAdjustmentGrid").data('kendoGrid').dataSource.data([]);
                //$("#transferDestinationGrid").data('kendoGrid').dataSource.data([]);
                //$("#transferSourceGrid").data('kendoGrid').dataSource.data([]);
                $("#totalSourceTransferCost").val(0.0);
                $("#totalDestinationTransferCost").val(0.0);

                stockJournalAction = "Manufacturing";
                renderFinishedGoodsItems();
                renderRawMaterialForFinishedGoods();
                getAutoVoucherNo();
                getAutoVoucherNoForTransfer();
                Utilities.SuccessNotification("Stock journal saved!");  //clear controls
                window.location = "/Inventory/StockJournal/Index";
            }
            else {
                Utilities.ErrorNotification("Could not save journal!");
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry something went wrong!");
        }
    });
}
function getPendingStockJournal(fromDate, toDate, status) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetPendingStockJournals?fromDate=" + fromDate + "&toDate=" + toDate,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(allUsers);
            pendingJournals = data.Table;
            var objToShow = [];

            $.each(pendingJournals, function (count, row) {
                var statusOutput = "";
                if (row.status == "Pending") {
                    statusOutput = '<span class="label label-warning"> Pending</span>';
                }
                else if (row.status == "In-Transit") {
                    statusOutput = '<span class="label label-primary"> In-Transit</span>';
                }
                else if (row.status == "Delivered") {
                    statusOutput = '<span class="label label-success"> Delivered</span>';
                }
                else if (row.status == "Cancelled") {
                    statusOutput = '<span class="label label-danger"> Cancelled</span>';
                }
                objToShow.push([
                    count + 1,
                    Utilities.FormatJsonDate(row.date),
                    row.narration == "" ? "NA" : row.narration,
                    ((findUsers(row.userId).userName) == "" ? "NA" : (findUsers(row.userId).userName)),
                    statusOutput,
                    '<button type="button" class="btn btn-primary btn-sm" onclick="getPendingStockJournalDetails(' + row.stockJournalMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                ]);
            });
            table = $('#stockJournalListTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

var pj = {};
var prod = [];
var cons = [];
var addCost = [];
function getPendingStockJournalDetails(id) {
    Utilities.Loader.Show();
    for (i = 0; i < pendingJournals.length; i++) {
        if (pendingJournals[i].stockJournalMasterId == id) {
            pj = pendingJournals[i];
        }
    }
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetPendingStockJournalDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            prod = data.production;
            cons = data.consumption;
            addCost = data.addCost;
            ledgers = data.ledger;
            console.log(pj);
            var statusOutput = "";
            if (pj.status == "Pending") {
                $("#ApproveTransfer").show();
                $("#cancelOrder").show();
                statusOutput = '<span class="label label-warning"> Pending</span>';
            }
            else if (pj.status == "In-Transit") {
                $("#ApproveTransfer").hide();
                $("#cancelOrder").hide();
                statusOutput = '<span class="label label-primary"> In-Transit</span>';
            }
            else if (pj.status == "Delivered") {
                $("#ApproveTransfer").hide();
                $("#cancelOrder").hide();
                statusOutput = '<span class="label label-success"> Delivered</span>';
            }
            else if (pj.status == "Cancelled") {
                $("#ApproveTransfer").hide();
                $("#cancelOrder").hide();
                statusOutput = '<span class="label label-danger"> Cancelled</span>';
            }
            $("#narrationDiv").html(pj.narration);
            $("#dateDiv").html(Utilities.FormatJsonDate(pj.date));
            $("#statusDiv").html(statusOutput);
            $("#transferredByDiv").html(findUsers(pj.userId).userName);

            renderTransferLineItems();

            //$("#detailsTbodyAdditionalCost").html(outputAddCost);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function renderTransferLineItems() {
    var outputCons = "";
    var outputProd = "";
    var outputAddCost = "";
    $.each(cons, function (count, row) {
        totalamount = totalamount + row.amount;
        if (pj.status == "Pending") {
            outputCons += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                            <td><button class="btn btn-sm btn-info" onclick="changeQuantity(' + row.ProductId + ',' + count + ')"><i class="fa fa-edit"></i></button></td>\
                        </tr>';
        }
        else {
            outputCons += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                        </tr>';
        }

    });
    $.each(prod, function (count, row) {
        //var lineItemStatus = "";
        //if (row.status == "In-Transit") {
        //    lineItemStatus = '<span class="label label-primary"> In-Transit </span>';
        //}
        //else if (row.status == "Delivered") {
        //    lineItemStatus = '<span class="label label-success"> Delivered </span>';
        //    buttonStatus = '';
        //}
        //else if (row.status == "Pending") {
        //    lineItemStatus = '<span class="label label-warning"> Pending Approval </span>';
        //}
        outputProd += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                        </tr>';
    });
    //$.each(addCost, function (count, row) {
    //    outputAddCost += '<tr>\
    //                <td>'+ (count + 1) + '</td>\
    //                <td>' + findLedger(row.ledgerId).ledgerName + '</td>\
    //                <td>&#8358;' + Utilities.FormatCurrency(row.debit) + '</td>\
    //                <td>&#8358;' + Utilities.FormatCurrency(row.credit) + '</td>\
    //            </tr>';
    //});

    $("#AmountTxt").val(Utilities.FormatCurrency(totalamount));
    $("#addCostTxt").val(Utilities.FormatCurrency(pj.additionalCost));
    $("#grandTotalTxt").val(Utilities.FormatCurrency(totalamount + pj.additionalCost));

    $("#availableQuantityDiv").html("");
    $("#detailsTbodyConsumption").html(outputCons);
    $("#detailsTbodyProduction").html(outputProd);
}
function changeQuantity(id, count) {
    Utilities.Loader.Show();
    var quantity = prompt("Please enter new Quantity:", "Enter New Quantity");
    if (quantity == null || quantity == "") {
        Utilities.ErrorNotification("Quantity cannot be Empty");
    } else {
        cons[count].qty = quantity;
        cons[count].amount = cons[count].rate * quantity;

        prod[count].qty = quantity;
        prod[count].amount = prod[count].rate * quantity;
    }
    $.ajax({
        url: API_BASE_URL + "/StockJournal/updateQuantity?idprod=" + prod[count].stockJournalDetailsId + "&idcons=" + cons[count].stockJournalDetailsId + "&quantity=" + quantity + "&amount=" + cons[count].amount,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            if (data == true) {
                renderTransferLineItems();
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function saveStockJournalforTransfer()
{

    stockJournalDetailsInfoConsumption = [];
    $.each(cons, function (count, row) {
        stockJournalDetailsInfoConsumption.push({
            ProductId: row.productId,
            Qty: row.qty,
            Rate: row.rate,
            UnitId: "",
            UnitConversionId: "" /*row.unitconversionId*/,
            BatchId: "",
            GodownId: row.godownId,
            RackId: row.rackId,
            Amount: (row.qty * row.rate),
            Slno: (count + 1)
        });
    });

    stockJournalDetailsInfoProduction = [];
    $.each(prod, function (count, row) {
        stockJournalDetailsInfoProduction.push({
            ProductId: row.productId,
            Qty: row.qty,
            Rate: row.rate,
            UnitId: "",
            UnitConversionId: "",
            BatchId: "",
            GodownId: row.godownId,
            RackId: row.rackId,
            Amount: (row.rate * row.qty),
            Slno: (count + 1)
        });
    });

    stockJournalMasterInfo = {
        Narration: pj.narration,
        Extra1: "Stock Transfer",
    };

    additionalCostItems = [];
    $.each(addCost, function (count, row) {
        additionalCostItems.push({
            LedgerId: row.ledgerId,
            Amount: row.debit
        });
    });
    var toSave = {
        StockJournalMasterInfo: stockJournalMasterInfo,
        StockJournalDetailsInfoConsumption: stockJournalDetailsInfoConsumption,
        StockJournalDetailsInfoProduction: stockJournalDetailsInfoProduction,
        //LedgerPostingInfo: ledgerPostingInfo,
        AdditionalCostInfo: additionalCostInfo,
        TotalAdditionalCost: pj.additionalCost,
        Date: pj.date,
        pendingId: pj.stockJournalMasterId,
        Currency: pj.exchangeRateId,
        AdditionalCostCashOrBankId: pj.ledgerId,
        AdditionalCostItems: additionalCostItems
    };
    console.log(toSave);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/saveStockJournalInTransit",
        type: "Post",
        contentType: "application/json",
        data: JSON.stringify(toSave),
        success: function (data) {
            $.each(prod, function (count, row) {
                row.ProductName = findProduct(row.productId).productName;
                row.Barcode = findProduct(row.productId).productCode;
                row.ProductCode = findProduct(row.productId).productCode;
                row.Store = findStore(row.godownId).godownName;
                row.Amount = Utilities.FormatCurrency(row.amount);
                row.Rate = Utilities.FormatCurrency(row.rate);
            });
            $.each(cons, function (count, row) {
                row.ProductName = findProduct(row.productId).productName;
                row.Barcode = findProduct(row.productId).productCode;
                row.ProductCode = findProduct(row.productId).productCode;
                row.Store = findStore(row.godownId).godownName;
                row.Amount = Utilities.FormatCurrency(row.amount);
                row.Rate = Utilities.FormatCurrency(row.rate);
            });
            if (data == true) {
                var columns = [
                    { title: "SL", dataKey: "slno" },
                    { title: "Barcode", dataKey: "Barcode" },
                    { title: "Product Code", dataKey: "ProductCode" },
                    { title: "Product Name", dataKey: "ProductName" },
                    { title: "Warehouse", dataKey: "Store" },
                    { title: "Quanity", dataKey: "qty" },
                ];
                var columns1 = [
                    { title: "SL", dataKey: "slno" },
                    { title: "Barcode", dataKey: "Barcode" },
                    { title: "Product Code", dataKey: "ProductCode" },
                    { title: "Product Name", dataKey: "ProductName" },
                    { title: "Warehouse", dataKey: "Store" },
                    { title: "Quanity", dataKey: "qty" },
                ];

                var doc = new jsPDF('landscape');
                doc.setFontSize(30);
                doc.text('WICHTECH GROUP', 15, 30);
                doc.setFontSize(8);
                doc.text('5, Wema Terrace, Osborne Estate, 2-9 Udi St, Ikoyi, Lagos, Nigeria', 15, 35);
                doc.text('Narration: ' + pj.narration + '\n\nTransfer Date: ' + Utilities.FormatJsonDate(pj.date) +
                   '\n\nTransfered By: ' + findUsers(pj.userId).userName, 240, 20);
                doc.text('Transfer(s) From', 15, 45);
                doc.autoTable(columns, cons,
                {
                    startY: doc.autoTableEndPosY() + 48, theme: 'grid', styles: {
                        fontSize: 8
                    }
                });
                doc.text('Transfer(s) To', 14, doc.autoTable.previous.finalY + 10);
                doc.autoTable(columns1, prod,
                {
                    startY: doc.autoTable.previous.finalY + 14, theme: 'grid', styles: {
                        fontSize: 8
                    }
                });
                doc.text('____________________\n\n Signature & Date\n\n Confirmed By: ' + findUsers(matUserInfo.UserId).userName, 14, doc.autoTable.previous.finalY + 20);
                doc.save('StockTransfer' + Utilities.FormatJsonDate(pj.date) + '.pdf');

                stockJournalDetailsInfoConsumption = [];
                stockJournalDetailsInfoProduction = [];

                additionalCostItems = [];
                stockJournalMasterInfo = {};
                additionalCostInfo = {};
                totalAdditionalCost = 0.0;

                Utilities.SuccessNotification("Stock Transfer Successful");  //clear controls
                window.location = "/Inventory/StockJournal/stockJournalConfirmation";
            }
            else {
                Utilities.ErrorNotification("Could not save journal!");
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry something went wrong!");
        }
    });
}

function findLedger(ledgerId) {
    var output = "N/A";
    for (i = 0; i < ledgers.length; i++) {
        if (ledgers[i].ledgerId == ledgerId) {
            output = ledgers[i];
        }
    }
    return output;
}