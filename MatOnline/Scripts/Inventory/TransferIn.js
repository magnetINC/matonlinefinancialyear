﻿var allUsers = [];
var allProducts = [];
var allStores = [];
var intransit = [];
var prod = [];
var cons = [];
var intransitSingle = {};

var currentLineItem = 0;

var stockJournalMasterInfo = {};
var stockJournalDetailsInfoConsumption = [];
var stockJournalDetailsInfoProduction = [];
//var ledgerPostingInfo = [];
var additionalCostInfo = {};
var additionalCostItems = [];
var stockAdjustment = [];

$(function () {
    getLookup();
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
});

function getLookup() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetLookups",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            allUsers = data.users;
            allProducts = data.AllProducts;
            allStores = data.AllStores;

            Utilities.Loader.Hide();

        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}
function GetTransferInTransit(id, fromDate, toDate, status) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetTransferInTransit?id=" + id,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            intransit = data;
            console.log(data);
            pendingJournals = data.Table;
            var objToShow = [];

            $.each(data, function (count, row) {
                var statusOutput = "";
                if (row.status == "In-Transit")
                {
                    if (row.status == "Pending") {
                        statusOutput = '<span class="label label-warning"> Pending</span>';
                    }
                    else if (row.status == "In-Transit") {
                        statusOutput = '<span class="label label-primary"> In-Transit</span>';
                    }
                    else if (row.status == "Delivered") {
                        statusOutput = '<span class="label label-success"> Delivered</span>';
                    }
                    else if (row.status == "Cancelled") {
                        statusOutput = '<span class="label label-danger"> Cancelled</span>';
                    }
                    objToShow.push([
                        count + 1,
                        Utilities.FormatJsonDate(row.date),
                        row.narration == "" ? "NA" : row.narration,
                        ((findUsers(row.userId).userName) == "" ? "NA" : (findUsers(row.userId).userName)),
                        statusOutput,
                        '<button type="button" class="btn btn-primary btn-sm" onclick="getTransferInDetails(' + row.id + ')"><i class="fa fa-eye"></i> View</button>'
                    ]);
                }
                //else {
                //    if (row.date >= fromDate && row.date <= toDate && row.status == status) {
                //        if (row.status == "Pending") {
                //            statusOutput = '<span class="label label-warning"> Pending</span>';
                //        }
                //        else if (row.status == "In-Transit") {
                //            statusOutput = '<span class="label label-primary"> In-Transit</span>';
                //        }
                //        else if (row.status == "Delivered") {
                //            statusOutput = '<span class="label label-success"> Delivered</span>';
                //        }
                //        else if (row.status == "Cancelled") {
                //            statusOutput = '<span class="label label-danger"> Cancelled</span>';
                //        }
                //        objToShow.push([
                //            count + 1,
                //            Utilities.FormatJsonDate(row.date),
                //            row.narration == "" ? "NA" : row.narration,
                //            ((findUsers(row.userId).userName) == "" ? "NA" : (findUsers(row.userId).userName)),
                //            statusOutput,
                //            '<button type="button" class="btn btn-primary btn-sm" onclick="getTransferInDetails(' + row.id + ')"><i class="fa fa-eye"></i> View</button>'
                //        ]);
                //    }
                //}   
            });
            table = $('#transferInListTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function getTransferInDetails(id)
{
    currentLineItem = id;
    Utilities.Loader.Show();
    for (i = 0; i < intransit.length; i++) {
        if (intransit[i].id == id) {
            intransitSingle = intransit[i];
        }
    }
    console.log(intransitSingle);
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetInTransitDetails?transferId=" + id + "&userId=" + matUserInfo.UserId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            prod = data.production;
            cons = data.consumption;
            var amount = 0;
            console.log(intransitSingle);
            $("#narrationDiv").html(intransitSingle.narration);
            $("#dateDiv").html(Utilities.FormatJsonDate(intransitSingle.date));
            $("#transferredByDiv").html(findUsers(intransitSingle.userId).userName);

            var outputCons = "";
            var outputProd = "";
            var outputAddCost = "";
            $.each(cons, function (count, row) {
                amount = amount + row.amount;
                outputCons += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                        </tr>';
            });
            $.each(prod, function (count, row) {
                var lineItemStatus = "";
                var buttonStatus = "";
                if (row.status == "Delivered") {
                    lineItemStatus = '<span class="label label-success"> Delivered </span>';
                    buttonStatus = '';
                    $("#confirmDelivery").hide();
                }
                else if (row.status == "In-Transit") {
                    lineItemStatus = '<span class="label label-primary"> In-Transit </span>';
                   // buttonStatus = '<button type="button" class="btn btn-success btn-sm" onclick="transferOnDelivery(' + row.stockJournalDetailsId + ')"> Confirm Delivery</button>';
                }
                //else if (row.status == "Pending") {
                //    lineItemStatus = '<span class="label label-warning"> Pending Approval </span>';
                //}
                outputProd += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productCode + '</td>\
                            <td>' + findProductByProductId(row.productId).productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                        </tr>';
            });

            //$("#AmountTxt").val(Utilities.FormatCurrency(amount));
            //$("#addCostTxt").val(Utilities.FormatCurrency(pj.additionalCost));
            //$("#grandTotalTxt").val(Utilities.FormatCurrency(amount + pj.additionalCost));

            $("#availableQuantityDiv").html("");
            $("#detailsTbodyConsumption").html(outputCons);
            $("#detailsTbodyProduction").html(outputProd);
            //$("#detailsTbodyAdditionalCost").html(outputAddCost);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function transferOnDelivery() {
    var output = {};
    var stockJournalDetailsInfoProduction = [];

    $.each(prod, function (count, row) {
        stockJournalDetailsInfoProduction.push({
            ProductId: row.productId,
            Qty: row.qty,
            Rate: row.rate,
            UnitId: "",
            UnitConversionId: "",
            BatchId: "",
            GodownId: row.godownId,
            RackId: row.rackId,
            Amount: (row.rate * row.qty),
            Slno: (count + 1)
        });
    });
    //$.each(prod, function (count, row) {
    //    stockJournalDetailsInfoProduction.push({
    //        StockJournalMasterId: row.decStockJournalId,
    //        ProductId: row.productId,
    //        Qty: row.qty,
    //        Rate: row.rate,
    //        UnitId: "",
    //        UnitConversionId: "",
    //        BatchId: "",
    //        GodownId: row.godownId,
    //        RackId: row.rackId,
    //        Amount: (row.rate * row.qty),
    //        Slno: (count + 1)
    //    });
    //});
    var toSave = {
        //StockJournalMasterInfo: stockJournalMasterInfo,
        //StockJournalDetailsInfoConsumption: stockJournalDetailsInfoConsumption,
        StockJournalDetailsInfoProduction: stockJournalDetailsInfoProduction,
        //LedgerPostingInfo: ledgerPostingInfo,
        //AdditionalCostInfo: additionalCostInfo,
        //TotalAdditionalCost: pj.additionalCost,
        Date: intransitSingle.date,
        pendingId: prod[0].decStockJournalId,
        AdditionalCostCashOrBankId: currentLineItem // using this to pass the stockjournalmasterpendingId to the api since its not been used
        //Currency: pj.exchangeRateId,
        //AdditionalCostCashOrBankId: pj.ledgerId,
       // AdditionalCostItems: additionalCostItems
    };
    console.log(toSave);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/saveStockTransferOnDelivery",
        type: "Post",
        contentType: "application/json",
        data: JSON.stringify(toSave),
        success: function (data) {
            if (data == true) {
                Utilities.SuccessNotification("Transfer Delivered Successfully");
                //getTransferInDetails(currentLineItem);
                window.location = "/Inventory/StockJournal/TransferIn";
            }
            else {
                Utilities.ErrorNotification("Could not save journal!");
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry something went wrong!");
        }
    });
}

function findUsers(userId) {
    var output = {};;
    for (i = 0; i < allUsers.length; i++) {
        if (allUsers[i].userId == userId) {
            output = allUsers[i];
            break;
        }
    }
    return output;
}
function findProductByProductId(productId) {
    var output = "N/A";
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].productId == productId) {
            output = allProducts[i];
        }
    }
    return output;
}
function findStore(storeId) {
    var output = {};
    for (i = 0; i < allStores.length; i++) {
        if (allStores[i].godownId == storeId) {
            output = allStores[i];
            break;
        }
    }
    return output;
}
