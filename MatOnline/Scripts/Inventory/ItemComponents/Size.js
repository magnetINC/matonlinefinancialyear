﻿var sizes = [];
var sizeToEdit = "";
var sizeIdToDelete = 0;
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created

$(function () {

    getSizes();

    $("#sizeTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        sizeToEdit = findSize(id);
        $("#editSizeId").val(sizeToEdit[0]);
        $("#editSize").val(sizeToEdit[1]);
        //$("#editNarration").val(brandToEdit[2]);
        //$("#editManufacturer").val(brandToEdit[3]);
        //$("#editExtra1").val(brandToEdit[4]);
        //$("#editExtra2").val(brandToEdit[5]);
        $('#editModal').modal('toggle');

    });

    $("#sizeTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        sizeToEdit = findSize(id);
        sizeIdToDelete = id;
        $("#deleteSize").html(sizeToEdit[1]);
        $('#deleteModal').modal('toggle');

    });


    $("#save").click(function () {
        if ($("#size").val() != "")
        {
            $("#errorMsg").html("");

            var sizeToSave = {
                Size: $("#size").val(),
                //Narration: $("#narration").val(),
                //Extra1: $("#extra1").val(),
                //Extra2: $("#extra2").val(),
                //Manufacturer: $("#manufacturer").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Size/AddSize",
                type: 'POST',
                data: JSON.stringify(sizeToSave),
                contentType: "application/json",
                success: function (data) {
                    if (data == 2) {
                        hideLoader();
                        Utilities.ErrorNotification("Size Already Exists");
                    }
                    else if (data == 1) {
                        table.destroy();
                        getSizes();
                        $("#size").val("");
                        $("#addModal").modal("hide");
                        Utilities.SuccessNotification("Size Saved Successfully");
                    }
                    else {
                        Utilities.ErrorNotification("Oops...Unable to Save Size");
                    }
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else
        {
            $("#errorMsg").html("Size name is required!");
        }
        
    });

    $("#delete").click(function () {
        var sizeToDelete = {
            SizeId: sizeIdToDelete
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/Size/DeleteSize",
            type: 'POST',
            data:JSON.stringify(sizeToDelete),
            contentType: "application/json",
            success: function (data) {
                //hideLoader();
                table.destroy();
                getSizes(); 
                $('#deleteModal').modal('toggle');
                Utilities.SuccessNotification("Delete Successfully");
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    });

    $("#saveChanges").click(function () {
        if ($("#editSize").val() != "") {
            $("#editErrorMsg").html("");

            var sizeToEdit = {
                SizeId: $("#editSizeId").val(),
                Size: $("#editSize").val(),
                //Narration: $("#editNarration").val(),
                //Extra1: $("#editExtra1").val(),
                //Extra2: $("#editExtra2").val(),
                //Manufacturer: $("#editManufacturer").val(),
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Size/EditSize",
                type: 'POST',
                data: JSON.stringify(sizeToEdit),
                contentType: "application/json",
                success: function (data) {
                    if(data > 0)
                    {
                        table.destroy();
                        getSizes();
                        $("#size").val("");
                        $("#editModal").modal("hide");
                        Utilities.SuccessNotification("Size Updated Successfully");
                    }
                    else {
                        Utilities.ErrorNotification("Oops!!...Size unable to update");
                    }
                    
                },
                error: function (request, error) {
                    hideLoader();
                    Utilities.ErrorNotification("Oops!!...Something went wrong");
                }
            });
        }
        else {
            $("#editErrorMsg").html("Brand name is required!");
        }

    });
});

function getSizes() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/Size/GetSizes",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function onSuccess(data) {
    var objToShow = [];
    $.each(data, function (i, record) {

        objToShow.push([
            (i + 1),
            record.Size,
            '<button type="button" class="btnEditModal btn btn-primary text-left" id="' + record.SizeId + '"><i class="fa fa-edit"></i> Edit</button>',
            '<button type="button" class="btnDeleteModal btn btn-danger text-left" id="' + record.SizeId + '"><i class="fa fa-times-circle"></i> Delete</button>'
        ]);
        sizes.push([
            record.SizeId,
            record.Size
        ]);
    });
    var count = 0;
    if (checkPriviledge("frmSize", "Update") === false) {
        objToShow.forEach(function (item) {
            item.splice(2, 1);
        });
        count++;
    }

    if (checkPriviledge("frmSize", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 3 - count;
            item.splice(recalculateIndex, 1);
        });
    }
    table = $('#sizeTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    hideLoader();
}

function findSize(id) {
    var size = "";
    for (i = 0; i < sizes.length; i++) {
        if (sizes[i][0] == id) {
            size = sizes[i];
            break;
        }
    }
    return size;
}