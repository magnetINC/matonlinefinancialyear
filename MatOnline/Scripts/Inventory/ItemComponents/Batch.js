﻿var products = [];
var batches = [];
var batchToEdit = "";
var batchIdToDelete = 0;
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created

$(function () {

    getProducts();

    //$('#manufacturingDate').datepicker({
    //    autoclose: true
    //});

    //$('#expiryDate').datepicker({
    //    autoclose: true
    //});
    //$(".select2").select2();

   

    $('#addModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });

    $("#batchTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        batchToEdit = findBatch(id);
        var productId = products.find(p => p.ProductId == batchToEdit[2]).ProductId;
        $("#editBatchId").val(batchToEdit[0]);
        $("#editBatchName").val(batchToEdit[1]);
        $("#editNarration").val(batchToEdit[5]);
        $("#editManufacturingDate").val(batchToEdit[3].substring(0,10));
        $("#editExpiryDate").val(batchToEdit[4].substring(0, 10));
        $("#products").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 1, 3));
        $("#products").chosen({ width: "100%", margin: "1px" });
        $("#products").val(productId);
        $("#products").trigger("chosen:updated");

        $('#editModal').modal('toggle');

    });

    $("#batchTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        batchToEdit = findBatch(id);
        batchIdToDelete = id;
        $("#deletebatchName").html(batchToEdit[1]);
        $('#deleteModal').modal('toggle');

    });


    $("#save").click(function () {
        if ($("#batchName").val() != "")
        {
            $("#errorMsg").html("");

            var batchToSave = {
                BatchNo: $("#batchName").val(),
                ProductId: $("#productId").val(),
                ManufacturingDate: $("#manufacturingDate").val(),
                ExpiryDate: $("#expiryDate").val(),
                narration: $("#editNarration").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Batch/AddBatch",
                type: 'POST',
                data: JSON.stringify(batchToSave),
                contentType: "application/json",
                success: function (data) {
                    //hideLoader();
                    table.destroy();
                    $("#batchName").val("");
                    $("#expiryDate").val(Utilities.YyMmDdDate());
                    $("#manufacturingDate").val(Utilities.YyMmDdDate());
                    $("#narration").val("");
                    $("#productId").val("");
                    $("#productId").trigger("chosen:updated");
                    getBatches();
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else
        {
            $("#errorMsg").html("batch name is required!");
        }
        
    });

    $("#delete").click(function () {
        var batchToDelete = {
            batchId: batchIdToDelete
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/batch/Deletebatch",
            type: 'POST',
            data:JSON.stringify(batchToDelete),
            contentType: "application/json",
            success: function (data) {
                //hideLoader();
                table.destroy();
                getBatches();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    });

    $("#saveChanges").click(function () {
        if ($("#editBatchName").val() != "") {
            $("#editErrorMsg").html("");

            var batchToEdit = {
                BatchId: $("#editBatchId").val(),
                BatchNo: $("#editBatchName").val(),
                ProductId: $("#products").val(),
                narration: $("#editNarration").val(),
                ExpiryDate: $("#editExpiryDate").val(),
                ManufacturingDate: $("#editManufacturingDate").val(),
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Batch/EditBatch",
                type: 'POST',
                data: JSON.stringify(batchToEdit),
                contentType: "application/json",
                success: function (data) {
                    //hideLoader();
                    table.destroy(); 
                    $("#editBatchName").val("");
                    $("#editManufacturingDate").val(Utilities.YyMmDdDate());
                    $("#editExpiryDate").val(Utilities.YyMmDdDate()); 
                    $("#editNarration").val(""); 
                    $("#products").val("");
                    $("#products").trigger("chosen:updated");
                    
                    //$("#batchTable tbody").html("");
                    //$('#batchTable').DataTable().destroy();
                    getBatches();
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else {
            $("#editErrorMsg").html("Batch name is required!");
        }

    });
});

function getBatches() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/Batch/GetBatches",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function onSuccess(data) {
    var objToShow = [];
    console.log(data); //return;
    batches = [];
    $.each(data, function (i, record) {
        var temp = findProduct(record.ProductId).ProductName;
        var prod = "" + temp + ""; //variable is quoted in string cz of an error with string type in the datatable.js lib
        objToShow.push([
            (i + 1),
            record.BatchNo,
            prod,
            Utilities.FormatJsonDate(record.ManufacturingDate),
            Utilities.FormatJsonDate(record.ExpiryDate),
            '<button type="button" class="btnEditModal btn btn-primary text-left" id="' + record.BatchId + '"><i class="fa fa-edit"></i> Edit</button>',
            '<button type="button" class="btnDeleteModal btn btn-danger text-left" id="' + record.BatchId + '"><i class="fa fa-times-circle"></i> Delete</button>'
        ]);
        batches.push([
            record.BatchId,
            record.BatchNo,
            record.ProductId,
            record.ManufacturingDate,
            record.ExpiryDate,
            record.narration
        ]);
    });
    var count = 0;
    if (checkPriviledge("frmProductBatch", "Update") === false) {
        objToShow.forEach(function (item) {
            item.splice(5, 1);
        });
        count++;
    }

    if (checkPriviledge("frmProductBatch", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 6 - count;
            item.splice(recalculateIndex, 1);
        });
    }
    table = $('#batchTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    hideLoader();
}

function getProducts() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            products = data;
            console.log(products);
            var productsHtml = "";
            $.each(products, function (i, record) {
                productsHtml += '<option value="'+record.ProductId+'">'+record.ProductName+'</option>';
            });
            //$("#productId").html(productsHtml);
            $("#productId").kendoDropDownList({
                filter: "contains",
                dataTextField: "ProductName",
                dataValueField: "ProductId",
                dataSource: products
            });
            //hideLoader();
            getBatches();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function findBatch(id) {
    var batch = "";
    for (i = 0; i < batches.length; i++) {
        if (batches[i][0] == id) {
            batch = batches[i];
            break;
        }
    }
    return batch;
}

function findProduct(id) {
    var product = "";
    for (i = 0; i < products.length; i++) {
        if (products[i].ProductId == id) {
            product = products[i];
            break;
        }
    }
    return product;
}