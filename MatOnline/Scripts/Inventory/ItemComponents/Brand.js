﻿var brands = [];
var brandToEdit = "";
var brandIdToDelete = 0;
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created

$(function () {

    getBrands();

    $("#brandTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        brandToEdit = findBrand(id);
        $("#editBrandId").val(brandToEdit[0]);
        $("#editBrandName").val(brandToEdit[1]);
        $("#editNarration").val(brandToEdit[2]);
        $("#editManufacturer").val(brandToEdit[3]);
        //$("#editExtra1").val(brandToEdit[4]);
        //$("#editExtra2").val(brandToEdit[5]);
        $('#editModal').modal('toggle');

    });

    $("#brandTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        brandToEdit = findBrand(id);
        brandIdToDelete = id;
        $("#deleteBrandName").html(brandToEdit[1]);
        $('#deleteModal').modal('toggle');

    });


    $("#save").click(function () {
        if ($("#brandName").val() != "")
        {
            $("#errorMsg").html("");

            var brandToSave = {
                BrandName: $("#brandName").val(),
                Narration: $("#narration").val(),
                Extra1: "",
                Extra2: "",
                Manufacturer: $("#manufacturer").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Brand/AddBrand",
                type: 'POST',
                data: JSON.stringify(brandToSave),
                contentType: "application/json",
                success: function (data) {
                    if (data == 2) {
                        hideLoader();
                        Utilities.ErrorNotification("Colour Already Exists");
                    }
                    else if (data == 1) {
                        table.destroy();
                        getBrands();
                        $("#brandName").val("");
                        $("#narration").val("");
                        $("#manufacturer").val("");
                        $("#addModal").modal("hide");
                        Utilities.SuccessNotification("Colour Saved Successfully");
                    }
                    else {
                        Utilities.ErrorNotification("Oops...Unable to Save Colour");
                    }
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else
        {
            $("#errorMsg").html("Brand name is required!");
        }
        
    });

    $("#delete").click(function () {
        var brandToDelete = {
            BrandId: brandIdToDelete
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/Brand/DeleteBrand",
            type: 'POST',
            data:JSON.stringify(brandToDelete),
            contentType: "application/json",
            success: function (data) {
                //hideLoader();
                table.destroy();
                $("#deleteModal").modal("hide");
                getBrands();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    });

    $("#saveChanges").click(function () {
        if ($("#editBrandName").val() != "") {
            $("#editErrorMsg").html("");

            var brandToEdit = {
                BrandId: $("#editBrandId").val(),
                BrandName: $("#editBrandName").val(),
                Narration: $("#editNarration").val(),
                Extra1: "",
                Extra2: "",
                Manufacturer: $("#editManufacturer").val(),
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Brand/EditBrand",
                type: 'POST',
                data: JSON.stringify(brandToEdit),
                contentType: "application/json",
                success: function (data) {
                    if (data > 0) {
                        table.destroy();
                        getBrands();
                        $("#editBrandName").val("");
                        $("#editNarration").val("");
                        $("#editManufacturer").val("");
                        $("#editModal").modal("hide");
                        Utilities.SuccessNotification("Colour Updated Successfully");
                    }
                    else {
                        Utilities.ErrorNotification("Oops!!...Colour unable to update");
                    }
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else {
            $("#editErrorMsg").html("Brand name is required!");
        }

    });
});

function getBrands() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/Brand/GetBrands",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function onSuccess(data) {
    var objToShow = [];
    brands = [];
    $.each(data, function (i, record) {

        objToShow.push([
            (i + 1),
            record.BrandName,
            record.Narration,
            record.Manufacturer,
            '<button type="button" class="btnEditModal btn btn-primary text-left" id="' + record.BrandId + '"><i class="fa fa-edit"></i></button>',
            '<button type="button" class="btnDeleteModal btn btn-danger text-left" id="' + record.BrandId + '"><i class="fa fa-times-circle"></i></button>'
        ]);
        brands.push([
            record.BrandId,
            record.BrandName,
            record.Narration,
            record.Manufacturer,
            record.Extra1,
            record.Extra2,
        ]);
    });
    var count = 0;
    if (checkPriviledge("frmBrand", "Update") === false) {
        objToShow.forEach(function (item) {
            item.splice(4, 1);
        });
        count++;
    }

    if (checkPriviledge("frmBrand", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 5 - count;
            item.splice(recalculateIndex, 1);
        });
    }
    table = $('#brandTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    hideLoader();
}

function findBrand(id) {
    var brand = "";
    for (i = 0; i < brands.length; i++) {
        if (brands[i][0] == id) {
            brand = brands[i];
            break;
        }
    }
    return brand;
}