﻿var units = [];
var unitToEdit = "";
var unitIdToDelete = 0;
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created

$(function () {

    getUnits();

    $("#unitTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        unitToEdit = findUnit(id);
        $("#editUnitId").val(unitToEdit[0]);
        $("#editUnit").val(unitToEdit[1]);
        $("#editDecimalPlace").val(unitToEdit[2]);
        $('#editModal').modal('toggle');

    });

    $("#unitTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        unitToEdit = findUnit(id);
        unitIdToDelete = id;
        $("#deleteUnit").html(unitToEdit[1]);
        $('#deleteModal').modal('toggle');
    });


    $("#save").click(function () {
        if ($("#unit").val() != "")
        {
            $("#errorMsg").html("");

            var unitToSave = {
                UnitName: $("#unit").val(),
                noOfDecimalplaces: $("#decimalPlace").val(),
                //Extra1: $("#extra1").val(),
                //Extra2: $("#extra2").val(),
                //Manufacturer: $("#manufacturer").val()08069706038
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Unit/AddUnit",
                type: 'POST',
                data: JSON.stringify(unitToSave),
                contentType: "application/json",
                success: function (data) {
                    if (data == 2) {
                        hideLoader();
                        Utilities.ErrorNotification("Unit Already Exists");
                    }
                    else if (data == 1) {
                        table.destroy();
                        getUnits();
                        $("#unit").val("");
                        $("#addModal").modal("hide");
                        Utilities.SuccessNotification("Unit Saved Successfully");
                    }
                    else {
                        Utilities.ErrorNotification("Oops...Unable to Save Unit");
                    }
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else
        {
            $("#errorMsg").html("Unit name is required!");
        }
        
    });

    $("#delete").click(function () {
        var unitToDelete = {
            UnitId: unitIdToDelete
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/Unit/DeleteUnit",
            type: 'POST',
            data:JSON.stringify(unitToDelete),
            contentType: "application/json",
            success: function (data) {
                //hideLoader();
                table.destroy();
                getUnits(); 

                if (data)
                {
                    $('#deleteModal').modal('hide');
                    Utilities.SuccessNotification("Delete Successful");
                } else {
                    $('#deleteModal').modal('hide');
                    Utilities.ErrorNotification("Unable to perform Operation: Transaction Exist for this Unit ");
                }
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    });

    $("#saveChanges").click(function () {
        if ($("#editUnit").val() != "") {
            $("#editErrorMsg").html("");

            var unitToEdit = {
                UnitId: $("#editUnitId").val(),
                UnitName: $("#editUnit").val(),
                noOfDecimalplaces: $("#editDecimalPlace").val(),
                //Narration: $("#editNarration").val(),
                //Extra1: $("#editExtra1").val(),
                //Extra2: $("#editExtra2").val(),
                //Manufacturer: $("#editManufacturer").val(),
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Unit/EditUnit",
                type: 'POST',
                data: JSON.stringify(unitToEdit),
                contentType: "application/json",
                success: function (data) {
                    if (data > 0) {
                        table.destroy();
                        getUnits();
                        $("#editUnit").val("");
                        $("#editDecimalPlace").val("");
                        $("#editModal").modal("hide");
                        Utilities.SuccessNotification("Unit Updated Successfully");
                    }
                    else {
                        Utilities.ErrorNotification("Oops!!...Unit unable to update");
                    }
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else {
            $("#editErrorMsg").html("Unit name is required!");
        }

    });
});

function getUnits() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/Unit/GetUnits",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function onSuccess(data) {
    var objToShow = [];
    $.each(data, function (i, record) {

        objToShow.push([
            (i + 1),
            record.UnitName,
            //record.formalName,
            //record.noOfDecimalplaces,
            '<button type="button" class="btnEditModal btn btn-primary text-left" id="' + record.UnitId + '"><i class="fa fa-edit"></i> Edit</button>',
            '<button type="button" class="btnDeleteModal btn btn-danger text-left" id="' + record.UnitId + '"><i class="fa fa-times-circle"></i> Delete</button>'
        ]);
        units.push([
            record.UnitId,
            record.UnitName,
            //record.formalName,
            record.noOfDecimalplaces
        ]);
    });
    var count = 0;
    if (checkPriviledge("frmUnit", "Update") === false) {
        objToShow.forEach(function (item) {
            item.splice(2, 1);
        });
        count++;
    }

    if (checkPriviledge("frmUnit", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 3 - count;
            item.splice(recalculateIndex, 1);
        });
    }
    table = $('#unitTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    hideLoader();
}

function findUnit(id) {
    var unit = "";
    for (i = 0; i < units.length; i++) {
        if (units[i][0] == id) {
            console.log(units[i]);
            unit = units[i];
            break;
        }
    }
    return unit;
}