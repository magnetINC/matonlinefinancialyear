﻿var stores = [];
var storeToEdit = "";
var storeIdToDelete = 0;
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created

$(function () {

    getStores();

    $("#storeTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        storeToEdit = findStore(id);
        $("#editStoreId").val(storeToEdit[0]);
        $("#editStore").val(storeToEdit[1]);
        $('#editModal').modal('toggle');
    });

    $("#storeTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        storeToEdit = findStore(id);
        storeIdToDelete = id;
        $("#deleteStore").html(storeToEdit[1]);
        $('#deleteModal').modal('toggle');

    });


    $("#save").click(function () {
        if ($("#store").val() != "")
        {
            $("#errorMsg").html("");

            var storeToSave = {
                GodownName: $("#store").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Store/AddStore",
                type: 'POST',
                data: JSON.stringify(storeToSave),
                contentType: "application/json",
                success: function (data) {
                    //hideLoader();
                    table.destroy();
                    getStores();

                    if (data)
                    {
                        $('#addModal').modal('hide');
                        $('#store').val('');
                        Utilities.SuccessNotification("Add New Store Successful");
                    }
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else
        {
            $("#errorMsg").html("Store name is required!");
        }
        
    });

    $("#delete").click(function () {
        var storeToDelete = {
            GodownId: storeIdToDelete
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/Store/DeleteStore",
            type: 'POST',
            data:JSON.stringify(storeToDelete),
            contentType: "application/json",
            success: function (data) {
                //hideLoader();
                table.destroy();
                getStores();

                if (data) {
                    $('#deleteModal').modal('hide');
                    Utilities.SuccessNotification("Delete Successful");
                } else {
                    $('#deleteModal').modal('hide');
                    Utilities.ErrorNotification("Oops!!! Unable to delete Store: Transaction Exist");
                }
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    });

    $("#saveChanges").click(function () {
        if ($("#editStore").val() != "") {
            $("#editErrorMsg").html("");

            var storeToEdit = {
                GodownId: $("#editStoreId").val(),
                GodownName: $("#editStore").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Store/EditStore",
                type: 'POST',
                data: JSON.stringify(storeToEdit),
                contentType: "application/json",
                success: function (data) {
                   // hideLoader();
                    table.destroy();
                    getStores();
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else {
            $("#editErrorMsg").html("Store name is required!");
        }

    });
});

function getStores() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/Store/GetStores",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function onSuccess(data) {
    var objToShow = [];
    $.each(data, function (i, record) {

        objToShow.push([
            (i + 1),
            record.GodownName,
            '<button type="button" class="btnEditModal btn btn-primary text-left" id="' + record.GodownId + '"><i class="fa fa-edit"></i> Edit</button>',
            '<button type="button" class="btnDeleteModal btn btn-danger text-left" id="' + record.GodownId + '"><i class="fa fa-times-circle"></i> Delete</button>'
        ]);
        stores.push([
            record.GodownId,
            record.GodownName
        ]);
    });
    var count = 0;
    if (checkPriviledge("frmStore", "Update") === false) {
        objToShow.forEach(function (item) {
            item.splice(2, 1);
        });
        count++;
    }

    if (checkPriviledge("frmStore", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 3 - count;
            item.splice(recalculateIndex, 1);
        });
    }
    table = $('#storeTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    hideLoader();
}

function findStore(id) {
    var store = "";
    for (i = 0; i < stores.length; i++) {
        if (stores[i][0] == id) {
            store = stores[i];
            break;
        }
    }
    return store;
}