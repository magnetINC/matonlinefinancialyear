﻿var stores = [];
var racks = [];
var rackToEdit = "";
var rackIdToDelete = 0;
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created

$(function () {

    getStores();

    $("#rackTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        rackToEdit = findRack(id);
        $("#editRackId").val(rackToEdit[0]);
        $("#editRack").val(rackToEdit[1]);
        $("#editStoreId").val(rackToEdit[2]);
        $('#editModal').modal('toggle');
    });

    $("#rackTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        rackToEdit = findRack(id);
        rackIdToDelete = id;
        $("#deleteStore").html(rackToEdit[1]);
        $('#deleteModal').modal('toggle');

    });


    $("#save").click(function () {
        if ($("#rack").val() != "")
        {
            $("#errorMsg").html("");

            var rackToSave = {
                RackName: $("#rack").val(),
                GodownId: $("#storeId").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Rack/AddRack",
                type: 'POST',
                data: JSON.stringify(rackToSave),
                contentType: "application/json",
                success: function (data) {
                    //hideLoader();
                    $("#rack").val("");
                    $("#storeId").val("");
                    $("#storeId").trigger("chosen:updated");
                    //table.destroy();
                    table.clear()
                    getStores();
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else
        {
            $("#errorMsg").html("rack name is required!");
        }
        
    });

    $("#delete").click(function () {
        var rackToDelete = {
            RackId: rackIdToDelete
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/Rack/DeleteRack",
            type: 'POST',
            data:JSON.stringify(rackToDelete),
            contentType: "application/json",
            success: function (data) {
                //hideLoader();
                table.clear();
                $('#rackTable').DataTable().destroy();
                getStores();
                //getRacks();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    });

    $("#saveChanges").click(function () {
        if ($("#editRack").val() != "") {
            $("#editErrorMsg").html("");

            var rackToEdit = {
                RackId: $("#editRackId").val(),
                RackName: $("#editRack").val(),
                GodownId: $("#editStoreId").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Rack/EditRack",
                type: 'POST',
                data: JSON.stringify(rackToEdit),
                contentType: "application/json",
                success: function (data) {
                   // hideLoader();
                    //table.destroy();
                    table.clear()
                    getStores();
                    ('#editModal').modal('toggle');
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else {
            $("#editErrorMsg").html("Store name is required!");
        }

    });
});

function getStores() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/Store/GetStores",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            stores = [];
            $.each(data, function (i, record) {                
                stores.push([
                    record.GodownId,
                    record.GodownName
                ]);
            });
            //var storesHtml = "";
            //$.each(stores, function (i, record) {
            //    storesHtml += '<option value="' + record[0] + '">' + record[1] + '</option>';
            //});
            //$("#storeId").html(storesHtml);
            $("#editStoreId").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
            $("#storeId").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
            $("#storeId").chosen({ width: "100%", margin: "1px" });
            //load the racks after loading stores
            getRacks();
           // hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function getRacks() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/Rack/GetRacks",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function onSuccess(data) {
    var objToShow = [];
    console.log(data);
    racks = [];
    $.each(data, function (i, record) {
        var temp = findStore(record.GodownId);
        objToShow.push([
            (i + 1),
            record.RackName,
            temp[1],
            '<button type="button" class="btnEditModal btn btn-primary text-left" id="' + record.RackId + '"><i class="fa fa-edit"></i> Edit</button>',
            '<button type="button" class="btnDeleteModal btn btn-danger text-left" id="' + record.RackId + '"><i class="fa fa-times-circle"></i> Delete</button>'
        ]);
        racks.push([
            record.RackId,
            record.RackName,
            record.GodownId
        ]);
    });

    var count = 0;
    if (checkPriviledge("frmRack", "Update") === false) {
        objToShow.forEach(function (item) {
            item.splice(3, 1);
        });
        count++;
    }

    if (checkPriviledge("frmRack", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 4 - count;
            item.splice(recalculateIndex, 1);
        });
    }
    if (table == "") {
        table = $('#rackTable').DataTable({
            data: objToShow,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
    }
    else {
        table.rows.add(objToShow).draw();
    }


    hideLoader();
}

function findStore(id) {
    var store = "";
    for (i = 0; i < stores.length; i++) {
        if (stores[i][0] == id) {
            store = stores[i];
            break;
        }
    }
    return store;
}

function findRack(id) {
    var rack = "";
    for (i = 0; i < racks.length; i++) {
        if (racks[i][0] == id) {
            rack = racks[i];
            break;
        }
    }
    return rack;
}