﻿var modelNumbers = [];
var modelNumberToEdit = "";
var modelNumberIdToDelete = 0;
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created

$(function () {

    getModelNumbers();

    $("#modelNumberTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        modelNumberToEdit = findModelNumber(id);
        $("#editModelNoId").val(modelNumberToEdit[0]);
        $("#editModelNo").val(modelNumberToEdit[1]);
        $('#editModal').modal('toggle');
    });

    $("#modelNumberTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        modelNumberToEdit = findModelNumber(id);
        modelNumberIdToDelete = id;
        $("#deleteModelNo").html(modelNumberToEdit[1]);
        $('#deleteModal').modal('toggle');

    });


    $("#save").click(function () {
        if ($("#modelNo").val() != "")
        {
            $("#errorMsg").html("");

            var modelNoToSave = {
                ModelNo: $("#modelNo").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/ModelNumber/AddModelNo",
                type: 'POST',
                data: JSON.stringify(modelNoToSave),
                contentType: "application/json",
                success: function (data) {
                    //hideLoader();
                    table.destroy();
                    getModelNumbers();
                    
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
            Utilities.SuccessNotification("Model Number Saved Successfully");
        }
        else
        {
            $("#errorMsg").html("Model number is required!");
        }
        
    });

    $("#delete").click(function () {
        var modelNoToDelete = {
            ModelNoId: modelNumberIdToDelete
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/ModelNumber/DeleteModelNo",
            type: 'POST',
            data:JSON.stringify(modelNoToDelete),
            contentType: "application/json",
            success: function (data) {
                //hideLoader();
                table.destroy();
                getModelNumbers();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    });

    $("#saveChanges").click(function () {
        if ($("#editModelNo").val() != "") {
            $("#editErrorMsg").html("");

            var sizeToEdit = {
                ModelNoId: $("#editModelNoId").val(),
                ModelNo: $("#editModelNo").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/ModelNumber/EditModelNo",
                type: 'POST',
                data: JSON.stringify(sizeToEdit),
                contentType: "application/json",
                success: function (data) {
                   // hideLoader();
                    table.destroy();
                    getModelNumbers();
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else {
            $("#editErrorMsg").html("Model number is required!");
        }

    });
});

function getModelNumbers() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/ModelNumber/GetModelNos",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            var objToShow = [];
            $.each(data, function (i, record) {
                
                objToShow.push([
                    (i + 1),
                    record.ModelNo,
                    '<button type="button" class="btnEditModal btn btn-primary text-left" id="' + record.ModelNoId + '"><i class="fa fa-edit"></i> Edit</button>',
                    '<button type="button" class="btnDeleteModal btn btn-danger text-left" id="' + record.ModelNoId + '"><i class="fa fa-times-circle"></i> Delete</button>'
                ]);
                modelNumbers.push([
                    record.ModelNoId,
                    record.ModelNo
                ]);
            });

            table = $('#modelNumberTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function findModelNumber(id) {
    var modelNumber = "";
    for (i = 0; i < modelNumbers.length; i++) {
        if (modelNumbers[i][0] == id) {
            modelNumber = modelNumbers[i];
            break;
        }
    }
    return modelNumber;
}