﻿var itemToEdit = {};
var bomForProduct = [];
var productDetail = [];
var units = [];
var stores = [];
var brands = [];
var racks = [];
var taxes = [];
var sizes = [];
var productGroups = [];
var modelNos = [];
var salesAccounts = [];
var expenseAccounts = [];
var bom = [];
var products = [];
var allUnitConversionItems = [];

var stocks = [];
var batch = [];
var condition = [{val:true, txt:"Yes"}, {val:false, txt: "No"}];

var imageName = "";

itemToEdit.ProductSp = {};
itemToEdit.ProductInfo = {};
itemToEdit.UnitConvertion = {};
itemToEdit.UnitConvertionInfo = {};
itemToEdit.NewStores = [];
itemToEdit.NewBoms = [];
itemToEdit.NewMultipleUnits = [];
itemToEdit.NewStockPostings = [];
itemToEdit.AutoBarcode = false;
itemToEdit.IsSaveBomCheck = false;
itemToEdit.IsSaveMultipleUnitCheck = false;
itemToEdit.IsOpeningStock = false;
itemToEdit.IsBatch = false;

var currentTab = "Main";


//on page load
$(function ()
{
    GetProductDetails(productId);
    GetLookUps();

    pageLoadProcesses();
});

function pageLoadProcesses()
{

    var imageName = "";
    document.getElementById('productImage').onchange = function () {
        //alert('Selected file: ' + this.value.replace(/C:\\fakepath\\/i, ''));
        imageName = this.value.replace(/C:\\fakepath\\/i, '');
    };
    $("#btnPrevious").attr("disabled", "disabled");
    $("#btnSaveItem").hide();

    $("#productImage").change(function () {
        //alert("dd");
        readURL(this);
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        currentTab = e.target.text;
        if (currentTab == "Main") {
            $("#btnPrevious").attr("disabled", "disabled");
            $("#btnNext").show();
            $("#btnSaveItem").hide();
        }
        else if (currentTab == "Secondary") {
            $("#btnPrevious").removeAttr("disabled");
            $("#btnNext").show();
            $("#btnSaveItem").hide();
        }
        else if (currentTab == "Others") {
            $("#btnPrevious").removeAttr("disabled");
            $("#btnNext").hide();
            $("#btnSaveItem").show();
        }
    })
    $("#btnNext").click(function () {

        if (currentTab == "Main") {
            $('.nav-tabs a[href="#tab2' + '"]').tab('show');
        }
        else if (currentTab == "Secondary") {
            $('.nav-tabs a[href="#tab3' + '"]').tab('show');
        }

    });
    $("#btnPrevious").click(function () {

        if (currentTab == "Others") {
            $('.nav-tabs a[href="#tab2' + '"]').tab('show');
        }
        else if (currentTab == "Secondary") {
            $('.nav-tabs a[href="#tab1' + '"]').tab('show');
        }

    });

    $('#store').on('change', function () {
        var filteredRacks2 = []; //empty the array each time function is called
        var chk = $(this).val();
        $.each(racks, function (i, record) {
            if (record.GodownId == chk) {
                filteredRacks2.push(record);
            }
        });
        var rackHtml2 = "";
        $.each(filteredRacks2, function (i, record) {
            rackHtml2 += '<option value="' + record.RackId + '">' + record.RackName + '</option>';
        });
        $("#rack").html(rackHtml2);
    });

    $('#itemType').on('change', function () {
        var val = this.value;

        switch (val) {
            case "Product":
                //$("#openingStock").attr('disabled', 'disabled');
                $("#lblSalesRate").html("Sales Rate");
                $("#openingStock").removeAttr('disabled');
                $("#addModal").removeAttr('disabled');
                $("#minimumStock").removeAttr('disabled');
                $("#maximumStock").removeAttr('disabled');
                $("#reorderLevel").removeAttr('disabled');
                $("#store").removeAttr('disabled');
                $("#rack").removeAttr('disabled');
                $("#mrp").removeAttr('disabled');
                $("#brand").removeAttr('disabled');
                $("#bom").removeAttr('disabled');
                $("#allowBatch").removeAttr('disabled');
                $("#multipleUnit").removeAttr('disabled');
                break;
            case "Service":
                $("#lblSalesRate").html("Service Rate");
                $("#openingStock").attr('disabled', 'disabled');
                $("#addModal").attr('disabled', 'disabled');
                $("#minimumStock").attr('disabled', 'disabled');
                $("#maximumStock").attr('disabled', 'disabled');
                $("#reorderLevel").attr('disabled', 'disabled');
                $("#store").attr('disabled', 'disabled');
                $("#rack").attr('disabled', 'disabled');
                $("#brand").attr('disabled', 'disabled');
                $("#mrp").attr('disabled', 'disabled');
                $("#bom").attr('disabled', 'disabled');
                $("#allowBatch").attr('disabled', 'disabled');
                $("#multipleUnit").attr('disabled', 'disabled');
                break;
        }
    });

    $('#chkActive').change(function () {
        if (this.checked) {
            isActive = true;
        }
        else {
            isActive = false;
        }
    });

    $('#chkReminder').change(function () {
        if (this.checked) {
            isShowReminder = true;
        }
        else {
            isShowReminder = false;
        }
    });

    $("#btnAddBomItem").click(function () {
        var r = $("#bomRawMaterial").val();
        var q = $("#bomQuantity").val();
        var u = $("#bomUnit").val();
        bom.push({
            bomId: 0,
            extra1: "",
            extra2: "",
            //extraDate: new Date(),
            rowmaterialId: parseFloat(r),
            unitId: u,
            quantity: parseFloat(q),
            productId: productId
        });
        console.log(bom);
        //reset control
        $("#bomQuantity").val(0);
        renderNewBomItems();
    });

    $("#productImage").change(function () {
        //alert("dd");
        readURL(this);
    });

    $("#btnUpdateItem").click(function () {
        UpdateProductDetails();
    });

    if (productDetail.Isopeningstock == false)
    {
        $("#openingStockStorePanel").css("display", "none");
    }
    
    document.getElementById('productImage').onchange = function () {
        //alert('Selected file: ' + this.value.replace(/C:\\fakepath\\/i, ''));
        imageName = this.value.replace(/C:\\fakepath\\/i, '');
    };
}


//get all look up data
function GetLookUps() {
    var lookUpsAjax = $.ajax({
        url: API_BASE_URL + "/EditItem/GetLookUps?id=" + productId,
        type: "Get",
        contentType: "application/json",
    });
    var groupAjax = $.ajax({
        url: API_BASE_URL + '/ItemGroup/GetProductGroups',
        type: 'Get',
        contentType: "application/json",
    });

    var salesAccountAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetSalesAccounts',
        type: 'Get'
    });

    var expenseAccountAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetExpenseAccount',
        type: 'Get'
    });

    $.when(lookUpsAjax, groupAjax, salesAccountAjax, expenseAccountAjax)
        .done(function (dataLookUps, groupdata, dataSalesAccount, dataExpenseAccount) {
        sizes = dataLookUps[0].Sizes;
        units = dataLookUps[0].Units;
        stores = dataLookUps[0].Stores;
        racks = dataLookUps[0].Racks;
        brands = dataLookUps[0].Brands;
        taxes = dataLookUps[0].Taxes;
        productGroups = groupdata[0];
        modelNos = dataLookUps[0].ModelNos;
        salesAccounts = dataSalesAccount[2].responseJSON;
        expenseAccounts = dataExpenseAccount[2].responseJSON;
        bom = dataLookUps[0].Bom;
        products = dataLookUps[0].Products;
        stocks = dataLookUps[0].Stocks;
        modelNos = dataLookUps[0].ModelNos;
        console.log(dataLookUps);

        PopulateControls();
        populateDropDownLists();
        });

    var itemsAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetProducts',
        type: 'Get'
    });

    var unitConversionLookupAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetUnitConversionItems",
        type: "GET",
        contentType: "application/json",
    });

    $.when(itemsAjax, unitConversionLookupAjax)
        .done(function (dataItems, dataUnitConversion) {
            items = dataItems[2].responseJSON;
            //units = dataUnit[2].responseJSON;
            allUnitConversionItems = dataUnitConversion[0].UnitCoversionItems;
            PopulateControls();
            Utilities.Loader.Hide();
            console.log("allUnitConversionItems", allUnitConversionItems)
        }
        );
}

//get product Details per Product
function GetProductDetails(productId)
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/EditItem/GetProductDetails?id=" + productId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            productDetail = data;
            Utilities.Loader.Hide();
            console.log(productDetail);
        },
        error: function (e) {
            Utilities.Loader.Hide();
        }
    });
}
//bind lookup data to html Controls
function PopulateControls()
{
    $("#blah").attr("src", '/Content/ProductImages/' + productDetail.ProductId + '.jpg');
    $("#productCode").val(productDetail.ProductCode);
    $("#productName").val(productDetail.ProductName);
    $("#purchaseRate").val(productDetail.PurchaseRate);
    $("#salesRate").val(productDetail.SalesRate);
    $("#minimumStock").val(productDetail.MinimumStock);
    $("#maximumStock").val(productDetail.MaximumStock);
    $("#reorderLevel").val(productDetail.ReorderLevel);
    $("#mrp").val(productDetail.Mrp);
    $("#partNumber").val(productDetail.PartNo);
    $("#txtAutoBarcode").val(productDetail.barcode);
    $("#effectiveDate").val(productDetail.EffectiveDate);
    $("#description").val(productDetail.Narration);
}

$("#bomRawMaterial").change(function () {
    var productId = $("#bomRawMaterial").val();
    var unitHtml = "";
    var unitsItemsOfProduct = allUnitConversionItems.filter(p => p.productId == productId);
    console.log("unitsItemsOfProduct", unitsItemsOfProduct)
    for (var i of unitsItemsOfProduct) {
        var unitName = units.find(p => p.unitId == i.unitId).unitName
        unitHtml += `<option value="${i.unitId}">${unitName}</option>`;
    }
    $("#bomUnit").html(unitHtml);
});
//populate dropdownlists
function populateDropDownLists()
{
    var salesAccountHtml = "";
    console.log(salesAccounts);
    $.each(salesAccounts, function (i, record) {
        var selected = "";
        if (record.LedgerId == productDetail.SalesAccount) {
            selected = "selected";
        }
        salesAccountHtml += '<option ' + selected + ' value="' + record.LedgerId + '">' + record.LedgerName + '</option>';
        
    });
    $("#salesAccount").html(salesAccountHtml);

    var expenseAccountHtml = "";
    $.each(expenseAccounts, function (i, record) {
        var selected = "";
        if (record.LedgerId == productDetail.ExpenseAccount) {
            selected = "selected";
        }
        expenseAccountHtml += '<option ' + selected + ' value="' + record.LedgerId + '">' + record.LedgerName + '</option>';
        
    });
    $("#expenseAccount").html(expenseAccountHtml);

    $("#store").html(Utilities.PopulateDropDownFromArray(stores, 0, 1));
    $("#rack").html(Utilities.PopulateDropDownFromArray(racks, 0, 1));
    $("#tax").html(Utilities.PopulateDropDownFromArray(taxes, 0, 1));
    $("#brand").html(Utilities.PopulateDropDownFromArray(brands, 1, 2));
    //$("#group").html(Utilities.PopulateDropDownFromArray(productGroups, 1, 2));
    $("#size").html(Utilities.PopulateDropDownFromArray(sizes, 0, 1));
    $("#unit").html(Utilities.PopulateDropDownFromArray(units, 1, 2));
    $("#bomRawMaterial").html(Utilities.PopulateDropDownFromArray(products, 0, 2));
    $("#bomUnit").html(Utilities.PopulateDropDownFromArray(units, 1, 2));
    //$("#allowBatch").html(Utilities.PopulateDropDownFromArray(condition, 0, 1));
    //$("#multipleUnit").html(Utilities.PopulateDropDownFromArray(condition, 0, 1));
    $("#Isopeningstock").html(Utilities.PopulateDropDownFromArray(condition, 0, 1));
    $("#modelNo").html(Utilities.PopulateDropDownFromArray(modelNos, 0, 1));
    //$("#bom").html(Utilities.PopulateDropDownFromArray(condition, 0, 1));

    /*$("#bomRawMaterial").select2();*/
    $("#bomRawMaterial").selectize();
    var filteredGroupObj = [];
    $.each(productGroups, function (i, record) {
        var upperLevelGroup = "";
        var currentGroup = {};
        for (i = 0; i < productGroups.length; i++) {
            if (productGroups[i].GroupId == record.GroupUnder) {
                currentGroup = productGroups[i];
                break;
            }
        }
        if (currentGroup.GroupUnder == 0) {
            upperLevelGroup = "Primary";
        }
        else {
            upperLevelGroup = currentGroup.GroupName;
        }
        if (record.Extra1 == "Sub Category") {
            //groupHtml += '<option value="' + record.GroupId + '">' + record.GroupName + ' : ' + upperLevelGroup + '</option>';
            var lv3 = findUpperLevelByGroupLevel(productGroups, upperLevelGroup);
            var lv2 = findUpperLevelByGroupLevel(productGroups, lv3);
            console.log(lv2);
            filteredGroupObj.push({
                GroupId: record.GroupId,
                GroupName: record.GroupName,
                CustomDisplay: record.GroupName + " : " + upperLevelGroup + " : " + lv3 + " : " + lv2
            });
        }
    });
    //$("#group").html(groupHtml);
    console.log(filteredGroupObj);
    $("#group").kendoDropDownList({
        filter: "contains",
        dataTextField: "CustomDisplay",
        dataValueField: "GroupId",
        dataSource: filteredGroupObj
    });

    $("#bom").kendoDropDownList({
        dataTextField: "txt",
        dataValueField: "val",
        dataSource: condition,
        select: function (e) {
            var item = this.dataItem(e.item);
            if (item.val == true)
            {
                $("#bomModal").modal("toggle");
            }
        }
    });
    $("#allowBatch").kendoDropDownList({
        dataTextField: "txt",
        dataValueField: "val",
        dataSource: condition
    });
    $("#multipleUnit").kendoDropDownList({
        dataTextField: "txt",
        dataValueField: "val",
        dataSource: condition
    });
    $("#modelNo").kendoDropDownList({
        dataTextField: "modelNo",
        dataValueField: "modelNoId",
        dataSource: modelNos
    });
    //$("#Isopeningstock").kendoDropDownList({
    //    dataTextField: "txt",
    //    dataValueField: "val",
    //    dataSource: condition
    //});

    //$("#salesAccount").val(findSalesAccount(productDetail.SalesAccount).ledgerId);
    //$("#salesAccount :selected").text(findSalesAccount(productDetail.SalesAccount).ledgerName);

    //$("#expenseAccount").val(findExpenseAccount(productDetail.ExpenseAccount).ledgerId);
    //$("#expenseAccount :selected").text(findExpenseAccount(productDetail.ExpenseAccount).ledgerName);

    $("#store").val(findStore(productDetail.GodownId).godownId);
    $("#store :selected").text(findStore(productDetail.GodownId).godownName);

    $("#rack").val(findRack(productDetail.RackId).rackId);
    $("#rack :selected").text(findRack(productDetail.RackId).rackName);

    $("#tax").val(findTax(productDetail.TaxId).taxId);
    $("#tax :selected").text(findTax(productDetail.TaxId).taxName);

    $("#brand").val(findBrand(productDetail.BrandId).brandId);
    $("#brand :selected").text(findBrand(productDetail.BrandId).brandName);

    $("#group").val(findProductGroup(productDetail.GroupId).groupId);
    $("#group :selected").text(findProductGroup(productDetail.GroupId).groupName);

    $("#size").val(findSizes(productDetail.SizeId).sizeId);
    $("#size :selected").text(findSizes(productDetail.SizeId).SizeName);

    $("#unit").val(findUnit(productDetail.UnitId).unitId);
    $("#unit :selected").text(findUnit(productDetail.UnitId).unitName);

    //$("#modelNo").val(findModelNo(productDetail.ModelNoId).modelNoId);
    //$("#modelNo :selected").text(findModelNo(productDetail.ModelNoId).modelNo);

    if (productDetail.IsBom == true) {
        $("#bom").data("kendoDropDownList").value(true);
    }
    else if (productDetail.IsBom == false) {
        $("#bom").data("kendoDropDownList").value(false);
    }
    if (productDetail.Ismultipleunit == true) {
        $("#multipleUnit").data("kendoDropDownList").value(true);
    }
    else if (productDetail.Ismultipleunit == false) {
        $("#multipleUnit").data("kendoDropDownList").value(false);
    }
    if (productDetail.IsallowBatch == true) {
        $("#allowBatch").data("kendoDropDownList").value(true);
    }
    else if (productDetail.IsallowBatch == false) {
        $("#allowBatch").data("kendoDropDownList").value(false);
    }

    if (productDetail.IsBom == true) {
        renderNewBomItems();
    }
    renderStoreItems();
}
//to render bom table 
function renderNewBomItems()
{
    var newBomItemsHtml = "";
    $.each(bom, function (i, record) {
        newBomItemsHtml += '<tr>\
                            <td>' + (i + 1) + '</td>\
                            <td>' + (findProduct(record.rowmaterialId).productName) + '</td>\
                            <td>' + record.quantity + '</td>\
                            <td>' + findUnit(record.unitId).unitName + '</td>\
                            <td>' + Utilities.FormatCurrency(findProduct(record.rowmaterialId).purchaseRate) + '</td>\
                            <td><button class="btn btn-danger" onclick="removeBomItem(' + record.rowmaterialId + ')"><i class="fa fa-times"></i></button></td>\
                          </tr>';
    });
    $("#bomTbody").html(newBomItemsHtml);
    $("#bomTotalInventoryAmount").val(Utilities.FormatCurrency(getBomTotalInventoryValue()));
}
//to remove bom object from array
function removeBomItem(id) {
   
    if (confirm("Remove this item?")) {
        var indexOfObjectToRemove = bom.findIndex(p => p.rowmaterialId == id);
        alert(indexOfObjectToRemove);
        bom.splice(indexOfObjectToRemove, 1);
        renderNewBomItems();
    }
}
function getBomTotalInventoryValue() {
    var total = 0.00;
    $.each(bom, function (i, record) {
        total += (findProduct(record.rowmaterialId).purchaseRate) * record.quantity;
    });
    return total;
}
//to render stock to table when batch is true
function renderStoreItems() {
    var storesHtml = "";
    var stockToShow = [];
    for (i = 0; i < stocks.length; i++)
    {
        if (stocks[i].productId == productId) {
            stockToShow.push(stocks[i]);
        }
    }
    console.log(stockToShow);
    $.each(stockToShow, function (i, record) {
        if (productDetail.IsallowBatch == true) {
            storesHtml += '<tr>\
                            <td class="col-md-1">' + (i + 1) + '</td>\
                            <td class="col-md-1">' + (findStore(record.godownId).godownName) + '</td>\
                            <td class="col-md-1">' + (findRack(record.rackId).rackName) + '</td>\
                            <td class="col-md-1 hideBatch">' + (findBatch(record.batchId).batchNo) + '</td>\
                            <td class="col-md-1 hideBatch">' + (findBatch(record.batchId).manufacturingDate) + '</td>\
                            <td class="col-md-1 hideBatch">' + (findBatch(record.batchId).expiryDate) + '</td>\
                            <td class="col-md-1">' + record.inwardQty + '</td>\
                            <td class="col-md-1">' + record.rate + '</td>\
                            <td class="col-md-1">' + (findUnit(record.unitId).unitName) + '</td>\
                            <td class="col-md-1">' + record.amount + '</td>\
                       </tr>';
        }
        else if (productDetail.IsallowBatch == false) {
            storesHtml += '<tr>\
                           <td class="col-md-1">' + (i + 1) + '</td>\
                            <td class="col-md-1">' + (findStore(record.godownId).godownName) + '</td>\
                            <td class="col-md-1">' + (findRack(record.rackId).rackName) + '</td>\
                            <td class="col-md-1">' + record.inwardQty + '</td>\
                            <td class="col-md-1">' + record.rate + '</td>\
                            <td class="col-md-1">' + (findUnit(record.unitId).unitName) + '</td>\
                            <td class="col-md-1">' + record.amount + '</td>\
                       </tr>';
        }

    });
    $("#stocktbody").html(storesHtml);
    $("#totalInventoryValue").val(getTotalInventoryValue());
}
function getTotalInventoryValue() {
    var total = 0.00;
    $.each(stores, function (i, record) {
        total += record.amount;
    });
    return total;
}

//update product details
function UpdateProductDetails()
{
    Utilities.Loader.Show();
    itemToEdit.ProductInfo =
            {
                ProductId: productId,
                ProductName: $("#productName").val(),
                ProductCode: $("#productCode").val(),
                PurchaseRate: $("#purchaseRate").val(),
                SalesRate: $("#salesRate").val(),
                Mrp: $("#mrp").val() == "" ? "NULL" : $("#mrp").val(),
                MaximumStock: $("#maximumStock").val(),
                MinimumStock: $("#minimumStock").val(),
                ReorderLevel: $("#reorderLevel").val(),
                TaxId: $("#tax").val(),
                UnitId: $("#unit").val(),
                GroupId: $("#group").val(),
                ProductType: $("#itemType").val(),
                SalesAccount: $("#salesAccount").val(),
                EffectiveDate: $("#effectiveDate").val(),
                ExpenseAccount: $("#expenseAccount").val(),
                TaxapplicableOn: $("#taxApplicable").val(),
                BrandId: $("#brand").val(),
                SizeId: $("#size").val(),
                ModelNoId: $("#modelNo").val(),
                GodownId: $("#store").val(),
                RackId: $("#rack").val(),
                IsallowBatch: $("#allowBatch").val(),
                IsBom: $("#bom").val(),
                PartNo: $("#partNumber").val() == "" ? "0" : $("#partNumber").val(),
                Isopeningstock: productDetail.Isopeningstock,
                Ismultipleunit: $("#multipleUnit").val(),
                IsActive: productDetail.IsActive,
                Extra1: imageName,
                Extra2: "",
                IsshowRemember: productDetail.IsshowRemember,   //isShowReminder
                Narration: $("#description").val()
            };
    for (i = 0; i < bom.length; i++)
    {
        itemToEdit.NewBoms.push(
            {
                Extra1: bom[i].extra1,
                Extra2: bom[i].extra2,
                Quantity: bom[i].quantity,
                RawMaterialId: bom[i].rowmaterialId,
                UnitId: bom[i].unitId,
                ProductId: bom[i].productId,
                BomId: bom[i].bomId
        });
    }

    console.log(itemToEdit);

    $.ajax({
        url: API_BASE_URL + "/EditItem/UpdateProductDetails",
        type: "Post",
        contentType: "application/json",
        data: JSON.stringify(itemToEdit),
        success: function (e) {
            $("#returnedProductId").val(itemToEdit.ProductInfo.ProductId);
            $("#frmProductImage").submit();
            Utilities.Loader.Hide();
            Utilities.SuccessNotification("Product Updated!");
            setTimeout(function () {
                window.location = "/Inventory/ItemCreation/Index";;
            }, 2000);
        },
        error: function (e) {
            Utilities.Loader.Hide();
        }
    });
}
//functions to find the exact value in dropdownlist
function findProduct(productId) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].productId == productId) {
            output = products[i];
            break;
        }
    }
    return output;
}
function findTax(taxId) {
    var output = {};
    for (i = 0; i < taxes.length; i++) {
        if (taxes[i].taxId == taxId) {
            output = taxes[i];
            break;
        }
    }
    return output;
}
function findExpenseAccount(ledgerId) {
    var output = {};
    for (i = 0; i < expenseAccounts.length; i++) {
        if (expenseAccounts[i].ledgerId == ledgerId) {
            output = expenseAccounts[i];
            break;
        }
    }
    return output;
}
function findSalesAccount(ledgerId) {
    var output = {};
    for (i = 0; i < salesAccounts.length; i++) {
        if (salesAccounts[i].ledgerId == ledgerId) {
            output = salesAccounts[i];
            break;
        }
    }
    return output;
}
function findBrand(brandId) {
    var output = {};
    for (i = 0; i < brands.length; i++) {
        if (brands[i].brandId == brandId) {
            output = brands[i];
            break;
        }
    }
    return output;
}
function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batch.length; i++) {
        if (batch[i].batchId == batchId) {
            output = batch[i];
            break;
        }
    }
    return output;
}
function findProductGroup(PGId) {
    var output = {};
    for (i = 0; i < productGroups.length; i++) {
        if (productGroups[i].groupId == PGId) {
            output = productGroups[i];
            break;
        }
    }
    return output;
}
function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}
function findSizes(sizeId) {
    var output = {};
    for (i = 0; i < sizes.length; i++) {
        if (sizes[i].sizeId == sizeId) {
            output = sizes[i];
            break;
        }
    }
    return output;
}
function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    storeId = output.godownId;
    return output;
}
//function findModelNo(modelNoId) {
//    var output = {};
//    for (i = 0; i < modelNos.length; i++) {
//        if (modelNos[i].modelNoId == modelNoId) {
//            output = modelNos[i];
//            break;
//        }
//    }
//    return output;
//}
function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}
function findCondition(cond) {
    var output = {};
    for (i = 0; i < condition.length; i++) {
        if (condition[i].val == cond) {
            output = condition[i];
            break;
        }
    }
    return output;
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function findUpperLevelByGroupLevel(groups,groupNameToFind)
{
    var obj = groups.find(p=>p.GroupName == groupNameToFind);
    var groupUnderName = groups.find(p=>p.GroupId == obj.GroupUnder);
    if (groupUnderName == null || groupUnderName == "")
    {
        return "N/A";
    }
    return groupUnderName.GroupName;
}
