﻿var products = [];
var productIdToDelete = 0;
var table = "";
var stores = [];
var productGroups = [];
var excelArrayData = [];
var excelArrayStockData = [];
var tableToUpload = "";

//lookup variables
var nextProductCode = "";
var modelNumbers = [];
var brands = [];
var sizes = [];
var racks = [];
var stores = [];
var taxes = [];
var units = [];
var salesAccounts = [];
var expenseAccounts = [];
//var filteredRacks = [];
var groups = [];
var items = [];
var itemToCreateList = [];

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to reflect, old table is destroyed
//and new one is created

$(function () {

    getProducts();
    populateLookups();
   
    $("#productTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        var productToEdit = findProduct(id);
        productIdToDelete = id;
        $("#deleteProduct").html(productToEdit.ProductName);
        $('#deleteModal').modal('toggle');
    });

    $("#customColums").change(function () {
        console.log($(this).val());
    });

    $("#save").click(function () {
        if ($("#unit").val() != "")
        {
            $("#errorMsg").html("");

            var unitToSave = {
                UnitName: $("#unit").val(),
                noOfDecimalplaces: $("#decimalPlace").val(),
                //Extra1: $("#extra1").val(),
                //Extra2: $("#extra2").val(),
                //Manufacturer: $("#manufacturer").val()08069706038
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Unit/AddUnit",
                type: 'POST',
                data: JSON.stringify(unitToSave),
                contentType: "application/json",
                success: function (data) {
                    //hideLoader();
                    table.destroy();
                    getUnits();
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else
        {
            $("#errorMsg").html("Unit name is required!");
        }
        
    });

    $("#saveChanges").click(function () {
        if ($("#editUnit").val() != "") {
            $("#editErrorMsg").html("");

            var unitToEdit = {
                UnitId: $("#editUnitId").val(),
                UnitName: $("#editUnit").val(),
                noOfDecimalplaces: $("#editDecimalPlace").val(),
                //Narration: $("#editNarration").val(),
                //Extra1: $("#editExtra1").val(),
                //Extra2: $("#editExtra2").val(),
                //Manufacturer: $("#editManufacturer").val(),
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Unit/EditUnit",
                type: 'POST',
                data: JSON.stringify(unitToEdit),
                contentType: "application/json",
                success: function (data) {
                   // hideLoader();
                    table.destroy();
                    getUnits();
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else {
            $("#editErrorMsg").html("Unit name is required!");
        }

    });
});


function getProducts() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetCustomProductList1",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (request, error) {
            Utilities.Loader.Hide();
        }
    });
}

function onSuccess(data) {
    products = data;
    //var objectOutput = [];
    //$.each(data, function (count, record) {
    //    var array = $.map(record, function (value, index) {
    //        return [value];
    //    });
    //    objectOutput.push(array);
    //});

    var filtered = [];
    var objToShow = [];
    var objectOutput = [];
    stores = data.Stores;
    productGroups = data.productGroups == undefined ? [] : data.productGroups;
    //productGroups = Object.entries(data.productGroups).length === 0 ? [] : data.productGroups;
    //$.each(filtered, function (count, record) {
    //    var array = $.map(record, function (value, index) {
    //        return [value];
    //    });
    //    objectOutput.push(array);
    //});
    //console.log(objectOutput);

    //$.each(data, function (i, record) {
    //    filtered.push({
    //        ProductCode: record.ProductCode,
    //        ProductName: record.ProductName,
    //        Barcode: record.barcode,
    //        SalesRate: record.SalesRate,
    //        PurchaseRate: record.PurchaseRate,
    //        Store: record.GodownId,
    //        ReorderLevel: record.ReorderLevel,
    //        MinStock: record.MinimumStock,
    //        MaxStock: record.MaximumStock
    //    });
    //});
    //var check = [1, 0, 3];
    //for (i = 0; i < check.length; i++)
    //{
    //    objToShow.push([
    //        (i + 1),
    //        check[i],
    //        record.ProductCode,
    //        "&#8358;" + Utilities.FormatCurrency(record.PurchaseRate),
    //        "&#8358;" + Utilities.FormatCurrency(record.SalesRate),
    //        '<a type="button" class="btnEditModal btn btn-primary text-left" href="/Inventory/ItemCreation/ViewDetails/' + record.ProductId + '"><i class="fa fa-info-circle"></i> View Details</a>',
    //        '<a type="button" class="btnEditModal btn btn-primary text-left" href="/Inventory/ItemCreation/EditItem/' + record.ProductId + '"><i class="fa fa-edit"></i> Edit</a>',
    //        '<button type="button" class="btn btn-danger text-left" id="' + record.ProductId + '" onclick="deleteItem(' + record.ProductId + ')"><i class="fa fa-times-circle"></i> Delete</button>'
    //    ]);
    //}
    $.each(data.Products, function (i, record) {
        var actionButtonDropdown = '<div class="btn-group itemAction">\
												<button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" type="button">\
													Action <i class="dropdown-caret fa fa-caret-down"></i>\
												</button>\
												<ul class="dropdown-menu dropdown-menu-right">\
													<li class="viewItem"><a href="/Inventory/ItemCreation/ViewDetails/' + record.ProductId + '"><i class="fa fa-info-circle"></i> View Details</a>\
													</li>\
													<li class="editItem"><a href="/Inventory/ItemCreation/EditItem/' + record.ProductId + '"><i class="fa fa-edit"></i> Edit</a>\
													</li>\
													<li class="deleteItem"><button type="button" class="btn text-left" id="' + record.ProductId + '" onclick="deleteItem(' + record.ProductId + ')"><i class="fa fa-times-circle"></i> Delete</button>\
													</li>\
												</ul>\
											</div>';

        var category = findUnderGroup(record.SubCategoryId);
        var subGroup = findUnderGroup(category.groupId);
        var grp = findUnderGroup(subGroup.groupId);
        objToShow.push([
            (i + 1),
            record.ProductName,
            record.ProductCode,
            //record.SalesRate,
            "&#8358;" + Utilities.FormatCurrency(record.SalesRate),
             (grp.groupName != undefined) ? grp.groupName : "N/A",
            (subGroup.groupName != undefined) ? subGroup.groupName : "N/A",
            (category.groupName != undefined) ? category.groupName : "N/A",
            findSubCategoryName(record.SubCategoryId),
            actionButtonDropdown
            //"&#8358;" + Utilities.FormatCurrency(record.PurchaseRate),
            //'<a type="button" class="btnEditModal btn btn-primary text-left" href="/Inventory/ItemCreation/ViewDetails/' + record.ProductId + '"><i class="fa fa-info-circle"></i> View Details</a>',
            //'<a type="button" class="btnEditModal btn btn-primary text-left" href="/Inventory/ItemCreation/EditItem/' + record.ProductId + '"><i class="fa fa-edit"></i> Edit</a>',
            //'<button type="button" class="btn btn-danger text-left" id="' + record.ProductId + '" onclick="deleteItem(' + record.ProductId + ')"><i class="fa fa-times-circle"></i> Delete</button>'
        ]);
    });
    console.log("objToShow", objToShow);
    table = $('#productTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        columnDefs: [
            { width: 200, targets: 1 }
        ]

    });
    var count = 0;
    if (checkPriviledge("frmProductCreation", "View") === false) {
        $(".viewItem").hide();
        count++;
    }
    if (checkPriviledge("frmProductCreation", "Update") === false) {
        $(".editItem").hide();
        count++;
    }
    if (checkPriviledge("frmProductCreation", "Delete") === false) {
        $(".deleteItem").hide();
        count++;
    }
    if (count === 3) {
        $(".itemAction").hide();
    }
    Utilities.Loader.Hide();
}

function findStore(storeId)
{
    var chk = stores.find(p=>p.godownId == storeId);
    if(chk==null || chk==undefined)
    {
        return "";
    }
    else
    {
        return chk.godownName;
    }
}

function findSubCategoryName(groupId)
{
    var chk = productGroups.find(p=>p.groupId == groupId);
    if (chk == null || chk == undefined) {
        return "N/A";
    }
    else {
        return chk.groupName;
    }
}

function findUnderGroup(underId) {
    var chk = productGroups.find(p=>p.groupId == underId);
    if (chk == null || chk == undefined) {
        return "N/A";
    }
    else
    {
        var returnVal = productGroups.find(p=>p.groupId == chk.narration);
        if (returnVal == null || returnVal == undefined) {
            return "N/A";
        }
        else
        {
            return returnVal;
        }
    }
}

function findProduct(id) {
    var product = "";
    for (i = 0; i < products.length; i++) {
        if (products[i].ProductId == id) {
            product = products[i];
            break;
        }
    }
    return product;
}

function deleteItem(id) {
    if (confirm("Remove this item?")) {
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/EditItem/DeleteItem?id=" + id,
            type: 'Get',
            contentType: "application/json",
            success: function (data) {
                if (data == "Unable to delete. Item contains reference.")
                {
                    Utilities.ErrorNotification(data);
                }
                else if (data == "A problem occured") {
                    Utilities.ErrorNotification(data);
                }
                else
                {
                    Utilities.SuccessNotification(data);
                    window.location = "/Inventory/ItemCreation/Index";
                }
                hideLoader();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    }
}



function uploadModal() {
    excelArrayData = [];
    $("#uploadItemModal").modal("show");
}

function ExportToTable() {
    var regex = /^([a-zA-Z0-9()\s_\\.\-:])+(.xlsx|.xls)$/;
    /*Checks whether the file is a valid excel file*/
    if (regex.test($("#excelfile").val().toLowerCase())) {
        var xlsxflag = false; /*Flag for checking whether excel is .xls format or .xlsx format*/
        if ($("#excelfile").val().toLowerCase().indexOf(".xlsx") > 0) {
            xlsxflag = true;
        }
        /*Checks whether the browser supports HTML5*/
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                var data = e.target.result;
                /*Converts the excel data in to object*/
                if (xlsxflag) {
                    var workbook = XLSX.read(data, { type: 'binary' });
                }
                else {
                    var workbook = XLS.read(data, { type: 'binary' });
                }
                /*Gets all the sheetnames of excel in to a variable*/
                var sheet_name_list = workbook.SheetNames;

                var cnt = 0; /*This is used for restricting the script to consider only first sheet of excel*/
                sheet_name_list.forEach(function (y) { /*Iterate through all sheets*/
                    /*Convert the cell value to Json*/
                    if (xlsxflag) {
                        var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
                        console.log("exceljson:", exceljson);
                    }
                    else {
                        var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);
                        console.log("exceljson:", exceljson);
                    }

                    if (exceljson.length > 0 && cnt == 0) {
                        //BindTable(exceljson, '#exceltable');  
                        console.log(exceljson);
                        var presentDate = new Date().toISOString();
                        for (i = 0; i < exceljson.length; i++) {
                            console.log(exceljson[i].Email);
                            exceljson[i].ProductName = exceljson[i]["Item Name"] == undefined ? "" : exceljson[i]["Item Name"];
                            exceljson[i].ProductCode = exceljson[i]["Item Code"] == undefined ? "" : exceljson[i]["Item Code"];
                            exceljson[i].Narration = exceljson[i]["Narration"] == undefined ? "" : exceljson[i]["Narration"];
                            exceljson[i].PurchaseRate = exceljson[i]["Purchase Rate"] == undefined ? "0" : exceljson[i]["Purchase Rate"];
                            exceljson[i].SalesRate = exceljson[i]["Sales Rate"] == undefined ? "0" : exceljson[i]["Sales Rate"];
                            exceljson[i].MaximumStock = exceljson[i]["Maximum Stock"] == undefined ? "0" : exceljson[i]["Maximum Stock"];
                            exceljson[i].MinimumStock = exceljson[i]["Minimum Stock"] == undefined ? "0" : exceljson[i]["Minimum Stock"];
                            exceljson[i].ReorderLevel = exceljson[i]["Reorder Level"] == undefined ? "0" : exceljson[i]["Reorder Level"];
                            exceljson[i].PartNo = exceljson[i]["Part No"] == undefined ? "" : exceljson[i]["Part No"];
                            exceljson[i].barCode = exceljson[i]["Barcode"] == undefined ? "" : exceljson[i]["Barcode"];
                            exceljson[i].Extra2 = exceljson[i]["Manufacturer"] == undefined ? "" : exceljson[i]["Manufacturer"];

                            var effectiveDate = new Date(exceljson[i]["Effective Date"]);
                            exceljson[i].EffectiveDate = isNaN(exceljson[i]["Effective Date"]) && effectiveDate.getMonth() > -1 ? effectiveDate.toISOString() : presentDate;

                            var mfDate = new Date(exceljson[i]["Manufacturing Date"]);
                            exceljson[i].MfD = isNaN(exceljson[i]["Manufacturing Date"]) && mfDate.getMonth() > -1 ? mfDate.toISOString() : "";
                            
                            if (exceljson[i]["Sub-Category"] != undefined) { 
                                var group = groups.find(p => p.GroupName.toUpperCase().trim() == exceljson[i]["Sub-Category"].toUpperCase().trim() && p.Extra1 == "Sub Category");
                            }
                            exceljson[i].GroupId = group == undefined ? "0" : group.GroupId;
                            exceljson[i]["Sub-Category"] = exceljson[i].GroupId == "0" ? "NA" : exceljson[i]["Sub-Category"];

                            if (exceljson[i]["Colour"]!=undefined) {
                                var brand = brands.find(p => p.BrandName.toUpperCase().trim() == exceljson[i]["Colour"].toUpperCase().trim());
                            }
                            exceljson[i].BrandId = brand == undefined ? "" : brand.BrandId;
                            exceljson[i]["Colour"] = exceljson[i].BrandId == "" ? "" : exceljson[i]["Colour"];

                            if (exceljson[i]["Unit"] != undefined) {
                                var unit = units.find(p => p.UnitName.toUpperCase().trim() == exceljson[i]["Unit"].toUpperCase().trim());
                            }
                            exceljson[i].UnitId = unit == undefined ? "" : unit.UnitId;
                            exceljson[i]["Unit"] = exceljson[i].UnitId == "" ? "NA" : exceljson[i]["Unit"];
                           
                            if (exceljson[i]["Size"] != undefined) {
                                var size = sizes.find(p => p.Size.toUpperCase().trim() == exceljson[i]["Size"].toUpperCase().trim());
                            }
                            exceljson[i].SizeId = size == undefined ? "" : size.SizeId;
                            exceljson[i]["Size"] = exceljson[i].SizeId == "" ? "NA" : exceljson[i]["Size"];
                           
                            if (exceljson[i]["Tax"]) {
                                var tax = taxes.find(p => p.TaxName.toUpperCase().trim() == exceljson[i]["Tax"].toUpperCase().trim());
                            }
                            exceljson[i].TaxId = tax == undefined ? "" : tax.TaxId;
                            exceljson[i]["Tax"] = exceljson[i].TaxId == "" ? "NA" : exceljson[i]["Tax"];

                            if (exceljson[i]["Sales Account"] != undefined) {
                                var salesAccount = salesAccounts.find(p => p.LedgerName.toUpperCase().trim() == exceljson[i]["Sales Account"].toUpperCase().trim());
                            }
                            exceljson[i].SalesAccount = salesAccount == undefined ? "" : salesAccount.LedgerId;
                            exceljson[i]["Sales Account"] = exceljson[i].SalesAccount == "" ? "NA" : exceljson[i]["Sales Account"];
                           
                            if (exceljson[i]["Expense Account"] != undefined) {
                                var expenseAccount = expenseAccounts.find(p => p.LedgerName.toUpperCase().trim() == exceljson[i]["Expense Account"].toUpperCase().trim());
                            }
                            exceljson[i].ExpenseAccount = expenseAccount == undefined ? "" : expenseAccount.LedgerId;
                            exceljson[i]["Expense Account"] = exceljson[i].ExpenseAccount == "" ? "NA" : exceljson[i]["Expense Account"];
                           
                            if (exceljson[i]["Model No"] != undefined) {
                                var modelNo = modelNumbers.find(p => p.ModelNo.toUpperCase().trim() == exceljson[i]["Model No"].toUpperCase().trim());
                            }
                            exceljson[i].ModelNoId = modelNo == undefined ? "" : modelNo.ModelNoId;
                            exceljson[i]["Model No"] = exceljson[i].ModelNoId == "" ? "NA" : exceljson[i]["Model No"];
                           
                            if (exceljson[i]["Warehouse"] != undefined) {
                                var warehouse = stores.find(p => p.GodownName.toUpperCase().trim() == exceljson[i]["Warehouse"].toUpperCase().trim());
                            }
                            exceljson[i].GodownId = warehouse == undefined ? "" : warehouse.GodownId;
                            exceljson[i]["Warehouse"] = exceljson[i].GodownId == "" ? "NA" : exceljson[i]["Warehouse"];
                            
                            if (exceljson[i]["Rack"] != undefined) {
                                var rack = racks.find(p => p.RackName.toUpperCase().trim() == exceljson[i]["Rack"].toUpperCase().trim());
                            }
                            exceljson[i].RackId = warehouse == undefined ? "" : warehouse.RackId;
                            exceljson[i]["Rack"] = exceljson[i].RackId == "" ? "NA" : exceljson[i]["Rack"];
                           
                            var isAllowBatch = exceljson[i]["Allow Batch(Yes/No)"] == undefined? "" : exceljson[i]["Allow Batch(manYes/No)"].toUpperCase().trim();
                            if (isAllowBatch == "NO" || isAllowBatch == "N" || isAllowBatch == "FALSE" || isAllowBatch == "F") {
                                exceljson[i].IsallowBatch = false;
                            }
                            else {
                                exceljson[i].IsallowBatch = true;
                            }

                            var productType = exceljson[i]["Item Type"] == undefined ? "" : exceljson[i]["Item Type"].toUpperCase().trim();
                            if(productType == "PRODUCT" || productType ==  "STOCK"){
                                exceljson[i].ProductType = "Product";
                            }
                            else if (productType == "SERVICE" || productType == "SERVICE / NON STOCK") {
                                exceljson[i].ProductType = "Service";
                            }
                            else {
                                exceljson[i].ProductType = "";
                            }

                            exceljson[i].Mrp = "NULL";
                            exceljson[i].IsOpeningstock = "False";
                            exceljson[i].TaxapplicableOn = "MRP";
                            exceljson[i].Extra1 = "NA";
                            exceljson[i].NewStores = [];
                            excelArrayData.push(exceljson[i]);

                            brand = undefined;
                            group = undefined;
                            unit = undefined;
                            size = undefined;
                            tax = undefined;
                            salesAccount = undefined;
                            expenseAccount = undefined;
                            modelNo = undefined;
                            warehouse = undefined;
                            rack = undefined;
                        }
                        cnt++;
                        renderExcelDataToTable();
                        console.log(excelArrayData);
                    }
                });
                //$('#exceltable').show();  
            }
            if (xlsxflag) {/*If excel file is .xlsx extension than creates a Array Buffer from excel*/
                reader.readAsArrayBuffer($("#excelfile")[0].files[0]);
            }
            else {
                reader.readAsBinaryString($("#excelfile")[0].files[0]);
            }
        }
        else {
            alert("Sorry! Your browser does not support HTML5!");
        }
    }
    else {
        alert("Please upload a valid Excel file!");
    }
}


function renderExcelDataToTable() {
    var objToShow = [];
    for (i = 0; i < excelArrayData.length; i++) {
        objToShow.push([
            (i + 1),
            excelArrayData[i].ProductCode,
            excelArrayData[i].ProductName,
            excelArrayData[i].Narration,
            excelArrayData[i]["Sub-Category"],
            excelArrayData[i].Colour,
            excelArrayData[i].Unit,
            excelArrayData[i].Size,
            excelArrayData[i].Extra2,
            excelArrayData[i].Tax,
            excelArrayData[i].PurchaseRate,
            excelArrayData[i].SalesRate,
            excelArrayData[i].MinimumStock,
            excelArrayData[i].MaximumStock,
            excelArrayData[i].ReorderLevel,
            excelArrayData[i].PartNo,
            excelArrayData[i].ProductType,
            excelArrayData[i]["Sales Account"],
            excelArrayData[i]["Expense Account"],
            excelArrayData[i]["Model No"],
            excelArrayData[i].Warehouse,
            excelArrayData[i].Rack,
            excelArrayData[i].IsallowBatch.toString(),
            excelArrayData[i].barCode,
            new Date(excelArrayData[i].EffectiveDate).toLocaleDateString(),
            '<button class="btn btn-sm btn-danger" onclick="removeFromExcelArray(' + i + ')"><i class="fa fa-trash"></i></button>'
        ]);
    }
    if (tableToUpload != "") {
        tableToUpload.destroy();
    }
    tableToUpload = $('#itemListToUpload').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "responsive": true
    });
    // $("#customerName").val(""),
    //$("#email").val(""),
    //$("#address").val(""),
    //$("#phoneNumber").val("")
}

function removeFromExcelArray(index) {
    excelArrayData.splice(index, 1);
    renderExcelDataToTable();
}

function clearExcelArrayTable() {
    excelArrayData = [];
    renderExcelDataToTable();
}

function populateLookups() {
    Utilities.Loader.Show();
    var productCodeAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetNextProductCode',
        type: 'Get'
    });

    var modelNumberAjax = $.ajax({
        url: API_BASE_URL + '/ModelNumber/GetModelNos',
        type: 'Get'
    });

    var brandAjax = $.ajax({
        url: API_BASE_URL + '/Brand/GetBrands',
        type: 'Get'
    });

    var sizeAjax = $.ajax({
        url: API_BASE_URL + '/Size/GetSizes',
        type: 'Get'
    });

    var rackAjax = $.ajax({
        url: API_BASE_URL + '/Rack/GetRacks',
        type: 'Get'
    });

    var storeAjax = $.ajax({
        url: API_BASE_URL + '/Store/GetStores',
        type: 'Get'
    });

    var taxAjax = $.ajax({
        url: API_BASE_URL + '/Tax/GetTaxes',
        type: 'Get'
    });

    var unitAjax = $.ajax({
        url: API_BASE_URL + '/Unit/GetUnits',
        type: 'Get'
    });

    var salesAccountAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetSalesAccounts',
        type: 'Get'
    });

    var expenseAccountAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetExpenseAccount',
        type: 'Get'
    });

    var groupAjax = $.ajax({
        url: API_BASE_URL + '/ItemGroup/GetProductGroups',
        type: 'Get'
    });

    var itemsAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetProducts',
        type: 'Get'
    });

    $.when(productCodeAjax, modelNumberAjax, brandAjax, sizeAjax, rackAjax, storeAjax, taxAjax, unitAjax, salesAccountAjax, expenseAccountAjax, groupAjax, itemsAjax)
        .done(function (dataProductCode, dataModelNumber, dataBrand, dataSize, dataRack, dataStore, dataTax, dataUnit, dataSalesAccount, dataExpenseAccount, dataGroup, dataItems) {

            nextProductCode = dataProductCode[2].responseJSON;
            modelNumbers = dataModelNumber[2].responseJSON;
            brands = dataBrand[2].responseJSON;
            sizes = dataSize[2].responseJSON;
            racks = dataRack[2].responseJSON;
            stores = dataStore[2].responseJSON;
            taxes = dataTax[2].responseJSON;
            units = dataUnit[2].responseJSON;
            salesAccounts = dataSalesAccount[2].responseJSON;
            expenseAccounts = dataExpenseAccount[2].responseJSON;
            groups = dataGroup[2].responseJSON;
            items = dataItems[2].responseJSON;

        }
	);
}

function addNewItem() {
    //links the stock to the item it's associated with
    excelArrayStockData.forEach(function (item) {
        //debugger;
        var product = excelArrayData.find(p => p.ProductCode.trim() == item.ProductCode.trim() && p.ProductName.toUpperCase().trim() == item.ProductName.toUpperCase().trim());
        if (product != undefined)
            product.NewStores.push(item);
    });

    for (var i = 0; i < excelArrayData.length; i++) {
        var itemToCreate = {};
        if (excelArrayData[i].ProductCode == "") {
            Utilities.ErrorNotification("Item code missing at row " + i+1 + "!");
            return;
        }
        if (excelArrayData[i].ProductName == "") {
            Utilities.ErrorNotification("Item name missing at row " + i+1 + "!");
            return;
        }
        if (excelArrayData[i].PurchaseRate == "0") {
            Utilities.ErrorNotification("Purchase rate missing at row " + i + 1 + "!");
            return;
        }
        if (excelArrayData[i].SalesRate == "0") {
            Utilities.ErrorNotification("Sales rate missing at row " + i + 1 + "!");
            return;
        }
        if (parseFloat(excelArrayData[i].MinimumStock) > parseFloat(excelArrayData[i].MaximumStock)) {
            Utilities.ErrorNotification("Minimum stock can't be greater than maximum stock!(Row " + i+1);
            return;
        }
        if (excelArrayData[i].UnitId == "") {
            Utilities.ErrorNotification("Unit missing at row " + i + 1 + "!");
            return;
        }
        if (excelArrayData[i].ProductType == "") {
            Utilities.ErrorNotification("Item type missing at row " + i + 1 + "!");
            return;
        }
        //if (excelArrayData[i].barCode == "") {
        //    Utilities.ErrorNotification("Barcode missing at row " + i+1 + "!");
        //    return;
        //}
        itemToCreate.ProductInfo = excelArrayData[i];
        itemToCreate.ProductInfo.PurchaseRate = Utilities.FormatToNumber(excelArrayData[i].PurchaseRate);
        itemToCreate.ProductInfo.SalesRate = Utilities.FormatToNumber(excelArrayData[i].SalesRate);
        itemToCreate.UnitConvertionInfo =
       {
           UnitId: ""
       };
        itemToCreate.NewStores = excelArrayData[i].NewStores;
       
        //if it has stock associated with it, set opening stock values to true
        if (excelArrayData[i].NewStores.length > 0) {
            itemToCreate.IsOpeningstock = true;
            itemToCreate.ProductInfo.Isopeningstock = true;
            //if it has at least one batch created in one store, set batch to true
            var itemWithBatch = excelArrayData[i].NewStores.find(p => p.Batch != "");
            if (itemWithBatch != undefined) {
                itemToCreate.IsBatch = true;
                itemToCreate.ProductInfo.IsallowBatch = true;
            }
            
        }
        else {
            itemToCreate.IsOpeningstock = false;
        }
        
        
        itemToCreateList.push(itemToCreate);
        
    }
    
    
    //if ($("#effectiveDate").val() == "") {
    //    Utilities.ErrorNotification("Date required!");
    //    return;
    //}

    
        //{
        //    ProductName: $("#productName").val(),
        //    ProductCode: $("#productCode").val(),
        //    PurchaseRate: $("#purchaseRate").val(),
        //    SalesRate: $("#salesRate").val(),
        //    Mrp: $("#mrp").val() == "" ? "NULL" : $("#mrp").val(),
        //    MaximumStock: $("#maximumStock").val(),
        //    MinimumStock: $("#minimumStock").val(),
        //    ReorderLevel: $("#reorderLevel").val(),
        //    TaxId: $("#tax").val(),
        //    UnitId: $("#unit").val(),
        //    GroupId: $("#group").val(),
        //    ProductType: $("#itemType").val(),
        //    SalesAccount: $("#salesAccount").val(),
        //    EffectiveDate: $("#effectiveDate").val(),
        //    ExpenseAccount: $("#expenseAccount").val(),
        //    TaxapplicableOn: $("#taxApplicable").val(),
        //    BrandId: $("#brand").val(),
        //    SizeId: $("#size").val(),
        //    ModelNoId: $("#modelNo").val(),
        //    GodownId: $("#store").val(),
        //    RackId: $("#rack").val(),
        //    IsallowBatch: $("#allowBatch").val(),
        //    IsBom: $("#bom").val(),
        //    PartNo: $("#partNumber").val() == "" ? "0" : $("#partNumber").val(),
        //    Isopeningstock: $("#openingStock").val(),
        //    Ismultipleunit: $("#multipleUnit").val(),
        //    IsActive: isActive,
        //    Extra1: imageName,
        //    Extra2: $("#manufacturer").val(),
        //    IsshowRemember: isShowReminder,   //isShowReminder
        //    Narration: $("#description").val()
        //};
        //if (!itemToCreate.AutoBarcode) {
        //    itemToCreate.ProductInfo.barcode = $("#txtAutoBarcode").val();
        //}
        //else {
        //    itemToCreate.ProductInfo.barCode = "";
    //}

    //iterate through stock array. 
    //where item name and item code are contained in stock array, push to item
    //item.openingStock = true
    //stocks attached to item have different batches
    //item.isAllowBatch = true
       
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/ProductCreation/AddProduct",
            type: 'POST',
            data: JSON.stringify(itemToCreateList),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                hideLoader();
                if (data.ResponseCode == 200) {
                    $("#returnedProductId").val(data);
                    $("#frmProductImage").submit();
                    Utilities.SuccessNotification(data.ResponseMessage);
                    //$("#successModal").modal("toggle");
                    //countDown("cd", 3);
                }
                else if (data.ResponseCode == 300) {
                    Utilities.ErrorNotification(data.ResponseMessage);
                    //$("#errorModal").modal("toggle");
                }
                else {
                    Utilities.ErrorNotification("Sorry, something went wrong!");
                    //$("#errorModal").modal("toggle");
                }
            },
            error: function (request, error) {
                Utilities.ErrorNotification("Sorry, something went wrong!");
                hideLoader();
            }
        });
};

function ExportStockToTable() {
    //var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xlsx|.xls)$/;
    var regex = /^([a-zA-Z0-9()\s_\\.\-:])+(.xlsx|.xls)$/;

    /*Checks whether the file is a valid excel file*/
    if (regex.test($("#stockExcelfile").val().toLowerCase())) {
        var xlsxflag = false; /*Flag for checking whether excel is .xls format or .xlsx format*/
        if ($("#stockExcelfile").val().toLowerCase().indexOf(".xlsx") > 0) {
            xlsxflag = true;
        }
        /*Checks whether the browser supports HTML5*/
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                var data = e.target.result;
                /*Converts the excel data in to object*/
                if (xlsxflag) {
                    var workbook = XLSX.read(data, { type: 'binary' });
                }
                else {
                    var workbook = XLS.read(data, { type: 'binary' });
                }
                /*Gets all the sheetnames of excel in to a variable*/
                var sheet_name_list = workbook.SheetNames;

                var cnt = 0; /*This is used for restricting the script to consider only first sheet of excel*/
                sheet_name_list.forEach(function (y) { /*Iterate through all sheets*/
                    /*Convert the cell value to Json*/
                    if (xlsxflag) {
                        var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
                        console.log("exceljson:", exceljson);
                    }
                    else {
                        var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);
                        console.log("exceljson:", exceljson);
                    }

                    if (exceljson.length > 0 && cnt == 0) {
                        //BindTable(exceljson, '#exceltable');  
                        console.log(exceljson);
                        var presentDate = new Date().toISOString();
                        for (i = 0; i < exceljson.length; i++) {
                            console.log(exceljson[i].Email);
                            exceljson[i].ProductName = exceljson[i]["Item Name"] == undefined ? "" : exceljson[i]["Item Name"];
                            exceljson[i].ProductCode = exceljson[i]["Item Code"] == undefined ? "" : exceljson[i]["Item Code"];
                            exceljson[i].Rate = exceljson[i]["Rate"] == undefined ? "0" : Utilities.FormatToNumber(exceljson[i]["Rate"]);
                            exceljson[i].Batch = exceljson[i]["Batch"] == undefined ? "0" : exceljson[i]["Batch"];
                            //exceljson[i].Amount = exceljson[i]["Amount"] == undefined ? "0" : Utilities.FormatToNumber(exceljson[i]["Amount"]);
                            exceljson[i].Batch = exceljson[i]["Batch"] == undefined ? "" : exceljson[i]["Batch"];
                            if (exceljson[i]["Warehouse"] != undefined) {
                                var warehouse = stores.find(p => p.godownName.toUpperCase().trim() == exceljson[i]["Warehouse"].toUpperCase().trim());
                            }
                            exceljson[i].GodownId = warehouse == undefined ? "" : warehouse.godownId;
                            exceljson[i]["Warehouse"] = exceljson[i].GodownId == "" ? "NA" : exceljson[i]["Warehouse"];

                            exceljson[i].RackId =  "";
                            if (warehouse != undefined && exceljson[i]["Rack"] != undefined) {
                                var rack = racks.find(p => p.RackName.toUpperCase().trim() == exceljson[i]["Rack"].toUpperCase().trim());
                                if (rack != undefined) {
                                    if (warehouse.godownId == rack.GodownId) {
                                        exceljson[i].RackId = rack.GodownId;
                                    }
                                }
                            }
                            exceljson[i]["Rack"] = exceljson[i].RackId == "" ? "NA" : exceljson[i].RackId;                                                                          

                            if (exceljson[i]["Unit"] != undefined) {
                                var unit = units.find(p => p.UnitName.toUpperCase().trim() == exceljson[i]["Unit"].toUpperCase().trim());
                            }
                            exceljson[i].UnitId = unit == undefined ? "" : unit.UnitId;
                            exceljson[i]["Unit"] = exceljson[i].UnitId == "" ? "NA" : exceljson[i]["Unit"];
                           
                                                       
                            var mfDate = new Date(exceljson[i]["Manufacturing Date"]);
                            exceljson[i].MfD = isNaN(exceljson[i]["Manufacturing Date"]) && mfDate.getMonth() > -1 ? mfDate.toISOString() : "";

                            var exDate = new Date(exceljson[i]["Expiry Date"]);
                            exceljson[i].ExpD = isNaN(exceljson[i]["Expiry Date"]) && exDate.getMonth() > -1 ? exDate.toISOString() : "";

                            excelArrayStockData.push(exceljson[i]);
                            warehouse = undefined;
                            rack = undefined;
                            unit = undefined;
                        }
                        cnt++;
                        renderExcelStockDataToTable();
                        console.log(excelArrayStockData);
                    }
                });
                //$('#exceltable').show();  
            }
            if (xlsxflag) {/*If excel file is .xlsx extension than creates a Array Buffer from excel*/
                reader.readAsArrayBuffer($("#stockExcelfile")[0].files[0]);
            }
            else {
                reader.readAsBinaryString($("#stockExcelfile")[0].files[0]);
            }
        }
        else {
            alert("Sorry! Your browser does not support HTML5!");
        }
    }
    else {
        alert("Please upload a valid Excel file!");
    }
}

function renderExcelStockDataToTable() {
    var objToShow = [];
    for (i = 0; i < excelArrayStockData.length; i++) {
        var amount = Utilities.FormatToNumber(excelArrayStockData[i]["Rate"]) * parseInt(excelArrayStockData[i].Quantity);
        objToShow.push([
            (i + 1),
            excelArrayStockData[i].ProductCode,
            excelArrayStockData[i].ProductName,
            excelArrayStockData[i].Warehouse,
            excelArrayStockData[i].Rack,
            excelArrayStockData[i].Quantity,
            excelArrayStockData[i].Rate,
            excelArrayStockData[i].Unit,
            amount,
            excelArrayStockData[i]["Batch"],
            new Date(excelArrayStockData[i].MfD).toLocaleDateString(),
            new Date(excelArrayStockData[i].ExpD).toLocaleDateString(),
            '<button class="btn btn-sm btn-danger" onclick="removeFromExcelArray(' + i + ')"><i class="fa fa-trash"></i></button>'
        ]);
    }
    if (tableToUpload != "") {
        tableToUpload.destroy();
    }
    tableToUpload = $('#stockListToUpload').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "responsive": true
    });
    // $("#customerName").val(""),
    //$("#email").val(""),
    //$("#address").val(""),
    //$("#phoneNumber").val("")
}

function removeStockFromExcelArray(index) {
    excelArrayStockData.splice(index, 1);
    renderExcelStockDataToTable();
}

function clearStockExcelArrayTable() {
    excelArrayStockData = [];
    renderExcelStockDataToTable();
}