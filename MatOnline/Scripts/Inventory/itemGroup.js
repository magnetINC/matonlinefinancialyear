﻿var productGroups = [];
var filteredGroups = [];
var filteredGroupsForEdit = [];
var table = "";
var groupToEdit = {};
var thisProductGroup = {};

$(function () {
    getProductGroup();

    $("#groupLevel").change(function () {
        filteredGroups = [];
        if ($(this).val() == "Division") {
            //division has only primary as parent group
            filteredGroups.push({ GroupId: "0", GroupName: "Primary" });
        }
        for(i=0;i<productGroups.length;i++)
        {            
            if ($(this).val() == "Group")
            {
                if (productGroups[i].Extra1 == "Division")
                {
                    filteredGroups.push(productGroups[i]);
                }
            }
            else if ($(this).val() == "Sub Group") {
                if (productGroups[i].Extra1 == "Group") {
                    filteredGroups.push(productGroups[i]);
                }
            }
            else if ($(this).val() == "Category") {
                if (productGroups[i].Extra1 == "Sub Group") {
                    filteredGroups.push(productGroups[i]);
                }
            }
            else if ($(this).val() == "Sub Category") {
                if (productGroups[i].Extra1 == "Category") {
                    filteredGroups.push(productGroups[i]);
                }
            }
        }
        var filteredGroupsHtml = "";
        for(i=0;i<filteredGroups.length;i++)
        {
            filteredGroupsHtml += '<option value="' + filteredGroups[i].GroupId + '">' + filteredGroups[i].GroupName + ' :: ' + findGroup(filteredGroups[i].GroupUnder /*filteredGroups[i].GroupId*/) + '</option>';
        }
        $("#under").html(filteredGroupsHtml);
    });

    $("#groupLevelEdit").change(function () {
        filteredGroupsForEdit = [];
        if ($(this).val() == "Division")
        {
            //division has only primary as parent group
            filteredGroupsForEdit.push({ GroupId: "0", GroupName: "Primary" });
        }
        for (i = 0; i < productGroups.length; i++) {
            if ($(this).val() == "Group") {
                if (productGroups[i].Extra1 == "Division") {
                    filteredGroupsForEdit.push(productGroups[i]);
                }
            }
            else if ($(this).val() == "Sub Group") {
                if (productGroups[i].Extra1 == "Group") {
                    filteredGroupsForEdit.push(productGroups[i]);
                }
            }
            else if ($(this).val() == "Category") {
                if (productGroups[i].Extra1 == "Sub Group") {
                    filteredGroupsForEdit.push(productGroups[i]);
                }
            }
            else if ($(this).val() == "Sub Category") {
                if (productGroups[i].Extra1 == "Category") {
                    filteredGroupsForEdit.push(productGroups[i]);
                }
            }
        }
        var filteredGroupsForEditHtml = "";
        for (i = 0; i < filteredGroupsForEdit.length; i++) {
            if (filteredGroupsForEdit[i].GroupUnder == 0 || filteredGroupsForEdit[i].GroupUnder == undefined) {
                filteredGroupsForEditHtml += '<option value="' + filteredGroupsForEdit[i].GroupId + '">' + filteredGroupsForEdit[i].GroupName + '</option>';
            }
            else {
                filteredGroupsForEditHtml += '<option value="' + filteredGroupsForEdit[i].GroupId + '">' + filteredGroupsForEdit[i].GroupName + ' :: ' + findGroup(filteredGroupsForEdit[i].GroupUnder) + '</option>';
            }
        }
        $("#underEdit").html(filteredGroupsForEditHtml);
    });

    $("#addCategory").click(function () {
        if($("#category").val()=="")
        {
            Utilities.ErrorNotification("Please enter category name");
            return;
        }
        else if ($("#groupLevel").val() == "") {
            Utilities.ErrorNotification("Please select category");
            return;
        }
        else if ($("#under").val() == "") {
            Utilities.ErrorNotification("Please select child category");
            return;
        }
        else if (isItemInGroup($("#category").val(),$("#under").val())==true)
        {
            Utilities.ErrorNotification("Sorry, " + $("#category").val() + " already exist under " + $("#under :selected").text());
            return;
        }
        else
        {
            //return;
            var toSave = {
                GroupName: $("#category").val(),
                GroupUnder: $("#under").val(),
                Extra1: $("#groupLevel").val()
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + '/ItemGroup/AddProductGroup',
                type: "POST",
                data:JSON.stringify(toSave),
                contentType: "application/json",
                success: function (data) {
                    if (data != null)
                    {
                        Utilities.SuccessNotification("Category created!");
                        $("#category").val("");
                        $("#groupLevel")[0].selectedIndex = 0;
                        $("#under").html("");
                        getProductGroup();
                    }
                    else
                    {
                        Utilities.ErrorNotification("Sorry, could not create group.");
                        Utilities.Loader.Hide();
                    }
                    
                },
                error: function (err) {
                    Utilities.ErrorNotification("Sorry, could not create group.");
                    Utilities.Loader.Hide();
                }
            });
        }
    });
});

//function renderGrid()
//{
//    $("#itemGroupGrid").kendoGrid({
//        dataSource:  {
//            transport: {
//                read: {
//                    url: API_BASE_URL + '/ItemGroup/GetProductGroups',
//                    type: 'Get',
//                    contentType: "application/json"
//                },
//                create: {
//                    url: API_BASE_URL + "/ItemGroup/AddProductGroup",
//                    type: 'POST',
//                    contentType: "application/json"
//                },
//                update: {
//                    url: API_BASE_URL + "/ItemGroup/EditProductGroup",
//                    type: 'POST',
//                    contentType: "application/json"
//                },
//                destroy: {
//                    url: API_BASE_URL + "/ItemGroup/DeleteProductGroup",
//                    type: 'POST',
//                    contentType: "application/json"
//                },
//                parameterMap: function (data) { return JSON.stringify(data); }
//            },
//            schema: {
//                model: {
//                    id: "GroupId",
//                    fields: {
//                        GroupName: { },
//                        GroupUnder: { editable: true, validation: { required: true } },
//                        //narration: { editable: true, validation: { required: true } },
//                        //description: { editable: true, validation: { required: true } }
//                    }
//                }
//            },
//        },
//        scrollable: false,
//        sortable: true,
//        pageable: true,
//        pageable: {
//            pageSize: 10,
//            pageSizes: [10, 25, 50, 100, 1000],
//            previousNext: true,
//            buttonCount: 5,
//        },
//        toolbar: ["create"],
//        columns:
//            [
//                { field: "GroupName", title: "Group Name" },
//                //{ field: "GroupUnder", title: "Group Name" },
//                { field: 'GroupUnder', title: 'Under', editor: itemGroupCombobox, template: "#=getAccountGroup(GroupUnder)#" },
//                {
//                    command:
//                      [{ name: "edit", text: {edit:"Edit",update:"Save"}},
//                       { name: "destroy" }
//                      ],
//                    title: "&nbsp;", width: "250px"
//                }
//            ],
//        //editable: "popup"
//    });
//}
//function getItemGroupList()
//{
//    Utilities.Loader.Show();
//    $.ajax({
//        url: API_BASE_URL + '/ItemGroup/GetProductGroups',
//        type: "GET",
//        contentType: "application/json",
//        success: function (data) {
//            itemGroup = data;
//            console.log(itemGroup);
//            renderItemGroupList();
//            Utilities.Loader.Hide();
//        },
//        error: function (err) {
//            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
//            Utilities.Loader.Hide();
//        }
//    });
//}
//function renderItemGroupList()
//{
//    var output = "";
//    $.each(itemGroup, function (count, row) {
//        if(row.GroupUnder == 0)
//        {
//            row.GroupUnder = 1;
//        }
//        output += '<tr>\
//                    <td style="white-space: nowrap;"><button onclick="RemoveFromLineItem(' + row.GroupId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button>\
//                    <td>' + row.GroupName + '</td>\
//                    <td>' + getAccountGroup(row.GroupUnder) + '</td>\
//                  </tr>';
//    });
//    $("#itemGroupList").html(output);
//}

function getProductGroup()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + '/ItemGroup/GetProductGroups',
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            productGroups = data;
            var objToShow = [];
            for (i = 0; i < productGroups.length; i++)
            {
                objToShow.push([
                                (i+1),
                                productGroups[i].GroupName,
                                findGroup(productGroups[i].GroupUnder),
                                productGroups[i].Extra1,
                                '<button class="btn btn-sm btn-primary" onclick="fetchForEdit(' + productGroups[i].GroupId + ')"><i class="fa fa-edit"></i></button>',
                                '<button class="btn btn-sm btn-danger" onclick="deleteGroup(' + productGroups[i].GroupId + ')"><i class="fa fa-times"></i></button>'
                                ]);
            }
            if (table != "")
            {
                table.destroy();
            }
            table = $('#itemGroupTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            Utilities.Loader.Hide();
        },
        error:function(err)
        {
            Utilities.Loader.Hide();
        }
    });
}

function isItemInGroup(category,under)
{
    for (i = 0; i < productGroups.length; i++)
    {
        if(productGroups[i].GroupName==category && productGroups[i].GroupUnder==under)
        {
            return true;
        }
    }
    return false;
}

function itemGroupCombobox(container, options) {
    $('<input required data-text-field="GroupName" data-value-field="GroupId" data-bind="value:' + options.field + '"/>')
		.appendTo(container)
		.kendoComboBox({
		    dataSource: productGroups,
		    placeholder: "Select Group",
            searchable: true
		});
}

function findGroup(id)
{
    if (id == 0) {
        return "Primary";
    }
    else if(id == undefined){
        return "";
    }
    else
    {
        return productGroups.find(p=>p.GroupId == id).GroupName;
        //for (var i = 0; i < productGroups.length; i++) {
        //    if (productGroups[i].GroupId == id) {
        //        return productGroups[i].GroupName;
        //    }
        //}
    }
}

function fetchForEdit(id)
{
    console.log(id);
    for (i = 0; i < productGroups.length; i++) {
        if (productGroups[i].GroupId == id) {
            thisProductGroup = productGroups[i];
        }
    }
    console.log(thisProductGroup);
    $("#editModal").modal("show");
    $("#categoryEdit").val(thisProductGroup.GroupName);
    $("#groupLevelEdit").val(thisProductGroup.Extra1);
    filteredGroupsForEdit = [];
    if ($("#groupLevelEdit").val() == "Division") {
        //division has only primary as parent group
        filteredGroupsForEdit.push({ GroupId: 0, GroupName: "Primary", GroupUnder: 0 });
    }
    console.log(filteredGroupsForEdit);
    for (i = 0; i < productGroups.length; i++) {
        if ($("#groupLevelEdit").val() == "Group") {
            if (productGroups[i].Extra1 == "Division") {
                filteredGroupsForEdit.push(productGroups[i]);
            }
        }
        else if ($("#groupLevelEdit").val() == "Sub Group") {
            if (productGroups[i].Extra1 == "Group") {
                filteredGroupsForEdit.push(productGroups[i]);
            }
        }
        else if ($("#groupLevelEdit").val() == "Category") {
            if (productGroups[i].Extra1 == "Sub Group") {
                filteredGroupsForEdit.push(productGroups[i]);
            }
        }
        else if ($("#groupLevelEdit").val() == "Sub Category") {
            if (productGroups[i].Extra1 == "Category") {
                filteredGroupsForEdit.push(productGroups[i]);
            }
        }
    }
    var filteredGroupsForEditHtml = "";
    for (i = 0; i < filteredGroupsForEdit.length; i++) {
        if (filteredGroupsForEdit[i].GroupUnder == 0 || filteredGroupsForEdit[i].GroupUnder == undefined)
        {
            filteredGroupsForEditHtml += '<option value="' + filteredGroupsForEdit[i].GroupId + '">' + filteredGroupsForEdit[i].GroupName + '</option>';
        }
        else {
            filteredGroupsForEditHtml += '<option value="' + filteredGroupsForEdit[i].GroupId + '">' + filteredGroupsForEdit[i].GroupName + ' :: ' + findGroup(filteredGroupsForEdit[i].GroupUnder) + '</option>';
        }
      
    }
    $("#underEdit").html(filteredGroupsForEditHtml);
    document.getElementById('underEdit').value = thisProductGroup.GroupUnder;
}

function editProductGroup()
{
    groupToEdit.GroupUnder = $("#underEdit").val();
    groupToEdit.GroupName = $("#categoryEdit").val();
    groupToEdit.Extra1 = $("#groupLevelEdit").val();
    groupToEdit.GroupId = thisProductGroup.GroupId;
   // groupToEdit.narration = thisProductGroup.Narration;
    groupToEdit.ExtraDate = thisProductGroup.ExtraDate;
    groupToEdit.Extra2 = "";
    console.log(groupToEdit);

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + '/ItemGroup/EditProductGroup',
        type: "POST",
        data: JSON.stringify(groupToEdit),
        contentType: "application/json",
        success: function (data) {
            if (data == 1) {
                $("#editModal").modal("hide");
                Utilities.SuccessNotification("Category Updated!");
                $("#categoryEdit").val("");
                $("#groupLevelEdit")[0].selectedIndex = 0;
                $("#underEdit").html("");
                getProductGroup();
            }
            else {
                Utilities.ErrorNotification("Sorry, could not Update group.");
                Utilities.Loader.Hide();
            }

        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry, could not Update group.");
            Utilities.Loader.Hide();
        }
    });
}

function deleteGroup(id)
{
    if (confirm('Delete this Group?')) {
        var output = {};
        for (i = 0; i < productGroups.length; i++) {
            if (productGroups[i].GroupId == id) {
                output = productGroups[i];
            }
        }

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + '/ItemGroup/DeleteProductGroup',
            type: "POST",
            data: JSON.stringify(output),
            contentType: "application/json",
            success: function (data) {
                if (data == true) {
                    Utilities.Loader.Hide();
                    Utilities.SuccessNotification("Category Deleted!");
                    getProductGroup();
                }
                else {
                    Utilities.ErrorNotification("Sorry, could not Delete group.");
                    Utilities.Loader.Hide();
                }

            },
            error: function (err) {
                Utilities.ErrorNotification("Sorry, could not Delete group.");
                Utilities.Loader.Hide();
            }
        });
    } 
}
