﻿var transferSource = [];
var transferDestination = [];
var master = {};

var accountLedgers = [];
var currencies = [];
var finishedGoods = [];
var allProducts = [];
var allStores = [];
var allRacks = [];
var allUnits = [];
var allBatches = [];
var cashOrBanks = [];
var allUsers = [];

$(function () {
    getLookup();

    $("#transferGoods").click(function () {
        var src = $("#transferSourceGrid").data().kendoGrid.dataSource.view();
        console.log(transferSource);
        $.each(transferDestination, function (count, row) 
        {
            if (row.stockJournalDetailsId == 0 || row.stockJournalDetailsId == undefined)
            {
                //var ind2 = transferDestination.findIndex(p=>p.stockJournalDetailsId == 0 || p.stockJournalDetailsId == undefined);
                transferDestination.splice(count, 1);
            }
            
        });
        $.each(src, function (count, row) {
            if (row.stockJournalDetailsId == 0 || row.stockJournalDetailsId == undefined)
            {
                transferDestination.push({
                    amount: row.amount,
                    barCode: row.barCode,
                    godownId: row.godownId,
                    productCode: row.productCode,
                    productId: row.productId,
                    productName: row.productName,
                    qty: row.qty,
                    rackId: row.rackId,
                    rate: row.rate,
                    unitId: row.unitId,
                    stockJournalDetailsId: 0,
                    stockJournalMasterId: jId
                });
            }
            
        });
        console.log(transferDestination);
        //transferDestination = src;

        //stockJournalDetailsInfoConsumption = [];//empty the array b4 populating
        //$.each(src, function (count, row) {
        //    finishedGoodsItems.push({
        //        productId: findProductIdByProductCode(row.productCode),
        //        Qty: row.qty,
        //        salesRate: row.rate,
        //        unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
        //        //UnitConversionId: row.unitconversionId,
        //        batchId: row.batchId,
        //        godownId: row.godownId,
        //        rackId: row.rackId,
        //        amount: (row.Qty * row.rate),
        //        Slno: (count + 1)
        //    });
        //});
        renderTransferDestinationGrid();

        //$("#transferDestinationGrid").data("kendoGrid").setDataSource(src);
        //console.log(src);


        var datasource = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
        //getTotalDestinationTransferCost(datasource);
    });
    $("#saveStockJournal").click(function () {
        var src = $("#transferSourceGrid").data().kendoGrid.dataSource.view();
        stockJournalDetailsInfoConsumption = [];
        $.each(src, function (count, row) {
            if (row.stockJournalDetailsId == undefined)
            {
                stockJournalDetailsInfoConsumption.push({
                    ProductId: row.productId,
                    Qty: row.qty,
                    Rate: row.rate,
                    UnitId: "",
                    UnitConversionId: "" /*row.unitconversionId*/,
                    BatchId: "",
                    GodownId: row.godownId,
                    RackId: row.rackId,
                    Amount: (row.qty * row.rate),
                    Slno: (count + 1),
                    StockJournalDetailsId: 0,
                    StockJournalMasterId: jId
                });
            }
            else {
                stockJournalDetailsInfoConsumption.push({
                    ProductId: row.productId,
                    Qty: row.qty,
                    Rate: row.rate,
                    UnitId: "",
                    UnitConversionId: "" /*row.unitconversionId*/,
                    BatchId: "",
                    GodownId: row.godownId,
                    RackId: row.rackId,
                    Amount: (row.qty * row.rate),
                    Slno: (count + 1),
                    StockJournalDetailsId: row.stockJournalDetailsId,
                    StockJournalMasterId: row.stockJournalMasterId
                });
            }
        });

        var destination = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
        stockJournalDetailsInfoProduction = [];
        $.each(destination, function (count, row) {
            stockJournalDetailsInfoProduction.push({
                ProductId: row.productId,
                Qty: row.qty,
                Rate: row.rate,
                UnitId: "",
                UnitConversionId: "",
                BatchId: "",
                GodownId: row.godownId,
                RackId: row.rackId,
                Amount: (row.rate * row.qty),
                Slno: (count + 1),
                StockJournalDetailsId: row.stockJournalDetailsId,
                StockJournalMasterId: row.stockJournalMasterId
            });
        });

        stockJournalMasterInfo = {
            Narration: $("#narration").val(),
            InvoiceNo: $("#formNoForTransfer").val(),
            Date: $("#transactionDate").val(),
            // Extra1: stockJournalAction,
            StockJournalMasterId : jId,
            Extra2: matUserInfo.UserId
        };

        additionalCostItems = [];
        var toSave = {
            StockJournalMasterInfo: stockJournalMasterInfo,
            StockJournalDetailsInfoConsumption: stockJournalDetailsInfoConsumption,
            StockJournalDetailsInfoProduction: stockJournalDetailsInfoProduction,
            //LedgerPostingInfo: ledgerPostingInfo,
            AdditionalCostInfo: {},
            TotalAdditionalCost: 0,
            Date: $("#transactionDate").val(),
            Currency: $("#currency").val(),
            AdditionalCostCashOrBankId: $("#bankOrCash").val(),
            AdditionalCostItems: []
        };
        console.log(toSave);

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/StockJournal/EditStockTransfer",
            type: "Post",
            contentType: "application/json",
            data: JSON.stringify(toSave),
            success: function (data) {
                if (data == true) {
                    //stockJournalDetailsInfoConsumption = [];
                    //stockJournalDetailsInfoProduction = [];

                    //additionalCostItems = [];
                    //stockJournalMasterInfo = {};
                    //additionalCostInfo = {};
                    //totalAdditionalCost = 0.0;
                    //$('#manufacturing').prop("checked", "checked");
                    //$("#quantityToManufacture").val(0);
                    //$("#narration").val("");
                    //$("#additionalCostTitle").html("Additional Cost / Narration");
                    //$("#stockAdjustmentGrid").data('kendoGrid').dataSource.data([]);
                    ////$("#transferDestinationGrid").data('kendoGrid').dataSource.data([]);
                    ////$("#transferSourceGrid").data('kendoGrid').dataSource.data([]);
                    ////$("#totalSourceTransferCost").val(0.0);
                    Utilities.Loader.Hide();
                    Utilities.SuccessNotification("Stock Transfer Updated Successfully!");  //clear controls
                    window.location = "/Inventory/StockJournal/stockJournalRegister";
                }
                else {
                    Utilities.Loader.Hide();
                    Utilities.ErrorNotification("Could not save journal!");
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
                Utilities.ErrorNotification("Sorry something went wrong!");
            }
        });
    });

});

function getLookup() {
    Utilities.Loader.Show();
    var lookupsAjax = $.ajax({
        url: API_BASE_URL + "/StockJournal/GetLookups",
        type: "GET",
        contentType: "application/json",
    });
    var detailsAjax = $.ajax({
        url: API_BASE_URL + "/StockJournal/GetPendingStockJournalDetails?id=" + jId,
        type: "GET",
        contentType: "application/json",
    });
    $.when(lookupsAjax, detailsAjax)
    .done(function (lookups, details) {
        console.log(details);
        finishedGoods = lookups[0].FinishedGoods;
        currencies = lookups[0].Currencies;
        allProducts = lookups[0].AllProducts;
        allStores = lookups[0].AllStores;
        allUnits = lookups[0].AllUnits;
        allRacks = lookups[0].AllRacks;
        allBatches = lookups[0].AllBatches;
        cashOrBanks = lookups[0].CashOrBanks;
        accountLedgers = lookups[0].AccountLedgers;
        allUsers = lookups[0].users;
        transferDestination = details[0].production;
        transferSource = details[0].consumption;
        master = details[0].master;

        // formNoForTransfer = lookups.formNoForTransfer;
        $("#formNoForTransfer").val(master[0].formNo);
        $("#narration").val(master[0].narration);
        $("#finishedGoods").html('<option value=""></option>' + Utilities.PopulateDropDownFromArray(finishedGoods, 1, 0));
        $("#currency").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));
        $("#bankOrCash").html(Utilities.PopulateDropDownFromArray(cashOrBanks, 1, 0));

        renderTransferSourceGrid();
        renderTransferDestinationGrid();
        Utilities.Loader.Hide();
        // $("#formNoTransfer").val(formNoForTransfer);
    });
}

//    function getDetailsToRender()
//{
//       // Utilities.Loader.Show();
//        $.ajax({
//            url: API_BASE_URL + "/StockJournal/GetPendingStockJournalDetails?id=" + jId,
//            type: "GET",
//            contentType: "application/json",
//            success: function (data) {
//                console.log(data);
//                transferDestination = data.production;
//                transferSource = data.consumption;
//                addCost = data.addCost;
//                ledgers = data.ledger;
            
//                renderTransferSourceGrid();
//                renderTransferDestinationGrid();

//               // Utilities.Loader.Hide();
//            },
//            error: function (err) {
//                //Utilities.Loader.Hide();
//            }
//        });
//    }

    function renderTransferSourceGrid() {
        $("#transferSourceGrid").kendoGrid({
            dataSource: {
                data: transferSource,
                schema: {
                    model: {
                        id: "productId",
                        fields: {
                            productCode: { editable: true, validation: { required: false } },
                            barCode: { editable: true, validation: { required: false } },
                            productName: { editable: true, validation: { required: false } },
                            qty: { editable: true, validation: { required: false, type: "number" } },
                            unitId: { editable: true, validation: { required: false } },
                            godownId: { editable: true, validation: { required: false } },
                            rackId: { editable: true, validation: { required: false } },
                            //batchId: { editable: true, validation: { required: true } },
                            rate: { editable: true, validation: { required: false, type: "number" } },
                            amount: { editable: true, validation: { required: false, type: "number" } },
                        }
                    }
                },
            },
            scrollable: false,
            sortable: true,
            pageable: true,
            pageable: {
                pageSize: 15,
                pageSizes: [5, 10, 20, 50, 100],
                previousNext: true,
                buttonCount: 5,
            },
            toolbar: [{ name: 'create', text: 'Add Item' }],
            columns: [
                { field: "productName", title: "Product Name", editor: productNameDropDownEditor, template: "#= findProduct(productId).productName #" },
                { field: "productCode", title: "Product Code", editor: productCodeDropDownEditor, template: "#= findProduct(productId).productCode #" },
                { field: "barCode", title: "BarCode", editor: barCodeDropDownEditor, template: "#= findProduct(productId).productCode #" },
                { field: "qty", title: "Quantity", editor: numericEditor },
                { field: "godownId", title: "Wareouse", editor: storeDropDownEditor, template: "#= showStore(godownId) #" },
                { field: "rackId", title: "Rack", editor: rackDropDownEditor, template: "#= showRack(rackId) #" },
                { field: "rate", title: "Rate" },
                {
                    field: "amount", title: "Amount",
                    editor: function (cont, options) {
                        $("<span>" + options.model.amount + "</span>").appendTo(cont);
                    }
                },
                {
                    command: [{ name: 'destroy', text: '' }]
                }],
            editable: "incell",
            save: function (data) {

                var datasource = $("#transferSourceGrid").data().kendoGrid.dataSource.view();
                //console.log(datasource);
                //getTotalSourceTransferCost(datasource);

                //console.log(datasource);
            },
            remove: function (e) {
                var datasource = $("#transferSourceGrid").data().kendoGrid.dataSource.view();
                var indexToRemove = getKendoLineItemIndex(datasource, e.model);
                datasource.splice(indexToRemove, 1);
                // getTotalSourceTransferCost(datasource);
                console.log(datasource);
            }
        });

        var grid = $("#transferSourceGrid").data("kendoGrid");
        grid.hideColumn(6);
        grid.hideColumn(7);
    }

    function renderTransferDestinationGrid() {
        $("#transferDestinationGrid").kendoGrid({
            dataSource: {
                data: transferDestination,
                schema: {
                    model: {
                        id: "productId",
                        fields: {
                            productName: { editable: false, validation: { required: true } },
                            productId: { editable: false, validation: { required: true } },
                            productCode: { editable: false, validation: { required: true } },
                            barCode: { editable: false, validation: { required: true } },
                            qty: { editable: false, validation: { required: true, type: "number" } },
                            unitId: { editable: false, validation: { required: true } },
                            godownId: { editable: true, validation: { required: true } },
                            rackId: { editable: true, validation: { required: true } },
                            //batchId: { editable: true, validation: { required: true } },
                            rate: { editable: false, validation: { required: true, type: "number" } },
                            amount: { editable: false, validation: { required: true, type: "number" } },
                        }
                    }
                },
            },
            scrollable: false,
            sortable: true,
            pageable: true,
            pageable: {
                pageSize: 15,
                pageSizes: [5, 10, 20, 50, 100],
                previousNext: true,
                buttonCount: 5,
            },
            //toolbar: [{ name: 'create', text: 'Add Goods' }], removed on recommendation by toyin based on what is in the windows form
            columns: [
                //{title:"#",template:"#= ++count #"},
                //{ field: "ledgerId", title: "Ac/ Ledger", editor: accountLedgerDropDownEditor, template: "#= getAccountLedger(ledgerId) #" },
                //{ field: "productCode", title: "Product Code", editor: productCodeDestinationDropDownEditor, template: "#= findProductByProductCode(productCode) #" },
                { field: "productName", title: "Product Name", editor: productNameDestinationDropDownEditor, template: "#= findProduct(productId).productName #" },
                { field: "barCode", title: "BarCode", editor: barCodeDestinationDropDownEditor, template: "#= findProduct(productId).productCode #" },
                { field: "qty", title: "Quantity", editor: destinationTransferNumericEditor },
                { field: "godownId", title: "Warehouse", editor: storeDestinationDropDownEditor, template: "#= showStore(godownId) #" },
                { field: "rackId", title: "Rack", editor: rackDestinationDropDownEditor, template: "#= showRack(rackId) #" },
                //{ field: "batchId", title: "Batch" },
                { field: "rate", title: "Rate" },
                { field: "amount", title: "Amount" },
                {
                    command: [{ name: 'destroy', text: '' }]
                }],
            editable: "incell",
            edit: function () {
                // console.log("edit");
            },
            beforeEdit: function (e) {
                var columnIndex = this.cellIndex(e.container);
                var fieldName = this.thead.find("th").eq(columnIndex).data("productCode");

                if (!isEditable(fieldName, e.model)) {
                    e.preventDefault();
                }
            },
            save: function (e) {
                var datasource = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
                //getTotalDestinationTransferCost(datasource);
                transferDestination = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
                //getTotalAdditionalCost(datasource);
                console.log(transferDestination);
                finishedGoodsRawMaterial = [];
                $.each(transferDestination, function (count, row) {
                    finishedGoodsRawMaterial.push({
                        productId: findProductIdByProductCode(row.productCode),
                        Qty: row.qty,
                        salesRate: row.rate,
                        unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
                        //UnitConversionId: row.unitconversionId,
                        batchId: row.batchId,
                        godownId: row.godownId,
                        rackId: row.rackId,
                        amount: (row.Qty * row.rate),
                        Slno: (count + 1)
                    });
                });
            },
            remove: function (e) {
                var datasource = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
                var indexToRemove = getKendoLineItemIndex(datasource, e.model);
                datasource.splice(indexToRemove, 1);
                //getTotalDestinationTransferCost(datasource);
                console.log(e);
                $.each(transferDestination, function (count, row) {
                    var index = transferDestination.findIndex(p=>p.productId == e.model.productId)
                    transferDestination.splice(index, 1);
                });

                finishedGoodsRawMaterial = [];
                $.each(transferDestination, function (count, row) {
                    finishedGoodsRawMaterial.push({
                        productId: findProductIdByProductCode(row.productCode),
                        Qty: row.qty,
                        salesRate: row.rate,
                        unitId: findProduct(findProductIdByProductCode(row.productCode)).unitId,
                        //UnitConversionId: row.unitconversionId,
                        batchId: row.batchId,
                        godownId: row.godownId,
                        rackId: row.rackId,
                        amount: (row.Qty * row.rate),
                        Slno: (count + 1)
                    });
                });
                //var datasource = $("#additionalCostGrid").data().kendoGrid.dataSource.view();
                //var indexToRemove = getKendoLineItemIndex(datasource, e.model);
                //datasource.splice(indexToRemove, 1);
                //getTotalAdditionalCost(datasource);
            }
        });
        var grid = $("#transferDestinationGrid").data("kendoGrid");
        grid.hideColumn(5);
        grid.hideColumn(6);
    }




    /********Editors for source transfer**********/
    function accountLedgerDropDownEditor(container, options) {
        $('<input required name="' + options.field + '" data-text-field="ledgerName" data-value-field="ledgerId" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: {
                    data: accountLedgers
                },
                template: '<span>#: ledgerName #</span>',
                filter: "contains",
            });
    }

    function productCodeDropDownEditor(container, options) {
        $('<input name="' + options.field + '" data-text-field="productCode" data-value-field="productCode" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "productCode",
                dataValueField: "productCode",
                dataSource: {
                    data: allProducts
                },
                template: '<span>#: productCode #</span>',
                filter: "contains",
                change: function (data) {
                    var grid = $("#transferSourceGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    model.set("productId", findProductByParam("productcode", model.productCode).productId);
                    model.set("barCode", findProductByParam("productcode", model.productCode).productCode);
                    model.set("productName", findProductByParam("productcode", model.productCode).productName);
                    model.set("rate", findProductByParam("productcode", model.productCode).salesRate);
                }
            });
    }

    function barCodeDropDownEditor(container, options) {
        $('<input name="' + options.field + '" data-text-field="productCode" data-value-field="productCode" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "productCode",
                dataValueField: "productCode",
                dataSource: {
                    data: allProducts
                },
                template: '<span>#: productCode #</span>',
                filter: "contains",
                change: function (data) {
                    var grid = $("#transferSourceGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    model.set("productId", findProductByParam("productcode", model.barCode).productId);
                    model.set("productCode", findProductByParam("productcode", model.barCode).productCode);
                    model.set("productName", findProductByParam("productcode", model.barCode).productName);
                    model.set("rate", findProductByParam("productcode", model.barCode).salesRate);
                }
            });
    }

    function productNameDropDownEditor(container, options) {
        $('<input name="' + options.field + '" data-text-field="productName" data-value-field="productName" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "productName",
                dataValueField: "productName",
                dataSource: {
                    data: allProducts
                },
                template: '<span>#: productName #</span>',
                filter: "contains",
                change: function (data) {
                    var grid = $("#transferSourceGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    model.set("productId", findProductByParam("productname", model.productName).productId);
                    model.set("barCode", findProductByParam("productname", model.productName).productCode);
                    model.set("productCode", findProductByParam("productname", model.productName).productCode);
                    model.set("rate", findProductByParam("productname", model.productName).salesRate);
                }
            });
    }

    function numericEditor(container, options) {
        $('<input value="0" name="' + options.field + '" data-text-field="" data-value-field="" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoNumericTextBox({
                min: 0,
                change: function (data) {
                    var grid = $("#transferSourceGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    //model.set("barCode", findProductByParam("productname", model.productName).productCode);
                    //model.set("productName", findProductByParam("productname", model.productName).productName);
                    //model.set("rate", findProductByParam("productname", model.productName).salesRate);
                    model.set("amount", model.qty * model.rate);
                    var datasource = $("#transferSourceGrid").data().kendoGrid.dataSource.view();
                   // getTotalSourceTransferCost(datasource);
                }
            });
    }

    function destinationTransferNumericEditor(container, options) {
        $('<input value="0" name="' + options.field + '" data-text-field="" data-value-field="" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoNumericTextBox({
                min: 0,
                change: function (data) {
                    var grid = $("#transferDestinationGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    //model.set("barCode", findProductByParam("productname", model.productName).productCode);
                    //model.set("productName", findProductByParam("productname", model.productName).productName);
                    //model.set("rate", findProductByParam("productname", model.productName).salesRate);
                    model.set("amount", model.qty * model.rate);
                    var datasource = $("#transferDestinationGrid").data().kendoGrid.dataSource.view();
                    //getTotalDestinationTransferCost(datasource);
                }
            });
    }

    function amountStockAdjustmentNumericEditor(container, options) {
        $('<input readonly name="' + options.field + '" data-text-field="" data-value-field="" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoNumericTextBox({
                min: 0
            });
    }
    function storeDropDownEditor(container, options) {
        $('<input name="' + options.field + '" class="tst" data-text-field="godownName" data-value-field="godownId" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "godownName",
                dataValueField: "godownId",
                dataSource: {
                    data: allStores
                },
                template: '<span>#: godownName #</span>',
                filter: "contains",
                change: function (data) {
                    //tempRacks = allRacks;
                    var grid = $("#transferSourceGrid").data("kendoGrid");
                    //console.log($("#transferSourceGrid").data("kendoGrid").dataSource.data()[0]);
                    //model = grid.dataItem(this.element.closest("tr"));
                    console.log("model", this);
                    //console.log("element", this.element);
                    //model.set("barCode", findProductByParam("productname", model.productName).productCode);
                    //model.set("productName", findProductByParam("productname", model.productName).productName);
                    //model.set("rate", findProductByParam("productname", model.productName).salesRate);
                    // model.set("amount", model.qty * model.rate);
                }
            });
    }

    function rackDropDownEditor(container, options) {
        $('<input name="' + options.field + '" data-text-field="rackName" data-value-field="rackId" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "rackName",
                dataValueField: "rackId",
                dataSource: {
                    data: tempRacks
                },
                template: '<span>#: rackName #</span>',
                filter: "contains",
                open: function () {
                    var grid = $("#transferSourceGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    var st = model.godownId;
                    var filtered = [];
                    model.set("rackId", "");     //empty what was selected
                    $.each(allRacks, function (count, row) {
                        if (row.godownId == st) {
                            filtered.push(row);
                        }
                    });
                    this.setDataSource(filtered);
                    //console.log(allRacks);
                }
            });
    }
    /*********end of source transfer editors*********************/

    /********Editors for destination transfer**********/
    function productCodeDestinationDropDownEditor(container, options) {
        $('<input required name="' + options.field + '" data-text-field="productCode" data-value-field="productCode" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "productCode",
                dataValueField: "productCode",
                dataSource: {
                    data: allProducts
                },
                template: '<span>#: productCode #</span>',
                filter: "contains",
                change: function (data) {
                    var grid = $("#transferDestinationGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    model.set("barCode", findProductByParam("productcode", model.productCode).productCode);
                    model.set("productName", findProductByParam("productcode", model.productCode).productName);
                    model.set("rate", findProductByParam("productcode", model.productCode).salesRate);
                }
            });
    }

    function barCodeDestinationDropDownEditor(container, options) {
        $('<input name="' + options.field + '" data-text-field="productCode" data-value-field="productCode" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "productCode",
                dataValueField: "productCode",
                dataSource: {
                    data: allProducts
                },
                template: '<span>#: productCode #</span>',
                filter: "contains",
                change: function (data) {
                    var grid = $("#transferSourceGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    model.set("barCode", findProductByParam("barcode", model.barCode).productCode);
                    model.set("productName", findProductByParam("barcode", model.barCode).productName);
                    model.set("rate", findProductByParam("barcode", model.barCode).salesRate);
                }
            });
    }

    function productNameDestinationDropDownEditor(container, options) {
        $('<input name="' + options.field + '" data-text-field="productName" data-value-field="productName" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "productName",
                dataValueField: "productName",
                dataSource: {
                    data: allProducts
                },
                template: '<span>#: productName #</span>',
                filter: "contains",
                change: function (data) {
                    var grid = $("#transferDestinationGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    model.set("barCode", findProductByParam("productname", model.productName).productCode);
                    model.set("productName", findProductByParam("productname", model.productName).productName);
                    model.set("rate", findProductByParam("productname", model.productName).salesRate);
                }
            });
    }

    function numericDestinationEditor(container, options) {
        $('<input name="' + options.field + '" data-text-field="" data-value-field="" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoNumericTextBox({
                min: 0,
                change: function (data) {
                    var grid = $("#transferDestinationGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    //model.set("barCode", findProductByParam("productname", model.productName).productCode);
                    //model.set("productName", findProductByParam("productname", model.productName).productName);
                    //model.set("rate", findProductByParam("productname", model.productName).salesRate);
                    model.set("amount", model.qty * model.rate);
                }
            });
    }

    function storeDestinationDropDownEditor(container, options) {
        $('<input name="' + options.field + '" class="tst" data-text-field="godownName" data-value-field="godownId" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "godownName",
                dataValueField: "godownId",
                dataSource: {
                    data: allStores
                },
                template: '<span>#: godownName #</span>',
                filter: "contains",
                change: function (data) {
                    //tempRacks = allRacks;
                    var grid = $("#transferDestinationGrid").data("kendoGrid");
                    //console.log($("#transferSourceGrid").data("kendoGrid").dataSource.data()[0]);
                    //model = grid.dataItem(this.element.closest("tr"));
                    console.log("model", grid);
                    //console.log("element", this.element);
                    //model.set("barCode", findProductByParam("productname", model.productName).productCode);
                    //model.set("productName", findProductByParam("productname", model.productName).productName);
                    //model.set("rate", findProductByParam("productname", model.productName).salesRate);
                    // model.set("amount", model.qty * model.rate);
                }
            });
    }

    function rackDestinationDropDownEditor(container, options) {
        $('<input name="' + options.field + '" data-text-field="rackName" data-value-field="rackId" data-bind="value:' + options.field + '"/>')
            .appendTo(container)
            .kendoComboBox({
                autoBind: true,
                highlightFirst: true,
                suggest: true,
                dataTextField: "rackName",
                dataValueField: "rackId",
                dataSource: {
                    data: tempRacks
                },
                template: '<span>#: rackName #</span>',
                filter: "contains",
                open: function () {
                    var grid = $("#transferDestinationGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    var st = model.godownId;
                    var filtered = [];
                    model.set("rackId", "");     //empty what was selected
                    $.each(allRacks, function (count, row) {
                        if (row.godownId == st) {
                            filtered.push(row);
                        }
                    });
                    this.setDataSource(filtered);
                    //console.log(allRacks);
                }
            });
    }
/*********end of manufacturing editors*********************/


    function findUnit(unitId) {
        var output = {};
        for (i = 0; i < allUnits.length; i++) {
            if (allUnits[i].unitId == unitId) {
                output = allUnits[i];
                break;
            }
        }
        //for (i = 0; i < allUnits.length; i++)
        //{
        //    if (allUnits[i].unitId == unitId) {
        //        output = allUnits[i];
        //        break;
        //    }
        //}
        return output;
    }

    function findBatch(batchId) {
        var output = {};;
        for (i = 0; i < allBatches.length; i++) {
            if (allBatches[i].batchId == batchId) {
                output = allBatches[i];
                break;
            }
        }
        return output;
    }

    function findUsers(userId) {
        var output = {};;
        for (i = 0; i < allUsers.length; i++) {
            if (allUsers[i].userId == userId) {
                output = allUsers[i];
                break;
            }
        }
        return output;
    }

    function findStore(storeId) {
        var output = {};
        for (i = 0; i < allStores.length; i++) {
            if (allStores[i].godownId == storeId) {
                output = allStores[i];
                break;
            }
        }
        return output;
    }

    function findRack(rackId) {
        var output = {};
        for (i = 0; i < allRacks.length; i++) {
            if (allRacks[i].rackId == rackId) {
                output = allRacks[i];
                break;
            }
        }
        return output;
    }

    function findProduct(productId) {
        var output = {};
        for (i = 0; i < allProducts.length; i++) {
            if (allProducts[i].productId == productId) {
                output = allProducts[i];
                break;
            }
        }
        return output;
    }

    function findProductByParam(paramType, param) {
        var output = {};
        for (i = 0; i < allProducts.length; i++) {
            if (paramType == "productcode") {
                if (allProducts[i].productCode == param) {
                    output = allProducts[i];
                    break;
                }
            }
            else if (paramType == "barcode") {
                if (allProducts[i].barcode == param) {
                    output = allProducts[i];
                    break;
                }
            }
            else if (paramType == "productname") {
                if (allProducts[i].productName == param) {
                    output = allProducts[i];
                    break;
                }
            }
        }
        return output;
    }

    function getKendoLineItemIndex(datasource, objectToFindIndex)    //function was created to remove lineitem from row wen its removed from
    {                                                               //kendo grid
        for (i = 0; i < datasource.length; i++) {
            if (objectToFindIndex.id == datasource[i].id) {
                return i;
            }
        }
        return -1;
    }
    function findProductByProductCode(productCode) {
        var output = "N/A";
        for (i = 0; i < allProducts.length; i++) {
            if (allProducts[i].productCode == productCode) {
                output = allProducts[i].productCode;
            }
        }
        return output;
    }

    function findProductByBarCode(barcode) {
        var output = "N/A";
        for (i = 0; i < allProducts.length; i++) {
            if (allProducts[i].productCode == barcode) {
                output = allProducts[i].productCode;
            }
        }
        return output;
    }

    function findProductIdByProductCode(productCode) {
        var output = "N/A";
        for (i = 0; i < allProducts.length; i++) {
            if (allProducts[i].productCode == productCode) {
                output = allProducts[i].productId;
            }
        }
        return output;
    }

    function findProductByProductName(productName) {
        var output = "N/A";
        for (i = 0; i < allProducts.length; i++) {
            if (allProducts[i].productName == productName) {
                output = allProducts[i].productName;
            }
        }
        return output;
    }

    function findProductByProductId(productId) {
        var output = "N/A";
        for (i = 0; i < allProducts.length; i++) {
            if (allProducts[i].productId == productId) {
                output = allProducts[i];
            }
        }
        return output;
    }

    function showStore(storeId) {
        var output = "N/A";
        for (i = 0; i < allStores.length; i++) {
            if (allStores[i].godownId == storeId) {
                output = allStores[i].godownName;
            }
        }
        return output;
    }

    function showRack(rackId) {
        var output = "N/A";
        for (i = 0; i < allRacks.length; i++) {
            if (allRacks[i].rackId == rackId) {
                output = allRacks[i].rackName;
            }
        }
        return output;
    }