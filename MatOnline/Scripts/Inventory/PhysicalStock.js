﻿var products = [];
var racks = [];
var batches = [];
var stores = [];
var units = [];
var searchedProduct = {};
var lineItems = [];
var allUnits = [];
var allStores = [];
var allRacks = [];
var sl = 0;

$(function () {
    getLookUps();

    $("#searchByProductCode").change(function () {
        searchProduct("ProductCode", $("#searchByProductCode").val(), 1);
    });
    $("#searchByProductName").change(function () {
        searchProduct("ProductName", $("#searchByProductName").val(),1);
    });
    $("#searchByBarcode").change(function () {
        searchProduct("ProductCode", $("#searchByBarcode").val(), 1);
    });
    $("#store").change(function () {
        $("#rack").html(Utilities.PopulateDropDownFromArray(getStoreRacks($("#store").val()), 0, 1));
    });

    $("#addLineItem").click(function () {
        sl = sl + 1;//searchedProduct.BatchId
        var quantity = $("#quantityToAdd").val();
        lineItems.push({
            Slno : sl,
            ProductId: searchedProduct.productId,
            GodownId: $("#store").val(),
            RackId: $("#rack").val(),
            Rack: $("#rack :selected").text(),
            BatchId: $("#batch :selected").val(),
            BatchNo:$("#batch :selected").text(),
            Qty : quantity,
            UnitId: $("#unit :selected").val(),
            Unit: $("#unit :selected").text(),
            Rate: searchedProduct.salesRate,
            Amount: searchedProduct.salesRate*quantity
        });
        console.log(lineItems);
        //clear controls
        $("#searchByBarcode").val("");
        $("#searchByBarcode").trigger("chosen:updated");
        $("#searchByProductCode").val("");
        $("#searchByProductCode").trigger("chosen:updated");
        $("#searchByProductName").val("");
        $("#searchByProductName").trigger("chosen:updated");
        $("#rack").val(null);
        $("#store").val(null);
        $("#quantityToAdd").val(1);
        $("#rate").val("");
        $("#batch").val(null);
        $("#batch :selected").text("");
        $("#unit").val(null);
        $("#unit :selected").text("");
        //end
        renderLineItems();
    });

    $("#savePhysicalStock").click(function () {
        if ($("#date").val() == "")
        {
            Utilities.ErrorNotification("Stock count date required!");
        }
        else if (lineItems.length < 1)
        {
            Utilities.ErrorNotification("No item to count!");
        }
        else
        {
            var objToSave = {
                TotalAmount: getTotalAmount(),
                StockDetails: lineItems,
                StockMaster: {
                    //VoucherNo: 18,
                    Extra1:$("#description").val(),
                    Date: $("#date").val(),
                    Narration: $("#narration").val(),
                    //InvoiceNo: 18
                }
            };

            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/PhysicalStock/SavePhysicalStockPending",
                type: 'POST',
                data: JSON.stringify(objToSave),
                contentType: "application/json",
                success: function (data) {
                    $("#date").val(todayDate());
                    $("#description").val("");
                    lineItems = [];
                    renderLineItems();
                    Utilities.Loader.Hide();
                    Utilities.SuccessNotification("Stock count recorded!");
                    console.log("response is ", data);
                },
                error: function () {
                    Utilities.ErrorNotification("Sorry, something went wrong!");
                    Utilities.Loader.Hide();
                }
            });
        }
        
    });
});

function renderLineItems()
{
    sl = 0; //to reset item count
    var output = "";
    $.each(lineItems, function (count, row) {
        output +=
            '<tr>\
                <td style="white-space: nowrap;">'+ row.Slno + '</td>\
                <td style="white-space: nowrap;">' + findProduct(row.ProductId).barcode + '</td>\
                <td style="white-space: nowrap;">' + findProduct(row.ProductId).ProductCode + '</td>\
                <td style="white-space: nowrap;">' + findProduct(row.ProductId).ProductName + '</td>\
                <td style="white-space: nowrap;">'+ row.Qty + '</td>\
                <td style="white-space: nowrap;">' + row.Unit + '</td>\
                <td style="white-space: nowrap;">' + getStore(row.GodownId).godownName + '</td>\
                <td style="white-space: nowrap;"><button onclick="removeLineItem('+ row.ProductId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
            </tr>\
            ';
        //totalLineItemAmount = (totalLineItemAmount + parseFloat(row.Amount));
        //totalTaxAmount = (totalTaxAmount + parseFloat(row.TaxAmount));
    });
    $("#physicalStockLineItemTbody").html(output);
    $("#totalAmount").val(Utilities.FormatCurrency(getTotalAmount()));
    //
                    //<td style="white-space: nowrap;"><button onclick="removeLineItem(' + obj.ProductId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
}

function getTotalAmount()
{
    var total = 0.0;
    for(i=0;i<lineItems.length;i++)
    {
        total=total+lineItems[i].Amount;
    }
    return total;
}

function removeLineItem(productId) {
    if (confirm("Remove this item?"))
    {
        var indexOfObjectToRemove = lineItems.findIndex(p=>p.ProductId == productId);
        lineItems.splice(indexOfObjectToRemove, 1);
        renderLineItems();
    }
}

function getLookUps() {
    Utilities.Loader.Show();
    var productsAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: "Get",
        contentType: "application/json"
    });

    var racksAjax = $.ajax({
        url: API_BASE_URL + "/Rack/GetRacks",
        type: "Get",
        contentType: "application/json"
    });

    var batchAjax = $.ajax({
        url: API_BASE_URL + "/Batch/GetBatches",
        type: "Get",
        contentType: "application/json"
    });

    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/SalesQuotation/GetLookups",
        type: "Get",
        contentType: "application/json"
    });

    $.when(productsAjax,racksAjax,batchAjax,lookUpAjax)
    .done(function (dataProducts, dataRacks, dataBatches,dataLookUps) {
        
        products = dataProducts[2].responseJSON;
        stores = dataLookUps[2].responseJSON.Stores;
        racks = dataLookUps[2].responseJSON.Racks;
        batches = dataLookUps[2].responseJSON.Batches;
        units = dataLookUps[2].responseJSON.Units;
        //racks = dataRacks[2].responseJSON;
        //batches = dataBatches[2].responseJSON;

        $("#searchByProductName").html(Utilities.PopulateDropDownFromArray(products, 3, 3));
        $("#searchByBarcode").html(Utilities.PopulateDropDownFromArray(products, 2, 2));
        $("#searchByProductCode").html(Utilities.PopulateDropDownFromArray(products, 2, 2));
        $("#searchByProductName").chosen({ width: "100%" });
        $("#searchByProductCode").chosen({ width: "100%" });
        $("#searchByBarcode").chosen({ width: "100%" });

        $("#unit").html(Utilities.PopulateDropDownFromArray(units, 1, 2));
        $("#store").html(Utilities.PopulateDropDownFromArray(stores, 0, 1));
        //$("#batch").html(Utilities.PopulateDropDownFromArray(getProductBatches(searchedProduct.productId), 0, 1));
        //$("#rate").val(Utilities.FormatCurrency(searchedProduct.salesRate));

        Utilities.Loader.Hide();
    });
}

function searchProduct(searchBy, filter, productId) {
    Utilities.Loader.Show();
    var prodAjax=$.ajax({
        url: API_BASE_URL + "/ProductCreation/SearchProduct",
        type: 'GET',
        data: { searchBy: searchBy, filter: filter },
        contentType: "application/json"
    });

    var prodStoreAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProductStores",
        type: 'GET',
        data: { productId: productId },
        contentType: "application/json"
    });

    $.when(prodAjax, prodStoreAjax)
    .done(function (dataProd,dataProdStore) {
        searchedProduct = dataProd[2].responseJSON.Product[0];
       // stores = dataProdStore[2].responseJSON;

        //allStores = dataProd[2].responseJSON.Stores;
        //allRacks = dataProd[2].responseJSON.Racks;
        //allUnits = dataProd[2].responseJSON.Unit;
        //console.log("al u",allUnits);
        $("#searchByBarcode").val(searchedProduct.productCode);
        $("#searchByBarcode").trigger("chosen:updated");
        $("#searchByProductCode").val(searchedProduct.productCode);
        $("#searchByProductCode").trigger("chosen:updated");
        $("#searchByProductName").val(searchedProduct.productName);
        $("#searchByProductName").trigger("chosen:updated");
        $("#rate").val(Utilities.FormatCurrency(searchedProduct.salesRate));
        

        Utilities.Loader.Hide();
    });
}

function getStoreRacks(storeId)
{
    var output = [];
    for(i=0;i<racks.length;i++)
    {
        if (racks[i].GodownId == storeId)
        {
            output.push(racks[i]);
        }
    }
    return output;
}

function getProductBatches(productId)
{
    var output = [];
    for (i = 0; i < batches.length; i++)
    {
        if (batches[i].ProductId == productId)
        {
            output.push(batches[i]);
        }
    }
    return output;
}

function findProduct(productId)
{
    var output = {};
    for(i=0;i<products.length;i++)
    {
        if(products[i].ProductId==productId)
        {
            output = products[i];
            break;
        }
    }
    return output
}

function getStore(storeId)
{
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output
}

function getRack(rackId) {
    var output = {};
    for (i = 0; i < allRacks.length; i++) {
        if (allRacks[i].rackId == rackId) {
            output = allRacks[i];
            break;
        }
    }
    return output
}

function getUnit(unitId) {
    var output = {};
    for (i = 0; i < allUnits.length; i++) {
        if (allUnits[i].unitId == unitId) {
            output = allUnits[i];
            break;
        }
    }
    return output
}