﻿var itemToCreate = {};
var productId = 1;
var product = {};
var productIdToDelete = 0;
var table = "";
var isActive = false;
var isShowReminder = false;
var currentTab = "Main";
var bomTotalInventoryAmount = 0;


//lookup variables
var nextProductCode = "";
var modelNumbers = [];
var brands = [];
var sizes = [];
var racks = [];
var stores = [];
var taxes = [];
var units = [];
var salesAccounts = [];
var expenseAccounts = [];
var filteredRacks = [];
var groups = [];
var items = [];


var storesToAddItem = [];
var storeId = 1;
var newBomItemId = 1;
var newMultipleUnitId = 1;
var newBomItems = [];
var newMultipleUnits = [];
var allUnitConversionItems = [];

itemToCreate.ProductSp = {};
itemToCreate.ProductInfo = {};
itemToCreate.UnitConvertion = {};
itemToCreate.UnitConvertionInfo = {};
itemToCreate.NewStores = [];
itemToCreate.NewBoms = [];
itemToCreate.NewMultipleUnits = [];
itemToCreate.NewStockPostings = [];
itemToCreate.AutoBarcode = false;
itemToCreate.IsSaveBomCheck = false;
itemToCreate.IsSaveMultipleUnitCheck = false;
itemToCreate.IsOpeningStock = false;
itemToCreate.IsBatch = false;

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created

$(function () {

    getProduct();
    populateLookups();

    $("#btnPrevious").attr("disabled", "disabled");
    $("#btnSaveItem").hide();

    $("#productImage").change(function () {
        //alert("dd");
        readURL(this);
    });

    $('a[data-toggle="tab"]').on('shown.bs.tab', function (e) {
        currentTab = e.target.text;
        if (currentTab == "Main")
        {
            $("#btnPrevious").attr("disabled", "disabled");
            $("#btnNext").show();
            $("#btnSaveItem").hide();
        }
        else if (currentTab == "Secondary")
        {
            $("#btnPrevious").removeAttr("disabled");
            $("#btnNext").show();
            $("#btnSaveItem").hide();
        }
        else if (currentTab == "Others")
        {
            $("#btnPrevious").removeAttr("disabled");
            $("#btnNext").hide();
            $("#btnSaveItem").show();
        }
    })

    $("#btnNext").click(function ()
    {
        
        if (currentTab == "Main")
        {
            $('.nav-tabs a[href="#tab2' + '"]').tab('show');
        }
        else if (currentTab == "Secondary")
        {
            $('.nav-tabs a[href="#tab3' + '"]').tab('show');
        }

    });

    $("#btnPrevious").click(function () {

        if (currentTab == "Others") {
            $('.nav-tabs a[href="#tab2' + '"]').tab('show');
        }
        else if (currentTab == "Secondary") {
            $('.nav-tabs a[href="#tab1' + '"]').tab('show');
        }

    });
    
    $('#newStore').on('change', function () {
        findStoreRacks($(this).val());
    });

    $('#store').on('change', function () {

        var filteredRacks2 = []; //empty the array each time function is called
        var chk = $(this).val();
        $.each(racks, function (i, record) {
            if (record.GodownId == chk) {
                filteredRacks2.push(record);
            }
        });
        var rackHtml2 = "";
        $.each(filteredRacks2, function (i, record) {
            rackHtml2 += '<option value="' + record.RackId + '">' + record.RackName + '</option>';
        });
        $("#rack").html(rackHtml2);
    });

    $("#newRate").change(function () {
        getNewStoreItemAmount();
    });

    $("#newQuantity").change(function () {
        getNewStoreItemAmount();
    });

    $("#addStore").click(function () {
        
        var amount = $("#newRate").val() * $("#newQuantity").val();
        storesToAddItem.push({
            id: (storeId++),
            storeId: $("#newStore").val(),
            store: $("#newStore option:selected").text(),
            rackId: $("#newRack").val(),
            rack: $("#newRack option:selected").text(),
            batch: $("#newBatch").val(),
            mfD: $("#newMf").val(),
            expD: $("#newExp").val(),
            quantity: $("#newQuantity").val(),
            rate: $("#newRate").val(),
            unit: $("#newUnit option:selected").text(),
            unitId: $("#newUnit").val(),
            amount: amount
        });
        $("#amount").val(amount);
        renderStoresToAddItemsTo();
        console.log(storesToAddItem); 
        //reset control
        $("#newMf").val("");
        $("#newExp").val("");
        $("#newQuantity").val("");
        $("#newRate").val("");
        $("#amount").val("");


    });

    $('#itemType').on('change', function () {
        var val = this.value;
        
        switch (val)
        {
            case "Product":
                //$("#openingStock").attr('disabled', 'disabled');
                $("#lblSalesRate").html("Sales Rate");
                $("#openingStock").removeAttr('disabled');
                $("#addModal").removeAttr('disabled');
                $("#minimumStock").removeAttr('disabled');
                $("#maximumStock").removeAttr('disabled');
                $("#reorderLevel").removeAttr('disabled');
                $("#store").removeAttr('disabled');
                $("#rack").removeAttr('disabled');
                $("#mrp").removeAttr('disabled');
                $("#brand").removeAttr('disabled');
                $("#bom").removeAttr('disabled');
                $("#allowBatch").removeAttr('disabled');
                $("#multipleUnit").removeAttr('disabled');
                break;
            case "Service":
                $("#lblSalesRate").html("Service Rate");
                $("#openingStock").attr('disabled', 'disabled');
                $("#addModal").attr('disabled', 'disabled');
                $("#minimumStock").attr('disabled', 'disabled');
                $("#maximumStock").attr('disabled', 'disabled');
                $("#reorderLevel").attr('disabled', 'disabled');
                $("#store").attr('disabled', 'disabled');
                $("#rack").attr('disabled', 'disabled');
                $("#brand").attr('disabled', 'disabled');
                $("#mrp").attr('disabled', 'disabled');
                $("#bom").attr('disabled', 'disabled');
                $("#allowBatch").attr('disabled', 'disabled');
                $("#multipleUnit").attr('disabled', 'disabled');
                break;
        }
    })

    $('#tax').on('change',
        function() {
            var val = this.value;
            if (val == "1") {
                $("#taxApplicable").attr('disabled', 'disabled');
            } else {
                $("#taxApplicable").removeAttr('disabled');
            }
        });

    $('#unit').on('change',
        function () {
            var unitVal = $("#unit").val();
            //$("#bomUnit").val(unitVal);
            $("#newUnit").val(unitVal);
            //renderNewBomItems();
        });


    $('#chkAutoBarcode').change(function () {
        if (this.checked)
        {
            $("#txtAutoBarcode").hide();
            itemToCreate.AutoBarcode = true;
        }
        else
        {
            $("#txtAutoBarcode").show();
            itemToCreate.AutoBarcode = false;
        }
    });

    $('#chkActive').change(function () {
        if (this.checked) {
            isActive = true;
        }
        else {
            isActive = false;
        }
    });

    $('#chkReminder').change(function () {
        if (this.checked) {
            isShowReminder = true;
        }
        else {
            isShowReminder = false;
        }
    });
    
    $('#allowBatch').on('change', function () {
        if (this.value == "True") {
            $(".hideBatch").css("display", "");
            itemToCreate.IsBatch = true;
            //warn before reseting 
            if (storesToAddItem.length > 0) {
                //warn user'

                Utilities.ConfirmFancy(
                    "You Have some pending record, Selecting this will reset your entries\n Do You Want to Reset It?",
                    function () {
                        storesToAddItem = [];
                        itemToCreate.NewStores = [];

                        $("#totalInventoryValue").val(getTotalInventoryValue());
                        $("#tbody").html("");
                        $("#newTbody").html("");
                        $("#amount").val(0);
                    }, function () {
                        // set dropdown value to False and reset to normal 
                        $('#allowBatch').val("False");
                        $(".hideBatch").css("display", "none");
                        itemToCreate.IsBatch = false;
                        $("#newBatch").val("");
                        $("#newMf").val("");
                        $("#newExp").val("");
                    });




                //reset value


            }
            else {
                $(".hideBatch").css("display", "none");
                itemToCreate.IsBatch = false;
                $("#newBatch").val("");
                $("#newMf").val("");
                $("#newExp").val("");
            }
        }
    });

    $('#bom').on('change', function () {
        if (this.value == "True")
        {
            itemToCreate.IsSaveBomCheck = true;
            $("#bomModal").modal("toggle");
        }
        else
        {
            itemToCreate.IsSaveBomCheck = false;
            //when its false, clear the bom object and also reset bom modal form
            itemToCreate.NewBoms = [];
            //call function to reset modal
        }
    });

    $('#multipleUnit').on('change', function () {
        if ($(this).val() == "True") {
            itemToCreate.IsSaveMultipleUnitCheck = true;
            $("#primaryUnit").val($("#unit option:selected").text());
            $("#newUnitModal").modal();
        }
        else {
            itemToCreate.IsSaveMultipleUnitCheck = false;
            //when its false, clear the bom object and also reset bom modal form
            itemToCreate.NewMultipleUnits = [];
        }
    });

    $('#openingStock').change(function () {
        var chk = $('#openingStock').val();
        if(chk=="True")
        {
            $("#openingStockStorePanel").css("display", "block");
            itemToCreate.IsOpeningStock = true;
        }
        else if (chk == "False")
        {
            $("#openingStockStorePanel").css("display", "none");
            itemToCreate.IsOpeningStock = false;
        }
    });
    var imageName = "";
    document.getElementById('productImage').onchange = function () {
        //alert('Selected file: ' + this.value.replace(/C:\\fakepath\\/i, ''));
        imageName = this.value.replace(/C:\\fakepath\\/i, '');
    };
    $("#btnSaveItem").click(function () {
        //var filename = $('input[type=file]').val().replace(/C:\\fakepath\\/i, '')
        
        //var a = $("#productImage").val().split("/");
        //console.log(a); return;
        console.log("min" + $("#minimumStock").val());
        console.log("max" + $("#maximumStock").val());
        console.log("reord" + $("#reorderLevel").val());
        console.log($("#minimumStock").val() > $("#maximumStock").val());
        //return;
        if ($("#productCode").val() == ""){
            Utilities.ErrorNotification("Item code required!");
            return;
        }
        if ($("#productName").val() == ""){
            Utilities.ErrorNotification("Item name required!");
            return;
        }
        if (($("#purchaseRate").val() == "" || Number($("#purchaseRate").val()) == 0) && $("#bom").val() == "False") {
            Utilities.ErrorNotification("Purchase rate required!");
            return;
        }
        if ($("#salesRate").val() == "" || Number($("#salesRate").val()) < 0) {
            Utilities.ErrorNotification("Sales rate required!");
            return;
        }
        if ($("#salesAccount").val() == "" || Number($("#salesAccount").val()) < 0) {
            Utilities.ErrorNotification("Income Account Required!");
            return;
        }
        if ($("#effectiveDate").val() == "" || Number($("#effectiveDate").val()) < 0) {
            Utilities.ErrorNotification("Choose Effective Date!");
            return;
        }
        if (parseFloat($("#minimumStock").val()) > parseFloat($("#maximumStock").val()))
        {
            Utilities.ErrorNotification("Minimum stock can't be greater than maximum stock!");
            return;
        }
        if ($("#unit").val() == "") {
            Utilities.ErrorNotification("Unit required!");
            return;
        }
        //if ($("#mrp").val() == "") {
        //    Utilities.ErrorNotification("MRP required!");
        //    return;
        //}
        //if ($("#partNumber").val() == "") {
        //    Utilities.ErrorNotification("Part number required!");
        //    return;
        //}
        if ($("#salesAccount").val() == "") {
            Utilities.ErrorNotification("Select Sales Account");
            return;
        }
        if ($("#expenseAccount").val() == "") {
            Utilities.ErrorNotification("Select Expenses Account");
            return;
        }
        if ($("#effectiveDate").val() == "") {
            Utilities.ErrorNotification("Date required!");
            return;
        }
        if (itemToCreate.AutoBarcode = false && $("#txtAutoBarcode").val() == "")
        {
            Utilities.ErrorNotification("Barcode required!");
            return;
        }
        else
        {
            var purchaseRate = ($("#purchaseRate").val() == "" || Number($("#purchaseRate").val()) == 0) && $("#bom").val() == "True" ? bomTotalInventoryAmount : $("#purchaseRate").val();
            itemToCreate.ProductInfo =
            {
            ProductName: $("#productName").val(),
            ProductCode: $("#productCode").val(),
            PurchaseRate: purchaseRate,
            SalesRate: $("#salesRate").val(),
            Mrp: $("#mrp").val()=="" ?"NULL" : $("#mrp").val(),
            MaximumStock: $("#maximumStock").val(),
            MinimumStock: $("#minimumStock").val(),
            ReorderLevel: $("#reorderLevel").val(),
            TaxId: $("#tax").val(),
            UnitId: $("#unit").val(),
            GroupId: $("#group").val(),
            ProductType: $("#itemType").val(),
            SalesAccount: $("#salesAccount").val(),
            EffectiveDate: $("#effectiveDate").val(),
            ExpenseAccount: $("#expenseAccount").val(),
            TaxapplicableOn: $("#taxApplicable").val(),
            BrandId: $("#brand").val(),
            SizeId: $("#size").val(),
            ModelNoId: $("#modelNo").val(),
            GodownId: $("#store").val(),
            RackId: $("#rack").val(),
            IsallowBatch: $("#allowBatch").val(),
            IsBom: $("#bom").val(),
            PartNo: $("#partNumber").val() == "" ? "0" : $("#partNumber").val(),
            Isopeningstock: $("#openingStock").val(),
            Ismultipleunit: $("#multipleUnit").val(),
            IsActive: isActive,
            Extra1: imageName,
            Extra2: $("#manufacturer").val(),
            IsshowRemember: isShowReminder,   //isShowReminder
            Narration:$("#description").val()
        };
            if (!itemToCreate.AutoBarcode) {
                itemToCreate.ProductInfo.barcode = $("#txtAutoBarcode").val();
            }
            else {
                itemToCreate.ProductInfo.barCode = "";
            }
            itemToCreate.UnitConvertionInfo =
            {
                UnitId: ""
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/ProductCreation/AddProduct",
                type: 'POST',
                data: JSON.stringify([itemToCreate]),
                contentType: "application/json",
                success: function (data) {
                    console.log(data);
                    hideLoader();
                    if (data.ResponseCode == 200) {
                        $("#returnedProductId").val(data);
                        $("#frmProductImage").submit();
                        Utilities.SuccessNotification(data.ResponseMessage);
                        //$("#successModal").modal("toggle");
                        //countDown("cd", 3);
                    }
                    else if (data.ResponseCode == 300) {
                        Utilities.ErrorNotification(data.ResponseMessage);
                        //$("#errorModal").modal("toggle");
                    }
                    else {
                        Utilities.ErrorNotification("Sorry, something went wrong!");
                        //$("#errorModal").modal("toggle");
                    }
                },
                error: function (request, error) {
                    Utilities.ErrorNotification("Sorry, something went wrong!");
                    hideLoader();
                }
            });
        }        
    });

    $("#btnAddBomItem").click(function () {
        newBomItems.push({
            id:(newBomItemId++),
            RawMaterialId: $("#bomRawMaterial").val(),
            RawMaterial: $("#bomRawMaterial option:selected").text(),
            UnitId: $("#bomUnit").val(),
            Unit: $("#bomUnit option:selected").text(),
            Quantity: $("#bomQuantity").val(),
        });
        //reset control
        $("#bomQuantity").val(0);
        renderNewBomItems();
    });
    
    $('#bomModal').on('hide.bs.modal', function (e) {
        if (itemToCreate.NewBoms == null || itemToCreate.NewBoms == "") {
            var chk = confirm("BOM inventory is empty. Do you want to close?");
            if (chk) {
                $("#bom").val("False");
                $("#bom option:selected").text("No");
            }
            else {
                e.preventDefault();
            }
        }
    });

    $('#newUnitModal').on('hide.bs.modal', function (e) {
        if (itemToCreate.NewMultipleUnits == null || itemToCreate.NewMultipleUnits == "") {
            var chk = confirm("Unit item is empty. Do you want to close?");
            if (chk)
            {
                $("#multipleUnit").val("False");
                $("#multipleUnit option:selected").text("No");
            }
            else
            {
                e.preventDefault();
            }
        }
    });
    $("#measuredQuantity").on("change", function() {
        var qty = $("#measuredQuantity").val();
        if (qty <= 0) {
            $("#measuredQuantity").css("border", "1px solid red");
        } else {
            $("#measuredQuantity").css("border", "1px solid black");
        }
    })
    $("#primaryUnitQuantity").on("change", function () {
        var qty = $("#primaryUnitQuantity").val();
        if(qty <= 0) {
            $("#primaryUnitQuantity").css("border", "1px solid red");
    } else {
            $("#primaryUnitQuantity").css("border", "1px solid black");
    }
    })
    $("#btnAddMutipleUnit").click(function () {

        //newMultipleUnits.push({
        //    Id:newMultipleUnits.length + 1,
        //    //q1:$("#q1").val(),
        //    //q2:$("#q2").val(),
        //    //ConversionRate: "",
        //    UnitId: $("#derivedUnit").val(),
        //    Unit:$("#derivedUnit option:selected").text(),
        //    Quantities :$("#measuredQuantity").val()
        //});  
        var measuredQty = $("#measuredQuantity").val();
        var primaryUnitQty = $("#primaryUnitQuantity").val();
        if (primaryUnitQty <= 0) {
            Utilities.Alert("Primary Quantity must be Greater than Zero");
            $("#primaryUnitQuantity").focus();
            $("#primaryUnitQuantity").css("border", "1px solid red");
        }
        else if (measuredQty <= 0) {
            Utilities.Alert("Derived Quantity must be Greater than Zero");
            $("#measuredQuantity").focus();
            $("#measuredQuantity").css("border", "1px solid red");

        }
        else {

      
        var newMultipleUnitHtml = `<tr>
                            <td>${itemToCreate.NewMultipleUnits.length + 1} </td>
                            <td>${$("#derivedUnit option:selected").text()}</td>
                            <td>${$("#measuredQuantity").val()}</td>
                            <td>${$("#primaryUnitQuantity").val()}</td>
                            <td>
                                <button class="btn btn-danger" onclick="removeMultipleUnitItem(${itemToCreate.NewMultipleUnits.length})" data-id = "${itemToCreate.NewMultipleUnits.length + 1}"><i class ="fa fa-times"></i></button>
                            </td>
                          
                          </tr>`;
            itemToCreate.NewMultipleUnits.push({
                Sn: itemToCreate.NewMultipleUnits.length + 1,
                ConversionRate: Number($("#primaryUnitQuantity").val()) / Number($("#measuredQuantity").val()),
                UnitId: $("#derivedUnit").val(),
                Quantities:$("#primaryUnitQuantity").val() + "-" + $("#measuredQuantity").val()
            });
             newMultipleUnits.push({
                Sn: itemToCreate.NewMultipleUnits.length +1,
                ConversionRate: Number($("#primaryUnitQuantity").val()) / Number($("#measuredQuantity").val()),
                UnitId : $("#derivedUnit").val(),
                Quantities: $("#primaryUnitQuantity").val() + "-" + $("#measuredQuantity").val(),
                PrimaryUnitQty: $("#primaryUnitQuantity").val(),
                MeasuredQty: $("#measuredQuantity").val()
                        });
            $("#mUnitTbody").append(newMultipleUnitHtml);
            $("#measuredQuantity").val(0);
            $("#primaryUnitQuantity").val(0);
        }
    });
  

    $("#btnAddNewBrand").click(function () {
        var brandToSave = {
            BrandName: $("#newBrandName").val(),
            Narration: "",
            Extra1: "",
            Extra2: "",
            Manufacturer: $("#newManufacturer").val()
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/Brand/AddBrand",
            type: 'POST',
            data: JSON.stringify(brandToSave),
            contentType: "application/json",
            success: function (data) {
                
                if(data!=null || data!="")
                {
                    $.ajax({
                        url: API_BASE_URL + "/Brand/GetBrands",
                        type: 'GET',
                        contentType: "application/json",
                        success: function (data) {
                            var output = "";
                            $.each(data, function (i, record) {
                                output += '<option value="' + record.BrandId + '">' + record.BrandName + '</option>';
                            });
                            $("#brand").html(output);
                            $("#newBrandName").val("");
                            $("#newManufacturer").val("");
                            alert("Data saved!");
                            hideLoader();
                        },
                        error: function (request, error) {
                            hideLoader();
                        }
                    });
                }
                else
                {
                    hideLoader();
                    alert("Oops! Sorry, something went wrong.");
                }
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    });

    $("#btnAddNewUnit").click(function () {
        var unitToSave = {
            UnitName: $("#newUnit").val(),
            noOfDecimalplaces: $("#newDp").val()
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/Unit/AddUnit",
            type: 'POST',
            data: JSON.stringify(unitToSave),
            contentType: "application/json",
            success: function (data) {
                if(data!=null || data!="")
                {
                    $.ajax({
                        url: API_BASE_URL + "/Unit/GetUnits",
                        type: 'GET',
                        contentType: "application/json",
                        success: function (data) {
                            var unitHtml = "";
                            $.each(units, function (i, record) {
                                unitHtml += '<option value="' + record.UnitId + '">' + record.UnitName + '</option>';
                            });
                            $("#unit").html(unitHtml);
                            $("#newUnit").html(unitHtml);
                            $("#bomUnit").html(unitHtml);
                            $("#newMultipleUnit").html(unitHtml);

                            $("#newUnit").val(0);
                            $("#newDp").val("");
                            hideLoader();
                        },
                        error: function (request, error) {
                            hideLoader();
                        }
                    });
                }
                else
                {
                    hideLoader();
                    alert("Oops! Sorry, something went wrong.");
                }
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    });
});

function getNewStoreItemAmount()
{
    var rate = $("#newRate").val();
    var qty = $("#newQuantity").val();
    var amt = (rate * qty).toFixed(2);
    $("#amount").val(amt);
}

function getProduct() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProduct?productId="+productId,
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            product = data;
            console.log(product);
                  
            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function findItem(id) {
    var item = "";
    for (i = 0; i < items.length; i++) {
        if (items[i].ProductId == id) {
            item = items[i];
            break;
        }
    }
    return item;
}

function findUnit(id) {
    var unit = "";
    for (i = 0; i < units.length; i++) {
        if (units[i].UnitId == id) {
            unit = units[i];
            break;
        }
    }
    return unit;
}

function populateLookups()
{
    Utilities.Loader.Show();
    var productCodeAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetNextProductCode',
        type: 'Get'
    });

    var modelNumberAjax = $.ajax({
        url: API_BASE_URL + '/ModelNumber/GetModelNos',
        type: 'Get'
    });

    var brandAjax = $.ajax({
        url: API_BASE_URL + '/Brand/GetBrands',
        type: 'Get'
    });

    var sizeAjax = $.ajax({
        url: API_BASE_URL + '/Size/GetSizes',
        type: 'Get'
    });

    var rackAjax = $.ajax({
        url: API_BASE_URL + '/Rack/GetRacks',
        type: 'Get'
    });

    var storeAjax = $.ajax({
        url: API_BASE_URL + '/Store/GetStores',
        type: 'Get'
    });

    var taxAjax = $.ajax({
        url: API_BASE_URL + '/Tax/GetTaxes',
        type: 'Get'
    });

    var unitAjax = $.ajax({
        url: API_BASE_URL + '/Unit/GetUnits',
        type: 'Get'
    });

    var salesAccountAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetSalesAccounts',
        type: 'Get'
    });

    var expenseAccountAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetExpenseAccount',
        type: 'Get'
    });

    var groupAjax = $.ajax({
        url: API_BASE_URL + '/ItemGroup/GetProductGroups',
        type: 'Get'
    });

    var itemsAjax = $.ajax({
        url: API_BASE_URL + '/ProductCreation/GetProducts',
        type: 'Get'
    });

    var unitConversionLookupAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetUnitConversionItems",
        type: "GET",
        contentType: "application/json",
    });

    $.when(productCodeAjax, modelNumberAjax, brandAjax, sizeAjax, rackAjax, storeAjax, taxAjax, unitAjax, salesAccountAjax, expenseAccountAjax, groupAjax, itemsAjax, unitConversionLookupAjax)
        .done(function (dataProductCode, dataModelNumber, dataBrand, dataSize, dataRack, dataStore, dataTax, dataUnit, dataSalesAccount, dataExpenseAccount, dataGroup, dataItems, dataUnitConversion) {

            nextProductCode = dataProductCode[2].responseJSON;
            modelNumbers = dataModelNumber[2].responseJSON;
            brands = dataBrand[2].responseJSON;
            sizes = dataSize[2].responseJSON;
            racks = dataRack[2].responseJSON;
            stores = dataStore[2].responseJSON;
            taxes = dataTax[2].responseJSON;
            units = dataUnit[2].responseJSON;
            salesAccounts = dataSalesAccount[2].responseJSON;
            expenseAccounts = dataExpenseAccount[2].responseJSON;
            groups = dataGroup[2].responseJSON;
            items = dataItems[2].responseJSON;
            allUnitConversionItems = dataUnitConversion[0].UnitCoversionItems;
            populateControls();
            Utilities.Loader.Hide();
        }
	);
}

function populateControls()
{
    $("#productCode").val(nextProductCode);

    var modelNoHtml = "";
    $.each(modelNumbers, function (i,record) {
        modelNoHtml+='<option value="'+record.ModelNoId+'">'+record.ModelNo+'</option>';
    });
    $("#modelNo").html(modelNoHtml);

    var brandHtml = "";
    //$.each(brands, function (i, record) {
    //    brandHtml += '<option value="' + record.BrandId + '">' + record.BrandName + '</option>';
    //});
    //$("#brand").html(brandHtml);
    $("#brand").kendoDropDownList({
        filter: "contains",
        dataTextField: "BrandName",
        dataValueField: "BrandId",
        dataSource: brands
    });

    var sizeHtml = "";
    $.each(sizes, function (i, record) {
        sizeHtml += '<option value="' + record.SizeId + '">' + record.Size + '</option>';
    });
    $("#size").html(sizeHtml);

    $("#size").kendoDropDownList({
        filter: "contains",
        optionLabel: "Please Select...",
        dataTextField: "Size",
        dataValueField: "SizeId",
        dataSource: sizes
    });

    var rackHtml = "";
    $.each(racks, function (i, record) {
       // rackHtml += '<option value="' + record.RackId + '">' + record.RackName + '</option>';
    });
    $("#rack").html(rackHtml);

    var storeHtml = "";
    $.each(stores, function (i, record) {
        storeHtml += '<option value="' + record.GodownId + '">' + ""+record.GodownName+"" + '</option>';
    });
    $("#store").html(storeHtml);
    $("#newStore").html(storeHtml);

    var taxHtml = "";
    $.each(taxes, function (i, record) {
        taxHtml += '<option value="' + record.TaxId + '">' + record.TaxName + '</option>';
    });
    $("#tax").html(taxHtml);

    var unitHtml = "";
    $.each(units, function (i, record) {
        unitHtml += '<option value="' + record.UnitId + '">' + record.UnitName + '</option>';
    });

    $("#unit").html(unitHtml);
    $("#derivedUnit").html(unitHtml);
    $("#newUnit").html(unitHtml);
    $("#bomUnit").html(unitHtml);
    $("#newMultipleUnit").html(unitHtml);

    var salesAccountHtml = "";
    $.each(salesAccounts, function (i, record) {
      //  if(record.LedgerId == 10)
       // {
            salesAccountHtml += '<option value="' + record.LedgerId + '">' + record.LedgerName + '</option>';
            return;
        //}
    });
    $("#salesAccount").html(salesAccountHtml);

    $("#salesAccount").kendoDropDownList({
        filter: "contains",
        optionLabel: "Please Select...",
        dataTextField: "LedgerName",
        dataValueField: "LedgerId",
        dataSource: salesAccounts
    });

    var expenseAccountHtml = "";
    $.each(expenseAccounts, function (i, record) {
    //    if (record.LedgerId == 58)
    //    {
            expenseAccountHtml += '<option value="' + record.LedgerId + '">' + record.LedgerName + '</option>';
            return;
      //  }
    });
    $("#expenseAccount").html(expenseAccountHtml);

    $("#expenseAccount").kendoDropDownList({
        filter: "contains",
        optionLabel: "Please Select...",
        dataTextField: "LedgerName",
        dataValueField: "LedgerId",
        dataSource: expenseAccounts
    });


    var groupHtml = "";
    var filteredGroupObj = [];
    $.each(groups, function (i, record) {
        var upperLevelGroup = "";
        var currentGroup = {};
        for (i = 0; i < groups.length; i++)
        {
            if(groups[i].GroupId==record.GroupUnder)
            {
                currentGroup = groups[i];
                break;
            }
        }
        if (currentGroup.GroupUnder == 0)
        {
            upperLevelGroup = "Primary";
        }
        else
        {
            upperLevelGroup = currentGroup.GroupName;
        }
        if (record.Extra1 == "Sub Category")
        {
            //groupHtml += '<option value="' + record.GroupId + '">' + record.GroupName + ' : ' + upperLevelGroup + '</option>';
            var lv3=findUpperLevelByGroupLevel(groups, upperLevelGroup);
            var lv2 = findUpperLevelByGroupLevel(groups, lv3);
            console.log(lv2);
            filteredGroupObj.push({
                GroupId: record.GroupId,
                GroupName: record.GroupName,
                CustomDisplay: record.GroupName + " : " + upperLevelGroup + " : " + lv3+" : "+lv2
            });
        }
    });
    //$("#group").html(groupHtml);
    //console.log(filteredGroupObj);
    $("#group").kendoDropDownList({
        filter: "contains",
        dataTextField: "CustomDisplay",
        dataValueField: "GroupId",
        dataSource: filteredGroupObj
    });

    var itemsHtml = "";
    $.each(items, function (i, record) {
        itemsHtml += '<option value="' + record.ProductId + '">' + record.ProductName + '</option>';
    });
    $("#bomRawMaterial").html(itemsHtml);

    /*$("#bomRawMaterial").kendoDropDownList({
        filter: "contains",
        //optionLabel: "Please Select...",
        dataTextField: "ProductName",
        dataValueField: "ProductId",
        dataSource: items,
    });
    console.log('items: ', items);*/

    $("#bomRawMaterial").selectize();
}

$("#bomRawMaterial").change(function () {
    var productId = $("#bomRawMaterial").val();
    var unitHtml = "";
    var unitsItemsOfProduct = allUnitConversionItems.filter(p => p.productId == productId);
    for (var i of unitsItemsOfProduct) {
        var unitName = units.find(p => p.UnitId == i.unitId).UnitName
        unitHtml += `<option value="${i.unitId}">${unitName}</option>`;
    }
    $("#bomUnit").html(unitHtml);
});

    function findUpperLevelByGroupLevel(groups, groupNameToFind) {
    var obj = groups.find(p=>p.GroupName == groupNameToFind);
    var groupUnderName = groups.find(p=>p.GroupId == obj.GroupUnder);
    if (groupUnderName == null || groupUnderName == "") {
        return "N/A";
    }
    return groupUnderName.GroupName;
}

    function removeStore(id) {
    var indexOfObjectToRemove =storesToAddItem.findIndex(p=>p.id ==id);
    storesToAddItem.splice(indexOfObjectToRemove, 1);
    console.log(storesToAddItem);
    renderStoresToAddItemsTo();
}

    function renderStoresToAddItemsTo() {
    var storesHtml = "";
    var NstoresHtml = "";
    itemToCreate.NewStores = [];
    $.each(storesToAddItem, function (i, record) {
        if (itemToCreate.IsBatch == true) {
            //storesHtml += '<div class="row">\
            //                <div class="col-md-1">' + (i + 1) + '</div>\
            //                <div class="col-md-1">' + record.store + '</div>\
            //                <div class="col-md-1">' + record.rack + '</div>\
            //                <div class="col-md-1 hideBatch">' + record.batch + '</div>\
            //                <div class="col-md-1 hideBatch">' + shrinkDate(record.mfD) + '</div>\
            //                <div class="col-md-1 hideBatch">' + shrinkDate(record.expD) + '</div>\
            //                <div class="col-md-1">' + record.quantity + '</div>\
            //                <div class="col-md-1">' + record.rate + '</div>\
            //                <div class="col-md-1">' + record.unit + '</div>\
            //                <div class="col-md-1">' + record.amount + '</div>\
            //                <div class="col-md-1"><button class="btn btn-danger" onclick="removeStore(' + record.id + ')"><i class="fa fa-times"></i></button></div>\
            //           </div>';
            NstoresHtml += ` <tr>
                                    <td ><b>${(i + 1) }</b></td>
                                    <td><b>${record.store}</b></td>
                                    <td ><b>${record.rack}</b></td>
                                    <td class=" hideBatch" style="display: ;"><b>${record.batch}</b></td>
                                    <td class=" hideBatch" style="display: ;"><b>${shrinkDate(record.mfD) }</b></td>
                                    <td class="hideBatch" style="display: ;"><b>${shrinkDate(record.expD) }</b></td>
                                    <td class=""><b>${record.quantity}</b></td>
                                    <td class=""><b>${record.rate}</b></td>
                                    <td class=""><b>${record.unit}</b></td>
                                    <td class=""><b>${record.amount}</b></td>
                                    <td class=""><button class="btn btn-danger" onclick="removeStore(${record.id})"><i class="fa fa-times"></i></button></td>
                                </tr>
                            `;
            itemToCreate.NewStores.push({
                    StoreId: record.storeId,
                    RackId: record.rackId,
                    Batch: record.batch,
                    MfD: record.mfD,
                    ExpD: record.expD,
                    Quantity: record.quantity,
                    Rate: record.rate,
                    UnitId: record.unitId,
                    Amount: record.amount
            });
        }
        else if (itemToCreate.IsBatch ==false) {
            //storesHtml += '<div class="row">\
            //                <div class="col-md-1">' + (i + 1) + '</div>\
            //                <div class="col-md-1">' + record.store + '</div>\
            //                <div class="col-md-1">' + record.rack + '</div>\
            //                <div class="col-md-1">' + record.quantity + '</div>\
            //                <div class="col-md-1">' + record.rate + '</div>\
            //                <div class="col-md-1">' + record.unit + '</div>\
            //                <div class="col-md-1">' + record.amount + '</div>\
            //                <div class="col-md-1"><button class="btn btn-danger" onclick="removeStore(' + record.id + ')"><i class="fa fa-times"></i></button></div>\
            //           </div>';
            NstoresHtml += ` <tr>
                                    <td ><b>${(i + 1) }</b></td>
                                    <td><b>${record.store}</b></td>
                                    <td ><b>${record.rack}</b></td>

                                    <td class=""><b>${record.quantity}</b></td>
                                    <td class=""><b>${record.rate}</b></td>
                                    <td class=""><b>${record.unit}</b></td>
                                    <td class=""><b>${record.amount}</b></td>
                                    <td class=""><button class="btn btn-danger" onclick="removeStore(${record.id})"><i class="fa fa-times"></i></button></td>
                                </tr>
                            `;
            itemToCreate.NewStores.push({
                    StoreId: record.storeId,
                    RackId: record.rackId,
                    Quantity: record.quantity,
                    Rate: record.rate,
                    UnitId: record.unitId,
                    Amount: record.amount
            });
    }

    });
    $("#tbody").html(storesHtml);
    $("#newTbody").html(NstoresHtml);
    $("#totalInventoryValue").val(getTotalInventoryValue());
}

    function findStoreRacks(storeId) {
    filteredRacks = []; //empty the array each time function is called
    $.each(racks, function (i, record) {
        if (record.GodownId ==storeId) {
            filteredRacks.push(record);
    }
    });

    var newRackHtml = "";
    $.each(filteredRacks, function (i, record) {
        newRackHtml += '<option value="' + record.RackId + '">' + record.RackName + '</option>';
    });
    $("#newRack").html(newRackHtml);
}

    function getTotalInventoryValue() {
    var total = 0.00;
    $.each(storesToAddItem, function (i, record) {
        total += record.amount;
    });
    return total;
}

    function shrinkDate(input) {
    var val = input.split("/");
    var year =val[2].substring(2, 4);
    return val[1] + "/" + val[0] + "/" +year;
}

    function renderNewBomItems() {
    var newBomItemsHtml = "";
    itemToCreate.NewBoms = [];
    $.each(newBomItems, function (i, record) {
        var product = findItem(record.RawMaterialId);
        var conversionRate = allUnitConversionItems.find(p => p.productId == product.ProductId && p.unitId == record.UnitId).conversionRate;
        var purchaseRate = product.PurchaseRate / conversionRate;
        var amount = purchaseRate * record.Quantity;
        newBomItemsHtml += '<tr>\
                            <td>' + (i + 1) + '</td>\
                            <td>' + record.RawMaterial + '</td>\
                            <td>' + record.Quantity + '</td>\
                            <td>' + /*$("#unit option:selected").text() */record.Unit + '</td>\
                            <td>' + Utilities.FormatCurrency(purchaseRate) + '</td>\
                            <td>' + Utilities.FormatCurrency(amount) + '</td>\
                            <td><button class="btn btn-danger" onclick="removeBomItem(' + record.id + ')"><i class="fa fa-times"></i></button></td>\
                          </tr>';
        itemToCreate.NewBoms.push({
                id: record.id,
                RawMaterialId: record.RawMaterialId,
                UnitId: record.UnitId, //$("#unit").val(),
                Quantity: record.Quantity,
                Amount: amount
        });
    });
    bomTotalInventoryAmount = getBomTotalInventoryValue();
    $("#bomTbody").html(newBomItemsHtml);
    $("#bomTotalInventoryAmount").val(Utilities.FormatCurrency(bomTotalInventoryAmount));
}

    function getBomTotalInventoryValue() {
    var total = 0.00;
    $.each(itemToCreate.NewBoms, function (i, record) {
        total += record.Amount;
    });
    return total;
}

    function removeBomItem(id) {
    var indexOfObjectToRemove = newBomItems.findIndex(p=>p.id == id);
    newBomItems.splice(indexOfObjectToRemove, 1);
    renderNewBomItems();
}

    function renderNewMultipleUnit() {

    var newMultipleUnitHtml = "";
    itemToCreate.NewMultipleUnits = [];
    $.each(newMultipleUnits, function (i, record) {
        newMultipleUnitHtml += '<tr>\
                            <td>' + (i + 1) + '</td>\
                            <td>' + findUnit(record.UnitId).UnitName + '</td>\
                            <td>' + record.MeasuredQty + '</td>\
                            <td>' + record.PrimaryUnitQty + '</td>\
                            <td><button class="btn btn-danger" onclick="removeMultipleUnitItem(' + i + ')"><i class="fa fa-times"></i></button></td>\
                          </tr>';
        itemToCreate.NewMultipleUnits.push({
                ConversionRate: record.ConversionRate,
                UnitId: record.UnitId,
                Quantities: record.Quantities
        });
    });
    $("#mUnitTbody").html(newMultipleUnitHtml);
}

    function removeMultipleUnitItem(id) {
    //var indexOfObjectToRemove = newMultipleUnits[id];
    newMultipleUnits.splice(id, 1);
    renderNewMultipleUnit();
}

    function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#blah').attr('src', e.target.result);
    }

        reader.readAsDataURL(input.files[0]);
    }
}