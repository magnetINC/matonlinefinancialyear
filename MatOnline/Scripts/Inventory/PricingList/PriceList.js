﻿var productGroups = [];
var products = [];
var pricingLevels = [];
var units = [];
var batches = [];
var productsForPriceList = [];
var priceLists = [];

var lookUps = {}
var PriceListSearchParam = {};
var thisProductForPriceList = {};
var newPrice = {};
var thisPriceList = {};
var priceListForEdit = {};

var table = "";
var table1 = "";
var pricingLevelId = 0;

$(function () {
    $("#successNotification").hide();
    $("#errorNotification").hide();
    $("#edit").hide();

    getLookUps();
    getProductsForPriceList(0, "", 0);
});

/* ========== API CALLS ============ */
function getLookUps()
{
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/PriceList/LookUps",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            lookUps = data.Response
            
            productGroups = lookUps.ProductGroups;
            products = lookUps.Products;
            units = lookUps.units;
            batches = lookUps.Batches;
            pricingLevels = lookUps.PricingLevels;

            renderLookUpDataToControls()

            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}
function getProductsForPriceList(productGroupId, pricingLevelName, productId)
{
    PriceListSearchParam.productGroupId = productGroupId;
    PriceListSearchParam.pricingLevelName = pricingLevelName;
    PriceListSearchParam.productId = productId;
    PriceListSearchParam.productCode = "";

    showLoader();
    $.ajax({
        url: API_BASE_URL + "/PriceList/GetProductsForPriceList",
        type: 'POST',
        data: JSON.stringify(PriceListSearchParam),
        contentType: "application/json",
        success: function (data) {
            productsForPriceList = data.Response;
            renderProductsToTable();
            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}
function getPriceListForThisProduct(productId) {
    pricingLevelId = $("#pricingLevelList").val();
    if ($("#pricingLevelList").val() == 0 || $("#pricingLevelList").val() == "") {
        Utilities.ErrorNotification("Please select Pricing Level.");
    }
    else {
        findProductForPriceList(productId);
        $.ajax({
            url: API_BASE_URL + "/PriceList/GetPriceList?pricingLevelId=" + $("#pricingLevelList").val() + "&productId=" + productId,
            type: 'GET',
            contentType: "application/json",
            success: function (data) {
                priceLists = data.Response;

                renderDataToPriceListPopUp()

                hideLoader();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    }
}
function addNewPrice() {
    if ($("#batchList").val() == 0 || $("#batchList").val() == "")
    {
        $("#errorNotification").html("Please select Batch");
        $("#successNotification").hide();
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 7000);
    }
    else if ($("#rate").val() == 0 || $("#rate").val() == "") {
        $("#errorNotification").html("Please enter Rate");
        $("#successNotification").hide();
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 7000);
    }
    else {
        newPrice.productId = thisProductForPriceList.productId;
        newPrice.pricingLevelId = pricingLevelId;
        newPrice.unitId = thisProductForPriceList.unitId;
        newPrice.batchId = $("#batchList").val();
        newPrice.rate = $("#rate").val();

        console.log(newPrice);
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/PriceList/SavePriceList",
            type: 'POST',
            data: JSON.stringify(newPrice),
            contentType: "application/json",
            success: function (data) {
                //console.log(data);
                if (data.ResponseCode == 200) {
                    getPriceListForThisProduct(thisProductForPriceList.productId);
                    $("#rate").val(1);
                    $('#batchList').data('kendoDropDownList').value(-1);

                    $("#successNotification").html("Price Added Successfully");
                    $("#successNotification").show();
                    $("#errorNotification").hide();
                    setTimeout(function () {
                        $("#successNotification").hide();
                    }, 7000);
                }
                else if (data.ResponseCode == 403) {
                    $("#errorNotification").html(data.ResponseMessage);
                    $("#successNotification").hide();
                    $("#errorNotification").show();
                    setTimeout(function () {
                        $("#errorNotification").hide();
                    }, 7000);
                }
                else {
                    $("#errorNotification").html(data.ResponseMessage);
                    $("#successNotification").hide();
                    $("#errorNotification").show();
                    setTimeout(function () {
                        $("#errorNotification").hide();
                    }, 7000);
                }
                hideLoader();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    }
}
function deletePriceList() {
    if (confirm('Are you sure you want to delete this price list?')) {
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/PriceList/DeletePriceList?pricingLevelId=" + pricingLevelId,
            type: 'GET',
            contentType: "application/json",
            success: function (data) {
                if (data.ResponseCode == 200) {
                    getPriceListForThisProduct(thisProductForPriceList.productId);
                    $("#rate").val(1);
                    $('#batchList').data('kendoDropDownList').value(-1);

                    $("#successNotification").html("Price List deleted Successfully");
                    $("#successNotification").show();
                    $("#errorNotification").hide();
                    setTimeout(function () {
                        $("#successNotification").hide();
                    }, 7000);
                }
                else if (data.ResponseCode == 403) {
                    $("#errorNotification").html(data.ResponseMessage);
                    $("#successNotification").hide();
                    $("#errorNotification").show();
                    setTimeout(function () {
                        $("#errorNotification").hide();
                    }, 7000);
                }
                else {
                    $("#errorNotification").html(data.ResponseMessage);
                    $("#successNotification").hide();
                    $("#errorNotification").show();
                    setTimeout(function () {
                        $("#errorNotification").hide();
                    }, 7000);
                }
                hideLoader();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    }
    
}
function editPrice() {
    if ($("#batchList").val() == 0 || $("#batchList").val() == "") {
        $("#errorNotification").html("Please select Batch");
        $("#successNotification").hide();
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 7000);
    }
    else if ($("#rate").val() == 0 || $("#rate").val() == "") {
        $("#errorNotification").html("Please enter Rate");
        $("#successNotification").hide();
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 7000);
    }
    else {
        priceListForEdit.pricingListId = thisPriceList.pricelistId;
        priceListForEdit.productId = thisProductForPriceList.productId;
        priceListForEdit.pricingLevelId = pricingLevelId;
        priceListForEdit.unitId = thisProductForPriceList.unitId;
        priceListForEdit.batchId = $("#batchList").val();
        priceListForEdit.rate = $("#rate").val();

        console.log(priceListForEdit);
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/PriceList/EditPriceList",
            type: 'POST',
            data: JSON.stringify(priceListForEdit),
            contentType: "application/json",
            success: function (data) {
                //console.log(data);
                if (data.ResponseCode == 200) {
                    getPriceListForThisProduct(thisProductForPriceList.productId);
                    $("#rate").val(1);
                    $('#batchList').data('kendoDropDownList').value(-1);

                    $("#edit").hide();
                    $("#save").show();

                    $("#successNotification").html("Price updated Successfully");
                    $("#successNotification").show();
                    $("#errorNotification").hide();
                    setTimeout(function () {
                        $("#successNotification").hide();
                    }, 7000);
                }
                else if (data.ResponseCode == 403) {
                    $("#errorNotification").html(data.ResponseMessage);
                    $("#successNotification").hide();
                    $("#errorNotification").show();
                    setTimeout(function () {
                        $("#errorNotification").hide();
                    }, 7000);
                }
                else {
                    $("#errorNotification").html(data.ResponseMessage);
                    $("#successNotification").hide();
                    $("#errorNotification").show();
                    setTimeout(function () {
                        $("#errorNotification").hide();
                    }, 7000);
                }
                hideLoader();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    }
}


/* ========== RENDER DATA TO CONTROLS ============ */
function renderLookUpDataToControls()
{
    $("#productGroupList").kendoDropDownList({
        optionLabel: "Please Select...",
        dataTextField: "groupName",
        dataValueField: "groupId",
        dataSource: productGroups,
        filter: "contains",
    });
    $("#productList").kendoDropDownList({
        optionLabel: "Please Select...",
        dataTextField: "productName",
        dataValueField: "productId",
        dataSource: products,
        filter: "contains",
    });
    $("#pricingLevelList").kendoDropDownList({
        optionLabel: "Please Select...",
        dataTextField: "pricinglevelName",
        dataValueField: "pricinglevelId",
        dataSource: pricingLevels,
        filter: "contains",
    });
    $("#batchList").kendoDropDownList({
        optionLabel: "Please Select...",
        dataTextField: "batchNo",
        dataValueField: "batchId",
        dataSource: batches,
        filter: "contains",
    });
}
function renderProductsToTable()
{
    if(table != "")
    {
        table.destroy();
    }
    var objToShow = [];
    $.each(productsForPriceList, function (i, record) {
        objToShow.push([
            (i + 1),
            record.productCode,
            record.productName,
            record.unitName,
            '&#8358;' + Utilities.FormatCurrency(record.salesRate),
            '&#8358;' + Utilities.FormatCurrency(record.purchaseRate),
            record.mrp,
            '<a class="btn btn-sm btn-primary" onclick="getPriceListForThisProduct(' + record.productId+ ')"><i class="fa fa-eye"></i> Price List</a>',
        ]);
    });
    table = $('#productsForPriceListTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

}
function renderDataToPriceListPopUp() {
    $("#priceListPopUp").modal("show");
    $('#priceListPopUp').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });

    $("#pricingLevel").val(findPricingLevels(pricingLevelId).pricinglevelName);
    $("#productName").val(thisProductForPriceList.productName);
    $("#productCode").val(thisProductForPriceList.productCode);
    $("#unit").val(thisProductForPriceList.unitName);

    if (table1 != "") {
        table1.destroy();
    }
    var objToShow = [];
    $.each(priceLists, function (i, record) {
        objToShow.push([
            (i + 1),
            record.batchNo,
            '&#8358;' + Utilities.FormatCurrency(record.rate),
            '<a class="btn btn-sm btn-primary" onclick="renderForEdit(' + record.pricelistId + ')"><i class="fa fa-edit"></i> Edit Rate</a>'
        ]);
    });
    table1 = $('#priceListTable').DataTable({
        data: objToShow,
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false
    });
}
function renderForEdit(id) {
    $("#edit").show();
    $("#save").hide();

    for (i = 0; i < priceLists.length; i++) {
        if (priceLists[i].pricelistId == id) {
            thisPriceList = priceLists[i];
            break;
        }
    }
    var dropdownlistBatch = $("#batchList").data("kendoDropDownList");
    dropdownlistBatch.value(thisPriceList.batchId);
    $("#rate").val(thisPriceList.rate);
}


/* ========== OTHERS ============ */
function searchForProducts() {
    var pgId = $("#productGroupList").val();
    var pId = $("#productList").val();

    getProductsForPriceList(pgId, "", pId);
}

function findProductForPriceList(id) {
    for (i = 0; i < productsForPriceList.length; i++) {
        if (productsForPriceList[i].productId == id) {
            thisProductForPriceList = productsForPriceList[i];
            break;
        }
    }
}
function findPricingLevels(id)
{
    var output = {};
    for (i = 0; i < pricingLevels.length; i++) {
        if (pricingLevels[i].pricinglevelId == id) {
            output = pricingLevels[i];
            break;
        }
    }
    return output;
}