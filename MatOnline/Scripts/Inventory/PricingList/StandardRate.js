﻿var productGroups = [];
var products = [];
var units = [];
var batches = [];
var productsForStandardRates = [];
var standardRates = [];

var lookUps = {}
var StandardRateSearchParam = {};
var thisProductForStandardRate = {};
var newStandardRate = {};
var thisStandardRate = {};
var standardRateForEdit = {};

var table = "";
var table1 = "";

$(function () {
    $("#successNotification").hide();
    $("#errorNotification").hide();
    $("#edit").hide();

    getLookUps();
    getProductsForStandardRate(0, "", 0);
});

/* ========== API CALLS ============ */
function getLookUps() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/StandardRate/GetLookUps",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            lookUps = data.Response

            productGroups = lookUps.ProductGroups;
            products = lookUps.Products;
            units = lookUps.units;
            batches = lookUps.Batches;

            renderLookUpDataToControls()

            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}
function getProductsForStandardRate(productGroupId, productId) {
    StandardRateSearchParam.productGroupId = productGroupId;
    StandardRateSearchParam.productId = productId;
    StandardRateSearchParam.productCode = "";

    showLoader();
    $.ajax({
        url: API_BASE_URL + "/StandardRate/GetProductsForStandardRate",
        type: 'POST',
        data: JSON.stringify(StandardRateSearchParam),
        contentType: "application/json",
        success: function (data) {
            productsForStandardRates = data.Response;
            renderProductsToTable();
            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}
function getStandardRateForThisProduct(productId) {
    findProductForStandardRate(productId);
    $.ajax({
        url: API_BASE_URL + "/StandardRate/GetStandardRates?productId=" + productId,
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            standardRates = data.Response;

            renderDataToStandardRatePopUp()

            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}
function addNewRate() {
    if ($("#batchList").val() == 0 || $("#batchList").val() == "") {
        $("#errorNotification").html("Please select Batch");
        $("#successNotification").hide();
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 7000);
    }
    else if ($("#rate").val() == 0 || $("#rate").val() == "") {
        $("#errorNotification").html("Please enter Rate");
        $("#successNotification").hide();
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 7000);
    }
    else if ($("#fromDate").val() == 0 || $("#fromDate").val() == "") {
        $("#errorNotification").html("Please enter From Date");
        $("#successNotification").hide();
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 7000);
    }
    else if ($("#toDate").val() == 0 || $("#toDate").val() == "") {
        $("#errorNotification").html("Please enter to Date");
        $("#successNotification").hide();
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 7000);
    }
    else {
        newStandardRate.productId = thisProductForStandardRate.productId;
        newStandardRate.unitId = thisProductForStandardRate.unitId;
        newStandardRate.batchId = $("#batchList").val();
        newStandardRate.rate = $("#rate").val();
        newStandardRate.applicableFrom = $("#fromDate").val();
        newStandardRate.applicableTo = $("#toDate").val();

        console.log(newStandardRate);
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/StandardRate/SaveStandardRate",
            type: 'POST',
            data: JSON.stringify(newStandardRate),
            contentType: "application/json",
            success: function (data) {
                //console.log(data);
                if (data.ResponseCode == 200) {
                    getStandardRateForThisProduct(thisProductForStandardRate.productId);
                    $("#rate").val(1);
                    $('#batchList').data('kendoDropDownList').value(-1);

                    $("#successNotification").html("Rate Added Successfully");
                    $("#successNotification").show();
                    $("#errorNotification").hide();
                    setTimeout(function () {
                        $("#successNotification").hide();
                    }, 7000);
                }
                else if (data.ResponseCode == 403) {
                    $("#errorNotification").html(data.ResponseMessage);
                    $("#successNotification").hide();
                    $("#errorNotification").show();
                    setTimeout(function () {
                        $("#errorNotification").hide();
                    }, 7000);
                }
                else {
                    $("#errorNotification").html(data.ResponseMessage);
                    $("#successNotification").hide();
                    $("#errorNotification").show();
                    setTimeout(function () {
                        $("#errorNotification").hide();
                    }, 7000);
                }
                hideLoader();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    }
}
function deleteStandardRate(id) {
    if(confirm('Are you sure you want to delete this standard rate?'))
    {
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/StandardRate/DeleteStandardRate?standardRateId=" + id,
            type: 'GET',
            contentType: "application/json",
            success: function (data) {
                if (data.ResponseCode == 200) {
                    getStandardRateForThisProduct(thisProductForStandardRate.productId);
                    $("#rate").val(1);
                    $('#batchList').data('kendoDropDownList').value(-1);

                    $("#successNotification").html("Sales Rate deleted Successfully");
                    $("#successNotification").show();
                    $("#errorNotification").hide();
                    setTimeout(function () {
                        $("#successNotification").hide();
                    }, 7000);
                }
                else if (data.ResponseCode == 403) {
                    $("#errorNotification").html(data.ResponseMessage);
                    $("#successNotification").hide();
                    $("#errorNotification").show();
                    setTimeout(function () {
                        $("#errorNotification").hide();
                    }, 7000);
                }
                else {
                    $("#errorNotification").html(data.ResponseMessage);
                    $("#successNotification").hide();
                    $("#errorNotification").show();
                    setTimeout(function () {
                        $("#errorNotification").hide();
                    }, 7000);
                }
                hideLoader();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    }
    
}
function editRate() {
    if ($("#batchList").val() == 0 || $("#batchList").val() == "") {
        $("#errorNotification").html("Please select Batch");
        $("#successNotification").hide();
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 7000);
    }
    else if ($("#rate").val() == 0 || $("#rate").val() == "") {
        $("#errorNotification").html("Please enter Rate");
        $("#successNotification").hide();
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 7000);
    }
    else if ($("#fromDate").val() == 0 || $("#fromDate").val() == "") {
        $("#errorNotification").html("Please enter From Date");
        $("#successNotification").hide();
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 7000);
    }
    else if ($("#toDate").val() == 0 || $("#toDate").val() == "") {
        $("#errorNotification").html("Please enter to Date");
        $("#successNotification").hide();
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 7000);
    }
    else {
        standardRateForEdit.standardRateId = thisStandardRate.standardRateId;
        standardRateForEdit.productId = thisProductForStandardRate.productId;
        standardRateForEdit.unitId = thisProductForStandardRate.unitId;
        standardRateForEdit.batchId = $("#batchList").val();
        standardRateForEdit.rate = $("#rate").val();
        standardRateForEdit.applicableFrom = $("#fromDate").val();
        standardRateForEdit.applicableTo = $("#toDate").val();

        console.log(newStandardRate);
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/StandardRate/EditStandardRate",
            type: 'POST',
            data: JSON.stringify(standardRateForEdit),
            contentType: "application/json",
            success: function (data) {
                //console.log(data);
                if (data.ResponseCode == 200) {
                    getStandardRateForThisProduct(thisProductForStandardRate.productId);
                    $("#rate").val(1);
                    $('#batchList').data('kendoDropDownList').value(-1);

                    $("#edit").hide();
                    $("#save").show();

                    $("#successNotification").html("Rate updated Successfully");
                    $("#successNotification").show();
                    $("#errorNotification").hide();
                    setTimeout(function () {
                        $("#successNotification").hide();
                    }, 7000);
                }
                else if (data.ResponseCode == 403) {
                    $("#errorNotification").html(data.ResponseMessage);
                    $("#successNotification").hide();
                    $("#errorNotification").show();
                    setTimeout(function () {
                        $("#errorNotification").hide();
                    }, 7000);
                }
                else {
                    $("#errorNotification").html(data.ResponseMessage);
                    $("#successNotification").hide();
                    $("#errorNotification").show();
                    setTimeout(function () {
                        $("#errorNotification").hide();
                    }, 7000);
                }
                hideLoader();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    }
}



/* ========== RENDER DATA TO CONTROLS ============ */
function renderLookUpDataToControls() {
    $("#productGroupList").kendoDropDownList({
        optionLabel: "Please Select...",
        dataTextField: "groupName",
        dataValueField: "groupId",
        dataSource: productGroups,
        filter: "contains",
    });
    $("#productList").kendoDropDownList({
        optionLabel: "Please Select...",
        dataTextField: "productName",
        dataValueField: "productId",
        dataSource: products,
        filter: "contains",
    });
    $("#batchList").kendoDropDownList({
        optionLabel: "Please Select...",
        dataTextField: "batchNo",
        dataValueField: "batchId",
        dataSource: batches,
        filter: "contains",
    });
}
function renderProductsToTable() {
    if (table != "") {
        table.destroy();
    }
    var objToShow = [];
    $.each(productsForStandardRates, function (i, record) {
        objToShow.push([
            (i + 1),
            record.productCode,
            record.productName,
            record.unitName,
            '<a class="btn btn-sm btn-primary" onclick="getStandardRateForThisProduct(' + record.productId + ')"><i class="fa fa-eye"></i> set standard rate</a>',
        ]);
    });
    table = $('#productsForStandardRateTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

}
function renderDataToStandardRatePopUp() {
    $("#standardRatePopUp").modal("show");
    $('#standardRatePopUp').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });

    //$("#pricingLevel").val(findPricingLevels(pricingLevelId).pricinglevelName);
    $("#productName").val(thisProductForStandardRate.productName);
    $("#productCode").val(thisProductForStandardRate.productCode);
    $("#unit").val(thisProductForStandardRate.unitName);

    if (table1 != "") {
        table1.destroy();
    }
    var objToShow = [];
    $.each(standardRates, function (i, record) {
        objToShow.push([
            (i + 1),
            record.applicableFrom,
            record.applicableTo,
            record.batchNo,
            '&#8358;' + Utilities.FormatCurrency(record.rate),
            '<a class="btn btn-sm btn-primary" onclick="renderForEdit(' + record.standardRateId + ')"><i class="fa fa-edit"></i> Edit Rate</a><a class="btn btn-sm btn-danger" onclick="deleteStandardRate(' + record.standardRateId + ')"><i class="fa fa-times"></i> Delete Rate</a>'
        ]);
    });
    table1 = $('#priceListTable').DataTable({
        data: objToShow,
        "paging": false,
        "lengthChange": false,
        "searching": false,
        "ordering": false,
        "info": false,
        "autoWidth": false
    });
}
function renderForEdit(id) {
    $("#edit").show();
    $("#save").hide();

    for (i = 0; i < standardRates.length; i++) {
        if (standardRates[i].standardRateId == id) {
            thisStandardRate = standardRates[i];
            break;
        }
    }
    var dropdownlistBatch = $("#batchList").data("kendoDropDownList");
    dropdownlistBatch.value(thisStandardRate.batchId);
    $("#rate").val(thisStandardRate.rate);
    $("#fromDate").val(thisStandardRate.applicableFrom);
    $("#toDate").val(thisStandardRate.applicableTo);
}


/* ========== OTHERS ============ */
function searchForProducts() {
    var pgId = $("#productGroupList").val();
    var pId = $("#productList").val();

    getProductsForStandardRate(pgId, pId);
}


function findProductForStandardRate(id) {
    for (i = 0; i < productsForStandardRates.length; i++) {
        if (productsForStandardRates[i].productId == id) {
            thisProductForStandardRate = productsForStandardRates[i];
            break;
        }
    }
}
