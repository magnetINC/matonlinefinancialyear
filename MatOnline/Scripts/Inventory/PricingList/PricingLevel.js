﻿var pricingLevels = [];
var pricingLevelToEdit = "";
var pricingLevelIdToDelete = 0;
var table = "";
var pricingLevel = [];
var thisPriceLevelId = 0;

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created

$(function () {
    $("#edit").hide();
    getPricingLevels();

    //$("#pricingLevelTable").on("click", ".btnEditModal", function () {
    //    var id = $(this).attr("id");
    //    pricingLevelToEdit = findPricingLevel(id);
    //    $("#editPricingLevelId").val(pricingLevelToEdit[0]);
    //    $("#editPricingLevel").val(pricingLevelToEdit[1]);
    //    $('#editModal').modal('toggle');

    //});

    //$("#pricingLevelTable").on("click", ".btnDeleteModal", function () {
    //    var id = $(this).attr("id");
    //    pricingLevelToEdit = findPricingLevel(id);
    //    pricingLevelIdToDelete = id;
    //    $("#deletePricingLevel").html(pricingLevelToEdit[1]);
    //    $('#deleteModal').modal('toggle');
    //});


    $("#save").click(function () {
        if ($("#pricingLevel").val() != "")
        {
            $("#errorMsg").html("");

            var pricingLevelToSave = {
                PricinglevelName: $("#pricingLevel").val(),
                Narration: $("#narration").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/PricingLevel/AddPricingLevel",
                type: 'POST',
                data: JSON.stringify(pricingLevelToSave),
                contentType: "application/json",
                success: function (data) {
                    //hideLoader();
                    Utilities.SuccessNotification("Pricing Level Saved Successfully");

                    table.destroy();
                    getPricingLevels();

                    $("#pricingLevel").val("");
                    $("#narration").val("");
                },
                error: function (request, error) {
                    hideLoader();
                    Utilities.ErrorNotification("Error Saving");
                }
            });
        }
        else
        {
            Utilities.ErrorNotification("Enter a Pricing Level");
        }
        
    });

    //$("#delete").click(function () {
    //    var pricingLevelToDelete = {
    //        pricingLevelId: pricingLevelIdToDelete
    //    };
    //    showLoader();
    //    $.ajax({
    //        url: API_BASE_URL + "/PricingLevel/DeletePricingLevel",
    //        type: 'POST',
    //        data:JSON.stringify(pricingLevelToDelete),
    //        contentType: "application/json",
    //        success: function (data) {
    //            //hideLoader();
    //            table.destroy();
    //            getPricingLevels();
    //        },
    //        error: function (request, error) {
    //            hideLoader();
    //        }
    //    });
    //});

    $("#saveChanges").click(function () {
        if ($("#editPricingLevel").val() != "") {
            $("#editErrorMsg").html("");

            var pricingLevelToEdit = {
                PricingLevelId: $("#PricingLevelId").val(),
                PricingLevelName: $("#PricingLevelName").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/PricingLevel/EditPricingLevel",
                type: 'POST',
                data: JSON.stringify(pricingLevelToEdit),
                contentType: "application/json",
                success: function (data) {
                   // hideLoader();
                    table.destroy();
                    getPricingLevels();
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else {
            $("#editErrorMsg").html("Pricing level is required!");
        }

    });
});
function editPricingLevel()
{
    if ($("#pricingLevel").val() != "") {

        var pricingLevelToEdit = {
            PricingLevelId: thisPriceLevelId,
            PricingLevelName: $("#pricingLevel").val(),
            Narration: $("#narration").val()
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/PricingLevel/EditPricingLevel",
            type: 'POST',
            data: JSON.stringify(pricingLevelToEdit),
            contentType: "application/json",
            success: function (data) {
                // hideLoader();
                Utilities.SuccessNotification("Pricing Level Saved Successfully");

                table.destroy();
                getPricingLevels();

                $("#pricingLevel").val("");
                $("#narration").val("");
            },
            error: function (request, error) {
                hideLoader();
                Utilities.ErrorNotification("Error Updating");
            }
        });
    }
    else {
        Utilities.ErrorNotification("Enter a Pricing Level");
    }
}
function deletePricingLevel(id)
{
    if (confirm('Delete this Pricing Level?'))
    {
        var pricingLevelToDelete = {
            pricingLevelId: id
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/PricingLevel/DeletePricingLevel",
            type: 'POST',
            data: JSON.stringify(pricingLevelToDelete),
            contentType: "application/json",
            success: function (data) {
                //hideLoader();
                table.destroy();
                getPricingLevels();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    }
}
function getPricingLevels() {
    showLoader();
    $("#edit").hide();
    $("#save").show();
    $.ajax({
        url: API_BASE_URL + "/PricingLevel/GetPricingLevels",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function onSuccess(data) {
    pricingLevels = data;
    var objToShow = [];
    $.each(data, function (i, record) {

        objToShow.push([
            (i + 1),
            record.pricinglevelName,
            record.narration,
            '<div class="btn-group btn-group-sm pricingLevelAction"><div class="dropdown"><button class="btn btn-default dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button><ul class="dropdown-menu"><li class="editPricingLevel" onclick="findPricingLevelForEdit(' + record.pricinglevelId + ')"><a><i class="fa fa-edit"></i> Edit</a></li><li class="deletePricingLevel" onclick="deletePricingLevel(' + record.pricinglevelId + ')"><a><i class="fa fa-times"></i> Delete</a></li></ul></div></div>',
        ]);
    });

    table = $('#pricingLevelTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    var count = 0;
    if (checkPriviledge("frmPricingLevel", "Update") === false) {
        $(".editPricingLevel").hide();
        count++;
    }
    if (checkPriviledge("frmPricingLevel", "Delete") === false) {
        $(".deletePricingLevel").hide();
        count++;
    }
    if (count === 2) {
        $(".pricingLevelAction").hide();
    }
    hideLoader();
}

function findPricingLevelForEdit(id) {
    showLoader();
    $("#edit").show();
    $("#save").hide();
    thisPriceLevelId = id;
    for (i = 0; i < pricingLevels.length; i++) {
        if (pricingLevels[i].pricinglevelId == id) {
            pricingLevel = pricingLevels[i];
            break;
        }
    }
    $("#pricingLevel").val(pricingLevel.pricinglevelName);
    $("#narration").val(pricingLevel.narration);
    hideLoader();
}