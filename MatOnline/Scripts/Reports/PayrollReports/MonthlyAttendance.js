﻿var table = "";
var mainOutput = [];
var designations = [];

$(function () {
    getDesignations();
})

function getDesignations()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PayrollReports/GetDesignations",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            designations = data.Response;
            $("#designation").kendoDropDownList({
                filter: "contains",
                dataTextField: "designationName",
                dataValueField: "designationId",
                dataSource: designations
            })
        }
    })
    Utilities.Loader.Hide();
}

function getMonthlyAttendanceReport()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PayrollReports/GetMonthlyAttendanceReportDetails?designation="+$('#designation').val()+"&status="+$('#type').val()+"&month="+$('#month').val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            designations = data.Response;
            //reb=nderData
        }
    })
    Utilities.Loader.Hide();
}

function renderData()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,

        ])
    })
}