﻿var table = "";
var payElements = [];
var mainOutputs = [];

$(function () {
    getPayHeadReportDetails();
})

//function getPayElements()
//{
//    Utilities.Loader.Show();

//    $.ajax({
//        url: API_BASE_URL + "/PayrollReports/GetPayHeadNames",
//        type: "GET",
//        contentType: "application/json",
//        success: function(data)
//        {
//            console.log(data);
//            payElements = data.Response;
//            $("#payElementType").kendoDropDownList({
//                filter: "contains",
//                dataTextField: "payHeadName",
//                dataValueField: "payHeadName",
//                dataSource: payElements,
//                optionLabel:"All"
//            })
//        }

//    })
//    Utilities.Loader.Hide();    

//}

function getPayHeadReportDetails()
{
    var payelement = 'All';
    var type = 'All';
   // $('#payElementType').val('All');
   //$('#type').val('All');

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetPayHeadReportDetails?payHeadName=" + payelement + "&type=" +type,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                mainOutputs = data.Response;
                renderdata();
            }
        })
        Utilities.Loader.Hide();
}


function renderdata() {
    var display = [];
    if (table != "") {
        table.destroy();
    }
    $.each(mainOutputs, function (index, row) {
        display.push([
            index + 1,
            row.payHeadName,
            row.type
        ])
    })
    table = $('#PayHeadReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

}