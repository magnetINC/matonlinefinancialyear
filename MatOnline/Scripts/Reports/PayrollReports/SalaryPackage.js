﻿var table = "";
var mainOutput = [];
var packageNames = [];
var status = [];

$(function () {
    getPackageNames();
})

function getPackageNames()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PayrollReports/GetPackageNames",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            packageNames = data.Response;
            //rendderfill-up
            $("#packageName").kendoDropDownList({
                filter: "contains",
                dataTextField: "salaryPackageName",
                dataValueField: "salaryPackageId",
                dataSource: packageNames,
                optionLabel:"All"
            })
        }
    })
    Utilities.Loader.Hide();
}

function getSalaryPackageReport()
{
    if ($('packageName').val() == "")
    {
        $('packageName').val('All');
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetSalaryPackageDetails?packageName=" + $('packageName').val() + "&status=" + $('#status').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                //renderdata
                renderData();
            }
        })

        Utilities.Loader.Hide();
    }
    else {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetSalaryPackageDetails?packageName=" + $('packageName').val() + "&status=" + $('#status').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                //renderdata
                renderData();
            }
        })

        Utilities.Loader.Hide();
    }

}


function renderData()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function(index,row)
    {
        display.push([
            index + 1,
            row.salaryPackageId,
            row.salaryPackageName,
            row.isActive,
            row.totalAmount
        ])
    })
    table = $('#SalaryPackageReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}