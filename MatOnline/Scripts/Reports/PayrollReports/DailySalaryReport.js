﻿var table = "";
var mainOutput = [];
var employees =[];
var designations = [];
var status = [];

$(function(){
    Loading();
})

function Loading() {
    Utilities.Loader.Show();
    getEmployees();
    getDesignations();
    Utilities.Loader.Hide();
}

function getEmployees()
{
    $.ajax({
        url:API_BASE_URL + "/PayrollReports/GetEmpCodes",
        type: "GET",
        contentType:"application/json",
        success: function(data)
        {
            console.log(data);
            employees = data.Response;
            $("#empCode").kendoDropDownList({
                filter: "contains",
                dataTextField: "employeeName",
                dataValueField: "employeeId ",
                dataSource: employees,
                optionLabel:"All"
            })
        }
    })
}

function getDesignations()
{
    $.ajax({
        url:API_BASE_URL + "/PayrollReports/GetDesignations",
        type: "GET",
        contentType:"application/json",
        success: function(data)
        {
            console.log(data);
            designations = data.Response;
            $("#designation").kendoDropDownList({
                filter: "contains",
                dataTextField: "designationName",
                dataValueField: "designationId",
                dataSource: designations,
                optionLabel:"All"
            })
        }
    })
}

function getDailySalaryReportDetails()
{
    if ($('#empCode').val() == "" && $('#designation').val() == "")
    {
        $('#designation').val('All');
        $('#empCode').val('All');
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetDailySalaryReportDetails?emp=" + $('#empCode').val() + "&designation=" + $('#designation').val() + "&from=" + $('#fromDate').val() + "&to=" + $('#toDate').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                mainOutput = data.Response;
                renderdata();
            }
        })
        Utilities.Loader.Hide();
    }
    else
    {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetDailySalaryReportDetails?emp=" + $('#empCode').val() + "&designation=" + $('#designation').val() + "&from=" + $('#fromDate').val() + "&to=" + $('#toDate').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                mainOutput = data.Response;
                renderdata();
            }
        })
        Utilities.Loader.Hide();
    }



}

function renderdata()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function(index, row){
        display.push([
            index + 1,
            row.employeeCode,
            row.employeeName,
            row.designationName,
            row.voucherNo,
            row.voucherTypeName,
            row.salaryDate,
            row.wage
        ])
    })
    table = $('#DailySalaryList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}