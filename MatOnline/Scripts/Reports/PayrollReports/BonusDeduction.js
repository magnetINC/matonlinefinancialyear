﻿var mainOutput = [];
var table = "";
var designations = [];
var empcodes = [];

$(function () {
    Loading();
})

function Loading()
{
    Utilities.Loader.Show();
    getEmpCodes();
    getDesignations();
    Utilities.Loader.Hide();
}

function getEmpCodes()
{
    $.ajax({
        url: API_BASE_URL + "/PayrollReports/GetEmpCodes",
        type: "GET",
        contentType: "application/json",
        success: function (data)
        {
            console.log(data);
            empcodes = data.Response;
            $("#empCode").kendoDropDownList({
                filter: "contains",
                dataTextField: "employeeName",
                dataValueField: "employeeId ",
                dataSource: empcodes,
                optionLabel:"All"
            })
        }
    })
}

function getDesignations()
{
    $.ajax({
        url: API_BASE_URL + "/PayrollReports/GetDesignations",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            designations = data.Response;
            $("#designation").kendoDropDownList({
                filter: "contains",
                dataTextField: "designationName",
                dataValueField: "designationId ",
                dataSource: designations,
                optionLabel:"All"
            })
        }
    })
}

function getBonusDeductionReport()
{
  

    if ($('#empCode').val() == "" && $('#designation').val() == "")
    {
        $('#empCode').val('All');
        $('#designation').val('All');
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetBonusDeductionDetails?from=" + $('#fromDate').val() + "&to=" + $('#toDate').val() + "&month=" + $('#salarymonth').val() + "&designation=" + $('#designation').val() + "&employee=" + $('#empCode').val() + "&bonusOrDeduction=" + $('#forType').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                mainOutput = data.Response;
                renderdata();
            }
        })
        Utilities.Loader.Hide();
        console.log($('#fromDate').val());
        console.log($('#toDate').val());
        console.log($('#salarymonth').val());
        console.log($('#designation').val());
        console.log($('#empCode').val());
        console.log($('#forType').val());
    }
    else {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetBonusDeductionDetails?from=" + $('#fromDate').val() + "&to=" + $('#toDate').val() + "&month=" + $('#salarymonth').val() + "&designation=" + $('#designation').val() + "&employee=" + $('#empCode').val() + "&bonusOrDeduction=" + $('#forType').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                mainOutput = data.Response;
                renderdata();
            }
        })
        Utilities.Loader.Hide();
    }


}

function renderdata()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.employeeId,
            row.date,
            row.employeeCode,
            row.employeeName,
            row.amount,
            row.narration
        ])
    })
    table = $('#BonusDeductionList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}