﻿var table = "";
var mainOutput = [];
var employees = [];
var designations = [];

$(function () {
    Loading();
})

function Loading()
{
    Utilities.Loader.Show();
    getEmployees();
    getDesignations();
    Utilities.Loader.Hide();


}

function getEmployees()
{
    $.ajax({
        url: API_BASE_URL + "/PayrollReports/GetEmpCodes",
        type: "GET",
        contentType: "application/json",
        success: function(data)
        {
            console.log(data);
            employees = data.Response;
            $("#empCode").kendoDropDownList({
                filter: "contains",
                dataTextField: "employeeName",
                dataValueField: "employeeId ",
                dataSource: employees,
                optionLabel:'All'
            })
        }       
    })
}

function getDesignations()
{
    $.ajax({
        url: API_BASE_URL + "/PayrollReports/GetDesignations",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            designations = data.Response;
            $("#designation").kendoDropDownList({
                filter: "contains",
                dataTextField: "designationName",
                dataValueField: "designationId",
                dataSource: designations,
                optionLabel:'All'
            })
        }
    })
}

function getEmployeeReportDetails()
{
    if ($('#empCode').val() == "" && $('#designation').val() =="")
    {
        $('#empCode').val("All");
        $('#designation').val("All");
       
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetEmployeeReportDetails?employee=" + $('#empCode').val() + "&designation=" + $('#designation').val() + "&status=" + $('#status').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                mainOutput = data.Response;
                renderData();
            }
        })
        Utilities.Loader.Hide();
        console.log($('#empCode').val());
        console.log($('#designation').val());
        console.log($('#status').val());


    }
    else
    {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetEmployeeReportDetails?employee=" + $('#empCode').val() + "&designation=" + $('#designation').val() + "&status=" + $('#status').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                mainOutput = data.Response; 
                renderData();
            }
        })
        Utilities.Loader.Hide();
        console.log($('#empCode').val());
        console.log($('#designation').val());
        console.log($('#status').val());
    }
   
}

function renderData()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.employeeId,
            row.employeeCode,
            row.employeeName,
            row.designationName,
            row.salaryPackageName,
            row.phoneNumber,
            row.mobileNumber
        ])
    })

    table = $('#EmployeeReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}