﻿var table = "";
var mainOutput = [];

$(function () {
    getEmployeeAddressBookDetails();
})

function getEmployeeAddressBookDetails()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PayrollReports/GetEmployeeAddressBookDetails",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            mainOutput = data.Response;
            renderData();
        }
    })
    Utilities.Loader.Hide();

}

function renderData()
{
    var display =[];
    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.employeeId,
            row.employeeCode,
            row.employeeName,
            row.designationName,
            row.address,
            row.phoneNumber,
            row.mobileNumber,
            row.email
        ])
    })

    table = $('#EmployeeAddressBookReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}