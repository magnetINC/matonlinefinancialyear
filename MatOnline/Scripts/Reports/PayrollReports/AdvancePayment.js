﻿var table = "";
var empcodes = [];
var salarymonths = [];
var mainOutput = [];

$(function () {
    Loading();
})

function Loading()
{
    Utilities.Loader.Show();
    getempcodes();
    Utilities.Loader.Hide();
   // console.log($('#fromDate').val());
}

function getempcodes()
{
    $.ajax({
        url: API_BASE_URL + "/PayrollReports/GetEmpCodes",
        type: "GET",
        contentType: "application/json",
        success: function(data)
        {
            console.log(data);
            empcodes = data.Response;
            $("#empCode").kendoDropDownList({
                filter: "contains",
                dataTextField: "employeeName",
                dataValueField: "employeeCode",
                dataSource: empcodes,
                optionLabel:"All"
            })
        }
    })
}

function getAdvancePaymentReport() {

    if ($('#empCode').val() == "")
    {
        $('#empCode').val('All');
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetAdvancePaymentDetails?from=" + $('#fromDate').val() + "&to=" + $('#toDate').val() + "&empCode=" + $('#empCode').val() + "&month=" + $('#salarymonth').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                mainOutput = data.Response;
                renderdata();
            }
        })
        Utilities.Loader.Hide();
    }
    else
    {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetAdvancePaymentDetails?from=" + $('#fromDate').val() + "&to=" + $('#toDate').val() + "&empCode=" + $('#empCode').val() + "&month=" + $('#salarymonth').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                mainOutput = data.Response;
                renderdata();
            }
        })
        Utilities.Loader.Hide();
    }

}

function renderdata()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            roe.date,
            row.employeeCode,
            row.employeeName,
            row.amount,
            row.narration
        ])
    })

    table = $('#AdvancePaymentList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}