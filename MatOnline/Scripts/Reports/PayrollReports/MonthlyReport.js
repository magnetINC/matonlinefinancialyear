﻿var table = "";
var mainOutput = [];
var designations = [];
var employees = [];

$(function()
{
    Loading();      
})

function Loading()
{
    Utilities.Loader.Show();
    getDesignations();
    getEmployees();
    Utilities.Loader.Hide();

}
function getDesignations()
{
    $.ajax({
        url: API_BASE_URL + "/PayrollReports/GetDesignations",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            designations = data.Response;
            $("#designation").kendoDropDownList({
                filter: "contains",
                dataTextField: "designationName",
                dataValueField: "designationId",
                dataSource: designations,
                optionLabel:"All"
            })
        }
    })
}
//GetDesignations
function getEmployees() {
    $.ajax({
        url: API_BASE_URL + "/PayrollReports/GetEmpCodes",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            employees = data.Response;
            $("#employee").kendoDropDownList({
                filter: "contains",
                dataTextField: "employeeName",
                dataValueField: "employeeId",
                dataSource: employees,
                optionLabel:"All"
            })
        }
    })
}
//GetMonthlySalaryReportDetails
function GetMonthlySalaryReportDetails()
{
    if ($('#designation').val() == "" && $('#employee').val() == "")
    {

        $('#designation').val('All');
        $('#employee').val('All');
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetMonthlySalaryReportDetails?from=" + $('#fromDate').val() + "&to=" + $('#toDate').val() + "&designation=" + $('#designation').val() + "&employee=" + $('#employee').val() + "&month=" + $('#salaryMonth').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                mainOutput = data.Response;
                renderData();
            }
        })
        Utilities.Loader.Hide();
        console.log($('#fromDate').val());
        console.log($('#toDate').val());
        console.log($('#designation').val());
        console.log($('#employee').val());
        console.log($('#salaryMonth').val());

    }
    else {
        Utilities.Loader.Show();

        $.ajax({
            url: API_BASE_URL + "/PayrollReports/GetMonthlySalaryReportDetails?from=" + $('#fromDate').val() + "&to=" + $('#toDate').val() + "&designation=" + $('#designation').val() + "&employee=" + $('#employee').val() + "&month=" + $('#salaryMonth').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                mainOutput = data.Response;
                renderData();
            }
        })
        Utilities.Loader.Hide();
    }

}

function renderData()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput,function(index, row)
    {
        display.push([
            index + 1,
           row.employeeName,
            row.employeeCode,
            row.totalAmount
        ])
    })
    table = $('#MonthlySalaryReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}

function getTotalAmount()
{
    var amount = 0;
    for(i=0;i<mainOutput.length;i++)
    {
        amount += mainOutput[i].totalAmount;
    }
    $('#totAmount').val(amount);
}