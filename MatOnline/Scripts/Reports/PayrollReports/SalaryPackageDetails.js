﻿var mainOutput = [];
var packageNames = [];
var table = "";

var amount = 0;
var addition = 0;
var deduction = 0;

$(function () {
    getSalaryPackageDetailsReport();
})

//function getPackageNames()
//{
//    $.ajax({
//        url: API_BASE_URL + "/PayrollReports/GetPackageNames",
//        type: "GET",
//        contentType: "application/json",
//        success: function(data)
//        {
//            console.log(data);
//            packageNames = data.Response;
//            $("#packageName").kendoDropDownList({
//                filter: "contains",
//                dataTextField: "salaryPackageName",
//                dataValueField: "salaryPackageId",
//                dataSource: packageNames,
//                optionLabel:'All'
//            })
//        }
//      })
//}

function getSalaryPackageDetailsReport()
{
    var packageName = 'All'
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PayrollReports/GetSalaryPackageReportDetails?packageName=" + packageName,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            mainOutput = data.Response;
            renderdata();
            getTotalAdditionOrDeduction();
            getTotalAmount();
            Utilities.Loader.Hide();

        }
    })
}

function renderdata()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.payHeadName,
            row.type,
            Utilities.FormatCurrency(row.amount)
        ])
    })
    table = $('#SalaryPackagedDetailsReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}

function getTotalAdditionOrDeduction()
{
    for(i=0;i<mainOutput.length;i++)
    {
        if(mainOutput[i].type == "Addition")
        {
            addition += mainOutput[i].amount;
            $('#totAdd').val(Utilities.FormatCurrency(addition));
        }
        else if(mainOutput[i].type == "Deduction")
        {
            deduction += mainOutput[i].amount;
            $('#totDeduc').val(Utilities.FormatCurrency(deduction));

        }
    }
}

function getTotalAmount()
{
    for(i=0;i<mainOutput.length;i++)
    {
        amount += mainOutput[i].amount;
    }
    $('#totAmount').val(Utilities.FormatCurrency(amount));
}