﻿var output = [];
var banks = [];
var table = "";

//runs on page load
$(function () {

    getLookUps();

    
});

function getLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BankingReports/GetAllBanks",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            banks = data.Response;
                $("#banks").kendoDropDownList({
                    filter: "contains",
                    dataTextField: "ledgerName",
                    dataValueField: "ledgerID",
                    dataSource: banks,
                    optionLabel:"Select Bank"
                    
                });
                Utilities.Loader.Hide();
            //to put the banks in the txtbox
          

        }

    });
}

function getBankTransferDetails()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BankingReports/ContraReports?ledgerName=" + $('#banks').val() + "&type=" + $('#forSelect').val() + "&from=" + $('#fromDate').val() + "&to=" + $('#toDate').val(),
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            Utilities.Loader.Hide();
            console.log(data);
            output = data.Response;
            RenderData();
           

        }
    });
}

//to render data in Data-Table
function RenderData()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    $.each(output, function(index,rowObj)
    {
        display.push([
            index + 1,
            rowObj.voucherTypeName,
            rowObj.invoiceNo,
            rowObj.date,
            '&#8358;' + Utilities.FormatCurrency(rowObj.amount),
            rowObj.userName,
            rowObj.narration
        ])  

    });
    table = $('#BankTransferList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
   }
