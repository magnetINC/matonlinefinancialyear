﻿var reports = [];
var banks = [];
var ledgers = [];

var table = "";

$(function () {
    getLookUps();
    getReport();

});

//====================== API CALLS ================
function getLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BankReports/LookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            ledgers = data.Response.ledgers;

            renderLookUpsToControls();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getReport()
{
    Utilities.Loader.Show();
    var lid = 0;
    if ($("#ledgers").val() == "" || $("#ledgers").val() == null || $("#ledgers").val() == undefined)
    {
        lid = 0;
    }
    $.ajax({
        url: API_BASE_URL + "/BankReports/ChequeOrBankReport?from=" + $("#fromDateIssue").val() + "&to=" + $("#toDateIssue").val() + "&chequeFromDate=" + $("#fromDateCheque").val()
            + "&chequeToDate=" + $("#toDateCheque").val() + "&ledger=" + lid + "&type=" + $("#format").val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            reports = data.Response;

            renderDataToTable();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}


//==========================RENDER DATA TO CONTROLS ======================
function renderLookUpsToControls()
{
    $("#ledgers").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: ledgers,
        optionLabel: "Please Select..."
    });
}
function renderDataToTable()
{
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }
    var tamount = 0;
    $.each(reports, function (count, row) {
        objToShow.push([
            count + 1,
            row["Voucher Type"],
            row["Voucher No"],
            row["Issued Date"],
            row["Party"],
            row["Cheque No"],
            row["Cheque Date"],
             '&#8358;' + Utilities.FormatCurrency(row.Amount)
        ]);

        tamount = tamount + row.Amount;
    });
    table = $('#cashOrChequeDetailsReport').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}