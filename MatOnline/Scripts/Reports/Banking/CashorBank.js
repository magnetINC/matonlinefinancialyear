﻿var output = [];
var table = "";

function CashorBankDetails()
{
    Utilities.Loader.Show();
    $.ajax({
        url:    API_BASE_URL + "/BankingReports/CashorBankReports?from=" + $('#fromDate').val() + "&to="+$('#toDate').val(),
        type: "GET",
        contentType: "application/json",
        success: function(data)
        {
            
            console.log(data);
            output = data.Response;
            RenderData();
            Utilities.Loader.Hide();
        }



    });
}

function RenderData()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }

    $.each(output, function (index, rowObj) {
        display.push([
        index + 1,
       rowObj.accountGroupName,
       '&#8358;' + Utilities.FormatCurrency(rowObj.Opening),
       '&#8358;'+Utilities.FormatCurrency(rowObj.Debit),
       '&#8358;' + Utilities.FormatCurrency(rowObj.Credit),
       '&#8358;' + Utilities.FormatCurrency(rowObj.Balance)
        
        ])
    });
    table = $('#CashorBankList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

}