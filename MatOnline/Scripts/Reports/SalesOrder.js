﻿var count = 0;
var totalLineItemAmount = 0.0;
var totalTaxAmount = 0.0;

var products = [];
var searchResult = {};
var salesOrderLineItems = [];
var customers = [];
var salesMen = [];
var applyOn = [];
var pricingLevel = [];
var currencies = [];
var units = [];
var batches = [];
var stores = [];
var racks = [];
var tax = {};
var quotationMasterId = 0;
var quotationDetailsId = 0;
var allTaxes = [];
var today = new Date();
$(function () {

    getLookUps();
    getSalesOrderMasterLastRowDetails()
    
    $("#dueDays").attr("disabled", "disabled");
    $("#quotationNo").attr("disabled", "disabled");
    $("#orderNo").attr("disabled", "disabled");
    

    $("#dueDate").change(function () {
        var transDate = new Date($("#transactionDate").val());
        var dueDate = new Date($("#dueDate").val());
        var timeDiff = Math.abs(dueDate.getTime() - transDate.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        $("#dueDays").val(diffDays);
    });

    $("#applyOn").change(function ()
    {
        if ($("#applyOn").val() != "NA")
        {
            getQuotationNumbers($("#customers").val(), $("#applyOn").val());
            $("#quotationNo").removeAttr("disabled");
            $("#btnAddLineItemModal").attr("disabled", "disabled");
        }
        else
        {
            $("#quotationNo").attr("disabled", "disabled");
            $("#btnAddLineItemModal").removeAttr("disabled");
        }
    });
    $("#customers").change(function ()
    {
        if ($("#applyOn").val() != "NA")
        {
            getQuotationNumbers($("#customers").val(), $("#applyOn").val());
        }
    });
    $("#searchByProductName").on("change", function () {
        searchProduct($("#searchByProductName").val(), $("#store").val(), "ProductName");
    });
    $("#searchByProductCode").on("change", function () {
        searchProduct($("#searchByProductCode").val(), $("#store").val(), "ProductCode");
    });
    $("#searchByBarcode").on("change", function () {
        searchProduct($("#searchByBarcode").val(), $("#store").val(), "Barcode");
    });
    $("#quantityToAdd").on("change", function () {
        var rate = $("#rate").val();
        var qty = $("#quantityToAdd").val();
        var amount = rate * qty;
        $("#amount").val(amount);
    });
    $("#rate").on("change", function () {
        var rate = $("#rate").val();
        var qty = $("#quantityToAdd").val();
        var amount = rate * qty;
        $("#amount").val(amount);
    });
    $("#quotationNo").on("change", function () {
        if ($("#quotationNo").val() != "NA")
        {
            getOrderDetails($("#quotationNo").val(), 0);
        }        
    });
    $("#store").on("change", function () {
        if ($("#store").val() != "") {
            $("#quantityToAdd").removeAttr("disabled");
        }
        else
        {
            $("#quantityToAdd").prop("disabled","disabled");
        }
    });
      
    $('#itemLineModal').on('show.bs.modal', function (e) {
        if ($("#store").val() == "") {
            Utilities.ErrorNotification("Please select store to pick item from.");
            e.preventDefault();
        }
    })

    var sl = 1;
    $("#addLineItem").click(function () {
        var qty = $("#quantityToAdd").val();
        //var rate = searchResult.salesRate;
        var rate = $("#rate").val();
        var gross = (qty * rate);
        var discount = 0; //discount percent
        var netAmount = gross - (discount / 100.00);
        var taxAmount = (netAmount * 0.05).toFixed(2);//searchResult.taxId == tax.TaxId ? (netAmount * 0.05).toFixed(2) : 0;
        var amount = parseFloat(gross) + parseFloat(taxAmount);

        if (qty == 0 || "") {
            Utilities.ErrorNotification("You need to fill the quantity field.")
            return false;
        }
        
        salesOrderLineItems.push({
            SlNo: sl,
            ProductId: searchResult.productId,
            ProductBarcode: searchResult.productCode,
            ProductCode: searchResult.productCode,
            ProductName: searchResult.productName,
            itemDescription: searchResult.narration,
            Brand: searchResult.brandName,
            Qty: qty,
            UnitId: searchResult.unitId,
            UnitConversionId: searchResult.unitConversionId,
            Extra1:$("#store").val(), //searchResult.godownId,
            RackId: searchResult.rackId,
            BatchId: searchResult.batchId,
            TaxId:tax.TaxId,
            Tax: searchResult.taxId == tax.TaxId ? tax.TaxName : "NA",
            TaxAmount: taxAmount,
            Rate: rate,
            Amount: amount
        });
        sl += 1;
        renderLineItems();
        $("#searchByBarcode").val("");
        $("#searchByBarcode").trigger("chosen:updated");
        $("#searchByProductCode").val("");
        $("#searchByProductCode").trigger("chosen:updated");
        $("#searchByProductName").val("");
        $("#searchByProductName").trigger("chosen:updated");
        $("#rate").val("");
        $("#quantityInStock").val(""); 
        $("#storeQuantityInStock").val(""); 
        $("#amount").val(""); 
        $("#description").val("");
        $("#quantityToAdd").val(0);
        
    });

    $("#save").click(function () {
        save();
    });
});

function getLookUps()
{
    Utilities.Loader.Show();
    var productsAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: "Get",
        contentType:"application/json"
    });

    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetLookups",
        type: "Post",
        contentType: "application/json"
    });

    $.when(productsAjax, lookUpAjax)
    .done(function (dataProducts, dataLookUps) {
        products = dataProducts[2].responseJSON;
        customers = dataLookUps[2].responseJSON.Customers;
        salesMen = dataLookUps[2].responseJSON.SalesMen;
        applyOn = dataLookUps[2].responseJSON.ApplyOn.splice(1, 1);
        pricingLevel = dataLookUps[2].responseJSON.PricingLevel;
        currencies = dataLookUps[2].responseJSON.Currencies;
        units = dataLookUps[2].responseJSON.Units;
        batches = dataLookUps[2].responseJSON.Batches;
        stores = dataLookUps[2].responseJSON.Stores;
        racks = dataLookUps[2].responseJSON.Racks;
        tax = dataLookUps[2].responseJSON.Tax;

        //remove sales quotation from apply on
        //var indexOfObjectToRemove = applyOn.findIndex(p=>p.voucherTypeId == 15);
        //applyOn.splice(0, 1);

        //$("#customers").html(Utilities.PopulateDropDownFromArray(customers,1,0));
        $("#customers").kendoDropDownList({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: {
                data: customers
            },
            filter: "contains",
        });
        $("#currency").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));
        //$("#salesMan").html("<option value=\"-1\">--Select--</option>"+Utilities.PopulateDropDownFromArray(salesMen, 0, 1));
        $("#store").html("<option value=\"\">--Select--</option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
        $("#applyOn").html("<option value=\"NA\">NA</option>" + Utilities.PopulateDropDownFromArray(applyOn, 0, 1));
        $("#pricingLevel").html("<option value=\"-1\">--Select--</option>" + Utilities.PopulateDropDownFromArray(pricingLevel, 0, 1));
        //$("#searchByProductName").html(Utilities.PopulateDropDownFromArray(products, 3, 3));
        //$("#searchByBarcode").html(Utilities.PopulateDropDownFromArray(products, 0, 0));
        //$("#searchByProductCode").html(Utilities.PopulateDropDownFromArray(products, 2, 2));
        $("#searchByProductName").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 3, 3));
        $("#searchByBarcode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 0, 0));
        $("#searchByProductCode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 2, 2));
        $("#searchByProductName").chosen({ width: "100%", margin: "1px" });
        $("#searchByBarcode").chosen({ width: "100%", margin: "1px" });
        $("#searchByProductCode").chosen({ width: "100%", margin: "1px" });


        Utilities.Loader.Hide();        
    });
}

function getQuotationNumbers(customerId, applyOn) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetQuotationNumbers?customerId=" + customerId + "&applyOn=" + applyOn + "&currentUserId=" + matUserInfo.UserId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            $("#quotationNo").html("<option value=\"NA\">-Select-</option>"+Utilities.PopulateDropDownFromArray(data, 0, 1));
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getOrderDetails(quotationNumber,salesOrderMasterId) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetOrderDetails?quotationNumber=" + quotationNumber + "&salesOrderMasterId=" + salesOrderMasterId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
//            console.log(data);
            quotationMasterId = data.Master[0].quotationMasterId;
            var details = data.Details;
            console.log("details",details);
            populateOrderMasterDetails(data.Master[0]);
            salesOrderLineItems = [];

            var sl = 1;
            var globalStoreId = details[0].extra1;
            var globalStore = findStore(globalStoreId);
            $("#store").val(globalStoreId);
            $("#store :selected").text(globalStore.godownName);
            //console.log(globalStore);
            //for (i = 0; i < details.length; i++)
            $.each(details, function (count, row) {
                var quotationDetailsId = row.quotationDetailsId;
                var prodId = row.productId;
                var prodQty = row.qty;
                var storeId = row.extra1;
                //console.log(storeId);
                var productDetails = findProduct(prodId);
                searchResult = productDetails;
                var rate = searchResult.SalesRate*prodQty;
                var taxAmount = searchResult.TaxId == tax.TaxId ? (rate * 0.05).toFixed(2) : 0;
                var amount = parseFloat(rate) + parseFloat(taxAmount);
                salesOrderLineItems.push({
                    SlNo: sl,
                    ProductId: searchResult.ProductId,
                    ProductBarcode: searchResult.ProductCode,
                    ProductCode: searchResult.ProductCode,
                    ProductName: searchResult.ProductName,
                    itemDescription: searchResult.Narration,
                    Brand: searchResult.BrandId,
                    Qty: prodQty,
                    UnitId: searchResult.UnitId,
                    UnitConversionId: searchResult.unitConversionId,
                    Extra1: storeId,
                    RackId: searchResult.RackId,
                    BatchId: searchResult.BatchId,
                    TaxId: searchResult.taxId,
                    Tax: tax.TaxName,
                    TaxAmount: taxAmount,
                    Rate: Utilities.FormatCurrency(searchResult.SalesRate),
                    Amount: amount,
                    QuotationDetailsId: quotationDetailsId
                    //quotationDetailsId
                });
                sl = sl + 1;
            });
            //console.log(salesOrderLineItems);
            renderLineItems();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getSalesOrderMasterLastRowDetails() {
    console.log(4658);
   // Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetSalesOrderMasterLastRowDetails",
        type: "POST",
        contentType: "application/json",
        success: function (data) {
            console.log(data); //return;
            $("#orderNo").val(data);
            
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong getting new order number.");
            Utilities.Loader.Hide();
        }
    });
}

function populateOrderMasterDetails(orderMaster)
{
    $("#orderNo").val(orderMaster.invoiceNo);
    $("#salesMan").val(orderMaster.employeeId).change();
    $("#pricingLevel").val(orderMaster.pricinglevelId).change();
}

function populateOrderNumbersOrReceiptNumbers(purchaseMode)
{
    if (purchaseMode == "NA")
    {
        $("#applyOn").attr("disabled", "disabled");
        $("#orderNo").attr("disabled", "disabled");
    }
    else if (purchaseMode == "Against Purchase Order")
    {
        $("#applyOn").removeAttr("disabled");
        $("#orderNo").removeAttr("disabled");

        $("#applyOn").html(Utilities.PopulateDropDownFromArray(purchaseOrderVoucherType, 0, 1));
        getOrderNumbers($("#suppliers").val(), 10);
    }
    else if (purchaseMode == "Against Material Receipt")
    {
        $("#applyOn").removeAttr("disabled");
        $("#orderNo").removeAttr("disabled");

        $("#applyOn").html(Utilities.PopulateDropDownFromArray(materialReceiptVoucherType, 0, 1));
        getReceiptNumbers($("#suppliers").val(), 11);
    }
}

function searchProduct(filter,storeId,searchBy) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/SearchProduct?filter=" + filter + "&searchBy=" + searchBy + "&storeId=" + storeId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            //console.log(tax); //return;
            var productDetails = data.Product[0];
            searchResult = productDetails; 
            
            $("#searchByBarcode").val(searchResult.productCode);
            $("#searchByBarcode").trigger("chosen:updated");
            $("#searchByProductCode").val(searchResult.productCode);
            $("#searchByProductCode").trigger("chosen:updated");
            $("#searchByProductName").val(searchResult.productName);
            $("#searchByProductName").trigger("chosen:updated");
           // $("#searchByProductName").data('kendoDropDownList').value(filter);
           // $("#searchByProductName").val(filter);
           // $("#searchByProductCode").val(filter);
           // $("#searchByBarcode").val(filter);
            $("#rate").val(productDetails.salesRate);
            $("#description").val(data.Narration);
            $("#amount").val(($("#rate").val() * $("#quantityToAdd").val()));
           // $("#description").val(productDetails.narration);

            if (data.QuantityInStock > 0) {
                $("#quantityInStock").css("color", "black");
                $("#quantityInStock").val(data.QuantityInStock);
            }
            else {
                $("#quantityInStock").css("color", "red");
                $("#quantityInStock").val("OUT OF STOCK!");
            }

            if (data.StoreQuantityInStock > 0) {
                $("#storeQuantityInStock").css("color", "black");
                $("#storeQuantityInStock").val(data.StoreQuantityInStock);
            }
            else {
                $("#storeQuantityInStock").css("color", "red");
                $("#storeQuantityInStock").val("OUT OF STOCK!");
            }

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function renderLineItems() {
    totalLineItemAmount = 0.0;
    var output = "";
    $.each(salesOrderLineItems, function (count, row) {
        //console.log("it is", row.Extra1);
        output +=
            '<tr>\
                <td style="white-space: nowrap;"><button onclick="removeLineItem('+ row.ProductId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
                <td style="white-space: nowrap;"><button onclick="Utilities.ShowStockCardDialog(' + row.ProductId + ')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></button></td>\
                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                <td style="white-space: nowrap;">' + row.ProductBarcode + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductCode + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductName + '</td>\
                <td style="white-space: nowrap;">' + ((row.itemDescription == undefined) ? "NA" : row.itemDescription) + '</td>\
                <td style="white-space: nowrap;">'+ row.Qty + '</td>\
                <td style="white-space: nowrap;">' + findUnit(row.UnitId).unitName + '</td>\
                <td style="white-space: nowrap;">'+ findStore(row.Extra1).godownName + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.TaxAmount) + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.Rate) + '</td>\
                <td style="white-space: nowrap;">'+ Utilities.FormatCurrency(row.Amount) + '</td>\
            </tr>\
            ';
        totalLineItemAmount = (totalLineItemAmount + row.Amount);
        //totalTaxAmount = (totalTaxAmount + parseFloat(row.TaxAmount));
    });
    $("#billAmount").val(Utilities.FormatCurrency(totalLineItemAmount));
    $("#grandTotal").val(Utilities.FormatCurrency(totalLineItemAmount + getTotalTaxAmount()));
    $("#salesOrderLineItemTbody").html(output);
    $("#taxAmountHtml").val(Utilities.FormatCurrency(getTotalTaxAmount()));
    settings.ApplySettings();
}

function removeLineItem(productId) {
    if (confirm("Remove this item?"))
    {
        var indexOfObjectToRemove = salesOrderLineItems.findIndex(p=>p.ProductId == productId);
        salesOrderLineItems.splice(indexOfObjectToRemove, 1);
        renderLineItems();
    }
}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}

function findProduct(productId)
{
    var output = {};
    for(i=0;i<products.length;i++)
    {
        if(products[i].ProductId==productId)
        {
            output = products[i];
            break;
        }
    }
    return output;
}

function findSalesMan(employeeId)
{
    var output = {};
    for(i=0;i<salesMen.length;i++)
    {
        if(salesMen[i].employeeId==employeeId)
        {
            output = salesMen[i];
            break;
        }
    }
    return output;
}

function accountLedgerDropDownEditor(container, options) {
    $('<input required name="' + options.field + '" data-text-field="ledgerName" data-value-field="ledgerId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: {
                data: accountLedgers
            },
            template: '<span>#: ledgerName #</span>',
            filter: "contains",
        });
}

function getAccountLedger(ledgerId) {
    for (i = 0; i < accountLedgers.length; i++) {
        if (accountLedgers[i].ledgerId == ledgerId) {
            return accountLedgers[i].ledgerName;
        }
    }
    return "";
}

function getTotalTaxAmount()
{
    var totalTax = 0.00;
    for(i=0;i<salesOrderLineItems.length;i++)
    {
        totalTax = totalTax + parseFloat(salesOrderLineItems[i].TaxAmount);
    }
    return totalTax;
}

function getTotalAdditionalCost(obj)
{
    var output = 0.0;
    for(i=0;i<obj.length;i++)
    {
        output = output + parseFloat(obj[i].amount);
    }
    totalAdditionalCost = output;
    renderLineItems();
}

function getKendoLineItemIndex(datasource,objectToFindIndex)    //function was created to remove lineitem from row wen its removed from
{                                                               //kendo grid
    for(i=0;i<datasource.length;i++)
    {
        if(objectToFindIndex.id==datasource[i].id)
        {
            return i;
        }
    }
    return -1;
}

function save()
{
    //perform validation in another boolean function which calls save() if it returns true
    if ($("#orderNo").val() == "")
    {
        Utilities.ErrorNotification("Order number required!");
        return;
    }
    else if ($("#transactionDate").val() == "")
    {
        Utilities.ErrorNotification("Transaction date required!");
        return;
    }
    else if ($("#dueDate").val() == "")
    {
        Utilities.ErrorNotification("Due date required!");
        return;
    }
    else if ($("#salesMan").val() == "-1")
    {
        Utilities.ErrorNotification("Select sales person!");
        return;
    }
    else if ($("#store").val() == "")
    {
        Utilities.ErrorNotification("Select store!");
        return;
    }
    else if (salesOrderLineItems.length < 1)
    {
        Utilities.ErrorNotification("Line item contains no record!");
        return;
    }
    else
    {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/CustomerCentre/GetCustomerBalance?ledgerId="+$("#customers").val(),
            type: "GET",
            contentType: "application/json",
            success:function(data)
            {
                //var balance = -1 * data;    //-1 to negate it and change the value to +ve
                console.log(data);
                var balance = data;
                //if (balance >= totalLineItemAmount)
                //{
                    var infoSalesOrderMaster = {
                        Date: $("#transactionDate").val(),
                        DueDate: $("#dueDate").val(),
                        LedgerId: $("#customers").val(),
                        InvoiceNo: $("#orderNo").val(),
                        VoucherNo: $("#orderNo").val(),
                        UserId: matUserInfo.UserId,
                        Cancelled: $("#cancelled").prop('checked'),
                        //SalesOrderMasterId: 0,
                        EmployeeId: $("#salesMan").val(),
                        Narration: $("#narration").val(),
                        ExchangeRateId: $("#currency").val(),
                        QuotationMasterId: 0,
                        TotalAmount: $("#billAmount").val(),
                        taxAmount: $("#taxAmountHtml").val(),
                        PricinglevelId: $("#pricingLevel").val(),
                        QuotationMasterId: quotationMasterId
                    };
                    var infoSalesOrderDetails = salesOrderLineItems;

                    //populate main object
                    var orderToSave = {
                        infoSalesOrderDetails: infoSalesOrderDetails,
                        infoSalesOrderMaster: infoSalesOrderMaster
                    };
                    //console.log(orderToSave); //return;
                    
                    $.ajax({
                        url: API_BASE_URL + "/SalesOrder/SaveSalesOrder",
                        type: "Post",
                        contentType: "application/json",
                        data: JSON.stringify(orderToSave),
                        success: function (e) {
                             Utilities.Loader.Hide();
                             if (e.ResponseCode == 200) {
                                Utilities.Loader.Hide();

                                var presentDate = today.getFullYear() + '-' + (today.getMonth() + 1) + '-' + today.getDate();
                                Utilities.SuccessNotification(e.ResponseMessage);
                                getSalesOrderMasterLastRowDetails();
                                $("#transactionDate").val(presentDate);
                                $("#dueDate").val(presentDate);
                                //$("#dueDate").val("0");
                                //$("#customers").text("");
                                $("#salesMan").text("");
                                salesOrderLineItems = [];
                                renderLineItems();
                                $("#narration").val("");
                            }
                             else if (e.ResponseCode == 400) {
                                 Utilities.Loader.Hide();
                                Utilities.ErrorNotification(e.ResponseMessage);
                            }
                        },
                        error: function (e) {
                            Utilities.Loader.Hide();
                        }
                    });
               // }
                //else
                //{
                //    Utilities.Loader.Hide();
                //    Utilities.ErrorNotification("Insufficient customer balance.<br/>Customer Balance: "+balance);
                //}
            },
            error:function(err)
            {
                Utilities.ErrorNotification("Sorry,could not get customer balance at this time.");
            }
        });
        
    }    
}

function clear()
{
    $("#voucherNo").val("");
    $("#invoiceNo").val("");
    $("#suppliers").val("");
    $("#transactionDate").val();
    $("#suppliers").val();
    $("#invoiceNo").val();
    $("#invoiceDate").val();
    $("#creditPeriod").val();
    $("#currency").val();
    $("#narration").val();
    $("#grandTotal").val("");
    $("#billAmount").val("");
    $("#taxAmountHtml").val("");
}
