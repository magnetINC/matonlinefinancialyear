﻿$("#viewReport").click(function () {

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerBalance/CustomerBalanceSummary?toDate=" + $("#toDate").val(),
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var subTotal = 0.0;
            var output = "";
            var count = 1;
            for (j = 0; j < data.length; j++)
            {
                output += '<tr>' +
                            '<td>' + count + '</td>' +
                            '<td>' + data[j].ledgerName + '</td>' +
                            '<td>&#8358;' + Utilities.FormatCurrency(data[j].openingBalance) + '</td>' +
                        '</tr>';
                count += 1;
                subTotal += data[j].openingBalance;
            }
            output += '<tr>' +
                        '<td colspan="2" style="text-align:center"> <b>SUB TOTAL</b></td>' +
                        '<td colspan="1"> <b>&#8358;' + Utilities.FormatCurrency(subTotal) + '</b></td>'
                    '</tr>';
            $("#customerBalanceTbody").html(output);
            Utilities.Loader.Hide();

            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        },
        error: function (err) {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });
});