﻿$("#viewReport").click(function ()
{
    var param =
    {
        FromDate: $("#fromDate").val(),
        ToDate: $("#toDate").val()
    };
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/FinancialStatements/GetProfitAndLossByConventional",
        type: "Post",
        data: JSON.stringify(param),
        contentType: "application/json",
        success: function (data)
        {
            console.log(data);// return;
            if (data.ResponseCode == 200)
            {
                var output = "";
                var count = 1;
                var profitAndLoss = data.Response;
                for (j = 0; j < profitAndLoss.length; j++)
                {
                    var amt1 = "";
                    var amt2 = "";
                    var acct = "";
                    if (profitAndLoss[j].TxtAmount1 == null || profitAndLoss[j].TxtAmount1 == "null")
                    {
                        amt1 = "";
                    }
                    else
                    {
                        amt1 = profitAndLoss[j].TxtAmount1;
                    }
                    if (profitAndLoss[j].TxtAmount2 == null || profitAndLoss[j].TxtAmount2 == "null")
                    {
                        amt2 = "";
                    }
                    else
                    {
                        amt2 = profitAndLoss[j].TxtAmount2;
                    }
                    if (profitAndLoss[j].TxtIncome == null || profitAndLoss[j].TxtIncome == "null")
                    {
                        acct = "";
                    }
                    else
                    {
                        acct = profitAndLoss[j].TxtIncome;
                    }

                    if (acct == "TOTAL REVENUE" || acct == "GROSS PROFIT/(LOSS)" || acct == "OPERATING PROFIT (LOSS)")
                    {
                        output += '<tr>' +
                                '<td><b>' + acct + '</b></td>' +
                                '<td>' + amt1 + '</td>' +
                                '<td><b>' + amt2 + '</b></td>' +
                            '</tr>';
                    }
                    else
                    {
                        output += '<tr>' +
                                '<td>' + acct + '</td>' +
                                '<td>' + amt1 + '</td>' +
                                '<td>' + amt2 + '</td>' +
                            '</tr>';
                    }
                    
                }
                $("#trialBalanceTbody").html(output);
            }
            else
            {
                Utilities.ErrorNotification("Could not load profit and loss!");
            }
            Utilities.Loader.Hide();
        },
        error: function (err)
        {
            Utilities.Loader.Hide();
            Utilities.ErrorNotification("Something went wrong!");
            //console.log(err)
        }
    });
});