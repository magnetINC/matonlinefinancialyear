﻿$("#viewReport").click(function () {
    var param=
    {
        ToDate: $("#toDate").val()
    };
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/FinancialStatements/GetTrialBalanceReport",
        type: "Post",
        data:JSON.stringify(param),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data.ResponseCode == 200)
            {
                var output = "";
                var count = 1;
                var trialBalance = data.Response;
                for (j = 0; j < trialBalance.length; j++)
                {
                    var ledgerName = "";
                    if (trialBalance[j].LedgerName == null || trialBalance[j].LedgerName == "null")
                    {
                        ledgerName = "";
                    }
                    else
                    {
                        ledgerName = trialBalance[j].LedgerName;
                    }

                    if (ledgerName == "TOTAL")
                    {
                        output += '<tr>' +
                                '<td></td>' +
                                '<td><b>' + ledgerName + '</b></td>' +
                                '<td><b>' + trialBalance[j].DebitDifference + '</b></td>' +
                                '<td><b>' + trialBalance[j].CreditDifference + '</b></td>' +
                            '</tr>';
                    }
                    else if (ledgerName == "")
                    {
                        output += '<tr>' +
                                '<td></td>' +
                                '<td><b>' + ledgerName + '</b></td>' +
                                '<td><b>' + trialBalance[j].DebitDifference + '</b></td>' +
                                '<td><b>' + trialBalance[j].CreditDifference + '</b></td>' +
                            '</tr>';
                    }
                    else
                    {
                        output += '<tr>' +
                                '<td>' + count + '</td>' +
                                '<td>' + ledgerName + '</td>' +
                                '<td>' + trialBalance[j].DebitDifference + '</td>' +
                                '<td>' + trialBalance[j].CreditDifference + '</td>' +
                            '</tr>';
                    }
                    
                    count += 1;
                }
                $("#trialBalanceTbody").html(output);
            }
            else
            {
                Utilities.ErrorNotification("Could not load trial balance!");
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            Utilities.ErrorNotification("Something went wrong!");
            console.log(err)
        }
    });
});