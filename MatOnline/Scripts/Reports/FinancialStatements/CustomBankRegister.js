﻿var agents = [];
var banks = [];
var register = [];

$(function () {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomBankRegister/GetLedgers",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            banks = [];
            agents = [];
            banks.push({
                ledgerId: "0",
                ledgerName: "All"
            });
            agents.push({
                ledgerId: "0",
                ledgerName: "All"
            });
            for (i = 0; i < data.Banks.length; i++)
            {
                banks.push({
                    ledgerId: data.Banks[i].ledgerId,
                    ledgerName: data.Banks[i].ledgerName
                });
            }
            for (i = 0; i < data.Agents.length; i++) {
                agents.push({
                    ledgerId: data.Agents[i].ledgerId,
                    ledgerName: data.Agents[i].ledgerName
                });
            }
            console.log(banks);
            $("#bank").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: banks
            });
            $("#agent").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: agents
            });

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });
});

$("#viewReport").click(function () {
    var param = {
        AgentId: $("#agent").val(),
        BankId: $("#bank").val(),
        FromDate: $("#fromDate").val(),
        ToDate: $("#toDate").val(),
    };
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomBankRegister/BankRegister",
        data:JSON.stringify(param),
        type: "POST",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var output = "";
            var count = 1;
            for (i = 0; i < data.length; i++)
            {
                output += '<tr>' +
                    '<td colspan="4"><b>'+data[i].BankName+'</b></td>'+
                          '</tr>';
                var bankData = data[i].AgentLists;
                var count = 1;
                output += '<tr>' +
                    '<td><b>S/N</b></td>' +
                    '<td><b>Agent Name</b></td>' +
                    '<td><b>Date</b></td>' +
                    '<td><b>Amount</b></td>' +
                          '</tr>';
                if(bankData.length<1)
                {
                    output += '<tr>' +
                                '<td colspan="4"><b>' + data[i].BankName + ' has not received payment.</b></td>' +
                              '</tr>';
                    output += '<tr>' +
                                '<td colspan="3" style="text-align:right"><b>SUB TOTAL</b></td>' +
                                '<td colspan="1" style="text-align:left"><b>0.0</b></td>'
                    '</tr>';
                }
                else
                {
                    var subTotal = 0.0;
                    for (j = 0; j < bankData.length; j++)
                    {
                        output += '<tr>' +
                        '<td>' + count + '</td>' +
                        '<td>' + bankData[j].LedgerName + '</td>' +
                        '<td>' + bankData[j].Date + '</td>' +
                        '<td>' + Utilities.FormatCurrency(bankData[j].Credit) + '</td>' +
                              '</tr>';
                        count += 1;
                        subTotal += bankData[j].Credit;
                    }
                    output += '<tr>' +
                                '<td colspan="3" style="text-align:right"><b>SUB TOTAL</b></td>' +
                                '<td colspan="1" style="text-align:left"><b>' + Utilities.FormatCurrency(subTotal) + '</b></td>'
                              '</tr>';
                }                
            }
            $("#bankRegisterTbody").html(output);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });
});