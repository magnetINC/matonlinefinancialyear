﻿$("#viewReport").click(function () {

    var fy = $("#yearF").val();

    var param =
    {
        FromDate: $("#fromDate").val(),
        ToDate: $("#toDate").val(),
        PreviousYear: $("#yearF").val(),
    };

    Utilities.Loader.Show();

    $.ajax({
        url: API_BASE_URL + "/FinancialStatements/GetBalanceSheetReport",
        type: "Post",
        data:JSON.stringify(param),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data.ResponseCode == 200)
            {
                var output = "";
                var count = 1;
                var trialBalance = data.Response;
                for (j = 0; j < trialBalance.length; j++)
                {
                    var ast = "";
                    var amt1="";
                    var amt2 = "";
                    var amt3 = "";

                    if (trialBalance[j].TxtAsset == "null" || trialBalance[j].TxtAsset == null)
                    {
                        ast = "";
                    }
                    else
                    {
                        ast = trialBalance[j].TxtAsset;
                    }

                    if (trialBalance[j].Amount1 == "null" || trialBalance[j].Amount1 == null)
                    {
                        amt1 = "";
                    }
                    else
                    {
                        amt1 = trialBalance[j].Amount1;
                    }

                    if (trialBalance[j].Amount2 == "null" || trialBalance[j].Amount2 == null)
                    {
                        amt2 = "";
                    }
                    else
                    {
                        amt2 = trialBalance[j].Amount2;
                    }

                    if (trialBalance[j].Amount3 == "null" || trialBalance[j].Amount3 == null)
                    {
                        amt3 = "";
                    }
                    else
                    {
                        amt3 = trialBalance[j].Amount3;
                    }

                    output += '<tr>' +
                                '<td>' + ast + '</td>' +
                                '<td>' + amt1 + '</td>' +
                                '<td>' + amt2 + '</td>' +
                                '<td>' + amt3 + '</td>' +
                            '</tr>';
                    //var ledgerName = "";
                    //if (trialBalance[j].LedgerName == null || trialBalance[j].LedgerName == "null")
                    //{
                    //    ledgerName = "";
                    //}
                    //else
                    //{
                    //    ledgerName = trialBalance[j].LedgerName;
                    //}

                    //if (ledgerName == "TOTAL")
                    //{
                    //    output += '<tr>' +
                    //            '<td></td>' +
                    //            '<td><b>' + ledgerName + '</b></td>' +
                    //            '<td><b>' + trialBalance[j].DebitDifference + '</b></td>' +
                    //            '<td><b>' + trialBalance[j].CreditDifference + '</b></td>' +
                    //        '</tr>';
                    //}
                    //else if (ledgerName == "")
                    //{
                    //    output += '<tr>' +
                    //            '<td></td>' +
                    //            '<td><b>' + ledgerName + '</b></td>' +
                    //            '<td><b>' + trialBalance[j].DebitDifference + '</b></td>' +
                    //            '<td><b>' + trialBalance[j].CreditDifference + '</b></td>' +
                    //        '</tr>';
                    //}
                    //else
                    //{
                    //    output += '<tr>' +
                    //            '<td>' + count + '</td>' +
                    //            '<td>' + ledgerName + '</td>' +
                    //            '<td>' + trialBalance[j].DebitDifference + '</td>' +
                    //            '<td>' + trialBalance[j].CreditDifference + '</td>' +
                    //        '</tr>';
                    //}
                    
                    count += 1;
                }
                $("#trialBalanceTbody").html(output);
            }
            else
            {
                Utilities.ErrorNotification("Could not load balance sheet!");
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            Utilities.ErrorNotification("Something went wrong!");
            //console.log(err)
        }
    });
});