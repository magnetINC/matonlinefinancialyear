﻿var typeOfVoucher = [];
var taxes = [];
var voucherType = [];
var taxReports = [];

var taxReportSearchParam = {};

var table = "";

$(function () {
    $("#transactionListForTaxReportProductwise").hide();

    getLookUps();

    $("#voucher").change(function () {
        getVoucherTypes();
    });
});

/* ======= API CALLS ======= */
function getLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/TaxReport/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            typeOfVoucher = data.Response.TypeOfVoucher;
            taxes = data.Response.Taxes;

            voucherType.push({ voucherTypeId: 0, voucherTypeName:"All"});

            renderLookUpDataToControls();
            getTaxReport();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getVoucherTypes()
{
    Utilities.Loader.Show();
    var vName = $("#voucher").data("kendoDropDownList").text();
    $.ajax({
        url: API_BASE_URL + "/TaxReport/GetVoucherTypes?voucherName=" + vName,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            voucherType = data.Response;

            $("#voucherType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: voucherType
            });

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getTaxReport()
{
    Utilities.Loader.Show();
    taxReportSearchParam.fromDate = $("#fromDate").val();
    taxReportSearchParam.toDate = $("#toDate").val();
    taxReportSearchParam.taxId = $("#tax").val();
    taxReportSearchParam.voucherTypeId = $("#voucher").val();
    taxReportSearchParam.typeOfVoucher = $("#voucherType").data("kendoDropDownList").text();
    taxReportSearchParam.billOrProductWise = $("#billOrProductWise").val();
    taxReportSearchParam.inputOrOutput = $("#inputOrOutput").val();
    
    console.log(taxReportSearchParam);
    $.ajax({
        url: API_BASE_URL + "/TaxReport/GetTaxReport",
        type: "POST",
        data: JSON.stringify(taxReportSearchParam),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            taxReports = data.Response;
            objToShow = [];

            if(table != "")
            {
                table.destroy();
            }
            if (taxReportSearchParam.billOrProductWise == "Bill Wise")
            {
                $("#transactionListForTaxReportBillWise").show();
                $("#transactionListForTaxReportProductwise").hide();
                $.each(taxReports, function (count, row) {
                    objToShow.push([
                        count + 1,
                        row.Date,
                        row.TypeofVoucher,
                        row.VoucherNo,
                        row.CashOrParty,
                        row.TIN,
                        row.CST,
                        '&#8358;' + Utilities.FormatCurrency(row.BillAmount),
                        '&#8358;' + Utilities.FormatCurrency(row.TaxAmount),
                        '&#8358;' + Utilities.FormatCurrency(row.CessAmount),
                    ]);
                });
                table = $('#transactionListForTaxReportBillWise').DataTable({
                    data: objToShow,
                    "responsive": true,
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
            }
            else {
                $("#transactionListForTaxReportBillWise").hide();
                $("#transactionListForTaxReportProductwise").show();
                $.each(taxReports, function (count, row) {
                    objToShow.push([
                        count + 1,
                        row.Date,
                        row.TypeofVoucher,
                        row.BillNo,
                        row.Item,
                        '&#8358;' + Utilities.FormatCurrency(row.BillAmount),
                        row.TaxPercent,
                        '&#8358;' + Utilities.FormatCurrency(row.TaxAmount),
                        '&#8358;' + Utilities.FormatCurrency(row.TotalAmount),
                    ]);
                });
                table = $('#transactionListForTaxReportProductwise').DataTable({
                    data: objToShow,
                    "responsive": true,
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
            }

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

/* ====== RENDER DATA TO CONTROLS ======= */
function renderLookUpDataToControls()
{
    $("#tax").kendoDropDownList({
        filter: "contains",
        dataTextField: "taxName",
        dataValueField: "taxId",
        dataSource: taxes
    });
    $("#voucher").kendoDropDownList({
        filter: "contains",
        dataTextField: "typeOfVoucher",
        dataValueField: "voucherTypeId",
        dataSource: typeOfVoucher
    });
    $("#voucherType").kendoDropDownList({
        filter: "contains",
        dataTextField: "voucherTypeName",
        dataValueField: "voucherTypeId",
        dataSource: voucherType
    });
}

function exportExcel()
{
    window.open('/Report/generalreports/ExportTaxReport?fdate=' + $("#fromDate").val() + '&tdate=' + $("#toDate").val() + '&vid=' + $("#voucher").val() + '&vname=' + $("#voucherType").data("kendoDropDownList").text() + '&tid=' + $("#tax").val() + '&bOrP=' + $("#billOrProductWise").val() + '&iOrO=' + $("#inputOrOutput").val(), '_blank');
}