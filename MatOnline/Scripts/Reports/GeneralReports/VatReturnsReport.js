﻿var typeOfVoucher = [];
var taxes = [];
var voucherType = [];
var vatReturns = [];

var VATReturnSearchParam = {};

var table = "";

$(function () {
    $("#transactionListForVatReturnsFormat2").hide();
    $("#netAmountFormat2").hide();

    getLookUps();

    $("#voucher").change(function () {
        getVoucherTypes();
    });
});

/* ======= API CALLS ======= */
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/VatReturnsReport/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            typeOfVoucher = data.Response.TypeOfVoucher;
            taxes = data.Response.Taxes;

            voucherType.push({ voucherTypeId: 0, voucherTypeName: "All" });

            renderLookUpDataToControls();
            getVatReturns();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getVoucherTypes() {
    Utilities.Loader.Show();
    var vName = $("#voucher").data("kendoDropDownList").text();
    $.ajax({
        url: API_BASE_URL + "/VatReturnsReport/GetVoucherTypes?voucherName=" + vName,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            voucherType = data.Response;

            $("#voucherType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: voucherType
            });

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getVatReturns() {
    Utilities.Loader.Show();
    VATReturnSearchParam.fromDate = $("#fromDate").val();
    VATReturnSearchParam.toDate = $("#toDate").val();
    VATReturnSearchParam.tax = $("#tax").data("kendoDropDownList").text();;
    VATReturnSearchParam.voucherTypeId = $("#voucher").val();
    VATReturnSearchParam.typeOfVoucher = $("#voucherType").data("kendoDropDownList").text();
    VATReturnSearchParam.format = $("#format").val();

    console.log(VATReturnSearchParam);
    $.ajax({
        url: API_BASE_URL + "/VatReturnsReport/GetVatReturns",
        type: "POST",
        data: JSON.stringify(VATReturnSearchParam),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            taxReports = data.Response;
            objToShow = [];

            var netTax = 0;
            if (table != "") {
                table.destroy();
            }
            if (VATReturnSearchParam.format == "type1") {
                $("#transactionListForVatReturnsFormat1").show();
                $("#transactionListForVatReturnsFormat2").hide();
                $("#netAmountFormat2").hide();
                $.each(taxReports, function (count, row) {
                    objToShow.push([
                        count + 1,
                        row.voucherName,
                        row['Invoice No'],
                        row.Date,
                        row['Party Name'],
                        row['Mailing Name'],
                        row['Tin No'],
                        '&#8358;' + Utilities.FormatCurrency(row['Sales Amound']),
                        '&#8358;' + Utilities.FormatCurrency(row['Tax Amount']),
                        '&#8358;' + Utilities.FormatCurrency(row['Net Amount']),
                        '&#8358;' + Utilities.FormatCurrency(row.billDiscount),
                        '&#8358;' + Utilities.FormatCurrency(row.grandtotal)
                    ]);
                });
                table = $('#transactionListForVatReturnsFormat1').DataTable({
                    data: objToShow,
                    "responsive": true,
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
                $("#companyName").removeAttr("hidden");
                $("#companyAddress").removeAttr("hidden");
            }
            else {
                $("#transactionListForVatReturnsFormat1").hide();
                $("#transactionListForVatReturnsFormat2").show();
                $("#netAmountFormat2").show();
                $.each(taxReports, function (count, row) {
                    objToShow.push([
                        count + 1,
                        row.voucherName,
                        row['Invoice No'],
                        row.Date,
                        row['Party Name'],
                        row['Mailing Name'],
                        row['Tin No'],
                        '&#8358;' + Utilities.FormatCurrency(row.grandtotal),
                        row.TaxName,
                        row.rate,
                        '&#8358;' + Utilities.FormatCurrency(row.TaxAmount),
                    ]);

                    if (row.voucherType == "Sales Invoice") {
                        netTax = netTax + row.TaxAmount;
                    }
                    if (row.voucherType == "Sales Return") {
                        netTax = netTax - row.TaxAmount;
                    }
                    if (row.voucherType == "Purchase Invoice") {
                        netTax = netTax - row.TaxAmount;
                    }
                    if (row.voucherType == "Purchase Return") {
                        netTax = netTax + row.TaxAmount;
                    }

                });
                table = $('#transactionListForVatReturnsFormat2').DataTable({
                    data: objToShow,
                    "responsive": true,
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
                console.log(netTax);
                if(netTax > 0)
                {
                    $("#netTaxPayable").val(Utilities.FormatCurrency(netTax));
                }
                else
                {
                    $("#netTaxPayable").val(Utilities.FormatCurrency(netTax * -1));
                }
                $("#companyName").removeAttr("hidden");
                $("#companyAddress").removeAttr("hidden");
            }

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

/* ====== RENDER DATA TO CONTROLS ======= */
function renderLookUpDataToControls() {
    $("#tax").kendoDropDownList({
        filter: "contains",
        dataTextField: "taxName",
        dataValueField: "taxId",
        dataSource: taxes
    });
    $("#voucher").kendoDropDownList({
        filter: "contains",
        dataTextField: "typeOfVoucher",
        dataValueField: "voucherTypeId",
        dataSource: typeOfVoucher
    });
    $("#voucherType").kendoDropDownList({
        filter: "contains",
        dataTextField: "voucherTypeName",
        dataValueField: "voucherTypeId",
        dataSource: voucherType
    });
}

function exportExcel() {
    window.open('/Report/generalreports/ExportVatReturns?fdate=' + $("#fromDate").val() + '&tdate=' + $("#toDate").val() + '&vid=' + $("#voucher").val() + '&vname=' + $("#voucherType").data("kendoDropDownList").text() + '&tax=' + $("#tax").data("kendoDropDownList").text() + '&format=' + $("#format").val(), '_blank');
}