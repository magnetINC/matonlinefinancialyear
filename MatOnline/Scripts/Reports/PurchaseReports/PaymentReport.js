﻿var mainOutput = [];
var voucherTypes = [];
var acctLedgerTypes = [];
var cashBanks = [];
var table = "";

$(function () {
    getCashorBanks();
    getAcctDetails();
    getVoucherDetails();

})
//finish up ajax

function getCashorBanks()
{
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetCashorBank",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            cashBanks = data.Response;
            $("#cb").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: cashBanks,
                optionLabel: "All"
            })
        }

    });
}

function getAcctDetails() {
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetAcctLedgerDetails",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            acctLedgerTypes = data.Response;
            $("#aLedger").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: acctLedgerTypes,
                optionLabel: "All"
            })
        }

    });
}

function getVoucherDetails() {
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetVoucherDetails",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            voucherTypes = data.Response;
            $("#formType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: voucherTypes,
                optionLabel: "All"
            })
        }

    });
}

function paymentReportDetails()
{
    if ($('#formType').val() == "" || $('#formType').val() == null)
    {
        $('#formType').val(0);
    }
    if ($('#cb').val() == "" || $('#cb').val() == null) {
        $('#cb').val(0);
    }
    if ($('#aLedger').val() == "" || $('#aLedger').val() == null) {
        $('#aLedger').val(0);
    }

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetPaymentReportDetails?fromDate=" + $('#fromDate').val() + "&toDate=" + $('#toDate').val() + "&formType=" + $('#formType').val() + "&cashorBank=" + $('#cb').val() + "&acctLedger=" + $('#aLedger').val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            mainOutput = data.Response;
            renderData();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        }

    });
    Utilities.Loader.Hide();
}

function renderData()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }

    $.each(mainOutput, function (index, row){
        display.push([
            index + 1,
            row.voucherTypeName,
            row.voucherNo,
            row.date,
            row.ledgerName,
            '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
            row.userName,
            row.narration
        ])
    });

    table = $('#PaymentReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}