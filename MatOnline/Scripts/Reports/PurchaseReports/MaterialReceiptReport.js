﻿var mainOutput = [];
var table = "";
var searchInputs = {};
var vouchers = [];
var suppliers = [];
var invoices = [];


$(document).ready(function () {
  
})

$(function () {
    
    getVoucherType();
    getsuppliers();
    //to load up orderNo select when a suplier is selected
    $('#supplier').change(function () {
        //alert("wollup");
        getOrderNo();
    });
   
    //get oda fill ups and finish up 
    //still not gotten other fill ups
});

function getsuppliers()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetSuppliers",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            suppliers = data.Response;
            $("#supplier").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: suppliers

            });
        }
    });
}

    function getVoucherType()//formType
    {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PurchaseReports/GetVoucherTypes",
            type: "GET",
            contentType: "application/json",
            success:function(data)
            {
                console.log(data);
                vouchers = data.Response;
                $("#formType").kendoDropDownList({
                    filter: "contains",
                    dataTextField: "voucherTypeName",
                    dataValueField: "voucherTypeId",
                    dataSource: vouchers

                });
                Utilities.Loader.Hide();
            }
        });

    }

    function getOrderNo()//invoiceNo or orderNo
    {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PurchaseReports/GetVoucherTypes?supplierId="+ $('#supplier').val(),
            type: "GET",
            contentType: "application/json",
            success:function(data)
            {
                console.log(data);
                invoices = data.Response;
                $("#orderNo").kendoDropDownList({
                    filter: "contains",
                    dataTextField: "voucherTypeName",
                    dataValueField: "voucherTypeId",
                    dataSource: invoices

                });
                Utilities.Loader.Hide();
            }

        });
    }

    function getSearchInputs()
    {
       
        searchInputs.fromDate =  $('#fromDate').val();
        searchInputs.toDate = $('#toDate').val();
        if ($('#supplier').val() == 0)
        {
            searchInputs.suppliers = -1;
        }
        else {
            searchInputs.suppliers = $('#supplier').val();
        }
        if ($('#formType').val() == 0) {
            searchInputs.formType = -1;
        }
        else {
            searchInputs.formType = $('#formType').val();
        }
        searchInputs.status = $('#status').val();  
        searchInputs.orderNo = $('#orderNo').val();

        console.log(searchInputs);

    }

    function GetMaterialReceiptReportDetails()//main for search button
    {
        getSearchInputs();
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PurchaseReports/MaterialReceiptReportDetails",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(searchInputs),
            success:function(data)
            {
                console.log(data);
                mainOutput = data.Response;
                renderData();
                Utilities.Loader.Hide();
                $("#companyName").removeAttr("hidden");
                $("#companyAddress").removeAttr("hidden");
                //api returns empty data so cannot view returned data
            }
         });
    }

    function renderData()
    {
        var display = [];
        //render to table
        if(table != "")
        {
            table.destroy();
        }

        $.each(mainOutput, function (index, row) {
            display.push([
                index + 1,
                row.invoiceNo,
                row.voucherTypeName,
                row.date,
                row.ledgerName,
                row.OrderNo,
                '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
                row.narration,
                row.userName
                //'<button class="btn btn-sm btn-primary" >view details</button>'
            ])
        });
        table = $('#MaterialReceiptReportList').DataTable({
            data: display,
            "paging": true,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
        Utilities.Loader.Hide();
    }

