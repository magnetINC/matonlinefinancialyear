﻿var mainOutput = [];
var searchInputs = {};
var table = "";
var suppliers = [];
var voucherTypes = [];


$(function () {

    getSuppliers();
    getVoucherTypes();

});

function getSuppliers()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetSuppliersForPurchaseOrder",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            suppliers = data.Response;
            $("#supplier").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: suppliers
                
            });
            Utilities.Loader.Hide();
        }

    });


}

function getVoucherTypes()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetAllVoucherType",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            voucherTypes = data.Response;
            $("#voucherType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: voucherTypes
            });
            Utilities.Loader.Hide();
        }      

    });
}

//main function for search inputs
function GetPurchaseOrderDetails()
{
    getSearchInputs();
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/PurchaseOrderReport",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(searchInputs),
        success: function (data) {
            console.log(data);
            mainOutput = data.Response;
            renderData();
            Utilities.Loader.Hide();
            getTotalAmount();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        }
    });
}

function getSearchInputs()
{
    
    if ($('#supplier').val() == 0) {
        $('#supplier').val(-1);
    }
    else {
        $('#supplier').val();
    }
    if ($('#voucherType').val() == 0 ) {
        $('#voucherType').val(-1);
    }
    else {
        $('#voucherType').val();
    }

    if ($('#status').val() == 0 || $('#status').val() == -1) {
        $('#status').val("All");
    }
    else {
        $('#status').val();
    }

    searchInputs.fromDate = $('#fromDate').val();
    searchInputs.toDate = $('#toDate').val();
    searchInputs.suppliers = $('#supplier').val();
    searchInputs.voucherType = $('#voucherType').val();
    searchInputs.status = $('#status').val();
    
    console.log(searchInputs);
}

function renderData()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }

    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.voucherTypeName,
            row.purchaseOrderMasterId,
            row.date,
            row.ledgerName,
            row.currencyName,
            '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
            row.dueDate,
            row.userName

        ])

        

    });

    table = $('#PurchaseOrderList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,   
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

    
}

function getTotalAmount()
{
    var tamount = 0;
    //console.log(display);
    for (i = 0; i < mainOutput.length; i++)
    {
        tamount +=  mainOutput[i].totalAmount;
    }

    $('#totAmount').val(Utilities.FormatCurrency(tamount));

}