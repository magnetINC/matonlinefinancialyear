﻿var table = "";
var mainInput = [];
var searchInput = {};
var suppliers = [];
var formtypes = [];
var products = [];

$(function () {
    getSuppliers();
    getFormTypes();
    getProducts();
})

function getSuppliers()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetAllSuppliers",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            suppliers = data.Response;
            $("#supplier").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: suppliers
            })
            Utilities.Loader.Hide();
        }
    });
}

function getFormTypes()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetVoucherTypesForPurchaseReturns",
        //url: API_BASE_URL + "/PurchaseReturn/GetVoucherTypes",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            formtypes = data.Response;
            $("#formType1").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: formtypes
            })
            Utilities.Loader.Hide();
        }

    });

}


function getProducts() {
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetProductsForPurchaseReturns",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            products = data.Response;
            $("#product").kendoDropDownList({
                filter: "contains",
                dataTextField: "productName",
                dataValueField: "productCode",
                dataSource: products
            })
        }
    });

}


function getSearchInputs()
{
    searchInput.fromDate = $('#fromDate').val();
    searchInput.toDate = $('#toDate').val();
    searchInput.supplier = $('#supplier').val();
    searchInput.formType = $('#formType1').val();
    
    searchInput.product = $('#product').val();
    console.log(searchInput);


}

//for main search
function GetPurchaseReturnReportDetails()
{
    getSearchInputs();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/PurchaseReturnReport",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(searchInput),
        success:function(data)
        {
            console.log(data);
            mainInput = data.Response;
            RenderData();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        }
    });
}

function RenderData()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }

    $.each(mainInput, function (index, row) {
        display.push([
            index + 1,
            row.voucherTypeId,
            row.voucherNo,
            row.ledgerName,
            row.date,
            '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
            row.userName
        ])
    });

    table = $('#PurchaseReturnReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}
//'&#8358;' + Utilities.FormatCurrency(row.totalAmount)
function getTotalAmount()
{
    var amount = 0;
    for(i = 0;i<mainInput.length;i++)
    {
        amount += mainInput[i].totalAmount;
    }
    $("#totAmount").val(Utilities.FormatCurrency(amount));
}
