﻿var table = "";
var mainOuptut = [];
var ledgers = [];
var searchInputs = {};

$(function () {
    getLedgers();
})

function getLedgers()
{
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetAccountLedgers",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            ledgers = data.Response;
            $("#aledger").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: ledgers
            })
        }
    });
}


//reports for search

function GetAgeingReportDetails() {
    Utilities.Loader.Show();
    getSearchInputs();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetAgeingReportDetails",
        type: "POST",
        data: JSON.stringify(searchInputs),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            mainOutput = data.Response;
            RenderData();
            Utilities.Loader.Hide();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        }
    });
}

function getSearchInputs() {

    searchInputs.ageing = $('#ageingDate').val();
    searchInputs.acctLedger = $('#aledger').val();

    console.log(searchInputs);
}

function RenderData() {
    var finalDisplay = [];
    if (table != "") {
        table.destroy();
    }

    $.each(mainOutput, function (index, rowObj) {
        finalDisplay.push([
            //put output data
            index + 1,
            rowObj.voucherTypeName,
            rowObj.purchaseOrderMasterId,

        ]);

    });
    table = $('#AgeingReportList').DataTable({
        data: finalDisplay,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

}