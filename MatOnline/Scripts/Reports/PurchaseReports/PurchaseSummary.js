﻿var suppliers = [];
var formTypes = [];
var products = [];
var table = "";
var mainOutput = [];
var  searchInput = {};//to get search parameters


$(function () {
    
    GetFormType();
    getProducts();
    getSuppliers();
});

function getProducts()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetAllProducts",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            products = data.Response;
            $("#productName").kendoDropDownList({
                filter: "contains",
                dataTextField: "productName",
                dataValueField: "productName",
                dataSource: products,
                optionLabel: "All"
                
            });

        }
    });
}

//removed option label of "ALL"

function GetFormType()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetAllFormType",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            formTypes = data.Response;
            $("#formType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: formTypes,
               
            });
            Utilities.Loader.Hide();
        }
    });

}

function getSuppliers()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetAllSuppliers",
        type: "GET",
        contentType: "application/json",
        success: function(data)
        {
            console.log(data);
            suppliers = data.Response;
            $("#supplier").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: suppliers,
                
            });
            
        }

    });
}
//to get data based on users inputs

function getUserInputs() {
    if ($('#formType').val() == "") {
        searchInput.formType = 0;
    }
    else {
        searchInput.formType = $('#formType').val();
    }
    if ($('#supplier').val() == "") {
        searchInput.supplier = 0;
    }
    else {
        searchInput.supplier = $('#supplier').val();
    }
    searchInput.fromDate = $('#fromDate').val();
    searchInput.toDate = $('#toDate').val();
    searchInput.purchaseMode = $('#forMode').val();


    searchInput.strColumn = $('#forType').val();
    searchInput.Status = $('#forStatus').val();

    searchInput.productName = $('#productName').val();
    if (searchInput.forType == "Form Date") {
        searchInput.strColumn == "Form Date";
    }
    else {
        searchInput.strColumn == "Invoice Date";
    }

    console.log(searchInput);

}

function GetPurchaseSummaryDetails()
{
    Utilities.Loader.Show();
    getUserInputs();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/PurchaseSummary",
        type: "POST",
        data: JSON.stringify(searchInput),
        contentType: "application/json",
        success: function(data)
        {
            console.log(data);
            mainOutput = data.Response;
            RenderData();
            Utilities.Loader.Hide();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        }
    });
}

function RenderData()
{
    var finalDisplay = [];
    if(table != "")
    {
        table.destroy();
    }

    $.each(mainOutput, function (index, rowObj) {
        finalDisplay.push([
            //put output data
            index + 1,
            rowObj.purchaseMasterId,
            rowObj.voucherDate,
            rowObj.invoiceDate,
            rowObj.ledgerName,
            rowObj.Qty,
            rowObj.grandTotal,
            rowObj.narration,
            rowObj.currenyName,
            rowObj.no,
            rowObj.userName
           
        ]);
        
    });
    table = $('#PurchaseSummaryList').DataTable({
        data: finalDisplay,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

}


//no data, so cannot get input for datatable