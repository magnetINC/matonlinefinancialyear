﻿$("#viewReport").click(function () {

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetSupplierBalanceReportDetails?toDate=" + $("#toDate").val(),
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var resp = data.Response;
            var subTotal = 0.0;
            var output = "";
            var count = 1;
            for (j = 0; j < resp.length; j++) {
                output += '<tr>' +
                    '<td>' + count + '</td>' +
                    '<td>' + resp[j].ledgerName + '</td>' +
                    '<td>&#8358;' + Utilities.FormatCurrency(resp[j].openingBalance) + '</td>' +
                    '</tr>';
                count += 1;
                subTotal += resp[j].openingBalance;
            }
            output += '<tr>' +
                '<td colspan="2" style="text-align:center"> <b>SUB TOTAL</b></td>' +
                '<td colspan="1"> <b>&#8358;' + Utilities.FormatCurrency(subTotal) + '</b></td>'
            '</tr>';
            $("#SupplierBalanceReportList").html(output);
            Utilities.Loader.Hide();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        },
        error: function (err) {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });
});