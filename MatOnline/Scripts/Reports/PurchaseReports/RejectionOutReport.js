﻿var table = "";
var suppliers = [];
var materialReceipts = [];
var mainOutput = [];
var vouchertypes = [];
var searchInputs = {};

$(function () {
    getsuppliers();
    getVoucherTypes();
    $('#supplier').change(function () {
       // alert("Holllah");
        getMaterialreceipts();
    });
})

function getsuppliers()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/GetSuppliers",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            suppliers = data.Response;
            $("#supplier").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",//not correct text and value
                dataValueField: "ledgerId",
                dataSource: suppliers

            });
            Utilities.Loader.Hide();
        }
    });

}

function getMaterialreceipts()
{
    //would be called on click of search input
    //firstly alert, enter material recipt
     Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PurchaseReports/GetMaterialReceipts?supplierId=" + $('#supplier').val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                materialReceipts = data.Response;
                $("#mrn").kendoDropDownList({
                    filter: "contains",
                    dataTextField: "ledgerName",
                    dataValueField: "ledgerId",
                    dataSource: materialReceipts
                    
                });
                Utilities.Loader.Hide();
            }
        });
 
}

function getVoucherTypes()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/VoucherTypesForReject",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            vouchertypes = data.Response;
            $("#formType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: vouchertypes

            });
            Utilities.Loader.Hide();
        }
    });

}

function getInputs()
{

    searchInputs.fromDate = $('#fromDate').val();
    searchInputs.toDate = $('#toDate').val();
    searchInputs.supplier = $('#supplier').val();
    searchInputs.formType = $('#formType').val();
    searchInputs.formNo = "";
    searchInputs.productName ="";
    searchInputs.productCode = "";
    searchInputs.MaterialReceiptNo = $('#mrn').val();

    if ($('#supplier').val() == "" || $('#supplier').val() == null || $('#supplier').val() == undefined) {
        searchInputs.supplier = 0;
    }

    if ($('#formType').val() == "" || $('#supplier').val() == null || $('#supplier').val() == undefined) {
        searchInputs.formType = 0;
    }

    if ($('#mrn').val() == "" || $('#mrn').val() == null || $('#mrn').val() == undefined) {
        searchInputs.MaterialReceiptNo = 0;
    }

    console.log(searchInputs);

}

function renderData()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }

    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.invoiceNo,
            row.voucherTypeName,
            row.date,
            row.ledgerName,
            row.userName,
            row.totalAmount
        ])
    });
   
    table = $('#RejectionOutReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}

function GetRejectionOutDetails()
{
    getInputs();
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReports/RejectionOutReportDetails",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(searchInputs),
        success:function(data)
        {
            console.log(data);
            mainOutput = data.Response;
            renderData();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        }
    });
    Utilities.Loader.Hide();
}

//renderdata: cannot render data yet as input is not complete/clear


