﻿var table = "";
var mainOutput = [];
var formtypes = [];
var salesmen = [];
var types = [];
var products = [];
var searchInputs = {};
var customers =[];


$(function () {
    Loading();
})


function Loading()
{
    Utilities.Loader.Show();
    getSalesmen();
    getTypes();
    getFormtypes();
    getproducts();
    getcustomers();
    Utilities.Loader.Hide();
    }

function getFormtypes()
{
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetVoucherTypeForDeliveryNote",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            formtypes = data.Response;
            $("#formType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: formtypes,
                optionLabel: "All"
            });
        }
    });
}


function getSalesmen() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetSalesMenForDeliveryNote",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            salesmen = data.Response;
            $("#salesMan").kendoDropDownList({
                filter: "contains",
                dataTextField: "employeeName",
                dataValueField: "employeeId",
                dataSource: salesmen,
            });
        }
    });
}

function getTypes() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetTypeforDeliveryNote",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            types = data.Response;
            $("#type").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: types,
                optionLabel: "All"
            });
        }
    });
}

function getproducts() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetProductsForSalesReport",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            products = data.Response;
            $("#productName").kendoDropDownList({
                filter: "contains",
                dataTextField: "productName",
                dataValueField: "productCode",
                dataSource: products,
                optionLabel: "All"
            });
        }
    });
}

function getcustomers() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetCustomersForDeliveryNote",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
             customers = data.Response;
             $("#customer").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: customers,
            });
        }
    });
}



function getSearchParameters()
{
    searchInputs.fromDate = $('#fromDate').val();
    searchInputs.toDate = $('#toDate').val();
    if ($('#formType').val() == "" || $('#formType').val() == null || $('#formType').val() == undefined) {
        searchInputs.formType = 0;
    }
    else {

        searchInputs.formType = $('#formType').val();
    }
    if ($('#type').val() == "" || $('#type').val() == null || $('#type').val() == undefined) {
        searchInputs.typeId = 0;
        searchInputs.type = "All";
    }
    else {
        searchInputs.typeId = $('#type').val();
        searchInputs.type = $('#type').data("kendoDropDownList").text();
    }

    searchInputs.status = $('#status').val();
    if ($('#productName').val() == "" || $('#productName').val() == null || $('#productName').val() == undefined)
    {
        searchInputs.productName = "";
    }
    else
    {

        searchInputs.productName = $('#productName').val();
    }
    searchInputs.customer = $('#customer').val();
    searchInputs.orderNo = "";

}

function getDeliveryNoteReport()
{
    getSearchParameters();
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetDeliveryNoteReport",
        type: "POST",
        contentType: "application/json",
        data:JSON.stringify(searchInputs),
        success: function(data)
        {
            console.log(data);
            mainOutput = data.Response;
            renderdata();
            Utilities.Loader.Hide();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        }
    })
}


function renderdata()
{
    var display = [];
    $.each(mainOutput, function (index, row) {
      display.push([
       index + 1,
       row.voucherNo,
       row.voucherTypeName,
       row.date,
       row.ledgerName,
       row.OrderNoOrQuotationNo,
       Utilities.FormatCurrency(row.amount),
       row.userName
       ])
    })
    if (table != "") {
        table.destroy();
    }
    table = $('#DeliveryNoteList').DataTable({
        data: display,
        "paging": false,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "responsive": true
    });
}
