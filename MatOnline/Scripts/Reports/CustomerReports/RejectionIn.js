﻿var table = "";
var mainOutput = [];
var formtypes = [];
var customers = [];
var deliverynotes = [];
var salesmen = [];


$(function () {
    loading();
})

function loading()
{
    Utilities.Loader.Show();
    getFormTypes();
    getCustomers();
    getSalesMen();
    getDeliveryNotes();
    Utilities.Loader.Hide();

}

function getFormTypes()
{
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetVoucherTypeForDeliveryNote",
        type: "GET",
        contentType: "application/json",
        success: function(data)
        {
            console.log(data);
            formtypes = data.Response;
            $("#formType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: formtypes,
            });
        }
    })
}

function getCustomers() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetCustomersForRejectionIn",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            customers = data.Response;
            $("#customer").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: customers,
            });
        }
    })
}

function getDeliveryNotes() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetDeliveryNotes",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            deliverynotes = data.Response;
            $("#dnoteno").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherNo",
                dataValueField: "deliveryNoteMasterId",
                dataSource: deliverynotes,
            });
        }
    })
}

function getSalesMen() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetSalesmenForRejectionIn",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            salesmen = data.Response;
            $("#salesMan").kendoDropDownList({
                filter: "contains",
                dataTextField: "employeeName",
                dataValueField: "employeeId",
                dataSource: salesmen,
            });
        }
    })
}


function getRejectionInDetails()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetRejectionInReport?fromdate="+$('#fromDate').val()+"&toDate="+$('#toDate').val()+"&formtype="+$('#formType').val()+"&customer="+$('#customer').val()+"&deliveryNote="+$('#dnoteno').val()+"&salesman="+$('#salesMan').val(),
        type: "GET",
        contentType: "application/json",
        success: function(data)
        {
            console.log(data);
            mainOutput = data.Response;
            renderdata();
        }
    })
    Utilities.Loader.Hide();
}

function renderdata()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.voucherTypeName,
            row.voucherNo,
            row.date,
            row.ledgerName,
            row.TotalAmount,
            row.deliveryNoteNo,
            row.userName
        ])
    });

    table = $('#RejectionInList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}
