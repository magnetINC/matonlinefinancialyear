﻿var table = "";
var mainOutput = [];
var salesman = [];
var products = [];
var formTypes = [];
var customers = [];
var searchInputs = {};

$(function () {
    Loading();
})


function Loading()
{
    Utilities.Loader.Show();
    getCustomers();
    getFormTypes();
    getProducts();
    getSalesMen();
    Utilities.Loader.Hide();
}

function getCustomers()
{
    
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetCustomers",
        type: "GET",
        contentType: "application/json",
        success: function(data)
        {
            console.log(data);
            customers = data.Response;
            $("#customer").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: customers,
                
            });
        }

    });
}

function getFormTypes() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetVoucherType",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            formTypes = data.Response;
            $("#formType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: formTypes,

            });
        }

    });
}

function getProducts() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetProducts",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            products = data.Response;
            $("#productName").kendoDropDownList({
                filter: "contains",
                dataTextField: "productName",
                dataValueField: "productName",
                dataSource: products,
                optionLabel: "All"
            });
        }

    });
}

function getSalesMen() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetSalesMan",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            salesman = data.Response;
            $("#salesMan").kendoDropDownList({
                filter: "contains",
                dataTextField: "employeeName",
                dataValueField: "employeeId",
                dataSource: salesman,

            });
        }

    });
}

function getSearchParameters()
{
  
}

function getQuotationReportDetails()
{
    Utilities.Loader.Show();

    searchInputs.fromDate = $('#fromDate').val();
    searchInputs.toDate = $('#toDate').val();
    if ($('#formType').val() == 0)
    {
        searchInputs.formType = -1;
    }
    if ($('#customer').val() == 0) {
        searchInputs.customer = -1;
    }
    if ($('#productName').val() == "") {
        searchInputs.productName = "";
    }
    searchInputs.salesMan = $('#salesMan').val();
    console.log(searchInputs);
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetSalesQuotationReportDetails",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(searchInputs),
        success: function (data) {
            console.log(data);
            mainOutput = data.Response;
            renderData();
        }

    });
    Utilities.Loader.Hide();
}

function renderData()
{
    var mainAmount = 0;
    var display = [];
    if(table != "")
    {
        table.destroy();

    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.voucherNo,
            row.voucherTypeName,
            row.ledgerName,
            row.userName,//used it for done by field, not sure
            row.date,
             '&#8358;' + Utilities.FormatCurrency(row.totalAmount)
        ])
        mainAmount = mainAmount + parseFloat(row.totalAmount);
    });
    table = $('#SalesQuotationList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    $('#totalAmount').val(Utilities.FormatCurrency(mainAmount));

}

function getTotalAmount()
{
    var mainAmount = 0;
    for(var i =0; i<=mainOutput.length;i++)
    {
       
    }
    $('#totalAmount').val(Utilities.FormatCurrency(mainAmount));
}