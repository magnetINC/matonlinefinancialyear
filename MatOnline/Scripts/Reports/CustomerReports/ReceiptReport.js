﻿var table = "";
var mainOutput = [];
var formTypes = [];
var cashbanks = [];
var ledgers = [];

$(function () {
    Loading();
})

function Loading()
{
    Utilities.Loader.Show();
    getformtypes();
    getLedgers();
    getCashBank();
    Utilities.Loader.Hide();
}

function getformtypes()
{
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetVouchersForReceipt",
        type: "GET",
        contentType: "application/json",
        success: function(data)
        {
            console.log(data);
            formTypes = data.Response;
            $("#formType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: formTypes,
                optionLabel: "All"
            })
        }

    })
}

function getLedgers() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetLedgersForReceipt",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            ledgers = data.Response;
            $("#aLedger").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: ledgers,
                optionLabel: "All"
            })
        }

    })
}

function getCashBank() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetCashorBanksForReceipt",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            cashbanks = data.Response;
            $("#cb").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: cashbanks,
                optionLabel:"All"
            })
        }

    })
}

function getReceiptReportDetails()
{
    if ($("#cb").val() == "" || $("#cb").val() == null || $("#cb").val() == undefined)
    {
        $("#cb").val(0);
    }
    if ($("#formType").val() == "" || $("#formType").val() == null || $("#formType").val() == undefined) {
        $("#formType").val(0);
    }
    if ($("#aLedger").val() == "" || $("#aLedger").val() == null || $("#aLedger").val() == undefined) {
        $("#aLedger").val(0);
    }
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetReceiptDetails?from=" + $('#fromDate').val() + "&to=" + $('#toDate').val() + "&formtype=" + $('#formType').val() + "&ledger=" + $('#aLedger').val() + "&cashbank=" + $('#cb').val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            mainOutput = data.Response;
            renderdata();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        }

    })
}

function renderdata()
{
    var display = [];

    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.voucherTypeName,
            row.voucherNo,
            row.date,
            row.ledgerName,
            Utilities.FormatCurrency(row.totalAmount),
            row.userName,
            row.narration
        ])
    })

    table = $('#ReceiptReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}