﻿var customerBalances = [];

var table = "";

$(function () {
    getCustomerBalances();
});

//========================= API CALLS=====================
function getCustomerBalances()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerReport/CustomerBalance?toDate=" + $("date").val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            customerBalances = data.Response;
            RenderBalances();

            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

//===================== RENDER DATA TO CONTROLS ========================
function RenderBalances()
{
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }
    $.each(customerBalances, function (count, row) {
        objToShow.push([
            count + 1,
            Utilities.FormatJsonDate(row.date),
            findVoucherTypes(row.voucherTypeId).voucherTypeName,
            row.voucherNo,
            row.narration,
             '&#8358;' + Utilities.FormatCurrency(row.grandTotal),
            '<a class="btn btn-primary" onclick="renderPartyBalances(' + count + ')">Apply Credit</a>'
        ]);
    });
    table = $('#invoiceListTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}