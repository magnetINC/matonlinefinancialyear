﻿var table = "";
var mainOutput = [];
var quotations = [];
var formTypes = [];
var states = [];
var cities = [];
var productGroups = [];
var salesMen = [];
var customers = [];
var searchInputs = {};

$(function () {

    Loading();
})


function Loading()
{
    Utilities.Loader.Show();
    getCities();
    getStates();
    getCustomers();
    getFormTypes();
    getSalesMen();
    getProductGroups();
    $('#customer').change(function () {
        getQuotations();
    })
    Utilities.Loader.Hide();
   
}

function getFormTypes()
{
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetVTypes",
        type: "GET",
        contentType: "application/json",
        success: function(data)
        {
            console.log(data);
            formTypes = data.Response;
            $("#formType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: formTypes,

            });
        }
    });
}

function getStates() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetAreas",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            states = data.Response;
            $("#state").kendoDropDownList({
                filter: "contains",
                dataTextField: "areaName",
                dataValueField: "areaId",
                dataSource: states,

            });
        }
    });
}

function getCities() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetRoutes",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            cities = data.Response;
            $("#city").kendoDropDownList({
                filter: "contains",
                dataTextField: "routeName",
                dataValueField: "routeId",
                dataSource: cities,

            });
        }
    });
}

function getProductGroups()
{
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetProductGroups",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            productGroups = data.Response;
            $("#productGroup").kendoDropDownList({
                filter: "contains",
                dataTextField: "groupName",
                dataValueField: "groupId",
                dataSource: productGroups,

            });
        }
    });
}

function getCustomers() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetCustomers",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            customers = data.Response;
            $("#customer").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: customers,

            });
        }
    });
}

function getSalesMen() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetSalesMan",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            salesMen = data.Response;
            $("#salesMan").kendoDropDownList({
                filter: "contains",
                dataTextField: "employeeName",
                dataValueField: "employeeId",
                dataSource: salesMen,

            });
        }
    });
}
function getQuotations()
{
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetQuotation?ledgerId=" + $('#customer').val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            quotations = data.Response;
            $("#quotationNo").kendoDropDownList({
                filter: "contains",
                dataTextField: "productName",
                dataValueField: "productCode",
                dataSource: quotations,

            });
        }
    });
}

function getSearchParameters()
{
    searchInputs.fromDate = $('#fromDate').val();
    searchInputs.toDate = $('#toDate').val();
    if ($('#formType').val() == 0)
    {
        searchInputs.formType = -1;
    }
    else {
        searchInputs.formType = $('#formType').val();
    }
    if ($('#customer').val() == 0) {
        searchInputs.customer = -1;
    }
    else{
        searchInputs.customer = $('#customer').val();
    }
    if ($('#salesMan').val() == 0) {
        searchInputs.salesMan = -1;
    }
    if ($('#productGroup').val() == 0) {
        searchInputs.productGroup = -1;
    }
    else {
        searchInputs.productGroup = $('#productGroup').val();
    }
    if ($('#state').val() == 0) {
        searchInputs.states = -1;
    }
    else {
        searchInputs.states = $('#state').val();
    }
    if ($('#city').val() == 0) {
        searchInputs.city = -1;
    }
    else {
        searchInputs.city = $('#city').val();
    }
    searchInputs.quotationNo = -1;

    console.log(searchInputs);
}

function getSalesOrderReport() {
    Utilities.Loader.Show();
    getSearchParameters();
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetSalesOrderReportDetails",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(searchInputs),
        success: function (data) {
            console.log(data);
            mainOutput = data.Response;
            renderData();
            Utilities.Loader.Hide();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        }
    });
}

function renderData()
{
    var mainAmount = 0;
    var display = [];
    if (table != "") {
        table.destroy();

    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.invoiceNo,
            row.voucherTypeName,
            row.date,
            row.ledgerName,
            row.dueDate,
            row.QuotationNo,
            row.currencyName,
             '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
            row.userName,
            row.narration,
        ])
        mainAmount = mainAmount + parseFloat(row.totalAmount);
    });
    table = $('#SalesOrderList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    $('#totalAmount').val(Utilities.FormatCurrency(mainAmount));
}

