﻿var table = "";
var mainOutput = [];
var customers = [];
var products = [];
var salesMen = [];
var formtypes = [];

$(function () {
    Loading();
})

function Loading()
{
    Utilities.Loader.Show();
    getFormTypes();
    getCustomers();
    getSalesmen();
    getProducts();
    Utilities.Loader.Hide();
}

function getFormTypes()
{
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetVoucherT",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            formtypes = data.Response;
            $("#formType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: formtypes,
            });
        }
    });
}

function getCustomers() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetCusts",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            customers = data.Response;
            $("#customer").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: customers,
            });
        }
    });
}


function getProducts() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetProds",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            products = data.Response;
            $("#productName").kendoDropDownList({
                filter: "contains",
                dataTextField: "productName",
                dataValueField: "productCode",
                dataSource: products,
                optionLabel: "All"
            });
        }
    });
}

function getSalesmen() {
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetSalesM",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            salesMen = data.Response;
            $("#salesMan").kendoDropDownList({
                filter: "contains",
                dataTextField: "employeeName",
                dataValueField: "employeeId",
                dataSource: salesMen,

            });
        }
    });
}

function getSalesReturnReport()
{
    var pcode = "";
    if ($("#productName").val() == "")
    {
        pcode = "";
    }
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetSalesReturnReport?fromDate=" + $('#fromDate').val() + "&toDate=" + $('#toDate').val() + "&formType=" + $('#formType').val() + "&customer=" + $('#customer').val() + "&salesMan=" + $('#salesMan').val() + "&product=" + pcode,
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            mainOutput = data.Response;
            renderData();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        }
    })
    Utilities.Loader.Hide();

}

function renderData()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }

    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.voucherTypeName,
            row.ledgerName,
            row.grandTotal,
            row.voucherNo,
            row.userName
        ])
    });

    table = $('#SalesReturnList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

}

