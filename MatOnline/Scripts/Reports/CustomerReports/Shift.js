﻿var table = "";
var mainOutput = [];
var ledgers = [];
var formTypes = [];

$(function () {
    getLedgers();
    getVoucherTypes();
})


function getLedgers()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetLedgers",
        type: "GET",
        contentType: "application/json",
        success: function(data)
        {
            console.log(data);
            ledgers = data.Response;
            $("#ledger").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: ledgers

            });
        }

    })
    Utilities.Loader.Hide();

}

function getVoucherTypes()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetVoucherTypes",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            formTypes = data.Response;
            $("#formType").kendoDropDownList({
                filter: "contains",
                dataTextField: "voucherTypeName",
                dataValueField: "voucherTypeId",
                dataSource: formTypes

            });
        }

    })
    Utilities.Loader.Hide();
}

function getShiftReportDetails()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetShiftReport?from=" + $('#fromDate').val() + "&to=" + $('#toDate').val() + "&vType=" + $('#formType').val() + "&ledger=" + $('#ledger').val(),
        type: "GET",
        contentType: "application/json",
        success: function(data)
        {
            console.log(data);
            mainOutput = data.Response;
            //renderData
            renderData();n
        }
    })
    Utilities.Loader.Hide();
}
   
function renderData()
{
    var display = [];
    if(table!="")
    {
        table.destroy();
    }

    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.Date,
            row.voucherTypeId,
            row.voucherNumber,
            row["Form Type"],
            row["Form No"],
            row.Narration,
            row.typeOfVoucher,
            row.Debit,
            row.Credit,
            row["Inward Qty"],
            row["Outward Qty"],
            row.MasterId
        ])
    });
    table = $('#ShiftList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}