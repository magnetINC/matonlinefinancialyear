﻿var table = "";
var salesModeddl = [
    {
        value: "All",
        text: "All"
    },
    {
        value: "NA",
        text: "NA"
    },
    {
        value: "Against SalesOrder",
        text: "Against SalesOrder"
    },
    {
        value: "Against SalesQuotation",
        text: "Against SalesQuotation"
    },
    {
        value: "Against DeliveryNote",
        text: "Against DeliveryNote"
    }
];
var statusddl = [
    {
        value: "All",
        text: "All"
    },
    {
        value: "Full",
        text: "Full"
    },
    {
        value: "Partial",
        text: "Partial"
    },
    {
        value: "Unpaid",
        text: "Unpaid"
    },
    {
        value: "Due",
        text: "Due"
    }
];
var customers = [];
var formTypes = [];
var state = [];
var products = [];

var searchInputs = {};
var report = []

//===================== API CALLS ==========================
$(function () {
    getLookUps();
    $("#SalesList").hide();
    $("#SalesListP").hide();
    $("#SalesListUP").hide();
});

function getLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            customers = data.Response.customers;
            formTypes = data.Response.formTypes;
            state = data.Response.states;
            products = data.Response.products;

            renderlookUpsToControls();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getSalesReport() {
    Utilities.Loader.Show();
    //getSearchParameters();

    searchInputs.fromDate = $('#fromDate').val();
    searchInputs.toDate = $('#toDate').val();
    if ($('#formType').val() == "") {
        $('#formType').val(0)
    }
    if ($('#customer').val() == "") {
        $('#customer').val(0)
    }
    if ($('#state').val() == "") {
        $('#state').val(0)
    }
    if ($('#products').val() == "") {
        $('#products').val("")
    }
    searchInputs.formType = $('#formType').val();
    searchInputs.customer = $('#customer').val();
    searchInputs.state = $('#state').val();
    searchInputs.city = 0;
    searchInputs.salesMode = $('#salesMode').val();
    searchInputs.status = $('#status').val();
    searchInputs.productName = $('#products').val();

    console.log(searchInputs);
    $.ajax({
        url: API_BASE_URL + "/CustomerReports/GetSalesReportDetails",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(searchInputs),
        success: function (data) {

            console.log(data);
            mainOutput = data.Response;
            renderData();

            Utilities.Loader.Hide();

            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        },
        error: function () {

            Utilities.Loader.Hide();
        }
    })
}



//=================== RENDER DATA TO CONTROLS ===================
function renderlookUpsToControls()
{
    $("#customer").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: customers,
    });
    $("#formType").kendoDropDownList({
        filter: "contains",
        dataTextField: "voucherTypeName",
        dataValueField: "voucherTypeId",
        dataSource: formTypes,
        optionLabel: "All"
    });
    $("#status").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: statusddl,
    });
    $("#salesMode").kendoDropDownList({
        filter: "contains",
        dataTextField: "text",
        dataValueField: "value",
        dataSource: salesModeddl,
    });
    $("#state").kendoDropDownList({
        filter: "contains",
        dataTextField: "godownName",
        dataValueField: "godownId",
        dataSource: state,
        optionLabel: "All"
    });
    $("#products").kendoDropDownList({
        filter: "contains",
        dataTextField: "productName",
        dataValueField: "productName",
        dataSource: products,
        optionLabel: "All"
    });
}
function renderData()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }


    if ($("#status").val() == "Full" || $("#status").val() == "Partial") {
        $.each(mainOutput, function (index, row) {
            display.push([
                index + 1,
                row.invoiceNo,
                //row.voucherTypeName, //FormType
                row.date,
                row.ledgerName,
                row.narration,
                row.invoiceRefNo,
                row.currencyName,
                row.userName,
                Utilities.FormatCurrency(row.grandTotal),
                Utilities.FormatCurrency(row.PaidAmount),
                Utilities.FormatCurrency(row.Balance)
            ])
        });

        table = $('#SalesListP').DataTable({
            data: display,
            "responsive": true,
            "paging": false,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
        $("#SalesList").hide();
        $("#SalesListP").show();
        $("#SalesListUP").hide();
    }
    if ($("#status").val() == "All" || $("#status").val() == "Due") {
        $.each(mainOutput, function (index, row) {
            display.push([
                index + 1,
                row.invoiceNo,
                //row.voucherTypeName, //FormType
                row.date,
                row.ledgerName,
                row.narration,
                row.invoiceRefNo,
                row.currencyName,
                row.userName,
                Utilities.FormatCurrency(row.grandTotal)
            ])
        });

        table = $('#SalesList').DataTable({
            data: display,
            "responsive": true,
            "paging": false,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
        $("#SalesList").show();
        $("#SalesListP").hide();
        $("#SalesListUP").hide();
    }
    if ($("#status").val() == "Unpaid") {
        $.each(mainOutput, function (index, row) {
            display.push([
                index + 1,
                row.invoiceNo,
                //row.voucherTypeName, //FormType
                row.date,
                row.ledgerName,
                row.narration,
                row.invoiceRefNo,
                row.currencyName,
                row.userName,
                Utilities.FormatCurrency(row.grandTotal),
                Utilities.FormatCurrency(row.UnpaidAmont),
                Utilities.FormatCurrency(row.ToBalance)
            ])
        });

        table = $('#SalesListUP').DataTable({
            data: display,
            "responsive": true,
            "paging": false,
            "lengthChange": true,
            "searching": true,
            "ordering": true,
            "info": true,
            "autoWidth": true
        });
        $("#SalesListUP").show();
        $("#SalesListP").hide();
        $("#SalesList").hide();
    }
}

//================ OTHER FUNCTUIONS ==============
function getSearchParameters() {
    searchInputs.fromDate = $('#fromDate').val();
    searchInputs.toDate = $('#toDate').val();
    if ($('#formType').val() == "") {
        $('#formType').val(0)
    }
    if ($('#customer').val() == "") {
        $('#customer').val(0)
    }
    if ($('#state').val() == "") {
        $('#state').val(0)
    }
    if ($('#products').val() == "") {
        $('#products').val("")
    }
    searchInputs.formType = $('#formType').val();
    searchInputs.customer = $('#customer').val();
    searchInputs.state = $('#state').val();
    searchInputs.city = 0;
    searchInputs.salesMode = $('#salesMode').val();
    searchInputs.status = $('#status').val();
    searchInputs.productName = $('#products').val();
}



