﻿var mainOutput = [];
var table = "";
var accountgroups = [];

$(function () {
    getAccountGroups();
})

function getAccountGroups()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ListReports/GetAccountGroups",
        type:"GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            accountgroups = data.Response;
            $("#aGroup").kendoDropDownList({
                filter: "contains",
                dataTextField: "accountGroupName",
                dataValueField: "accountGroupId",
                dataSource: accountgroups
               
            })
        }
    })
    Utilities.Loader.Hide();
}


function getAccountListDetails() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ListReports/GetAccountListDetails?acctgroupId=" + $('#aGroup').val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            accountgroups = data.Response;
            renderData();
        }
    })
    Utilities.Loader.Hide();
}

function renderData()
{
    var display = [];

    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.ledgerName,
            row.nature,
            row.description,
            row.accountNumber,
            row.mailingName,
            row.Email,
            row.CD,
            row,Balance
        ])
    });

    table = $('#AccountListReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}