﻿var modelNumbers = [{ modelNoId: 0, modelNo: "All" }];
var brands = [{brandId:0,brandName:"All"}];
var size = [{ sizeId: 0, size: "All" }];
var productGroups = [{ groupId: 0, groupName: "All", CustomDisplay: "All" }];
var table = "";

$(function () {

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/InventoryReportLookup/InventoryStatistics",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            
            for (var i = 0; i < data.ModelNo.length; i++){
                modelNumbers.push({
                    modelNoId: data.ModelNo[i].modelNoId,
                    modelNo: data.ModelNo[i].modelNo
                });
            }
            for (var i = 0; i < data.Brand.length; i++) {
                brands.push({
                    brandId: data.Brand[i].brandId,
                    brandName: data.Brand[i].brandName
                });
            }
            for (var i = 0; i < data.Size.length; i++) {
                size.push({
                    sizeId: data.Size[i].sizeId,
                    size: data.Size[i].size
                });
            }
            for (var i = 0; i < data.ProductGroup.length; i++) {
               // if (record.Extra1 == "Category" || record.Extra1 == "Sub Category") {
                    //groupHtml += '<option value="' + record.GroupId + '">' + record.GroupName + ' : ' + upperLevelGroup + '</option>';
                    //filteredGroupObj.push({
                    //    GroupId: record.GroupId,
                    //    GroupName: record.GroupName,
                    //    CustomDisplay: record.GroupName + " : " + upperLevelGroup
                    //});
                    productGroups.push({
                        groupId: data.ProductGroup[i].groupId,
                        groupName: data.ProductGroup[i].groupName,
                        CustomDisplay: data.ProductGroup[i].groupName + " : " + getGroupUnderName(data.ProductGroup, data.ProductGroup[i].narration)
                    });
               // }
                
            }

            $("#productGroup").kendoDropDownList({
                filter: "contains",
                optionLabel: "Please Select...",
                dataTextField: "CustomDisplay",
                dataValueField: "groupId",
                dataSource: productGroups
            });
            $("#brand").kendoDropDownList({
                filter: "contains",
                optionLabel: "Please Select...",
                dataTextField: "brandName",
                dataValueField: "brandId",
                dataSource: brands
            });
            $("#modelNo").kendoDropDownList({
                filter: "contains",
                //optionLabel: "Please Select...",
                dataTextField: "modelNo",
                dataValueField: "modelNoId",
                dataSource: modelNumbers
            });
            $("#size").kendoDropDownList({
                filter: "contains",
                optionLabel: "Please Select...",
                dataTextField: "size",
                dataValueField: "sizeId",
                dataSource: size
            });

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });

    $("#search").click(function () {
        var param = {
                BrandId: $("#brand").val(),
                ModelNoId: $("#modelNo").val(),
                SizeId: $("#size").val(),
                ProductGroupId: $("#productGroup").val(),
                Criteria: $("#criteria").val(),
                BatchName: $("#batchNo").val(),
                FromDate: $("#fromDate").val(),
                ToDate: $("#toDate").val()
        };
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/InventoryReport/ProductStatistics",
            type: "POST",
            data:JSON.stringify(param),
            contentType: "application/json",
            success: function (data)
            {
                var output = [];
                var optional = "";
                $("#optional").html("");
                for (var i = 0; i < data.length; i++)
                {                    
                    if (data[i].minimumStock != undefined)
                    {
                        optional = data[i].minimumStock;
                        $("#optional").html("Min. Stock");
                    }
                    if (data[i].reorderLevel != undefined)
                    {
                        optional = data[i].reorderLevel;
                        $("#optional").html("Reorder Level");
                    }
                    if (data[i].maximumStock != undefined)
                    {
                        optional = data[i].maximumStock;
                        $("#optional").html("Max. Stock");
                    }
                    
                    output.push([
                        (i + 1),
                        data[i].productCode,
                        data[i].productName,
                        data[i].batchNo,
                        data[i].brandName,
                        Utilities.FormatCurrency(data[i].salesRate),
                        Utilities.FormatCurrency(data[i].salesRate),
                        data[i].CurrentStock,
                        data[i].unitName,
                        optional
                    ]);
                }
                if (table != "")
                {
                    table.destroy();
                }
                table = $('#inventoryStatTbody').DataTable({
                    data: output,
                    "paging": true,
                    "lengthChange": true,
                    "searching": true,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
                console.log(data);
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
                console.log(err)
            }
        });
    });

});

function getGroupUnderName(productGroupObject,groupUnderId)
{
    if (groupUnderId == 1)
    {
        return "Primary";
    }
    //if (productGroupObject.find(p=>p.narration == groupUnderId) == null || productGroupObject.find(p=>p.narration == groupUnderId) == undefined)
    //{
    //    return "NA";
    //}
    return (productGroupObject.find(p=>p.groupId == groupUnderId)==undefined)?"N/A":productGroupObject.find(p=>p.groupId == groupUnderId).groupName;

    //$.each(groups, function (i, record) {
    //    var upperLevelGroup = "";
    //    var currentGroup = {};
    //    for (i = 0; i < groups.length; i++) {
    //        if (groups[i].GroupId == record.GroupUnder) {
    //            currentGroup = groups[i];
    //            break;
    //        }
    //    }
    //    if (currentGroup.GroupUnder == 0) {
    //        upperLevelGroup = "Primary";
    //    }
    //    else {
    //        upperLevelGroup = currentGroup.GroupName;
    //    }
    //    if (record.Extra1 == "Category" || record.Extra1 == "Sub Category") {
    //        //groupHtml += '<option value="' + record.GroupId + '">' + record.GroupName + ' : ' + upperLevelGroup + '</option>';
    //        filteredGroupObj.push({
    //            GroupId: record.GroupId,
    //            GroupName: record.GroupName,
    //            CustomDisplay: record.GroupName + " : " + upperLevelGroup
    //        });
    //    }
    //});
}