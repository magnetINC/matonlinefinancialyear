﻿var products = [];
var voucherType = [];
var searchParam = {};
var stockJournals = [];
var lookups = {};
var table = "";
var allStores = [];
var accountLedgers = [];

$(function () {
    getLookup();
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/InventoryReportLookup/StockJournal",
        type: "GET",
        contentType: "application/json",
        data: JSON.stringify(searchParam),
        success: function (data) {
            lookups = data;
            $("#productCode").kendoDropDownList({
                filter: "contains",
                optionLabel: "Please Select...",
                dataTextField: "productCode",
                dataValueField: "productCode",
                dataSource: lookups.Products
            });
            $("#product").kendoDropDownList({
                filter: "contains",
                optionLabel: "Please Select...",
                dataTextField: "productName",
                dataValueField: "productName",
                dataSource: lookups.Products
            });
            $("#store").html(Utilities.PopulateDropDownFromArray(lookups.VoucherTypes, 1, 1));
            
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });

    $("#search").click(function () {
        loadStockJournals();
    });

});

function getLookup() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetLookups",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            //finishedGoods = data.FinishedGoods;
            //currencies = data.Currencies;
            //allProducts = data.AllProducts;
            allStores = data.AllStores;
            //allUnits = data.AllUnits;
            //allRacks = data.AllRacks;
            //allBatches = data.AllBatches;
            //cashOrBanks = data.CashOrBanks;
            accountLedgers = data.AccountLedgers;

            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function loadStockJournals()
{
    //var prod=$("#product").val()=="0"?"All":$("#product").val();
    searchParam = {
        "FromDate": $("#fromDate").val(),
        "ToDate": $("#toDate").val(),
        "VoucherType": 0,
        "VoucherNo": $("#formNo").val(),
        "Product": $("#product").val(),
        "ProductCode": $("#productCode").val()
    };

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/InventoryReport/StockJournal",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(searchParam),
        success:function(data)
        {
            console.log(data);
            stockJournals = data;
            var objToShow = [];
            $.each(stockJournals, function (count, row) {
                objToShow.push([
                    count + 1,
                    row.invoiceNo,
                    row.date,
                    row.Qty,
                    row.narration,
                    '<button type="button" class="btn btn-primary btn-sm" onclick="stockJournalDetails(' + row.stockJournalMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                ]);
            });
            if(table=="")
            {
                table = $('#stockJournalTbody').DataTable({
                    data: objToShow,
                    "paging": true,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
            }
            else
            {
                table.destroy();
                table = $('#stockJournalTbody').DataTable({
                    data: objToShow,
                    "paging": true,
                    "lengthChange": true,
                    "searching": false,
                    "ordering": true,
                    "info": true,
                    "autoWidth": true
                });
            }
            Utilities.Loader.Hide();
        },
        error:function(err)
        {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });
}

function stockJournalDetails(id)
{
    Utilities.Loader.Show()
    $.ajax({
        url: API_BASE_URL + "/InventoryReport/stockJournalDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            var master = data.Master;
            var cons = data.consumption;
            var prod = data.production;
            var cashorbank = data.cashorbank;
            var addCost = data.directAdditionalCost;
            console.log(data);
            //var statusOutput = "";
            //if (pj.status == "Pending") {
            //    $("#ApproveTransfer").show();
            //    $("#cancelOrder").show();
            //    statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending</span>';
            //}
            //else if (pj.status == "Approved") {
            //    $("#ApproveTransfer").hide();
            //    $("#cancelOrder").hide();
            //    statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Approved</span>';
            //}
            //else if (pj.status == "Cancelled") {
            //    $("#ApproveTransfer").hide();
            //    $("#cancelOrder").hide();
            //    statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
            //}          
            //$("#statusDiv").html(statusOutput);
            var amount = 0;
            var outputCons = "";
            var outputProd = "";
            var outputAddCost = "";
            $.each(cons, function (count, row) {
                amount = amount + row.amount;
                outputCons += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + row.productCode + '</td>\
                            <td>' + row.productCode + '</td>\
                            <td>' + row.productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                        </tr>';
            });
            $.each(prod, function (count, row) {
                outputProd += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td>' + row.productCode + '</td>\
                            <td>' + row.productCode + '</td>\
                            <td>' + row.productName + '</td>\
                            <td>' + findStore(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                        </tr>';
            });
            $.each(addCost, function (count, row) {
                outputAddCost += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td>' + findLedger(row.ledgerId).ledgerName + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.debit) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.credit) + '</td>\
                        </tr>';
            });

            $.each(master, function (count, row) {
                $("#narrationDiv").html(row.narration);
                $("#stockJournalDiv").html(row.extra1);
                $("#dateDiv").html(row.date);
                $("#AmountTxt").val(Utilities.FormatCurrency(amount));
                $("#addCostTxt").val(Utilities.FormatCurrency(row.additionalCost));
                $("#grandTotalTxt").val(Utilities.FormatCurrency(amount + row.additionalCost));
            });
           
            $("#availableQuantityDiv").html("");
            $("#detailsTbodyConsumption").html(outputCons);
            $("#detailsTbodyProduction").html(outputProd);
            $("#detailsTbodyAdditionalCost").html(outputAddCost);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < allStores.length; i++) {
        if (allStores[i].godownId == storeId) {
            output = allStores[i];
            break;
        }
    }
    return output;
}
function findLedger(ledgerId) {
    var output = {};
    for (i = 0; i < accountLedgers.length; i++) {
        if (accountLedgers[i].ledgerId == ledgerId) {
            output = accountLedgers[i];
            break;
        }
    }
    return output;
}