﻿var products = [];
var stores = [];
var searchParam = {};
var stockVariance = [];
var lookups = {};

$(function () {

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/InventoryReportLookup/StockVariance",
        type: "GET",
        contentType: "application/json",
        data: JSON.stringify(searchParam),
        success: function (data) {
            lookups = data;
            //$("#product").html(Utilities.PopulateDropDownFromArray(lookups.Products, 0, 2));
            $("#store").html(Utilities.PopulateDropDownFromArray(lookups.Stores, 0, 1));

            $("#product").kendoDropDownList({
                filter: "contains",
                dataTextField: "productName",
                dataValueField: "productId",
                dataSource: lookups.Products
            });
            
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });

    $("#search").click(function () {
        loadStockVariance();
    });

});

function loadStockVariance()
{    
    //var prod = $("#product").val() == "0" ? "All" : $("#product").val();
    //var str = $("#store").val() == "0" ? "All" : $("#product").val();
    searchParam = {
        ProductId: $("#product").val(),
        StoreId: $("#store").val(),
        ItemCode: $("#productCode").val()
    };

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/InventoryReport/StockVariance",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(searchParam),
        success:function(data)
        {
            stockVariance = data;
            var output = "";
            for (i = 0; i < stockVariance.length; i++)
            {
                var diff = stockVariance[i].qty - stockVariance[i].SystemStock;
                var stkVal = diff * stockVariance[i].rate;
                output += '<tr>\
                            <td>' +(i+1)+'</td>\
                            <td>' + Utilities.FormatJsonDate(stockVariance[i].extraDate) + '</td>\
                            <td>' + stockVariance[i].productCode + '</td>\
                            <td>' + stockVariance[i].productName + '</td>\
                            <td>' + stockVariance[i].qty + '</td>\
                            <td>' + stockVariance[i].godownName + '</td>\
                            <td>' + (stockVariance[i].SystemStock).replace(".00","") + '</td>\
                            <td>' + stockVariance[i].Batch + '</td>\
                            <td>' + Utilities.FormatCurrency(stockVariance[i].rate) + '</td>\
                            <td>' + diff + '</td>\
                            <td>' + Utilities.FormatCurrency(stkVal) + '</td>\
                           </tr>';
            }
            $("#stockVarianceTbody").html(output);
            Utilities.Loader.Hide();
        },
        error:function(err)
        {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });
}