﻿var products = [];
var stores = [];
var searchParam = {};
var stockSummary = [];
var lookups = {};

$(function () {

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/InventoryReportLookup/StockDetails",
        type: "GET",
        contentType: "application/json",
        data: JSON.stringify(searchParam),
        success: function (data) {
            lookups = data;


            // ======== RENDER DATA TO CONTROLS
            $("#store").kendoDropDownList({
                filter: "contains",
                dataTextField: "godownName",
                dataValueField: "godownId",
                dataSource: lookups.Store,
                optionLabel: {
                    godownName: "All",
                    godownId: "0"
                }
            });

            $("#batchNo").kendoDropDownList({
                filter: "contains",
                dataTextField: "batchNo",
                dataValueField: "batchId",
                dataSource: lookups.Batch,
                optionLabel: {
                    batchNo: "All",
                    batchId: "All"
                }
            });

            $("#product").kendoDropDownList({
                filter: "contains",
                dataTextField: "productName",
                dataValueField: "productId",
                dataSource: lookups.Product,
                optionLabel: {
                    productName: "All",
                    productId: "0"
                }
            });

            //console.log(lookups);

            $('.k-widget.k-dropdown').css('width', '100%');

            //$("#store").html('<option value="0">All</option>' + Utilities.PopulateDropDownFromArray(lookups.Store, 0, 1));
            //$("#batchNo").html('<option value="All">All</option>' + Utilities.PopulateDropDownFromArray(lookups.Batch, 0, 1));
            //$("#product").html('<option value="0">All</option>' + Utilities.PopulateDropDownFromArray(lookups.Product, 0, 2));

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });

    $("#search").click(function () {
        loadStockSummary();
    });

});

function loadStockSummary()
{
    searchParam = {
        StoreId: ($("#store").val() == null) ? '0' : $("#store").val(),
        BatchNo: ($("#batchNo").val() == null) ?'All': $("#batchNo").val(),
        ProductId: ($("#product").val() == null) ? '0' : $("#product").val(),
        ProductCode: $("#productCode").val(),
        refNo: "",
        TransactionType: ($("#TransactionType").val() == null || $("#TransactionType").val() == undefined) ? '0' : $("#TransactionType").val(),
    };
    /*debugger;*/
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/InventoryReport/StockSummary",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(searchParam),
        success:function(data)
        {
            stockSummary = data.StockSummary;
            //console.log(stockDetails);
            //return;
            var output = "";
            for (i = 0; i < stockSummary.length; i++)
            {                
                if(stockSummary[i].StoreName=="Total Stock Value:")
                {
                    output += '<tr style="background-color:#00bfff;">\
                            <td>' + stockSummary[i].ProductName + '</td>\
                            <td>' + stockSummary[i].ProductCode + '</td>\
                            <td>' + stockSummary[i].StoreName + '</td>\
                            <td>' + stockSummary[i].QtyBalance + '</td>\
                            <td>' + stockSummary[i].AvgCostValue + '</td>\
                            <td>' + stockSummary[i].StockValue + '</td>\
                            <td></td>\
                           </tr>';
                }
                else
                {
                    output += '<tr>\
                            <td>' + stockSummary[i].ProductName + '</td>\
                            <td>' + stockSummary[i].ProductCode + '</td>\
                            <td>' + stockSummary[i].StoreName + '</td>\
                            <td>' + Number(stockSummary[i].QtyBalance).toFixed(2) + '</td>\
                            <td>' + stockSummary[i].AvgCostValue + '</td>\
                            <td>' + stockSummary[i].StockValue + '</td>\
                             <td>' + stockSummary[i].UnitName + '</td>\
                           </tr>';
                }
            }
            $("#stockSummaryTbody").html(output);
            $("#totalStockValue").val(data.TotalStockValue);
            Utilities.Loader.Hide();
        },
        error:function(err)
        {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });
}