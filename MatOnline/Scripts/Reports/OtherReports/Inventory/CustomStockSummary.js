﻿var master = [];
var productGroups = [];
var divisions = [];
var groups = [];
var subGroups = [];
var categories = [];
var subCategories = [];
var stores = [];
var filteredGroups = [];
var remainingGroups = [];

$(function () {
    
    loadProfiles();

    $("#search").click(function () {

        for (i = 0; i < divisions.length; i++)
        {
            if ($("#divisions").val() != "")
            {
                filteredGroups.push({ groupId: divisions[i].groupId, groupName: divisions[i].groupName });
            }
        }
        for (i = 0; i < groups.length; i++)
        {
            if ($("#groups").val() != "") {
                filteredGroups.push({ groupId: groups[i].groupId, groupName: groups[i].groupName });
            }
        }
        for (i = 0; i < subGroups.length; i++) {
            if ($("#subgroups").val() != "") {
                filteredGroups.push({ groupId: subGroups[i].groupId, groupName: subGroups[i].groupName });
            }
        }
        for (i = 0; i < categories.length; i++) {
            if ($("#categories").val() != "") {
                filteredGroups.push({ groupId: categories[i].groupId, groupName: categories[i].groupName });
            }
        }
        for (i = 0; i < subCategories.length; i++) {
            if ($("#subcategories").val() != "") {
                filteredGroups.push({ groupId: subCategories[i].groupId, groupName: subCategories[i].groupName });
            }
        }
        for (k = 0; k < productGroups.length; k++)
        {
            if(filteredGroups.findIndex(p=>p.groupId==productGroups[i].groupId)>-1)
            {
                remainingGroups.push({ groupId: productGroups[i].groupId, groupName: productGroups[i].groupName });
            }
        }
        //console.log(filteredGroups);
        //return;
        var param = {
            FromDate: $("#from").val(),
            ToDate: $("#to").val()
        };
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/inventoryreport/CustomStockSummaryReport",
            data: JSON.stringify(param),
            type: "POST",
            contentType: "application/json",
            success: function (data) {
                var output = "";
                master = data;
                console.log(data);

                var GrandLocationTotalOpeningStock = 0;
                var GrandLocationTotalDeliveryNote = 0;
                var GrandLocationTotalMaterialReceipt = 0;
                var GrandLocationTotalAdjustment = 0;
                var GrandLocationTotalTransferIn = 0;
                var GrandLocationTotalTransferOut = 0;
                var GrandLocationTotal = 0;

                var selectedStore = $("#stores").val();

                for (i = 0; i < data.length; i++)
                {
                    var locationTotalOpeningStock = 0;
                    var locationTotalDeliveryNote = 0;
                    var locationTotalMaterialReceipt = 0;
                    var locationTotalAdjustment = 0;
                    var locationTotalTransferIn = 0;
                    var locationTotalTransferOut = 0;
                    var locationTotal = 0;

                    if (selectedStore == "All")
                    {
                        output += '<tr>\
                                            <td><h4>'+ data[i].StoreName + '</h4></td>\
                                            <td><b>Part No.</b></td>\
                                            <td><b>Item Name</b></td>\
                                            <td><b>Size</b></td>\
                                            <td><b>Opening Stock</b></td>\
                                            <td><b>Sales</b></td>\
                                            <td><b>Purchase</b></td>\
                                            <td><b>Adjustment</b></td>\
                                            <td><b>Trans. In</b></td>\
                                            <td><b>Trans. Out</b></td>\
                                            <td><b>Total</b></td>\
                                        </tr>';
                        for (k = 0; k < data[i].ItemLines.length; k++)
                        {
                            var item = data[i].ItemLines[k];
                            
                            //if (item.ProductGroupId == $("#subcategories").val())
                            //if (remainingGroups.find(p=>p.groupId == item.ProductGroupId) != null && remainingGroups.find(p=>p.groupId == item.ProductGroupId) != undefined)
                            if ($("#subcategories").val() != "") {
                                if (item.SubCategoryId == $("#subcategories").val()) {
                                    if (item.TotalOpeningStock > 0 || item.TotalDeliveryNote > 0 || item.TotalMaterialReceipt > 0
                                    && item.TotalAdjustment > 0 || item.TotalTransferIn > 0 || item.TotalTransferOut > 0) {
                                        var itemLineTotal = (item.TotalOpeningStock + item.TotalMaterialReceipt + item.TotalTransferIn) -
                                            (item.TotalDeliveryNote + item.TotalAdjustment + item.TotalTransferOut);

                                        locationTotalOpeningStock = locationTotalOpeningStock + item.TotalOpeningStock;
                                        locationTotalDeliveryNote = locationTotalDeliveryNote + item.TotalDeliveryNote;
                                        locationTotalMaterialReceipt = locationTotalMaterialReceipt + item.TotalMaterialReceipt;
                                        locationTotalAdjustment = locationTotalAdjustment + item.TotalAdjustment;
                                        locationTotalTransferIn = locationTotalTransferIn + item.TotalTransferIn;
                                        locationTotalTransferOut = locationTotalTransferOut + item.TotalTransferOut;
                                        locationTotal = locationTotal + itemLineTotal;

                                        output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                ' + item.Description + '\
                                            </td>\
                                            <td>\
                                                ' + item.ItemName + '\
                                            </td>\
                                            <td>\
                                                ' + item.Size + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalOpeningStock + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalDeliveryNote + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalMaterialReceipt + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalAdjustment + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferIn + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferOut + '\
                                            </td>\
                                            <td>\
                                                '+ itemLineTotal + '\
                                            </td>\
                                        </tr>';
                                    }
                                }
                            }
                            else if ($("#categories").val() != "") {
                                if (item.CategoryId == $("#categories").val()) {
                                    if (item.TotalOpeningStock > 0 || item.TotalDeliveryNote > 0 || item.TotalMaterialReceipt > 0
                                    && item.TotalAdjustment > 0 || item.TotalTransferIn > 0 || item.TotalTransferOut > 0) {
                                        var itemLineTotal = (item.TotalOpeningStock + item.TotalMaterialReceipt + item.TotalTransferIn) -
                                            (item.TotalDeliveryNote + item.TotalAdjustment + item.TotalTransferOut);

                                        locationTotalOpeningStock = locationTotalOpeningStock + item.TotalOpeningStock;
                                        locationTotalDeliveryNote = locationTotalDeliveryNote + item.TotalDeliveryNote;
                                        locationTotalMaterialReceipt = locationTotalMaterialReceipt + item.TotalMaterialReceipt;
                                        locationTotalAdjustment = locationTotalAdjustment + item.TotalAdjustment;
                                        locationTotalTransferIn = locationTotalTransferIn + item.TotalTransferIn;
                                        locationTotalTransferOut = locationTotalTransferOut + item.TotalTransferOut;
                                        locationTotal = locationTotal + itemLineTotal;

                                        output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                ' + item.Description + '\
                                            </td>\
                                            <td>\
                                                ' + item.ItemName + '\
                                            </td>\
                                            <td>\
                                                ' + item.Size + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalOpeningStock + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalDeliveryNote + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalMaterialReceipt + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalAdjustment + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferIn + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferOut + '\
                                            </td>\
                                            <td>\
                                                '+ itemLineTotal + '\
                                            </td>\
                                        </tr>';
                                    }
                                }
                            }
                            else if ($("#groups").val() != "") {
                                if (item.GroupId == $("#groups").val()) {
                                    if (item.TotalOpeningStock > 0 || item.TotalDeliveryNote > 0 || item.TotalMaterialReceipt > 0
                                    && item.TotalAdjustment > 0 || item.TotalTransferIn > 0 || item.TotalTransferOut > 0) {
                                        var itemLineTotal = (item.TotalOpeningStock + item.TotalMaterialReceipt + item.TotalTransferIn) -
                                            (item.TotalDeliveryNote + item.TotalAdjustment + item.TotalTransferOut);

                                        locationTotalOpeningStock = locationTotalOpeningStock + item.TotalOpeningStock;
                                        locationTotalDeliveryNote = locationTotalDeliveryNote + item.TotalDeliveryNote;
                                        locationTotalMaterialReceipt = locationTotalMaterialReceipt + item.TotalMaterialReceipt;
                                        locationTotalAdjustment = locationTotalAdjustment + item.TotalAdjustment;
                                        locationTotalTransferIn = locationTotalTransferIn + item.TotalTransferIn;
                                        locationTotalTransferOut = locationTotalTransferOut + item.TotalTransferOut;
                                        locationTotal = locationTotal + itemLineTotal;

                                        output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                ' + item.Description + '\
                                            </td>\
                                            <td>\
                                                ' + item.ItemName + '\
                                            </td>\
                                            <td>\
                                                ' + item.Size + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalOpeningStock + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalDeliveryNote + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalMaterialReceipt + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalAdjustment + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferIn + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferOut + '\
                                            </td>\
                                            <td>\
                                                '+ itemLineTotal + '\
                                            </td>\
                                        </tr>';
                                    }
                                }
                            }
                            else if ($("#subgroups").val() != "") {
                                if (item.SubGroupId == $("#subgroups").val()) {
                                    if (item.TotalOpeningStock > 0 || item.TotalDeliveryNote > 0 || item.TotalMaterialReceipt > 0
                                    && item.TotalAdjustment > 0 || item.TotalTransferIn > 0 || item.TotalTransferOut > 0) {
                                        var itemLineTotal = (item.TotalOpeningStock + item.TotalMaterialReceipt + item.TotalTransferIn) -
                                            (item.TotalDeliveryNote + item.TotalAdjustment + item.TotalTransferOut);

                                        locationTotalOpeningStock = locationTotalOpeningStock + item.TotalOpeningStock;
                                        locationTotalDeliveryNote = locationTotalDeliveryNote + item.TotalDeliveryNote;
                                        locationTotalMaterialReceipt = locationTotalMaterialReceipt + item.TotalMaterialReceipt;
                                        locationTotalAdjustment = locationTotalAdjustment + item.TotalAdjustment;
                                        locationTotalTransferIn = locationTotalTransferIn + item.TotalTransferIn;
                                        locationTotalTransferOut = locationTotalTransferOut + item.TotalTransferOut;
                                        locationTotal = locationTotal + itemLineTotal;

                                        output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                ' + item.Description + '\
                                            </td>\
                                            <td>\
                                                ' + item.ItemName + '\
                                            </td>\
                                            <td>\
                                                ' + item.Size + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalOpeningStock + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalDeliveryNote + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalMaterialReceipt + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalAdjustment + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferIn + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferOut + '\
                                            </td>\
                                            <td>\
                                                '+ itemLineTotal + '\
                                            </td>\
                                        </tr>';
                                    }
                                }
                            }
                            else if ($("#divisions").val() != "") {
                                if (item.DivisionId == $("#divisions").val()) {
                                    if (item.TotalOpeningStock > 0 || item.TotalDeliveryNote > 0 || item.TotalMaterialReceipt > 0
                                    && item.TotalAdjustment > 0 || item.TotalTransferIn > 0 || item.TotalTransferOut > 0) {
                                        var itemLineTotal = (item.TotalOpeningStock + item.TotalMaterialReceipt + item.TotalTransferIn) -
                                            (item.TotalDeliveryNote + item.TotalAdjustment + item.TotalTransferOut);

                                        locationTotalOpeningStock = locationTotalOpeningStock + item.TotalOpeningStock;
                                        locationTotalDeliveryNote = locationTotalDeliveryNote + item.TotalDeliveryNote;
                                        locationTotalMaterialReceipt = locationTotalMaterialReceipt + item.TotalMaterialReceipt;
                                        locationTotalAdjustment = locationTotalAdjustment + item.TotalAdjustment;
                                        locationTotalTransferIn = locationTotalTransferIn + item.TotalTransferIn;
                                        locationTotalTransferOut = locationTotalTransferOut + item.TotalTransferOut;
                                        locationTotal = locationTotal + itemLineTotal;

                                        output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                ' + item.Description + '\
                                            </td>\
                                            <td>\
                                                ' + item.ItemName + '\
                                            </td>\
                                            <td>\
                                                ' + item.Size + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalOpeningStock + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalDeliveryNote + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalMaterialReceipt + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalAdjustment + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferIn + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferOut + '\
                                            </td>\
                                            <td>\
                                                '+ itemLineTotal + '\
                                            </td>\
                                        </tr>';
                                    }
                                }
                            }
                        }
                        output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                <b>TOTAL</b>\
                                            </td>\
                                            <td>\
                                                ' + locationTotalOpeningStock + '\
                                            </td>\
                                            <td>\
                                                ' + locationTotalDeliveryNote + '\
                                            </td>\
                                            <td>\
                                                ' + locationTotalMaterialReceipt + '\
                                            </td>\
                                            <td>\
                                                ' + locationTotalAdjustment + '\
                                            </td>\
                                            <td>\
                                                ' + locationTotalTransferIn + '\
                                            </td>\
                                            <td>\
                                                ' + locationTotalTransferOut + '\
                                            </td>\
                                            <td>\
                                                '+ locationTotal + '\
                                            </td>\
                                        </tr>';

                        GrandLocationTotalOpeningStock = GrandLocationTotalOpeningStock + locationTotalOpeningStock;
                        GrandLocationTotalDeliveryNote = GrandLocationTotalDeliveryNote + locationTotalDeliveryNote;
                        GrandLocationTotalMaterialReceipt = GrandLocationTotalMaterialReceipt + locationTotalMaterialReceipt;
                        GrandLocationTotalAdjustment = GrandLocationTotalAdjustment + locationTotalAdjustment;
                        GrandLocationTotalTransferIn = GrandLocationTotalTransferIn + locationTotalTransferIn;
                        GrandLocationTotalTransferOut = GrandLocationTotalTransferOut + locationTotalTransferOut;
                        GrandLocationTotal = GrandLocationTotal + locationTotal;
                    }

                    else if (selectedStore == data[i].StoreId)
                    {
                        output += '<tr>\
                                            <td><h4>'+ data[i].StoreName + '</h4></td>\
                                            <td><b>Part No.</b></td>\
                                            <td><b>Item Name</b></td>\
                                            <td><b>Size</b></td>\
                                            <td><b>Opening Stock</b></td>\
                                            <td><b>Sales</b></td>\
                                            <td><b>Purchase</b></td>\
                                            <td><b>Adjustment</b></td>\
                                            <td><b>Trans. In</b></td>\
                                            <td><b>Trans. Out</b></td>\
                                            <td><b>Total</b></td>\
                                        </tr>';
                        for (k = 0; k < data[i].ItemLines.length; k++)
                        {
                            var item = data[i].ItemLines[k];

                            //if (remainingGroups.find(p=>p.groupId == item.ProductGroupId) != null && remainingGroups.find(p=>p.groupId == item.ProductGroupId) != undefined)
                            //if (item.ProductGroupId == $("#subcategories").val())
                            if ($("#subcategories").val() != "")
                            {
                                if (item.SubCategoryId == $("#subcategories").val())
                                {
                                    if (item.TotalOpeningStock > 0 || item.TotalDeliveryNote > 0 || item.TotalMaterialReceipt > 0
                                    && item.TotalAdjustment > 0 || item.TotalTransferIn > 0 || item.TotalTransferOut > 0) {
                                        var itemLineTotal = (item.TotalOpeningStock + item.TotalMaterialReceipt + item.TotalTransferIn) -
                                            (item.TotalDeliveryNote + item.TotalAdjustment + item.TotalTransferOut);

                                        locationTotalOpeningStock = locationTotalOpeningStock + item.TotalOpeningStock;
                                        locationTotalDeliveryNote = locationTotalDeliveryNote + item.TotalDeliveryNote;
                                        locationTotalMaterialReceipt = locationTotalMaterialReceipt + item.TotalMaterialReceipt;
                                        locationTotalAdjustment = locationTotalAdjustment + item.TotalAdjustment;
                                        locationTotalTransferIn = locationTotalTransferIn + item.TotalTransferIn;
                                        locationTotalTransferOut = locationTotalTransferOut + item.TotalTransferOut;
                                        locationTotal = locationTotal + itemLineTotal;

                                        output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                ' + item.Description + '\
                                            </td>\
                                            <td>\
                                                ' + item.ItemName + '\
                                            </td>\
                                            <td>\
                                                ' + item.Size + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalOpeningStock + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalDeliveryNote + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalMaterialReceipt + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalAdjustment + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferIn + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferOut + '\
                                            </td>\
                                            <td>\
                                                '+ itemLineTotal + '\
                                            </td>\
                                        </tr>';
                                    }
                                }
                            }
                            else if ($("#categories").val() != "")
                            {
                                if (item.CategoryId == $("#categories").val()) {
                                    if (item.TotalOpeningStock > 0 || item.TotalDeliveryNote > 0 || item.TotalMaterialReceipt > 0
                                    && item.TotalAdjustment > 0 || item.TotalTransferIn > 0 || item.TotalTransferOut > 0) {
                                        var itemLineTotal = (item.TotalOpeningStock + item.TotalMaterialReceipt + item.TotalTransferIn) -
                                            (item.TotalDeliveryNote + item.TotalAdjustment + item.TotalTransferOut);

                                        locationTotalOpeningStock = locationTotalOpeningStock + item.TotalOpeningStock;
                                        locationTotalDeliveryNote = locationTotalDeliveryNote + item.TotalDeliveryNote;
                                        locationTotalMaterialReceipt = locationTotalMaterialReceipt + item.TotalMaterialReceipt;
                                        locationTotalAdjustment = locationTotalAdjustment + item.TotalAdjustment;
                                        locationTotalTransferIn = locationTotalTransferIn + item.TotalTransferIn;
                                        locationTotalTransferOut = locationTotalTransferOut + item.TotalTransferOut;
                                        locationTotal = locationTotal + itemLineTotal;

                                        output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                ' + item.Description + '\
                                            </td>\
                                            <td>\
                                                ' + item.ItemName + '\
                                            </td>\
                                            <td>\
                                                ' + item.Size + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalOpeningStock + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalDeliveryNote + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalMaterialReceipt + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalAdjustment + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferIn + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferOut + '\
                                            </td>\
                                            <td>\
                                                '+ itemLineTotal + '\
                                            </td>\
                                        </tr>';
                                    }
                                }
                            }
                            else if ($("#groups").val() != "")
                            {
                                if (item.GroupId == $("#groups").val()) {
                                    if (item.TotalOpeningStock > 0 || item.TotalDeliveryNote > 0 || item.TotalMaterialReceipt > 0
                                    && item.TotalAdjustment > 0 || item.TotalTransferIn > 0 || item.TotalTransferOut > 0) {
                                        var itemLineTotal = (item.TotalOpeningStock + item.TotalMaterialReceipt + item.TotalTransferIn) -
                                            (item.TotalDeliveryNote + item.TotalAdjustment + item.TotalTransferOut);

                                        locationTotalOpeningStock = locationTotalOpeningStock + item.TotalOpeningStock;
                                        locationTotalDeliveryNote = locationTotalDeliveryNote + item.TotalDeliveryNote;
                                        locationTotalMaterialReceipt = locationTotalMaterialReceipt + item.TotalMaterialReceipt;
                                        locationTotalAdjustment = locationTotalAdjustment + item.TotalAdjustment;
                                        locationTotalTransferIn = locationTotalTransferIn + item.TotalTransferIn;
                                        locationTotalTransferOut = locationTotalTransferOut + item.TotalTransferOut;
                                        locationTotal = locationTotal + itemLineTotal;

                                        output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                ' + item.Description + '\
                                            </td>\
                                            <td>\
                                                ' + item.ItemName + '\
                                            </td>\
                                            <td>\
                                                ' + item.Size + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalOpeningStock + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalDeliveryNote + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalMaterialReceipt + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalAdjustment + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferIn + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferOut + '\
                                            </td>\
                                            <td>\
                                                '+ itemLineTotal + '\
                                            </td>\
                                        </tr>';
                                    }
                                }
                            }
                            else if ($("#subgroups").val() != "")
                            {
                                if (item.SubGroupId == $("#subgroups").val()) {
                                    if (item.TotalOpeningStock > 0 || item.TotalDeliveryNote > 0 || item.TotalMaterialReceipt > 0
                                    && item.TotalAdjustment > 0 || item.TotalTransferIn > 0 || item.TotalTransferOut > 0) {
                                        var itemLineTotal = (item.TotalOpeningStock + item.TotalMaterialReceipt + item.TotalTransferIn) -
                                            (item.TotalDeliveryNote + item.TotalAdjustment + item.TotalTransferOut);

                                        locationTotalOpeningStock = locationTotalOpeningStock + item.TotalOpeningStock;
                                        locationTotalDeliveryNote = locationTotalDeliveryNote + item.TotalDeliveryNote;
                                        locationTotalMaterialReceipt = locationTotalMaterialReceipt + item.TotalMaterialReceipt;
                                        locationTotalAdjustment = locationTotalAdjustment + item.TotalAdjustment;
                                        locationTotalTransferIn = locationTotalTransferIn + item.TotalTransferIn;
                                        locationTotalTransferOut = locationTotalTransferOut + item.TotalTransferOut;
                                        locationTotal = locationTotal + itemLineTotal;

                                        output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                ' + item.Description + '\
                                            </td>\
                                            <td>\
                                                ' + item.ItemName + '\
                                            </td>\
                                            <td>\
                                                ' + item.Size + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalOpeningStock + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalDeliveryNote + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalMaterialReceipt + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalAdjustment + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferIn + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferOut + '\
                                            </td>\
                                            <td>\
                                                '+ itemLineTotal + '\
                                            </td>\
                                        </tr>';
                                    }
                                }
                            }
                            else if ($("#divisions").val() != "")
                            {
                                if (item.DivisionId == $("#divisions").val())
                                {
                                    if (item.TotalOpeningStock > 0 || item.TotalDeliveryNote > 0 || item.TotalMaterialReceipt > 0
                                    && item.TotalAdjustment > 0 || item.TotalTransferIn > 0 || item.TotalTransferOut > 0) {
                                        var itemLineTotal = (item.TotalOpeningStock + item.TotalMaterialReceipt + item.TotalTransferIn) -
                                            (item.TotalDeliveryNote + item.TotalAdjustment + item.TotalTransferOut);

                                        locationTotalOpeningStock = locationTotalOpeningStock + item.TotalOpeningStock;
                                        locationTotalDeliveryNote = locationTotalDeliveryNote + item.TotalDeliveryNote;
                                        locationTotalMaterialReceipt = locationTotalMaterialReceipt + item.TotalMaterialReceipt;
                                        locationTotalAdjustment = locationTotalAdjustment + item.TotalAdjustment;
                                        locationTotalTransferIn = locationTotalTransferIn + item.TotalTransferIn;
                                        locationTotalTransferOut = locationTotalTransferOut + item.TotalTransferOut;
                                        locationTotal = locationTotal + itemLineTotal;

                                        output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                ' + item.Description + '\
                                            </td>\
                                            <td>\
                                                ' + item.ItemName + '\
                                            </td>\
                                            <td>\
                                                ' + item.Size + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalOpeningStock + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalDeliveryNote + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalMaterialReceipt + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalAdjustment + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferIn + '\
                                            </td>\
                                            <td>\
                                                ' + item.TotalTransferOut + '\
                                            </td>\
                                            <td>\
                                                '+ itemLineTotal + '\
                                            </td>\
                                        </tr>';
                                    }
                                }
                            }
                        }
                        output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                <b>TOTAL</b>\
                                            </td>\
                                            <td>\
                                                ' + locationTotalOpeningStock + '\
                                            </td>\
                                            <td>\
                                                ' + locationTotalDeliveryNote + '\
                                            </td>\
                                            <td>\
                                                ' + locationTotalMaterialReceipt + '\
                                            </td>\
                                            <td>\
                                                ' + locationTotalAdjustment + '\
                                            </td>\
                                            <td>\
                                                ' + locationTotalTransferIn + '\
                                            </td>\
                                            <td>\
                                                ' + locationTotalTransferOut + '\
                                            </td>\
                                            <td>\
                                                '+ locationTotal + '\
                                            </td>\
                                        </tr>';

                        GrandLocationTotalOpeningStock = GrandLocationTotalOpeningStock + locationTotalOpeningStock;
                        GrandLocationTotalDeliveryNote = GrandLocationTotalDeliveryNote + locationTotalDeliveryNote;
                        GrandLocationTotalMaterialReceipt = GrandLocationTotalMaterialReceipt + locationTotalMaterialReceipt;
                        GrandLocationTotalAdjustment = GrandLocationTotalAdjustment + locationTotalAdjustment;
                        GrandLocationTotalTransferIn = GrandLocationTotalTransferIn + locationTotalTransferIn;
                        GrandLocationTotalTransferOut = GrandLocationTotalTransferOut + locationTotalTransferOut;
                        GrandLocationTotal = GrandLocationTotal + locationTotal;
                    }

                }
                //renderGrid();
                //<td style="border-color: black;">\
                output += '<tr>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                &nbsp;\
                                            </td>\
                                            <td>\
                                                <b>GRAND TOTAL</b>\
                                            </td>\
                                            <td>\
                                                <b>' + GrandLocationTotalOpeningStock + '</b>\
                                            </td>\
                                            <td>\
                                                <b>' + GrandLocationTotalDeliveryNote + '</b>\
                                            </td>\
                                            <td>\
                                                <b>' + GrandLocationTotalMaterialReceipt + '</b>\
                                            </td>\
                                            <td>\
                                                <b>' + GrandLocationTotalAdjustment + '</b>\
                                            </td>\
                                            <td>\
                                                <b>' + GrandLocationTotalTransferIn + '</b>\
                                            </td>\
                                            <td>\
                                                ' + GrandLocationTotalTransferOut + '</b>\
                                            </td>\
                                            <td>\
                                                <b>' + GrandLocationTotal + '</b>\
                                            </td>\
                                        </tr>';
                $("#reportTbody").html(output);
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    });

    $("#excelExport").click(function () {
        $('#rptTable').tableExport({ type: 'excel', escape: 'false' });
    });
        
    $("#divisions").change(function () {
        var groupsHtml = "<option value=\"\">--select--</option>";
        groups = [];
        for (i = 0; i < productGroups.length; i++) {
            if (productGroups[i].Extra1 == "Group" && productGroups[i].GroupUnder == $("#divisions").val()) {
                groups.push({ groupId: productGroups[i].GroupId, groupName: productGroups[i].GroupName });
                groupsHtml += '<option value="' + productGroups[i].GroupId + '">' + productGroups[i].GroupName + '</option>';
            }
        }
        $("#groups").html(groupsHtml);
    });

    $("#groups").change(function () {
        var subGroupsHtml = "<option value=\"\">--select--</option>";
        subGroups = [];
        for (i = 0; i < productGroups.length; i++) {
            if (productGroups[i].Extra1 == "Sub Group" && productGroups[i].GroupUnder == $("#groups").val()) {
                subGroups.push({ groupId: productGroups[i].GroupId, groupName: productGroups[i].GroupName });
                subGroupsHtml += '<option value="' + productGroups[i].GroupId + '">' + productGroups[i].GroupName + '</option>';
            }
        }
        $("#subgroups").html(subGroupsHtml);
    });

    $("#subgroups").change(function () {
        var categoriesHtml = "<option value=\"\">--select--</option>";
        subGroups = [];
        for (i = 0; i < productGroups.length; i++) {
            if (productGroups[i].Extra1 == "Category" && productGroups[i].GroupUnder == $("#subgroups").val()) {
                categories.push({ groupId: productGroups[i].GroupId, groupName: productGroups[i].GroupName });
                categoriesHtml += '<option value="' + productGroups[i].GroupId + '">' + productGroups[i].GroupName + '</option>';
            }
        }
        $("#categories").html(categoriesHtml);
    });

    $("#categories").change(function () {
        var subCategoriesHtml = "<option value=\"\">--select--</option>";
        subcategories = [];
        for (i = 0; i < productGroups.length; i++) {
            if (productGroups[i].Extra1 == "Sub Category" && productGroups[i].GroupUnder == $("#categories").val()) {
                subCategories.push({ groupId: productGroups[i].GroupId, groupName: productGroups[i].GroupName });
                subCategoriesHtml += '<option value="' + productGroups[i].GroupId + '">' + productGroups[i].GroupName + '</option>';
            }
        }
        $("#subcategories").html(subCategoriesHtml);
    });
});

function loadProfiles() {
    Utilities.Loader.Show();
    var profileAjx = $.ajax({
        url: API_BASE_URL + '/ItemGroup/GetProductGroups',
        type: "GET",
        contentType: "application/json"
    });

    var storesAjx = $.ajax({
        url: API_BASE_URL + '/Store/GetStores',
        type: "GET",
        contentType: "application/json"
    });

    $.when(profileAjx, storesAjx)
    .done(function (dataProfiles, dataStores) {
        stores = dataStores[2].responseJSON;
        productGroups = dataProfiles[2].responseJSON;

        var objToShow = [];
        var divisionsHtml = "<option>--select--</option>";
        var groupsHtml = "<option>--select--</option>";
        var subGroupsHtml = "<option>--select--</option>";
        var categoriesHtml = "<option>--select--</option>";
        var subCategoriesHtml = "<option>--select--</option>";


        for (i = 0; i < productGroups.length; i++)
        {
            if (productGroups[i].Extra1 == "Division")
            {
                filteredGroups.push({ groupId: productGroups[i].GroupId, groupName: productGroups[i].GroupName });
                divisions.push({ groupId: productGroups[i].GroupId, groupName: productGroups[i].GroupName });
                divisionsHtml += '<option value="' + productGroups[i].GroupId + '">' + productGroups[i].GroupName + '</option>';
            }

        }
        $("#divisions").html(divisionsHtml);
        //$("#groups").html(groupsHtml);
        //$("#subGroups").html(subGroupsHtml);
        //$("#categoriess").html(categoriesHtml);
        //$("#subCategories").html(subCategoriesHtml);

        var storesHtml = "<option value=\"All\">All</option>";
        for (i = 0; i < stores.length; i++) {
            storesHtml += '<option value="' + stores[i].GodownId + '">' + stores[i].GodownName + '</option>';
        }
        $("#stores").html(storesHtml);
        Utilities.Loader.Hide();
    });
}