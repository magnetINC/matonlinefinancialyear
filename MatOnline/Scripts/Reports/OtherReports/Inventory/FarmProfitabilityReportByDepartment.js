﻿var categories = [];
getLookUps();
var teehee = [];



function getLookUps() {
    var categoryLookupAjax = $.ajax({
        url: API_BASE_URL + "/Category/GetCategory",
        type: "GET",
        contentType: "application/json",
    });

    $.when(categoryLookupAjax)
        .done(function (dataCategory) {
            categories = dataCategory;
            console.log("categories", dataCategory);
            $("#category").html("<option value=\"0\">All</option>" + Utilities.PopulateDropDownFromArray(categories, 0, 1));
            $("#category").chosen({ width: "100%", margin: "1px" });
        });
}

$(function () {
    $.get(API_BASE_URL + "/Store/GetStores", function (data) {
        var html = "";
        html += "<option value='0'>-All-</option>";

        data.forEach(function (a) {
            html += `<option value="${a.GodownId}">${a.GodownName}</option>`;
        });
        $("#storeCtrl").html(html);
    });
});

$("#search").click(function () {
    var storeId = 0; // $("#storeCtrl").val();
    var categoryId = $("#category").val();
    var fromDate = $("#fromDate").val();
    var toDate = $("#toDate").val();

    loadReport(fromDate, toDate, categoryId, storeId);

});
//(DateTime? fromDate , DateTime? toDate, int? batchId, int? storeId )
function loadReport(fromDate, toDate, categoryId, storeId) {
    $.ajax(API_BASE_URL + "/StockJournal/GetStockProfitabilityByDepartmentRecord/?fromDate=" + fromDate + "&toDate=" + toDate + "&categoryId=" + categoryId + "&storeId=" + storeId,
        {
            type: "get",
            beforeSend: function () {
                Utilities.Loader.Show();
            },
            complete: function () {
                Utilities.Loader.Hide();
            },
            success: function (data) {
                
                var html = "";
                var consumptionStockList = [];
                var productionStockList = [];
                var otherCostsList = [];
                var adjustmentsList = [];
                var productionData = [];
                
                $("#tblBodyHarvest").html("");
                //html +=
                //    `<tr><th style="white-space: nowrap;"  class="text-center"  colspan='7'>-------------------------------Harvest--------------------------------------</th>  </tr>`;
              
                data.forEach(function (a) {
                    if (a.StockMaster.extra1 == "Manufacturing") {
                        ////
                        productionData.push(a);
                        if (categoryId != 0 && categoryId != undefined) {
                            a.StockDetail = a.StockDetail.filter(a => {
                                var cid = (a.Category != null ||
                                    a.Category != undefined)
                                    ? a.Category.CategoryId
                                    : 0;
                                return cid == categoryId;
                            });
                        }

                        //if (storeId != 0 && storeId != "") {
                        //    a.StockDetail = a.StockDetail.filter(c => c.Stock.godownId == storeId);
                        //}

                        //for (var i of a.StockDetail) {

                        //    if (i.Stock.consumptionOrProduction == "Consumption") {

                        //        if (a.StockMaster.stockJournalMasterId == i.Stock.stockJournalMasterId) {
                        //            var addFeedConsumptionHeader = a.StockDetail.length == 0 ? '' : `<tr>
                        //                        <th style="white-space: nowrap;" colspan='7'>Feed Consumption</th>  </tr>`;
                        //            html += addFeedConsumptionHeader;
                        //            break;
                        //        }
                        //    }
                        //}

                        for (var b of a.StockDetail) {
                            if (b.Stock.consumptionOrProduction == "Consumption") {
                                //html += `<tr>
                                //   <th style="white-space: nowrap;" colspan='7'>Feed Consumption</th>  </tr>`;

                                var bomQty = b.BOM.quantity;
                                var itemProductionStockDetail = a.StockDetail.find(p => p.Stock.consumptionOrProduction == "Production");
                                var standardQuantity = itemProductionStockDetail == undefined ? "NA" : bomQty * itemProductionStockDetail.Stock.qty;
                                var averageCost = 0;
                                var totalQty = 0;
                                var totalCost = 0;
                                b.AverageCost.forEach(b => totalCost += (b.inwardQty * (b.rate).toFixed(2)));
                                b.AverageCost.forEach(b => totalQty += b.inwardQty);
                                averageCost = totalCost / totalQty;
                                if (a.StockMaster.stockJournalMasterId == b.Stock.stockJournalMasterId)
                                    var obj = {};
                                obj.productName = b.Product.productName;
                                obj.productId = b.Stock.productId;
                                obj.debit = b.Stock.qty * averageCost;
                                consumptionStockList.push(obj);
                                //    html +=
                                //        `<tr>
                                //    <td style="white-space: nowrap;">${new Date(a.StockMaster.extraDate).toDateString()}</td>
                                //    <td style="white-space: nowrap;">${b.Product.productName}</td>
                                //    <td style="white-space: nowrap;">${(b.Project != null ||
                                //            b.Project != undefined)
                                //            ? b.Project.ProjectName
                                //            : "NA"}</td>
                                //                    <td style="white-space: nowrap;">${(b.Store != null ||
                                //            b.Store != undefined)
                                //            ? b.Store.godownName
                                //            : "NA"}</td>
                                //                    <td style="white-space: nowrap;">${b.Stock.qty * averageCost}</td>
                                //                    <td style="white-space: nowrap;"></td>
                                //                    <td style="white-space: nowrap;"></td>
                                //</tr>`;
                            }
                            if (b.Stock.consumptionOrProduction == "Production") {
                                if (a.StockMaster.stockJournalMasterId == b.Stock.stockJournalMasterId)
                                    var obj = {};
                                obj.productName = b.Product.productName;
                                obj.productId = b.Stock.productId;
                                /*obj.credit = b.Stock.qty * b.Stock.rate; To pick the SaleRate For BOM */
                                obj.credit = b.Stock.qty * b.Product.salesRate;
                                obj.qty = b.Stock.qty;
                                /*obj.rate = b.Stock.rate; To pick the SaleRate For BOM*/
                                obj.rate = b.Product.salesRate;
                                obj.rateCount = 1;
                                productionStockList.push(obj);
                                //    html += `<tr>
                                //                <th style="white-space: nowrap;" colspan='7'>Item Production</th>  </tr>`;
                                //html += `<tr>
                                //    <td style="white-space: nowrap;">${new Date(a.StockMaster.extraDate).toDateString()}</td>

                                //                    <td style="white-space: nowrap;">${b.Product.productName}</td>
                                //    <td style="white-space: nowrap;">${(b.Project != null ||
                                //        b.Project != undefined)
                                //        ? b.Project.ProjectName
                                //        : "NA"}</td>
                                //                    <td style="white-space: nowrap;">
                                //               ${(b.Store != null ||
                                //        b.Store != undefined)
                                //        ? b.Store.godownName
                                //        : "NA"}
                                //                  </td>
                                //                    <td style="white-space: nowrap;"></td>
                                //                    <td style="white-space: nowrap;">${b.Stock.qty * b.Stock.rate}</td>
                                //                    <td style="white-space: nowrap;"></td>
                                //                </tr>`;
                            }
                        }
                    }
                    //start additional cost
                    if (a.StockDetail.length != 0 && a.AdditionalCosts.length != 0) {
                        //html += `<tr>
                        //                        <th style="white-space: nowrap;" colspan='7'>Additional Costs</th>  </tr>
                        //                        <tr>
                        //                        <th style="white-space: nowrap;">Date</th>
                        //                        <th style="white-space: nowrap;">Ac/Ledger</th>
                        //                        <th style="white-space: nowrap;">Amount</th>
                        //                        <th style="white-space: nowrap;">Project</th></tr>`;
                        a.AdditionalCosts.forEach(function (item) {
                            var obj = {};
                            obj.ledgerName = item.AccountLedgerName;
                            obj.ledgerId = item.AdditionalCost.ledgerId;
                            obj.debit = item.AdditionalCost.debit;
                            otherCostsList.push(obj);
                            //html += `<tr>
                            //        <td style="white-space: nowrap;">${new Date(item.AdditionalCost.extraDate).toDateString()}</td>
                            //        <td style="white-space: nowrap;">${item.AccountLedgerName}</td>
                            //        <td style="white-space: nowrap;">${item.AdditionalCost.debit}</td>
                            //      <td style="white-space: nowrap;">${item.Project == undefined?"NA": item.Project.ProjectName}</td>
                            //         </tr>`;
                        })
                    }
                    //if (a.StockDetail.length != 0)
                    //    html += '<tr style="border-bottom: 2px solid #4d627b;"><td colspan="11">   </td> </tr>';
                    //end additional cost
                }
                );

                //start merge
                
                console.log("consumptionStockList:", consumptionStockList);
                console.log("productionStockList:", productionStockList);
                console.log("otherCostsList", otherCostsList);
                //teehee = productionStockList;
                var consumptionStockListMerger = [];
                var productionStockListMerger = [];
                var otherCostsListMerger = [];
                var consumptionStockListTotal = 0;
                var productionStockListTotal = 0;
                var otherCostsListTotal = 0;
                var adjustmentListTotal = 0;
                var farmActivityProfiabilityTotal = 0;
                consumptionStockList.forEach(function (item) {
                    var mergerItem = consumptionStockListMerger.find(p => p.productId == item.productId);
                    if (mergerItem == undefined) {
                        consumptionStockListMerger.push(item)
                    }
                    else {
                        mergerItem.debit += item.debit;
                    }

                });

                productionStockList.forEach(function (item) {
                    var mergerItem = productionStockListMerger.find(p => p.productId == item.productId);
                    if (mergerItem == undefined) {
                        productionStockListMerger.push(item)
                    }
                    else {
                        mergerItem.credit += item.credit;
                        mergerItem.qty += item.qty;
                        mergerItem.rate += item.rate;
                        mergerItem.rateCount++
                    }

                });
                otherCostsList.forEach(function (item) {
                    var mergerItem = otherCostsListMerger.find(p => p.ledgerId == item.ledgerId);
                    if (mergerItem == undefined) {
                        otherCostsListMerger.push(item)
                    }
                    else {
                        mergerItem.debit += item.debit;
                    }

                });
                console.log("consumptionStockListMerger:", consumptionStockListMerger);
                console.log("productionStockListMerger:", productionStockListMerger);
                console.log("otherCostsListMerger", otherCostsListMerger);
                html +=
                    `<tr><th style="white-space: nowrap;"   colspan='4'>Qty Produced</th>  </tr>`;
                if (productionStockListMerger.length != 0) {
                    productionStockListMerger.forEach(function (item) {
                        productionStockListTotal += item.credit;
                        html += `<tr>
                                    <td style="white-space: nowrap;">${item.productName}</td>
                                     <td style="white-space: nowrap;"></td>
                                    <td style="white-space: nowrap;">${Utilities.FormatCurrency(item.credit)} </td>
                                     </tr>`;
                        //${item.qty} * ${(item.rate / item.rateCount).toFixed(2)}
                    })
                }
                html += `<tr style="background-color: #b3b3b359 !important;">
                                    <td style="white-space: nowrap;"><strong>Subtotal</strong></td>
                                     <td style="white-space: nowrap;"></td>
                                    <td style="white-space: nowrap;"><strong>${Utilities.FormatCurrency(productionStockListTotal)}</strong></td>
                                     </tr>`;
                html +=
                    `<tr><th style="white-space: nowrap;"  colspan='4'>Cost of Production</th>  </tr>`;
                if (consumptionStockListMerger.length != 0) {
                    consumptionStockListMerger.forEach(function (item) {
                        consumptionStockListTotal += item.debit;
                        html += `<tr>
                                    <td style="white-space: nowrap;">${item.productName}</td>
                                    <td style="white-space: nowrap;">${Utilities.FormatCurrency(item.debit)}</td>
                                     <td style="white-space: nowrap;"></td>
                                     </tr>`;
                    })
                }
                html += `<tr style="background-color: #b3b3b359 !important;">
                                    <td style="white-space: nowrap;"><strong>Subtotal</strong></td>
                                    <td style="white-space: nowrap;"></td>
                                     <td style="white-space: nowrap;"><strong>(${Utilities.FormatCurrency(consumptionStockListTotal)})</strong></td>
                                     </tr>`;
                html +=
                    `<tr><th style="white-space: nowrap;"  colspan='4'>Other Costs</th>  </tr>`;
                if (otherCostsListMerger.length != 0) {
                    otherCostsListMerger.forEach(function (item) {
                        otherCostsListTotal += item.debit;
                        html += `<tr>
                                    <td style="white-space: nowrap;">${item.ledgerName}</td>
                                    <td style="white-space: nowrap;">${Utilities.FormatCurrency(item.debit)}</td>
                                     <td style="white-space: nowrap;"></td>
                                     </tr>`;
                    })
                }
                html += `<tr style="background-color: #b3b3b359 !important;">
                                    <td style="white-space: nowrap;"><strong>Subtotal</strong></td>
                                    <td style="white-space: nowrap;"></td>
                                     <td style="white-space: nowrap;"><strong>(${Utilities.FormatCurrency(otherCostsListTotal)})</strong></td>
                                     </tr>`;
                //end merge

                //Stock Out Reports

                data.forEach(function (a) {

                    if (a.StockMaster.extra1 == "Stock Out") {
                        if (categoryId != 0 && categoryId != undefined) {
                            a.StockDetail = a.StockDetail.filter(a => {
                                var cid = (a.Category != null ||
                                    a.Category != undefined)
                                    ? a.Category.categoryId
                                    : 0;
                                return cid == categoryId;
                            });
                        }

                        if (storeId != 0 && storeId != "") {
                            a.StockDetail = a.StockDetail.filter(c => c.Stock.godownId == storeId);
                        }

                        for (var b of a.StockDetail) {

                            if (b.Stock.consumptionOrProduction == "Production") {
                                if (a.StockMaster.stockJournalMasterId == b.Stock.stockJournalMasterId)
                                    var obj = {};
                                obj.productName = b.Product.productName;
                                obj.productId = b.Stock.productId;
                                obj.qty = b.Stock.qty;
                                adjustmentsList.push(obj);
                            }
                        }
                    }

                });
                console.log("adjustmentsList:", adjustmentsList);
                var adjustmentsListWithProductData = [];

                var arc = [];
                //find all the data related to particular products in the adjustmentsList
                adjustmentsList.forEach(function (element) {
                    var productData = [];
                    productionData.forEach(function (item) {
                        if (item.StockDetail.length > 0) {
                            if (item.StockDetail[item.StockDetail.length - 1].Product.productId == element.productId) {
                                productData.push(item);
                            }
                        }
                    })
                    adjustmentsListWithProductData.push(productData);
                });
                var isAdjustment = false;

                //if a product has data, add them up to get the adjustment value
                for (var i = 0; i < adjustmentsListWithProductData.length; i++) {
                 
                    if (adjustmentsListWithProductData[i].length > 1) {
                        if (isAdjustment == false) {
                            isAdjustment = true;
                            html +=
                                `<tr><th style="white-space: nowrap;"  colspan='4'>Stock Out</th>  </tr>`;
                        }
                        var obj = {};
                        var adjustmentValue = 0;
                        obj.debit = 0;
                        obj.productQty = 0;
                        var finishedGoodItemIndex = adjustmentsListWithProductData[i][0].StockDetail.length - 1;
                        obj.productName = adjustmentsListWithProductData[i][0].StockDetail[finishedGoodItemIndex].Product.productName;
                        obj.qtyAdjusted = adjustmentsList[i].qty;

                        for (var ad of adjustmentsListWithProductData[i]) {
                            for (var bd of ad.StockDetail) {
                                if (bd.Stock.consumptionOrProduction == "Consumption") {
                                    var averageCost = 0;
                                    var totalQty = 0;
                                    var totalCost = 0;
                                    bd.AverageCost.forEach(b => totalCost += (b.inwardQty * (b.rate).toFixed(2)));
                                    bd.AverageCost.forEach(b => totalQty += b.inwardQty);
                                    averageCost = totalCost / totalQty;
                                    obj.debit += bd.Stock.qty * averageCost;
                                }
                                else if (bd.Stock.consumptionOrProduction == "Production") {
                                    obj.productQty += bd.Stock.qty;
                                }
                            }
                            ad.AdditionalCosts.forEach(function (item) {
                                obj.debit += item.AdditionalCost.debit;
                            })
                        }
                        adjustmentValue = obj.debit / obj.productQty * obj.qtyAdjusted;
                        adjustmentListTotal += adjustmentValue;
                        html += `<tr>
                                    <td style="white-space: nowrap;">${obj.productName}</td>
                                    <td style="white-space: nowrap;">${Utilities.FormatCurrency(adjustmentValue)}</td>
                                     <td style="white-space: nowrap;"></td>
                                     </tr>`;;
                    }
                }
                farmActivityProfiabilityTotal = productionStockListTotal - consumptionStockListTotal - otherCostsListTotal - adjustmentListTotal;
                html += `<tr style="background-color: #b3b3b359 !important;">
                                    <td style="white-space: nowrap;"><strong>Subtotal</strong></td>
                                    <td style="white-space: nowrap;"></td>
                                     <td style="white-space: nowrap;"><strong>(${Utilities.FormatCurrency(adjustmentListTotal)})</strong></td>
                                     </tr>`;
                html += `<tr style="background-color: #b3b3b359 !important;">
                                    <td style="white-space: nowrap;"><strong>Total Profit</strong></td>
                                    <td style="white-space: nowrap;"></td>
                                     <td style="white-space: nowrap;"><strong>${Utilities.FormatCurrency(farmActivityProfiabilityTotal)}</strong></td>
                                     </tr>`;
                html += `<tr style="background-color: white !important;color:#ffc107; font-weight: bolder; font-style: italic;">
                                     <td style="white-space: nowrap;" colspan="3"><strong>Note:</strong> <strong>QTY PRODUCED = Total Qty * Sales Rate </strong></td>
                                     </tr>`;

                console.log("adjustmentsListWithProductData", adjustmentsListWithProductData)
                $("#tblBodyHarvest").html(html);
            }

        });
    ;

}