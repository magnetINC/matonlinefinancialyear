﻿var products = [];
var stores = [];
var searchParam = {};
var physicalStock = [];
var lookups = {};
var table = "";

$(function () {

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/InventoryReportLookup/PhysicalStock",
        type: "GET",
        contentType: "application/json",
        data: JSON.stringify(searchParam),
        success: function (data)
        {
            lookups = data;
            $("#formType").html('<option value="0">All</option>' + Utilities.PopulateDropDownFromArray(lookups.VoucherTypes, 0, 1));
            $("#store").html('<option value="0">All</option>' + Utilities.PopulateDropDownFromArray(lookups.Store, 0, 1));
            //$("#product").html('<option value="0">All</option>' + Utilities.PopulateDropDownFromArray(lookups.Product, 0, 2));
            var prods = [{
                            productId: "0",
                            productName: "All"
                        }];
            for (i = 0; i < lookups.Product.length; i++) {
                prods.push({
                    productName: lookups.Product[i].productName,
                    productId: lookups.Product[i].productId
                });
            }
            $("#product").kendoDropDownList({
                filter: "contains",
                dataTextField: "productName",
                dataValueField: "productId",
                dataSource: prods
            });

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });

    $("#search").click(function () {
        loadPhysicalStock();
    });

});

function loadPhysicalStock()
{
    searchParam = {
        FromDate: $("#fromDate").val(),
        ToDate: $("#toDate").val(),
        ProductName: $("#productName").val(),
        VoucherNo: $("#formNo").val(),
        VoucherTypeId: $("#formType").val(),
        ProductCode: $("#product").val(),
        StoreId: $("#store").val(),
        StrProductCode: $("#productCode").val()
    };

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/InventoryReport/PhysicalStock",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(searchParam),
        success:function(data)
        {
            physicalStock = data;
            //console.log(physicalStock);
            //return;
            var output = "";
            var objToShow = [];
            for (i = 0; i < physicalStock.length; i++)
            {
                objToShow.push([
                    (i + 1),
                    physicalStock[i].productcode,
                    physicalStock[i].godownname,
                    physicalStock[i].qty,
                    physicalStock[i].productname,
                    physicalStock[i].brandname,
                    physicalStock[i].size,
                    Utilities.FormatCurrency(physicalStock[i].purchaserate),
                    Utilities.FormatCurrency(physicalStock[i].salesrate)
                ]);
                //output += '<tr>\
                //            <td>' +(i+1)+'</td>\
                //            <td>' + physicalStock[i].productcode + '</td>\
                //            <td>' + physicalStock[i].godownname + '</td>\
                //            <td>' + physicalStock[i].qty + '</td>\
                //            <td>' + physicalStock[i].productname + '</td>\
                //            <td>' + physicalStock[i].brandname + '</td>\
                //            <td>' + physicalStock[i].size + '</td>\
                //            <td>NA</td>\
                //            <td>' + Utilities.FormatCurrency(physicalStock[i].purchaserate) + '</td>\
                //            <td>' + Utilities.FormatCurrency(physicalStock[i].salesrate) + '</td>\
                //           </tr>';

                //<td>' + physicalStock[i].mrp.toFixed(2) + '</td>\
            }
            //$("#physicalStockTbody").html(output);
            if (table != "")
            {
                table.destroy();
            }
            table = $('#physicalstockTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            Utilities.Loader.Hide();
        },
        error:function(err)
        {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });
}