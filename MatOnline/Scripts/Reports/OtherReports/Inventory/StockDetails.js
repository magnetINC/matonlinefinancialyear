﻿var products = [];
var stores = [];
var searchParam = {};
var stockDetails = [];
var lookups = {};
var projects = [];
var categories = [];

$(function () {

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/InventoryReportLookup/StockDetails",
        type: "GET",
        contentType: "application/json",
        data: JSON.stringify(searchParam),
        success: function (data) {
            lookups = data;
            $("#store").html('<option value="0">All</option>' + Utilities.PopulateDropDownFromArray(lookups.Store, 0, 1));
            $("#store").chosen({ width: "100%", margin: "1px" });
            $("#batchNo").html('<option value="All">All</option>' + Utilities.PopulateDropDownFromArray(lookups.Batch, 0, 1));
            $("#batchNo").chosen({ width: "100%", margin: "1px" });
            $("#product").html('<option value="0">All</option>' + Utilities.PopulateDropDownFromArray(lookups.Product, 0, 2));
            $("#product").chosen({ width: "100%", margin: "1px" });

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            console.log(err);
        }
    });

    $("#search").click(function ()
    {
        loadStockDetails();
    });

});

function loadStockDetails() {

    //debugger;

    searchParam = {
        FromDate: $("#fromDate").val(),
        ToDate: $("#toDate").val(),
        StoreId: $("#store").val(),
        BatchNo: $("#batchNo").val(),
        ProductId: $("#product").val(),
        ProductCode: $("#productCode").val(),
        ProjectId: $("#project").val(),
        CategoryId: $("#category").val(),
        TransactionType: $("#TransactionType").val(),
        refNo: ""
    };
  
    Utilities.Loader.Show();

    $.ajax({
        url: API_BASE_URL + "/InventoryReport/StockReportDetails",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(searchParam),
        success: function (data)
        {
          //  alert(data);
            stockDetails = data.StockReportRowItem;
           // var StockDetailsOpenningBalance = data.OpenningBalanceStockDetails;
            //debugger;
            console.log("stockDetails", stockDetails);                    
            stockDetails = CleanDbStockDetails(data.StockReportRowItem)

            var output = "";

            //var outputOpenningBalance = "";
            //if (StockDetailsOpenningBalance != undefined)
            //{
            //    outputOpenningBalance += '<tr style="background-color:#00bfff;">\
            //                <td>' + StockDetailsOpenningBalance.Store + '</td>\
            //                <td>' + StockDetailsOpenningBalance.ProductName + '</td>\
            //                <td>' + StockDetailsOpenningBalance.Title + '</td>\
            //                <td>' + StockDetailsOpenningBalance.VoucherType + '</td>\
            //                <td>' + StockDetailsOpenningBalance.Batch + '</td>\
            //                <td>' + StockDetailsOpenningBalance.DateStart + '</td>\
            //                <td>' + StockDetailsOpenningBalance.Rate + '</td>\
            //                <td>' + StockDetailsOpenningBalance.InWard + '</td>\
            //                <td>' + StockDetailsOpenningBalance.OutWard + '</td>\
            //                <td>' + StockDetailsOpenningBalance.QtyRemain + '</td>\
            //                <td>' + StockDetailsOpenningBalance.AvaCost + '</td>\
            //                <td>' + StockDetailsOpenningBalance.StockValue + '</td>\
            //                <td>' + StockDetailsOpenningBalance.Project + '</td>\
            //                <td>' + StockDetailsOpenningBalance.Category + '</td>\
            //               </tr>';

            //}
            //else
            //{

            //    outputOpenningBalance += '<tr style="background-color:#00bfff;">\
            //                <td>' + StockDetailsOpenningBalance.Store + '</td>\
            //                <td>' + StockDetailsOpenningBalance.ProductName + '</td>\
            //                <td>' + StockDetailsOpenningBalance.Title + '</td>\
            //                <td>' + StockDetailsOpenningBalance.VoucherType + '</td>\
            //                <td>' + StockDetailsOpenningBalance.Batch + '</td>\
            //                <td>' + StockDetailsOpenningBalance.DateStart + '</td>\
            //                <td>' + StockDetailsOpenningBalance.Rate + '</td>\
            //                <td>' + StockDetailsOpenningBalance.InWard + '</td>\
            //                <td>' + StockDetailsOpenningBalance.OutWard + '</td>\
            //                <td>' + StockDetailsOpenningBalance.QtyRemain + '</td>\
            //                <td>' + StockDetailsOpenningBalance.AvaCost + '</td>\
            //                <td>' + StockDetailsOpenningBalance.StockValue + '</td>\
            //                <td>' + StockDetailsOpenningBalance.Project + '</td>\
            //                <td>' + StockDetailsOpenningBalance.Category + '</td>\
            //               </tr>';
            //}

            //$("#stockDetailsOpenBalanceTbody").html(outputOpenningBalance);
            //+ StockDetailsOpenningBalance.DateEnd +

            if (stockDetails.length == 6)
            {
                for (i = 0; i < stockDetails.length; i++) {
                    if (stockDetails[i].RefNo != null) {
                        if (stockDetails[i].VoucherTypeName == "Sub total: ") {
                            output += '<tr style="background-color:#07b0a5;">\
                            <td>' + stockDetails[i].StoreName + '</td>\
                            <td>' + stockDetails[i].ProductName + '</td>\
                            <td>' + stockDetails[i].ProductCode + '</td>\
                            <td>' + stockDetails[i].RefNo + '</td>\
                            <td>' + stockDetails[i].VoucherTypeName + '</td>\
                            <td>' + stockDetails[i].Batch + '</td>\
                            <td>' + stockDetails[i].Date + '</td>\
                            <td>' + stockDetails[i].Rate + '</td>\
                            <td>' + stockDetails[i].InwardQty / 2 + '</td>\
                            <td>' + stockDetails[i].OutwardQty + '</td>\
                            <td>' + stockDetails[i].QtyBalance + '</td>\
                            <td>' + stockDetails[i].AverageCost + '</td>\
                            <td>' + stockDetails[i].StockValue + '</td>\
                            <td>' + projectName + '</td>\
                            <td>' + categoryName + '</td>\
                            <td>' + unitName + '</td>\
                           </tr>';
                        }
                        else if (stockDetails[i].VoucherTypeName == "Opening Balance: ") {
                            output += '<tr style="background-color:#bebebe;border-bottom:black solid 1px;">\
                            <td>' + stockDetails[i].StoreName + '</td>\
                            <td>' + stockDetails[i].ProductName + '</td>\
                            <td>' + stockDetails[i].ProductCode + '</td>\
                            <td>' + stockDetails[i].RefNo + '</td>\
                            <td>' + stockDetails[i].VoucherTypeName + '</td>\
                            <td>' + stockDetails[i].Batch + '</td>\
                            <td>' + stockDetails[i].Date + '</td>\
                            <td>' + stockDetails[i].Rate + '</td>\
                            <td>' + stockDetails[i].InwardQty / 2 + '</td>\
                            <td>' + stockDetails[i].OutwardQty + '</td>\
                            <td>' + stockDetails[i].QtyBalance + '</td>\
                            <td>' + stockDetails[i].AverageCost + '</td>\
                            <td>' + stockDetails[i].StockValue + '</td>\
                            <td>' + projectName + '</td>\
                            <td>' + categoryName + '</td>\
                            <td>' + unitName + '</td>\
                           </tr>';
                        }
                        else {
                            var projectObj = projects.find(p => p.ProjectId == stockDetails[i].ProjectId);
                            var projectName = projectObj == undefined ? "" : projectObj.ProjectName;
                            var categoryObj = categories.find(c => c.CategoryId == stockDetails[i].CategoryId);
                            var categoryName = categoryObj == undefined ? "" : categoryObj.CategoryName;
                            var unitName = stockDetails[i].UnitName == null ? "" : stockDetails[i].UnitName;
                            stockDetails[i].VoucherTypeName = stockDetails[i].VoucherTypeName == "Manufacturing" ? "Item Production" : stockDetails[i].VoucherTypeName;
                            stockDetails[i].VoucherTypeName = stockDetails[i].VoucherTypeName == "Stock Adjustment" && stockDetails[i].VoucherTypeName != "Item Collection" ?
                                "Adjustment" : stockDetails[i].VoucherTypeName;
                            output += '<tr>\
                            <td>' + stockDetails[i].StoreName + '</td>\
                            <td>' + stockDetails[i].ProductName + '</td>\
                            <td>' + stockDetails[i].ProductCode + '</td>\
                            <td>' + stockDetails[i].RefNo + '</td>\
                            <td>' + stockDetails[i].VoucherTypeName + '</td>\
                            <td>' + stockDetails[i].Batch + '</td>\
                            <td>' + stockDetails[i].Date + '</td>\
                            <td>' + stockDetails[i].Rate + '</td>\
                            <td>' + stockDetails[i].InwardQty + '</td>\
                            <td>' + stockDetails[i].OutwardQty + '</td>\
                            <td>' + stockDetails[i].QtyBalance + '</td>\
                            <td>' + stockDetails[i].AverageCost + '</td>\
                            <td>' + stockDetails[i].StockValue + '</td>\
                            <td>' + projectName + '</td>\
                            <td>' + categoryName + '</td>\
                            <td>' + unitName + '</td>\
                           </tr>';
                        }
                    }
                }
            }
            else
            {
                for (i = 0; i < stockDetails.length; i++) {
                    if (stockDetails[i].RefNo != null) {
                        if (stockDetails[i].VoucherTypeName == "Sub total: ") {
                            output += '<tr style="background-color:#07b0a5;">\
                            <td>' + stockDetails[i].StoreName + '</td>\
                            <td>' + stockDetails[i].ProductName + '</td>\
                            <td>' + stockDetails[i].ProductCode + '</td>\
                            <td>' + stockDetails[i].RefNo + '</td>\
                            <td>' + stockDetails[i].VoucherTypeName + '</td>\
                            <td>' + stockDetails[i].Batch + '</td>\
                            <td>' + stockDetails[i].Date + '</td>\
                            <td>' + stockDetails[i].Rate + '</td>\
                            <td>' + stockDetails[i].InwardQty + '</td>\
                            <td>' + stockDetails[i].OutwardQty + '</td>\
                            <td>' + stockDetails[i].QtyBalance + '</td>\
                            <td>' + stockDetails[i].AverageCost + '</td>\
                            <td>' + stockDetails[i].StockValue + '</td>\
                            <td>' + projectName + '</td>\
                            <td>' + categoryName + '</td>\
                            <td>' + unitName + '</td>\
                           </tr>';
                        }
                        else if (stockDetails[i].VoucherTypeName == "Opening Balance: ") {
                            output += '<tr style="background-color:#bebebe;border-bottom:black solid 1px;">\
                            <td>' + stockDetails[i].StoreName + '</td>\
                            <td>' + stockDetails[i].ProductName + '</td>\
                            <td>' + stockDetails[i].ProductCode + '</td>\
                            <td>' + stockDetails[i].RefNo + '</td>\
                            <td>' + stockDetails[i].VoucherTypeName + '</td>\
                            <td>' + stockDetails[i].Batch + '</td>\
                            <td>' + stockDetails[i].Date + '</td>\
                            <td>' + stockDetails[i].Rate + '</td>\
                            <td>' + stockDetails[i].InwardQty + '</td>\
                            <td>' + stockDetails[i].OutwardQty + '</td>\
                            <td>' + stockDetails[i].QtyBalance + '</td>\
                            <td>' + stockDetails[i].AverageCost + '</td>\
                            <td>' + stockDetails[i].StockValue + '</td>\
                            <td>' + projectName + '</td>\
                            <td>' + categoryName + '</td>\
                            <td>' + unitName + '</td>\
                           </tr>';
                        }
                        else {
                            var projectObj = projects.find(p => p.ProjectId == stockDetails[i].ProjectId);
                            var projectName = projectObj == undefined ? "" : projectObj.ProjectName;
                            var categoryObj = categories.find(c => c.CategoryId == stockDetails[i].CategoryId);
                            var categoryName = categoryObj == undefined ? "" : categoryObj.CategoryName;
                            var unitName = stockDetails[i].UnitName == null ? "" : stockDetails[i].UnitName;
                            stockDetails[i].VoucherTypeName = stockDetails[i].VoucherTypeName == "Manufacturing" ? "Item Production" : stockDetails[i].VoucherTypeName;
                            stockDetails[i].VoucherTypeName = stockDetails[i].VoucherTypeName == "Stock Adjustment" && stockDetails[i].VoucherTypeName != "Item Collection" ?
                                "Adjustment" : stockDetails[i].VoucherTypeName;
                            output += '<tr>\
                            <td>' + stockDetails[i].StoreName + '</td>\
                            <td>' + stockDetails[i].ProductName + '</td>\
                            <td>' + stockDetails[i].ProductCode + '</td>\
                            <td>' + stockDetails[i].RefNo + '</td>\
                            <td>' + stockDetails[i].VoucherTypeName + '</td>\
                            <td>' + stockDetails[i].Batch + '</td>\
                            <td>' + stockDetails[i].Date + '</td>\
                            <td>' + stockDetails[i].Rate + '</td>\
                            <td>' + stockDetails[i].InwardQty + '</td>\
                            <td>' + stockDetails[i].OutwardQty + '</td>\
                            <td>' + stockDetails[i].QtyBalance + '</td>\
                            <td>' + stockDetails[i].AverageCost + '</td>\
                            <td>' + stockDetails[i].StockValue + '</td>\
                            <td>' + projectName + '</td>\
                            <td>' + categoryName + '</td>\
                            <td>' + unitName + '</td>\
                           </tr>';
                        }
                    }

                }
            }

            $("#stockDetailsTbody").html(output);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });
}


getLookUps();


function CleanDbStockDetails(dbStockDetails) {
    /*
     * 	- if opening balance and opening stock exist, filter out opening stock 	else display as is
     * 	1. get distinct ProductNames
     * 	2. create a stockDetailList = []
     * 	2. foreach ProductName
     * 	    a. get available entries
     * 	    b. if VoucherTypeName === "Opening Balance: " and VoucherTypeName === "Opening Stock" exist
     * 	        i. select VoucherTypeName === "Opening Stock" entry
     * 	        ii. replace VoucherTypeName with "Opening Balance: "
     * 	        iii. add this entry to stockDetailList
     * 	    c. add all other entries as is
     */

    const OpeningBalance = "Opening Balance: ";
    const OpeningStock = "Opening Stock";
    let stockDetailProcessList = [];
    let stockDetailProcessNames = dbStockDetails.map(x => x.ProductName);
    let uniqueStockDetailProcessNames = stockDetailProcessNames.filter(Utilities.OnlyUnique);
    uniqueStockDetailProcessNames = uniqueStockDetailProcessNames.filter(x => x.length > 0);
    for (let uniqueStock of uniqueStockDetailProcessNames) {
        let productList = dbStockDetails.filter(x => x.ProductName === uniqueStock);
        let openingBalance = productList.find(x => x.VoucherTypeName === OpeningBalance);
        let openingStock = productList.find(x => x.VoucherTypeName === OpeningStock);

        if (openingBalance !== undefined && openingStock !== undefined) {
            openingStock.VoucherTypeName = OpeningBalance;
            stockDetailProcessList.push(openingStock);
            let productsVoidOfBalanceOrStock = productList.filter(x => x.VoucherTypeName !== OpeningBalance && x.VoucherTypeName !== OpeningStock);
            stockDetailProcessList.push(...productsVoidOfBalanceOrStock);
        } else {
            if (openingBalance === undefined && openingStock !== undefined) stockDetailProcessList.push(openingStock);
            if (openingBalance !== undefined && openingStock === undefined) stockDetailProcessList.push(OpeningBalance);

            let productsVoidOfBalanceOrStock = productList.filter(x => x.VoucherTypeName !== OpeningBalance && x.VoucherTypeName !== OpeningStock);
            stockDetailProcessList.push(...productsVoidOfBalanceOrStock);

            //stockDetailProcessList.push(...productList);
        }
    }
    return stockDetailProcessList;
}


function getLookUps()
{
    var projectLookupAjax = $.ajax({
        url: API_BASE_URL + "/Project/GetProject",
        type: "GET",
        contentType: "application/json",
    });

    var categoryLookupAjax = $.ajax({
        url: API_BASE_URL + "/Category/GetCategory",
        type: "GET",
        contentType: "application/json",
    });

    $.when(projectLookupAjax, categoryLookupAjax)
        .done(function (dataProject, dataCategory) {
            projects = dataProject[0];
            categories = dataCategory[0];
            console.log("projects", dataProject);
            console.log("categories", dataCategory);
            $("#project").html("<option value=\"0\">All</option>" + Utilities.PopulateDropDownFromArray(projects, 0, 1));
            $("#project").chosen({ width: "100%", margin: "1px" });
            $("#category").html("<option value=\"0\">All</option>" + Utilities.PopulateDropDownFromArray(categories, 0, 1));
            $("#category").chosen({ width: "100%", margin: "1px" });
        });
}