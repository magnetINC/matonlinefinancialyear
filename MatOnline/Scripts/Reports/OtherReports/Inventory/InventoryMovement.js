﻿var products = [];
var stores = [];
var searchParam = {};
var inventoryMovement = [];
var lookups = {};

$(function () {

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/InventoryReportLookup/InventoryMovement",
        type: "GET",
        contentType: "application/json",
        data: JSON.stringify(searchParam),
        success: function (data) {
            lookups = data;
            $("#formType").html('<option value="0">All</option>' + Utilities.PopulateDropDownFromArray(lookups.VoucherTypes, 0, 1));
            $("#store").html('<option value="0">All</option>' + Utilities.PopulateDropDownFromArray(lookups.Store, 0, 1));
            $("#batchNo").html('<option value="-1">All</option>' + Utilities.PopulateDropDownFromArray(lookups.Batch, 0, 1));
            $("#productGroup").html('<option value="0">All</option>' + Utilities.PopulateDropDownFromArray(lookups.ProductGroup, 1, 2));
            $("#product").html('<option value="0">All</option>' + Utilities.PopulateDropDownFromArray(lookups.Product, 0, 2));

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });

    $("#search").click(function () {
        loadInventoryMovement();
    });

});

function loadInventoryMovement()
{    
    searchParam = {
        FromDate: $("#fromDate").val(),
        ToDate: $("#toDate").val(),
        VoucherType: $("#formType").val(),
        VoucherNo: $("#formNo").val(),
        ProductId: $("#product").val(),
        ProductCode: $("#productCode").val(),
        StoreId: $("#store").val(),
        ProductGroup: $("#productGroup").val(),
        BatchNoId: $("#batchNo").val(),
    };

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/InventoryReport/InventoryMovement",
        type: "POST",
        contentType: "application/json",
        data: JSON.stringify(searchParam),
        success:function(data)
        {
            inventoryMovement = data;
            //console.log(inventoryMovement);
            //return;
            var output = "";
            for (i = 0; i < inventoryMovement.length; i++)
            {
                output += '<tr>\
                            <td>' +(i+1)+'</td>\
                            <td>' + inventoryMovement[i].voucherTypeName + '</td>\
                            <td>' + inventoryMovement[i].voucherNo + '</td>\
                            <td>' + inventoryMovement[i].productCode + '</td>\
                            <td>' + inventoryMovement[i].productName + '</td>\
                            <td>' + inventoryMovement[i].Store + '</td>\
                            <td>' + inventoryMovement[i].date + '</td>\
                            <td>' + inventoryMovement[i].batchNo + '</td>\
                            <td>' + inventoryMovement[i].unitName + '</td>\
                            <td>' + (inventoryMovement[i].inwardQty).toFixed(0) + '</td>\
                            <td>' + (inventoryMovement[i].outwardQty).toFixed(0) + '</td>\
                           </tr>';
            }
            //$("#thead").html('<tr>\
            //                    <th>S/N</th>\
            //                    <th>Form Type</th>\
            //                    <th>Form No.</th>\
            //                    <th>Item Code</th>\
            //                    <th>Item Name</th>\
            //                    <th>Store</th>\
            //                    <th>Date</th>\
            //                    <th>Batch No.</th>\
            //                    <th>Unit</th>\
            //                    <th>Inward</th>\
            //                    <th>Outward</th>\
            //                 </tr>');
            $("#inventoryMovementTbody").html(output);
            Utilities.Loader.Hide();
        },
        error:function(err)
        {
            Utilities.Loader.Hide();
            console.log(err)
        }
    });
}