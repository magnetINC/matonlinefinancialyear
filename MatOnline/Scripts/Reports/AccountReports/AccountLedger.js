﻿var table = "";
var accountGroups = [];
var accountLedgers = [];
var mainOutput = [];

$(function () {
    Loading();
})

function Loading()
{
    Utilities.Loader.Show();
   
    getGroups();
    $('#aGroup').change(function(){
        getLedgers();
    })
   
    Utilities.Loader.Hide();

}

function getLedgers()
{
    $.ajax({
        url: API_BASE_URL + "/AccountReports/GetLedgers?acctGrpId=" + $('#aGroup').val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            accountLedgers = data.Response;
            $("#aLedger").kendoDropDownList({
                filter: "contains",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: accountLedgers,
              optionLabel:"All"
            })

        }
    });
}

function getGroups() {
    $.ajax({
        url: API_BASE_URL + "/AccountReports/GetAccountGroups",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            accountGroups = data.Response;
            $("#aGroup").kendoDropDownList({
                filter: "contains",
                dataTextField: "accountGroupName",
                dataValueField: "accountGroupId",
                dataSource: accountGroups,
              optionLabel: "All"
            })

        }
    });
}

//for main output
function getAccountLedgerDetails()
{
  
    if ($("#aLedger").val() == "")
    {
        
        $("#aLedger").val(0);
    }
 
    if ($("#aGroup").val() == "") {
       
        $("#aGroup").val(-1);
    }
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AccountReports/GetAccountLedgerDetails?from=" + $('#fromDate').val() + "&to=" + $('#toDate').val() + "&ledger=" + $('#aLedger').val() + "&group=" + $('#aGroup').val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            mainOutput = data.Response;
            renderdata();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        }
    });
    Utilities.Loader.Hide();
    
}

function renderdata()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.ledgerName,
            row.Opening,
            row.Debit,
            row.Credit,
            row.Closing
        ])
    })
    table = $('#AccountLedgerReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

}