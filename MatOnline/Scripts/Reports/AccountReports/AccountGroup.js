﻿var table = "";
var mainOutput = [];

function getAccountGroup()
{
    $.ajax({
        url: API_BASE_URL + "/AccountReports/GetAccountGroupReportDetails",
        type:"GET",
        contentType:"application/json",
        success: function(data)
        {
            console.log(data);
            mainOutput = data.Response;
            //renderdata
        }
    })
}
//need to sort out what to do at api 
function renderdata()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,

        ])
    });

}

//for main output
function getAccountGroupDetails() {

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AccountReports/GetAccountGroupDetails?from=" + $('#fromDate').val() + "&to=" + $('#toDate').val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            mainOutput = data.Response;
            renderdata();
            $("#companyName").removeAttr("hidden");
            $("#companyAddress").removeAttr("hidden");
        }
    });
    Utilities.Loader.Hide();
    
}

function renderdata() {
    var display = [];
    if (table != "") {
        table.destroy();
    }
    $.each(mainOutput, function (index, row) {
        display.push([
            index + 1,
            row.accountGroupName,
            row.OpBalance,
            row.debit,
            row.credit,
            row.balance1
        ])
    })
    table = $('#AccountGroupReportList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

}

