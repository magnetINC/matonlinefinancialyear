﻿var ProductList = [];

$(function () {
    getAutoVoucherNo();
    currentDate();
    getAccountGroup();
    getCurrency();

    $("#Qty").change(function () {
        $("#Amount").val(getTotalAmount());
    });
    $("#btnLineItemModal").click(function () {
        clearLineItemForm();
        getProductAll()
        $("#LineItemModal").modal("toggle");
    });
    
    $("#DueDate").change(function () {
        var date1 = new Date($("#TransDate").val());
        var date2 = new Date($("#DueDate").val());
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        $("#DueDays").val(diffDays);
    });
});

function getAccountGroup() {
    //  showLoader();
    $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/GetSuppliers",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            var str = "";
            $.each(data, function (i, record) {
                str += "<option value = '" + record.ledgerId + "'>" + record.ledgerName + "</option>";
            });
            $("#Supplier").html(str);

        },
        error: function (request, error) {
            // hideLoader();
        }
    });
}
function getCurrency() {
    $.ajax({
        url: API_BASE_URL + "/Dropdown/CurrencyDropdown",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            var str = "";
            $.each(data, function (i, record) {
                str += "<option value = '" + record.exchangeRateId + "'>" + record.currencyName + "</option>";
            });
            $("#Currency").html(str);
        },
        error: function (request, error) {
        }
    });
}
function getProductAll() {
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            var str = "<option value = ''></option>";
            $.each(data, function (i, record) {
                str += "<option value = '" + record.ProductCode + "'>" + record.ProductCode + "</option>";
            });
            $("#ProductCode").html(str);
            $("#ProductCode").chosen({ width: "100%", margin: "1px" });

            var str = "<option value = ''></option>";
            $.each(data, function (i, record) {
                str += "<option value = '" + record.ProductName + "'>" + record.ProductName + "</option>";
            });
            $("#ProductName").html(str);
            $("#ProductName").chosen({ width: "100%" });

            var str = "<option value = ''></option>";
            $.each(data, function (i, record) {
                str += "<option value = '" + record.barcode + "'>" + record.barcode + "</option>";
            });
            $("#Barcode").html(str);
            $("#Barcode").chosen({ width: "100%" });
        },
        error: function (request, error) {
        }
    });
}
function searchProduct(searchBy, filter) {
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/SearchProduct?filter=" + filter + "&searchBy=" + searchBy,
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            ProductList = data;
            console.log(data)
            fillProductForm(data)
        },
        error: function (request, error) {
        }
    });
}
var lineItemList = [];
var lineItemSiNo = 1;
var columns = ["SI No", "Barcode", "Product Code", "Product Name", "Description", "Qty", "Unit", /*"Batch",*/ "RateAmount", "Amount", /*"Tax", "Tax Amount"*/];


//add a row
function AddLineItemToList() {
    lineItem = {
        SiNo: lineItemSiNo++,
        Barcode: $("#Barcode").val(),
        ProductCode: $("#ProductCode").val(),
        ProductName: $("#ProductName").val(),
        Description: $("#Description").val(),
        Qty: $("#Qty").val(),
        Unit: $("#Unit").val(), 
        Rate: $("#Rate").val(),
        Amount: $("#Amount").val()
    };
    id = "bill"
    $("#TotalAmount").val(calculateTotal(lineItemList).TotalAmount);
    $("#TotalTax").val(calculateTotal(lineItemList).TotalTax);
    $("#GrandTotal").val(calculateTotal(lineItemList).GrandTotal);
    //clear form
    clearLineItemForm()

}
function getTotalAmount()
{
    var totalAmount = 0;
    var rate = $("#Rate").val();
    rate = rate.replace(/,/g, "");
    totalAmount = $("#Qty").val() * $("#Rate").val();
    return totalAmount;
}

//remove a line item
function lineItemRemove(rowId) {
    if (rowId > 0) {
        var indexOfObjectToRemove = lineItemList.findIndex(p=>p.SiNo == rowId);
        lineItemList.splice(indexOfObjectToRemove, 1);
        lineItemTable(columns, lineItemList);
        $("#TotalAmount").val(calculateTotal(lineItemList).TotalAmount);
        //$("#TotalTax").val(calculateTotal(lineItemList).TotalTax);
        //$("#GrandTotal").val(calculateTotal(lineItemList).GrandTotal);
    }  
}
// fill product fill or line item base on product code or product name or barcode
function fillProductForm(obj) {
    $("#Barcode").val(obj.Product[0].productCode);
    $("#Barcode").trigger("chosen:updated");
    $("#ProductCode").val(obj.Product[0].productCode);
    $("#ProductCode").trigger("chosen:updated");
    $("#ProductName").val(obj.Product[0].productName);
    $("#ProductName").trigger("chosen:updated");
    $("#Description").val(obj.Product[0].narration);
    $("#Qty").val("");
    $("#Batch").val("");
    $("#Rate").val(Utilities.FormatCurrency(obj.Product[0].salesRate));
    setUnit(obj.Unit);
}
function clearLineItemForm() {
    $("#Barcode").val("");
    $("#Barcode").trigger("chosen:updated");
    $("#ProductCode").val("");
    $("#ProductCode").trigger("chosen:updated");
    $("#ProductName").val("");
    $("#ProductName").trigger("chosen:updated");
    $("#Description").val("");
    $("#Qty").val("");
    $("#Unit").val("");
    //$("#Batch").val("");
    $("#Rate").val("");
    $("#Amount").val("");
    //$("#Tax").val("");
    //$("#TaxAmount").val("");
}
function clearForm() {
    lineItemList = [];
    lineItemTable(columns, lineItemList);
    $("#TotalAmount").val("");
    $("#Narration").val("");
   // $("#TotalTax").val("");
   // $("#GrandTotal").val("");
}
function setUnit(data) {
    var str = "";
    $.each(data, function (i, record) {
        str += "<option value = '" + record.unitId + "'>" + record.unitName + "</option>";
    });
    $("#Unit").html(str);
}

//============= Save Sales Quote==================
$("#btnSave").click(function () {

    SalesQuotationObj =
    {
        SiNo:"",
        OrderNo: $("#QuotationNo").val(),
        Supplier: $("#Supplier").val(),
        //SalesMan: $("#SalesMan").val(),
        TransDate: $("#TransDate").val(),
        DueDate: $("#DueDate").val(),
        DueDays: $("#DueDays").val(),
        //PricingLevel: $("#PricingLevel").val(),
        Currency: $("#Currency").val(),
        Narration: $("#Narration").val(),
        TotalAmount: $("#TotalAmount").val(),
        //TotalTax: $("#TotalTax").val(),
        //GrandTotal: $("#GrandTotal").val(),
        LineItems: lineItemList
        
    };
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/SavePurchaseOrder",
        type: 'POST',
        data: JSON.stringify(SalesQuotationObj),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            Utilities.SuccessNotification("Purchase Order is saved!");
            clearForm();
            getAutoVoucherNo();
            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
});



function getAutoVoucherNo() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StockJournal/GetAutoVoucherNo",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            $("#QuotationNo").val(data);
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}