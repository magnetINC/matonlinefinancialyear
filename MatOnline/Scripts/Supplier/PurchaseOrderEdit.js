﻿var Productist = [];
var SupplierList = [];
var CurrencyList = [];
var TaxList = [];
var UnitList = [];
var InvoiceNo = "";
var LineItems = [];
var searchResult = {};
var productToAdd = {};
var tax = {};
var purchaseRate = 0;
var sl = 1;
var units = [];
var masterData = {};
var orderDetailsData = [];

$(function () {
    GetLookUpData();

    $("#btnLineItemModal").click(function () {
        $("#LineItemModal").modal("toggle");
    });
    $("#Barcode").change(function () {
        searchProduct("ProductCode", $("#Barcode").val());
    });
    $("#ProductCode").change(function () {
        searchProduct("ProductCode", $("#ProductCode").val());
    });
    $("#ProductName").change(function () {
        searchProduct("ProductName", $("#ProductName").val());
    });
    $("#Qty").keyup(function () {
        calculateAmountOfItemToAdd();
    });
    //$("#Rate").change(function () {
    //    calculateAmountOfItemToAdd();
    //});
    $("#Rate").keyup(function () {
        calculateAmountOfItemToAdd();
    });
    $("#Tax").change(function () {
        var isFound = false;
        for (i = 0; i < TaxList.length; i++) {
            if (TaxList[i].taxId == $("#Tax").val()) {
                tax = TaxList[i];
                isFound = true;
                break;
            }
        }
        if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
        {
            tax = { taxId: 0, taxName: "NA", rate: 0 };
        }
        purchaseRate = $("#Rate").val();
        calculateAmountOfItemToAdd();
    });
});
function openModal() {
    $("#LineItemModal").modal("show");
    $('#LineItemModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
}
//to get all look up data
function GetLookUpData() {
    Utilities.Loader.Show();
    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/LookUpData",
        type: "Get",
        contentType: "application/json"
    });
    var autoInvoiceNoAjax = $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/GetAutoVoucherNo",
        type: "Get",
        contentType: "application/json"
    });
    var getDetailsAjax = $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/GetPurchaseOrderDetails?purchaseOrderMasterId=" + purchaseOrderId,
        type: "Get",
        contentType: "application/json"
    });
    $.when(lookUpAjax, autoInvoiceNoAjax, getDetailsAjax)
     .done(function (dataLookUp, dataInvoiceNo, detailsData) {
         console.log(detailsData);
         Productist = dataLookUp[0].Products;
         SupplierList = dataLookUp[0].Suppliers;
         CurrencyList = dataLookUp[0].Currencies;
         TaxList = dataLookUp[0].Taxes;
         UnitList = dataLookUp[0].Units
         InvoiceNo = dataInvoiceNo[2].responseJSON;
         masterData = detailsData[0].Master;
         orderDetailsData = detailsData[0].Details;

         $.each(orderDetailsData, function (count, row) {
             LineItems.push({
                 slno: count + 1,
                 orderDetailsId: row.purchaseOrderDetailsId,
                 Barcode: row.productCode,
                 ProductCode: row.productCode,
                 ProductId : row.productId,
                 ProductName: row.productName,
                 Description: row.itemDescription,
                 Qty: row.qty,
                 Unit: row.unitId,
                 Rate: row.rate,
                 Amount: row.amount,
                 TaxId: $("#Tax").val(),
                 TaxAmount: 0.00
             });
         });
         //to render data to controls after successful data fetch
         RenderDatatoControls();
         RenderLineItem();
     });
    Utilities.Loader.Hide();
}

function onChange() {
    //alert(dropdownlistPN.value());
};
//to render data to controls
function RenderDatatoControls() {
    // $("#Supplier").html(Utilities.PopulateDropDownFromArray(SupplierList, 1, 0));
    $("#Currency").html(Utilities.PopulateDropDownFromArray(CurrencyList, 1, 0));
    $("#Tax").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(TaxList, 0, 1));
    $("#Supplier").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: SupplierList,
        optionLabel: "Please Select"
    });
    $("#Barcode").kendoDropDownList({
        optionLabel: "Please Select...",
        dataTextField: "productCode",
        dataValueField: "productCode",
        dataSource: Productist
    });
    $("#ProductCode").kendoDropDownList({
        filter: "contains",
        optionLabel: "Please Select...",
        dataTextField: "productCode",
        dataValueField: "productCode",
        dataSource: Productist
    });
    $("#ProductName").kendoDropDownList({
        filter: "contains",
        optionLabel: "Please Select...",
        dataTextField: "productName",
        dataValueField: "productName",
        dataSource: Productist,
        //change : onChange
    });
    var dropdownlistPN = $("#ProductName").data("kendoDropDownList");

    //$("#Barcode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(Productist, 0, 0));
    //$("#ProductCode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(Productist, 1, 1));
    //$("#ProductName").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(Productist, 2, 2));
    //$("#Unit").html(Utilities.PopulateDropDownFromArray(UnitList, 1, 2));

    console.log(masterData);
    $("#orderNo").val(masterData.InvoiceNo);
    $("#DueDate").val(Utilities.FormatJsonDate(masterData.DueDate));
    $("#TransDate").val(Utilities.FormatJsonDate(masterData.Date));
    var dropdownlistSupp = $("#Supplier").data("kendoDropDownList");
    dropdownlistSupp.value(masterData.LedgerId);

    //currentDate();
    var date1 = new Date($("#TransDate").val());
    var date2 = new Date($("#DueDate").val());
    var timeDiff = Math.abs(date2.getTime() - date1.getTime());
    var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
    $("#DueDays").val(diffDays);
}
//search for product
function searchProduct(searchBy, filter) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/SearchProduct?filter=" + filter + "&searchBy=" + searchBy,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            searchResult = {};
            searchResult = data.Product;
            console.log(searchResult)
            purchaseRate = searchResult.PurchaseRate
            var uni = data.Unit[0];
            $("#Rate").val(searchResult.PurchaseRate);
            $("#Description").val(searchResult.Narration)
            $("#Unit").val(uni.unitName);

            var dropdownlistBarcode = $("#Barcode").data("kendoDropDownList");
            var dropdownlistProductCode = $("#ProductCode").data("kendoDropDownList");
            var dropdownlistProductName = $("#ProductName").data("kendoDropDownList");

            //alert(dropdownlistBarcode.value());

            dropdownlistBarcode.value(searchResult.ProductCode);
            dropdownlistProductCode.value(searchResult.ProductCode);
            dropdownlistProductName.value(searchResult.ProductName);

            //$("#Barcode").val(searchResult.productCode);
            //$("#Barcode").trigger("chosen:updated");
            //$("#ProductCode").val(searchResult.productCode);
            //$("#ProductCode").trigger("chosen:updated");
            //$("#ProductName").val(searchResult.productName);
            //$("#ProductName").trigger("chosen:updated");

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}
//to add object to line item
function AddtoLineItem() {
    var a = $("#Amount").val();
    var amt = a.replace(/,/g, "");
    var amount = Number.parseFloat(amt);
    var t = $("#TaxAmount").val();
    var tamt = t.replace(/,/g, "");
    var tamount = Number.parseFloat(tamt);

    LineItems.push({
        slno: sl,
        orderDetailsId: 0,
        Barcode: $("#Barcode").val(),
        ProductCode: $("#ProductCode").val(),
        ProductName: $("#ProductName").val(),
        ProductId: searchResult.ProductId,
        Description: $("#Description").val(),
        Qty: $("#Qty").val(),
        Unit: $("#Unit").val(),
        Rate: $("#Rate").val(),
        Amount: amount,
        TaxId: $("#Tax").val(),
        TaxAmount: tamount
    });
    sl = sl + 1;
    RenderLineItem();
    clearLineItemForm();
}
//to render line item
function RenderLineItem() {
    console.log(LineItems);
    //Utilities.ShowStockCardDialog($("#stockCardProducts").val());
    var output = "";
    $.each(LineItems, function (count, row) {
        var prod = row;
        //if(prod.Barcode == undefined)
        //{
        //    prod.Barcode = "";
        //}
        console.log(prod);
        if (prod.taxAmount == null) {
            prod.taxAmount = 0;
        }
        output += '<tr>\
                    <td style="white-space: nowrap;"><button onclick="RemoveFromLineItem(' + prod.ProductId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>\
                    <td><button onclick="Utilities.ShowStockCardDialog(' + prod.productId + ')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></button></td>\
                    <td>'+ (count + 1) + '</td>\
                    <td>' + ((prod.Barcode == undefined) ? "-" : prod.Barcode) + '</td>\
                    <td>' + prod.ProductCode + '</td>\
                    <td>' + prod.ProductName + '</td>\
                    <td>' + prod.Description + '</td>\
                    <td>' + prod.Unit + '</td>\
                    <td>' + prod.Qty + '</td>\
                    <td>&#8358;' + Utilities.FormatCurrency(prod.Rate) + '</td>\
                    <td>&#8358;' + Utilities.FormatCurrency(prod.Amount) + '</td>\
                  </tr>';
    });

    $("#OrderLineItemTbody").html(output);
    $("#billAmount").val(Utilities.FormatCurrency(getTotalAmount()));
    $("#taxAmount").val(Utilities.FormatCurrency(getTotalTax()));
    $("#grandTotal").val(Utilities.FormatCurrency(getTotalAmount() + getTotalTax()));
}
//remove item obj from line item
function RemoveFromLineItem(productId) {
    console.log("i'm getting the cunt here");
    if (confirm("Remove this item?")) {
        var indexOfObjectToRemove = LineItems.findIndex(p=>p.ProductId == productId);
        LineItems.splice(indexOfObjectToRemove, 1);
        RenderLineItem();
    }
}

//save New Purchase order
$("#btnSave").click(function () {

    SalesQuotationObj =
    {
        SiNo: "",
        orderId: masterData.PurchaseOrderMasterId,
        OrderNo: $("#orderNo").val(),
        Supplier: $("#Supplier").val(),
        TransDate: $("#TransDate").val(),
        DueDate: $("#DueDate").val(),
        DueDays: $("#DueDays").val(),
        Currency: $("#Currency").val(),
        Narration: $("#Narration").val(),
        TotalAmount: $("#billAmount").val(),
        LineItems: LineItems

    };
    console.log(SalesQuotationObj);
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/EditPurchaseOrder",
        type: 'POST',
        data: JSON.stringify(SalesQuotationObj),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if(data == true)
            {
                hideLoader();
                Utilities.SuccessNotification("Purchase Order is Updated!");
                window.location = "/Supplier/PurchaseOrder/Register";
            }
            else
            {
                hideLoader();
                Utilities.SuccessNotification("Purchase Order was not Updated!");
            }
        },
        error: function (request, error) {
            hideLoader();
        }
    });
});
function calculateAmountOfItemToAdd() {
    if (tax.rate == undefined) {
        tax.rate = 0;
    }
    var r = $("#Rate").val();
    var rate = r.replace(/,/g, "");

    var qty = $("#Qty").val();
    var amount = rate * qty;
    var taxAmount = amount * (tax.rate / 100.0);

    $("#Amount").val(Utilities.FormatCurrency(amount));
    $("#TaxAmount").val(Utilities.FormatCurrency(taxAmount));
}
function getTotalAmount() {
    var amt = 0.0;
    for (i = 0; i < LineItems.length; i++) {
        amt = amt + parseFloat(LineItems[i].Amount);
    }
    return amt;
}
function getTotalTax() {
    var amt = 0.0;
    for (i = 0; i < LineItems.length; i++) {
        amt = amt + parseFloat(LineItems[i].TaxAmount);
    }
    return amt;
}
function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}
function clearLineItemForm() {
    $("#Barcode").val("");
    $("#Barcode").trigger("chosen:updated");
    $("#ProductCode").val("");
    $("#ProductCode").trigger("chosen:updated");
    $("#ProductName").val("");
    $("#ProductName").trigger("chosen:updated");
    $("#Description").val("");
    $("#Qty").val("");
    $("#Unit").val("");
    $("#Rate").val("");
    $("#Amount").val("");
    $("#Tax").val("");
    $("#TaxAmount").val("");
}
function clearForm() {
    $("#DueDate").val("");
    $("#DueDays").val("");
    $("#Currency").val("");
    $("#Narration").val("");
    $("#billAmount").val("");
    $("#taxAmount").val("");
    $("#grandTotal").val("");
    LineItems = [];
}