﻿var LineItems = [];
var suppliers = [];
var currencies = [];
var ApplyOn = [];
var units = [];
var taxes = [];
var batches = [];
var racks = [];
var stores = [];

var ga = 0;
var na = 0;
var ta = 0;
var da = 0;

var totalLineItemAmount = 0.0;
var totalNetAmount = 0.0;
var totalBillDiscount = 0.0;
var totalTaxAmount = 0.0;
var totalLineGross = 0.0;
var lineItemNet = 0.0;
var lineItemGross = 0.0;
var lineItemAmount = 0.0;
var taxRate = 0.0;
var taxAmount = 0.0;
var taxName = "";
var PurchaseReturntoSave = {};

$(function () {
    loadLookups();
    $("#suppliers").change(function () {
        LoadInvoiceNumbers();
    });
    $("#applyOn").change(function () {
        LoadInvoiceNumbers();
    });

    $("#orderNo").change(function (event) {
        event.preventDefault();
        var purchaseMasterId = $("#orderNo").val();
        if (purchaseMasterId > 0) {
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/PurchaseReturn/GetPurchaseDetails?purchaseMasterId=" + purchaseMasterId,
                type: "GET",
                contentType: "application/json",
                success: function (data) {
                    LineItems = data;
                    console.log(data);
                    Utilities.Loader.Hide();
                    var count = 0;
                    for (var i of LineItems) {
                        count += 1;
                        i.sn = count;
                        i.qtyToReturn = 0;
                    }
                    renderPurchaseDetailsLineItems();

                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }
    });

    $("#save").click(function () {
        save();
    });
});
function renderPurchaseDetailsLineItems() {
    var output = "";
    var tla = 0.0;
    var tln = 0.0;
    var tld = 0.0;
    var tlt = 0.0;
    var tlg = 0.0;
  
    $.each(LineItems, function (count, row) {
        lineItemGross = row.rate * row.qty;
        lineItemNet = lineItemGross - row.discount;
        if(row.taxId == 0)
        {
            taxRate = 0;
            taxName = "NA";
        }
        else
        {
            taxRate = findTax(row.taxId).rate;
            taxName = findTax(row.taxId).taxName;
        }
        taxAmount = ((lineItemNet * taxRate) / 100)
        lineItemAmount = lineItemNet + taxAmount;
        output +=
            '<tr>\
                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                <td style="white-space: nowrap;">' + row.productName + '</td>\
                <td style="white-space: nowrap;">' + row.itemDescription + '</td>\
                <td style="white-space: nowrap;">' + row.qty + '</td>\
                <td style="white-space: nowrap;"><input id="' + row.sn + '" type="number" class="form-control" onblur="onLeaveQty(' + row.sn + ')" value="' + row.qtyToReturn + '"/></td>\
                <td style="white-space: nowrap;">' + findUnit(row.unitId).unitName + '</td>\
                <td style="white-space: nowrap;">' + findStore(+row.godownId).godownName + '</td>\
                <td style="white-space: nowrap;">' + findRack(row.rackId).rackName + '</td>\
                <td style="white-space: nowrap;">' + findBatch(row.batchId).batchNo + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.rate) + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(lineItemGross) + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(lineItemNet) + '</td>\
                <td style="white-space: nowrap;">' + taxName + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(taxAmount) + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(lineItemAmount) + '</td>\
            </tr>\
            ';
        tla = (tla + parseFloat(lineItemAmount));
        tld = (tld + parseFloat(row.discount));
        tlt = (tlt + parseFloat(taxAmount));
        tln = (tln + parseFloat(lineItemNet));
        tlg = (tlg + parseFloat(lineItemGross));

        totalLineItemAmount = tla;
        totalBillDiscount = tld;
        totalNetAmount = tln;
        totalTaxAmount = tlt;
        totalLineGross = tlg;
    });
    $("#totalAmount").val(Utilities.FormatCurrency(totalLineGross));
    $("#totalDiscount").val(Utilities.FormatCurrency(totalBillDiscount));
    $("#grandTotal").val(Utilities.FormatCurrency(totalLineItemAmount));
    $("#purchaseReturnLineItemTbody").html(output);
} 

function renderPurchaseDetailsLineItemsAfterEdit() {
    var output = "";
    var tla = 0.0;
    var tln = 0.0;
    var tld = 0.0;
    var tlt = 0.0;
    var tlg = 0.0;

    $.each(LineItems, function (count, row) {
        lineItemGross = row.grossAmount;
        lineItemNet = lineItemGross - row.discount;
        if (row.taxId == 0) {
            taxRate = 0;
            taxName = "NA";
        }
        else {
            taxRate = findTax(row.taxId).rate;
            taxName = findTax(row.taxId).taxName;
        }
        taxAmount = ((lineItemNet * taxRate) / 100);
        lineItemAmount = lineItemNet + taxAmount;
        output +=
            '<tr>\
                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                <td style="white-space: nowrap;">' + row.productName + '</td>\
                <td style="white-space: nowrap;">' + row.itemDescription + '</td>\
                <td style="white-space: nowrap;">' + row.qty + '</td>\
                <td style="white-space: nowrap;"><input id="' + row.sn + '" type="number" class="form-control" onblur="onLeaveQty(' + row.sn + ')" value="' + row.qtyToReturn + '"/></td>\
                <td style="white-space: nowrap;">' + findUnit(row.unitId).unitName + '</td>\
                <td style="white-space: nowrap;">' + findStore(+row.godownId).godownName + '</td>\
                <td style="white-space: nowrap;">' + findRack(row.rackId).rackName + '</td>\
                <td style="white-space: nowrap;">' + findBatch(row.batchId).batchNo + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.rate) + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(lineItemGross) + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(lineItemNet) + '</td>\
                <td style="white-space: nowrap;">' + taxName + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(taxAmount) + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(lineItemAmount) + '</td>\
            </tr>\
            ';
        tla = (tla + parseFloat(lineItemAmount));
        tld = (tld + parseFloat(row.discount));
        tlt = (tlt + parseFloat(taxAmount));
        tln = (tln + parseFloat(lineItemNet));
        tlg = (tlg + parseFloat(lineItemGross));

        totalLineItemAmount = tla;
        totalBillDiscount = tld;
        totalNetAmount = tln;
        totalTaxAmount = tlt;
        totalLineGross = tlg;
    });
    $("#totalAmount").val(Utilities.FormatCurrency(totalLineGross));
    $("#totalDiscount").val(Utilities.FormatCurrency(totalBillDiscount));
    $("#grandTotal").val(Utilities.FormatCurrency(totalLineItemAmount));
    $("#purchaseReturnLineItemTbody").html(output);
}
function onLeaveQty(productId) {
    //console.log("b4 update", deliveryNoteLineItems);
    $.each(LineItems, function (count, row) {
        if (row.sn == productId) { 
            if ($("#" + row.sn).val() > row.qty) {
                $("#" + row.sn).val(0);
                Utilities.Alert("You cant return more than what was ordered");
                return;
            }
            row.qtyToReturn = $("#" + row.sn).val();
            lineItemGross = row.rate * row.qtyToReturn;
            row.grossAmount = lineItemGross;
            lineItemNet = lineItemGross - row.discount;
            row.netAmount = lineItemNet;
           
            row.netValue = lineItemGross - row.discount;
            if (row.taxId == 0) {
                taxRate = 0;
                taxName = "NA";
            }
            else {
                taxRate = findTax(row.taxId).rate;
                taxName = findTax(row.taxId).taxName;
            }
            taxAmount = ((lineItemNet * taxRate) / 100);
            row.taxAmount = taxAmount;
            lineItemAmount = lineItemNet + taxAmount;
            row.amount = lineItemAmount;
            renderPurchaseDetailsLineItemsAfterEdit();
            return;
        }
    });
    //console.log("after update", deliveryNoteLineItems);
}

function LoadInvoiceNumbers()
{
    var supplierId = $("#suppliers").val();
    var applyOn = $("#applyOn").val();
    if (supplierId > 0) {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PurchaseReturn/InvoiceNoComboFill?decLedger=" + supplierId + "&decvoucherTypeId=" + applyOn,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                var orderNoHtml = "<option></option>";
                $.each(data, function (count, record) {
                    orderNoHtml += '<option value="' + record.purchaseMasterId + '">' + record.invoiceNo + '</option>';
                });
                $("#orderNo").html(orderNoHtml);
                Utilities.Loader.Hide();
            },
            error: function () {
                Utilities.Loader.Hide();
            }
        });
    }
}
function loadLookups() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReturn/InvoiceLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            suppliers = data.Customers;
            currencies = data.Currencies;
            ApplyOn = data.ApplyOn;
            taxes = data.Tax;
            batches = data.Batches;
            racks = data.Racks;
            stores = data.Stores;
            units = data.Units;
            populateLookupsControls();
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
    $.ajax({
        url: API_BASE_URL + "/PurchaseReturn/GetAutoFormNo",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            $("#voucherNo").val(data);
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}
function populateLookupsControls() {
    var currencyHtml = "";
    $.each(currencies, function (count, record) {
        currencyHtml += '<option value="' + record.exchangeRateId + '">' + record.currencyName + '</option>';
    });
    $("#currency").html(currencyHtml);

    var suppliersHtml = "<option></option>";
    $.each(suppliers, function (count, record) {
        suppliersHtml += '<option value="' + record.ledgerId + '">' + record.ledgerName + '</option>';
    });
    $("#suppliers").html(suppliersHtml);

    var applyOnHtml = "";
    $.each(ApplyOn, function (count, record) {
        applyOnHtml += '<option value="' + record.voucherTypeId + '">' + record.voucherTypeName + '</option>';
    });
    $("#applyOn").html(applyOnHtml);
}
function save()
{
    var input = [];
    var sl = 0;
    var gross = 0.0, total = 0.0, totalTax = 0.0, totalNet = 0.0, totalDiscount=0.0;
    for (i = 0; i < LineItems.length; i++) {

        var gross1 = LineItems[i].rate * LineItems[i].qtyToReturn; //Number(lineItemGross);
        var total1 = LineItems[i].rate * LineItems[i].qtyToReturn; // Number(lineItemAmount);
        gross += gross1;
        total += total1;
        totalTax += Number(taxAmount);
        totalNet += gross1; // Number(lineItemNet);
        totalDiscount += Number(LineItems[i].discount);
        input.push({
            ProductId: LineItems[i].productId,
            ProductBarcode: LineItems[i].barcode,
            ProductCode: LineItems[i].productCode,
            ProductName: LineItems[i].productName,
            CategoryId: LineItems[i].CategoryId,
            ProjectId: LineItems[i].ProjectId,
            Discount: LineItems[i].discount,
            GodownId: LineItems[i].godownId,
            GrossAmount: gross1,
            NetAmount: gross1,
            MaterialReceiptId: LineItems[i].receiptDetailsId,
            PurchaseDetailsId: LineItems[i].purchaseDetailsId,
            Description: "",
            Quantity: LineItems[i].qtyToReturn,
            UnitId: LineItems[i].unitId,
            UnitConversionId: LineItems[i].unitConversionId,
            RackId: LineItems[i].rackId,
            VoucherNo: LineItems[i].voucherNo,
            VoucherTypeId: LineItems[i].voucherTypeId,
            InvoiceNo: LineItems[i].invoiceNo,
            BatchId: LineItems[i].batchId,
            Rate: LineItems[i].rate,
            PurchaseOrderDetailsId: LineItems[i].orderDetailsId,
            TaxId: LineItems[i].taxId,
            taxAmount: taxAmount,
            Amount: total1
        });
    }
    PurchaseReturntoSave.ReturnNo = $("#voucherNo").val();
    PurchaseReturntoSave.Date = $("#transactionDate").val();
    PurchaseReturntoSave.SupplierId = $("#suppliers").val();
    PurchaseReturntoSave.Narration = $("#narration").val();
    PurchaseReturntoSave.DoneBy = $("#UserId").html(),
    PurchaseReturntoSave.TotalAmount = total;
    PurchaseReturntoSave.GrandTotal = gross;
    PurchaseReturntoSave.TransportationCompany = $("#transportationCompany").val();
    PurchaseReturntoSave.LrNo = $("#lrNo").val();
    PurchaseReturntoSave.CurrencyId = $("#currency").val();
    PurchaseReturntoSave.PurchaseMasterId = $("#orderNo").val();
    PurchaseReturntoSave.DiscountAmount = totalDiscount;
    PurchaseReturntoSave.TaxAmount = totalTax;
    PurchaseReturntoSave.NetAmount = totalNet;
    PurchaseReturntoSave.LineItems = input;
    debugger;
    console.log(PurchaseReturntoSave); //return;
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseReturn/Save",
        type: "Post",
        contentType: "application/json",
        data: JSON.stringify(PurchaseReturntoSave),
        success: function (data) {
            Utilities.Loader.Hide();
            Utilities.SuccessNotification(data);
            window.location = "/Supplier/PurchaseReturn/Index";
        },
        error: function (e) {
            Utilities.Loader.Hide();
        }
    });
}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}
function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}
function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}
function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}
function findTax(taxId) {
    var output = {};
    for (i = 0; i < taxes.length; i++) {
        if (taxes[i].taxId == taxId) {
            output = taxes[i];
            break;
        }
    }
    return output;
}