﻿var ListForSearch = [];
var suppliers = [];
var users = [];
var lineItems = [];
var products = [];
var units = [];

var master1 = {};
var master2 = {};
var details = [];
var toSave = {};

$(function () {
    $("#formNo").hide();
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
});
function getMaterialReceiptList(fromDate, toDate, approved) {

    var tr = toDate + " 23:59:59 PM";
    console.log(tr);
    param = {
        FromDate: fromDate,
        ToDate: tr
    };
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/MaterialReceipt/GetPendingMaterialReceipt?fromDate=" + fromDate + "&toDate=" + tr + "&approved=" + approved,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            suppliers = data.Suppliers;
            ListForSearch = data.Data.Table;
            User = data.User;

            var output = "";
            var objToShow = [];
            $.each(ListForSearch, function (count, row) {
                objToShow.push([
                    count + 1,
                    ((row.invoiceNo == "") ? "NA" : (row.invoiceNo)),
                    Utilities.FormatJsonDate(row.date),
                    findSuppliers(row.ledgerId).ledgerName,
                    '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
                    findUser(row.userId).userName,
                    //row.approved,
                    '<button type="button" class="btn btn-primary btn-sm" onclick="getMaterialReceiptTransaction(' + row.materialReceiptMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                ]);
            });
            table = $('#materialReceiptListTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            //$("#salesQuotationListTbody").html(output);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getMaterialReceiptTransaction(id) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/MaterialReceipt/GetPendingMaterialReceiptDetails?materialReceiptMasterId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            master1 = data.Master1.Table;
            //master2 = data.Master2;
            details = data.FullDetails;
            products = data.products;
            units = data.units;
            var statusOutput = "";
            $.each(master1, function (count, row) {
                if (row.approved == "Pending") {
                    $("#ApproveOrder").show();
                    $("#cancelOrder").show();
                    statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending Approval</span>';
                }
                else if (row.approved == "Approved") {
                    $("#ApproveOrder").hide();
                    $("#cancelOrder").hide();
                    statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Approved</span>';
                }
                else if (row.approved == "Cancelled") {
                    $("#ApproveOrder").hide();
                    $("#cancelOrder").hide();
                    statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
                }


                $("#orderNoDiv").html(row.invoiceNo);
                $("#orderDateDiv").html(Utilities.FormatJsonDate(row.date));
                $("#orderNoDiv").html(row.invoiceNo);
                $("#raisedByDiv").html(findUser(row.userId).userName);
                $("#raisedForDiv").html(findSuppliers(row.ledgerId).ledgerName);
                $("#statusDiv").html(statusOutput);
                $("#orderAmountTxt").val(Utilities.FormatCurrency(row.totalAmount));
                //$("#taxAmountTxt").val(Utilities.FormatCurrency(tAmount));
                $("#grandTotalTxt").val(Utilities.FormatCurrency(row.totalAmount));
            });

            var output = "";
            $.each(details, function (count, row) {
                output += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td><center>' + ((findProduct(row.productId).barcode == undefined) ? "-" : (findProduct(row.productId).barcode)) + '</center></td>\
                            <td>' + findProduct(row.productId).productCode + '</td>\
                            <td>' + findProduct(row.productId).productName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>' + findUnit(row.unitId).unitName + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                        </tr>';
            });


            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function saveMaterialReceipt() {
    Utilities.Loader.Show();
    toSave = [];
    $.each(details, function (count, row) {
        lineItems.push({
            ProductId: row.productId,
            OrderDetailsId: row.orderDetailsId,
            Quantity: row.qty,
            UnitId: row.unitId,
            UnitConversionId: row.unitConversionId,
            Rate: row.rate,
            SL: row.slNo,
            Description: row.itemDescription
        });
    });

    $.each(master1, function (count, row) {
        toSave = {
            //DeliveryNoteNo: $("#invoiceNo").val(),
            Date: row.date,
            SupplierId: row.ledgerId,
            Narration: row.narration,
            TotalAmount: row.totalAmount,
            OrderMasterId: row.orderMasterId,
            OrderNo: row.invoiceNo,
            LineItems: lineItems,
        };
    });
    console.log(toSave);
    $.ajax({
        url: API_BASE_URL + "/MaterialReceipt/SaveFunction",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data) {
            if (data == true) {
                Utilities.SuccessNotification("Material Receipt Approved Successfully");
                $("#detailsModal").modal("hide");
                table.destroy();
                if ($("#backDays").val() == "custom") {
                    getMaterialReceiptList($("#fromDate").val(), $("#toDate").val(), $("#orderStatus").val());
                }
                else {
                    getMaterialReceiptList("2017-01-01", $("#toDate").val(), $("#orderStatus").val());
                }
            }
            else {
                Utilities.ErrorNotification("Material Receipt Approval Failed");
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function findSuppliers(ledgerId) {
    var output = {};
    for (i = 0; i < suppliers.length; i++) {
        if (suppliers[i].ledgerId == ledgerId) {
            output = suppliers[i];
            break;
        }
    }
    return output;
}

function findUser(userId) {
    var output = {};
    for (i = 0; i < User.length; i++) {
        if (User[i].userId == userId) {
            output = User[i];
            break;
        }
    }
    return output;
}

function findProduct(id) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].productId == id) {
            output = products[i];
            break;
        }
    }
    return output;
}
function findUnit(id) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == id) {
            output = units[i];
            break;
        }
    }
    return output;
}