﻿var orderListForSearch = []
var master = {};
var details = [];
var toSave = {};
//var supp = [];
//var users = [];

$(function () {
    loadLookUps();

    $("#formNo").hide();
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });
});

//to get purchase order istings
function getPurchaseOrderListings(fromDate, toDate, status)
{
    //param = {
    //    InvoiceNo: "",
    //    LedgerId: -1,
    //    FromDate: fromDate,
    //    ToDate: toDate + " 23:59:59",
    //    Condition: condition != "" ? condition : "All"
    //};
    Utilities.Loader.Show();
    //loadLookUps();
    $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/PurchaseOrderListing?fromDate=" + fromDate + "&toDate=" + toDate + " 23:59:59&approved=" + status,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            orderListForSearch = data.Table;
            var output = "";
            var objToShow = [];
            $.each(data.Table, function (count, row) {   
                objToShow.push([
                    count + 1,
                    row.invoiceNo,
                    Utilities.FormatJsonDate(row.date),
                    Utilities.FormatJsonDate(row.dueDate),
                    findSuppliers(row.ledgerId).ledgerName,
                    '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
                    findUser(row.userId).userName,
                    //row.approved,
                    '<button type="button" class="btn btn-primary btn-sm" onclick="getOrderTransaction(' + row.purchaseOrderMasterId + ')"><i class="fa fa-eye"></i> View</button>'
                ]);
            });
            table = $('#purchaseOrderListTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
           // $("#salesQuotationListTbody").html(output);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
//to get listing Details
function getOrderTransaction(id)
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/GetPurchaseOrderDetails?purchaseOrderMasterId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            master = data.Master;
            details = data.Details;
            var statusOutput = "";
            if (master.orderStatus == "Pending") {
                $("#confirmOrder").show();
                $("#cancelOrder").show();
                statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending</span>';
            }
            else if (master.orderStatus == "Approved") {
                $("#confirmOrder").hide();
                $("#cancelOrder").hide();
                statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Confirmed</span>';
            }
            else if (master.orderStatus == "Cancelled") {
                $("#confirmOrder").hide();
                $("#cancelOrder").hide();
                statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
            }
            var output = "";
            for (i = 0; i < details.length; i++) {
                //if (details[i].barcode)
                //{
                //    details[i].barcode = "";
                //}
                output += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td><center>' + ((details[i].barcode == undefined) ? "-" : (details[i].barcode)) + '</center></td>\
                            <td>' + details[i].productCode + '</td>\
                            <td>' + details[i].productName + '</td>\
                            <td>' + details[i].qty + '</td>\
                            <td>' + details[i].unitName + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].amount) + '</td>\
                        </tr>';
            }

            $("#orderNoDiv").html(master.InvoiceNo);
            $("#orderDateDiv").html(Utilities.FormatJsonDate(master.Date));
            $("#orderNoDiv").html(master.InvoiceNo);
            $("#raisedByDiv").html(master.userName);
            $("#raisedForDiv").html(master.ledgerName);
            $("#statusDiv").html(statusOutput);
            $("#orderAmountTxt").val(Utilities.FormatCurrency(master.TotalAmount));
            //$("#taxAmountTxt").val(Utilities.FormatCurrency(tAmount));
            $("#grandTotalTxt").val(Utilities.FormatCurrency(master.TotalAmount));

            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
//save to material receipt pending list
function savePending()
{
    Utilities.Loader.Show();

    var lineitems = [];
    var sl = 1;

    for (i = 0; i < details.length; i++) {
        lineitems.push({
            ProductId: details[i].productId,
            Quantity: details[i].qty,
            OrderDetailsId: details[i].purchaseOrderDetailsId,
            Rate: details[i].rate,
            Description: details[i].narration,
            UnitId: details[i].unitId,
            UnitConversionId: details[i].unitConversionId,
            StoreId: details[i].godownId,
            RackId: details[i].rackId,
            BatchId: details[i].batchId,
            SL: sl
            });
        sl += 1;
    }

    toSave.ReceiptNo = master.InvoiceNo;
    toSave.TotalAmount = master.TotalAmount;
    toSave.date = master.Date;
    toSave.SupplierId = master.LedgerId;
    toSave.OrderMasterId = master.PurchaseOrderMasterId;
    toSave.Narration = master.Narration;
    toSave.LineItems = lineitems;
    console.log(toSave);

    $.ajax({
        url: API_BASE_URL + "/MaterialReceipt/SavePending",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data == true) {
                Utilities.SuccessNotification("Material Receipt Confirmed Successfully");
                $("#detailsModal").modal("hide");
                table.destroy();
                if ($("#backDays").val() == "custom") {
                    getPurchaseOrderListings($("#fromDate").val(), $("#toDate").val(), $("#orderStatus").val());
                }
                else {
                    getPurchaseOrderListings("2017-01-01", $("#toDate").val(), "Pending");
                }
            }
            else {
                Utilities.ErrorNotification("Material Receipt Confirmation Failed");
            }

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });

}

function findSuppliers(id) {
    var output = {};
    for (i = 0; i < supp.length; i++) {
        if (supp[i].ledgerId == id) {
            output = supp[i];
            break;
        }
    }
    return output;
}
function findUser(id) {
    var output = {};
    for (i = 0; i < users.length; i++) {
        if (users[i].userId == id) {
            output = users[i];
            break;
        }
    }
    return output;
}