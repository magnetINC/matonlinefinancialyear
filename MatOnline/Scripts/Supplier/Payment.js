﻿var BankOrCash = [];
var suppliers = [];
var expenses = [];
var paymentSource = [];
var accountsSource = [];
var supplierInvoices = [];
var partyBalances = [];
var receiptDetails = [];
var unprocessedInvoicePayments = [];

var CurrentBalance = 0;
var voucherNo = "";
var selectedSupplier = 0;

$(function(){
    LoadLookUps();

    $("#save").click(function () {
        savePayment();
    });

    $("#currentBalance").click(function () {
        GetCurrentBalance();
    });
    $("#accountType").change(function () {
        paymentSource = [];
        renderSupplierGrid();
    });
    $('#partyBalanceTbody').on('change', '.invAmt', function () {
        var indexOfObject = unprocessedInvoicePayments.findIndex(p=>p.InvoiceNo == $(this).attr("id"));
        if (indexOfObject >= 0) //object exists, just modify
        {
            unprocessedInvoicePayments[indexOfObject].Amount = $(this).val();
        }
        else    //object does not exist, insert new record
        {
            unprocessedInvoicePayments.push({
                InvoiceNo: $(this).attr("id"),
                Amount: $(this).val()
            });
        }
        paymentSource.find(p=>p.LedgerId == selectedSupplier).Amount = getTotalInvoiceAmount();    //set total amount for receipt details line object
        $("#amt" + selectedSupplier).val(getTotalInvoiceAmount());
        $("#totalInvoiceAmt").val(getTotalInvoiceAmount());
        //console.log(customersRow);
    });
    $("#savePartyBalance").click(function () {
        var isInvoiceExists = partyBalances.findIndex(p=>p.LedgerId == selectedSupplier);
        if (isInvoiceExists >= 0)  //record exists, fetch and edit record
        {
            for (i = 0; i < supplierInvoices.length; i++)   //loop through all the invoices, and update amount based on invoice number
            {                                               //with what's in unprocessedInvoicePayments array

                var amtIndex = unprocessedInvoicePayments.findIndex(p=>p.InvoiceNo == supplierInvoices[i].invoiceNo);
                if (amtIndex >= 0) //means amount's invoice number is in customerinvoices array
                {
                    partyBalances[isInvoiceExists].Amount = unprocessedInvoicePayments[amtIndex].Amount;
                }
            }
        }
        else    //record not found, so create new object
        {
            for (i = 0; i < supplierInvoices.length; i++)   //loop through all the invoices, and update amount based on invoice number
            {                                               //with what's in unprocessedInvoicePayments array

                var amtIndex = unprocessedInvoicePayments.findIndex(p=>p.InvoiceNo == supplierInvoices[i].invoiceNo);
                if (amtIndex >= 0 && unprocessedInvoicePayments[amtIndex].Amount > 0) //means amount's invoice number is in customerinvoices array
                {
                    partyBalances.push({
                        //Date: Utilities.YyMmDdDate(),
                        Amount: unprocessedInvoicePayments[amtIndex].Amount,
                        LedgerId: selectedSupplier,
                        ReferenceType: "Against",
                        AgainstInvoiceNo: $("#voucherNo").val(),
                        AgainstVoucherNo: $("#voucherNo").val(),
                        VoucherTypeId: supplierInvoices[i].voucherTypeId,
                        VoucherNo: supplierInvoices[i].voucherNo,
                        InvoiceNo: supplierInvoices[i].invoiceNo
                    });
                    // console.log(partyBalances);
                }
            }
        }
        $("#totalAmount").val(getTotalAmount());
        $("#applyOnAccount").modal("hide");
    });
    $('#suppliersRowTbody').on('change', '.amt', function () {
        var tempId = $(this).attr("id");
        var id = tempId.replace('amt', '');
        var indexToFind = paymentSource.findIndex(p=>p.LedgerId == id);
        if (indexToFind >= 0) {
            paymentSource[indexToFind].Amount = $(this).val();
            paymentSource[indexToFind].grossAmount = 0;
            paymentSource[indexToFind].Memo = "";
            paymentSource[indexToFind].Status = "";
            paymentSource[indexToFind].ExchangeRateId = 1;
            console.log(paymentSource);
        }
        else {
            paymentSource.push({
                Memo: "",
                LedgerId: id,
                Amount: $(this).val(),
                grossAmount: 0,
                Status: "",
                ExchangeRateId: 1
            });
        }
        // }

    });
    $('#suppliersRowTbody').on('change', '.supp', function () {
        console.log(this.value);
        var id = this.value;
        if (id != "0")   //means first item is not selected
        {
            var indexOfObject = paymentSource.findIndex(p=>p.ledgerId == id);
            selectedSupplier = id;
            if (indexOfObject >= 0)
            {
            }
            else {
                paymentSource.push({
                    LedgerId: $(this).val()
                });
                renderSupplierGrid();
            }

        }
    });
    $('#suppliersRowTbody').on('change', '.apply', function () {

        var val = this.value;
        var supplierToApplyOn = "";
        if (val != "") {
            supplierToApplyOn = $(this).attr("id").replace("apply", "");
        }
        if (val == "Against") {
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/PaymentVoucher/getPartyBalanceComboFill?id=" + supplierToApplyOn + "&voucherNo=" + $("#voucherNo").val(),
                type: "GET",
                contentType: "application/json",
                success: function (data) {
                    supplierInvoices = data;
                    console.log(supplierInvoices);
                    renderSupplierInvoices();
                    var id = selectedSupplier;
                    var indexToFind = paymentSource.findIndex(p=>p.LedgerId == id);
                    if (indexToFind >= 0) {
                        paymentSource[indexToFind].Amount = $('#applyOnAccountAmount').val();
                        paymentSource[indexToFind].grossAmount = 0;
                        paymentSource[indexToFind].Memo = "";
                        paymentSource[indexToFind].Status = "Against";
                        paymentSource[indexToFind].ExchangeRateId = 1;
                    }
                    else {
                        paymentSource.push({
                            Memo: "",
                            LedgerId: id,
                            Amount: $('#applyOnAccountAmount').val(),
                            grossAmount: 0,
                            Status: "Against",
                            ExchangeRateId: 1
                        });
                    }
                    $("#applyModal").modal("show");
                    //console.log(data);
                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.Loader.Hide();
                }
            });
        }
        else if (val == "onAccount") {
            $("#applyOnAccount").modal("show");
            var id = selectedSupplier;
            var indexToFind = paymentSource.findIndex(p=>p.LedgerId == id);
            if (indexToFind >= 0) {
                paymentSource[indexToFind].Amount = $('#applyOnAccountAmount').val();
                paymentSource[indexToFind].grossAmount = 0;
                paymentSource[indexToFind].Memo = "";
                paymentSource[indexToFind].Status = "OnAccount";
                paymentSource[indexToFind].ExchangeRateId = 1;
            }
            else {
                paymentSource.push({
                    Memo: "",
                    LedgerId: id,
                    Amount: $('#applyOnAccountAmount').val(),
                    grossAmount: 0,
                    Status: "onAccount",
                    ExchangeRateId: 1
                });
            }
        }
        // }

    });
    $('#suppliersRowTbody').on('change', '.memo', function () {
        console.log(this.value);
        var id = selectedSupplier;
        var indexToFind = paymentSource.findIndex(p=>p.LedgerId == id);
        if (indexToFind >= 0) {
            paymentSource[indexToFind].Amount = $('#applyOnAccountAmount').val();
            paymentSource[indexToFind].grossAmount = 0;
            paymentSource[indexToFind].Memo = this.value;
            paymentSource[indexToFind].ExchangeRateId = 1;
        }
        else {
            paymentSource.push({
                Memo: this.value,
                LedgerId: id,
                Amount: $('#applyOnAccountAmount').val(),
                grossAmount: 0,
                Status: "Against",
                ExchangeRateId: 1
            });
        }
    });
    $('#suppliersRowTbody').on('change', '.wht', function () {
        console.log(this.value);
        var id = selectedSupplier;
        var indexToFind = paymentSource.findIndex(p=>p.LedgerId == id);
        if (indexToFind >= 0) {
            paymentSource[indexToFind].Amount = $('#applyOnAccountAmount').val();
            paymentSource[indexToFind].grossAmount = 0;
            paymentSource[indexToFind].Memo = this.value;
            paymentSource[indexToFind].ExchangeRateId = 1;
            paymentSource[indexToFind].TaxId = 1;
        }
        else {
            paymentSource.push({
                Memo: this.value,
                LedgerId: id,
                Amount: $('#applyOnAccountAmount').val(),
                grossAmount: 0,
                Status: "Against",
                ExchangeRateId: 1,
                TaxId: 1
            });
        }
    });
    $('#applyOnAccountAmount').change(function () {
        var id = selectedSupplier;
        var indexToFind = paymentSource.findIndex(p=>p.LedgerId == id);
        if (indexToFind >= 0)
        {
            paymentSource[indexToFind].Amount = $('#applyOnAccountAmount').val();
            paymentSource[indexToFind].grossAmount = 0;
            paymentSource[indexToFind].Memo = "";
        }
        else
        {
            paymentSource.push({
                Memo: "",
                LedgerId: id,
                Amount: $('#applyOnAccountAmount').val(),
                grossAmount: 0
            });
        }
    });
    $("#saveAmountApplyOnAccount").click(function () {

        $("#amt" + selectedSupplier).val($("#applyOnAccountAmount").val());

        partyBalances.push({
            //Date: Utilities.YyMmDdDate(),
            Amount: $("#applyOnAccountAmount").val(),
            LedgerId: selectedSupplier,
            ReferenceType: "OnAccount",
            VoucherNo: $("#voucherNo").val(),
            InvoiceNo: $("#voucherNo").val()
        });
        //console.log(receiptDetails);
        $("#totalAmount").val(getTotalAmount());

        $("#applyOnAccount").modal("hide");
    });
});
function LoadLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PaymentVoucher/getAutoVoucherNo",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            $("#voucherNo").val(data);
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
    $.ajax({
        url: API_BASE_URL + "/PaymentVoucher/LookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            BankOrCash = data.BankOrCash;
            suppliers = data.Suppliers;
            expenses = data.Expenses;
            voucherNo = data.voucherNo;
            RenderControlData();

            renderSupplierGrid();
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}
function RenderControlData()
{
    var BankOrCashHtml = "";
    $.each(BankOrCash, function (count, record) {
        BankOrCashHtml += '<option value="' + record.ledgerId + '">' + record.ledgerName + '</option>';
    });
    $("#bankorcash").html(BankOrCashHtml);
}

function renderSupplierGrid() {
    if ($("#accountType").val() == 1)
    {
        accountsSource = [];
        accountsSource = suppliers;
    }
    else if ($("#accountType").val() == 2)
    {
        accountsSource = [];
        accountsSource = expenses;
    }
    var output = "";
    //console.log(customersRow);
    $.each(paymentSource, function (count, row) {
        var totalAmount = 0;
        var ledgerId = row.LedgerId;
        console.log(ledgerId);
        output += '<tr>\
                    <td>'+ (count + 1) + '</td>\
                    <td><select class="form-control supp">' + '<option value="0">-Select-</option>' + renderSelectedSupplierDropdown(ledgerId) + '</select></td>\
                    <td><select class="form-control apply" id="apply' + ledgerId + '"><option value="">-Select-</option><option value="onAccount">On Account</option><option value="Against">Against</option></select></td>\
                    <td><input class="form-control amt" value="' + (row.Amount == undefined ? 0 : row.Amount) + '" id="amt' + ledgerId + '" type="text" readonly="readonly"/></td>\
                    <td><select class="form-control"><option value="1">Naira | NGN</option></select></td>\
                    <td><select class="form-control wht" id="wht' + ledgerId + '"><option value="0">-Select-</option><option value="1">WHT - 5%</option></select></td>\
                    <td><input class="form-control" id="memo' + ledgerId + '" value="' + (row.Memo == undefined ? " " : row.Memo) + '"/></td>\
                    <td><button class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>\
                   </tr>';
        //var rowAmt = (row.Amount == undefined) ? 0 : row.Amount;
        //totalAmount = totalAmount + parseFloat(rowAmt);
        //console.log(totalAmount);
    });

    output += '<tr>\
                    <td>' + (paymentSource.length + 1) + '</td>\
                    <td><select class="form-control supp">' + '<option value="0">-Select-</option>' + Utilities.PopulateDropDownFromArray(accountsSource, 0, 2) + '</select></td>\
                    <td><select class="form-control apply"><option>--Select--</option><option>On Account</option><option>Against</option></select></td>\
                    <td><input class="form-control amt" type="text"/></td>\
                    <td><select class="form-control"><option>Naira | NGN</option></select></td>\
                    <td><select class="form-control"><option value="0">-Select-</option><option>WHT - 5%</option></select></td>\
                    <td><input class="form-control" type="text"/></td>\
                    <td><button class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>\
                   </tr>';
    //console.log(output);var 
    $("#suppliersRowTbody").html(output);
    $("#totalAmount").val(getTotalAmount());
    if ($("#accountType").val() == 2) {
        $('.amt').attr("readonly", false);
        $('#amt' + selectedSupplier).attr("readonly", false);
        $('.apply').attr("disabled", true);
    }
    else {
        $('.amt').attr("readonly", true);
        $('.apply').attr("disabled", false);
    }
}
function getTotalAmount() {
    var totalAmount = 0;
    $.each(paymentSource, function (count, row) {
        var rowAmt = row.Amount == undefined ? 0 : row.Amount;
        totalAmount = totalAmount + parseFloat(rowAmt);
    });
    return totalAmount;
}
function getTotalInvoiceAmount() {
    var amt = 0.0;
    for (i = 0; i < unprocessedInvoicePayments.length; i++) {
        var chk = unprocessedInvoicePayments[i].Amount == undefined ? 0 : parseFloat(unprocessedInvoicePayments[i].Amount)
        amt = amt + chk;
    }
    return amt;
}
function renderSUpplierInvoices() {
    var output = "";
    for (i = 0; i < supplierInvoices.length; i++) {
        output +=
           '<tr>\
            <td>'+ (i + 1) + '</td>\
            <td>' + supplierInvoices[i].display + ' -- ' + supplierInvoices[i].balance + '</td>\
            <td>' + supplierInvoices[i].invoiceNo + '</td>\
            <td>' + supplierInvoices[i].balance + '</td>\
            <td><input class="form-control invAmt" id="' + supplierInvoices[i].invoiceNo + '" type="text"/></td>\
            <td>Cr</td>\
        </tr>';
    }
    $("#partyBalanceTbody").html(output);
}
function GetCurrentBalance()
{
    Utilities.Loader.Show();
    var ledgerId = $("#bankorcash").val();
    $.ajax({
        url: API_BASE_URL + "/PaymentVoucher/GetCurrentBalance?ledgerId=" + ledgerId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            $("#balanceText").html(Utilities.FormatCurrency(data));
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function renderSelectedSupplierDropdown(selectedValue) {
    var output = "";
    console.log(accountsSource);
    for (i = 0; i < accountsSource.length; i++) {
        if (accountsSource[i].ledgerId == selectedValue) {
            output += '<option selected value="' + accountsSource[i].ledgerId + '">' + accountsSource[i].ledgerName + '</option>';
        }
        else {
            output += '<option value="' + accountsSource[i].ledgerId + '">' + accountsSource[i].ledgerName + '</option>';
        }
    }
    return output;
}

function savePayment() {
    if ($("#voucherNo").val() == "") {
        Utilities.ErrorNotification("Voucher number required!");
        return;
    }
    else if ($("#transactionDate").val() == "") {
        Utilities.ErrorNotification("Payment Date required!");
        return;
    }
    else if ($("#chequeDate").val() == "") {
        Utilities.ErrorNotification("Cheque Date required!");
        return;
    }
    else {
        console.log(this.value);
        var id = selectedSupplier;
        var indexToFind = paymentSource.findIndex(p=>p.LedgerId == id);
        if (indexToFind >= 0) {
            paymentSource[indexToFind].Amount = $("#amt" + selectedSupplier).val();
        }
        var paymentMaster = {
            VoucherNo: $("#voucherNo").val(),
            InvoiceNo: $("#voucherNo").val(),
            LedgerId: $("#bankorcash").val(),
            Narration: "",
            UserId: 1,
            Date: $("#transactionDate").val(),
            TotalAmount: $("#totalAmount").val(),
            ChequeNo: $("#chqrefNo").val(),
            ChequeDate: $("#chequeDate").val(),
            PaymentDetails: paymentSource,
            PartyBalances: partyBalances
        };
        console.log(paymentMaster);
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PaymentVoucher/Save",
            data: JSON.stringify(paymentMaster),
            type: "POST",
            contentType: "application/json",
            success: function (data) {
                Utilities.SuccessNotification("Payment saved!");
                Utilities.Loader.Hide();
                window.location = "/supplier/paymentvoucher/index";
            },
            error: function (err) {
                Utilities.SuccessNotification("Oops! Something went wrong!");
                Utilities.Loader.Hide();
            }
        });
    }

}

function renderSupplierInvoices() {
    var output = "";
    for (i = 0; i < supplierInvoices.length; i++) {
        output +=
           '<tr>\
            <td>'+ (i + 1) + '</td>\
            <td>' + supplierInvoices[i].display + ' -- ' + supplierInvoices[i].balance + '</td>\
            <td>' + supplierInvoices[i].invoiceNo + '</td>\
            <td>' + supplierInvoices[i].balance + '</td>\
            <td><input class="form-control invAmt" id="' + supplierInvoices[i].invoiceNo + '" type="text"/></td>\
            <td>Cr</td>\
        </tr>';
    }
    $("#partyBalanceTbody").html(output);
}