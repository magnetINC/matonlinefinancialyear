﻿var suppliers = [];
var currencies = [];
var voucherTypes = [];
var LineItems = [];
var totalLineItemAmount = 0;

var units = [];
var batches = [];
var stores = [];
var racks = [];
var taxes = [];
var allProd = [];
var rejectionoutNo = 0;

$(function () {
    loadLookups();

    $("#suppliers").change(function () {
        var supplierId = $("#suppliers").val();
        var applyOn = $("#applyOn").val();
        if(supplierId>0)
        {
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/RejectionOut/MaterialReceiptNumbers?supplierId=" + supplierId + "&applyOn="+applyOn,
                type: "GET",
                contentType: "application/json",
                success: function (data) {

                    var materialReceiptNoHtml = "<option></option>";
                    $.each(data, function (count, record) {
                        materialReceiptNoHtml += '<option value="' + record.materialReceiptMasterId + '">' + record.invoiceNo + '</option>';
                    });
                    $("#materialReceiptNo").html(materialReceiptNoHtml);
                    console.log(data);
                    Utilities.Loader.Hide();
                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }
    });

    $("#materialReceiptNo").change(function () {
        var mNo = $("#materialReceiptNo").val();
        if (mNo > 0) {
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/RejectionOut/GetMaterialReceiptDetails?materialReceiptNo=" + mNo,
                type: "GET",
                contentType: "application/json",
                success: function (data) {
                    for (i = 0; i < data.length; i++) {
                        data[i].QtyToReturn = 0;
                    }
                    LineItems = data;
                    console.log(data);

                    renderLineItems();
                    Utilities.Loader.Hide();
                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }
    });
    $("#applyOn").on("change", function () {
        var supplierId = $("#suppliers").val();
        var applyOn = $("#applyOn").val();
        if (supplierId > 0) {
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/RejectionOut/MaterialReceiptNumbers?supplierId=" + supplierId + "&applyOn=" + applyOn,
                type: "GET",
                contentType: "application/json",
                success: function (data) {

                    var materialReceiptNoHtml = "<option></option>";
                    $.each(data, function (count, record) {
                        materialReceiptNoHtml += '<option value="' + record.materialReceiptMasterId + '">' + record.invoiceNo + '</option>';
                    });
                    $("#materialReceiptNo").html(materialReceiptNoHtml);
                    console.log(data);
                    Utilities.Loader.Hide();
                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }
    });

    $("#save").click(function () {
        Save();
    });
});

function loadLookups()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/RejectionOut/RejectionOutLookups",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            suppliers = data.Suppliers;
            currencies = data.Currencies;
            voucherTypes = data.VoucherTypes;
            batches = data.Batches;
            units = data.Unit;
            stores = data.stores;
            racks = data.racks;
            taxes = data.taxes;
            allProd = data.allproducts;
            rejectionoutNo = data.rejectionoutNo

            console.log(data);
            populateLookupsControls();
            Utilities.Loader.Hide();
        },
        error:function()
        {
            Utilities.Loader.Hide();
        }
    });
}

function populateLookupsControls()
{
    $("#orderNo").val(rejectionoutNo);

    var currencyHtml = "";
    $.each(currencies, function (count,record) {
        currencyHtml += '<option value="' + record.exchangeRateId + '">' + record.currencyName + '</option>';
    });
    $("#currency").html(currencyHtml);

    //var suppliersHtml = "<option></option>";
    //$.each(suppliers, function (count, record) {
    //    suppliersHtml += '<option value="' + record.ledgerId + '">' + record.ledgerName + '</option>';
    //});
    //$("#suppliers").html(suppliersHtml);
    $("#suppliers").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: suppliers,
        optionLabel: "please Select..."
    });

    var applyOnHtml = "";
    applyOnHtml +=  '<option></option>';
    $.each(voucherTypes, function (count, record) {
        applyOnHtml += '<option value="' + record.voucherTypeId + '">' + record.voucherTypeName + '</option>';
    });
    $("#applyOn").html(applyOnHtml);


}

function renderLineItems()
{
    var output = "";
    var totalAmount = 0.0;
    $.each(LineItems, function (count, row) {
        output +=
            '\
            <tr>\
                 <td style="white-space: nowrap;">' + (count + 1) + '</td>\
                <td style="white-space: nowrap;">' + row.barcode + '</td>\
                <td style="white-space: nowrap;">' + row.barcode + '</td>\
                <td style="white-space: nowrap;">'+ findProduct(row.productId).productName + '</td>\
                <td style="white-space: nowrap;">'+ row.qty + '</td>\
                <td style="white-space: nowrap;">'+ row.QtyToReturn + '</td>\
                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                <td style="white-space: nowrap;">' +  findUnit(row.unitId).unitName + '</td>\
                <td style="white-space: nowrap;">' + findStore(row.godownId).godownName + '</td>\
                <td><button class="btn btn-sm btn-info" onclick="changeQuantity(' + row.productId + ')"><i class="fa fa-edit"></i></button></td>\
            </tr>\
            ';
        totalAmount = (totalAmount + row.amount)
        totalLineItemAmount = totalAmount;

        console.log(totalAmount);
    });
    $("#lineItemTbody").html(output);
    $("#totalAmount").val(Utilities.FormatCurrency(totalAmount));
}

function Save()
{
    Utilities.Loader.Show();
    for (i = 0; i < LineItems.length; i++) {
        LineItems[i].Quantity = LineItems[i].QtyToReturn;
        LineItems[i].StoreId = LineItems[i].godownId;
    }
    newRejectionOut = {
        Date: $("#transactionDate").val(),
        SupplierId: $("#suppliers").val(),
        MaterialReceiptNo: $("#materialReceiptNo").val(),
        Narration: $("#narration").val(),
        TotalAmount: totalLineItemAmount,
        TransportCompany: $("#transportationCompany").val(),
        LrNo: $("#lrNo").val(),
        CurrencyId: $("#currency").val(),
        LineItems: LineItems,
        RejectionOutNo: $("#orderNo").val(),
        DoneBy: $("#UserId").html(),
    };
    console.log(newRejectionOut);
    $.ajax({
        url: API_BASE_URL + "/RejectionOut/SaveOrEditFunction",
        type: "POST",
        data: JSON.stringify(newRejectionOut),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            Utilities.SuccessNotification("Rejection Saved successfully");
            window.location = "/Supplier/RejectionOut/Index";
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function changeQuantity(productId) {
    $("#quantityModal").modal("show");
    productToEdit = productId;
}

function applyQuantityOnObject() {
    console.log(productToEdit);
    for (i = 0; i < LineItems.length; i++) {
        if (LineItems[i].productId == productToEdit) {
            if ($("#quantity").val() <= LineItems[i].qty) {
                LineItems[i].QtyToReturn = $("#quantity").val();
                LineItems[i].amount = LineItems[i].rate * $("#quantity").val();
            }
            else {
                Utilities.ErrorNotification("Quantity to Return cannot be Higher than Quantity Ordered");
            }
        }

    }
    $("#quantity").val("");
    $("#quantityModal").modal("hide");
    renderLineItems();
}



function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}
function findProduct(productId) {
    var output = {};
    for (i = 0; i < allProd.length; i++) {
        if (allProd[i].productId == productId) {
            output = allProd[i];
            break;
        }
    }
    return output;
}