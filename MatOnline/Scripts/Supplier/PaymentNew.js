﻿var bankOrCash = [];
var suppliers = [];
var expenses = [];
var accountsSource = [];
var supplierInvoices = [];
var paymentSource = [];
var partyBalances = [];
var voucherNo = "";
var supplierToApplyOn = 0;
var transactionDetails = {};
var partyBalanceDetails = [];
var toSave = {};
//var typeItem = {};
var partyBalanceRenderDetails = [];
var totalAmount = 0;
var itemAmountList = [];
var deleteIndex;


$(function () {

    if ($("#queryString").val() != "") {
        viewDetailsClick($("#queryString").val());
    } 
    
    $("#currentBalance").click(function () {
        GetCurrentBalance();
    });
    $("#typeParent").hide();
    $("#balanceText").hide();
    getVoucherNo();
    LoadLookUps();
});

/* ======= API CALLS ======= */
function getVoucherNo()
{
    Utilities.Loader.Show();
   $.ajax({
        url: API_BASE_URL + "/PaymentVoucher/getAutoVoucherNo",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            $("#voucherNo").val(data);
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}
function LoadLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PaymentVoucher/LookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            bankOrCash = data.BankOrCash;
            suppliers = data.Suppliers;
            expenses = data.Expenses;
            voucherNo = data.voucherNo;
            voucherTypes = data.VoucherTypes;
            RenderControlData();

            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}


$("#save , #savePrint").click(savePayment);

function savePayment() {
    var myId = $(this).attr('id');
    console.log(myId);
    if ($("#voucherNo").val() == "") {
        Utilities.ErrorNotification("Voucher number required!");
        return;
    }
    else if ($("#transactionDate").val() == "") {
        Utilities.ErrorNotification("Payment Date required!");
        return;
    }
    else if ($("#chequeDate").val() == "") {
        Utilities.ErrorNotification("Cheque Date required!");
        return;
    }
    else if ($("#bankorcash").val() == 0 || $("#bankorcash").val() == null || $("#bankorcash").val() == undefined) {
        Utilities.ErrorNotification("Please select a bank");
        return;
    }
    else {
        console.log(this.value);
        var id = supplierToApplyOn;
        var indexToFind = paymentSource.findIndex(p=>p.LedgerId == id);
        if (indexToFind >= 0) {
            paymentSource[indexToFind].Amount = $("#amt" + supplierToApplyOn).val();
        }
        var paymentMaster = {
            VoucherNo: $("#voucherNo").val(),
            InvoiceNo: $("#voucherNo").val(),     //This is where i need to get my invoice number from.
            LedgerId: $("#bankorcash").val(),
            DoneBy: $("#UserId").html(),
            Narration: "",
            UserId: 1,
            //UserId: $("#suppliers").val(),
            Date: $("#transactionDate").val(),
            TotalAmount: getTotalPaymentDetailsAmount(),
            ChequeNo: $("#chqrefNo").val(),
            ChequeDate: $("#chequeDate").val(),
            //Details: paymentSource,
            PaymentDetails: paymentSource,
            PartyBalances: partyBalances
        };
        Utilities.Loader.Show();
        //console.log('test', paymentMaster);
        debugger;
        $.ajax({
            url: API_BASE_URL + "/PaymentVoucher/Save",
            data: JSON.stringify(paymentMaster),
            type: "POST",
            contentType: "application/json",
            success: function (data) {
                Utilities.SuccessNotification("Payment saved!");
                Utilities.Loader.Hide();
                if (myId == "savePrint") {
                    print(paymentMaster, paymentSource);
                }
                window.location = "/supplier/paymentvoucher/index";
            },
            error: function (err) {
                Utilities.SuccessNotification("Oops! Something went wrong!");
                Utilities.Loader.Hide();
            }
        });
    }
}


//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;

//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}



function print(paymentMaster, paymentSource) {
    var objToPrint = [];

    $.each(paymentSource, function (count, row) {
        var newAmount = parseFloat(row.Amount);
        objToPrint.push({
            "SlNo": count + 1,
            "payTo": findSuppliers(row.LedgerId).ledgerName,
            "InvoiceNo": row.InvoiceNo,
            "Currency": 'Naira | NGN',
            "Memo": row.Memo,
            "Amount": 'N' + Utilities.FormatCurrency(newAmount),
        });
    });

    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();
    var thisEmail = $("#companyEmail").val();
    var thisPhone = $("#companyPhone").val();

    var thisVoucherNo = paymentMaster.VoucherNo;

    //get the value selected from options
    var domNode = document.getElementById("bankorcash");
    var value = domNode.selectedIndex;
    var thisAccount = domNode.options[value].text;

    var thisDate = paymentMaster.Date;
    var totalAmt = paymentMaster.TotalAmount;

    columns = [
        { title: "S/N", dataKey: "SlNo" },
        { title: "Pay To", dataKey: "payTo" },
        { title: "Currency", dataKey: "Currency" },
        { title: "Invoice No", dataKey: "InvoiceNo" },
        { title: "Memo", dataKey: "Memo" },
        { title: "Amount", dataKey: "Amount" },
    ];

    var doc = new jsPDF('portrait', 'pt', 'letter');
    doc.setDrawColor(0);

    //start drawing
    doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text(companyName, 320, 80, 'center');
    doc.setFontSize(9);
    doc.setFontType("normal");
    doc.text(companyAddress, 320, 90, 'center');

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Email Id: ', 400, 150);
    doc.setFontType("normal");
    doc.text(thisEmail, 460, 150);
    doc.setFontType("bold");
    doc.text('Phone: ', 400, 165);
    doc.setFontType("normal");
    doc.text(thisPhone, 460, 165);

    doc.line(40, 170, 570, 170);

    doc.setFontType("bold");
    doc.setFontSize(9);
    doc.text('Account:', 40, 185);
    doc.setFontType("normal");
    doc.text(thisAccount, 85, 185);

    doc.setFontType("bold");
    doc.text('Voucher No: ', 400, 185);
    doc.setFontType("normal");
    doc.text(thisVoucherNo, 460, 185);
    doc.setFontType("bold");
    doc.text('Date: ', 400, 205);
    doc.setFontType("normal");
    doc.text(thisDate, 460, 205);

    doc.line(40, 220, 570, 220);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text('Payments', 250, 240);

    //doc.line(120, 20, 120, 60); horizontal line
    doc.setFontSize(8);
    doc.autoTable(columns, objToPrint, {
        startY: 255,
        theme: 'striped',
        styles: {
            fontSize: 8,
        },
        columnStyles: {
            SlNo: { columnWidth: 30, },
            Amount: { columnWidth: 80, halign: 'right' },
            InvoiceNo: { columnWidth: 80},
            Memo: { columnWidth: 100 },
            PayTo: { columnWidth: 110 },
            Currency: { columnWidth: 100 }

        },
    });

    doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Total Amount: NGN' + Utilities.FormatCurrency(parseInt(totalAmt)), 555, doc.autoTable.previous.finalY + 20, "right");

    doc.line(40, doc.autoTable.previous.finalY + 30, 570, doc.autoTable.previous.finalY + 30);

    doc.setFontType("bold");
    doc.text('Amount in Words: ', 40, doc.autoTable.previous.finalY + 60);
    doc.setFontType("normal");

    //remove commas
    //var tempTotal = totalAmt.replace(/,/g, "");
    //convert to number
    var amtToBeConverted = parseInt(totalAmt);

    //capitalize Each Word
    var wordFormat = Utilities.NumberToWords(amtToBeConverted);
    const words = wordFormat.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());

    doc.text(words + ' Naira ONLY.', 120, doc.autoTable.previous.finalY + 60);

    doc.setFontType("bold");
    doc.text('Payment approved by:  _________________________________', 40, doc.autoTable.previous.finalY + 100);

    doc.text('Done by:  ' + $("#nameSpan").html(), 350, doc.autoTable.previous.finalY + 100);
    doc.text('_________________', 390, doc.autoTable.previous.finalY + 103);
    doc.autoPrint();
    doc.save('Payment.pdf');
    //window.open(doc.output('bloburl'));
    window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
}


function GetCurrentBalance() {
    var ledgerId = $("#bankorcash").val();
    if(ledgerId == 0 || ledgerId == null || ledgerId == undefined)
    {
        Utilities.ErrorNotification("Please select a Bank");
        return;
    }
    else
    {
        Utilities.Loader.Show();
        debugger;

        $.ajax({
            url: API_BASE_URL + "/PaymentVoucher/GetCurrentBalance?ledgerId=" + ledgerId,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                var currentBalance = "₦" + Utilities.FormatCurrency(data);
                $("#balanceText").val(currentBalance);
                $("#balanceText").show();
                Utilities.Loader.Hide();
            },
            error: function () {
                Utilities.Loader.Hide();
            }
        });
    }
}

/* ======= RENDER DATA TO CONTROLS ======= */
function RenderControlData()
{
    //$("#bankorcash").kendoDropDownList({
    //    filter: "contains",
    //    optionLabel: "Please Select...",
    //    dataTextField: "ledgerName",
    //    dataValueField: "ledgerId",
    //    dataSource: bankOrCash
    //});
    $("#bankorcash").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(bankOrCash, 1, 0));
    $("#bankorcash").chosen({ width: "100%", margin: "1px" });
}


var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the positive lable as key
    //...
};

function confirmDelete(index) {
    deleteIndex = index;
    eModal.confirm(options).then(removeLineItem);
}

function removeLineItem() {
    paymentSource.splice(deleteIndex, 1);
    partyBalances.splice(deleteIndex, 1);
    renderPayments();
}

function changeAmount(selectObject) {
    var newAmount = parseFloat(selectObject.value);
    $("#totalPaymentAmt").val(Utilities.FormatCurrency(newAmount));
}
function getTotalInvoiceAmount() {
    var amt = 0.0;
    for (i = 0; i < unprocessedInvoicePayments.length; i++) {
        var chk = unprocessedInvoicePayments[i].Amount == undefined ? 0 : parseFloat(unprocessedInvoicePayments[i].Amount)
        amt = amt + chk;
    }
    return amt;
}
function addPaymentToObject()
{
    if ($("#suppliers").val() == "") {
        Utilities.ErrorNotification("Select a Supplier");
        return false;
    }

    if (partyBalanceRenderDetails.length < 1) {
        Utilities.ErrorNotification("Nothing to add");
        return false;
    }

    for (var i = 0; i < partyBalanceRenderDetails.length; i++) {
        let credit = itemAmountList[i] == undefined ? 0 : itemAmountList[i];
        //console.log({ credit })
        if (Number(credit) > 0) {
            debugger;
            partyBalances.push({
                Date: Utilities.YyMmDdDate(),
                Credit: itemAmountList[i] == undefined ? 0 : itemAmountList[i],
                LedgerId: $("#suppliers").val(),
                ReferenceType: partyBalanceRenderDetails[i].reference,
                VoucherNo: partyBalanceRenderDetails[i].voucherNo,
                InvoiceNo: partyBalanceRenderDetails[i].invoiceNo
            });
        }
    }
    //partyBalances.push({
    //    Date: Utilities.YyMmDdDate(),
    //    Credit: $("#totalPaymentAmount").val(),
    //    LedgerId: $("#suppliers").val(),
    //    ReferenceType: "OnAccount",
    //    VoucherNo: $("#voucherNo").val(),
    //    InvoiceNo: $("#voucherNo").val()
    //});


    //always empty array to populate with updated data
    /*paymentSource.push({
        Memo: $("#memo").val(),
        LedgerId: $("#suppliers").val(),
        Amount: $('#totalPaymentAmount').val(),
        grossAmount: 0,
        Status: "OnAccount",
        ExchangeRateId: $("#currency").val()
    });*/

    for (var i = 0; i < partyBalanceRenderDetails.length; i++) {
        let credit = itemAmountList[i] == undefined ? 0 : itemAmountList[i];
        if (Number(credit) > 0) {
            paymentSource.push({
                Memo: $("#memo").val(),
                LedgerId: $("#suppliers").val(),
                Amount: itemAmountList[i] == undefined ? 0 : itemAmountList[i],
                grossAmount: 0,
                Status: partyBalanceRenderDetails[i].reference,
                ExchangeRateId: $("#currency").val(),
                InvoiceNo: partyBalanceRenderDetails[i].invoiceNo
            });
        }
    }

    //console.log("Payments " , paymentSource);
    renderPayments();
    //$("#suppliers").val("");
    //$("#suppliers").trigger("chosen:updated");
    $("#memo").val("");
    //$("#reference").val("");
    //$("#reference").trigger("chosen:updated");
    partyBalanceRenderDetails = [];
    itemAmountList = [];
    $("#totalPaymentAmount").val(0);
    renderPartyBalanceDetails();
}


function renderPayments() {
    var output = "";
    console.log(paymentSource);
    $.each(paymentSource, function (count, row) {
        var newAmount = parseFloat(row.Amount);
        output += '<tr>\
                    <td>'+ (count + 1) + '</td>\
                    <td>' + findSuppliers(row.LedgerId).ledgerName + '</td>\
                    <td>&#8358;' + Utilities.FormatCurrency(newAmount) + '</td>\
                    <td>'+ row.InvoiceNo +'</td>\
                    <td>Naira | NGN </td>\
                    <td>' + row.Memo + '</td>\
                    <td><button class="btn btn-sm btn-danger" onclick="confirmDelete(' + count + ')"><i class="fa fa-times"></i></button></td>\
                   </tr>';
    });
    $("#suppliersRowTbody").html(output);
    $("#totalAmount").val(Utilities.FormatCurrency(getTotalPaymentDetailsAmount()));

    $("#amount").val(0);
    $("#memo").val("");
    $("#totalPaymentAmt").val(0);
}


function openModal() {
    if ($("#bankorcash").val() == "") {
        Utilities.ErrorNotification("You need to fill the Bank/Cash field!");
        return false;
    }

    $("#paymentModal").modal("show");
    $('#paymentModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });

    if ($("#accountType").val() == 1) {
        accountsSource = [];
        accountsSource = suppliers;
    }
    else if ($("#accountType").val() == 2) {
        accountsSource = [];
        accountsSource = expenses;
    }

    $("#suppliers").kendoDropDownList({
        filter: "contains",
        optionLabel: "Please Select...",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: accountsSource
    });
}

//$("#suppliers").change(function () {
//        var ledgerId = parseInt($("#suppliers").val());
//        Utilities.Loader.Show();
//        $.ajax({
//            url: API_BASE_URL + "/SupplierCentre/GetLedgerDetails?ledgerId=" + ledgerId,
//            type: "GET",
//            contentType: "application/json",
//            success: function (data) {
//                transactionDetails = data;
//                renderTransactionDetails();
//                Utilities.Loader.Hide();
//            },
//            error: function (err) {
//                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
//                Utilities.Loader.Hide();
//            }
//        });
//});



$("#suppliers").change(function () {

    var newReference = $("#reference").val();
    //$("#reference").val("");
    $("#typeParent").hide();
    var ledgerId = parseInt($("#suppliers").val());
    console.log({ newReference })

    suppliers.forEach(function (item) {
        if (item.ledgerId == ledgerId) {
            toSave.LedgerId = ledgerId;
            //toSave.VoucherTypeId = voucherTypes.find(p => p.typeOfVoucher == "Payments").voucherTypeId;
            toSave.VoucherTypeId = voucherTypes.find(p => p.typeOfVoucher == "Purchase Invoice").voucherTypeId;
            toSave.AccountGroupId = item.accountGroupId;
            toSave.CrDr = "Dr"; //
            return true;
        }
    });
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SupplierCentre/GetPartyBalanceDetails",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data) {
            partyBalanceDetails = data;
            //$("#partyBalanceTbody").html('');
            //console.log("party balance", data);
            renderPartyBalanceDetails()
            populateTableWithData(newReference);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });

});

function populateTableWithData(newReference) {
    if (newReference === "Against" || newReference === "OnAccount" ) {
        if ($("#reference").val() === "Against") {
            //$("#typeParent").show();

            //$("#type").kendoDropDownList({
            //    filter: "contains",
            //    optionLabel: "Please Select...",
            //    dataTextField: "display",
            //    dataValueField: "invoiceNo",
            //    dataSource: partyBalanceDetails.PartyDetails
            //});

            let allPartyDetails = partyBalanceDetails.PartyDetails.map(item => ({ ...item, reference: 'Against' }));
            partyBalanceRenderDetails = allPartyDetails;
            renderPartyBalanceDetails();

        }
        else if ($("#reference").val() == "OnAccount") {
            partyBalanceRenderDetails = partyBalanceDetails.PartyDetails.filter(x => x.reference === 'Against')
            var typeItem = {};
            typeItem.balance = "";
            typeItem.display = "";
            typeItem.invoiceNo = "";
            typeItem.voucherNo = "";
            typeItem.reference = "On Account";
            partyBalanceRenderDetails.push(typeItem);
            renderPartyBalanceDetails();
            $("#typeParent").hide();
        }
        else {
            $("#typeParent").hide();
        }
    } 
}

$("#reference").change(function () {
    if ($("#reference").val() == "Against") {
        //$("#typeParent").show();
        $("#type").kendoDropDownList({
            filter: "contains",
            optionLabel: "Please Select...",
            dataTextField: "display",
            dataValueField: "invoiceNo",
            dataSource: partyBalanceDetails.PartyDetails
        });

        if (partyBalanceDetails.PartyDetails) {
            let allPartyDetails = partyBalanceDetails.PartyDetails.map(item => ({ ...item, reference: 'Against' }));
            partyBalanceRenderDetails = allPartyDetails;
            renderPartyBalanceDetails();
        }
    }
    else if ($("#reference").val() == "OnAccount") {
        partyBalanceRenderDetails = partyBalanceDetails.PartyDetails.filter(x => x.reference === 'Against')
        var typeItem = {};
        typeItem.balance = "";
        typeItem.display = "";
        typeItem.invoiceNo = "";
        typeItem.voucherNo = "";
        typeItem.reference = "On Account";
        partyBalanceRenderDetails.push(typeItem);
        renderPartyBalanceDetails();
        $("#typeParent").hide();
    }

    else {
        $("#typeParent").hide();
    }
});

$("#type").change(function () {
    if ($("#type").val() != "") {
        var typeItem = {};
        typeItem = partyBalanceDetails.PartyDetails.find(p => p.invoiceNo == $("#type").val());
        typeItem.reference = "Against";
        partyBalanceRenderDetails.push(typeItem);
        renderPartyBalanceDetails();
    }
});

$(".lineItemAmt").change(
    calculateTotalAmount
);

function getTotalPaymentDetailsAmount() {
    var totalPaymentDetailsAmt = 0.0;
    $.each(paymentSource, function (count, row) {
        var chk = row.Amount == undefined ? 0 : row.Amount;
        totalPaymentDetailsAmt = totalPaymentDetailsAmt + parseFloat(chk);
    });
    return totalPaymentDetailsAmt;
}

function findSuppliers(ledgerId) {
    var output = {};
    for (i = 0; i < suppliers.length; i++) {
        if (suppliers[i].ledgerId == ledgerId) {
            output = suppliers[i];
            break;
        }
    }
    for (i = 0; i < expenses.length; i++) {
        if (expenses[i].ledgerId == ledgerId) {
            output = expenses[i];
            break;
        }
    }
    return output;
}


function renderPartyBalanceDetails() {
    var output = "";//"<thead><th>S/N</th><th>Date</th><th>Type</th><th>Num</th><th>Memo</th><th>Debit</th><th>Credit</th><th>Balance</th></thead><tbody>";
    var counter = 0;

    $.each(partyBalanceRenderDetails, function (count, row) {
        //overallBalance += row.OpeningCredit - row.OpeningDebit;
        counter++;
        output +=
            '<tr>\
                <td style="white-space: nowrap;">'+ counter + '</td>\
                <td style="white-space: nowrap;">' + row.reference + '</td>\
                <td style="white-space: nowrap;">' + row.display + '</td>\
                <td style="white-space: nowrap;">' + row.invoiceNo + '</td>\
                <td style="white-space: nowrap;" class="outstandingItem">' + row.balance + '</td>\
                <td style="white-space: nowrap;">' + '<input data-id="' + counter + '" onfocusout="calculateTotalAmount(' + counter + ')" class="form-control lineItemAmt" value="" type="number" min=' + 0 + ' max=' + row.balance + ' /> ' + '</td >\
                <td style = "white-space: nowrap;" > ' + 'Dr' + '</td >\
            </tr>\
            ';
    });
    $("#partyBalanceTbody").html(output);
    if($("#reference").val() === "OnAccount"){
        $(".outstandingTablehead").hide(); 
        $(".outstandingItem").hide()
    }
    else {
        $(".outstandingTablehead").show();
        $(".outstandingItem").show()
    }
    
 }

function viewDetailsClick(pendingPaymentMasterIdFromUrl) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PaymentVoucher/GetOnePendingPayment?pendingPaymentMasterIdFromUrl=" + pendingPaymentMasterIdFromUrl,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var master = data.PendingPaymentMaster[0]; console.log("master:", master);
            var details = data.PendingPaymentDetails; console.log("master:", master);
            ///
            var ledgerId = master.ledgerId;
            setTimeout(function () {
                $("#bankorcash").val("56");
                $("#bankorcash").trigger("chosen:updated");
            }, 500);
            
            $("#accountType").val(2); 
            $("#totalAmount").val(master.totalAmount)

            details.forEach(function (item) {
                partyBalances.push({
                    Date: item.extraDate,
                    Credit: "Dr",
                    LedgerId: item.ledgerId,
                    ReferenceType: "OnAccount",
                    VoucherNo: master.voucherNo,
                    InvoiceNo: master.invoiceNo
                });

                paymentSource.push({
                    Memo: item.Memo,
                    LedgerId: item.ledgerId,
                    Amount:item.amount,
                    grossAmount: item.grossAmount,
                    Status: "OnAccount",
                    ExchangeRateId: item.exchangeRateId
                });

            });

            setTimeout(renderPayments, 500);
           
           

            ////
           // for (var i = 0; i < details.length; i++) {
           //     var unitName = findUnit(details[i].UnitId).unitName == undefined ? "N/A" : findUnit(details[i].UnitId).unitName;
           //     var batchNo = findStore(+details[i].GodownId).godownName = undefined ? "N/A" : findStore(+details[i].GodownId).godownName;
           //     output +=
           //'<tr>\
           //     <td style="white-space: nowrap;"><button onclick="removeLineItem(' + details[i].ProductId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
           //     <td style="white-space: nowrap;">'+ (i + 1) + '</td>\
           //     <td style="white-space: nowrap;">' + findProduct(details[i].ProductId).ProductCode + '</td>\
           //     <td style="white-space: nowrap;">' + findProduct(details[i].ProductId).ProductName + '</td>\
           //     <td style="white-space: nowrap;">' + findStore(+details[i].GodownId).godownName + '</td>\
           //     <td style="white-space: nowrap;">' + details[i].Qty + '</td>\
           //     <td style="white-space: nowrap;">' + unitName + '</td>\
           //     <td style="white-space: nowrap;">' + batchNo + '</td>\
           //     <td style="white-space: nowrap;">' + details[i].Rate + '</td>\
           //     <td style="white-space: nowrap;">' + details[i].GrossAmount + /*'</td>\
           //     <td style="white-space: nowrap;">' + row.Discount + '</td>\
           //     <td style="white-space: nowrap;">'+ row.NetAmount + */ '</td>\
           //     <td style="white-space: nowrap;">' + details[i].TaxName + '</td>\
           //     <td style="white-space: nowrap;">' + details[i].TaxAmount + '</td>\
           //     <td style="white-space: nowrap;">' + details[i].Amount + '</td>\
           // </tr>\
           // ';
           // }
           // $("#posLineItemTbody").html(output);
           // $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry, could not load pending details.");
            Utilities.Loader.Hide();
        }
    });
}

function calculateTotalAmount(counter) {

    itemAmountList = [];
    var row = partyBalanceRenderDetails[counter -1];
    console.log({ counter, row, event });
    let totalAmount = 0;
    let userVal = Number(event.target.value);
    var balance = Number(row.balance);
    if (userVal > row.balance && row.reference === 'Against') {
        event.target.value = balance;
    }
    var amtList = document.getElementsByClassName('lineItemAmt');
    for (var i = 0; i < amtList.length; i++) {
        if (amtList[i].value.trim() == "") {
            amtList[i].value = 0;
        }
        totalAmount += parseFloat(amtList[i].value);
        itemAmountList.push(amtList[i].value);
    }

    $("#totalPaymentAmount").val(totalAmount);
}
//function renderTransactionDetails() {
//    var output = "";//"<thead><th>S/N</th><th>Date</th><th>Type</th><th>Num</th><th>Memo</th><th>Debit</th><th>Credit</th><th>Balance</th></thead><tbody>";
//    var counter = 0;
//    var overallBalance = 0.0;
//    var debitBalance = 0.0;
//    var creditBalance = 0.0;
//    totalTaxAmount = 0.0;
//    totalBillDiscountAmount = 0.0;
//    totalAmount = 0.0;
        
//        if (transactionDetails.LedgerDetails.Table.length > 0) {
//            $.each(transactionDetails.LedgerDetails.Table, function (count, row) {
//                counter++;
//                overallBalance += row.OpeningCredit - row.OpeningDebit;
//                output +=
//                    '<tr>\
//                <td style="white-space: nowrap;">'+ counter + '</td>\
//                <td style="white-space: nowrap;">' + transactionDetails.OpeningDate + '</td>\
//                <td style="white-space: nowrap;">' + 'Opening Balance' + '</td>\
//                <td style="white-space: nowrap;">' + 'N/A' + '</td>\
//                <td style="white-space: nowrap;">' + '' + '</td>\
//                <td style="white-space: nowrap;">' + row.OpeningDebit + '</td>\
//                <td style="white-space: nowrap;">' + row.OpeningCredit + '</td>\
//                <td style="white-space: nowrap;">' + (parseFloat(row.OpeningCredit) - parseFloat(row.OpeningDebit)) + '</td>\
//            </tr>\
//            ';
//            })
//        }
//        $.each(transactionDetails.LedgerDetails.Table1, function (count, row) {
//            counter++;
//            overallBalance += row.credit - row.debit;
//            output +=
//                '<tr>\
//                <td style="white-space: nowrap;">'+ counter + '</td>\
//                <td style="white-space: nowrap;">' + row.date + '</td>\
//                <td style="white-space: nowrap;">' + row.voucherTypeName + '</td>\
//                <td style="white-space: nowrap;">' + row.invoiceNo + '</td>\
//                <td style="white-space: nowrap;">' + row.Memo + '</td>\
//                <td style="white-space: nowrap;">' + row.debit + '</td>\
//                <td style="white-space: nowrap;">' + row.credit + '</td>\
//                <td style="white-space: nowrap;">' + overallBalance + '</td>\
//            </tr>\
//            ';
//        });
//        $("#transactionDetailsTbody").html(output);
//        $("#totalPaymentAmt").val(overallBalance);
//}