﻿var Productist = [];
var SupplierList = [];
var CurrencyList = [];
var TaxList = [];
var UnitList = [];
var InvoiceNo = "";
var LineItems = [];
var stores = [];
var searchResult = {};
var productToAdd = {};
var taxItem = { taxId: 0, taxName: "NA", rate: 0 };
var tax = {};
var purchaseRate = 0;
var sl = 1;
var units = [];

$(function () {
    GetLookUpData();

    $("#btnLineItemModal").click(function () {
        $("#LineItemModal").modal("toggle");
    });
    $("#Barcode").change(function () {
        searchProduct("ProductCode", $("#Barcode").val());
    });
    $("#ProductCode").change(function () {
        searchProduct("ProductCode", $("#ProductCode").val());
    });
    $("#ProductName").change(function () {
        searchProduct("ProductName", $("#ProductName").val());
    });
    $("#Qty").keyup(function () {
        calculateAmountOfItemToAdd();
    });
    $("#Qty").on("change", function () {
        var rate = $("#Rate").val();
        var qty = $("#Qty").val();
        var amount = rate * qty;
        $("#Amount").val(amount);
        taxReset();
    });
    
    $("#store").on("change", function () {
        if ($("#store").val() != "") {
            $("#Qty").removeAttr("disabled");
        }
        else {
            $("#Qty").prop("disabled", "disabled");
        }
    });
    //$("#Rate").change(function () {
    //    calculateAmountOfItemToAdd();
    //});
    $("#Rate").keyup(function () {
        calculateAmountOfItemToAdd();
    });
    $("#Tax").change(function () {
        var isFound = false;
        for (i = 0; i < TaxList.length; i++) {
            if (TaxList[i].taxId == $("#Tax").val()) {
                tax = TaxList[i];
                isFound = true;
                break;
            }
        }
        if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
        {
            tax = { taxId: 0, taxName: "NA", rate: 0 };
        }
        purchaseRate = $("#Rate").val();
        calculateAmountOfItemToAdd();
    });
});

//find suppliers:
function findSuppliers(id) {
    var output = {};
    for (i = 0; i < SupplierList.length; i++) {
        if (SupplierList[i].ledgerId == id) {
            output = SupplierList[i];
            break;
        }
    }
    return output;
}




function openModal() {
    $("#LineItemModal").modal("show");
    $('#LineItemModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
}
//to get all look up data
function GetLookUpData() {
    Utilities.Loader.Show();
    var storeAjax = $.ajax({
        url: API_BASE_URL + "/SalesOrder/GetLookups",
        type: "Post",
        contentType: "application/json"
    });
    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/LookUpData1",
        type: "Get",
        contentType: "application/json"
    });
    var autoInvoiceNoAjax = $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/GetAutoVoucherNo",
        type: "Get",
        contentType: "application/json"
    });
    $.when(lookUpAjax, autoInvoiceNoAjax, storeAjax)
        .done(function (dataLookUp, dataInvoiceNo, dataStore) {
         console.log(dataLookUp);
         Productist = dataLookUp[0].Products;
         SupplierList = dataLookUp[0].Suppliers;
         CurrencyList = dataLookUp[0].Currencies;
         TaxList = dataLookUp[0].Taxes;
         UnitList = dataLookUp[0].Units
            InvoiceNo = dataInvoiceNo[2].responseJSON;
            stores = dataStore[2].responseJSON.Stores;
         //to render data to controls after successful data fetch
         RenderDatatoControls();
     });
    Utilities.Loader.Hide();
}

function onChange() {
    //alert(dropdownlistPN.value());
};
//to render data to controls
function RenderDatatoControls() {
    // $("#Supplier").html(Utilities.PopulateDropDownFromArray(SupplierList, 1, 0));
    $("#Currency").html(Utilities.PopulateDropDownFromArray(CurrencyList, 1, 0));
    $("#Tax").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(TaxList, 0, 1));
    $("#Supplier").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: SupplierList,
        optionLabel: "Please Select..."
    });
    $("#store").html("<option value=\"\">--Select--</option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
    $("#store").chosen({ width: "100%", margin: "1px" });
    //$("#Barcode").kendoDropDownList({
    //    optionLabel: "Please Select...",
    //    dataTextField: "productCode",
    //    dataValueField: "productCode",
    //    dataSource: Productist
    //});
    //$("#ProductCode").kendoDropDownList({
    //    filter: "contains",
    //    optionLabel: "Please Select...",
    //    dataTextField: "productCode",
    //    dataValueField: "productCode",
    //    dataSource: Productist
    //});
    //$("#ProductName").kendoDropDownList({
    //    filter: "contains",
    //    optionLabel: "Please Select...",
    //    dataTextField: "productName",
    //    dataValueField: "productName",
    //    dataSource: Productist,
    //    //change : onChange
    //});
    //var dropdownlistPN = $("#ProductName").data("kendoDropDownList");


    if (Productist.length > 0) {


        var productNameHtml = "";
        var productCodeHtml = "";
        var barCodeHtml = "";

        $.each(Productist, function (count, record) {
            if (record.IsInRightYear == true) {
                productNameHtml += '<option value="' + record.ProductName + '">' + record.ProductName + '</option>';
                productCodeHtml += '<option value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                barCodeHtml += '<option value="' + record.barcode + '">' + record.barcode + '</option>';
            }
            else {
                productNameHtml += '<option disabled="disabled" value="' + record.ProductName + '">' + record.ProductName + '( This product is not active because it was created in higher financial year )</option>';
                productCodeHtml += '<option disabled="disabled" value="' + record.ProductCode + '">' + record.ProductCode + '( This product is not active because it was created in higher financial year )</option>';
                barCodeHtml += '<option disabled="disabled" value="' + record.barcode + '">' + record.barcode + '( This product is not active because it was created in higher financial year )</option>';
            }
        });

        $("#ProductName").html(productNameHtml);
        $("#ProductCode").html(productCodeHtml);
        $("#Barcode").html(barCodeHtml);


        $("#ProductName").chosen({ width: "100%", margin: "1px" });
        $("#Barcode").chosen({ width: "100%", margin: "1px" });
        $("#ProductCode").chosen({ width: "100%", margin: "1px" });
    }


    //$("#Barcode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(Productist, 0, 0));
    //$("#ProductCode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(Productist, 1, 1));
    //$("#ProductName").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(Productist, 2, 2));






    $("#Unit").html(Utilities.PopulateDropDownFromArray(UnitList, 1, 2));
    $("#QuotationNo").val(InvoiceNo);

    $("#Barcode").chosen({ width: "100%", margin: "1px" });
    $("#ProductCode").chosen({ width: "100%", margin: "1px" });
    $("#ProductName").chosen({ width: "100%", margin: "1px" });

    currentDate();
    $("#DueDate").change(function () {
        var date1 = new Date($("#TransDate").val());
        var date2 = new Date($("#DueDate").val());
        var timeDiff = Math.abs(date2.getTime() - date1.getTime());
        var diffDays = Math.ceil(timeDiff / (1000 * 3600 * 24));
        $("#DueDays").val(diffDays);
    });
}
//search for product
function searchProduct(searchBy, filter) {
    //if (searchBy == "ProductCode")
    //{
    //    for (i = 0; i < Productist.length; i++)
    //    {
    //        if(Productist[i].productCode == filter)
    //        {
    //            var dropdownlistBarcode = $("#Barcode").data("kendoDropDownList");
    //            var dropdownlistProductCode = $("#ProductCode").data("kendoDropDownList");
    //            var dropdownlistProductName = $("#ProductName").data("kendoDropDownList");

    //            dropdownlistBarcode.value(Productist[i].productCode);
    //            dropdownlistProductCode.value(Productist[i].productCode);
    //            dropdownlistProductName.value(Productist[i].productName);
    //            $("#Rate").val(Productist[i].PurchaseRate);
    //            $("#Description").val(Productist[i].Narration)
    //            $("#Unit").val(uni.unitName);
    //        }
    //    }
    //}
    //return;

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/SearchProduct?filter=" + filter + "&searchBy=" + searchBy,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            searchResult = {};
            searchResult = data.Product;
            console.log(searchResult)
            purchaseRate = searchResult.PurchaseRate
           var uni = data.Unit[0];
            $("#Rate").val(searchResult.PurchaseRate);
            $("#Description").val(searchResult.Narration)
            $("#Unit").val(uni.unitName);

            //var dropdownlistBarcode = $("#Barcode").data("kendoDropDownList");
            //var dropdownlistProductCode = $("#ProductCode").data("kendoDropDownList");
            //var dropdownlistProductName = $("#ProductName").data("kendoDropDownList");

            ////alert(dropdownlistBarcode.value());

            //dropdownlistBarcode.value(searchResult.ProductCode);
            //dropdownlistProductCode.value(searchResult.ProductCode);
            //dropdownlistProductName.value(searchResult.ProductName);

            $("#Barcode").val(searchResult.ProductCode);
            $("#Barcode").trigger("chosen:updated");
            $("#ProductCode").val(searchResult.ProductCode);
            $("#ProductCode").trigger("chosen:updated");
            $("#ProductName").val(searchResult.ProductName);
            $("#ProductName").trigger("chosen:updated");

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}
//to add object to line item
function AddtoLineItem() {

    if ($("#Qty").val() == "0" || $("#Qty").val() == "") {
        Utilities.ErrorNotification("The quantity field cannot be empty.");
        return false;
    }
    if ($("#store").val() == "0" || $("#store").val() == "") {
        Utilities.ErrorNotification("Select Store");
        return false;
    }
    var a = $("#Amount").val();
    var amt = a.replace(/,/g, "");
    var amount = Number.parseFloat(amt);
    var t = $("#TaxAmount").val();
    var tamt = t.replace(/,/g, "");
    var tamount = Number.parseFloat(tamt);

    LineItems.push({
        slno: sl,
        Barcode: $("#Barcode").val(),
        ProductCode: $("#ProductCode").val(),
        ProductName: $("#ProductName").val(),
        Description: $("#Description").val(),
        ProductId: searchResult.ProductId,
        Qty: $("#Qty").val(),
        Unit: $("#Unit").val(),
        Extra1: $("#store").val(),
        Rate: $("#Rate").val(),
        Amount: amount,
        TaxId: $("#Tax").val(),
        tAmount: tamount
    });
    sl = sl + 1;
    RenderLineItem();
    clearLineItemForm();
}
//to render line item
function RenderLineItem() {
    console.log(LineItems);
    //Utilities.ShowStockCardDialog($("#stockCardProducts").val());
    var output = "";
    $.each(LineItems, function (count, row) {
        var prod = row;
        //if(prod.Barcode == undefined)
        //{
        //    prod.Barcode = "";
        //}
        console.log(prod);
        if (prod.tAmount == null) {
            prod.tAmount = 0;
        }
        output += '<tr>\
                    <td style="white-space: nowrap;"><button onclick="RemoveFromLineItem(' + prod.ProductId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>\
                    <td><button onclick="Utilities.ShowStockCardDialog(' + prod.ProductId + ')" class="btn btn-sm btn-primary"><i class="fa fa-info"></i></button></td>\
                    <td>'+ (count + 1) + '</td>\
                    <td>' + ((prod.Barcode == undefined) ? "-" : prod.Barcode) + '</td>\
                    <td>' + prod.ProductCode + '</td>\
                    <td>' + prod.ProductName + '</td>\
                    <td>' + prod.Description + '</td>\
                    <td>' + prod.Unit + '</td>\
                    <td>'+ findStore(prod.Extra1).godownName + '</td>\
                    <td>' + prod.Qty + '</td>\
                    <td>' + Utilities.FormatCurrency(prod.Rate) + '</td>\
                    <td>' + Utilities.FormatCurrency(prod.Amount) + '</td>\
                  </tr>';
    });

    $("#OrderLineItemTbody").html(output);
    $("#billAmount").val(Utilities.FormatCurrency(getTotalAmount()));
    $("#taxAmount").val(Utilities.FormatCurrency(getTotalTax()));
    $("#grandTotal").val(Utilities.FormatCurrency(getTotalAmount() + getTotalTax()));
}
//remove item obj from line item
function RemoveFromLineItem(productId) {
    console.log(LineItems);
    if (confirm("Remove this item?")) {
        var indexOfObjectToRemove = LineItems.findIndex(p=>p.ProductId == productId);
        LineItems.splice(indexOfObjectToRemove, 1);
        RenderLineItem();
    }
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function taxReset() {
    var qty = $("#Qty").val();
    var rate = $("#Rate").val();
    var gross = (qty * rate);
    var discount = 0; //discount percent
    var netAmount = gross - (discount / 100.00);
    //var taxAmount = netAmount * tax.find(p => p.taxId == parseInt($("#tax").val())).rate / 100;
    var taxAmount = netAmount * taxItem.rate / 100;
    $("#TaxAmount").val(taxAmount);
}

//save New Purchase order
$("#btnSave, #savePrint").click(savePurchaseOrder);

function savePurchaseOrder() {
    var myId = $(this).attr('id');
    if ($("#Supplier").val() == 0)
    {
        Utilities.ErrorNotification("Please Select an Agent");
    }
    else
    {
        SalesQuotationObj =
        {
            SiNo: "",
            OrderNo: $("#QuotationNo").val(),
            Supplier: $("#Supplier").val(),
            TransDate: $("#TransDate").val(),
            DueDate: $("#DueDate").val(),
            DueDays: $("#DueDays").val(),
            Currency: $("#Currency").val(),
            Narration: $("#Narration").val(),
            TotalAmount: $("#billAmount").val(),
            //DoneBy: 'dami', 
            DoneBy: $("#UserId").html(), 
            LineItems: LineItems

        };
        console.log(SalesQuotationObj);
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/PurchaseOrder/SavePurchaseOrder",
            type: 'POST',
            data: JSON.stringify(SalesQuotationObj),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if(data == true){
                    Utilities.SuccessNotification("Purchase Order is saved!");
                    if (myId == "savePrint") {
                        print(SalesQuotationObj.LineItems);
                    }
                    clearForm();
                    RenderLineItem();
                    GetLookUpData();
                }
                else{
                    Utilities.ErrorNotification("Something went wrong!");
                }
                hideLoader();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    }
  
}
//to get current date
function currentDate() {
    var today = new Date();
    var dd = today.getDate();
    var mm = today.getMonth() + 1; //January is 0!

    var yyyy = today.getFullYear();
    if (dd < 10) {
        dd = '0' + dd;
    }
    if (mm < 10) {
        mm = '0' + mm;
    }
    var todayDate = yyyy + '-' + mm + '-' + dd;
    $("#TransDate").val(todayDate);
}
//
function calculateAmountOfItemToAdd() {
    if (tax.rate == undefined) {
        tax.rate = 0;
    }
    var r = $("#Rate").val();
    var rate = r.replace(/,/g, "");

    var qty = $("#Qty").val();
    var amount = rate * qty;
    var taxAmount = amount * (tax.rate / 100.0);

    $("#Amount").val(Utilities.FormatCurrency(amount));
    $("#TaxAmount").val(Utilities.FormatCurrency(taxAmount));
}
function getTotalAmount() {
    var amt = 0.0;
    for (i = 0; i < LineItems.length; i++) {
        amt = amt + parseFloat(LineItems[i].Amount);
    }
    return amt;
}
function getTotalTax() {
    var amt = 0.0;
    for (i = 0; i < LineItems.length; i++) {
        amt = amt + parseFloat(LineItems[i].tAmount);
    }
    return amt;
}
function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}
function clearLineItemForm() {
    $("#Barcode").val("");
    $("#Barcode").trigger("chosen:updated");
    $("#ProductCode").val("");
    $("#ProductCode").trigger("chosen:updated");
    $("#ProductName").val("");
    $("#ProductName").trigger("chosen:updated");
    $("#Description").val("");
    $("#Qty").val("");
    $("#store").val("");
    $("#Unit").val("");
    $("#Rate").val("");
    $("#Amount").val("");
    $("#Tax").val("");
    $("#TaxAmount").val("");
}
function clearForm() {
   // $("#DueDate").val("");
    $("#DueDays").val("");
    $("#Currency").val("");
    $("#Narration").val("");
    $("#billAmount").val("");
    $("#taxAmount").val("");
    $("#grandTotal").val("");
    LineItems = [];
}



//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;


//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}




function print(LineItems) {
    var objToPrint = [];
    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();
    var thisEmail = $("#companyEmail").val();
    var thisPhone = $("#companyPhone").val();

    var Narration = $("#Narration").val() == "" ? 'N/A' : $("#Narration").val();

    var thisOrderNo = $("#QuotationNo").val();
    var thisDate = $("#TransDate").val();
    //var thisCurency = $("#Currency").val();
    var totalAmt = $("#grandTotal").val();
    var thisSupplier = findSuppliers($("#Supplier").val()).ledgerName;

  

    $.each(LineItems, function (count, row) {
        objToPrint.push({
            "SlNo": count + 1,
            "Barcode": ((row.Barcode == undefined) ? "-" : (row.Barcode)),
            "ProducCode": row.ProductCode,
            "ProductName": row.ProductName,
            "Qty": row.Qty,
            "UnitName": row.Unit,
            "Rate": 'N' + Utilities.FormatCurrency(parseInt(row.Rate)),
            "Amount": 'N' + Utilities.FormatCurrency(row.Amount),
            "Descptn": row.itemDescription == null ? "N/A" : row.Description
        });
    });

    columns = [
        { title: "S/N", dataKey: "SlNo" },
        { title: "Barcode", dataKey: "Barcode" },
        { title: "Product Code", dataKey: "ProducCode" },
        { title: "Product Name", dataKey: "ProductName" },
        { title: "Qty", dataKey: "Qty" },
        { title: "Unit", dataKey: "UnitName" },
        { title: "Rate", dataKey: "Rate" },
        { title: "Description", dataKey: "Descptn" },
        { title: "Amount", dataKey: "Amount" },
    ];

    var doc = new jsPDF('portrait', 'pt', 'letter');
    doc.setDrawColor(0);

    //start drawing

    doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text(companyName, 320, 80, 'center');
    doc.setFontSize(9);
    doc.setFontType("normal");
    doc.text(companyAddress, 320, 90, 'center');

    

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Email Id: ', 400, 150);
    doc.setFontType("normal");
    doc.text(thisEmail, 460, 150);
    doc.setFontType("bold");
    doc.text('Phone: ', 400, 165);
    doc.setFontType("normal");
    doc.text(thisPhone, 460, 165);

    doc.line(40, 170, 570, 170);

    doc.setFontType("bold");
    doc.setFontSize(9);
    doc.text('Date:', 40, 185);
    doc.setFontType("normal");
    doc.text(Utilities.FormatJsonDate(thisDate), 85, 185);
    doc.setFontType("bold");
    doc.text('Narration:', 40, 205);
    doc.setFontType("normal");
    doc.text(Narration, 85, 205);

    doc.setFontType("bold");
    doc.text('Order No: ', 400, 185);
    doc.setFontType("normal");
    doc.text(thisOrderNo, 463, 185);
    doc.setFontType("bold");
    doc.text('Supplier: ', 400, 205);
    doc.setFontType("normal");
    doc.text(thisSupplier, 463, 205);

    doc.line(40, 220, 570, 220);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text('Purchase Order Details', 240, 240);

    //doc.line(120, 20, 120, 60); horizontal line
    doc.setFontSize(8);
    doc.autoTable(columns, objToPrint, {
        startY: 255,
        theme: 'striped',
        styles: {
            fontSize: 8,
        },
        columnStyles: {
            SlNo: { columnWidth: 30, },
            Amount: { columnWidth: 80, halign: 'right' },
        },
    });

    doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Total Amount: NGN' + totalAmt, 555, doc.autoTable.previous.finalY + 20, "right");

    doc.line(40, doc.autoTable.previous.finalY + 30, 570, doc.autoTable.previous.finalY + 30);

    doc.setFontType("bold");
    doc.text('Amount in Words: ', 40, doc.autoTable.previous.finalY + 60);
    doc.setFontType("normal");


    //remove commas
    var tempTotal = totalAmt.replace(/,/g, "");
    //convert to number
    var amtToBeConverted = parseInt(tempTotal);

    //capitalize Each Word
    var wordFormat = Utilities.NumberToWords(amtToBeConverted);
    const words = wordFormat.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());

    doc.text(words + ' Naira ONLY.', 120, doc.autoTable.previous.finalY + 60);

    doc.setFontType("bold");
    doc.text('Order approved by:  _________________________________', 40, doc.autoTable.previous.finalY + 100);

    doc.text('Done by:  ' + $("#nameSpan").html(), 350, doc.autoTable.previous.finalY + 100);
    doc.text('_________________', 390, doc.autoTable.previous.finalY + 103);
    doc.autoPrint();
    doc.save('Purchase_Order_Details.pdf');
    //window.open(doc.output('bloburl'));
    window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
}