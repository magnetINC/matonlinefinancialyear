﻿var count = 0;
var lineItemCount = 0;
var additionalCost = [];
var lineItems = [];
var totalLineItemAmount = 0.0;
var formNo = "";

var suppliers = [];
var accountLedgers = [];
var currencies = [];
var products = [];
var orderNumbers = [];
var receiptNumbers = [];
var purchaseOrderVoucherType = [{1:10, 2:"Purchase Order"}];
var materialReceiptVoucherType =[ {1:11, 2:"Material Receipt"}];
var purchaseInvoiceLineItems = [];
var units = [];
var batches = [];
var stores = [];
var racks = [];
var tax = {};
var taxes = [];
var totalAdditionalCost = 0.0;
var receiptMasterId = 0;
var orderMasterId = 0;
var details = [];
var productToEdit = 0;
var grossValue = 0;
var netValue = 0;
var itemToEditIndex;
var itemToEdit;
var deleteIndex;
var productBatches = [];
var resetBatch = true;
var resetTaxVal = true;
var projects = [];
var categories = [];

$(function () {

    getLookUps();
   // GetRecords();
    $("#applyOn").attr("disabled", "disabled");
    $("#orderNo").attr("disabled", "disabled");

    $("#purchaseMode").change(function ()
    {
        populateOrderNumbersOrReceiptNumbers($(this).val());
    });

    $("#suppliers").change(function ()
    {
        populateOrderNumbersOrReceiptNumbers($("#purchaseMode").val());
    });

    $("#searchByProductName").on("change", function () {
        resetBatch = true;
        resetTaxVal = true;
        searchProduct($("#searchByProductName").val(), "ProductName");
    });
    $("#searchByProductCode").on("change", function () {
        resetBatch = true;
        resetTaxVal = true;
        searchProduct($("#searchByProductCode").val(), "ProductCode");
    });
    $("#searchByBarcode").on("change", function () {
        resetBatch = true;
        resetTaxVal = true;
        searchProduct($("#searchByBarcode").val(), "Barcode");
    });

    $("#orderNo").change(function () {
        if ($("#purchaseMode").val() == "Against Purchase Order")
        {
            orderMasterId = $("#orderNo").val();
            var invoiceNo = $("#orderNo :selected").text();
            if (invoiceNo != "") {
                Utilities.Loader.Show();
                $.ajax({
                    url: API_BASE_URL + "/PurchaseInvoice/GetPurchaseInvoiceLineItemFromPurchaseOrder?orderMasterId=" + orderMasterId,
                    type: "GET",
                    contentType: "application/json",
                    success: function (data) {
                        var sl = 1;
                        console.log(data);
                        //return;
                        details = data;
                        console.log(details);
                        purchaseInvoiceLineItems = []; //clear array
                        $.each(details, function (count, row) {
                            var netAmount = row.grossValue - (row.discount / 100.00);
                            var taxAmount = 0;

                            if (row.taxId == 0) {
                                taxAmount = 0;
                            } else {
                                tax = taxes.find(p => row.taxId == productDetails.taxId);
                                taxAmount = netAmount * (tax.Rate / 100.0).toFixed(2);
                            }
                            var amount = taxAmount + netAmount;
                            purchaseInvoiceLineItems.push({
                                SLNo: sl,
                                ProductId: row.productId,
                                ProductBarcode: findProduct(row.productId).barcode,
                                ProductCode: findProduct(row.productId).ProductCode,
                                ProductName: findProduct(row.productId).ProductName,
                                itemDescription: row.itemDescription,
                                Brand: row.brandName,
                                Qty: row.qty,
                                UnitId: row.unitId,
                                UnitConversionId: row.unitConversionId,
                                GodownId: row.godownId,
                                ProjectId: row.ProjectId,
                                CategoryId: row.CategoryId,
                                StoreId: 1,
                                RackId: 1,
                                BatchId: 1,
                                GrossAmount: row.grossValue,
                                Discount: row.discount,
                                NetAmount: netAmount,
                                TaxId: row.taxId,
                                Tax: findTax(row.taxId).taxName,
                                TaxAmount: taxAmount,
                                Rate: row.rate,
                                Amount: amount,
                                ReceiptDetailsId: row.materialReceiptDetailsId,
                                OrderDetailsId: row.purchaseOrderDetailsId,
                            });
                        });
                        sl += 1;
                        renderLineItems();
                        Utilities.Loader.Hide();
                    },
                    error: function () {
                        Utilities.Loader.Hide();
                    }
                });
            }
        }
        else if ($("#purchaseMode").val() == "Against Material Receipt")
        {
            receiptMasterId = $("#orderNo").val();
            var invoiceNo = $("#orderNo :selected").text();
            if (invoiceNo != "") {
                Utilities.Loader.Show();
                $.ajax({
                    url: API_BASE_URL + "/PurchaseInvoice/GetPurchaseInvoicetLineItemFromMaterialReceipt?receiptMasterId=" + receiptMasterId,
                    type: "GET",
                    contentType: "application/json",
                    success: function (data) {
                        var sl = 1;
                        console.log(data);
                        //return;
                        details = data;
                        purchaseInvoiceLineItems = []; //clear array
                        $.each(details, function (count, row) {
                            var netAmount = row.grossValue - (row.discount / 100.00);
                            var taxAmount = 0;
                            
                            if(row.taxId == 0){
                                taxAmount = 0;
                            } else {
                                tax = taxes.find(p => row.taxId == productDetails.taxId);
                                taxAmount = netAmount * (tax.Rate / 100.0).toFixed(2);
                            }
                            var amount = taxAmount + netAmount;
                            purchaseInvoiceLineItems.push({
                                SLNo: sl,
                                ProductId: row.productId,
                                ProductBarcode: findProduct(row.productId).barcode,
                                ProductCode: findProduct(row.productId).ProductCode,
                                ProductName: findProduct(row.productId).ProductName,
                                itemDescription: row.itemDescription,
                                Brand: row.brandName,
                                Qty: row.qty,
                                UnitId: row.unitId,
                                UnitConversionId: row.unitConversionId,
                                GodownId: row.godownId,
                                ProjectId: row.ProjectId,
                                CategoryId: row.CategoryId,
                                StoreId: 1,
                                RackId: 1,
                                BatchId: 1,
                                GrossAmount: row.grossValue,
                                Discount: row.discount,
                                NetAmount: netAmount,
                                TaxId: row.taxId,
                                Tax: findTax(row.taxId).taxName,
                                TaxAmount: taxAmount,
                                Rate: row.rate,
                                Amount: amount,
                                ReceiptDetailsId: row.materialReceiptDetailsId,
                                OrderDetailsId: row.purchaseOrderDetailsId,
                            });
                        });
                        sl += 1;
                        renderLineItems();
                        Utilities.Loader.Hide();
                    },
                    error: function () {
                        Utilities.Loader.Hide();
                    }
                });
            }
        }
        

    });

    $("#quantityToAdd").on("change", function () {
        calculateValues();
    });
    $("#rate").on("change", function () {
        calculateValues();
    });
    $("#Tax").on("change", function () {
        resetTax();
        calculateValues();
    });

    var sl = 1;
    $("#addLineItem").click(function () {
        if ($("#quantityToAdd").val() == "" || ($("#quantityToAdd").val() == 0)) {
            Utilities.ErrorNotification("You need add a quantity!");
            return false;
        }

        if ($("#store").val() == "") {
            Utilities.ErrorNotification("You need to select a store!");
            return false;
        }
        var batch = $("#batch").val();
        if (batch== "" || batch== null || batch == undefined ) {
            Utilities.ErrorNotification("select a batch, , if you cant find a batch, create a batch for this product ");
            return false;
        }

        console.log(searchResult);
        var qty = $("#quantityToAdd").val();
        var rate = $("#rate").val();
        var gross = Utilities.FormatToNumber($("#grossValue").val());
        var discount = $("#discount").val(); 
        var percentDiscount = $("#percentDiscount").val(); //discount percent
        var netAmount = Utilities.FormatToNumber($("#netValue").val());
        var taxAmount = $("#TaxAmount").val();//netAmount * (tax.rate / 100.0).toFixed(2);
        var amount = Utilities.FormatToNumber($("#amount").val());
        //var batchId = $("#batch").val();

        purchaseInvoiceLineItems.push({
            SlNo: sl,
            ProductId: searchResult.productId,
            ProductBarcode: searchResult.productCode,
            ProductCode: searchResult.productCode,
            ProductName: searchResult.productName,
            itemDescription: searchResult.narration,
            Brand: searchResult.brandName,
            Qty: qty,
            UnitId: searchResult.unitId,
            UnitConversionId: searchResult.unitConversionId,
            GodownId: $("#store").val(),
            RackId: searchResult.rackId,
            StoreId: $("#store").val(),
            //BatchId: searchResult.batchId,
            BatchId: $("#batch").val(),
            GrossAmount: gross,
            Discount: discount,
            PercentDiscount:percentDiscount,
            NetAmount: netAmount,
            ProjectId: $("#project").val(),
            CategoryId: $("#category").val(),
            TaxId: $("#Tax").val(),
            Tax: searchResult.taxId == $("#Tax").val() ? tax.taxName : "NA",
            TaxAmount: taxAmount,
            Rate: rate,
            Amount: amount
        });
        sl += 1;
        
        $("#searchByBarcode").val("");
        $("#searchByBarcode").trigger("chosen:updated");
        $("#searchByProductCode").val("");
        $("#searchByProductCode").trigger("chosen:updated");
        $("#searchByProductName").val("");
        $("#searchByProductName").trigger("chosen:updated");
        $("#store").val("");
        $("#store").trigger("chosen:updated");
        $("#project").val("");
        $("#project").trigger("chosen:updated");
        $("#category").val("");
        $("#category").trigger("chosen:updated");
        //$("#tax").val("0"); 
        $("#TaxAmount").val("0");
        $("#quantityToAdd").val("");
        $("#quantityInStock").val("");
        $("#amount").val(0);
        $("#rate").val(0);
        $("#description").val("");
        $("#unit").val("");
        $("#discount").val(0);
        $("#percentDiscount").val(0);
        $("#grossValue").val("0");
        $("#netValue").val("0");
        $("#discount").val(0); 
        $("#batch").val("");
        $("#storeQuantityInStock").val("");
        renderLineItems();
    });

    $("#applyTax").change(function () {
        var isFound = false;
        for (i = 0; i < taxes.length; i++) {
            if (taxes[i].taxId == $("#applyTax").val()) {
                tax = taxes[i];
                isFound = true;
                break;
            }
        }
        if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
        {
            tax = { taxId: 0, taxName: "NA", rate: 0 };
        }
        else
        {
            for (i = 0; i < purchaseInvoiceLineItems.length; i++) {
                if (purchaseInvoiceLineItems[i].ProductId == productToEdit) {
                    var rate = purchaseInvoiceLineItems[i].Rate;
                    var qty = purchaseInvoiceLineItems[i].Qty;
                    var amount = rate * qty;
                    var discount = 0; //discount percent
                    var netAmount = amount - (discount / 100.00);
                    var taxAmount = netAmount * (tax.rate / 100.0).toFixed(2);

                    console.log("I'm getting here = " + taxAmount);

                    purchaseInvoiceLineItems[i].TaxId = $("#applyTax").val();
                    purchaseInvoiceLineItems[i].TaxAmount = taxAmount;
                    purchaseInvoiceLineItems[i].Tax = $("#applyTax option:selected").text();
                }

            }
            $("#applyTaxAmount").val(taxAmount);
        }
    });

    $("#save").click(function () {
        save();
    });
});

function getLookUps()
{
    Utilities.Loader.Show();
    var projectLookupAjax = $.ajax({
        url: API_BASE_URL + "/Project/GetProject",
        type: "GET",
        contentType: "application/json",
    });

    var categoryLookupAjax = $.ajax({
        url: API_BASE_URL + "/Category/GetCategory",
        type: "GET",
        contentType: "application/json",
    });
    var productsAjax = $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: "Get",
        contentType:"application/json"
    });

    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/PurchaseInvoice/InvoiceLookUps",
        type: "Get",
        contentType: "application/json"
    });

    var autoFormNoAjax = $.ajax({
        url: API_BASE_URL + "/PurchaseInvoice/GetAutoFormNo",
        type: "Get",
        contentType: "application/json"
    });

    $.when(productsAjax, lookUpAjax, autoFormNoAjax, projectLookupAjax, categoryLookupAjax)
        .done(function (dataProducts, dataLookUps, dataFormNo, dataProject, dataCategory) {
        products = dataProducts[2].responseJSON;
        suppliers = dataLookUps[2].responseJSON.Suppliers;
        currencies = dataLookUps[2].responseJSON.Currencies;
        units = dataLookUps[2].responseJSON.Units;
        stores = dataLookUps[2].responseJSON.Stores;
        racks = dataLookUps[2].responseJSON.Racks;
        batches = dataLookUps[2].responseJSON.Batches;
        projects = dataProject[0];
        categories = dataCategory[0];
        //tax = dataLookUps[2].responseJSON.Tax;
        taxes = dataLookUps[2].responseJSON.Taxes;
        accountLedgers = dataLookUps[2].responseJSON.AccountLedgers;
        formNo = dataFormNo[0]
            console.log("projects", projects);
            console.log(tax);

        $("#taxAmountHtml").html(tax.TaxName);
        //$("#suppliers").html(Utilities.PopulateDropDownFromArray(suppliers, 1, 0));
        $("#suppliers").kendoDropDownList({
            filter: "contains",
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: suppliers,
            optionLabel: "Please Select..."
        });

        var ledgerId = $("#ledgerId").val();
        $("#suppliers").data('kendoDropDownList').value(ledgerId);

        $("#Tax").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(taxes, 0, 1));
        $("#applyTax").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(taxes, 0, 1));
        $("#currency").html(Utilities.PopulateDropDownFromArray(currencies, 1, 0));
        $("#searchByProductName").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 3, 3));
        $("#searchByBarcode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 0, 0));
        $("#searchByProductCode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 2, 2));
        $("#store").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
        $("#Tax").chosen({ width: "100%", margin: "1px" });
        $("#searchByProductName").chosen({ width: "100%", margin: "1px" });
        $("#searchByBarcode").chosen({ width: "100%", margin: "1px" });
        $("#searchByProductCode").chosen({ width: "100%", margin: "1px" });
        $("#store").chosen({ width: "100%", margin: "1px" });
        $("#voucherNo").val(formNo);
        $("#project").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(projects, 0, 1));
        $("#project").chosen({ width: "100%", margin: "1px" });
        $("#category").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(categories, 0, 1));
        $("#category").chosen({ width: "100%", margin: "1px" });
       
        renderAdditionalCostGrid();
          
        Utilities.Loader.Hide();
            GetRecords();
            if (master.voucherNo != undefined) {
                document.getElementById("voucherNo").value = master.voucherNo;
            }
            
    });
}

var master = {};

function formatDate(st)
{
    let str = new Date(st);
    let day = str.getDate();
    let mnth = str.getMonth() + 1;
    let year = str.getFullYear();
    return `${mnth}/${day}/${year}`;
}

function GetRecords()
{
    var id = $("#masterId").val();

    $.ajax(API_BASE_URL + "/PurchaseInvoice/GetDetails/" + id,
        {
            success: function (res)
            {
                master = res.master;
                document.getElementById("transactionDate").value = formatDate(master.date);
                document.getElementById("invoiceDate").value = formatDate(master.vendorInvoiceDate);
                document.getElementById("invoiceNo").value = master.vendorInvoiceNo;
                
                purchaseInvoiceLineItems = [];
                var sl = 0;
                for (var i of res.details)
                {
                    sl += 1;
                    purchaseInvoiceLineItems.push({
                        SLNo: sl,
                        ProductId: i.product.productId,
                        ProductBarcode: i.product.productCode,
                        ProductCode: i.product.productCode,
                        ProductName: i.product.productName,
                        itemDescription: i.product.narration,
                        Brand: "",
                        Qty: i.detail.qty,
                        UnitId: i.detail.unitId,
                        UnitConversionId: i.detail.unitConversionId,
                        GodownId: i.detail.godownId,
                        ProjectId: i.detail.ProjectId,
                        CategoryId: i.detail.CategoryId,
                        StoreId: i.detail.godownId,
                        RackId: i.detail.rackId,
                        BatchId: i.detail.batchId,
                        GrossAmount: i.detail.grossAmount,
                        Discount: i.detail.discount,
                        NetAmount: i.detail.netAmount,
                        TaxId: i.detail.taxId,
                        Tax: findTax(i.detail.taxId).taxName,
                        TaxAmount: i.detail.taxAmount,
                        Rate: i.detail.rate,
                        Amount: i.detail.amount,
                        ReceiptDetailsId: i.detail.materialReceiptDetailsId,
                        OrderDetailsId: i.detail.purchaseOrderDetailsId,
                        PurchaseDetailsId: i.detail.purchaseDetailsId
                    });
                }


               
                renderLineItems();
            }
        });
}

function renderAdditionalCostGrid() {
    $("#additionalCostGrid").kendoGrid({
        dataSource: {
            transport: {
                read: function (entries) {
                    entries.success(additionalCost);
                },
                create: function (entries) {
                    entries.success(entries.data);
                },
                update: function (entries) {
                    entries.success();
                },
                destroy: function (entries) {
                    entries.success();
                },
                parameterMap: function (data) { return JSON.stringify(data); }
            },
            schema: {
                model: {
                    id: "ledgerId",
                    fields: {
                        ledgerId: { editable: true, validation: { required: true } },
                        amount: { editable: true, validation: { required: true, type: "number" } },
                    }
                }
            },
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 5,
            pageSizes: [5, 10, 20, 50, 100],
            previousNext: true,
            buttonCount: 5,
        },
        toolbar: [{ name: 'create', text: 'Add Cost' }],
        columns: [
            //{title:"#",template:"#= ++count #"},
            { field: "ledgerId", title: "Ac/ Ledger", editor: accountLedgerDropDownEditor, template: "#= getAccountLedger(ledgerId) #" },
            { field: "amount", title: "Amount" },
            {
                command: [{ name: 'edit', text: {edit:"",update:"",cancel:""} }, { name: 'destroy', text: '' }]
            }],
        editable: "inline",
        save: function (e) {
            var datasource = $("#additionalCostGrid").data().kendoGrid.dataSource.view();
            getTotalAdditionalCost(datasource);
            console.log(datasource);
        },
        remove:function(e)
        {
            var datasource = $("#additionalCostGrid").data().kendoGrid.dataSource.view();
            var indexToRemove = getKendoLineItemIndex(datasource, e.model);
            datasource.splice(indexToRemove, 1);
            getTotalAdditionalCost(datasource);
        }
    });

}

function viewDetailsClick(e) {
    var tr = $(e.target).closest("tr"); // get the current table row (tr)
    // get the data bound to the current table row
    var data = this.dataItem(tr);//userid
    console.log(data);
}

function getOrderNumbers(supplierId, voucherTypeId)
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseInvoice/GetPurchaseOrderNumbers?supplierId=" + supplierId + "&voucherTypeId="+voucherTypeId,
        type: "Get",
        contentType: "application/json",
        success:function(data)
        {
            $("#orderNo").html(Utilities.PopulateDropDownFromArray(data,0,1));
            Utilities.Loader.Hide();
        },
        error:function(err)
        {
            Utilities.Loader.Hide();
        }
    });
}

function getReceiptNumbers(supplierId, voucherTypeId) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseInvoice/GetMaterialReceiptNumbers?supplierId=" + supplierId + "&voucherTypeId=" + voucherTypeId,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            $("#orderNo").html(Utilities.PopulateDropDownFromArray(data, 0, 1));
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function populateOrderNumbersOrReceiptNumbers(purchaseMode)
{
    if (purchaseMode == "NA")
    {
        $("#applyOn").attr("disabled", "disabled");
        $("#orderNo").attr("disabled", "disabled");
    }
    else if (purchaseMode == "Against Purchase Order")
    {
        $("#applyOn").removeAttr("disabled");
        $("#orderNo").removeAttr("disabled");

        $("#applyOn").html(Utilities.PopulateDropDownFromArray(purchaseOrderVoucherType, 0, 1));
        getOrderNumbers($("#suppliers").val(), 10);
    }
    else if (purchaseMode == "Against Material Receipt")
    {
        $("#applyOn").removeAttr("disabled");
        $("#orderNo").removeAttr("disabled");

        $("#applyOn").html(Utilities.PopulateDropDownFromArray(materialReceiptVoucherType, 0, 1));
        getReceiptNumbers($("#suppliers").val(), 11);
    }
} 

$("#batch").change(function () {
    if($("#searchByProductName").val() == ""){
        return false;
    }
    searchProduct($("#searchByProductName").val(), "ProductName");
    resetBatch = false;
    resetTaxVal = false;
});

$("#store").change(function () {
    if ($("#searchByProductName").val() == "") {
        return false;
    }
    searchProduct($("#searchByProductName").val(), "ProductName");
    resetBatch = false;
    resetTaxVal = false;
});

function searchProduct(filter, searchBy) {
    //clearLineItems();
    var storeId = $("#store").val() == "" ? 0 : parseInt($("#store").val());
    var batchId = $("#batch").val() == "" ? 0 : parseInt( $("#batch").val());
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/SearchProduct?filter=" + filter + "&searchBy=" + searchBy + "&storeId=" + storeId + "&batchId=" + batchId,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            //console.log(tax); //return;
            var productDetails = data.Product[0];
            searchResult = productDetails;
            productBatches = batches.filter(p => p.productId == parseInt(searchResult.productId));

            tax = taxes.find(p => p.taxId == productDetails.taxId);
            var rate = parseFloat(productDetails.purchaseRate);
            var qty = parseFloat($("#quantityToAdd").val());
            var amount = rate * qty;
            var discount = parseFloat($("#discount").val());
            var netAmount = amount - (discount / 100.00);
            var taxAmount = netAmount * (tax.rate / 100.0).toFixed(2);

            if(resetBatch == true){
                populateBatch();
            }
            if (resetTaxVal == true) {
                $("#Tax").val(productDetails.taxId);
                $("#Tax").trigger("chosen:updated");
            }
            
            $("#rate").val(rate);
            $("#amount").val(amount);
            $("#description").val(productDetails.narration);
            $("#TaxAmount").val(taxAmount);
            $("#searchByBarcode").val(searchResult.productCode);
            $("#searchByBarcode").trigger("chosen:updated");
            $("#searchByProductCode").val(searchResult.productCode);
            $("#searchByProductCode").trigger("chosen:updated");
            $("#searchByProductName").val(searchResult.productName);
            $("#searchByProductName").trigger("chosen:updated");
            
            if (data.QuantityInStock > 0) {
                $("#quantityInStock").css("color", "black");
                $("#quantityInStock").val(data.QuantityInStock);
            }
            else if ($("#batch").val() != "") {
                $("#quantityInStock").css("color", "red");
                $("#quantityInStock").val("OUT OF STOCK! " + data.QuantityInStock + "STOCK");
            }
            if (data.StoreQuantityInStock > 0 ) {
                $("#storeQuantityInStock").css("color", "black");
                $("#storeQuantityInStock").val(data.StoreQuantityInStock);
            }
            else if ($("#batch").val() != "" && $("#store").val() != "") {
                $("#storeQuantityInStock").css("color", "red");
                $("#storeQuantityInStock").val("OUT OF STOCK! " + data.StoreQuantityInStock + " STOCK");
            }

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function populateBatch() {
    $("#batch").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(productBatches, 0, 1));
    $("#batch").trigger("chosen:updated");
}

function populateTax() {
    // $("#Tax").html(Utilities.PopulateDropDownFromArray([tax], 0, 1) + "<option value=1>NA</option>");
    $("#Tax").val();
    $("#Tax").trigger("chosen:updated");
}

$("#quantity").change(function () {
    resetTax();
    calculateValues();
}); 

function discountChange(discountParam, amountParam) {
    //amountParam = Utilities.FormatToNumber(amountParam);
    var discount = discountParam != "" ? parseFloat(discountParam) : 0;
    var percentDiscount = (discount * 100) / parseFloat(amountParam);
    return [discount, percentDiscount];
}

function percentDiscountChange(percentDiscountParam, amountParam) {
    if (isNaN(amountParam)) {
        amountParam = Utilities.FormatToNumber(amountParam);
    }
    var percentDiscount = percentDiscountParam != "" ? parseFloat(percentDiscountParam) : 0;
    var discount = percentDiscount / 100 * parseFloat(amountParam);
    var total = amountParam - discount;
    return [percentDiscount, discount, total];
}

$("#discount").change(function () {
    var newPercentDiscount = discountChange($("#discount").val(), ($("#grossValue").val()));
    $("#percentDiscount").val(newPercentDiscount[1].toFixed(2));
    calculateValues();
});

$("#percentDiscount").change(function () {
    var newDiscount = percentDiscountChange($("#percentDiscount").val(), $("#grossValue").val());
    $("#discount").val(newDiscount[1].toFixed(2));
    calculateValues();
});

$("#billDiscount").change(function () {
    var newPercentDiscount = discountChange($("#billDiscount").val(), totalLineItemAmount);
    var grandTotal = totalLineItemAmount - parseFloat($("#billDiscount").val());
    $("#percentBillDiscount").val(newPercentDiscount[1].toFixed(2));
    $("#grandTotal").val(grandTotal);
});

$("#percentBillDiscount").change(function () {
   var newDiscount = percentDiscountChange($("#percentBillDiscount").val(), totalLineItemAmount);
   $("#billDiscount").val(newDiscount[1].toFixed(2));
    $("#grandTotal").val(newDiscount[2]);
});

function calculateValues() {
    var grossValue = parseFloat($("#rate").val()) * parseFloat($("#quantityToAdd").val());
    var netValue = grossValue - parseFloat($("#discount").val());
    var taxAmount = netValue * (tax.rate / 100.0);
    var amount = netValue + taxAmount;
    var newPercentDiscount = discountChange($("#discount").val(), grossValue);
    $("#grossValue").val(Utilities.FormatCurrency(grossValue));
    $("#netValue").val(Utilities.FormatCurrency(netValue));
    $("#amount").val(Utilities.FormatCurrency(amount));
    $("#TaxAmount").val(Utilities.FormatCurrency(taxAmount));
    $("#percentDiscount").val(newPercentDiscount[1].toFixed(2));
}

function resetTax() {
    var isFound = false;
    for (i = 0; i < taxes.length; i++) {
        if (taxes[i].taxId == $("#Tax").val()) {
            tax = taxes[i];
            isFound = true;
            break;
        }
    }
    if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
    {
        tax = { taxId: 0, taxName: "NA", rate: 0 };
    }
    //        console.log(tax);
}

function renderLineItems() {
    totalLineItemAmount = 0.0;
    var output = "";
    console.log(purchaseInvoiceLineItems);
    $.each(purchaseInvoiceLineItems, function (count, row) {
        //var totAmount = row.NetAmount + row.TaxAmount;
        var tax = row.Tax == undefined ? "NA" : row.Tax;
        row.PercentDiscount = row.PercentDiscount == undefined ? (parseFloat(row.Discount) * 100) / parseFloat(row.Amount) : row.PercentDiscount;
        //var thisCategory = row.CategoryId == null ? "N/A" : getCategoryName(row.Category);
        //var thisProject = row.ProjectId == null ? "N/A" : getProjectName(row.Project);
        var projectObj = projects.find(p => p.ProjectId == row.ProjectId);
        var projectName = projectObj == undefined ? "NA" : projectObj.ProjectName;
        var categoryObj = categories.find(p => p.CategoryId == row.CategoryId);
        var categoryName = categoryObj == undefined ? "NA" : categoryObj.CategoryName;
        output +=
            '<tr>\
                <td style="white-space: nowrap;"><button onclick="confirmDelete(' + count + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
                <td style="white-space: nowrap;"><button onclick="applyTax(' + row.ProductId + ' )" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></button></td>\
                <td style="white-space: nowrap;"><button onclick="applyDiscount(' + count + ' )" class="btn btn-sm btn-primary"><i class="fa fa-edit"></i></button></td>\
                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductBarcode + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductCode + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductName + '</td>\
                <td style="white-space: nowrap;">'+ (row.itemDescription == null ? "" : row.itemDescription) + '</td>\
             <td style="white-space: nowrap;">' + categoryName + '</td>\
                 <td style="white-space: nowrap;">' + projectName + '</td>\
                <td style="white-space: nowrap;">' + findUnit(row.UnitId).unitName + '</td>\
                <td style="white-space: nowrap;">' + (findStore(+row.GodownId).godownName == undefined ? "NA" : findStore(+row.GodownId).godownName) + '</td>\
                <td style="white-space: nowrap;">' + (findRack(row.RackId).rackName == undefined ? "NA" : findRack(row.RackId).rackName ) + '</td>\
                <td style="white-space: nowrap;">' + (findBatch(row.BatchId).batchNo == undefined ? "NA" : findBatch(row.BatchId).batchNo) + '</td>\
                <td style="white-space: nowrap;">'+ row.Qty + '</td>\
                <td style="white-space: nowrap;">&#8358;' + row.Rate + '</td>\
                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.GrossAmount) + '</td>\
                 <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.Discount) + '</td>\
                <td style="white-space: nowrap;">' + Utilities.FormatCurrency(row.PercentDiscount) + '</td>\
                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.NetAmount) + '</td>\
                <td style="white-space: nowrap;">' + tax + '</td>\
                <td style="white-space: nowrap;">' + row.TaxAmount + '</td>\
                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.Amount) + '</td>\
            </tr>\
            ';
        // <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(totAmount) + '</td>\
        console.log(row.Amount);
        totalLineItemAmount = (totalLineItemAmount + parseFloat(row.Amount));
    });

    var discount = parseFloat($("#billDiscount").val());
    var newPercentDisount = discountChange(discount, totalLineItemAmount);
    var grandTotal = totalLineItemAmount - discount;
    $("#percentBillDiscount").val(newPercentDisount[1].toFixed(2));
    $("#grandTotal").val(grandTotal);
    $("#totalAmount").val(Utilities.FormatCurrency(totalLineItemAmount));
    //$("#grandTotal").val(Utilities.FormatCurrency((totalLineItemAmount) + parseFloat(totalAdditionalCost) + getTotalTaxAmount()));
    $("#purchaseInvoiceLineItemTbody").html(output);
    document.getElementById("voucherNo").value = master.voucherNo;
    //$("#totalTaxAmount").val(Utilities.FormatCurrency(getTotalTaxAmount()));
}

function removeLineItem(productId) {
    if (confirm("Remove this item?"))
    {
        var indexOfObjectToRemove = purchaseInvoiceLineItems.findIndex(p=>p.ProductId == productId);
        purchaseInvoiceLineItems.splice(indexOfObjectToRemove, 1);
        renderLineItems();
    }
}

function applyTax(id) {
    $("#applyTaxModal").modal("show");
    productToEdit = id;
}

function applyTaxOnObject() {
    renderLineItems();
    $("#applyTaxModal").modal("hide");
    $("#applyTaxAmount").val(0);
    document.getElementById('applyTax').selectedIndex = 0;
}

function applyDiscount(id) {
    itemToEditIndex = id;
    itemToEdit = purchaseInvoiceLineItems[id];
    $("#applyDiscountModal").modal("show");
    $("#applyDiscountAmount").val(itemToEdit.Discount);
    $("#applyPercentDiscount").val(itemToEdit.PercentDiscount);
}

$("#applyDiscountAmount").change(function () {
    var newPercentDiscount = discountChange($("#applyDiscountAmount").val(), itemToEdit.GrossAmount);
    $("#applyPercentDiscount").val(newPercentDiscount[1].toFixed(2));
});

$("#applyPercentDiscount").change(function () {
    var newDiscount = percentDiscountChange($("#applyPercentDiscount").val(), itemToEdit.GrossAmount);
    $("#applyDiscountAmount").val(newDiscount[1].toFixed(2));
});

function applyDiscountOnObject() {
    var grossValue = itemToEdit.Rate * itemToEdit.Qty;
    var netValue = grossValue - parseFloat($("#applyDiscountAmount").val());
    var tax = taxes.find(p => p.taxId == itemToEdit.TaxId);
    var taxRate = tax != undefined ? tax.rate : 0;
    var taxAmount = netValue * (taxRate / 100.0);
    var amount = netValue + taxAmount;

    itemToEdit.Discount = $("#applyDiscountAmount").val();
    itemToEdit.PercentDiscount = $("#applyPercentDiscount").val();
    itemToEdit.GrossAmount = grossValue;
    itemToEdit.NetAmount = netValue;
    itemToEdit.TaxAmount = taxAmount;
    itemToEdit.Amount = amount;
    purchaseInvoiceLineItems[itemToEditIndex] = itemToEdit;
    renderLineItems();
}

var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the positive lable as key
    //...
};

function confirmDelete(index) {
    deleteIndex = index;
    eModal.confirm(options).then(removeLineItem);
}

function removeLineItem() {
    purchaseInvoiceLineItems.splice(deleteIndex, 1);
    renderLineItems();
}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == unitId) {
            output = units[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}

function getCategoryName(id) {
    var CategoryName;
    for (i = 0; i < categories.length; i++) {
        if (categories[i].CategoryId == id) {
            CategoryName = categories[i].CategoryName;
            break;
        }
    }
    return CategoryName;
}

function getProjectName(id) {
    var ProjectName = {};
    for (i = 0; i < projects.length; i++) {
        if (projects[i].ProjectId == id) {
            ProjectName = projects[i].ProjectName;
            break;
        }
    }
    return ProjectName;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}

function findTax(taxId) {
    var output = {};
    for (i = 0; i < taxes.length; i++) {
        if (taxes[i].taxId == taxId) {
            output = taxes[i];
            break;
        }
    }
    return output;
}

function findProduct(id) {
    var product = "";
    for (i = 0; i < products.length; i++) {
        if (products[i].ProductId == id) {
            product = products[i];
            break;
        }
    }
    return product;
}

function accountLedgerDropDownEditor(container, options) {
    $('<input required name="' + options.field + '" data-text-field="ledgerName" data-value-field="ledgerId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: {
                data: accountLedgers
            },
            template: '<span>#: ledgerName #</span>',
            filter: "contains",
        });
}

function getAccountLedger(ledgerId) {
    for (i = 0; i < accountLedgers.length; i++) {
        if (accountLedgers[i].ledgerId == ledgerId) {
            return accountLedgers[i].ledgerName;
        }
    }
    return "";
}

function getTotalTaxAmount()
{
    var totalTax = 0.00;
    for(i=0;i<purchaseInvoiceLineItems.length;i++)
    {
        totalTax = totalTax + parseFloat(purchaseInvoiceLineItems[i].TaxAmount);
    }
    return totalTax;
}

function getTotalAdditionalCost(obj)
{
    var output = 0.0;
    for(i=0;i<obj.length;i++)
    {
        output = output + parseFloat(obj[i].amount);
    }
    totalAdditionalCost = output;
    renderLineItems();
}

function getKendoLineItemIndex(datasource,objectToFindIndex)    //function was created to remove lineitem from row wen its removed from
{                                                               //kendo grid
    for(i=0;i<datasource.length;i++)
    {
        if(objectToFindIndex.id==datasource[i].id)
        {
            return i;
        }
    }
    return -1;
}

function save()
{
    //perform validation in another boolean function which calls save() if it returns true
    var ledg = $("#suppliers").val();
    if (ledg == "" || ledg == undefined) {
        Utilities.Alert("Please Select a Supplier");
        return;
    }

    var purchaseMasterInfo = {
        VoucherNo: $("#voucherNo").val(),
        InvoiceNo: $("#invoiceNo").val(),
        Date: $("#transactionDate").val(),
        LedgerId: $("#suppliers").val(),
        VendorInvoiceNo: $("#invoiceNo").val(),
        VendorInvoiceDate: $("#invoiceDate").val(),
        CreditPeriod: $("#creditPeriod").val(),
        ExchangeRateId: $("#currency").val(),
        Narration: $("#narration").val(),
        AdditionalCost: totalAdditionalCost,
        BillDiscount: 0, //set later
        GrandTotal: parseFloat($("#grandTotal").val().replace(/,/g, '')),
        TotalAmount:parseFloat($("#totalAmount").val().replace(/,/g, '')) ,
        TotalTax: getTotalTaxAmount(),
        TransportationCompany: "",
        lrNo: "",
        MaterialReceiptMasterId: receiptMasterId,
        PurchaseOrderMasterId: orderMasterId
    };
    var purchaseDetails = purchaseInvoiceLineItems;
    var additionalCostInfo = [];
    var additionalCostGrid = $("#additionalCostGrid").data().kendoGrid.dataSource.view();
        for (i = 0; i < additionalCostGrid.length; i++)
        {
            additionalCostInfo.push({
                Debit: additionalCostGrid[i].amount,
                LedgerId: additionalCostGrid[i].ledgerId
            });
        }
        console.log(additionalCostGrid);
    var customProductInfo = [];
        for (i = 0; i < purchaseDetails.length; i++)
        {            
            customProductInfo.push({
                ProductInfo: { ProductCode: purchaseDetails[i].ProductCode },
                NetAmount: purchaseDetails[i].NetAmount
            });
        }
    var purchaseBillTaxInfo = [{
        TaxId: tax.taxId,
        TaxAmount:getTotalTaxAmount()
    }];

    //populate main object
    var invoiceToSave = {
        VoucherNumber: $("#voucherNo").val(),
        GrandTotal: parseFloat($("#grandTotal").val().replace(/,/g, '')), 
        TotalAdvance: 0,
        PurchaseModeText: $("#purchaseMode :selected").text(),
        PurchaseMasterInfo: purchaseMasterInfo,
        PurchaseDetails: purchaseDetails,
        AdditionalCostInfo: additionalCostInfo,
        ProductInfo: customProductInfo,
        PurchaseBillTaxInfo: purchaseBillTaxInfo,
        AdvancePayment:[]
    };
    console.log(invoiceToSave); //return;
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseInvoice/SavePurchaseInvoice",
        type: "Post",
        contentType: "application/json",
        data:JSON.stringify(invoiceToSave),
        success:function(e)
        {
            Utilities.Loader.Hide();
            Utilities.SuccessNotification("Purchase Invoice is saved!");
            window.location = "/Supplier/PurchaseInvoice/Index";
        },
        error:function(e)
        {
            Utilities.Loader.Hide();
        }
    });
}
function saveEdit() {
    master.narration = $("#narration").val();
    master.totalAmount = $("#totalAmount").val();
    master.billDiscount = $("#billDiscount").val();
    master.percentBillDiscount = $("#percentBillDiscount").val();
    master.grandTotal = $("#grandTotal").val();

    master.vendorInvoiceNo = document.getElementById("invoiceNo").value;
    master.date = document.getElementById("transactionDate").value;
    master.vendorInvoiceDate = document.getElementById("invoiceDate").value;
    master.ledgerId = document.getElementById("suppliers").value;

    var details = [];
    for (var i of purchaseInvoiceLineItems) {
        var r = {
            "purchaseDetailsId": i.PurchaseDetailsId,
            "purchaseMasterId": master.purchaseMasterId,
            "receiptDetailsId": i.ReceiptDetailsId,
            "orderDetailsId": i.OrderDetailsId,
            "productId": i.ProductId,
            "qty": i.Qty,
            "rate": i.Rate,
            "unitId": i.UnitId,
            "unitConversionId": i.UnitConversionId,
            "discount": i.Discount,
            "taxId": i.TaxId,
            "batchId": i.BatchId,
            "godownId": i.GodownId,
            "projectId": i.ProjectId,
            "categoryId": i.CategoryId,
            "rackId": i.RackId,
            "taxAmount": i.TaxAmount,
            "grossAmount": i.GrossAmount,
            "netAmount": i.NetAmount,
            "amount": i.Amount,
            "slNo": i.SLNo,
            "itemDescription": i.itemDescription
        }
        details.push(r);
    }

    Utilities.Loader.Show();
    $.ajax(API_BASE_URL + "/PurchaseInvoice/Edit",
        {
            type: "post",
            data: {
                PurchaseMaster: master,
                PurchaseDetails: details
            },
            success: function(res) {

                Utilities.Loader.Hide();
                Utilities.Alert(res.message);
                location.href = "/Supplier/PurchaseInvoice/Register";
            },
            error: function () {
                Utilities.Loader.Hide();
            }
        });

}
function clear()
{
    $("#voucherNo").val("");
    $("#invoiceNo").val("");
    $("#suppliers").val("");
    $("#transactionDate").val();
    $("#suppliers").val();
    $("#invoiceNo").val();
    $("#invoiceDate").val();
    $("#creditPeriod").val();
    $("#currency").val();
    $("#narration").val();
    $("#grandTotal").val(0.0);
    $("#totalAmount").val(0.0);
    $("#taxAmountHtml").val(0.0);
}
function clearLineItems() {
    $("#searchByBarcode").val("");
    $("#searchByBarcode").trigger("chosen:updated");
    $("#searchByProductCode").val("");
    $("#searchByProductCode").trigger("chosen:updated");
    $("#searchByProductName").val("");
    $("#searchByProductName").trigger("chosen:updated");
    $("#store").val("");
    $("#store").trigger("chosen:updated");
    //$("#tax").val("0"); 
    $("#TaxAmount").val(0);
    $("#quantityToAdd").val("");
    $("#quantityInStock").val("");
    $("#amount").val(0);
    $("#rate").val(0);
    $("#description").val("");
    $("#unit").val("");
    $("#discount").val(0);
    $("#percentDiscount").val(0);
    $("#grossValue").val("0");
    $("#netValue").val("0");
    $("#discount").val(0);
    $("#batch").val("");
    $("#storeQuantityInStock").val("");
}