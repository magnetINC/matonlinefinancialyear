﻿var suppliers = []; var expenses = []; var currencies = []; var ledgers = []; var details = [];

var master = {};

var formNo = 0; var table = "";

$(function () {
    getLookUps();
});

//=====================  API CALLS========================
function getLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AccruedExpenses/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);

            formNo = data.Response.formNo
            suppliers = data.Response.suppliers;
            expenses = data.Response.expenses;
            currencies = data.Response.currencies;
            ledgers = data.Response.allLedgers;

            renderLookUpsToControls();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function save()
{
    if ($("#supplier").val() == "" || $("#supplier").val() == null || $("#supplier").val() == undefined)
    {
        Utilities.ErrorNotification("Please Select a Supplier");
    }
    else if (details.length <= 0)
    {
        Utilities.ErrorNotification("Cannot save an empty invoice");
    }
    else {
        Utilities.Loader.Show();
        master.Supplierid = $("#supplier").val();
        master.TotalAmount = $("#totalAmount").val();
        master.TransactionDate = $("#date").val();
        master.InvoiceDate = $("#date").val();
        master.FormNo = $("#invoiceNo").val();
        master.InvoiceNo = $("#invoiceNo").val();
        master.Currencyid = $("#currency").val();
        master.details = details;


        $.ajax({
            url: API_BASE_URL + "/AccruedExpenses/SaveAccruedExpenses",
            type: "POST",
            data: JSON.stringify(master),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification(data.ResponseMessage);
                    //getAvancePaymentsForListing($("#month").val())
                    Utilities.Loader.Hide();
                    window.location = "/Supplier/AccruedExpenses/SaveAccruedExpenses"
                }
                else {
                    Utilities.ErrorNotification("Something went wrong");
                    Utilities.Loader.Hide();
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}


//===================== RENDER DATA TO HTML CONTROLS ============
function renderLookUpsToControls()
{
    $("#supplier").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: suppliers,
        optionLabel: "Please Select..."
    }); 
    $("#expenses").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: expenses,
        optionLabel: "Please Select..."
    });
    $("#currency").kendoDropDownList({
        filter: "contains",
        dataTextField: "currencyName",
        dataValueField: "exchangeRateId",
        dataSource: currencies,
        //optionLabel: "All"
    });
    $("#invoiceNo").val(formNo);
}
function renderExpenseDateToTable()
{
    var output = "";
    var objToShow = [];
    
    if (table != "") {
        table.destroy();
    }
   
    $.each(details, function (count, row) {
        objToShow.push([
            count + 1,
            findLedger(row.LedgerId).ledgerName,
            '&#8358;' + Utilities.FormatCurrency(row.Amount),
            row.Memo
        ]);
    });

    $("#totalAmount").val(Utilities.FormatCurrency(calculateTotalAmount()));
    table = $('#expensesTable').DataTable({
        data: objToShow,
        "responsive": false,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}


//================= OTHER FUNCTIONS ===============
function openModal()
{
    $("#newExpenseModal").modal("show");
    $('#newExpenseModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });

}

function addToObject()
{
    var index = details.findIndex(p=>p.LedgerId == $("#expenses").val());
    if (index >= 0)
    {
        Utilities.ErrorNotification("This Expense has been selected already.");
    }
    else if ($("#expenses").val() == "" || $("#expenses").val() == undefined || $("#expenses").val() == null)
    {
        Utilities.ErrorNotification("Please select an expense account");
    }
    else if (parseFloat($("#amount").val()) <= 0)
    {
        Utilities.ErrorNotification("Please enter a valid amount");
    }
    else
    {
        details.push({
            LedgerId: $("#expenses").val(),
            Amount: parseFloat($("#amount").val()),
            Memo: $("#memo").val()
        });
        renderExpenseDateToTable();
        $("#amount").val(0);
        $("#memo").val("");
    }    
}

function calculateTotalAmount()
{
    var totalAmount = 0;
    $.each(details, function (count, row) {
        totalAmount = totalAmount + row.Amount;
    });
    return totalAmount;
}

function removeFromDetails(id) {
    var indexofobjecttoremove = details.findIndex(p=>p.LedgerId == id);
    details.splice(indexofobjecttoremove, 1);

    renderExpenseDateToTable();
}
function findLedger(id) {
    var output = {};
    for (i = 0; i < ledgers.length; i++) {
        if (ledgers[i].ledgerId == id) {
            output = ledgers[i];
            break;
        }
    }
    return output;
}