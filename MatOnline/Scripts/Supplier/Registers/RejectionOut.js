﻿var suppliers = [];
var param = {};
var registers = [];
function loadData() {
    var today = Date.now();
    param = {
        InvoiceNo: $("#orderNo").val() != "" ? $("#orderNo").val() : "",
        LedgerId: $("#suppliers").val() !="" ? $("#suppliers").val() : -1,
        FromDate: $("#lastMonth").val(),
        ToDate: $("#toDate").val(),
        DoneBy: $("#nameSpan").html(),
        Condition: $("#condition").val() !="" ?$("#condition").val() : "All"
    };
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/RejectionOut/Registers",
        type: "Post",
        contentType: "application/json",
        data:JSON.stringify(param),
        success: function (data) {
            registers = data;
            console.log(registers);
            renderGrid();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}


$(function () {    
    
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/GetSuppliers",
        type: "Get",
        contentType:"application/json",
        success:function(data)
        {
            PriviledgeSettings.ApplyUserPriviledge();
            
            suppliers = data;
            populateSuppliers();
            Utilities.Loader.Hide();
        },
        error:function(err)
        {
            Utilities.Loader.Hide();
        }
    });
    /*loadData();*/
    $("#search").click(function () {
        param = {
            InvoiceNo: $("#orderNo").val() != "" ? $("#orderNo").val() : "",
            LedgerId: $("#suppliers").val() !="" ? $("#suppliers").val() : -1,
            FromDate: $("#fromDate").val(),
            ToDate: $("#toDate").val(),
            Condition: $("#condition").val() !="" ?$("#condition").val() : "All"
        };
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/RejectionOut/Registers",
            type: "Post",
            contentType: "application/json",
            data:JSON.stringify(param),
            success: function (data) {
                registers = data;
                console.log("list",registers);
                renderGrid();
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });

    });
});

function renderGrid()
{       
    //$("#registerGrid").kendoGrid({
    //    dataSource:  registers,
    //    scrollable: false,
    //    sortable: true,
    //    pageable: true,
    //        pageSize: 10,
    //    pageable: {
    //        pageSizes: [10, 25, 50, 100, 1000],
    //        previousNext: true,
    //        buttonCount: 5,
    //    },
    //    columns: [
    //        { field: "RejectionOutNo", title: "Rejection Out No" },   rejectionOutMasterId: 3
    //        { field: "Date", title: "Date" },Date: "06 Nov 2019"
    //        { field: "ledgerName", title: "Cash/Party" }, ledgerName: "GREAT"
    //        { field: "BillAmount", title: "Bill Amount" }, BillAmount: 1000
    //        { field: "narration", title: "Narration" },  narration: ""
    //        { field: "currencyName", title: "Currency" },  
    //        { field: "userName", title: "Done By" }, userName: "admin"
    //        {
    //            command: [{ name: 'viewDetails', text: 'View Details', click: viewDetailsClick }]
    //        }],
    //    //editable: "popup"
    //});
    //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
    setTimeout(onSuccess, 500);

    
}

function onSuccess() {
    var dataObj = [];
    var numbering = 1;
    for (var i of registers) {
        var doneBy = i.doneBy == null ? i.userName : i.doneBy;
        dataObj.push([
            //i.rejectionOutMasterId,
            numbering,
            i.voucherNo,
            i.Date,
            i.ledgerName,
            Utilities.FormatCurrency(i.BillAmount),
            i.narration,
            doneBy,
            //i.userName,
            `<a onclick="viewRejection(${i.rejectionOutMasterId})" class="btn btn-info btn-xs"><i class="fa fa-eye"></i> View</a>`
        ]); numbering++;
    }
    if (checkPriviledge("frmRejectionOut", "View") === false) {
        dataObj.forEach(function (item) {
            item.splice(6, 1);
        });
    }
    $("#listTable").DataTable({
        data: dataObj,
        destroy: true,
        paging: true
    });
}

function populateSuppliers()
{
    var output = '<option value="-1">-Select Supplier-</option>';
    $.each(suppliers, function (count, record) {
        output += '<option value="'+record.ledgerId+'">'+record.ledgerName+'</option>';
    });
    $("#suppliers").html(output);
}

function viewDetailsClick(e) {
    var tr = $(e.target).closest("tr"); // get the current table row (tr)
    // get the data bound to the current table row
    var data = this.dataItem(tr);//userid
    console.log(data);
}

function viewRejection(id) {
    $.ajax(API_BASE_URL + "/RejectionOut/GetRejectionDetails/"+id,
        {
            type: "get",
            beforeSend: () => { Utilities.Loader.Show() },
            complete: () => { Utilities.Loader.Hide() },
            success: (data) => {
                console.log(data);
                var html = "";
                var total = 0.0;
                var sn = 0;
                for (var i of data) {
                    sn += 1;
                    var r = `<tr>
                                    <td style="white-space: nowrap;">${sn}</td>
                                    <td style="white-space: nowrap;">${i.Product.productCode}</td>
                                    <td style="white-space:nowrap">${i.Product.productCode}</td>
                                    <td style="white-space: nowrap;">${i.Product.productCode}</td>
                                    <td style="white-space: nowrap;">${i.Store.godownName}</td>
                                    <td style="white-space: nowrap;">${i.RejectionDetails.qty}</td>
                                    <td style="white-space: nowrap;">${i.Unit.unitName}</td>
                                    <td style="white-space: nowrap; text-align: right">${Utilities.FormatCurrency(i.RejectionDetails.rate)}</td>
                                    <td style="white-space: nowrap; text-align: right">${Utilities.FormatCurrency(i.RejectionDetails.amount)}</td>
                                    <td style="white-space: nowrap; text-align: right">${0.0}</td>
                                </tr> `;

                    html += r;
                    total = i.RejectionMaster.totalAmount;
                }
                var master = data[0].RejectionMaster;
                var ledger = data[0].Ledger;
                $("#invoiceNoDiv").text(master.invoiceNo);
                $("#invoiceDateDiv").text( Utilities.FormatJsonDate(master.date));
                $("#customerDiv").text(ledger.ledgerName);
                $("#totalAmount").val('N'+master.totalAmount);
                $("#taxAmountTxt").val('N'+0.0);
                $("#grandTotalTxt").val('N'+master.totalAmount);

                $("#delete").attr("data-id", master.rejectionOutMasterId);
                $("#edit").attr("data-id", master.rejectionOutMasterId);
                //id = "invoiceDateDiv"
                //id = "customerDiv"
                //id = "totalAmount"
                //id = "taxAmountTxt"
                //id = "grandTotalTxt"
               
                //$("#txtTotalAmount").text(total);
                $("#detailsTbody").html(html);
                $("#modalList").modal();
            }
        });
}

$(function() {
    $("#edit").click(function () {
        var id = $(this).attr("data-id");
        location.href = "/Supplier/RejectionOut/Edit/" + id;
    });

    $("#delete").click(function () {
        var id = $(this).attr("data-id");
        Utilities.ConfirmFancy("Are You Sure You Want to Delete? ", 
        $.ajax(API_BASE_URL + "/Supplier/RejectionOut/Delete/"+id,
            {
                type: "delete",
                success: function (res) {
                    Utilities.InfoNotification(res.message);
                    location.href = "//Supplier/RejectionOut/Register";
                }
            })
            );

    });
})