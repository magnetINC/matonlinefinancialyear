﻿var orderListForSearch = []
var master = {};
var details = [];
var toSave = {};
var orderId = 0;
//var supp = [];
var allUsers = [];

$(function () {
    //loadLookUps();
    $("#formNo").hide();
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });

    $.ajax({
        url: API_BASE_URL + "/UserCreation/GetAllUses",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            allUsers = data.Users;
        }
    });
});

function getPurchaseOrderToList(fromDate, toDate)
{
    param = {
        InvoiceNo: "",
        LedgerId: -1,
        FromDate: fromDate,
        ToDate: toDate + " 23:59:59",
        Condition: "All"
    };
    Utilities.Loader.Show();
    //loadLookUps();
    $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/Registers",
        type: "POST",
        data: JSON.stringify(param),
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });


}

function onSuccess(data) {
    orderListForSearch = data;
    var output = "";
    var objToShow = [];
    $.each(data, function (count, row) {
        var doneBy = row.doneBy == null ? row.userName : row.doneBy;
        objToShow.push([
            count + 1,
            row.invoiceNo,
            row.date,
            row.dueDate,
            row.ledgerName,
            Utilities.FormatCurrency(row.totalAmount),
            doneBy,
            //row.approved,
            '<button type="button" class="btn btn-info btn-xs" onclick="getOrderTransaction(' + row.purchaseOrderMasterId + ')"><i class="fa fa-eye"></i> View</button>',
            '<button type="button" class="btn btn-primary btn-xs" onclick="printOrderTransaction(' + row.purchaseOrderMasterId + ')"><i class="fa fa-print"></i> Print</button>'
        ]);
    });
    if (checkPriviledge("frmPurchaseOrder", "View") === false) {
        objToShow.forEach(function (item) {
            item.splice(7, 1);
        });
    }
    if (checkPriviledge("frmPurchaseOrder", "Update") === false) {
        $('.editOrder').hide();
    }
    if (checkPriviledge("frmPurchaseOrder", "Delete") === false) {
        $('.deleteOrder').hide();
    }
    table = $('#purchaseOrderListTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    // $("#salesQuotationListTbody").html(output);
    Utilities.Loader.Hide();
}

function getOrderTransaction(id) {
    var doneBy = orderListForSearch.find(x => x.purchaseOrderMasterId == id).doneBy;
    var doneByFinder = allUsers.find(x => x.userName == doneBy);
    var viewDoneBy = doneByFinder == undefined ? 'admin' : doneByFinder.firstName + ' ' + doneByFinder.lastName;

    orderId = id;
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/GetPurchaseOrderDetails?purchaseOrderMasterId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            master = data.Master;
            details = data.Details;
            console.log({ details });
            var statusOutput = "";
            if (master.orderStatus == "Pending") {
                $("#confirmOrder").show();
                $("#cancelOrder").show();
                statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending</span>';
            }
            else if (master.orderStatus == "Approved") {
                $("#confirmOrder").hide();
                $("#cancelOrder").hide();
                statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Confirmed</span>';
            }
            else if (master.orderStatus == "Cancelled") {
                $("#confirmOrder").hide();
                $("#cancelOrder").hide();
                statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
            }
            var output = "";
            for (i = 0; i < details.length; i++) {
                //if (details[i].barcode)
                //{
                //    details[i].barcode = "";
                //}

                console.log('details0: ', details[0]);
                output += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td><center>' + ((details[i].barcode == undefined) ? "-" : (details[i].barcode)) + '</center></td>\
                            <td>' + details[i].productCode + '</td>\
                            <td>' + details[i].productName + '</td>\
                            <td>' + details[i].qty + '</td>\
                            <td>' + details[i].unitName + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].amount) + '</td>\
                        </tr>';
            }

            $("#orderNoDiv").html(master.InvoiceNo);
            $("#orderDateDiv").html(Utilities.FormatJsonDate(master.Date));
            $("#orderNoDiv").html(master.InvoiceNo);
            $("#raisedByDiv").html(viewDoneBy);
            $("#raisedForDiv").html(master.ledgerName);
            //$("#statusDiv").html(statusOutput);
            $("#orderAmountTxt").val(Utilities.FormatCurrency(master.TotalAmount));
            //$("#taxAmountTxt").val(Utilities.FormatCurrency(tAmount));
            //$("#grandTotalTxt").val(Utilities.FormatCurrency((master.TotalAmount + master.TaxAmount)));

            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}


//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;

//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}


function printOrderTransaction(id) {

    var doneBy = orderListForSearch.find(x => x.purchaseOrderMasterId == id).doneBy;
    var doneByFinder = allUsers.find(x => x.userName == doneBy);
    var printDoneBy = doneByFinder == undefined ? 'admin' : doneByFinder.firstName + ' ' + doneByFinder.lastName;

    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();
    var thisEmail = $("#companyEmail").val();
    var thisPhone = $("#companyPhone").val(); 
    var objToPrint = [];
    orderId = id;
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/GetPurchaseOrderDetails?purchaseOrderMasterId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log('printAll: ', data);
            master = data.Master;
            details = data.Details;

            $.each(details, function (count, row) {
                objToPrint.push({
                    "SlNo": count + 1,
                    "Barcode": ((row.barcode == undefined) ? "-" : (row.barcode)),
                    "ProducCode": row.productCode,
                    "ProductName": row.productName,
                    "Qty": row.qty,
                    "UnitName": row.unitName,
                    "Rate": 'N'+ Utilities.FormatCurrency(row.rate),
                    "Amount": 'N' + Utilities.FormatCurrency(row.amount),
                    "Descptn": row.itemDescription == null ? "N/A" : row.itemDescription
                });
            });

            columns = [
                { title: "S/N", dataKey: "SlNo" },
                { title: "Barcode", dataKey: "Barcode" },
                { title: "Product Code", dataKey: "ProducCode" },
                { title: "Product Name", dataKey: "ProductName" },
                { title: "Qty", dataKey: "Qty" },
                { title: "Unit", dataKey: "UnitName" },
                { title: "Rate", dataKey: "Rate" },
                { title: "Description", dataKey: "Descptn" },
                { title: "Amount", dataKey: "Amount" },
            ];
            
            //console.log(master.userName);
            
            var doc = new jsPDF('portrait', 'pt', 'letter');
            doc.setDrawColor(0);

            //start drawing
            doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);

            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(companyName, 320, 80, 'center');
            doc.setFontSize(9);
            doc.setFontType("normal");
            doc.text(companyAddress, 320, 90, 'center');

            

            //continue drawing
            doc.setFontSize(9);
            doc.setFontType("bold");
            doc.text('Email Id: ', 400, 150);
            doc.setFontType("normal");
            doc.text(thisEmail, 460, 150);
            doc.setFontType("bold");
            doc.text('Phone: ', 400, 165);
            doc.setFontType("normal");
            doc.text(thisPhone, 460, 165);

            doc.line(40, 170, 570, 170);


            var Narration = master.Narration == "" ? 'N/A' : master.Narration;

            doc.setFontType("bold");
            doc.setFontSize(9);
            doc.text('Date:', 40, 185);
            doc.setFontType("normal");
            doc.text(Utilities.FormatJsonDate(master.Date), 85, 185);
            doc.setFontType("bold");
            doc.text('Narration:', 40, 205);
            doc.setFontType("normal");
            doc.text(Narration, 85, 205);

            doc.setFontType("bold");
            doc.text('Order No: ', 400, 185);
            doc.setFontType("normal");
            doc.text(master.InvoiceNo, 463, 185);
            doc.setFontType("bold");
            doc.text('Supplier: ', 400, 205);
            doc.setFontType("normal");
            doc.text(master.ledgerName, 463, 205);

            doc.line(40, 220, 570, 220);

            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text('Purchase Order Details', 240, 240);

            //doc.line(120, 20, 120, 60); horizontal line
            doc.setFontSize(8);
            doc.autoTable(columns, objToPrint, {
                startY: 255,
                theme: 'striped',
                styles: {
                    fontSize: 8,
                },
                columnStyles: {
                    SlNo: { columnWidth: 30, },
                    Amount: { columnWidth: 80, halign: 'right' },
                },
            });

            doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

            doc.setFontSize(9);
            doc.setFontType("bold");
            doc.text('Total Amount: NGN' + master.TotalAmount, 555, doc.autoTable.previous.finalY + 20, "right");

            doc.line(40, doc.autoTable.previous.finalY + 30, 570, doc.autoTable.previous.finalY + 30);

            doc.setFontType("bold");
            doc.text('Amount in Words: ', 40, doc.autoTable.previous.finalY + 60);
            doc.setFontType("normal");

            //capitalize Each Word
            var wordFormat = Utilities.NumberToWords(master.TotalAmount);
            const words = wordFormat.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());

            doc.text(words + ' Naira ONLY.', 120, doc.autoTable.previous.finalY + 60);

            doc.setFontType("bold");
            doc.text('Order approved by:  _________________________________', 40, doc.autoTable.previous.finalY + 100);
           
            doc.text('Done by:  ' + printDoneBy, 350, doc.autoTable.previous.finalY + 100);
            doc.text('______________', 390, doc.autoTable.previous.finalY + 103);
            doc.autoPrint();
            doc.save('Purchase_Order_Details.pdf');
            //window.open(doc.output('bloburl'));
            window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}



function editPurchaseOrder()
{
    window.location = "/Supplier/PurchaseOrder/PurchaseOrderEdit/" + orderId;

}
function deletePurchaseOrder()
{
    if (confirm('Delete this Order?')) {
  
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PurchaseOrder/DeletePurchaseOrder?id=" + orderId,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data == true) {
                    Utilities.SuccessNotification("Order Deleted successfully");
                    $("#detailsModal").modal("hide");
                    window.location = "/Supplier/PurchaseOrder/Register"
                }
                else {
                    Utilities.ErrorNotification("Oops! Sorry, couldn't delete");
                }
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                Utilities.Loader.Hide();
            }
        });
    }
}
















































////to get purchase order istings
//function getPurchaseOrderListings(fromDate, toDate, status) {
//    //param = {
//    //    InvoiceNo: "",
//    //    LedgerId: -1,
//    //    FromDate: fromDate,
//    //    ToDate: toDate + " 23:59:59",
//    //    Condition: condition != "" ? condition : "All"
//    //};
//    Utilities.Loader.Show();
//    //loadLookUps();
//    $.ajax({
//        url: API_BASE_URL + "/PurchaseOrder/PurchaseOrderListing?fromDate=" + fromDate + "&toDate=" + toDate + " 23:59:59&approved=" + status,
//        type: "Get",
//        contentType: "application/json",
//        success: function (data) {
//            console.log(data);
//            orderListForSearch = data.Table;
//            var output = "";
//            var objToShow = [];
//            $.each(data.Table, function (count, row) {
//                objToShow.push([
//                    count + 1,
//                    row.invoiceNo,
//                    Utilities.FormatJsonDate(row.date),
//                    Utilities.FormatJsonDate(row.dueDate),
//                    findSuppliers(row.ledgerId).ledgerName,
//                    '&#8358;' + Utilities.FormatCurrency(row.totalAmount),
//                    findUser(row.userId).userName,
//                    //row.approved,
//                    '<button type="button" class="btn btn-primary btn-sm" onclick="getOrderTransaction(' + row.purchaseOrderMasterId + ')"><i class="fa fa-eye"></i> View</button>'
//                ]);
//            });
//            table = $('#purchaseOrderListTable').DataTable({
//                data: objToShow,
//                "paging": true,
//                "lengthChange": true,
//                "searching": true,
//                "ordering": true,
//                "info": true,
//                "autoWidth": true
//            });
//            // $("#salesQuotationListTbody").html(output);
//            Utilities.Loader.Hide();
//        },
//        error: function (err) {
//            Utilities.Loader.Hide();
//        }
//    });
//}
////to get listing Details
//function getOrderTransaction(id) {
//    Utilities.Loader.Show();
//    $.ajax({
//        url: API_BASE_URL + "/PurchaseOrder/GetPurchaseOrderDetails?purchaseOrderMasterId=" + id,
//        type: "GET",
//        contentType: "application/json",
//        success: function (data) {
//            console.log(data);
//            master = data.Master;
//            details = data.Details;
//            var statusOutput = "";
//            if (master.orderStatus == "Pending") {
//                $("#confirmOrder").show();
//                $("#cancelOrder").show();
//                statusOutput = '<span class="label label-warning"><i class="fa fa-question"></i> Pending</span>';
//            }
//            else if (master.orderStatus == "Approved") {
//                $("#confirmOrder").hide();
//                $("#cancelOrder").hide();
//                statusOutput = '<span class="label label-success"><i class="fa fa-check"></i> Confirmed</span>';
//            }
//            else if (master.orderStatus == "Cancelled") {
//                $("#confirmOrder").hide();
//                $("#cancelOrder").hide();
//                statusOutput = '<span class="label label-danger"><i class="fa fa-times"></i> Cancelled</span>';
//            }
//            var output = "";
//            for (i = 0; i < details.length; i++) {
//                //if (details[i].barcode)
//                //{
//                //    details[i].barcode = "";
//                //}
//                output += '<tr>\
//                            <td>'+ (i + 1) + '</td>\
//                            <td><center>' + ((details[i].barcode == undefined) ? "-" : (details[i].barcode)) + '</center></td>\
//                            <td>' + details[i].productCode + '</td>\
//                            <td>' + details[i].productName + '</td>\
//                            <td>' + details[i].qty + '</td>\
//                            <td>' + details[i].unitName + '</td>\
//                            <td>&#8358;' + Utilities.FormatCurrency(details[i].rate) + '</td>\
//                            <td>&#8358;' + Utilities.FormatCurrency(details[i].amount) + '</td>\
//                        </tr>';
//            }

//            $("#orderNoDiv").html(master.InvoiceNo);
//            $("#orderDateDiv").html(Utilities.FormatJsonDate(master.Date));
//            $("#orderNoDiv").html(master.InvoiceNo);
//            $("#raisedByDiv").html(master.userName);
//            $("#raisedForDiv").html(master.ledgerName);
//            //$("#statusDiv").html(statusOutput);
//            $("#orderAmountTxt").val(Utilities.FormatCurrency(master.TotalAmount));
//            //$("#taxAmountTxt").val(Utilities.FormatCurrency(tAmount));
//            $("#grandTotalTxt").val(Utilities.FormatCurrency(master.TotalAmount));

//            $("#availableQuantityDiv").html("");
//            $("#detailsTbody").html(output);
//            $("#detailsModal").modal("show");
//            Utilities.Loader.Hide();
//        },
//        error: function (err) {
//            Utilities.Loader.Hide();
//        }
//    });
//}
////save to material receipt pending list
//function savePending() {
//    Utilities.Loader.Show();

//    var lineitems = [];
//    var sl = 1;

//    for (i = 0; i < details.length; i++) {
//        lineitems.push({
//            ProductId: details[i].productId,
//            Quantity: details[i].qty,
//            OrderDetailsId: details[i].purchaseOrderDetailsId,
//            Rate: details[i].rate,
//            Description: details[i].narration,
//            UnitId: details[i].unitId,
//            UnitConversionId: details[i].unitConversionId,
//            StoreId: details[i].godownId,
//            RackId: details[i].rackId,
//            BatchId: details[i].batchId,
//            SL: sl
//        });
//        sl += 1;
//    }

//    toSave.ReceiptNo = master.InvoiceNo;
//    toSave.TotalAmount = master.TotalAmount;
//    toSave.date = master.Date;
//    toSave.SupplierId = master.LedgerId;
//    toSave.OrderMasterId = master.PurchaseOrderMasterId;
//    toSave.Narration = master.Narration;
//    toSave.LineItems = lineitems;
//    console.log(toSave);

//    $.ajax({
//        url: API_BASE_URL + "/MaterialReceipt/SavePending",
//        type: "POST",
//        data: JSON.stringify(toSave),
//        contentType: "application/json",
//        success: function (data) {
//            console.log(data);
//            if (data == true) {
//                Utilities.SuccessNotification("Material Receipt Confirmed Successfully");
//                $("#detailsModal").modal("hide");
//                table.destroy();
//                if ($("#backDays").val() == "custom") {
//                    getPurchaseOrderListings($("#fromDate").val(), $("#toDate").val(), $("#orderStatus").val());
//                }
//                else {
//                    getPurchaseOrderListings("2017-01-01", $("#toDate").val(), $("#orderStatus").val());
//                }
//            }
//            else {
//                Utilities.ErrorNotification("Material Receipt Confirmation Failed");
//            }

//            Utilities.Loader.Hide();
//        },
//        error: function (err) {
//            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
//            Utilities.Loader.Hide();
//        }
//    });

//}

//function findSuppliers(id) {
//    var output = {};
//    for (i = 0; i < supp.length; i++) {
//        if (supp[i].ledgerId == id) {
//            output = supp[i];
//            break;
//        }
//    }
//    return output;
//}
//function findUser(id) {
//    var output = {};
//    for (i = 0; i < users.length; i++) {
//        if (users[i].userId == id) {
//            output = users[i];
//            break;
//        }
//    }
//    return output;
//}