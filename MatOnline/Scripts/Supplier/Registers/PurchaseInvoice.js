﻿var suppliers = [];
var formTypes = [];
var param = {};
var registers = [];
var details = [];
var dateType = "Voucher Date";
var table = "";
var thisInvoiceMaster = {};
var projects = [];
var categories = [];

$(function ()
{
    PriviledgeSettings.ApplyUserPriviledge();
    Utilities.Loader.Show();
   
    var suppliersAjax = $.ajax({
        url: API_BASE_URL + "/Payments/GetSuppliers",
        type: "Get",
        contentType: "application/json"
    });

    var formTypesAjax = $.ajax({
        url: API_BASE_URL + "/PurchaseInvoice/GetFormTypes",
        type: "Get",
        contentType: "application/json"
    });

    $.when(suppliersAjax, formTypesAjax)
        .done(function (dataSuppliers, dataFormTypes) {
            suppliers = dataSuppliers[2].responseJSON;
            formTypes = dataFormTypes[2].responseJSON;

            populateFormTypes();
            populateSuppliers();

            Utilities.Loader.Hide();
        });

    var projectLookupAjax = $.ajax({
        url: API_BASE_URL + "/Project/GetProject",
        type: "GET",
        contentType: "application/json",
    });

    var categoryLookupAjax = $.ajax({
        url: API_BASE_URL + "/Category/GetCategory",
        type: "GET",
        contentType: "application/json",
    });

    $.when(projectLookupAjax, categoryLookupAjax)
        .done(function (dataProject, dataCategory) {
            projects = dataProject[0];
            categories = dataCategory[0];

            console.log("projects", projects)
        });

    $("#search").click(function () {
        GetPurchaseInvoices($("#fromDate").val(), $("#toDate").val());
    });

    function GetPurchaseInvoices(fromDate, toDate) {
        param = {
            InvoiceNo: "",
            LedgerId: 0,
            FromDate: fromDate,
            ToDate: toDate + " 23:59:59",
            PurchaseMode: "All",
            VoucherType: 0,
            Column: "Voucher Date"
        };
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PurchaseInvoice/Registers",
            type: "Post",
            contentType: "application/json",
            data: JSON.stringify(param),
            success: function (data) {
                registers = data;
                console.log(registers);
                //renderGrid();
                renderDataToTable();
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
    //$("#voucherDate").check
    $("input:radio[name=dateType]").click(function () {
        var value = $(this).val();
        dateType = value;
    });
});

function renderDataToTable() {
    //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been 
    //filled before it is used here, thereby avoiding errors eg datatable errors.
    setTimeout(onSuccess, 500);

}

function onSuccess()
{
    var output = "";
    
    var objToShow = [];
    $.each(registers, function (count, row) {
        var doneBy = row.doneBy == null ? row.userName : row.doneBy;
        console.log(row.vendorInvoiceNo);
        objToShow.push([
            count + 1,
            row.invoiceNo,
            row.vendorInvoiceNo,
            row.no,
            row.invoiceDate,
            row.ledgerName,
            Utilities.FormatCurrency(row.grandTotal),
            doneBy,
            //row.doneBy,
            //row.approved,
            '<button type="button" class="btn btn-info btn-xs" onclick="getInvoiceDetails(' + row.purchaseMasterId + ')"><i class="fa fa-eye"></i> View</button>',
            //'<button type="button" class="btn btn-primary btn-xs" onclick="printInvoiceDetails(' + row.purchaseMasterId + ')"><i class="fa fa-print"></i> Print</button>'
        ]);
    });

    if (table != "")
    {
        table.destroy();
    }

    //if (checkPriviledge("frmPurchaseInvoice", "View") === false) {
    //    objToShow.forEach(function (item) {
    //        item.splice(8, 1);
    //    });
    //}

    // console.log("Christmas is coming, moaned the grinch", objToShow);

    table = $('#purchaseInvoiceLineItemTbody').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}

function getInvoiceDetails(id) {
    Utilities.Loader.Show();
    for (i = 0; i < registers.length; i++) {
        if (registers[i].purchaseMasterId == id) {
            thisInvoiceMaster = registers[i];
            break;
        }
    }
    $.ajax({
        url: API_BASE_URL + "/PurchaseInvoice/RegisterDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            details = data;
            var output = "";
            var totalTax = 0;
            var netAmount = 0;
            var totalAmount = 0;
            for (i = 0; i < details.length; i++) {
                //if (details[i].barcode)
                //{
                //    details[i].barcode = "";
                //}
                var grossValue = details[i].rate * details[i].qty;
                var netValue = grossValue - details[i].discount;
                var totAmount = netValue + details[i].taxAmount;
                //var thisCategory = row.Category == null ? "N/A" : getCategoryName(details[i].CategoryId);
                //var thisProject = row.Project == null ? "N/A" : getProjectName(details[i].ProjectId);
                var projectObj = projects.find(p => p.ProjectId == details[i].ProjectId);
                var projectName = projectObj == undefined ? "NA" : projectObj.ProjectName;
                var categoryObj = categories.find(p => p.CategoryId == details[i].CategoryId);
                var categoryName = categoryObj == undefined ? "NA" : categoryObj.CategoryName;
                output += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td><center>' + ((details[i].barcode == undefined) ? "-" : (details[i].barcode)) + '</center></td>\
                            <td>' + details[i].productCode + '</td>\
                            <td>' + details[i].productName + '</td>\
                            <td>' + details[i].itemDescription + '</td>\
                            <td>' + projectName + '</td>\
                            <td>' + categoryName + '</td>\
                            <td>' + details[i].qty + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(grossValue) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].discount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(netValue) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].taxAmount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(totAmount) + '</td>\
                        </tr>';
                totalTax = totalTax + details[i].taxAmount;
                netAmount = netAmount + netValue
            }
            totalAmount = totalTax + netAmount;
            $("#edit").attr("data-id", id);
            $("#delete").attr("data-id", id);
            $("#invoiceNoDiv").html(thisInvoiceMaster.invoiceNo);
            $("#invoiceDateDiv").html(thisInvoiceMaster.invoiceDate);
            $("#orderNoDiv").html(thisInvoiceMaster.no);
            $("#raisedByDiv").html(thisInvoiceMaster.userName);
            $("#raisedForDiv").html(thisInvoiceMaster.ledgerName);
            //$("#statusDiv").html(statusOutput);
            $("#totalAmount").val(Utilities.FormatCurrency(netAmount));
            $("#totalTaxAmount").val(Utilities.FormatCurrency(totalTax));
            $("#grandTotal").val(Utilities.FormatCurrency(totalAmount));

            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getCategoryName(id) {
    var CategoryName;
    for (i = 0; i < categories.length; i++) {
        if (categories[i].CategoryId == id) {
            CategoryName = categories[i].CategoryName;
            break;
        }
    }
    return CategoryName;
}

function getProjectName(id) {
    var ProjectName = {};
    for (i = 0; i < projects.length; i++) {
        if (projects[i].ProjectId == id) {
            ProjectName = projects[i].ProjectName;
            break;
        }
    }
    return ProjectName;
}
$(function () {
    $("#edit").click(function () {
        var id = $(this).attr("data-id");
        /// redirect 
        location.href = "/Supplier/PurchaseInvoice/Edit/" + id;
    });

    $("#delete").click(function () {
        var id = $(this).attr("data-id");
        /// redirect 
        Utilities.ConfirmFancy("Are you sure you want to delete ?",
            function () {
                $.ajax(API_BASE_URL + "/PurchaseInvoice/Delete/" + id,
                    {
                        type: "delete",
                        success: function (res) {
                            Utilities.Alert(res.message);
                            setTimeout(function () {
                                location.href = "/Supplier/PurchaseInvoice/Register";
                            },
                                300);
                        }
                    });
            });

    });
});
function renderGrid() {
    $("#registerGrid").kendoGrid({
        dataSource: registers,
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 10,
            pageSizes: [10, 25, 50, 100, 1000],
            previousNext: true,
            buttonCount: 5,
        },
        columns: [
            { field: "invoiceNo", title: "Form No" },
            { field: "voucherTypeName", title: "Form Type" },
            { field: "voucherDate", title: "Form Date" },
            { field: "invoiceDate", title: "Invoice Date" },
            { field: "ledgerName", title: "Supplier" },
            { field: "grandTotal", title: "Bill Amount" },
            { field: "currencyName", title: "Currency" },
            { field: "no", title: "Order No/Receipt No" },
            { field: "userName", title: "Done By" },
            { field: "narration", title: "Narration" },
            {
                command: [{ name: 'viewDetails', text: 'View Details', click: viewDetailsClick }]
            }],
        //editable: "popup"
    });

}

function populateSuppliers() {
    var output = '<option value="0" selected>All</option>';
    $.each(suppliers, function (count, record) {
        output += '<option value="' + record.ledgerId + '">' + record.ledgerName + '</option>';
    });
    $("#suppliers").html(output);
}

function populateFormTypes() {
    var output = '';
    console.log(formTypes);
    $.each(formTypes, function (count, record) {
        output += '<option value="' + record.voucherTypeId + '">' + record.voucherTypeName + '</option>';
    });
    $("#formType").html(output);
}

function viewDetailsClick(e) {
    var tr = $(e.target).closest("tr"); // get the current table row (tr)
    // get the data bound to the current table row
    var data = this.dataItem(tr);//userid
    console.log(data);
}