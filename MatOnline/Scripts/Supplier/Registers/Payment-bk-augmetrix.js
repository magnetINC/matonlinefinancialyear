﻿var suppliers = [];
var param = {};
var registers = [];

$(function () {    
    
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Payments/GetSuppliers",
        type: "Get",
        contentType:"application/json",
        success:function(data)
        {
            suppliers = data;
            populateSuppliers();
            Utilities.Loader.Hide();
        },
        error:function(err)
        {
            Utilities.Loader.Hide();
        }
    });

    $("#search").click(function () {
        param = {
            InvoiceNo: $("#orderNo").val() != "" ? $("#orderNo").val() : "",
            LedgerId: $("#suppliers").val() !="" ? $("#suppliers").val() : -1,
            FromDate: $("#fromDate").val(),
            ToDate: $("#toDate").val()
        };
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/Payments/Registers",
            type: "Post",
            contentType: "application/json",
            data:JSON.stringify(param),
            success: function (data) {
                registers = data;
                console.log(registers);
                renderGrid();
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });

    });
});

function renderGrid()
{       
    $("#registerGrid").kendoGrid({
        dataSource:  registers,
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 10,
            pageSizes: [10, 25, 50, 100, 1000],
            previousNext: true,
            buttonCount: 5,
        },
        columns: [
            { field: "invoiceNo", title: "Form No" },
            { field: "voucherTypeName", title: "Form Type" },
            { field: "date", title: "Date" },
            { field: "ledgerName", title: "Cash / Bank" },
            { field: "narration", title: "Narration" },
            { field: "totalAmount", title: "Amount" },
            {
                command: [{ name: 'viewDetails', text: 'View Details', click: viewDetailsClick }]
            }],
        //editable: "popup"
    });

}

function populateSuppliers()
{
    var output = '<option value="0" selected>All</option>';
    $.each(suppliers, function (count, record) {
        output += '<option value="'+record.ledgerId+'">'+record.ledgerName+'</option>';
    });
    $("#suppliers").html(output);
}

function viewDetailsClick(e) {
    var tr = $(e.target).closest("tr"); // get the current table row (tr)
    // get the data bound to the current table row
    var data = this.dataItem(tr);//userid
    console.log(data);
}