﻿var ListForSearch = [];
var suppliers = [];
var users = [];
var lineItems = [];
var products = [];
var units = [];
var warehouses = [];
var master1 = {};
var master2 = {};
var details = [];
var thisMR = {};
var toSave = {};
var allUsers = [];

$(function () {
    $("#formNo").hide();
    $(".dtHide").hide();
    $("#backDays").change(function () {
        if ($(this).val() == "custom") {
            $(".dtHide").show();
        }
        else {
            //getSalesQuotationsList("2017-01-01", $("#toDate").val(), "Pending");
            $(".dtHide").hide();
        }
    });

    $.ajax({
        url: API_BASE_URL + "/UserCreation/GetAllUses",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            allUsers = data.Users;
        }
    });


});

function getMaterialReceiptList(fromDate, toDate, approved)
{
    var searchParam = {
        fromDate: fromDate,
        toDate: toDate + " 23:59:59 PM",
        ledgerId: 0,
        invoiceNo: ""
    };

    Utilities.Loader.Show();

    $.ajax({
        url: API_BASE_URL + "/MaterialReceipt/Registers",
        type: "Post",
        data: JSON.stringify(searchParam),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (err)
        {
            Utilities.Loader.Hide();
        }
    });
}

function onSuccess(data) {
    console.log('data = ', data)
    suppliers = data.Suppliers;
    ListForSearch = data.Data.Table;
    User = data.User;
    warehouses = data.warehouses;
    var output = "";
    var objToShow = [];
    $.each(ListForSearch, function (count, row) {
        var action = `<div class="btn-group btn-group-sm">
                        <div class="dropdown">
                            <button class="btn btn-sm btn-success btn-active-white dropdown-toggle action" data-toggle="dropdown" type="button">
                                Action <i class="dropdown-caret"></i>
                            </button>
                            <ul class="dropdown-menu">
                                         <li>
                                  <button type="button" class="btn btn-primary btn-sm viewDetails" onclick="getMaterialReceiptTransaction(${row.materialReceiptMasterId})"><i class="fa fa-eye"></i> View</button>
                                    </li>
                                <li>
                                  <a style="color:white;" type="button" class="btn btn-warning link-button btn-sm editDetails" href="/Supplier/MaterialReceipt/Edit/${row.materialReceiptMasterId}"><i class="fa fa-edit"></i> Edit</a>
                                </li>
                                    <li>
                                  <button type="button" class="btn btn-danger btn-sm deleteDetails" onclick="deleteMaterialReceiptTransaction(${row.materialReceiptMasterId})"><i class="fa fa-trash"></i> Delete</button>
                                    </li>
                            </ul>
                        </div>
                    </div>
                 `;
        //var doneBy = row.doneBy == null ? findUser(row.userId).userName : row.doneby;
        var doneBy = allUsers.find(id => id.userId == row.doneby);
        var doneByName = doneBy == undefined ? 'admin' : doneBy.userName;
       
        objToShow.push([
            count + 1,
            row.invoiceNo,
            row.lrNo,
            Utilities.FormatJsonDate(row.date),
            (findSuppliers(row.ledgerId).ledgerName == undefined) ? "" : findSuppliers(row.ledgerId).ledgerName,
            Utilities.FormatCurrency(row.totalAmount),
            //findUser(row.userId).userName,
            doneByName,
            '<button type="button" class="btn btn-info btn-xs viewDetails" onclick="getMaterialReceiptTransaction(' + row.materialReceiptMasterId + ')"><i class="fa fa-eye"></i> View</button>', 
            '<button type="button" class="btn btn-primary btn-xs printDetails" onclick="printMaterialReceiptTransaction(' + row.materialReceiptMasterId + ')"><i class="fa fa-print"></i> Print</button>'
        ]);
    });

    table = $('#materialReceiptListTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

    //$("#salesQuotationListTbody").html(output);
    var isFalse = 0;
    if (checkPriviledge("frmMaterialReceipt", "View") === false) {
        $('.viewDetails').hide();
        isFalse++;
    }
    if (checkPriviledge("frmMaterialReceipt", "Update") === false) {
        $('.editDetails').hide();
        isFalse++; 
    }
    if (checkPriviledge("frmMaterialReceipt", "Delete") === false) 
    {
        $('.deleteDetails').hide();
        isFalse++; 
    }
    if(isFalse == 3)
    {
        $('.action').hide();
    }
    Utilities.Loader.Hide();
}

function getMaterialReceiptTransaction(id) {
    Utilities.Loader.Show();

    for (i = 0; i < ListForSearch.length; i++) {
        if (ListForSearch[i].materialReceiptMasterId == id) {
            thisMR = ListForSearch[i];
        }
    }
    console.log(thisMR);
    $.ajax({
        url: API_BASE_URL + "/MaterialReceipt/RegisterDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            products = data.products;
            units = data.units;
            var details = data.detailsfull
            var output = "";
            $.each(details, function (count, row) {
                var Unit = findUnit(row.unitId).unitName;
                if (Unit == undefined) Unit = '-';
                console.log(row.amount);
                output += '<tr>\
                            <td>'+ (count + 1) + '</td>\
                            <td><center>' + ((findProduct(row.productId).barcode == undefined) ? "-" : (findProduct(row.productId).barcode)) + '</center></td>\
                            <td>' + findProduct(row.productId).productCode + '</td>\
                            <td>' + findProduct(row.productId).productName + '</td>\
                            <td>' + findWarehouses(row.godownId).godownName + '</td>\
                            <td>' + row.qty + '</td>\
                            <td>' + Unit + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(row.amount) + '</td>\
                        </tr>';
            });

            $("#orderNoDiv").html(thisMR.invoiceNo);
            $("#orderDateDiv").html(Utilities.FormatJsonDate(thisMR.date));
            $("#orderNoDiv").html(thisMR.invoiceNo);
            $("#raisedByDiv").html(findUser(thisMR.userId).userName);
            $("#raisedForDiv").html(findSuppliers(thisMR.ledgerId).ledgerName);
            $("#orderAmountTxt").val(Utilities.FormatCurrency(thisMR.totalAmount));
            //$("#taxAmountTxt").val(Utilities.FormatCurrency(tAmount));
            $("#grandTotalTxt").val(Utilities.FormatCurrency(thisMR.totalAmount));
            $("#buttonsEditAndDelete").html('<a style="color:white;" type="button" class="btn btn-warning link-button btn-sm editDetails" href="/Supplier/MaterialReceipt/Edit/'+thisMR.materialReceiptMasterId+'"><i class="fa fa-edit"></i> Edit</a>'+
                '<button type="button" class="btn btn-danger btn-sm deleteDetails" onclick="deleteMaterialReceiptTransaction('+thisMR.materialReceiptMasterId+')"><i class="fa fa-trash"></i> Delete</button>');

            $("#availableQuantityDiv").html("");
            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
} 

//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;


//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}


function printMaterialReceiptTransaction(id) {
    Utilities.Loader.Show();

    for (i = 0; i < ListForSearch.length; i++) {
        if (ListForSearch[i].materialReceiptMasterId == id) {
            thisMR = ListForSearch[i];
        }
    }

    var thisUser = findUser(thisMR.doneby);
    var doneBy = thisUser.firstName + ' ' + thisUser.lastName;

    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();
    var thisEmail = $("#companyEmail").val();
    var thisPhone = $("#companyPhone").val();
    var JournalNo = thisMR.invoiceNo;
    var thisDate = Utilities.FormatJsonDate(thisMR.date);
    var customer = findSuppliers(thisMR.ledgerId).ledgerName;

    $.ajax({
        url: API_BASE_URL + "/MaterialReceipt/RegisterDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            products = data.products;
            units = data.units;
            var details = data.detailsfull;

            var objToPrint = [];
            $.each(details, function (count, row) {
                var Unit = findUnit(row.unitId).unitName;
                if (Unit == undefined) Unit = '-';
                objToPrint.push({
                    "SlNo": count + 1,
                    "Barcode": ((findProduct(row.productId).barcode == undefined) ? "-" : (findProduct(row.productId).barcode)),
                    "ProductCode": findProduct(row.productId).productCode,
                    "ProductName": findProduct(row.productId).productName,
                    "WareHouse": findWarehouses(row.godownId).godownName,
                    "Quantity": row.qty,
                    "Unit": Unit,
                    "Rate": 'N'+Utilities.FormatCurrency(row.rate),
                    "Amount": 'N'+Utilities.FormatCurrency(row.amount)
                });
            });

            columns = [
                { title: "S/N", dataKey: "SlNo" },
                { title: "Barcode", dataKey: "Barcode" },
                { title: "Product Code", dataKey: "ProductCode" },
                { title: "Product Name", dataKey: "ProductName" },
                { title: "WareHouse", dataKey: "WareHouse" },
                { title: "Quantity", dataKey: "Quantity" },
                { title: "Unit", dataKey: "Unit" },
                { title: "Rate", dataKey: "Rate" },
                { title: "Amount", dataKey: "Amount" }
            ];


            var doc = new jsPDF('portrait', 'pt', 'letter');
            doc.setDrawColor(0);

            //start drawing

            doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);

            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(companyName, 320, 80, 'center');
            doc.setFontSize(9);
            doc.setFontType("normal");
            doc.text(companyAddress, 320, 90, 'center');

            doc.setFontSize(9);
            doc.setFontType("bold");
            doc.text('Email Id: ', 400, 150);
            doc.setFontType("normal");
            doc.text(thisEmail, 460, 150);
            doc.setFontType("bold");
            doc.text('Phone: ', 400, 165);
            doc.setFontType("normal");
            doc.text(thisPhone, 460, 165);

            doc.line(40, 170, 570, 170);


            //doc.line(395, 175, 395, 215);//horizontal line
            doc.setFontType("bold");
            doc.text('Supplier: ', 40, 185);
            doc.setFontType("normal");
            doc.text(customer, 100, 185);

            doc.setFontType("bold");
            doc.text('Order No: ', 400, 185);
            doc.setFontType("normal");
            doc.text(JournalNo, 460, 185);
            doc.setFontType("bold");
            doc.text('Date: ', 400, 205);
            doc.setFontType("normal");
            doc.text(thisDate, 460, 205);

            doc.line(40, 220, 570, 220);

            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text('Material Receipt', 250, 240);

            //doc.line(120, 20, 120, 60); horizontal line
            doc.setFontSize(8);
            doc.autoTable(columns, objToPrint, {
                startY: 255,
                theme: 'striped',
                styles: { "overflow": "linebreak", "cellWidth": "wrap", "rowPageBreak": "avoid", "fontSize": "7", "lineColor": "100", "lineWidth": ".25" },

                columnStyles: {
                    SlNo: { columnWidth: 30, },
                    Barcode: { columnWidth: 60},
                    ProductCode: { columnWidth: 60},
                    ProductName: { columnWidth: 60 },
                    WareHouse: { columnWidth: 80 },
                    Quantity: { columnWidth: 40 },
                    Unit: { columnWidth: 60 },
                    Rate: { columnWidth: 60, halign: 'right' },
                    Amount: { columnWidth: 80, halign: 'right' }

                },
            });

            doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

            doc.setFontSize(9);
            doc.setFontType("bold");
            doc.text('Grand total: N' + Utilities.FormatCurrency(thisMR.totalAmount), 555, doc.autoTable.previous.finalY + 20, "right");

            doc.line(40, doc.autoTable.previous.finalY + 35, 570, doc.autoTable.previous.finalY + 35);

            //convert to words
            var TotalInWords = Utilities.NumberToWords(thisMR.totalAmount);

            //Capitalize Each Word
            const words = TotalInWords.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());

            var strArr = doc.splitTextToSize('Grand Total in Words: ' + words + ' Naira Only.', 500);
            doc.text(strArr, 40, doc.autoTable.previous.finalY + 75);

            doc.setFontType("bold");
            doc.text('Approved by:  _________________________________', 40, doc.autoTable.previous.finalY + 135);

            doc.text('Done by:  ' + doneBy, 350, doc.autoTable.previous.finalY + 135);
            doc.text('___________________', 390, doc.autoTable.previous.finalY + 140);
            doc.autoPrint();
            //doc.save('Material Receipt.pdf');
            //window.open(doc.output('bloburl'));
            window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
            
            /*$("#raisedByDiv").html(findUser(thisMR.userId).userName);
            $("#raisedForDiv").html();
            $("#orderAmountTxt").val(Utilities.FormatCurrency(thisMR.totalAmount));
            
            */

           


            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });



















   /* 

    

    

    

    

   

    */
       
}





function deleteMaterialReceiptTransaction(id) {

    $("#detailsModal").modal('hide');
    Swal.fire({
        title: 'Warning',
        text: "Are you sure you want to delete this record",
        type: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, I am Sure'
    }).then((result) => {
        if (result.value) {
            $.ajax({
                url: API_BASE_URL + "/MaterialReceipt/Delete/?id=" + id,
                type: "GET",
                contentType: "application/json",
                beforeSend: () => {
                    Utilities.Loader.Show();
                },
                complete: () => {
                    Utilities.Loader.Hide();
                },
                success: function (data) {
                    if (data == true) {
                        Utilities.SuccessNotification("Delete Successful");
                        location.reload();
                    }

                },
                error: function (err) {
                    Utilities.Loader.Hide();
                }
            });
        }
    });


        
}
function findSuppliers(ledgerId) {
    var output = {};
    for (i = 0; i < suppliers.length; i++) {
        if (suppliers[i].ledgerId == ledgerId) {
            output = suppliers[i];
            break;
        }
    }
    return output;
}

function findUser(userId) {
    var output = {};
    for (i = 0; i < User.length; i++) {
        if (User[i].userId == userId) {
            output = User[i];
            break;
        }
    }
    return output;
}

function findProduct(id) {
    var output = {};
    for (i = 0; i < products.length; i++) {
        if (products[i].productId == id) {
            output = products[i];
            break;
        }
    }
    return output;
}
function findUnit(id) {
    var output = {};
    for (i = 0; i < units.length; i++) {
        if (units[i].unitId == id) {
            output = units[i];
            break;
        }
    }
    return output;
}
function findWarehouses(id) {
    var output = {};
    for (i = 0; i < warehouses.length; i++) {
        if (warehouses[i].godownId == id) {
            output = warehouses[i];
            break;
        }
    }
    return output;
}
