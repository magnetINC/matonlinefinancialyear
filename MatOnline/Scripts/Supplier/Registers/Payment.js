﻿var suppliers = [];
var param = {};
var registers = [];
var allUsers = [];
var allPayments = [];

$(function () {    
    
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Payments/GetSuppliers",
        type: "Get",
        contentType:"application/json",
        success:function(data)
        {
            suppliers = data;
            populateSuppliers();
            Utilities.Loader.Hide();
        },
        error:function(err)
        {
            Utilities.Loader.Hide();
        }
    });

    $.ajax({
        url: API_BASE_URL + "/UserCreation/GetAllUses",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            allUsers = data.Users;
        }
    });


    $("#search").click(function () {
        param = {
            InvoiceNo: $("#orderNo").val() != "" ? $("#orderNo").val() : "",
            LedgerId: $("#suppliers").val() !="" ? $("#suppliers").val() : -1,
            FromDate: $("#fromDate").val(),
            ToDate: $("#toDate").val()
        };
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/Payments/Registers",
            type: "Post",
            contentType: "application/json",
            data:JSON.stringify(param),
            success: function (data) {

                registers = [];

                console.log(data);

                var count = 0;

                for (var i of data)
                {
                    if (i.voucherTypeName == "Payment Voucher") {
                        allPayments.push(i);
                        var doneBy = i.doneBy == null ? 'Admin' : i.doneBy;
                        count++;
                        var d = new Date(i.date);
                        registers.push([
                            count,
                            i.invoiceNo,
                            i.voucherTypeName,
                            d.toLocaleDateString(),
                            i.ledgerName,
                            i.totalAmount,
                            doneBy,
                            '<button type="button" class="btn btn-info btn-xs" onclick="DisplayPaymentDetail(' + i.paymentMasterId +')"><i class="fa fa-eye"></i> View</button>',
                            '<button  class="btn btn-primary btn-xs btn-labeled fa fa-print" onclick="printReport(' + i.paymentMasterId + ')">Print</button> '
                         ]);
                    }
                }
                console.log(registers);
                renderGrid();
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });

    });
});


function LoadRecorders(fromDate, toDate) {

    param = {
        InvoiceNo: $("#orderNo").val() != "" ? $("#orderNo").val() : "",
        LedgerId: $("#suppliers").val() != "" ? $("#suppliers").val() : -1,
        FromDate: fromDate,
        ToDate: toDate,
    };
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Payments/Registers",
        type: "Post",
        contentType: "application/json",
        data: JSON.stringify(param),
        success: function (data) {

            registers = [];

            console.log(data);

            var count = 0;

            for (var i of data) {
                if (i.voucherTypeName == "Payment Voucher") {
                    allPayments.push(i);
                    var doneBy = i.doneBy == null ? 'Admin' : i.doneBy;
                    count++;
                    var d = new Date(i.date);
                    registers.push([
                        count,
                        i.invoiceNo,
                        i.voucherTypeName,
                        d.toLocaleDateString(),
                        i.ledgerName,
                        i.totalAmount,
                        doneBy,
                        '<button type="button" class="btn btn-info btn-xs" onclick="DisplayPaymentDetail(' + i.paymentMasterId + ')"><i class="fa fa-eye"></i> View</button>',
                        '<button  class="btn btn-primary btn-xs btn-labeled fa fa-print" onclick="printReport(' + i.paymentMasterId + ')">Print</button> '
                    ]);
                }
            }
            console.log(registers);
            renderGrid();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });

}


function renderGrid()
{       
    $('#PaymentListTable').DataTable({
        data: registers,
        destroy: true,
        searching: true,
        ordering: true
    });
    
}

function populateSuppliers()
{
    var output = '<option value="0" selected>All</option>';
    $.each(suppliers, function (count, record) {
        output += '<option value="'+record.ledgerId+'">'+record.ledgerName+'</option>';
    });
    $("#suppliers").html(output);
}

function viewDetailsClick(e) {
    var tr = $(e.target).closest("tr"); // get the current table row (tr)
    // get the data bound to the current table row
    var data = this.dataItem(tr);//userid
    console.log(data);
}


$("#delete").click(function () {

    var id = $(this).attr("data-id");

    Utilities.ConfirmFancy("Are Sure You Want to Delete? You Cant Undo This. But Can Be Tracked",
        function () {
            $.ajax(API_BASE_URL + "/Payments/Delete/?masterid=" + id,
                {
                    type: "DELETE",
                    success: function (data) {
                        Utilities.Alert(data.message);
                        setTimeout(function () {
                            location.href = "/Supplier/Payment/Register";
                        },
                            3000);
                    }
                });
        }
    );

});




function DisplayPaymentDetail(MasterId) {
    var doneBy = allPayments.find(x => x.paymentMasterId == MasterId).doneBy;
    var doneByFinder = allUsers.find(x => x.userName == doneBy);
    var viewDoneBy = doneByFinder == undefined ? 'admin' : doneByFinder.firstName + ' ' + doneByFinder.lastName;

    var url = API_BASE_URL + "/Payments/GetPaymentDetails/?masterid=" + MasterId;

    $.ajax(url,
        {
            type: "get",
            beforeSend: () => { Utilities.Loader.Show() },
            complete: () => { Utilities.Loader.Hide() },
            success: function (data) {

                var count = 0;
                var html = "";

                console.log("view data", data);

                var master = data.master;
                var projects = data.projects;
                var categories = data.categories;

                $("#detailsModal").modal();
                var id = MasterId;

                for (var i of data.details) {

                    console.log(data.details);
                    count++;

                    var project = 0;
                    var category = 0;

                    if (i.ProjectId != 0) {
                        project = projects.find(x => x.ProjectId == i.ProjectId).ProjectName;
                    } else {
                        project = "N/A";
                    }

                    if (i.CategoryId != 0) {
                        category = categories.find(x => x.CategoryId == i.CategoryId).CategoryName;
                    } else {
                        category = "N/A";
                    }

                    var invoiceNo = i.invoiceNo == undefined ? '-' : i.invoiceNo;

                    html += `
                            <tr>            
                                <td >${count}</td>
                                <td >${i.ledgerName}</td>
                                <td >${Utilities.FormatCurrency(i.amount)}</td>
                                <td >${invoiceNo}</td>
                                <td >${i.Memo}</td>
                                <td >${project}</td>
                                <td >${category}</td>
                            </tr>   
                                `;


                    $("#FormNoDiv").text(master.InvoiceNo);
                    $("#PaymentDateDiv").text(Utilities.FormatJsonDate(master.Date));
                    $("#CashBankDiv").text(master.LedgerName);
                    //$("#UserDiv").text(master.UserName);
                    $("#UserDiv").text(viewDoneBy);
                    $("#ChequeNoDiv").text(i.chequeNo != "" ? i.chequeNo : 'N/A' );
                    $("#ChequeDateDiv").text(Utilities.FormatJsonDate(i.chequeDate));
                    $("#CurrencyDiv").text(data.currency);

                    $("#totalAmount").val(Utilities.FormatCurrency(master.TotalAmount));
                }
                $("#detailsTbody").html(html);
                $("#delete").attr('data-id', id);
            }
        });
}



function printReport(MasterId) {

    var url = API_BASE_URL + "/Payments/GetPaymentDetails/?masterid=" + MasterId;
    var printData =[];
    printData.lineItems = [];

    $.ajax(url,
        {
            type: "get",
            beforeSend: () => { Utilities.Loader.Show() },
            complete: () => { Utilities.Loader.Hide() },
            success: function (data) {
                var count = 0;

                console.log("Master", data.master);
                console.log(data);

                var master = data.master;
                var projects = data.projects;
                var categories = data.categories;

               
                var id = MasterId;

                for (var i of data.details) {

                    count++;

                    var project = 0;
                    var category = 0;

                    if (i.ProjectId != 0) {
                        project = projects.find(x => x.ProjectId == i.ProjectId).ProjectName;
                    } else {
                        project = "N/A";
                    }

                    if (i.CategoryId != 0) {
                        category = categories.find(x => x.CategoryId == i.CategoryId).CategoryName;
                    } else {
                        category = "N/A";
                    }


                    printData.lineItems.push({
                        SlNo: count,
                        Particulars : i.ledgerName,
                        Amount: Utilities.FormatCurrency(i.amount),
                        InvoiceNo: i.invoiceNo == undefined ? '-' : i.invoiceNo,
                        Memo : i.Memo,
                        Project : project,
                        Department : category
                    });
                    
                    printData.FormNo = master.InvoiceNo;
                    printData.PaymentDate = Utilities.FormatJsonDate(master.Date);
                    printData.CashBank = master.LedgerName;
                    printData.User = master.UserName;
                    printData.ChequeNo = i.chequeNo != "" ? i.chequeNo : 'N/A';
                    printData.ChequeDate = Utilities.FormatJsonDate(i.chequeDate);
                    printData.Currency = data.currency;
                    printData.totalAmount = Utilities.FormatCurrency(master.TotalAmount);

                    var doneBy = allPayments.find(x => x.paymentMasterId == MasterId).doneBy;
                    var doneByFinder = allUsers.find(x => x.userName == doneBy);
                    printData.doneBy = doneByFinder == undefined ? 'admin' : doneByFinder.firstName + ' ' + doneByFinder.lastName;
                    console.log("cheque Number" + printData.ChequeNo);

                }

                print(printData);
            }
        });
}

//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;


//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}



function print(printData) {
    console.log(printData);
    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();
    var thisEmail = $("#companyEmail").val();
    var thisPhone = $("#companyPhone").val();

    var thisChequeNumber = printData.ChequeNo;
    var thisVoucherNo = printData.FormNo;
    var thisAccount = printData.CashBank;

    var thisDate = printData.PaymentDate;
    var thisCurrency = printData.Currency;
    var totalAmt = printData.totalAmount;
    var doneBy = printData.doneBy;


    
    //, printData.ChequeDate, printData.User;

    columns = [
        { title: "S/N", dataKey: "SlNo" },
        { title: "Ledgers", dataKey: "Particulars" },
        { title: "Invoice No:", dataKey: "InvoiceNo" },
        { title: "Memo", dataKey: "Memo" },
        { title: "Amount", dataKey: "Amount" },
    ];

    var doc = new jsPDF('portrait', 'pt', 'letter');
    doc.setDrawColor(0);

    
    doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text(companyName, 320, 80, 'center');
    doc.setFontSize(9);
    doc.setFontType("normal");
    doc.text(companyAddress, 320, 90, 'center');

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Email Id: ', 400, 150);
    doc.setFontType("normal");
    doc.text(thisEmail, 460, 150);
    doc.setFontType("bold");
    doc.text('Phone: ', 400, 165);
    doc.setFontType("normal");
    doc.text(thisPhone, 460, 165);

    doc.line(40, 170, 570, 170);

    doc.setFontType("bold");
    doc.setFontSize(9);
    doc.text('Account:', 40, 185);
    doc.setFontType("normal");
    doc.text(thisAccount, 85, 185);
    doc.setFontType("bold");
    doc.text('Cheque Number:', 40, 205);
    doc.setFontType("normal");
    doc.text(thisChequeNumber, 120, 205);

    doc.setFontType("bold");
    doc.text('Voucher No: ', 400, 185);
    doc.setFontType("normal");
    doc.text(thisVoucherNo, 460, 185);
    doc.setFontType("bold");
    doc.text('Date: ', 400, 205);
    doc.setFontType("normal");
    doc.text(thisDate, 460, 205);

    doc.line(40, 220, 570, 220);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text('Payments', 260, 240);

    //doc.line(120, 20, 120, 60); horizontal line
    doc.setFontSize(8);
    doc.autoTable(columns, printData.lineItems, {
        startY: 255,
        theme: 'striped',
        styles: {
            fontSize: 8,
        },
        columnStyles: {
            SlNo: { columnWidth: 30, },
            Amount: { columnWidth: 80, halign: 'right' },
            Memo: { columnWidth: 100 },
            Expenses: { columnWidth: 110 },
            Department: { columnWidth: 100 },
            Projects: { columnWidth: 100 }

        },
    });

    doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Total Amount: NGN' + totalAmt, 570, doc.autoTable.previous.finalY + 20, "right");

    doc.line(40, doc.autoTable.previous.finalY + 30, 570, doc.autoTable.previous.finalY + 30);

    doc.setFontType("bold");
    doc.text('Amount in Words: ', 40, doc.autoTable.previous.finalY + 60);
    doc.setFontType("normal");

    //remove commas
    var tempTotal = totalAmt.replace(/,/g, "");
    //convert to number
    var amtToBeConverted = parseInt(tempTotal);

    //capitalize Each Word
    var wordFormat = Utilities.NumberToWords(amtToBeConverted);
    const words = wordFormat.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());

    doc.text(words + ' Naira ONLY.', 120, doc.autoTable.previous.finalY + 60);

    doc.setFontType("bold");
    doc.text('Payment approved by:  _________________________________', 40, doc.autoTable.previous.finalY + 100);

    doc.text('Done by:  ' + doneBy, 350, doc.autoTable.previous.finalY + 100);
    doc.text('______________', 390, doc.autoTable.previous.finalY + 103);

    doc.autoPrint();
    doc.save('Payment' + printData.FormNo + '.pdf');
    //window.open(doc.output('bloburl'));
    window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
}


