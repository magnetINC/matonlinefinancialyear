﻿var suppliers = [];
var formTypes = [];
var param = {};
var registers = [];
var invoiceNumbers = [];

$(function () {    
    
    Utilities.Loader.Show();
    var suppliersAjax = $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/GetSuppliers",
        type: "Get",
        contentType:"application/json"
    });

    var formTypesAjax = $.ajax({
        url: API_BASE_URL + "/PurchaseReturn/GetVoucherTypes",
        type: "Get",
        contentType: "application/json"
    });

    var invoiceNumbersAjax = $.ajax({
        url: API_BASE_URL + "/PurchaseReturn/GetInvoiceNumbers?supplierId=-1&voucherTypeId=-1",
        type: "Get",
        contentType: "application/json"
    });

    $.when(suppliersAjax, formTypesAjax,invoiceNumbersAjax)
    .done(function (dataSuppliers,dataFormTypes,dataInvoiceNumbers) {
        console.log("formType ni", dataFormTypes);
        suppliers = dataSuppliers[2].responseJSON;
        formTypes = dataFormTypes[2].responseJSON;
        invoiceNumbers = dataInvoiceNumbers[2].responseJSON;

        populateFormTypes();
        populateSuppliers();
        populateInvoiceNumbers();

        Utilities.Loader.Hide();
    });

    $("#search").click(function () {
        //alert($("#suppliers").val() == 0 ? -1 : $("#suppliers").val()); return;
        param = {
            InvoiceNo: $("#orderNo").val(),
            LedgerId: $("#suppliers").val() == 0 ? -1 : $("#suppliers").val(),
            FromDate: $("#fromDate").val(),
            ToDate: $("#toDate").val(),
            AgainstInvoiceNo: $("#invoiceNumber").val() == "" ? 0 : $("#invoiceNumber").val(),
            VoucherType: $("#formType").val() == 0 ? -1 : $("#formType").val()
        };
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PurchaseReturn/Registers",
            type: "Post",
            contentType: "application/json",
            data:JSON.stringify(param),
            success: function (data) {
                registers = data;
                console.log(registers);
                renderGrid();
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });

    });

    //$("#voucherDate").check
    $("input:radio[name=dateType]").click(function () {
        var value = $(this).val();
        dateType = value;
    });

    $("#suppliers").change(function () {
        var supplier= $("#suppliers").val() == 0 ? -1 : $("#suppliers").val();
        var voucherType= $("#formType").val() == 0 ? -1 : $("#formType").val();

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PurchaseReturn/GetInvoiceNumbers?supplierId="+supplier+"&voucherTypeId="+voucherType,
            type: "Get",
            contentType: "application/json",
            success: function (data) {
                invoiceNumbers = data;
                populateInvoiceNumbers();
                Utilities.Loader.Hide();
            },
            error: function () {
                Utilities.Loader.Hide();
            }
        });
    });
});

function renderGrid()
{       
    $("#registerGrid").kendoGrid({
        dataSource:  registers,
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 10,
            pageSizes: [10, 25, 50, 100, 1000],
            previousNext: true,
            buttonCount: 5,
        },
        columns: [
            { field: "ReturnNo", title: "Form No" },
            { field: "voucherTypeName", title: "Form Type" },
            { field: "date", title: "Date" },
            { field: "ledgerName", title: "Supplier" },
            { field: "grandTotal", title: "Grand Total" },
            { field: "currencyName", title: "Currency" },
            { field: "AgainstInvoiceNo", title: "Invoice No" },
            { field: "narration", title: "Narration" },
            { field: "userName", title: "Done By" },
            {
                command: [{ name: 'viewDetails', text: 'View Details', click: viewDetailsClick }]
            }],
        //editable: "popup"
    });

}

function populateSuppliers()
{
    var output = '<option value="0" selected>All</option>';
    $.each(suppliers, function (count, record) {
        output += '<option value="'+record.ledgerId+'">'+record.ledgerName+'</option>';
    });
    $("#suppliers").html(output);
}

function populateFormTypes() {
    var output = '';
    $.each(formTypes, function (count, record) {
        output += '<option value="' + record.voucherTypeId + '">' + record.voucherTypeName + '</option>';
    });
    $("#formType").html(output);
}

function populateInvoiceNumbers() {
    var output = '';
    $.each(invoiceNumbers, function (count, record) {
        output += '<option value="' + record.purchaseMasterId + '">' + record.invoiceNo + '</option>';
    });
    $("#invoiceNumber").html(output);
}

function viewDetailsClick(e) {
    var tr = $(e.target).closest("tr"); // get the current table row (tr)
    // get the data bound to the current table row
    var data = this.dataItem(tr);//userid
    console.log(data);
}