﻿var allProducts = [];
var salesInvoiceLineItems = [];
var searchResult = {};
var customers = [];
var salesMen = [];
var pricingLevels = [];
var currencies = [];
var units = [];
var batches = [];
var stores = [];
var racks = [];
var taxes = [];
var totalLineItemAmount = 0;
var applyOn = [];
var prod = {};
var purchaseMasterId = 0;
var details = {};

var toSave = {};
var Productist = [];
var SupplierList = [];
var CurrencyList = [];
var TaxList = [];
var UnitList = [];
var InvoiceNo = "";
var LineItems = [];
var searchResult = {};
var tax = {};
var purchaseRate = 0;
var sl = 1;
var productToEdit = 0;
var deleteIndex;

$(function () {
    GetLookUpData();
    getAllProducts();

    //$("#applyOn").change(function () {
    //    if ($("#applyOn").val() == 10) {
    //        $("#newLineItem").hide();
    //        LineItems = [];
    //        RenderLineItem();
    //    }
    //    else {
    //        $("#newLineItem").show();
    //        LineItems = [];
    //        RenderLineItem();
    //    }
    //});

    $("#btnLineItemModal").click(function () {
        $("#LineItemModal").modal("toggle");
    });

    $("#Barcode").change(function () {
        searchProduct("ProductCode", $("#Barcode").val());
    });

    $("#ProductCode").change(function () {
        searchProduct("ProductCode", $("#ProductCode").val());
    });

    $("#ProductName").change(function () {
        searchProduct("ProductName", $("#ProductName").val());
    });

    $("#Qty").keyup(function () {
        calculateAmountOfItemToAdd();
    });

    $("#Rate").change(function () {
        calculateAmountOfItemToAdd();
    });

    $("#applyOn").on("change", function () {
        var val = $("#applyOn").val();
        if (val == 10) {
            LineItems = [];
            RenderLineItem();
            loadOrderNumbers();
        }
        else if(val == 0){
            $("#orderNo").html("");
            LineItems = [];
            RenderLineItem();
        }
        console.log(val);
    });

    $("#Supplier").on("change", function () {
        var val = $("#applyOn").val();
        if (val == 10) {
            loadOrderNumbers();
        }
        console.log(val);
    });

    $("#Tax").change(function ()
    {
        var isFound = false;
        for (i = 0; i < TaxList.length; i++) {
            if (TaxList[i].taxId == $("#Tax").val()) {
                tax = TaxList[i];
                isFound = true;
                break;
            }
        }

        if (isFound == false)   //means tax 0 was selected which is not in the array from database so revert to the static tax(non vat)
        {
            tax = { taxId: 0, taxName: "NA", rate: 0 };
        }
        // purchaseRate = $("#Rate").val();
        calculateAmountOfItemToAdd();
    });

    $("#orderNo").change(function () {
        purchaseMasterId = $("#orderNo").val();
        var invoiceNo = $("#orderNo :selected").text();
        if (orderNo != "") {
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/MaterialReceipt/GetMaterialReceiptLineItemFromOrderNo?invoiceNo=" + purchaseMasterId,
                type: "GET",
                contentType: "application/json",
                success: function (data) {
                    var sl = 1;
                    console.log(data);
                    //return;
                    details = data.Details;
                    batches = data.Batches;
                    units = data.Units;
                   // stores = data.Stores;
                    racks = data.Racks;
                    taxes = data.Taxes;

                    LineItems = [];
                    //clear array
                    $.each(details, function (count, row) {
                        LineItems.push({
                            SL: count + 1,
                            ProductId: row.productId,
                            ProductBarcode: findProduct(row.productId).barcode,
                            ProductCode: findProduct(row.productId).ProductCode,
                            ProductName: findProduct(row.productId).ProductName,
                            Description: row.itemDescription,
                            Brand: row.brandName,
                            Quantity: row.qty,
                            UnitId: row.unitId,
                            UnitConversionId: row.unitConversionId,
                            StoreId: row.godownId,
                            RackId: row.rackId,
                            BatchId: row.batchId,
                            Rate: row.rate,
                            Amount: row.amount,
                            InvoiceNo: row.invoiceNo,
                            VoucherNo: row.voucherNo,
                            //voucherTypeId: row.voucherTypeId,
                            OrderDetailsId: row.purchaseOrderDetailsId,
                        });
                    });
                   
                    for (i = 0; i < LineItems.length; i++) {
                        LineItems[i].QtyReceived = LineItems[i].Quantity;
                    }
                    sl += 1;
                    RenderLineItem();


                    Utilities.Loader.Hide();
                },
                error: function () {
                    Utilities.Loader.Hide();
                }
            });
        }

    });
});
function openModal() {
    $("#LineItemModal").modal("show");
    $('#LineItemModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
}

//find suppliers:
function findSuppliers(id) {
    var output = {};
    for (i = 0; i < SupplierList.length; i++) {
        if (SupplierList[i].ledgerId == id) {
            output = SupplierList[i];
            break;
        }
    }
    return output;
}

//to get all look up data
function GetLookUpData() {
    Utilities.Loader.Show();
    var lookUpAjax = $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/LookUpData1",
        type: "Get",
        contentType: "application/json"
    });
    var lookUpAjax2 = $.ajax({
        url: API_BASE_URL + "/MaterialReceipt/InvoiceLookUps",
        type: "GET",
        contentType: "application/json",
    });
    var autoInvoiceNoAjax = $.ajax({
        url: API_BASE_URL + "/MaterialReceipt/GetAutoVoucherNo",
        type: "Get",
        contentType: "application/json"
    });
    $.when(lookUpAjax, lookUpAjax2, autoInvoiceNoAjax)
     .done(function (dataLookUp, dataLookUp2, dataInvoiceNo) {
         console.log(dataInvoiceNo);
         console.log(dataLookUp);
         Productist = dataLookUp[0].Products;
         stores = dataLookUp2[0].stores;
         SupplierList = dataLookUp[0].Suppliers;
         CurrencyList = dataLookUp[0].Currencies;
         TaxList = dataLookUp[0].Taxes;
         UnitList = dataLookUp[0].Units
         InvoiceNo = dataInvoiceNo[2].responseJSON;
         applyOn = dataLookUp2[0].ApplyOn; 
         $("#formNo").val(dataInvoiceNo[2].responseJSON);
         //to render data to controls after successful data fetch
         RenderDatatoControls();
         Utilities.Loader.Hide();

         var masterId = $("#masterId").val(); //this does not exist in the view, thus call depending on it will fail ...
         if (masterId == '' || masterId == undefined) {
             return;
         } else {
             LoadDataForEdit(masterId);

         }
     });
}
//to render data to controls
function RenderDatatoControls() {
    // $("#Supplier").html(Utilities.PopulateDropDownFromArray(SupplierList, 1, 0));
    $("#Currency").html(Utilities.PopulateDropDownFromArray(CurrencyList, 1, 0));
    $("#Tax").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(TaxList, 0, 1));
    
    $("#Supplier").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: SupplierList,
        optionLabel: "Please Select..."
    });
    //$("#Barcode").kendoDropDownList({
    //    optionLabel: "Please Select...",
    //    dataTextField: "productCode",
    //    dataValueField: "productCode",
    //    dataSource: Productist
    //});
    //$("#ProductCode").kendoDropDownList({
    //    filter: "contains",
    //    optionLabel: "Please Select...",
    //    dataTextField: "productCode",
    //    dataValueField: "productCode",
    //    dataSource: Productist
    //});
    //$("#ProductName").kendoDropDownList({
    //    filter: "contains",
    //    optionLabel: "Please Select...",
    //    dataTextField: "productName",
    //    dataValueField: "productName",
    //    dataSource: Productist
    //});


    if (Productist.length > 0) {


        var productNameHtml = "";
        var productCodeHtml = "";
        var barCodeHtml = "";

        $.each(Productist, function (count, record) {
            if (record.IsInRightYear == true) {
                productNameHtml += '<option value="' + record.ProductName + '">' + record.ProductName + '</option>';
                productCodeHtml += '<option value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                barCodeHtml += '<option value="' + record.barcode + '">' + record.barcode + '</option>';
            }
            else {
                productNameHtml += '<option disabled="disabled" value="' + record.ProductName + '">' + record.ProductName + '( This product is not active because it was created in higher financial year )</option>';
                productCodeHtml += '<option disabled="disabled" value="' + record.ProductCode + '">' + record.ProductCode + '( This product is not active because it was created in higher financial year )</option>';
                barCodeHtml += '<option disabled="disabled" value="' + record.barcode + '">' + record.barcode + '( This product is not active because it was created in higher financial year )</option>';
            }
        });

        $("#ProductName").html(productNameHtml);
        $("#ProductCode").html(productCodeHtml);
        $("#Barcode").html(barCodeHtml);


        $("#ProductName").chosen({ width: "100%", margin: "1px" });
        $("#Barcode").chosen({ width: "100%", margin: "1px" });
        $("#ProductCode").chosen({ width: "100%", margin: "1px" });
    }

    //$("#Barcode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(Productist, 0, 0));
    //$("#ProductCode").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(Productist, 1, 1));
    //$("#ProductName").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(Productist, 2, 2));
    //$("#ProductName").chosen({ width: "100%", margin: "1px" });
    //$("#Barcode").chosen({ width: "100%", margin: "1px" });
    //$("#ProductCode").chosen({ width: "100%", margin: "1px" });

    $("#Unit").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(UnitList, 1, 2));
    $("#stores").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));
    $("#stores2").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(stores, 0, 1));

    $("#stores2").chosen({ width: "100%", margin: "1px" });

    var applyOnHtml = "";
    $.each(applyOn, function (count, record) {
        applyOnHtml += '<option value="' + record.voucherTypeId + '">' + record.voucherTypeName + '</option>';
    });
    $("#applyOn").html(applyOnHtml);
}
var sl = 1;


function formatDate(st) {
    let str = new Date(st);
    let day = str.getDate();
    let mnth = str.getMonth() + 1;
    let year = str.getFullYear();
    return `${mnth}/${day}/${year}`;
}

function LoadDataForEdit(masterId)
{
    $.ajax(API_BASE_URL + "/MaterialReceipt/GetMaterialReceiptLineItemFromMasterId/?masterId=" + masterId,
        {
            type: "get",
            beforeSend: () => { Utilities.Loader.Show() },
            complete: () => { Utilities.Loader.Hide() },
            success: (data) =>
            {
                if (data != null)
                {
                    $("#formNo").val(data.invoiceNo);
                    $("#orderNo").val(data.orderNo);
                    $("#invoiceNo").val(data.vendorInvoiceNo);
                    $("#Supplier").data('kendoDropDownList').value(data.ledgerId);
                    $("#voucherNo").val(data.voucherTypeId);
                    $("#transactionDate").val(formatDate(data.date));

                    sl = 1;

                    for (var i of data.lineItem)
                    {
                        var amount = (Number)
                        LineItems.push({

                            slno: sl,
                            MaterialDetailId: i.MaterialDetailId,
                            ProductId: i.ProductId,
                            Barcode: i.ProductCode,
                            ProductCode: i.ProductCode,
                            ProductName: i.ProductName,
                            Description: i.Description,
                            Quantity: i.Quantity,
                            QtyReceived: i.Quantity,
                            Unit: i.UnitId,
                            Rate: i.Rate,
                            StoreId: i.StoreId,
                            Amount: i.Amount,
                            TaxId: "0",
                            TaxAmount: "0"
                        });

                        sl = sl + 1;
                        RenderLineItem();
                    }
                }
            }
        });
}
 
//$("#addSalesInvoiceLineItem").click(function () {
//    salesInvoiceLineItems.push({
//        SL: sl,
//        ProductId: searchResult.productId,
//        ProductBarcode: searchResult.barcode,
//        ProductCode: searchResult.productCode,
//        ProductName: searchResult.productName,
//        Description: searchResult.narration,
//        Brand: searchResult.brandName,
//        Quantity: $("#quantityToAdd").val(),
//        UnitId: searchResult.unitId,
//        UnitConversionId: searchResult.unitConversionId,
//        StoreId: $("#stores2").val(),
//        RackId: searchResult.rackId,
//        BatchId: searchResult.batchId,
//        Rate: searchResult.salesRate,
//        Amount: ($("#quantityToAdd").val() * searchResult.salesRate).toFixed(2)
//    });
//    sl += 1;
//    renderSalesInvoiceLineItems();
//});
$("#saveMaterialReceipt, #savePrint").click(saveReceipt);

function saveReceipt() {
    var myId = $(this).attr('id');
    var sup = $("#Supplier").val(); 
    if (sup == '' || sup == undefined) {
        Utilities.Alert("Select Supplier");
        return;
    }
    for (i = 0; i < LineItems.length; i++) {
        if (LineItems[i].StoreId == "" || LineItems[i].StoreId == undefined) {
            Utilities.Alert("Store Cannot be empty, Edit the line items and select store");
            return;
        }
    }
     Utilities.Loader.Show();
    for (i = 0; i < LineItems.length; i++) {
        LineItems[i].Quantity = LineItems[i].QtyReceived;
    }

    var orderMasterId = $("#orderNo").val();
    if (details[0] == undefined) {
        orderMasterId = 0;
    }

    toSave = {
        ReceiptNo: $("#formNo").val(),
        Date: $("#transactionDate").val(),
        SupplierId: $("#Supplier").val(),
        DeliveryMode: $("#salesMode").val(),
        Narration: $("#narration").val(),
        TotalAmount: totalLineItemAmount,
        TransportaionCompany: $("#transportationCompany").val(),
        LrNo: $("#lrNo").val(),
        OrderNo:  $("#orderNo").val(),
        PricingLevelId: $("#pricingLevel").val(),
        DoneBy: $("#UserId").html(),
        SalesManId: matUserInfo.userId,
        CurrencyId: 1,
        OrderMasterId: orderMasterId,
        WayBill: $("#WayBill").val(),
        LineItems: LineItems,
    };
    console.log(toSave);

    $.ajax({
        url: API_BASE_URL + "/MaterialReceipt/SaveOrEdit",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data.ResponseCode == 200) {
                Utilities.SuccessNotification(data.ResponseMessage);
                if (myId == "savePrint") {
                    print();
                }
                //Utilities.SuccessNotification("Material Receipt Confirmed Successfully");
                window.location = "/Supplier/MaterialReceipt/Index";
            }
            else {
                Utilities.ErrorNotification("Material Receipt Confirmation Failed");
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

$("#Edit").click(function() {
    Utilities.Loader.Show();
    for (i = 0; i < LineItems.length; i++) {
        LineItems[i].Quantity = LineItems[i].QtyReceived;
    }
    var orderMasterId;
    if (details[0] == undefined) {
        orderMasterId = 0;
    }
    toSave = {
        MaterialMasterId: $("#masterId").val(),
        ReceiptNo: $("#formNo").val(),
        Date: $("#transactionDate").val(),
        SupplierId: $("#Supplier").val(),
        DeliveryMode: $("#salesMode").val(),
        Narration: $("#narration").val(),
        TotalAmount: totalLineItemAmount,
        TransportaionCompany: $("#transportationCompany").val(),
        LrNo: $("#lrNo").val(),
        OrderNo: $("#orderNo :selected").text(),
        PricingLevelId: $("#pricingLevel").val(),
        SalesManId: matUserInfo.userId,
        CurrencyId: 1,
        OrderMasterId: orderMasterId,
        WayBill: $("#invoiceNo").val(),
        LineItems: LineItems,
    };
    console.log(toSave);
    $.ajax({
        url: API_BASE_URL + "/MaterialReceipt/EditFunction",
        type: "POST",
        data: JSON.stringify(toSave),
        contentType: "application/json",
        success: function (data)
        {
            console.log(data);
            if (data.ResponseCode == 200)
            {
                Utilities.SuccessNotification(data.ResponseMessage);
                //Utilities.SuccessNotification("Material Receipt Confirmed Successfully");
                window.location = "/Supplier/MaterialReceipt/Register";
            }
            else
            {
                Utilities.ErrorNotification("Material Receipt Confirmation Failed");
            }
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
});
function calculateAmountOfItemToAdd() {
    if (tax.rate == undefined) {
        tax.rate = 0;
    }
    var qty = $("#Qty").val();
    var amount = $("#Rate").val() * qty;
    var taxAmount = amount * (tax.rate / 100.0);

    $("#Amount").val(Utilities.FormatCurrency(amount));
    $("#TaxAmount").val(Utilities.FormatCurrency(taxAmount));
}

function openModal() {
    $("#LineItemModal").modal("show");
    $('#LineItemModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
}

function loadOrderNumbers() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/MaterialReceipt/GetOrderNo?voucherTypeId=" + $("#applyOn").val() + "&supplierId=" + $("#Supplier").val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var orderNoHtml = '<option></option>';
            $.each(data, function (count, row) {
                orderNoHtml += '<option value="' + row.purchaseOrderMasterId + '">' + row.invoiceNo + '</option>';
            });
            $("#orderNo").html(orderNoHtml);
            Utilities.Loader.Hide();
        },
        error: function () {
            Utilities.Loader.Hide();
        }
    });
}

function searchProduct(searchBy, filter) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PurchaseOrder/SearchProduct?&searchBy=" + searchBy + "&filter=" + filter,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            searchResult = data.Product;
            console.log(data)
            purchaseRate = searchResult.PurchaseRate
            $("#Rate").val(searchResult.PurchaseRate);
            $("#productId").val(searchResult.ProductId)
            $("#Description").val(searchResult.Narration)
            $("#Unit").val(data.Unit[0].unitId);

            //var dropdownlistBarcode = $("#Barcode").data("kendoDropDownList");
            //var dropdownlistProductCode = $("#ProductCode").data("kendoDropDownList");
            //var dropdownlistProductName = $("#ProductName").data("kendoDropDownList");

            //dropdownlistBarcode.value(searchResult.ProductCode);
            //dropdownlistProductCode.value(searchResult.ProductCode);
            //dropdownlistProductName.value(searchResult.ProductName);

            $("#Barcode").val(searchResult.ProductCode);
            $("#Barcode").trigger("chosen:updated");
            $("#ProductCode").val(searchResult.ProductCode);
            $("#ProductCode").trigger("chosen:updated");
            $("#ProductName").val(searchResult.ProductName);
            $("#ProductName").trigger("chosen:updated");

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

var slToEdit;
function changeQuantity(id, sl) {
    $("#quantityModal").modal("show");
    productToEdit = id;
    slToEdit = sl;
}

function applyQuantityOnObject() {
    console.log(productToEdit);
    for (i = 0; i < LineItems.length; i++) {
        if (LineItems[i].SL == slToEdit) {
            if ($("#quantity").val() <= LineItems[i].Quantity) {
                LineItems[i].QtyReceived = $("#quantity").val();
                LineItems[i].Amount = LineItems[i].Rate * $("#quantity").val();
                LineItems[i].StoreId = $("#stores").val();
            }
            else {
                Utilities.ErrorNotification("Quantity Received cannot be Higher than Quantity Ordered");
            }
           
        }
    }

    
    $("#quantity").val("");
    $("#quantityModal").modal("hide");
    RenderLineItem();
}

function AddtoLineItem() {
    var a = $("#Amount").val();
    var amt = a.replace(/,/g, "");
    var amount = Number.parseFloat(amt);
    var t = $("#TaxAmount").val();
    var tamt = t.replace(/,/g, "");
    var tamount = Number.parseFloat(tamt);
    if ($("#Qty").val() == "" && $("#stores2").val() == "" || $("#stores2").val() == 0)
    {
        Utilities.ErrorNotification("Please fill all the appropriate fields");
    }
    else if ($("#stores2").val() == "" || $("#stores2").val() == 0) {
        Utilities.ErrorNotification("Please select store and try again");
    }
    else
    {
        LineItems.push({
            slno: sl,
            ProductId: $("#productId").val(),
            Barcode: $("#Barcode").val(),
            ProductCode: $("#ProductCode").val(),
            ProductName: $("#ProductName").val(),
            Description: $("#Description").val(),
            Quantity: $("#Qty").val(),
            QtyReceived: $("#Qty").val(),
            Unit: $("#Unit").val(),
            Rate: $("#Rate").val(),
            StoreId: $("#stores2").val(),
            Amount: amount,
            TaxId: $("#Tax").val(),
            TaxAmount: tamount
        });
        sl = sl + 1;
        RenderLineItem();
        clearLineItemForm();
    }
}

function getAllProducts() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/ProductCreation/GetProducts",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            allProducts = data;

            var productNameHtml = "";
            var productCodeHtml = "";
            var barCodeHtml = "";

            $.each(allProducts, function (count, record) {
                productNameHtml += '<option value="' + record.ProductName + '">' + record.ProductName + '</option>';
                productCodeHtml += '<option value="' + record.ProductCode + '">' + record.ProductCode + '</option>';
                barCodeHtml += '<option value="' + record.barcode + '">' + record.barcode + '</option>';
            });
            $("#searchByProductName").html(productNameHtml);
            $("#searchByProductCode").html(productCodeHtml);
            $("#searchByBarcode").html(barCodeHtml);

            $("#searchByProductName").chosen({ width: "100%" });
            $("#searchByProductCode").chosen({ width: "100%" });
            $("#searchByBarcode").chosen({ width: "100%" });
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
            Utilities.Loader.Hide();
        }
    });
}

function changeQuantityValidation()
{
    Utilities.ErrorNotification("Cannot change quantity received; This Item was not Ordered");
}

function RenderLineItem() {
    totalLineItemAmount = 0.0;
    var output = "";
    console.log(LineItems);
    $.each(LineItems, function (count, row) {
        var changeQuantityStatus = "";
        // var rowUnit = (UnitList.find(p => p.unitId == parseInt($("#Unit").val())).unitName == undefined) ? "NA" : UnitList.find(p => p.unitId == 1).unitName;
        var unitObj = UnitList.find(p => p.unitId == parseInt($("#Unit").val()));
        var rowUnit = unitObj == undefined ? "NA" : unitObj.unitName;
        if (row.Quantity == 0)
        {
            changeQuantityStatus = '<button class="btn btn-sm btn-info" onclick="changeQuantityValidation()"><i class="fa fa-edit"></i></button>';
        }
        else
        {
            changeQuantityStatus = '<button class="btn btn-sm btn-info" onclick="changeQuantity(' + row.ProductId + ','+row.SL+' )"><i class="fa fa-edit"></i></button>';
        }

        var rowUnit = (UnitList.find(p => p.SLNO == 1).unitName == undefined) ? "NA" : UnitList.find(p => p.SLNO == 1).unitName; 
        //if (row.Quantity == 0)
        //{
        //    changeQuantityStatus = '<button class="btn btn-sm btn-info" onclick="changeQuantityValidation()"><i class="fa fa-edit"></i></button>';
        //}
        //else
        //{
        //    changeQuantityStatus = '<button class="btn btn-sm btn-info" onclick="changeQuantity(' + row.ProductId + ')"><i class="fa fa-edit"></i></button>';
        //}
        changeQuantityStatus = '<button class="btn btn-sm btn-info" onclick="changeQuantity(' + row.ProductId + ',' + row.SL +' )">Edit Store/Qty <i class="fa fa-edit"></i></button>';
        output +=
            '<tr>\
                <td style="white-space: nowrap;"><button onclick="confirmDelete(' + count + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>\
                <td style="white-space: nowrap;">'+ (count + 1) + '</td>\
                <td style="white-space: nowrap;">' + row.ProductCode + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductCode + '</td>\
                <td style="white-space: nowrap;">'+ row.ProductName + '</td>\
                <td style="white-space: nowrap;">' + (row.Description == null ? "N/A" : row.Description) + '</td>\
                <td style="white-space: nowrap;">'+ row.Quantity + '</td>\
                <td style="white-space: nowrap;">' + row.QtyReceived + '</td>\
                <td style="white-space: nowrap;">' + ((findStore(row.StoreId).godownName == null) ? "NA" : (findStore(row.StoreId).godownName)) + '</td>\
                <td style="white-space: nowrap;">' + rowUnit + '</td>\
                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.Rate) + '</td>\
                <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(row.Amount) + '</td>\
                <td>' + changeQuantityStatus + '</td>\
            </tr>\
            ';
        totalLineItemAmount = totalLineItemAmount + row.Amount;
    });

    console.log(totalLineItemAmount);
    $("#totalAmount").val(Utilities.FormatCurrency(totalLineItemAmount));
    $("#salesInvoiceLineItemTbody").html(output);
}

function removeLineItem(productId) {
    var indexOfObjectToRemove = salesInvoiceLineItems.findIndex(p => p.ProductId == productId);
    salesInvoiceLineItems.splice(indexOfObjectToRemove, 1);
    renderSalesInvoiceLineItems();
}

var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the positive lable as key
    //...
};

function confirmDelete(index) {
    deleteIndex = index;
    eModal.confirm(options).then(removeLineItem);
}

function removeLineItem() {
    LineItems.splice(deleteIndex, 1);
    RenderLineItem();
}

function findUnit(unitId) {
    var output = {};
    for (i = 0; i < UnitList.length; i++) {
        if (UnitList[i].unitId == unitId) {
            output = UnitList[i];
            break;
        }
    }
    return output;
}

function findBatch(batchId) {
    var output = {};
    for (i = 0; i < batches.length; i++) {
        if (batches[i].batchId == batchId) {
            output = batches[i];
            break;
        }
    }
    return output;
}

function findStore(storeId) {
    var output = {};
    for (i = 0; i < stores.length; i++) {
        if (stores[i].godownId == storeId) {
            output = stores[i];
            break;
        }
    }
    return output;
}

function findRack(rackId) {
    var output = {};
    for (i = 0; i < racks.length; i++) {
        if (racks[i].rackId == rackId) {
            output = racks[i];
            break;
        }
    }
    return output;
}
function findProduct(productId) {
    var output = {};
    for (i = 0; i < allProducts.length; i++) {
        if (allProducts[i].ProductId == productId) {
            output = allProducts[i];
            break;
        }
    }
    return output;
}
function clearLineItemForm() {
    $("#Barcode").val("");
    $("#Barcode").trigger("chosen:updated");
    $("#ProductCode").val("");
    $("#ProductCode").trigger("chosen:updated");
    $("#ProductName").val("");
    $("#ProductName").trigger("chosen:updated");
    $("#Description").val("");
    $("#stores2").val("");
    $("#stores2").trigger("chosen:updated");
    $("#Qty").val("");
    $("#Unit").val("");
    $("#Rate").val("");
    $("#Amount").val("");
    $("#Tax").val("");
    $("#TaxAmount").val("");
}
function clearForm() {
    //$("#DueDate").val("");
    //$("#DueDays").val("");
    $("#Currency").val("");
    $("#narration").val("");
    $("#billAmount").val("");
    $("#taxAmount").val("");
    $("#grandTotal").val("");
    $("#orderNo").val("");
    $("#orderNo").trigger("chosen:updated");
    LineItems = [];
}



//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;


//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}



function print() {
    var objToPrint = [];
    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();
    var thisEmail = $("#companyEmail").val();
    var thisPhone = $("#companyPhone").val();

    var Narration = $("#narration").val() == "" ? 'N/A' : $("#Narration").val();

    var thisOrderNo = $("#formNo").val();
    var thisDate = $("#transactionDate").val();
    //var thisCurency = $("#Currency").val();
    var totalAmt = $("#totalAmount").val();
    var thisSupplier = findSuppliers($("#Supplier").val()).ledgerName;



    $.each(LineItems, function (count, row) {
        objToPrint.push({
            "SlNo": count + 1,
            "Barcode": ((row.Barcode == undefined) ? "-" : (row.Barcode)),
            "ProducCode": row.ProductCode,
            "ProductName": row.ProductName,
            "WareHouse": (findStore(row.StoreId).godownName == null) ? "N/A" : (findStore(row.StoreId).godownName),
            "Qty": row.Quantity,
            "UnitName": row.Unit,
            "Rate": 'N' + Utilities.FormatCurrency(parseInt(row.Rate)),
            "Amount": 'N' + Utilities.FormatCurrency(row.Amount),
            "Descptn": row.Description == null ? "N/A" : row.Description
        });
    });

    columns = [
        { title: "S/N", dataKey: "SlNo" },
        { title: "Barcode", dataKey: "Barcode" },
        { title: "Product Code", dataKey: "ProducCode" },
        { title: "Product Name", dataKey: "ProductName" },
        { title: "WareHouse", dataKey: "WareHouse" },
        { title: "Qty", dataKey: "Qty" },
        { title: "Unit", dataKey: "UnitName" },
        { title: "Rate", dataKey: "Rate" },
        { title: "Description", dataKey: "Descptn" },
        { title: "Amount", dataKey: "Amount" },
    ];

    var doc = new jsPDF('portrait', 'pt', 'letter');
    doc.setDrawColor(0);

    //start drawing

    doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text(companyName, 320, 80, 'center');
    doc.setFontSize(9);
    doc.setFontType("normal");
    doc.text(companyAddress, 320, 90, 'center');



    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Email Id: ', 400, 150);
    doc.setFontType("normal");
    doc.text(thisEmail, 460, 150);
    doc.setFontType("bold");
    doc.text('Phone: ', 400, 165);
    doc.setFontType("normal");
    doc.text(thisPhone, 460, 165);

    doc.line(40, 170, 570, 170);

    doc.setFontType("bold");
    doc.setFontSize(9);
    doc.text('Date:', 40, 185);
    doc.setFontType("normal");
    doc.text(Utilities.FormatJsonDate(thisDate), 85, 185);
    doc.setFontType("bold");
    doc.text('Narration:', 40, 205);
    doc.setFontType("normal");
    doc.text(Narration, 85, 205);

    doc.setFontType("bold");
    doc.text('Order No: ', 400, 185);
    doc.setFontType("normal");
    doc.text(thisOrderNo, 463, 185);
    doc.setFontType("bold");
    doc.text('Supplier: ', 400, 205);
    doc.setFontType("normal");
    doc.text(thisSupplier, 463, 205);

    doc.line(40, 220, 570, 220);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text('Material Receipt', 240, 240);

    //doc.line(120, 20, 120, 60); horizontal line
    doc.setFontSize(8);
    doc.autoTable(columns, objToPrint, {
        startY: 255,
        theme: 'striped',
        styles: {
            fontSize: 8,
        },
        columnStyles: {
            SlNo: { columnWidth: 30, },
            Amount: { columnWidth: 80, halign: 'right' },
        },
    });

    doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Total Amount: NGN' + Utilities.FormatCurrency(totalAmt), 555, doc.autoTable.previous.finalY + 20, "right");

    doc.line(40, doc.autoTable.previous.finalY + 30, 570, doc.autoTable.previous.finalY + 30);

    doc.setFontType("bold");
    doc.text('Amount in Words: ', 40, doc.autoTable.previous.finalY + 60);
    doc.setFontType("normal");


    //remove commas
    var tempTotal = totalAmt.replace(/,/g, "");
    //convert to number
    var amtToBeConverted = parseInt(tempTotal);

    //capitalize Each Word
    var wordFormat = Utilities.NumberToWords(amtToBeConverted);
    const words = wordFormat.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());

    doc.text(words + ' Naira ONLY.', 120, doc.autoTable.previous.finalY + 60);

    doc.setFontType("bold");
    doc.text('Order approved by:  _________________________________', 40, doc.autoTable.previous.finalY + 100);

    doc.text('Done by:  ' + $("#nameSpan").html(), 350, doc.autoTable.previous.finalY + 100);
    doc.text('_________________', 390, doc.autoTable.previous.finalY + 103);
    doc.autoPrint();
    doc.save('Purchase_Order_Details.pdf');
    //window.open(doc.output('bloburl'));
    window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");
}