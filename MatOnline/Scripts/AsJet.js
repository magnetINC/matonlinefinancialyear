﻿$("#search").click(function () {

    var projectId = $("#projectCtrl").val();
    var storeId = $("#storeCtrl").val();
    var fromDate = $("#fromDate").val();
    var toDate = $("#toDate").val();
    loadReport(fromDate, toDate, projectId, storeId);
});
//(DateTime? fromDate , DateTime? toDate, int? batchId, int? storeId )

function loadReport(fromDate, toDate, projectId, storeId) {
    //debugger;
    /*"https://45.40.135.165:3789"*/
    $.ajax(API_BASE_URL + "/StockJournal/GetStockRecord/?fromDate=" + fromDate + "&toDate=" + toDate + "&projectId=" + projectId + "&storeId=" + storeId,
        {
            type: "get",
            contentType: "application/json",
            dataType: 'json',
            beforeSend: function () {
                Utilities.Loader.Show();
            },
            complete: function () {
                Utilities.Loader.Hide();
            },
            success: function (data) {


                //console.log(data);
                var html = "";
                $("#tblBodyHarvest").html("");
                html +=
                    `<tr><th style="white-space: nowrap;"  class="text-center"  colspan='17'>-------------------------------Harvest--------------------------------------</th>  </tr>`;

                data.forEach(function (a) {

                    if (a.StockMaster.extra1 == "Manufacturing") {
                        ////
                        if (projectId != 0) {
                            a.StockDetail = a.StockDetail.filter(a => {
                                var pid = (a.Project != null ||
                                    a.Project != undefined)
                                    ? a.Project.ProjectId
                                    : 0;
                                return pid == projectId;
                            });
                        }

                        if (storeId != 0) {
                            a.StockDetail = a.StockDetail.filter(c => c.Stock.godownId == storeId);
                        }

                        for (var i of a.StockDetail) {

                            if (i.Stock.consumptionOrProduction == "Consumption") {

                                if (a.StockMaster.stockJournalMasterId == i.Stock.stockJournalMasterId) {
                                    var addFeedConsumptionHeader = a.StockDetail.length == 0 ? '' : `<tr>
                                                                <th style="white-space: nowrap;" colspan='17'>Feeds/Raw Materials Consumption</th>  </tr>`;
                                    html += addFeedConsumptionHeader;
                                    break;
                                }
                            }
                        }

                        var totalstdcost = 0;
                        var totalRate = 0;
                        for (var b of a.StockDetail) {
                            if (b.Stock.consumptionOrProduction == "Consumption") {
                                //html += `<tr>
                                //   <th style="white-space: nowrap;" colspan='7'>Feed Consumption</th>  </tr>`;

                                var bomQty = b.BOM.quantity;
                                var itemProductionStockDetail = a.StockDetail.find(p => p.Stock.consumptionOrProduction == "Production");

                                var standardQuantity = itemProductionStockDetail == undefined ? "NA" : (bomQty * itemProductionStockDetail.Stock.qty);

                                if ("LOAF OF BREAD" == itemProductionStockDetail.Product.productName) {
                                  //  var dgs = itemProductionStockDetail.Stock.qty;
                                   // alert(bomQty + "   ysys " + standardQuantity + "    " + b.BOM.productId);
                                   
                                }

                                if (a.StockMaster.stockJournalMasterId == b.Stock.stockJournalMasterId)
                                    html +=
                                        `<tr>
                                                    <td style="white-space: nowrap;">${new Date(a.StockMaster.date).toDateString()}</td>
                                                    <td style="white-space: nowrap;">${a.StockMaster.invoiceNo}</td>
                                                    <td style="white-space: nowrap;">${a.StockMaster.extra2}</td>
                                                    <td style="white-space: nowrap;">${b.Product.productName}</td>
                                                    <td style="white-space: nowrap;">${b.Unit.unitName}</td>
                                                    <td style="white-space: nowrap;">${(b.Stock.rate).toFixed(2)}</td>
                                                    <td style="white-space: nowrap;">${(b.Batch != null || b.Batch != undefined
                                        )
                                            ? b.Batch.batchNo
                                            : "NA"}</td>
                                                    <td style="white-space: nowrap;">${(b.Project != null ||
                                            b.Project != undefined)
                                            ? b.Project.ProjectName
                                            : "NA"}</td>
                                                                    <td style="white-space: nowrap;">${(b.Category != null ||
                                            b.Category != undefined)
                                            ? b.Category.CategoryName
                                            : "NA"}</td>
                                                                    <td style="white-space: nowrap;">${(b.Store != null ||
                                            b.Store != undefined)
                                            ? b.Store.godownName
                                            : "NA"}</td>
                                                                    <td style="white-space: nowrap;">${(b.Stock.qty).toFixed(2)}</td>
                                                                    <td style="white-space: nowrap;">${standardQuantity}</td>
                                                                    <td style="white-space: nowrap;">${(standardQuantity - b.Stock.qty).toFixed(2)}</td>
                                                                    <td style="white-space: nowrap;">${(b.Stock.rate * b.Stock.qty).toFixed(2)}</td>
                                                                    <td style="white-space: nowrap;">${(b.Stock.rate * standardQuantity).toFixed(2)}</td>
                                                                    <td style="white-space: nowrap;">${((b.Stock.rate * standardQuantity) - (b.Stock.rate * b.Stock.qty)).toFixed(2)}</td>
                                                </tr>`;

                                totalstdcost += b.Stock.rate * standardQuantity;

                                totalRate += b.Stock.rate * b.Stock.qty;
                            }

                            if (b.Stock.consumptionOrProduction == "Production") {
                                if (a.StockMaster.stockJournalMasterId == b.Stock.stockJournalMasterId)
                                    if (b.Stock.rate <= 0)
                                    {
                                        b.Stock.rate = totalRate / itemProductionStockDetail.Stock.qty;

                                        var toUpdate = {
                                            Rate: b.Stock.rate,
                                            JMasterId: b.Stock.stockJournalMasterId,
                                            JMasterDetailsId: b.Stock.stockJournalDetailsId,
                                        };
                                       
                                        $.ajax({
                                            url: API_BASE_URL + "/StockJournal/Update",
                                            type: "Post",
                                            data: JSON.stringify(toUpdate),
                                            contentType: "application/json",
                                            success: function (data)
                                            {
                                                
                                            },
                                            error: function (err)
                                            {
                                               
                                            }
                                        });

                                    }
                                    else {
                                       
                                        b.Stock.rate = b.Stock.rate;
                                    }
                                    html += `<tr>
                                                                <th style="white-space: nowrap;" colspan='17'>Item Produced</th>  </tr>`;
                                html += `<tr>
                                                    <td style="white-space: nowrap;">${new Date(a.StockMaster.extraDate).toDateString()}</td>
                                                    <td style="white-space: nowrap;">${a.StockMaster.invoiceNo}</td>
                                                    <td style="white-space: nowrap;">${a.StockMaster.extra2}</td>
                                                                    <td style="white-space: nowrap;">${b.Product.productName}</td>
                                                                    <td style="white-space: nowrap;">${b.Unit.unitName}</td>
                                                                    <td style="white-space: nowrap;">${(b.Stock.rate).toFixed(2)}</td>
                                                                  <td style="white-space: nowrap;">${(b.Batch != null ||
                                        b.Batch != undefined
                                    )
                                        ? b.Batch.batchNo
                                        : "NA"}</td>
                                                    <td style="white-space: nowrap;">${(b.Project != null ||
                                        b.Project != undefined)
                                        ? b.Project.ProjectName
                                        : "NA"}</td>
                                                                    <td style="white-space: nowrap;">${(b.Category != null ||
                                        b.Category != undefined)
                                        ? b.Category.CategoryName
                                        : "NA"}</td>
                                                                    <td style="white-space: nowrap;">
                                                               ${(b.Store != null ||
                                        b.Store != undefined)
                                        ? b.Store.godownName
                                        : "NA"}
                                                                  </td>
                                                                    <td style="white-space: nowrap;">${(b.Stock.qty).toFixed(2)}</td>
                                                                    <td style="white-space: nowrap;">-</td>
                                                                    <td style="white-space: nowrap;">-</td>
                                                                    <td style="white-space: nowrap;">${(b.Stock.rate).toFixed(2)}</td>
                                                                    <td style="white-space: nowrap;">${(totalstdcost / b.Stock.qty).toFixed(2)}</td>
                                                                    <td style="white-space: nowrap;">${((totalstdcost / b.Stock.qty) - b.Stock.rate).toFixed(2)}</td>
                                                                </tr>`;
                            }
                        }

                        //start ex
                        if (a.StockDetail.length != 0 && a.AdditionalCosts.length != 0) {
                            html += `<tr>
                                                                <th style="white-space: nowrap;" colspan='7'>Additional Costs</th>  </tr>
                                                                <tr>
                                                                <th style="white-space: nowrap;">Date</th>
                                                                <th style="white-space: nowrap;">Ac/Ledger</th>
                                                                <th style="white-space: nowrap;">Amount</th>
                                                                <th style="white-space: nowrap;">Project</th>
                                                                <th style="white-space: nowrap;">Department</th></tr>`;
                            a.AdditionalCosts.forEach(function (item) {
                                html += `<tr>
                                                    <td style="white-space: nowrap;">${new Date(item.AdditionalCost.extraDate).toDateString()}</td>
                                                    <td style="white-space: nowrap;">${item.AccountLedgerName}</td>
                                                    <td style="white-space: nowrap;">${item.AdditionalCost.debit}</td>
                                                  <td style="white-space: nowrap;">${item.Project == undefined ? "NA" : item.Project.ProjectName}</td>
                                                    <td style="white-space: nowrap;">${item.Category == undefined ? "NA" : item.Category.CategoryName}</td>
                                                     </tr>`;
                            })
                        }
                        if (a.StockDetail.length != 0)
                            html += '<tr style="border-bottom: 2px solid #4d627b;"><td colspan="17">   </td> </tr>';
                        //end ex
                    }

                });
                //Stock Out Reports
                for (var r of data) {
                    if (r.StockMaster.extra1 == "Stock Out") {
                        html +=
                            `<tr><th style="white-space: nowrap;"  class="text-center"  colspan='9'>-------------------------------${r.StockMaster.extra1}--------------------------------------</th>  </tr>`;
                        break;
                    }
                }
                data.forEach(function (a) {

                    if (a.StockMaster.extra1 == "Stock Out") {
                        if (projectId != 0) {
                            a.StockDetail = a.StockDetail.filter(a => {
                                var pid = (a.Project != null ||
                                    a.Project != undefined)
                                    ? a.Project.ProjectId
                                    : 0;
                                return pid == projectId;
                            });
                        }

                        if (storeId != 0) {
                            a.StockDetail = a.StockDetail.filter(c => c.Stock.godownId == storeId);
                        }
                        for (var i of a.StockDetail) {

                            if (i.Stock.consumptionOrProduction == "Consumption") {

                                if (a.StockMaster.stockJournalMasterId == i.Stock.stockJournalMasterId) {
                                    html += `<tr>
                                                                <th style="white-space: nowrap;" colspan='9'>Feeds/Raw Materials Consumption</th>  </tr>`;
                                    break;
                                }
                            }
                        }

                        for (var b of a.StockDetail) {
                            debugger;

                            if (b.Stock.consumptionOrProduction == "Consumption") {
                                //html += `<tr>
                                //    <th style="white-space: nowrap;" colspan='7'>Feed Consumption</th>  </tr>`;
                                if (a.StockMaster.stockJournalMasterId == b.Stock.stockJournalMasterId)

                                    html +=
                                        `<tr>
                                                        <td style="white-space: nowrap;">${new Date(a.StockMaster.extraDate).toDateString()}</td>
                                                    <td style="white-space: nowrap;">${a.StockMaster.invoiceNo}</td>
                                                    <td style="white-space: nowrap;">${a.StockMaster.extra2}</td>

                                                        <td style="white-space: nowrap;">${b.Product.productName}</td>
                                                      <td style="white-space: nowrap;"> ${(b.Unit != null || b.Unit != undefined
                                        )
                                            ? b.Unit.unitName
                                            : "NA"}</td>
                                                        <td style="white-space: nowrap;">${(b.Stock.rate).toFixed(2)}</td>
                                                       <td style="white-space: nowrap;">${(b.Batch != null || b.Batch != undefined
                                        )
                                            ? b.Batch.batchNo
                                            : "NA"}</td>
                                                        <td style="white-space: nowrap;">${(b.Project != null ||
                                            b.Project != undefined)
                                            ? b.Project.ProjectName
                                            : "NA"}</td>
                                                                        <td style="white-space: nowrap;">${(b.Category != null ||
                                            b.Category != undefined)
                                            ? b.Category.CategoryName
                                            : "NA"}</td>
                                                                        <td style="white-space: nowrap;">${(b.Store != null ||
                                            b.Store != undefined)
                                            ? b.Store.godownName
                                            : "NA"}</td>
                                                                        <td style="white-space: nowrap;">${(b.Stock.qty).toFixed(2)}</td>
                                                                        <td style="white-space: nowrap;"></td>
                                                    </tr>`;
                            }
                            if (b.Stock.consumptionOrProduction == "Production") {
                                if (a.StockMaster.stockJournalMasterId == b.Stock.stockJournalMasterId)
                                    if (finishedGoodsIdList.includes(b.Product.productId))
                                        html += `<tr>
                                                                <th style="white-space: nowrap;" colspan='9'>Item Produced</th>  </tr>`;
                                    else if (!rawMaterialsIdList.includes(b.Product.productId))
                                        html += `<tr>
                                                                <th style="white-space: nowrap;" colspan='9'>Other Items</th>  </tr>`;
                                //html += `<tr>
                                //  <th style="white-space: nowrap;" colspan='7'>Item Production</th>  </tr>`;
                                html += `<tr>
                                                        <td style="white-space: nowrap;">${new Date(a.StockMaster.extraDate).toDateString()}</td>
                                                                        <td style="white-space: nowrap;">${a.StockMaster.invoiceNo}</td>
                                                                        <td style="white-space: nowrap;">${a.StockMaster.extra2}</td>
                                                                        <td style="white-space: nowrap;">${b.Product.productName}</td>
                                                                        <td style="white-space: nowrap;">${b.Unit.unitName}</td>
                                                                        <td style="white-space: nowrap;">${(b.Stock.rate).toFixed(2)}</td>
                                                                     <td style="white-space: nowrap;">${(b.Batch != null ||
                                        b.Batch != undefined
                                    )
                                        ? b.Batch.batchNo
                                        : "NA"}</td>
                                                        <td style="white-space: nowrap;">${(b.Project != null ||
                                        b.Project != undefined)
                                        ? b.Project.ProjectName
                                        : "NA"}</td>
                                                                        <td style="white-space: nowrap;">${(b.Category != null ||
                                        b.Category != undefined)
                                        ? b.Category.CategoryName
                                        : "NA"}</td>
                                                                        <td style="white-space: nowrap;">${(b.Store != null ||
                                        b.Store != undefined)
                                        ? b.Store.godownName
                                        : "NA"}</td>
                                                        <td style="white-space: nowrap;">${(b.Stock.qty).toFixed(2)}</td>
                                                                        <td style="white-space: nowrap;"></td>
                                                                    </tr>`;
                            }
                        }
                        //start ex
                        if (a.StockDetail.length != 0 && a.AdditionalCosts.length != 0) {
                            html += `<tr>
                                                                <th style="white-space: nowrap;" colspan='7'>Additional Costs</th>  </tr>
                                                                <tr>
                                                                <th style="white-space: nowrap;">Date</th>
                                                                <th style="white-space: nowrap;">Ac/Ledger</th>
                                                                <th style="white-space: nowrap;">Amount</th>
                                                                <th style="white-space: nowrap;">Project</th>
                                                                <th style="white-space: nowrap;">Department</th></tr>`;
                            a.AdditionalCosts.forEach(function (item) {
                                html += `<tr>
                                                    <td style="white-space: nowrap;">${new Date(item.AdditionalCost.extraDate).toDateString()}</td>
                                                    <td style="white-space: nowrap;">${item.AccountLedgerName}</td>
                                                    <td style="white-space: nowrap;">${item.AdditionalCost.debit}</td>
                                                  <td style="white-space: nowrap;">${item.Project == undefined ? "NA" : item.Project.ProjectName}</td>
                                                    <td style="white-space: nowrap;">${item.Category == undefined ? "NA" : item.Category.CategoryName}</td>
                                                     </tr>`;
                            })
                        }
                        if (a.StockDetail.length != 0)
                            html += '<tr style="border-bottom: 2px solid #4d627b;"><td colspan="15">   </td> </tr>';
                        //end ex
                    }

                });


                //Other Reports
                for (var rr of data) {
                    if (rr.StockMaster.extra1 == "Stock Transfer") {

                        html +=
                            `
                        tr><th style="white-space: nowrap;"  class="text-center"  colspan='9'>-----------------------------${rr.StockMaster.extra1}-------------------------------------</th>  </tr>
                        `;
                        break;
                    }

                }
                data.forEach(function (a) {
                    var transferFromHeading = false;
                    if (a.StockMaster.extra1 == "Stock Transfer") {
                        if (projectId != 0) {
                            a.StockDetail = a.StockDetail.filter(a => {
                                var pid = (a.Project != null ||
                                    a.Project != undefined)
                                    ? a.Project.ProjectId
                                    : 0;
                                return pid == projectId;
                            });
                        }

                        if (storeId != 0) {
                            a.StockDetail = a.StockDetail.filter(c => c.Stock.godownId == storeId);
                        }

                        for (var i of a.StockDetail) {

                            if (i.Stock.consumptionOrProduction == "Consumption") {

                                if (a.StockMaster.stockJournalMasterId == i.Stock.stockJournalMasterId) {
                                    html += `<tr>
                                                                <th style="white-space: nowrap;" colspan='9'>Trasfer From</th>  </tr>`;
                                    break;
                                }
                            }
                        }

                        //transfer from

                        for (var b of a.StockDetail) {
                            if (b.Stock.consumptionOrProduction == "Consumption") {
                                //html += `<tr>
                                //    <th style="white-space: nowrap;" colspan='7'>Feed Consumption</th>  </tr>`;
                                if (a.StockMaster.stockJournalMasterId == b.Stock.stockJournalMasterId)
                                    html +=
                                        `
                                                  <tr>
                                                    <td style="white-space: nowrap;">${new Date(a.StockMaster.extraDate).toDateString()}</td>
                                                    <td style="white-space: nowrap;">${a.StockMaster.invoiceNo}</td>
                                                    <td style="white-space: nowrap;">${a.StockMaster.extra2}</td>
                                                    <td style="white-space: nowrap;">${b.Product.productName}</td>
                                                  <td style="white-space: nowrap;"> ${(b.Unit != null || b.Unit != undefined
                                        )
                                            ? b.Unit.unitName
                                            : "NA"}</td>
                                                    <td style="white-space: nowrap;">${b.Stock.rate}</td>
                                                    <td style="white-space: nowrap;">${(b.Batch != null || b.Batch != undefined
                                        )
                                            ? b.Batch.batchNo
                                            : "NA"}</td>
                                                    <td style="white-space: nowrap;">${(b.Project != null ||
                                            b.Project != undefined)
                                            ? b.Project.ProjectName
                                            : "NA"}</td>
                                                                    <td style="white-space: nowrap;">${(b.Category != null ||
                                            b.Category != undefined)
                                            ? b.Category.CategoryName
                                            : "NA"}</td>
                                                                    <td>${(b.Stock.Store != null ||
                                            b.Store != undefined)
                                            ? b.Store.godownName
                                            : "NA"} </td>
                                                    <td style="white-space: nowrap;">${(b.Stock.qty).toFixed(2)}</td>
                                                                    <td style="white-space: nowrap;"></td>
                                                </tr>`;

                            }

                            if (b.Stock.consumptionOrProduction == "Production") {
                                if (a.StockMaster.stockJournalMasterId == b.Stock.stockJournalMasterId) {
                                    if (transferFromHeading == false) {
                                        html += `<tr>
                                                                <th style="white-space: nowrap;" colspan='9'>Transfer To</th>  </tr>`;
                                        transferFromHeading = true;
                                    }

                                    html += `<tr>
                                                    <td style="white-space: nowrap;">${new Date(a.StockMaster.extraDate).toDateString()}</td>
                                                                    <td style="white-space: nowrap;">${a.StockMaster.invoiceNo}</td>
                                                                    <td style="white-space: nowrap;">${a.StockMaster.extra2}</td>
                                                                    <td style="white-space: nowrap;">${b.Product.productName}</td>
                                                                    <td style="white-space: nowrap;">${b.Unit.unitName}</td>
                                                                    <td style="white-space: nowrap;">${(b.Stock.rate).toFixed(2)}</td>
                                                             <td style="white-space: nowrap;">${(b.Batch != null || b.Batch != undefined
                                        )
                                            ? b.Batch.batchNo
                                            : "NA"}</td>
                                                    <td style="white-space: nowrap;">${(b.Project != null ||
                                            b.Project != undefined)
                                            ? b.Project.ProjectName
                                            : "NA"}</td>
                                                                    <td style="white-space: nowrap;">${(b.Category != null ||
                                            b.Category != undefined)
                                            ? b.Category.CategoryName
                                            : "NA"}</td>
                                                                    <td style="white-space: nowrap;">${(b.Stock.Store != null ||
                                            b.Store != undefined)
                                            ? b.Store.godownName
                                            : "NA"}</td>
                                                                    <td style="white-space: nowrap;">${(b.Stock.qty).toFixed(2)}</td>
                                                                    <td style="white-space: nowrap;"></td>
                                                                </tr>`;
                                }
                            }
                        }
                        //start ex
                        if (a.StockDetail.length != 0 && a.AdditionalCosts.length != 0) {
                            html += `<tr>
                                                                <th style="white-space: nowrap;" colspan='7'>Additional Costs</th>  </tr>
                                                                <tr>
                                                                <th style="white-space: nowrap;">Date</th>
                                                                <th style="white-space: nowrap;">Ac/Ledger</th>
                                                                <th style="white-space: nowrap;">Amount</th>
                                                                <th style="white-space: nowrap;">Project</th>
                                                                <th style="white-space: nowrap;">Department</th></tr>`;
                            a.AdditionalCosts.forEach(function (item) {
                                html += `<tr>
                                                    <td style="white-space: nowrap;">${new Date(item.AdditionalCost.extraDate).toDateString()}</td>
                                                    <td style="white-space: nowrap;">${item.AccountLedgerName}</td>
                                                    <td style="white-space: nowrap;">${item.AdditionalCost.debit}</td>
                                                  <td style="white-space: nowrap;">${item.Project == undefined ? "NA" : item.Project.ProjectName}</td>
                                                    <td style="white-space: nowrap;">${item.Category == undefined ? "NA" : item.Category.CategoryName}</td>
                                                     </tr>`;
                            })
                        }
                        if (a.StockDetail.length != 0)
                            html += '<tr style="border-bottom: 2px solid #4d627b;"><td colspan="17">   </td> </tr>';
                        //end ex
                    }

                });
                $("#tblBodyHarvest").html(html);
            }

        });
    ;

}