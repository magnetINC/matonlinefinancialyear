﻿var users = [];
var roles = [];
var stores = [];
var table = "";
var userToBlock = {};

$(function () {    
    
    renderGrid();

    $("#createUser").click(function () {
        if ($("#username").val() == "")
        {
            Utilities.ErrorNotification("Username can't be blank!");
            return;
        }
        else if ($("#password").val() != $("#confPassword").val())
        {
            Utilities.ErrorNotification("Password do not match!");
            return;
        }
        else if ($("#fname").val() == "")
        {
            Utilities.ErrorNotification("First name can't be blank!");
            return;
        }
        else if ($("#lname").val() == "") {
            Utilities.ErrorNotification("Last name can't be blank!");
            return;
        }
        else if ($("#role").val() == "") {
            Utilities.ErrorNotification("Select user role!");
            return;
        }
        else if ($("#store").val() == "") {
            Utilities.ErrorNotification("Select user store!");
            return;
        }
        else
        {
            var userToSave = {
                Username: $("#username").val(),
                Password: $("#password").val(),
                FirstName: $("#fname").val(),
                LastName: $("#lname").val(),
                StoreId: $("#store").val(),
                Active: $("#active").val() == "Active" ? true : false,
                RoleId: $("#role").val(),
                Narration: $("#narration").val()
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/UserCreation/AddUser",
                type: "Post",
                data: JSON.stringify(userToSave),
                contentType: "application/json",
                success: function (data) {
                    //console.log(data);
                    $("#returnedUserId").val(data);
                    renderGrid();
                    Utilities.SuccessNotification("User created!");

                    if ($("#createsaleman").prop("checked") == true) {
                        createsalesman(data);
                    }

                    $("#username").val("");
                    $("#password").val("");
                    $("#fname").val("");
                    $("#lname").val("");
                    $("#narration").val("");

                    
                    setTimeout(function () {

                        $("#frmSignatureImage").submit();

                        Utilities.Loader.Hide();

                    }, 3000);

                },
                error: function (err) {
                    Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                    Utilities.Loader.Hide();
                }
            });
        }
    });

    function createsalesman(userid)
    {
        var salesManToSave = {
            UserInfo: {
                FirstName: $("#fname").val(),
                LastName: $("#lname").val(),
                UserName: $("#username").val(),
                RoleId: $("#role").val(),
                PhoneNumber: '',
                MobileNumber: '',
                Active: $("#active").val() == "Active" ? true : false,
                Address: $("#store").html(),
                Password: $("#password").val(),
                Narration: "Created the salesman from user creation automatically UserId : " + userid
            },
            EmployeeInfo: {
                EmployeeName: $("#fname").val() + " " + $("#lname").val(),
                EmployeeCode: $("#username").val(),
                Email: '',
                RoleId: $("#role").val(),
                PhoneNumber: '',
                MobileNumber: '',
                IsActive: $("#active").val() == "Active" ? true : false,
                Address: $("#store").html(),
                Narration: "Created the salesman from user creation automatically UserId : " + userid
            }
        }

        $.ajax({
            url: API_BASE_URL + "/Pos/SaveSalesman",
            type: 'POST',
            data: JSON.stringify(salesManToSave),
            contentType: "application/json",
            success: function (data) {
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Salesman: " +data.ResponseMessage);
                }
                else if (data.ResponseCode == 403) {
                    Utilities.ErrorNotification("Salesman: " + data.ResponseMessage);
                }
            },
            error: function (request, error) {
                Utilities.ErrorNotification("Error Creating User as Salesman");
            }
        });

    }


    $("#signatureImage").change(function () {
        readURL(this);
    });

    $("#saveChanges").click(function () {

        if ($("#editpassword").val() != $("#editconfPassword").val()) {
            Utilities.ErrorNotification("Password do not match!");
            return;
        }
        else {

            var userToEdit = {
                UserId: $("#editUserId").val(),
                Username: $("#editUsername").val(),
                FirstName: $("#editFirstName").val(),
                LastName: $("#editLastName").val(),
                StoreId: $("#editStore").val(),
                RoleId: $("#editRole").val(),
                Active: true,
                Password: $("#editpassword").val()
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/UserCreation/EditUser",
                type: "Post",
                data: JSON.stringify(userToEdit),
                contentType: "application/json",
                success: function (data) {
                    $("#editModal").modal("hide");
                    if (data == "SUCCESS") {
                        Utilities.SuccessNotification("User details updated!");
                        renderGrid();
                    }
                    else {
                        Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                    }
                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.ErrorNotification("Oops! Sorry, something went wrong.");
                    Utilities.Loader.Hide();
                }
            });

        }

    });


});

function renderGrid()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/UserCreation/GetAllUses",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            users = data.Users;
            roles = data.Roles;
            stores = data.Stores;
            $("#store").html(Utilities.PopulateDropDownFromArray(stores, 0, 1));
            $("#editStore").html(Utilities.PopulateDropDownFromArray(stores, 0, 1));

            populateRoles();
            if (table != "")
            {
                table.destroy();
            }

            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors.
            setTimeout(onSuccess, 500, data);

           
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function onSuccess() {
    var objToShow = [];
    var editButton = "";
    for (i = 0; i < users.length; i++) {
        var roleObj = roles.find(p=>p.roleId == users[i].roleId);
        var roleName = roleObj == undefined? "NA" : roleObj.role;
        var storeObj = stores.find(p=>p.godownId == users[i].storeId);
        var storeName = storeObj == undefined? "NA" : storeObj.godownName;
        objToShow.push([
            users[i].userName,
            users[i].firstName + " " + users[i].lastName,
            roleName,
            storeName,
            (users[i].active == true) ? '<span><b>Active</b></span>' : '<span style="color:#e79824;"><b>Inactive</b></span>',
            '<button class="btn btn-primary btn-labeled fa fa-edit" onclick="editUserClick(' + users[i].userId + ');">Edit</button>',
            //'<button class="btn btn-mint btn-labeled fa fa-refresh" onclick="deleteUserClick(' + users[i].userId + ');">Reset Password</button>',
                (users[i].active == true) ? '<button class="btn btn-warning btn-labeled fa fa-ban" onclick="blockUserClick(' + users[i].userId + ');">Block &nbsp;&nbsp;&nbsp;&nbsp;</button>' :
                                         '<button class="btn btn-success btn-labeled fa fa-unlock" onclick="blockUserClick(' + users[i].userId + ');">Unblock</button>',
            '<button class="btn btn-danger btn-labeled fa fa-times" onclick="deleteUserClick(' + users[i].userId + ');">Delete</button>'
        ]);
    }
    var count = 0;
    if (checkPriviledge("frmUserCreation", "Update") === false) {
        objToShow.forEach(function (item) {
            item.splice(5, 2);
        });
        count+= 2;
    }
    if (checkPriviledge("frmUserCreation", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 7 - count;
            item.splice(recalculateIndex, 1);
        });
    }

    table = $('#usersTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

    Utilities.Loader.Hide();
}

function populateRoles()
{
    var output = "<option>-Select Role-</option>";
    $.each(roles, function (count, record) {
        output += '<option value="'+record.roleId+'">'+record.role+'</option>';
    });
    $("#role").html(output);
    $("#editRole").html(output);
}

function editUserClick(id) {    
    var data = users.find(p=>p.userId==id);
    $("#editUserId").val(data.userId);
    $("#editUsername").val(data.userName);
    $("#editFirstName").val(data.firstName);
    $("#editLastName").val(data.lastName);
    $("#editStore").val(data.storeId);
    $("#editStore :selected").text(showStore(data.storeId));
    $("#editRole").val(data.roleId);
    $("#editRole :selected").text(data.role);
    $("#editModal").modal("show");
}
function blockUserClick2(e) {    
    var data = users.find(p=>p.userId==id);
    user = data;

    $("#userBlockModal").modal("show");
    console.log(user);
}

function blockUserClick(id) {
    var user = users.find(p=>p.userId == id);

    userToBlock.userId = user.userId;
    //var narration = $("#userNarration").val();
    if (user.active)
    {
        $("#blockButton").html("Block");
        $("#blockUserTitle").html("Block User");
    }
    else
    {
        $("#blockButton").html("Unblock");
        $("#blockUserTitle").html("Unblock User");
    }

    $("#userBlockModal").modal("show");
}

function confirmblock(user)
{
    Utilities.Loader.Show();
    
    userToBlock.active = $("#userStatus").val();
    userToBlock.narration = $("#userNarration").val();

    $.ajax({
        url: API_BASE_URL + "/UserCreation/BlockUserAccount",
        type: "POST",
        data: JSON.stringify(userToBlock),
        contentType: "application/json",
        success: function (data) {
            if (data == true) {
                $("#userBlockModal").modal("hide");
                Utilities.SuccessNotification("User account blocked!");
            }
            else {
                Utilities.ErrorNotification("Sorry, try again");
            }
            renderGrid();
            //Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function deleteUserClick(id) {
    var user = users.find(p=>p.userId == id);

    var userToDelete = {
        UserId: user.userId
    };
    if(confirm("Do you really want to delete?"))
    {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/UserCreation/DeleteUser",
            type: "Post",
            data: JSON.stringify(userToDelete),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data == true)  //user has no transaction referenced and has been deleted
                {
                    Utilities.SuccessNotification("User deleted!");
                    renderGrid();
                }
                else
                {
                    Utilities.ErrorNotification("Transaction reference exist for this user.");
                    Utilities.Loader.Hide();
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}

function readURL(input) {
    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#imgSig').attr('src', e.target.result);
        }

        reader.readAsDataURL(input.files[0]);
    }
}

function showStore(storeId)
{
    if(storeId==0)
    {
        return "NA";
    }
    else
    {
        var storeName = stores.find(p=>p.godownId == storeId).godownName;
        if(storeName==undefined || storeName==null)
        {
            return "NA";
        }
        else
        {
            return storeName;
        }
    }
}