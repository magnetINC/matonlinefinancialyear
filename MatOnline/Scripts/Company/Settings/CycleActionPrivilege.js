﻿var roles = [];
var cycleActions = [];

Utilities.Loader.Show();
var rolesAjx = $.ajax({
    url: API_BASE_URL + "/Role/GetRolesForPriviledges",
    type: "GET",
    contentType: "application/json"
});

var cycleActionAjx = $.ajax({
    url: API_BASE_URL + "/RolePriviledgeSettings/LoadCycleActionPrivilege",
    type: "GET",
    contentType: "application/json"
});

$(function () {
    
    $.when(rolesAjx, cycleActionAjx)
    .done(function (dataRoles, dataCycleAction) {

        roles = dataRoles[2].responseJSON;
        $("#stockControl").html('<option value="">-Select-</option>' + Utilities.PopulateDropDownFromArray(roles, 0, 1));
        $("#generalManager").html('<option value="">-Select-</option>' + Utilities.PopulateDropDownFromArray(roles, 0, 1));
        $("#salesRep").html('<option value="">-Select-</option>' + Utilities.PopulateDropDownFromArray(roles, 0, 1));
        $("#stockOfficer").html('<option value="">-Select-</option>' + Utilities.PopulateDropDownFromArray(roles, 0, 1));
        $("#warehouseManager").html('<option value="">-Select-</option>' + Utilities.PopulateDropDownFromArray(roles, 0, 1));

        cycleActions = dataCycleAction[2].responseJSON;
        for (i = 0; i < cycleActions.length; i++) {
            if (cycleActions[i].CycleAction == "General Manager" && cycleActions[i].RoleId != null) {
                $("#generalManager").val(cycleActions[i].RoleId);
                $("#generalManager :selected").text(roles.find(p=>p.roleId == cycleActions[i].RoleId).role);
            }
            if (cycleActions[i].CycleAction == "Stock Control" && cycleActions[i].RoleId != null) {
                $("#stockControl").val(cycleActions[i].RoleId);
                $("#stockControl :selected").text(roles.find(p=>p.roleId == cycleActions[i].RoleId).role);
            }
            if (cycleActions[i].CycleAction == "Sales Representative" && cycleActions[i].RoleId != null) {
                $("#salesRep").val(cycleActions[i].RoleId);
                $("#salesRep :selected").text(roles.find(p=>p.roleId == cycleActions[i].RoleId).role);
            }
            if (cycleActions[i].CycleAction == "Stock Officer" && cycleActions[i].RoleId != null) {
                $("#stockOfficer").val(cycleActions[i].RoleId);
                $("#stockOfficer :selected").text(roles.find(p=>p.roleId == cycleActions[i].RoleId).role);
            }
        }

        Utilities.Loader.Hide();
    });
    
    $("#btnSaveCycleRestriction").click(function () {

        if ($("#stockControl").val() == "")
        {
            Utilities.ErrorNotification("Please specify role for stock control");
            return;
        }
        if ($("#generalManager").val() == "")
        {
            Utilities.ErrorNotification("Please specify role for general manager");
            return;
        }
        if ($("#salesRep").val() == "")
        {
            Utilities.ErrorNotification("Please specify role for sales representative");
            return;
        }
        if ($("#stockOfficer").val() == "")
        {
            Utilities.ErrorNotification("Please specify role for stock officer");
            return;
        }
        if ($("#warehouseManager").val() == "")
        {
            Utilities.ErrorNotification("Please specify role for warehouse manager");
            return;
        }
        


        var toSave = [];
        toSave.push({
            CycleAction: "Stock Control",
            RoleId: $("#stockControl").val()
        });
        toSave.push({
            CycleAction: "General Manager",
            RoleId: $("#generalManager").val()
        });
        toSave.push({
            CycleAction: "Sales Representative",
            RoleId: $("#salesRep").val()
        });
        toSave.push({
            CycleAction: "Stock Officer",
            RoleId: $("#stockOfficer").val()
        });
        toSave.push({
            CycleAction: "Warehouse Manager",
            RoleId: $("#warehouseManager").val()
        });

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/RolePriviledgeSettings/SaveCycleActionPrivilege",
            type: "POST",
            data:JSON.stringify(toSave),
            contentType: "application/json",
            success: function (data) {
                Utilities.SuccessNotification("Settings saved!");
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.ErrorNotification("Sorry, something went wrong.");
                Utilities.Loader.Hide();
            }
        });
    });
});