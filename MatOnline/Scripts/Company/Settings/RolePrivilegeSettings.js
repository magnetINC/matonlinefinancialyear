﻿var toSave = [];
var roles = [];
var rolePriviledges = [];
var selectedRolePriviledges = [];

var rolesAjax = $.ajax({
    url: API_BASE_URL + "/Role/GetRolesForPriviledges",
    type: "GET",
    contentType: "application/json"
});

var rolePriviledgesAjax = $.ajax({
    url: API_BASE_URL + "/RolePriviledgeSettings/LoadSettings",
    type: "GET",
    contentType: "application/json"
});

$(function () {
    
    $('input[type="checkbox"]').click(function () {
        if ($(this).prop("checked") == true) {
            var current = $(this).attr("id");
            if (current.indexOf("-All") > 0)
            {
                var fullId = $(this).attr("id");
                var strippedId = fullId.replace("-All", "");
                $("#" + strippedId + "-Save").prop("checked", true);
                $("#" + strippedId + "-Update").prop("checked", true);
                $("#" + strippedId + "-Delete").prop("checked", true);
                $("#" + strippedId + "-View").prop("checked", true);
            }
        }
        else if ($(this).prop("checked") == false)
        {
            var current = $(this).attr("id");
            if (current.indexOf("-All") > 0) {
                var fullId = $(this).attr("id");
                var strippedId = fullId.replace("-All", "");
                $("#" + strippedId + "-Save").prop("checked", false);
                $("#" + strippedId + "-Update").prop("checked", false);
                $("#" + strippedId + "-Delete").prop("checked", false);
                $("#" + strippedId + "-View").prop("checked", false);
            }
        }
    });
    $("input:checkbox").each(function () {
        //$(this).prop("checked", false);
        if ($(this).prop("checked"))
        {
            //$("#frmCustomer-Save").prop("checked", true);
            var current = $(this).attr("id");
            if (current.indexOf("-All") > 0) {
                var fullId = $(this).attr("id");
                var strippedId = fullId.replace("-All", "");
                $("#"+strippedId + "-Save").prop("checked", true);
                $("#" + strippedId + "-Update").prop("checked", true);
                $("#" + strippedId + "-Delete").prop("checked", true);
                $("#" + strippedId + "-View").prop("checked", true);
                // console.log(current);
            }
        }
              
    });

    $("#btnSave").click(function () {
        if ($("#role").val() == "")
        {
            Utilities.ErrorNotification("Please select role!");
            return;
        }
        else
        {
            toSave = [];
            $("input:checkbox").each(function () {

                if ($(this).prop("checked")) {
                    //console.log($(this).attr("class"));
                    toSave.push({
                        RoleId: $("#role").val(),
                        FormName: getFormName($(this).attr("id")),
                        Action: getAction($(this).attr("id"))
                    });
                }
            });
            if (toSave.length < 1)
            {
                Utilities.ErrorNotification("Please selected role has no priviledge assigned!");
                return;
            }
            else
            {
                Utilities.Loader.Show();
                $.ajax({
                    url: API_BASE_URL + "/RolePriviledgeSettings/SaveSettings",
                    type: "POST",
                    data: JSON.stringify(toSave),
                    contentType: "application/json",
                    success: function (data) {
                        if (data) {
                            Utilities.SuccessNotification("Settings saved!");
                            updateRolePriviledgesVariable();
                            window.location.reload();
                            //Utilities.Loader.Hide();
                        }
                        else {
                            Utilities.ErrorNotification("Sorry, something went wrong!");
                            Utilities.Loader.Hide();
                        }
                    },
                    error: function (err) {
                        Utilities.Loader.Hide();
                    }
                });
                //console.log(toSave);
            }
        }        
    });

    $("input:radio[name=frmCompanyAction]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#company input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#company input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmMasters1Action]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#masters1 input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#masters1 input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmMasters2Action]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#masters2 input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#masters2 input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmMasters3Action]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#masters3 input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#masters3 input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmTransactions1Action]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#transactions1 input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#transactions1 input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmTransactions2Action]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#transactions2 input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#transactions2 input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmTransactions3Action]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#transactions3 input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#transactions3 input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmRegister]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#register input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#register input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmPayroll1Action]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#payroll1 input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#payroll1 input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmPayroll2Action]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#payroll2 input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#payroll2 input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmBudgetAction]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#budget input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#budget input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmSettingsAction]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#settings input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#settings input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmSearchAction]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#search input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#search input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmReminderAction]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#reminder input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#reminder input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmFinancialStatementsAction]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#financialStatements input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#financialStatements input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmReports1]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#reports1 input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#reports1 input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });
    $("input:radio[name=frmReports2]").click(function () {
        var tabAction = $(this).val();

        if (tabAction == "SELECT_ALL") {
            $("#reports2 input:checkbox").each(function () {
                $(this).prop("checked", true);
            });
        }
        else if (tabAction == "CLEAR") {
            $("#reports2 input:checkbox").each(function () {
                $(this).prop("checked", false);
            });
        }
    });


    
    $.when(rolesAjax, rolePriviledgesAjax)
    .done(function (dataRoles,dataRolesPriviledges) {
        roles = dataRoles[2].responseJSON;
        rolePriviledges = dataRolesPriviledges[2].responseJSON;
        $("#role").html('<option value="">-Select Role-</option>' + Utilities.PopulateDropDownFromArray(roles, 0, 1));
    });

    $("#role").change(function () {
        selectedRolePriviledges = [];
        $.each(rolePriviledges, function (count, record) {
            
            if(record.roleId==$("#role").val())
            {
                console.log(rolePriviledges);
                selectedRolePriviledges.push(record);
            }
        });
        checkRolePriviledges();
    });

});

function selectOrClearAll()
{
    
    //$("input:radio[name=frmReports1]").click(function () {
    //    var tabAction = $(this).val();

    //    if (tabAction == "SELECT_ALL") {
    //        $("#reports1 input:checkbox").each(function () {
    //            $(this).prop("checked", true);
    //        });
    //    }
    //    else if (tabAction == "CLEAR") {
    //        $("#reports1 input:checkbox").each(function () {
    //            $(this).prop("checked", false);
    //        });
    //    }
    //});
    var formNames = [{ formName: "frmReports2", tabPaneId: "reports2" }];
    for (i = 0; i < formNames.length; i++)
    {
        alert(formNames[0].tabPaneId);
        $("input:radio[name="+formNames[i].formName+"]").click(function () {
            var tabAction = $(this).val();

            if (tabAction == "SELECT_ALL") {
                $("#" + formNames[i].tabPaneId + " input:checkbox").each(function () {
                    $(this).prop("checked", true);
                });
            }
            else if (tabAction == "CLEAR") {
                $("#" + formNames[i].tabPaneId + " input:checkbox").each(function () {
                    $(this).prop("checked", false);
                });
            }
        });
    }
    
}

function getFormName(input) {
    var val = input.split("-");
    return val[0];
}

function getAction(input) {
    var val = input.split("-");
    return val[1];
}

function checkRolePriviledges() //check the checkboxes for a role when d role is selected
{
    uncheckAllCheckboxes(); //uncheck all what was checked b4;
    $.each(selectedRolePriviledges, function (count, row) {
        var id = row.formName + "-" + row.action;
        $("#"+id).prop("checked", true);
    });
}

function uncheckAllCheckboxes()
{
    $("input:checkbox").each(function ()
    {
        if($(this).prop("checked"))
        {
            $(this).prop("checked", false);
        }
    });
}

function updateRolePriviledgesVariable()
{
    $.ajax({
        url: API_BASE_URL + "/RolePriviledgeSettings/LoadSettings",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            rolePriviledges = data;
            Utilities.Loader.Hide();
        }
    });
}