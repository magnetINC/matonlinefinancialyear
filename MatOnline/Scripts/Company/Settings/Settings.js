﻿var settings = [];

$(function () {
    loadSettings();

    $("#saveSettings").click(function () {
        settings = [];  //reset settings array
        var setup = [
            { ControlId: "payroll", SettingsId: 1, Status: "Yes" },
            { ControlId: "budget", SettingsId: 2, Status: "Yes" },
            { ControlId: "tax", SettingsId: 3, Status: "Yes" },
            { ControlId: "allowZeroEntry", SettingsId: 6, Status: "Yes" },
            { ControlId: "showCurrencySymbol", SettingsId: 7, Status: "Yes" },
            { ControlId: "tickPrint", SettingsId: 8, Status: "Yes" },
            { ControlId: "autoProductCode", SettingsId: 9, Status: "Yes" },
            { ControlId: "barcode", SettingsId: 10, Status: "Yes" },
            { ControlId: "allowBatch", SettingsId: 11, Status: "Yes" },
            { ControlId: "allowSize", SettingsId: 12, Status: "Yes" },
            { ControlId: "allowModelNo", SettingsId: 13, Status: "Yes" },
            { ControlId: "project", SettingsId: 37, Status: "Yes" },
            { ControlId: "showSalesRate", SettingsId: 16, Status: "Yes" },
            { ControlId: "showMrp", SettingsId: 17, Status: "Yes" },
            { ControlId: "showPurchaseRate", SettingsId: 36, Status: "Yes" },
            { ControlId: "showUnit", SettingsId: 18, Status: "Yes" },
            { ControlId: "showSize", SettingsId: 19, Status: "Yes" },
            { ControlId: "showModelNo", SettingsId: 20, Status: "Yes" },
            { ControlId: "showDiscount", SettingsId: 21, Status: "Yes" },
            { ControlId: "showProductCode", SettingsId: 22, Status: "Yes" },
            { ControlId: "showBrand", SettingsId: 23, Status: "Yes" },
            { ControlId: "showDiscountPercentage", SettingsId: 24, Status: "Yes" },
            { ControlId: "category", SettingsId: 38, Status: "Yes" },
            { ControlId: "showWithholdingTax", SettingsId: 39, Status: "Yes" },
            { ControlId: "confirmationFor", SettingsId: 31, Status: "Yes" },
            { ControlId: "add", SettingsId: 32, Status: "Yes" },
            { ControlId: "edit", SettingsId: 33, Status: "Yes" },
            { ControlId: "delete", SettingsId: 34, Status: "Yes" },
            { ControlId: "close", SettingsId: 35, Status: "Yes" },
            { ControlId: "printer", SettingsId: 26, Status: $("#printer").val() },
            { ControlId: "directPrint", SettingsId: 30, Status: "Yes" },
            { ControlId: "negativeCashTransaction", SettingsId: 27, Status: $("#negativeCashTransaction").val() },
            { ControlId: "stockValueCalculation", SettingsId: 29, Status: $("#stockValueCalculation").val() },
            { ControlId: "negativeStockStatus", SettingsId: 28, Status: $("#negativeStockStatus").val() }
        ];
        for (i = 0; i < setup.length; i++)
        {
            if(setup[i].SettingsId==26 || setup[i].SettingsId==27 || setup[i].SettingsId==28 || setup[i].SettingsId==29)    //controls are dropdowns
            {
                settings.push({
                    SettingsId: setup[i].SettingsId,
                    Status: setup[i].Status
                });
            }
            else    //controls are checkboxes
            {
                var check=$("#" + setup[i].ControlId).prop('checked');
                if (check==true)
                {
                    settings.push({
                        SettingsId: setup[i].SettingsId,
                        Status: setup[i].Status
                    });
                }
                else
                {
                    settings.push({
                        SettingsId: setup[i].SettingsId,
                        Status: "NO"
                    });
                }
            }
        }
        
        console.log(settings);
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/Settings/UpdateSettings",
            type: "POST",
            contentType: "application/json",
            data:JSON.stringify(settings),
            success: function (data) {
                console.log(data);
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    });
});

function loadSettings()
{
    //Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Settings/LoadSettings",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            renderSettings(data)
            //Utilities.Loader.Hide();
        },
        error:function(err)
        {
            //Utilities.Loader.Hide();
        }
    });
}

function renderSettings(settingsObject)
{    
    for (i = 0; i < settingsObject.length; i++)
    {
        var settingsId=settingsObject[i].settingsId;
        var status=settingsObject[i].status;
        if (settingsId == 1 && status=="Yes"){
            $('#payroll').prop("checked", true);
        }
        if (settingsId == 2 && status == "Yes") {
            $('#budget').prop("checked", true);
        }
        if (settingsId == 3 && status == "Yes") {
            $('#tax').prop("checked", true);
        }
        if (settingsId == 6 && status == "Yes") {
            $('#allowZeroEntry').prop("checked", true);
        }
        if (settingsId == 7 && status == "Yes") {
            $('#showCurrencySymbol').prop("checked", true);
        }
        if (settingsId == 8 && status == "Yes") {
            $('#tickPrint').prop("checked", true);
        }
        if (settingsId == 9 && status == "Yes") {
            $('#autoProductCode').prop("checked", true);
        }
    }
}