﻿var roles = [];
var table = "";


$(function () {
    renderGrid();

    $("#createRole").click(function () {
        var val = $("#role").val();
        if(val=="")
        {
            Utilities.ErrorNotification("Please enter role name");
            return;
        }
        else
        {
            var toSave = {
                Role:val
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/Role/AddRole",
                data:JSON.stringify(toSave),
                type: "Post",
                contentType: "application/json",
                success: function (data)
                {
                    if (data == null)
                    {
                        Utilities.ErrorNotification(val + " already exist!");
                        Utilities.Loader.Hide();
                        return;
                    }
                    else
                    {
                        Utilities.SuccessNotification("Role added!");
                        $("#role").val("");
                        $("#createRoleModal").modal("hide");
                        renderGrid();
                    }
                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.ErrorNotification("Sorry, could not add user role.");
                    Utilities.Loader.Hide();
                }
            });
        }
    });

    $("#saveChanges").click(function () {
        var val = $("#editRole").val();
        if (val == "") {
            Utilities.ErrorNotification("Please enter role name");
            return;
        }
        else {
            var toSave = {
                RoleId:$("#editRoleId").val(),
                Role: val
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/Role/EditRole",
                data: JSON.stringify(toSave),
                type: "Post",
                contentType: "application/json",
                success: function (data) {
                    if (data == null) {
                        Utilities.ErrorNotification(val + " already exist!");
                        Utilities.Loader.Hide();
                        return;
                    }
                    else {
                        Utilities.SuccessNotification("Changes saved!");
                        $("#editRoleId").val("");
                        $("#editRole").val("");
                        $("#editRoleModal").modal("hide");
                        renderGrid();
                    }
                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.ErrorNotification("Sorry, could not add user role.");
                    Utilities.Loader.Hide();
                }
            });
        }
    });
});

function renderGrid()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Role/GetRoles",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors.
            setTimeout(onSuccess, 500, data);

            
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry, could not load user roles.");
            Utilities.Loader.Hide();
        }
    });
    return;
    $("#roleGrid").kendoGrid({
        dataSource:  {
            transport: {
                read: {
                    url: API_BASE_URL + '/Role/GetRoles',
                    type: 'Get',
                    contentType: "application/json"
                },
                create: {
                    url: API_BASE_URL + "/Role/AddRole",
                    type: 'POST',
                    contentType: "application/json"
                },
                update: {
                    url: API_BASE_URL + "/Role/EditRole",
                    type: 'POST',
                    contentType: "application/json"
                },
                destroy: {
                    url: API_BASE_URL + "/Role/DeleteRole",
                    type: 'POST',
                    contentType: "application/json"
                },
                parameterMap: function (data) { return JSON.stringify(data); }
            },
            schema: {
                model: {
                    id: "RoleId",
                    fields: {
                        RoleId: { editable: false, type: "number" },
                        Role: { editable: true, validation: { required: true } },
                        //narration: { editable: true, validation: { required: true } },
                        //description: { editable: true, validation: { required: true } }

                    }
                }
            },
        },
        dataBound: function () {

            $(".k-grid-add").addClass("frmRole-Save hide");
            $(".k-grid-edit").addClass("frmRole-Update hide");
            $(".k-grid-delete").addClass("frmRole-Delete hide");
            PriviledgeSettings.ApplyUserPriviledge();
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 10,
            pageSizes: [10, 25, 50, 100, 1000],
            previousNext: true,
            buttonCount: 5,
        },
        toolbar: [{ name: "create", text: "Add Role" }],
        columns: [
            { field: "Role", title: "Role" },
            //{ field: "Narration", title: "Narration"},
            { command: ["edit", "destroy"], title: "&nbsp;", width: "250px" }],
        editable: "popup"
    });
    //$(".k-grid-edit").addClass("frmRole-Update");
    //console.log("hey2 ");
}//k-grid-edit

function onSuccess(data) {
    roles = data;
    if (table != "") {
        table.destroy();
    }
    var objToShow = [];
    for (i = 0; i < roles.length; i++) {
        objToShow.push([
            (i + 1),
            roles[i].Role,
            '<button class="btn btn-primary btn-labeled fa fa-edit" onclick="editRoleClick(' + roles[i].RoleId + ');">Edit</button>',
                     '<button class="btn btn-danger btn-labeled fa fa-times" onclick="deleteRoleClick(' + roles[i].RoleId + ');">Delete</button>'
        ]);
    }
    var count = 0;
    if (checkPriviledge("frmRole", "Update") === false) {
        objToShow.forEach(function (item) {
            item.splice(2, 1);
        });
        count++;
    }
    if (checkPriviledge("frmRole", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 3 - count;
            item.splice(recalculateIndex, 1);
        });
    }
    table = $('#rolesTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

    Utilities.Loader.Hide();
}

function deleteRoleClick(roleId)
{
    var conf = confirm("Do you really want to delete record?");
    if(conf==true)
    {
        var roleObj = roles.find(p=>p.RoleId == roleId);
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/Role/DeleteRole",
            type: "Post",
            data: JSON.stringify(roleObj),
            contentType: "application/json",
            success: function (data)
            {
                if (data == "ROLE_HAS_BEEN_REFERENCED")
                {
                    Utilities.ErrorNotification("Can't delete role already assigned to a user!");
                    Utilities.Loader.Hide();
                    return;
                }
                else if (data == "SUCCESS")
                {
                    Utilities.SuccessNotification("Role deleted!");
                    renderGrid();
                }
                Utilities.Loader.Hide();
            },
            error: function (err)
            {
                Utilities.ErrorNotification("Sorry, could not delete user role.");
                Utilities.Loader.Hide();
            }
        });
    }
}

function editRoleClick(roleId)
{
    var roleObj = roles.find(p=>p.RoleId == roleId);
    $("#editRoleId").val(roleObj.RoleId);
    $("#editRole").val(roleObj.Role);
    $("#editRoleModal").modal("show");
}