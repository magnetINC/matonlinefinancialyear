﻿var journals = [];
var table = "";
var thisJournal = {};
var itemsToBeDeleted = [];
var journalLineItems = [];
var deleteIndex = "";
var journalDetailsIndex = "";
var allProjects = [];
var allCategories = [];

function getCategoryName(id) {
    var CategoryName;
    for (i = 0; i < allCategories.length; i++) {
        if (allCategories[i].CategoryId == id) {
            CategoryName = allCategories[i].CategoryName;
        }
    }
    return CategoryName;
}

function getProjectName(id) {
    var ProjectName;
    for (i = 0; i < allProjects.length; i++) {
        if (allProjects[i].ProjectId == id) {
            ProjectName = allProjects[i].ProjectName;
        }
    }
    return ProjectName;
}

$(function () {
    loadRegisters();
    allProjectsAndCategories();
});

function loadRegisters() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/RunGeneralJournal/GetJournals",
        type: "Get",
        contentType: "application/json",
        success: function (data) {

            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function onSuccess(data) {
    journals = data;
    var objToShow = [];
    console.log('test', data)

    for (i = 0; i < data.length; i++) {
        var done = data[i].doneby == null ? 'admin' : data[i].doneby;
        objToShow.push([
            (i + 1),
            data[i].invoiceNo,
            Utilities.FormatJsonDate(data[i].date),
            Utilities.FormatCurrency(parseFloat(data[i].totalAmount)),
            data[i].narration,
            //data[i].doneBy,
            done,
            '<button class="btn btn-icon btn-xs btn-info" onclick="viewDetails(' + data[i].journalMasterId + ')"><i class="fa fa-eye"></i> View</button> ',
            '<button class="btn btn-icon btn-xs btn-primary" onclick="PrintDetails(' + data[i].journalMasterId + ')"><i class="fa fa-print"></i> Print</button> '
        ]);
    }

    if (checkPriviledge("frmGeneralJournal", "View") === false) {
        objToShow.forEach(function (item) {
            item.splice(5, 2);
        });
    }

    table = $('#journalTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    Utilities.Loader.Hide();
}

function viewDetails(id) {
    itemsToBeDeleted = [];
    for (i = 0; i < journals.length; i++) {
        if (journals[i].journalMasterId == id) {
            thisJournal = journals[i];
        }
    }

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/RunGeneralJournal/ViewJournal?journalMasterId=" + id,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            var details = data.Details;
           
            journalLineItems = data.Details;
            console.log("details", details);
            $("#journalNoDiv").html(data.JournalNo);
            $("#journalDateDiv").html(data.JournalDate);
            $("#createdByDiv").html(data.UserFullName);
            $("#journalMasterId").val(data.JournalMasterId);
            $("#debitAmtTxt").val(Utilities.FormatCurrency(data.TotalAmount));
            $("#creditAmtTxt").val(Utilities.FormatCurrency(data.TotalAmount));
            var output = "";

            /*for (i = 0; i < details.length; i++) {
                //var thisProjectName = details[i].ProjectId == "0" ? "N/A" : getProjectName(details[i].ProjectId);
                //var thisCategoryName = details[i].CategoryId == "0" ? "N/A" : getCategoryName(details[i].CategoryId);
                var projectObj = allProjects.find(p => p.ProjectId == details[i].ProjectId);
                var projectName = projectObj == undefined ? "NA" : projectObj.ProjectName;
                var projectId = projectObj == undefined ? "NA" : projectObj.ProjectId;
                var categoryObj = allCategories.find(p => p.CategoryId == details[i].CategoryId);
                var categoryName = categoryObj == undefined ? "NA" : categoryObj.CategoryName;
                var categoryId = categoryObj == undefined ? "NA" : categoryObj.CategoryId;
                output += '<tr>' +
                    '<td>' + (i + 1) + '</td>' +
                    '<td>' + details[i].LedgerName + '</td>' +
                    '<td>' + Utilities.FormatCurrency(details[i].Debit) + '</td>' +
                    '<td>' + Utilities.FormatCurrency(details[i].Credit) + '</td>' +

                    '<td>' + details[i].Memo + '</td>' +
                    '<td>' + projectName + '</td>' +
                    '<td class="hidden">' + projectId + '</td>' +
                    '<td>' + categoryName + '</td>' +
                    '<td class="hidden">' + categoryId + '</td>' +
                    '<td class="hidden">' + details[i].JournalDetails + '</td>' +
                    '<td class="deleteJournal">' +
                    '<button class="btn btn-danger" onclick="confirmDelete(' + i + ',' + details[i].JournalDetails + ')";" id="deleteJournal" type="button"><i class="fa fa-times"></i></button>' +
                    //'<td>' + '<button class="btn btn-danger" onclick="removeJournalItem(' + details[i].JournalDetails + ')"><i class="fa fa-times"></i></button>'+'</td>' +
                    //'<button class="btn btn-danger" onclick="confirmDelete(' + i + ',' + details[i].JournalDetails + ')";" id="deleteJournal" type="button"><i class="fa fa-times"></i></button>' +
                    //'<div style="display:none;">' + details[i].JournalDetails + '</div>' +
                    '</td>' +
                    '<td class="editJournal">' + '<button onclick="editDetails(this.parentElement.parentElement)" data-target="#itemLineModal" data-toggle="modal" class="btn btn-primary btn-labeled" data-original-title="Add Line Item"><i class="fa fa-edit"></i></button> ' +
                    '</td>'

                '</tr>';
            }*/

            $.each(details, function (index, row) {

                var thisCategory = allCategories.find(x => x.CategoryId == row.CategoryId);
                var thisCategoryName = thisCategory == undefined ? 'N/A' : thisCategory.CategoryName;

                var thisProject = allProjects.find(x => x.ProjectId == row.ProjectId);
                var thisProjectName = thisProject == undefined ? 'N/A' : thisProject.ProjectName;

                output += '<tr>' +
                    '<td>' + (index + 1) + '</td>' +
                    '<td>' + row.LedgerName + '</td>' +
                    '<td>' + Utilities.FormatCurrency(row.Debit) + '</td>' +
                    '<td>' + Utilities.FormatCurrency(row.Credit) + '</td>' +
                    '<td>' + row.Memo + '</td>' +
                    '<td>' + thisProjectName + '</td>' +
                    '<td>' + thisCategoryName + '</td>' +
                    '<td class="deleteJournal">' +

                    // '<input type="checkbox" name="potentialDeletesCheck" value="' + details[i].JournalDetails + '" id ="potentialDeletesCheck"><br>' +
                    '<button class="btn btn-danger btn-xs" onclick="confirmDelete(' + index + ',' + row.JournalDetails + ')";" id="deleteJournal" type="button"><i class="fa fa-times"></i></button>' +
                    '<div style="display:none;">' + row.JournalDetails + '</div>' +
                    '</td>' +
                    '<td class="editJournal">' + '<button onclick="editDetails(this.parentElement.parentElement)" data-target="#itemLineModal" data-toggle="modal" class="btn btn-primary btn-labeled btn-xs" data-original-title="Add Line Item"><i class="fa fa-edit"></i></button> ' +
                    '</td>'

                '</tr>';

                
            })

            $("#detailsTbody").html(output);
            $("#detailsModal").modal("show");
            if (checkPriviledge("frmGeneralJournal", "Delete") === false) {
                $(".deleteJournal").hide();
            }
            if (checkPriviledge("frmGeneralJournal", "Update") === false) {
                $(".editJournal").hide();
            }
            //table = $('#detailsTbody').DataTable({
            //    data: output,
            //    "paging": true,
            //});
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry, could not load journal details.");
            Utilities.Loader.Hide();
        }
    });
}


//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;

//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}

function PrintDetails(id) {
    for (i = 0; i < journals.length; i++) {
        if (journals[i].journalMasterId == id) {
            thisJournal = journals[i];
        }
    }

    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/RunGeneralJournal/ViewJournal?journalMasterId=" + id,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            //console.log(data);
            var details = data.Details;

            var objToPrint = [];
            $.each(details, function (count, row) {
                /*var thisCategory = row.CategoryId == "0" ? "N/A" : getCategoryName(row.CategoryId);
                var thisProject = row.ProjectId == "0" ? "N/A" : getProjectName(row.CategoryId);*/

                var thisCategory = allCategories.find(x => x.CategoryId == row.CategoryId);
                var thisCategoryName = thisCategory == undefined ? 'N/A' : thisCategory.CategoryName;

                var thisProject = allProjects.find(x => x.ProjectId == row.ProjectId);
                var thisProjectName = thisProject == undefined ? 'N/A' : thisProject.ProjectName;

                objToPrint.push({
                    "SlNo": count + 1,
                    "Name": row.LedgerName,
                    "Project": thisProjectName,
                    "Category": thisCategoryName,
                    "Debit": Utilities.FormatCurrency(row.Debit),
                    "Credit": Utilities.FormatCurrency(row.Credit),
                    "Memo": row.Memo
                });
            });

            var companyName = $("#companyName").val();
            var companyAddress = $("#companyAddress").val();
            var thisEmail = $("#companyEmail").val();
            var thisPhone = $("#companyPhone").val();
            var JournalNo = data.JournalNo;
            var JornalDate = data.JournalDate;
            var createdBy = data.UserFullName;
            var credit = Utilities.FormatCurrency(data.TotalAmount);
            var debit = Utilities.FormatCurrency(data.TotalAmount);
            var masterId = data.JournalMasterId;
            var doneBy = data.UserFullName;


            columns = [
                { title: "S/N", dataKey: "SlNo" },
                { title: "Account Ledger", dataKey: "Name" },
                { title: "Debit", dataKey: "Debit" },
                { title: "Credit", dataKey: "Credit" },
                { title: "Project", dataKey: "Project" },
                { title: "Category", dataKey: "Category" },
                { title: "Memo", dataKey: "Memo" }
            ];

            var doc = new jsPDF('portrait', 'pt', 'letter');
            doc.setDrawColor(0);

            //start drawing

            doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);

            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text(companyName, 320, 80, 'center');
            doc.setFontSize(9);
            doc.setFontType("normal");
            doc.text(companyAddress, 320, 90, 'center');

            doc.setFontSize(9);
            doc.setFontType("bold");
            doc.text('Email Id: ', 400, 150);
            doc.setFontType("normal");
            doc.text(thisEmail, 460, 150);
            doc.setFontType("bold");
            doc.text('Phone: ', 400, 165);
            doc.setFontType("normal");
            doc.text(thisPhone, 460, 165);

            doc.line(40, 170, 570, 170);


            //doc.line(395, 175, 395, 215);//horizontal line
            doc.setFontType("bold");
            doc.text('Journal No: ', 400, 185);
            doc.setFontType("normal");
            doc.text(JournalNo, 460, 185);
            doc.setFontType("bold");
            doc.text('Journal Date: ', 400, 205);
            doc.setFontType("normal");
            doc.text(JornalDate, 460, 205);

            doc.line(40, 220, 570, 220);

            doc.setFontSize(12);
            doc.setFontType("bold");
            doc.text('Journal Voucher', 250, 240);

            //doc.line(120, 20, 120, 60); horizontal line
            doc.setFontSize(8);
            doc.autoTable(columns, objToPrint, {
                startY: 255,
                theme: 'striped',
                styles: { "overflow": "linebreak", "cellWidth": "wrap", "rowPageBreak": "avoid", "fontSize": "7", "lineColor": "100", "lineWidth": ".25" },

                columnStyles: {
                    SlNo: { columnWidth: 30, },
                    Debit: { columnWidth: 60, halign: 'right' },
                    Credit: { columnWidth: 60, halign: 'right' },
                    Name: { columnWidth: 110 },
                    Category: { columnWidth: 100 },
                    Project: { columnWidth: 100 }

                },
            });

            doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

            doc.setFontSize(9);
            doc.setFontType("bold");
            doc.text('Total Debit: N' + debit, 555, doc.autoTable.previous.finalY + 20, "right");
            doc.text('Total Credit: N' + credit, 555, doc.autoTable.previous.finalY + 40, "right");

            doc.line(40, doc.autoTable.previous.finalY + 50, 570, doc.autoTable.previous.finalY + 50);

            //convert to words
            var InWords = Utilities.NumberToWords(data.TotalAmount);
            //Capitalize Each Word
            const words = InWords.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());


            var strArr1 = doc.splitTextToSize('Total Debit In Words: ' + words + ' Naira Only.', 500);
            doc.text(strArr1, 40, doc.autoTable.previous.finalY + 90);

            var strArr2 = doc.splitTextToSize('Total Credit In Words: ' + words + ' Naira Only.', 500);
            doc.text(strArr2, 40, doc.autoTable.previous.finalY + 115);

            doc.setFontType("bold");
            doc.text('Journal approved by:  ___________', 40, doc.autoTable.previous.finalY + 150);

            doc.text('Done by: ' + doneBy, 350, doc.autoTable.previous.finalY + 150);
            doc.text('______', 390, doc.autoTable.previous.finalY + 153);
            doc.autoPrint();
            doc.save('Journal Voucher.pdf');
            //window.open(doc.output('bloburl'));
            window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry, could not print  journal details.");
            Utilities.Loader.Hide();
        }
    });

}

function deleteJournal() {
    var conf = confirm("Do you really want to delete this journal?");
    if (conf == true) {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/RunGeneralJournal/DeleteJournal?id=" + thisJournal.journalMasterId + "&voucherNo=" + thisJournal.voucherNo,

            type: "Get",
            contentType: "application/json",
            success: function (data) {
                // console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Journal deleted successfully");
                    $("#detailsModal").modal("hide");
                    window.location = "/Company/RunGeneralJournals/JournalRegister";
                }
                else if (data.ResponseCode == 403) {
                    Utilities.ErrorNotification(data.Response.ResponseMessage);
                }
                else {
                    Utilities.ErrorNotification("Sorry, Someting went wrong");
                }

                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.ErrorNotification("Sorry, could not load journal details.");
                Utilities.Loader.Hide();
            }
        });
    }
}


//function potentialDeletes(deleteIndex) {
//    itemsToBeDeleted.push(journalDetailsIndex);
//    var ledgerList = document.getElementById("detailsTbody").childNodes;
//    for (var i = 0; i < ledgerList.length; i++) {
//        if (ledgerList[i].firstElementChild.innerText == (deleteIndex + 1)) {
//            document.getElementById("detailsTbody").removeChild(ledgerList[i]);
//        }
//    };
//}

//var options = {
//    message: "Are you sure you want to proceed with the deletion?",
//    title: 'Confirm Delete',
//    size: 'sm',
//    subtitle: 'smaller text header',
//    label: "Yes"   // use the possitive lable as key
//    //...
//};


//function confirmDelete(item, details) {
//    deleteIndex = item;
//    journalDetailsIndex = details;
//    eModal.confirm(options).then(potentialDeletes);
//}


function potentialDeletes() {
    itemsToBeDeleted.push(journalDetailsIndex);
    var ledgerList = document.getElementById("detailsTbody").childNodes;
    for (var i = 0; i < ledgerList.length; i++) {
        if (ledgerList[i].firstElementChild.innerText == (deleteIndex + 1)) {
            document.getElementById("detailsTbody").removeChild(ledgerList[i]);
        }
    };
}

var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the possitive lable as key
    //...
};


function confirmDelete(item, details) {
    deleteIndex = item;
    journalDetailsIndex = details;
    eModal.confirm(options).then(potentialDeletes);
}


function refresh() {
    window.location = "/Company/RunGeneralJournals/JournalRegister"
}

function deleteJournalEntries() {
    var itemsString = "";

    itemsToBeDeleted.forEach(function (item) { itemsString += "_" + item; })
    //var conf = confirm("Do you really want to make journal?");
    //if (conf == true) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/RunGeneralJournal/DeleteJournalEntries?journalMasterId=" + thisJournal.journalMasterId + "&journalEntries=" + itemsString,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data.ResponseCode == 200) {
                Utilities.SuccessNotification("Journal Details Deleted Successfully");
                $("#detailsModal").modal("hide");
                window.location = "/Company/RunGeneralJournals/JournalRegister";
            }
            else if (data.ResponseCode == 403) {
                Utilities.ErrorNotification("Could not delete. There is a variance between total credit and debit.");
                $("#detailsModal").modal("hide");
                setTimeout(refresh, 5000);
            }
            else {
                Utilities.ErrorNotification("Sorry, Someting went wrong");
            }

            // Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry, could not load journal details.");
            Utilities.Loader.Hide();
        }
    });
    //}
}

function creditAndDebitTotal() {
    var list = document.getElementById('detailsTbody');
    var creditTotal = 0;
    var debitTotal = 0;
    for (var i = 0; i < list.childElementCount; i++) {
        // if (checkBox.checked == false) {
        var creditString = list.children[i].children[3].innerText;
        var creditValue = creditString.replace(/\,/g, '');
        var creditFloat = parseFloat(creditValue);
        creditTotal += creditFloat;

        var debitString = list.children[i].children[2].innerText;
        var debitValue = debitString.replace(/\,/g, '');
        var debitFloat = parseFloat(debitValue);
        debitTotal += debitFloat;
        //}
    }
    var debitDisplay = document.getElementById('debitAmtTxt');
    var creditDisplay = document.getElementById('creditAmtTxt');
    debitDisplay.value = Utilities.FormatCurrency(debitTotal);
    creditDisplay.value = Utilities.FormatCurrency(creditTotal);
}

setInterval(creditAndDebitTotal, 300);

$(function () {
    $('#itemLineModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
    getDropdowns();
    //selectedLedgerId = 14;
    $('#newModal').on('hidden.bs.modal', function () {
        var datasource = $('#journalsGrid').data().kendoGrid.dataSource.data();
        //var grid = $("#journalsGrid").data("kendoGrid");
        //console.log(grid);
        //var model = grid.dataItem(this.element.closest("tr"));
        //selectedLedgerId = 14;//model.ledgerId;
        var indexOfSelectedLedger = -1;
        console.log(accountLedgers.find(p => p.ledgerId == selectedLedgerId));
        for (i = 0; i < datasource.length; i++) {
            if (datasource[i].ledgerId == selectedLedgerId) {
                indexOfSelectedLedger = i;
                break;
            }
        }
        datasource[indexOfSelectedLedger].set('amount', getTotalInvoiceAmount(selectedLedgerId));
        var totalDrCr = getTotalCreditAndDebitAmount();
        console.log(totalDrCr);
        $("#totalDebit").val(totalDrCr[0]);
        $("#totalCredit").val(totalDrCr[1]);
    });

    $("#addLineItem").click(function () {
        getDropdowns();
        var selectedLedger = $("#accountLedger").val();
        var selectedDrCr = $("#drOrCr").val();
        var amt = $("#amount").val();
        var memo = "";
        var projectId = $("#project").val();
        var categoryId = $("#category").val();

        var serialId = $("#serialId").val();
        if ($("#memo").val() == "" || $("#memo").val() == undefined || $("#memo").val() == null) {
            memo = "N/A";
        }
        else {
            memo = $("#memo").val();
        }

        if (selectedLedger == "" || selectedLedger == null) {
            Utilities.ErrorNotification("Please select account ledger!");
            return;
        }
        if (selectedDrCr == "") {
            Utilities.ErrorNotification("Please select debit or credit!");
            return;
        }
        if (amt == "") {
            Utilities.ErrorNotification("Please enter amount!");
            return;
        }

        if (selectedDrCr == "Cr") {
            journalLineItems = [];
            journalLineItems.push({
                LedgerId: selectedLedger,
                Memo: memo,
                Debit: 0,
                Credit: amt,
                ChequeDate: "",
                ChequeNo: "",
                SerialId: serialId,
                Extra1: "OnAccount",
                ProjectId: projectId,
                CategoryId: categoryId,
                JournalDetailsId:"0"
            });
        }
        else if (selectedDrCr == "Dr") {
            journalLineItems = [];
            journalLineItems.push({
                LedgerId: selectedLedger,
                Memo: memo,
                Debit: amt,
                Credit: 0,
                ChequeDate: "",
                ChequeNo: "",
                SerialId: serialId,
                Extra1: "OnAccount",
                ProjectId: projectId,
                CategoryId: categoryId,
                JournalDetailsId: "0"
            });
        }

        renderLineItems();
        $("#accountLedger").data("kendoDropDownList").value("Please select");
        $("select#drOrCr")[0].selectedIndex = 0;
        $("#amount").val("");
        $("#memo").val("");
        $("#project").val("");
        $("#project").trigger("chosen:updated");
        $("#category").val("");
        $("#category").trigger("chosen:updated");

    });

    $('#againstModal').on('hidden.bs.modal', function () {
        var datasource = $('#journalsGrid').data().kendoGrid.dataSource.data();
        var indexOfSelectedLedger = -1;
        for (i = 0; i < datasource.length; i++) {
            if (datasource[i].ledgerId == selectedLedgerId) {
                indexOfSelectedLedger = i;
                break;
            }
        }
        datasource[indexOfSelectedLedger].set('amount', getTotalInvoiceAmount(selectedLedgerId));
        var totalDrCr = getTotalCreditAndDebitAmount();
        console.log(totalDrCr);
        $("#totalDebit").val(totalDrCr[0]);
        $("#totalCredit").val(totalDrCr[1]);
    });

    $('#onAccountModal').on('hidden.bs.modal', function () {
        var datasource = $('#journalsGrid').data().kendoGrid.dataSource.data();
        var indexOfSelectedLedger = -1;
        for (i = 0; i < datasource.length; i++) {
            if (datasource[i].ledgerId == selectedLedgerId) {
                indexOfSelectedLedger = i;
                break;
            }
        }
        datasource[indexOfSelectedLedger].set('amount', getTotalInvoiceAmount(selectedLedgerId));
        var totalDrCr = getTotalCreditAndDebitAmount();
        console.log(totalDrCr);
        $("#totalDebit").val(totalDrCr[0]);
        $("#totalCredit").val(totalDrCr[1]);
    });

    $('#partyBalanceTbody').on('change', '.invAmt', function () {
        //console.log($(this).val());
        var indexOfObject = applyAgainstEntries.findIndex(p => p.InvoiceNo == $(this).attr("id"));
        applyAgainstEntries[indexOfObject].Amount = $(this).val();
        selectedLedgerId = applyAgainstEntries[indexOfObject].LedgerId;

        $("#totalInvoiceAmt").val(Utilities.FormatCurrency(getTotalInvoiceAmount(selectedLedgerId)));
    });

    $("#applyAmount").change(function () {
        var drCr = "";
        var datasource = $('#journalsGrid').data().kendoGrid.dataSource.data();
        for (i = 0; i < datasource.length; i++) {
            if (datasource[i].ledgerId == selectedLedgerId) {
                drCr = datasource[i].DrCr;
                break;
            }
        }
        applyAgainstEntries.push({
            LedgerId: selectedLedgerId,
            DrCr: drCr,
            Amount: $("#applyAmount").val(),
            ReferenceType: "OnAccount",
            //AgainstInvoiceNo: "0",
            //AgainstVoucherNo:"0",
            //AgainstVoucherTypeId:1
        });
    });

    $("#newAmount").change(function () {
        var drCr = "";
        var datasource = $('#journalsGrid').data().kendoGrid.dataSource.data();
        for (i = 0; i < datasource.length; i++) {
            if (datasource[i].ledgerId == selectedLedgerId) {
                drCr = datasource[i].DrCr;
                break;
            }
        }
        applyAgainstEntries.push({
            LedgerId: selectedLedgerId,
            DrCr: drCr,
            Amount: $("#newAmount").val(),
            ReferenceType: "New",
            //AgainstInvoiceNo: "0",
            //AgainstVoucherNo:"0",
            //AgainstVoucherTypeId:1
        });
    });

    $("#saveJournal").click(function () {
        //var datasource = $('#journalsGrid').data().kendoGrid.dataSource.data();
        //console.log(datasource);
        //console.log(applyAgainstEntries);
        //return;
        //var totalDrCr = getTotalCreditAndDebitAmount();
        //console.log(journalLineItems);
        if ($("#totalDebit").val() != $("#totalCredit").val()) {
            Utilities.ErrorNotification("Total credit must be same as total debit!");
            return;
        }
        else {
            var journalMasterInfo = {
                VoucherNo: $("#voucherNo").val(),
                InvoiceNo: $("#voucherNo").val(),
                TotalAmount: 0,
                Narration: "",
                Date: $("#date").val()
            };
            var totalAmt = 0.0;
            var journalDetails = [];
            for (i = 0; i < journalLineItems.length; i++) {
                //console.log(journalLineItems);
                totalAmt += (parseFloat(journalLineItems[i].Credit) + parseFloat(journalLineItems[i].Debit));
                if (journalLineItems[i].Credit > 0) {
                    journalDetails.push({
                        LedgerId: journalLineItems[i].LedgerId,
                        Memo: journalLineItems[i].Memo,
                        Debit: 0,
                        Credit: journalLineItems[i].Credit,
                        ChequeDate: "",
                        ChequeNo: "",
                        SerialId: serialId,
                        Extra1: "OnAccount", //datasource[i].apply
                        ProjectId: ledgerList[i].childNodes[6].innerText,
                        CategoryId: ledgerList[i].childNodes[8].innerText,
                        JournalDetailsId: journalLineItems[i].JournalDetailsId
                    });
                }
                else if (journalLineItems[i].Debit > 0) {
                    journalDetails.push({
                        LedgerId: journalLineItems[i].LedgerId,
                        Memo: journalLineItems[i].Memo,
                        Debit: journalLineItems[i].Debit,
                        Credit: 0,
                        ChequeDate: "",
                        ChequeNo: "",
                        SerialId: serialId,
                        Extra1: "OnAccount", //datasource[i].apply
                        ProjectId: ledgerList[i].childNodes[6].innerText,
                        CategoryId: ledgerList[i].childNodes[8].innerText,
                        JournalDetailsId: journalLineItems[i].JournalDetailsId
                    });
                }
            }

            journalMasterInfo.TotalAmount = totalAmt;

            var partyBalance = [];
            for (i = 0; i < applyAgainstEntries.length; i++) {
                partyBalance.push({
                    AgainstInvoiceNo: applyAgainstEntries[i].InvoiceNo,
                    AgainstVoucherNo: applyAgainstEntries[i].InvoiceNo,
                    Credit: applyAgainstEntries[i].Amount,
                    InvoiceNo: $("#voucherNo").val(),
                    LedgerId: applyAgainstEntries[i].LedgerId,
                    ReferenceType: "Against",
                    VoucherNo: $("#voucherNo").val(),
                    AgainstVoucherTypeId: applyAgainstEntries[i].VoucherTypeId
                });
            }

            var toSave = {
                JournalMasterInfo: journalMasterInfo,
                JournalDetailsInfo: journalDetails,
                PartyBalanceInfo: partyBalance
            };
            //console.log(toSave); return;
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/RunGeneralJournal/Save",
                type: "Post",
                data: JSON.stringify(toSave),
                contentType: "application/json",
                success: function (data) {
                    if (data == true) {
                        Utilities.SuccessNotification("Journal saved!");
                        window.location = "/Company/RunGeneralJournals/Index";
                    }
                    else {
                        Utilities.ErrorNotification("Journal could not be saved.");
                    }
                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.Loader.Hide();
                }
            });
            //setTimeout(function () {
            //    Utilities.SuccessNotification("Journal saved!");
            //    Utilities.Loader.Hide();
            //    window.location = "/Company/RunGeneralJournals/Index";
            //}, 3000);
        }
    });

});


function renderLineItems() {
    var totalCredit = 0.0;
    var totalDebit = 0.0;
    var output = "";
    var index = 0;
    var lastIndex = "";
    var serialId = document.getElementById('serialId');

    if (document.getElementById('detailsTbody').lastElementChild == null) {
        lastIndex = 0;
    }
    else {
        lastIndex = document.getElementById('detailsTbody').lastElementChild.firstChild.innerText;
    }
    if (!journalLineItems[0].SerialId) {
        for (i = 0; i < journalLineItems.length; i++) {
            totalCredit += parseFloat(journalLineItems[i].Credit);
            totalDebit += parseFloat(journalLineItems[i].Debit);
            var projectObj = allProjects.find(p => p.ProjectId == journalLineItems[i].ProjectId);
            var projectName = projectObj == undefined ? "NA" : projectObj.ProjectName;
            var projectId = projectObj == undefined ? "NA" : projectObj.ProjectId;
            var categoryObj = allCategories.find(p => p.CategoryId == journalLineItems[i].CategoryId);
            var categoryName = categoryObj == undefined ? "NA" : categoryObj.CategoryName;
            var categoryId = categoryObj == undefined ? "NA" : categoryObj.CategoryId;
            output += '<tr>' +
                '<td>' + (parseInt(lastIndex) + 1) + '</td>' +
                '<td>' + accountLedgers.find(p => p.ledgerId == journalLineItems[i].LedgerId).ledgerName + '</td>' +
                '<td>' + Utilities.FormatCurrency(journalLineItems[i].Debit) + '</td>' +
                '<td>' + Utilities.FormatCurrency(journalLineItems[i].Credit) + '</td>' +

                '<td>' + journalLineItems[i].Memo + '</td>' +
                '<td>' + projectName + '</td>' +
                '<td class="hidden">' + projectId + '</td>' +
                '<td>' + categoryName + '</td>' +
                '<td class="hidden">' + categoryId + '</td>' +
                '<td class="hidden">' + i + '</td>' +
                //'<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button> ' +
                '<td>' + '<button class="btn btn-danger deleteJournal" onclick="potentialDeletes(' + i + ',' + 0 + ')";" type="button"><i class="fa fa-times"></i></button>' + ' </td>' +
                //'<td><input type="checkbox" name="potentialDeletesCheck" value="' + 0 + '" id ="potentialDeletesCheck"</td><br>' +
                // '<td>' +
                //  '<button  data-target="#itemLineModal" data-toggle="modal" class="btn btn-primary btn-labeled" data-original-title="Add Line Item"><i class="fa fa-edit"></i></button> ' +
                // '</td>' +
                '<td>' + '<button  onclick="editDetails(this.parentElement.parentElement)" data-target="#itemLineModal" data-toggle="modal" class="btn btn-primary btn-labeled editJournal" data-original-title="Add Line Item"><i class="fa fa-edit"></i></button> ' +
                '</td>' +
                // '<td><button onclick="removeLineItem(' + journalLineItems[i].LedgerId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>' +
                '</tr>';
            $("#detailsTbody").append(output);
        }
    }
    else {
        var ledgerList = document.getElementById("detailsTbody").childNodes;
        for (var i = 0; i < ledgerList.length; i++) {
            if (ledgerList[i].firstElementChild.innerText == journalLineItems[0].SerialId) {
                ledgerList[i].childNodes[1].innerText = accountLedgers.find(p => p.ledgerId == journalLineItems[0].LedgerId).ledgerName;
                ledgerList[i].childNodes[2].innerText = journalLineItems[0].Debit;
                ledgerList[i].childNodes[3].innerText = journalLineItems[0].Credit;
                ledgerList[i].childNodes[4].innerText = journalLineItems[0].Memo;
                ledgerList[i].childNodes[5].innerText = findProject(journalLineItems[0].ProjectId).ProjectName;
                ledgerList[i].childNodes[6].innerText = findCategory(journalLineItems[0].CategoryId).CategoryName;
            }
        };
    }
    $("#totalDebit").val(Utilities.FormatCurrency(totalDebit));
    $("#totalCredit").val(Utilities.FormatCurrency(totalCredit));
    // $("#detailsTbody").html(output);
    journalLineItems = [];
    serialId.value = "";
    $("#itemLineModal").modal("hide");

}


function removeJournalItem(id) {
    if (confirm("Remove this item?")) {
        var indexOfObjectToRemove = journalLineItems.findIndex(p => p.JournalDetails == id);
        journalLineItems.splice(indexOfObjectToRemove, 1);
        renderLineItems();
    }
}

function getDropdowns() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/RunGeneralJournal/GetDropdowns",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            accountLedgers = data.AccountLedger;
            console.log(accountLedgers);
            currencies = data.Currencies;
            $("#voucherNo").val(data.InvoiceNo);
            $("#accountLedger").kendoDropDownList({
                filter: "contains",
                optionLabel: "Please Select...",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                index: 0,
                dataSource: accountLedgers
            });
            //console.log(currencies);
            //renderGrid();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function editDetails(details) {
    var itemModal = document.getElementById('itemLineModal');
    var amount = document.getElementById('amount');
    var memo = document.getElementById('memo');
    var debitOrCredit = document.getElementById('drOrCr');
    var serialId = document.getElementById('serialId');
    var serialValue = details.children[0].innerText;
    var accountLedger = details.children[1].innerText;
    var credit = parseFloat((details.children[3].innerText).replace(/\,/g, ''));
    var debit = parseFloat((details.children[2].innerText).replace(/\,/g, ''));
    var projectId = details.children[6].innerText;
    var categoryId = details.children[8].innerText;

    $("#project").val(findProject(projectId));
    $("#project option:selected").text(findProject(projectId).ProjectName);
    $("#category").val(findCategory(categoryId));
    $("#category option:selected").text(findCategory(categoryId).CategoryName);
    if (credit != 0.00) {
        amount.value = credit;
        debitOrCredit.value = "Cr";
    } else if (debit != 0.00) {
        amount.value = debit;
        debitOrCredit.value = "Dr"
    }
    memo.value = details.children[4].innerText;

    var dropdownlist = $("#accountLedger").data("kendoDropDownList");
    serialId.value = serialValue;


    //dropdownlist.select(7);
    //console.log(dropdownlist.dataSource.options.data);
    var dropDown = dropdownlist.dataSource.options.data;
    for (var i = 0; i < dropDown.length; i++) {
        if (dropDown[i].ledgerName == accountLedger) {
            dropdownlist.select(i + 1);
        }
    }

}

function findProject(projectId) {
    var output = {};
    for (i = 0; i < allProjects.length; i++) {
        if (allProjects[i].ProjectId == projectId) {
            output = allProjects[i];
            break;
        }
    }
    return output;
}

function findCategory(categoryId) {
    var output = {};
    for (i = 0; i < allCategories.length; i++) {
        if (allCategories[i].CategoryId == categoryId) {
            output = allCategories[i];
            break;
        }
    }
    return output;
}

$("#updateJournal").click(function () {

    if ($("#debitAmtTxt").val() != $("#creditAmtTxt").val()) {
        Utilities.ErrorNotification("Total credit must be same as total debit!");
        return;
    }
    else {

        if (itemsToBeDeleted.length > 0) {
            deleteJournalEntries();
        }
        journalLineItems = [];
        var ledgerList = document.getElementById("detailsTbody").childNodes;
        console.log(ledgerList);
        for (var i = 0; i < ledgerList.length; i++) {
            var debit = 0;
            var credit = 0;
            if (parseFloat(ledgerList[i].childNodes[2].innerText) != 0) {
                debit = ledgerList[i].childNodes[2].innerText;
                debit = parseFloat(debit.replace(/\,/g, ''));
            } else if (parseFloat(ledgerList[i].childNodes[3].innerText) != 0) {
                credit = ledgerList[i].childNodes[3].innerText;
                credit = parseFloat(credit.replace(/\,/g, ''));
            }
            var thisProject = allProjects.find(x => x.ProjectName.trim() == ledgerList[i].childNodes[5].innerText.trim());
            var thisProjectId = thisProject == undefined ? "0" : thisProject.ProjectId;

            var thisCategory = allCategories.find(x => x.CategoryName.trim() == ledgerList[i].childNodes[6].innerText.trim());
            var thisCategoryId = thisCategory == undefined ? "0" : thisCategory.CategoryId;

            let prjId = ledgerList[i].childNodes[7].childNodes.length == 2 ? ledgerList[i].childNodes[7].childNodes[1].innerText : "0";

           /// console.log(ledgerList[i].childNodes[7])
           // console.log("Real " , prjId)
           // return;

            journalLineItems.push({
                LedgerId: accountLedgers.find(p => p.ledgerName.trim() == ledgerList[i].childNodes[1].innerText.trim()).ledgerId,
                Memo: ledgerList[i].childNodes[4].innerText,
                Debit: debit,
                Credit: credit,
                ChequeDate: "",
                ChequeNo: "",
                JournalDetailsId: prjId,
                Extra1: "OnAccount",
                ProjectId: thisProjectId,
                CategoryId: thisCategoryId
            });
          
        };
        var journalMasterId = document.getElementById("journalMasterId").value;
        var toSave = {
            JournalDetailsInfo: journalLineItems,
            JournalMasterId: journalMasterId

        };
        
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/RunGeneralJournal/UpdateJournal",
            type: "Post",
            data: JSON.stringify(toSave),
            contentType: "application/json",
            success: function (data) {
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Journal Details Updated!");
                    $("#detailsModal").modal("hide");
                    window.location = "/Company/RunGeneralJournals/JournalRegister";
                }
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
});




function allProjectsAndCategories() {
    var projectLookupAjax = $.ajax({
        url: API_BASE_URL + "/Project/GetProject",
        type: "GET",
        contentType: "application/json",
    });

    var categoryLookupAjax = $.ajax({
        url: API_BASE_URL + "/Category/GetCategory",
        type: "GET",
        contentType: "application/json",
    });

    $.when(projectLookupAjax, categoryLookupAjax)
        .done(function (dataProject, dataCategory) {
            allProjects = dataProject[0];
            allCategories = dataCategory[0];
            $("#project").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allProjects, 0, 1));
            $("#project").chosen({ width: "100%", margin: "1px" });
            $("#category").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(allCategories, 0, 1));
            $("#category").chosen({ width: "100%", margin: "1px" });
        });
}

allProjectsAndCategories();