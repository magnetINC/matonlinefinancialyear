﻿var accountLedgers = [];
var currencies = [];
var journalsEntries = [];
var customerSupplierBalance = [];
var journalToProcess = [];
var journalPartyBalance = [];
var selectedLedgerId = 0;
var applyAgainstEntries = [];
var journalLineItems = [];
var projects = [];
var depts = [];
var printData = {};
printData.lineItems = [];
printData.totalCredit = 0;
printData.totalDebit = 0;
var base64Img = null;
var imgurl = "";

validateImage();

getLookUps();

/*$("#amount").on('keyup', function () {
    var n = parseInt($(this).val().replace(/\D/g, ''), 10);
    $(this).val(n.toLocaleString());
});*/



function FormatCurrency(ctrl) {
    //Check if arrow keys are pressed - we want to allow navigation around textbox using arrow keys
    if (event.keyCode == 37 || event.keyCode == 38 || event.keyCode == 39 || event.keyCode == 40) {
        return;
    }

    var val = ctrl.value;

    val = val.replace(/,/g, "")
    ctrl.value = "";
    val += '';
    x = val.split('.');
    x1 = x[0];
    x2 = x.length > 1 ? '.' + x[1] : '';

    var rgx = /(\d+)(\d{3})/;

    while (rgx.test(x1)) {
        x1 = x1.replace(rgx, '$1' + ',' + '$2');
    }

    ctrl.value = x1 + x2;
}

function CheckNumeric() {
    return event.keyCode >= 48 && event.keyCode <= 57 || event.keyCode == 46;
}







$(function () {
    $('#itemLineModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
    getDropdowns();
    //selectedLedgerId = 14;
    $('#newModal').on('hidden.bs.modal', function () {
        var datasource = $('#journalsGrid').data().kendoGrid.dataSource.data();
        //var grid = $("#journalsGrid").data("kendoGrid");
        //console.log(grid);
        //var model = grid.dataItem(this.element.closest("tr"));
        //selectedLedgerId = 14;//model.ledgerId;
        var indexOfSelectedLedger = -1;
        console.log(accountLedgers.find(p=>p.ledgerId == selectedLedgerId));
        for (i = 0; i < datasource.length; i++) {
            if (datasource[i].ledgerId == selectedLedgerId) {
                indexOfSelectedLedger = i;
                break;
            }
        }
        datasource[indexOfSelectedLedger].set('amount', getTotalInvoiceAmount(selectedLedgerId));
        var totalDrCr = getTotalCreditAndDebitAmount();
        console.log(totalDrCr);
        $("#totalDebit").val(totalDrCr[0]);
        $("#totalCredit").val(totalDrCr[1]);
    });

    $("#addLineItem").click(function () {
        getDropdowns();
        var selectedLedger = $("#accountLedger").val();
        var selectedDrCr = $("#drOrCr").val();
        var string_amt = $("#amount").val(); //wip
        var amt = Number(string_amt.replace(/,/g, ''));
        var memo = "";
        var projectId = $("#project").val();
        var categoryId = $("#category").val();
        console.log(string_amt);
        console.log(amt);
        if ($("#memo").val() == "" || $("#memo").val() == undefined || $("#memo").val() == null)
        {
            memo = "N/A";
        }
        else
        {
            memo = $("#memo").val();
        }

        if(selectedLedger=="" || selectedLedger==null)
        {
            Utilities.ErrorNotification("Please select account ledger!");
            return;
        }
        if (selectedDrCr == "")
        {
            Utilities.ErrorNotification("Please select debit or credit!");
            return;
        }
        if (amt == "" || amt == "NaN")
        {
            Utilities.ErrorNotification("Please enter amount!");
            return;
        }

        if (selectedDrCr == "Cr") {
            journalLineItems.push({
                LedgerId: selectedLedger,
                Memo: memo,
                Debit: 0,
                Credit: amt,
                ChequeDate: "",
                ChequeNo: "",
                Extra1: "OnAccount",
                ProjectId: projectId,
                CategoryId: categoryId
            });
        }
        else if (selectedDrCr == "Dr") {
            journalLineItems.push({
                LedgerId: selectedLedger,
                Memo: memo,
                Debit: amt,
                Credit: 0,
                ChequeDate: "",
                ChequeNo: "",
                Extra1: "OnAccount",
                ProjectId: projectId,
                CategoryId: categoryId
            });
        }
        
        $("#accountLedger").data("kendoDropDownList").value("Please select");
        $("select#drOrCr").val(''); //$("select#drOrCr")[0].selectedIndex = 0;
        $("#amount").val("");
        $("#memo").val("");
        $("#project").val("");
        $("#project").trigger("chosen:updated");
        $("#category").val("");
        $("#category").trigger("chosen:updated");

        renderLineItems();
    });

    $('#againstModal').on('hidden.bs.modal', function () {
        var datasource = $('#journalsGrid').data().kendoGrid.dataSource.data();
        var indexOfSelectedLedger = -1;
        for (i = 0; i < datasource.length; i++) {
            if (datasource[i].ledgerId == selectedLedgerId) {
                indexOfSelectedLedger = i;
                break;
            }
        }
        datasource[indexOfSelectedLedger].set('amount', getTotalInvoiceAmount(selectedLedgerId));
        var totalDrCr = getTotalCreditAndDebitAmount();
        console.log(totalDrCr);
        $("#totalDebit").val(totalDrCr[0]);
        $("#totalCredit").val(totalDrCr[1]);
    });

    $('#onAccountModal').on('hidden.bs.modal', function () {
        var datasource = $('#journalsGrid').data().kendoGrid.dataSource.data();
        var indexOfSelectedLedger = -1;
        for (i = 0; i < datasource.length; i++) {
            if (datasource[i].ledgerId == selectedLedgerId) {
                indexOfSelectedLedger = i;
                break;
            }
        }
        datasource[indexOfSelectedLedger].set('amount', getTotalInvoiceAmount(selectedLedgerId));
        var totalDrCr = getTotalCreditAndDebitAmount();
        console.log(totalDrCr);
        $("#totalDebit").val(totalDrCr[0]);
        $("#totalCredit").val(totalDrCr[1]);
    });

    $('#partyBalanceTbody').on('change', '.invAmt', function () {
        //console.log($(this).val());
        var indexOfObject = applyAgainstEntries.findIndex(p=>p.InvoiceNo == $(this).attr("id"));
        applyAgainstEntries[indexOfObject].Amount = $(this).val();
        selectedLedgerId=applyAgainstEntries[indexOfObject].LedgerId;
        
        $("#totalInvoiceAmt").val(Utilities.FormatCurrency(getTotalInvoiceAmount(selectedLedgerId)));
    });

    $("#applyAmount").change(function () {
        var drCr = "";
        var datasource = $('#journalsGrid').data().kendoGrid.dataSource.data();
        for (i = 0; i < datasource.length; i++) {
            if (datasource[i].ledgerId == selectedLedgerId) {
                drCr = datasource[i].DrCr;
                break;
            }
        }
        applyAgainstEntries.push({
            LedgerId: selectedLedgerId,
            DrCr: drCr,
            Amount: $("#applyAmount").val(),
            ReferenceType: "OnAccount",
            //AgainstInvoiceNo: "0",
            //AgainstVoucherNo:"0",
            //AgainstVoucherTypeId:1
        });
    });

    $("#newAmount").change(function () {
        var drCr = "";
        var datasource = $('#journalsGrid').data().kendoGrid.dataSource.data();
        for (i = 0; i < datasource.length; i++) {
            if (datasource[i].ledgerId == selectedLedgerId) {
                drCr = datasource[i].DrCr;
                break;
            }
        }
        applyAgainstEntries.push({
            LedgerId: selectedLedgerId,
            DrCr: drCr,
            Amount: $("#newAmount").val(),
            ReferenceType: "New",
            //AgainstInvoiceNo: "0",
            //AgainstVoucherNo:"0",
            //AgainstVoucherTypeId:1
        });
    });

    $("#saveJournal, #savePrint").click(function () {
        var myid = $(this).attr('id');
        //var datasource = $('#journalsGrid').data().kendoGrid.dataSource.data();
        //console.log(datasource);
        //console.log(applyAgainstEntries);
        //return;
        //var totalDrCr = getTotalCreditAndDebitAmount();
        //console.log(journalLineItems);

        if ($("#totalDebit").val() != $("#totalCredit").val())
        {
            Utilities.ErrorNotification("Total credit must be same as total debit!");
            return;
        }
        else
        {
            var journalMasterInfo = {    
                VoucherNo:$("#voucherNo").val(),
                InvoiceNo:  $("#voucherNo").val(),
                TotalAmount: 0,
                Narration: "",
                Date: $("#date").val(),
                UserId: matUserInfo.UserId
            };
            var totalAmt = 0.0;
            var journalDetails = [];
            printData.date = $("#date").val();
            printData.journalNo = $("#voucherNo").val();
            //reset journalPartyBalance
            journalPartyBalance = [];
            for (i = 0; i < journalLineItems.length; i++)
            {
                //console.log(journalLineItems);
                totalAmt += (parseFloat(journalLineItems[i].Credit) + parseFloat(journalLineItems[i].Debit));
                printData.totalCredit += journalLineItems[i].Credit;
                printData.totalDebit -= journalLineItems[i].Debit;
                var projectObj = projects.find(a => a.ProjectId == journalLineItems[i].ProjectId);
                var projectName = projectObj == undefined ? "" : projectObj.ProjectName;
                var categoryObj = depts.find(c => c.CategoryId == journalLineItems[i].CategoryId);
                var categoryName = categoryObj == undefined ? "" : categoryObj.CategoryName;
                if (journalLineItems[i].Credit >0)
                {
                    journalDetails.push({
                        LedgerId: journalLineItems[i].LedgerId,
                        Memo: journalLineItems[i].Memo,
                        Debit: 0,
                        Credit: journalLineItems[i].Credit,
                        ChequeDate: "",
                        ChequeNo: "",
                        Extra1: "OnAccount", //datasource[i].apply
                        ProjectId: journalLineItems[i].ProjectId,
                        CategoryId: journalLineItems[i].CategoryId
                    });
                    printData.lineItems.push({
                        SN: i + 1,
                        LedgerName: accountLedgers.find(a => a.ledgerId == journalLineItems[i].LedgerId).ledgerName,
                        Debit: 0,
                        Credit: journalLineItems[i].Credit,
                        ProjectName: projectName,
                        CategoryName: categoryName,
                        Memo: journalLineItems[i].Memo
                    });
                }
                else if (journalLineItems[i].Debit>0)
                {
                    journalDetails.push({
                        LedgerId: journalLineItems[i].LedgerId,
                        Memo: journalLineItems[i].Memo,
                        Debit: journalLineItems[i].Debit,
                        Credit: 0,
                        ChequeDate: "",
                        ChequeNo: "",
                        Extra1: "OnAccount", //datasource[i].apply
                        ProjectId: journalLineItems[i].ProjectId,
                        CategoryId: journalLineItems[i].CategoryId
                    });
                    printData.lineItems.push({
                        SN: i + 1,
                        LedgerName: accountLedgers.find(a => a.ledgerId == journalLineItems[i].LedgerId).ledgerName,
                        Debit: journalLineItems[i].Debit,
                        Credit: 0,
                        ProjectName: projectName,
                        CategoryName: categoryName,
                        Memo: journalLineItems[i].Memo
                    });
                }

                //add JournalLine Items which are either Payables or Receivables to the partybalance dataset 
                var targetAcctLedger = accountLedgers.find(x => x.ledgerId.toString() === journalLineItems[i].LedgerId)
                if (targetAcctLedger) {
                    const payablesAndReceivables = ["Account Receivables", "Account Payables"];
                    var targetAccountGroupName = targetAcctLedger.accountGroupName;
                    if (payablesAndReceivables.indexOf(targetAccountGroupName) > -1) {
                        //add entry for the journal to the partybalance list | General Journals voucherTypeId is 6
                        let debit = journalLineItems[i].Debit > 0 ? journalLineItems[i].Debit : 0;
                        let credit = journalLineItems[i].Credit > 0 ? journalLineItems[i].Credit : 0;
                        journalPartyBalance.push({
                            AgainstInvoiceNo: 'NA',
                            AgainstVoucherNo: 'NA',
                            Credit: credit,
                            Debit: debit,
                            InvoiceNo: $("#voucherNo").val(),
                            LedgerId: targetAcctLedger.ledgerId,
                            ReferenceType: "NEW",
                            VoucherNo: $("#voucherNo").val(),
                            //VoucherTypeId: targetAccountGroupName === "Account Receivables"  ? 28 : 29,
                            VoucherTypeId: 6,
                            AgainstVoucherTypeId: 6
                        });

                    }
                }
                
            }
           
            journalMasterInfo.TotalAmount = totalAmt;
            journalMasterInfo.doneBy = $("#UserId").html();
            var partyBalance = [];
            for (i = 0; i < applyAgainstEntries.length; i++)
            {
                partyBalance.push({
                    AgainstInvoiceNo: applyAgainstEntries[i].InvoiceNo,
                    AgainstVoucherNo: applyAgainstEntries[i].InvoiceNo,
                    Credit: applyAgainstEntries[i].Amount,
                    InvoiceNo: $("#voucherNo").val(),
                    LedgerId: applyAgainstEntries[i].LedgerId,
                    ReferenceType: "Against",
                    VoucherNo: $("#voucherNo").val(),
                    AgainstVoucherTypeId: applyAgainstEntries[i].VoucherTypeId
                });
            }            

            var toSave = {
                JournalMasterInfo: journalMasterInfo,
                JournalDetailsInfo: journalDetails,
                PartyBalanceInfo: partyBalance,
                JournalPartyBalanceInfo: journalPartyBalance
            };
            //console.log(toSave); return;
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/RunGeneralJournal/Save",
                type: "Post",
                data:JSON.stringify(toSave),
                contentType: "application/json",
                success: function (data) {
                    if (data == true)
                    {
                        Utilities.SuccessNotification("Journal saved!");
                        if (myid == "savePrint") {
                            print();
                        }
                        window.location = "/Company/RunGeneralJournals/Index";
                    }
                    else
                    {
                        Utilities.ErrorNotification("Journal could not be saved.");
                    }
                    Utilities.Loader.Hide();
                },
                error: function (err) {
                    Utilities.Loader.Hide();
                }
            });
            //setTimeout(function () {
            //    Utilities.SuccessNotification("Journal saved!");
            //    Utilities.Loader.Hide();
            //    window.location = "/Company/RunGeneralJournals/Index";
            //}, 3000);
        }
    });


    $("#accountLedger").change(function () {

        accountLedgers.forEach(function (ledger) {

            if ($("#accountLedger").val() == ledger.ledgerId) {

                if (ledger.accountNature.includes('Liabilities')) {
                    $("#drOrCr").val("Cr");
                }
                else if (ledger.accountNature.includes('Assets')) {
                    $("#drOrCr").val("Dr");
                }
                else if (ledger.accountNature.includes('Expenses')) {
                    $("#drOrCr").val("Dr");
                }
                else if (ledger.accountNature.includes('Income')) {
                    $("#drOrCr").val("Cr");
                }
                else {
                    $("#drOrCr").val("NA");
                }
            };
        });

    });

});


function renderLineItems()
{
    var totalCredit = 0.0;
    var totalDebit = 0.0;
    var output = "";
    for (i = 0; i < journalLineItems.length; i++)
    {
        totalCredit += parseFloat(journalLineItems[i].Credit);
        totalDebit += parseFloat(journalLineItems[i].Debit);
        var projectObj = projects.find(p => p.ProjectId == journalLineItems[i].ProjectId);
        var projectName = projectObj == undefined? "NA" : projectObj.ProjectName;
        var categoryObj = depts.find(p => p.CategoryId == journalLineItems[i].CategoryId);
        var categoryName = categoryObj == undefined ? "NA" : categoryObj.CategoryName;
        var thisDebit = journalLineItems[i].Debit == 0 ? "" : Utilities.FormatCurrency(parseFloat(journalLineItems[i].Debit));
        var thisCredit = journalLineItems[i].Credit == 0 ? "" : Utilities.FormatCurrency(parseFloat(journalLineItems[i].Credit));

        output += '<tr>'+
                    '<td>' + (i + 1) + '</td>'+
                    '<td>' + accountLedgers.find(p => p.ledgerId == journalLineItems[i].LedgerId).ledgerName + '</td>' +
                    '<td style="text-align: right;">' + Utilities.FormatCurrency(thisDebit) + '</td>' +
                    '<td style="text-align: right;">' + Utilities.FormatCurrency(thisCredit) + '</td>' +
                    '<td>' + projectName + '</td>' +
                    '<td>' + categoryName + '</td>' +
                    '<td>' + journalLineItems[i].Memo + '</td>' +
                    '<td><button onclick="removeLineItem(' + journalLineItems[i].LedgerId + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></td>'+
                   '</tr>';
    }
    //console.log(totalCredit);
    //console.log(totalDebit);
    //console.log(journalLineItems);
    $("#totalDebit").val(Utilities.FormatCurrency(totalDebit));
    $("#totalCredit").val(Utilities.FormatCurrency(totalCredit));
    $("#journalLineItemTbody").html(output);
}

function removeLineItem(ledgerId) {
    var id = ledgerId.toString().trim();
    if (confirm("Remove this record?")) {
        var indexOfObjectToRemove = journalLineItems.findIndex(p => p.LedgerId == id);
        journalLineItems.splice(indexOfObjectToRemove, 1);
        renderLineItems();
    }
}

function getTotalInvoiceAmount(ledgerId)
{
    var totalSupplierCustomerBalance = 0;
    
    for (i = 0; i < applyAgainstEntries.length; i++) {
        if (applyAgainstEntries[i].LedgerId == selectedLedgerId) {
            totalSupplierCustomerBalance += parseFloat(applyAgainstEntries[i].Amount);
        }
    }
    return totalSupplierCustomerBalance;
}

function renderGrid()
{
    $("#journalsGrid").kendoGrid({
        dataSource:  {
            transport: {
                read: function (entries) {
                    entries.success(journalsEntries);
                },
                create: function (entries) {
                    console.log(entries);
                    entries.success(entries.data);
                },
                update: function (entries) {
                    entries.success();
                },
                destroy: function (entries) {
                    entries.success();
                },
                parameterMap: function (data) { return JSON.stringify(data); }
            },
            schema: {
                model: {
                    id: "id",
                    fields: {
                        ledgerId: { editable: true, validation: {required:true  } },
                        drcr: { editable: true, validation: { required: true } },
                        //apply: { editable: true},
                        amount: { editable: true, validation: { required: true } },
                        memo: { editable: true, validation: { required: false } },
                        //description: { editable: true, validation: { required: true } }
                    }
                }
            },
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        pageable: {
            pageSize: 10,
            pageSizes: [10, 25, 50, 100, 1000],
            previousNext: true,
            buttonCount: 5,
        },
        //detailInit: detailInit,
        toolbar: ["create"],
        columns: [
                    { field: "ledgerId", title: "Account Ledger", editor: accountLedgerDropDownEditor, template: "#= getAccountLedger(ledgerId) #" },
                    { field: "drcr", title: "Dr/Cr", editor: drcrDropDownEditor },
                    //{ field: "apply", title: "Apply", editor: applyDropDownEditor },
                    { field: "amount", title: "Amount",editor:amountNumericEditor },
                    { field: "memo", title: "Memo" },
                    {
                        //command: [
                        //            {
                        //                name: "edit",
                        //                text: {
                        //                        edit: "Edit",               // This is the localization for Edit button
                        //                        update: "Save",             // This is the localization for Update button
                        //                        cancel: "Cancel"    // This is the localization for Cancel button
                        //                        }
                        //            },
                        //            {
                        //                name: "destroy",
                        //                text: "Remove"           // This is the localization for Delete button
                        //            }
                        //        ]
                        command: [{name:"destroy", title: "", text: "", width: "100px"}]
                    }
                ],
        editable: "incell",
        save:function(e)
        {
            //var k = $("#journalsGrid").data().kendoGrid.dataSource.view();
            //console.log(k);
            var totalDrCr = getTotalCreditAndDebitAmount();
            $("#totalDebit").val(totalDrCr[0]);
            $("#totalCredit").val(totalDrCr[1]);
        },
        remove: function (e) {
            //var modelToRemove = e.model;
            //console.log(modelToRemove);
            //var indexOfObjectToRemove = applyAgainstEntries.findIndex(p=>p.LedgerId == modelToRemove.ledgerId);
            //applyAgainstEntries.splice(indexOfObjectToRemove, 1);
            var totalDrCr = getTotalCreditAndDebitAmount();
            $("#totalDebit").val(totalDrCr[0]);
            $("#totalCredit").val(totalDrCr[1]);
        }
    });

}

function detailInit(e)
{
    $("<div/>").appendTo(e.detailCell).kendoGrid({
        dataSource: {
            transport: {
                read: function (entries) {
                    var details = [];
                    if (typeof (e.data.StoreLines) === "undefined") {
                        details = [];
                    }
                    else {
                        details = e.data.StoreLines;
                    }
                    entries.success(details);
                },
                parameterMap: function (data) { return JSON.stringify(data); }
            },
            schema: {
                model: {
                    id: "StoreId",
                    fields: {
                        StoreName: { editable: false },
                        TotalDeliveryNote: { editable: false }
                    },
                }
            }
        },
        scrollable: false,
        sortable: true,
        pageable: true,
        //},
        columns: [
            { field: 'StoreName', title: 'Warehouse' },
            { field: 'TotalDeliveryNote', title: 'Total Sales' },
            { field: 'TotalMaterialReceipt', title: 'Total Purchase' },
            { field: 'TotalAdjustment', title: 'Total Adjustment' },
            { field: 'TotalTransferIn', title: 'Total Transfer In' },
            { field: 'TotalTransferOut', title: 'Total Transfer Out' },
        ],
        pageable: {
            pageSize: 10,
            pageSizes: [10, 25, 50, 100, 1000],
            previousNext: true,
            buttonCount: 5,
        },
    });

}

function getDropdowns()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/RunGeneralJournal/GetDropdowns",
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            accountLedgers = data.AccountLedger;
            console.log(accountLedgers);
            currencies = data.Currencies;
            $("#voucherNo").val(data.InvoiceNo);
            $("#accountLedger").kendoDropDownList({
                filter: "contains",
                optionLabel: "Please Select...",
                dataTextField: "ledgerName",
                dataValueField: "ledgerId",
                dataSource: accountLedgers
            });
            //console.log(currencies);
            //renderGrid();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function accountLedgerDropDownEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="ledgerName" data-value-field="ledgerId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "ledgerName",
            dataValueField: "ledgerId",
            dataSource: {
                data: accountLedgers
            },
            template: '<span>#: ledgerName #</span>',
            filter: "contains",
        });
}

function drcrDropDownEditor(container, options) {
    var data = [
                { id: "Dr",val:"Dr" },
                { id: "Cr", val: "Cr" }
                ];
    $('<input name="' + options.field + '" data-text-field="val" data-value-field="id" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "val",
            dataValueField: "id",
            dataSource: {
                data: data
            },
            //template: '<span>#: val #</span>',
            filter: "contains",
            change:function(e)
            {
                //var grid = $("#journalsGrid").data("kendoGrid"),
                //model = grid.dataItem(this.element.closest("tr"));
                //var accountGroupName = accountLedgers.find(p=>p.ledgerId == model.ledgerId).accountGroupName;
                //selectedLedgerId = model.ledgerId;
                //if (accountGroupName != "Account Receivables" && accountGroupName != "Account Payables")
                //{
                //    $("#newModal").modal("show");
                //}
                //console.log(model);
            }
        });
}

function amountNumericEditor(container, options) {
    $('<input name="' + options.field + '" data-text-field="val" data-value-field="id" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoNumericTextBox({
            min: 0
        });
}

function applyDropDownEditor(container, options) {
    var data = [
                //{ val: "New" },
                { val: "On Account" },
                { val: "Against" }
               ];
    $('<input name="' + options.field + '" data-text-field="val" data-value-field="id" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "val",
            dataValueField: "val",
            dataSource: {
                data: data
            },
            filter: "contains",
            change:function()
            {
                var applyAction = this.value();
                //alert(applyAction);
                if(applyAction == "Against")
                {
                    $("#againstModal").modal("show");
                    var grid = $("#journalsGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    selectedLedgerId = model.ledgerId;

                    //console.log(model);
                    if (applyAgainstEntries.findIndex(p=>p.LedgerId == selectedLedgerId)>=0)
                    {
                        console.log(applyAgainstEntries);
                        var output = "";
                        for (i = 0; i < applyAgainstEntries.length; i++)
                        {
                            if (applyAgainstEntries[i].LedgerId == selectedLedgerId)
                            {
                                output +=
                               '<tr>\
                                    <td>'+ (i + 1) + '</td>\
                                    <td>' + applyAgainstEntries[i].InvoiceNo + '</td>\
                                    <td>' + applyAgainstEntries[i].Balance + '</td>\
                                    <td><input class="form-control invAmt" type="number" min="0" value="' + applyAgainstEntries[i].Amount + '" id="' + applyAgainstEntries[i].InvoiceNo + '"/></td>\
                                    <td>' + applyAgainstEntries[i].DrCr + '</td>\
                                </tr>';
                            }
                            
                        }
                        $("#partyBalanceTbody").html(output);
                    }
                    else
                    {
                        getInvoices(model.ledgerId, model.drcr, 4);
                    }                    
                }
                else if (applyAction == "On Account") {
                    $("#onAccountModal").modal("show");
                    var grid = $("#journalsGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    selectedLedgerId = model.ledgerId;
                }
                else if(applyAction=="New")
                {
                    //$("#onAccountModal").modal("show");
                    var grid = $("#journalsGrid").data("kendoGrid"),
                    model = grid.dataItem(this.element.closest("tr"));
                    selectedLedgerId = model.ledgerId;
                }
                //console.log(journalsEntries);
                //$("#journalsGrid").data().kendoGrid.expandRow(this.tbody.find("tr.k-master-row").first());
            }
        });
}

function getInvoices(ledgerId,drCr,voucherNo)
{
    Utilities.Loader.Show();
    var param = {
        LedgerId: ledgerId,
        DebitOrCredit: drCr,
        voucherNo: voucherNo
    };
    $.ajax({
        url: API_BASE_URL + "/RunGeneralJournal/GetPartyBalance",
        type: "Post",
        data:JSON.stringify(param),
        contentType: "application/json",
        success: function (data) {
           // console.log(data);
            customerSupplierBalance = data;
            var output = "";
            for (i = 0; i < data.length; i++) {
                applyAgainstEntries.push({
                    LedgerId: ledgerId,
                    DrCr: drCr,
                    Balance: data[i].balance,
                    InvoiceNo: data[i].invoiceNo,
                    VoucherTypeId: data[i].voucherTypeId,
                    Amount: 0,
                    ReferenceType: "Against"
                });

                output +=
                   '<tr>\
                        <td>'+ (i + 1) + '</td>\
                        <td>' + data[i].invoiceNo + '</td>\
                        <td>' + data[i].balance + '</td>\
                        <td><input class="form-control invAmt" type="number" min="0" value="0" id="' + data[i].invoiceNo + '"/></td>\
                        <td>' + drCr + '</td>\
                    </tr>';
            }
            $("#partyBalanceTbody").html(output);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function currencyDropDownEditor(container, options) {
    $('<input required name="' + options.field + '" data-text-field="currencyName" data-value-field="exchangeRateId" data-bind="value:' + options.field + '"/>')
        .appendTo(container)
        .kendoComboBox({
            autoBind: true,
            highlightFirst: true,
            suggest: true,
            dataTextField: "currencyName",
            dataValueField: "exchangeRateId",
            dataSource: {
                data: currencies
            },
            template: '<span>#: currencyName #</span>',
            filter: "contains",
        });
}

function applyClick(e)
{
    var tr = $(e.target).closest("tr"); // get the current table row (tr)
    // get the data bound to the current table row
    var data = this.dataItem(tr);//userid
    model.set("amount",90);
    console.log(data);
}

function getAccountLedger(ledgerId)
{
    for(i=0;i<accountLedgers.length;i++)
    {
        if(accountLedgers[i].ledgerId==ledgerId)
        {
            return accountLedgers[i].ledgerName;
        }
    }
    return "";
}

function getCurrency(id) {
    for (i = 0; i < currencies.length; i++) {
        if (currencies[i].exchangeRateId == id) {
            return currencies[i].currencyName;
        }
    }
    return "";
}

function getTotalCreditAndDebitAmount()
{
    var source = $('#journalsGrid').data().kendoGrid.dataSource.data();
    console.log(source);
    var output = [];
    output[0] = 0.00;
    output[1] = 0.00;

    for(i=0;i<source.length;i++)
    {
        if(source[i].amount!="" && !isNaN(source[i].amount) && source[i].drcr=="Dr")
        {
            output[0] += output[0] + source[i].amount;
        }

        else if (source[i].amount != "" && !isNaN(source[i].amount) && source[i].drcr == "Cr")
        {
            output[1] += output[1] + source[i].amount;
        }
    }
    return output;
}

function getLookUps() {
    var projectLookupAjax = $.ajax({
        url: API_BASE_URL + "/Project/GetProject",
        type: "GET",
        contentType: "application/json",
    });

    var categoryLookupAjax = $.ajax({
        url: API_BASE_URL + "/Category/GetCategory",
        type: "GET",
        contentType: "application/json",
    });

    $.when(projectLookupAjax, categoryLookupAjax)
    .done(function (dataProject, dataCategory) {
        projects = dataProject[0];
        depts = dataCategory[0];
        console.log("projects", dataProject);
        console.log("depts", dataCategory);
        $("#project").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(projects, 0, 1));
        $("#project").chosen({ width: "100%", margin: "1px" });
        $("#category").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(depts, 0, 1));
        $("#category").chosen({ width: "100%", margin: "1px" });
    });
}

function validateImage() {
    imgurl = '../../Assets/mat/img/MatOnlineLogo.png';
    // imgurl = 'images-folder/' + user.StoreId + '.png';
    imgToBase64(imgurl, function (base64) {
        base64Img = base64;
    });
    //if (base64Img == null) {

    //    imgurl = 'images-folder/' + user.StoreId + '.png';
    //    imgToBase64(imgurl, function (base64) {
    //        base64Img = base64;
    //    });
    //}
}

function imgToBase64(src, callback) {
    var outputFormat = src.substr(-3) === 'png' ? 'image/png' : 'image/jpeg';
    var img = new Image();
    img.crossOrigin = 'Anonymous';
    img.onload = function () {
        console.log("sherlock's portriat");
        var canvas = document.createElement('CANVAS');
        var ctx = canvas.getContext('2d');
        var dataURL;
        canvas.height = this.naturalHeight;
        canvas.width = this.naturalWidth;
        ctx.drawImage(this, 0, 0);
        dataURL = canvas.toDataURL(outputFormat);
        callback(dataURL);
        imgurl = '../../Assets/mat/img/MatOnlineLogo.png'; 
        //if (dataURL.slice(11, 15) == "jpeg") {
        //    imgurl = 'images-folder/' + user.StoreId + '.jpg';
        //}
        //else {
        //    imgurl = 'images-folder/' + user.StoreId + '.png';
        //}
    };
    img.src = src;
    if (img.complete || img.complete === undefined) {
        img.src = "data:image/gif;base64,R0lGODlhAQABAIAAAAAAAP///ywAAAAAAQABAAACAUwAOw==";
        img.src = src;
    }
}


//get image
var imgsrc = $("#companyImage").attr('src');
var imageUrl;


//convert image to base64;
function getDataUri(url, callback) {
    var image = new Image();
    image.setAttribute('crossOrigin', 'anonymous'); //getting images from external domain
    image.onload = function () {
        var canvas = document.createElement('canvas');
        canvas.width = this.naturalWidth;
        canvas.height = this.naturalHeight;

        //next three lines for white background in case png has a transparent background
        var ctx = canvas.getContext('2d');
        ctx.fillStyle = '#fff';  /// set white fill style
        ctx.fillRect(0, 0, canvas.width, canvas.height);
        canvas.getContext('2d').drawImage(this, 0, 0);
        // resolve(canvas.toDataURL('image/jpeg'));
        callback(canvas.toDataURL('image/jpeg'));
    };
    image.src = url;
}

getDataUri(imgsrc, getLogo);

//save the logo to global;
function getLogo(logo) {
    imageUrl = logo;
}


function print() {
    var totalCredit = 0.0;
    var totalDebit = 0.0;
     
    var companyName = $("#companyName").val();
    var companyAddress = $("#companyAddress").val();
    var thisEmail = $("#companyEmail").val();
    var thisPhone = $("#companyPhone").val();
    var JournalNo = $("#voucherNo").val();
    var thisDate = $("#date").val();
    
    var objToPrint = [];
    $.each(journalLineItems, function (count, row) {
        totalCredit += parseFloat(row.Credit);
        totalDebit += parseFloat(row.Debit);
        var projectObj = projects.find(p => p.ProjectId == row.ProjectId);
        var projectName = projectObj == undefined ? "NA" : projectObj.ProjectName;
        var categoryObj = depts.find(p => p.CategoryId == row.CategoryId);
        var categoryName = categoryObj == undefined ? "NA" : categoryObj.CategoryName;
        var thisDebit = row.Debit == 0 ? "" : Utilities.FormatCurrency(row.Debit);
        var thisCredit = row.Credit == 0 ? "" : Utilities.FormatCurrency(row.Credit);
        objToPrint.push({
            "SlNo": count + 1,
            "Name": accountLedgers.find(p => p.ledgerId == row.LedgerId).ledgerName,
            "Debit": thisDebit,
            "Credit": thisCredit,
            "Project": projectName,
            "Dept": categoryName,
            "Memo": row.Memo
        });
    });

    columns = [
        { title: "S/N", dataKey: "SlNo" },
        { title: "Account Ledger", dataKey: "Name" },
        { title: "Dr", dataKey: "Debit" },
        { title: "Cr", dataKey: "Credit" },
        { title: "Project", dataKey: "Project" },
        { title: "Department", dataKey: "Dept" },
        { title: "Memo", dataKey: "Memo" }
    ];

    var doc = new jsPDF('portrait', 'pt', 'letter');
    doc.setDrawColor(0);

    //start drawing

    doc.addImage(imageUrl, 'jpg', 40, 80, 80, 70);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text(companyName, 320, 80, 'center');
    doc.setFontSize(9);
    doc.setFontType("normal");
    doc.text(companyAddress, 320, 90, 'center');

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Email Id: ', 400, 150);
    doc.setFontType("normal");
    doc.text(thisEmail, 460, 150);
    doc.setFontType("bold");
    doc.text('Phone: ', 400, 165);
    doc.setFontType("normal");
    doc.text(thisPhone, 460, 165);

    doc.line(40, 170, 570, 170);


    //doc.line(395, 175, 395, 215);//horizontal line
    doc.setFontType("bold");
    doc.text('Journal No: ', 400, 185);
    doc.setFontType("normal");
    doc.text(JournalNo, 460, 185);
    doc.setFontType("bold");
    doc.text('Journal Date: ', 400, 205);
    doc.setFontType("normal");
    doc.text(thisDate, 460, 205);

    doc.line(40, 220, 570, 220);

    doc.setFontSize(12);
    doc.setFontType("bold");
    doc.text('Journal Voucher', 250, 240);

    //doc.line(120, 20, 120, 60); horizontal line
    doc.setFontSize(8);
    doc.autoTable(columns, objToPrint, {
        startY: 255,
        theme: 'striped',
        styles: { "overflow": "linebreak", "cellWidth": "wrap", "rowPageBreak": "avoid", "fontSize": "7", "lineColor": "100", "lineWidth": ".25"},

        columnStyles: {
            SlNo: { columnWidth: 30, },
            Debit: { columnWidth: 60, halign: 'right' },
            Credit: { columnWidth: 60, halign: 'right' },
            Name: { columnWidth: 110 },
            Dept: { columnWidth: 100 },
            Project: { columnWidth: 100 }

        },
    });

    doc.line(40, doc.autoTable.previous.finalY + 5, 570, doc.autoTable.previous.finalY + 5);

    doc.setFontSize(9);
    doc.setFontType("bold");
    doc.text('Total Debit: N' + Utilities.FormatCurrency(totalDebit), 555, doc.autoTable.previous.finalY + 20, "right");
    doc.text('Total Credit: N' + Utilities.FormatCurrency(totalCredit), 555, doc.autoTable.previous.finalY + 40, "right");

    doc.line(40, doc.autoTable.previous.finalY + 50, 570, doc.autoTable.previous.finalY + 50);

    //convert to words
    var DebitInWords = Utilities.NumberToWords(totalDebit);
    var CeditInWords = Utilities.NumberToWords(totalCredit);

    //Capitalize Each Word
    const wordsDebit = DebitInWords.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());
    const wordsCredit = CeditInWords.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());

    var strArr1 = doc.splitTextToSize('Total Debit In Words: ' + wordsDebit + ' Naira Only.', 500);
    doc.text(strArr1, 40, doc.autoTable.previous.finalY + 90);

    var strArr2 = doc.splitTextToSize('Total Credit In Words: ' + wordsDebit + ' Naira Only.', 500);
    doc.text(strArr2, 40, doc.autoTable.previous.finalY + 115);

    doc.setFontType("bold");
    doc.text('Journal approved by:  _________________________________', 40, doc.autoTable.previous.finalY + 150);

    doc.text('Done by:  ' + $("#nameSpan").html(), 350, doc.autoTable.previous.finalY + 150);
    doc.text('___________________', 390, doc.autoTable.previous.finalY + 155);
    doc.autoPrint();
    doc.save('Journal Voucher.pdf');
    //window.open(doc.output('bloburl'));
    window.open(doc.output('bloburl'), "_blank", "toolbar=yes,top=500,right=500,width=1000,height=1000");  
}