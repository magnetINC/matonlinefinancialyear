﻿var accountGroups = [];
var ledger = {};
var ledgerwe = {};
var groups = [];
var cities = [];
var states = [];

Utilities.Loader.Show();

var groupsAjx = $.ajax({
    url: API_BASE_URL + "/AccountGroup/GetAccountGroups/?newRecord=" + true,
    type: "GET",
    contentType: "application/json"
});

var citiesAjx = $.ajax({
    url: API_BASE_URL + '/City/GetCities',
    type: "GET",
    contentType: "application/json"
});

var statesAjx = $.ajax({
    url: API_BASE_URL + '/State/GetStates',
    type: "GET",
    contentType: "application/json"
});

$(function () {

    getAccountGroups();
    $("#secondaryTab").hide();
    $("#r1").hide();
    $("#r2").hide();
    $("#r3").hide();
    $("#errorMsg").hide();
    $("#successMsg").hide();

    $("#saveAccount").click(function ()
    {
        ledgerwe = {};

        if (populateObject())
        {
           // var PostData = JSON.stringify(ledger);
            ledgerwe.IsBankAccount = false;
            Utilities.Loader.Show();
           
            $.ajax({
                url: API_BASE_URL + "/AccountLedger/CreateAccountLedger",
                type: 'POST',
                data: JSON.stringify(ledgerwe),
                contentType: "application/json",
                success: function (data)
                {
                    if (data.LedgerCreation == "SUCCESS")
                    {
                        $("#successMsg").show();

                        //reset form to default state
                        $("#secondaryTab").hide();
                        $("#r1").hide();
                        $("#r2").hide();
                        $("#r3").hide();
                        $("#errorMsg").hide();

                        $("#ledgerName").val("");
                        $("#openingBalance").val("");
                        $("#narration").val("");
                        $("#companyName").val();
                        $("#address").val();
                        $("#phone").val();
                        $("#mobileNo").val();
                        $("#email").val();
                        $("#creditPeriod").val(0);
                        $("#creditLimit").val(0);
                        $("#tin").val();
                        $("#pan").val();
                        $("#accountNumber").val("");
                        $("#branchName").val("");
                        $("#branchCode").val("");
                        $("#accountCode").val("");
                        $("#active").prop("checked", false);
                        $("#accountGroup").val("0")

                    }
                    else if (data.LedgerCreation == "LEDGER_EXISTS") {
                        $("#errorMsg").html('<strong>Oops!</strong> Account name already exists!');
                        $("#errorMsg").show();
                    }
                    else if (data.LedgerCreation == "ACCOUNTCODE_EXISTS") {
                        $("#errorMsg").html('<strong>Oops!</strong> Account Code already exists!');
                        $("#errorMsg").show();
                    }
                    else {
                        $("#errorMsg").html('<strong>Oops!</strong> Something went wrong!');
                        $("#errorMsg").show();
                    }

                    Utilities.Loader.Hide();
                },
                error: function (request, error) {
                    Utilities.Loader.Hide();
                }
            });
        }

    });


    $("#accountGroup").change(function (e) {
        var val = $("#accountGroup").val();
        if (val == 22 || val == 26)  //if account group is account recieveable or account payable, show "secondary details" tab
        {
            $("#secondaryTab").show();
            $("#billByBill").val("true");
            $("#billByBill").attr("disabled", "disabled");
        }
        else {
            $("#secondaryTab").hide();
        }
        if (val == 27) //if account group is cash,deactivate ledger name control
        {
            $("#ledgerName").attr("disabled", "disabled");
        }
        else {
            $("#ledgerName").removeAttr("disabled", "disabled");
        }
        if (val == 17 || val == 28)  //if account group is bank or bank overdraft, show bank details controls
        {
            $("#r1").show();
        }
        else {
            $("#r1").hide();
        }
    });
    $("#state").change(function () {
        var val = $("#state").val();
        var stateCities = findStateCities(val);
        var citiesHtml = "<option></option>";
        $.each(stateCities, function (i, record) {
            //populate city dropdown
            citiesHtml += '<option value="' + record.RouteId + '">' + record.RouteName + '</option>';
        });
        $("#city").html(citiesHtml);
    });

    $.when(groupsAjx, citiesAjx, statesAjx)
        .done(function (dataGroup, dataCities, dataStates) {
            groups = dataGroup[2].responseJSON;
            cities = dataCities[2].responseJSON;
            states = dataStates[2].responseJSON;
            populateControls();
            Utilities.Loader.Hide();
        });

});

function getAccountGroups() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AccountGroup/GetAccountGroups/?newRecord=" + true,
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            accountGroups = data;
            console.log(accountGroups);
            var groupHtml = "<option value='0'>-Select-</option>";
            $.each(data, function (i, record) {
                //populate group dropdown
                if (record.AccountGroupId != 23 /*&& record.AccountGroupId != 11*/) {
                    groupHtml += '<option value="' + record.AccountGroupId + '">' + record.AccountGroupName + '</option>';
                }
            });
            $("#accountGroup").html(groupHtml);
            // $("#accountGroup").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(data, 1, 2));
            // $("#accountGroup").trigger("chosen:updated");
            Utilities.Loader.Hide();
        },
        error: function (request, error) {
            Utilities.Loader.Hide();
        }
    });
}

function populateObject()
{
    
    ledgerwe.AccountLedgerInfo = {};
    ledgerwe.IsBankAccount = false;
    ledgerwe.IsCashAccount = false;
    ledgerwe.IsSundryDebtorOrCreditor = false;

    var isAllValidated = true;
    var crOrDr = "";

       //Utilities.IsValueEmptyOrNull($("#accountGroup").val()) &&

    if ($("#accountGroup").val() != "0")
    {
        var selectedAccountGroupNature = groups.find(p => p.AccountGroupId == $("#accountGroup").val()).nature;

        if (selectedAccountGroupNature == "Assets" || selectedAccountGroupNature == "Expenses")
        {
            crOrDr = "Dr";
        }
        else if (selectedAccountGroupNature == "Liabilities" || selectedAccountGroupNature == "Income")
        {
            crOrDr = "Cr";
        }
    }

    ledgerwe.AccountLedgerInfo =
    {
        AccountGroupId: $("#accountGroup").val() /*Utilities.IsValueEmptyOrNull($("#accountGroup").val()) && $("#accountGroup").val() != "0" ? $("#accountGroup").val() : isAllValidated = false*/,
        LedgerName: $("#ledgerName").val(), /*Utilities.IsValueEmptyOrNull($("#ledgerName").val()) ? $("#ledgerName").val() : isAllValidated = false*/
        Extra1: $("#accountCode").val(),
        OpeningBalance: $("#openingBalance").val(),
        CrOrDr: crOrDr,
        Narration: $("#narration").val(),
        MailingName: $("#companyName").val(),
        Address: $("#address").val(),
        State: "",
        Phone: $("#phone").val(),
        Mobile: $("#mobileNo").val(),
        Email: $("#email").val(),
        CreditPeriod: $("#creditPeriod").val(),
        CreditLimit: $("#creditLimit").val(),
        PricinglevelId: $("#pricingLevel").val(),
        BillByBill: $("#billByBill").val(),
        Tin: $("#tin").val(),
        Cst: "",
        Pan: $("#pan").val(),
        RouteId: $("#city").val(),
        BankAccountNumber: $("#accountNumber").val(),
        BranchName: $("#branchName").val(),
        BranchCode: $("#branchCode").val(),
        AreaId: $("#state").val(),
        IsActive: $('input[id="active"]').is(':checked')
    };

    if (isAllValidated)
    {
        $("#errorMsg").hide();
        return true;
    }
    else
    {
        $("#errorMsg").show();
        return false;
    }

    console.log(ledgerwe);
}

function populateControls()
{
    var groupHtml = "<option>-Select-</option>";
    var stateHtml = "<option>-Select-</option>";
    $.each(groups, function (i, record) {
        //populate group dropdown [Restricting Stock and Cost of Sales]
        if (record.AccountGroupId != 23 /*&& record.AccountGroupId != 11*/) {
            groupHtml += '<option value="' + record.AccountGroupId + '">' + record.AccountGroupName + '</option>';
        }

    });
    $.each(states, function (i, record) {
        //populate state dropdown
        stateHtml += '<option value="' + record.AreaId + '">' + record.AreaName + '</option>';
    });
    $("#accountGroup").html(groupHtml);
    $("#state").html(stateHtml);
}

function findStateCities(stateId)
{
    var foundCities = [];
    $.each(cities, function (count, record) {
        if (record.AreaId == stateId) {
            foundCities.push(record);
        }
    });
    return foundCities;
}


