﻿var accounts = [];
var table = "";
var deleteIndex = "";
var ledgerList = [];
var excelArrayData = [];
var tableToUpload = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created
var groupsAjx = $.ajax({
    url: API_BASE_URL + '/AccountGroup/GetAccountGroups',
    type: "GET",
    contentType: "application/json"
});

var citiesAjx = $.ajax({
    url: API_BASE_URL + '/City/GetCities',
    type: "GET",
    contentType: "application/json"
});

var statesAjx = $.ajax({
    url: API_BASE_URL + '/State/GetStates',
    type: "GET",
    contentType: "application/json"
});

$(function () {

    getAccounts();

    $.when(groupsAjx, citiesAjx, statesAjx)
   .done(function (dataGroup, dataCities, dataStates) {
       groups = dataGroup[2].responseJSON;
       cities = dataCities[2].responseJSON;
       states = dataStates[2].responseJSON;
       Utilities.Loader.Hide();
   });

    $("#cityTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        cityToEdit = findCity(id);
        $("#editCityId").val(cityToEdit[0]);
        $("#editCityName").val(cityToEdit[1]);
        $("#editStateId").val(cityToEdit[2]);
        $("#editNarration").val(cityToEdit[3]);
        $("#editDate").val(cityToEdit[4]);
        $("#editExtra1").val(cityToEdit[5]);
        $("#editExtra2").val(cityToEdit[6]);
        $('#editModal').modal('toggle');

    });

    $("#cityTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        cityToEdit = findCity(id);
        cityIdToDelete = id;
        $("#deleteCityName").html(cityToEdit[1]);
        $('#deleteModal').modal('toggle');

    });

    $("#accountTable").on("click", ".btnDetails", function () {
        var id = $(this).attr("id");
       // $("#frm" + id).submit();
//        alert(id);
        
    });


    $("#saveCity").click(function () {
        if ($("#cityName").val() != "")
        {
            $("#errorMsg").html("");

            var cityToSave = {
                RouteName: $("#cityName").val(),
                AreaId: $("#stateId").val(),
                Narration: $("#narration").val(),
                Extra1: $("#extra1").val(),
                Extra2: $("#extra2").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/City/AddCity",
                type: 'POST',
                data: JSON.stringify(cityToSave),
                contentType: "application/json",
                success: function (data) {
                    //hideLoader();
                    table.destroy();
                    getCities();
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else
        {
            $("#errorMsg").html("City name is required!");
        }
        
    });

    $("#delete").click(function () {
        var cityToDelete = {
            RouteId: cityIdToDelete
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/City/DeleteCity",
            type: 'POST',
            data:JSON.stringify(cityToDelete),
            contentType: "application/json",
            success: function (data) {
                //hideLoader();
                table.destroy();
                getCities();
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    });

    $("#saveChanges").click(function () {
        if ($("#editRouteName").val() != "") {
            $("#editErrorMsg").html("");

            var cityToEdit = {
                RouteId: $("#editCityId").val(),
                RouteName: $("#editCityName").val(),
                AreaId: $("#editStateId").val(),
                Narration: $("#editNarration").val(),
                //ExtraDate: $("#editDate").val(),
                Extra1: $("#editExtra1").val(),
                Extra2: $("#editExtra2").val()
            };
            console.log(cityToEdit);
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/City/EditCity",
                type: 'POST',
                data: JSON.stringify(cityToEdit),
                contentType: "application/json",
                success: function (data) {
                   // hideLoader();
                    table.destroy();
                    getCities();
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else {
            $("#editErrorMsg").html("City name is required!");
        }

    });

});

function getAccounts() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/AccountLedger/GetAccountLedgers",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            accounts = data;
            console.log(accounts);
            var objToShow = [];
            $.each(data, function (i, record) {
                var bal = 0;
                if (record.accountGroupName.trim() == "Bank" || record.accountGroupName.trim() == "Expenses" || record.accountGroupName.trim() == "Account Receivables"
                    || record.accountGroupName.includes("Assets") || record.accountGroupName.trim() == "Cash" || record.accountGroupName.trim() == "Cost Of Sales") {
                    bal = record.debit - record.credit;
                }
                else{
                   bal =  record.credit - record.debit;
                }
                //if (record.credit >= record.debit)
                //{
                //    bal = record.credit - record.debit;
                //}
                //else if (record.debit > record.credit)
                //{
                //    bal = record.debit - record.credit;
                //}
                objToShow.push([
                    (i + 1),
                    record.ledgerName,
                    record.accountGroupName,
                    record.extra1,
                    (bal.toFixed(2).indexOf('-') > -1) ? ('<span style="color:red;">' + Utilities.FormatCurrency(parseInt(bal.toFixed(2))) + '</span>') : Utilities.FormatCurrency(parseInt(bal.toFixed(2))),
                    '<form method="post" action="/Company/AccountLedger/Details" id="frm' + record.ledgerId + '"><button name="ledgerId" type="submit" class="btnDetails btn btn-info btn-sm" value="' + record.ledgerId + '"><i class="fa fa-info"></i> Details</button></form>',
                    '<form method="post" action="/Company/AccountLedger/Edit" id="frme' + record.ledgerId + '"><button name="ledgerId" type="submit" class="btnDetails btn btn-warning btn-sm" value="' + record.ledgerId + '"><i class="fa fa-info"></i> Edit</button></form>',
                    //'<button type="button" class="frmAccountLedger-Update hide btnEditModal btn btn-primary btn-sm" id="' + record.ledgerId + '"><i class="fa fa-edit"></i> Edit</button>',
                    '<button type="button" onclick="confirmDelete(' + record.ledgerId + ');" class="frmAccountLedger-Delete btnDeleteModal btn btn-danger btn-sm" id="' + record.ledgerId + '"><i class="fa fa-times-circle"></i> Delete</button>'
                ]);
            });

            var count = 0;
            if (checkPriviledge("frmAccountLedger", "View") === false) {
                objToShow.forEach(function (item) {
                    item.splice(5, 1);
                });
                count++;
            }
            if (checkPriviledge("frmAccountLedger", "Update") === false) {
                objToShow.forEach(function (item) {
                    var recalculateIndex = 6 - count;
                    item.splice(recalculateIndex, 1);
                });
                count++;
            }
            if (checkPriviledge("frmAccountLedger", "Delete") === false) {
                objToShow.forEach(function (item) {
                    var recalculateIndex = 7 - count;
                    item.splice(recalculateIndex, 1);
                });
                count++;
            }
            table = $('#accountTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true,
            });
            PriviledgeSettings.ApplyUserPriviledge();
            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}



$('#accountTable').on('draw.dt', function () {
    console.log('Event activate when user click the DT pagination');
    PriviledgeSettings.ApplyUserPriviledge();
});


function findState(id) {
    var state = "";
    for (i = 0; i < states.length; i++) {
        if (states[i].AreaId == id) {
            state = states[i];
            break;
        }
    }
    return state;
}

function findCity(id) {
    var city = "";
    for (i = 0; i < cities.length; i++) {
        if (cities[i][0] == id) {
            city = cities[i];            
            break;
        }
    }
    return city;
}


var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the possitive lable as key
    //...
};


function confirmDelete(item) {
    deleteIndex = item;
   
    eModal.confirm(options).then(deleteLedger);
}

function deleteLedger() {
    //Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AccountLedger/DeleteLedger?id=" + deleteIndex,

        type: "Get",
        contentType: "application/json",
        success: function (data) {
            // console.log(data);
            if (data.ResponseCode == 200) {
                Utilities.SuccessNotification("Ledger deleted successfully");
                $("#detailsModal").modal("hide");
                window.location = "/Company/AccountLedger/Index";
            }
            else if (data.ResponseCode == 403) {
                Utilities.ErrorNotification(data.ResponseMessage);
            }
            else {
                Utilities.ErrorNotification("Sorry, Someting went wrong");
            }

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry, could not load ledger details.");
            Utilities.Loader.Hide();
        }
    });
}

function addNewLedger() {
    ledgerList;
    var errorMessage = "";
    //ledgerList.forEach(function (item, count) {
    //    if (!Utilities.IsValueEmptyOrNull(item.AccountGroupId) || item.AccountGroupId == "0" || !Utilities.IsValueEmptyOrNull(item.LedgerName)) {
    //        errorMessage = "Ensure the Ledger Name and Account Group fields at row " + (count + 1) + " are filled correctly.";
    //        $("#errorMsg").html(errorMessage);
    //        $("#errorMsg").show();
    //        return false;
    //    }
    //})
    for (var i = 0; i < excelArrayData.length; i++) {
        excelArrayData[i].OpeningBalance = 0;
        excelArrayData[i].Cst = "";
        if (!Utilities.IsValueEmptyOrNull(excelArrayData[i].AccountGroupId) || excelArrayData[i].AccountGroupId == "0"
            || (!Utilities.IsValueEmptyOrNull(excelArrayData[i].LedgerName) && excelArrayData[i].AccountGroupId != "27")) {
            errorMessage = "Ensure the Ledger Name and Account Group fields at row " + (i + 1) + " are filled correctly.";
            //$("#errorMsg").html(errorMessage);
            Utilities.ErrorNotification(errorMessage);
            excelArrayData[i].BillByBill = true;
            return false;
        }
        if (excelArrayData[i].AccountGroupId != 26 || excelArrayData[i].AccountGroupId != 22) {
            excelArrayData[i].MailingName = null;
            excelArrayData[i].Address = null;
            excelArrayData[i].Phone = null;
            excelArrayData[i].Mobile = null;
            excelArrayData[i].Email = null;
            excelArrayData[i].BillByBill = false;
            excelArrayData[i].Tin = null;
            excelArrayData[i].Cst = null;
            excelArrayData[i].Pan = null;
            if (excelArrayData[i].AccountGroupId != 17 || excelArrayData[i].AccountGroupId != 28) {
                excelArrayData[i].BranchName = null;
                excelArrayData[i].BranchCode = null;
            }
            ledgerList.push({
                AccountLedgerInfo: excelArrayData[i],
                IsBankAccount:{},
                IsCashAccount:false,
                IsSundryDebtorOrCreditor: false
            })
        }
    }
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AccountLedger/CreateAccountLedger",
        type: 'POST',
        data: JSON.stringify(ledgerList),
        contentType: "application/json",
        success: function (data) {
            if (data.LedgerCreation == "SUCCESS") {

                Utilities.SuccessNotification("Account(s) created!");
                clearExcelArrayTable();
            }
            else if (data.LedgerCreation == "LEDGER_EXISTS") {
                Utilities.ErrorNotification(data.Message);
            }
            else {
                Utilities.ErrorNotification("Something went wrong!");
            }

            Utilities.Loader.Hide();
        },
        error: function (request, error) {
            Utilities.Loader.Hide();
        }
    });
}


function uploadModal() {
    excelArrayData = [];
    $("#uploadLedgerModal").modal("show");
}

function ExportToTable() {
    var regex = /^([a-zA-Z0-9()\s_\\.\-:])+(.xlsx|.xls)$/;
    /*Checks whether the file is a valid excel file*/
    if (regex.test($("#excelfile").val().toLowerCase())) {
        var xlsxflag = false; /*Flag for checking whether excel is .xls format or .xlsx format*/
        if ($("#excelfile").val().toLowerCase().indexOf(".xlsx") > 0) {
            xlsxflag = true;
        }
        /*Checks whether the browser supports HTML5*/
        if (typeof (FileReader) != "undefined") {
            var reader = new FileReader();
            reader.onload = function (e) {
                var data = e.target.result;
                /*Converts the excel data in to object*/
                if (xlsxflag) {
                    var workbook = XLSX.read(data, { type: 'binary' });
                }
                else {
                    var workbook = XLS.read(data, { type: 'binary' });
                }
                /*Gets all the sheetnames of excel in to a variable*/
                var sheet_name_list = workbook.SheetNames;

                var cnt = 0; /*This is used for restricting the script to consider only first sheet of excel*/
                sheet_name_list.forEach(function (y) { /*Iterate through all sheets*/
                    /*Convert the cell value to Json*/
                    if (xlsxflag) {
                        var exceljson = XLSX.utils.sheet_to_json(workbook.Sheets[y]);
                        console.log("exceljson:", exceljson);
                    }
                    else {
                        var exceljson = XLS.utils.sheet_to_row_object_array(workbook.Sheets[y]);
                        console.log("exceljson:", exceljson);
                    }

                    if (exceljson.length > 0 && cnt == 0) {
                        //BindTable(exceljson, '#exceltable');  
                        console.log(exceljson);
                        for (i = 0; i < exceljson.length; i++) {
                            console.log(exceljson[i].Email);
                            exceljson[i].LedgerName = exceljson[i]["Account Name"] == undefined ? "" : exceljson[i]["Account Name"];
                            exceljson[i].Extra1 = exceljson[i]["Account Code"] == undefined ? "" : exceljson[i]["Account Code"];
                            exceljson[i].BranchCode = exceljson[i]["Branch Code"] == undefined ? "" : exceljson[i]["Branch Code"];
                            exceljson[i].BranchName = exceljson[i]["Branch Name"] == undefined ? "" : exceljson[i]["Branch Name"];
                            exceljson[i].BankAccountNumber = exceljson[i]["Account No"] == undefined ? "" : exceljson[i]["Account No"];
                            exceljson[i].Phone = exceljson[i]["Phone No"] == undefined ? "" : exceljson[i]["Phone No"];
                            exceljson[i].Mobile = exceljson[i]["Mobile No"] == undefined ? "" : exceljson[i]["Mobile No"];
                            exceljson[i].Address = exceljson[i]["Address"] == undefined ? "" : exceljson[i]["Address"];
                            exceljson[i].Email = exceljson[i]["Email"] == undefined ? "" : exceljson[i]["Email"];
                            exceljson[i].CreditLimit = exceljson[i]["Credit Limit"] == undefined ? "" : exceljson[i]["Credit Limit"];
                            //exceljson[i].Cst = exceljson[i]["Position"] == undefined ? "" : exceljson[i]["Position"];
                            exceljson[i].Tin = exceljson[i]["Tin"] == undefined ? "" : exceljson[i]["Tin"];
                            exceljson[i].Pan = exceljson[i]["Pan"] == undefined ? "" : exceljson[i]["Pan"];
                            exceljson[i].CreditPeriod = exceljson[i]["Credit Period"] == undefined ? "" : exceljson[i]["Credit Period"];
                            exceljson[i].MailingName = exceljson[i]["Mailing Name"] == undefined ? "" : exceljson[i]["Mailing Name"];
                            exceljson[i].CompanyName = exceljson[i]["Company Name"] == undefined ? "" : exceljson[i]["Company Name"];
                           // exceljson[i].crOrDr = exceljson[i]["Cr/Dr"] == undefined ? "" : exceljson[i]["Cr/Dr"];
                            exceljson[i].Narration = exceljson[i]["Narration"] == undefined ? "" : exceljson[i]["Narration"];
                            exceljson[i]["State"] = exceljson[i]["State"] == undefined ? "" : exceljson[i]["State"];
                            exceljson[i]["City"] = exceljson[i]["City"] == undefined ? "" : exceljson[i]["City"];
                            exceljson[i]["Active"] = exceljson[i]["Active"] == undefined ? "" : exceljson[i]["Active"];
                            exceljson[i].IsActive = exceljson[i]["Active"].toUpperCase().trim() == "NO" || exceljson[i]["Active"].toUpperCase().trim() == "FALSE" ? false : true;
                            exceljson[i].PricingLevel = exceljson[i]["Pricing Level"] == undefined ? "" : exceljson[i]["Pricing Level"];
                            var state = states.find(p => p.AreaName.toUpperCase().trim() == exceljson[i]["State"].toUpperCase().trim());
                            exceljson[i].AreaId = state == undefined ? "" : state.AreaId;
                            exceljson[i]["State"] = exceljson[i].AreaId == "" ? "" : exceljson[i]["State"];
                            var city = cities.find(p => p.RouteName.toUpperCase().trim() == exceljson[i]["City"].toUpperCase().trim() && p.areaId == exceljson[i].AreaId);
                            exceljson[i].RouteId = city == undefined ? "" : city.RouteId;
                            exceljson[i]["City"] = exceljson[i].RouteId == "" ? "" : exceljson[i]["City"];
                            //var pricingLevel = pricingLevels.find(p => p.PricinglevelName.toUpperCase().trim() == exceljson[i]["Pricing Level"].toUpperCase().trim());
                            //exceljson[i].PricinglevelId = pricingLevel == undefined ? "" : pricingLevel.PricinglevelId;
                                                    
                            var accountGroup = groups.find(p => p.AccountGroupName.toUpperCase().trim() == exceljson[i]["Account Group"].toUpperCase().trim());
                            exceljson[i].AccountGroupId = accountGroup == undefined ? "" : accountGroup.AccountGroupId;
                            exceljson[i].AccountGroupName = accountGroup == undefined ? "" : accountGroup.AccountGroupName;
                            if (accountGroup != undefined) {
                                if (accountGroup.nature == "Assets" || accountGroup.nature == "Expenses") {
                                    exceljson[i].crOrDr = "Dr";
                                }
                                else if (accountGroup.nature ==  "Liabilities"  || accountGroup.nature == "Income") {
                                    exceljson[i].crOrDr = "Cr";
                                }
                                
                            }

                            excelArrayData.push(exceljson[i])
                            console.log(exceljson[i].Email);
                        }
                        cnt++;
                        renderExcelDataToTable();
                        console.log(excelArrayData);
                    }
                });
                //$('#exceltable').show();  
            }
            if (xlsxflag) {/*If excel file is .xlsx extension than creates a Array Buffer from excel*/
                reader.readAsArrayBuffer($("#excelfile")[0].files[0]);
            }
            else {
                reader.readAsBinaryString($("#excelfile")[0].files[0]);
            }
        }
        else {
            alert("Sorry! Your browser does not support HTML5!");
        }
    }
    else {
        alert("Please upload a valid Excel file!");
    }
}


function renderExcelDataToTable() {
    var objToShow = [];
    for (i = 0; i < excelArrayData.length; i++) {
        objToShow.push([
            (i + 1),
            excelArrayData[i].LedgerName,
            excelArrayData[i].AccountGroupName,
            excelArrayData[i].Narration,
            excelArrayData[i].BankAccountNumber,
            excelArrayData[i].CompanyName,
            excelArrayData[i].Email,
            excelArrayData[i].IsActive.toString(),
            excelArrayData[i].Extra1,
            excelArrayData[i].Address,
            excelArrayData[i].State,
            excelArrayData[i].Phone,
            excelArrayData[i].Mobile,
            excelArrayData[i].CreditLimit,
            excelArrayData[i].CreditPeriod,
            excelArrayData[i].PricingLevel,
            excelArrayData[i].Tin,
            excelArrayData[i].Pan,
            excelArrayData[i].City,
            excelArrayData[i].BranchCode,
            excelArrayData[i].BranchName,
            '<button class="btn btn-sm btn-danger" onclick="removeFromExcelArray(' + i + ')"><i class="fa fa-trash"></i></button>'
        ]);
    }
    if (tableToUpload != "") {
        tableToUpload.destroy();
    }
    tableToUpload = $('#ledgerListToUpload').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "responsive": true
    });
    // $("#customerName").val(""),
    //$("#email").val(""),
    //$("#address").val(""),
    //$("#phoneNumber").val("")
}

function removeFromExcelArray(index) {
    excelArrayData.splice(index, 1);
    renderExcelDataToTable();
}

function clearExcelArrayTable() {
    excelArrayData = [];
    renderExcelDataToTable();
}