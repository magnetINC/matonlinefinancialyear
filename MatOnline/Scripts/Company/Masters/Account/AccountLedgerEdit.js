﻿
$("#updateLedger").click(function () {

    var tbody = document.getElementById("ledgerPostingItemDetails");
    var debit = 0; 
    var credit = 0;
    var tableCrOrDr = "";
    
    tbody.childNodes.forEach(function (item){ if(item.childNodes[1] !== undefined){
        debit+=parseFloat(item.childNodes[7].innerText);
        credit+=parseFloat(item.childNodes[9].innerText);
    }
    });
    if(credit > debit){
        tableCrOrDr = "Cr";
    }
    else{
        tableCrOrDr = "Dr";
    }
    var ledgerItems = {
        LedgerId: $("#ledgerIdToViewDetails").val(),
        AccountGroupId: $("#groupSelect").val(),
        LedgerName: $("#ledgerName").val(),
        //OpeningBalance: $("#openingBalance").val(),
        IsDefault: $("#idDefaultToViewDetails").val(),
        CrOrDr: tableCrOrDr,
        Narration: $("#narration").val(),
        MailingName: $("#companyName").val(),
        Address: $("#address").val(),
        //State: $("#state").val(),
        Phone: $("#phone").val(),
        Mobile: $("#mobileNo").val(),
        Email: $("#email").val(),
        CreditPeriod: $("#creditPeriod").val(),
        CreditLimit: $("#creditLimit").val(),
        PricingLevelId: $("#pricingLevel").val(),
        BillByBill: $("#billByBillSelect").val(),
        Tin: $("#tin").val(),
        Cst: $("#position").val(),
        Pan: $("#pan").val(),
        RouteId: $("#routeIdToViewDetails").val(),
        BankAccountNumber: $("#accountNumber").val(),
        BranchName: $("#branchName").val(),
        BranchCode: $("#branchCode").val(),
        ExtraDate: $("#toDate").val(),
        Extra1: $("#accountCode").val(),
        Extra2: $("#extra2ToViewDetails").val(),
        AreaId: $("#city").val(),
        IsActive: $("#isActiveToViewDetails").val(),
        

          // AccountCode: $("#accountCode").val(),      
          // FrmDate: $("#frmDate").val(), ToDate: $("#toDate").val(),
           //Position: $("#position").val()       
          
          
            };
        //var journalMasterId = document.getElementById("journalMasterId").value;
        var toSave = {
            AccountLedgerInfo: ledgerItems
        };
        console.log(toSave); 
        //Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/AccountLedger/UpdateLedgerDetails",
            type: "Post",
            data: JSON.stringify(toSave),
            contentType: "application/json",
            success: function (data) {
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Ledger Details Updated!");
                   // $("#detailsModal").modal("hide");
                    window.location = "/Company/AccountLedger/Index";
                }
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
});