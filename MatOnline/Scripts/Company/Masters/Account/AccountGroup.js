﻿var accountGroups = [];
var cities = [];
var cityToEdit = "";
var cityIdToDelete = 0;
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created


$(function () {

    getAccountGroups();

    $("#accountGroupTable").on("click", ".btnViewModal", function () {
        var id = $(this).attr("id");
        getAccountGroupDetails(id);
        $("#editAccountGroupName").attr("disabled", "disabled");
        $("#editUnderGroupId").attr("disabled", "disabled");
        $("#editNarration").attr("disabled", "disabled");
        $("#editNature").attr("disabled", "disabled");
        $("#editAffectGrossProfit").attr("disabled", "disabled");
        $("#saveChanges").hide();
        $("#editModalLabel").html("Account Group Details");
    });

    $("#accountGroupTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        $("#editAccountGroupId").val(id);
        getAccountGroupDetails(id);
    });

    $("#accountGroupTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        deleteAccountGroup(id);
    });

    $("#saveAccountGroup").click(function () {
        if ($("#accountGroupName").val() != "")
        {
            $("#errorMsg").html("");

            var accountGroupToSave = {
                AccountGroupName: $("#accountGroupName").val(),
                GroupUnder: $("#underGroupId").val(),
                Narration: $("#narration").val(),
                Nature: $("#nature").val(),
                IsDefault: false,
                AffectGrossProfit: $("#editAffectGrossProfit").val()
                        };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/AccountGroup/AddAccountGroup",
                type: 'POST',
                data: JSON.stringify(accountGroupToSave),
                contentType: "application/json",
                success: function (data) {
                    //hideLoader();
                    table.destroy();
                    if (data == true)
                    {
                        $("#accountGroupName").val("");
                    }
                    getAccountGroups();
                    $('#addModal').modal('toggle');
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else
        {
            $("#errorMsg").html("Account group name is required!");
        }
        
    });
       
    $("#saveChanges").click(function () {
        if ($("#ediAccountGroupName").val() != "") {
            $("#editErrorMsg").html("");

            var accountGroupToEdit = {
                AccountGroupId:$("#editAccountGroupId").val(),
                AccountGroupName: $("#editAccountGroupName").val(),
                GroupUnder: $("#editUnderGroupId").val(),
                Narration: $("#editNarration").val(),
                Nature: $("#editNature").val(),
                IsDefault: false,
                AffectGrossProfit: $("#editAffectGrossProfit").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/AccountGroup/EditAccountGroup",
                type: 'POST',
                data: JSON.stringify(accountGroupToEdit),
                contentType: "application/json",
                success: function (data) {
                    //hideLoader();
                    table.destroy();
                    if (data == true) {
                        $("#editAccountGroupName").val("");
                    }
                    getAccountGroups();
                    $('#editModal').modal('toggle');
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else {
            $("#editErrorMsg").html("Account group name is required!");
        }
    });

    $("#editModal").on("hide.bs.modal", function () {
        $("#editAccountGroupName").removeAttr("disabled");
        $("#editUnderGroupId").removeAttr("disabled");
        $("#editNarration").removeAttr("disabled");
        $("#editNature").removeAttr("disabled");
        $("#editAffectGrossProfit").removeAttr("disabled");
        $("#saveChanges").show();
        $("#editModalLabel").html("Edit Account Group");
    });
});

function getAccountGroups() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/AccountGroup/GetAccountGroups",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors.
            setTimeout(onSuccess, 500, data);

            
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function onSuccess(data) {
    accountGroups = data;
    //console.log(accountGroups);
    var underGroupHtml = "";
    var objToShow = [];
    $.each(data, function (i, record) {
        objToShow.push([
            (i + 1),
            record.AccountGroupName,
            record.Under,
            //'<button type="button" class="frmAccountGroup-View btnViewModal btn btn-info text-left" id="' + record.AccountGroupId + '"><i class="fa fa-info"></i> View</button>',
            //'<button type="button" class="frmAccountGroup-Update btnEditModal btn btn-primary text-left" id="' + record.AccountGroupId + '"><i class="fa fa-edit"></i> Edit</button>',
            //'<button type="button" class="frmAccountGroup-Delete btnDeleteModal btn btn-danger text-left" id="' + record.AccountGroupId + '"><i class="fa fa-times-circle"></i> Delete</button>'
        ]);

        //populate "under" group dropdown
        underGroupHtml += '<option value="' + record.AccountGroupId + '">' + record.AccountGroupName + '</option>';
    });
    $("#underGroupId").html(underGroupHtml);
    $("#editUnderGroupId").html(underGroupHtml);
    var count = 0;
    if (checkPriviledge("frmAccountGroup", "View") === false) {
        objToShow.forEach(function (item) {
            item.splice(3, 1);
        });
        count++;
    }
    if (checkPriviledge("frmAccountGroup", "Update") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 4 - count;
            item.splice(recalculateIndex, 1);
        });
        count++;
    }
    if (checkPriviledge("frmAccountGroup", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 5 - count;
            item.splice(recalculateIndex, 1);
        });
        count++;
    }
    table = $('#accountGroupTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    hideLoader();
}

$('#accountGroupTable').on('draw.dt', function () {
    //console.log('Event activate when user click the DT pagination');
    PriviledgeSettings.ApplyUserPriviledge();
});

function getAccountGroupDetails(acctGrpId)
{
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/AccountGroup/GetAccountGroup?accountGroupId="+acctGrpId,
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            $("#editAccountGroupName").val(data.AccountGroupName);
            $("#editUnderGroupId").val(data.GroupUnder);
            $("#editNarration").val(data.Narration);
            $("#editNature").val(data.Nature);
            data.AffectGrossProfit = $("#editAffectGrossProfit").val();
            //$("#editAffectGrossProfit").val(data.AffectGrossProfit);

            hideLoader();
            $('#editModal').modal('toggle');
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}


function deleteAccountGroup(groupId)
{
    var conf = confirm("Delete this record?");
    if (conf)
    {
        var groupToDelete = {
            AccountGroupId: groupId
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/AccountGroup/DeleteAccountGroup",
            type: 'POST',
            data: JSON.stringify(groupToDelete),
            contentType: "application/json",
            success: function (data) {

                if (data == true)  //Group has no transaction referenced and can be deleted
                {
                    Utilities.SuccessNotification("Group deleted!");
                    table.destroy();
                    getAccountGroups();
                    hideLoader();
                }
                else {

                    Utilities.ErrorNotification("Transaction reference exist for this Group.");
                    hideLoader();          
                }

            },
            error: function (request, error) {
                hideLoader();
            }
        });
    }    
}