﻿var creditBalance = 0.0;
var debitBalance = 0.0;
var balance = 0.0;
var ledgerDetailsObj = {};
var formTypes = ["Bank Transfers","Payment Form","Receipt Form","General Journals","PDC Clearance","PDC Payable","PDC Receivable","Purchase Invoice","Purchase Return","Sales Invoice","Sales Return","Sales Invoice","Sales Return","Service Voucher","Credit Note","Debit Note","Advance Payment","Monthly Salary Form"];
var stores = [];
var units = [];
var tableToUpload = "";

$(function () {
    getAccounts();
    getLookUps();
});

function getAccounts() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/AccountLedger/GetLedgerDetails?ledgerId=" + ledgerIdToViewDetails,
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            ledgerDetailsObj = data;
            var ledgerDetailsInfo = data.AccountLedgerInfo;
            $("#ledgerIdToViewDetails").val(ledgerDetailsInfo.LedgerId);
            $("#accountGroupIdToViewDetails").val(ledgerDetailsInfo.AccountGroupId);
            $("#idDefaultToViewDetails").val(ledgerDetailsInfo.IsDefault);
            $("#crOrDrViewDetails").val(ledgerDetailsInfo.CrOrDr);
            $("#cstViewDetails").val(ledgerDetailsInfo.Cst);
            $("#routeIdToViewDetails").val(ledgerDetailsInfo.RouteId);              
            $("#extra1ToViewDetails").val(ledgerDetailsInfo.Extra1);                  
            $("#extra2ToViewDetails").val(ledgerDetailsInfo.Extra2);                                  
            $("#areaIdToViewDetails").val(ledgerDetailsInfo.AreaId);                                  
            $("#isActiveToViewDetails").val(ledgerDetailsInfo.IsActive);                                              
            $("#accountGroupAndLedger, #thisAccountName").html(ledgerDetailsObj.AccountGroup.AccountGroupName + " >> " + ledgerDetailsObj.AccountLedgerInfo.LedgerName);
            $("#accountGroup").html(ledgerDetailsObj.AccountGroup.AccountGroupName);

            populateDetails();
            populateTransactions();
            populateSelect(data.AccountGroupsList, data.AccountLedgerInfo);
            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}
console.log('thisGroup:', $("#accountGroup").html());
function getLookUps() {
    var storesLookupAjax = $.ajax({
        url: API_BASE_URL + "/Store/GetStores",
        type: "GET",
        contentType: "application/json",
    });

    var unitLookupAjax = $.ajax({
        url: API_BASE_URL + "/Unit/GetUnits",
        type: "GET",
        contentType: "application/json",
    });

    $.when(storesLookupAjax, unitLookupAjax)
    .done(function (dataStores, dataUnits) {
        stores = dataStores;
        units = dataUnits[0];
        console.log("units:", dataUnits);
    });
}

function formatJsonDate(jsonDate)
{
    var datePt = jsonDate.split("T");
    var dt = datePt[0].split("-");
    return dt[2]+"-"+monthString(dt[1])+"-"+dt[0]
}

function monthString(num)
{
    if (num == "01" || num== "1")
        return "Jan";
    if (num == "02" || num == "2")
        return "Feb";
    if (num == "03" || num == "3")
        return "Mar";
    if (num == "04" || num == "4")
        return "Apr";
    if (num == "05" || num == "5")
        return "May";
    if (num == "06" || num == "6")
        return "Jun";
    if (num == "07" || num == "7")
        return "Jul";
    if (num == "08" || num == "8")
        return "Aug";
    if (num == "09" || num == "9")
        return "Sep";
    if (num == "10")
        return "Oct";
    if (num == "11")
        return "Nov";
    if (num == "12")
        return "Dec";
}

function populateDetails()
{
    var info = ledgerDetailsObj;
    var ledgerInfo = info.AccountLedgerInfo;
    //main details
    $("#accountCode").val(ledgerInfo.Extra1);
    $("#ledgerName").val(ledgerInfo.LedgerName);
    $("#accountGroup").val(info.AccountGroup.AccountGroupName);
    $("#narration").val(ledgerInfo.Narration);
    $("#branchName").val(ledgerInfo.BranchName);
    $("#branchCode").val(ledgerInfo.BranchCode);
    $("#accountNumber").val(ledgerInfo.BankAccountNumber);
    $("#openingBalance").val(ledgerInfo.OpeningBalance);
    $("#frmDate").val(formatJsonDate(info.FromDate));
    $("#toDate").val(formatJsonDate(info.ToDate));

    //secondary details
    $("#companyName").val(ledgerInfo.MailingName);
    $("#accountNumber").val(ledgerInfo.BankAccountNumber);
    $("#mobileNo").val(ledgerInfo.Mobile);
    $("#phone").val(ledgerInfo.Phone);
    $("#email").val(ledgerInfo.Email);
    $("#position").val(ledgerInfo.Cst);
    $("#pricingLevel").val(ledgerInfo.PricinglevelId);
    $("#billByBill").val(ledgerInfo.BillByBill);
    $("#creditLimit").val(ledgerInfo.CreditLimit);
    $("#creditPeriod").val(ledgerInfo.CreditPeriod);
    $("#tin").val(ledgerInfo.Tin);
    $("#pan").val(ledgerInfo.Pan);
    $("#state").val(ledgerInfo.State);
    $("#city").val(ledgerInfo.AreaId);
    $("#address").val(ledgerInfo.Address);

    
    $("#billByBillSelect").val("" + ledgerInfo.BillByBill + "");
    //$("#accountGroupSelect").kendoDropDownList({
    //    filter: "contains",
    //    optionLabel: "Please Select...",
    //    dataTextField: "ledgerName",
    //    dataValueField: "ledgerId",
    //    dataSource: accountLedgers
    //});
}

function populateTransactions()
{
    console.log('ths grp id:', ledgerDetailsObj.AccountGroup.AccountGroupId);
    var gId = ledgerDetailsObj.AccountGroup.AccountGroupId;
    var output = "";
    var objToShow = [];
    var summary = 0;
    if (ledgerDetailsObj.CustomerAccountDetails.Table.length > 0)  //opening balance row
    {
        $.each(ledgerDetailsObj.CustomerAccountDetails.Table, function (i, record) {
            if (gId == 22 || gId == 3 || gId == 20 || gId == 36 || gId == 1 || gId == 10 || gId == 14) {
                summary += parseInt(record.OpeningCredit) - parseInt(record.OpeningDebit);
            }
            else {
                summary += parseInt(record.OpeningDebit) - parseInt(record.OpeningCredit);
            }
            objToShow.push([
                formatJsonDate(ledgerDetailsObj.FromDate),
                "Opening Balance",
                " - ",
                " - ",
                Utilities.FormatCurrency(parseInt(record.OpeningDebit)),
                Utilities.FormatCurrency(parseInt(record.OpeningCredit)),
                Utilities.FormatCurrency(summary),
                '<button class="btn btn-primary btn-labeled fa fa-info noExl" disabled="disabled">Details</button>'
            ]);
            creditBalance += parseFloat(record.OpeningCredit);
            debitBalance += parseFloat(record.OpeningDebit);
        });
    }
    if (ledgerDetailsObj.CustomerAccountDetails.Table1.length > 0) {
        $.each(ledgerDetailsObj.CustomerAccountDetails.Table1, function (i, record) {
            if (gId == 22 || gId == 3 || gId == 20 || gId == 36 || gId == 1 || gId == 10 || gId == 14) {
                summary += parseInt(record.credit) - parseInt(record.debit);
            }
            else {
                summary += parseInt(record.debit) - parseInt(record.credit);
            }
            var masterId = ledgerDetailsObj.AccountLedgerInfo.LedgerName === "Opening Balance Equity" ? record.invoiceNo : record.masterId;
            objToShow.push([
                formatJsonDate(record.date),
                record.voucherTypeName,
                record.invoiceNo,
                record.Memo,
                Utilities.FormatCurrency(parseInt(record.debit)),
                Utilities.FormatCurrency(parseInt(record.credit)),
                Utilities.FormatCurrency(summary),
                '<button type="button" class="btn btn-primary btn-labeled fa fa-info noExl" ' + disableDetailsButton(record.voucherTypeName) + ' onclick = "showTransactionDetails(\'' + record.ledgerId + '\',\'' + record.voucherTypeName + '\',\'' + masterId + '\')" > Details</button >'
            ]);
            /*output += '<tr>\
                                <td>' + formatJsonDate(record.date) + '</td>\
                                <td>' + record.voucherTypeName + '</td>\
                                <td>' + record.invoiceNo + '</td>\
                                <td>'+ record.debit + '</td>\
                                <td>'+ record.credit + '</td>\
                                <td>' + record.Memo + '</td>\
                                <td>\
                                   <!--<form id="frm' + record.ledgerId + '" action="/Banking/BankTransfer/EditBankTransfer" method="post">-->\
                                    <form id="frm' + record.ledgerId + '" action="#" method="post">\
                                        <input type="hidden" name="LedgerId" value="' + record.ledgerId + '"/>\
                                        <input type="hidden" name="MasterId" value="' + record.masterId + '"/>\
                                        <button type="button" class="btn btn-primary btn-labeled fa fa-info" ' + disableDetailsButton(record.voucherTypeName) + ' onclick="showTransactionDetails(\'' + record.ledgerId + '\',\'' + record.voucherTypeName + '\',\'' + masterId + '\')">Details</button>\
                                    </form>\
                                </td>\
                            <tr>\
                           ';*/
            creditBalance += parseFloat(record.credit);
            debitBalance += parseFloat(record.debit);
        });
    }
    //if (ledgerDetailsObj.AccountLedgerInfo.CrOrDr == "Dr") {
    //if (debitBalance > creditBalance) {
    //    balance = creditBalance - debitBalance;
    //}
    //else {
    //    balance = creditBalance - debitBalance;
    //}

    var accountGroupName = ledgerDetailsObj.AccountGroup.AccountGroupName;
    if (accountGroupName.trim() == "Bank" || accountGroupName.trim() == "Expenses" || accountGroupName.trim() == "Account Receivables"
        || accountGroupName.includes("Assets") || accountGroupName.trim() == "Cash" || accountGroupName.trim() == "Cost Of Sales") {
        balance =  debitBalance - creditBalance;
    }
    else {
        balance = creditBalance - debitBalance;
    }
   // balance = creditBalance - debitBalance;

    //$("#ledgerPostingItemDetails").html(output);

    if (tableToUpload != "") {
        tableToUpload.destroy();
    }
    tableToUpload = $('#ledgerPosting').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true,
        "responsive": true,
    });



    if (balance.toFixed(2).indexOf('-') > -1)   //if balance is negative, set amount color to red
    {
        $("#transactionSummary").css("color", "red");
        $("#transactionSummary").html("₦" + Utilities.FormatCurrency(balance)); 
    }
    else {
        $("#transactionSummary").css("color", "black");
        $("#transactionSummary").html("₦" + Utilities.FormatCurrency(balance));
    }
}


function populateSelect(accountGroupList, accountLedgerInfo) {
    if (document.getElementById("groupSelect") != undefined) {
        var selectElem = "<option value='" + accountLedgerInfo.AccountGroupId + "'> " + accountGroupList.find(p=>p.accountGroupId == parseInt(accountLedgerInfo.AccountGroupId)).accountGroupName + " </option>";
        accountGroupList.forEach(function (item) {
            if (item.accountGroupId != 23 /*&& item.accountGroupId != 11*/) {
                selectElem += "<option value='" + item.accountGroupId + "'> " + item.accountGroupName + " </option>";
            }
        });
        var selectTag = document.getElementById("groupSelect");
        selectTag.innerHTML = selectElem;
        console.log(selectElem);
    }
}
function disableDetailsButton(formType)
{
    //if(formTypes.indexOf(formType)>-1)
    //{
    //    return '';
    //}
    //else
    //{
    //    return 'disabled="diabled"';
    //}
}

function showTransactionDetails(ledgerId, formType, decMasterId)
{
    console.log("ledger id : ", ledgerId);
    console.log("form type : ", formType);
    debugger;
    switch(formType)
    {
        //case "Bank Transfers":
        //    var param = {
        //        ledgerId: ledgerId,
        //        decMasterId: decMasterId
        //    };
        //    getBankTransfersDetails(param);
        //    //$("#bankTransferModal").modal("toggle");
        //    break;
        case "Payment Form":
            $("#bomModal").modal("toggle");
            break;

        case "General Journals":
            GNJviewDetails(decMasterId);
            break;
        case "Purchase inv_1":
            getPurchaseInvoiceDetails(decMasterId);
            break;
        case "Sales inv_1":
            getSalesInvoiceDetails(decMasterId, ledgerId);
            break;
        case "Opening Stock":
            getOpeningStockDetails(decMasterId);
            break;
        case "Bank Transfers":
            getBankTransferDetails(decMasterId, ledgerId);
            break;
        case "Receipt Voucher":
            getReceiptVoucherDetails(decMasterId);
            break;
        case "Payment Voucher":
            getPaymentVoucherDetails(decMasterId);
            break;
    }
}

function getBankTransfersDetails(param)
{
    $("#frm" + param.ledgerId).submit({param});
    //Utilities.Loader.Show();
    //$.ajax({
    //    url: API_BASE_URL + "/AccountLedgerTransactionDetails/GetBankTransferDetails",
    //    type: "POST",
    //    data:JSON.stringify(param),
    //    contentType: "application/json",
    //    success:function(data)
    //    {
    //        console.log(data);
    //        $("#").val();
    //        $("#").val();
    //        $("#").val();
    //        $("#").val();
    //        $("#").val();
    //        $("#").val();
    //        $("#").val();
    //        $("#").val();
    //        $("#").val();
    //        Utilities.Loader.Hide();
    //    },
    //    error:function(err)
    //    {
    //        Utilities.Alert("Sorry, details could not be loaded.\nYou may want to check your internet connection.");
    //        Utilities.Loader.Hide();
    //    }
    //});
}


function GNJviewDetails(id) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/RunGeneralJournal/ViewJournal?journalMasterId=" + id,
        type: "Get",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var details = data.Details;
            $("#journalNoDiv").html(data.JournalNo);
            $("#journalDateDiv").html(data.JournalDate);
            $("#createdByDiv").html(data.UserFullName);
            $("#debitAmtTxt").val(Utilities.FormatCurrency(data.TotalAmount));
            $("#creditAmtTxt").val(Utilities.FormatCurrency(data.TotalAmount));
            var output = "";
            for (i = 0; i < details.length; i++) {
                output += '<tr>' +
                               '<td>' + (i + 1) + '</td>' +
                               '<td>' + details[i].LedgerName + '</td>' +
                               '<td>' + Utilities.FormatCurrency(details[i].Debit) + '</td>' +
                               '<td>' + Utilities.FormatCurrency(details[i].Credit) + '</td>' +
                               '<td>' + details[i].Memo + '</td>' +
                               '<td>' +
                                    '<button class="btn btn-primary btn-sm"><i class="fa fa-edit"></i></button> ' +
                                    '<button class="btn btn-danger btn-sm"><i class="fa fa-times"></i></button>' +
                               '</td>' +
                          '</tr>';
            }
            $("#GeneralJournalDetailsModalTbody").html(output);
            $("#GeneralJournalDetailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.ErrorNotification("Sorry, could not load journal details.");
            Utilities.Loader.Hide();
        }
    });
}

function getPurchaseInvoiceDetails(id) {
    Utilities.Loader.Show();
    var master = ledgerDetailsObj.CustomerAccountDetails.Table1;
    for (i = 0; i < master.length; i++) {
        if (master[i].masterId == id) {
           var thisInvoiceMaster = master[i];
            break;
        }
    }
    $.ajax({
        url: API_BASE_URL + "/PurchaseInvoice/RegisterDetails?id=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            details = data;
            var output = "";
            var totalTax = 0;
            var netAmount = 0;
            var totalAmount = 0;
            for (i = 0; i < details.length; i++) {
                //if (details[i].barcode)
                //{
                //    details[i].barcode = "";
                //}
                var grossValue = details[i].rate * details[i].qty;
                var netValue = grossValue - details[i].discount;
                var totAmount = netValue + details[i].taxAmount
                output += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td><center>' + ((details[i].barcode == undefined) ? "-" : (details[i].barcode)) + '</center></td>\
                            <td>' + details[i].productCode + '</td>\
                            <td>' + details[i].productName + '</td>\
                            <td>' + details[i].itemDescription + '</td>\
                            <td>' + details[i].qty + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].rate) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(grossValue) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].discount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(netValue) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].taxAmount) + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(totAmount) + '</td>\
                        </tr>';
                totalTax = totalTax + details[i].taxAmount;
                netAmount = netAmount + netValue
            }
            totalAmount = totalTax + netAmount;
            var purchaseInvoiceDate = (new Date(thisInvoiceMaster.date)).toDateString();
            $("#purchaseInvoiceNoDiv").html(thisInvoiceMaster.invoiceNo);
            $("#purchaseInvoiceDateDiv").html(purchaseInvoiceDate);
           // $("#purchaseInvoiceOrderNoDiv").html(thisInvoiceMaster.no);
           // $("#purchaseInvoiceRaisedByDiv").html(thisInvoiceMaster.userName);
            $("#purchaseInvoiceRaisedForDiv").html(thisInvoiceMaster.ledgerName);
            //$("#statusDiv").html(statusOutput);
            $("#purchaseInvoiceTotalAmount").val(Utilities.FormatCurrency(netAmount));
            $("#purchaseInvoiceTotalTaxAmount").val(Utilities.FormatCurrency(totalTax));
            $("#purchaseInvoiceGrandTotal").val(Utilities.FormatCurrency(totalAmount));

            $("#purchaseInvoiceDetailsTbody").html(output);
            $("#purchaseInvoiceDetailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function getSalesInvoiceDetails(saleMasterId, ledgerId) {
    var url = API_BASE_URL +
        "/SalesInvoice/GetSalesOrderAndDeliverFromMaster/?saleMasterId=" +
        saleMasterId;
    //var url = API_BASE_URL +
    //    "/SalesInvoice/GetInvoiceDetailsFromLegderPosting/?salesMasterId=" +
    //    saleMasterId + "&ledgerId=" + ledgerId;

    $.ajax(url,
        {
            type: "get",
            beforeSend: () => { Utilities.Loader.Show() },
            complete: () => { Utilities.Loader.Hide() },
            success: function (data) {
                var obj = [];
                var count = 0;
                var html = "";
                $("#salesInvoiceDetailsModal").modal();
                for (var i of data) {
                    console.log(data);
                //invoiceListTable
                    count++;
                    for (var p of i.SaleDetail) {
                        var warehouseObj = stores.find(s => s.GodownId == parseInt(p.godownId));
                        var warehouse = warehouseObj == undefined? "NA": warehouseObj.GodownName
                        var unitObj = units.find(p => p.unitId == parseInt(p.unitId));
                        var unit = unitObj == undefined ? "NA" : unitObj.unitName;
                        html += `
                                   <tr>
                                        <td >${count}</td>
                                        <td >${p.tbl_Product.productCode}</td>
                                        <td >${p.tbl_Product.productCode}</td>
                                        <td >${p.tbl_Product.productName}</td>
                                        <td >${warehouse}</td>
                                        <td>${p.qty}</td>
                                        <td>${unit}</td>
                                        <td>${p.tbl_Product.purchaseRate}</td>
                                        <td>${p.rate}</td>
                                        <td>${p.amount}</td>
                                        <td>${p.taxAmount}</td>

                            </tr>
                                `;

                    }

                    $("#salesInvoiceNoDiv").text(i.SaleMaster.invoiceNo);
                    $("#salesInvoiceDateDiv").text(Utilities.FormatJsonDate(i.SaleMaster.date));
                    $("#salesInvoiceCustomerDiv").text(i.Ledger.ledgerName);
                    $("#salesInvoiceTotalAmount").val(i.SaleMaster.totalAmount - i.SaleMaster.taxAmount);
                    $("#salesInvoiceTaxAmountTxt").val(Utilities.FormatCurrency(i.SaleMaster.taxAmount));
                    $("#salesInvoiceGrandTotalTxt").val(Utilities.FormatCurrency(i.SaleMaster.totalAmount));
                }
                $("#salesInvoiceDetailsTbody").html(html);
            }
        });
}

function getOpeningStockDetails(productId) {
   // var url = API_BASE_URL + "/ProductCreation/ProductCreation/?productId=" + productId;
    $.ajax({
        url: API_BASE_URL + "/InventoryReport/GetOpeningStockForProduct?productId=" + productId,
        type: "GET",
        contentType: "application/json",
        success: function (details) {
            console.log(details);
            var output = ""
            var totalAmount = 0;
            for (i = 0; i < details.length; i++) {
               
                totalAmount += (details[i].InwardQty * details[i].Rate);
                output += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td>' + ((details[i].Warehouse == undefined) ? "-" : (details[i].Warehouse)) + '</td>\
                            <td>' + details[i].BatchNo + '</td>\
                            <td>' + details[i].UnitName + '</td>\
                            <td>' + details[i].InwardQty + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].Rate) + '</td>\
                        </tr>';
               // totalTax = totalTax + details[i].taxAmount;
               // netAmount = netAmount + netValue
            }
            // totalAmount = totalTax + netAmount;

            $("#openingStockTotalAmount").val(Utilities.FormatCurrency(totalAmount));
            var openingStockDate = (new Date(details[0].Date)).toDateString();
            $("#openingStockProductDiv").html(details[0].ProductName);
            $("#openingStockDateDiv").html(openingStockDate);
            $("#openingStockDetailsModalTbody").html(output);
            $("#openingStockDetailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });

}


function getBankTransferDetails(productId, ledgerId) {
    var bankTransferParam = {};
    bankTransferParam.decMasterId = productId,
    bankTransferParam.ledgerId =
    $.ajax({
        url: API_BASE_URL + "/AccountLedgerTransactionDetails/GetBankTransferDetails",
        type: "POST",
        data: JSON.stringify(bankTransferParam),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var master = data.infoContraMaster;
            var details = data.dtbl;
            var output = ""
            var totalAmount = 0;
            for (i = 0; i < details.length; i++) {

                totalAmount += details[i].amount;
                output += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td>' + ((details[i].ledgerName == undefined) ? "-" : (details[i].ledgerName)) + '</td>\
                            <td>' + details[i].chequeNo + '</td>\
                            <td>' + details[i].chequeDate + '</td>\
                            <td>' + details[i].Memo + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].amount) + '</td>\
                        </tr>';
            }

            var bankTranferDate = (new Date(master.Date)).toDateString();

            $("#bankTransferDateDiv").html(bankTranferDate);
            $("#bankTransferTotalAmount").val(Utilities.FormatCurrency(totalAmount));
            $("#bankTransferLedgerNameDiv").html(master.LedgerName);
            $("#bankTransferUserDiv").html(master.UserName);
            $("#bankTransferTypeDiv").html(master.Type);
            $("#bankTransferDetailsModalTbody").html(output);
            $("#bankTransferDetailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });

}



function getReceiptVoucherDetails(Id) {
   
    $.ajax({
        url: API_BASE_URL + "/Receipt/getMasterandDetailsForEdit?id=" + Id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            var master = data.master;
            var details = data.details;
            var output = ""
            var totalAmount = 0;
            for (i = 0; i < details.length; i++) {

                totalAmount += details[i].amount;
                var detailsLedgerName = details[i].ledgerName == undefined || details[i].ledgerName == "" ? "-" : details[i].ledgerName;
                var detailsChequeNo = details[i].chequeNo == undefined || details[i].chequeNo == "" ? "N/A" : details[i].chequeNo;
                var detailsChequeDate = new Date(details[i].chequeDate).toLocaleDateString();
                output += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td>' + detailsLedgerName + '</td>\
                            <td>' + detailsChequeNo + '</td>\
                            <td>' + detailsChequeDate + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].amount) + '</td>\
                        </tr>';
            }

            var receiptDate = (new Date(master.Date)).toDateString();

            $("#receiptDateDiv").html(receiptDate);
            $("#receiptTotalAmount").val(Utilities.FormatCurrency(totalAmount));
            $("#receiptLedgerNameDiv").html(master.LedgerName);
            $("#receiptVoucherNoDiv").html(master.VoucherNo);
            $("#receiptUserDiv").html(master.UserName);
            $("#receiptDetailsModalTbody").html(output);
            $("#receiptDetailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });

}



function getPaymentVoucherDetails(Id) {

    $.ajax({
        url: API_BASE_URL + "/PaymentVoucher/getMasterandDetails?id=" + Id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            debugger;
            console.log(data);
            var master = data.master;
            var details = data.details;
            var output = ""
            var totalAmount = 0;
            for (i = 0; i < details.length; i++) {

                totalAmount += details[i].amount;
                var detailsLedgerName = details[i].ledgerName == undefined || details[i].ledgerName == "" ? "-" : details[i].ledgerName;
                var detailsChequeNo = details[i].chequeNo == undefined || details[i].chequeNo == "" ? "N/A" : details[i].chequeNo;
                var detailsChequeDate = new Date(details[i].chequeDate).toLocaleDateString();
                output += '<tr>\
                            <td>'+ (i + 1) + '</td>\
                            <td>' + detailsLedgerName + '</td>\
                            <td>' + detailsChequeNo + '</td>\
                            <td>' + detailsChequeDate + '</td>\
                            <td>&#8358;' + Utilities.FormatCurrency(details[i].amount) + '</td>\
                        </tr>';
            }

            var paymentVoucherDate = (new Date(master.Date)).toDateString();

            $("#paymentVoucherDateDiv").html(paymentVoucherDate);
            $("#paymentVoucherTotalAmount").val(Utilities.FormatCurrency(totalAmount));
            $("#paymentVoucherLedgerNameDiv").html(master.LedgerName);
            $("#paymentVoucherNoDiv").html(master.VoucherNo);
            $("#paymentVoucherUserDiv").html(master.UserName);
            $("#paymentVoucherDetailsModalTbody").html(output);
            $("#paymentVoucherDetailsModal").modal("show");
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });

}