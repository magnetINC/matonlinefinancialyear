﻿var states = [];
var stateToEdit = "";
var stateIdToDelete = 0;
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created


$(function () {

    getStates();

    $("#stateTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        stateToEdit = findState(id);
        $("#editStateId").val(stateToEdit[0]);
        $("#editStateName").val(stateToEdit[1]);
        $("#editNarration").val(stateToEdit[2]);
        $("#editDate").val(stateToEdit[3]);
        $("#editExtra1").val(stateToEdit[4]);
        $("#editExtra2").val(stateToEdit[5]);
        $('#editModal').modal('toggle');

    });

    $("#stateTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        stateToEdit = findState(id);
        stateIdToDelete = id;
        $("#deleteStateName").html(stateToEdit[1]);
        $('#deleteModal').modal('toggle');

    });

    $("#createState").click(function () {
        $("#addModal").modal("show");
    });


    $("#saveState").click(function () {
        if ($("#stateName").val() != "")
        {
            $("#errorMsg").html("");

            var stateToSave = {
                AreaName: $("#stateName").val(),
                Narration: $("#narration").val(),
                Extra1: "",
                Extra2: ""
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/State/AddState",
                type: 'POST',
                data: JSON.stringify(stateToSave),
                contentType: "application/json",
                success: function (data) {
                    if(data == -1)
                    {
                        hideLoader();
                        Utilities.ErrorNotification("State Exists already");
                    }
                    else if(data == 0)
                    {
                        hideLoader();
                        Utilities.ErrorNotification("Error Saving State");
                    }
                    else
                    {
                        $('#addModal').modal("hide");
                        $("#stateName").val("");
                        $("#narration").val("");
                        hideLoader();
                        Utilities.SuccessNotification("State saved successfully");
                        table.destroy();
                        getStates();
                    }
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else
        {
            $("#errorMsg").html("State name is required!");
        }
        
    });

    $("#delete").click(function () {
        var stateToDelete = {
            AreaId: stateIdToDelete
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/State/DeleteState",
            type: 'POST',
            data:JSON.stringify(stateToDelete),
            contentType: "application/json",
            success: function (data) {
                if(data == 1)
                {
                    $('#deleteModal').modal('hide');
                    hideLoader();
                    Utilities.SuccessNotification("State Deleted Successfully");
                    table.destroy();
                    getStates();
                }
                else if (data == -1) {
                    hideLoader();
                    $('#deleteModal').modal('hide');
                    Utilities.ErrorNotification("State Refernece Exists");
                }
                else
                {
                    Utilities.ErrorNotification("Could not delete State");
                    hideLoader();
                }
                //hideLoader();
             
            },
            error: function (request, error) {
                hideLoader();
            }
        });
    });

    $("#saveChanges").click(function () {
        if ($("#editStateName").val() != "") {
            $("#editErrorMsg").html("");

            var stateToEdit = {
                AreaId: $("#editStateId").val(),
                AreaName: $("#editStateName").val(),
                Narration: $("#editNarration").val(),
                ExtraDate: $("#editDate").val(),
                Extra1: "",
                Extra2: ""
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/State/EditState",
                type: 'POST',
                data: JSON.stringify(stateToEdit),
                contentType: "application/json",
                success: function (data) {
                    // hideLoader();
                    Utilities.SuccessNotification("Changes made successfully.");
                    table.destroy();
                    getStates();
                    $('#editModal').modal("hide");
                    $("#editStateName").val("");
                    $("#editNarration").val("");
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else {
            $("#editErrorMsg").html("State name is required!");
        }

    });
});

function getStates() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/State/GetStates",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function onSuccess(data) {
    var objToShow = [];
    $.each(data, function (i, record) {

        objToShow.push([
            (i + 1),
            record.AreaName,
            record.Narration,
            '<button type="button" class="frmState-Update btnEditModal btn btn-primary text-left" id="' + record.AreaId + '"><i class="fa fa-edit"></i> Edit</button>',
            '<button type="button" class="frmState-Delete btnDeleteModal btn btn-danger text-left" id="' + record.AreaId + '"><i class="fa fa-times-circle"></i> Delete</button>'
        ]);
        states.push([
            record.AreaId,
            record.AreaName,
            record.Narration,
            record.ExtraDate,
            record.Extra1,
            record.Extra2
        ]);
    });
    var count = 0;
    if (checkPriviledge("frmState", "Update") === false) {
        objToShow.forEach(function (item) {
            item.splice(3, 1);
        });
        count++;
    }
    if (checkPriviledge("frmState", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 4 - count;
            item.splice(recalculateIndex, 1);
        });
    }
    table = $('#stateTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

    hideLoader();
}

function showModal() {

    $('#myModal').modal('toggle');
    $('#myModal').on('hide.bs.modal', function (e) {

    })
}

function findState(id) {
    var state = "";
    for (i = 0; i < states.length; i++) {
        if (states[i][0] == id) {
            state = states[i];
        }
    }
    return state;
}