﻿var batchToEdit = "";
var batchIdToDelete = 0;
var table = "";
var groups = [];
var taxes = [];
var taxToEdit = "";
var taxIdToDelete = 0;

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created

$(function () {
    getTaxes();
    getLookUps();
   

    //$('#manufacturingDate').datepicker({
    //    autoclose: true
    //});

    //$('#expiryDate').datepicker({
    //    autoclose: true
    //});
    //$(".select2").select2();

   

    $('#addModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });

    $("#batchTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        batchToEdit = findBatch(id);
        var productId = products.find(p => p.ProductId == batchToEdit[2]).ProductId;
        $("#editBatchId").val(batchToEdit[0]);
        $("#editBatchName").val(batchToEdit[1]);
        $("#editNarration").val(batchToEdit[5]);
        $("#editManufacturingDate").val(batchToEdit[3].substring(0,10));
        $("#editExpiryDate").val(batchToEdit[4].substring(0, 10));
        $("#products").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(products, 1, 3));
        $("#products").chosen({ width: "100%", margin: "1px" });
        $("#products").val(productId);
        $("#products").trigger("chosen:updated");

        $('#editModal').modal('toggle');

    });

    $("#batchTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        batchToEdit = findBatch(id);
        batchIdToDelete = id;
        $("#deletebatchName").html(batchToEdit[1]);
        $('#deleteModal').modal('toggle');

    });


    

   

    $("#saveChjkanges").click(function () {
        if ($("#editBatchName").val() != "") {
            $("#editErrorMsg").html("");

            var batchToEdit = {
                BatchId: $("#editBatchId").val(),
                BatchNo: $("#editBatchName").val(),
                ProductId: $("#products").val(),
                narration: $("#editNarration").val(),
                ExpiryDate: $("#editExpiryDate").val(),
                ManufacturingDate: $("#editManufacturingDate").val(),
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/Batch/EditBatch",
                type: 'POST',
                data: JSON.stringify(batchToEdit),
                contentType: "application/json",
                success: function (data) {
                    //hideLoader();
                    table.destroy(); 
                    $("#editBatchName").val("");
                    $("#editManufacturingDate").val(Utilities.YyMmDdDate());
                    $("#editExpiryDate").val(Utilities.YyMmDdDate()); 
                    $("#editNarration").val(""); 
                    $("#products").val("");
                    $("#products").trigger("chosen:updated");
                    
                    //$("#batchTable tbody").html("");
                    //$('#batchTable').DataTable().destroy();
                    getBatches();
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else {
            $("#editErrorMsg").html("Batch name is required!");
        }

    });
});


function getTaxes() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/Tax/GetTaxes",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            var objToShow = [];
            console.log(data);
            taxes = data;
            $.each(data, function (i, record) {
                var isActiveLabel = record.IsActive == "1" ? '<center><label class="label label-primary">Yes<label></center>' : '<center><label class="label label-default">No<label><center>'
                objToShow.push([
                    (i + 1),
                    record.TaxName,
                    record.ApplicableOn,
                    record.CalculatingMode,
                    isActiveLabel,
                    '<button type="button" class="btnEditModal btn btn-primary text-left" id="' + record.TaxId + '"><i class="fa fa-edit"></i> Edit</button>',
                    '<button type="button" class="btnDeleteModal btn btn-danger text-left" id="' + record.TaxId + '"><i class="fa fa-times-circle"></i> Delete</button>'
                ]);
            });
           
            table = $('#taxTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}
function getLookUps() {
    var groupsLookupAjax = $.ajax({
        url: API_BASE_URL + '/AccountGroup/GetAccountGroups',
        type: "GET",
        contentType: "application/json"
    });

    $.when(groupsLookupAjax)
    .done(function (dataGroups) {
        groups = dataGroups;
        console.log("groups", dataGroups);
        $("#group").html("<option value=\"\"></option>" + Utilities.PopulateDropDownFromArray(groups, 1, 2));
        $("#group").chosen({ width: "100%", margin: "1px" });
    });
}

$("#save").click(function () {
    if (!validateTaxDetails()) {
        return false;
    }
    var taxToSave = {
        taxName: $("#taxName").val(),
        applicableOn: $("#applicableOn").val(),
        rate: $("#taxRate").val(),
        calculatingMode: $("#calculatingMode").val(),
        isActive: $('#isActive').is(":checked"),
        type: $("#taxType").val(),
        narration: $("#narration").val(),
        //accountGroupId: $("#group").val(),
        extra2: ""
    }

    showLoader();
    $.ajax({
        url: API_BASE_URL + "/Tax/AddTax",
        type: 'POST',
        data: JSON.stringify(taxToSave),
        contentType: "application/json",
        success: function (data) {
            hideLoader();
            table.destroy();
            Utilities.SuccessNotification("Tax saved successfully!");
            $("#taxName").val("");
            $("#taxRate").val("");
            $("#applicableOn").val("NA");
            $("#taxType").val("NA");
            $("#calculatingMode").val("All");
            $("#narration").val("");
            //$("#group").val("");
            //$("#group").trigger("chosen:updated");
            getTaxes();
        },
        error: function (request, error) {
            Utilities.ErrorNotification("Something went wrong!")
            hideLoader();
        }
    });

});

function validateTaxDetails() {
    //if ($("#group").val() == "") {
    //    Utilities.Alert("Select a group!");
    //    return false;
    //}
    if ($("#taxName").val() == "") {
        Utilities.Alert("You have to input a tax name!");
        return false
    }
    else if ($("#taxRate").val() == "" || Number($("#taxRate").val()) <= 0) {
        Utilities.Alert("Invalid Tax Rate!");
        return false
    }
    return true;
}

$("#taxTable").on("click", ".btnEditModal", function () {
    var id = $(this).attr("id");
    taxToEdit = taxes.find(p => p.TaxId == id);
    if (taxToEdit != undefined) {
        $("#editTaxId").val(taxToEdit.TaxId);
        $("#editTaxType").val(taxToEdit.Type);
        $("#editTaxName").val(taxToEdit.TaxName);
        $("#editTaxRate").val(taxToEdit.Rate);
        $("#editApplicableOn").val(taxToEdit.ApplicableOn);
        $("#editCalculatingMode").val(taxToEdit.CalculatingMode);
        $("#editTaxName").val(taxToEdit.TaxName);
        $("#editNarration").val(taxToEdit.Narration);
    }


    $('#editModal').modal('toggle');

});

$("#saveChanges").click(function () {
    if (!validateTaxDetailsToEdit()) {
        return false;
    }
    var taxToSave = {
        taxId: $("#editTaxId").val(),
        taxName: $("#editTaxName").val(),
        applicableOn: $("#editApplicableOn").val(),
        rate: $("#editTaxRate").val(),
        calculatingMode: $("#editCalculatingMode").val(),
        isActive: $('#editIsActive').is(":checked"),
        type: $("#editTaxType").val(),
        narration: $("#editNarration").val(),
        extra2: ""
    }
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/Tax/EditTax",
        type: 'POST',
        data: JSON.stringify(taxToSave),
        contentType: "application/json",
        success: function (data) {
            hideLoader();
            Utilities.SuccessNotification("Tax updated successfully!");
            table.destroy();
            $("#editTaxName").val("");
            $("#editTaxRate").val("");
            $("#editApplicableOn").val("NA");
            $("#editTaxType").val("NA");
            $("#editCalculatingMode").val("All");
            $("#editNarration").val("");
            getTaxes();
        },
        error: function (request, error) {
            Utilities.ErrorNotification("Something went wrong!")
            hideLoader();
        }
    });
});

function validateTaxDetailsToEdit() {
    if ($("#editTaxName").val() == "") {
        Utilities.Alert("You have to input a tax name!");
        return false
    }
    else if ($("#editTaxRate").val() == "" || Number($("#editTaxRate").val()) <= 0) {
        Utilities.Alert("Invalid Tax Rate!");
        return false
    }
    return true;
}

$("#taxTable").on("click", ".btnDeleteModal", function () {
    var id = $(this).attr("id");
    taxToEdit = taxes.find(p => p.TaxId == id);
    if (taxToEdit != undefined) {
        $("#deleteTaxName").html(taxToEdit.TaxName);
        $('#deleteModal').modal('toggle');
        taxIdToDelete = id;
    }
    else {
        Utilities.ErrorNotification("Something went wrong!");
    }
});

$("#delete").click(function () {
    var taxToDelete = {
        taxId: taxIdToDelete
    };;
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/Tax/DeleteTax",
        type: 'POST',
        data: JSON.stringify(taxToDelete),
        contentType: "application/json",
        success: function (data) {
            hideLoader();
            Utilities.SuccessNotification("Tax deleted successfully!");
            $('#deleteModal').modal('toggle');
            table.destroy();
            getTaxes();
        },
        error: function (request, error) {
            Utilities.ErrorNotification("Something went wrong!");
            hideLoader();
        }
    });
});