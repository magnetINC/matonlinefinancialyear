﻿var states = [];
var cities = [];
var cityToEdit = "";
var cityIdToDelete = 0;
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created


$(function () {

    getStates();

    $("#cityTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        cityToEdit = findCity(id);
        $("#editCityId").val(cityToEdit[0]);
        $("#editCityName").val(cityToEdit[1]);
        $("#editStateId").val(cityToEdit[2]);
        $("#editNarration").val(cityToEdit[3]);
        $("#editDate").val(cityToEdit[4]);
        $("#editExtra1").val(cityToEdit[5]);
        $("#editExtra2").val(cityToEdit[6]);
        $('#editModal').modal('toggle');

    });

    $("#cityTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        cityToEdit = findCity(id);
        cityIdToDelete = id;
        $("#deleteCityName").html(cityToEdit[1]);
        $('#deleteModal').modal('toggle');

    });


    $("#saveCity").click(function () {
        if ($("#cityName").val() != "")
        {
            $("#errorMsg").html("");

            var cityToSave = {
                RouteName: $("#cityName").val(),
                AreaId: $("#stateId").val(),
                Narration: $("#narration").val(),
                Extra1:"",
                Extra2: ""
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/City/AddCity",
                type: 'POST',
                data: JSON.stringify(cityToSave),
                contentType: "application/json",
                success: function (data) {
                    if (data == -1) {
                        hideLoader();
                        Utilities.ErrorNotification("City Exists already");
                    }
                    else if (data == 0) {
                        hideLoader();
                        Utilities.ErrorNotification("Error Saving city");
                    }
                    else {
                        $('#addModal').modal("hide");
                        $("#cityName").val("");
                        $("#narration").val("");
                        hideLoader();
                        Utilities.SuccessNotification("City saved successfully");
                        table.destroy();
                        getCities();
                    }
                },
                error: function (request, error) {
                    hideLoader();
                }
            });
        }
        else
        {
            $("#errorMsg").html("City name is required!");
        }
        
    });

    $("#delete").click(function () {
        var cityToDelete = {
            RouteId: cityIdToDelete
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/City/DeleteCity",
            type: 'POST',
            data:JSON.stringify(cityToDelete),
            contentType: "application/json",
            success: function (data) {
                //hideLoader();
                if(data == true)
                {
                    table.destroy();
                    getCities();
                    $('#deleteModal').modal("hide");
                    hideLoader();
                    Utilities.SuccessNotification("City deleted successfully");
                }
                else {
                    hideLoader();
                    Utilities.ErrorNotification("could not delete city");
                }
            },
            error: function (request, error) {
                hideLoader();
                Utilities.ErrorNotification("Error deleting");
            }
        });
    });

    $("#saveChanges").click(function () {
        if ($("#editRouteName").val() != "") {
            $("#editErrorMsg").html("");

            var cityToEdit = {
                RouteId: $("#editCityId").val(),
                RouteName: $("#editCityName").val(),
                AreaId: $("#editStateId").val(),
                Narration: $("#editNarration").val(),
                //ExtraDate: $("#editDate").val(),
                Extra1: "",
                Extra2: ""
            };
            console.log(cityToEdit);
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/City/EditCity",
                type: 'POST',
                data: JSON.stringify(cityToEdit),
                contentType: "application/json",
                success: function (data) {
                   // hideLoader();
                    table.destroy();
                    getCities();
                    $('#editModal').modal("hide");
                    Utilities.SuccessNotification("City saved successfully");
                },
                error: function (request, error) {
                    hideLoader();
                    Utilities.ErrorNotification("Error Saving");
                }
            });
        }
        else {
            $("#editErrorMsg").html("City name is required!");
        }

    });
});

function getStates() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/State/GetStates",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            states = data;
            //hideLoader();
            var citiesHtml = "";
            $.each(states, function (i, record) {
                citiesHtml += '<option value="'+record.AreaId+'">'+record.AreaName+'</option>';
            });
            $("#stateId").html(citiesHtml);
            $("#editStateId").html(citiesHtml);
            getCities();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function getCities() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/City/GetCities",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors.
            setTimeout(onSuccess, 500, data);   
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function onSuccess(data) {
    var objToShow = [];
    $.each(data, function (i, record) {
        objToShow.push([
            (i + 1),
            record.RouteName,
            findState(record.AreaId).AreaName,
            '<button type="button" class="frmCity-Update btnEditModal btn btn-primary text-left" id="' + record.RouteId + '"><i class="fa fa-edit"></i> Edit</button>',
            '<button type="button" class="frmCity-Delete btnDeleteModal btn btn-danger text-left" id="' + record.RouteId + '"><i class="fa fa-times-circle"></i> Delete</button>'
        ]);
        cities.push([
            record.RouteId,
            record.RouteName,
            record.AreaId,
            record.Narration,
            record.ExtraDate,
            record.Extra1,
            record.Extra2
        ]);
    });
    var count = 0;
    if (checkPriviledge("frmCity", "Update") === false) {
        objToShow.forEach(function (item) {
            item.splice(3, 1);
        });
        count++;
    }
    if (checkPriviledge("frmCity", "Delete") === false) {
        objToShow.forEach(function (item) {
            var recalculateIndex = 4 - count;
            item.splice(recalculateIndex, 1);
        });
    }
    table = $('#cityTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    hideLoader();
}

function findState(id) {
    var state = "";
    for (i = 0; i < states.length; i++) {
        if (states[i].AreaId == id) {
            state = states[i];
            break;
        }
    }
    return state;
}

function findCity(id) {
    var city = "";
    for (i = 0; i < cities.length; i++) {
        if (cities[i][0] == id) {
            city = cities[i];            
            break;
        }
    }
    return city;
}