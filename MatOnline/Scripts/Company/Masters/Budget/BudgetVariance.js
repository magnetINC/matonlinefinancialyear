﻿var budgetVariance = [];
var budgets = [];

var table = "";

$(function () {
    getLookUps();
    $("#budget").change(function () {
        getBudgetVariances($("#budget").val())
    });
});


//=============== API CALLS ==============
function getLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BudgetVariance/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            budgets = data.Response;

            renderDataToControls();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getBudgetVariances(id)
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BudgetVariance/GetBudgetVariance?decId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            budgetVariance = data.Response;
            console.log(budgetVariance);

            renderBudgetVariance();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}


//=============RENDER DATA TO CONTROLS===========
function renderDataToControls()
{
    $("#budget").kendoDropDownList({
        filter: "contains",
        dataTextField: "budgetName",
        dataValueField: "budgetMasterId",
        dataSource: budgets,
        optionLabel: "Please Select..."
    });
}

function renderBudgetVariance()
{
    var output = "";
    var objToShow = [];
    var statusOutput = "";

    if (table != "") {
        table.destroy();
    }
    $.each(budgetVariance, function (count, row) {
        objToShow.push([
            count + 1,
            row.particular,
            '&#8358;' + Utilities.FormatCurrency(row.BudgetDr),
            '&#8358;' + Utilities.FormatCurrency(row.ActualDr),
            '&#8358;' + Utilities.FormatCurrency(row.VarianceDr),
            '&#8358;' + Utilities.FormatCurrency(row.BudgetCr),
            '&#8358;' + Utilities.FormatCurrency(row.ActualCr),
            '&#8358;' + Utilities.FormatCurrency(row.VarianceCr),
        ]);
    });
    table = $('#budgetVarianceTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}