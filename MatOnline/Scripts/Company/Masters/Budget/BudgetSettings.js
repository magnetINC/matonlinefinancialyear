﻿var budgets = [];
var ledgers = [];
var groups = [];
var budgetItems = [];
var budgetDetails = [];
var particular = [];

var thisBudget = {};
var newBudget = {};

var table = "";

$(function () {
    getLookUps();
    getMasters($("#accountType").val());

    $("#newBudget").hide();
    $("#accountTypeSearch").change(function () {
        if ($("#accountTypeSearch").val() == "Account Ledger") {
            particular = [];
            $.each(ledgers, function (count, row) {
                particular.push({ particular: row.ledgerName });
                renderLookUpDataToControls();
            });
        }
        else {
            particular = [];
            $.each(groups, function (count, row) {
                particular.push({ particular: row.accountGroupName });
                renderLookUpDataToControls();
            });
        }
        getMasters($("#accountTypeSearch").val());
    });
    $("#accountType").change(function () {
        if ($("#accountType").val() == "Account Ledger") {
            particular = [];
            $.each(ledgers, function (count, row) {
                particular.push({ particular: row.ledgerName });
            });
            renderLookUpDataToControls();
        }
        else {
            particular = [];
            $.each(groups, function (count, row) {
                particular.push({ particular: row.accountGroupName });
            });
            renderLookUpDataToControls();
        }
    });
    $("#search").click(function () {
        getMasters($("#accountType").val())
    });
});

//========== API CALLS ==========
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BudgetSettings/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            ledgers = data.Response.accounts;
            groups = data.Response.groups;

            particular = [];
            $.each(ledgers, function (count, row) {
                particular.push({ particular: row.ledgerName });
                renderLookUpDataToControls();
            });

            renderLookUpDataToControls();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getMasters(accountType) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BudgetSettings/GetBudgetSettings?accountType=" + accountType,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            budgets = data.Response;

            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors.
            setTimeout(renderMasterToTable, 500);

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getDetails(id) {
    budgetItems = [];
    thisBudget = [];
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BudgetSettings/GetBudgetSettingDetails?budgetSettingId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            budgetDetails = data.Response;
            var indexOfObject = budgets.findIndex(p => p.budgetMasterId == id);
            thisBudget = budgets[indexOfObject];

            $.each(budgetDetails, function (count, row) {
                budgetItems.push({
                    BudgetMasterId: row.budgetMasterId,
                    BudgetDetailsId: row.budgetDetailsId,
                    Particular: row.particular,
                    Credit: row.credit,
                    Debit: row.debit,
                    Extra1: "",
                    Extra2: "",
                    ExtraDate: new Date()
                });
            });

            $("#budgetViewModal").modal("show");
            renderBudgetDetailsToTable();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function deleteSetting(id) {
    if (confirm("Do you really want to delete a budget")) {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/BudgetSettings/DeleteBudgetSettings?budgetSettingId=" + id,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification(data.ResponseMessage);
                    window.location = "/Company/Budget/BudgetSettings";

                    Utilities.Loader.Hide();
                }
                else {
                    Utilities.SuccessNotification(data.ResponseMessage);
                }

                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}
function saveBudget() {
    if ($("#budgetName").val() == "") {
        Utilities.ErrorNotification("please enter budget Name");
    }
    else {
        console.log(calculateDebitAndCreditTotal());
        thisBudget.BudgetName = $("#budgetName").val();
        thisBudget.Type = $("#accountType").val();
        thisBudget.TotalDr = calculateDebitAndCreditTotal().debit; //$("#totalDr").val();
        thisBudget.TotalCr = calculateDebitAndCreditTotal().credit; //$("#totalCr").val();
        thisBudget.FromDate = $("#fromDate").val();
        thisBudget.ToDate = $("#toDate").val();
        thisBudget.Narration = $("#narration").val();
        thisBudget.Extra1 = "";
        thisBudget.Extra2 = "";
        thisBudget.ExtraDate = new Date();

        console.log(thisBudget);

        var toSave = { master: thisBudget, details: budgetItems }

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/BudgetSettings/SaveBudgetSettings",
            type: "POST",
            data: JSON.stringify(toSave),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Settings saved succeessfully");
                    window.location = "/Company/Budget/BudgetSettings";
                    Utilities.Loader.Hide();
                    budgetItems = []; 
                }
                else {
                    Utilities.SuccessNotification(data.ResponseMessage);
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}
function saveChanges() {
    if (thisBudget == {} || budgetItems == []) {
        Utilities.ErrorNotification("Budget Items and details cannot be empty");
    }
    else {
        var editBudget = {};

        editBudget.BudgetMasterId = thisBudget.budgetMasterId;
        editBudget.BudgetName = $("#budgetNameView").val();
        editBudget.Type = thisBudget.type;
        editBudget.TotalDr = calculateDebitAndCreditTotal().debit;// $("#totalDrView").val();
        editBudget.TotalCr = calculateDebitAndCreditTotal().credit;// $("#totalCrView").val();
        editBudget.FromDate = $("#fromDateView").val();
        editBudget.ToDate = $("#toDateView").val();
        editBudget.Narration = $("#narrationView").val();
        editBudget.Extra1 = "";
        editBudget.Extra2 = "";
        editBudget.ExtraDate = new Date();

        $.each(budgetItems, function (count, row) {
            row.Extra1 = "";
        });

        var toSave = { master: editBudget, details: budgetItems }
        console.log(toSave);

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/BudgetSettings/EditBudgetSettings",
            type: "POST",
            data: JSON.stringify(toSave),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Settings saved succeessfully");
                    window.location = "/Company/Budget/BudgetSettings";
                    Utilities.Loader.Hide();
                }
                else {
                    Utilities.SuccessNotification(data.ResponseMessage);
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}

// ======== RENDER DATA TO CONTROLS
function renderLookUpDataToControls() {
    $("#particular").kendoDropDownList({
        filter: "contains",
        dataTextField: "particular",
        dataValueField: "particular",
        dataSource: particular,
        optionLabel: "Please Select..."
    });
}

$("#particular").change(function () {
    let groupId = "";
    let groupObj = {};
    ledgers.forEach(function (ledger) {
        groupId = ledger.accountGroupId;
        if ($("#particular").val() == ledger.ledgerName) {
            groupObj = groups.find(x => x.accountGroupId == groupId);
            if (groupObj.nature == "Liabilities") {
                $("#drOrCr").val("Cr");
            }
            else if (groupObj.nature == "Assets") {
                $("#drOrCr").val("Dr");
            }
            else if (groupObj.nature == "Expenses") {
                $("#drOrCr").val("Dr");
            }
            else if (groupObj.nature == "Income") {
                $("#drOrCr").val("Cr");
            }
            else {
                $("#drOrCr").val("NA");
            }
        };
    });


});


function renderMasterToTable() {
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }
    $.each(budgets, function (count, row) {
        objToShow.push([
            count + 1,
            row.budgetName,
            Utilities.FormatJsonDate(row.fromDate),
            Utilities.FormatJsonDate(row.toDate),
            '&#8358;' + Utilities.FormatCurrency(row.totalDr),
            '&#8358;' + Utilities.FormatCurrency(row.totalCr),
            row.narration,
            '<div class="btn-group btn-group-sm action">\
                <div class="dropdown">\
                    <button class="btn btn-primary dropdown-toggle " data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button>\
                    <ul class="dropdown-menu">\
                        <li onclick="getDetails(' + row.budgetMasterId + ')" class="viewDetails"><a><i class="fa fa-eye "></i> View Details</a></li>\
                        <li onclick="deleteSetting(' + row.budgetMasterId + ')" class="delete"><a><i class="fa fa-times"></i> Delete</a></li>\
                    </ul>\
                </div>\
            </div>'
        ]);
    });
    table = $('#budgetsTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    var isFalse = 0;
    if (checkPriviledge("frmBudget", "View") === false) {
        $('.viewDetails').hide();
        isFalse++;
    }
    if (checkPriviledge("frmBudget", "Delete") === false) {
        $('.delete').hide();
        isFalse++;
    }
    if (isFalse == 2) {
        $('.action').hide();
    }
}
function renderBudgetItemsToTable() {
    var output = "";
    var objToShow = [];

    if (table != "") {

        table.destroy();
    }

    $.each(budgetItems, function (count, row) {
        row.Extra1 = count;
        objToShow.push([
            count + 1,
            row.Particular,
            '&#8358;' + Utilities.FormatCurrency(row.Debit),
            '&#8358;' + Utilities.FormatCurrency(row.Credit),
            '<a class="btn btn-danger" onclick="removeFromBudgetItems(' + row.Extra1 + ');"><i class="fa fa-trash"></i></a>'
        ]);
    });
    $("#totalCr").val(Utilities.FormatCurrency(calculateDebitAndCreditTotal().credit));
    $("#totalDr").val(Utilities.FormatCurrency(calculateDebitAndCreditTotal().debit));
    table = $('#newBudgetsTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}
function renderBudgetDetailsToTable() {
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }

    $.each(budgetItems, function (count, row) {
        row.Extra1 = count;
        objToShow.push([
            count + 1,
            row.Particular,
            '&#8358;' + Utilities.FormatCurrency(row.Debit),
            '&#8358;' + Utilities.FormatCurrency(row.Credit),
            '<a class="btn btn-danger" onclick="removeFromBudgetDetails(' + row.Extra1 + ');"><i class="fa fa-trash"></i></a>'
        ]);
    });
    $("#totalCrView").val(Utilities.FormatCurrency(calculateDebitAndCreditTotal().credit));
    $("#totalDrView").val(Utilities.FormatCurrency(calculateDebitAndCreditTotal().debit));
    $("#budgetNameView").val(thisBudget.budgetName);
    $("#narrationView").val(thisBudget.narration);
    $("#fromDateView").val(thisBudget.fromDate.split('T')[0]);
    $("#toDateView").val(thisBudget.toDate.split('T')[0]);
    $("#accountTypeView").val(thisBudget.type);

    table = $('#budgetListTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}


function addBudgetItemsToArray() {
    var dr = 0;
    var cr = 0;
    var val = $("#particular").val();

    var indexOfObject = budgetItems.findIndex(p => p.Particular == val);

    if (indexOfObject >= 0) {
        Utilities.ErrorNotification("This Ledger Account has been Selected already");
    }
    else if ($("#amount").val() <= 0) {
        Utilities.ErrorNotification("Invalid Amount");
    }
    else {
        if ($("#drOrCr").val() == "Cr") {
            cr = $("#amount").val();
            dr = 0;
        }
        else {
            dr = $("#amount").val();
            cr = 0;
        }
        budgetItems.push({
            Particular: $("#particular").val(),
            Credit: cr,
            Debit: dr,
            Extra1: "",
            Extra2: "",
            ExtraDate: new Date()
        });
    }
    $("#totalCr").val(Utilities.FormatCurrency(calculateDebitAndCreditTotal().credit));
    $("#totalDr").val(Utilities.FormatCurrency(calculateDebitAndCreditTotal().debit));
    renderBudgetItemsToTable();
    //renderBudgetDetailsToTable();
    console.log(budgetItems);
    clearNewBudgetModal();
    $('#particular option').prop('selected', function () {
        return this.defaultSelected;
    });
    //$("#budgetModal").modal("hide");

}

function clearNewBudgetModal() {
    $("#amount").val('');
    $("#drOrCr").val('');
    $("#particular").data("kendoDropDownList").select(0); 
}


function addLineItems() {
    $("#budgetModal").modal("show");
    $('#budgetModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
}
function showNewBudget() {
    $("#newBudget").show();
    $("#viewBudget").hide();
}
function Close() {
    $("#newBudget").hide();
    $("#viewBudget").show();
}

function removeFromBudgetDetails(count) {
    var indexofobjecttoremove = budgetItems.findIndex(p => p.Extra1 == count);
    budgetItems.splice(indexofobjecttoremove, 1);

    renderBudgetDetailsToTable();
}
function removeFromBudgetItems(count) {
    var indexofobjecttoremove = budgetItems.findIndex(p => p.Extra1 == count);
    budgetItems.splice(indexofobjecttoremove, 1);

    renderBudgetItemsToTable();
}
function findLedger(id) {
    var output = {};
    for (i = 0; i < ledgers.length; i++) {
        if (ledgers[i].ledgerId == id) {
            output = ledgers[i];
            break;
        }
    }
    return output;
}
function calculateDebitAndCreditTotal() {
    var credit = 0;
    var debit = 0;

    for (i = 0; i < budgetItems.length; i++) {
        credit = credit + parseFloat(budgetItems[i].Credit);
        debit = debit + parseFloat(budgetItems[i].Debit);
    }
    var obj = { credit: credit, debit: debit }

    return obj;
}
function calculateDebitAndCreditTotal2() {
    var credit = 0;
    var debit = 0;

    for (i = 0; i < budgetDetails.length; i++) {
        credit = credit + parseFloat(budgetDetails[i].credit);
        debit = debit + parseFloat(budgetDetails[i].debit);
    }
    var obj = { cr: credit, dr: debit }

    return obj;
}

