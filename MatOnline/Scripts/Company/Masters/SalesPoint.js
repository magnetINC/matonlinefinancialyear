﻿var salesPoints = [];
var salesPointToEdit = "";
var salesPointIdToDelete = 0;
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created


$(function () {
    PriviledgeSettings.ApplyUserPriviledge();

    getSalesPoint();
    $("#salesPointTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        salesPointToEdit = findSalesPoint(id);
        
        $("#editSalesPointId").val(salesPointToEdit[0]);
        $("#editSalesPointName").val(salesPointToEdit[1]);
        $('#editModal').modal('toggle');

    });

    $("#salesPointTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        salesPointToEdit = findSalesPoint(id);
        salesPointIdToDelete = id;
        $("#deleteSalesPointName").html(salesPointToEdit[1]);
        $('#deleteModal').modal('toggle');

    });


    $("#saveSalesPoint").click(function () {
        if ($("#salesPointName").val() != "")
        {
            $("#errorMsg").html("");

            var salesPointToSave = {
                CounterName: $("#salesPointName").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/SalesPoint/AddCounter",
                type: 'POST',
                data: JSON.stringify(salesPointToSave),
                contentType: "application/json",
                success: function (data) {
                    //hideLoader();
                    Utilities.SuccessNotification("Creation Successfull!");
                    table.destroy();
                    getSalesPoint();
                },
                error: function (request, error) {
                    hideLoader();
                    Utilities.ErrorNotification("Something went wrong!");
                }
            });
            $("#salesPointName").val("");
        }
        else
        {
            $("#errorMsg").html("Sales point name is required!");
        }
        
    });

    $("#delete").click(function () {
        var salesPointToDelete = {
            CounterId: salesPointIdToDelete
        };
        showLoader();
        $.ajax({
            url: API_BASE_URL + "/SalesPoint/DeleteCounter",
            type: 'POST',
            data:JSON.stringify(salesPointToDelete),
            contentType: "application/json",
            success: function (data) {
                //hideLoader();
                table.destroy();
                Utilities.SuccessNotification("Deletion Successfull!");
                getSalesPoint();
            },
            error: function (request, error) {
                hideLoader();
                Utilities.ErrorNotification("Something went wrong!");
            }
        });
    });

    $("#saveChanges").click(function () {
        if ($("#editSalesPointName").val() != "") {
            $("#editErrorMsg").html("");

            var salesPointToEdit = {
                CounterId: $("#editSalesPointId").val(),
                CounterName: $("#editSalesPointName").val()
            };
            showLoader();
            $.ajax({
                url: API_BASE_URL + "/SalesPoint/EditCounter",
                type: 'POST',
                data: JSON.stringify(salesPointToEdit),
                contentType: "application/json",
                success: function (data) {
                   // hideLoader();
                    table.destroy();
                    Utilities.SuccessNotification("Edited Successfully!");
                    getSalesPoint();
                },
                error: function (request, error) {
                    hideLoader();
                    Utilities.ErrorNotification("Something went wrong!");
                }
            });
            $("#editSalesPointName").val("");
        }
        else {
            $("#editErrorMsg").html("Sales point name is required!");
        }

    });
});

function getSalesPoint() {
    showLoader();
    $.ajax({
        url: API_BASE_URL + "/SalesPoint/GetCounters",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            var objToShow = [];
            $.each(data, function (i, record) {

                objToShow.push([
                    (i + 1),
                    record.CounterName,
                    '<button type="button" class="btnEditModal btn btn-primary text-left" id="' + record.CounterId + '"><i class="fa fa-edit"></i> Edit</button>',
                    '<button type="button" class="btnDeleteModal btn btn-danger text-left" id="' + record.CounterId + '"><i class="fa fa-times-circle"></i> Delete</button>'
                ]);
                salesPoints.push([
                    record.CounterId,
                    record.CounterName
                ]);
            });
            debugger;
            var count = 0;
            if (checkPriviledge("frmSalesPoint", "Update") === false) {
                objToShow.forEach(function (item) {
                    item.splice(2, 1);
                });
                count++;
            }

            if (checkPriviledge("frmSalesPoint", "Delete") === false) {
                objToShow.forEach(function (item) {
                    var recalculateIndex = 3 - count;
                    item.splice(recalculateIndex, 1);
                });
            }
            console.log(salesPoints);
            table=$('#salesPointTable').DataTable({
                data: objToShow,
                "paging": true,
                "lengthChange": true,
                "searching": true,
                "ordering": true,
                "info": true,
                "autoWidth": true
            });
            hideLoader();
        },
        error: function (request, error) {
            hideLoader();
        }
    });
}

function showModal() {

    $('#myModal').modal('toggle');
    $('#myModal').on('hide.bs.modal', function (e) {

    })
}

function findSalesPoint(id) {
    var salesPoint = "";
    for (i = 0; i < salesPoints.length; i++) {
        if (salesPoints[i][0] == id) {
            salesPoint = salesPoints[i];
        }
    }
    return salesPoint;
}

var setWaitInterval = setInterval(waitTillUserPrivilegeLoads, 100);

function waitTillUserPrivilegeLoads() {
    if(userPriviledges.length != 0){
        getSalesPoint();
        clearInterval(setWaitInterval);
    }
}