﻿var newHoliday = {}
var holidays = [];
var table = "";

$(function () {
    getHolidays();
});

//======= API CALLS =======
function getHolidays()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/HolidaySettings/GetHolidays",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            holidays = data.Response;
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(renderHolidaysToTable, 500, data);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function saveHoliday() {
    if ($("#holidayName").val() == "")
    {
        Utilities.ErrorNotification("Please enter hoilday Name");
    }
    else if ($("#holidayDate").val() == "") {
        Utilities.ErrorNotification("Please enter hoilday Date");
    }
    else {
        newHoliday.Date = $("#holidayDate").val();
        newHoliday.HolidayName = $("#holidayName").val();
        newHoliday.Narration = $("#narration").val();
        newHoliday.Extra1 = "";
        newHoliday.Extra2 = "";
        newHoliday.ExtraDate = new Date();
        console.log(newHoliday);

         Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/HolidaySettings/SaveNewHoliday",
            type: "POST",
            data: JSON.stringify(newHoliday),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200)
                {
                    Utilities.SuccessNotification("Holiday saved succeessfully");
                    window.location = "/Payroll/HolidaySettings/Index";
                    Utilities.Loader.Hide();
                }
                else
                {
                    Utilities.SuccessNotification(data.ResponseMessage);
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}
function deleteHoliday(id)
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/HolidaySettings/DeleteHoliday?holidayId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if(data.ResponseCode == 200)
            {
                Utilities.SuccessNotification("Holiday deleted Successfully.");
                getHolidays();
                Utilities.Loader.Hide();
            }
            else {
                Utilties.ErrorNotification(data.ResponseMessage);
            }
          
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

// ======= RENDER DATA TO CONTROLS =======
function renderHolidaysToTable()
{
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }
    $.each(holidays, function (count, row) {
        objToShow.push([
            count + 1,
            Utilities.FormatJsonDate(row.date),
            row.holidayName,
            row.narration,
            '<button class="btn btn-danger deleteHoliday" onclick="deleteHoliday(' + row.holidayId + ')"><i class="fa fa-trash"></i></button>'
        ]);
    });
    table = $('#holidaysTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    if (checkPriviledge("frmHolidaySettings", "Delete") === false) {
        $(".deleteHoliday").hide();
    }
}