﻿var dailySalariesToProcess = [];
var cashOrBank = [];


var voucherNo = "";
var table = "";

$(function () {
    getLookUps();
});

// ========== API CALLS =========
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/DailySalary/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            cashOrBank = data.Response.cashOrBank;
            voucherNo = data.Response.invoiceNo;

            renderLookUpDataToControls();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getDailySalariesToProcess() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/DailySalary/GetDailySalaryForProcessing?date=" + $("#salaryDate").val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            dailySalariesToProcess = data.Response;

            //$.each(DailySalariesToProcess, function (count, row) {
            //    if(row.staus == null || row.status == undefined || row.status == "")
            //    {
            //        row.status = "Pending";
            //    }
            //});

            renderDetailsForView();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function saveDailySalaries() {
    //Utilities.Loader.Show();
    if ($("#cashOrBank").val() == 0 || $("#cashOrBank").val() == "" || $("#cashOrBank").val() == null) {
        Utilities.ErrorNotification("Please Select a Bank");
    }
    else {
        var master = {};
        master.LedgerId = $("#cashOrBank").val();
        master.VoucherNo = $("#voucherNo").val();
        master.InvoiceNo = $("#voucherNo").val();
        master.Date = $("#date").val();
        master.salaryDate = $("#salaryDate").val();
        master.TotalAmount = $("#totalSalary").val();
        master.Narration = $("#narration").val();
        master.ExtraDate = new Date();
        master.Extra1 = "";
        master.Extra2 = "";
        master.SuffixPrefixId = 1;
        master.FinancialYearId = 1;
        master.VoucherTypeId = 26;
        var details = [];
        $.each(dailySalariesToProcess, function (count, row) {
            details.push({
                EmployeeId: row.employeeId,
                Wage: row.bonusAmount,
                Status: row.status,
                ExtraDate: new Date(),
                Extra1: "",
                Extra2: ""
            });
        });
        var toSave = { master: master, details: details };
        console.log(toSave);
        $.ajax({
            url: API_BASE_URL + "/DailySalary/SaveSalary",
            type: "POST",
            data: JSON.stringify(toSave),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification(data.ResponseMessage);
                    window.location = "/Payroll/DailySalary/Index";
                    Utilities.Loader.Hide();
                }
                else {
                    Utilities.ErrorNotification(data.ResponseMessage);
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }

}


// ========== RENDER DATA TO CONTROLS ==========
function renderLookUpDataToControls() {
    $("#voucherNo").val(voucherNo);
    $("#cashOrBank").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: cashOrBank,
        optionLabel: "Please Select..."
    });
}
function renderDetailsForView() {
    console.log(dailySalariesToProcess);
    var output = "";
    var attendanceOutput = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }
    $.each(dailySalariesToProcess, function (count, row) {
        if (row.status == "Paid") {
            statusOutput = '<span class="label label-success">Paid!</span>';
        }
        else {
            statusOutput = '<span class="label label-warning">Pending Payment</span>';
        }
        if (row.attendance == "Present") {
            attendanceOutput = '<span class="label label-mint">Present</span>';
        }
        else if(row.attendance == "Absent") {
            attendanceOutput = '<span class="label label-danger">Absent</span>';
        }
        else
        {
            attendanceOutput = '<span class="label label-info">Half-Day</span>';
        }
        objToShow.push([
            count + 1,
            row.employeeCode,
            row.employeeName,
            '&#8358;' + Utilities.FormatCurrency(row.dailyWage),
            attendanceOutput,
            statusOutput,
            '<div class="btn-group btn-group-sm">\
                <div class="dropdown">\
                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" type="button"> Salary Status <i class="dropdown-caret"></i></button>\
                    <ul class="dropdown-menu">\
                        <li onclick="changeToPaid(' + row.employeeId + ')"><a>Paid</a></li>\
                        <li onclick="changeToPending(' + row.employeeId + ')"><a>Pending</a></li>\
                    </ul>\
                </div>\
            </div>'
        ]);
    });
    table = $('#dailySalaryDetails').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    $("#totalSalary").val(Utilities.FormatCurrency(getTotalSalary()));
}

function changeToPaid(id) {
    var indexOfObject = dailySalariesToProcess.findIndex(p=>p.employeeId == id);
    dailySalariesToProcess[indexOfObject].status = "Paid";

    renderDetailsForView();
}
function changeToPending(id) {
    var indexOfObject = dailySalariesToProcess.findIndex(p=>p.employeeId == id);
    dailySalariesToProcess[indexOfObject].status = "Pending";

    renderDetailsForView();
}
function getTotalSalary() {
    var totalSalary = 0;
    $.each(dailySalariesToProcess, function (count, row) {
        if (row.status == "Paid") {
            totalSalary = totalSalary + row.dailyWage
        }
    });
    return totalSalary;
}