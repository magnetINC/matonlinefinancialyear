﻿var receivingLedgers = [];

var toSave = {};

$(function () {
    getLookUps();
});

// ========== API CALLS =========
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/StatutoryComplianceSettings/AccountGroupComboFill",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            receivingLedgers = data.Response;

            renderLookUpDataToControls();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function saveAdvancePayment() {
    if ($("#receivingLedgerPension").val() == "" || $("#receivingLedgerPension").val() == 0 | $("#receivingLedgerPension").val() == null) {
        Utilities.ErrorNotification("Please Select a receiving ledger for Pension");
    }
    else if ($("#receivingLedgerPAYE").val() == "" || $("#receivingLedgerPAYE").val() == 0 || $("#receivingLedgerPAYE").val() == null) {
        Utilities.ErrorNotification("Please Select a receiving ledger for PAYE");
    }
    else {
        toSave.IsPAYEApplicable = $("#applyPAYE").val();
        toSave.IsPensionApplicable = $("#applyPension").val();
        toSave.PAYEReceivingLedgerId = $("#receivingLedgerPAYE").val();
        toSave.PensionReceivingLedgerId = $("#receivingLedgerPension").val();

        console.log(toSave);
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/StatutoryComplianceSettings/SaveStatutoryComplianceSettings",
            type: "POST",
            data: JSON.stringify(toSave),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification(data.ResponseMessage);
                    window.location = "/Payroll/StatutoryComplianceSettings/Index";
                    Utilities.Loader.Hide();
                }
                else {
                    Utilities.ErrorNotification(data.ResponseMessage);
                    Utilities.Loader.Hide();
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}


/* ======= RENDER DATA TO CONTOLS ======= */
function renderLookUpDataToControls() {
    $("#receivingLedgerPension").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: receivingLedgers,
        optionLabel: "Please Select..."
    });
    $("#receivingLedgerPAYE").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: receivingLedgers,
        optionLabel: "Please Select..."
    });
}