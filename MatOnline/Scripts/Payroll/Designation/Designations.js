﻿var designations = [];
var designationToEdit = "";
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created


$(function () {

    getDesignations();
    $("#designationTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        designationToEdit = findDesignation(id);
        $("#editDesignationName").val(designationToEdit.designationName);
        $("#editClInMonth").val(designationToEdit.leaveDays);
        $("#editNarration").val(designationToEdit.narration);
        $("#editAdvancePayment").val(designationToEdit.advanceAmount);
        $('#editModal').modal('toggle');
    });

    $("#designationTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        if(Utilities.Confirm("Delete this record?"))
        {
            var designationToDelete = {
                designationId: id
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/Designation/DeleteDesignation",
                type: 'POST',
                data: JSON.stringify(designationToDelete),
                contentType: "application/json",
                success: function (data) {
                    table.destroy();
                    getDesignations();
                },
                error: function (request, error) {
                    Utilities.Loader.Hide();
                }
            });
        }
    });

    $("#saveDesignation").click(function () {
       
        var errorMsg = "";
        errorMsg += $("#designationName").val() == "" ? "Designation name required.<br>" : "";
        if (errorMsg== "")
        {
            var designationToSave = {
                designationName: $("#designationName").val(),
                advanceAmount: $("#advancePayment").val(),
                leaveDays: $("#clInMonth").val(),
                narration: $("#narration").val()
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/Designation/AddDesignation",
                type: 'POST',
                data: JSON.stringify(designationToSave),
                contentType: "application/json",
                success: function (data) {
                    table.destroy();
                    getDesignations();
                    Utilities.SuccessNotification("Designation Saved");
                    $("#designationName").val("");
                    $("#advancePayment").val(0);
                    $("#clInMonth").val(0);
                    $("#narration").val("");
                },
                error: function (request, error) {
                    Utilities.Loader.Hide();
                    Utilities.ErrorNotification("Something went wrong!");
                }
            });
        }
        else
        {
            Utilities.ErrorNotification(errorMsg);
        }
        
    });

    $("#saveChanges").click(function () {
        var errorMsg = "";
        errorMsg += $("#editDesignationName").val() == "" ? "Designation name required.<br>" : "";
        if (errorMsg == "")
        {
            var toEdit = {
                designationId: designationToEdit.designationId,
                designationName: $("#editDesignationName").val(),
                advanceAmount: $("#editAdvancePayment").val(),
                leaveDays: $("#editClInMonth").val(),
                narration: $("#editNarration").val()
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/Designation/EditDesignation",
                type: 'POST',
                data: JSON.stringify(toEdit),
                contentType: "application/json",
                success: function (data) {
                    table.destroy();
                    getDesignations();
                    Utilities.SuccessNotification("Changes Saved!");
                    $("#editDesignationName").val("");
                    $("#editAdvancePayment").val(0);
                    $("#editClInMonth").val(0);
                    $("#editNarration").val("");
                    $('#editModal').modal('toggle');
                },
                error: function (request, error) {
                    Utilities.Loader.Hide();
                    Utilities.ErrorNotification("Something went wrong!");
                }
            });
        }
        else {
            Utilities.ErrorNotification(errorMsg);
        }
    });
});

function getDesignations() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Designation/GetDesignations",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (request, error) {
            Utilities.Loader.Hide();
        }
    });
}

function onSuccess(data) {
    var objToShow = [];
    designations = data;
    $.each(data, function (i, record) {
        objToShow.push([
            (i + 1),
            record.designationName,
            '<div class="btn-group btn-group-sm designationAction"><div class="dropdown"><button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button><ul class="dropdown-menu"><li class="btnEditModal editDesignation" id="' + record.designationId + '"><a><i class="fa fa-edit"></i> Edit</a></li><li class="btnDeleteModal deleteDesignation" id="' + record.designationId + '"><a><i class="fa fa-times"></i> Delete</a></li></ul></div></div>'
        ]);
    });
   
    table = $('#designationTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

    var count = 0;
    if (checkPriviledge("frmDesignation", "Update") === false) {
        $(".editDesignation").hide();
        count++;
    }
    if (checkPriviledge("frmDesignation", "Delete") === false) {
        $(".deleteDesignation").hide();
        count++;
    }
    if (count === 2) {
        $(".designationAction").hide();
    }
    Utilities.Loader.Hide();
}

function findDesignation(id) {
    var designation = "";
    for (i = 0; i < designations.length; i++) {
        if (designations[i].designationId == id) {
            designation = designations[i];
        }
    }
    return designation;
}