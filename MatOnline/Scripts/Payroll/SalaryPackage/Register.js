﻿var salaryPackageRegister = [];
var thisSalaryPackage = {};
var table = "";

$(function () {
    getSalaryPackages();
});

$("#search").click(getSalaryPackageRegister);

//============= API CALLS===========
function getSalaryPackageRegister()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalaryPackage/SalaryPackageRegister?packageName=" + $("#salaryPackage").val() + "&status=" + $("#status").val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            salaryPackageRegister = data.Response;
            // renderEmployeeDetailsForView();
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(renderSalaryPackage, 500, data);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function deleteSalaryPackage(id) {
    if (confirm("Are you sure you want to delete this Salary Package")) {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/SalaryPackage/DeleteSalaryPackage?salaryPackageId=" + id,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Salary Package Deleted Successfully.");
                    getSalaryPackages();
                }
                else {
                    Utilities.ErrorNotification(data.ResponseMessage);
                }
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}

function getSalaryPackages() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalaryPackage/GetSalaryPackage",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            salaryPackages = data.Response;
            // renderEmployeeDetailsForView();
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(renderSalaryPackages, 500, data);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

// =============  RENDER DATA TO CONTROLS ============
function renderSalaryPackages() {
    var objToShow = [];
    $.each(salaryPackages, function (i, record) {
        objToShow.push([
            (i + 1),
            record.salaryPackageName,
            record.isActive == 1 ? "Yes" : "No",
            "₦ " + Utilities.FormatCurrency(record.totalAmount),
            '<div class="btn-group btn-group-sm salaryPackageAction"><div class="dropdown"><button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button><ul class="dropdown-menu"><li class="btnEditModal editSalaryPackage" id="' + record.salaryPackageId + '"><a href="/Payroll/SalaryPackage/Edit/' + record.salaryPackageId + '"><i class="fa fa-edit"></i> Edit</a></li><li class="btnDeleteModal deleteSalaryPackage" onclick="deleteSalaryPackage(' + record.salaryPackageId + ')"><a><i class="fa fa-times"></i> Delete</a></li></ul></div></div>'
        ]);
    });
   
    table = $('#salaryPackageRegisterTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    table.destroy();

    var count = 0;
    if (checkPriviledge("frmSalaryPackageCreation", "Update") === false) {
        $(".editSalaryPackage").hide();
        count++;
    }
    if (checkPriviledge("frmSalaryPackageCreation", "Delete") === false) {
        $(".deleteSalaryPackage").hide();
        count++;
    }
    if (count === 2) {
        $(".salaryPackageAction").hide();
    }
}
// =============  RENDER DATA TO CONTROLS ============
function renderSalaryPackage()
{
    var objToShow = [];
    $.each(salaryPackageRegister, function (i, record) {
        objToShow.push([
            (i + 1),
            record.salaryPackageName,
            record.isActive == 1 ? "Yes" : "No",
            "₦ " + Utilities.FormatCurrency(record.totalAmount),
            '<div class="btn-group btn-group-sm salaryPackageAction"><div class="dropdown"><button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button><ul class="dropdown-menu"><li class="btnEditModal editSalaryPackage" id="' + record.salaryPackageId + '"><a><i class="fa fa-edit"></i> Edit</a></li><li class="btnDeleteModal deleteSalaryPackage"  onclick="deleteSalaryPackage(' + record.salaryPackageId + ')"><a><i class="fa fa-times"></i> Delete</a></li></ul></div></div>'
        ]);
    });

    table = $('#salaryPackageRegisterTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    table.destroy();

    var count = 0;
    if (checkPriviledge("frmSalaryPackageCreation", "Update") === false) {
        $(".editSalaryPackage").hide();
        count++;
    }
    if (checkPriviledge("frmSalaryPackageCreation", "Delete") === false) {
        $(".deleteSalaryPackage").hide();
        count++;
    }
    if (count === 2) {
        $(".salaryPackageAction").hide();
    }
}