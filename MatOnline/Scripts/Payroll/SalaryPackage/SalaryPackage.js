﻿var payElements = [];
var newSalaryPackageMaster = {};
var newSalaryPackage = {};
var payElementsForSalary = [];

var table = "";
var totalAmount = 0;

$(function () {
    $("#successNotification").hide();
    $("#errorNotification").hide();

    getLookUps();
});


/* ======= API CALLS ======= */
function getLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalaryPackage/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            payElements = data.Response.PayElements;

            renderLookUpDataToControls();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function saveSalaryPackage()
{
    if ($("#packageName").val() == "" || $("#packageName").val() == null)
    {
        Utilities.ErrorNotification("Please enter Salary Package Name");
    }
    else
    {
        newSalaryPackageMaster.salaryPackageName = $("#packageName").val();
        newSalaryPackageMaster.isActive = $("#isActive").val();
        newSalaryPackageMaster.narration = $("#narration").val();
        newSalaryPackageMaster.totalAmount = calculateTotalAmount();
        newSalaryPackageMaster.extraDate = new Date();
        newSalaryPackageMaster.extra1 = "";
        newSalaryPackageMaster.extra2 = "";

        newSalaryPackage.master = newSalaryPackageMaster;
        newSalaryPackage.details = payElementsForSalary;

        console.log(newSalaryPackage);

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/SalaryPackage/AddSalaryPackage",
            type: "POST",
            data: JSON.stringify(newSalaryPackage),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200)
                {
                    Utilities.SuccessNotification("Salary Package saved succeessfully");
                    window.location = "/Payroll/SalaryPackage/Index";
                    Utilities.Loader.Hide();
                }
                else
                {
                    Utilities.SuccessNotification(data.ResponseMessage);
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}

/* ======= RENDER DATA TO CONTOLS ======= */
function renderLookUpDataToControls()
{
    $("#payHead").kendoDropDownList({
        filter: "contains",
        dataTextField: "payHeadName",
        dataValueField: "payHeadId",
        dataSource: payElements
    });
}
function renderPayElementsToTable()
{
    var output = "";
    var objToShow = [];

    if (table != "")
    {
        table.destroy();
    }
    $.each(payElementsForSalary, function (count, row) {
        objToShow.push([
            count + 1,
            findPayElement(row.payHeadId).payHeadName,
            row.type,
            '&#8358;' + Utilities.FormatCurrency(parseFloat(row.amount)),
            '<button type="button" class="btn btn-danger btn-sm" onclick="removeFromPayElement(' + row.payHeadId + ')"><i class="fa fa-trash"></i></button>'
        ]);
    });
    table = $('#payElementsTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    console.log(calculateTotalAmount());
    $("#totalAmount").val(Utilities.FormatCurrency(calculateTotalAmount()));
}


function addNewPayElement()
{
    $("#newPayElementModal").modal("toggle");
    $('#newPayElementModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
}
function addNewPayElementToObject()
{
    var indexToCheckForReference = payElementsForSalary.findIndex(p=>p.payHeadId == $("#payHead").val());

    if ($("#amount").val() == 0 || $("#amount").val() == "" || $("#amount").val() == null)
    {
        $("#errorNotification").html("Enter a valid Amount");
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 4000);
    }
    else if (indexToCheckForReference > -1)
    {
        $("#errorNotification").html("This Pay Element as been Added already");
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 4000);
    }
    else {
        payElementsForSalary.push({
            payHeadId: $("#payHead").val(),
            type: findPayElement($("#payHead").val()).type,
            amount: $("#amount").val(),
            narration: $("#narration").val(),
            extraDate: new Date(),
            extra1: "",
            extra2: ""
        });
        $("#successNotification").html("added successfully");
        $("#successNotification").show();
        setTimeout(function () {
            $("#successNotification").hide();
        }, 4000);
        renderPayElementsToTable();
    }
    console.log(payElementsForSalary);
}
function removeFromPayElement(id)
{
    if (confirm("Remove this Element?")) {
        ccccc
    }
}

function calculateTotalAmount()
{
    totalAmount = 0;
    for (i = 0; i < payElementsForSalary.length; i++) {
        totalAmount = parseFloat(totalAmount) + parseFloat(payElementsForSalary[i].amount);
    }
    return totalAmount;
}
function findPayElement(id)
{
    var output = {};
    for (i = 0; i < payElements.length; i++)
    {
        if (payElements[i].payHeadId == id)
        {
            output = payElements[i];
            break;
        }
    }
    return output;
}