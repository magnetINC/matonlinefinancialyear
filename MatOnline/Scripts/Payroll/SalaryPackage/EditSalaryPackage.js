﻿var payElements = [];
var newSalaryPackageMaster = {};
var newSalaryPackage = {};
var payElementsForSalary = [];
var totalLineItemAmount = 0.0;
var table = "";
var totalAmount = 0;

$(function () {
    $("#successNotification").hide();
    $("#errorNotification").hide();

    //getLookUps();
    GetRecords();
});


/* ======= API CALLS ======= */
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalaryPackage/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log("data", data);
            payElements = data.Response.PayElements;

            renderLookUpDataToControls();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

var master = {};

function GetRecords() {
    var id = $("#masterId").val();
    $.ajax(API_BASE_URL + "/SalaryPackage/GetSalaryPackage?salaryPackageId=" + id,
        {
            success: function (res) {
                master = res.Response.master;
                details = res.Response.details;
                salaryPackagePayHeadItems = [];
                var sl = 0;
                if (details != null) {
                    for (var i of details) {
                        sl += 1;
                        salaryPackagePayHeadItems.push({
                            SLNo: sl,
                            payHeadId: i.payHeadId,
                            Amount: i.Amount,
                        });
                    }
                }



                $("#packageName").val(master.SalaryPackageName);
                //$("isActive").val();
                $("#narration").val(master.Narration);
                renderPayHeadItems();
            }
        });
}

function renderPayHeadItems() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/SalaryPackage/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log("data", data);
            payElements = data.Response.PayElements;

            renderLookUpDataToControls();
            totalLineItemAmount = 0.0;
            var output = "";
            console.log(salaryPackagePayHeadItems);
            $.each(salaryPackagePayHeadItems, function (count, row) {
                var payHeadObj = payElements.find(p => p.payHeadId == row.payHeadId);
                var payHead = payHeadObj == undefined ? "NA" : payHeadObj.payHeadName;
                var type = payHeadObj == undefined ? "NA" : payHeadObj.type;
                output +=
                    '<tr>\
                <td style="white-space: nowrap;"><button onclick="confirmDelete(' + count + ')" class="btn btn-sm btn-danger"><i class="fa fa-times"></i></button></>\
                <td style="white-space: nowrap;">' + payHead + '</td>\
                <td style="white-space: nowrap;">' + type + '</td>\
                 <td style="white-space: nowrap;">' + row.Amount + '</td>\
            </tr>\
            ';
                // <td style="white-space: nowrap;">&#8358;' + Utilities.FormatCurrency(totAmount) + '</td>\
                //console.log(row.Amount);
                //totalLineItemAmount = (totalLineItemAmount + parseFloat(row.Amount));
                $("#totalAmount").val(Utilities.FormatCurrency(calculateTotalAmount()));
            });

            //$("#totalAmount").val(Utilities.FormatCurrency(totalLineItemAmount));


            $("#salaryPackagePayHeadItemTbody").html(output);
            //$("#totalTaxAmount").val(Utilities.FormatCurrency(getTotalTaxAmount()));

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });

}

function removeLineItem(payHeadId) {
    if (confirm("Remove this item?")) {
        var indexOfObjectToRemove = salaryPackagePayHeadItems.findIndex(p => p.payHeadId == payHeadId);
        salaryPackagePayHeadItems.splice(indexOfObjectToRemove, 1);
        renderPayHeadItems();
    }
}

function removeLineItem() {
    salaryPackagePayHeadItems.splice(deleteIndex, 1);
    renderPayHeadItems();
}

var options = {
    message: "Are you sure you want to proceed with the deletion?",
    title: 'Confirm Delete',
    size: 'sm',
    subtitle: 'smaller text header',
    label: "Yes"   // use the positive lable as key
    //...
};


function confirmDelete(index) {
    deleteIndex = index;
    eModal.confirm(options).then(removeLineItem);
}



function saveEdit() {
    if ($("#packageName").val() == "" || $("#packageName").val() == null) {
        Utilities.ErrorNotification("Please enter Salary Package Name");
    }
    else {
        newSalaryPackageMaster.salaryPackageName = $("#packageName").val();
        newSalaryPackageMaster.salaryPackageId = $("#masterId").val();
        newSalaryPackageMaster.isActive = $("#isActive").val();
        newSalaryPackageMaster.narration = $("#narration").val();
        newSalaryPackageMaster.totalAmount = calculateTotalAmount();
        newSalaryPackageMaster.extraDate = new Date();
        newSalaryPackageMaster.extra1 = "";
        newSalaryPackageMaster.extra2 = "";

        newSalaryPackage.master = newSalaryPackageMaster;
        newSalaryPackage.details = salaryPackagePayHeadItems;

        console.log(newSalaryPackage);

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/SalaryPackage/UpdateSalaryPackage",
            type: "POST",
            data: JSON.stringify(newSalaryPackage),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Salary Package updated succeessfully");
                    window.location = "/Payroll/SalaryPackage/Register";
                    Utilities.Loader.Hide();
                }
                else {
                    Utilities.SuccessNotification(data.ResponseMessage);
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}



/* ======= RENDER DATA TO CONTOLS ======= */
function renderLookUpDataToControls() {
    $("#payHead").kendoDropDownList({
        filter: "contains",
        dataTextField: "payHeadName",
        dataValueField: "payHeadId",
        dataSource: payElements
    });
}


function addNewPayElement() {
    $("#newPayElementModal").modal("toggle");
    $('#newPayElementModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
}
function addNewPayElementToObject() {
    var indexToCheckForReference = salaryPackagePayHeadItems.findIndex(p => p.payHeadId == $("#payHead").val());

    if ($("#amount").val() == 0 || $("#amount").val() == "" || $("#amount").val() == null) {
        $("#errorNotification").html("Enter a valid Amount");
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 4000);
    }
    else if (indexToCheckForReference > -1) {
        $("#errorNotification").html("This Pay Element as been Added already");
        $("#errorNotification").show();
        setTimeout(function () {
            $("#errorNotification").hide();
        }, 4000);
    }
    else {
        salaryPackagePayHeadItems.push({
            payHeadId: $("#payHead").val(),
            type: findPayElement($("#payHead").val()).type,
            Amount: $("#amount").val(),
            Narration: $("#narration").val(),
            ExtraDate: new Date(),
            Extra1: "",
            Extra2: ""
        });
        $("#successNotification").html("added successfully");
        $("#successNotification").show();
        setTimeout(function () {
            $("#successNotification").hide();
        }, 4000);
        renderPayHeadItems();
        //renderPayElementsToTable();
    }
    //console.log(salaryPackagePayHeadItems);
}
function removeFromPayElement(id) {
    if (confirm("Remove this Element?")) {
        ccccc
    }
}

function calculateTotalAmount() {
    totalAmount = 0;
    for (i = 0; i < salaryPackagePayHeadItems.length; i++) {
        totalAmount = parseFloat(totalAmount) + parseFloat(salaryPackagePayHeadItems[i].Amount);
    }
    return totalAmount;
}
function findPayElement(id) {
    var output = {};
    for (i = 0; i < payElements.length; i++) {
        if (payElements[i].payHeadId == id) {
            output = payElements[i];
            break;
        }
    }
    return output;
}