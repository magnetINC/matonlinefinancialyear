﻿var payElements = [];
var accountLedgers = [];
var accountGroup = [];
var payElementToEdit = "";
var table = "";

//wherever table.destroy() is called, its because it table can't be reinitialized so for new data to refelect, old table is destroyed
//and new one is created


$(function () {

    getPayElements();
    $("#payElementTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        payElementToEdit = findPayElement(id);
        var selectedLedger = findLedger(payElementToEdit.ReceivingLedgerId); console.log(selectedLedger.ledgerName);
        $("#editPayElementName").val(payElementToEdit.payHeadName);
        $("#editNarration").val(payElementToEdit.narration);
        //$("#editLedger").val(selectedLedger.ledgerId);
        //$("#editLedger").text(selectedLedger.ledgerName);
        $("#editType").val(payElementToEdit.type);
        $('#editModal').modal('toggle');
    });

    $("#payElementTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        if(Utilities.Confirm("Delete this record?"))
        {
            var payElementToDelete = {
                payHeadId: id
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/PayElement/DeletePayElement",
                type: 'POST',
                data: JSON.stringify(payElementToDelete),
                contentType: "application/json",
                success: function (data) {
                    table.destroy();
                    getPayElements();
                },
                error: function (request, error) {
                    Utilities.Loader.Hide();
                }
            });
        }
    });

    $("#savePayElement").click(function () {
       
        var errorMsg = "";
        errorMsg += $("#payElementName").val() == "" ? "Pay element name required.<br>" : "";
        errorMsg += $("#ledger").val() == "" ? "Recieving ledger required.<br>" : "";
        errorMsg += $("#type").val() == "" ? "Pay element type required.<br>" : "";
        if (errorMsg== "")
        {
            var payElementToSave = {
                payHeadName: $("#payElementName").val(),
                ReceivingLedgerId: $("#ledger").val(),
                type: $("#type").val(),
                narration: $("#narration").val()
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/PayElement/AddPayElement",
                type: 'POST',
                data: JSON.stringify(payElementToSave),
                contentType: "application/json",
                success: function (data) {
                    table.destroy();
                    getPayElements();
                    Utilities.SuccessNotification("Pay Element Saved");
                    $("#payElementName").val("");
                    $("#narration").val("");
                },
                error: function (request, error) {
                    Utilities.Loader.Hide();
                    Utilities.ErrorNotification("Something went wrong!");
                }
            });
        }
        else
        {
            Utilities.ErrorNotification(errorMsg);
        }
        
    });

    $("#saveChanges").click(function () {
        var errorMsg = "";
        errorMsg += $("#editPayElementName").val() == "" ? "Pay element name required.<br>" : "";
        errorMsg += $("#editLedger").val() == "" ? "Recieving ledger required.<br>" : "";
        errorMsg += $("#editType").val() == "" ? "Pay element type required.<br>" : "";
        if (errorMsg == "") {
            var toEdit = {
                payHeadId:payElementToEdit.payHeadId,
                payHeadName: $("#editPayElementName").val(),
                ReceivingLedgerId: $("#editLedger").val(),
                type: $("#editType").val(),
                narration: $("#editNarration").val()
            };
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/PayElement/EditPayElement",
                type: 'POST',
                data: JSON.stringify(toEdit),
                contentType: "application/json",
                success: function (data) {
                    table.destroy();
                    getPayElements();
                    Utilities.SuccessNotification("Changes saved!");
                    $("#editPayElementName").val("");
                    $("#editNarration").val("");
                },
                error: function (request, error) {
                    Utilities.Loader.Hide();
                    Utilities.ErrorNotification("Something went wrong!");
                }
            });
        }
        else {
            Utilities.ErrorNotification(errorMsg);
        }
    });
});

function getPayElements() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PayElement/GetPayElements",
        type: 'GET',
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(onSuccess, 500, data);
        },
        error: function (request, error) {
            Utilities.Loader.Hide();
        }
    });
}

function onSuccess(data) {
    payElements = data.PayElement;
    accountLedgers = data.AccountLedger;
    accountGroup = data.AccountGroup;
    var objToShow = [];
    var ledgerHtml = "";

    $.each(payElements, function (i, record) {
        var action = ["Basic Salary", "Housing Allowance", "Transport Allowance"].includes(record.payHeadName) ?
            "" : '<div class="btn-group btn-group-sm payElementAction"><div class="dropdown"><button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button><ul class="dropdown-menu"><li class="btnEditModal editPayElement" id="'
            + record.payHeadId + '"><a><i class="fa fa-edit"></i> Edit</a></li><li class="btnDeleteModal deletePayElement" id="'
            + record.payHeadId + '"><a><i class="fa fa-times"></i> Delete</a></li></ul></div></div>'; 
        objToShow.push([
            (i + 1),
            record.payHeadName,
            action
        ]);
    });
    $.each(accountLedgers, function (i, record) {
        //populate ledger combobox
        var groupObj = accountGroup.find(x => x.accountGroupId == record.accountGroupId);

        if (groupObj.nature == "Expenses") {

            ledgerHtml += '<option value="' + record.ledgerId + '">' + record.ledgerName + '</option>';
        }

    });
    $("#ledger").html(ledgerHtml);
    $('#ledger').chosen({ width: '95%' });

    $("#editLedger").html(ledgerHtml);
    $('#editLedger').chosen({ width: '95%' });

    table = $('#payElementTable').DataTable({
        data: objToShow,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

    var count = 0;
    if (checkPriviledge("frmPayElement", "Update") === false) {
        $(".editPayElement").hide();
        count++;
    }
    if (checkPriviledge("frmPayElement", "Delete") === false) {
        $(".deletePayElement").hide();
        count++;
    }
    if (count === 2) {
        $(".payElementAction").hide();
    }

    Utilities.Loader.Hide();
}

function findPayElement(id) {
    var payElement = "";
    for (i = 0; i < payElements.length; i++) {
        if (payElements[i].payHeadId == id) {
            payElement = payElements[i];
        }
    }
    return payElement;
}

function findLedger(id) {
    var ledger = "";
    for (i = 0; i < accountLedgers.length; i++) {
        if (accountLedgers[i].ledgerId == id) {
            ledger = accountLedgers[i];
        }
    }
    return ledger;
}