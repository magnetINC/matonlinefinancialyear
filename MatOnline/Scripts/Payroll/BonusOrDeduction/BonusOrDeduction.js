﻿var newBonuaOrDeduction = {};

var accounts = [];
var employees = [];

$(function () {
    getLookUps();
});


// ========== API CALLS =========
function getLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/BonusOrDeduction/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            accounts = data.Response.accounts;
            employees = data.Response.employees;

            renderLookUpDataToControls();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function saveBonusOrDeduction()
{
    if ($("#bonusAmount").val() == "" || $("#deductionAmount").val() == "")
    {
        Utilities.ErrorNotification("Please Enter an amount for bonus or deduction");
    }
    else {
        newBonuaOrDeduction.EmployeeId = $("#employeeCode").val();
        newBonuaOrDeduction.Date = $("#date").val();
        newBonuaOrDeduction.Month = $("#month").val();
        newBonuaOrDeduction.BonusAmount = $("#bonusAmount").val();
        newBonuaOrDeduction.DeductionAmount = $("#deductionAmount").val();
        newBonuaOrDeduction.Narration = $("#narration").val();
        newBonuaOrDeduction.ReceivingLedgerId = $("#receivingLedger").val();
        newBonuaOrDeduction.ExtraDate = new Date();
        newBonuaOrDeduction.Extra1 = "";
        newBonuaOrDeduction.Extra2 = "";
        console.log(newBonuaOrDeduction);

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/BonusOrDeduction/SaveBonusOrDeduction",
            type: "POST",
            data: JSON.stringify(newBonuaOrDeduction),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Bonus/Deduction saved succeessfully");
                    window.location = "/Payroll/BonusOrDeduction/Index";
                    Utilities.Loader.Hide();
                }
                else {
                    Utilities.SuccessNotification(data.ResponseMessage);
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}

/* ======= RENDER DATA TO CONTOLS ======= */
function renderLookUpDataToControls() {
    $("#employeeCode").kendoDropDownList({
        filter: "contains",
        dataTextField: "employeeName",
        dataValueField: "employeeId",
        dataSource: employees,
        optionLabel: "Please Select..."
    });
    $("#receivingLedger").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: accounts,
        optionLabel: "Please Select..."
    });
}