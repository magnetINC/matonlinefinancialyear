﻿var employees = [];
var designation = [];
var salaryPackages = [];

var employeeDetails = {};
var saveChanges = {};

var table = "";
var thisEmployee = 0;

$(function () {
    getLookUps();
});
// ========== API CALLS =========
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Employee/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            salaryPackages = data.Response.salaryPackages;
            designation = data.Response.designation;
            employees = data.Response.employeesList;

            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(renderEmployeesForView, 500, data);

            renderLookUpDataToControls();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getEmployeeDetails(id)
{
    thisEmployee = id;
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Employee/ViewEmployeeDetails?employeeId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            employeeDetails = data.Response;
            $("#detailsModal").modal("show");
            renderEmployeeDetailsForView();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function saveChange()
{
    if ($("#employeeName").val() == "") {
        Utilities.ErrorNotification("Please enter employee name");
    }
    else if ($("#employeeCode").val() == "") {
        Utilities.ErrorNotification("Please enter employee code");
    }
    else if ($("#designation").val() == "") {
        Utilities.ErrorNotification("Please select employee designation");
    }
    else if ($("#salaryType").val() == "daily" && $("#dailySalary").val() == "") {
        Utilities.ErrorNotification("Please enter a valid daily salary");
    }
    else {
        addEmployeeToObject();

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/Employee/UpdateEmployee",
            type: "POST",
            data: JSON.stringify(saveChanges),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Employee saved succeessfully");
                    window.location = "/Payroll/Employee/Register";
                    Utilities.Loader.Hide();
                }
                else {
                    Utilities.Loader.Hide();
                    Utilities.SuccessNotification(data.ResponseMessage);
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}
function deleteEmployee(id) {
    if (confirm("Are you sure you want to delete this Employee")) {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/Employee/DeleteEmployee?employeeId=" + id,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Employee Deleted Successfully.");
                    getLookUps();
                }
                else {
                    Utilities.ErrorNotification(data.ResponseMessage);
                }
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}


//=========== RENDER DATA TO CONTROLS ==========
function renderLookUpDataToControls() {
    $("#salaryPackage").kendoDropDownList({
        filter: "contains",
        dataTextField: "salaryPackageName",
        dataValueField: "salaryPackageId",
        dataSource: salaryPackages,
        optionLabel: "Please Select..."
    });
    $("#designation").kendoDropDownList({
        filter: "contains",
        dataTextField: "designationName",
        dataValueField: "designationId",
        dataSource: designation,
        optionLabel: "Please Select..."
    });
}
function renderEmployeesForView() {
    var output = "";
    var genderOutput = "";
    var salPackageOutput = "";
    var salTypeOutput = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }
    $.each(employees, function (count, row) {
        if (row.gender == "Male") {
            genderOutput = '<span class="label label-info">Male</span>';
        }
        else if (row.gender == "Female") {
            genderOutput = '<span class="label label-pink">Female</span>';
        }
        if (row.salaryType == "Monthly")
        {
            salTypeOutput = '<span class="label label-default">Monthly Salary</span>';
            salPackageOutput = findSalaryPackage(row.defaultPackageId).salaryPackageName;
        }
        else
        {
            salTypeOutput = '<span class="label label-primary">Daily Wage</span>';
            salPackageOutput = 'NA';
        }
        objToShow.push([
            count + 1,
            row.employeeCode,
            row.employeeName,
            findDesignation(row.designationId).designationName,
            salPackageOutput,
            salTypeOutput,
            genderOutput,
            '<div class="btn-group btn-group-sm employeeAction">\
                <div class="dropdown">\
                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button>\
                    <ul class="dropdown-menu">\
                        <li class="viewEmployee" onclick="getEmployeeDetails(' + row.employeeId + ')"><a>View Details</a></li>\
                        <li class="deleteEmployee" onclick="deleteEmployee(' + row.employeeId + ')"><a>Delete</a></li>\
                    </ul>\
                </div>\
            </div>'
        ]);
    });
    table = $('#employeeListTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    var count = 0;
    if (checkPriviledge("frmEmployeeCreation", "View") === false) {
        $(".viewEmployee").hide();
        count++;
    }
    if (checkPriviledge("frmEmployeeCreation", "Delete") === false) {
        $(".deleteEmployee").hide();
        count++;
    }
    if (checkPriviledge("frmEmployeeCreation", "Update") === false) {
        $(".editEmployee").hide();
    }
    if (count === 2) {
        $(".employeeAction").hide();
    }
}

function renderEmployeeDetailsForView()
{
    $("#saveChanges").hide();

    $('#detailsModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
    $('#detailsBody').find(':input').prop('disabled', true);
    //e.preventDefault();

    var dropdownlistSP = $("#salaryPackage").data("kendoDropDownList");
    dropdownlistSP.value(employeeDetails.DefaultPackageId);
    var dropdownlistDesg = $("#designation").data("kendoDropDownList");
    dropdownlistDesg.value(employeeDetails.DesignationId);

    //$("#salaryPackage").val(employeeDetails.DefaultPackageId);
    //$("#designation").val(employeeDetails.DesignationId);
    $("#employeeName").val(employeeDetails.EmployeeName);
    $("#employeeCode").val(employeeDetails.EmployeeCode);
    $("#dob").val(Utilities.FormatJsonDate(employeeDetails.Dob));
    $("#maritalStatus").val(employeeDetails.MaritalStatus);
    $("#gender").val(employeeDetails.Gender);
    $("#qualification").val(employeeDetails.Qualification);
    $("#address").val(employeeDetails.Address);
    $("#phoneNumber").val(employeeDetails.PhoneNumber);
    $("#mobileNumber").val(employeeDetails.MobileNumber);
    $("#email").val(employeeDetails.Email);
    $("#joinDate").val(Utilities.FormatJsonDate(employeeDetails.JoiningDate));
    $("#terminationDate").val(Utilities.FormatJsonDate(employeeDetails.TerminationDate));
    $("#narration").val(employeeDetails.Narration);
    $("#bloodGroup").val(employeeDetails.BloodGroup);
    $("#passportNumber").val(employeeDetails.PassportNo);
    $("#PExpiry").val(Utilities.FormatJsonDate(employeeDetails.PassportExpiryDate));
    $("#workPermitNumber").val(employeeDetails.LabourCardNumber);
    $("#WPExpiry").val(Utilities.FormatJsonDate(employeeDetails.LabourCardExpiryDate));
    $("#visaNumber").val(employeeDetails.VisaNumber);
    $("#VExpiry").val(Utilities.FormatJsonDate(employeeDetails.VisaExpiryDate));
    $("#salaryType").val(employeeDetails.SalaryType);
    $("#dailySalary").val(employeeDetails.DailyWage);
    $("#bankName").val(employeeDetails.BankName);
    $("#branch").val(employeeDetails.BranchName);
    $("#accountNumber").val(employeeDetails.BankAccountNumber);
    $("#branchCode").val(employeeDetails.BranchCode);
    $("#pensionNumber").val(employeeDetails.PanNumber);
    $("#pfaNumber").val(employeeDetails.PfNumber);
    $("#esiNumber").val(employeeDetails.EsiNumber);
}

function editEmployee()
{
    $('#detailsBody').find(':input').prop('disabled', false);
    $("#editEmployee").hide();
    $("#saveChanges").show();

}
function toDelete()
{
    deleteEmployee(thisEmployee);
}

function findDesignation(id) {
    var output = {};
    for (i = 0; i < designation.length; i++) {
        if (designation[i].designationId == id) {
            output = designation[i];
            break;
        }
    }
    return output;
}
function findSalaryPackage(id) {
    var output = {};
    for (i = 0; i < salaryPackages.length; i++) {
        if (salaryPackages[i].salaryPackageId == id) {
            output = salaryPackages[i];
            break;
        }
    }
    return output;
}
function addEmployeeToObject() {
    saveChanges.EmployeeId = thisEmployee;
    saveChanges.DefaultPackageId = $("#salaryPackage").val();
    saveChanges.DesignationId = $("#designation").val();
    saveChanges.EmployeeName = $("#employeeName").val();
    saveChanges.EmployeeCode = $("#employeeCode").val();
    saveChanges.Dob = $("#dob").val();
    saveChanges.MaritalStatus = $("#maritalStatus").val();
    saveChanges.Gender = $("#gender").val();
    saveChanges.Qualification = $("#qualification").val();
    saveChanges.Address = $("#address").val();
    saveChanges.PhoneNumber = $("#phoneNumber").val();
    saveChanges.MobileNumber = $("#mobileNumber").val();
    saveChanges.Email = $("#email").val();
    saveChanges.JoiningDate = $("#joinDate").val();
    saveChanges.TerminationDate = $("#terminationDate").val();
    saveChanges.IsActive = true;
    saveChanges.Narration = "";
    saveChanges.BloodGroup = $("#bloodGroup").val();
    saveChanges.PassportNo = $("#passportNumber").val();
    saveChanges.PassportExpiryDate = $("#PExpiry").val();
    saveChanges.LabourCardNumber = $("#workPermitNumber").val();
    saveChanges.LabourCardExpiryDate = $("#WPExpiry").val();
    saveChanges.VisaNumber = $("#visaNumber").val();
    saveChanges.VisaExpiryDate = $("#VExpiry").val();
    saveChanges.SalaryType = $("#salaryType").val();
    saveChanges.DailyWage = $("#dailySalary").val();
    saveChanges.BankName = $("#bankName").val();
    saveChanges.BranchName = $("#branch").val();
    saveChanges.BankAccountNumber = $("#accountNumber").val();
    saveChanges.BranchCode = $("#branchCode").val();
    saveChanges.PanNumber = $("#pensionNumber").val();
    saveChanges.PfNumber = $("#pfaNumber").val();
    saveChanges.EsiNumber = $("#esiNumber").val();
    saveChanges.ExtraDate = new Date();
    saveChanges.Extra1 = "";
    saveChanges.Extra2 = "";
    saveChanges.ExtraDate = new Date();
    saveChanges.Extra1 = "";
    saveChanges.Extra2 = "";

    //renderDataForConfirmation();
}