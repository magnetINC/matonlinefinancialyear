﻿var newEmployee = {};
var salaryPackages = [];
var designation = [];

$(function () {
    $("#dailySalarySpan").hide();
    getLookUps();

    $("#salaryType").change(function () {
        if ($("#salaryType").val() == "monthly")
        {
            $("#dailySalarySpan").hide();
            $("#salaryPackageSpan").show();
        }
        else
        {
            $("#dailySalarySpan").show();
            $("#salaryPackageSpan").hide();
        }
    });
});


/* ======= API CALLS ======= */
function getLookUps()
{
    //debugger;
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Employee/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            salaryPackages = data.Response.salaryPackages;
            designation = data.Response.designation;

            renderLookUpDataToControls();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function _saveNewEmployee() {
    alert($("#salaryPackage").val());
}

function saveNewEmployee()
{
    if ($("#employeeName").val() == "") {
        Utilities.ErrorNotification("Please enter employee name");
    }
    else if ($("#employeeCode").val() == "") {
        Utilities.ErrorNotification("Please enter employee code");
    }
    else if ($("#designation").val() == "") {
        Utilities.ErrorNotification("Please select employee designation");
    }
    else if ($("#salaryType").val() == "daily" && $("#dailySalary").val() == "") {
        Utilities.ErrorNotification("Please enter a valid daily salary");
    }
    else if ($("#salaryPackage").val() == "") { 
        Utilities.ErrorNotification("Please Select Salary Package");
    }
    else {
        addEmployeeToObject();

        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/Employee/SaveEmployee",
            type: "POST",
            data: JSON.stringify(newEmployee),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Employee saved succeessfully");
                    window.location = "/Payroll/Employee/EmployeeCreation";
                    Utilities.Loader.Hide();
                }
                else {
                    Utilities.SuccessNotification(data.ResponseMessage);
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}


/* ======= RENDER DATA TO CONTROLS ====== */
function renderLookUpDataToControls() {
    $("#salaryPackage").kendoDropDownList({
        filter: "contains",
        dataTextField: "salaryPackageName",
        dataValueField: "salaryPackageId",
        dataSource: salaryPackages,
        optionLabel: "Please Select..."
    });
    $("#designation").kendoDropDownList({
        filter: "contains",
        dataTextField: "designationName",
        dataValueField: "designationId",
        dataSource: designation,
        optionLabel: "Please Select..."
    });
}
function renderDataForConfirmation()
{
    console.log(newEmployee);
         $("#salPackConfirm").html(newEmployee.DefaultPackageId);
         $("#desigConfirm").html(newEmployee.DesignationId);
         $("#employeeNameConfirm").html(newEmployee.EmployeeName);
         $("#employeeCodeConfirm").html(newEmployee.EmployeeCode);
         $("#dobConfirm").html(newEmployee.Dob);
         $("#maritalStatusConfirm").html(newEmployee.MaritalStatus);
         $("#genderConfirm").html(newEmployee.Gender);
         $("#qualificationConfirm").html(newEmployee.Qualification);
         $("#addressConfirm").html(newEmployee.Address);
         $("#phoneConfirm").html(newEmployee.PhoneNumber);
         $("#mobileConfirm").html(newEmployee.MobileNumber);
         $("#emailConfirm").html(newEmployee.Email);
         $("#jdConfirm").html(newEmployee.JoiningDate);
         $("#tdConfirm").html(newEmployee.TerminationDate);
         $("#narrationConfirm").html(newEmployee.Narration);
         $("#bgConfirm").html(newEmployee.BloodGroup);
         $("#passConfirm").html(newEmployee.PassportNo);
         $("#passExpConfirm").html(newEmployee.PassportExpiryDate);
         $("#workPConfirm").html(newEmployee.LabourCardNumber);
         $("#workPExpConfirm").html(newEmployee.LabourCardExpiryDate);
         $("#visaConfirm").html(newEmployee.VisaNumber);
         $("#visaExpConfirm").html(newEmployee.VisaExpiryDate);
         $("#salTypeConfirm").html(newEmployee.SalaryType);
         $("#dailySalConfirm").html(newEmployee.DailyWage);
         $("#bankNameConfirm").html(newEmployee.BankName);
         $("#branchConfirm").html(newEmployee.BranchName);
         $("#accNoConfirm").html(newEmployee.BankAccountNumber);
         $("#bcodeConfirm").html(newEmployee.BranchCode);
         $("#pensionConfirm").html(newEmployee.PanNumber);
         $("#pfaConfirm").html(newEmployee.PfNumber);
         $("#esiConfirm").html(newEmployee.EsiNumber);
}


//===> OTHER FUNCTIONS
function addEmployeeToObject()
{
    newEmployee.DefaultPackageId = $("#salaryPackage").val();
    newEmployee.DesignationId = $("#designation").val();
    newEmployee.EmployeeName = $("#employeeName").val();
    newEmployee.EmployeeCode = $("#employeeCode").val();
    newEmployee.Dob = $("#dob").val();
    newEmployee.MaritalStatus = $("#maritalStatus").val();
    newEmployee.Gender = $("#gender").val();
    newEmployee.Qualification = $("#qualification").val();
    newEmployee.Address = $("#address").val();
    newEmployee.PhoneNumber = $("#phoneNumber").val();
    newEmployee.MobileNumber = $("#mobileNumber").val();
    newEmployee.Email = $("#email").val();
    newEmployee.JoiningDate = $("#joinDate").val();
    newEmployee.TerminationDate = $("#terminationDate").val();
    newEmployee.IsActive = true;
    newEmployee.Narration = $("#narration").val();
    newEmployee.BloodGroup = $("#bloodGroup").val();
    newEmployee.PassportNo = $("#passportNumber").val();
    newEmployee.PassportExpiryDate = $("#PExpiry").val();
    newEmployee.LabourCardNumber = $("#workPermitNumber").val();
    newEmployee.LabourCardExpiryDate = $("#WPExpiry").val();
    newEmployee.VisaNumber = $("#visaNumber").val();
    newEmployee.VisaExpiryDate = $("#VExpiry").val();
    newEmployee.SalaryType = $("#salaryType").val();
    newEmployee.DailyWage = $("#dailySalary").val();
    newEmployee.BankName = $("#bankName").val();
    newEmployee.BranchName = $("#branch").val();
    newEmployee.BankAccountNumber = $("#accountNumber").val();
    newEmployee.BranchCode = $("#branchCode").val();
    newEmployee.PanNumber = $("#pensionNumber").val();
    newEmployee.PfNumber = $("#pfaNumber").val();
    newEmployee.EsiNumber = $("#esiNumber").val();
    newEmployee.ExtraDate = new Date();
    newEmployee.Extra1 = "";
    newEmployee.Extra2 = "";

    //renderDataForConfirmation();
}

//restrict phone inputs to numbers:
function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : evt.keyCode;
    if (charCode != 46 && charCode > 31
        && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

//validate email
$(function () {
    var regExp = /^([\w\.\+]{1,})([^\W])(@)([\w]{1,})(\.[\w]{1,})+$/;
    $('[type="email"]').on('keyup', function () {
        regExp.test($(this).val()) ? $('#invalidEmail').hide() : $('#invalidEmail').show();
    });
});

    
    