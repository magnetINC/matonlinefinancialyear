﻿var advancePayments = [];
var receivingLedgers = [];
var cashOrBank = [];
var employees = [];

var thisAdvancePaymentMaster = {};
var thisAdvancePaymentDetails = {};

var advancePaymentId = 0;
var table = "";

$(function () {
    getLookUps();
    $("#search").click(function () {
        getAvancePaymentsForRegister($("#month").val())
    });
});

//========== API CALLS ==========
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AdvancePayment/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            receivingLedgers = data.Response.receivingLedger;
            employees = data.Response.EmployeesList;
            cashOrBank = data.Response.cashOrBank;

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getAvancePaymentsForRegister(date)
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AdvancePayment/getAdvancePaymentsForRegister?month=" + date,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            advancePayments = data.Response;

            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(renderAdvancePaymentForView, 500);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getAdvancePaymentDetails(id) {
    advancePaymentId = id;
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AdvancePayment/getAdvancePaymentsDetailsForRegister?advancePaymentId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            thisAdvancePaymentDetails = data.Response;
            $("#detailsModal").modal("show");

            renderAdvancePaymentDetailsForView();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function deleteAdvancePayment(id) {
    $.each(advancePayments, function (count, row) {
        if (row.advancePaymentId == id) {
            thisAdvancePaymentMaster = row;
        }
    });
    if (confirm("Are you sure you want to delete this Advance Payment")) {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/AdvancePayment/DeleteAdvancePayment?apId=" + id + "&employeeId=" + thisAdvancePaymentMaster.employeeId + "&month=" + thisAdvancePaymentMaster.date + "&voucherNo=" + thisAdvancePaymentMaster.voucherNo,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Advance Payment Deleted Successfully.");
                    getAvancePaymentsForRegister($("#month").val())
                }
                else {
                    Utilities.ErrorNotification(data.ResponseMessage);
                }
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}


//=========== RENDER DATA TO CONTROLS ==========
function renderAdvancePaymentForView() {
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();    }

    $.each(advancePayments, function (count, row) {
        if (row.Status == "Pending Approval") {
            statusOutput = '<span class="label label-warning">Pending Approval</span>';
            objToShow.push([
                count + 1,
                row.VoucherNo,
                findEmployee(row.EmployeeId).employeeCode,
                findEmployee(row.EmployeeId).employeeName,
                '&#8358;' + Utilities.FormatCurrency(row.Amount),
                Utilities.FormatJsonDate(row.Date),
                Utilities.FormatJsonDate(row.SalaryMonth),
                findBank(row.LedgerId).ledgerName,
                row.Chequenumber,
                Utilities.FormatJsonDate(row.ChequeDate),
                statusOutput,
                '<div class="btn-group btn-group-sm">\
                    <div class="dropdown">\
                        <button class="btn btn-primary dropdown-toggle advancePaymentAction-One" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button>\
                        <ul class="dropdown-menu">\
                            <li class="editAdvancePayment" onclick="approveAdvancePayment(' + row.AdvancePaymentId + ')"><a>Approve Payment</a></li>\
                            <li class="editAdvancePayment" onclick="getAdvancePaymentDetails(' + row.AdvancePaymentId + ')"><a>Edit Payment</a></li>\
                            <li class="deleteAdvancePayment" onclick="deleteAdvancePayment(' + row.AdvancePaymentId + ')"><a>Cancel Payment</a></li>\
                        </ul>\
                    </div>\
                </div>'
            ]);
        }
        else if (row.Status == "Approved") {
            statusOutput = '<span class="label label-success">Approved</span>';
            objToShow.push([
                count + 1,
                row.VoucherNo,
                findEmployee(row.EmployeeId).employeeCode,
                findEmployee(row.EmployeeId).employeeName,
                '&#8358;' + Utilities.FormatCurrency(row.Amount),
                Utilities.FormatJsonDate(row.Date),
                Utilities.FormatJsonDate(row.SalaryMonth),
                findBank(row.LedgerId).ledgerName,
                row.Chequenumber,
                Utilities.FormatJsonDate(row.ChequeDate),
                statusOutput,
                '<div class="btn-group btn-group-sm">\
                    <div class="dropdown">\
                        <button class="btn btn-primary dropdown-toggle advancePaymentAction-two" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button>\
                        <ul class="dropdown-menu">\
                            <li class="deleteAdvancePayment" onclick="deleteAdvancePayment(' + row.AdvancePaymentId + ')"><a>Delete Payment</a></li>\
                        </ul>\
                    </div>\
                </div>'
            ]);
        }
    });
    table = $('#advancePaymentListTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    var count = 0;
    if (checkPriviledge("frmAdvancePayment", "Update") === false) {
        $(".editAdvancePayment").hide();
        count++;
    }
    if (checkPriviledge("frmAdvancePayment", "Delete") === false) {
        $(".advancePaymentAction-two").hide();
        $(".deleteAdvancePayment").hide();
        count++;
    }
    if (count === 2) {
        $(".advancePaymentAction-one").hide();
    }

}


function renderAdvancePaymentDetailsForView() {
    var output = "";
    output += '<tr>\
                        <td>1</td>\
                        <td>' + findEmployee(thisAdvancePaymentDetails.EmployeeId).employeeName + '</td>\
                        <td>' + thisAdvancePaymentDetails.Chequenumber + '</td>\
                        <td>' + Utilities.FormatJsonDate(thisAdvancePaymentDetails.ChequeDate) + '</td>\
                        <td>&#8358;' + Utilities.FormatCurrency(thisAdvancePaymentDetails.Amount) + '</td>\
                        <td>' + findBank(thisAdvancePaymentDetails.LedgerId).ledgerName + '</td>\
                    </tr>';
    $("#dateSpan").html(Utilities.FormatJsonDate(thisAdvancePaymentDetails.Date));
    $("#advPayNoSpan").html(thisAdvancePaymentDetails.InvoiceNo);
    $("#monthSpan").html(Utilities.FormatJsonDate(thisAdvancePaymentDetails.SalaryMonth));
    $("#narrationSpan").html(thisAdvancePaymentDetails.Narration);

    $("#detailsTbody").html(output);
}

function findEmployee(id) {
    var output = {};
    for (i = 0; i < employees.length; i++) {
        if (employees[i].employeeId == id) {
            output = employees[i];
            break;
        }
    }
    return output;
}
function findBank(id) {
    var output = {};
    for (i = 0; i < cashOrBank.length; i++) {
        if (cashOrBank[i].ledgerId == id) {
            output = cashOrBank[i];
            break;
        }
    }
    return output;
}
function findReceivingLedger(id) {
    var output = {};
    for (i = 0; i < receivingLedgers.length; i++) {
        if (receivingLedgers[i].ledgerId == id) {
            output = receivingLedgers[i];
            break;
        }
    }
    return output;
}
function toDelete() {
    deleteAdvancePayment(advancePaymentId);
    $("#detailsModal").modal("show");
}