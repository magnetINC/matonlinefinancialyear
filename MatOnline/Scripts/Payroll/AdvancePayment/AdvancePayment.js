﻿var employees = [];
var receivingLedgers = [];
var cashOrBank = [];

var infoAdvancepayment = {};

var invoiceNo = 0;

$(function () {
    getLookUps();

    $("#cashOrBank").change(function () {
        if ($("#cashOrBank").val() == 1)
        {
            $("#chqDiv").hide();

        }
        else {
            $("#chqDiv").show();
        }
    });
});

// ========== API CALLS =========
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AdvancePayment/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            receivingLedgers = data.Response.receivingLedger;
            employees = data.Response.employees;
            cashOrBank = data.Response.cashOrBank;
            invoiceNo = data.Response.InvoiceNo;

            renderLookUpDataToControls();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function saveAdvancePayment() {
    if ($("#employee").val() == "" || $("#employee").val() == 0 || $("#employee").val() == null) {
        Utilities.ErrorNotification("Please Select a Employee");
    }
    else if ($("#cashOrBank").val() == "" || $("#cashOrBank").val() == 0 || $("#cashOrBank").val() == null) {
        Utilities.ErrorNotification("Please Select a Bank or Cash Account");
    }
    else if ($("#receivingLedger").val() == "" || $("#receivingLedger").val() == 0 || $("#receivingLedger").val() == null) {
        Utilities.ErrorNotification("Please Select a Receiving Ledger");
    }
    else if ($("#month").val() == "" || $("#month").val() == 0 || $("#month").val() == null) {
        Utilities.ErrorNotification("Please Select Month");
    }
    else if ($("#amount").val() == "" || $("#amount").val() == 0 || $("#amount").val() == null) {
        Utilities.ErrorNotification("Please enter a valid amount");
    }
    else if ($("#cashOrBank").val() != 1 && $("#chequeNo").val() == "") {
        Utilities.ErrorNotification("Please enter cheque number");
    }
    else {
        infoAdvancepayment.VoucherNo = $("#advPaymentNo").val();
        infoAdvancepayment.EmployeeId = $("#employee").val();
        infoAdvancepayment.SalaryMonth = $("#month").val();
        infoAdvancepayment.Chequenumber = $("#chequeNo").val();
        infoAdvancepayment.Date = $("#date").val();
        infoAdvancepayment.Amount = $("#amount").val();
        infoAdvancepayment.InvoiceNo = $("#advPaymentNo").val();
        infoAdvancepayment.LedgerId = $("#cashOrBank").val();
        infoAdvancepayment.ChequeDate = $("#chequeDate").val();
        infoAdvancepayment.Narration = $("#narration").val();
        infoAdvancepayment.ReceivingLedgerId = $("#receivingLedger").val();

        console.log(infoAdvancepayment);
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/AdvancePayment/SaveAdvancePaymentPending",
            type: "POST",
            data: JSON.stringify(infoAdvancepayment),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification(data.ResponseMessage);
                    window.location = "/Payroll/AdvancePayment/Index";
                    Utilities.Loader.Hide();
                }
                else {
                    Utilities.ErrorNotification(data.ResponseMessage);
                    Utilities.Loader.Hide();
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}


/* ======= RENDER DATA TO CONTOLS ======= */
function renderLookUpDataToControls() {
    $("#employee").kendoDropDownList({
        filter: "contains",
        dataTextField: "employeeName",
        dataValueField: "employeeId",
        dataSource: employees,
        optionLabel: "Please Select..."
    });
    $("#receivingLedger").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: receivingLedgers,
        optionLabel: "Please Select..."
    });
    $("#cashOrBank").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: cashOrBank,
        optionLabel: "Please Select..."
    });
    $("#advPaymentNo").val(invoiceNo);
}