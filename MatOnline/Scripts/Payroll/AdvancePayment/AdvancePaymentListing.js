﻿var advancePayments = [];
var receivingLedgers = [];
var cashOrBank = [];
var employees = [];

var thisAdvancePayment = {};
//var thisAdvancePaymentDetails = {};

var advancePaymentId = 0;
var table = "";

$(function () {
    getLookUps();
    $("#search").click(function () {
        getAvancePaymentsForListing($("#month").val())
    });
});

//========== API CALLS ==========
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AdvancePayment/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            receivingLedgers = data.Response.receivingLedger;
            employees = data.Response.EmployeesList;
            cashOrBank = data.Response.cashOrBank;

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getAvancePaymentsForListing(date) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/AdvancePayment/GetAdvancePaymentsForListing?month=" + date,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            advancePayments = data.Response;

            renderAdvancePaymentForView();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function deleteAdvancePayment(id) {
    $.each(advancePayments, function (count, row) {
        if (row.advancePaymentId == id) {
            thisAdvancePaymentMaster = row;
        }
    });
    if (confirm("Are you sure you want to delete this Advance Payment")) {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/AdvancePayment/DeleteAdvancePayment?apId=" + id + "&employeeId=" + thisAdvancePaymentMaster.employeeId + "&month=" + thisAdvancePaymentMaster.date + "&voucherNo=" + thisAdvancePaymentMaster.voucherNo,
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Advance Payment Deleted Successfully.");
                    getAvancePaymentsForListing($("#month").val())
                }
                else {
                    Utilities.ErrorNotification(data.ResponseMessage);
                }
                Utilities.Loader.Hide();
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
}
function approveAdvancePayment(id)
{
    Utilities.Loader.Show();
    for (i = 0; i < advancePayments.length; i++)
    {
        if(advancePayments[i].advancePaymentId == id)
        {
            thisAdvancePayment = advancePayments[i];
            break;
        }
    }
    $.ajax({
        url: API_BASE_URL + "/AdvancePayment/SaveAdvnacePayment",
        type: "POST",
        data: JSON.stringify(thisAdvancePayment),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data.ResponseCode == 200) {
                Utilities.SuccessNotification(data.ResponseMessage);
                getAvancePaymentsForListing($("#month").val())
                Utilities.Loader.Hide();
            }
            else {
                Utilities.ErrorNotification(data.ResponseMessage);
                Utilities.Loader.Hide();
            }
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
 

//=========== RENDER DATA TO CONTROLS ==========
function renderAdvancePaymentForView() {
    var output = "";
    var objToShow = [];
    var statusOutput = "";

    if (table != "") {
        table.destroy();
    }
    $.each(advancePayments, function (count, row) {
        if (row.status == "Pending Approval") {
            statusOutput = '<span class="label label-warning">Pending Approval</span>';
            objToShow.push([
              count + 1,
              row.voucherNo,
              findEmployee(row.employeeId).employeeCode,
              findEmployee(row.employeeId).employeeName,
              '&#8358;' + Utilities.FormatCurrency(row.amount),
              Utilities.FormatJsonDate(row.date),
              Utilities.FormatJsonDate(row.salaryMonth),
              findBank(row.ledgerId).ledgerName,
              row.chequenumber,
              Utilities.FormatJsonDate(row.chequeDate),
              statusOutput,
              '<div class="btn-group btn-group-sm">\
                        <div class="dropdown">\
                            <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button>\
                            <ul class="dropdown-menu">\
                                <li onclick="approveAdvancePayment(' + row.advancePaymentId + ')"><a>Approve Payment</a></li>\
                                <li onclick="getAdvancePaymentDetails(' + row.advancePaymentId + ')"><a>Edit Payment</a></li>\
                                <li onclick="deleteAdvancePayment(' + row.advancePaymentId + ')"><a>Cancel Payment</a></li>\
                            </ul>\
                        </div>\
                    </div>'
            ]);
        }
        //else if (row.status == "Approved") {
        //    statusOutput = '<span class="label label-success">Approved</span>';
        //}
    });
    table = $('#advancePaymentListTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}


function findEmployee(id) {
    var output = {};
    for (i = 0; i < employees.length; i++) {
        if (employees[i].employeeId == id) {
            output = employees[i];
            break;
        }
    }
    return output;
}
function findBank(id) {
    var output = {};
    for (i = 0; i < cashOrBank.length; i++) {
        if (cashOrBank[i].ledgerId == id) {
            output = cashOrBank[i];
            break;
        }
    }
    return output;
}
function findReceivingLedger(id) {
    var output = {};
    for (i = 0; i < receivingLedgers.length; i++) {
        if (receivingLedgers[i].ledgerId == id) {
            output = receivingLedgers[i];
            break;
        }
    }
    return output;
}
function toDelete() {
    deleteAdvancePayment(advancePaymentId);
    $("#detailsModal").modal("show");
}




//function getAdvancePaymentDetails(id) {
//    advancePaymentId = id;
//    Utilities.Loader.Show();
//    $.ajax({
//        url: API_BASE_URL + "/AdvancePayment/GetAdvancePaymentDetailsForListing?advancePaymentId=" + id,
//        type: "GET",
//        contentType: "application/json",
//        success: function (data) {
//            console.log(data);
//            thisAdvancePaymentDetails = data.Response;
//            $("#detailsModal").modal("show");

//            renderAdvancePaymentDetailsForView();
//            Utilities.Loader.Hide();
//        },
//        error: function (err) {
//            Utilities.Loader.Hide();
//        }
//    });
//}

//function renderAdvancePaymentDetailsForView() {
//    var output = "";
//    output += '<tr>\
//                        <td>1</td>\
//                        <td>' + findEmployee(thisAdvancePaymentDetails.EmployeeId).employeeName + '</td>\
//                        <td>' + thisAdvancePaymentDetails.Chequenumber + '</td>\
//                        <td>' + Utilities.FormatJsonDate(thisAdvancePaymentDetails.ChequeDate) + '</td>\
//                        <td>&#8358;' + Utilities.FormatCurrency(thisAdvancePaymentDetails.Amount) + '</td>\
//                        <td>' + findBank(thisAdvancePaymentDetails.LedgerId).ledgerName + '</td>\
//                    </tr>';
//    $("#dateSpan").html(Utilities.FormatJsonDate(thisAdvancePaymentDetails.Date));
//    $("#advPayNoSpan").html(thisAdvancePaymentDetails.InvoiceNo);
//    $("#monthSpan").html(Utilities.FormatJsonDate(thisAdvancePaymentDetails.SalaryMonth));
//    $("#narrationSpan").html(thisAdvancePaymentDetails.Narration);

//    $("#detailsTbody").html(output);
//}