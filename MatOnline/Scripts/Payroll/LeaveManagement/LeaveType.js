﻿var input = {};
var table = "";
var leaves = [];
var leavesforEdit = "";

$(function () {
    Utilities.Loader.Show();
    getData();
    Utilities.Loader.Hide();

    //on click of the edit button
    $("#leaveTypeTable").on("click", ".btnEditModal", function () {
        var id = $(this).attr("id");
        leavesforEdit = getLeaveType(id);
        console.log(leavesforEdit);
        $("#editLeaveTypeName").val(leavesforEdit.LeaveTypeName);
        $("#editDuration").val(leavesforEdit.LeaveDuration);
        $("#editCategory").val(leavesforEdit.LeaveCategory);
        if (leavesforEdit.Stage1 == 1)
        {
            $('#editStage1').prop('checked', true);
        }
        else {
            $('#editStage1').prop('checked', false);
        }
        if (leavesforEdit.Stage2 == 1) {
            $('#editStage2').prop('checked', true);
        }
        else {
            $('#editStage2').prop('checked', false);
        }
        if (leavesforEdit.Stage3 == 1) {
            $('#editStage3').prop('checked', true);
        }
        else {
            $('#editStage3').prop('checked', false);
        }
        if (leavesforEdit.HR == 1) {
            $('#editHr').prop('checked', true);
        }
        else {
            $('#editHr').prop('checked', false);
        }
        $('#editModal').modal('toggle');
        //console.log($("#editCategory").val());

    });

    //to save changes on modal for edit
    $("#saveChanges").click(function () {
        var errorMsg = "";
        errorMsg += $("#editLeaveTypeName").val() == "" ? "Leave Type name required.<br>" : "";
        errorMsg += $("#editDuration").val() == "" ? "Duration required.<br>" : "";
        errorMsg += $("#editCategory").val() == "" ? "Category Name required.<br>" : "";

        if ($('#editStage1').is(":checked")) {
            $('#editStage1').val(1);
        }
        else {
            $('#editStage1').val(0);
        }

        if ($('#editStage2').is(":checked")) {
            $('#editStage2').val(1);
        }
        else {
            $('#editStage2').val(0);
        }

        if ($('#editStage3').is(":checked")) {
            $('#editStage3').val(1);
        }
        else {
            $('#editStage3').val(0);
        }

        if ($('#editHr').is(":checked")) {
            $('#editHr').val(1);
        }
        else {
            $('#editHr').val(0);
        }

        if (errorMsg == "") {
            var toEdit = {
                leaveTypeId:leavesforEdit.LeaveTypeId,
                leaveType: $("#editLeaveTypeName").val(),
                duration: $("#editDuration").val(),
                category: $("#editCategory").val(),
                stage1: $("#editStage1").val(),
                stage2: $("#editStage2").val(),
                stage3: $("#editStage3").val(),
                hr: $('#editHr').val()
            };
            Utilities.Loader.Show();
            console.log(toEdit.leaveTypeId)
            $.ajax({
                url: API_BASE_URL + "/LeaveManagement/UpdateLeaveType",
                type: 'POST',
                data: JSON.stringify(toEdit),
                contentType: "application/json",
                success: function (data) {
                    //table.destroy();
                    console.log(data);
                    getData();
                    Utilities.SuccessNotification("Changes saved!");
                    $("#editLeaveTypeName").val("");
                    $("#editDuration").val("");
                },
                error: function (request, error) {
                    Utilities.Loader.Hide();
                    Utilities.ErrorNotification("Something went wrong!");
                }
            });
            Utilities.Loader.Hide();
        }
        else {
            Utilities.ErrorNotification(errorMsg);
        }
    });

    //to delete leave ype   
    $("#leaveTypeTable").on("click", ".btnDeleteModal", function () {
        var id = $(this).attr("id");
        var newLeave = getLeaveType(id);
        if (Utilities.Confirm("Delete this record?")) {
            var leaveTypeToDelete = {
                leaveTypeId: newLeave.LeaveTypeId,
                duration: newLeave.LeaveDuration,
                category: newLeave.LeaveCategory,
                leaveType: newLeave.LeaveTypeName
            };
            console.log(leaveTypeToDelete);
            Utilities.Loader.Show();
            $.ajax({
                url: API_BASE_URL + "/LeaveManagement/DeleteLeaveType",
                type: 'POST',
                data:JSON.stringify(leaveTypeToDelete),
                contentType: "application/json",
                success: function (data) {
                    //table.destroy();
                    console.log(data);
                    getData();
                },
                error: function (request, error) {
                    Utilities.Loader.Hide();
                }
            });
            Utilities.Loader.Hide();
        }
    });
})

function createLeaveType()
{
    var errorMessage = "";
    errorMessage += $("#leaveTypeName").val() == "" ? "Leave type Name required.</br>" : "";
    errorMessage += $("#duration").val() == "" ? "Duration required.</br>" : "";
    errorMessage += $("#category").val() == "" ? "Category Name required.</br>" : "";

    if (errorMessage == "")
    {
        if ($('#stage1').is(":checked"))
        {
            $('#stage1').val(1);
        }
        else {
            $('#stage1').val(0);
        }

        if ($('#stage2').is(":checked"))
        {
            $('#stage2').val(1);
        }
        else {
            $('#stage2').val(0);
        }

        if ($('#stage3').is(":checked"))
        {
            $('#stage3').val(1);
        }
        else
        {
            $('#stage3').val(0);
        }

        if ($('#hr').is(":checked"))
        {
            $('#hr').val(1);
        }
        else
        {
            $('#hr').val(0);
        }
        //added new properties for the object
        //need to create and update workflow table
        input.LeaveType = $("#leaveTypeName").val();
        input.duration = $("#duration").val();
        input.category = $("#category").val();
        input.stage1 = $("#stage1").val();
        input.stage2 = $("#stage2").val();
        input.stage3 = $("#stage3").val();
        input.hr = $("#hr").val();


        console.log(input);
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/LeaveManagement/CreateLeaveType",
            type: "POST",
            data: JSON.stringify(input),
            contentType: "application/json",
            success: function (data) {
                //destroytable
               //table.destroy();
                //call rendertable function
                // renderTable();
               getData();
                //call success notification
                console.log(data);
            },
            error: function (request, error) {
                Utilities.Loader.Hide();
                Utilities.ErrorNotification("Something went wrong!");
            }
        })
      $("#leaveTypeName").val('');
       $("#duration").val('');
       $('#stage1').prop("checked", false);
       $('#stage2').prop("checked", false);
       $('#stage3').prop("checked", false);
       $('#hr').prop("checked", false);
        Utilities.Loader.Hide();
        
    }
    else
    {
        Utilities.ErrorNotification(errorMessage);
    }
}

function getData()
{
    $.ajax({
        url: API_BASE_URL + "/LeaveManagement/GetLeaveTypes",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            leaves = data.Response;
            console.log(data);
            renderTable();
        }
    })
   //get data from DB ans store in data variable
}


function renderTable()
{
    if (table != "")
    {
        table.destroy();
    }
    var display = [];

    $.each(leaves, function (index, row) {
        display.push([
        index + 1,
        row.LeaveTypeName,
        '<div class="btn-group btn-group-sm"><div class="dropdown"><button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button><ul class="dropdown-menu"><li class="btnEditModal" id="' + row.LeaveTypeId + '"><a><i class="fa fa-edit"></i> Edit</a></li><li class="btnDeleteModal" id="' + row.LeaveTypeId + '"><a><i class="fa fa-times"></i> Delete</a></li></ul></div></div>'
        ])
    });

    table = $('#leaveTypeTable').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });

}



function getLeaveType(id)
{
    console.log(leaves);
    var leaveType = "";
    for(i=0;i<leaves.length;i++)
    {
        if(leaves[i].LeaveTypeId == id)
        {
            leaveType = leaves[i];
        }
    }
    console.log(leaveType);
    return leaveType;
}