﻿var applications = [];
var table = "";
var leaves  = [];
var employees = [];


//for the workflow, need to create an entry on application, and the each time it is approved, create an 
//entry showing the stage it is at.
$(function () {
   
    getLeaveTypes();
    getEmployees();
    getApplications();
    ///finish up other views and then all that concerns approval as it is

    //on click of button approve and cancel

    $('#LeaveApprovalList').on('click', '.btnCancel', function () {
        var id = $(this).attr('id');
        console.log(id);
        var applToDel = getLeaveApplById(id);
        console.log(applToDel);
        var del = {
            id: applToDel.StageId
        };
        //need to delete for everywhere if application is deleted at anytime
        if(confirm("Are you sure you want to cancel and delete this application, It would delete for all other Stages "))
        {
            $.ajax({
                url:API_BASE_URL + "/LeaveManagement/DeleteApplStageOne",
                type: "POST",
                contentType: "application/json",
                data:JSON.stringify(del),
                success:function(data)
                {
                    console.log(data);
                    getApplications();
                }
            })

            $.ajax({
                url: API_BASE_URL + "/LeaveManagement/DeleteApplStageTwo",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(del),
                success: function (data) {
                    console.log(data);
                    getApplications();
                }
            })

            $.ajax({
                url: API_BASE_URL + "/LeaveManagement/DeleteApplStageThree",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(del),
                success: function (data) {
                    console.log(data);
                    getApplications();
                }
            })

            $.ajax({
                url: API_BASE_URL + "/LeaveManagement/DeleteApplHr",
                type: "POST",
                contentType: "application/json",
                data: JSON.stringify(del),
                success: function (data) {
                    console.log(data);
                    getApplications();
                }
            })
        }

    })


    $('#LeaveApprovalList').on('click', '.btnApprove', function () {
        var id = $(this).attr("id");
        console.log(id);
        var applToAppr = getLeaveApplById(id);
        console.log(applToAppr);
        //need to get all applications and post backk to db with ID as stageId
        //do a post to to upxdate approve section to change tet to approve and notifiy manager

        $.ajax({
            url: API_BASE_URL + "/LeaveManagement/UpDateLeaveApplStageOne",
            type: 'POST',
            contentType: "application/json",
            data:JSON.stringify(applToAppr),
            success:function(data)
            {
                console.log(data);
                getApplications();
            }
        })

    })
})

function getLeaveTypes() {
    Utilities.Loader.Show();
    //talk to api to get employess
    $.ajax({
        url: API_BASE_URL + "/LeaveManagement/GetLeaveTypes",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            leaves = data.Response;
            console.log(leaves);
        }
    });
    Utilities.Loader.Hide();
}

function getEmployees() {
    Utilities.Loader.Show();
    //talk to api to get employess
    $.ajax({
        url: API_BASE_URL + "/LeaveManagement/GetEmployees",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            employees = data.Response;
            console.log(employees);
        }
    });
    Utilities.Loader.Hide();
}

function getApplications()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/LeaveManagement/GetLeaveStageOne",
        type: "GET",
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            applications = data.Response;
            renderTable();
            console.log(applications);
        }
    })
    Utilities.Loader.Hide();
}
// OFE OME
function renderTable()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    var status = "";
   
    $.each(applications, function (index, row) {
        if (row.Approved == null || row.Approved == "" || row.Approved == undefined)
        {
            status =  '<span class="badge badge-pill badge-warning">Pending</span>'
        }
        else {
            status = '<span class="badge badge-pill badge-success">Approved</span>'
        }
        display.push([
            index + 1,
            getEmployeeName(row.EmployeeId),
            getLeaveNames(row.LeaveTypeId),
            Utilities.FormatJsonDate(row.StartDate),
            Utilities.FormatJsonDate(row.EndDate),
            row.Duration,
            status,
        '<div class="btn-group btn-group-sm"><div class="dropdown"><button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button><ul class="dropdown-menu"><li class="btnApprove" id="' + row.StageId + '"><a><i class="fa fa-edit"></i> Approve</a></li><li class="btnCancel" id="' + row.StageId + '"><a><i class="fa fa-times"></i> Cancel</a></li></ul></div></div>'
    ])
    })

    table = $('#LeaveApprovalList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,   
        "autoWidth": true
    });
}

function getLeaveNames(id)
{
    console.log(leaves);
    var forLeave = "";
    for(i=0;i<leaves.length;i++)
    {
        if(id == leaves[i].LeaveTypeId)
        {
            forLeave = leaves[i].LeaveTypeName;
            break;
        }
    }
    console.log(forLeave);
    return forLeave;
}

function getEmployeeName(id)
{
    console.log(employees);
    var emp = {};
    for(i=0;i<employees.length;i++)
    {
        if(id == employees[i].employeeId)
        {
            emp = employees[i].employeeName;
            break;
        }
    }
    return emp;
}

function getLeaveApplById(id)
{
    console.log(applications);
    var l ={}; 
    for (i = 0; i < applications.length; i++) {
        if (applications[i].StageId == id) {
            l = applications[i];
            break;
        }
    }
        console.log(l);
    return l;
  //  console.log(l);
}