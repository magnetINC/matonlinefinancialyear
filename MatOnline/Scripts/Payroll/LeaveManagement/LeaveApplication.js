﻿var employees = [];
var leaves = [];
var leaveApplication = {};
var applications = [];
var table = "";
var name = [];
var text = "Pending";
var actualLeave = {};

//now when a leave is picked , need to chk for duration and the stages picked
//to create the applications to each stage

$(function () {
    getEmployees();
    getLeaveTypes();
    getLeaveApplications();
    $('#startDate').change(function () {
        var start = new Date($('#startDate').val());
        var end = new Date($('#endDate').val());
        var aDay = 24 * 60 * 60 * 1000;
        var numberOfDays = Math.ceil((end.getTime() - start.getTime()) / aDay);
        var mainDays = numberOfDays - 2;
        console.log(numberOfDays);
        console.log(start);
        console.log(end);
        $('#days').val(numberOfDays);
    })

    $('#endDate').change(function () {
        var start = new Date($('#startDate').val());
        var end = new Date($('#endDate').val());
        var aDay = 24 * 60 * 60 * 1000;
        var numberOfDays = Math.ceil((end.getTime() - start.getTime()) / aDay);
        console.log(numberOfDays);
        console.log(start);
        console.log(end);
        $('#days').val(numberOfDays);
    })
 
    //no need for approval on this view
    $('#LeaveApprovalList').on('click', '.btnApprove', function () {
        
        //get the id of row click,
        var id = $(this).attr("id");
        var upLeaveApp = getLeaveAppById(id);
        console.log(upLeaveApp);
        $.ajax({
            url: API_BASE_URL + "/LeaveManagement/UpdateLeaveAppl",
            type: "POST",
            contentType: "application/json",
            data: JSON.stringify(upLeaveApp),
            success: function (data) {
                console.log(data);
            }
        });
        //update the table so it woukd now be approved on approve
        //change pending to approve
        //create a new model for update, so the name would be the same
    });
    //also no need fr this on this view
    $('#LeaveApprovalList').on('click', '.btnCancel', function () {
        var id = $(this).attr("id");
        var delLeave = getLeaveAppById(id);
        var del = {
            id:delLeave.LeaveApplicationId
        }
        if (confirm("Are u sure you want to cancel and delete this Leave Application"))
        {
            $.ajax({
                url: API_BASE_URL + "/LeaveManagement/DeleteLeaveApplication",
                type: 'POST',
                contentType: "application/json",
                data: JSON.stringify(del),
                success: function (data) {
                    console.log(data);
                }
            });
            getLeaveApplications();
        }
     
    })
}) 

//would pick the Id of the employee in login, so no need for an employee dropDown
function getEmployees()
{
    Utilities.Loader.Show();
    //talk to api to get employess
    $.ajax({
        url: API_BASE_URL + "/LeaveManagement/GetEmployees",
        type: "GET",
        contentType: "application/json",
        success: function (data)
        {
            console.log(data);
            employees = data.Response;
            $("#employee").kendoDropDownList({
                filter: "contains",
                dataTextField: "employeeName",
                dataValueField: "employeeId",
                dataSource: employees,
            });
        }
    });
    Utilities.Loader.Hide();
}

//to get leaveTypes
//
function getLeaveTypes() {
    Utilities.Loader.Show();
    //talk to api to get employess
    $.ajax({
        url: API_BASE_URL + "/LeaveManagement/GetLeaveTypes",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            leaves = data.Response;
            console.log(leaves);
            $("#leaveType").kendoDropDownList({
                filter: "contains",
                dataTextField: "LeaveTypeName",
                dataValueField: "LeaveTypeId",
                dataSource: leaves,
            });
        }
    });
    Utilities.Loader.Hide();
}

//need to do a check for the stages , create for each of them
//also creates an entry for the work flow table
function createApplication()
{
    var selectedLeave = $('#leaveType').val();
    var leavesToUse = getLeaves(selectedLeave);
    console.log(leavesToUse);
    if ($('#days').val() > leavesToUse.Duration)
    {
        Utilities.ErrorNotification("Duration cannot exceed set duration for Leave Type!");
        alert("canaopewrfngfporgmw3flndfklpkvdslvmfldfgbdnlvnzdlnlsns");
    }
    else {
        // need to check if the duration is more than that allowed for the leavee
        leavesToUse.employee = $('#employee').val();
        leavesToUse.leave = $('#leaveType').val();
        leavesToUse.duration = $('#days').val();
        leavesToUse.startDate = $('#startDate').val();
        leavesToUse.endDate = $('#endDate').val();

        if ($('#days').val() == "")
        {
            Utilities.ErrorNotification("Duration cannot be empty, choose appropriate date intervals");
        }
        else {
            if (leavesToUse.Stage1 == 1 && leavesToUse.Stage2 == 1 && leavesToUse.Stage3 == 1 && leavesToUse.HR == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage1",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });

                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage2",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });

                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage3",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });


                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveForHr",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
                //pply ot stage 1,2,3,hr
            }
            else if (leavesToUse.Stage1 == 1 && leavesToUse.Stage2 == 1 && leavesToUse.Stage3 == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage1",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });

                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage2",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });

                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage3",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
            else if (leavesToUse.Stage1 == 1 && leavesToUse.Stage2 == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage1",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });

                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage2",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
            else if (leavesToUse.Stage1 == 1 && leavesToUse.Stage3 == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage1",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });

                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage3",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
            else if (leavesToUse.Stage1 == 1 && leavesToUse.HR == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage1",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });

                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateleaveHr",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
            else if (leavesToUse.Stage1 == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage1",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
            else if (leavesToUse.Stage2 == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage2",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
            else if (leavesToUse.Stage3 == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage3",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
            else if (leavesToUse.HR == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveForHr",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
            else if (leavesToUse.Stage2 == 1 && leavesToUse.Stage3 == 1 && leavesToUse.HR == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage1",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });

                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage3",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });

                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveForHr",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
            else if (leavesToUse.Stage2 == 1 && leavesToUse.Stage3 == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage2",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });

                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage3",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
            else if (leavesToUse.Stage2 == 1 && leavesToUse.HR == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage2",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });

                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveForHr",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
            }
            else if (leavesToUse.Stage3 == 1 && leavesToUse.HR == 1) {
                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveStage3",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });

                $.ajax({
                    url: API_BASE_URL + "/LeaveManagement/CreateLeaveForHr",
                    type: "POST",
                    contentType: "application/json",
                    data: JSON.stringify(leavesToUse),
                    success: function (data) {
                        console.log(data);
                    }
                });
            }


            Utilities.Loader.Show();
            leaveApplication.employee = $('#employee').val();
            leaveApplication.leave = $('#leaveType').val();
            leaveApplication.startDate = $('#startDate').val();
            leaveApplication.endDate = $('#endDate').val();
            leaveApplication.days = $('#days').val();
            console.log(leaveApplication);

            $.ajax({
                url: API_BASE_URL + "/LeaveManagement/CreateLeaveApplication",
                type: 'POST',
                data: JSON.stringify(leaveApplication),
                contentType: "application/json",
                success: function (data) {
                    console.log(data);
                    getLeaveApplications();
                }
            })
            Utilities.Loader.Hide();

            $('#days').val("");
            Utilities.SuccessNotification("Leave Application Successful!");
        }
        // if($('#days').val() > )
        

        //create first entry for work flow, use if stmnts to control flow
     
      
    }
}


function createWorkFlow()
{
    //need to chk which srage is first based on leavestouse.Sate

    var workFlow = {};
    workFlow.IssuedBy = $('#employee').val();
    workFlow.LeaveType = $('#leaveType').val();
    workFlow.Stage1 = 1;
    workFlow.Stage2 = 1;
    workFlow.Stage3 = 1;
    workFlow.Hr = 1;

    //loop to chk which is first stage
    
    //insert into workflow table, the idea being the leave would pass through various stages, based on the leave type
    

}
//for the leave approval
function getLeaveApplications()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/LeaveManagement/GetLeaveApplication",
        type: 'GET',
        contentType: "application/json",
        success:function(data)
        {
            console.log(data);
            applications = data.Response;
            renderTable();
        }
    })
    Utilities.Loader.Hide();
}

//all these would now be in the various application tables
function renderTable()
{
    var display = [];
    if(table != "")
    {
        table.destroy();
    }
    getLeaveName();
    $.each(applications, function (index, row) {
        display.push([
            index + 1,
            getEmployeeNames(row.EmployeeId),
            getLeaveName(row.LeaveTypeId),
            Utilities.FormatJsonDate(row.StartDate),
            Utilities.FormatJsonDate(row.EndDate),
            row.Number_Of_Days,
            //button for approve, on click, 
            '<span class="badge badge-pill badge-warning">Pending</span>',
        '<div class="btn-group btn-group-sm"><div class="dropdown"><button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button><ul class="dropdown-menu"><li class="btnApprove" id="' + row.LeaveApplicationId + '"><a><i class="fa fa-edit"></i> Approve</a></li><li class="btnCancel" id="' + row.LeaveApplicationId + '"><a><i class="fa fa-times"></i> Cancel</a></li></ul></div></div>'
        ])
    })

    table = $('#LeaveApprovalList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}

//gets the chosen leave
function getLeaves(id)
{
    for(i= 0;i<=leaves.length;i++)
    {
        if(leaves[i].LeaveTypeId == id)
        {
            actualLeave = leaves[i];
            break;
        }
    }
    return actualLeave;
}

function getLeaveName(id)
{
    console.log(leaves);
    var output = "";
    for(i=0;i<leaves.length;i++)
    {
        if(leaves[i].LeaveTypeId == id)
        {
            //name[i] = leaves[i].LeaveTypeName;  
            //leave.id == id in the rendered table
            // the name and display
            output = leaves[i].LeaveTypeName;
            break;
        }
    }
    return output;
    console.log(output);
}


function getEmployeeNames(id)
{
    var emp = "";
    for (i = 0;i<employees.length;i++)
    {
        if(id == employees[i].employeeId)
        {
            emp = employees[i].employeeName;
            break;
        }
    }
    return emp; 
    console.log(emp);
}

//to get leave application based on id
function getLeaveAppById(id)
{
    var leaveApp = {};
    for (i = 0; i <= applications.length; i++) {
        if (applications[i].LeaveApplicationId == id) {
            leaveApp = applications[i];
            break;
        }
    }
    console.log(leaveApp);
    return leaveApp;
    //now to put gotten leave and post it with approve
}


//wont work
function renderTable2() {
    var display = [];
    if (table != "") {
        table.destroy();
    }
    getLeaveName();
    $.each(applications, function (index, row) {
        display.push([
            index + 1,
            getEmployeeNames(row.EmployeeId),
            getLeaveName(row.LeaveTypeId),
            Utilities.FormatJsonDate(row.StartDate),
            Utilities.FormatJsonDate(row.EndDate),
            row.Number_Of_Days,
            //button for approve, on click, 
            '<span class="badge badge-pill badge-success">Approved</span>',
        '<div class="btn-group btn-group-sm"><div class="dropdown"><button class="btn btn-success dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button><ul class="dropdown-menu"><li class="btnApprove" id="' + row.LeaveApplicationId + '"><a><i class="fa fa-edit"></i> Approve</a></li><li class="btnCancel" id="' + row.LeaveApplicationId + '"><a><i class="fa fa-times"></i> Cancel</a></li></ul></div></div>'
        ])
    })

    table = $('#LeaveApprovalList').DataTable({
        data: display,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}