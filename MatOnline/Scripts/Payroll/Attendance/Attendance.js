﻿var previousAttendance = [];
var newAttendance = [];
var newAttendanceToSave = [];
var details = [];

var master = {};

var table = "";
employeeStatusToEdit = 0;

$(function () {
    getAttendance($("#date").val());

    $("#date").change(function () {
        getAttendance($("#date").val());
    });
    $("#save").hide();

});

// ========== API CALLS ==========
function getAttendance(date)
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Attendance/GetAttendance?date=" + date,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if(data.ResponseMessage == "For new day")
            {
                previousAttendance = [];
                newAttendance = data.Response;
                $.each(newAttendance, function (count, row) {
                    row.status = "Present";
                    row.narration = "";
                });
                console.log("new");
                console.log(newAttendance);
            }
            else
            {
                newAttendance = [];
                previousAttendance = data.Response;
                console.log("already taken");
                console.log(previousAttendance);
            }
            
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(renderAttendanceDataToTable, 500);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function saveAttendance()
{
    for (i = 0; i < newAttendance.length; i++) {
        details.push({
            EmployeeId: newAttendance[i].employeeId,
            Status: newAttendance[i].status,
            Narration: newAttendance[i].narration,
            ExtraDate: new Date(),
            Extra1: "",
            Extra2: ""
        });
    }

    var master = {
        Date: $("#date").val(),
        Narration: $("#narrationMaster").val(),
        ExtraDate: new Date(),
        Extra1: "",
        Extra2: ""
    };

    var ToSave = { master: master, details: details };
    console.log(ToSave);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/Attendance/SaveAttendance",
        type: "POST",
        data: JSON.stringify(ToSave),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data.ResponseCode == 200) {
                Utilities.SuccessNotification("Attendance saved succeessfully");
                window.location = "/Payroll/Attendance/Index";
                Utilities.Loader.Hide();
            }
            else {
                Utilities.SuccessNotification(data.ResponseMessage);
            }
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });

}



// ========== RENDER DATA TO CONTROLS ==========
function renderAttendanceDataToTable()
{
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }
    if (newAttendance.length > 0)
    {
        $("#save").show();
        objToShow = [];
        $.each(newAttendance, function (count, row) {
            if (row.status == "Present") {
                statusOutput = '<span class="label label-success">Present</span>';
            }
            else if (row.status == "Half Day") {
                statusOutput = '<span class="label label-warning">Half Day</span>';
            }
            else if (row.status == "Absent") {
                statusOutput = '<span class="label label-danger">Absent</span>';
            }
            objToShow.push([
                count + 1,
                row.employeeCode,
                row.employeeName,
                statusOutput,
                row.narration,
                '<button class="btn btn-primary editAttendance" onclick="changeStatus(' + row.employeeId + ')"><i class="fa fa-edit"></i>    Change Status</button>'
            ]);
        });
    }
    else if (previousAttendance.length > 0)
    {
        $("#save").hide();
        objToShow = [];
        var statusOutput = "";
        $("#narrationMaster").val(previousAttendance[0].MasterNarration);
        $.each(previousAttendance, function (count, row) {
            if (row.status == "Present") {
                statusOutput = '<span class="label label-success">Present</span>';
            }
            else if (row.status == "Half Day") {
                statusOutput = '<span class="label label-warning">Half Day</span>';
            }
            else if (row.status == "Absent") {
                $("#ApproveOrder").hide();
                $("#cancelOrder").hide();
                statusOutput = '<span class="label label-danger">Absent</span>';
            }
            objToShow.push([
                count + 1,
                row.employeeCode,
                row.employeeName,
                statusOutput,
                row.narration,
                ''
            ]);
        });
    }

    table = $('#attendanceTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    if (checkPriviledge("frmAttendance", "Update") === false) {
        $(".editAttendance").hide();
    }
}


// ========== OTHER FUNCTIONS ==========
function changeStatus(id)
{
    employeeStatusToEdit = id;
    $("#changeStatusModal").modal("show");
    $('#changeStatusModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
}
function applyStatusToEmployee()
{
    var indexOfObject = newAttendance.findIndex(p=>p.employeeId == employeeStatusToEdit);
    newAttendance[indexOfObject].status = $("#status").val();
    newAttendance[indexOfObject].narration = $("#narration").val();

    //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
    setTimeout(renderAttendanceDataToTable, 500);
    //renderAttendanceDataToTable();
    $("#changeStatusModal").modal("hide");
}


