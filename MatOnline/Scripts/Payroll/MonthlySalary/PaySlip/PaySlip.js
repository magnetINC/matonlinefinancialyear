﻿var employees = [];
var salaryInfo = [];
var companyInfo = {}
var employeeInfo = {};
var attendanceInfo = {};

$(function () {
    $("#printablediv").hide();
    getLookUps();
});

function getPaySlip()
{
    if ($("#date").val() == "" || $("#date").val() == null || $("#date").val() == undefined)
    {
        Utilities.ErrorNotification("Please Select Salary Month");
    }
    else if ($("#employee").val() == 0 || $("#employee").val() == "" || $("#employee").val() == null || $("#employee").val() == undefined)
    {
        Utilities.ErrorNotification("Please Select Employee");
    }
    else
    {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PaySlip/GetPaySlipForPrint?employeeId=" + $("#employee").val() + "&month=" + $("#date").val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if(data.ResponseCode == 200)
                {
                    companyInfo = data.Response.Table[0];
                    employeeInfo = data.Response.Table1[0];
                    salaryInfo = data.Response.Table1;
                    attendanceInfo = data.Response.Table2[0];
                    renderDataToControls();

                    Utilities.Loader.Hide();
                }
                else if(data.ResponseCode == 205)
                {
                    Utilities.Loader.Hide();

                    Utilities.ErrorNotification("Salary Not Paid");
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }

}
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PaySlip/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            employees = data.Response;

            renderLookUpDataToControls();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function renderLookUpDataToControls()
{
    $("#employee").kendoDropDownList({
        filter: "contains",
        dataTextField: "employeeName",
        dataValueField: "employeeId",
        dataSource: employees,
        optionLabel: "Please Select..."
    });
}
function renderDataToControls()
{
    console.log(employeeInfo);
    console.log(attendanceInfo);
    console.log(salaryInfo);


    if (employeeInfo == undefined || salaryInfo == [])
    {
        $("#printablediv").hide();
        $("#errorDiv").show();
        $("#errorDiv").html("<center><h3>Salary has not been processed for this employee</h3></center>");
    }
    else
    {
        $("#errorDiv").hide();
        //render company data
        $("#companyName").html(companyInfo.companyName);
        $("#companyAddress").html(companyInfo.address);

        //render employee data
        $("#employeeCode").html(employeeInfo.employeeCode);
        $("#employeeName").html(employeeInfo.employeeName);
        $("#designation").html(employeeInfo.designationName);
        $("#salaryMonth").html(employeeInfo.Month);
        $("#salaryDate").html(employeeInfo.Date);

        //render attendance data
        $("#workingDays").html(attendanceInfo.WorkingDay);
        $("#presentDays").html(attendanceInfo.PresentDays);
        $("#leaveDays").html(attendanceInfo.LeaveDays);

        //render earnings
        var outputEarning = "";
        var totalEarning = 0;
        $.each(salaryInfo, function (count, row) {
            totalEarning = totalEarning + row.ADDamount;
            outputEarning += '<tr>\
                <td>'+ row.ADDpayheadName + '</td>\
                <td><b>' + Utilities.FormatCurrency(row.ADDamount) + '</b></td>\
            </tr>';
        });
        $("#earningsTable").html(outputEarning);

        //render deductions
        var outputDeductions = "";
        outputDeductions += '<tr>\
                <td>Deduction</td>\
                <td><b>' + Utilities.FormatCurrency(salaryInfo[0].Deduction) + '</b></td>\
            </tr>\
            <tr>\
                <td>PAYE</td>\
                <td><b>' + Utilities.FormatCurrency(salaryInfo[0].PAYE) + '</b></td>\
            </tr>\
            <tr>\
                <td>Pension</td>\
                <td><b>' + Utilities.FormatCurrency(salaryInfo[0].Pension) + '</b></td>\
            </tr>\
            <tr>\
                <td>Advance</td>\
                <td><b>' + Utilities.FormatCurrency(salaryInfo[0].Advance) + '</b></td>\
            </tr><tr></tr><tr></tr>';
        $("#deductionTable").html(outputDeductions);

        //render total earning and deduction
        var totaldeduction = (salaryInfo[0].Advance + salaryInfo[0].Pension + salaryInfo[0].PAYE + salaryInfo[0].Deduction);
        $("#totalEarnings").html(Utilities.FormatCurrency(totalEarning));
        $("#totalDeductions").html(Utilities.FormatCurrency(totaldeduction));


        //render net salary
        $("#netPay").html(Utilities.FormatCurrency(salaryInfo[0].Salary));
        $("#printablediv").show();

    }

}