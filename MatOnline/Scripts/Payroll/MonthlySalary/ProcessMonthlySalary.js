﻿var monthlySalariesToProcess = [];
var filteredLedgers = [];
var cashOrBank = [];


var voucherNo = "";
var table = "";

$(function () {
    getLookUps();
});

// ========== API CALLS =========
function getLookUps()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/MonthlySalary/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            //filteredLedgers = data.Response.filteredLedgers;  
            cashOrBank = data.Response.cashOrBank;
            voucherNo = data.Response.invoiceNo;

            renderLookUpDataToControls();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getMonthlySalariesToProcess()
{
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/MonthlySalary/GetMonthlySalaryForProcessing?date=" + $("#month").val(),
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            monthlySalariesToProcess = data.Response;

            //$.each(monthlySalariesToProcess, function (count, row) {
            //    if(row.staus == null || row.status == undefined || row.status == "")
            //    {
            //        row.status = "Pending";
            //    }
            //});

            renderDetailsForView();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function saveMonthlySalaries()
{
    //Utilities.Loader.Show();
    //if($("#filteredLedgers").val() == 0 || $("#filteredLedgers").val() == "" || $("#filteredLedgers").val() == null)
    //{
    //    Utilities.ErrorNotification("Please Select a Bank");
    //}
    if ($("#cashOrBank").val() == 0 || $("#cashOrBank").val() == "" || $("#cashOrBank").val() == null) {
        Utilities.ErrorNotification("Please Select a Bank");
    }
    else if ($("#month").val() == "" || $("#month").val() == null) {
        Utilities.ErrorNotification("Please Select a month");
    }
    else {
        Utilities.Loader.Show();
        var master = {};
        //master.LedgerId = $("#filteredLedgers").val();
        master.LedgerId = $("#cashOrBank").val();
        master.VoucherNo = $("#voucherNo").val();
        master.InvoiceNo = $("#voucherNo").val();
        master.Date = $("#date").val();
        master.Month = $("#month").val();
        master.TotalAmount = $("#totalSalary").val();
        master.Narration = $("#narration").val();
        master.ExtraDate = new Date();
        master.Extra1 = "";
        master.Extra2 = "";
        master.SuffixPrefixId = 1;
        master.FinancialYearId = 1;
        master.VoucherTypeId = 27;

        var details = [];
        $.each(monthlySalariesToProcess, function (count, row) {
            details.push({
                EmployeeId: row.employeeId,
                Bonus: row.bonusAmount,
                Deduction: row.deductionAmount,
                Advance: row.amount,
                AdvancePaymentLedgerId: row.advancePaymentLedgerId,
                Lop: row.lop,
                PAYE: row.PAYE,
                Pension: row.Pension,
                Salary: row.salary,
                Status: row.status,
                ExtraDate: new Date(),
                Extra1: row.defaultpackageid,
                Extra2: ""
            });
        });
        var toSave = { master: master, details: details };
        console.log(toSave);
        $.ajax({
            url: API_BASE_URL + "/MonthlySalary/SaveSalary",
            type: "POST",
            data: JSON.stringify(toSave),
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if (data.ResponseCode == 200) {
                    Utilities.SuccessNotification("Salaries saved succeessfully");
                    window.location = "/Payroll/MonthlySalary/ProcessMonthlySalary";
                    Utilities.Loader.Hide();
                }
                else {
                    Utilities.Loader.Hide();
                    Utilities.ErrorNotification(data.ResponseMessage);
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }
    
}


// ========== RENDER DATA TO CONTROLS ==========
function renderLookUpDataToControls()
{
    $("#voucherNo").val(voucherNo);
    $("#cashOrBank").kendoDropDownList({
        filter: "contains",
        dataTextField: "ledgerName",
        dataValueField: "ledgerId",
        dataSource: cashOrBank,
        optionLabel: "Please Select..."
    });
    //$("#filteredLedgers").kendoDropDownList({
    //    filter: "contains",
    //    dataTextField: "ledgerName",
    //    dataValueField: "ledgerId",
    //    dataSource: filteredLedgers,
    //    optionLabel: "Please Select..."
    //});
}
function renderDetailsForView() {
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }
    $.each(monthlySalariesToProcess, function (count, row) {
        if (row.status == "Paid") {
            statusOutput = '<span class="label label-success">Paid!</span>';
        }
        else{
            statusOutput = '<span class="label label-warning">Pending Payment</span>';
        }
        objToShow.push([
            count + 1,
            row.employeeCode,
            row.employeeName,
            '&#8358;' + Utilities.FormatCurrency(row.bonusAmount),
            '&#8358;' + Utilities.FormatCurrency(row.deductionAmount),
            '&#8358;' + Utilities.FormatCurrency(row.amount),
            '&#8358;' + Utilities.FormatCurrency(row.PAYE),
            '&#8358;' + Utilities.FormatCurrency(row.Pension),
            '&#8358;' + Utilities.FormatCurrency(row.salary),
            statusOutput,
            '<div class="btn-group btn-group-sm">\
                <div class="dropdown">\
                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" type="button"> Salary Status <i class="dropdown-caret"></i></button>\
                    <ul class="dropdown-menu">\
                        <li onclick="changeToPaid(' + row.employeeId + ')"><a>Paid</a></li>\
                        <li onclick="changeToPending(' + row.employeeId + ')"><a>Pending</a></li>\
                    </ul>\
                </div>\
            </div>'
        ]);
    });
    table = $('#monthlySalaryDetails').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    $("#totalSalary").val(Utilities.FormatCurrency(getTotalSalary()));
}

function changeToPaid(id)
{
    var indexOfObject = monthlySalariesToProcess.findIndex(p=>p.employeeId == id);
    monthlySalariesToProcess[indexOfObject].status = "Paid";

    renderDetailsForView();
}
function changeToPending(id) {
    var indexOfObject = monthlySalariesToProcess.findIndex(p=>p.employeeId == id);
    monthlySalariesToProcess[indexOfObject].status = "Pending";

    renderDetailsForView();
}
function getTotalSalary()
{
    var totalSalary = 0;
    $.each(monthlySalariesToProcess, function (count, row) {
        if(row.status == "Paid")
        {
            totalSalary = totalSalary + row.salary
        }
    });
    return totalSalary;
}