﻿var monthlySalaries = [];
var monthlySalaryDetails = [];
var salaryPackages = [];
var employees = [];
var newSalarySettingDetails = [];
var newSalarySettingDetailsToSave = [];

var thisMonthlySalaryDetail = {};

var table = "";
var table1 = "";
var table2 = "";
var employeeSalaryToEdit = 0;
var newEmployeeSalaryToEdit = 0;
var salaryMonthToView = 0;

$(function () {
    $("#newMonthlySalary").hide();

    getLookUps();
    getMasters();
});

// ======== API CALLS ========
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/MonthlySalarySettings/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            console.log("data", data);
            salaryPackages = data.Response.SalaryPackages;
            employees = data.Response.Employees;
            newSalarySettingDetails = employees;

            renderLookUpDataToControls();
            renderEmployeesForNewSetting();
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getMasters() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/MonthlySalarySettings/GetMaster",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            monthlySalaries = data.Response;
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(renderMasterToTable, 500);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function getDetails(id) {
    salaryMonthToView = id;
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/MonthlySalarySettings/GetDetails?monthlySalaryId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            monthlySalaryDetails = data.Response;
            PriviledgeSettings.ApplyUserPriviledge();
            //the function is placed in a setTimeout so the userPriviledges array in PriviledgeSettings is sure to have been filled before it is used here, thereby avoiding errors eg datatable errors.
            setTimeout(renderDetailsForView, 500);
            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function saveChanges() {
    for (i = 0; i < monthlySalaryDetails.length; i++) {
        if (monthlySalaryDetails[i].monthlySalaryDetailsId == employeeSalaryToEdit) {
            thisMonthlySalaryDetail = monthlySalaryDetails[i];
            break;
        }
    }
    console.log(thisMonthlySalaryDetail);
    thisMonthlySalaryDetail.salaryPackageId = $("#salaryPackageDropDown").val();
    console.log(thisMonthlySalaryDetail);
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/MonthlySalarySettings/EditMonthlySalary",
        type: "POST",
        data: JSON.stringify(thisMonthlySalaryDetail),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data.ResponseCode == 200) {
                Utilities.SuccessNotification("Monthly Salary Updated succeessfully");
                getDetails(salaryMonthToView);
                $("#changeSPModal").modal("hide");


                Utilities.Loader.Hide();
            }
            else {
                Utilities.SuccessNotification(data.ResponseMessage);
            }
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function saveNewSetting() {
    for (i = 0; i < newSalarySettingDetails.length; i++) {
        newSalarySettingDetailsToSave.push({
            EmployeeId: newSalarySettingDetails[i].employeeId,
            SalaryPackageId: newSalarySettingDetails[i].defaultPackageId,
            ExtraDate: new Date(),
            Extra1: "",
            Extra2: ""
        });
    }
    var master = {
        SalaryMonth: $("#date").val(),
        Narration: $("#narration").val(),
        ExtraDate: new Date(),
        Extra1: "",
        Extra2: ""
    };
    var ToSave = { master: master, details: newSalarySettingDetailsToSave };
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/MonthlySalarySettings/SaveMonthlySalary",
        type: "POST",
        data: JSON.stringify(ToSave),
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            if (data.ResponseCode == 200) {
                Utilities.SuccessNotification("Settings saved succeessfully");
                window.location = "/Payroll/MonthlySalary/MonthlySalarySettings";
                Utilities.Loader.Hide();
            }
            else {
                Utilities.SuccessNotification(data.ResponseMessage);
            }
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}
function deleteSetting(id) {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/MonthlySalarySettings/DeleteMonthlySalary?monthlySalaryId=" + id,
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            if (data.ResponseCode == 200) {
                Utilities.SuccessNotification("Monthly Salary Deleted succeessfully");
                window.location = "/Payroll/MonthlySalary/MonthlySalarySettings";

                Utilities.Loader.Hide();
            }
            else {
                Utilities.SuccessNotification(data.ResponseMessage);
            }

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

// ======== RENDER DATA TO CONTROLS
function renderLookUpDataToControls() {
    $("#salaryPackageDropDown").kendoDropDownList({
        filter: "contains",
        dataTextField: "salaryPackageName",
        dataValueField: "salaryPackageId",
        dataSource: salaryPackages,
        optionLabel: "Please Select..."
    });
    $("#salaryPackageNewDropDown").kendoDropDownList({
        filter: "contains",
        dataTextField: "salaryPackageName",
        dataValueField: "salaryPackageId",
        dataSource: salaryPackages,
        optionLabel: "Please Select..."
    });
}
function renderMasterToTable() {
    var output = "";
    var objToShow = [];

    if (table != "") {
        table.destroy();
    }
    $.each(monthlySalaries, function (count, row) {
        objToShow.push([
            count + 1,
            Utilities.FormatJsonDate(row.salaryMonth),
            row.narration,
            '<div class="btn-group btn-group-sm salarySettingsAction">\
                <div class="dropdown">\
                    <button class="btn btn-primary dropdown-toggle" data-toggle="dropdown" type="button"> Action <i class="dropdown-caret"></i></button>\
                    <ul class="dropdown-menu">\
                        <li class="viewSalarySettings" onclick="getDetails(' + row.monthlySalaryId + ')"><a><i class="fa fa-eye"></i> View Details</a></li>\
                        <li class="deleteSalarySettings" onclick="deleteSetting(' + row.monthlySalaryId + ')"><a><i class="fa fa-times"></i> Delete</a></li>\
                    </ul>\
                </div>\
            </div>'
        ]);
    });
    table = $('#salaryMonthsTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    var count = 0;
    if (checkPriviledge("frmMonthlySalarySettings", "View") === false) {
        $(".viewSalarySettings").hide();
        count++;
    }
    if (checkPriviledge("frmMonthlySalarySettings", "Delete") === false) {
        $(".deleteSalarySettings").hide();
        count++;
    }
    if (count === 2) {
        $(".salarySettingsAction").hide();
    }
}
function renderDetailsForView() {
    var output = "";
    var objToShow = [];

    if (table1 != "") {
        table1.destroy();
    }
    console.log("monthlySalaryDetails", monthlySalaryDetails)
    $.each(monthlySalaryDetails, function (count, row) {
        var salaryPackageName = row.salaryPackageId == 0 ? "" : findsalaryPackages(row.salaryPackageId).salaryPackageName;
        objToShow.push([
            count + 1,
            findEmployees(row.employeeId).employeeName,
            findEmployees(row.employeeId).employeeCode,
            salaryPackageName,
            '<button class="editSalaryPackage btn btn-primary" onclick="changeSalaryPackage(' + row.monthlySalaryDetailsId + ')"><i class="fa fa-edit"></i>   change Package</button>'
        ]);
    });
    table1 = $('#salaryMonthDetailsTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
    if (checkPriviledge("frmMonthlySalarySettings", "Update") === false) {
        $(".editSalaryPackage").hide();
    }
}

function renderEmployeesForNewSetting() {
    var output = "";
    var objToShow = [];

    if (table2 != "") {
        table2.destroy();
    }
    //console.log("newSalarySettingDetails", newSalarySettingDetails)
    $.each(newSalarySettingDetails, function (count, row) {
        var salaryPackageName = findsalaryPackages(row.defaultPackageId).salaryPackageName;
        salaryPackageName = salaryPackageName == undefined ? "" : salaryPackageName;
        objToShow.push([
            count + 1,
            row.employeeName,
            row.employeeCode,
            salaryPackageName,
            '<buton class="btn btn-primary" onclick="changeSalaryPackageForNew(' + row.employeeId + ')"><i class="fa fa-edit"></i>   change Package</button>'
        ]);
    });
    table2 = $('#newSalaryMonthDetailsTable').DataTable({
        data: objToShow,
        "responsive": true,
        "paging": true,
        "lengthChange": true,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": true
    });
}

// ========== OTHER FUNCTIONS =========
function changeSalaryPackage(id) {
    employeeSalaryToEdit = id;
    $("#changeSPModal").modal("show");
    $('#changeSPModal').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
}
function newSalarySetting() {
    $("#newMonthlySalary").show();
    $("#viewMonthlySalary").hide();
}
function changeSalaryPackageForNew(id) {
    newEmployeeSalaryToEdit = id;
    $("#changeSPModalNew").modal("show");
    $('#changeSPModalNew').on('shown.bs.modal', function () { $(document).off('focusin.modal'); });
}
function applyChangeForNew() {
    //for (i = 0; i < newSalarySettingDetails.length; i++) {
    //    if (newSalarySettingDetails[i].employeeId == newEmployeeSalaryToEdit) {
    //        newSalarySettingDetails[i].dailyWage1 == $("#salaryPackageNewDropDown").val();
    //        break;
    //    }
    //}
    var indexOfObject = newSalarySettingDetails.findIndex(p => p.employeeId == newEmployeeSalaryToEdit);
    newSalarySettingDetails[indexOfObject].defaultPackageId = $("#salaryPackageNewDropDown").val();
    console.log(newSalarySettingDetails);

    renderEmployeesForNewSetting();
    $("#changeSPModalNew").modal("hide");
}

function findEmployees(id) {
    var output = {};
    for (i = 0; i < employees.length; i++) {
        if (employees[i].employeeId == id) {
            output = employees[i];
            break;
        }
    }
    return output;
}
function findsalaryPackages(id) {
    var output = {};
    for (i = 0; i < salaryPackages.length; i++) {
        if (salaryPackages[i].salaryPackageId == id) {
            output = salaryPackages[i];
            break;
        }
    }
    return output;
}
function close() {
    $("#newMonthlySalary").hide();
    $("#viewMonthlySalary").show();
}