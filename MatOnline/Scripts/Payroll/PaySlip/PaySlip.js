﻿var employees = [];
var salaryInfo = [];
var companyInfo = {}
var employeeInfo = {};
var attendanceInfo = {};

$(function () {
    $("#printablediv").hide();
    getLookUps();
});

function getPaySlip()
{
    if ($("#date").val() == "" || $("#date").val() == null || $("#date").val() == undefined)
    {
        Utilities.ErrorNotification("Please Select Salary Month");
    }
    else if ($("#employee").val() == 0 || $("#employee").val() == "" || $("#employee").val() == null || $("#employee").val() == undefined)
    {
        Utilities.ErrorNotification("Please Select Employee");
    }
    else
    {
        Utilities.Loader.Show();
        $.ajax({
            url: API_BASE_URL + "/PaySlip/GetPaySlipForPrint?employeeId=" + $("#employee").val() + "&month=" + $("#date").val(),
            type: "GET",
            contentType: "application/json",
            success: function (data) {
                console.log(data);
                if(data.ResponseCode == 200)
                {
                    companyInfo = data.Response.Table[0];
                    employeeInfo = data.Response.Table1[0];
                    salaryInfo = data.Response.Table1;
                    attendanceInfo = data.Response.Table2[0];
                    renderDataToControls();

                    Utilities.Loader.Hide();
                }
                else if(data.ResponseCode == 205)
                {
                    Utilities.Loader.Hide();

                    Utilities.ErrorNotification("Salary Not Paid");
                }
            },
            error: function (err) {
                Utilities.Loader.Hide();
            }
        });
    }

}
function getLookUps() {
    Utilities.Loader.Show();
    $.ajax({
        url: API_BASE_URL + "/PaySlip/GetLookUps",
        type: "GET",
        contentType: "application/json",
        success: function (data) {
            console.log(data);
            employees = data.Response;

            renderLookUpDataToControls();

            Utilities.Loader.Hide();
        },
        error: function (err) {
            Utilities.Loader.Hide();
        }
    });
}

function renderLookUpDataToControls()
{
    $("#employee").kendoDropDownList({
        filter: "contains",
        dataTextField: "employeeName",
        dataValueField: "employeeId",
        dataSource: employees,
        optionLabel: "Please Select..."
    });
}
function renderDataToControls()
{
    console.log(employeeInfo);
    console.log(attendanceInfo);
    console.log(salaryInfo);


    if (employeeInfo == undefined || salaryInfo == [])
    {
        $("#printablediv").hide();
        $("#errorDiv").show();
        $("#errorDiv").html("<center><h3>Salary has not been processed for this employee</h3></center>");
    }
    else
    {
        $("#errorDiv").hide();
        //render company data
        $("#companyName").html(companyInfo.companyName);
        $("#companyAddress").html(companyInfo.address);
        $("td img").attr("src", companyInfo.extra2);
        //render employee data
        $("#employeeCode").html(employeeInfo.employeeCode);
        $("#employeeName").html(employeeInfo.employeeName);
        $("#designation").html(employeeInfo.designationName);
        $("#salaryMonth").html(employeeInfo.Month);
        $("#salaryDate").html(employeeInfo.Date);

        //render attendance data
        $("#workingDays").html(attendanceInfo.WorkingDay);
        $("#presentDays").html(attendanceInfo.PresentDays);
        $("#leaveDays").html(attendanceInfo.LeaveDays);

        //render earnings
        var outputEarning = "";
        var totalEarning = 0;
        var Bonus = salaryInfo[0].Bonus == null ? 0 : Utilities.FormatCurrency(salaryInfo[0].Bonus);
        $.each(salaryInfo, function (count, row) {
            totalEarning = totalEarning + row.ADDamount;
            var addAmt = row.ADDamount == null ? 0 : row.ADDamount;
            outputEarning += '<tr>\
                <td>'+ row.ADDpayheadName + '</td>\
                <td style="text-align:right"><b>' + Utilities.FormatCurrency(addAmt) + '</b></td>\
            </tr>';
        });
        outputEarning += '<tr>\
                <td>Bonus</td>\
                <td style="text-align:right"><b>' + Bonus + '</b></td>\
            </tr>\'';
        totalEarning = totalEarning + salaryInfo[0].Bonus;
        $("#earningsTable").html(outputEarning);

        //render deductions
        var outputDeductions = "";
        var Deduction = salaryInfo[0].Deduction == null ? 0 : Utilities.FormatCurrency(salaryInfo[0].Deduction);
        var PAYE = salaryInfo[0].PAYE == null ? 0 : Utilities.FormatCurrency(salaryInfo[0].PAYE);
        var Pension = salaryInfo[0].Pension == null ? 0 : Utilities.FormatCurrency(salaryInfo[0].Pension);
        var Advance = salaryInfo[0].Advance == null ? 0 : Utilities.FormatCurrency(salaryInfo[0].Advance);
        outputDeductions += '<tr>\
                <td>Deduction</td>\
                <td style="text-align:right"><b>' + Deduction + '</b></td>\
            </tr>\
            <tr>\
                <td>PAYE</td>\
                <td style="text-align:right"><b>' + PAYE + '</b></td>\
            </tr>\
            <tr>\
                <td>Pension</td>\
                <td style="text-align:right"><b>' + Pension + '</b></td>\
            </tr>\
            <tr>\
                <td>Advance</td>\
                <td style="text-align:right"><b>' + Advance + '</b></td>\
            </tr><tr></tr><tr></tr>';
        $("#deductionTable").html(outputDeductions);

        //render total earning and deduction
        var totaldeduction = (salaryInfo[0].Advance + salaryInfo[0].Pension + salaryInfo[0].PAYE + salaryInfo[0].Deduction);
        $("#totalEarnings").html(Utilities.FormatCurrency(totalEarning));
        $("#totalDeductions").html(Utilities.FormatCurrency(totaldeduction));


        //render net salary
        var Salary = salaryInfo[0].Salary == null ? 0 : Utilities.FormatCurrency(salaryInfo[0].Salary);
        $("#netPay").html(Salary);

        //remove commas
        var salaryAmt = Salary;
        var tempTotal = salaryAmt.replace(/,/g, "");
        //convert to number
        var amtNaira = Math.floor(tempTotal);
        var amtKobo = parseInt(tempTotal.split(".")[1]);
        //capitalize Each Word
        var wordFormat = Utilities.NumberToWords(amtNaira);
        var wordFormat1 = Utilities.NumberToWords(amtKobo);
        const words = wordFormat.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());
        const words1 = wordFormat1.replace(/(^\w{1})|(\s+\w{1})/g, letter => letter.toUpperCase());
        var strArr = words + ' Naira ' + words1+ ' Kobo Only';
        $("#netPayWord").html(strArr);
        $("#printablediv").show();

    }

}