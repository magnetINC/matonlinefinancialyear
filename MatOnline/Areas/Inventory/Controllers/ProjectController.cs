﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Inventory.Controllers
{
    public class ProjectController : Controller
    {
        // GET: Inventory/Project
        public ActionResult Create()
        {
            List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmProject", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pagePrivileges;
            return View();
        }
      
    }
}