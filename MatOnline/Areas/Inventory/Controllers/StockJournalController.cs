﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Inventory.Controllers
{
    public class StockJournalController : Controller
    {
        // GET: Inventory/ItemCreation
        public ActionResult Index()
        {
            Authorize.IsAuthorize("frmStockJournal", "Save", User.Identity.Name);
            return View();
        }
        public ActionResult StockJournalConfirmation()
        {
            return View();
        }
        public ActionResult StockJournalRegister()
        {
            return View();
        }
        public ActionResult TransferIn()
        {
            return View();
        }
        public ActionResult TransferOut()
        {
            return View();
        }
        public ActionResult StockTransferEdit(int id)
        {
            ViewBag.journalId = id;
            return View();
        }

        public ActionResult SimpleStockJornal()
        {
            List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmFarmBuild", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pagePrivileges;
            return View();
        }

        public ActionResult SimpleStockJornalRegister()
        {
            Authorize.IsAuthorize("frmFarmActivityReport", "View", User.Identity.Name);
            return View();
        }

    }
}