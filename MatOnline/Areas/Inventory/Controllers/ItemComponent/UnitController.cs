﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Inventory.Controllers.ItemComponent
{
    public class UnitController : Controller
    {
        // GET: Inventory/Unit
        public ActionResult Index()
        {
            List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmUnit", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pagePrivileges;
            return View();
        }
    }
}