﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Inventory.Controllers.ItemComponent
{
    public class RackController : Controller
    {
        // GET: Inventory/Rack
        public ActionResult Index()
        {
            List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmRack", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pagePrivileges;
            return View();
        }
    }
}