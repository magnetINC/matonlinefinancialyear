﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;

using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClosedXML;
using ClosedXML.Excel;

using MatApi.Models.Reports.OtherReports;
using MATFinancials.DAL;
using MatApi.Models;
using MATFinancials.Classes.HelperClasses;
using System.Globalization;
using MatOnline.Models;
//using Excel = Microsoft.Office.Interop.Excel;
//using DocumentFormat.OpenXml;

namespace MatOnline.Areas.Inventory.Controllers
{
    public class ItemCreationController : Controller
    {
        // GET: Inventory/ItemCreation
        public ActionResult Index()
        {
            dynamic pageAndItemObj = Authorize.IsAuthorizeWithList("frmProductRegister", "frmProductCreation", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pageAndItemObj.pagePrivileges;
            ViewBag.itemPrivileges = pageAndItemObj.itemPrivileges;
            return View();
        }

        public ActionResult ViewDetails(int id)
        {
            ViewBag.ProductId = id;
            return View();
        }

        public ActionResult CreateItem()
        {
            return View();
        }

        public ActionResult EditItem(int id)
        {
            Authorize.IsAuthorize("frmProductCreation", "Update", User.Identity.Name);
            ViewBag.ProductId = id;
            return View();
        }

        [HttpPost]
        public ActionResult CreateItem(HttpPostedFileBase productImage)
        {
            try
            {
                if (productImage!=null && productImage.ContentLength > 0)
                {
                    //string fileName = Path.GetFileName(productImage.FileName);
                    string fileName = Request["returnedProductId"];
                    string extension = Path.GetExtension(productImage.FileName);
                    string path = Path.Combine(Server.MapPath("~/Content/ProductImages"), fileName + extension);
                    productImage.SaveAs(path);
                }
                //ViewBag.Message = "File Uploaded Successfully!!";
                return RedirectToAction("CreateItem", "ItemCreation", new { area = "Inventory" });
            }
            catch
            {
                //ViewBag.Message = "File upload failed!!";
                return RedirectToAction("CreateItem", "ItemCreation", new { area = "Inventory" });
            }
        }

        [HttpPost]
        public ActionResult EditItem(HttpPostedFileBase productImage)
        {
            try
            {
                string fn = Request["returnedProductId"];
                string ext = Path.GetExtension(productImage.FileName);
                var img = Path.GetFileName(Server.MapPath("~/Content/ProductImages/"));
                //DirectoryInfo pth = Server.MapPath("~/Content/ProductImages/" + fn + "."+ ext);
                if (productImage.ContentLength > 0)
                {
                    //string fileName = Path.GetFileName(productImage.FileName);
                    string fileName = Request["returnedProductId"];
                    string extension = Path.GetExtension(productImage.FileName);
                    string path = Path.Combine(Server.MapPath("~/Content/ProductImages"), fileName + extension);
                    productImage.SaveAs(path);
                }
                //ViewBag.Message = "File Uploaded Successfully!!";
                return RedirectToAction("CreateItem", "ItemCreation", new { area = "Inventory" });
            }
            catch
            {
                //ViewBag.Message = "File upload failed!!";
                return RedirectToAction("CreateItem", "ItemCreation", new { area = "Inventory" });
            }
        }
        [HttpGet]
        public FileResult Export()
        {
            //NorthwindEntities entities = new NorthwindEntities();
            DataTable excelData = new DataTable("Grid");
            //Excel.Workbook xlWorkBook;

            var productGroup = new ProductGroupSP().ProductGroupViewAll();
            var size = new SizeSP().SizeViewAll();
            var unit = new UnitSP().UnitViewAll();
            var brand = new BrandSP().BrandViewAll();
            var model = new ModelNoSP().ModelNoViewAll();
            var tax = new TaxSP().TaxViewAll();
            var store = new GodownSP().GodownOnlyViewAll();

            List<ProductGroupInfo> pg = new List<ProductGroupInfo>();
            for (var i = 0; i < productGroup.Rows.Count; i++)
            {
                pg.Add(new ProductGroupInfo
                {
                    GroupId = Convert.ToDecimal(productGroup.Rows[i]["groupId"]),
                    GroupName = productGroup.Rows[i]["groupName"].ToString(),
                    GroupUnder = Convert.ToDecimal(productGroup.Rows[i]["narration"])
                });
            }
            List<SizeInfo> s = new List<SizeInfo>();
            for (var i = 0; i < size.Rows.Count; i++)
            {
                s.Add(new SizeInfo
                {
                    SizeId = Convert.ToDecimal(size.Rows[i]["sizeId"]),
                    Size = size.Rows[i]["size"].ToString()
                });
            }
            List<UnitInfo> u = new List<UnitInfo>();
            for (var i = 0; i < unit.Rows.Count; i++)
            {
                u.Add(new UnitInfo
                {
                    UnitId = Convert.ToDecimal(unit.Rows[i]["unitId"]),
                    UnitName = unit.Rows[i]["unitName"].ToString()
                });
            }
            List<BrandInfo> b = new List<BrandInfo>();
            for (var i = 0; i < brand.Rows.Count; i++)
            {
                b.Add(new BrandInfo
                {
                    BrandId = Convert.ToDecimal(brand.Rows[i]["brandId"]),
                    BrandName = brand.Rows[i]["brandName"].ToString()
                });
            }
            //List<ModelNoInfo> m = new List<ModelNoInfo>();
            //for(var i = 0; i < model.Rows.Count; i++)
            //{
            //    m.Add(new ModelNoInfo {
            //        ModelNoId = Convert.ToDecimal(model.Rows[i]["modelNoId"]),
            //        ModelNo = model.Rows[i]["modelNo"].ToString()
            //    });
            //}
            List<TaxInfo> t = new List<TaxInfo>();
            for (var i = 0; i < tax.Rows.Count; i++)
            {
                t.Add(new TaxInfo
                {
                    TaxId = Convert.ToDecimal(tax.Rows[i]["taxId"]),
                    TaxName = tax.Rows[i]["taxName"].ToString()
                });
            }
            List<GodownInfo> g = new List<GodownInfo>();
            for (var i = 0; i < store.Rows.Count; i++)
            {
                g.Add(new GodownInfo
                {
                    GodownId = Convert.ToDecimal(store.Rows[i]["godownId"]),
                    GodownName = store.Rows[i]["godownName"].ToString()
                });
            }
            excelData.Columns.AddRange(new DataColumn[] { new DataColumn("Product Code"),
                                            new DataColumn("Product Name"),
                                            new DataColumn("Narration"),
                                            new DataColumn("Division"),
                                            new DataColumn("Group"),
                                            new DataColumn("Sub-Group"),
                                            new DataColumn("Category"),
                                            new DataColumn("Sub-Category"),
                                            new DataColumn("Color"),
                                            new DataColumn("Unit"),
                                            new DataColumn("Size"),
                                            new DataColumn("Manufacturer"),
                                            new DataColumn("Tax"),
                                            //new DataColumn("Tax Applicable On"),
                                            new DataColumn("Purchase Rate"),
                                            new DataColumn("Sales Rate"),
                                            //new DataColumn("MRP"),
                                            new DataColumn("Minimum Stock"),
                                            new DataColumn("Maximum Stock"),
                                            new DataColumn("Re-Order Level"),
                                            //new DataColumn("Warehouse"),
                                            new DataColumn("Part No."),
                                            new DataColumn("Product Type"),
                                            new DataColumn("Quantity On Hand")
                                        });

            var products = new ProductSP().ProductViewAll();

            for (var p = 0; p < products.Rows.Count; p++)
            {
                var stockCardLocations = ProductStockCard(Convert.ToDecimal(products.Rows[p]["productId"])).StockCardLocations;
                decimal quantityOnHand = 0;
                foreach(var scl in stockCardLocations)
                {
                    quantityOnHand = quantityOnHand + scl.QuantityOnHand;
                }

                var thisSubCategory = pg.FirstOrDefault(gid => gid.GroupId == Convert.ToDecimal(products.Rows[p]["groupId"]));
                var thisCategory = thisSubCategory == null? null : pg.FirstOrDefault(gid => gid.GroupId == Convert.ToDecimal(thisSubCategory.GroupUnder));
                var thisSubGroup = thisCategory == null? null : pg.FirstOrDefault(gid => gid.GroupId == Convert.ToDecimal(thisCategory.GroupUnder));
                var thisGroup = thisSubCategory == null? null : pg.FirstOrDefault(gid => gid.GroupId == Convert.ToDecimal(thisSubGroup.GroupUnder));
                var thisDivision = thisGroup == null? null : pg.FirstOrDefault(gid => gid.GroupId == Convert.ToDecimal(thisGroup.GroupUnder));
                var thisBrand = b.FirstOrDefault(bid => bid.BrandId == Convert.ToDecimal(products.Rows[p]["brandId"]));
                var thisUnit = u.FirstOrDefault(uid => uid.UnitId == Convert.ToDecimal(products.Rows[p]["unitId"]));
                var thisSize = s.FirstOrDefault(sid => sid.SizeId == Convert.ToDecimal(products.Rows[p]["sizeId"]));
                             // var thisModelNo = m.FirstOrDefault(mid => mid.ModelNoId == Convert.ToDecimal(products.Rows[p]["modelNoId"]));
                var thisTax = t.FirstOrDefault(tid => tid.TaxId == Convert.ToDecimal(products.Rows[p]["taxId"]));
                //var thisStore = g.FirstOrDefault(gdId => gdId.GodownId == Convert.ToDecimal(products.Rows[p]["godownId"]));
                string subCategoryGroupName = thisSubCategory == null ? String.Empty : thisSubCategory.GroupName;
                string categoryGroupName = thisCategory == null ? String.Empty : thisCategory.GroupName;
                string subGroupName = thisSubGroup == null ? String.Empty : thisSubGroup.GroupName;
                string groupGroupName = thisGroup == null ? String.Empty : thisGroup.GroupName;
                string divisionGroupName = thisGroup == null ? String.Empty : thisDivision.GroupName;
                string brandName = thisBrand == null ? String.Empty : thisBrand.BrandName;
                string unitName = thisUnit == null ? String.Empty : thisUnit.UnitName;
                string sizeName = thisSize == null ? String.Empty : thisSize.Size;
                string taxName = thisTax == null ? String.Empty : thisTax.TaxName;
                try
                {
                    excelData.Rows.Add(
                    products.Rows[p]["productCode"].ToString(),
                    products.Rows[p]["productName"].ToString(),
                    products.Rows[p]["narration"].ToString(),
                    divisionGroupName,// thisDivision.GroupName.ToString(),
                    groupGroupName,//thisGroup.GroupName.ToString(),
                    subGroupName,//thisSubGroup.GroupName.ToString(),
                    categoryGroupName,//thisCategory.GroupName.ToString(),
                    subCategoryGroupName,//thisSubCategory.GroupName.ToString(),
                    brandName,//thisBrand.BrandName.ToString(),
                    unitName,//thisUnit.UnitName.ToString(),
                    sizeName,//thisSize.Size.ToString(),
                    products.Rows[p]["extra2"].ToString(),
                    taxName,//thisTax.TaxName.ToString(),
                   // products.Rows[p]["taxapplicableOn"].ToString(),
                    Math.Round(Convert.ToDecimal(products.Rows[p]["purchaseRate"]), 2).ToString("C", new CultureInfo("HA-LATN-NG")),
                    Math.Round(Convert.ToDecimal(products.Rows[p]["salesRate"]) , 2).ToString("C", new CultureInfo("HA-LATN-NG")),
                    //products.Rows[p]["mrp"].ToString(),
                    Math.Round(Convert.ToDecimal(products.Rows[p]["minimumStock"]), 2).ToString(),
                    Math.Round(Convert.ToDecimal(products.Rows[p]["maximumStock"]), 2).ToString(),
                    Math.Round(Convert.ToDecimal(products.Rows[p]["reorderLevel"]), 2).ToString(),
                    //thisStore.GodownName.ToString(),
                    products.Rows[p]["partNo"].ToString(),
                    products.Rows[p]["productType"].ToString(),
                    Math.Round(Convert.ToDecimal(quantityOnHand), 2).ToString());
                }
                catch (Exception e)
                {

                }
                // products.Rows[p]["productType"]);
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(excelData);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Item List.xlsx");
                }
            }
            //string data = null;
            //int i = 0;
            //int j = 0;

            //Excel.Application xlApp;
            //Excel.Worksheet xlWorkSheet;
            //object misValue = System.Reflection.Missing.Value;
            //xlApp = new Excel.Application();
            //xlWorkBook = xlApp.Workbooks.Add(misValue);
            //xlWorkSheet = (Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            //for (i = 0; i <= dt.Rows.Count - 1; i++)
            //{
            //    for (j = 0; j <= dt.Columns.Count - 1; j++)
            //    {
            //        data = dt.Rows[i].ItemArray[j].ToString();
            //        xlWorkSheet.Cells[i + 1, j + 1] = data;
            //    }
            //}
            //using (MemoryStream stream = new MemoryStream())
            //{
            //    xlWorkBook.SaveAs(stream);
            //    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "itemList.xls");
            //}
        }

        //used to get quantities per store
        private StockSummary stockSummary(StockSummarySearch input)
        {
            List<StockSummary> stockSummary = new List<StockSummary>();
            StockSummary response = new StockSummary();

            decimal returnValue = 0;
            try
            {
                DBMatConnection conn = new DBMatConnection();
                StockPostingSP spstock = new StockPostingSP();
                DataTable dtbl = new DataTable();
                decimal productId = input.ProductId;
                string pCode = input.ProductCode.ToString();
                decimal storeId = input.StoreId;
                string batch = input.BatchNo;

                string queryStr = "select top 1 f.fromDate from tbl_FinancialYear f";
                DateTime lastFinYearStart = Convert.ToDateTime(conn.getSingleValue(queryStr));

                //dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, Convert.ToDateTime("04/30/2017") /*PublicVariables._dtToDate*/, pCode, "");
                dtbl = spstock.StockReportDetailsGridFill(productId, storeId, batch, lastFinYearStart, MATFinancials.PublicVariables._dtToDate, pCode, "");

                decimal currentProductId = 0, currentGodownID = 0, prevGodownID = 0;
                decimal prevProductId = 0;
                decimal inwardQty2 = 0, outwardQty2 = 0, inwardqt = 0, outwardqt = 0, rate2 = 0;
                string productName = "", storeName = "", productCode = "";
                decimal qtyBal = 0, stockValue = 0;
                decimal computedAverageRate = 0, previousRunningAverage = 0;
                decimal totalAssetValue = 0, qtyBalance = 0;
                decimal value1 = 0;
                int i = 0;
                bool isAgainstVoucher = false;

                foreach (DataRow dr in dtbl.Rows)
                {
                    currentProductId = Convert.ToDecimal(dr["productId"]);
                    currentGodownID = Convert.ToDecimal(dr["godownId"]);
                    productCode = dr["productCode"].ToString();
                    string voucherType = "", refNo = "";

                    decimal inwardQty = (from p in dtbl.AsEnumerable()
                                         where p.Field<decimal>("productId") == prevProductId
                                         select p.Field<decimal>("inwardQty")).Sum();
                    decimal outwardQty = (from p in dtbl.AsEnumerable()
                                          where p.Field<decimal>("productId") == prevProductId
                                          select p.Field<decimal>("outwardQty")).Sum();

                    inwardQty2 = Convert.ToDecimal(dr["inwardQty"]);
                    outwardQty2 = Convert.ToDecimal(dr["outwardQty"]);
                    decimal purchaseRate = Convert.ToDecimal(dr["purchaseRate"]);
                    string againstVoucherType = Convert.ToString(dr["AgainstVoucherTypeName"]);
                    rate2 = Convert.ToDecimal(dr["rate"]);
                    if (previousRunningAverage != 0 && currentProductId == prevProductId)
                    {
                        purchaseRate = previousRunningAverage;
                    }
                    if (currentProductId == prevProductId)
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        prevProductId = Convert.ToDecimal(dr["productId"]);
                        productCode = dr["productCode"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    else
                    {
                        productName = dr["productName"].ToString();
                        storeName = dr["godownName"].ToString();
                        inwardqt = (from p in dtbl.AsEnumerable()
                                    where p.Field<decimal>("productId") == currentProductId
                                     && p.Field<decimal>("godownId") == currentGodownID
                                    select p.Field<decimal>("inwardQty")).Sum();

                        outwardqt = (from p in dtbl.AsEnumerable()
                                     where p.Field<decimal>("productId") == currentProductId
                                      && p.Field<decimal>("godownId") == currentGodownID
                                     select p.Field<decimal>("outwardQty")).Sum();
                    }
                    //======================== 2017-02-27 =============================== //
                    //if (againstVoucherType != "NA")
                    //{
                    //    voucherType = againstVoucherType;
                    //}
                    //else
                    //{
                    //    voucherType = dr["typeOfVoucher"].ToString();
                    //}
                    refNo = dr["refNo"].ToString();
                    if (againstVoucherType != "NA")
                    {
                        refNo = dr["againstVoucherNo"].ToString();
                        isAgainstVoucher = true;//againstVoucherType = dr["typeOfVoucher"].ToString();
                        againstVoucherType = dr["AgainstVoucherTypeName"].ToString();
                        //---------------- added 20170321 to make it DN DN SI and MR MR PI instead of DN SI SI and MR PI PI
                        string queryString = string.Format("select typeOfVoucher from tbl_VoucherType WHERE voucherTypeId = '{0}'", Convert.ToDecimal(dr["againstVoucherTypeId"].ToString()));
                        string typeOfVoucher = conn.getSingleValue(queryString);
                        if (dr["typeOfVoucher"].ToString().ToLower() == "delivery note" && typeOfVoucher.ToLower() == "sales invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                        if (dr["typeOfVoucher"].ToString().ToLower() == "material receipt" && typeOfVoucher.ToLower() == "purchase invoice")
                        {
                            againstVoucherType = dr["typeOfVoucher"].ToString();
                        }
                    }
                    voucherType = dr["typeOfVoucher"].ToString();
                    //---------  COMMENT END ----------- //
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if (outwardQty2 > 0)
                    {
                        if (voucherType == "Sales Invoice" && outwardQty2 > 0)
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = purchaseRate;
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = Math.Round(computedAverageRate, 2, MidpointRounding.AwayFromZero) * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Material Receipt")
                        {
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue -= outwardQty2 * rate2;
                        }

                        else if (outwardQty2 > 0 && (voucherType == "Rejection Out") || (voucherType == "Purchase Return") || (voucherType == "Sales Return") || (voucherType == "Rejection In" || voucherType == "Stock Transfer"))
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else if (outwardQty2 > 0 && voucherType == "Sales Order")
                        {
                            if (qtyBal == 0)
                            {
                                computedAverageRate = (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * 1;
                            }
                            else
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                        else
                        {
                            if (qtyBal != 0)
                            {
                                computedAverageRate = (stockValue / qtyBal);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                            else
                            {
                                computedAverageRate = previousRunningAverage; // (stockValue / 1);
                                qtyBal += inwardQty2 - outwardQty2;
                                stockValue = computedAverageRate * qtyBal;
                            }
                        }
                    }
                    else if (voucherType == "Sales Return" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Rejection In" && inwardQty2 > 0)
                    {   //added 18112016
                        if (qtyBal == 0)
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * 1;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                    }
                    else if (voucherType == "Sales Invoice" && inwardQty2 > 0)
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;

                        }
                        else
                        {
                            computedAverageRate = previousRunningAverage; // (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue = computedAverageRate * qtyBal;
                        }
                        // voucherType = "Sales Return";
                    }
                    else if (voucherType == "Delivery Note")
                    {
                        if (qtyBal != 0)
                        {
                            computedAverageRate = (stockValue / qtyBal);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = (stockValue / 1);
                            qtyBal += inwardQty2 - outwardQty2;
                            stockValue += inwardQty2 * purchaseRate;
                        }
                    }
                    else
                    {
                        // hndles other transactions such as purchase, opening balance, etc
                        qtyBal += inwardQty2 - outwardQty2;
                        stockValue += (inwardQty2 * rate2);
                    }
                    if (voucherType == "Stock Journal")
                    {
                        voucherType = "Stock Transfer";
                    }
                    if ((currentProductId != prevProductId && i != 0) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        //qtyBal = Convert.ToDecimal(dr["inwardQty"]);
                        //stockValue = inwardQty2 * rate2;
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        if (voucherType == "Sales Invoice")
                        {
                            stockValue = qtyBal * purchaseRate;
                        }
                        else
                        {
                            stockValue = qtyBal * rate2;
                        }
                        //totalAssetValue += Math.Round(value1, 2);
                        totalAssetValue += value1;
                        //i++;
                        returnValue = totalAssetValue;
                    }
                    // -----------------added 2017-02-21 -------------------- //
                    if ((currentProductId != prevProductId) || (prevGodownID != currentGodownID && prevGodownID != 0))
                    {
                        if (voucherType == "Sales Invoice")
                        {
                            computedAverageRate = purchaseRate;
                        }
                        else
                        {
                            computedAverageRate = rate2;
                        }
                        qtyBal = Convert.ToDecimal(dr["inwardQty"]) - Convert.ToDecimal(dr["outwardQty"]);
                        stockValue = qtyBal * computedAverageRate;
                    }
                    // ------------------------------------------------------ //

                    previousRunningAverage = computedAverageRate;

                    value1 = stockValue;
                    prevProductId = currentProductId;
                    prevGodownID = currentGodownID;

                    if (i == dtbl.Rows.Count - 1)
                    {
                        var avgCost = "";
                        if (value1 != 0 && (inwardqt - outwardqt) != 0)
                        {
                            avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                        }
                        else
                        {
                            avgCost = "0.0";
                        }

                        stockSummary.Add(new StockSummary
                        {
                            ProductId = dr["productId"].ToString(),
                            StoreId = dr["godownId"].ToString(),
                            ProductName = dr["productName"].ToString(),
                            StoreName = dr["godownName"].ToString(),
                            PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                            SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                            QtyBalance = (inwardqt - outwardqt).ToString(),
                            AvgCostValue = avgCost,
                            StockValue = value1.ToString("N2"),
                            ProductCode = dr["productCode"].ToString()
                        });
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    else if ((dtbl.Rows[i]["productCode"].ToString() != dtbl.Rows[i + 1]["productCode"].ToString()) || (Convert.ToDecimal(dtbl.Rows[i]["godownId"]) != Convert.ToDecimal(dtbl.Rows[i + 1]["godownId"])))
                    {
                        var avgCost = "";
                        if (value1 != 0 && (inwardqt - outwardqt) != 0)
                        {
                            avgCost = (value1 / (inwardqt - outwardqt)).ToString("N2");
                        }
                        else
                        {
                            avgCost = "0.0";
                        }

                        stockSummary.Add(new StockSummary
                        {
                            ProductId = dr["productId"].ToString(),
                            StoreId = dr["godownId"].ToString(),
                            ProductName = dr["productName"].ToString(),
                            StoreName = dr["godownName"].ToString(),
                            PurchaseRate = Convert.ToDecimal(dr["purchaseRate"]).ToString("N2"),
                            SalesRate = Convert.ToDecimal(dr["rate"]).ToString("N2"),
                            QtyBalance = (inwardqt - outwardqt).ToString(),
                            AvgCostValue = avgCost,
                            StockValue = value1.ToString("N2"),
                            ProductCode = dr["productCode"].ToString()
                        });
                        qtyBalance += (inwardqt - outwardqt);
                    }
                    i++;
                }
                decimal totalStockValue = 0;
                foreach (var row in stockSummary)
                {
                    totalStockValue += Convert.ToDecimal(row.StockValue);
                }

                //not needed since its not needed to be displayed
                //stockSummary.Add(new StockSummary
                //{
                //    ProductName = "",
                //    StoreName = "Total Stock Value:",
                //    PurchaseRate = "",
                //    SalesRate = "",
                //    QtyBalance = qtyBalance.ToString("N2"),
                //    AvgCostValue = (totalStockValue / qtyBalance).ToString("N2"),
                //    StockValue = totalStockValue.ToString("N2"),
                //    ProductCode = ""
                //});
                //stockSummary.Add(new StockSummary
                //{
                //    ProductName = "",
                //    StoreName = "",
                //    PurchaseRate = "",
                //    SalesRate = "",
                //    QtyBalance = "",
                //    AvgCostValue = "",
                //    StockValue = "=============",
                //    ProductCode = ""
                //});

            }
            catch (Exception ex)
            {
                //formMDI.infoError.ErrorString = "STKSR1:" + ex.Message;
            }
            response = stockSummary.FirstOrDefault();
            return response;
        }
        public StockCardVM ProductStockCard(decimal productId)
        {
            MATFinancials.ProductInfo product = new ProductSP().ProductView(productId);
            StockCardVM card = new StockCardVM();
            List<StockCardLocations> cardLocations = new List<StockCardLocations>();

            //SalesOrderMasterSP spSalesOrderMaster = new SalesOrderMasterSP();
            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();
            /*DataTable dt = new DataTable(),*/
            DataTable dt2 = new DataTable();
            DataTable dtbl = new DataTable(), dtbl2 = new DataTable();
            // dt = spSalesOrderMaster.GetSalesOrderNoIncludePendingCorrespondingtoLedgerforSI(0, 0, 0);
            dt2 = spPurchaseMaster.GetOrderNoCorrespondingtoLedgerByNotInCurrPI(0, 0, 0);
            dtbl = SalaryHelper.salesOrderDetails();
            dtbl2 = SalaryHelper.purchaseOrderDetails();
            //if (dt != null && dt.Rows.Count > 0)
            //{
            //    for (int i = 0; i < dt.Rows.Count; i++)
            //    {
            //        DataRow row = dtbl.NewRow();
            //        row["salesOrderMasterId"] = Convert.ToDecimal(dt.Rows[i]["salesOrderMasterId"]);
            //        dtbl.Rows.Add(row);
            //    }
            //}

            if (dt2 != null && dt2.Rows.Count > 0)
            {
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    DataRow row = dtbl2.NewRow();
                    row["purchaseOrderMasterId"] = Convert.ToDecimal(dt2.Rows[i]["purchaseOrderMasterId"]);
                    dtbl2.Rows.Add(row);
                }
            }

            DBMatConnection conn = new DBMatConnection();
            conn.AddParameter("@productId", productId);
            conn.AddParameter("@date", DateTime.Now);
            conn.AddParameter("@salesArrayList", dtbl);
            conn.AddParameter("@purchaseArrayList", dtbl2);
            var quickView = conn.getDataSet("CurrentStockQuickView");

            foreach (DataRow row in quickView.Tables[0].Rows)
            {
                StockSummarySearch param = new StockSummarySearch
                {
                    ToDate = MATFinancials.PublicVariables._dtToDate,
                    ProductId = productId,
                    StoreId = Convert.ToDecimal(row[4].ToString()),
                    BatchNo = "All",
                    ProductCode = "",
                    RefNo = ""
                };
                StockSummary summary = stockSummary(param);
                cardLocations.Add(new StockCardLocations
                {
                    StoreId = Convert.ToDecimal(row[4].ToString()),
                    StoreName = row[2].ToString(),
                    QuantityOnHand = summary == null ? 0 : Convert.ToDecimal(summary.QtyBalance),
                    SalesOrderQuantity = getSalesOrderForStoreProduct(productId, Convert.ToDecimal(row[4].ToString())),
                    //SalesOrderQuantity= getSalesOrderBalanceForStore(productId, Convert.ToDecimal(row[4].ToString()))
                });
            }

            card.ProductCode = product.ProductCode;
            card.ProductId = product.ProductId;
            card.ProductName = product.ProductName;
            card.ProductDescription = product.Narration;
            card.PurchaseOrder = getPurchaseOrderBalanceForProduct(productId);
            card.StockCardLocations = cardLocations;

            return card;
        }

        private decimal getSalesOrderForStoreProduct(decimal productId, decimal storeId)
        {
            DBMatConnection conn = new DBMatConnection();
            string queryStr = string.Format("SELECT SUM(qty) FROM tbl_SalesOrderDetails WHERE productId={0} and extra1='{1}' ", productId, storeId);
            string val1 = conn.getSingleValue(queryStr);
            if (val1 == "" || val1 == string.Empty || val1 == null)
            {
                val1 = "0.0";
            }

            //select * from tbl_deliverynotedetails where productid=1 and godownId=2 and orderdetails1id in(select salesOrderDetailsId from tbl_salesorderdetails where productid=1 and extra1='2')
            string queryStr2 = string.Format("select sum(qty) from tbl_deliverynotedetails where productid={0} and godownId={1} and orderdetails1id in(select salesOrderDetailsId from tbl_salesorderdetails where productid={0} and extra1='{1}')", productId, storeId);
            string val2 = conn.getSingleValue(queryStr2);
            if (val2 == "" || val2 == string.Empty || val2 == null)
            {
                val2 = "0.0";
            }
            decimal salesOrderBalance = Convert.ToDecimal(val1) - Convert.ToDecimal(val2);
            return Convert.ToDecimal(salesOrderBalance);
        }
        private decimal getPurchaseOrderBalanceForProduct(decimal productId)
        {
            PurchaseMasterSP spPurchaseMaster = new PurchaseMasterSP();
            PurchaseOrderDetailsSP spPurchaseOrderDetails = new PurchaseOrderDetailsSP();
            DBMatConnection conn = new DBMatConnection();
            DataSet ds = new DataSet();
            DataTable dt2 = new DataTable();
            DataTable dtbl = new DataTable(), dtbl2 = new DataTable();

            decimal purchaseOrderBalance = 0;

            dt2 = spPurchaseMaster.GetOrderNoCorrespondingtoLedgerByNotInCurrPI(0, 0, 0);
            dtbl2 = SalaryHelper.purchaseOrderDetails();

            if (dt2 != null && dt2.Rows.Count > 0)
            {
                for (int i = 0; i < dt2.Rows.Count; i++)
                {
                    DataRow row = dtbl2.NewRow();
                    row["purchaseOrderMasterId"] = Convert.ToDecimal(dt2.Rows[i]["purchaseOrderMasterId"]);
                    dtbl2.Rows.Add(row);
                }

                // get balances remaining for all the available purchase order for the item
                foreach (DataRow row in dt2.Rows)
                {
                    DataTable dtblDetails = new DataTable();
                    dtblDetails = spPurchaseOrderDetails.PurchaseOrderDetailsViewByOrderMasterIdWithRemainingByNotInCurrPI
                               (Convert.ToDecimal(row["purchaseOrderMasterId"]), 0, 0);
                    purchaseOrderBalance += Convert.ToDecimal(dtblDetails.AsEnumerable().Where(i => i.Field<decimal>("productId") == productId)
                        .Select(i => i.Field<decimal>("qty")).Sum());
                }
            }
            return purchaseOrderBalance;
        }

    }
}