﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Inventory.Controllers
{
    public class PhysicalStockController : Controller
    {
        // GET: Inventory/PhysicalStock
        public ActionResult Index()
        {
            dynamic pageAndItemObj = Authorize.IsAuthorizeWithList("frmPhysicalStockRegister", "frmPhysicalStock", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pageAndItemObj.pagePrivileges;
            ViewBag.itemPrivileges = pageAndItemObj.itemPrivileges;
            return View();
        }
    }
}