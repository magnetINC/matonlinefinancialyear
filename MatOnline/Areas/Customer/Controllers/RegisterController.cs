﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Customer.Controllers
{
    public class RegisterController : Controller
    {
        // GET: Customer/Register
        public ActionResult SalesQuotation()
        {
            return View();
        }
        public ActionResult SalesOrder()
        {
            return View();
        }
        public ActionResult DeliveryNote()
        {
            return View();
        }
        public ActionResult RejectionIn()
        {
            return View();
        }

        public ActionResult SalesInvoice()
        {
            Authorize.IsAuthorize("frmSalesInvoiceRegister", "View", User.Identity.Name); 
            return View();
        }
        public ActionResult SalesReturns()
        {
            return View();
        }
        public ActionResult PDCReceivable()
        {
            return View();
        }
        public ActionResult PDCClearance()
        {
            return View();
        }
        public ActionResult Receipts()
        {
            return View();
        }
        public ActionResult CreditNote()
        {
            return View();
        }
    }
}