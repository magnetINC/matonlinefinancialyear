﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MatOnline.Reports;
using MatOnline.Models;

namespace MatOnline.Areas.Customer.Controllers
{
    public class RejectionInController : Controller
    {
        // GET: Customer/RejectionIn
        public ActionResult Index(int? deliveryTxtId = null)
        {
            try
            {
                Authorize.IsAuthorize("frmRejectionIn", "Save", User.Identity.Name);
                using (var context = new DBMATAccounting_MagnetEntities())
                {
                  
                    if (deliveryTxtId != null)
                    {
                        var trans = context.tbl_DeliveryNoteMaster.Find(deliveryTxtId);
                        if (trans != null)
                        {
                            var ledger = context.tbl_AccountLedger
                                .FirstOrDefault(a => a.ledgerId == trans.ledgerId); 

                            ViewBag.ledgerId = trans.ledgerId;
                            ViewBag.ledgerName = "Agent - Name ";
                            if (ledger != null)
                            {
                                ViewBag.ledgerName = ledger.ledgerName;
                            }

                            ViewBag.vouchureId = trans.voucherNo;
                            ViewBag.txtId = trans.deliveryNoteMasterId;
                        }
                    }
                }
            }
            catch (Exception ex )
            {

            }


            return View();
        }
        public ActionResult RejectionInRegister()
        {
            dynamic pageAndItemObj = Authorize.IsAuthorizeWithList("frmRejectionInRegister", "frmRejectionIn", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pageAndItemObj.pagePrivileges;
            ViewBag.itemPrivileges = pageAndItemObj.itemPrivileges;
            return View();
        }
        public ActionResult RejectionInListing()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            try
            {
                using (var context = new DBMATAccounting_MagnetEntities())
                {
                    var trans = context.tbl_RejectionInMaster.Find(id);
                        if (trans != null)
                        {
                            var ledger = context.tbl_AccountLedger
                                .FirstOrDefault(a => a.ledgerId == trans.ledgerId);

                            ViewBag.ledgerId = trans.ledgerId;
                            ViewBag.ledgerName = "Agent - Name ";
                            if (ledger != null)
                            {
                                ViewBag.ledgerName = ledger.ledgerName;
                            }

                            ViewBag.vouchureId = trans.voucherNo;
                            ViewBag.txtId = trans.deliveryNoteMasterId;
                            ViewBag.masterId = trans.rejectionInMasterId;
                            return View(trans);
                    }
                    }
            }
            catch (Exception ex)
            {

            }


            return View();
        }
    }
}