﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MATClassLibrary;
using MatOnline.Reports;
using MatOnline.Models;

namespace MatOnline.Areas.Customer.Controllers
{
    public class SalesInvoiceController : Controller
    {
        // GET: Customer/SalesInvoice
        public ActionResult Index(int? ledgerId = null, int? txtId= null, int? deliveryTxtId = null)
        {
          
                try
                {
                Authorize.IsAuthorize("frmSalesInvoice", "Save", User.Identity.Name);
                using (var context = new DBMATAccounting_MagnetEntities())
                    {
                        if (ledgerId != null)
                        {
                            var ledger = context.tbl_AccountLedger.Where(a => a.ledgerId == ledgerId).FirstOrDefault();
                            if (ledger != null)
                            {
                                ViewBag.ledgerName = ledger.ledgerName;
                                ViewBag.ledgerId = ledger.ledgerId;
                            }
                        }

                        if (txtId != null)
                        {
                            var trans = context.tbl_SalesOrderMaster.Find(txtId);
                            if (trans != null)
                            {

                                ViewBag.ledgerId = trans.ledgerId;
                                ViewBag.vouchureId = trans.voucherNo;
                                ViewBag.txtId = trans.salesOrderMasterId;
                            }
                        }
                        if (deliveryTxtId != null)
                        {
                            var trans = context.tbl_DeliveryNoteMaster.Find(deliveryTxtId);
                            if (trans != null)
                            {

                                ViewBag.ledgerId = trans.ledgerId;
                                ViewBag.vouchureId = trans.voucherNo;
                                ViewBag.txtId = trans.orderMasterId; 
                            }
                        }
                    }
                
                }
                catch (Exception ex )
                {

                }
           
                return View();
        }

        public ActionResult Edit(int id)
        {

            try
            {
                using (var context = new DBMATAccounting_MagnetEntities())
                {
                    var master = context.tbl_SalesMaster.Find(id);
                    if (master == null) return HttpNotFound();
                    var ledger = context.tbl_AccountLedger.Find(master.ledgerId);
                        if (ledger != null)
                        {
                            ViewBag.ledgerName = ledger.ledgerName;
                            ViewBag.ledgerId = ledger.ledgerId;
                        }
                            ViewBag.vouchureId = master.voucherNo;
                            ViewBag.txtId = master.salesMasterId;
                            return View(master);
                }
            }
            catch (Exception ex)
            {

            }
            return View();
        }

        public ActionResult List()
        {
            Authorize.IsAuthorize("frmSalesInvoiceRegister", "View", User.Identity.Name);
            return View();
        }
    }
}