﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MatOnline.Reports;
using MatOnline.Models;

namespace MatOnline.Areas.Customer.Controllers
{
    public class ReceiptController : Controller
    {
        // GET: Customer/Receipt
        public ActionResult Index()
        {
            Authorize.IsAuthorize("frmReceiptRegister", "View", User.Identity.Name);       
            return View();
        }

        public ActionResult Receipt(int? invoiceTxnId = null)
        {
            try
            {
                Authorize.IsAuthorize("frmReceipts", "Save", User.Identity.Name);
                using (var context = new DBMATAccounting_MagnetEntities())
                {

                    if (invoiceTxnId != null)
                    {
                        var trans = context.tbl_SalesMaster.Find(invoiceTxnId);
                        if (trans != null)
                        {
                            ViewBag.ledgerId = trans.ledgerId;
                            ViewBag.vouchureId = trans.voucherNo;
                            ViewBag.txtId = trans.salesMasterId;
                            ViewBag.amount = trans.totalAmount;
                        }
                    }
               
                }
            }
            catch (Exception ex )
            {

            }

            return View();
        }
        public ActionResult EditReceipt(int id)
        {
            ViewBag.ReceiptId = id;
            return View();
        }
        public ActionResult ApplyCredit()
        {
            return View();
        }
    }
}