﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MatOnline.Reports;
using MatOnline.Models;

namespace MatOnline.Areas.Customer.Controllers
{
    public class SalesReturnController : Controller
    {
        // GET: Customer/SalesReturn
        public ActionResult Index(int? invoicetxtId = null)
        {
            try
            {
                Authorize.IsAuthorize("frmSalesReturn", "Save", User.Identity.Name);
                using (var context = new Reports.DBMATAccounting_MagnetEntities())
                {
                  
                    if (invoicetxtId != null)
                    {
                        var trans = context.tbl_SalesMaster.Find(invoicetxtId);
                        if (trans != null)
                        {
                            var ledger = context.tbl_AccountLedger
                                .FirstOrDefault(a => a.ledgerId == trans.ledgerId); 

                            ViewBag.ledgerId = trans.ledgerId;
                            ViewBag.ledgerName = "Customer - Name ";
                            if (ledger != null)
                            {
                                ViewBag.ledgerName = ledger.ledgerName;
                            }

                            ViewBag.vouchureId = trans.voucherNo;
                            ViewBag.txtId = trans.salesMasterId;
                            ViewBag.vouchureTypeId = trans.voucherTypeId;
                        }
                    }
                }
            }
            catch (Exception ex )
            {

            }


            return View();
           
        }

        public ActionResult Edit(int id)
        {
            try
            {
                using (var context = new DBMATAccounting_MagnetEntities())
                {
                    var trans = context.tbl_SalesReturnMaster.Find(id);
                    if (trans == null) return HttpNotFound( "The Resource you request is not found, please contact service provider");
                    if (trans != null)
                    {
                        var ledger = context.tbl_AccountLedger
                            .FirstOrDefault(a => a.ledgerId == trans.ledgerId);

                        ViewBag.ledgerId = trans.ledgerId;
                        ViewBag.ledgerName = "Customer - Name ";
                        if (ledger != null)
                        {
                            ViewBag.ledgerName = ledger.ledgerName;
                        }

                        ViewBag.vouchureId = trans.voucherNo;
                        ViewBag.txtId = trans.salesReturnMasterId;
                        ViewBag.vouchureTypeId = trans.voucherTypeId;
                    }
                }
            }
            catch (Exception ex)
            {

            }


            return View();

        }
        public ActionResult SalesReturnListing()
        {
            Authorize.IsAuthorize("frmBankSalesReturnRegister", "View", User.Identity.Name);
            return View();
        }
        public ActionResult SalesReturnRegister()
        {
            return View();
        }
    }
}