﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MatOnline.Reports;
using MatOnline.Models;

namespace MatOnline.Areas.Customer.Controllers
{
    public class SalesOrderController : Controller
    {
        // GET: Customer/SalesOrder 
        private DBMATAccounting_MagnetEntities context = new DBMATAccounting_MagnetEntities();
        public ActionResult Index()
        {
            Authorize.IsAuthorizeWithList("frmSalesOrder", "Save", User.Identity.Name);
            return View();
        }

        public ActionResult OrderConfirmationListing()
        {
            return View();
        }

        public ActionResult AllOrderListing()
        {
            return View(); 
        }
        //public ActionResult OrderAuthorizationListing()
        //{
        //    return View();
        //}

        //public ActionResult ReleaseFormListing()
        //{
        //    return View();
        //}

        public ActionResult ReleaseFormDetails()
        {
            return View();
        }

        public ActionResult WaybillGenerationListing()
        {
            return View();
        }

        public ActionResult GenerateWaybill()
        {
            return View();
        }

        public ActionResult SalesOrderRegister()
        {
            Authorize.IsAuthorize("frmSalesOrderRegister", "View", User.Identity.Name);
            return View();
        }
        public ActionResult SalesOrderConfirmationRegister()
        {
            return View();
        }
        public ActionResult SalesOrderRegisterDetails(int id)
        {
            ViewBag.salesOderMasterId = id;
            var orderMaster = context.tbl_SalesOrderMaster.Where(a => a.salesOrderMasterId == id).SingleOrDefault();
            if (orderMaster != null)
            {
                var customer = context.tbl_AccountLedger
                    .FirstOrDefault(a => a.ledgerId == orderMaster.ledgerId);
                ViewBag.CustomerId = customer.ledgerId;
                ViewBag.CustomerName = customer.ledgerName; 

            }
          
            return View();
        }

        public ActionResult OrderToConfirmation()
        {
            return View();
        }

        public ActionResult OrderAuthorizationListing()
        {
            return View();
        }

        public ActionResult ReleaseFormListing()
        {
            return View();
        }
    }

    public class OrderConfirmationDetailsVM
    {
        public int MyProperty { get; set; }
    }
}