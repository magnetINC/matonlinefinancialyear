﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClosedXML;
using ClosedXML.Excel;
using MatApi.Models.Reports.OtherReports;
using MATFinancials.DAL;
using MatApi.Models;
using MATFinancials.Classes.HelperClasses;
using System.Globalization;
using MatOnline.Models;

namespace MatOnline.Areas.Customer.Controllers
{
    public class CustomerCentreController : Controller
    {
        // GET: Customer/CustomerCentre
        public ActionResult Index()
        {
            List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmCustomer", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pagePrivileges;
            return View();
        }
        public FileResult Export()
        {
            DataTable excelData = new DataTable("Grid");

            excelData.Columns.AddRange(new DataColumn[] {
                new DataColumn("Agent Name"),
                new DataColumn("Agent Code"),
                new DataColumn("State"),
                new DataColumn("Opening Balance"),
                new DataColumn("CrOrDr"),
                new DataColumn("Narration"),
                new DataColumn("Maling Name"),
                new DataColumn("Address"),
                new DataColumn("Phone"),
                new DataColumn("Mobile"),
                new DataColumn("Email"),
                new DataColumn("Credit Period"),
                new DataColumn("Credit Limit"),
                new DataColumn("Bank Acc.No."),
                new DataColumn("Branch Name"),
                new DataColumn("Branch Code"),
            });
            DBMatConnection conn = new DBMatConnection();
            string query = string.Format("SELECT * FROM tbl_AccountLedger where accountGroupId = 26");
            var agentList = conn.customSelect(query);

            var state = new AreaSP().AreaViewAll();
            List<AreaInfo> st = new List<AreaInfo>();
            for (var i = 0; i < state.Rows.Count; i++)
            {
                st.Add(new AreaInfo
                {
                    AreaId = Convert.ToDecimal(state.Rows[i]["areaId"]),
                    AreaName = state.Rows[i]["areaName"].ToString()
                });
            }

            for (var p = 0; p < agentList.Rows.Count; p++)
            {
                var thisState = st.FirstOrDefault(stId => stId.AreaId == Convert.ToDecimal(agentList.Rows[p]["areaId"]));
                try
                {
                    excelData.Rows.Add(
                    agentList.Rows[p]["ledgerName"].ToString(),
                    agentList.Rows[p]["extra1"].ToString(),
                    thisState.AreaName.ToString(),
                    agentList.Rows[p]["openingBalance"].ToString(),
                    agentList.Rows[p]["crOrDr"].ToString(),
                    agentList.Rows[p]["narration"].ToString(),
                    agentList.Rows[p]["mailingName"].ToString(),
                    agentList.Rows[p]["address"].ToString(),
                    agentList.Rows[p]["phone"].ToString(),
                    agentList.Rows[p]["mobile"].ToString(),
                    agentList.Rows[p]["email"].ToString(),
                    agentList.Rows[p]["creditPeriod"].ToString(),
                    Math.Round(Convert.ToDecimal(agentList.Rows[p]["creditLimit"]), 2).ToString(),
                    agentList.Rows[p]["bankAccountNumber"].ToString(),
                    agentList.Rows[p]["branchName"].ToString(),
                    agentList.Rows[p]["branchCode"].ToString()
                    );
                }
                catch (Exception e)
                {

                }
            }


            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(excelData);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Agent List.xlsx");
                }
            }
        }
        
        public ActionResult TransactionList()
        {
            return View();
        }
    }
}