﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Customer.Controllers
{
    public class PosController : Controller
    {
        // GET: Customer/Pos
        public ActionResult Index()
        {
            Authorize.IsAuthorize("frmPos", "Save", User.Identity.Name);
            return View();
        }

        public ActionResult ActivityLog()
        {
            return View();
        }
        public ActionResult SalesMan()
        {
            return View();
        }
    }
}