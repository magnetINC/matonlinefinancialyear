﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Report.Controllers.CustomerReports
{
    public class CustomerReportsController : Controller
    {
        // GET: Report/CustomerReports


        public ActionResult CustomerBalance()
        {
            return View();
        }

        public ActionResult AgeingSummary()
        {
            return View();
        }

        public ActionResult SalesSummary()
        {
            return View();
        }

        public ActionResult Shift()
        {
            return View();
        }

        public ActionResult SalesQuotation()
        {
            return View();
        }

        public ActionResult SalesOrder()
        {
            return View();
        }

        public ActionResult Sales()
        {
            return View();
        }

        public ActionResult SalesReturn()
        {
            return View();
        }

        public ActionResult DeliveryNote()
        {
            return View();
        }

        public ActionResult RejectionIn()
        {
            return View();
        }

        public ActionResult Receipt()
        {
            return View();
        }
    }
}