﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Report.Controllers.BankingReports
{
    public class BankingReportsController : Controller
    {
        // GET: Report/BankingReports
        public ActionResult ChequeOrCashDetails()
        {
            return View();
        }

        public ActionResult CashorBankDetails()
        {
            return View();
        }

        public ActionResult BankTransferReport()
        {
            return View();
        }
    }
}