﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Report.Controllers.PayrollReports
{
    public class PayrollReportsController : Controller
    {
        // GET: Report/PayrollReports
        //public ActionResult Index()
        //{
        //    return View();
        //}


        public ActionResult AdvancePayment()
        {
            return View();
        }

        public ActionResult BonusDeduction()
        {
            return View();
        }

        public ActionResult DailyAttendance()
        {
            return View();
        }

        public ActionResult DailySalary()
        {
            return View();
        }

        public ActionResult EmployeeReport()
        {
            return View();
        }

        public ActionResult PayHeadReport()
        {
            return View();
        }

        public ActionResult SalaryPackageDetails()
        {
            return View();
        }

        public ActionResult SalaryPackage()
        {
            return View();
        }

        public ActionResult MonthlySalary()
        {
            return View();
        }

        public ActionResult EmployeeAddressBook()
        {
            return View();
        }

        public ActionResult MonthlyAttendance()
        {
            return View();
        }
    }
}