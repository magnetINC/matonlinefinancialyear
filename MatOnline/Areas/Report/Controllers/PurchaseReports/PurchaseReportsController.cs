﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Report.PurchaseReports
{
    public class PurchaseReportsController : Controller
    {
        // GET: Report/PurchaseReports
        public ActionResult PurchaseSummary()
        {
            return View();
        }

        public ActionResult PurchaseOrder()
        {
            return View();
        }

        public ActionResult MaterialReceipt()
        {
            return View();
        }

        public ActionResult RejectionOut()
        {
             return View();
        }

        public ActionResult PurchaseReturn()
        {
            return View();
        }

        public ActionResult Payment()
        {
            return View();
        }

        public ActionResult Ageing()
        {
            return View();
        }

        public ActionResult SupplierBalance()
        {
            return View();
        }
    }

}