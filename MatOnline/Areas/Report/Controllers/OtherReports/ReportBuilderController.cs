﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MatOnline.Enums;
using Syncfusion.Pdf;
using Syncfusion.HtmlConverter;


namespace MatOnline.Areas.Report.Controllers.OtherReports
{
    public class ReportBuilderController : Controller
    {

        // GET: Report/ReportBuilder
        public ActionResult Index(int id, int reportType)
        {
            ViewBag.id = id;
            ViewBag.reportType = reportType;
            return View();
        } 

        public async System.Threading.Tasks.Task<ActionResult> Index2()
        {

            HtmlToPdfConverter htmlConverter = new HtmlToPdfConverter(HtmlRenderingEngine.WebKit);

            WebKitConverterSettings settings = new WebKitConverterSettings();

            //Set WebKit path
            settings.WebKitPath = Server.MapPath("~/QtBinaries");
            
            //Assign WebKit settings to HTML converter
            htmlConverter.ConverterSettings = settings;

            //Convert URL to PDF
            var hostaddress = "http://" + Request.Url.Authority;
            var url = hostaddress+ Url.Action("Index", "ReportBuilder", new {id = 40067, reportType = 1});
            PdfDocument document = htmlConverter.Convert(url);
            //Save and close the PDF document
            //Response.Write(document);
            document.Save("SalesInvoice.pdf", HttpContext.ApplicationInstance.Response, HttpReadType.Save);

            document.Close(true);
            return View();
        }
    }

}