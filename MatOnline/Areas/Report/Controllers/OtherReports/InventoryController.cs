﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Report.Controllers.OtherReports
{
    public class InventoryController : Controller
    {
        // GET: Report/Inventory
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult StockJournals()
        {
            return View();
        }

        public ActionResult StockVariance()
        {
            return View();
        }

        public ActionResult InventoryMovement()
        {
            return View();
        }

        public ActionResult PhysicalStock()
        {
            Authorize.IsAuthorize("frmPhysicalStockReport", "View", User.Identity.Name); 
            return View();
        }

        public ActionResult StockDetails()
        {
            return View();
        }

        public ActionResult StockSummary()
        {
            return View();
        }

        public ActionResult CustomStockSummary()
        {
            return View();
        }

        public ActionResult InventoryStatistics()
        {
            return View();
        }
        public ActionResult FarmActivityProfitability()
        {
            return View();
        }
        public ActionResult FarmActivityProfitabilityByDepartment()
        {
            return View();
        }
    }
}