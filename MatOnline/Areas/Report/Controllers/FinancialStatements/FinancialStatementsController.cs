﻿using MATClassLibrary.Classes.SP;
using MatOnline.Model;
using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Report.Controllers.FinancialStatements
{
    public class FinancialStatementsController : Controller
    {
        // GET: Report/FinancialS
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult TrialBalance()
        {
            Authorize.IsAuthorize("frmFinancialStatementTrialBalance", "View", User.Identity.Name);
            return View();
        }  
        public ActionResult TrialBalance2()
        {
            return View();
        }

        public ActionResult ProfitAndLoss()
        {
            return View();
        }   
        public ActionResult ProfitAndLoss2()
        {
            return View();
        }
        public ActionResult ProfitAndLossStandard()
        {
            return View();
        }
        public ActionResult ProfitAndLossStandard2()
        {
            return View();
        }
        public ActionResult BalanceSheet()
        {
            return View();
        } 
        
        public ActionResult BalanceSheet2()
        {
            return View();
        }

        public ActionResult BalanceSheet3()
        {
            return View();
        }

        [HttpPost]
        public async Task<ActionResult> GetDateRangeSession(object AllDate)
        {
            try
            {
                var Result = await MatOnline.Areas.Supplier.MatOnlineSession.GetDateRangeSession();

                if (Result !=null)
                    return Json(Result, JsonRequestBehavior.AllowGet);

                return Json("100", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public async Task<ActionResult> ClearDateRangeSession(object AllDate)
        {
            try
            {
                var Result = await MatOnline.Areas.Supplier.MatOnlineSession.ClearDateRangeSession();

                if (Result)
                    return Json("200", JsonRequestBehavior.AllowGet);


                return Json("100", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public async Task<ActionResult> SetDateRangeSession(PickerModel AllDate)
        {
            try
            {

                if(AllDate == null)
                {
                    return Json("100", JsonRequestBehavior.AllowGet);
                }

                var Result = await MatOnline.Areas.Supplier.MatOnlineSession.SetDateRangeSession(AllDate);

                if (Result)
                    return Json("200", JsonRequestBehavior.AllowGet);

                return Json("100", JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}