﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClosedXML;
using ClosedXML.Excel;
using MatApi.Models.Reports.OtherReports;
using MATFinancials.DAL;
using MatApi.Models;
using MATFinancials.Classes.HelperClasses;
using System.Globalization;

namespace MatOnline.Areas.Report.Controllers.GeneralReports
{
    public class GeneralReportsController : Controller
    {
        // GET: Report/GenralReports
        public ActionResult TaxReport()
        {
            return View();
        }
        public FileResult ExportTaxReport(DateTime fdate, DateTime tdate, decimal vid, string vname, decimal tid, string bOrP, bool iOrO)
        {
            DataTable excelData = new DataTable("Grid");

            if(bOrP == "Bill Wise")
            {
                excelData.Columns.AddRange(new DataColumn[] {
                    new DataColumn("Date"),
                    new DataColumn("Voucher Type"),
                    new DataColumn("Voucher No"),
                    new DataColumn("Cash Or Party"),
                    new DataColumn("TIN"),
                    new DataColumn("CST"),
                    new DataColumn("Bill Amount"),
                    new DataColumn("Tax Amount"),
                    new DataColumn("Cess Amount"),
                });


                DataTable dtbl = new DataTable();
                dtbl = new TaxSP().TaxReportGridFillByBillWise(fdate, tdate, tid, vid, vname, iOrO);

                for (var p = 0; p < dtbl.Rows.Count; p++)
                {
                    try
                    {
                        excelData.Rows.Add(
                            dtbl.Rows[p]["Date"].ToString(),
                            dtbl.Rows[p]["TypeofVoucher"].ToString(),
                            dtbl.Rows[p]["VoucherNo"].ToString(),
                            dtbl.Rows[p]["CashOrParty"].ToString(),
                            dtbl.Rows[p]["TIN"].ToString(),
                            dtbl.Rows[p]["CST"].ToString(),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["BillAmount"]), 2).ToString("C", new CultureInfo("HA-LATN-NG")),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["TaxAmount"]), 2).ToString("C", new CultureInfo("HA-LATN-NG")),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["CessAmount"]), 2).ToString("C", new CultureInfo("HA-LATN-NG"))
                        );
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
            else
            {
                excelData.Columns.AddRange(new DataColumn[] {
                    new DataColumn("Date"),
                    new DataColumn("Voucher Type"),
                    new DataColumn("Bill No"),
                    new DataColumn("Item"),
                    new DataColumn("Bill Amount"),
                    new DataColumn("Tax Percent"),
                    new DataColumn("Tax Amount"),
                    new DataColumn("Total Amount"),
                });


                DataTable dtbl = new DataTable();
                dtbl = new TaxSP().TaxReportGridFillByProductwise(fdate, tdate, tid, vid, vname, iOrO);

                for (var p = 0; p < dtbl.Rows.Count; p++)
                {
                    try
                    {
                        excelData.Rows.Add(
                            dtbl.Rows[p]["Date"].ToString(),
                            dtbl.Rows[p]["TypeofVoucher"].ToString(),
                            dtbl.Rows[p]["BillNo"].ToString(),
                            dtbl.Rows[p]["Item"].ToString(),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["BillAmount"]), 2).ToString("C", new CultureInfo("HA-LATN-NG")),
                            dtbl.Rows[p]["TaxPercent"].ToString(),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["TaxAmount"]), 2).ToString("C", new CultureInfo("HA-LATN-NG")),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["TotalAmount"]), 2).ToString("C", new CultureInfo("HA-LATN-NG"))
                        );
                    }
                    catch (Exception e)
                    {

                    }
                }
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(excelData);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Tax Report " + DateTime.Now + " (" + bOrP + ").xlsx");
                }
            }
        }

        public ActionResult VatReturnsReport()
        {
            return View();
        }
        public FileResult ExportVatReturns(DateTime fdate, DateTime tdate, decimal vid, string vname, string tax, string format)
        {
            DataTable excelData = new DataTable("Grid");

            if (format == "type1")
            {
                excelData.Columns.AddRange(new DataColumn[] {
                    new DataColumn("Voucher"),
                    new DataColumn("Voucher No"),
                    new DataColumn("Date"),
                    new DataColumn("Cash Or Party"),
                    new DataColumn("Mailing Name"),
                    new DataColumn("TIN"),
                    new DataColumn("Bill Amount"),
                    new DataColumn("Tax Amount"),
                    new DataColumn("Net Amount"),
                    new DataColumn("Adjust"),
                    new DataColumn("Grand Total"),
                });


                DataTable dtbl = new DataTable();
                dtbl = new VoucherTypeSP().VatGridFill(fdate, tdate, vname, vid, format, tax);

                for (var p = 0; p < dtbl.Rows.Count; p++)
                {
                    try
                    {
                        excelData.Rows.Add(
                            dtbl.Rows[p]["voucherName"].ToString(),
                            dtbl.Rows[p]["Invoice No"].ToString(),
                            dtbl.Rows[p]["Date"].ToString(),
                            dtbl.Rows[p]["Party Name"].ToString(),
                            dtbl.Rows[p]["Mailing Name"].ToString(),
                            dtbl.Rows[p]["Tin No"].ToString(),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["Sales Amound"]), 2).ToString("C", new CultureInfo("HA-LATN-NG")),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["Tax Amount"]), 2).ToString("C", new CultureInfo("HA-LATN-NG")),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["Net Amount"]), 2).ToString("C", new CultureInfo("HA-LATN-NG")),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["billDiscount"]), 2).ToString("C", new CultureInfo("HA-LATN-NG")),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["grandtotal"]), 2).ToString("C", new CultureInfo("HA-LATN-NG"))
                        );
                    }
                    catch (Exception e)
                    {

                    }
                }
            }
            else
            {
                excelData.Columns.AddRange(new DataColumn[] {
                    new DataColumn("Voucher"),
                    new DataColumn("Voucher No"),
                    new DataColumn("Date"),
                    new DataColumn("Cash / Party"),
                    new DataColumn("Mailing Name"),
                    new DataColumn("TIN"),
                    new DataColumn("Grand Total"),
                    new DataColumn("Tax"),
                    new DataColumn("Tax Percent"),
                    new DataColumn("Tax Amount"),
                });


                DataTable dtbl = new DataTable();
                dtbl = new VoucherTypeSP().VatGridFill(fdate, tdate, vname, vid, format, tax);

                for (var p = 0; p < dtbl.Rows.Count; p++)
                {
                    try
                    {
                        excelData.Rows.Add(
                            dtbl.Rows[p]["voucherName"].ToString(),
                            dtbl.Rows[p]["Invoice No"].ToString(),
                            dtbl.Rows[p]["Date"].ToString(),
                            dtbl.Rows[p]["Party Name"].ToString(),
                            dtbl.Rows[p]["Mailing Name"].ToString(),
                            dtbl.Rows[p]["Tin No"].ToString(),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["grandtotal"]), 2).ToString("C", new CultureInfo("HA-LATN-NG")),
                            dtbl.Rows[p]["TaxName"].ToString(),
                            dtbl.Rows[p]["rate"].ToString(),
                            Math.Round(Convert.ToDecimal(dtbl.Rows[p]["TaxAmount"]), 2).ToString("C", new CultureInfo("HA-LATN-NG"))
                        );
                    }
                    catch (Exception e)
                    {

                    }
                }
            }

            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(excelData);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "VAT Return Report " + DateTime.Now + ".xlsx");
                }
            }
        }
    }
}