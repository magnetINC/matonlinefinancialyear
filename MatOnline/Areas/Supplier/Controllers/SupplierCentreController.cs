﻿using MATFinancials;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClosedXML;
using ClosedXML.Excel;
using MatApi.Models.Reports.OtherReports;
using MATFinancials.DAL;
using MatApi.Models;
using MATFinancials.Classes.HelperClasses;
using System.Globalization;
using MatOnline.Models;

namespace MatOnline.Areas.Supplier.Controllers
{
    public class SupplierCentreController : Controller
    {
        // GET: Supplier/SupplierCentre
        public ActionResult Index()
        {
            List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmSupplier", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pagePrivileges;
            return View();
        }
        public FileResult Export()
        {
            DataTable excelData = new DataTable("Grid");

            excelData.Columns.AddRange(new DataColumn[] {
                new DataColumn("Supplier Name"),
                //new DataColumn("Agent Code"),
                //new DataColumn("State"),
                new DataColumn("Opening Balance"),
                new DataColumn("CrOrDr"),
                new DataColumn("Narration"),
                new DataColumn("Maling Name"),
                new DataColumn("Address"),
                new DataColumn("Phone"),
                new DataColumn("Mobile"),
                new DataColumn("Email"),
                new DataColumn("Credit Period"),
                new DataColumn("Credit Limit"),
                new DataColumn("Bank Acc.No."),
                new DataColumn("Branch Name"),
                new DataColumn("Branch Code"),
            });
            DBMatConnection conn = new DBMatConnection();
            string query = string.Format("SELECT * FROM tbl_AccountLedger where accountGroupId = 22");
            var supplierlist = conn.customSelect(query);

            for (var p = 0; p < supplierlist.Rows.Count; p++)
            {
                try
                {
                    excelData.Rows.Add(
                    supplierlist.Rows[p]["ledgerName"].ToString(),
                    //supplierlist.Rows[p]["extra1"].ToString(),
                   // thisState.AreaName.ToString(),
                    supplierlist.Rows[p]["openingBalance"].ToString(),
                    supplierlist.Rows[p]["crOrDr"].ToString(),
                    supplierlist.Rows[p]["narration"].ToString(),
                    supplierlist.Rows[p]["mailingName"].ToString(),
                    supplierlist.Rows[p]["address"].ToString(),
                    supplierlist.Rows[p]["phone"].ToString(),
                    supplierlist.Rows[p]["mobile"].ToString(),
                    supplierlist.Rows[p]["email"].ToString(),
                    supplierlist.Rows[p]["creditPeriod"].ToString(),
                    Math.Round(Convert.ToDecimal(supplierlist.Rows[p]["creditLimit"]), 2).ToString("C", new CultureInfo("HA-LATN-NG")),
                    supplierlist.Rows[p]["bankAccountNumber"].ToString(),
                    supplierlist.Rows[p]["branchName"].ToString(),
                    supplierlist.Rows[p]["branchCode"].ToString()
                    );
                }
                catch (Exception e)
                {

                }
            }


            using (XLWorkbook wb = new XLWorkbook())
            {
                wb.Worksheets.Add(excelData);
                using (MemoryStream stream = new MemoryStream())
                {
                    wb.SaveAs(stream);
                    return File(stream.ToArray(), "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Supplier List.xlsx");
                }
            }
        }
    }
}