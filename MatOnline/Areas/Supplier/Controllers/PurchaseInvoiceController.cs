﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MatOnline.Reports;


namespace MatOnline.Areas.Supplier.Controllers
{
    public class PurchaseInvoiceController : Controller
    {
        DBMATAccounting_MagnetEntities context = new DBMATAccounting_MagnetEntities();
        // GET: Supplier/PurchaseInvoice
        public ActionResult Index()
        {
            Authorize.IsAuthorize("frmPurchaseInvoice", "Save", User.Identity.Name);
            return View();
        }

        public ActionResult Edit(int id)
        {
            var master = context.tbl_PurchaseMaster.Find(id);
            if (master == null) return HttpNotFound();

            ViewBag.Id = master.purchaseMasterId;
            ViewBag.LedgerId = master.ledgerId;

            return View(master);
        }
        public ActionResult Register()
        {
            Authorize.IsAuthorize("frmPurchaseInvoiceRegister", "View", User.Identity.Name);
            return View();
        }
    }
}