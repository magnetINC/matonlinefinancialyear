﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Supplier.Controllers
{
    public class MaterialReceiptController : Controller
    {
        // GET: Supplier/MaterialReceipt
        public ActionResult Index()
        {
            Authorize.IsAuthorize("frmMaterialReceipt", "Save", User.Identity.Name);
            return View(); 
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult MaterialReceiptApproval()
        {
            return View();
        }

        public ActionResult Edit(int id)
        {
            ViewBag.Id = id;
            return View();
        }
    }
}