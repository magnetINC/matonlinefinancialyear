﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MatOnline.Reports;
using MatOnline.Models;

namespace MatOnline.Areas.Supplier.Controllers
{
    public class RejectionOutController : Controller
    {
        DBMATAccounting_MagnetEntities context = new DBMATAccounting_MagnetEntities();

        // GET: Supplier/RejectionOut
        public ActionResult Index()
        {
            Authorize.IsAuthorizeWithList("frmRejectionOut", "Save", User.Identity.Name); 
            return View();
        }

        public ActionResult Edit(int id)
        {
            var record = context.tbl_RejectionOutMaster.Where(a => a.rejectionOutMasterId == id).FirstOrDefault();
            if (record == null) return HttpNotFound("The Resource you requested cannot be found");

            ViewBag.Id = record.rejectionOutMasterId;
            ViewBag.LedgerId = record.ledgerId;
            return View(record);
        }
        public ActionResult Register()
        {
            return View();
        }
    }
}