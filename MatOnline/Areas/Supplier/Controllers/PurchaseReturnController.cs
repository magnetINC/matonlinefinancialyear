﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MatOnline.Reports;

namespace MatOnline.Areas.Supplier.Controllers
{
    public class PurchaseReturnController : Controller
    {
        DBMATAccounting_MagnetEntities context  = new DBMATAccounting_MagnetEntities();
        // GET: Supplier/PurchaseReturn
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Register()
        {
            dynamic pageAndItemObj = Authorize.IsAuthorizeWithList("frmPurchaseReturnRegister", "frmPurchaseReturn", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pageAndItemObj.pagePrivileges;
            ViewBag.itemPrivileges = pageAndItemObj.itemPrivileges;
            return View();
        }

        public ActionResult Edit(int id)
        {
            var master = context.tbl_PurchaseReturnMaster.Find(id);
            if (master == null) return HttpNotFound();

            ViewBag.Id = master.purchaseMasterId;
            return View(master);
        }
    }
}