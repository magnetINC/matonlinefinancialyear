﻿
using DocumentFormat.OpenXml.Drawing;
using DocumentFormat.OpenXml.Drawing.Charts;
using DocumentFormat.OpenXml.ExtendedProperties;
using MATClassLibrary.Classes.SP;
using MatOnline.Areas.MatOnlineSession;
using MatOnline.Reports;
using Newtonsoft.Json;
using Org.BouncyCastle.Asn1.Ocsp;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;

namespace MatOnline.Areas.Supplier
{
    public class MatOnlineSession
    {

        

        public static Boolean IsSignIn = false;
        public const string FinanacialYearSession = "FinanacialYearSession";
        public const string FinanacialYearSession12 = "FinanacialYearSession12";
        public const string DatePickerFinanacialYearSession12 = "DatePickerFinanacialYearSession12";

        public bool IsObjectInDate(DateTime EffectiveDate,DateTime PublicVariablesFromDate,  DateTime PublicVariablesToDate)
        {
            try
            {
                var IsInRightYear = false;
                var effectiveDate = EffectiveDate;
                var ToFinancialYear = PublicVariablesToDate;
                var FromFinancialYear = PublicVariablesFromDate;

                if (/*effectiveDate >= FromFinancialYear &&*/ effectiveDate <= ToFinancialYear)
                {
                    IsInRightYear = true;
                }
                else
                {
                    IsInRightYear = false;
                }

                return IsInRightYear;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static LoginResponseVM loginResponse
        {
            get
            {
                try
                {
                    if (System.Web.HttpContext.Current.Session[FinanacialYearSession] != null)
                    {
                        var data = (string)System.Web.HttpContext.Current.Session[FinanacialYearSession];
                        var newFinancialYear = JsonConvert.DeserializeObject<LoginResponseVM>(data.ToString());

                        newFinancialYear.ToDate.AddDays(+1);
                        newFinancialYear._dtToDate.AddDays(+1);
                        //newFinancialYear._dtToDate.Day = (newFinancialYear._dtToDate.Day + 1);
                        IsSignIn = true;
                        return (newFinancialYear);
                    }
                    else
                    {
                        var uri = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToLower();

                        if (!uri.Contains("/Dashboard/Index".ToLower()) && !uri.Contains("/security/login/".ToLower()) && !uri.EndsWith(".css".ToLower()) && !uri.EndsWith(".js".ToLower()) && uri.Length > 25 && !uri.Contains("favicon.ico".ToLower()) && !uri.Contains("/assets/mat/".ToLower()) && !uri.Contains("/assets/".ToLower()) && !uri.Contains(".jpg".ToLower()) && !uri.Contains(".png".ToLower()) && !uri.Contains(".bmt".ToLower()) && !uri.Contains(".jpg".ToLower()) && !uri.Contains("Scripts/".ToLower()) && !uri.Contains(".jpeg".ToLower()))
                        {
                            if (System.Web.HttpContext.Current.Session[FinanacialYearSession] == null)
                            {
                                //var url = System.Web.HttpContext.Current.Request.Url.AbsoluteUri.ToLower();
                                //.Url.AbsoluteUri.Replace(url, "");
                                System.Web.HttpContext.Current.Response.Redirect("/Security/Login/Logout");
                            }
                            else
                            {
                                IsSignIn = true;
                                return null;
                            }
                        }
                    }

                    IsSignIn = false;
                    return null;
                }
                catch (Exception ex)
                {
                    IsSignIn = false;
                    return null;
                }
            }
        }

        public static bool LoggOutAllSession(string Notices)
        {
            try
            {
                var newContext = new Reports.DBMATAccounting_MagnetEntities();
                var Result = newContext.LoginDates.ToList();

                foreach (var itm in Result)
                {
                    itm.SignOutNoticesBoard = Notices;
                    itm.SignOutPeriod = DateTime.UtcNow;
                    itm.Status = SignOutAuth;
                    newContext.Entry(itm).State = System.Data.Entity.EntityState.Modified;
                    newContext.SaveChanges();
                }

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static int GetLoggInSession()
        {
            try
            {
                var newContext = new Reports.DBMATAccounting_MagnetEntities();
                var Result = newContext.LoginDates.Where(p => p.Status == RunningAuth || p.Status == StoppedAuth).ToList();



                return Result.Count;

            }
            catch (Exception ex)
            {
                return 0;
            }
        }

        public static DBMATAccounting_MagnetEntities newContext = new Reports.DBMATAccounting_MagnetEntities();

        public  MatOnlineSession()
        {
            newContext = new Reports.DBMATAccounting_MagnetEntities();
        }

        public static object LogginBrowserSession()
        {
            object AutoQ = null;
            try
            {
                var sessionID = FormsAuthentication.GetAuthCookie("userId", true);

                string User = sessionID.Value;

                var data = (string)System.Web.HttpContext.Current.Session.SessionID;
                var CurrentDate = DateTime.Now;
                var IPAddress = GetIp();
                newContext = new Reports.DBMATAccounting_MagnetEntities();
                var Result = newContext.LoginDates.Where(p => (p.Status == RunningAuth) /*&& p.UserId == User*/ && p.SessionID == data && (p.IPAddress != IPAddress || p.IPAddress == IPAddress)).ToList();

                if(Result.Count > 0)
                {
                    foreach (var itm in Result)
                    {

                        if((itm.LoginDate.Value.Day < CurrentDate.Day && itm.LoginDate.Value.Month == CurrentDate.Month && itm.LoginDate.Value.Year == CurrentDate.Year) || (itm.LoginDate.Value.Month < CurrentDate.Month && itm.LoginDate.Value.Year == CurrentDate.Year) || itm.LoginDate.Value.Year < CurrentDate.Year)
                        {
                            itm.SignOutPeriod = DateTime.UtcNow;
                            itm.SignOutDate = DateTime.UtcNow;
                            itm.Status = StoppedAuth;
                            newContext.Entry(itm).State = System.Data.Entity.EntityState.Modified;
                            newContext.SaveChanges();
                        }

                        if (itm.LoginDate <= CurrentDate)
                        {
                            var LoginDate = (CurrentDate - itm.LoginDate.Value);

                            DateTime pin = CurrentDate , pout = itm.LoginDate.Value;
                            TimeSpan spanMe = pout.Subtract(pin);

                            //spanMe.Minutes

                            var LessCurrentHours = (itm.LoginDate.Value.Hour);
                            var CurrentHours = CurrentDate.Hour;

                            var ReHours = (CurrentHours - LessCurrentHours);

                            if (spanMe.Hours > 6)
                            {
                                itm.SignOutPeriod = DateTime.UtcNow;
                                itm.SignOutDate = DateTime.UtcNow;
                                itm.Status = StoppedAuth;
                                newContext.Entry(itm).State = System.Data.Entity.EntityState.Modified;
                                newContext.SaveChanges();
                            }
                        }
                    }
                }
                else
                {
                    LoginDates newLgin = new LoginDates();
                    newLgin.LoginDate = DateTime.Now;
                    newLgin.IPAddress = IPAddress;
                    newLgin.BroswerID = sessionID.Value;
                    newLgin.SessionID = data;
                    newLgin.Status = RunningAuth;
                    newLgin.UserId = User;
                    newContext.LoginDates.Add(newLgin);
                    newContext.SaveChanges();
                }

                newContext = new Reports.DBMATAccounting_MagnetEntities();
                Result = newContext.LoginDates.Where(p => (p.Status == SignOutAuth) /*&& p.UserId == User*/ && p.SessionID == data && (p.IPAddress != IPAddress || p.IPAddress == IPAddress)).ToList();

                if(Result.Count > 0)
                {
                    var LastLoginSession = Result[Result.Count - 1];

                    if (LastLoginSession.Status == SignOutAuth)
                    {
                        AutoQ = new { HasAutoSignOut = true, Notices = LastLoginSession.SignOutNoticesBoard };

                    }
                    else if (LastLoginSession.Status == RunningAuth)
                    {
                        AutoQ = new { HasAutoSignOut = false };
                    }
                    else
                    {
                        AutoQ = new { HasAutoSignOut = false };
                    }

                }
                else
                {
                    AutoQ = new { HasAutoSignOut = false };
                }

                return AutoQ;
            }
            catch (Exception ex)
            {
                AutoQ = new { HasAutoSignOut = false };

                return AutoQ;
            }
        }

        public static string GetIp()
        {
            string ip = System.Web.HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"];
            if (string.IsNullOrEmpty(ip))
            {
                ip = System.Web.HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"];
            }
            return ip;
        }

        public const string SignOutAuth = "Generic SignOut";
        public const string RunningAuth = "Running Auth";
        public const string StoppedAuth = "Stopped Auth";

        public static void FillInSession(string datas)
        {
            try
            {

                if (System.Web.HttpContext.Current.Session[FinanacialYearSession12] == null)
                {
                    System.Web.HttpContext.Current.Session.Remove(MatOnline.Areas.Supplier.MatOnlineSession.FinanacialYearSession);
                    System.Web.HttpContext.Current.Session.Add(MatOnline.Areas.Supplier.MatOnlineSession.FinanacialYearSession, datas);
                }
                
                if (System.Web.HttpContext.Current.Session[FinanacialYearSession12] != null)
                {
                    var data = (string)System.Web.HttpContext.Current.Session[FinanacialYearSession12];
                   
                    var newFinancialYear = JsonConvert.DeserializeObject<LoginResponseVM>(data.ToString());

                    var newFinancialYear12 = JsonConvert.DeserializeObject<LoginResponseVM>(datas.ToString());

                    newFinancialYear12.FinancialYearDisplay = newFinancialYear.FinancialYearDisplay;
                    newFinancialYear12.FinancialYear = newFinancialYear.FinancialYear;
                    newFinancialYear12._dtToDate = newFinancialYear._dtToDate;
                    newFinancialYear12._dtFromDate = newFinancialYear._dtFromDate;
                    newFinancialYear12._decCurrentFinancialYearId = newFinancialYear._decCurrentFinancialYearId;
                    newFinancialYear12.CurrentFinicialId = newFinancialYear.CurrentFinicialId;
                    newFinancialYear12.ToDate = newFinancialYear.ToDate;
                    newFinancialYear12.FromDate = newFinancialYear.FromDate;
                    newFinancialYear12.CurrentFinicialYearId = newFinancialYear.CurrentFinicialYearId;
                    newFinancialYear12.IsInCurrentFinancialYear = false;

                    System.Web.HttpContext.Current.Session.Remove(MatOnline.Areas.Supplier.MatOnlineSession.FinanacialYearSession);
                   
                    System.Web.HttpContext.Current.Session.Add(MatOnline.Areas.Supplier.MatOnlineSession.FinanacialYearSession, JsonConvert.SerializeObject(newFinancialYear12));
                  
                    System.Web.HttpContext.Current.Session.Remove(MatOnline.Areas.Supplier.MatOnlineSession.FinanacialYearSession12);
                }
            }
            catch(Exception ex)
            {

            }
        }

        public static void FillInPerviousSession(string datas)
        {
            try
            {
                System.Web.HttpContext.Current.Session.Remove(MatOnline.Areas.Supplier.MatOnlineSession.FinanacialYearSession12);
                System.Web.HttpContext.Current.Session.Add(MatOnline.Areas.Supplier.MatOnlineSession.FinanacialYearSession12, datas);
            }
            catch (Exception ex)
            {

            }
        }

        public static Reports.tbl_FinancialYear GetFinancials()
        {
            try
            {
                var newContext = new Reports.DBMATAccounting_MagnetEntities();
                var Result = newContext.tbl_FinancialYear.Where( f => f.IsFinanacialYearExpired.Value).ToList();

                if (Result == null || Result.Count <= 0)
                {
                    return null;
                }

                if (Result != null && Result.Count > 0)
                {
                    return Result[Result.Count - 1];
                }

                return null;
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public static List<Reports.tbl_FinancialYear> GetFinancialYear()
        {
            try
            {
                var newContext = new Reports.DBMATAccounting_MagnetEntities();
                var Result = newContext.tbl_FinancialYear.ToList();

               
                return Result;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

        public static async Task<bool> SetDateRangeSession(PickerModel AllDate)
        {
            try
            {
                System.Web.HttpContext.Current.Session.Remove(MatOnline.Areas.Supplier.MatOnlineSession.DatePickerFinanacialYearSession12);
                System.Web.HttpContext.Current.Session.Add(MatOnline.Areas.Supplier.MatOnlineSession.DatePickerFinanacialYearSession12, JsonConvert.SerializeObject(AllDate));

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<bool> ClearDateRangeSession()
        {
            try
            {
                System.Web.HttpContext.Current.Session.Remove(MatOnline.Areas.Supplier.MatOnlineSession.DatePickerFinanacialYearSession12);

                return true;
            }
            catch (Exception ex)
            {
                return false;
            }
        }

        public static async Task<PickerModel> GetDateRangeSession()
        {
            try
            {
                if(System.Web.HttpContext.Current.Session[MatOnline.Areas.Supplier.MatOnlineSession.DatePickerFinanacialYearSession12] != null)
                {
                    var sessV = (string)System.Web.HttpContext.Current.Session[MatOnline.Areas.Supplier.MatOnlineSession.DatePickerFinanacialYearSession12];

                    var SessionID = JsonConvert.DeserializeObject<PickerModel>(sessV);

                    System.Web.HttpContext.Current.Session.Remove(MatOnline.Areas.Supplier.MatOnlineSession.DatePickerFinanacialYearSession12);

                    return SessionID;
                }
                return null;
            }
            catch (Exception ex)
            {
                return null;
            }
        }
    }
}