﻿using MatApi.Models.Security;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Results;
using System.Web.Mvc;
using System.Web.Security;

namespace MatOnline.Areas.Security.Controllers
{
    public class LoginController : Controller
    {
        // GET: Security/Login
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult Login()
        {
            

            return View();
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            Session.Clear();

            return Redirect("/Security/Login/Login");
        }

        //[HttpPost]
        //public ActionResult ProcessLogin()
        //{
        //    FormsAuthentication.SetAuthCookie(Request["userId"], true);
        //    //return Redirect("/General/Dashboard/Index");

        //    return RedirectToAction("Index", "Dashboard", new {area = "General"});
        //    // return Json("/General/Dashboard/Index");
        //}
        [HttpPost]
        public JsonResult ProcessLogin(string userId)
        {
            FormsAuthentication.SetAuthCookie(Request["userId"], true);
            //return Redirect("/General/Dashboard/Index");

            return Json("/General/Dashboard/Index");
        }

        [HttpPost]
        public ActionResult ConfirmSessionLogins(string data)
        {
            try
            {
                var fad = MatOnline.Areas.Supplier.MatOnlineSession.GetLoggInSession();

                return Json(fad, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(0);
            }
        }

        [HttpPost]
        public ActionResult RunSessionLogins(string userId)
        {
            try
            {
                var fad = MatOnline.Areas.Supplier.MatOnlineSession.LogginBrowserSession();

                return Json(fad, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { HasAutoSignOut = false });
            }
        }

        [HttpPost]
        public ActionResult SetFinancialYearSession(string data)
        {
            try
            {
                MatOnline.Areas.Supplier.MatOnlineSession.FillInSession(data);



                return Json("True");
            }
            catch(Exception ex)
            {
                return Json("False");
            }
        }

        [HttpPost]
        public ActionResult SetPreviousFinancialYearSession(string data)
        {
            try
            {
                MatOnline.Areas.Supplier.MatOnlineSession.FillInPerviousSession(data);

                return Json("True");
            }
            catch (Exception ex)
            {
                return Json("False");
            }
        }

        [HttpPost]
        public ActionResult CheckCurrentFinancialYear(string userId)
        {
            try
            {
                var newContext = new Reports.DBMATAccounting_MagnetEntities();
                var Result = newContext.tbl_FinancialYear.Where(u => u.IsFinanacialYearExpired == true).FirstOrDefault();

                if(Result != null)
                {
                    var ResultList = newContext.tbl_FinancialYear.ToList();
                    if(ResultList != null && ResultList.Count > 0)
                    {
                        var LastFin = ResultList[ResultList.Count - 1];
                        if(LastFin != null)
                        {
                            if(Result.toDate != LastFin.toDate && Result.fromDate != LastFin.fromDate)
                            {
                                return Json(new { IsCurrenty = true, FinancialDate = Result.fromDate.Value.ToShortDateString() + "-" + Result.toDate.Value.ToShortDateString() }, JsonRequestBehavior.AllowGet);
                            }
                        }
                    }
                }

                return Json(new { IsCurrenty = false, FinancialDate = "" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { IsCurrenty = false, FinancialDate = "" }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult CheckIfSesstionExit(string userId)
        {
            try
            {
                var fad = MatOnline.Areas.Supplier.MatOnlineSession.loginResponse;
                if(fad==null)
                {
                    return Json(new { HasAutoSignOut = true });
                }
                else
                {
                    return Json(new { HasAutoSignOut = false });
                }
                return Json(new { HasAutoSignOut = false });
            }
            catch (Exception ex)
            {
                return Json(new { HasAutoSignOut = false });
            }
        }

        [HttpGet]
        public ActionResult AccessDenied()
        {
            return View();
        }

        [HttpGet]
        public ActionResult ChangePassword()
        {
            return View();
        }
    }
}