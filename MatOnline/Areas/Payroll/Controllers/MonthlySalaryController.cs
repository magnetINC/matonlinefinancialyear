﻿using MatOnline.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Payroll.Controllers
{
    public class MonthlySalaryController : Controller
    {
        // GET: Payroll/MonthlySalary
        public ActionResult MonthlySalarySettings()
        {
            List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmMonthlySalarySettings", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pagePrivileges;
            return View();
        }

        public ActionResult ProcessMonthlySalary()
        {
            Authorize.IsAuthorize("frmMonthlySalaryVoucher", "Save", User.Identity.Name);
            return View();
        }

        //public async Task<ActionResult> ProcessMonthlySalary()
        //{
        //    Authorize.IsAuthorize("frmMonthlySalaryVoucher", "Save", User.Identity.Name);
        //    HttpClient client;
        //    string url = "/MonthlySalary/GetLookUps";
        //    client = new HttpClient();
        //    client.BaseAddress = new Uri("http://localhost:30170");
        //    client.DefaultRequestHeaders.Accept.Add(new System.Net.Http.Headers.MediaTypeWithQualityHeaderValue("application/json"));
        //    HttpResponseMessage response = client.GetAsync(url).Result;

        //    dynamic FromURL = response.Content.ReadAsStringAsync().Result;

        //    return Json(new { response = true, result = response            }, JsonRequestBehavior.AllowGet);
        //}

    }
}