﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Payroll.Controllers
{
    public class AttendanceController : Controller
    {
        // GET: Payroll/Attendance
        public ActionResult Index()
        {
            List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmAttendance", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pagePrivileges;
            return View();
        }
    }
}