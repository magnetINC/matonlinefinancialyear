﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Payroll.Controllers
{
    public class BonusOrDeductionController : Controller
    {
        // GET: Payroll/BonusOrDeduction
        public ActionResult Index()
        {
            Authorize.IsAuthorize("frmBonusDeduction", "Save", User.Identity.Name);
            return View();
        }
    }
}