﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Payroll.Controllers
{
    public class EmployeeController : Controller
    {
        // GET: Payroll/Employee
        public ActionResult EmployeeCreation()
        {
            Authorize.IsAuthorize("frmEmployeeCreation", "Save", User.Identity.Name);
            return View();
        }
        public ActionResult Register()
        {
            dynamic pageAndItemObj = Authorize.IsAuthorizeWithList("frmEmployeeRegister", "frmEmployeeCreation", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pageAndItemObj.pagePrivileges;
            ViewBag.itemPrivileges = pageAndItemObj.itemPrivileges;
            return View();
        }
    }
}