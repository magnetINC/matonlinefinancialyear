﻿using MatOnline.Models;
using MatOnline.Reports;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Payroll.Controllers
{
    public class SalaryPackageController : Controller
    {
        DBMATAccounting_MagnetEntities context = new DBMATAccounting_MagnetEntities();
        // GET: Payroll/SalaryPackage
        public ActionResult Index()
        {
            Authorize.IsAuthorize("frmSalaryPackageCreation", "Save", User.Identity.Name);
            return View();
        }
        public ActionResult Register()
        {
            dynamic pageAndItemObj = Authorize.IsAuthorizeWithList("frmSalaryPackageRegister", "frmSalaryPackageCreation", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pageAndItemObj.pagePrivileges;
            ViewBag.itemPrivileges = pageAndItemObj.itemPrivileges;
            return View();
        }
        public ActionResult Edit(int id)
        {
            var master = context.tbl_SalaryPackage.Find(id);
            if (master == null) return HttpNotFound();

            ViewBag.Id = master.salaryPackageId;
            //ViewBag.LedgerId = master.payHeadId;

            return View(master);
        }
    }
}