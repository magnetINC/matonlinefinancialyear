﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Payroll.Controllers
{
    public class PaySlipController : Controller
    {
        // GET: Payroll/PaySlip
        public ActionResult Index()
        {
            Authorize.IsAuthorize("frmPayslip", "Save", User.Identity.Name);
            return View();
        }
    }
}