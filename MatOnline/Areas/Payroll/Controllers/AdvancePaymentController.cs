﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Payroll.Controllers
{
    public class AdvancePaymentController : Controller
    {
        // GET: Payroll/AdvancePayment
        public ActionResult Index()
        {
            Authorize.IsAuthorize("frmAdvancePayment", "Save", User.Identity.Name);
            return View();
        }
        public ActionResult AdvancePaymentListing()
        {
            return View();
        }
        public ActionResult Register()
        {
            return View();
        }
    }
}