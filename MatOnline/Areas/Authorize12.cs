﻿using MATFinancials.DAL;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;

namespace MatOnline.Areas
{
    public static class Authorize12
    {
        public static void IsAuthorize(string pageName, string action, string userId)
        {
            //if(userId>1)    //user is not admin
            // {
            var context = new RequestContext(new HttpContextWrapper(System.Web.HttpContext.Current), new RouteData());
            var urlHelper = new UrlHelper(context);
            if (userId == "")
            {
                //redirect back to login
                HttpContext.Current.Response.Redirect("/Security/Login/Login");
            }
            DBMatConnection con = new DBMatConnection();
            string userQuery = string.Format("SELECT * from tbl_User WHERE userId = {0}", Convert.ToDecimal(userId));
            var user = con.customSelect(userQuery);
            decimal roleId = Convert.ToDecimal(user.Rows[0]["roleId"]);
            //rolePriviledges = db.tbl_Privilege.Where(p => p.roleId == roleId).ToList();
            string query = string.Format("SELECT * FROM tbl_Privilege WHERE roleId={0} AND formName='{1}' AND action='{2}'", roleId, pageName, action);
            var dt = con.ExecuteQuery(query);
            if (dt.Tables[0].Rows.Count < 1)
            {
                //System.Web.Mvc.RedirectResult("");
                //Redirect("");

                //var url = urlHelper.Action("Index", new { OtherParm = "other value" });
                HttpContext.Current.Response.Redirect("/Security/Login/AccessDenied");
            }
            //  }
        }
        public static List<string> IsAuthorizeWithList(string pageName, string action, string userId)
        {
            var context = new RequestContext(new HttpContextWrapper(System.Web.HttpContext.Current), new RouteData());
            var urlHelper = new UrlHelper(context);
            if (userId == "")
            {
                //redirect back to login
                HttpContext.Current.Response.Redirect("/Security/Login/Login");
            }
            var db = new Reports.DBMATAccounting_MagnetEntities();
            DBMatConnection con = new DBMatConnection();
            List<Reports.tbl_Privilege> rolePrivileges;
            var pagePrivileges = new List<string>();
            //List<Reports.tbl_Role> roles = db.tbl_Role.ToList();
            string userQuery = string.Format("SELECT * from tbl_User WHERE userId = {0}", Convert.ToDecimal(userId));
            var user = con.customSelect(userQuery);
            decimal roleId = Convert.ToDecimal(user.Rows[0]["roleId"]);
            rolePrivileges = db.tbl_Privilege.Where(p => p.roleId == roleId).ToList();
            if (rolePrivileges.Any(p => p.formName == pageName && p.action == action) == false)
            {
                HttpContext.Current.Response.Redirect("/Security/Login/AccessDenied");
            }
            else
            {
                pagePrivileges = rolePrivileges.Where(p => p.formName == pageName).Select(p => p.action).ToList();
            }

            return pagePrivileges;
        }
        public static dynamic IsAuthorizeWithList(string pageName, string itemName, string action, string userId)
        {
            var context = new RequestContext(new HttpContextWrapper(System.Web.HttpContext.Current), new RouteData());
            var urlHelper = new UrlHelper(context);
            if (userId == "")
            {
                //redirect back to login
                HttpContext.Current.Response.Redirect("/Security/Login/Login");
            }
            var db = new Reports.DBMATAccounting_MagnetEntities();
            DBMatConnection con = new DBMatConnection();
            List<Reports.tbl_Privilege> rolePrivileges;
            dynamic pageAndItemObj = new ExpandoObject();
            var pagePrivileges = new List<string>();
            var itemPrivileges = new List<string>();
            //List<Reports.tbl_Role> roles = db.tbl_Role.ToList();
            string userQuery = string.Format("SELECT * from tbl_User WHERE userId = {0}", Convert.ToDecimal(userId));
            var user = con.customSelect(userQuery);
            decimal roleId = Convert.ToDecimal(user.Rows[0]["roleId"]);
            rolePrivileges = db.tbl_Privilege.Where(p => p.roleId == roleId).ToList();
            if (rolePrivileges.Any(p => p.formName == pageName && p.action == action) == false)
            {
                HttpContext.Current.Response.Redirect("/Security/Login/AccessDenied");
            }
            else
            {
                pageAndItemObj.pagePrivileges = rolePrivileges.Where(p => p.formName == pageName).Select(p => p.action).ToList();
                pageAndItemObj.itemPrivileges = rolePrivileges.Where(p => p.formName == itemName).Select(p => p.action).ToList();
            }

            return pageAndItemObj;
        }
    }
}