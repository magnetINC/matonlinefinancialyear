﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Company.Controllers.Masters.Account
{
    public class AccountGroupController : Controller
    {
        // GET: Company/AccountGroup
        public ActionResult Index()
        {
            List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmAccountGroup", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pagePrivileges;
            return View();
        }
    }
}