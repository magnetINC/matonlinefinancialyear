﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Company.Controllers.Masters
{
    public class SalesPointController : Controller
    {
        // GET: Company/SalesPoint
        public ActionResult Index()
        {
            List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmSalesPoint", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pagePrivileges;
            return View();
        }
    }
}