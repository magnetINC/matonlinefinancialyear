﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Company.Controllers.Settings
{
    public class UserCreationController : Controller
    {
        // GET: Company/UserCreation
        public ActionResult Index()
        {
            List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmUserCreation", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pagePrivileges;
            return View();
        }
        [HttpPost]
        public ActionResult Index(HttpPostedFileBase signatureImage)
        {
            try
            {
                if (signatureImage.ContentLength > 0)
                {
                    //string fileName = Path.GetFileName(productImage.FileName);
                    string fileName = Request["returnedUserId"];
                    string extension = Path.GetExtension(signatureImage.FileName);
                    string path = Path.Combine(Server.MapPath("~/Content/Signatures"), fileName + extension);
                    signatureImage.SaveAs(path);
                }
                //ViewBag.Message = "File Uploaded Successfully!!";
                return RedirectToAction("Index", "UserCreation", new { area = "Company" });
            }
            catch
            {
                //ViewBag.Message = "File upload failed!!";
                return RedirectToAction("Index", "UserCreation", new { area = "Company" });
            }
        }
    }
}