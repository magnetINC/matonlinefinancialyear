﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Company.Controllers.Settings
{
    public class RolePriviledgeSettingsController : Controller
    {
        // GET: Company/RolePriviledgeSettings
        public ActionResult Index()
        {
            if (Convert.ToDecimal(User.Identity.Name) > 1)    //user is not admin
            {
                Authorize.IsAuthorize("frmRolePriviledgeSettings", "View", User.Identity.Name);            
            }
            return View();
        }
    }
}