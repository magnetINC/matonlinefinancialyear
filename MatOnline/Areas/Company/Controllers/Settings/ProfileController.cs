﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using MatOnline.Reports;
using MatOnline.Models;

namespace MatOnline.Areas.Company.Controllers.Settings
{
    public class ProfileController : Controller
    {
        // GET: Company/Profile 

        DBMATAccounting_MagnetEntities context = new DBMATAccounting_MagnetEntities();
        public ActionResult Index()
        {
            try
            {
                List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmCompany", "View", User.Identity.Name);
                ViewBag.pagePrivileges = pagePrivileges;
                var profile = context.tbl_Company.FirstOrDefault();
                return View(profile);
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        public ActionResult EditProfile(int id)
        {
            try
            {
                Authorize.IsAuthorize("frmCompany", "Update", User.Identity.Name);
                var find = context.tbl_Company.Where(a => a.companyId == id).FirstOrDefault();
                return View(find);
            }
            catch(Exception ex)
            {
                return null;
            }
        }

        [HttpPost]
        public ActionResult EditProfile(tbl_Company company)
        {
            try
            {
                var find = context.tbl_Company.Where(a => a.companyId == company.companyId).FirstOrDefault();
                if (find != null)
                {
                    find.currencyId = company.currencyId;
                    find.extra2 = company.extra2;
                    find.address = company.address;
                    find.booksBeginingFrom = company.booksBeginingFrom;
                    find.companyName = company.companyName;
                    find.web = company.web;
                    find.country = company.country;
                    find.financialYearFrom = company.financialYearFrom;
                    find.emailId = company.emailId;
                    find.state = company.state;
                    find.mailingName = company.mailingName;
                    find.phone = company.phone;

                    var ProdEffectiveDates = context.tbl_Product.Where(a => a.isopeningstock == true).ToList();

                    if (ProdEffectiveDates.Count > 0)
                    {
                        foreach (var itm in ProdEffectiveDates)
                        {
                            itm.effectiveDate = company.financialYearFrom;
                            context.Entry(itm).State = EntityState.Modified;
                            context.SaveChanges();
                        }
                    }


                    context.Entry(find).State = EntityState.Modified;
                    context.SaveChanges();
                }

                return RedirectToAction("Index");
            }
            catch(Exception ex)
            {
                return RedirectToAction("Index");
            }
        }
    }
}