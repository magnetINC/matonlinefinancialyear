﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Company.Controllers.Settings
{
    public class FinancialYearController : Controller
    {

        // GET: Company/FinancialYear
        public ActionResult Index()
        {
            List<string> pagePrivileges = Authorize.IsAuthorizeWithList("frmFinancialYear", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pagePrivileges;
            return View();
        }

        public ActionResult Create()
        {
            Authorize.IsAuthorize("frmFinancialYear", "Save", User.Identity.Name);
            return View();
        }
    }
}