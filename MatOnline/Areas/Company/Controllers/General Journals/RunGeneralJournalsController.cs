﻿using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Company.Controllers.General_Journals
{
    public class RunGeneralJournalsController : Controller
    {
        // GET: Company/RunGeneralJournals
        public ActionResult Index()
        {
            Authorize.IsAuthorize("frmGeneralJournal", "Save", User.Identity.Name);
            return View();
        }

        public ActionResult JournalRegister()
        {
            dynamic pageAndItemObj = Authorize.IsAuthorizeWithList("frmJournalRegister", "frmGeneralJournal", "View", User.Identity.Name);
            ViewBag.pagePrivileges = pageAndItemObj.pagePrivileges;
            ViewBag.itemPrivileges = pageAndItemObj.itemPrivileges;
            return View();
        }
    }
}