﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Banking.Controllers
{
    public class BankRegistersController : Controller
    {
        // GET: Banking/BankRegisters
        public ActionResult BankRegister()
        {
            return View();
        }
        public ActionResult CashRegister()
        {
            return View();
        }
        public ActionResult BankTransferRegister()
        {
            return View();
        }
        public ActionResult OtherRegister()
        {
            return View();
        }
    }
}