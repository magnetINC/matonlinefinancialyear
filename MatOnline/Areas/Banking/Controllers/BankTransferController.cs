﻿using MatOnline.Areas.Banking.Models;
using MatOnline.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace MatOnline.Areas.Banking.Controllers
{
    public class BankTransferController : Controller
    {
        // GET: Banking/BankTransfer
        public ActionResult Index()
        {
            Authorize.IsAuthorize("frmBankTransfer", "Save", User.Identity.Name);
            return View();
        }

        [HttpPost]
        public ActionResult EditBankTransfer(BankTransferVM input)
        {
            ViewBag.BankTransferParam = input;
            return View(input);
        }
    }
}