﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatOnline.Areas.Banking.Models
{
    public class BankTransferVM
    {
        public decimal LedgerId { get; set; }
        public decimal MasterId { get; set; }
    }
}