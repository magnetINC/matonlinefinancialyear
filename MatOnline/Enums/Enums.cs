﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace MatOnline.Enums
{
    public enum ReportType
    {
        SalesInvoice, SalesOrder, DeliveryNote, RejectionIn, Receipt, GeneralJournal, PettyExpenses
    }
}