﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.Security;

namespace MatOnline
{
    public class MvcApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            
            AreaRegistration.RegisterAllAreas();
            RouteConfig.RegisterRoutes(RouteTable.Routes);
        }

        protected void Application_BeginRequest(Object sender, EventArgs e)
        {
          
        }

        protected void Application_EndRequest(Object sender, EventArgs e)
        {
            try
            {

                //var u = Request.ServerVariables("HTTP_USER_AGENT");

                var uri = Request.Url.AbsoluteUri.ToLower();

                if (!uri.Contains("/Dashboard/Index".ToLower()) && !uri.Contains("/security/login/".ToLower()) && !uri.EndsWith(".css".ToLower()) && !uri.EndsWith(".js".ToLower()) && uri.Length > 25 && !uri.Contains("favicon.ico".ToLower()) && !uri.Contains("/assets/mat/".ToLower()) && !uri.Contains("/assets/".ToLower()) && !uri.Contains(".jpg".ToLower()) && !uri.Contains(".png".ToLower()) && !uri.Contains(".bmt".ToLower()) && !uri.Contains(".jpg".ToLower()) && !uri.Contains("Scripts/".ToLower()) && !uri.Contains(".jpeg".ToLower()))
                {
                    
                    if (MatOnline.Areas.Supplier.MatOnlineSession.IsSignIn == false)
                    {
                        var url = Request.Url.AbsoluteUri.ToLower();
                        Response.Redirect("/Security/Login/Logout");
                    }
                }

                //put DetectMobileBrowsersCode Here, for exmaple if user agents contains apple , android , etc ...

                //if (b.IsMatch(u) || v.IsMatch(Left(u, 4)))
                //{
                //    Response.Redirect("http://m.yoursite.com");
                //}

            }
            catch (Exception ex)
            {

            }

        }

    }
}
