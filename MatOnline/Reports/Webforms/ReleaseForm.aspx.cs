﻿using MATFinancials.DAL;
using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace MatOnline.Reports.Webforms
{
    public partial class ReleaseForm : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            if(!IsPostBack)
            {
                decimal g = Convert.ToDecimal(Request.QueryString["qry"]);
                PopulateReport(g);
            }
        }

        private void PopulateReport(decimal param)
        {
            //using (DBMATAccounting_MagnetEntities dc = new DBMATAccounting_MagnetEntities())
            //{
            //    var v = (from a in dc.rpt_releaseformproc()
            //             select a);
            //    ReportDataSource rd = new ReportDataSource("ReleaseFormDataSet", v.ToList());
            //    ReportViewer1.LocalReport.DataSources.Add(rd);
            //    ReportViewer1.LocalReport.Refresh();
            //}
            DBMatConnection db = new DBMatConnection();

            DataTable dt = GetSPResult(param);
            ReportViewer1.Visible = true;
            ReportViewer1.Width = 800;
            var deliveryMasterInfo = new MATFinancials.DeliveryNoteMasterSP().DeliveryNoteMasterView(param);
            var storeId = db.getSingleValue(string.Format("select top 1 godownid from tbl_deliverynotedetails where deliverynotemasterid={0}",deliveryMasterInfo.DeliveryNoteMasterId));
            //var orderMasterInfo = new MATFinancials.SalesOrderMasterSP().SalesOrderMasterView(deliveryMasterInfo.OrderMasterId);
            string updateSalesOrderQuery = string.Format("UPDATE tbl_SalesOrderMaster SET authorizationstatus='Processed' WHERE salesOrderMasterId={0}", deliveryMasterInfo.OrderMasterId);
            db.customUpdateQuery(updateSalesOrderQuery);
            
            //if (db.customUpdateQuery(query) > 0)

            //string extra2Query = string.Format("SELECT extra2 FROM tbl_deliverynotemaster WHERE deliverynotemasterid={0}", param);
            //string userId = db.getSingleValue2(extra2Query);
            //if(userId=="")
            //{
            //    string loggedInUser = User.Identity.Name;
            //    string updateQuery = string.Format("UPDATE tbl_deliverynotemaster SET extra2='{1}' WHERE salesOrderMasterId={0}", param,loggedInUser);
            //    db.customUpdateQuery(updateQuery);
            //}

            string query = "SELECT RoleId FROM tbl_CycleActionPriviledge WHERE CycleAction='Warehouse Manager'";
            decimal warehouseManagerRoleId = Convert.ToDecimal(new DBMatConnection().getSingleValue(query));
            var userInfo = new MATFinancials.UserSP().UserView(Convert.ToDecimal(User.Identity.Name));

            //var generatedBy = new MATFinancials.UserSP().UserView(new MATFinancials.DeliveryNoteMasterSP().DeliveryNoteMasterView(param).UserId);
            var generatedBy = new MATFinancials.UserSP().UserView(Convert.ToDecimal(User.Identity.Name));

            string query2 = string.Format("SELECT * FROM tbl_user WHERE storeId='{0}' AND roleId={1}", generatedBy.StoreId, warehouseManagerRoleId);
            //var authorizedByUserInfo = new DBMatConnection().customSelect(query2).Rows[0].ItemArray;
            var authorizedByUserInfo = "";
            
            string processedQuery = string.Format("UPDATE tbl_deliverynotemaster SET status='Processed' WHERE deliverynotemasterid={0}", param);
            db.customUpdateQuery(processedQuery);

            ReportViewer1.LocalReport.ReportPath = "Reports\\Rdlc\\RptReleaseForm.rdlc";
            ReportViewer1.LocalReport.DataSources.Clear();
            ReportViewer1.LocalReport.EnableExternalImages = true;

            //ReportParameter p1 = new ReportParameter("AuthorizedBy", authorizedByUserInfo[3] + " " + authorizedByUserInfo[4], true);
            ReportParameter p1 = new ReportParameter("AuthorizedBy", ReportSignatories.signatoryName(storeId,8), true);
            string imagePath = new Uri(Server.MapPath(ReportSignatories.signaturePath(storeId, 8))).AbsoluteUri;
            ReportParameter p2 = new ReportParameter("AuthorizedBySignature", imagePath, true);
            var authorizedBy = new MATFinancials.UserSP().UserView(new MATFinancials.DeliveryNoteMasterSP().DeliveryNoteMasterView(param).UserId);
            ReportParameter p3 = new ReportParameter("DeliveredBy", ReportSignatories.signatoryName(storeId, 5), true);
            string imagePath2 = new Uri(Server.MapPath(ReportSignatories.signaturePath(storeId, 5))).AbsoluteUri;
            ReportParameter p4 = new ReportParameter("DeliveredBySignature", imagePath2, true);
            
            ReportParameter p5 = new ReportParameter("GeneratedBy", generatedBy.FirstName+" "+generatedBy.LastName, true);


            ReportViewer1.LocalReport.SetParameters(new ReportParameter[] { p1, p2, p3, p4,p5 });
            ReportViewer1.LocalReport.DataSources.Add(new ReportDataSource("RptReleaseFormDataset", dt));
            ReportViewer1.LocalReport.Refresh();

            //ReportParameter[] prms = new ReportParameter[1];
            //prms[0] = new ReportParameter("@Param1", "Alex Tosin", false);
            //ReportViewer1.LocalReport.SetParameters(prms);
        }

        private DataTable GetSPResult(decimal param)
        {
            DataTable ResultsTable = new DataTable();

            //SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["connectionString"].ConnectionString);
            DBMatConnection con = new DBMatConnection();
            try
            {
                SqlCommand cmd = new SqlCommand("rpt_releaseformproc", con.Connection);
                cmd.CommandType = CommandType.StoredProcedure;
                cmd.Parameters.AddWithValue("@deliveryNoteMasterId", param);
                SqlDataAdapter adapter = new SqlDataAdapter(cmd);
                adapter.Fill(ResultsTable);
            }

            catch (Exception ex)
            {
                Response.Write(ex.ToString());
            }
            finally
            {
                if (con.Connection != null)
                {
                    con.Connection.Close();
                }
            }

            return ResultsTable;
        }
    }

    public static class ReportSignatories
    {
        public static string signatoryName(string storeId,decimal roleId)
        {
            DBMatConnection db = new DBMatConnection();
            string query = string.Format("SELECT concat(firstname,' ',lastname) from tbl_user where roleid={0} and storeid='{1}'",roleId, storeId);
            string name = db.getSingleValue(query);
            return name;
        }

        public static string signaturePath(string storeId, decimal roleId)
        {
            DBMatConnection db = new DBMatConnection();
            string query = string.Format("SELECT userId from tbl_user where roleid={0} and storeid='{1}'",roleId, storeId);
            string name = db.getSingleValue(query);
            return "~/Content/Signatures/"+name+".jpg";
        }

        //public static string getAuthorizedBy(string storeId)
        //{
        //    DBMatConnection db = new DBMatConnection();
        //    string query = string.Format("SELECT concat(firstname,' ',lastname) from tbl_user where roleid=8 and storeid='{1}'", storeId);
        //    string name = db.getSingleValue(query);
        //    return name;
        //}

        //public static string getVerifiedBy(string storeId)
        //{
        //    DBMatConnection db = new DBMatConnection();
        //    string query = string.Format("SELECT concat(firstname,' ',lastname) from tbl_user where roleid=6 and storeid='{1}'", storeId);
        //    string name = db.getSingleValue(query);
        //    return name;
        //}

        //public static string getReleasedBy(string storeId)
        //{
        //    DBMatConnection db = new DBMatConnection();
        //    string query = string.Format("SELECT concat(firstname,' ',lastname) from tbl_user where roleid=5 and storeid='{1}'", storeId);
        //    string name = db.getSingleValue(query);
        //    return name;
        //}
    }
}