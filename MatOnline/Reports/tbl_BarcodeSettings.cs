//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MatOnline.Reports
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_BarcodeSettings
    {
        public decimal barcodeSettingsId { get; set; }
        public Nullable<bool> showProductCode { get; set; }
        public Nullable<bool> showCompanyName { get; set; }
        public string companyName { get; set; }
        public Nullable<bool> showPurchaseRate { get; set; }
        public Nullable<bool> showMRP { get; set; }
        public string point { get; set; }
        public string zero { get; set; }
        public string one { get; set; }
        public string two { get; set; }
        public string three { get; set; }
        public string four { get; set; }
        public string five { get; set; }
        public string six { get; set; }
        public string seven { get; set; }
        public string eight { get; set; }
        public string nine { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public Nullable<System.DateTime> extraDate { get; set; }
    }
}
