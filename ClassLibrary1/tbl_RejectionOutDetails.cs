//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClassLibrary1
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_RejectionOutDetails
    {
        public decimal rejectionOutDetailsId { get; set; }
        public Nullable<decimal> rejectionOutMasterId { get; set; }
        public Nullable<decimal> materialReceiptDetailsId { get; set; }
        public Nullable<decimal> productId { get; set; }
        public Nullable<decimal> qty { get; set; }
        public Nullable<decimal> rate { get; set; }
        public Nullable<decimal> unitId { get; set; }
        public Nullable<decimal> unitConversionId { get; set; }
        public Nullable<decimal> batchId { get; set; }
        public Nullable<decimal> godownId { get; set; }
        public Nullable<decimal> rackId { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<int> slno { get; set; }
        public Nullable<System.DateTime> extraDate { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public Nullable<int> ProjectId { get; set; }
        public Nullable<int> CategoryId { get; set; }
    }
}
