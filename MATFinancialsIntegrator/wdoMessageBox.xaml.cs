﻿
 

using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MATFinancialsSkate
{
    /// <summary>
    /// Interaction logic for wdoMessageBox.xaml
    /// </summary>
    public partial class wdoMessageBox : Window
    {
        #region Function
        /// <summary>
        /// Constructor
        /// </summary>
        public wdoMessageBox(string ss)
        {
            InitializeComponent();
            txtMessage.Text = ss;
            btnOk.Focus();
        }

        #endregion


        #region Events
              /// <summary>
        /// Event for Button OK click.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOk_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                this.Close();
            }

            catch (Exception ex)
            {
                System.Windows.Application.Current.Dispatcher.Invoke((Action)(() => new wdoMessageBox("WDOMB002" + ex.ToString()).ShowDialog()));
            }


        }
        #endregion
    }
}
