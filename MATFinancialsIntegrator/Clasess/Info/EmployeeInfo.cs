﻿
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MATFinancialsSkate
{
    class EmployeeInfo
    {
        public string employeeCode { get; set;}
        public string name { get; set;}
        public string eMail { get; set;}
        public string narration { get; set;}
        public string phone { get; set;}
        public string mobile { get; set;}
        public string address { get; set;}
        public string isActive { get; set; }
    }
}
