USE [DBMATAccounting_HisPro_Test]
GO
DELETE FROM [dbo].[TransactionAuditLog]
GO
DELETE FROM [dbo].[tbl_StockJournalMaster_Pending]
GO
DELETE FROM [dbo].[tbl_StockJournalMaster]
GO
DELETE FROM [dbo].[tbl_StockJournalDetails_Pending]
GO
DELETE FROM [dbo].[tbl_StockJournalDetails]
GO
DELETE FROM [dbo].[tbl_SalesReturnMaster_Pending]
GO
DELETE FROM [dbo].[tbl_SalesReturnDetails_Pending]
GO
DELETE FROM [dbo].[tbl_SalesQuotationMaster]
GO
DELETE FROM [dbo].[tbl_SalesQuotationDetails]
GO
DELETE FROM [dbo].[tbl_SalaryPackageDetails]
GO
DELETE FROM [dbo].[tbl_SalaryPackage]
GO
DELETE FROM [dbo].[tbl_Reminder]
GO
DELETE FROM [dbo].[tbl_RejectionInMaster_Pending]
GO
DELETE FROM [dbo].[tbl_RejectionInDetails_Pending]
GO
DELETE FROM [dbo].[tbl_ReceiptMaster]
GO
DELETE FROM [dbo].[tbl_ReceiptDetails]
GO
DELETE FROM [dbo].[tbl_PurchaseReturnBilltax]
GO
DELETE FROM [dbo].[tbl_PurchaseBillTax]
GO
DELETE FROM [dbo].[tbl_ProductGroup]
GO
DELETE FROM [dbo].[tbl_PhysicalStockMaster_Pending]
GO
DELETE FROM [dbo].[tbl_PhysicalStockMaster]
GO
DELETE FROM [dbo].[tbl_PhysicalStockDetails_Pending]
GO
DELETE FROM [dbo].[tbl_PhysicalStockDetails]
GO
DELETE FROM [dbo].[tbl_PendingSalesMaster]
GO
DELETE FROM [dbo].[tbl_PendingSalesDetails]
GO
DELETE FROM [dbo].[tbl_PaymentMaster]
GO
DELETE FROM [dbo].[tbl_PaymentDetails]
GO
DELETE FROM [dbo].[tbl_PartyBalance]
GO
DELETE FROM [dbo].[tbl_MonthlySalaryDetails]
GO
DELETE FROM [dbo].[tbl_MonthlySalary]
GO
DELETE FROM [dbo].[tbl_MaterialReceiptMaster_Pending]
GO
DELETE FROM [dbo].[tbl_MaterialReceiptDetails_Pending]
GO
DELETE FROM [dbo].[tbl_LedgerPosting]
GO
DELETE FROM [dbo].[tbl_JournalMaster]
GO
DELETE FROM [dbo].[tbl_JournalDetails]
GO
DELETE FROM [dbo].[tbl_Designation]
GO
DELETE FROM [dbo].[tbl_DebitNoteMaster]
GO
DELETE FROM [dbo].[tbl_DebitNoteDetails]
GO
DELETE FROM [dbo].[tbl_DailySalaryVoucherMaster]
GO
DELETE FROM [dbo].[tbl_DailySalaryVoucherDetails]
GO
DELETE FROM [dbo].[tbl_DailyAttendanceMaster]
GO
DELETE FROM [dbo].[tbl_DailyAttendanceDetails]
GO
DELETE FROM [dbo].[tbl_Counter]
GO
DELETE FROM [dbo].[tbl_ContraMaster]
GO
DELETE FROM [dbo].[tbl_ContraDetails]
GO
DELETE FROM [dbo].[tbl_BudgetMaster]
GO
DELETE FROM [dbo].[tbl_BudgetDetails]
GO
DELETE FROM [dbo].[tbl_BonusDeduction]
GO
DELETE FROM [dbo].[tbl_Batch]
GO
DELETE FROM [dbo].[tbl_AdvancePayment_Pending]
GO
DELETE FROM [dbo].[tbl_AdvancePayment]
GO
DELETE FROM [dbo].[tbl_AccruedExpensesMaster]
GO
DELETE FROM [dbo].[tbl_AccruedExpenseDetails]
GO
DELETE FROM [dbo].[tbl_StockPosting]
GO
DELETE FROM [dbo].[tbl_SalesReturnDetails]
GO
DELETE FROM [dbo].[tbl_SalesReturnMaster]
GO
DELETE FROM [dbo].[tbl_SalesOrderDetails]
GO
DELETE FROM [dbo].[tbl_SalesOrderMaster]
GO
DELETE FROM [dbo].[tbl_SalesDetails]
GO
DELETE FROM [dbo].[tbl_SalesMaster]
GO
DELETE FROM [dbo].[tbl_RejectionOutDetails]
GO
DELETE FROM [dbo].[tbl_RejectionOutMaster]
GO
DELETE FROM [dbo].[tbl_RejectionInDetails]
GO
DELETE FROM [dbo].[tbl_RejectionInMaster]
GO
DELETE FROM [dbo].[tbl_PurchaseReturnDetails]
GO
DELETE FROM [dbo].[tbl_PurchaseReturnMaster]
GO
DELETE FROM [dbo].[tbl_PurchaseOrderDetails]
GO
DELETE FROM [dbo].[tbl_PurchaseOrderMaster]
GO
DELETE FROM [dbo].[tbl_PurchaseDetails]
GO
DELETE FROM [dbo].[tbl_PurchaseMaster]
GO
DELETE FROM [dbo].[tbl_MaterialReceiptDetails]
GO
DELETE FROM [dbo].[tbl_MaterialReceiptMaster]
GO
DELETE FROM [dbo].[tbl_DeliveryNoteDetails]
GO
DELETE FROM [dbo].[tbl_DeliveryNoteMaster]
GO
DELETE FROM [dbo].[tbl_Product]
GO

DELETE FROM [dbo].[tbl_AccountLedger]
      WHERE accountGroupId = '26'
GO

DELETE FROM [dbo].[tbl_AccountLedger]
      WHERE accountGroupId = '22'

DELETE FROM [dbo].[tbl_AccountLedger]
      WHERE accountGroupId = '28'
GO 
SET IDENTITY_INSERT [dbo].[tbl_Counter] ON 
GO
INSERT [dbo].[tbl_Counter] ([counterId], [counterName], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(1 AS Numeric(18, 0)), N'All', N'', CAST(0x0000A6B101124402 AS DateTime), N'', N'')
GO
INSERT [dbo].[tbl_Counter] ([counterId], [counterName], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(2 AS Numeric(18, 0)), N'Bariga', N'', CAST(0x0000A6FD00D3C47C AS DateTime), N'', N'')
GO
INSERT [dbo].[tbl_Counter] ([counterId], [counterName], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(3 AS Numeric(18, 0)), N'Iyana Ipaja', N'', CAST(0x0000A6FD00D421E4 AS DateTime), N'', N'')
GO
SET IDENTITY_INSERT [dbo].[tbl_Counter] OFF

TRUNCATE TABLE [dbo].[tbl_Designation]
GO
SET IDENTITY_INSERT [dbo].[tbl_Designation] ON 
GO
INSERT [dbo].[tbl_Designation] ([designationId], [designationName], [leaveDays], [advanceAmount], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(1 AS Numeric(18, 0)), N'Salesman', CAST(0.00 AS Decimal(18, 2)), CAST(50000.00000 AS Decimal(18, 5)), N'', CAST(N'2018-05-08T09:15:29.080' AS DateTime), N'', N'')
GO
INSERT [dbo].[tbl_Designation] ([designationId], [designationName], [leaveDays], [advanceAmount], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(2 AS Numeric(18, 0)), N'Credit Controller', CAST(0.00 AS Decimal(18, 2)), CAST(50000.00000 AS Decimal(18, 5)), N'', CAST(N'2018-05-08T09:15:37.997' AS DateTime), N'', N'')
GO
INSERT [dbo].[tbl_Designation] ([designationId], [designationName], [leaveDays], [advanceAmount], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(20009 AS Numeric(18, 0)), N'Corporate officer', CAST(3.00 AS Decimal(18, 2)), CAST(0.00000 AS Decimal(18, 5)), N'', CAST(N'2017-07-02T18:42:57.883' AS DateTime), N'', N'')
GO
INSERT [dbo].[tbl_Designation] ([designationId], [designationName], [leaveDays], [advanceAmount], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(20010 AS Numeric(18, 0)), N'Admin', CAST(0.00 AS Decimal(18, 2)), CAST(150000.00000 AS Decimal(18, 5)), N'', CAST(N'2018-05-08T09:03:21.533' AS DateTime), N'', N'')
GO
INSERT [dbo].[tbl_Designation] ([designationId], [designationName], [leaveDays], [advanceAmount], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(20011 AS Numeric(18, 0)), N'JUNIOR STAFF', CAST(1.00 AS Decimal(18, 2)), CAST(98000.00000 AS Decimal(18, 5)), N'', CAST(N'2018-05-03T10:53:29.447' AS DateTime), N'', N'')
GO
INSERT [dbo].[tbl_Designation] ([designationId], [designationName], [leaveDays], [advanceAmount], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(20013 AS Numeric(18, 0)), N'Jun Software Developer', CAST(1.00 AS Decimal(18, 2)), CAST(97630.00000 AS Decimal(18, 5)), N'', CAST(N'2018-05-03T10:54:47.267' AS DateTime), N'', N'')
GO
INSERT [dbo].[tbl_Designation] ([designationId], [designationName], [leaveDays], [advanceAmount], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(20015 AS Numeric(18, 0)), N'Senior Internal Auditor ', CAST(0.00 AS Decimal(18, 2)), CAST(0.00000 AS Decimal(18, 5)), N'', CAST(N'2019-08-22T10:44:06.807' AS DateTime), N'', N'')
GO
INSERT [dbo].[tbl_Designation] ([designationId], [designationName], [leaveDays], [advanceAmount], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(20016 AS Numeric(18, 0)), N'SNR DEVELOPER', CAST(5.00 AS Decimal(18, 2)), CAST(50000.00000 AS Decimal(18, 5)), N'', CAST(N'2019-10-16T11:15:26.707' AS DateTime), N'', N'')
GO
SET IDENTITY_INSERT [dbo].[tbl_Designation] OFF
GO


TRUNCATE TABLE [dbo].[tbl_PayHead]
GO
INSERT [dbo].[tbl_PayHead] ([payHeadName], [type], [narration], [extraDate], [extra1], [extra2], [ReceivingLedgerId]) VALUES (N'Transport Allowance', N'Addition', N'', CAST(0x0000A6F900BABFD0 AS DateTime), N'', N'', 4)
GO
INSERT [dbo].[tbl_PayHead] ([payHeadName], [type], [narration], [extraDate], [extra1], [extra2], [ReceivingLedgerId]) VALUES (N'Housing Allowance', N'Addition', N'', CAST(0x0000A6FF00F97D88 AS DateTime), N'', N'', 4)
GO
INSERT [dbo].[tbl_PayHead] ([payHeadName], [type], [narration], [extraDate], [extra1], [extra2], [ReceivingLedgerId]) VALUES (N'Medical Allowance', N'Addition', N'', CAST(0x0000A70400CEC46C AS DateTime), N'', N'', 4)
GO
INSERT [dbo].[tbl_PayHead] ([payHeadName], [type], [narration], [extraDate], [extra1], [extra2], [ReceivingLedgerId]) VALUES (N'Utilities', N'Addition', N'', CAST(0x0000A7070100AE8C AS DateTime), N'', N'', 4)
GO
INSERT [dbo].[tbl_PayHead] ([payHeadName], [type], [narration], [extraDate], [extra1], [extra2], [ReceivingLedgerId]) VALUES (N'Permded', N'Deduction', N'', CAST(0x0000A75300E8AAE4 AS DateTime), N'', N'', 4)
GO
INSERT [dbo].[tbl_PayHead] ([payHeadName], [type], [narration], [extraDate], [extra1], [extra2], [ReceivingLedgerId]) VALUES (N'Basic Salary', N'Addition', N'', CAST(0x0000AAB100C82075 AS DateTime), N'', N'', 4)
GO
