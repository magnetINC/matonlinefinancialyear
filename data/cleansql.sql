
--ensure you are on the right database this operation cannot be reversed 
use [DBMATAccounting_HisPro_Test]
GO
DELETE FROM [dbo].[tbl_StockJournalMaster_Pending]
GO
DELETE FROM [dbo].[tbl_StockJournalMaster]
GO
DELETE FROM [dbo].[tbl_StockJournalDetails_Pending]
GO
DELETE FROM [dbo].[tbl_StockJournalDetails]
GO
DELETE FROM [dbo].[tbl_SalesReturnMaster_Pending]
GO
DELETE FROM [dbo].[tbl_SalesReturnMaster]
GO
DELETE FROM [dbo].[tbl_SalesReturnDetails_Pending]
GO
DELETE FROM [dbo].[tbl_SalesReturnDetails]
GO
DELETE FROM [dbo].[tbl_SalesQuotationMaster]
GO
DELETE FROM [dbo].[tbl_SalesQuotationDetails]
GO
DELETE FROM [dbo].[tbl_SalesOrderMaster]
GO
DELETE FROM [dbo].[tbl_SalesOrderDetails]
GO
DELETE FROM [dbo].[tbl_SalesMaster]
GO
DELETE FROM [dbo].[tbl_SalesDetails]
GO
DELETE FROM [dbo].[tbl_SalaryPackageDetails]
GO
DELETE FROM [dbo].[tbl_SalaryPackage]
GO
DELETE FROM [dbo].[tbl_RejectionOutMaster]
GO
DELETE FROM [dbo].[tbl_RejectionOutDetails]
GO
DELETE FROM [dbo].[tbl_RejectionInMaster_Pending]
GO
DELETE FROM [dbo].[tbl_RejectionInMaster]
GO
DELETE FROM [dbo].[tbl_RejectionInDetails_Pending]
GO
DELETE FROM [dbo].[tbl_RejectionInDetails]
GO
DELETE FROM [dbo].[tbl_ReceiptMaster]
GO
DELETE FROM [dbo].[tbl_ReceiptDetails]
GO
DELETE FROM [dbo].[tbl_PurchaseReturnMaster]
GO
DELETE FROM [dbo].[tbl_PurchaseReturnDetails]
GO
DELETE FROM [dbo].[tbl_PurchaseReturnBilltax]
GO
DELETE FROM [dbo].[tbl_PurchaseOrderMaster]
GO
DELETE FROM [dbo].[tbl_PurchaseOrderDetails]
GO
DELETE FROM [dbo].[tbl_PurchaseMaster]
GO
DELETE FROM [dbo].[tbl_PurchaseDetails]
GO
DELETE FROM [dbo].[tbl_PurchaseBillTax]
GO
DELETE FROM [dbo].[tbl_ProductGroup]
GO
DELETE FROM [dbo].[tbl_Product]
GO
DELETE FROM [dbo].[tbl_PhysicalStockMaster_Pending]
GO
DELETE FROM [dbo].[tbl_PhysicalStockMaster]
GO
DELETE FROM [dbo].[tbl_PhysicalStockDetails_Pending]
GO
DELETE FROM [dbo].[tbl_PhysicalStockDetails]
GO
DELETE FROM [dbo].[tbl_PendingSalesMaster]
GO
DELETE FROM [dbo].[tbl_PDCReceivableMaster]
GO
DELETE FROM [dbo].[tbl_PaymentMaster]
GO
DELETE FROM [dbo].[tbl_PaymentDetails]
GO
DELETE FROM [dbo].[tbl_PartyBalance]
GO
DELETE FROM [dbo].[tbl_MonthlySalaryDetails]
GO
DELETE FROM [dbo].[tbl_MonthlySalary]
GO
DELETE FROM [dbo].[tbl_MaterialReceiptMaster_Pending]
GO
DELETE FROM [dbo].[tbl_MaterialReceiptMaster]
GO
DELETE FROM [dbo].[tbl_MaterialReceiptDetails_Pending]
GO
DELETE FROM [dbo].[tbl_MaterialReceiptDetails]
GO
DELETE FROM [dbo].[tbl_LedgerPosting]
GO
DELETE FROM [dbo].[tbl_JournalMaster]
GO
DELETE FROM [dbo].[tbl_JournalDetails]
GO
DELETE FROM [dbo].[tbl_Designation]
GO
DELETE FROM [dbo].[tbl_DeliveryNoteMaster]
GO
DELETE FROM [dbo].[tbl_DeliveryNoteDetails]
GO
DELETE FROM [dbo].[tbl_DebitNoteMaster]
GO
DELETE FROM [dbo].[tbl_DebitNoteDetails]
GO
DELETE FROM [dbo].[tbl_DailySalaryVoucherMaster]
GO
DELETE FROM [dbo].[tbl_DailySalaryVoucherDetails]
GO
DELETE FROM [dbo].[tbl_DailyAttendanceMaster]
GO
DELETE FROM [dbo].[tbl_DailyAttendanceDetails]
GO
DELETE FROM [dbo].[tbl_Counter]
GO
DELETE FROM [dbo].[tbl_ContraMaster]
GO
DELETE FROM [dbo].[tbl_ContraDetails]
GO
DELETE FROM [dbo].[tbl_BudgetMaster]
GO
DELETE FROM [dbo].[tbl_BudgetDetails]
GO
DELETE FROM [dbo].[tbl_BonusDeduction]
GO
DELETE FROM [dbo].[tbl_BankReconciliation]
GO
DELETE FROM [dbo].[tbl_AdvancePayment_Pending]
GO
DELETE FROM [dbo].[tbl_AdvancePayment]
GO
DELETE FROM [dbo].[tbl_AccruedExpensesMaster]
GO
DELETE FROM [dbo].[tbl_AccruedExpenseDetails]
GO
DELETE FROM [dbo].[tbl_Employee]
    
GO

DELETE FROM [dbo].[tbl_AccountLedger]
      WHERE accountGroupId = '26'
GO

DELETE FROM [dbo].[tbl_AccountLedger]
      WHERE accountGroupId = '22'
GO 

DELETE FROM [dbo].[tbl_StockPosting]
   
GO


DELETE FROM [dbo].[tbl_Batch]
    

 --ddd new  
 GO
SET IDENTITY_INSERT [dbo].[tbl_Counter] ON

GO
INSERT [dbo].[tbl_Counter] ([counterId], [counterName], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(1 AS Numeric(18, 0)), N'All', N'', CAST(0x0000A6B101124402 AS DateTime), N'', N'')
GO
INSERT [dbo].[tbl_Counter] ([counterId], [counterName], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(2 AS Numeric(18, 0)), N'Bariga', N'', CAST(0x0000A6FD00D3C47C AS DateTime), N'', N'')
GO
INSERT [dbo].[tbl_Counter] ([counterId], [counterName], [narration], [extraDate], [extra1], [extra2]) VALUES (CAST(3 AS Numeric(18, 0)), N'Iyana Ipaja', N'', CAST(0x0000A6FD00D421E4 AS DateTime), N'', N'')
GO

SET IDENTITY_INSERT [dbo].[tbl_Counter] OFF
GO


DELETE FROM [dbo].[tbl_Tax] WHERE taxId = 10012 OR taxId = 10013
TRUNCATE TABLE .[dbo].[tbl_Brand]
TRUNCATE TABLE [dbo].[tbl_Tax]
TRUNCATE TABLE .[dbo].[tbl_Size]

Go
UPDATE [dbo].[tbl_AccountLedger] SET billByBill = 1 WHERE accountGroupId = 22 OR accountGroupId = 26

GO
INSERT [dbo].[tbl_Tax] ([taxName], [applicableOn], [rate], [calculatingMode], [narration], [isActive], [extraDate], [extra1], [extra2], [type]) VALUES ( 'NA1', 'NA1', CAST(0.00000 AS Decimal(18, 5)), NULL, NULL, 1, NULL, NULL, NULL, NULL)
GO

Go
 INSERT INTO [dbo].[tbl_Tax] ([taxName], [applicableOn], [rate], [calculatingMode], [narration], [isActive], [extraDate], [extra1], [extra2], [type]) VALUES (N'VAT', N'Product', CAST(5.00000 AS Decimal(18, 5)), NULL, NULL, 1, CAST(N'2017-09-12T00:00:00.000' AS DateTime), NULL, NULL, NULL)
 GO
-- Add a new column to the categorytable but check if it exist 
IF COL_LENGTH('tbl_Category', 'BelongsToCategoryId') IS NULL
BEGIN
    ALTER TABLE tbl_Category
    ADD [BelongsToCategoryId] INT
END

Go
DELETE FROM [dbo].[tbl_Bom]
    
GO
