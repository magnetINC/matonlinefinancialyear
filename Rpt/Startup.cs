﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(Rpt.Startup))]
namespace Rpt
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
