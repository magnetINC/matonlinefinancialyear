﻿
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using MATFinancials.MATFinancialsIChart.DataSources;

namespace MATFinancials.MATFinancialsIChart
{
	public interface IOneDimensionalChart
	{
		IPointDataSource DataSource { get; set; }
		event EventHandler DataChanged;
	}
}
