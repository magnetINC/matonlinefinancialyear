﻿
 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;

namespace MATFinancialsI.DAL
{
    public class CompanyNameSP : DBConnection
    {
        public String GetCompanyName()
        {
            if (sqlCon.State == System.Data.ConnectionState.Closed)
            {
                sqlCon.Open();
            }
            SqlCommand sqlCmd = new SqlCommand("MiracleI_SelectCompanyName", sqlCon);
            sqlCmd.CommandType = System.Data.CommandType.StoredProcedure;
            return Convert.ToString((sqlCmd.ExecuteScalar() != null) ? sqlCmd.ExecuteScalar() : string.Empty);
        }
    }
}
