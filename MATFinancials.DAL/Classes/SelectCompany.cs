﻿

 
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using Entity;

namespace MATFinancialsI.DAL
{
   public class SelectCompany:DBConnection
    {
        public List<DataTable> GetCompanyNames()
        {
            List<DataTable> dataTableList = new List<DataTable>();
            if (ConnectionString.IsConnectionTrue)
            {
                DataTable dtbl = new DataTable();
               
                SqlDataAdapter sqlDa = new SqlDataAdapter("CompanyViewAllForSelectCompany", sqlCon);
                sqlDa.SelectCommand.CommandType = CommandType.StoredProcedure;
                sqlDa.Fill(dtbl);
                dataTableList.Add(dtbl);
            }
            return dataTableList;
        }
    }
}
