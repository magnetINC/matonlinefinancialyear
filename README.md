# README #
MatOnline Erp Software with Accounting, Payroll, and Inventory management module
### Software  Requirement ###
* DotNet Version 4.6.1 and above, 
* Visual Studio V2019 and above,
* SQL server 2012
### How do I get project runing ###
* Clone the project to your local machine
* Create a database eg MatonlineDb
* You need to run initialize script, to create tables, procedures, views and function as well as to seed the database with record, script file can be found under data folder named **MatonlineCreateDbScript.sql
* Configure and update connection strings in web.config file inside matonline web project and api project  (folders: matOnline and matApi), also navigate to MatClassLibrary/Classes/General/DBConnection.cs file and update the connection string found inside the constructor method DBConnection() 

* Set Multiple Start up project to matApi and MatOnline 
* Run the program , if everthing goes well use admin as user name and password to login

### Knowledge Required to contribute ###

* C#
* EntityFramwork
* Linq 
* Sql
* Html 
* Javascript

### Guildliness to contribute ###
 
 * write clean and readable code
 * try keep the project updated with latest technology and procedures 
 * When coding, code in a manner that other can easily debug your code. 
