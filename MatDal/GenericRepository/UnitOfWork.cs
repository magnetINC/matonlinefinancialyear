﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MatDal.GenericRepository
{
    
    public class UnitOfWork : IDisposable
    {
        private DBMATAccounting_MagnetEntities context = new DBMATAccounting_MagnetEntities();
        private GenericRepository<tbl_SalesOrderMaster> salesOrderMasterRepository;
        private GenericRepository<tbl_AccountLedger> accountLedgerRepository;
        private GenericRepository<tbl_SalesOrderDetails> saleOrderDetailsRepository;
        private GenericRepository<tbl_Product> producRepository;
        private GenericRepository<tbl_User> userRepository;

        public GenericRepository<tbl_SalesOrderMaster> SalesOrderMasterRepository
        {
            get
            {

                if (this.salesOrderMasterRepository == null)
                {
                    this.salesOrderMasterRepository = new GenericRepository<tbl_SalesOrderMaster>(context);
                }
                return salesOrderMasterRepository;
            }
        }

        public GenericRepository<tbl_SalesOrderDetails> SalesOrderDetailRepository
        {
            get
            {

                if (this.saleOrderDetailsRepository == null)
                {
                    this.saleOrderDetailsRepository = new GenericRepository<tbl_SalesOrderDetails>(context);
                }

                return saleOrderDetailsRepository;
            }
        } 
        public GenericRepository<tbl_AccountLedger> AccountLedgerRepository
        {
            get
            {

                if (this.accountLedgerRepository == null)
                {
                    this.accountLedgerRepository = new GenericRepository<tbl_AccountLedger>(context);
                }

                return accountLedgerRepository;
            }
        }
        public GenericRepository<tbl_Product> ProductRepository
        {
            get
            {

                if (this.accountLedgerRepository == null)
                {
                    this.producRepository = new GenericRepository<tbl_Product>(context);
                }

                return producRepository;
            }
        }
        public GenericRepository<tbl_User> UserRepository
        {
            get
            {

                if (this.userRepository == null)
                {
                    this.userRepository = new GenericRepository<tbl_User>(context);
                }

                return userRepository;
            }
        }
        public void Save()
        {
            context.SaveChanges();
        }

        private bool disposed = false;

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {
                    context.Dispose();
                }
            }
            this.disposed = true;
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }
    }
}
