//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MatDal
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_emailConfig
    {
        public int emailConfigurationId { get; set; }
        public string emailID { get; set; }
        public string logonPassword { get; set; }
        public string IsDefault { get; set; }
        public string smtp { get; set; }
        public string portNumber { get; set; }
        public Nullable<bool> enableSSL { get; set; }
    }
}
