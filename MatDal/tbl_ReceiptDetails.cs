//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace MatDal
{
    using System;
    using System.Collections.Generic;
    
    public partial class tbl_ReceiptDetails
    {
        public decimal receiptDetailsId { get; set; }
        public Nullable<decimal> receiptMasterId { get; set; }
        public Nullable<decimal> ledgerId { get; set; }
        public Nullable<decimal> amount { get; set; }
        public Nullable<decimal> exchangeRateId { get; set; }
        public string chequeNo { get; set; }
        public Nullable<System.DateTime> chequeDate { get; set; }
        public Nullable<System.DateTime> extraDate { get; set; }
        public string extra1 { get; set; }
        public string extra2 { get; set; }
        public Nullable<int> ProjectId { get; set; }
        public Nullable<int> CategoryId { get; set; }
        public Nullable<decimal> withholdingTaxId { get; set; }
        public Nullable<decimal> withholdingTaxAmount { get; set; }
        public Nullable<decimal> grossAmount { get; set; }
        public string invoiceNo { get; set; }
    }
}
